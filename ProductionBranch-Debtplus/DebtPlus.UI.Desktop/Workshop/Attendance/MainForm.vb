#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}
#End Region

Option Compare Binary
Option Explicit On 
Option Strict On

Namespace Workshop.Attendance
    Friend Class MainForm
        Inherits DebtPlus.Data.Forms.DebtPlusForm

        Friend ap As ArgParser = Nothing
        Public Sub New(ByVal ap As ArgParser)
            MyClass.New()
            Me.ap = ap
            AddHandler Me.Load, AddressOf MainForm_Load
            AddHandler Page_Select1.Completed, AddressOf SelectWorkshop1_Completed
            AddHandler Page_Attendance1.PreviousPage, AddressOf Page_Attendance1_PreviousPage
        End Sub

#Region " Windows Form Designer generated code "

        Public Sub New()
            MyBase.New()

            'This call is required by the Windows Form Designer.
            InitializeComponent()

            'Add any initialization after the InitializeComponent() call

        End Sub

        'Form overrides dispose to clean up the component list.
        Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
            If disposing Then
                If components IsNot Nothing Then
                    components.Dispose()
                End If
            End If
            MyBase.Dispose(disposing)
        End Sub

        'Required by the Windows Form Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Windows Form Designer
        'It can be modified using the Windows Form Designer.  
        'Do not modify it using the code editor.
        Friend WithEvents Page_Select1 As Page_Select
        Friend WithEvents Page_Attendance1 As Page_Attendance
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
            Me.Page_Select1 = New Page_Select
            Me.Page_Attendance1 = New Page_Attendance

            Me.SuspendLayout()

            '
            'Page_Select1
            '
            Me.Page_Select1.Appearance.BackColor = System.Drawing.SystemColors.Window
            Me.Page_Select1.Appearance.Options.UseBackColor = True
            Me.Page_Select1.Dock = System.Windows.Forms.DockStyle.Fill
            Me.Page_Select1.Location = New System.Drawing.Point(0, 0)
            Me.Page_Select1.Name = "Page_Select1"
            Me.Page_Select1.Size = New System.Drawing.Size(496, 310)
            Me.ToolTipController1.SetSuperTip(Me.Page_Select1, Nothing)
            Me.Page_Select1.TabIndex = 0
            '
            'Page_Attendance1
            '
            Me.Page_Attendance1.Dock = System.Windows.Forms.DockStyle.Fill
            Me.Page_Attendance1.Location = New System.Drawing.Point(0, 0)
            Me.Page_Attendance1.Name = "Page_Attendance1"
            Me.Page_Attendance1.Size = New System.Drawing.Size(496, 310)
            Me.ToolTipController1.SetSuperTip(Me.Page_Attendance1, Nothing)
            Me.Page_Attendance1.TabIndex = 1
            Me.Page_Attendance1.Workshop = -1
            '
            'MainForm
            '
            Me.AutoScaleBaseSize = New System.Drawing.Size(5, 14)
            Me.ClientSize = New System.Drawing.Size(496, 310)
            Me.Controls.Add(Me.Page_Select1)
            Me.Controls.Add(Me.Page_Attendance1)
            Me.Name = "MainForm"
            Me.ToolTipController1.SetSuperTip(Me, Nothing)
            Me.Text = "Update Workshop Attendance"

            Me.ResumeLayout(False)

        End Sub

#End Region

        Private Sub MainForm_Load(ByVal sender As Object, ByVal e As System.EventArgs)
            With Page_Attendance1
                .Visible = False
            End With

            With Page_Select1
                .Visible = True
                .Process()
            End With
        End Sub

        Private Sub SelectWorkshop1_Completed(ByVal Sender As Object, ByVal Workshop As System.Int32)

            With Page_Select1
                .Visible = False
            End With

            With Page_Attendance1
                .Workshop = Workshop
                .Visible = True
                .Process()
            End With
        End Sub

        Private Sub Page_Attendance1_PreviousPage(ByVal sender As Object, ByVal e As System.EventArgs)
            With Page_Attendance1
                .Visible = False
            End With

            With Page_Select1
                .Visible = True
                .Process()
            End With
        End Sub
    End Class
End Namespace
