#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}
#End Region

Option Compare Binary
Option Explicit On 
Option Strict On

Imports System.Data.SqlClient
Imports DebtPlus.Interfaces
Imports System.Windows.Forms
Imports System.Drawing

Namespace Workshop.Attendance
    Friend Class Page_Select
        Inherits DevExpress.XtraEditors.XtraUserControl
        Public Event Completed(ByVal Sender As Object, ByVal Workshop As System.Int32)
        Private Workshop As System.Int32 = -1

        Public Sub New()
            MyBase.New()
            InitializeComponent()
            AddHandler Button_Cancel.Click, AddressOf Button_Cancel_Click
            AddHandler Button_OK.Click, AddressOf Button_OK_Click
            AddHandler GridView1.FocusedRowChanged, AddressOf GridView1_FocusedRowChanged
            AddHandler GridView1.DoubleClick, AddressOf GridView1_DoubleClick
            AddHandler GridView1.MouseDown, AddressOf GridView1_MouseDown
            AddHandler Me.Load, AddressOf SelectWorkshop_Load
        End Sub

#Region " Windows Form Designer generated code "

        'UserControl overrides dispose to clean up the component list.
        Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
            If disposing Then
                If components IsNot Nothing Then
                    components.Dispose()
                End If
            End If
            MyBase.Dispose(disposing)
        End Sub

        'Required by the Windows Form Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Windows Form Designer
        'It can be modified using the Windows Form Designer.  
        'Do not modify it using the code editor.
        Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents GridControl1 As DevExpress.XtraGrid.GridControl
        Friend WithEvents GridView1 As DevExpress.XtraGrid.Views.Grid.GridView
        Friend WithEvents Button_OK As DevExpress.XtraEditors.SimpleButton
        Friend WithEvents Button_Cancel As DevExpress.XtraEditors.SimpleButton
        Friend WithEvents GridColumn_workshop As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents GridColumn_time As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents GridColumn_location As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents GridColumn_description As DevExpress.XtraGrid.Columns.GridColumn
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
            Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl
            Me.GridControl1 = New DevExpress.XtraGrid.GridControl
            Me.GridView1 = New DevExpress.XtraGrid.Views.Grid.GridView
            Me.GridColumn_workshop = New DevExpress.XtraGrid.Columns.GridColumn
            Me.GridColumn_workshop.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True
            Me.GridColumn_time = New DevExpress.XtraGrid.Columns.GridColumn
            Me.GridColumn_time.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True
            Me.GridColumn_location = New DevExpress.XtraGrid.Columns.GridColumn
            Me.GridColumn_location.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True
            Me.GridColumn_description = New DevExpress.XtraGrid.Columns.GridColumn
            Me.GridColumn_description.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True
            Me.Button_OK = New DevExpress.XtraEditors.SimpleButton
            Me.Button_Cancel = New DevExpress.XtraEditors.SimpleButton
            CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.GridView1, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.SuspendLayout()
            '
            'LabelControl1
            '
            Me.LabelControl1.Location = New System.Drawing.Point(8, 8)
            Me.LabelControl1.Name = "LabelControl1"
            Me.LabelControl1.Size = New System.Drawing.Size(384, 14)
            Me.LabelControl1.TabIndex = 1
            Me.LabelControl1.Text = "Select the workshop to be used"
            '
            'GridControl1
            '
            Me.GridControl1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                        Or System.Windows.Forms.AnchorStyles.Left) _
                        Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            '
            'GridControl1.EmbeddedNavigator
            '
            Me.GridControl1.EmbeddedNavigator.Name = ""
            Me.GridControl1.Location = New System.Drawing.Point(0, 32)
            Me.GridControl1.MainView = Me.GridView1
            Me.GridControl1.Name = "GridControl1"
            Me.GridControl1.Size = New System.Drawing.Size(400, 144)
            Me.GridControl1.TabIndex = 1
            Me.GridControl1.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridView1})
            '
            'GridView1
            '
            Me.GridView1.Appearance.ColumnFilterButton.BackColor = System.Drawing.Color.FromArgb(CType(104, Byte), CType(184, Byte), CType(251, Byte))
            Me.GridView1.Appearance.ColumnFilterButton.BorderColor = System.Drawing.Color.FromArgb(CType(104, Byte), CType(184, Byte), CType(251, Byte))
            Me.GridView1.Appearance.ColumnFilterButton.ForeColor = System.Drawing.Color.White
            Me.GridView1.Appearance.ColumnFilterButton.Options.UseBackColor = True
            Me.GridView1.Appearance.ColumnFilterButton.Options.UseBorderColor = True
            Me.GridView1.Appearance.ColumnFilterButton.Options.UseForeColor = True
            Me.GridView1.Appearance.ColumnFilterButtonActive.BackColor = System.Drawing.Color.FromArgb(CType(170, Byte), CType(216, Byte), CType(254, Byte))
            Me.GridView1.Appearance.ColumnFilterButtonActive.BorderColor = System.Drawing.Color.FromArgb(CType(170, Byte), CType(216, Byte), CType(254, Byte))
            Me.GridView1.Appearance.ColumnFilterButtonActive.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.ColumnFilterButtonActive.Options.UseBackColor = True
            Me.GridView1.Appearance.ColumnFilterButtonActive.Options.UseBorderColor = True
            Me.GridView1.Appearance.ColumnFilterButtonActive.Options.UseForeColor = True
            Me.GridView1.Appearance.Empty.BackColor = System.Drawing.Color.FromArgb(CType(236, Byte), CType(246, Byte), CType(255, Byte))
            Me.GridView1.Appearance.Empty.BackColor2 = System.Drawing.Color.White
            Me.GridView1.Appearance.Empty.Options.UseBackColor = True
            Me.GridView1.Appearance.EvenRow.BackColor = System.Drawing.Color.FromArgb(CType(247, Byte), CType(251, Byte), CType(255, Byte))
            Me.GridView1.Appearance.EvenRow.BorderColor = System.Drawing.Color.FromArgb(CType(247, Byte), CType(251, Byte), CType(255, Byte))
            Me.GridView1.Appearance.EvenRow.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.EvenRow.Options.UseBackColor = True
            Me.GridView1.Appearance.EvenRow.Options.UseBorderColor = True
            Me.GridView1.Appearance.EvenRow.Options.UseForeColor = True
            Me.GridView1.Appearance.FilterCloseButton.BackColor = System.Drawing.Color.FromArgb(CType(104, Byte), CType(184, Byte), CType(251, Byte))
            Me.GridView1.Appearance.FilterCloseButton.BorderColor = System.Drawing.Color.FromArgb(CType(104, Byte), CType(184, Byte), CType(251, Byte))
            Me.GridView1.Appearance.FilterCloseButton.ForeColor = System.Drawing.Color.White
            Me.GridView1.Appearance.FilterCloseButton.Options.UseBackColor = True
            Me.GridView1.Appearance.FilterCloseButton.Options.UseBorderColor = True
            Me.GridView1.Appearance.FilterCloseButton.Options.UseForeColor = True
            Me.GridView1.Appearance.FilterPanel.BackColor = System.Drawing.Color.FromArgb(CType(236, Byte), CType(246, Byte), CType(255, Byte))
            Me.GridView1.Appearance.FilterPanel.BackColor2 = System.Drawing.Color.White
            Me.GridView1.Appearance.FilterPanel.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.FilterPanel.Options.UseBackColor = True
            Me.GridView1.Appearance.FilterPanel.Options.UseForeColor = True
            Me.GridView1.Appearance.FixedLine.BackColor = System.Drawing.Color.FromArgb(CType(59, Byte), CType(133, Byte), CType(195, Byte))
            Me.GridView1.Appearance.FixedLine.Options.UseBackColor = True
            Me.GridView1.Appearance.FocusedCell.BackColor = System.Drawing.Color.White
            Me.GridView1.Appearance.FocusedCell.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.FocusedCell.Options.UseBackColor = True
            Me.GridView1.Appearance.FocusedCell.Options.UseForeColor = True
            Me.GridView1.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb(CType(38, Byte), CType(109, Byte), CType(189, Byte))
            Me.GridView1.Appearance.FocusedRow.BorderColor = System.Drawing.Color.FromArgb(CType(59, Byte), CType(139, Byte), CType(206, Byte))
            Me.GridView1.Appearance.FocusedRow.ForeColor = System.Drawing.Color.White
            Me.GridView1.Appearance.FocusedRow.Options.UseBackColor = True
            Me.GridView1.Appearance.FocusedRow.Options.UseBorderColor = True
            Me.GridView1.Appearance.FocusedRow.Options.UseForeColor = True
            Me.GridView1.Appearance.FooterPanel.BackColor = System.Drawing.Color.FromArgb(CType(104, Byte), CType(184, Byte), CType(251, Byte))
            Me.GridView1.Appearance.FooterPanel.BorderColor = System.Drawing.Color.FromArgb(CType(104, Byte), CType(184, Byte), CType(251, Byte))
            Me.GridView1.Appearance.FooterPanel.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.FooterPanel.Options.UseBackColor = True
            Me.GridView1.Appearance.FooterPanel.Options.UseBorderColor = True
            Me.GridView1.Appearance.FooterPanel.Options.UseForeColor = True
            Me.GridView1.Appearance.GroupButton.BackColor = System.Drawing.Color.FromArgb(CType(104, Byte), CType(184, Byte), CType(251, Byte))
            Me.GridView1.Appearance.GroupButton.BorderColor = System.Drawing.Color.FromArgb(CType(104, Byte), CType(184, Byte), CType(251, Byte))
            Me.GridView1.Appearance.GroupButton.Options.UseBackColor = True
            Me.GridView1.Appearance.GroupButton.Options.UseBorderColor = True
            Me.GridView1.Appearance.GroupFooter.BackColor = System.Drawing.Color.FromArgb(CType(170, Byte), CType(216, Byte), CType(254, Byte))
            Me.GridView1.Appearance.GroupFooter.BorderColor = System.Drawing.Color.FromArgb(CType(170, Byte), CType(216, Byte), CType(254, Byte))
            Me.GridView1.Appearance.GroupFooter.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.GroupFooter.Options.UseBackColor = True
            Me.GridView1.Appearance.GroupFooter.Options.UseBorderColor = True
            Me.GridView1.Appearance.GroupFooter.Options.UseForeColor = True
            Me.GridView1.Appearance.GroupPanel.BackColor = System.Drawing.Color.FromArgb(CType(236, Byte), CType(246, Byte), CType(255, Byte))
            Me.GridView1.Appearance.GroupPanel.BackColor2 = System.Drawing.Color.White
            Me.GridView1.Appearance.GroupPanel.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.GroupPanel.Options.UseBackColor = True
            Me.GridView1.Appearance.GroupPanel.Options.UseForeColor = True
            Me.GridView1.Appearance.GroupRow.BackColor = System.Drawing.Color.FromArgb(CType(170, Byte), CType(216, Byte), CType(254, Byte))
            Me.GridView1.Appearance.GroupRow.BorderColor = System.Drawing.Color.FromArgb(CType(170, Byte), CType(216, Byte), CType(254, Byte))
            Me.GridView1.Appearance.GroupRow.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.GroupRow.Options.UseBackColor = True
            Me.GridView1.Appearance.GroupRow.Options.UseBorderColor = True
            Me.GridView1.Appearance.GroupRow.Options.UseForeColor = True
            Me.GridView1.Appearance.HeaderPanel.BackColor = System.Drawing.Color.FromArgb(CType(139, Byte), CType(201, Byte), CType(254, Byte))
            Me.GridView1.Appearance.HeaderPanel.BorderColor = System.Drawing.Color.FromArgb(CType(139, Byte), CType(201, Byte), CType(254, Byte))
            Me.GridView1.Appearance.HeaderPanel.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.HeaderPanel.Options.UseBackColor = True
            Me.GridView1.Appearance.HeaderPanel.Options.UseBorderColor = True
            Me.GridView1.Appearance.HeaderPanel.Options.UseForeColor = True
            Me.GridView1.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.FromArgb(CType(105, Byte), CType(170, Byte), CType(225, Byte))
            Me.GridView1.Appearance.HideSelectionRow.BorderColor = System.Drawing.Color.FromArgb(CType(83, Byte), CType(155, Byte), CType(215, Byte))
            Me.GridView1.Appearance.HideSelectionRow.ForeColor = System.Drawing.Color.FromArgb(CType(236, Byte), CType(246, Byte), CType(255, Byte))
            Me.GridView1.Appearance.HideSelectionRow.Options.UseBackColor = True
            Me.GridView1.Appearance.HideSelectionRow.Options.UseBorderColor = True
            Me.GridView1.Appearance.HideSelectionRow.Options.UseForeColor = True
            Me.GridView1.Appearance.HorzLine.BackColor = System.Drawing.Color.FromArgb(CType(104, Byte), CType(184, Byte), CType(251, Byte))
            Me.GridView1.Appearance.HorzLine.Options.UseBackColor = True
            Me.GridView1.Appearance.OddRow.BackColor = System.Drawing.Color.FromArgb(CType(236, Byte), CType(246, Byte), CType(255, Byte))
            Me.GridView1.Appearance.OddRow.BorderColor = System.Drawing.Color.FromArgb(CType(236, Byte), CType(246, Byte), CType(255, Byte))
            Me.GridView1.Appearance.OddRow.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.OddRow.Options.UseBackColor = True
            Me.GridView1.Appearance.OddRow.Options.UseBorderColor = True
            Me.GridView1.Appearance.OddRow.Options.UseForeColor = True
            Me.GridView1.Appearance.Preview.Font = New System.Drawing.Font("Verdana", 7.5!)
            Me.GridView1.Appearance.Preview.ForeColor = System.Drawing.Color.FromArgb(CType(83, Byte), CType(155, Byte), CType(215, Byte))
            Me.GridView1.Appearance.Preview.Options.UseFont = True
            Me.GridView1.Appearance.Preview.Options.UseForeColor = True
            Me.GridView1.Appearance.Row.BackColor = System.Drawing.Color.FromArgb(CType(247, Byte), CType(251, Byte), CType(255, Byte))
            Me.GridView1.Appearance.Row.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.Row.Options.UseBackColor = True
            Me.GridView1.Appearance.Row.Options.UseForeColor = True
            Me.GridView1.Appearance.RowSeparator.BackColor = System.Drawing.Color.FromArgb(CType(236, Byte), CType(246, Byte), CType(255, Byte))
            Me.GridView1.Appearance.RowSeparator.BackColor2 = System.Drawing.Color.White
            Me.GridView1.Appearance.RowSeparator.Options.UseBackColor = True
            Me.GridView1.Appearance.SelectedRow.BackColor = System.Drawing.Color.FromArgb(CType(83, Byte), CType(155, Byte), CType(215, Byte))
            Me.GridView1.Appearance.SelectedRow.ForeColor = System.Drawing.Color.White
            Me.GridView1.Appearance.SelectedRow.Options.UseBackColor = True
            Me.GridView1.Appearance.SelectedRow.Options.UseForeColor = True
            Me.GridView1.Appearance.TopNewRow.BackColor = System.Drawing.Color.White
            Me.GridView1.Appearance.TopNewRow.Options.UseBackColor = True
            Me.GridView1.Appearance.VertLine.BackColor = System.Drawing.Color.FromArgb(CType(104, Byte), CType(184, Byte), CType(251, Byte))
            Me.GridView1.Appearance.VertLine.Options.UseBackColor = True
            Me.GridView1.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.GridColumn_workshop, Me.GridColumn_time, Me.GridColumn_location, Me.GridColumn_description})
            Me.GridView1.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus
            Me.GridView1.GridControl = Me.GridControl1
            Me.GridView1.Name = "GridView1"
            Me.GridView1.OptionsBehavior.Editable = False
            Me.GridView1.OptionsView.ShowGroupPanel = False
            Me.GridView1.OptionsView.ShowIndicator = False
            Me.GridView1.SortInfo.AddRange(New DevExpress.XtraGrid.Columns.GridColumnSortInfo() {New DevExpress.XtraGrid.Columns.GridColumnSortInfo(Me.GridColumn_workshop, DevExpress.Data.ColumnSortOrder.Descending)})
            '
            'GridColumn_workshop
            '
            Me.GridColumn_workshop.Caption = "ID"
            Me.GridColumn_workshop.CustomizationCaption = "Workshop ID"
            Me.GridColumn_workshop.FieldName = "workshop"
            Me.GridColumn_workshop.Name = "GridColumn_workshop"
            '
            'GridColumn_time
            '
            Me.GridColumn_time.Caption = "Date/Time"
            Me.GridColumn_time.CustomizationCaption = "Date for workshop"
            Me.GridColumn_time.DisplayFormat.FormatString = "M/dd/yyyy h:mm tt"
            Me.GridColumn_time.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime
            Me.GridColumn_time.FieldName = "start_time"
            Me.GridColumn_time.GroupFormat.FormatString = "d"
            Me.GridColumn_time.GroupFormat.FormatType = DevExpress.Utils.FormatType.DateTime
            Me.GridColumn_time.Name = "GridColumn_time"
            Me.GridColumn_time.Visible = True
            Me.GridColumn_time.VisibleIndex = 0
            Me.GridColumn_time.Width = 95
            '
            'GridColumn_location
            '
            Me.GridColumn_location.Caption = "Location"
            Me.GridColumn_location.CustomizationCaption = "Location"
            Me.GridColumn_location.FieldName = "location"
            Me.GridColumn_location.Name = "GridColumn_location"
            Me.GridColumn_location.Visible = True
            Me.GridColumn_location.VisibleIndex = 1
            Me.GridColumn_location.Width = 150
            '
            'GridColumn_description
            '
            Me.GridColumn_description.Caption = "Description"
            Me.GridColumn_description.CustomizationCaption = "Description"
            Me.GridColumn_description.FieldName = "description"
            Me.GridColumn_description.Name = "GridColumn_description"
            Me.GridColumn_description.Visible = True
            Me.GridColumn_description.VisibleIndex = 2
            Me.GridColumn_description.Width = 151
            '
            'Button_OK
            '
            Me.Button_OK.Anchor = System.Windows.Forms.AnchorStyles.Bottom
            Me.Button_OK.Location = New System.Drawing.Point(211, 184)
            Me.Button_OK.Name = "Button_OK"
            Me.Button_OK.TabIndex = 2
            Me.Button_OK.Text = "Next >"
            Me.Button_OK.ToolTip = "Click here to select the workshop"
            '
            'Button_Cancel
            '
            Me.Button_Cancel.Anchor = System.Windows.Forms.AnchorStyles.Bottom
            Me.Button_Cancel.Location = New System.Drawing.Point(115, 184)
            Me.Button_Cancel.Name = "Button_Cancel"
            Me.Button_Cancel.TabIndex = 3
            Me.Button_Cancel.Text = "&Cancel"
            Me.Button_Cancel.ToolTip = "Click here to cancel the form and return to the previous screen"
            '
            'Page_Select
            '
            Me.Appearance.BackColor = System.Drawing.SystemColors.Window
            Me.Appearance.Options.UseBackColor = True
            Me.Controls.Add(Me.Button_Cancel)
            Me.Controls.Add(Me.Button_OK)
            Me.Controls.Add(Me.GridControl1)
            Me.Controls.Add(Me.LabelControl1)
            Me.Name = "Page_Select"
            Me.Size = New System.Drawing.Size(400, 224)
            CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.GridView1, System.ComponentModel.ISupportInitialize).EndInit()
            Me.ResumeLayout(False)

        End Sub

#End Region

        ''' <summary>
        ''' Process the CANCEL button on the form
        ''' </summary>
        Private Sub Button_Cancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
            FindForm.Close()
        End Sub

        ''' <summary>
        ''' Process the OK button on the form
        ''' </summary>
        Private Sub Button_OK_Click(ByVal sender As Object, ByVal e As System.EventArgs)
            RaiseEvent Completed(Me, Workshop)
        End Sub

        ''' <summary>
        ''' Load the list of workshops to select
        ''' </summary>
        Public Sub Process()
            Dim cmd As SqlClient.SqlCommand = New SqlCommand
            With cmd
                .Connection = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
                .CommandText = "lst_workshops_pending"
                .CommandType = CommandType.StoredProcedure
            End With

            Dim current_cursor As Cursor = System.Windows.Forms.Cursor.Current
            Dim ds As New System.Data.DataSet
            Try
                System.Windows.Forms.Cursor.Current = Cursors.WaitCursor
                Dim da As New SqlClient.SqlDataAdapter(cmd)
                da.Fill(ds, "workshops")

                With GridControl1
                    .DataSource = ds.Tables(0).DefaultView
                    .RefreshDataSource()
                End With

            Catch ex As SqlClient.SqlException
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error reading Workshop IDs")

            Finally
                System.Windows.Forms.Cursor.Current = current_cursor
            End Try
        End Sub

        ''' <summary>
        ''' Process a change in the current row for the control
        ''' </summary>
        Private Sub GridView1_FocusedRowChanged(ByVal sender As Object, ByVal e As DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs)
            Dim gv As DevExpress.XtraGrid.Views.Grid.GridView = CType(sender, DevExpress.XtraGrid.Views.Grid.GridView)
            Dim ctl As DevExpress.XtraGrid.GridControl = gv.GridControl
            Dim ControlRow As System.Int32 = e.FocusedRowHandle

            ' Find the datarowview from the input tables.
            Dim drv As System.Data.DataRowView = Nothing
            If ControlRow >= 0 Then
                Dim vue As DataView = CType(ctl.DataSource, DataView)
                drv = CType(gv.GetRow(ControlRow), System.Data.DataRowView)
            End If

            ' Ask the user to do something with the selected row
            Workshop = -1
            If drv("item_key") IsNot Nothing AndAlso drv("item_key") IsNot System.DBNull.Value Then Workshop = Convert.ToInt32(drv("item_key"))
            Button_OK.Enabled = (Workshop > 0)
        End Sub

        ''' <summary>
        ''' Double Click on an item -- edit it
        ''' </summary>
        Private Sub GridView1_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs)

            ' Find the row targetted as the double-click item
            Dim gv As DevExpress.XtraGrid.Views.Grid.GridView = CType(sender, DevExpress.XtraGrid.Views.Grid.GridView)
            Dim ctl As DevExpress.XtraGrid.GridControl = CType(gv.GridControl, DevExpress.XtraGrid.GridControl)
            Dim hi As DevExpress.XtraGrid.Views.Grid.ViewInfo.GridHitInfo = gv.CalcHitInfo((ctl.PointToClient(Windows.Forms.Control.MousePosition)))
            Dim ControlRow As System.Int32 = hi.RowHandle

            ' Find the datarowview from the input tables.
            Dim drv As System.Data.DataRowView = Nothing
            If ControlRow >= 0 Then
                Dim vue As DataView = CType(ctl.DataSource, DataView)
                drv = CType(gv.GetRow(ControlRow), System.Data.DataRowView)
            End If

            ' If there is a row then select it
            Workshop = -1
            If drv IsNot Nothing Then
                If drv("item_key") IsNot Nothing AndAlso drv("item_key") IsNot System.DBNull.Value Then Workshop = Convert.ToInt32(drv("item_key"))
            End If

            ' If there is a workshop then select it
            If Workshop > 0 Then Button_OK.PerformClick()
        End Sub

        Private Sub GridView1_MouseDown(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs)
            Dim gv As DevExpress.XtraGrid.Views.Grid.GridView = GridView1
            Dim ctl As DevExpress.XtraGrid.GridControl = CType(gv.GridControl, DevExpress.XtraGrid.GridControl)
            Dim hi As DevExpress.XtraGrid.Views.Grid.ViewInfo.GridHitInfo = gv.CalcHitInfo(New Point(e.X, e.Y))

            ' Remember the position for the popup menu handler.
            Dim ControlRow As System.Int32 = -1
            If hi.InRow Then
                ControlRow = hi.RowHandle
            End If

            Workshop = -1
            If ControlRow >= 0 Then
                Dim drv As System.Data.DataRowView = CType(GridView1.GetRow(ControlRow), System.Data.DataRowView)
                If drv IsNot Nothing Then
                    If drv("item_key") IsNot Nothing AndAlso drv("item_key") IsNot System.DBNull.Value Then Workshop = Convert.ToInt32(drv("item_key"))
                End If
                Button_OK.Enabled = (Workshop > 0)
            End If
        End Sub

        Private Sub SelectWorkshop_Load(ByVal sender As System.Object, ByVal e As System.EventArgs)
            Dim MyForm As Form = Me.FindForm

            ' Assign the controllers to the controls for this control.
            If MyForm IsNot Nothing AndAlso TypeOf MyForm Is IForm Then
                With Button_Cancel
                    .ToolTipController = CType(MyForm, IForm).GetToolTipController
                End With

                With Button_OK
                    .ToolTipController = CType(MyForm, IForm).GetToolTipController
                End With

                With GridControl1
                    .ToolTipController = CType(MyForm, IForm).GetToolTipController
                End With
            End If
        End Sub

    End Class
End Namespace
