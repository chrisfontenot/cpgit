#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}
#End Region

Option Compare Binary
Option Explicit On 
Option Strict On

Imports System.Data.SqlClient
Imports System.Windows.Forms

Namespace Workshop.Attendance
    Public Class Page_Attendance
        Inherits DevExpress.XtraEditors.XtraUserControl
        Public Event PreviousPage As EventHandler
        Public Property Workshop() As Int32 = -1

        Public Sub New()
            MyBase.New()
            InitializeComponent()
            AddHandler SimpleButton_Prev.Click, AddressOf SimpleButton_Prev_Click
            AddHandler Button_OK.Click, AddressOf Button_OK_Click

            'Add any initialization after the InitializeComponent() call
            GridColumn_client.DisplayFormat.Format = New DebtPlus.Utils.Format.Client.CustomFormatter
            GridColumn_client.GroupFormat.Format = New DebtPlus.Utils.Format.Client.CustomFormatter
        End Sub

#Region " Windows Form Designer generated code "

        'UserControl overrides dispose to clean up the component list.
        Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
            If disposing Then
                If components IsNot Nothing Then
                    components.Dispose()
                End If
            End If
            MyBase.Dispose(disposing)
        End Sub

        'Required by the Windows Form Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Windows Form Designer
        'It can be modified using the Windows Form Designer.  
        'Do not modify it using the code editor.
        Friend WithEvents SimpleButton_Prev As DevExpress.XtraEditors.SimpleButton
        Friend WithEvents Button_OK As DevExpress.XtraEditors.SimpleButton
        Friend WithEvents GridControl1 As DevExpress.XtraGrid.GridControl
        Friend WithEvents GridView1 As DevExpress.XtraGrid.Views.Grid.GridView
        Friend WithEvents GridColumn_kept As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents Status_Kept As DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit
        Friend WithEvents GridColumn_StatusMissed As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents Status_Missed As DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit
        Friend WithEvents GridColumn_client As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents GridColumn_ClientName As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents GridColumn_seats As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents LabelControl2 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents LabelControl3 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents LabelControl4 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents workshop_name As DevExpress.XtraEditors.LabelControl
        Friend WithEvents workshop_date As DevExpress.XtraEditors.LabelControl
        Friend WithEvents workshop_location As DevExpress.XtraEditors.LabelControl
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
            Me.SimpleButton_Prev = New DevExpress.XtraEditors.SimpleButton
            Me.Button_OK = New DevExpress.XtraEditors.SimpleButton
            Me.GridControl1 = New DevExpress.XtraGrid.GridControl
            Me.GridView1 = New DevExpress.XtraGrid.Views.Grid.GridView
            Me.GridColumn_kept = New DevExpress.XtraGrid.Columns.GridColumn
            Me.GridColumn_kept.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True
            Me.Status_Kept = New DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit
            Me.GridColumn_StatusMissed = New DevExpress.XtraGrid.Columns.GridColumn
            Me.GridColumn_StatusMissed.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True
            Me.Status_Missed = New DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit
            Me.GridColumn_client = New DevExpress.XtraGrid.Columns.GridColumn
            Me.GridColumn_client.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True
            Me.GridColumn_seats = New DevExpress.XtraGrid.Columns.GridColumn
            Me.GridColumn_seats.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True
            Me.GridColumn_ClientName = New DevExpress.XtraGrid.Columns.GridColumn
            Me.GridColumn_ClientName.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True
            Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl
            Me.LabelControl2 = New DevExpress.XtraEditors.LabelControl
            Me.LabelControl3 = New DevExpress.XtraEditors.LabelControl
            Me.LabelControl4 = New DevExpress.XtraEditors.LabelControl
            Me.workshop_name = New DevExpress.XtraEditors.LabelControl
            Me.workshop_date = New DevExpress.XtraEditors.LabelControl
            Me.workshop_location = New DevExpress.XtraEditors.LabelControl
            CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.GridView1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.Status_Kept, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.Status_Missed, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.SuspendLayout()
            '
            'SimpleButton_Prev
            '
            Me.SimpleButton_Prev.Anchor = System.Windows.Forms.AnchorStyles.Bottom
            Me.SimpleButton_Prev.Location = New System.Drawing.Point(115, 264)
            Me.SimpleButton_Prev.Name = "SimpleButton_Prev"
            Me.SimpleButton_Prev.Size = New System.Drawing.Size(75, 23)
            Me.SimpleButton_Prev.TabIndex = 0
            Me.SimpleButton_Prev.Text = "< Prev"
            '
            'Button_OK
            '
            Me.Button_OK.Anchor = System.Windows.Forms.AnchorStyles.Bottom
            Me.Button_OK.Location = New System.Drawing.Point(211, 264)
            Me.Button_OK.Name = "Button_OK"
            Me.Button_OK.Size = New System.Drawing.Size(75, 23)
            Me.Button_OK.TabIndex = 1
            Me.Button_OK.Text = "Finish"
            '
            'GridControl1
            '
            Me.GridControl1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                        Or System.Windows.Forms.AnchorStyles.Left) _
                        Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.GridControl1.Location = New System.Drawing.Point(0, 112)
            Me.GridControl1.MainView = Me.GridView1
            Me.GridControl1.Name = "GridControl1"
            Me.GridControl1.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.Status_Kept, Me.Status_Missed})
            Me.GridControl1.Size = New System.Drawing.Size(400, 144)
            Me.GridControl1.TabIndex = 2
            Me.GridControl1.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridView1})
            '
            'GridView1
            '
            Me.GridView1.Appearance.ColumnFilterButton.BackColor = System.Drawing.Color.FromArgb(CType(CType(104, Byte), Int32), CType(CType(184, Byte), Int32), CType(CType(251, Byte), Int32))
            Me.GridView1.Appearance.ColumnFilterButton.BorderColor = System.Drawing.Color.FromArgb(CType(CType(104, Byte), Int32), CType(CType(184, Byte), Int32), CType(CType(251, Byte), Int32))
            Me.GridView1.Appearance.ColumnFilterButton.ForeColor = System.Drawing.Color.White
            Me.GridView1.Appearance.ColumnFilterButton.Options.UseBackColor = True
            Me.GridView1.Appearance.ColumnFilterButton.Options.UseBorderColor = True
            Me.GridView1.Appearance.ColumnFilterButton.Options.UseForeColor = True
            Me.GridView1.Appearance.ColumnFilterButtonActive.BackColor = System.Drawing.Color.FromArgb(CType(CType(170, Byte), Int32), CType(CType(216, Byte), Int32), CType(CType(254, Byte), Int32))
            Me.GridView1.Appearance.ColumnFilterButtonActive.BorderColor = System.Drawing.Color.FromArgb(CType(CType(170, Byte), Int32), CType(CType(216, Byte), Int32), CType(CType(254, Byte), Int32))
            Me.GridView1.Appearance.ColumnFilterButtonActive.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.ColumnFilterButtonActive.Options.UseBackColor = True
            Me.GridView1.Appearance.ColumnFilterButtonActive.Options.UseBorderColor = True
            Me.GridView1.Appearance.ColumnFilterButtonActive.Options.UseForeColor = True
            Me.GridView1.Appearance.Empty.BackColor = System.Drawing.Color.FromArgb(CType(CType(236, Byte), Int32), CType(CType(246, Byte), Int32), CType(CType(255, Byte), Int32))
            Me.GridView1.Appearance.Empty.BackColor2 = System.Drawing.Color.White
            Me.GridView1.Appearance.Empty.Options.UseBackColor = True
            Me.GridView1.Appearance.EvenRow.BackColor = System.Drawing.Color.FromArgb(CType(CType(247, Byte), Int32), CType(CType(251, Byte), Int32), CType(CType(255, Byte), Int32))
            Me.GridView1.Appearance.EvenRow.BorderColor = System.Drawing.Color.FromArgb(CType(CType(247, Byte), Int32), CType(CType(251, Byte), Int32), CType(CType(255, Byte), Int32))
            Me.GridView1.Appearance.EvenRow.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.EvenRow.Options.UseBackColor = True
            Me.GridView1.Appearance.EvenRow.Options.UseBorderColor = True
            Me.GridView1.Appearance.EvenRow.Options.UseForeColor = True
            Me.GridView1.Appearance.FilterCloseButton.BackColor = System.Drawing.Color.FromArgb(CType(CType(104, Byte), Int32), CType(CType(184, Byte), Int32), CType(CType(251, Byte), Int32))
            Me.GridView1.Appearance.FilterCloseButton.BorderColor = System.Drawing.Color.FromArgb(CType(CType(104, Byte), Int32), CType(CType(184, Byte), Int32), CType(CType(251, Byte), Int32))
            Me.GridView1.Appearance.FilterCloseButton.ForeColor = System.Drawing.Color.White
            Me.GridView1.Appearance.FilterCloseButton.Options.UseBackColor = True
            Me.GridView1.Appearance.FilterCloseButton.Options.UseBorderColor = True
            Me.GridView1.Appearance.FilterCloseButton.Options.UseForeColor = True
            Me.GridView1.Appearance.FilterPanel.BackColor = System.Drawing.Color.FromArgb(CType(CType(236, Byte), Int32), CType(CType(246, Byte), Int32), CType(CType(255, Byte), Int32))
            Me.GridView1.Appearance.FilterPanel.BackColor2 = System.Drawing.Color.White
            Me.GridView1.Appearance.FilterPanel.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.FilterPanel.Options.UseBackColor = True
            Me.GridView1.Appearance.FilterPanel.Options.UseForeColor = True
            Me.GridView1.Appearance.FixedLine.BackColor = System.Drawing.Color.FromArgb(CType(CType(59, Byte), Int32), CType(CType(133, Byte), Int32), CType(CType(195, Byte), Int32))
            Me.GridView1.Appearance.FixedLine.Options.UseBackColor = True
            Me.GridView1.Appearance.FocusedCell.BackColor = System.Drawing.Color.White
            Me.GridView1.Appearance.FocusedCell.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.FocusedCell.Options.UseBackColor = True
            Me.GridView1.Appearance.FocusedCell.Options.UseForeColor = True
            Me.GridView1.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb(CType(CType(38, Byte), Int32), CType(CType(109, Byte), Int32), CType(CType(189, Byte), Int32))
            Me.GridView1.Appearance.FocusedRow.BorderColor = System.Drawing.Color.FromArgb(CType(CType(59, Byte), Int32), CType(CType(139, Byte), Int32), CType(CType(206, Byte), Int32))
            Me.GridView1.Appearance.FocusedRow.ForeColor = System.Drawing.Color.White
            Me.GridView1.Appearance.FocusedRow.Options.UseBackColor = True
            Me.GridView1.Appearance.FocusedRow.Options.UseBorderColor = True
            Me.GridView1.Appearance.FocusedRow.Options.UseForeColor = True
            Me.GridView1.Appearance.FooterPanel.BackColor = System.Drawing.Color.FromArgb(CType(CType(104, Byte), Int32), CType(CType(184, Byte), Int32), CType(CType(251, Byte), Int32))
            Me.GridView1.Appearance.FooterPanel.BorderColor = System.Drawing.Color.FromArgb(CType(CType(104, Byte), Int32), CType(CType(184, Byte), Int32), CType(CType(251, Byte), Int32))
            Me.GridView1.Appearance.FooterPanel.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.FooterPanel.Options.UseBackColor = True
            Me.GridView1.Appearance.FooterPanel.Options.UseBorderColor = True
            Me.GridView1.Appearance.FooterPanel.Options.UseForeColor = True
            Me.GridView1.Appearance.GroupButton.BackColor = System.Drawing.Color.FromArgb(CType(CType(104, Byte), Int32), CType(CType(184, Byte), Int32), CType(CType(251, Byte), Int32))
            Me.GridView1.Appearance.GroupButton.BorderColor = System.Drawing.Color.FromArgb(CType(CType(104, Byte), Int32), CType(CType(184, Byte), Int32), CType(CType(251, Byte), Int32))
            Me.GridView1.Appearance.GroupButton.Options.UseBackColor = True
            Me.GridView1.Appearance.GroupButton.Options.UseBorderColor = True
            Me.GridView1.Appearance.GroupFooter.BackColor = System.Drawing.Color.FromArgb(CType(CType(170, Byte), Int32), CType(CType(216, Byte), Int32), CType(CType(254, Byte), Int32))
            Me.GridView1.Appearance.GroupFooter.BorderColor = System.Drawing.Color.FromArgb(CType(CType(170, Byte), Int32), CType(CType(216, Byte), Int32), CType(CType(254, Byte), Int32))
            Me.GridView1.Appearance.GroupFooter.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.GroupFooter.Options.UseBackColor = True
            Me.GridView1.Appearance.GroupFooter.Options.UseBorderColor = True
            Me.GridView1.Appearance.GroupFooter.Options.UseForeColor = True
            Me.GridView1.Appearance.GroupPanel.BackColor = System.Drawing.Color.FromArgb(CType(CType(236, Byte), Int32), CType(CType(246, Byte), Int32), CType(CType(255, Byte), Int32))
            Me.GridView1.Appearance.GroupPanel.BackColor2 = System.Drawing.Color.White
            Me.GridView1.Appearance.GroupPanel.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.GroupPanel.Options.UseBackColor = True
            Me.GridView1.Appearance.GroupPanel.Options.UseForeColor = True
            Me.GridView1.Appearance.GroupRow.BackColor = System.Drawing.Color.FromArgb(CType(CType(170, Byte), Int32), CType(CType(216, Byte), Int32), CType(CType(254, Byte), Int32))
            Me.GridView1.Appearance.GroupRow.BorderColor = System.Drawing.Color.FromArgb(CType(CType(170, Byte), Int32), CType(CType(216, Byte), Int32), CType(CType(254, Byte), Int32))
            Me.GridView1.Appearance.GroupRow.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.GroupRow.Options.UseBackColor = True
            Me.GridView1.Appearance.GroupRow.Options.UseBorderColor = True
            Me.GridView1.Appearance.GroupRow.Options.UseForeColor = True
            Me.GridView1.Appearance.HeaderPanel.BackColor = System.Drawing.Color.FromArgb(CType(CType(139, Byte), Int32), CType(CType(201, Byte), Int32), CType(CType(254, Byte), Int32))
            Me.GridView1.Appearance.HeaderPanel.BorderColor = System.Drawing.Color.FromArgb(CType(CType(139, Byte), Int32), CType(CType(201, Byte), Int32), CType(CType(254, Byte), Int32))
            Me.GridView1.Appearance.HeaderPanel.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.HeaderPanel.Options.UseBackColor = True
            Me.GridView1.Appearance.HeaderPanel.Options.UseBorderColor = True
            Me.GridView1.Appearance.HeaderPanel.Options.UseForeColor = True
            Me.GridView1.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Int32), CType(CType(170, Byte), Int32), CType(CType(225, Byte), Int32))
            Me.GridView1.Appearance.HideSelectionRow.BorderColor = System.Drawing.Color.FromArgb(CType(CType(83, Byte), Int32), CType(CType(155, Byte), Int32), CType(CType(215, Byte), Int32))
            Me.GridView1.Appearance.HideSelectionRow.ForeColor = System.Drawing.Color.FromArgb(CType(CType(236, Byte), Int32), CType(CType(246, Byte), Int32), CType(CType(255, Byte), Int32))
            Me.GridView1.Appearance.HideSelectionRow.Options.UseBackColor = True
            Me.GridView1.Appearance.HideSelectionRow.Options.UseBorderColor = True
            Me.GridView1.Appearance.HideSelectionRow.Options.UseForeColor = True
            Me.GridView1.Appearance.HorzLine.BackColor = System.Drawing.Color.FromArgb(CType(CType(104, Byte), Int32), CType(CType(184, Byte), Int32), CType(CType(251, Byte), Int32))
            Me.GridView1.Appearance.HorzLine.Options.UseBackColor = True
            Me.GridView1.Appearance.OddRow.BackColor = System.Drawing.Color.FromArgb(CType(CType(236, Byte), Int32), CType(CType(246, Byte), Int32), CType(CType(255, Byte), Int32))
            Me.GridView1.Appearance.OddRow.BorderColor = System.Drawing.Color.FromArgb(CType(CType(236, Byte), Int32), CType(CType(246, Byte), Int32), CType(CType(255, Byte), Int32))
            Me.GridView1.Appearance.OddRow.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.OddRow.Options.UseBackColor = True
            Me.GridView1.Appearance.OddRow.Options.UseBorderColor = True
            Me.GridView1.Appearance.OddRow.Options.UseForeColor = True
            Me.GridView1.Appearance.Preview.Font = New System.Drawing.Font("Verdana", 7.5!)
            Me.GridView1.Appearance.Preview.ForeColor = System.Drawing.Color.FromArgb(CType(CType(83, Byte), Int32), CType(CType(155, Byte), Int32), CType(CType(215, Byte), Int32))
            Me.GridView1.Appearance.Preview.Options.UseFont = True
            Me.GridView1.Appearance.Preview.Options.UseForeColor = True
            Me.GridView1.Appearance.Row.BackColor = System.Drawing.Color.FromArgb(CType(CType(247, Byte), Int32), CType(CType(251, Byte), Int32), CType(CType(255, Byte), Int32))
            Me.GridView1.Appearance.Row.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.Row.Options.UseBackColor = True
            Me.GridView1.Appearance.Row.Options.UseForeColor = True
            Me.GridView1.Appearance.RowSeparator.BackColor = System.Drawing.Color.FromArgb(CType(CType(236, Byte), Int32), CType(CType(246, Byte), Int32), CType(CType(255, Byte), Int32))
            Me.GridView1.Appearance.RowSeparator.BackColor2 = System.Drawing.Color.White
            Me.GridView1.Appearance.RowSeparator.Options.UseBackColor = True
            Me.GridView1.Appearance.SelectedRow.BackColor = System.Drawing.Color.FromArgb(CType(CType(83, Byte), Int32), CType(CType(155, Byte), Int32), CType(CType(215, Byte), Int32))
            Me.GridView1.Appearance.SelectedRow.ForeColor = System.Drawing.Color.White
            Me.GridView1.Appearance.SelectedRow.Options.UseBackColor = True
            Me.GridView1.Appearance.SelectedRow.Options.UseForeColor = True
            Me.GridView1.Appearance.TopNewRow.BackColor = System.Drawing.Color.White
            Me.GridView1.Appearance.TopNewRow.Options.UseBackColor = True
            Me.GridView1.Appearance.VertLine.BackColor = System.Drawing.Color.FromArgb(CType(CType(104, Byte), Int32), CType(CType(184, Byte), Int32), CType(CType(251, Byte), Int32))
            Me.GridView1.Appearance.VertLine.Options.UseBackColor = True
            Me.GridView1.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.GridColumn_kept, Me.GridColumn_StatusMissed, Me.GridColumn_client, Me.GridColumn_seats, Me.GridColumn_ClientName})
            Me.GridView1.GridControl = Me.GridControl1
            Me.GridView1.Name = "GridView1"
            Me.GridView1.OptionsView.ShowGroupPanel = False
            Me.GridView1.OptionsView.ShowIndicator = False
            Me.GridView1.ShowButtonMode = DevExpress.XtraGrid.Views.Base.ShowButtonModeEnum.ShowAlways
            Me.GridView1.SortInfo.AddRange(New DevExpress.XtraGrid.Columns.GridColumnSortInfo() {New DevExpress.XtraGrid.Columns.GridColumnSortInfo(Me.GridColumn_client, DevExpress.Data.ColumnSortOrder.Ascending)})
            '
            'GridColumn_kept
            '
            Me.GridColumn_kept.AppearanceCell.Options.UseTextOptions = True
            Me.GridColumn_kept.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
            Me.GridColumn_kept.AppearanceHeader.Options.UseTextOptions = True
            Me.GridColumn_kept.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
            Me.GridColumn_kept.Caption = "Attended"
            Me.GridColumn_kept.ColumnEdit = Me.Status_Kept
            Me.GridColumn_kept.CustomizationCaption = "Client Kept Appt"
            Me.GridColumn_kept.FieldName = "status_kept"
            Me.GridColumn_kept.Name = "GridColumn_kept"
            Me.GridColumn_kept.Visible = True
            Me.GridColumn_kept.VisibleIndex = 0
            Me.GridColumn_kept.Width = 59
            '
            'Status_Kept
            '
            Me.Status_Kept.AutoHeight = False
            Me.Status_Kept.Name = "Status_Kept"
            Me.Status_Kept.Tag = "K"
            '
            'GridColumn_StatusMissed
            '
            Me.GridColumn_StatusMissed.AppearanceCell.Options.UseTextOptions = True
            Me.GridColumn_StatusMissed.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
            Me.GridColumn_StatusMissed.AppearanceHeader.Options.UseTextOptions = True
            Me.GridColumn_StatusMissed.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
            Me.GridColumn_StatusMissed.Caption = "No Show"
            Me.GridColumn_StatusMissed.ColumnEdit = Me.Status_Missed
            Me.GridColumn_StatusMissed.CustomizationCaption = "Client missed the appointment"
            Me.GridColumn_StatusMissed.FieldName = "status_missed"
            Me.GridColumn_StatusMissed.Name = "GridColumn_StatusMissed"
            Me.GridColumn_StatusMissed.Visible = True
            Me.GridColumn_StatusMissed.VisibleIndex = 1
            Me.GridColumn_StatusMissed.Width = 59
            '
            'Status_Missed
            '
            Me.Status_Missed.AutoHeight = False
            Me.Status_Missed.Name = "Status_Missed"
            Me.Status_Missed.Tag = "M"
            '
            'GridColumn_client
            '
            Me.GridColumn_client.AppearanceCell.Options.UseTextOptions = True
            Me.GridColumn_client.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.GridColumn_client.AppearanceHeader.Options.UseTextOptions = True
            Me.GridColumn_client.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.GridColumn_client.Caption = "Client"
            Me.GridColumn_client.CustomizationCaption = "Client ID"
            Me.GridColumn_client.DisplayFormat.FormatString = "f0"
            Me.GridColumn_client.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom
            Me.GridColumn_client.FieldName = "client"
            Me.GridColumn_client.GroupFormat.FormatString = "f0"
            Me.GridColumn_client.GroupFormat.FormatType = DevExpress.Utils.FormatType.Custom
            Me.GridColumn_client.Name = "GridColumn_client"
            Me.GridColumn_client.OptionsColumn.AllowEdit = False
            Me.GridColumn_client.SortMode = DevExpress.XtraGrid.ColumnSortMode.Value
            Me.GridColumn_client.Visible = True
            Me.GridColumn_client.VisibleIndex = 2
            Me.GridColumn_client.Width = 58
            '
            'GridColumn_seats
            '
            Me.GridColumn_seats.AppearanceCell.Options.UseTextOptions = True
            Me.GridColumn_seats.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.GridColumn_seats.AppearanceHeader.Options.UseTextOptions = True
            Me.GridColumn_seats.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.GridColumn_seats.Caption = "Seats"
            Me.GridColumn_seats.CustomizationCaption = "Seats"
            Me.GridColumn_seats.DisplayFormat.FormatString = "f0"
            Me.GridColumn_seats.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.GridColumn_seats.FieldName = "workshop_people"
            Me.GridColumn_seats.GroupFormat.FormatString = "f0"
            Me.GridColumn_seats.GroupFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.GridColumn_seats.Name = "GridColumn_seats"
            Me.GridColumn_seats.OptionsColumn.AllowEdit = False
            Me.GridColumn_seats.Visible = True
            Me.GridColumn_seats.VisibleIndex = 3
            Me.GridColumn_seats.Width = 45
            '
            'GridColumn_ClientName
            '
            Me.GridColumn_ClientName.Caption = "Name"
            Me.GridColumn_ClientName.CustomizationCaption = "Client Name"
            Me.GridColumn_ClientName.FieldName = "client1_name"
            Me.GridColumn_ClientName.Name = "GridColumn_ClientName"
            Me.GridColumn_ClientName.OptionsColumn.AllowEdit = False
            Me.GridColumn_ClientName.Visible = True
            Me.GridColumn_ClientName.VisibleIndex = 4
            Me.GridColumn_ClientName.Width = 175
            '
            'LabelControl1
            '
            Me.LabelControl1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                        Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.LabelControl1.Appearance.Options.UseTextOptions = True
            Me.LabelControl1.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
            Me.LabelControl1.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Top
            Me.LabelControl1.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap
            Me.LabelControl1.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
            Me.LabelControl1.Location = New System.Drawing.Point(11, 3)
            Me.LabelControl1.Name = "LabelControl1"
            Me.LabelControl1.Size = New System.Drawing.Size(379, 47)
            Me.LabelControl1.TabIndex = 3
            Me.LabelControl1.Text = "This function will permit you to mark the clients who attended a workshop. Check " & _
                "the clients who attended the workshop or not and press the FINISH button."
            '
            'LabelControl2
            '
            Me.LabelControl2.Appearance.Options.UseTextOptions = True
            Me.LabelControl2.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.LabelControl2.Location = New System.Drawing.Point(8, 56)
            Me.LabelControl2.Name = "LabelControl2"
            Me.LabelControl2.Size = New System.Drawing.Size(82, 13)
            Me.LabelControl2.TabIndex = 4
            Me.LabelControl2.Text = "Workshop Name:"
            Me.LabelControl2.UseMnemonic = False
            '
            'LabelControl3
            '
            Me.LabelControl3.Appearance.Options.UseTextOptions = True
            Me.LabelControl3.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.LabelControl3.Location = New System.Drawing.Point(8, 72)
            Me.LabelControl3.Name = "LabelControl3"
            Me.LabelControl3.Size = New System.Drawing.Size(27, 13)
            Me.LabelControl3.TabIndex = 5
            Me.LabelControl3.Text = "Date:"
            Me.LabelControl3.UseMnemonic = False
            '
            'LabelControl4
            '
            Me.LabelControl4.Appearance.Options.UseTextOptions = True
            Me.LabelControl4.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.LabelControl4.Location = New System.Drawing.Point(8, 88)
            Me.LabelControl4.Name = "LabelControl4"
            Me.LabelControl4.Size = New System.Drawing.Size(44, 13)
            Me.LabelControl4.TabIndex = 6
            Me.LabelControl4.Text = "Location:"
            Me.LabelControl4.UseMnemonic = False
            '
            'workshop_name
            '
            Me.workshop_name.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                        Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.workshop_name.Appearance.Options.UseTextOptions = True
            Me.workshop_name.Appearance.TextOptions.Trimming = DevExpress.Utils.Trimming.EllipsisCharacter
            Me.workshop_name.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap
            Me.workshop_name.Location = New System.Drawing.Point(104, 56)
            Me.workshop_name.Name = "workshop_name"
            Me.workshop_name.Size = New System.Drawing.Size(0, 13)
            Me.workshop_name.TabIndex = 7
            Me.workshop_name.UseMnemonic = False
            '
            'workshop_date
            '
            Me.workshop_date.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                        Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.workshop_date.Appearance.Options.UseTextOptions = True
            Me.workshop_date.Appearance.TextOptions.Trimming = DevExpress.Utils.Trimming.EllipsisCharacter
            Me.workshop_date.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap
            Me.workshop_date.Location = New System.Drawing.Point(104, 72)
            Me.workshop_date.Name = "workshop_date"
            Me.workshop_date.Size = New System.Drawing.Size(0, 13)
            Me.workshop_date.TabIndex = 8
            Me.workshop_date.UseMnemonic = False
            '
            'workshop_location
            '
            Me.workshop_location.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                        Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.workshop_location.Appearance.Options.UseTextOptions = True
            Me.workshop_location.Appearance.TextOptions.Trimming = DevExpress.Utils.Trimming.EllipsisCharacter
            Me.workshop_location.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap
            Me.workshop_location.Location = New System.Drawing.Point(104, 88)
            Me.workshop_location.Name = "workshop_location"
            Me.workshop_location.Size = New System.Drawing.Size(0, 13)
            Me.workshop_location.TabIndex = 9
            Me.workshop_location.UseMnemonic = False
            '
            'Page_Attendance
            '
            Me.Controls.Add(Me.workshop_location)
            Me.Controls.Add(Me.workshop_date)
            Me.Controls.Add(Me.workshop_name)
            Me.Controls.Add(Me.LabelControl4)
            Me.Controls.Add(Me.LabelControl3)
            Me.Controls.Add(Me.LabelControl2)
            Me.Controls.Add(Me.LabelControl1)
            Me.Controls.Add(Me.GridControl1)
            Me.Controls.Add(Me.Button_OK)
            Me.Controls.Add(Me.SimpleButton_Prev)
            Me.Name = "Page_Attendance"
            Me.Size = New System.Drawing.Size(400, 304)
            CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.GridView1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.Status_Kept, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.Status_Missed, System.ComponentModel.ISupportInitialize).EndInit()
            Me.ResumeLayout(False)
            Me.PerformLayout()

        End Sub

#End Region

        Private ReadOnly ds As New DataSet("ds")

        Private WithEvents _tbl As DataTable
        Private Property tbl As DataTable
            Get
                Return _tbl
            End Get
            Set(value As DataTable)
                If _tbl IsNot Nothing Then
                    RemoveHandler _tbl.ColumnChanged, AddressOf tbl_ColumnChanged
                End If
                _tbl = value
                If _tbl IsNot Nothing Then
                    AddHandler _tbl.ColumnChanged, AddressOf tbl_ColumnChanged
                End If
            End Set
        End Property

        ''' <summary>
        ''' Load the list of clients who have pending appointments
        ''' </summary>
        Private Sub ReadWorkshopInfo()
            Dim cn As SqlClient.SqlConnection = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
            Dim rd As SqlClient.SqlDataReader = Nothing
            Try
                cn.Open()
                With New SqlCommand
                    .Connection = cn
                    .CommandText = "SELECT wn.description as workshop_name, w.start_time, wl.name as workshop_location FROM workshops w with (nolock) LEFT OUTER JOIN workshop_types wn WITH (NOLOCK) ON w.workshop_type = wn.workshop_type LEFT OUTER JOIN workshop_locations wl WITH (NOLOCK) ON w.workshop_location = wl.workshop_location WHERE w.workshop=@workshop"
                    .Parameters.Add("@workshop", SqlDbType.Int).Value = Workshop
                    rd = .ExecuteReader(CommandBehavior.CloseConnection Or CommandBehavior.SingleRow)
                End With

                If rd.Read Then
                    If Not rd.IsDBNull(0) Then workshop_name.Text = rd.GetString(0).Trim()
                    If Not rd.IsDBNull(1) Then workshop_date.Text = String.Format("{0:MM/dd/yyyy hh:mm tt}", rd.GetDateTime(1))
                    If Not rd.IsDBNull(2) Then workshop_location.Text = rd.GetString(2)
                End If

            Catch ex As SqlClient.SqlException
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error reading workshop information")

            Finally
                If rd IsNot Nothing AndAlso Not rd.IsClosed Then rd.Close()
                If cn IsNot Nothing AndAlso cn.State <> ConnectionState.Closed Then cn.Close()
            End Try
        End Sub

        ''' <summary>
        ''' Load the list of clients who have pending appointments
        ''' </summary>
        Public Sub Process()
            ds.Clear()

            ' Read the information for the workshop

            Dim current_cursor As Cursor = System.Windows.Forms.Cursor.Current
            Try
                System.Windows.Forms.Cursor.Current = Cursors.WaitCursor

                ' Read the workshop information first
                ReadWorkshopInfo()

                ' Try to fill the workshop details next
                Using cmd As SqlClient.SqlCommand = New SqlCommand
                    With cmd
                        .Connection = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
                        .CommandText = "SELECT ca.client_appointment, ca.client, ca.appt_time, ca.counselor, ca.start_time, ca.end_time, ca.office, ca.workshop, ca.workshop_people, ca.appt_type, ca.status, ca.result, ca.previous_appointment, ca.referred_to, ca.referred_by, ca.partner, ca.bankruptcy_class, ca.priority, ca.housing, ca.post_purchase, ca.credit, ca.callback_ph, ca.HousingFeeAmount, ca.confirmation_status, ca.date_confirmed, ca.date_updated, ca.date_created, ca.created_by, convert(bit,0) as status_kept, convert(bit,0) as status_missed, dbo.format_normal_name(p1n.prefix,p1n.first,p1n.middle,p1n.last,p1n.suffix) as client1_name FROM client_appointments ca WITH (NOLOCK) LEFT OUTER JOIN people p1 ON ca.client = p1.client AND 1 = p1.relation LEFT OUTER JOIN names p1n WITH (NOLOCK) ON p1.nameid = p1n.name WHERE ca.office IS NULL AND ca.workshop=@workshop AND ca.status='P'"
                        .Parameters.Add("@workshop", SqlDbType.Int).Value = Workshop
                    End With
                    Using da As New SqlClient.SqlDataAdapter(cmd)
                        da.Fill(ds)
                    End Using
                End Using
                tbl = ds.Tables(0)

                With GridControl1
                    .DataSource = tbl.DefaultView
                    .RefreshDataSource()
                End With

            Catch ex As SqlClient.SqlException
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error reading client appointments")

            Finally
                System.Windows.Forms.Cursor.Current = current_cursor
            End Try
        End Sub

        ''' <summary>
        ''' The previous button was clicked
        ''' </summary>
        Private Sub SimpleButton_Prev_Click(ByVal sender As Object, ByVal e As EventArgs)
            SaveChanges()
            RaiseEvent PreviousPage(Me, EventArgs.Empty)
        End Sub

        ''' <summary>
        ''' The Next button was clicked
        ''' </summary>
        Private Sub Button_OK_Click(ByVal sender As Object, ByVal e As EventArgs)
            FindForm.Close()
        End Sub

        ''' <summary>
        ''' Hook into the form closing event when we load
        ''' </summary>
        Protected Overrides Sub OnLoad(ByVal e As EventArgs)
            MyBase.OnLoad(e)
            AddHandler FindForm.Closing, AddressOf Form_Closing
        End Sub

        ''' <summary>
        ''' The form is being closed. Save the changes.
        ''' </summary>
        Private Sub Form_Closing(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs)
            SaveChanges()
        End Sub

        ''' <summary>
        ''' Routine to save the changed data
        ''' </summary>
        Private Sub SaveChanges()

            If tbl IsNot Nothing Then
                Dim current_cursor As Cursor = System.Windows.Forms.Cursor.Current
                Try
                    System.Windows.Forms.Cursor.Current = Cursors.WaitCursor

                    Using update_cmd As SqlClient.SqlCommand = New SqlCommand
                        With update_cmd
                            .Connection = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
                            .CommandText = "UPDATE client_appointments SET status=@status WHERE client_appointment=@client_appointment"
                            .Parameters.Add("@status", SqlDbType.VarChar, 2, "status")
                            .Parameters.Add("@client_appointment", SqlDbType.Int, 0, "client_appointment")
                        End With

                        Using da As New SqlClient.SqlDataAdapter
                            da.UpdateCommand = update_cmd
                            da.Update(tbl)
                        End Using
                    End Using

                Catch ex As SqlClient.SqlException
                    DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error reading client appointments")

                Finally
                    System.Windows.Forms.Cursor.Current = current_cursor
                End Try
            End If
        End Sub

        ''' <summary>
        ''' Process a change on the status of the appointment
        ''' </summary>
        Private Sub tbl_ColumnChanged(ByVal sender As Object, ByVal e As DataColumnChangeEventArgs)

            ' Look for a change in our checkbox items to update the current appointment status
            Select Case e.Column.ColumnName
                Case "status_kept"
                    If Convert.ToBoolean(e.ProposedValue) Then e.Row("status_missed") = False

                Case "status_missed"
                    If Convert.ToBoolean(e.ProposedValue) Then e.Row("status_kept") = False

                Case Else
                    Return
            End Select

            ' Set the proper status for the row
            If Convert.ToBoolean(e.Row("status_missed")) Then
                e.Row("status") = "M"
            ElseIf Convert.ToBoolean(e.Row("status_kept")) Then
                e.Row("status") = "K"
            Else
                e.Row("status") = "P"
            End If
        End Sub
    End Class
End Namespace
