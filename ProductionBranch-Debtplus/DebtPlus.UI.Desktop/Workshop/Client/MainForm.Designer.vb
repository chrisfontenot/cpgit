﻿Imports DebtPlus.UI.Desktop.Workshop.Client.Workshops

Namespace Workshop.Client
    <Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
    Partial Class MainForm
        Inherits DebtPlus.Data.Forms.DebtPlusForm

        'Form overrides dispose to clean up the component list.
        <System.Diagnostics.DebuggerNonUserCode()> _
        Protected Overrides Sub Dispose(ByVal disposing As Boolean)
            Try
                If disposing AndAlso components IsNot Nothing Then
                    components.Dispose()
                End If
            Finally
                MyBase.Dispose(disposing)
            End Try
        End Sub

        'Required by the Windows Form Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Windows Form Designer
        'It can be modified using the Windows Form Designer.  
        'Do not modify it using the code editor.
        <System.Diagnostics.DebuggerStepThrough()> _
        Private Sub InitializeComponent()
            Me.NewWorkshopSelectionControl1 = New NewWorkshopSelectionControl
            Me.ClientSelectionControl1 = New ClientSelectionControl
            CType(Me.DxErrorProvider1, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.SuspendLayout()
            '
            'NewWorkshopSelectionControl1
            '
            Me.NewWorkshopSelectionControl1.Dock = System.Windows.Forms.DockStyle.Fill
            Me.NewWorkshopSelectionControl1.Location = New System.Drawing.Point(0, 0)
            Me.NewWorkshopSelectionControl1.Name = "NewWorkshopSelectionControl1"
            Me.NewWorkshopSelectionControl1.Size = New System.Drawing.Size(617, 341)
            Me.ToolTipController1.SetSuperTip(Me.NewWorkshopSelectionControl1, Nothing)
            Me.NewWorkshopSelectionControl1.TabIndex = 0
            '
            'ClientSelectionControl1
            '
            Me.ClientSelectionControl1.Dock = System.Windows.Forms.DockStyle.Fill
            Me.ClientSelectionControl1.Location = New System.Drawing.Point(0, 0)
            Me.ClientSelectionControl1.Name = "ClientSelectionControl1"
            Me.ClientSelectionControl1.Size = New System.Drawing.Size(617, 341)
            Me.ToolTipController1.SetSuperTip(Me.ClientSelectionControl1, Nothing)
            Me.ClientSelectionControl1.TabIndex = 1
            Me.ClientSelectionControl1.Visible = False
            '
            'MainForm
            '
            Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
            Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
            Me.ClientSize = New System.Drawing.Size(617, 341)
            Me.Controls.Add(Me.NewWorkshopSelectionControl1)
            Me.Controls.Add(Me.ClientSelectionControl1)
            Me.Name = "MainForm"
            Me.ToolTipController1.SetSuperTip(Me, Nothing)
            Me.Text = "Workshop Clients"
            CType(Me.DxErrorProvider1, System.ComponentModel.ISupportInitialize).EndInit()
            Me.ResumeLayout(False)

        End Sub
        Friend WithEvents NewWorkshopSelectionControl1 As NewWorkshopSelectionControl
        Friend WithEvents ClientSelectionControl1 As ClientSelectionControl

    End Class
End Namespace