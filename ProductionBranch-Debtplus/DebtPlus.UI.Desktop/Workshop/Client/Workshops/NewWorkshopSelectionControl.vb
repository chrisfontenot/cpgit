#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}
#End Region

Option Compare Binary
Option Explicit On
Option Strict On

Imports System.Windows.Forms

Namespace Workshop.Client.Workshops
    Friend Class NewWorkshopSelectionControl
        Inherits WorkshopSelectionControl

        Public Sub New()
            MyBase.New()
            InitializeComponent()
            AddHandler Button_New.Click, AddressOf Button_New_Click
        End Sub

        Private Sub Button_New_Click(ByVal sender As Object, ByVal e As System.EventArgs)
            Dim vue As System.Data.DataView = DirectCast(GridControl1.DataSource, System.Data.DataView)
            Dim drv As System.Data.DataRowView = vue.AddNew

            drv.BeginEdit()

            ' Find a good "start time" for the workshop
            Dim StartingTime As DateTime = DateTime.Now.AddHours(1)
            StartingTime = New DateTime(StartingTime.Year, StartingTime.Month, StartingTime.Day, StartingTime.Hour, 0, 0)
            drv("start_time") = StartingTime

            With New NewWorkshop(drv)
                If .ShowDialog() = DialogResult.OK Then
                    drv.EndEdit()
                    GlobalValues.workshop = Convert.ToInt32(drv("workshop"))
                    RaiseCompleted()
                Else
                    drv.CancelEdit()
                End If
            End With
        End Sub
    End Class
End Namespace
