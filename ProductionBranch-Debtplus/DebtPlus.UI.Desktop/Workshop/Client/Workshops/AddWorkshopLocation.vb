#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}
#End Region

Option Compare Binary
Option Explicit On
Option Strict On

Imports DebtPlus.Utils
Imports System.Data.SqlClient
Imports System.Windows.Forms

Namespace Workshop.Client.Workshops
    Public Class AddWorkshopLocation

        Private drv As System.Data.DataRowView = Nothing
        Public Sub New(ByVal drv As System.Data.DataRowView)
            MyBase.New()
            Me.InitializeComponent()
            Me.drv = drv
            AddHandler Me.Load, AddressOf AddWorkshopLocation_Load
            AddHandler Button_OK.Click, AddressOf Button_OK_Click
        End Sub

        Private Sub AddWorkshopLocation_Load(ByVal sender As System.Object, ByVal e As System.EventArgs)

            With tx_description
                .DataBindings.Clear()
                .DataBindings.Add("EditValue", drv, "name")
                AddHandler .EditValueChanged, AddressOf tx_description_EditValueChanged
            End With

            With tx_name
                .DataBindings.Clear()
                .DataBindings.Add("EditValue", drv, "organization")
                AddHandler .EditValueChanged, AddressOf tx_description_EditValueChanged
            End With

            With lk_type
                Dim tbl As DataTable = WorkshopLocationTypesTable()
                .Properties.DataSource = tbl.DefaultView
                .Properties.ValueMember = "organization_type"
                .Properties.DisplayMember = "name"

                .DataBindings.Clear()
                .DataBindings.Add(New Binding("EditValue", drv, "type"))
                AddHandler .EditValueChanged, AddressOf tx_description_EditValueChanged
            End With

            With adr
                With .Line1
                    If drv.IsNew OrElse drv("address1") Is System.DBNull.Value Then drv("address1") = String.Empty
                    .DataBindings.Clear()
                    .DataBindings.Add(New Binding("EditValue", drv, "address1"))
                    AddHandler .EditValueChanged, AddressOf tx_description_EditValueChanged
                End With

                With .Line2
                    If drv.IsNew OrElse drv("address2") Is System.DBNull.Value Then drv("address2") = String.Empty
                    .DataBindings.Clear()
                    .DataBindings.Add(New Binding("EditValue", drv, "address2"))
                    AddHandler .EditValueChanged, AddressOf tx_description_EditValueChanged
                End With

                With .City
                    If drv.IsNew OrElse drv("city") Is System.DBNull.Value Then drv("city") = String.Empty
                    .DataBindings.Clear()
                    .DataBindings.Add(New Binding("EditValue", drv, "city"))
                    AddHandler .EditValueChanged, AddressOf tx_description_EditValueChanged
                End With

                With .PostalCode
                    If drv.IsNew OrElse drv("postalcode") Is System.DBNull.Value Then drv("postalcode") = String.Empty
                    .DataBindings.Clear()
                    .DataBindings.Add(New Binding("EditValue", drv, "postalcode"))
                    AddHandler .EditValueChanged, AddressOf tx_description_EditValueChanged
                End With

                With .States
                    If drv.IsNew OrElse drv("state") Is System.DBNull.Value Then drv("state") = .EditValue
                    .DataBindings.Clear()
                    .DataBindings.Add(New Binding("EditValue", drv, "state"))
                    AddHandler .EditValueChanged, AddressOf tx_description_EditValueChanged
                End With
            End With

            With ph_phone
                If drv.IsNew OrElse drv("phone") Is System.DBNull.Value Then drv("phone") = .EditValue
                .DataBindings.Clear()
                .DataBindings.Add(New Binding("EditValue", drv, "phone"))
                AddHandler .EditValueChanged, AddressOf tx_description_EditValueChanged
            End With

            With sp_milage
                If drv.IsNew OrElse drv("milage") Is System.DBNull.Value Then drv("milage") = .EditValue
                .DataBindings.Clear()
                .DataBindings.Add(New Binding("EditValue", drv, "milage"))
                AddHandler .EditValueChanged, AddressOf tx_description_EditValueChanged
            End With

            With me_directions
                If drv.IsNew OrElse drv("directions") Is System.DBNull.Value Then drv("directions") = .EditValue
                .DataBindings.Clear()
                .DataBindings.Add(New Binding("EditValue", drv, "directions"))
                AddHandler .EditValueChanged, AddressOf tx_description_EditValueChanged
            End With

            EnableOK()
        End Sub

        Private Function WorkshopLocationTypesTable() As DataTable
            Dim tbl As DataTable = ds.Tables("workshop_organization_types")

            If tbl Is Nothing Then
                Dim cmd As SqlClient.SqlCommand = New SqlCommand
                With cmd
                    .Connection = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
                    .CommandText = "SELECT [organization_type],[name],[ActiveFlag],[date_created],[created_by] FROM workshop_organization_types"
                    .CommandType = CommandType.Text
                End With
                Dim da As New SqlClient.SqlDataAdapter(cmd)

                Dim current_cursor As System.Windows.Forms.Cursor = System.Windows.Forms.Cursor.Current
                Try
                    System.Windows.Forms.Cursor.Current = Cursors.WaitCursor
                    da.Fill(ds, "workshop_organization_types")
                    tbl = ds.Tables("workshop_organization_types")
                    With tbl
                        .PrimaryKey = New DataColumn() {.Columns("organization_type")}
                        With .Columns("organization_type")
                            .AutoIncrement = True
                            .AutoIncrementSeed = -1
                            .AutoIncrementStep = -1
                        End With
                    End With

                Catch ex As SqlClient.SqlException
                    DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error reading workshop_organization_types table")

                Finally
                    System.Windows.Forms.Cursor.Current = current_cursor
                End Try
            End If

            Return tbl
        End Function

        Private Sub EnableOK()

            Do
                If tx_description.Text.Trim() = String.Empty Then Exit Do
                If lk_type.EditValue Is Nothing OrElse lk_type.EditValue Is System.DBNull.Value Then Exit Do
                If tx_name.Text.Trim() = String.Empty Then Exit Do

                Dim Milage As System.Int32
                If Int32.TryParse(sp_milage.Text, Milage) Then
                    If Milage >= 0 Then
                        Button_OK.Enabled = True
                        Return
                    End If
                End If
                Exit Do
            Loop
            Button_OK.Enabled = False
        End Sub

        Private Sub tx_description_EditValueChanged(ByVal sender As Object, ByVal e As System.EventArgs)
            EnableOK()
        End Sub

        Private Sub Button_OK_Click(ByVal sender As Object, ByVal e As System.EventArgs)
            Dim cn As SqlClient.SqlConnection = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
            Dim current_cursor As System.Windows.Forms.Cursor = System.Windows.Forms.Cursor.Current
            Dim workshop_location As System.Int32 = -1

            Try
                cn.Open()
                With New SqlCommand
                    .Connection = cn
                    .CommandText = "INSERT INTO workshop_locations (name,organization,type,address1,address2,city,state,postalcode,phone,directions,milage) values (@name,@organization,@type,@address1,@address2,@city,@state,@postalcode,@phone,@directions,@milage); SELECT scope_identity() as workshop_location;"
                    .Parameters.Add("@name", SqlDbType.VarChar, 80).Value = drv("name")
                    .Parameters.Add("@organization", SqlDbType.VarChar, 80).Value = drv("organization")
                    .Parameters.Add("@type", SqlDbType.Int).Value = drv("type")
                    .Parameters.Add("@address1", SqlDbType.VarChar, 80).Value = drv("address1")
                    .Parameters.Add("@address2", SqlDbType.VarChar, 80).Value = drv("address2")
                    .Parameters.Add("@city", SqlDbType.VarChar, 80).Value = drv("city")
                    .Parameters.Add("@state", SqlDbType.Int).Value = drv("state")
                    .Parameters.Add("@postalcode", SqlDbType.VarChar, 80).Value = drv("postalcode")
                    .Parameters.Add("@phone", SqlDbType.VarChar, 50).Value = drv("phone")
                    .Parameters.Add("@directions", SqlDbType.VarChar, 1024).Value = drv("directions")
                    .Parameters.Add("@milage", SqlDbType.Float).Value = drv("milage")

                    Dim objAnswer As Object = .ExecuteScalar
                    If objAnswer IsNot Nothing AndAlso objAnswer IsNot System.DBNull.Value Then workshop_location = Convert.ToInt32(objAnswer)
                End With

                If workshop_location < 0 Then Throw New ApplicationException("No workshop_location for new record")
                drv("workshop_location") = workshop_location
                DialogResult = Windows.Forms.DialogResult.OK

            Catch ex As SqlClient.SqlException
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error adding workshop_location")

            Finally
                If cn IsNot Nothing AndAlso cn.State <> ConnectionState.Closed Then cn.Close()
                System.Windows.Forms.Cursor.Current = current_cursor
            End Try
        End Sub
    End Class
End Namespace
