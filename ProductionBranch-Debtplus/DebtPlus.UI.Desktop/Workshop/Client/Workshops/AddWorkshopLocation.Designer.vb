﻿Namespace Workshop.Client.Workshops
    <Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
    Partial Class AddWorkshopLocation
        Inherits DebtPlus.Data.Forms.DebtPlusForm

        'Form overrides dispose to clean up the component list.
        <System.Diagnostics.DebuggerNonUserCode()> _
        Protected Overrides Sub Dispose(ByVal disposing As Boolean)
            Try
                If disposing AndAlso components IsNot Nothing Then
                    components.Dispose()
                End If
            Finally
                MyBase.Dispose(disposing)
            End Try
        End Sub

        'Required by the Windows Form Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Windows Form Designer
        'It can be modified using the Windows Form Designer.  
        'Do not modify it using the code editor.
        <System.Diagnostics.DebuggerStepThrough()> _
        Private Sub InitializeComponent()
            Me.LabelControl2 = New DevExpress.XtraEditors.LabelControl
            Me.tx_description = New DevExpress.XtraEditors.TextEdit
            Me.Button_OK = New DevExpress.XtraEditors.SimpleButton
            Me.Button_Cancel = New DevExpress.XtraEditors.SimpleButton
            Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl
            Me.lk_type = New DevExpress.XtraEditors.LookUpEdit
            Me.sp_milage = New DevExpress.XtraEditors.SpinEdit
            Me.LabelControl3 = New DevExpress.XtraEditors.LabelControl
            Me.LabelControl4 = New DevExpress.XtraEditors.LabelControl
            Me.LabelControl5 = New DevExpress.XtraEditors.LabelControl
            Me.LabelControl6 = New DevExpress.XtraEditors.LabelControl
            Me.me_directions = New DevExpress.XtraEditors.MemoExEdit
            Me.adr = New DebtPlus.Data.Controls.AddressParts
            Me.ph_phone = New DebtPlus.Data.Controls.PhoneNumber
            Me.tx_name = New DevExpress.XtraEditors.TextEdit
            Me.LabelControl7 = New DevExpress.XtraEditors.LabelControl
            CType(Me.DxErrorProvider1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.tx_description.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.lk_type.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.sp_milage.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.me_directions.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.ph_phone.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.tx_name.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.SuspendLayout()
            '
            'LabelControl2
            '
            Me.LabelControl2.Location = New System.Drawing.Point(12, 18)
            Me.LabelControl2.Name = "LabelControl2"
            Me.LabelControl2.Size = New System.Drawing.Size(27, 13)
            Me.LabelControl2.TabIndex = 0
            Me.LabelControl2.Text = "Name"
            '
            'tx_description
            '
            Me.tx_description.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                        Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.tx_description.Location = New System.Drawing.Point(85, 15)
            Me.tx_description.Name = "tx_description"
            Me.tx_description.Properties.MaxLength = 50
            Me.tx_description.Size = New System.Drawing.Size(218, 20)
            Me.tx_description.TabIndex = 1
            '
            'Button_OK
            '
            Me.Button_OK.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.Button_OK.Enabled = False
            Me.Button_OK.Location = New System.Drawing.Point(309, 12)
            Me.Button_OK.Name = "Button_OK"
            Me.Button_OK.Size = New System.Drawing.Size(75, 23)
            Me.Button_OK.TabIndex = 14
            Me.Button_OK.Text = "&OK"
            '
            'Button_Cancel
            '
            Me.Button_Cancel.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.Button_Cancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
            Me.Button_Cancel.Location = New System.Drawing.Point(309, 41)
            Me.Button_Cancel.Name = "Button_Cancel"
            Me.Button_Cancel.Size = New System.Drawing.Size(75, 23)
            Me.Button_Cancel.TabIndex = 15
            Me.Button_Cancel.Text = "&Cancel"
            '
            'LabelControl1
            '
            Me.LabelControl1.Location = New System.Drawing.Point(12, 41)
            Me.LabelControl1.Name = "LabelControl1"
            Me.LabelControl1.Size = New System.Drawing.Size(24, 13)
            Me.LabelControl1.TabIndex = 2
            Me.LabelControl1.Text = "Type"
            '
            'lk_type
            '
            Me.lk_type.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                        Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.lk_type.Location = New System.Drawing.Point(85, 38)
            Me.lk_type.Name = "lk_type"
            Me.lk_type.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.lk_type.Properties.Columns.AddRange(New DevExpress.XtraEditors.Controls.LookUpColumnInfo() {New DevExpress.XtraEditors.Controls.LookUpColumnInfo("name", "Name", 20, DevExpress.Utils.FormatType.None, "", True, DevExpress.Utils.HorzAlignment.[Default], DevExpress.Data.ColumnSortOrder.Ascending)})
            Me.lk_type.Properties.NullText = ""
            Me.lk_type.Properties.ShowFooter = False
            Me.lk_type.Properties.ShowHeader = False
            Me.lk_type.Size = New System.Drawing.Size(218, 20)
            Me.lk_type.TabIndex = 3
            Me.lk_type.Properties.SortColumnIndex = 0
            '
            'sp_milage
            '
            Me.sp_milage.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.sp_milage.EditValue = New Decimal(New Int32() {0, 0, 0, 0})
            Me.sp_milage.Location = New System.Drawing.Point(323, 170)
            Me.sp_milage.Name = "sp_milage"
            Me.sp_milage.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton})
            Me.sp_milage.Size = New System.Drawing.Size(61, 20)
            Me.sp_milage.TabIndex = 11
            '
            'LabelControl3
            '
            Me.LabelControl3.Location = New System.Drawing.Point(12, 199)
            Me.LabelControl3.Name = "LabelControl3"
            Me.LabelControl3.Size = New System.Drawing.Size(47, 13)
            Me.LabelControl3.TabIndex = 12
            Me.LabelControl3.Text = "Directions"
            '
            'LabelControl4
            '
            Me.LabelControl4.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.LabelControl4.Location = New System.Drawing.Point(287, 173)
            Me.LabelControl4.Name = "LabelControl4"
            Me.LabelControl4.Size = New System.Drawing.Size(30, 13)
            Me.LabelControl4.TabIndex = 10
            Me.LabelControl4.Text = "Milage"
            '
            'LabelControl5
            '
            Me.LabelControl5.Location = New System.Drawing.Point(12, 173)
            Me.LabelControl5.Name = "LabelControl5"
            Me.LabelControl5.Size = New System.Drawing.Size(30, 13)
            Me.LabelControl5.TabIndex = 8
            Me.LabelControl5.Text = "Phone"
            '
            'LabelControl6
            '
            Me.LabelControl6.Location = New System.Drawing.Point(12, 99)
            Me.LabelControl6.Name = "LabelControl6"
            Me.LabelControl6.Size = New System.Drawing.Size(39, 13)
            Me.LabelControl6.TabIndex = 6
            Me.LabelControl6.Text = "Address"
            '
            'me_directions
            '
            Me.me_directions.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                        Or System.Windows.Forms.AnchorStyles.Left) _
                        Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.me_directions.Location = New System.Drawing.Point(85, 196)
            Me.me_directions.Name = "me_directions"
            Me.me_directions.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.me_directions.Properties.MaxLength = 1024
            Me.me_directions.Properties.ShowIcon = False
            Me.me_directions.Size = New System.Drawing.Size(299, 20)
            Me.me_directions.TabIndex = 13
            '
            'adr
            '
            Me.adr.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                        Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.adr.Location = New System.Drawing.Point(85, 96)
            Me.adr.Name = "adr"
            Me.adr.Size = New System.Drawing.Size(299, 72)
            Me.ToolTipController1.SetSuperTip(Me.adr, Nothing)
            Me.adr.TabIndex = 7
            '
            'ph_phone
            '
            Me.ph_phone.Location = New System.Drawing.Point(85, 170)
            Me.ph_phone.Name = "ph_phone"
            Me.ph_phone.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
            Me.ph_phone.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom
            Me.ph_phone.Properties.Mask.AutoComplete = DevExpress.XtraEditors.Mask.AutoCompleteType.Strong
            Me.ph_phone.Properties.Mask.BeepOnError = True
            Me.ph_phone.Properties.Mask.EditMask = "(999) 000-0000"
            Me.ph_phone.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Simple
            Me.ph_phone.Properties.Mask.SaveLiteral = False
            Me.ph_phone.Properties.Mask.UseMaskAsDisplayFormat = True
            Me.ph_phone.Size = New System.Drawing.Size(100, 20)
            Me.ph_phone.TabIndex = 9
            Me.ph_phone.ToolTip = "This is the telephone number"
            '
            'tx_name
            '
            Me.tx_name.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                        Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.tx_name.Location = New System.Drawing.Point(85, 70)
            Me.tx_name.Name = "tx_name"
            Me.tx_name.Properties.MaxLength = 50
            Me.tx_name.Size = New System.Drawing.Size(299, 20)
            Me.tx_name.TabIndex = 5
            '
            'LabelControl7
            '
            Me.LabelControl7.Location = New System.Drawing.Point(12, 73)
            Me.LabelControl7.Name = "LabelControl7"
            Me.LabelControl7.Size = New System.Drawing.Size(61, 13)
            Me.LabelControl7.TabIndex = 4
            Me.LabelControl7.Text = "Organization"
            '
            'AddWorkshopLocation
            '
            Me.AcceptButton = Me.Button_OK
            Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
            Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
            Me.CancelButton = Me.Button_Cancel
            Me.ClientSize = New System.Drawing.Size(396, 229)
            Me.Controls.Add(Me.tx_name)
            Me.Controls.Add(Me.LabelControl7)
            Me.Controls.Add(Me.ph_phone)
            Me.Controls.Add(Me.adr)
            Me.Controls.Add(Me.me_directions)
            Me.Controls.Add(Me.LabelControl6)
            Me.Controls.Add(Me.LabelControl5)
            Me.Controls.Add(Me.LabelControl4)
            Me.Controls.Add(Me.LabelControl3)
            Me.Controls.Add(Me.sp_milage)
            Me.Controls.Add(Me.lk_type)
            Me.Controls.Add(Me.LabelControl1)
            Me.Controls.Add(Me.Button_Cancel)
            Me.Controls.Add(Me.Button_OK)
            Me.Controls.Add(Me.tx_description)
            Me.Controls.Add(Me.LabelControl2)
            Me.Name = "AddWorkshopLocation"
            Me.ToolTipController1.SetSuperTip(Me, Nothing)
            Me.Text = "Add New Workshop Location"
            CType(Me.DxErrorProvider1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.tx_description.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.lk_type.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.sp_milage.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.me_directions.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.ph_phone.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.tx_name.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            Me.ResumeLayout(False)
            Me.PerformLayout()

        End Sub
        Friend WithEvents LabelControl2 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents tx_description As DevExpress.XtraEditors.TextEdit
        Friend WithEvents Button_OK As DevExpress.XtraEditors.SimpleButton
        Friend WithEvents Button_Cancel As DevExpress.XtraEditors.SimpleButton
        Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents lk_type As DevExpress.XtraEditors.LookUpEdit
        Friend WithEvents sp_milage As DevExpress.XtraEditors.SpinEdit
        Friend WithEvents LabelControl3 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents LabelControl4 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents LabelControl5 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents LabelControl6 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents me_directions As DevExpress.XtraEditors.MemoExEdit
        Friend WithEvents adr As DebtPlus.Data.Controls.AddressParts
        Friend WithEvents ph_phone As DebtPlus.Data.Controls.PhoneNumber
        Friend WithEvents tx_name As DevExpress.XtraEditors.TextEdit
        Friend WithEvents LabelControl7 As DevExpress.XtraEditors.LabelControl
    End Class
End Namespace
