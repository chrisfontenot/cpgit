#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}
#End Region

Option Compare Binary
Option Explicit On
Option Strict On

Imports DebtPlus.Svc.DataSets
Imports DebtPlus.Utils
Imports System.Data.SqlClient
Imports System.Windows.Forms

Namespace Workshop.Client.Workshops
    Public Class NewWorkshop

        Private drv As System.Data.DataRowView = Nothing
        Public Sub New(ByVal drv As System.Data.DataRowView)
            MyBase.New()
            Me.InitializeComponent()
            AddHandler Me.Load, AddressOf NewWorkshop_Load
            AddHandler Button_OK.Click, AddressOf Button_OK_Click
            Me.drv = drv
        End Sub

        Private Sub NewWorkshop_Load(ByVal sender As Object, ByVal e As System.EventArgs)

            ' Load the workshop types
            With lk_workshop
                Dim tbl As DataTable = WorkshopTypesTable()
                With .Properties
                    .DataSource = New System.Data.DataView(tbl, String.Empty, "description", DataViewRowState.CurrentRows)
                    .DisplayMember = "description"
                    .ValueMember = "workshop_type"
                End With

                ' Bind the data to the control
                .DataBindings.Clear()
                .DataBindings.Add(New Binding("EditValue", drv, "workshop_type"))

                ' If there is no workshop type then create one
                If drv.IsNew Then
                    Dim vue As New System.Data.DataView(tbl, String.Empty, "workshop_type", DataViewRowState.CurrentRows)
                    If vue.Count > 0 Then drv("workshop_type") = vue(0)("workshop_type")
                End If
                AddHandler .ButtonClick, AddressOf lk_workshop_ButtonClick
                AddHandler .EditValueChanged, AddressOf EditValueChanged
            End With

            ' Set the location
            With lk_location
                Dim tbl As DataTable = WorkshopLocationsTable()
                With .Properties
                    .DataSource = New System.Data.DataView(tbl, String.Empty, "name", DataViewRowState.CurrentRows)
                    .DisplayMember = "name"
                    .ValueMember = "workshop_location"
                End With

                ' Bind the data to the control
                .DataBindings.Clear()
                .DataBindings.Add(New Binding("EditValue", drv, "workshop_location"))

                ' If there is no workshop type then create one
                If drv.IsNew Then
                    Dim vue As New System.Data.DataView(tbl, String.Empty, "workshop_location", DataViewRowState.CurrentRows)
                    If vue.Count > 0 Then drv("workshop_location") = vue(0)("workshop_location")
                End If

                AddHandler .ButtonClick, AddressOf lk_location_ButtonClick
                AddHandler .EditValueChanged, AddressOf EditValueChanged
            End With

            ' Set the counselor
            With lk_counselor
                Using DataClass As New ClientDataSet()
                    Dim counselorID As Int32? = DebtPlus.Utils.Nulls.v_Int32(drv("counselor"))
                    Dim lst As System.Collections.Generic.List(Of DebtPlus.LINQ.counselor) = DebtPlus.LINQ.Cache.counselor.CounselorsList(counselorID)

                    With .Properties
                        .DataSource = lst
                        .DisplayMember = "Name"
                        .ValueMember = "Person"
                    End With

                    ' If there is no counselor for this workshop then find the default item
                    If drv("counselor") Is System.DBNull.Value Then
                        Dim defItem As DebtPlus.LINQ.counselor = lst.Find(Function(s) s.Default)
                        If defItem IsNot Nothing Then
                            drv("counselor") = defItem.Id
                        End If
                    End If

                    ' Bind the data to the control
                    .DataBindings.Clear()
                    .DataBindings.Add(New Binding("EditValue", drv, "counselor"))

                    AddHandler .EditValueChanged, AddressOf EditValueChanged
                End Using
            End With

            ' Set the starting date/time values
            If drv("start_time") IsNot Nothing AndAlso drv("start_time") IsNot System.DBNull.Value Then
                dt_date.EditValue = Convert.ToDateTime(drv("start_time"))
            End If

            ' Start with no seats. We will add attendees later when we have completed the processing
            If drv.IsNew Then
                drv("seats_available") = 0
            End If

            ' Add the handlers to detect changes
            AddHandler dt_date.EditValueChanged, AddressOf dt_date_EditValueChanged
            AddHandler dt_date.EditValueChanging, AddressOf dt_date_EditValueChanging

            ' Enable/Disable the OK button
            EnableOK()
        End Sub

        Private Function WorkshopTypesTable() As System.Data.DataTable
            Dim tbl As System.Data.DataTable = ds.Tables("workshop_types")

            If tbl Is Nothing Then
                Dim cmd As SqlClient.SqlCommand = New SqlCommand
                With cmd
                    .Connection = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
                    .CommandText = "SELECT [workshop_type],[description],[duration],[HUD_Grant],[HousingFeeAmount],[ActiveFlag],[date_created],[created_by] FROM workshop_types"
                    .CommandType = CommandType.Text
                End With
                Dim da As New SqlClient.SqlDataAdapter(cmd)

                Dim current_cursor As System.Windows.Forms.Cursor = System.Windows.Forms.Cursor.Current
                Try
                    System.Windows.Forms.Cursor.Current = Cursors.WaitCursor
                    da.Fill(ds, "workshop_types")
                    tbl = ds.Tables("workshop_types")
                    With tbl
                        .PrimaryKey = New DataColumn() {.Columns("workshop_type")}
                        With .Columns("workshop_type")
                            .AutoIncrement = True
                            .AutoIncrementSeed = -1
                            .AutoIncrementStep = -1
                        End With
                    End With

                Catch ex As SqlClient.SqlException
                    DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error reading workshop_types table")

                Finally
                    System.Windows.Forms.Cursor.Current = current_cursor
                End Try
            End If

            Return tbl
        End Function

        Private Function WorkshopLocationsTable() As System.Data.DataTable
            Dim tbl As System.Data.DataTable = ds.Tables("workshop_locations")

            If tbl Is Nothing Then
                Dim cmd As SqlClient.SqlCommand = New SqlCommand
                With cmd
                    .Connection = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
                    .CommandText = "SELECT [workshop_location],[name],[organization],[type],[AddressID],[TelephoneID],[directions],[milage],[ActiveFlag],[hcs_id],[date_created],[created_by] FROM workshop_locations"
                    .CommandType = CommandType.Text
                End With
                Dim da As New SqlClient.SqlDataAdapter(cmd)

                Dim current_cursor As System.Windows.Forms.Cursor = System.Windows.Forms.Cursor.Current
                Try
                    System.Windows.Forms.Cursor.Current = Cursors.WaitCursor
                    da.Fill(ds, "workshop_locations")
                    tbl = ds.Tables("workshop_locations")
                    With tbl
                        .PrimaryKey = New DataColumn() {.Columns("workshop_location")}
                        With .Columns("workshop_location")
                            .AutoIncrement = True
                            .AutoIncrementSeed = -1
                            .AutoIncrementStep = -1
                        End With
                    End With

                Catch ex As SqlClient.SqlException
                    DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error reading workshop_locations table")

                Finally
                    System.Windows.Forms.Cursor.Current = current_cursor
                End Try
            End If

            Return tbl
        End Function

        Private Sub lk_workshop_ButtonClick(ByVal sender As Object, ByVal e As DevExpress.XtraEditors.Controls.ButtonPressedEventArgs)
            If e.Button.Kind = DevExpress.XtraEditors.Controls.ButtonPredefines.Plus Then
                Dim tbl As DataTable = WorkshopTypesTable()
                Dim vue As DataView = tbl.DefaultView
                Dim edit_drv As DataRowView = vue.AddNew
                With New AddWorkshopType(edit_drv)
                    If .ShowDialog() = Windows.Forms.DialogResult.OK Then
                        edit_drv.EndEdit()
                        drv("workshop_type") = edit_drv("workshop_type")
                        lk_workshop.EditValue = edit_drv("workshop_type")
                    Else
                        edit_drv.CancelEdit()
                    End If
                End With
            End If
        End Sub

        Private Sub lk_location_ButtonClick(ByVal sender As Object, ByVal e As DevExpress.XtraEditors.Controls.ButtonPressedEventArgs)
            If e.Button.Kind = DevExpress.XtraEditors.Controls.ButtonPredefines.Plus Then
                Dim tbl As DataTable = WorkshopLocationsTable()
                Dim vue As DataView = tbl.DefaultView
                Dim edit_drv As DataRowView = vue.AddNew

                With New AddWorkshopLocation(edit_drv)

                    If .ShowDialog() = Windows.Forms.DialogResult.OK Then
                        edit_drv.EndEdit()
                        drv("workshop_location") = edit_drv("workshop_location")
                        lk_location.EditValue = edit_drv("workshop_location")
                    Else
                        edit_drv.CancelEdit()
                    End If
                End With
            End If
        End Sub

        Private Sub dt_date_EditValueChanged(ByVal sender As Object, ByVal e As System.EventArgs)
            Dim dt As New Date(Convert.ToDateTime(dt_date.EditValue).Year, Convert.ToDateTime(dt_date.EditValue).Month, Convert.ToDateTime(dt_date.EditValue).Day, Convert.ToDateTime(dt_date.EditValue).Hour, Convert.ToDateTime(dt_date.EditValue).Minute, 0)
            drv("start_time") = dt
            EnableOK()
        End Sub

        Private Sub dt_date_EditValueChanging(ByVal sender As Object, ByVal e As DevExpress.XtraEditors.Controls.ChangingEventArgs)
            Dim dt As Date = Convert.ToDateTime(e.NewValue)
            Dim days As System.Int32 = (Date.Now - dt).Days
            If days > 60 Then
                days = (Date.Now - Convert.ToDateTime(e.OldValue)).Days
                If days < 60 Then
                    e.Cancel = DebtPlus.Data.Forms.MessageBox.Show("You are attempting to create a workshop that is more than 60 days ago." + Environment.NewLine + "Are you really sure that you want to do this?" + Environment.NewLine + Environment.NewLine + "It is a very old workshop and that may effect existing reports.", "Are you sure?", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2) <> Windows.Forms.DialogResult.Yes
                End If
            End If
        End Sub

        Private Sub EnableOK()
            Do
                If lk_counselor.EditValue Is Nothing OrElse lk_counselor.EditValue Is System.DBNull.Value Then Exit Do
                If lk_location.EditValue Is Nothing OrElse lk_location.EditValue Is System.DBNull.Value Then Exit Do
                If lk_workshop.EditValue Is Nothing OrElse lk_workshop.EditValue Is System.DBNull.Value Then Exit Do

                Button_OK.Enabled = True
                Return
            Loop
            Button_OK.Enabled = False
        End Sub

        Private Sub EditValueChanged(ByVal sender As Object, ByVal e As System.EventArgs)
            EnableOK()
        End Sub

        Private Sub Button_OK_Click(ByVal sender As Object, ByVal e As System.EventArgs)
            Dim cn As SqlClient.SqlConnection = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
            Dim current_cursor As System.Windows.Forms.Cursor = System.Windows.Forms.Cursor.Current
            Dim workshop As System.Int32 = -1

            Try
                cn.Open()
                With New SqlCommand
                    .Connection = cn
                    .CommandText = "INSERT INTO workshops (workshop_type, start_time, workshop_location, seats_available, counselor) values (@workshop_type, @start_time, @workshop_location, @seats_available, @counselor); SELECT scope_identity() as workshop;"
                    .Parameters.Add("@workshop_type", SqlDbType.Int).Value = drv("workshop_type")
                    .Parameters.Add("@start_time", SqlDbType.DateTime).Value = drv("start_time")
                    .Parameters.Add("@workshop_location", SqlDbType.Int).Value = drv("workshop_location")
                    .Parameters.Add("@seats_available", SqlDbType.Int).Value = drv("seats_available")
                    .Parameters.Add("@counselor", SqlDbType.VarChar, 80).Value = drv("counselor")

                    Dim objAnswer As Object = .ExecuteScalar
                    If objAnswer IsNot Nothing AndAlso objAnswer IsNot System.DBNull.Value Then workshop = Convert.ToInt32(objAnswer)
                End With

                If workshop < 0 Then Throw New ApplicationException("No workshop for new record")
                drv("workshop") = workshop
                DialogResult = Windows.Forms.DialogResult.OK

            Catch ex As SqlClient.SqlException
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error adding workshop")

            Finally
                If cn IsNot Nothing AndAlso cn.State <> ConnectionState.Closed Then cn.Close()
                System.Windows.Forms.Cursor.Current = current_cursor
            End Try
        End Sub
    End Class
End Namespace
