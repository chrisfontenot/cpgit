﻿Namespace Workshop.Client.Workshops
    <Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
    Partial Class NewWorkshopSelectionControl
        Inherits WorkshopSelectionControl

        'UserControl overrides dispose to clean up the component list.
        <System.Diagnostics.DebuggerNonUserCode()> _
        Protected Overrides Sub Dispose(ByVal disposing As Boolean)
            Try
                If disposing AndAlso components IsNot Nothing Then
                    components.Dispose()
                End If
            Finally
                MyBase.Dispose(disposing)
            End Try
        End Sub

        'Required by the Windows Form Designer
        Private components As System.ComponentModel.IContainer

        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
            Me.Button_New = New DevExpress.XtraEditors.SimpleButton
            CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.GridView1, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.SuspendLayout()
            '
            'LabelControl1
            '
            Me.LabelControl1.Text = "Select the workshop that you wish to update or click New to create a new workshop" & _
                ""
            '
            'Button_OK
            '
            Me.Button_OK.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.Button_OK.Location = New System.Drawing.Point(218, 182)
            Me.Button_OK.TabIndex = 3
            '
            'Button_Cancel
            '
            Me.Button_Cancel.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.Button_Cancel.Location = New System.Drawing.Point(309, 182)
            Me.Button_Cancel.TabIndex = 4
            '
            'Button_New
            '
            Me.Button_New.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
            Me.Button_New.Location = New System.Drawing.Point(13, 182)
            Me.Button_New.Name = "Button_New"
            Me.Button_New.Size = New System.Drawing.Size(75, 23)
            Me.Button_New.TabIndex = 2
            Me.Button_New.Text = "&New..."
            Me.Button_New.ToolTip = "Create a new workshop"
            '
            'NewWorkshopSelectionControl
            '
            Me.Controls.Add(Me.Button_New)
            Me.Name = "NewWorkshopSelectionControl"
            Me.Controls.SetChildIndex(Me.Button_Cancel, 0)
            Me.Controls.SetChildIndex(Me.Button_OK, 0)
            Me.Controls.SetChildIndex(Me.Button_New, 0)
            Me.Controls.SetChildIndex(Me.LabelControl1, 0)
            Me.Controls.SetChildIndex(Me.GridControl1, 0)
            CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.GridView1, System.ComponentModel.ISupportInitialize).EndInit()
            Me.ResumeLayout(False)

        End Sub
        Friend WithEvents Button_New As DevExpress.XtraEditors.SimpleButton
    End Class
End Namespace
