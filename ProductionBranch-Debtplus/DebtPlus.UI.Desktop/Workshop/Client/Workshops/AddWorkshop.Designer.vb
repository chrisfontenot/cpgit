﻿Namespace Workshop.Client.Workshops
    <Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
    Partial Class NewWorkshop
        Inherits DebtPlus.Data.Forms.DebtPlusForm

        'Form overrides dispose to clean up the component list.
        <System.Diagnostics.DebuggerNonUserCode()> _
        Protected Overrides Sub Dispose(ByVal disposing As Boolean)
            Try
                If disposing AndAlso components IsNot Nothing Then
                    components.Dispose()
                End If
            Finally
                MyBase.Dispose(disposing)
            End Try
        End Sub

        'Required by the Windows Form Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Windows Form Designer
        'It can be modified using the Windows Form Designer.  
        'Do not modify it using the code editor.
        <System.Diagnostics.DebuggerStepThrough()> _
        Private Sub InitializeComponent()
            Dim SuperToolTip3 As DevExpress.Utils.SuperToolTip = New DevExpress.Utils.SuperToolTip
            Dim ToolTipTitleItem3 As DevExpress.Utils.ToolTipTitleItem = New DevExpress.Utils.ToolTipTitleItem
            Dim ToolTipItem3 As DevExpress.Utils.ToolTipItem = New DevExpress.Utils.ToolTipItem
            Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(NewWorkshop))
            Dim SuperToolTip1 As DevExpress.Utils.SuperToolTip = New DevExpress.Utils.SuperToolTip
            Dim ToolTipTitleItem1 As DevExpress.Utils.ToolTipTitleItem = New DevExpress.Utils.ToolTipTitleItem
            Dim ToolTipItem1 As DevExpress.Utils.ToolTipItem = New DevExpress.Utils.ToolTipItem
            Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl
            Me.LabelControl2 = New DevExpress.XtraEditors.LabelControl
            Me.lk_workshop = New DevExpress.XtraEditors.LookUpEdit
            Me.LabelControl3 = New DevExpress.XtraEditors.LabelControl
            Me.dt_date = New DevExpress.XtraEditors.DateEdit
            Me.LabelControl4 = New DevExpress.XtraEditors.LabelControl
            Me.lk_location = New DevExpress.XtraEditors.LookUpEdit
            Me.lk_counselor = New DevExpress.XtraEditors.LookUpEdit
            Me.LabelControl5 = New DevExpress.XtraEditors.LabelControl
            Me.Button_OK = New DevExpress.XtraEditors.SimpleButton
            Me.Button_Cancel = New DevExpress.XtraEditors.SimpleButton
            CType(Me.DxErrorProvider1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.lk_workshop.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.dt_date.Properties.VistaTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.dt_date.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.lk_location.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.lk_counselor.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.SuspendLayout()
            '
            'LabelControl1
            '
            Me.LabelControl1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                        Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.LabelControl1.Location = New System.Drawing.Point(12, 12)
            Me.LabelControl1.Name = "LabelControl1"
            Me.LabelControl1.Size = New System.Drawing.Size(307, 13)
            Me.LabelControl1.TabIndex = 0
            Me.LabelControl1.Text = "Please enter the information to create a new workshop instance"
            '
            'LabelControl2
            '
            Me.LabelControl2.Location = New System.Drawing.Point(12, 51)
            Me.LabelControl2.Name = "LabelControl2"
            Me.LabelControl2.Size = New System.Drawing.Size(48, 13)
            Me.LabelControl2.TabIndex = 1
            Me.LabelControl2.Text = "&Workshop"
            '
            'lk_workshop
            '
            Me.lk_workshop.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                        Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.lk_workshop.Location = New System.Drawing.Point(84, 48)
            Me.lk_workshop.Name = "lk_workshop"
            Me.lk_workshop.Properties.ActionButtonIndex = 1
            Me.lk_workshop.Properties.AllowFocused = False
            Me.lk_workshop.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Plus), New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.lk_workshop.Properties.Columns.AddRange(New DevExpress.XtraEditors.Controls.LookUpColumnInfo() {New DevExpress.XtraEditors.Controls.LookUpColumnInfo("description", "Description", 200, DevExpress.Utils.FormatType.None, "", True, DevExpress.Utils.HorzAlignment.Near)})
            Me.lk_workshop.Properties.NullText = ""
            Me.lk_workshop.Properties.ShowFooter = False
            Me.lk_workshop.Properties.ShowHeader = False
            Me.lk_workshop.Size = New System.Drawing.Size(281, 20)
            ToolTipTitleItem3.Text = "Workshop Type"
            ToolTipItem3.LeftIndent = 6
            ToolTipItem3.Text = resources.GetString("ToolTipItem3.Text")
            SuperToolTip3.Items.Add(ToolTipTitleItem3)
            SuperToolTip3.Items.Add(ToolTipItem3)
            Me.lk_workshop.SuperTip = SuperToolTip3
            Me.lk_workshop.TabIndex = 2
            Me.lk_workshop.Properties.SortColumnIndex = 0
            '
            'LabelControl3
            '
            Me.LabelControl3.Location = New System.Drawing.Point(12, 77)
            Me.LabelControl3.Name = "LabelControl3"
            Me.LabelControl3.Size = New System.Drawing.Size(40, 13)
            Me.LabelControl3.TabIndex = 3
            Me.LabelControl3.Text = "&Location"
            '
            'dt_date
            '
            Me.dt_date.EditValue = Nothing
            Me.dt_date.Location = New System.Drawing.Point(84, 126)
            Me.dt_date.Name = "dt_date"
            Me.dt_date.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.dt_date.Properties.DisplayFormat.FormatString = "g"
            Me.dt_date.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime
            Me.dt_date.Properties.EditFormat.FormatString = "g"
            Me.dt_date.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime
            Me.dt_date.Properties.Mask.BeepOnError = True
            Me.dt_date.Properties.Mask.EditMask = "g"
            Me.dt_date.Properties.VistaDisplayMode = DevExpress.Utils.DefaultBoolean.[True]
            Me.dt_date.Properties.VistaEditTime = DevExpress.Utils.DefaultBoolean.[True]
            Me.dt_date.Properties.VistaTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton})
            Me.dt_date.Size = New System.Drawing.Size(281, 20)
            Me.dt_date.TabIndex = 8
            Me.dt_date.ToolTip = "Date for the workshop"
            '
            'LabelControl4
            '
            Me.LabelControl4.Location = New System.Drawing.Point(12, 103)
            Me.LabelControl4.Name = "LabelControl4"
            Me.LabelControl4.Size = New System.Drawing.Size(47, 13)
            Me.LabelControl4.TabIndex = 5
            Me.LabelControl4.Text = "&Presenter"
            '
            'lk_location
            '
            Me.lk_location.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                        Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.lk_location.Location = New System.Drawing.Point(84, 74)
            Me.lk_location.Name = "lk_location"
            Me.lk_location.Properties.ActionButtonIndex = 1
            Me.lk_location.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Plus), New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.lk_location.Properties.Columns.AddRange(New DevExpress.XtraEditors.Controls.LookUpColumnInfo() {New DevExpress.XtraEditors.Controls.LookUpColumnInfo("name", "ID", 20, DevExpress.Utils.FormatType.None, "", True, DevExpress.Utils.HorzAlignment.[Default], DevExpress.Data.ColumnSortOrder.None)})
            Me.lk_location.Properties.NullText = ""
            Me.lk_location.Properties.ShowFooter = False
            Me.lk_location.Properties.ShowHeader = False
            Me.lk_location.Size = New System.Drawing.Size(281, 20)
            ToolTipTitleItem1.Text = "Workshop Location"
            ToolTipItem1.LeftIndent = 6
            ToolTipItem1.Text = resources.GetString("ToolTipItem1.Text")
            SuperToolTip1.Items.Add(ToolTipTitleItem1)
            SuperToolTip1.Items.Add(ToolTipItem1)
            Me.lk_location.SuperTip = SuperToolTip1
            Me.lk_location.TabIndex = 4
            Me.lk_location.Properties.SortColumnIndex = 0
            '
            'lk_counselor
            '
            Me.lk_counselor.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                        Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.lk_counselor.Location = New System.Drawing.Point(84, 100)
            Me.lk_counselor.Name = "lk_counselor"
            Me.lk_counselor.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.lk_counselor.Properties.Columns.AddRange(New DevExpress.XtraEditors.Controls.LookUpColumnInfo() {New DevExpress.XtraEditors.Controls.LookUpColumnInfo("Name", "ID", 20, DevExpress.Utils.FormatType.None, "", True, DevExpress.Utils.HorzAlignment.[Default], DevExpress.Data.ColumnSortOrder.None)})
            Me.lk_counselor.Properties.NullText = ""
            Me.lk_counselor.Properties.ShowFooter = False
            Me.lk_counselor.Properties.ShowHeader = False
            Me.lk_counselor.Size = New System.Drawing.Size(281, 20)
            Me.lk_counselor.TabIndex = 6
            Me.lk_counselor.ToolTip = "Name of the counselor/instructor who is conducting the workshop"
            Me.lk_counselor.Properties.SortColumnIndex = 0
            '
            'LabelControl5
            '
            Me.LabelControl5.Location = New System.Drawing.Point(12, 129)
            Me.LabelControl5.Name = "LabelControl5"
            Me.LabelControl5.Size = New System.Drawing.Size(49, 13)
            Me.LabelControl5.TabIndex = 7
            Me.LabelControl5.Text = "&Date/Time"
            '
            'Button_OK
            '
            Me.Button_OK.Anchor = System.Windows.Forms.AnchorStyles.Bottom
            Me.Button_OK.Enabled = False
            Me.Button_OK.Location = New System.Drawing.Point(110, 162)
            Me.Button_OK.Name = "Button_OK"
            Me.Button_OK.Size = New System.Drawing.Size(75, 23)
            Me.Button_OK.TabIndex = 10
            Me.Button_OK.Text = "&OK"
            '
            'Button_Cancel
            '
            Me.Button_Cancel.Anchor = System.Windows.Forms.AnchorStyles.Bottom
            Me.Button_Cancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
            Me.Button_Cancel.Location = New System.Drawing.Point(191, 162)
            Me.Button_Cancel.Name = "Button_Cancel"
            Me.Button_Cancel.Size = New System.Drawing.Size(75, 23)
            Me.Button_Cancel.TabIndex = 11
            Me.Button_Cancel.Text = "&Cancel"
            '
            'NewWorkshop
            '
            Me.AcceptButton = Me.Button_OK
            Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
            Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
            Me.CancelButton = Me.Button_Cancel
            Me.ClientSize = New System.Drawing.Size(377, 197)
            Me.Controls.Add(Me.Button_Cancel)
            Me.Controls.Add(Me.Button_OK)
            Me.Controls.Add(Me.LabelControl5)
            Me.Controls.Add(Me.lk_counselor)
            Me.Controls.Add(Me.lk_location)
            Me.Controls.Add(Me.LabelControl4)
            Me.Controls.Add(Me.dt_date)
            Me.Controls.Add(Me.LabelControl3)
            Me.Controls.Add(Me.lk_workshop)
            Me.Controls.Add(Me.LabelControl2)
            Me.Controls.Add(Me.LabelControl1)
            Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
            Me.Name = "NewWorkshop"
            Me.ToolTipController1.SetSuperTip(Me, Nothing)
            Me.Text = "New Workshop"
            CType(Me.DxErrorProvider1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.lk_workshop.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.dt_date.Properties.VistaTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.dt_date.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.lk_location.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.lk_counselor.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            Me.ResumeLayout(False)
            Me.PerformLayout()

        End Sub
        Protected Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
        Protected Friend WithEvents LabelControl2 As DevExpress.XtraEditors.LabelControl
        Protected Friend WithEvents lk_workshop As DevExpress.XtraEditors.LookUpEdit
        Protected Friend WithEvents LabelControl3 As DevExpress.XtraEditors.LabelControl
        Protected Friend WithEvents dt_date As DevExpress.XtraEditors.DateEdit
        Protected Friend WithEvents LabelControl4 As DevExpress.XtraEditors.LabelControl
        Protected Friend WithEvents lk_location As DevExpress.XtraEditors.LookUpEdit
        Protected Friend WithEvents lk_counselor As DevExpress.XtraEditors.LookUpEdit
        Protected Friend WithEvents LabelControl5 As DevExpress.XtraEditors.LabelControl
        Protected Friend WithEvents Button_OK As DevExpress.XtraEditors.SimpleButton
        Protected Friend WithEvents Button_Cancel As DevExpress.XtraEditors.SimpleButton
    End Class
End Namespace
