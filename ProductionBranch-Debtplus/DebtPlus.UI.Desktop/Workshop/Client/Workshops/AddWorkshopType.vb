#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}
#End Region

Option Compare Binary
Option Explicit On
Option Strict On

Imports System.Data.SqlClient
Imports System.Windows.Forms

Namespace Workshop.Client.Workshops
    Public Class AddWorkshopType

        Private drv As System.Data.DataRowView = Nothing
        Public Sub New(ByVal drv As System.Data.DataRowView)
            MyBase.New()
            Me.InitializeComponent()
            Me.drv = drv
            AddHandler Me.Load, AddressOf AddWorkshopType_Load
            AddHandler cbl_contents.ItemCheck, AddressOf cbl_contents_ItemCheck
            AddHandler tx_description.EditValueChanged, AddressOf tx_description_EditValueChanged
            AddHandler Button_OK.Click, AddressOf Button_OK_Click
        End Sub

#Region " MyCheckedControl "

        ''' <summary>
        ''' Class to handle the checked status of our indicators
        ''' </summary>
        Private Class MyCheckedControl
            Inherits DevExpress.XtraEditors.Controls.CheckedListBoxItem

            Public Shadows Property description() As String
                Get
                    Return Convert.ToString(Me.Value)
                End Get
                Set(ByVal value As String)
                    Me.Value = value
                End Set
            End Property

            Private _content_type As System.Int32 = -1
            Public Property content_type() As System.Int32
                Get
                    Return _content_type
                End Get
                Set(ByVal value As System.Int32)
                    _content_type = value
                End Set
            End Property

            Public Sub New(ByVal Description As String, ByVal content_type As System.Int32, ByVal CheckState As CheckState)
                MyBase.New()
                Me.description = Description
                Me.content_type = content_type
                Me.CheckState = CheckState
            End Sub
        End Class
#End Region

        Private Sub AddWorkshopType_Load(ByVal sender As System.Object, ByVal e As System.EventArgs)
            With tx_description
                If drv("description") Is Nothing OrElse drv("description") Is System.DBNull.Value Then drv("description") = String.Empty
                .DataBindings.Clear()
                .DataBindings.Add(New Binding("EditValue", drv, "description"))
                AddHandler .EditValueChanged, AddressOf tx_description_EditValueChanged
            End With

            With sp_duration
                If drv("duration") Is Nothing OrElse drv("duration") Is System.DBNull.Value Then drv("duration") = 15
                .DataBindings.Clear()
                .DataBindings.Add(New Binding("EditValue", drv, "duration"))
                AddHandler .EditValueChanging, AddressOf sp_duration_EditValueChanging
                AddHandler .EditValueChanged, AddressOf tx_description_EditValueChanged
            End With

            Dim tbl As DataTable = WorkshopContentsTable()
            Dim vue As DataView = New DataView(tbl, String.Empty, "description", DataViewRowState.CurrentRows)
            With cbl_contents
                With .Items
                    .Clear()
                    For Each drv As System.Data.DataRowView In vue
                        Dim description As String = Convert.ToString(drv("description"))
                        Dim content_type As System.Int32 = Convert.ToInt32(drv("content_type"))
                        Dim checked As CheckState = CType(drv("checked"), CheckState)
                        .Add(New MyCheckedControl(description, content_type, checked))
                    Next
                End With
            End With

            ' Enable/Disable the OK button
            EnableOK()
        End Sub

        Private Function WorkshopContentsTable() As DataTable
            Dim tbl As DataTable = Nothing
            Dim ds As New System.Data.DataSet("ds")

            Dim cmd As SqlClient.SqlCommand = New SqlCommand
            With cmd
                .Connection = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
                .CommandType = CommandType.Text
                If drv.IsNew Then
                    .CommandText = "select wt.content_type, wt.name as description, convert(int, 0) as checked from workshop_content_types wt"
                Else
                    .CommandText = "select wt.content_type, wt.name as description, convert(int, case when wc.content_type is not null then 1 else 0 end) as checked from workshop_content_types wt left outer join workshop_contents wc ON wt.content_type = wc.content_type AND wc.workshop_type = @workshop_type"
                    .Parameters.Add("@workshop_type", SqlDbType.Int).Value = drv("workshop_type")
                End If
            End With

            Dim da As New SqlClient.SqlDataAdapter(cmd)
            Dim current_cursor As System.Windows.Forms.Cursor = System.Windows.Forms.Cursor.Current

            Try
                System.Windows.Forms.Cursor.Current = Cursors.WaitCursor
                da.Fill(ds, "workshop_content_types")
                tbl = ds.Tables("workshop_content_types")

            Catch ex As SqlClient.SqlException
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error reading workshop_content_types table")

            Finally
                System.Windows.Forms.Cursor.Current = current_cursor
            End Try

            Return tbl
        End Function

        Private Sub sp_duration_EditValueChanging(ByVal sender As Object, ByVal e As DevExpress.XtraEditors.Controls.ChangingEventArgs)
            Dim NewValue As System.Int32
            If e.NewValue IsNot Nothing AndAlso Int32.TryParse(Convert.ToString(e.NewValue), NewValue) Then
                If NewValue > 0 Then Return
            End If
            e.Cancel = True
        End Sub

        Private Sub cbl_contents_ItemCheck(ByVal sender As Object, ByVal e As DevExpress.XtraEditors.Controls.ItemCheckEventArgs)
            EnableOK()
        End Sub

        Private Sub tx_description_EditValueChanged(ByVal sender As Object, ByVal e As System.EventArgs)
            EnableOK()
        End Sub

        Private Sub EnableOK()
            Do
                If Convert.ToString(tx_description.EditValue).Trim() = String.Empty Then Exit Do
                If Convert.ToInt32(sp_duration.EditValue) <= 0 Then Exit Do

                For Each item As MyCheckedControl In cbl_contents.Items
                    If item.CheckState = CheckState.Checked Then
                        Button_OK.Enabled = True
                        Return
                    End If
                Next
                Exit Do
            Loop

            Button_OK.Enabled = False
        End Sub

        Private Sub Button_OK_Click(ByVal sender As Object, ByVal e As System.EventArgs)
            Dim cn As SqlClient.SqlConnection = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
            Dim txn As SqlClient.SqlTransaction = Nothing
            Dim cmd As SqlClient.SqlCommand

            Dim current_cursor As System.Windows.Forms.Cursor = System.Windows.Forms.Cursor.Current
            Try
                System.Windows.Forms.Cursor.Current = Cursors.WaitCursor

                ' Open the connection and make a transaction
                cn.Open()
                txn = cn.BeginTransaction(IsolationLevel.ReadUncommitted)

                ' Start by ensuring that we have a workshop_type
                Dim workshop_type As System.Int32 = -1
                If Not drv.IsNew AndAlso drv("workshop_type") IsNot Nothing AndAlso drv("workshop_type") Is System.DBNull.Value Then workshop_type = Convert.ToInt32(drv("workshop_type"))

                If workshop_type < 0 Then
                    cmd = New SqlCommand
                    With cmd
                        .Connection = cn
                        .Transaction = txn
                        .CommandText = "INSERT INTO workshop_types (description,duration) VALUES (@description, @duration); SELECT scope_identity() AS workshop_type"
                        .Parameters.Add("@description", SqlDbType.VarChar, 50).Value = Convert.ToString(tx_description.EditValue).Trim()
                        .Parameters.Add("@duration", SqlDbType.Int).Value = Convert.ToInt32(sp_duration.EditValue)

                        Dim objAnswer As Object = .ExecuteScalar()
                        If objAnswer IsNot Nothing AndAlso objAnswer IsNot System.DBNull.Value Then workshop_type = Convert.ToInt32(objAnswer)
                    End With

                    If workshop_type < 0 Then Throw New ApplicationException("Error attempting to create a workshop_type")
                    drv("workshop_type") = workshop_type
                End If

                ' Discard all checked items for this workshop type
                cmd = New SqlCommand
                With cmd
                    .Connection = cn
                    .Transaction = txn
                    .CommandText = "DELETE FROM workshop_contents WHERE workshop_type=@workshop_type"
                    .Parameters.Add("@workshop_type", SqlDbType.Int).Value = workshop_type
                    .ExecuteNonQuery()
                End With

                ' Add any checked items to the list
                For Each item As MyCheckedControl In cbl_contents.Items
                    If item.CheckState = CheckState.Checked Then
                        cmd = New SqlCommand
                        With cmd
                            .Connection = cn
                            .Transaction = txn
                            .CommandText = "INSERT INTO workshop_contents(workshop_type, content_type) VALUES (@workshop_type, @content_type)"
                            .Parameters.Add("@workshop_type", SqlDbType.Int).Value = workshop_type
                            .Parameters.Add("@content_type", SqlDbType.Int).Value = item.content_type
                            .ExecuteNonQuery()
                        End With
                    End If
                Next

                ' Commit the transaction
                txn.Commit()
                txn = Nothing

                ' Complete the dialog properly
                DialogResult = Windows.Forms.DialogResult.OK

            Catch ex As SqlClient.SqlException
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error updating workshop_types information")

            Finally
                If txn IsNot Nothing Then
                    Try
                        txn.Rollback()
                    Catch exRollback As System.Exception
                    End Try
                    txn.Dispose()
                    txn = Nothing
                End If

                If cn IsNot Nothing Then cn.Dispose()
                System.Windows.Forms.Cursor.Current = current_cursor
            End Try
        End Sub
    End Class
End Namespace
