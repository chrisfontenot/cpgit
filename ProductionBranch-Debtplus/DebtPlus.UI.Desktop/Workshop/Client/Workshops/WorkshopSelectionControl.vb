#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}
#End Region

Option Compare Binary
Option Explicit On
Option Strict On

Imports System.Data.SqlClient
Imports System.Windows.Forms
Imports System.Drawing

Namespace Workshop.Client.Workshops
    Friend Class WorkshopSelectionControl
        Inherits DevExpress.XtraEditors.XtraUserControl

        Public Event Completed As EventHandler
        Public Event Cancelled As EventHandler

        Public Sub New()
            MyBase.New()
            InitializeComponent()

            ' Add the "Handles" clauses here
            AddHandler Button_Cancel.Click, AddressOf Button_Cancel_Click
            AddHandler Button_OK.Click, AddressOf Button_OK_Click
            AddHandler GridView1.FocusedRowChanged, AddressOf GridView1_FocusedRowChanged
            AddHandler GridView1.DoubleClick, AddressOf GridView1_DoubleClick
            AddHandler GridView1.MouseDown, AddressOf GridView1_MouseDown
        End Sub

        ''' <summary>
        ''' Process the CANCEL button on the form
        ''' </summary>
        Protected Sub RaiseCancelled()
            RaiseEvent Cancelled(Me, EventArgs.Empty)
        End Sub

        Protected Sub Button_Cancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
            RaiseCancelled()
        End Sub

        ''' <summary>
        ''' Process the OK button on the form
        ''' </summary>
        Protected Sub RaiseCompleted()
            RaiseEvent Completed(Me, EventArgs.Empty)
        End Sub

        Protected Sub Button_OK_Click(ByVal sender As Object, ByVal e As System.EventArgs)
            RaiseCompleted()
        End Sub

        ''' <summary>
        ''' Load the list of workshopes for the ACH files
        ''' </summary>
        Protected Friend Function Save() As Boolean
            Return True
        End Function

        Protected Friend Sub Process()
            Dim tbl As DataTable = ds.Tables("workshops")
            If tbl Is Nothing Then
                Dim cmd As SqlClient.SqlCommand = New SqlCommand
                With cmd
                    .Connection = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
                    .CommandText = "SELECT w.workshop, w.workshop_type, w.start_time, w.workshop_location, w.seats_available, w.CounselorID, w.HUD_Grant, w.HousingFeeAmount, w.Guest, w.date_created, w.created_by, w.counselor, dbo.format_counselor_name(w.counselor) AS formatted_counselor, wl.name AS formatted_workshop_location, wt.description AS formatted_workshop_type FROM workshops w WITH (NOLOCK) LEFT OUTER JOIN workshop_types wt WITH (NOLOCK) ON w.workshop_type = wt.workshop_type LEFT OUTER JOIN workshop_locations wl WITH (NOLOCK) ON w.workshop_location = wl.workshop_location WHERE w.start_time BETWEEN dbo.date_only (dateadd(d, -30, getdate())) AND dbo.date_only(dateadd(d, 31, getdate()))"
                    .CommandType = CommandType.Text
                End With

                Dim da As New SqlClient.SqlDataAdapter(cmd)
                Dim current_cursor As Cursor = System.Windows.Forms.Cursor.Current

                Try
                    System.Windows.Forms.Cursor.Current = Cursors.WaitCursor
                    da.Fill(ds, "workshops")
                    tbl = ds.Tables("workshops")

                Catch ex As SqlClient.SqlException
                    DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error reading workshops")

                Finally
                    System.Windows.Forms.Cursor.Current = current_cursor
                End Try
            End If

            With GridControl1
                .DataSource = tbl.DefaultView
                .RefreshDataSource()
            End With
        End Sub

        ''' <summary>
        ''' Process a change in the current row for the control
        ''' </summary>
        Protected Sub GridView1_FocusedRowChanged(ByVal sender As Object, ByVal e As DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs)
            Dim gv As DevExpress.XtraGrid.Views.Grid.GridView = CType(sender, DevExpress.XtraGrid.Views.Grid.GridView)
            Dim ctl As DevExpress.XtraGrid.GridControl = gv.GridControl
            Dim ControlRow As System.Int32 = e.FocusedRowHandle

            ' Find the datarowview from the input tables.
            Dim drv As System.Data.DataRowView = Nothing
            If ControlRow >= 0 Then
                Dim vue As DataView = CType(ctl.DataSource, DataView)
                drv = CType(gv.GetRow(ControlRow), System.Data.DataRowView)
            End If

            ' Ask the user to do something with the selected row
            workshop = -1
            If drv IsNot Nothing Then
                If drv("workshop") IsNot Nothing AndAlso drv("workshop") IsNot System.DBNull.Value Then workshop = Convert.ToInt32(drv("workshop"))
            End If
            Button_OK.Enabled = (workshop > 0)
        End Sub

        ''' <summary>
        ''' Double Click on an item -- edit it
        ''' </summary>
        Protected Sub GridView1_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs)

            ' Find the row targetted as the double-click item
            Dim gv As DevExpress.XtraGrid.Views.Grid.GridView = CType(sender, DevExpress.XtraGrid.Views.Grid.GridView)
            Dim ctl As DevExpress.XtraGrid.GridControl = CType(gv.GridControl, DevExpress.XtraGrid.GridControl)
            Dim hi As DevExpress.XtraGrid.Views.Grid.ViewInfo.GridHitInfo = gv.CalcHitInfo((ctl.PointToClient(Windows.Forms.Control.MousePosition)))
            Dim ControlRow As System.Int32 = hi.RowHandle

            ' Find the datarowview from the input tables.
            Dim drv As System.Data.DataRowView = Nothing
            If ControlRow >= 0 Then
                Dim vue As DataView = CType(ctl.DataSource, DataView)
                drv = CType(gv.GetRow(ControlRow), System.Data.DataRowView)
            End If

            ' If there is a row then select it
            workshop = -1
            If drv IsNot Nothing Then
                If drv("workshop") IsNot Nothing AndAlso drv("workshop") IsNot System.DBNull.Value Then workshop = Convert.ToInt32(drv("workshop"))
            End If

            ' If there is a workshop then select it
            If workshop > 0 Then Button_OK.PerformClick()
        End Sub

        ''' <summary>
        ''' Handle a mouse click on the control
        ''' </summary>
        Protected Sub GridView1_MouseDown(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs)
            Dim gv As DevExpress.XtraGrid.Views.Grid.GridView = GridView1
            Dim ctl As DevExpress.XtraGrid.GridControl = CType(gv.GridControl, DevExpress.XtraGrid.GridControl)
            Dim hi As DevExpress.XtraGrid.Views.Grid.ViewInfo.GridHitInfo = gv.CalcHitInfo(New Point(e.X, e.Y))

            ' Remember the position for the popup menu handler.
            Dim ControlRow As System.Int32 = -1
            If hi.InRow Then
                ControlRow = hi.RowHandle
            End If

            workshop = -1
            If ControlRow >= 0 Then
                Dim drv As System.Data.DataRowView = CType(GridView1.GetRow(ControlRow), System.Data.DataRowView)
                If drv IsNot Nothing Then
                    If drv("workshop") IsNot Nothing AndAlso drv("workshop") IsNot System.DBNull.Value Then workshop = Convert.ToInt32(drv("workshop"))
                End If
                Button_OK.Enabled = (workshop > 0)
            End If
        End Sub
    End Class
End Namespace
