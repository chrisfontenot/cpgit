﻿Namespace Workshop.Client.Workshops
    <Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
    Partial Class AddWorkshopType
        Inherits DebtPlus.Data.Forms.DebtPlusForm

        'Form overrides dispose to clean up the component list.
        <System.Diagnostics.DebuggerNonUserCode()> _
        Protected Overrides Sub Dispose(ByVal disposing As Boolean)
            Try
                If disposing AndAlso components IsNot Nothing Then
                    components.Dispose()
                End If
            Finally
                MyBase.Dispose(disposing)
            End Try
        End Sub

        'Required by the Windows Form Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Windows Form Designer
        'It can be modified using the Windows Form Designer.  
        'Do not modify it using the code editor.
        <System.Diagnostics.DebuggerStepThrough()> _
        Private Sub InitializeComponent()
            Me.LabelControl2 = New DevExpress.XtraEditors.LabelControl
            Me.tx_description = New DevExpress.XtraEditors.TextEdit
            Me.LabelControl3 = New DevExpress.XtraEditors.LabelControl
            Me.sp_duration = New DevExpress.XtraEditors.SpinEdit
            Me.LabelControl4 = New DevExpress.XtraEditors.LabelControl
            Me.cbl_contents = New DevExpress.XtraEditors.CheckedListBoxControl
            Me.LabelControl5 = New DevExpress.XtraEditors.LabelControl
            Me.Button_OK = New DevExpress.XtraEditors.SimpleButton
            Me.Button_Cancel = New DevExpress.XtraEditors.SimpleButton
            CType(Me.DxErrorProvider1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.tx_description.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.sp_duration.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.cbl_contents, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.SuspendLayout()
            '
            'LabelControl2
            '
            Me.LabelControl2.Location = New System.Drawing.Point(12, 18)
            Me.LabelControl2.Name = "LabelControl2"
            Me.LabelControl2.Size = New System.Drawing.Size(53, 13)
            Me.LabelControl2.TabIndex = 0
            Me.LabelControl2.Text = "Description"
            '
            'tx_description
            '
            Me.tx_description.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                        Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.tx_description.Location = New System.Drawing.Point(73, 15)
            Me.tx_description.Name = "tx_description"
            Me.tx_description.Properties.MaxLength = 50
            Me.tx_description.Size = New System.Drawing.Size(230, 20)
            Me.tx_description.TabIndex = 1
            '
            'LabelControl3
            '
            Me.LabelControl3.Location = New System.Drawing.Point(12, 44)
            Me.LabelControl3.Name = "LabelControl3"
            Me.LabelControl3.Size = New System.Drawing.Size(41, 13)
            Me.LabelControl3.TabIndex = 2
            Me.LabelControl3.Text = "Duration"
            '
            'sp_duration
            '
            Me.sp_duration.EditValue = New Decimal(New Int32() {15, 0, 0, 0})
            Me.sp_duration.Location = New System.Drawing.Point(73, 41)
            Me.sp_duration.Name = "sp_duration"
            Me.sp_duration.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[False]
            Me.sp_duration.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton})
            Me.sp_duration.Properties.Increment = New Decimal(New Int32() {15, 0, 0, 0})
            Me.sp_duration.Properties.IsFloatValue = False
            Me.sp_duration.Properties.Mask.BeepOnError = True
            Me.sp_duration.Properties.Mask.EditMask = "n0"
            Me.sp_duration.Properties.MaxValue = New Decimal(New Int32() {1440, 0, 0, 0})
            Me.sp_duration.Properties.NullText = "0"
            Me.sp_duration.Size = New System.Drawing.Size(57, 20)
            Me.sp_duration.TabIndex = 3
            Me.sp_duration.ToolTip = "Number of minutes that this workshop should take."
            '
            'LabelControl4
            '
            Me.LabelControl4.Location = New System.Drawing.Point(136, 44)
            Me.LabelControl4.Name = "LabelControl4"
            Me.LabelControl4.Size = New System.Drawing.Size(37, 13)
            Me.LabelControl4.TabIndex = 4
            Me.LabelControl4.Text = "minutes"
            '
            'cbl_contents
            '
            Me.cbl_contents.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                        Or System.Windows.Forms.AnchorStyles.Left) _
                        Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.cbl_contents.Location = New System.Drawing.Point(12, 97)
            Me.cbl_contents.Name = "cbl_contents"
            Me.cbl_contents.Size = New System.Drawing.Size(372, 160)
            Me.cbl_contents.TabIndex = 6
            '
            'LabelControl5
            '
            Me.LabelControl5.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                        Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.LabelControl5.Appearance.BackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Int32), CType(CType(192, Byte), Int32), CType(CType(255, Byte), Int32))
            Me.LabelControl5.Appearance.BackColor2 = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Int32), CType(CType(192, Byte), Int32), CType(CType(255, Byte), Int32))
            Me.LabelControl5.Appearance.BorderColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Int32), CType(CType(192, Byte), Int32), CType(CType(255, Byte), Int32))
            Me.LabelControl5.Appearance.Options.UseBackColor = True
            Me.LabelControl5.Appearance.Options.UseBorderColor = True
            Me.LabelControl5.Appearance.Options.UseTextOptions = True
            Me.LabelControl5.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
            Me.LabelControl5.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center
            Me.LabelControl5.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap
            Me.LabelControl5.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
            Me.LabelControl5.Location = New System.Drawing.Point(12, 78)
            Me.LabelControl5.Name = "LabelControl5"
            Me.LabelControl5.Size = New System.Drawing.Size(372, 13)
            Me.LabelControl5.TabIndex = 5
            Me.LabelControl5.Text = "Contents"
            '
            'Button_OK
            '
            Me.Button_OK.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.Button_OK.Enabled = False
            Me.Button_OK.Location = New System.Drawing.Point(309, 12)
            Me.Button_OK.Name = "Button_OK"
            Me.Button_OK.Size = New System.Drawing.Size(75, 23)
            Me.Button_OK.TabIndex = 7
            Me.Button_OK.Text = "&OK"
            '
            'Button_Cancel
            '
            Me.Button_Cancel.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.Button_Cancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
            Me.Button_Cancel.Location = New System.Drawing.Point(309, 41)
            Me.Button_Cancel.Name = "Button_Cancel"
            Me.Button_Cancel.Size = New System.Drawing.Size(75, 23)
            Me.Button_Cancel.TabIndex = 8
            Me.Button_Cancel.Text = "&Cancel"
            '
            'AddWorkshopType
            '
            Me.AcceptButton = Me.Button_OK
            Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
            Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
            Me.CancelButton = Me.Button_Cancel
            Me.ClientSize = New System.Drawing.Size(396, 269)
            Me.Controls.Add(Me.Button_Cancel)
            Me.Controls.Add(Me.Button_OK)
            Me.Controls.Add(Me.LabelControl5)
            Me.Controls.Add(Me.cbl_contents)
            Me.Controls.Add(Me.LabelControl4)
            Me.Controls.Add(Me.sp_duration)
            Me.Controls.Add(Me.LabelControl3)
            Me.Controls.Add(Me.tx_description)
            Me.Controls.Add(Me.LabelControl2)
            Me.Name = "AddWorkshopType"
            Me.ToolTipController1.SetSuperTip(Me, Nothing)
            Me.Text = "Add New Workshop Type"
            CType(Me.DxErrorProvider1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.tx_description.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.sp_duration.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.cbl_contents, System.ComponentModel.ISupportInitialize).EndInit()
            Me.ResumeLayout(False)
            Me.PerformLayout()

        End Sub
        Friend WithEvents LabelControl2 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents tx_description As DevExpress.XtraEditors.TextEdit
        Friend WithEvents LabelControl3 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents sp_duration As DevExpress.XtraEditors.SpinEdit
        Friend WithEvents LabelControl4 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents cbl_contents As DevExpress.XtraEditors.CheckedListBoxControl
        Friend WithEvents LabelControl5 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents Button_OK As DevExpress.XtraEditors.SimpleButton
        Friend WithEvents Button_Cancel As DevExpress.XtraEditors.SimpleButton
    End Class
End Namespace
