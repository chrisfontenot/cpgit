#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}
#End Region

Imports System.Data.SqlClient
Imports DebtPlus.Interfaces.Client
Imports DebtPlus.UI.Client.Widgets.Search
Imports System.Windows.Forms

Namespace Workshop.Client

    Public Class ClientSelectionControl
        Public Event Completed As EventHandler
        Public Event Cancelled As EventHandler

        ' Table for the updates of the clients
        Private WithEvents _EditTable As System.Data.DataTable = Nothing
        Private Property EditTable As DataTable
            Get
                Return _EditTable
            End Get
            Set(value As DataTable)
                If _EditTable IsNot Nothing Then
                    RemoveHandler _EditTable.ColumnChanged, AddressOf EditTable_ColumnChanged
                    RemoveHandler _EditTable.ColumnChanging, AddressOf EditTable_ColumnChanging
                    RemoveHandler _EditTable.TableNewRow, AddressOf EditTable_TableNewRow
                End If
                _EditTable = value
                If _EditTable IsNot Nothing Then
                    AddHandler _EditTable.ColumnChanged, AddressOf EditTable_ColumnChanged
                    AddHandler _EditTable.ColumnChanging, AddressOf EditTable_ColumnChanging
                    AddHandler _EditTable.TableNewRow, AddressOf EditTable_TableNewRow
                End If
            End Set
        End Property

        Private ClientColumn As System.Data.DataColumn = Nothing

        Public Sub New()
            MyBase.New()
            InitializeComponent()

            AddHandler Button_Cancel.Click, AddressOf Button_Cancel_Click
            AddHandler Button_OK.Click, AddressOf Button_OK_Click
            AddHandler RepositoryItemButtonEdit1.ButtonClick, AddressOf RepositoryItemButtonEdit1_Click
        End Sub

        ''' <summary>
        ''' Process the CANCEL button on the form
        ''' </summary>
        Protected Sub RaiseCancelled()
            RaiseEvent Cancelled(Me, EventArgs.Empty)
        End Sub

        Protected Sub Button_Cancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
            RaiseCancelled()
        End Sub

        ''' <summary>
        ''' Process the OK button on the form
        ''' </summary>
        Protected Sub RaiseCompleted()
            RaiseEvent Completed(Me, EventArgs.Empty)
        End Sub

        Protected Sub Button_OK_Click(ByVal sender As Object, ByVal e As System.EventArgs)
            RaiseCompleted()
        End Sub

        Protected Friend Sub Process()
            Dim cmd As SqlClient.SqlCommand = New SqlCommand
            With cmd
                .Connection = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
                .CommandText = "select ca.client_appointment, ca.client, v.name, v.active_status, dbo.address_block(v.addr1, v.addr2, v.addr3, default, default) as address, ca.workshop_people, ca.status, ca.result, ca.housing, ca.post_purchase, ca.credit, ca.callback_ph, ca.confirmation_status, ca.date_confirmed, ca.date_updated, ca.date_created, ca.created_by from client_appointments ca left outer join view_client_address v on ca.client = v.client where ca.office is null and ca.workshop = @workshop ORDER BY ca.client"
                .CommandType = CommandType.Text
                .Parameters.Add("@workshop", SqlDbType.Int).Value = GlobalValues.workshop
            End With

            Dim ds As New System.Data.DataSet("ds")
            Dim da As New SqlClient.SqlDataAdapter(cmd)
            da.Fill(ds, "client_appointments")

            EditTable = ds.Tables("client_appointments")
            With EditTable
                With .Columns("client_appointment")
                    .AutoIncrement = True
                    .AutoIncrementSeed = -1
                    .AutoIncrementStep = -1
                End With

                ClientColumn = .Columns("client")
                ClientColumn.DefaultValue = 0

                .Columns("date_created").DefaultValue = Now.Date
                .Columns("created_by").DefaultValue = "Me"
                .Columns("workshop_people").DefaultValue = 1
                .Columns("name").DefaultValue = String.Empty
                .Columns("housing").DefaultValue = False
                .Columns("credit").DefaultValue = False
                .Columns("post_purchase").DefaultValue = False
                .Columns("confirmation_status").DefaultValue = 0
                .Columns("status").DefaultValue = "K"

                .PrimaryKey = New DataColumn() {.Columns("client_appointment")}
                .Constraints.Add("UniqueClient", .Columns("client"), False)
            End With

            Dim vue As System.Data.DataView = EditTable.DefaultView
            With GridControl1
                .DataSource = vue
                .RefreshDataSource()
            End With

            Dim tbl As New System.Data.DataTable("appointment_status_types")
            With tbl
                .Columns.Add(New System.Data.DataColumn("status", GetType(String)))
                .Columns.Add(New System.Data.DataColumn("description", GetType(String)))
                With .Rows
                    .Add(New Object() {"M", "No Show"})
                    .Add(New Object() {"K", "Show"})
                End With
                .PrimaryKey = New DataColumn() {.Columns(0)}
            End With
            ds.Tables.Add(tbl)

            With RepositoryItemLookUpEdit1
                .DataSource = tbl
                .DisplayMember = "description"
                .ValueMember = "status"
                .NullText = "Show"
            End With

            With RepositoryItemButtonEdit1
                .NullText = System.String.Empty
                .AllowNullInput = DevExpress.Utils.DefaultBoolean.False

                With .DisplayFormat
                    .FormatString = "0000000"
                    .FormatType = DevExpress.Utils.FormatType.Custom
                    .Format = New DebtPlus.Utils.Format.Client.CustomFormatter
                End With

                With .EditFormat
                    .FormatString = "f0"
                    .FormatType = DevExpress.Utils.FormatType.Numeric
                End With

                ' Numbers only, please.
                With .Mask
                    .BeepOnError = True
                    .EditMask = "f0"
                    .MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
                End With

                .Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far

                With .Buttons(0)
                    .Tag = "search"
                    .ToolTip = "Click here to search for a client ID"

                    ' Load the search bitmap and attach it to the button
                    Dim _assembly As System.Reflection.Assembly = System.Reflection.Assembly.GetExecutingAssembly()
                    Dim ios As System.IO.Stream = _assembly.GetManifestResourceStream(_assembly.GetName.Name + ".Find.bmp")
                    .Image = New System.Drawing.Bitmap(ios)
                    ios.Close()

                    .Kind = DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph
                End With
            End With
        End Sub

        Private Sub RepositoryItemButtonEdit1_Click(ByVal sender As Object, ByVal e As EventArgs)
            Dim searchClass As IClientSearch = New ClientSearchClass()
            If searchClass IsNot Nothing Then
                With searchClass
                    If .ShowDialog() = DialogResult.OK Then
                        GridView1.SetFocusedRowCellValue(GridColumn_client, .ClientId)
                    End If
                End With
            End If
        End Sub

        Private Sub EditTable_ColumnChanged(ByVal sender As Object, ByVal e As DataColumnChangeEventArgs)
            Dim tbl As System.Data.DataTable = CType(sender, DataTable)

            With tbl
                If e.Column Is ClientColumn Then
                    Dim ValidClient As Boolean = False
                    Dim cn As SqlClient.SqlConnection = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
                    Dim rd As SqlClient.SqlDataReader = Nothing
                    Dim current_cursor As System.Windows.Forms.Cursor = System.Windows.Forms.Cursor.Current

                    If DebtPlus.Utils.Nulls.DInt(e.ProposedValue) > 0 Then
                        Try
                            cn.Open()
                            Using cmd As SqlClient.SqlCommand = New SqlCommand
                                With cmd
                                    .Connection = cn
                                    .CommandText = "SELECT c.active_status, dbo.format_normal_name(p.prefix, p.first, p.middle, p.last, p.suffix) as name, dbo.address_block(c.address1, c.address2, dbo.format_city_state_zip(c.city, c.state, c.postalcode), default, default) as address FROM clients c WITH (NOLOCK) LEFT OUTER JOIN people p ON p.client = c.client and 1 = p.relation WHERE c.client = @client"
                                    .CommandType = CommandType.Text
                                    .Parameters.Add("@client", SqlDbType.Int).Value = e.ProposedValue
                                    rd = .ExecuteReader(CommandBehavior.CloseConnection Or CommandBehavior.SingleRow)
                                End With
                            End Using

                            If rd.Read Then
                                e.Row.Item("active_status") = rd.GetValue(0)
                                e.Row.Item("name") = rd.GetValue(1)
                                e.Row.Item("address") = rd.GetValue(2)
                                ValidClient = True
                            End If

                        Finally
                            If rd IsNot Nothing Then rd.Dispose()
                            If cn IsNot Nothing Then cn.Dispose()
                            System.Windows.Forms.Cursor.Current = current_cursor
                        End Try
                    End If

                    If Not ValidClient Then
                        Throw New ArgumentException("Invalid Client")
                    End If
                End If
            End With
        End Sub

        Private Sub EditTable_ColumnChanging(ByVal sender As Object, ByVal e As System.Data.DataColumnChangeEventArgs)
        End Sub

        Private Sub EditTable_TableNewRow(ByVal sender As Object, ByVal e As System.Data.DataTableNewRowEventArgs)
            'Dim row As System.Data.DataRow = e.Row
            'With row
            '.Item("status") = "K"
            '.Item("workshop_people") = 1
            '.Item("client") = 0
            'End With
        End Sub

        Protected Friend Function Save() As Boolean
            Dim answer As Boolean = False
            Dim cn As SqlClient.SqlConnection = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
            Dim txn As SqlClient.SqlTransaction = Nothing
            Dim current_cursor As System.Windows.Forms.Cursor = System.Windows.Forms.Cursor.Current

            ' Ignore the save if there is no workshop
            If GlobalValues.workshop <= 0 OrElse EditTable Is Nothing Then Return True

            Try
                cn.Open()
                txn = cn.BeginTransaction

                ' Add the rows to the database
                Dim AddView As New System.Data.DataView(EditTable, String.Empty, String.Empty, DataViewRowState.Added)
                If AddView.Count > 0 Then
                    AddRows(AddView, cn, txn)
                End If

                ' Update the rows that have been changed
                Dim ChangeView As New System.Data.DataView(EditTable, String.Empty, String.Empty, DataViewRowState.ModifiedCurrent)
                If ChangeView.Count > 0 Then
                    ChangeRows(ChangeView, cn, txn)
                End If

                ' Delete the rows that have been deleted
                Dim DeleteView As New System.Data.DataView(EditTable, String.Empty, String.Empty, DataViewRowState.Deleted)
                If DeleteView.Count > 0 Then
                    DeleteRows(DeleteView, cn, txn)
                End If

                ' Commit the changes
                txn.Commit()
                txn = Nothing
                answer = True

            Catch ex As SqlClient.SqlException
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error updating client appointments table")

            Finally
                If txn IsNot Nothing Then
                    Try
                        txn.Rollback()
                    Catch exRollback As System.Exception
                    End Try
                    txn.Dispose()
                    txn = Nothing
                End If

                If cn IsNot Nothing Then cn.Dispose()

                System.Windows.Forms.Cursor.Current = current_cursor
            End Try

            Return answer
        End Function

        Private Sub AddRows(ByVal vue As System.Data.DataView, ByVal cn As SqlClient.SqlConnection, ByVal txn As SqlClient.SqlTransaction)

            For Each drv As System.Data.DataRowView In vue
                If drv("status") Is Nothing OrElse drv("status") Is System.DBNull.Value Then drv("status") = "K"

                Using cmd As SqlClient.SqlCommand = New SqlCommand
                    With cmd
                        .Connection = cn
                        .Transaction = txn

                        ' Add the workshop appointment to the client_appointments
                        .CommandText = "SET NOCOUNT ON; INSERT INTO client_appointments(client, workshop, workshop_people, status, start_time) SELECT @client, workshop, @workshop_people, @status, start_time FROM workshops WHERE workshop = @workshop; SELECT scope_identity() as client_appointment; SET NOCOUNT OFF;"
                        .Parameters.Add("@client", SqlDbType.Int).Value = drv("ClientId")
                        .Parameters.Add("@workshop_people", SqlDbType.Int).Value = drv("workshop_people")
                        .Parameters.Add("@status", SqlDbType.VarChar, 10).Value = drv("status")
                        .Parameters.Add("@workshop", SqlDbType.Int).Value = GlobalValues.workshop

                        ' Save the workshop appointment
                        drv("client_appointment") = .ExecuteScalar
                    End With
                End Using

                ' Change the client
                Using cmd As SqlClient.SqlCommand = New SqlCommand
                    With cmd
                        .Connection = cn
                        .Transaction = txn
                        .CommandText = "UPDATE update_clients SET active_status = 'WKS' WHERE client = @client AND active_status = 'CRE';"
                        .Parameters.Add("@client", SqlDbType.Int).Value = drv("client")
                        .ExecuteNonQuery()
                    End With
                End Using
            Next
        End Sub

        Private Sub ChangeRows(ByVal vue As System.Data.DataView, ByVal cn As SqlClient.SqlConnection, ByVal txn As SqlClient.SqlTransaction)
            For Each drv As System.Data.DataRowView In vue
                With New SqlCommand
                    .Connection = cn
                    .Transaction = txn

                    ' Add the workshop appointment to the client_appointments
                    .CommandText = "UPDATE client_appointments SET client = @client, workshop_people = @workshop_people, status = @status WHERE client_appointment = @client_appointment;"
                    .Parameters.Add("@client", SqlDbType.Int).Value = drv("client")
                    .Parameters.Add("@workshop_people", SqlDbType.Int).Value = drv("workshop_people")
                    .Parameters.Add("@status", SqlDbType.VarChar, 10).Value = drv("status")
                    .Parameters.Add("@client_appointment", SqlDbType.Int).Value = drv("client_appointment")

                    .ExecuteNonQuery()
                End With
            Next
        End Sub

        Private Sub DeleteRows(ByVal vue As System.Data.DataView, ByVal cn As SqlClient.SqlConnection, ByVal txn As SqlClient.SqlTransaction)
            For Each drv As System.Data.DataRowView In vue
                With New SqlCommand
                    .Connection = cn
                    .Transaction = txn

                    ' Add the workshop appointment to the client_appointments
                    .CommandText = "DELETE FROM client_appointments WHERE client_appointment = @client_appointment;"
                    .Parameters.Add("@client_appointment", SqlDbType.Int).Value = drv("client_appointment")

                    .ExecuteNonQuery()
                End With
            Next
        End Sub
    End Class
End Namespace
