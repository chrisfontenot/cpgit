﻿Namespace Workshop.Client
    <Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
    Partial Class ClientSelectionControl
        Inherits DevExpress.XtraEditors.XtraUserControl

        'UserControl overrides dispose to clean up the component list.
        <System.Diagnostics.DebuggerNonUserCode()> _
        Protected Overrides Sub Dispose(ByVal disposing As Boolean)
            Try
                If disposing AndAlso components IsNot Nothing Then
                    components.Dispose()
                End If
            Finally
                MyBase.Dispose(disposing)
            End Try
        End Sub

        'Required by the Windows Form Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Windows Form Designer
        'It can be modified using the Windows Form Designer.  
        'Do not modify it using the code editor.
        <System.Diagnostics.DebuggerStepThrough()> _
        Private Sub InitializeComponent()
            Me.Button_Cancel = New DevExpress.XtraEditors.SimpleButton
            Me.Button_OK = New DevExpress.XtraEditors.SimpleButton
            Me.GridControl1 = New DevExpress.XtraGrid.GridControl
            Me.GridView1 = New DevExpress.XtraGrid.Views.Grid.GridView
            Me.GridColumn_client_appointment = New DevExpress.XtraGrid.Columns.GridColumn
            Me.GridColumn_client_appointment.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True
            Me.GridColumn_client = New DevExpress.XtraGrid.Columns.GridColumn
            Me.GridColumn_client.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True
            Me.RepositoryItemButtonEdit1 = New DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit
            Me.GridColumn_name = New DevExpress.XtraGrid.Columns.GridColumn
            Me.GridColumn_name.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True
            Me.GridColumn_active_status = New DevExpress.XtraGrid.Columns.GridColumn
            Me.GridColumn_active_status.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True
            Me.GridColumn_workshop_people = New DevExpress.XtraGrid.Columns.GridColumn
            Me.GridColumn_workshop_people.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True
            Me.RepositoryItemSpinEdit1 = New DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit
            Me.GridColumn_status = New DevExpress.XtraGrid.Columns.GridColumn
            Me.GridColumn_status.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True
            Me.RepositoryItemLookUpEdit1 = New DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit
            Me.GridColumn_result = New DevExpress.XtraGrid.Columns.GridColumn
            Me.GridColumn_result.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True
            Me.GridColumn_housing = New DevExpress.XtraGrid.Columns.GridColumn
            Me.GridColumn_housing.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True
            Me.GridColumn_post_purchase = New DevExpress.XtraGrid.Columns.GridColumn
            Me.GridColumn_post_purchase.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True
            Me.GridColumn_credit = New DevExpress.XtraGrid.Columns.GridColumn
            Me.GridColumn_credit.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True
            Me.GridColumn_callback_ph = New DevExpress.XtraGrid.Columns.GridColumn
            Me.GridColumn_callback_ph.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True
            Me.GridColumn_confirmation_status = New DevExpress.XtraGrid.Columns.GridColumn
            Me.GridColumn_confirmation_status.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True
            Me.GridColumn_date_confirmed = New DevExpress.XtraGrid.Columns.GridColumn
            Me.GridColumn_date_confirmed.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True
            Me.GridColumn_date_updated = New DevExpress.XtraGrid.Columns.GridColumn
            Me.GridColumn_date_updated.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True
            Me.GridColumn_date_created = New DevExpress.XtraGrid.Columns.GridColumn
            Me.GridColumn_date_created.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True
            Me.GridColumn_created_by = New DevExpress.XtraGrid.Columns.GridColumn
            Me.GridColumn_created_by.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True
            Me.GridColumn_address = New DevExpress.XtraGrid.Columns.GridColumn
            Me.GridColumn_address.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True
            CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.GridView1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.RepositoryItemButtonEdit1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.RepositoryItemSpinEdit1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.RepositoryItemLookUpEdit1, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.SuspendLayout()
            '
            'Button_Cancel
            '
            Me.Button_Cancel.Anchor = System.Windows.Forms.AnchorStyles.Bottom
            Me.Button_Cancel.Location = New System.Drawing.Point(195, 152)
            Me.Button_Cancel.Name = "Button_Cancel"
            Me.Button_Cancel.Size = New System.Drawing.Size(75, 23)
            Me.Button_Cancel.TabIndex = 5
            Me.Button_Cancel.Text = "&Cancel"
            '
            'Button_OK
            '
            Me.Button_OK.Anchor = System.Windows.Forms.AnchorStyles.Bottom
            Me.Button_OK.Location = New System.Drawing.Point(99, 152)
            Me.Button_OK.Name = "Button_OK"
            Me.Button_OK.Size = New System.Drawing.Size(75, 23)
            Me.Button_OK.TabIndex = 4
            Me.Button_OK.Text = "&OK"
            '
            'GridControl1
            '
            Me.GridControl1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                        Or System.Windows.Forms.AnchorStyles.Left) _
                        Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.GridControl1.EmbeddedNavigator.Name = ""
            Me.GridControl1.Location = New System.Drawing.Point(0, 0)
            Me.GridControl1.MainView = Me.GridView1
            Me.GridControl1.Name = "GridControl1"
            Me.GridControl1.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.RepositoryItemLookUpEdit1, Me.RepositoryItemButtonEdit1, Me.RepositoryItemSpinEdit1})
            Me.GridControl1.Size = New System.Drawing.Size(369, 146)
            Me.GridControl1.TabIndex = 6
            Me.GridControl1.UseEmbeddedNavigator = True
            Me.GridControl1.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridView1})
            '
            'GridView1
            '
            Me.GridView1.Appearance.ColumnFilterButton.BackColor = System.Drawing.Color.FromArgb(CType(CType(104, Byte), Int32), CType(CType(184, Byte), Int32), CType(CType(251, Byte), Int32))
            Me.GridView1.Appearance.ColumnFilterButton.BorderColor = System.Drawing.Color.FromArgb(CType(CType(104, Byte), Int32), CType(CType(184, Byte), Int32), CType(CType(251, Byte), Int32))
            Me.GridView1.Appearance.ColumnFilterButton.ForeColor = System.Drawing.Color.White
            Me.GridView1.Appearance.ColumnFilterButton.Options.UseBackColor = True
            Me.GridView1.Appearance.ColumnFilterButton.Options.UseBorderColor = True
            Me.GridView1.Appearance.ColumnFilterButton.Options.UseForeColor = True
            Me.GridView1.Appearance.ColumnFilterButtonActive.BackColor = System.Drawing.Color.FromArgb(CType(CType(170, Byte), Int32), CType(CType(216, Byte), Int32), CType(CType(254, Byte), Int32))
            Me.GridView1.Appearance.ColumnFilterButtonActive.BorderColor = System.Drawing.Color.FromArgb(CType(CType(170, Byte), Int32), CType(CType(216, Byte), Int32), CType(CType(254, Byte), Int32))
            Me.GridView1.Appearance.ColumnFilterButtonActive.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.ColumnFilterButtonActive.Options.UseBackColor = True
            Me.GridView1.Appearance.ColumnFilterButtonActive.Options.UseBorderColor = True
            Me.GridView1.Appearance.ColumnFilterButtonActive.Options.UseForeColor = True
            Me.GridView1.Appearance.Empty.BackColor = System.Drawing.Color.FromArgb(CType(CType(236, Byte), Int32), CType(CType(246, Byte), Int32), CType(CType(255, Byte), Int32))
            Me.GridView1.Appearance.Empty.BackColor2 = System.Drawing.Color.White
            Me.GridView1.Appearance.Empty.Options.UseBackColor = True
            Me.GridView1.Appearance.EvenRow.BackColor = System.Drawing.Color.FromArgb(CType(CType(247, Byte), Int32), CType(CType(251, Byte), Int32), CType(CType(255, Byte), Int32))
            Me.GridView1.Appearance.EvenRow.BorderColor = System.Drawing.Color.FromArgb(CType(CType(247, Byte), Int32), CType(CType(251, Byte), Int32), CType(CType(255, Byte), Int32))
            Me.GridView1.Appearance.EvenRow.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.EvenRow.Options.UseBackColor = True
            Me.GridView1.Appearance.EvenRow.Options.UseBorderColor = True
            Me.GridView1.Appearance.EvenRow.Options.UseForeColor = True
            Me.GridView1.Appearance.FilterCloseButton.BackColor = System.Drawing.Color.FromArgb(CType(CType(104, Byte), Int32), CType(CType(184, Byte), Int32), CType(CType(251, Byte), Int32))
            Me.GridView1.Appearance.FilterCloseButton.BorderColor = System.Drawing.Color.FromArgb(CType(CType(104, Byte), Int32), CType(CType(184, Byte), Int32), CType(CType(251, Byte), Int32))
            Me.GridView1.Appearance.FilterCloseButton.ForeColor = System.Drawing.Color.White
            Me.GridView1.Appearance.FilterCloseButton.Options.UseBackColor = True
            Me.GridView1.Appearance.FilterCloseButton.Options.UseBorderColor = True
            Me.GridView1.Appearance.FilterCloseButton.Options.UseForeColor = True
            Me.GridView1.Appearance.FilterPanel.BackColor = System.Drawing.Color.FromArgb(CType(CType(236, Byte), Int32), CType(CType(246, Byte), Int32), CType(CType(255, Byte), Int32))
            Me.GridView1.Appearance.FilterPanel.BackColor2 = System.Drawing.Color.White
            Me.GridView1.Appearance.FilterPanel.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.FilterPanel.Options.UseBackColor = True
            Me.GridView1.Appearance.FilterPanel.Options.UseForeColor = True
            Me.GridView1.Appearance.FixedLine.BackColor = System.Drawing.Color.FromArgb(CType(CType(59, Byte), Int32), CType(CType(133, Byte), Int32), CType(CType(195, Byte), Int32))
            Me.GridView1.Appearance.FixedLine.Options.UseBackColor = True
            Me.GridView1.Appearance.FocusedCell.BackColor = System.Drawing.Color.White
            Me.GridView1.Appearance.FocusedCell.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.FocusedCell.Options.UseBackColor = True
            Me.GridView1.Appearance.FocusedCell.Options.UseForeColor = True
            Me.GridView1.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb(CType(CType(38, Byte), Int32), CType(CType(109, Byte), Int32), CType(CType(189, Byte), Int32))
            Me.GridView1.Appearance.FocusedRow.BorderColor = System.Drawing.Color.FromArgb(CType(CType(59, Byte), Int32), CType(CType(139, Byte), Int32), CType(CType(206, Byte), Int32))
            Me.GridView1.Appearance.FocusedRow.ForeColor = System.Drawing.Color.White
            Me.GridView1.Appearance.FocusedRow.Options.UseBackColor = True
            Me.GridView1.Appearance.FocusedRow.Options.UseBorderColor = True
            Me.GridView1.Appearance.FocusedRow.Options.UseForeColor = True
            Me.GridView1.Appearance.FooterPanel.BackColor = System.Drawing.Color.FromArgb(CType(CType(104, Byte), Int32), CType(CType(184, Byte), Int32), CType(CType(251, Byte), Int32))
            Me.GridView1.Appearance.FooterPanel.BorderColor = System.Drawing.Color.FromArgb(CType(CType(104, Byte), Int32), CType(CType(184, Byte), Int32), CType(CType(251, Byte), Int32))
            Me.GridView1.Appearance.FooterPanel.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.FooterPanel.Options.UseBackColor = True
            Me.GridView1.Appearance.FooterPanel.Options.UseBorderColor = True
            Me.GridView1.Appearance.FooterPanel.Options.UseForeColor = True
            Me.GridView1.Appearance.GroupButton.BackColor = System.Drawing.Color.FromArgb(CType(CType(104, Byte), Int32), CType(CType(184, Byte), Int32), CType(CType(251, Byte), Int32))
            Me.GridView1.Appearance.GroupButton.BorderColor = System.Drawing.Color.FromArgb(CType(CType(104, Byte), Int32), CType(CType(184, Byte), Int32), CType(CType(251, Byte), Int32))
            Me.GridView1.Appearance.GroupButton.Options.UseBackColor = True
            Me.GridView1.Appearance.GroupButton.Options.UseBorderColor = True
            Me.GridView1.Appearance.GroupFooter.BackColor = System.Drawing.Color.FromArgb(CType(CType(170, Byte), Int32), CType(CType(216, Byte), Int32), CType(CType(254, Byte), Int32))
            Me.GridView1.Appearance.GroupFooter.BorderColor = System.Drawing.Color.FromArgb(CType(CType(170, Byte), Int32), CType(CType(216, Byte), Int32), CType(CType(254, Byte), Int32))
            Me.GridView1.Appearance.GroupFooter.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.GroupFooter.Options.UseBackColor = True
            Me.GridView1.Appearance.GroupFooter.Options.UseBorderColor = True
            Me.GridView1.Appearance.GroupFooter.Options.UseForeColor = True
            Me.GridView1.Appearance.GroupPanel.BackColor = System.Drawing.Color.FromArgb(CType(CType(236, Byte), Int32), CType(CType(246, Byte), Int32), CType(CType(255, Byte), Int32))
            Me.GridView1.Appearance.GroupPanel.BackColor2 = System.Drawing.Color.White
            Me.GridView1.Appearance.GroupPanel.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.GroupPanel.Options.UseBackColor = True
            Me.GridView1.Appearance.GroupPanel.Options.UseForeColor = True
            Me.GridView1.Appearance.GroupRow.BackColor = System.Drawing.Color.FromArgb(CType(CType(170, Byte), Int32), CType(CType(216, Byte), Int32), CType(CType(254, Byte), Int32))
            Me.GridView1.Appearance.GroupRow.BorderColor = System.Drawing.Color.FromArgb(CType(CType(170, Byte), Int32), CType(CType(216, Byte), Int32), CType(CType(254, Byte), Int32))
            Me.GridView1.Appearance.GroupRow.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.GroupRow.Options.UseBackColor = True
            Me.GridView1.Appearance.GroupRow.Options.UseBorderColor = True
            Me.GridView1.Appearance.GroupRow.Options.UseForeColor = True
            Me.GridView1.Appearance.HeaderPanel.BackColor = System.Drawing.Color.FromArgb(CType(CType(139, Byte), Int32), CType(CType(201, Byte), Int32), CType(CType(254, Byte), Int32))
            Me.GridView1.Appearance.HeaderPanel.BorderColor = System.Drawing.Color.FromArgb(CType(CType(139, Byte), Int32), CType(CType(201, Byte), Int32), CType(CType(254, Byte), Int32))
            Me.GridView1.Appearance.HeaderPanel.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.HeaderPanel.Options.UseBackColor = True
            Me.GridView1.Appearance.HeaderPanel.Options.UseBorderColor = True
            Me.GridView1.Appearance.HeaderPanel.Options.UseForeColor = True
            Me.GridView1.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Int32), CType(CType(170, Byte), Int32), CType(CType(225, Byte), Int32))
            Me.GridView1.Appearance.HideSelectionRow.BorderColor = System.Drawing.Color.FromArgb(CType(CType(83, Byte), Int32), CType(CType(155, Byte), Int32), CType(CType(215, Byte), Int32))
            Me.GridView1.Appearance.HideSelectionRow.ForeColor = System.Drawing.Color.FromArgb(CType(CType(236, Byte), Int32), CType(CType(246, Byte), Int32), CType(CType(255, Byte), Int32))
            Me.GridView1.Appearance.HideSelectionRow.Options.UseBackColor = True
            Me.GridView1.Appearance.HideSelectionRow.Options.UseBorderColor = True
            Me.GridView1.Appearance.HideSelectionRow.Options.UseForeColor = True
            Me.GridView1.Appearance.HorzLine.BackColor = System.Drawing.Color.FromArgb(CType(CType(104, Byte), Int32), CType(CType(184, Byte), Int32), CType(CType(251, Byte), Int32))
            Me.GridView1.Appearance.HorzLine.Options.UseBackColor = True
            Me.GridView1.Appearance.OddRow.BackColor = System.Drawing.Color.FromArgb(CType(CType(236, Byte), Int32), CType(CType(246, Byte), Int32), CType(CType(255, Byte), Int32))
            Me.GridView1.Appearance.OddRow.BorderColor = System.Drawing.Color.FromArgb(CType(CType(236, Byte), Int32), CType(CType(246, Byte), Int32), CType(CType(255, Byte), Int32))
            Me.GridView1.Appearance.OddRow.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.OddRow.Options.UseBackColor = True
            Me.GridView1.Appearance.OddRow.Options.UseBorderColor = True
            Me.GridView1.Appearance.OddRow.Options.UseForeColor = True
            Me.GridView1.Appearance.Preview.Font = New System.Drawing.Font("Verdana", 7.5!)
            Me.GridView1.Appearance.Preview.ForeColor = System.Drawing.Color.FromArgb(CType(CType(83, Byte), Int32), CType(CType(155, Byte), Int32), CType(CType(215, Byte), Int32))
            Me.GridView1.Appearance.Preview.Options.UseFont = True
            Me.GridView1.Appearance.Preview.Options.UseForeColor = True
            Me.GridView1.Appearance.Row.BackColor = System.Drawing.Color.FromArgb(CType(CType(247, Byte), Int32), CType(CType(251, Byte), Int32), CType(CType(255, Byte), Int32))
            Me.GridView1.Appearance.Row.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.Row.Options.UseBackColor = True
            Me.GridView1.Appearance.Row.Options.UseForeColor = True
            Me.GridView1.Appearance.RowSeparator.BackColor = System.Drawing.Color.FromArgb(CType(CType(236, Byte), Int32), CType(CType(246, Byte), Int32), CType(CType(255, Byte), Int32))
            Me.GridView1.Appearance.RowSeparator.BackColor2 = System.Drawing.Color.White
            Me.GridView1.Appearance.RowSeparator.Options.UseBackColor = True
            Me.GridView1.Appearance.SelectedRow.BackColor = System.Drawing.Color.FromArgb(CType(CType(83, Byte), Int32), CType(CType(155, Byte), Int32), CType(CType(215, Byte), Int32))
            Me.GridView1.Appearance.SelectedRow.ForeColor = System.Drawing.Color.White
            Me.GridView1.Appearance.SelectedRow.Options.UseBackColor = True
            Me.GridView1.Appearance.SelectedRow.Options.UseForeColor = True
            Me.GridView1.Appearance.TopNewRow.BackColor = System.Drawing.Color.White
            Me.GridView1.Appearance.TopNewRow.Options.UseBackColor = True
            Me.GridView1.Appearance.VertLine.BackColor = System.Drawing.Color.FromArgb(CType(CType(104, Byte), Int32), CType(CType(184, Byte), Int32), CType(CType(251, Byte), Int32))
            Me.GridView1.Appearance.VertLine.Options.UseBackColor = True
            Me.GridView1.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.GridColumn_client_appointment, Me.GridColumn_client, Me.GridColumn_name, Me.GridColumn_active_status, Me.GridColumn_workshop_people, Me.GridColumn_status, Me.GridColumn_result, Me.GridColumn_housing, Me.GridColumn_post_purchase, Me.GridColumn_credit, Me.GridColumn_callback_ph, Me.GridColumn_confirmation_status, Me.GridColumn_date_confirmed, Me.GridColumn_date_updated, Me.GridColumn_date_created, Me.GridColumn_created_by, Me.GridColumn_address})
            Me.GridView1.CustomizationFormBounds = New System.Drawing.Rectangle(638, 264, 208, 170)
            Me.GridView1.GridControl = Me.GridControl1
            Me.GridView1.Name = "GridView1"
            Me.GridView1.NewItemRowText = "Add new items here..."
            Me.GridView1.OptionsCustomization.AllowRowSizing = True
            Me.GridView1.OptionsDetail.AutoZoomDetail = True
            Me.GridView1.OptionsFilter.UseNewCustomFilterDialog = True
            Me.GridView1.OptionsNavigation.EnterMoveNextColumn = True
            Me.GridView1.OptionsView.EnableAppearanceEvenRow = True
            Me.GridView1.OptionsView.EnableAppearanceOddRow = True
            Me.GridView1.OptionsView.NewItemRowPosition = DevExpress.XtraGrid.Views.Grid.NewItemRowPosition.Top
            Me.GridView1.OptionsView.RowAutoHeight = True
            Me.GridView1.OptionsView.ShowGroupPanel = False
            '
            'GridColumn_client_appointment
            '
            Me.GridColumn_client_appointment.AppearanceCell.Options.UseTextOptions = True
            Me.GridColumn_client_appointment.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.GridColumn_client_appointment.AppearanceHeader.Options.UseTextOptions = True
            Me.GridColumn_client_appointment.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.GridColumn_client_appointment.Caption = "ID"
            Me.GridColumn_client_appointment.CustomizationCaption = "Record ID"
            Me.GridColumn_client_appointment.DisplayFormat.FormatString = "f0"
            Me.GridColumn_client_appointment.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.GridColumn_client_appointment.FieldName = "client_appointment"
            Me.GridColumn_client_appointment.GroupFormat.FormatString = "f0"
            Me.GridColumn_client_appointment.GroupFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.GridColumn_client_appointment.Name = "GridColumn_client_appointment"
            Me.GridColumn_client_appointment.OptionsColumn.AllowEdit = False
            Me.GridColumn_client_appointment.ToolTip = "ID of record in database"
            '
            'GridColumn_client
            '
            Me.GridColumn_client.AppearanceCell.Options.UseTextOptions = True
            Me.GridColumn_client.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.GridColumn_client.AppearanceHeader.Options.UseTextOptions = True
            Me.GridColumn_client.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.GridColumn_client.Caption = "Client"
            Me.GridColumn_client.ColumnEdit = Me.RepositoryItemButtonEdit1
            Me.GridColumn_client.CustomizationCaption = "Client ID"
            Me.GridColumn_client.DisplayFormat.FormatString = "f0"
            Me.GridColumn_client.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.GridColumn_client.FieldName = "client"
            Me.GridColumn_client.GroupFormat.FormatString = "f0"
            Me.GridColumn_client.GroupFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.GridColumn_client.Name = "GridColumn_client"
            Me.GridColumn_client.ToolTip = "Client ID number"
            Me.GridColumn_client.Visible = True
            Me.GridColumn_client.VisibleIndex = 0
            Me.GridColumn_client.Width = 76
            '
            'RepositoryItemButtonEdit1
            '
            Me.RepositoryItemButtonEdit1.AutoHeight = False
            Me.RepositoryItemButtonEdit1.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton})
            Me.RepositoryItemButtonEdit1.DisplayFormat.FormatString = "f0"
            Me.RepositoryItemButtonEdit1.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom
            Me.RepositoryItemButtonEdit1.EditFormat.FormatString = "f0"
            Me.RepositoryItemButtonEdit1.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.RepositoryItemButtonEdit1.Name = "RepositoryItemButtonEdit1"
            '
            'GridColumn_name
            '
            Me.GridColumn_name.Caption = "Name"
            Me.GridColumn_name.CustomizationCaption = "Client Name"
            Me.GridColumn_name.FieldName = "name"
            Me.GridColumn_name.Name = "GridColumn_name"
            Me.GridColumn_name.OptionsColumn.AllowEdit = False
            Me.GridColumn_name.ToolTip = "Client Name"
            Me.GridColumn_name.Visible = True
            Me.GridColumn_name.VisibleIndex = 3
            Me.GridColumn_name.Width = 255
            '
            'GridColumn_active_status
            '
            Me.GridColumn_active_status.Caption = "Active Status"
            Me.GridColumn_active_status.CustomizationCaption = "Active Status"
            Me.GridColumn_active_status.FieldName = "active_status"
            Me.GridColumn_active_status.Name = "GridColumn_active_status"
            Me.GridColumn_active_status.OptionsColumn.AllowEdit = False
            Me.GridColumn_active_status.ToolTip = "Active Status"
            '
            'GridColumn_workshop_people
            '
            Me.GridColumn_workshop_people.AppearanceCell.Options.UseTextOptions = True
            Me.GridColumn_workshop_people.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.GridColumn_workshop_people.AppearanceHeader.Options.UseTextOptions = True
            Me.GridColumn_workshop_people.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.GridColumn_workshop_people.Caption = "People"
            Me.GridColumn_workshop_people.ColumnEdit = Me.RepositoryItemSpinEdit1
            Me.GridColumn_workshop_people.CustomizationCaption = "Number of people attending"
            Me.GridColumn_workshop_people.DisplayFormat.FormatString = "f0"
            Me.GridColumn_workshop_people.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.GridColumn_workshop_people.FieldName = "workshop_people"
            Me.GridColumn_workshop_people.GroupFormat.FormatString = "f0"
            Me.GridColumn_workshop_people.GroupFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.GridColumn_workshop_people.Name = "GridColumn_workshop_people"
            Me.GridColumn_workshop_people.ToolTip = "Number of people attending"
            Me.GridColumn_workshop_people.Visible = True
            Me.GridColumn_workshop_people.VisibleIndex = 1
            Me.GridColumn_workshop_people.Width = 83
            '
            'RepositoryItemSpinEdit1
            '
            Me.RepositoryItemSpinEdit1.AutoHeight = False
            Me.RepositoryItemSpinEdit1.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton})
            Me.RepositoryItemSpinEdit1.DisplayFormat.FormatString = "f0"
            Me.RepositoryItemSpinEdit1.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.RepositoryItemSpinEdit1.EditFormat.FormatString = "f0"
            Me.RepositoryItemSpinEdit1.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.RepositoryItemSpinEdit1.IsFloatValue = False
            Me.RepositoryItemSpinEdit1.Mask.EditMask = "N00"
            Me.RepositoryItemSpinEdit1.MaxLength = 3
            Me.RepositoryItemSpinEdit1.MaxValue = New Decimal(New Int32() {100, 0, 0, 0})
            Me.RepositoryItemSpinEdit1.MinValue = New Decimal(New Int32() {1, 0, 0, 0})
            Me.RepositoryItemSpinEdit1.Name = "RepositoryItemSpinEdit1"
            '
            'GridColumn_status
            '
            Me.GridColumn_status.Caption = "Status"
            Me.GridColumn_status.ColumnEdit = Me.RepositoryItemLookUpEdit1
            Me.GridColumn_status.CustomizationCaption = "Appointment Status"
            Me.GridColumn_status.FieldName = "status"
            Me.GridColumn_status.Name = "GridColumn_status"
            Me.GridColumn_status.ToolTip = "Appointment Status"
            Me.GridColumn_status.Visible = True
            Me.GridColumn_status.VisibleIndex = 2
            Me.GridColumn_status.Width = 103
            '
            'RepositoryItemLookUpEdit1
            '
            Me.RepositoryItemLookUpEdit1.AllowNullInput = DevExpress.Utils.DefaultBoolean.[False]
            Me.RepositoryItemLookUpEdit1.AutoHeight = False
            Me.RepositoryItemLookUpEdit1.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.RepositoryItemLookUpEdit1.Columns.AddRange(New DevExpress.XtraEditors.Controls.LookUpColumnInfo() {New DevExpress.XtraEditors.Controls.LookUpColumnInfo("status", "Status", 20, DevExpress.Utils.FormatType.None, "", False, DevExpress.Utils.HorzAlignment.[Default], DevExpress.Data.ColumnSortOrder.None), New DevExpress.XtraEditors.Controls.LookUpColumnInfo("description", "Description", 20, DevExpress.Utils.FormatType.None, "", True, DevExpress.Utils.HorzAlignment.[Default], DevExpress.Data.ColumnSortOrder.None)})
            Me.RepositoryItemLookUpEdit1.Name = "RepositoryItemLookUpEdit1"
            Me.RepositoryItemLookUpEdit1.ShowFooter = False
            '
            'GridColumn_result
            '
            Me.GridColumn_result.Caption = "Result"
            Me.GridColumn_result.CustomizationCaption = "Appointment Result"
            Me.GridColumn_result.FieldName = "result"
            Me.GridColumn_result.Name = "GridColumn_result"
            Me.GridColumn_result.OptionsColumn.AllowEdit = False
            Me.GridColumn_result.ToolTip = "Appointment Result"
            '
            'GridColumn_housing
            '
            Me.GridColumn_housing.AppearanceCell.Options.UseTextOptions = True
            Me.GridColumn_housing.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
            Me.GridColumn_housing.AppearanceHeader.Options.UseTextOptions = True
            Me.GridColumn_housing.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
            Me.GridColumn_housing.Caption = "Housing"
            Me.GridColumn_housing.CustomizationCaption = "Housing Appointment?"
            Me.GridColumn_housing.FieldName = "housing"
            Me.GridColumn_housing.Name = "GridColumn_housing"
            Me.GridColumn_housing.OptionsColumn.AllowEdit = False
            Me.GridColumn_housing.ToolTip = "Is this a housing appointment?"
            '
            'GridColumn_post_purchase
            '
            Me.GridColumn_post_purchase.AppearanceCell.Options.UseTextOptions = True
            Me.GridColumn_post_purchase.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
            Me.GridColumn_post_purchase.AppearanceHeader.Options.UseTextOptions = True
            Me.GridColumn_post_purchase.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
            Me.GridColumn_post_purchase.Caption = "Post"
            Me.GridColumn_post_purchase.CustomizationCaption = "Post Purchase Appointment?"
            Me.GridColumn_post_purchase.FieldName = "post_purchase"
            Me.GridColumn_post_purchase.Name = "GridColumn_post_purchase"
            Me.GridColumn_post_purchase.OptionsColumn.AllowEdit = False
            Me.GridColumn_post_purchase.ToolTip = "Is this a post-purchase appointment?"
            '
            'GridColumn_credit
            '
            Me.GridColumn_credit.AppearanceCell.Options.UseTextOptions = True
            Me.GridColumn_credit.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
            Me.GridColumn_credit.AppearanceHeader.Options.UseTextOptions = True
            Me.GridColumn_credit.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
            Me.GridColumn_credit.Caption = "Credit"
            Me.GridColumn_credit.CustomizationCaption = "Credit Review Appointment?"
            Me.GridColumn_credit.FieldName = "credit"
            Me.GridColumn_credit.Name = "GridColumn_credit"
            Me.GridColumn_credit.OptionsColumn.AllowEdit = False
            Me.GridColumn_credit.ToolTip = "Is this a credit review appointment?"
            '
            'GridColumn_callback_ph
            '
            Me.GridColumn_callback_ph.Caption = "Callback #"
            Me.GridColumn_callback_ph.CustomizationCaption = "Callback Telephone No."
            Me.GridColumn_callback_ph.FieldName = "callback_ph"
            Me.GridColumn_callback_ph.Name = "GridColumn_callback_ph"
            Me.GridColumn_callback_ph.OptionsColumn.AllowEdit = False
            Me.GridColumn_callback_ph.ToolTip = "Callback telephone number"
            '
            'GridColumn_confirmation_status
            '
            Me.GridColumn_confirmation_status.Caption = "Confirmed"
            Me.GridColumn_confirmation_status.CustomizationCaption = "Confirmation Status"
            Me.GridColumn_confirmation_status.FieldName = "confirmation_status"
            Me.GridColumn_confirmation_status.Name = "GridColumn_confirmation_status"
            Me.GridColumn_confirmation_status.OptionsColumn.AllowEdit = False
            Me.GridColumn_confirmation_status.ToolTip = "Appointment Confirmation Status"
            '
            'GridColumn_date_confirmed
            '
            Me.GridColumn_date_confirmed.AppearanceCell.Options.UseTextOptions = True
            Me.GridColumn_date_confirmed.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.GridColumn_date_confirmed.AppearanceHeader.Options.UseTextOptions = True
            Me.GridColumn_date_confirmed.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.GridColumn_date_confirmed.Caption = "date_confirmed"
            Me.GridColumn_date_confirmed.CustomizationCaption = "date_confirmed"
            Me.GridColumn_date_confirmed.DisplayFormat.FormatString = "d"
            Me.GridColumn_date_confirmed.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime
            Me.GridColumn_date_confirmed.FieldName = "date_confirmed"
            Me.GridColumn_date_confirmed.GroupFormat.FormatString = "d"
            Me.GridColumn_date_confirmed.GroupFormat.FormatType = DevExpress.Utils.FormatType.DateTime
            Me.GridColumn_date_confirmed.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.[Date]
            Me.GridColumn_date_confirmed.Name = "GridColumn_date_confirmed"
            Me.GridColumn_date_confirmed.OptionsColumn.AllowEdit = False
            Me.GridColumn_date_confirmed.ToolTip = "Date appointment was confirmed"
            '
            'GridColumn_date_updated
            '
            Me.GridColumn_date_updated.AppearanceCell.Options.UseTextOptions = True
            Me.GridColumn_date_updated.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.GridColumn_date_updated.AppearanceHeader.Options.UseTextOptions = True
            Me.GridColumn_date_updated.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.GridColumn_date_updated.Caption = "Date Updated"
            Me.GridColumn_date_updated.CustomizationCaption = "Date Record Updated"
            Me.GridColumn_date_updated.DisplayFormat.FormatString = "d"
            Me.GridColumn_date_updated.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime
            Me.GridColumn_date_updated.FieldName = "date_updated"
            Me.GridColumn_date_updated.GroupFormat.FormatString = "d"
            Me.GridColumn_date_updated.GroupFormat.FormatType = DevExpress.Utils.FormatType.DateTime
            Me.GridColumn_date_updated.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.[Date]
            Me.GridColumn_date_updated.Name = "GridColumn_date_updated"
            Me.GridColumn_date_updated.OptionsColumn.AllowEdit = False
            Me.GridColumn_date_updated.ToolTip = "Date appointment was last changed"
            '
            'GridColumn_date_created
            '
            Me.GridColumn_date_created.AppearanceCell.Options.UseTextOptions = True
            Me.GridColumn_date_created.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.GridColumn_date_created.AppearanceHeader.Options.UseTextOptions = True
            Me.GridColumn_date_created.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.GridColumn_date_created.Caption = "Created"
            Me.GridColumn_date_created.CustomizationCaption = "Date Record Created"
            Me.GridColumn_date_created.DisplayFormat.FormatString = "d"
            Me.GridColumn_date_created.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime
            Me.GridColumn_date_created.FieldName = "date_created"
            Me.GridColumn_date_created.GroupFormat.FormatString = "d"
            Me.GridColumn_date_created.GroupFormat.FormatType = DevExpress.Utils.FormatType.DateTime
            Me.GridColumn_date_created.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.[Date]
            Me.GridColumn_date_created.Name = "GridColumn_date_created"
            Me.GridColumn_date_created.OptionsColumn.AllowEdit = False
            Me.GridColumn_date_created.ToolTip = "Date/Time when the appointment was created"
            '
            'GridColumn_created_by
            '
            Me.GridColumn_created_by.Caption = "Counselor"
            Me.GridColumn_created_by.CustomizationCaption = "Person who created the record"
            Me.GridColumn_created_by.FieldName = "created_by"
            Me.GridColumn_created_by.Name = "GridColumn_created_by"
            Me.GridColumn_created_by.OptionsColumn.AllowEdit = False
            Me.GridColumn_created_by.ToolTip = "Name of the person who created the appointment"
            '
            'GridColumn_address
            '
            Me.GridColumn_address.Caption = "Address"
            Me.GridColumn_address.CustomizationCaption = "Client Address"
            Me.GridColumn_address.FieldName = "address"
            Me.GridColumn_address.Name = "GridColumn_address"
            Me.GridColumn_address.OptionsColumn.AllowEdit = False
            Me.GridColumn_address.OptionsColumn.AllowGroup = DevExpress.Utils.DefaultBoolean.[False]
            Me.GridColumn_address.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
            Me.GridColumn_address.ToolTip = "Client's Address"
            '
            'ClientSelectionControl
            '
            Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
            Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
            Me.Controls.Add(Me.GridControl1)
            Me.Controls.Add(Me.Button_Cancel)
            Me.Controls.Add(Me.Button_OK)
            Me.Name = "ClientSelectionControl"
            Me.Size = New System.Drawing.Size(369, 189)
            CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.GridView1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.RepositoryItemButtonEdit1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.RepositoryItemSpinEdit1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.RepositoryItemLookUpEdit1, System.ComponentModel.ISupportInitialize).EndInit()
            Me.ResumeLayout(False)

        End Sub
        Protected Friend WithEvents Button_Cancel As DevExpress.XtraEditors.SimpleButton
        Protected Friend WithEvents Button_OK As DevExpress.XtraEditors.SimpleButton
        Friend WithEvents GridControl1 As DevExpress.XtraGrid.GridControl
        Friend WithEvents GridView1 As DevExpress.XtraGrid.Views.Grid.GridView
        Friend WithEvents GridColumn_client_appointment As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents GridColumn_client As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents GridColumn_name As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents GridColumn_active_status As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents GridColumn_workshop_people As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents GridColumn_status As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents GridColumn_result As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents GridColumn_housing As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents GridColumn_post_purchase As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents GridColumn_credit As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents GridColumn_callback_ph As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents GridColumn_confirmation_status As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents GridColumn_date_confirmed As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents GridColumn_date_updated As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents GridColumn_date_created As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents GridColumn_created_by As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents RepositoryItemLookUpEdit1 As DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit
        Friend WithEvents RepositoryItemButtonEdit1 As DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit
        Friend WithEvents RepositoryItemSpinEdit1 As DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit
        Friend WithEvents GridColumn_address As DevExpress.XtraGrid.Columns.GridColumn

    End Class
End Namespace