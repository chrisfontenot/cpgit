#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}
#End Region

Option Compare Binary
Option Explicit On
Option Strict On

Namespace Workshop.Client

    Friend Class MainForm

        Friend ap As ArgParser = Nothing
        Public Sub New(ByVal ap As ArgParser)
            MyBase.New()
            Me.InitializeComponent()
            Me.ap = ap

            AddHandler Me.FormClosing, AddressOf MainForm_FormClosing
            AddHandler Me.Load, AddressOf MainForm_Load
            AddHandler NewWorkshopSelectionControl1.Cancelled, AddressOf NewWorkshopSelectionControl1_Cancelled
            AddHandler NewWorkshopSelectionControl1.Completed, AddressOf NewWorkshopSelectionControl1_Completed
            AddHandler ClientSelectionControl1.Cancelled, AddressOf ClientSelectionControl1_Cancelled
            AddHandler ClientSelectionControl1.Completed, AddressOf ClientSelectionControl1_Completed
        End Sub

        Private Sub MainForm_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs)
            If ClientSelectionControl1.Visible Then
                If Not ClientSelectionControl1.Save() Then
                    e.Cancel = True
                End If
            End If
        End Sub

        Private Sub MainForm_Load(ByVal sender As Object, ByVal e As System.EventArgs)
            With ClientSelectionControl1
                .Visible = False
            End With

            With NewWorkshopSelectionControl1
                .Visible = True
                .Process()
            End With
        End Sub

        Private Sub NewWorkshopSelectionControl1_Cancelled(ByVal sender As Object, ByVal e As System.EventArgs)
            Me.Close()
        End Sub

        Private Sub NewWorkshopSelectionControl1_Completed(ByVal sender As Object, ByVal e As System.EventArgs)
            With NewWorkshopSelectionControl1
                .Visible = False
            End With

            With ClientSelectionControl1
                .Visible = True
                .Process()
            End With
        End Sub

        Private Sub ClientSelectionControl1_Cancelled(ByVal sender As Object, ByVal e As System.EventArgs)
            With ClientSelectionControl1
                .Visible = False
            End With

            GlobalValues.workshop = -1
            With NewWorkshopSelectionControl1
                .Visible = True
                .Process()
            End With
        End Sub

        Private Sub ClientSelectionControl1_Completed(ByVal sender As Object, ByVal e As System.EventArgs)

            With ClientSelectionControl1
                If .Save() Then
                    .Visible = False

                    GlobalValues.workshop = -1
                    With NewWorkshopSelectionControl1
                        .Visible = True
                        .Process()
                    End With
                End If
            End With
        End Sub
    End Class
End Namespace