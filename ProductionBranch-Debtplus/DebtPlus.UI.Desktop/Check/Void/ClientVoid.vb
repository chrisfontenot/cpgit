#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}
#End Region

Option Compare Binary
Option Explicit On
Option Strict On

Imports System.ComponentModel
Imports DebtPlus.UI.Common
Imports DebtPlus.Interfaces
Imports System.Windows.Forms

Namespace Check.Void
    Friend Class ClientVoid
        Inherits VoidTemplate

#Region " Windows Form Designer generated code "

        Public Sub New()
            MyBase.New()
            InitializeComponent()
        End Sub

        'UserControl overrides dispose to clean up the component list.
        Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
            If disposing Then
                If components IsNot Nothing Then
                    components.Dispose()
                End If
            End If
            MyBase.Dispose(disposing)
        End Sub

        'Required by the Windows Form Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Windows Form Designer
        'It can be modified using the Windows Form Designer.  
        'Do not modify it using the code editor.
        Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents lbl_client As DevExpress.XtraEditors.LabelControl
        Friend WithEvents LabelControl4 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents LabelControl3 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents lbl_client_name As DevExpress.XtraEditors.LabelControl
        Friend WithEvents lbl_client_addr As DevExpress.XtraEditors.LabelControl
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
            Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl()
            Me.lbl_client_name = New DevExpress.XtraEditors.LabelControl()
            Me.lbl_client = New DevExpress.XtraEditors.LabelControl()
            Me.LabelControl4 = New DevExpress.XtraEditors.LabelControl()
            Me.LabelControl3 = New DevExpress.XtraEditors.LabelControl()
            Me.lbl_client_addr = New DevExpress.XtraEditors.LabelControl()
            CType(Me.Combo_Reason.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.SuspendLayout()
            '
            'Label_Amount
            '
            Me.Label_Amount.Appearance.Options.UseTextOptions = True
            Me.Label_Amount.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            '
            'Combo_Reason
            '
            '
            'LabelControl1
            '
            Me.LabelControl1.Location = New System.Drawing.Point(0, 80)
            Me.LabelControl1.Name = "LabelControl1"
            Me.LabelControl1.Size = New System.Drawing.Size(31, 13)
            Me.LabelControl1.TabIndex = 0
            Me.LabelControl1.Text = "Name:"
            Me.LabelControl1.UseMnemonic = False
            '
            'lbl_client_name
            '
            Me.lbl_client_name.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                                               Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.lbl_client_name.Appearance.Options.UseTextOptions = True
            Me.lbl_client_name.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
            Me.lbl_client_name.Appearance.TextOptions.Trimming = DevExpress.Utils.Trimming.Character
            Me.lbl_client_name.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Top
            Me.lbl_client_name.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap
            Me.lbl_client_name.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
            Me.lbl_client_name.Location = New System.Drawing.Point(88, 80)
            Me.lbl_client_name.Name = "lbl_client_name"
            Me.lbl_client_name.Size = New System.Drawing.Size(232, 13)
            Me.lbl_client_name.TabIndex = 1
            Me.lbl_client_name.UseMnemonic = False
            '
            'lbl_client
            '
            Me.lbl_client.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                                          Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.lbl_client.Appearance.Options.UseTextOptions = True
            Me.lbl_client.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
            Me.lbl_client.Appearance.TextOptions.Trimming = DevExpress.Utils.Trimming.Character
            Me.lbl_client.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Top
            Me.lbl_client.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap
            Me.lbl_client.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
            Me.lbl_client.Location = New System.Drawing.Point(88, 56)
            Me.lbl_client.Name = "lbl_client"
            Me.lbl_client.Size = New System.Drawing.Size(232, 13)
            Me.lbl_client.TabIndex = 3
            Me.lbl_client.UseMnemonic = False
            '
            'LabelControl4
            '
            Me.LabelControl4.Location = New System.Drawing.Point(0, 56)
            Me.LabelControl4.Name = "LabelControl4"
            Me.LabelControl4.Size = New System.Drawing.Size(31, 13)
            Me.LabelControl4.TabIndex = 2
            Me.LabelControl4.Text = "Client:"
            Me.LabelControl4.UseMnemonic = False
            '
            'LabelControl3
            '
            Me.LabelControl3.Location = New System.Drawing.Point(0, 104)
            Me.LabelControl3.Name = "LabelControl3"
            Me.LabelControl3.Size = New System.Drawing.Size(27, 13)
            Me.LabelControl3.TabIndex = 4
            Me.LabelControl3.Text = "Addr:"
            Me.LabelControl3.UseMnemonic = False
            '
            'lbl_client_addr
            '
            Me.lbl_client_addr.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                                               Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.lbl_client_addr.Appearance.Options.UseTextOptions = True
            Me.lbl_client_addr.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
            Me.lbl_client_addr.Appearance.TextOptions.Trimming = DevExpress.Utils.Trimming.Character
            Me.lbl_client_addr.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Top
            Me.lbl_client_addr.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap
            Me.lbl_client_addr.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
            Me.lbl_client_addr.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder
            Me.lbl_client_addr.CausesValidation = False
            Me.lbl_client_addr.Location = New System.Drawing.Point(88, 104)
            Me.lbl_client_addr.Name = "lbl_client_addr"
            Me.lbl_client_addr.Size = New System.Drawing.Size(232, 101)
            Me.lbl_client_addr.TabIndex = 20
            Me.lbl_client_addr.ToolTip = "Creditor's payment address."
            Me.lbl_client_addr.UseMnemonic = False
            '
            'ClientVoid
            '
            Me.Controls.Add(Me.lbl_client_addr)
            Me.Controls.Add(Me.LabelControl3)
            Me.Controls.Add(Me.lbl_client)
            Me.Controls.Add(Me.LabelControl4)
            Me.Controls.Add(Me.lbl_client_name)
            Me.Controls.Add(Me.LabelControl1)
            Me.Name = "ClientVoid"
            Me.Controls.SetChildIndex(Me.Combo_Reason, 0)
            Me.Controls.SetChildIndex(Me.Label_Date, 0)
            Me.Controls.SetChildIndex(Me.Label_Amount, 0)
            Me.Controls.SetChildIndex(Me.LabelControl1, 0)
            Me.Controls.SetChildIndex(Me.lbl_client_name, 0)
            Me.Controls.SetChildIndex(Me.LabelControl4, 0)
            Me.Controls.SetChildIndex(Me.lbl_client, 0)
            Me.Controls.SetChildIndex(Me.LabelControl3, 0)
            Me.Controls.SetChildIndex(Me.lbl_client_addr, 0)
            CType(Me.Combo_Reason.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            Me.ResumeLayout(False)
            Me.PerformLayout()

        End Sub

#End Region

        Public Overrides Sub ProcessVoid()

            ' Do the standard void processing
            MyBase.ProcessVoid()

            ' Clear my display areas as well
            lbl_client.Text = String.Empty
            lbl_client_name.Text = String.Empty
            lbl_client_addr.Text = String.Empty
        End Sub

        <Description("Is the transaction a valid type for this processing function?"), Browsable(False)> _
        Protected Friend Overrides Function ValidTranType(ByVal tran_type As String) As Boolean
            tran_type = tran_type.ToUpper()
            Return (tran_type = "CR" OrElse tran_type = "AR")
        End Function

        Private _client As System.Int32 = Int32.MinValue
        <Description("Client ID associated with the check"), Browsable(False)> _
        Protected Friend Property client() As Int32
            Get
                Return _client
            End Get
            Set(ByVal value As Int32)
                _client = value
                lbl_client.Text = String.Format("{0:0000000}", value)
            End Set
        End Property

        Protected Overrides Sub OnFirstLoad()
            Dim myForm As Form = Me.FindForm
            MyBase.OnFirstLoad()

            Dim tooltipcontroller As DevExpress.Utils.ToolTipController = Nothing

            If TypeOf myForm Is IForm Then
                With CType(myForm, IForm)
                    tooltipcontroller = .GetToolTipController
                End With
            End If

            ' Set the controllers
            For Each ctl As Control In Controls
                If TypeOf ctl Is DevExpress.XtraEditors.BaseControl Then
                    CType(ctl, DevExpress.XtraEditors.BaseControl).ToolTipController = tooltipcontroller
                End If
            Next

            ' Clear my display areas as well
            lbl_client.Text = String.Empty
            lbl_client_name.Text = String.Empty
            lbl_client_addr.Text = String.Empty
        End Sub

        Public Overrides Function Type() As String
            Return "client"
        End Function

        <Description("Read the client information"), Browsable(False)> _
        Public Overrides Function ValidCheck(ByVal row As DataRow) As Boolean
            Dim answer As Boolean = False
            Dim activeStatus As String = String.Empty

            ' Do standard processing. It turns false so we don't care at this point.
            MyBase.ValidCheck(row)

            ' Retrieve the information from the trust register
            client = Convert.ToInt32(row("client"))

            ' Clear the name field
            lbl_client_name.Text = String.Empty
            lbl_client_addr.Text = String.Empty
            lbl_client.Text = String.Empty

            If client < 0 Then
                Return False
            End If

            Using bc As New DebtPlus.LINQ.BusinessContext()
                Using cm As New CursorManager()
                    Dim r = bc.view_client_addresses.Where(Function(s) s.client = client).Select(Function(s) New With {.client = s.client, .name = s.name, .addr1 = s.addr1, .addr2 = s.addr2, .addr3 = s.addr3, .ActiveStatus = s.active_status}).FirstOrDefault()

                    ' The client must exist.
                    If r Is Nothing Then
                        DebtPlus.Data.Forms.MessageBox.Show(String.Format("The client {0} could not be found. A refund is not possible at this time.", String.Format("{0:0000000}", client)), "Invalid Client ID", MessageBoxButtons.OK, MessageBoxIcon.Error)
                        Return False
                    End If

                    ' Double check the active status for the client
                    If r.ActiveStatus <> "A" AndAlso r.ActiveStatus <> "AR" Then
                        If DebtPlus.Data.Forms.MessageBox.Show(String.Format("Client {0} is not active." + Environment.NewLine + Environment.NewLine + "Do you still wish to continue to refund {1:c2} to this client?", client, Amount), "Client Not Active", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) <> DialogResult.Yes Then
                            Return False
                        End If
                    End If

                    ' Format the address line block as needed.
                    Dim sb As New System.Text.StringBuilder()
                    For Each strng As String In New String() {r.addr1, r.addr2, r.addr3}
                        If Not String.IsNullOrWhiteSpace(strng) Then
                            sb.Append(Environment.NewLine)
                            sb.Append(strng)
                        End If
                    Next

                    If sb.Length > 0 Then
                        sb.Remove(0, 2)
                    End If

                    lbl_client_addr.Text = sb.ToString()
                    lbl_client_name.Text = r.name
                    lbl_client.Text = String.Format("{0:0000000}", r.client)
                End Using
            End Using

            ' If successful, then populate the list of reasons
            Dim colItems As New System.Collections.ArrayList()
            DebtPlus.LINQ.Cache.message.getList() _
                .FindAll(Function(s) s.item_type = "VR") _
                .ToList() _
                .ForEach(Sub(s) colItems.Add(New DebtPlus.Data.Controls.ComboboxItem(s.description, s.item_value, s.ActiveFlag)))

            MyBase.Combo_Reason.Properties.Items.Clear()
            MyBase.Combo_Reason.Properties.Sorted = True
            MyBase.Combo_Reason.Properties.Items.AddRange(colItems.ToArray())

            Return True
        End Function
    End Class
End Namespace
