#Region "Copyright 2000-2012 DebtPlus, L.L.C."

'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}

#End Region

Option Compare Binary
Option Explicit On
Option Strict On

Imports System.ComponentModel
Imports DebtPlus.Data.Controls
Imports DebtPlus.UI.Common
Imports System.Windows.Forms

Namespace Check.Void
    Friend Class CreditorVoid
        Inherits VoidTemplate

#Region " Windows Form Designer generated code "

        Public Sub New()
            MyBase.New()
            InitializeComponent()
            GridColumn_client.DisplayFormat.Format = New DebtPlus.Utils.Format.Client.CustomFormatter
        End Sub

        'UserControl overrides dispose to clean up the component list.
        Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
            If disposing Then
                If components IsNot Nothing Then
                    components.Dispose()
                End If
            End If
            MyBase.Dispose(disposing)
        End Sub

        'Required by the Windows Form Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Windows Form Designer
        'It can be modified using the Windows Form Designer.  
        'Do not modify it using the code editor.
        Friend WithEvents LabelControl4 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents lbl_creditor_name As DevExpress.XtraEditors.LabelControl
        Friend WithEvents GridControl1 As DevExpress.XtraGrid.GridControl
        Friend WithEvents GridView1 As DevExpress.XtraGrid.Views.Grid.GridView
        Friend WithEvents GridColumn_client As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents GridColumn_ClientName As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents GridColumn_Gross As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents GridColumn_Deducted As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents GridColumn_Billed As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents GridColumn_Net As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents GridColumn_Active_Status As DevExpress.XtraGrid.Columns.GridColumn

        <System.Diagnostics.DebuggerStepThrough()>
        Private Sub InitializeComponent()
            Dim StyleFormatCondition1 As DevExpress.XtraGrid.StyleFormatCondition = New DevExpress.XtraGrid.StyleFormatCondition
            Me.GridColumn_Active_Status = New DevExpress.XtraGrid.Columns.GridColumn
            Me.GridColumn_Active_Status.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True
            Me.lbl_creditor_name = New DevExpress.XtraEditors.LabelControl
            Me.LabelControl4 = New DevExpress.XtraEditors.LabelControl
            Me.GridControl1 = New DevExpress.XtraGrid.GridControl
            Me.GridView1 = New DevExpress.XtraGrid.Views.Grid.GridView
            Me.GridColumn_client = New DevExpress.XtraGrid.Columns.GridColumn
            Me.GridColumn_client.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True
            Me.GridColumn_ClientName = New DevExpress.XtraGrid.Columns.GridColumn
            Me.GridColumn_ClientName.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True
            Me.GridColumn_Gross = New DevExpress.XtraGrid.Columns.GridColumn
            Me.GridColumn_Gross.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True
            Me.GridColumn_Deducted = New DevExpress.XtraGrid.Columns.GridColumn
            Me.GridColumn_Deducted.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True
            Me.GridColumn_Billed = New DevExpress.XtraGrid.Columns.GridColumn
            Me.GridColumn_Billed.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True
            Me.GridColumn_Net = New DevExpress.XtraGrid.Columns.GridColumn
            Me.GridColumn_Net.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True
            CType(Me.Combo_Reason.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.GridView1, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.SuspendLayout()
            '
            'Label_Amount
            '
            Me.Label_Amount.Appearance.Options.UseTextOptions = True
            Me.Label_Amount.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            '
            'Combo_Reason
            '
            Me.Combo_Reason.Properties.Mask.AutoComplete = DevExpress.XtraEditors.Mask.AutoCompleteType.Optimistic
            Me.Combo_Reason.Properties.Mask.BeepOnError = True
            Me.Combo_Reason.Properties.Mask.EditMask = "[^ ]+.*"
            Me.Combo_Reason.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx
            '
            'GridColumn_Active_Status
            '
            Me.GridColumn_Active_Status.Caption = "Actvie Status"
            Me.GridColumn_Active_Status.FieldName = "active_status"
            Me.GridColumn_Active_Status.Name = "GridColumn_Active_Status"
            '
            'lbl_creditor_name
            '
            Me.lbl_creditor_name.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                                                 Or System.Windows.Forms.AnchorStyles.Right), 
                                                System.Windows.Forms.AnchorStyles)
            Me.lbl_creditor_name.Appearance.Options.UseTextOptions = True
            Me.lbl_creditor_name.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
            Me.lbl_creditor_name.Appearance.TextOptions.Trimming = DevExpress.Utils.Trimming.Character
            Me.lbl_creditor_name.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Top
            Me.lbl_creditor_name.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap
            Me.lbl_creditor_name.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
            Me.lbl_creditor_name.Location = New System.Drawing.Point(88, 61)
            Me.lbl_creditor_name.Name = "lbl_creditor_name"
            Me.lbl_creditor_name.Size = New System.Drawing.Size(232, 13)
            Me.lbl_creditor_name.TabIndex = 1
            Me.lbl_creditor_name.UseMnemonic = False
            '
            'LabelControl4
            '
            Me.LabelControl4.Location = New System.Drawing.Point(0, 61)
            Me.LabelControl4.Name = "LabelControl4"
            Me.LabelControl4.Size = New System.Drawing.Size(43, 13)
            Me.LabelControl4.TabIndex = 2
            Me.LabelControl4.Text = "Creditor:"
            Me.LabelControl4.UseMnemonic = False
            '
            'GridControl1
            '
            Me.GridControl1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                                             Or System.Windows.Forms.AnchorStyles.Left) _
                                            Or System.Windows.Forms.AnchorStyles.Right), 
                                           System.Windows.Forms.AnchorStyles)
            Me.GridControl1.EmbeddedNavigator.Name = ""
            Me.GridControl1.Location = New System.Drawing.Point(0, 80)
            Me.GridControl1.MainView = Me.GridView1
            Me.GridControl1.Name = "GridControl1"
            Me.GridControl1.Size = New System.Drawing.Size(336, 128)
            Me.GridControl1.TabIndex = 6
            Me.GridControl1.TabStop = False
            Me.GridControl1.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridView1})
            '
            'GridView1
            '
            Me.GridView1.Appearance.ColumnFilterButton.BackColor = System.Drawing.Color.FromArgb(CType(CType(104, Byte), Int32), CType(CType(184, Byte), Int32), CType(CType(251, Byte), Int32))
            Me.GridView1.Appearance.ColumnFilterButton.BorderColor = System.Drawing.Color.FromArgb(CType(CType(104, Byte), Int32), CType(CType(184, Byte), Int32), CType(CType(251, Byte), Int32))
            Me.GridView1.Appearance.ColumnFilterButton.ForeColor = System.Drawing.Color.White
            Me.GridView1.Appearance.ColumnFilterButton.Options.UseBackColor = True
            Me.GridView1.Appearance.ColumnFilterButton.Options.UseBorderColor = True
            Me.GridView1.Appearance.ColumnFilterButton.Options.UseForeColor = True
            Me.GridView1.Appearance.ColumnFilterButtonActive.BackColor = System.Drawing.Color.FromArgb(CType(CType(170, Byte), Int32), CType(CType(216, Byte), Int32), CType(CType(254, Byte), Int32))
            Me.GridView1.Appearance.ColumnFilterButtonActive.BorderColor = System.Drawing.Color.FromArgb(CType(CType(170, Byte), Int32), CType(CType(216, Byte), Int32), CType(CType(254, Byte), Int32))
            Me.GridView1.Appearance.ColumnFilterButtonActive.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.ColumnFilterButtonActive.Options.UseBackColor = True
            Me.GridView1.Appearance.ColumnFilterButtonActive.Options.UseBorderColor = True
            Me.GridView1.Appearance.ColumnFilterButtonActive.Options.UseForeColor = True
            Me.GridView1.Appearance.Empty.BackColor = System.Drawing.Color.FromArgb(CType(CType(236, Byte), Int32), CType(CType(246, Byte), Int32), CType(CType(255, Byte), Int32))
            Me.GridView1.Appearance.Empty.BackColor2 = System.Drawing.Color.White
            Me.GridView1.Appearance.Empty.Options.UseBackColor = True
            Me.GridView1.Appearance.EvenRow.BackColor = System.Drawing.Color.FromArgb(CType(CType(247, Byte), Int32), CType(CType(251, Byte), Int32), CType(CType(255, Byte), Int32))
            Me.GridView1.Appearance.EvenRow.BorderColor = System.Drawing.Color.FromArgb(CType(CType(247, Byte), Int32), CType(CType(251, Byte), Int32), CType(CType(255, Byte), Int32))
            Me.GridView1.Appearance.EvenRow.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.EvenRow.Options.UseBackColor = True
            Me.GridView1.Appearance.EvenRow.Options.UseBorderColor = True
            Me.GridView1.Appearance.EvenRow.Options.UseForeColor = True
            Me.GridView1.Appearance.FilterCloseButton.BackColor = System.Drawing.Color.FromArgb(CType(CType(104, Byte), Int32), CType(CType(184, Byte), Int32), CType(CType(251, Byte), Int32))
            Me.GridView1.Appearance.FilterCloseButton.BorderColor = System.Drawing.Color.FromArgb(CType(CType(104, Byte), Int32), CType(CType(184, Byte), Int32), CType(CType(251, Byte), Int32))
            Me.GridView1.Appearance.FilterCloseButton.ForeColor = System.Drawing.Color.White
            Me.GridView1.Appearance.FilterCloseButton.Options.UseBackColor = True
            Me.GridView1.Appearance.FilterCloseButton.Options.UseBorderColor = True
            Me.GridView1.Appearance.FilterCloseButton.Options.UseForeColor = True
            Me.GridView1.Appearance.FilterPanel.BackColor = System.Drawing.Color.FromArgb(CType(CType(236, Byte), Int32), CType(CType(246, Byte), Int32), CType(CType(255, Byte), Int32))
            Me.GridView1.Appearance.FilterPanel.BackColor2 = System.Drawing.Color.White
            Me.GridView1.Appearance.FilterPanel.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.FilterPanel.Options.UseBackColor = True
            Me.GridView1.Appearance.FilterPanel.Options.UseForeColor = True
            Me.GridView1.Appearance.FixedLine.BackColor = System.Drawing.Color.FromArgb(CType(CType(59, Byte), Int32), CType(CType(133, Byte), Int32), CType(CType(195, Byte), Int32))
            Me.GridView1.Appearance.FixedLine.Options.UseBackColor = True
            Me.GridView1.Appearance.FocusedCell.BackColor = System.Drawing.Color.White
            Me.GridView1.Appearance.FocusedCell.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.FocusedCell.Options.UseBackColor = True
            Me.GridView1.Appearance.FocusedCell.Options.UseForeColor = True
            Me.GridView1.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb(CType(CType(38, Byte), Int32), CType(CType(109, Byte), Int32), CType(CType(189, Byte), Int32))
            Me.GridView1.Appearance.FocusedRow.BorderColor = System.Drawing.Color.FromArgb(CType(CType(59, Byte), Int32), CType(CType(139, Byte), Int32), CType(CType(206, Byte), Int32))
            Me.GridView1.Appearance.FocusedRow.ForeColor = System.Drawing.Color.White
            Me.GridView1.Appearance.FocusedRow.Options.UseBackColor = True
            Me.GridView1.Appearance.FocusedRow.Options.UseBorderColor = True
            Me.GridView1.Appearance.FocusedRow.Options.UseForeColor = True
            Me.GridView1.Appearance.FooterPanel.BackColor = System.Drawing.Color.FromArgb(CType(CType(104, Byte), Int32), CType(CType(184, Byte), Int32), CType(CType(251, Byte), Int32))
            Me.GridView1.Appearance.FooterPanel.BorderColor = System.Drawing.Color.FromArgb(CType(CType(104, Byte), Int32), CType(CType(184, Byte), Int32), CType(CType(251, Byte), Int32))
            Me.GridView1.Appearance.FooterPanel.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.FooterPanel.Options.UseBackColor = True
            Me.GridView1.Appearance.FooterPanel.Options.UseBorderColor = True
            Me.GridView1.Appearance.FooterPanel.Options.UseForeColor = True
            Me.GridView1.Appearance.GroupButton.BackColor = System.Drawing.Color.FromArgb(CType(CType(104, Byte), Int32), CType(CType(184, Byte), Int32), CType(CType(251, Byte), Int32))
            Me.GridView1.Appearance.GroupButton.BorderColor = System.Drawing.Color.FromArgb(CType(CType(104, Byte), Int32), CType(CType(184, Byte), Int32), CType(CType(251, Byte), Int32))
            Me.GridView1.Appearance.GroupButton.Options.UseBackColor = True
            Me.GridView1.Appearance.GroupButton.Options.UseBorderColor = True
            Me.GridView1.Appearance.GroupFooter.BackColor = System.Drawing.Color.FromArgb(CType(CType(170, Byte), Int32), CType(CType(216, Byte), Int32), CType(CType(254, Byte), Int32))
            Me.GridView1.Appearance.GroupFooter.BorderColor = System.Drawing.Color.FromArgb(CType(CType(170, Byte), Int32), CType(CType(216, Byte), Int32), CType(CType(254, Byte), Int32))
            Me.GridView1.Appearance.GroupFooter.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.GroupFooter.Options.UseBackColor = True
            Me.GridView1.Appearance.GroupFooter.Options.UseBorderColor = True
            Me.GridView1.Appearance.GroupFooter.Options.UseForeColor = True
            Me.GridView1.Appearance.GroupPanel.BackColor = System.Drawing.Color.FromArgb(CType(CType(236, Byte), Int32), CType(CType(246, Byte), Int32), CType(CType(255, Byte), Int32))
            Me.GridView1.Appearance.GroupPanel.BackColor2 = System.Drawing.Color.White
            Me.GridView1.Appearance.GroupPanel.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.GroupPanel.Options.UseBackColor = True
            Me.GridView1.Appearance.GroupPanel.Options.UseForeColor = True
            Me.GridView1.Appearance.GroupRow.BackColor = System.Drawing.Color.FromArgb(CType(CType(170, Byte), Int32), CType(CType(216, Byte), Int32), CType(CType(254, Byte), Int32))
            Me.GridView1.Appearance.GroupRow.BorderColor = System.Drawing.Color.FromArgb(CType(CType(170, Byte), Int32), CType(CType(216, Byte), Int32), CType(CType(254, Byte), Int32))
            Me.GridView1.Appearance.GroupRow.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.GroupRow.Options.UseBackColor = True
            Me.GridView1.Appearance.GroupRow.Options.UseBorderColor = True
            Me.GridView1.Appearance.GroupRow.Options.UseForeColor = True
            Me.GridView1.Appearance.HeaderPanel.BackColor = System.Drawing.Color.FromArgb(CType(CType(139, Byte), Int32), CType(CType(201, Byte), Int32), CType(CType(254, Byte), Int32))
            Me.GridView1.Appearance.HeaderPanel.BorderColor = System.Drawing.Color.FromArgb(CType(CType(139, Byte), Int32), CType(CType(201, Byte), Int32), CType(CType(254, Byte), Int32))
            Me.GridView1.Appearance.HeaderPanel.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.HeaderPanel.Options.UseBackColor = True
            Me.GridView1.Appearance.HeaderPanel.Options.UseBorderColor = True
            Me.GridView1.Appearance.HeaderPanel.Options.UseForeColor = True
            Me.GridView1.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Int32), CType(CType(170, Byte), Int32), CType(CType(225, Byte), Int32))
            Me.GridView1.Appearance.HideSelectionRow.BorderColor = System.Drawing.Color.FromArgb(CType(CType(83, Byte), Int32), CType(CType(155, Byte), Int32), CType(CType(215, Byte), Int32))
            Me.GridView1.Appearance.HideSelectionRow.ForeColor = System.Drawing.Color.FromArgb(CType(CType(236, Byte), Int32), CType(CType(246, Byte), Int32), CType(CType(255, Byte), Int32))
            Me.GridView1.Appearance.HideSelectionRow.Options.UseBackColor = True
            Me.GridView1.Appearance.HideSelectionRow.Options.UseBorderColor = True
            Me.GridView1.Appearance.HideSelectionRow.Options.UseForeColor = True
            Me.GridView1.Appearance.HorzLine.BackColor = System.Drawing.Color.FromArgb(CType(CType(104, Byte), Int32), CType(CType(184, Byte), Int32), CType(CType(251, Byte), Int32))
            Me.GridView1.Appearance.HorzLine.Options.UseBackColor = True
            Me.GridView1.Appearance.OddRow.BackColor = System.Drawing.Color.FromArgb(CType(CType(236, Byte), Int32), CType(CType(246, Byte), Int32), CType(CType(255, Byte), Int32))
            Me.GridView1.Appearance.OddRow.BorderColor = System.Drawing.Color.FromArgb(CType(CType(236, Byte), Int32), CType(CType(246, Byte), Int32), CType(CType(255, Byte), Int32))
            Me.GridView1.Appearance.OddRow.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.OddRow.Options.UseBackColor = True
            Me.GridView1.Appearance.OddRow.Options.UseBorderColor = True
            Me.GridView1.Appearance.OddRow.Options.UseForeColor = True
            Me.GridView1.Appearance.Preview.Font = New System.Drawing.Font("Verdana", 7.5!)
            Me.GridView1.Appearance.Preview.ForeColor = System.Drawing.Color.FromArgb(CType(CType(83, Byte), Int32), CType(CType(155, Byte), Int32), CType(CType(215, Byte), Int32))
            Me.GridView1.Appearance.Preview.Options.UseFont = True
            Me.GridView1.Appearance.Preview.Options.UseForeColor = True
            Me.GridView1.Appearance.Row.BackColor = System.Drawing.Color.FromArgb(CType(CType(247, Byte), Int32), CType(CType(251, Byte), Int32), CType(CType(255, Byte), Int32))
            Me.GridView1.Appearance.Row.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.Row.Options.UseBackColor = True
            Me.GridView1.Appearance.Row.Options.UseForeColor = True
            Me.GridView1.Appearance.RowSeparator.BackColor = System.Drawing.Color.FromArgb(CType(CType(236, Byte), Int32), CType(CType(246, Byte), Int32), CType(CType(255, Byte), Int32))
            Me.GridView1.Appearance.RowSeparator.BackColor2 = System.Drawing.Color.White
            Me.GridView1.Appearance.RowSeparator.Options.UseBackColor = True
            Me.GridView1.Appearance.SelectedRow.BackColor = System.Drawing.Color.FromArgb(CType(CType(83, Byte), Int32), CType(CType(155, Byte), Int32), CType(CType(215, Byte), Int32))
            Me.GridView1.Appearance.SelectedRow.ForeColor = System.Drawing.Color.White
            Me.GridView1.Appearance.SelectedRow.Options.UseBackColor = True
            Me.GridView1.Appearance.SelectedRow.Options.UseForeColor = True
            Me.GridView1.Appearance.TopNewRow.BackColor = System.Drawing.Color.White
            Me.GridView1.Appearance.TopNewRow.Options.UseBackColor = True
            Me.GridView1.Appearance.VertLine.BackColor = System.Drawing.Color.FromArgb(CType(CType(104, Byte), Int32), CType(CType(184, Byte), Int32), CType(CType(251, Byte), Int32))
            Me.GridView1.Appearance.VertLine.Options.UseBackColor = True
            Me.GridView1.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.GridColumn_client, Me.GridColumn_ClientName, Me.GridColumn_Gross, Me.GridColumn_Deducted, Me.GridColumn_Billed, Me.GridColumn_Net, Me.GridColumn_Active_Status})
            Me.GridView1.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus
            StyleFormatCondition1.Appearance.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Int32), CType(CType(192, Byte), Int32), CType(CType(192, Byte), Int32))
            StyleFormatCondition1.Appearance.BackColor2 = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Int32), CType(CType(192, Byte), Int32), CType(CType(192, Byte), Int32))
            StyleFormatCondition1.Appearance.BorderColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Int32), CType(CType(192, Byte), Int32), CType(CType(192, Byte), Int32))
            StyleFormatCondition1.Appearance.Options.UseBackColor = True
            StyleFormatCondition1.Appearance.Options.UseBorderColor = True
            StyleFormatCondition1.ApplyToRow = True
            StyleFormatCondition1.Column = Me.GridColumn_Active_Status
            StyleFormatCondition1.Condition = DevExpress.XtraGrid.FormatConditionEnum.NotEqual
            StyleFormatCondition1.Value1 = "A"
            Me.GridView1.FormatConditions.AddRange(New DevExpress.XtraGrid.StyleFormatCondition() {StyleFormatCondition1})
            Me.GridView1.GridControl = Me.GridControl1
            Me.GridView1.Name = "GridView1"
            Me.GridView1.OptionsBehavior.Editable = False
            Me.GridView1.OptionsView.ShowFooter = True
            Me.GridView1.OptionsView.ShowGroupPanel = False
            Me.GridView1.OptionsView.ShowIndicator = False
            '
            'GridColumn_client
            '
            Me.GridColumn_client.AppearanceCell.Options.UseTextOptions = True
            Me.GridColumn_client.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.GridColumn_client.AppearanceHeader.Options.UseTextOptions = True
            Me.GridColumn_client.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.GridColumn_client.Caption = "Client"
            Me.GridColumn_client.DisplayFormat.FormatString = "0000000"
            Me.GridColumn_client.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom
            Me.GridColumn_client.FieldName = "client"
            Me.GridColumn_client.Name = "GridColumn_client"
            Me.GridColumn_client.Visible = True
            Me.GridColumn_client.VisibleIndex = 0
            '
            'GridColumn_ClientName
            '
            Me.GridColumn_ClientName.Caption = "Name"
            Me.GridColumn_ClientName.FieldName = "client_name"
            Me.GridColumn_ClientName.Name = "GridColumn_ClientName"
            Me.GridColumn_ClientName.Visible = True
            Me.GridColumn_ClientName.VisibleIndex = 1
            '
            'GridColumn_Gross
            '
            Me.GridColumn_Gross.AppearanceCell.Options.UseTextOptions = True
            Me.GridColumn_Gross.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.GridColumn_Gross.AppearanceHeader.Options.UseTextOptions = True
            Me.GridColumn_Gross.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.GridColumn_Gross.Caption = "Gross"
            Me.GridColumn_Gross.DisplayFormat.FormatString = "c2"
            Me.GridColumn_Gross.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.GridColumn_Gross.FieldName = "debit_amt"
            Me.GridColumn_Gross.GroupFormat.FormatString = "c2"
            Me.GridColumn_Gross.GroupFormat.FormatType = DevExpress.Utils.FormatType.DateTime
            Me.GridColumn_Gross.Name = "GridColumn_Gross"
            Me.GridColumn_Gross.SummaryItem.DisplayFormat = "{0:c}"
            Me.GridColumn_Gross.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum
            Me.GridColumn_Gross.Visible = True
            Me.GridColumn_Gross.VisibleIndex = 2
            '
            'GridColumn_Deducted
            '
            Me.GridColumn_Deducted.AppearanceCell.Options.UseTextOptions = True
            Me.GridColumn_Deducted.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.GridColumn_Deducted.AppearanceHeader.Options.UseTextOptions = True
            Me.GridColumn_Deducted.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.GridColumn_Deducted.Caption = "Deducted"
            Me.GridColumn_Deducted.DisplayFormat.FormatString = "c2"
            Me.GridColumn_Deducted.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.GridColumn_Deducted.FieldName = "deducted_amt"
            Me.GridColumn_Deducted.GroupFormat.FormatString = "c2"
            Me.GridColumn_Deducted.GroupFormat.FormatType = DevExpress.Utils.FormatType.DateTime
            Me.GridColumn_Deducted.Name = "GridColumn_Deducted"
            Me.GridColumn_Deducted.SummaryItem.DisplayFormat = "{0:c}"
            Me.GridColumn_Deducted.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum
            Me.GridColumn_Deducted.Visible = True
            Me.GridColumn_Deducted.VisibleIndex = 3
            '
            'GridColumn_Billed
            '
            Me.GridColumn_Billed.AppearanceCell.Options.UseTextOptions = True
            Me.GridColumn_Billed.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.GridColumn_Billed.AppearanceHeader.Options.UseTextOptions = True
            Me.GridColumn_Billed.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.GridColumn_Billed.Caption = "Billed"
            Me.GridColumn_Billed.DisplayFormat.FormatString = "c2"
            Me.GridColumn_Billed.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.GridColumn_Billed.FieldName = "billed_amt"
            Me.GridColumn_Billed.GroupFormat.FormatString = "c2"
            Me.GridColumn_Billed.GroupFormat.FormatType = DevExpress.Utils.FormatType.DateTime
            Me.GridColumn_Billed.Name = "GridColumn_Billed"
            Me.GridColumn_Billed.SummaryItem.DisplayFormat = "{0:c}"
            Me.GridColumn_Billed.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum
            Me.GridColumn_Billed.Visible = True
            Me.GridColumn_Billed.VisibleIndex = 4
            '
            'GridColumn_Net
            '
            Me.GridColumn_Net.AppearanceCell.Options.UseTextOptions = True
            Me.GridColumn_Net.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.GridColumn_Net.AppearanceHeader.Options.UseTextOptions = True
            Me.GridColumn_Net.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.GridColumn_Net.Caption = "Net"
            Me.GridColumn_Net.DisplayFormat.FormatString = "c2"
            Me.GridColumn_Net.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.GridColumn_Net.FieldName = "net_amt"
            Me.GridColumn_Net.GroupFormat.FormatString = "c2"
            Me.GridColumn_Net.GroupFormat.FormatType = DevExpress.Utils.FormatType.DateTime
            Me.GridColumn_Net.Name = "GridColumn_Net"
            Me.GridColumn_Net.SummaryItem.DisplayFormat = "{0:c}"
            Me.GridColumn_Net.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum
            Me.GridColumn_Net.Visible = True
            Me.GridColumn_Net.VisibleIndex = 5
            '
            'CreditorVoid
            '
            Me.Controls.Add(Me.GridControl1)
            Me.Controls.Add(Me.LabelControl4)
            Me.Controls.Add(Me.lbl_creditor_name)
            Me.Name = "CreditorVoid"
            Me.Controls.SetChildIndex(Me.Combo_Reason, 0)
            Me.Controls.SetChildIndex(Me.Label_Date, 0)
            Me.Controls.SetChildIndex(Me.Label_Amount, 0)
            Me.Controls.SetChildIndex(Me.lbl_creditor_name, 0)
            Me.Controls.SetChildIndex(Me.LabelControl4, 0)
            Me.Controls.SetChildIndex(Me.GridControl1, 0)
            CType(Me.Combo_Reason.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.GridView1, System.ComponentModel.ISupportInitialize).EndInit()
            Me.ResumeLayout(False)
            Me.PerformLayout()
        End Sub

#End Region

        Public Overrides Sub ProcessVoid()
            MyBase.ProcessVoid()

            ' Clear my display areas as well
            lbl_creditor_name.Text = String.Empty

            ' Empty the grid
            GridControl1.DataSource = Nothing
            GridControl1.RefreshDataSource()
        End Sub

        <Description("Is the transaction a valid type for this processing function?"), Browsable(False)>
        Protected Friend Overrides Function ValidTranType(ByVal tran_type As String) As Boolean

            ' We accept only creditor checks.
            Select Case tran_type.ToUpper()
                Case "AD", "MD", "CM"
                    Return True
                Case Else
                    Return False
            End Select
        End Function

        Private _creditor As String = String.Empty

        <Description("Creditor ID associated with the check"), Browsable(False)>
        Protected Friend Property creditor() As String
            Get
                Return _creditor
            End Get
            Set(ByVal value As String)
                _creditor = value.ToUpper()
            End Set
        End Property

        Protected Overrides Sub OnFirstLoad()
            MyBase.OnFirstLoad()
            lbl_creditor_name.Text = String.Empty
        End Sub

        Public Overrides Function Type() As String
            Return "creditor"
        End Function

        <Description("Read the creditor information"), Browsable(False)>
        Public Overrides Function ValidCheck(ByVal row As DataRow) As Boolean
            Dim answer As Boolean = False
            Dim creditorName As String = String.Empty

            ' Pass the request along to the parent
            MyBase.ValidCheck(row)

            ' Set the parameters for processing
            lbl_creditor_name.Text = String.Empty
            creditor = Convert.ToString(row("creditor"))

            ' Read the name of the creditor. We ignore errors as they fold down to unknown name.
            Dim gsr As Repository.GetScalarResult = Repository.Creditors.GetNameById(creditor)
            If gsr.Success AndAlso gsr.Result IsNot DBNull.Value Then
                creditorName = Convert.ToString(gsr.Result)
            End If

            ' Combine the creditor ID and the name for the display
            If creditor <> String.Empty AndAlso creditorName <> String.Empty Then
                lbl_creditor_name.Text = String.Format("[{0}] {1}", creditor, creditorName)
            Else
                lbl_creditor_name.Text = creditor + creditorName
            End If

            ' Read the check contents
            'Dim frm As New WaitDialogForm("Reading check contents")
            'frm.Show()
            'TODO: We may wish to add a second variant on CursorManager that accepts a message and displays a dialog with that message.

            Dim ds As New DataSet("ds")
            Const transactionsTableName = "transactions"

            With (New CursorManager())
                Dim gdr As Repository.GetDataResult = Repository.General.GetRegisterPeopleNameTransactions(ds, transactionsTableName, trust_register)
                If Not gdr.Success Then
                    DebtPlus.UI.Common.ErrorHandling.HandleErrors(gdr.ex, "Repository.General.GetRegisterPeopleNameTransactions failed")
                End If
            End With

            Dim tbl As DataTable = ds.Tables(transactionsTableName)
            With tbl
                .PrimaryKey = New DataColumn() {.Columns("client_creditor_register")}
                .Columns.Add("deducted_amt", GetType(Decimal), "iif([creditor_type]='D',fairshare_amt,0)").DefaultValue = 0D
                .Columns.Add("billed_amt", GetType(Decimal), "iif([creditor_type]<>'N' AND [creditor_type]<>'D',fairshare_amt,0)").DefaultValue = 0D
                .Columns.Add("net_amt", GetType(Decimal), "[debit_amt]-[deducted_amt]").DefaultValue = 0D
            End With

            ' Give the user an error if the net total does not match the check
            If tbl IsNot Nothing Then
                With GridControl1
                    .DataSource = tbl.DefaultView
                    .RefreshDataSource()
                End With

                Dim transactionTotal As Object = tbl.Compute("sum(net_amt)", String.Empty)
                If transactionTotal Is DBNull.Value Then transactionTotal = 0D
                answer = (Convert.ToDecimal(transactionTotal) = Amount)
                If Not answer Then
                    DebtPlus.Data.Forms.MessageBox.Show(String.Format("The NET dollar amount of the transactions ({0:c}) does not match the amount written for the check ({1:c}). The check may not be voided.", Convert.ToDecimal(transactionTotal), Amount), "Data Integretry check", MessageBoxButtons.OK, MessageBoxIcon.Error)
                Else

                    ' If successful, then populate the list of reasons
                    With Combo_Reason.Properties.Items
                        .Clear()
                        For Each item As DebtPlus.LINQ.message In DebtPlus.LINQ.Cache.message.getList().FindAll(Function(s) s.item_type = "VD").OrderBy(Function(s) s.description).ToList
                            .Add(New DebtPlus.Data.Controls.ComboboxItem(item.description, item.Id, item.ActiveFlag))
                        Next
                    End With
                End If
            End If

            Return answer
        End Function

    End Class

End Namespace
