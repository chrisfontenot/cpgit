#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}
#End Region

Option Compare Binary
Option Explicit On
Option Strict On

Imports System.ComponentModel
Imports DebtPlus.Utils
Imports System.Data.SqlClient
Imports DebtPlus.Interfaces
Imports System.Windows.Forms

Namespace Check.Void

    Friend Class VoidTemplate
        Inherits DevExpress.XtraEditors.XtraUserControl

        ' Event handler to indicate that the form was changed
        Public Event DataChanged As EventHandler
        Protected Overridable Sub OnDataChanged(ByVal e As System.EventArgs)
            RaiseEvent DataChanged(Me, e)
        End Sub

        Protected Sub RaiseDataChanged()
            OnDataChanged(EventArgs.Empty)
        End Sub

        ' Determine if an error is indicated in the form
        Public Overridable ReadOnly Property HasErrors() As Boolean
            Get
                Dim ErrorString As String = String.Empty
                If Combo_Reason.Text.Trim() = String.Empty Then
                    ErrorString = "Entry Required"
                End If

                ' Set the error text
                With CType(Me.FindForm, IForm)
                    .GetErrorProvider.SetError(Combo_Reason, ErrorString)
                End With

                Return ErrorString <> String.Empty
            End Get
        End Property

        Private _Amount As Decimal = 0D
        Protected Property Amount() As Decimal
            Get
                Return _Amount
            End Get
            Set(ByVal Value As Decimal)
                _Amount = Value
                Label_Amount.Text = String.Format("{0:c}", Value)
            End Set
        End Property

        Private _CheckDate As Date = Date.MinValue
        Protected Property CheckDate() As Date
            Get
                Return _CheckDate
            End Get
            Set(ByVal Value As Date)
                _CheckDate = Value
                If Value = Date.MinValue Then
                    Label_Date.Text = String.Empty
                Else
                    Label_Date.Text = Value.ToShortDateString
                End If
            End Set
        End Property

        Public Overridable Sub ProcessVoid()

            UnRegiserHandlers()
            Try
                Using cm As New DebtPlus.UI.Common.CursorManager()
                    Using cn As SqlClient.SqlConnection = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
                        cn.Open()
                        Using cmd As SqlClient.SqlCommand = New SqlCommand()
                            cmd.Connection = cn
                            cmd.CommandText = "xpr_void_check"
                            cmd.CommandType = System.Data.CommandType.StoredProcedure
                            cmd.Parameters.Add("RETURN", System.Data.SqlDbType.Int).Direction = ParameterDirection.ReturnValue
                            cmd.Parameters.Add("@trust_register", System.Data.SqlDbType.Int).Value = trust_register
                            cmd.Parameters.Add("@message", System.Data.SqlDbType.VarChar, 50).Value = Combo_Reason.Text.Trim()
                            cmd.ExecuteNonQuery()

                            Dim ChecksVoided As Int32 = Convert.ToInt32(cmd.Parameters(0).Value)
                            If ChecksVoided = 0 Then
                                DebtPlus.Data.Forms.MessageBox.Show("The check was not voided because it is no longer a valid check to void", "Sorry, but I could not void the check", MessageBoxButtons.OK, MessageBoxIcon.Hand)
                                Return
                            End If
                        End Using
                    End Using
                End Using

                ' Clear the current controls
                CheckDate = Date.MinValue
                Amount = 0D
                Label_Amount.Text = String.Empty
                Label_Date.Text = String.Empty
                Combo_Reason.SelectedIndex = -1

            Catch ex As SqlClient.SqlException
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error attempting to void the check")

            Finally
                RegiserHandlers()
            End Try
        End Sub

#Region " Windows Form Designer generated code "

        Public Sub New()
            MyBase.New()
            InitializeComponent()
        End Sub

        'UserControl overrides dispose to clean up the component list.
        Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
            If disposing Then
                If components IsNot Nothing Then
                    components.Dispose()
                End If
            End If
            MyBase.Dispose(disposing)
        End Sub

        'Required by the Windows Form Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Windows Form Designer
        'It can be modified using the Windows Form Designer.  
        'Do not modify it using the code editor.
        Protected Friend WithEvents Label_Amount As DevExpress.XtraEditors.LabelControl
        Friend WithEvents LabelControl8 As DevExpress.XtraEditors.LabelControl
        Protected Friend WithEvents Label_Date As DevExpress.XtraEditors.LabelControl
        Protected Friend WithEvents Combo_Reason As DevExpress.XtraEditors.ComboBoxEdit
        Friend WithEvents LabelControl5 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents LabelControl2 As DevExpress.XtraEditors.LabelControl
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
            Me.Label_Amount = New DevExpress.XtraEditors.LabelControl
            Me.LabelControl8 = New DevExpress.XtraEditors.LabelControl
            Me.Label_Date = New DevExpress.XtraEditors.LabelControl
            Me.Combo_Reason = New DevExpress.XtraEditors.ComboBoxEdit
            Me.LabelControl5 = New DevExpress.XtraEditors.LabelControl
            Me.LabelControl2 = New DevExpress.XtraEditors.LabelControl
            CType(Me.Combo_Reason.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.SuspendLayout()
            '
            'Label_Amount
            '
            Me.Label_Amount.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.Label_Amount.Appearance.Options.UseTextOptions = True
            Me.Label_Amount.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.Label_Amount.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
            Me.Label_Amount.Location = New System.Drawing.Point(215, 35)
            Me.Label_Amount.Name = "Label_Amount"
            Me.Label_Amount.Size = New System.Drawing.Size(105, 13)
            Me.Label_Amount.TabIndex = 5
            Me.Label_Amount.UseMnemonic = False
            '
            'LabelControl8
            '
            Me.LabelControl8.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.LabelControl8.Location = New System.Drawing.Point(168, 35)
            Me.LabelControl8.Name = "LabelControl8"
            Me.LabelControl8.Size = New System.Drawing.Size(41, 13)
            Me.LabelControl8.TabIndex = 4
            Me.LabelControl8.Text = "Amount:"
            Me.LabelControl8.UseMnemonic = False
            '
            'Label_Date
            '
            Me.Label_Date.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
            Me.Label_Date.Location = New System.Drawing.Point(88, 32)
            Me.Label_Date.Name = "Label_Date"
            Me.Label_Date.Size = New System.Drawing.Size(63, 19)
            Me.Label_Date.TabIndex = 3
            Me.Label_Date.UseMnemonic = False
            '
            'Combo_Reason
            '
            Me.Combo_Reason.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                                            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.Combo_Reason.Location = New System.Drawing.Point(88, 4)
            Me.Combo_Reason.Name = "Combo_Reason"
            Me.Combo_Reason.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.Combo_Reason.Properties.Mask.AutoComplete = DevExpress.XtraEditors.Mask.AutoCompleteType.Optimistic
            Me.Combo_Reason.Properties.Mask.BeepOnError = True
            Me.Combo_Reason.Properties.Mask.EditMask = "[^ ]+.*"
            Me.Combo_Reason.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx
            Me.Combo_Reason.Properties.MaxLength = 50
            Me.Combo_Reason.Size = New System.Drawing.Size(232, 20)
            Me.Combo_Reason.TabIndex = 1
            Me.Combo_Reason.ToolTip = "Enter or choose a reason why you wish to void the check."
            '
            'LabelControl5
            '
            Me.LabelControl5.Location = New System.Drawing.Point(0, 7)
            Me.LabelControl5.Name = "LabelControl5"
            Me.LabelControl5.Size = New System.Drawing.Size(40, 13)
            Me.LabelControl5.TabIndex = 0
            Me.LabelControl5.Text = "&Reason:"
            '
            'LabelControl2
            '
            Me.LabelControl2.Location = New System.Drawing.Point(0, 35)
            Me.LabelControl2.Name = "LabelControl2"
            Me.LabelControl2.Size = New System.Drawing.Size(59, 13)
            Me.LabelControl2.TabIndex = 2
            Me.LabelControl2.Text = "Check Date:"
            Me.LabelControl2.UseMnemonic = False
            '
            'VoidTemplate
            '
            Me.Controls.Add(Me.Label_Amount)
            Me.Controls.Add(Me.LabelControl8)
            Me.Controls.Add(Me.Label_Date)
            Me.Controls.Add(Me.Combo_Reason)
            Me.Controls.Add(Me.LabelControl5)
            Me.Controls.Add(Me.LabelControl2)
            Me.Name = "VoidTemplate"
            Me.Size = New System.Drawing.Size(336, 208)
            CType(Me.Combo_Reason.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            Me.ResumeLayout(False)
            Me.PerformLayout()

        End Sub

#End Region

        <Description("Is the transaction a valid type for this processing function?"), Browsable(False)> _
        Protected Friend Overridable Function ValidTranType(ByVal tran_type As String) As Boolean
            Return False
        End Function

        Private _trust_register As System.Int32 = Int32.MinValue
        <Description("Trust Register associated with the check"), Browsable(False)> _
        Protected Friend Overridable Property trust_register() As System.Int32
            Get
                Return _trust_register
            End Get
            Set(ByVal Value As System.Int32)
                _trust_register = Value
            End Set
        End Property

        Protected Overrides Sub OnFirstLoad()
            MyBase.OnFirstLoad()

            UnRegiserHandlers()
            Try
                Dim MyForm As System.Windows.Forms.Form = Me.FindForm
                Dim tooltipcontroller As DevExpress.Utils.ToolTipController = Nothing

                If TypeOf MyForm Is IForm Then
                    With CType(MyForm, IForm)
                        tooltipcontroller = .GetToolTipController
                    End With
                End If

                ' Set the controllers
                For Each ctl As Control In Controls
                    If TypeOf ctl Is DevExpress.XtraEditors.BaseControl Then
                        CType(ctl, DevExpress.XtraEditors.BaseControl).ToolTipController = tooltipcontroller
                    End If
                Next
            Finally
                RegiserHandlers()
            End Try
        End Sub

        Private Sub RegiserHandlers()
            AddHandler Combo_Reason.SelectedIndexChanged, AddressOf Combo_Reason_Changed
            AddHandler Combo_Reason.TextChanged, AddressOf Combo_Reason_Changed
        End Sub

        Private Sub UnRegiserHandlers()
            RemoveHandler Combo_Reason.SelectedIndexChanged, AddressOf Combo_Reason_Changed
            RemoveHandler Combo_Reason.TextChanged, AddressOf Combo_Reason_Changed
        End Sub

        <Description("Read the client information"), Browsable(False)> _
        Public Overridable Function ValidCheck(ByVal row As System.Data.DataRow) As Boolean

            ' Save the information that is common for both forms
            Amount = 0D
            CheckDate = Date.MinValue
            trust_register = 0

            If row IsNot Nothing Then
                If Not row.IsNull("trust_register") Then trust_register = Convert.ToInt32(row("trust_register"))
                If Not row.IsNull("amount") Then Amount = Convert.ToDecimal(row("amount"))
                If Not row.IsNull("date_created") Then CheckDate = Convert.ToDateTime(row("date_created"))
            End If

            Return False
        End Function

        Public Overridable Function Type() As String
            Return "unknown"
        End Function

        Private Sub Combo_Reason_Changed(ByVal sender As Object, ByVal e As System.EventArgs)
            RaiseDataChanged()
        End Sub
    End Class
End Namespace
