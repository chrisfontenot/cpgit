#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}
#End Region

Option Compare Binary
Option Explicit On
Option Strict On

Imports DebtPlus.Utils
Imports System.Data.SqlClient
Imports System.Windows.Forms

Namespace Check.Void

    Friend Class VoidForm
        Inherits DebtPlus.Data.Forms.DebtPlusForm

        Friend ap As ArgParser

        Public Sub New()
            MyBase.New()
            InitializeComponent()
        End Sub

        Public Sub New(ByVal ap As ArgParser)
            MyClass.New()
            Me.ap = ap
            RegisterHandlers()
        End Sub

        Private Sub RegisterHandlers()
            AddHandler Load, AddressOf VoidForm_Load
            AddHandler Button_Cancel.Click, AddressOf Button_Cancel_Click
            AddHandler Button_OK.Click, AddressOf Button_OK_Click
            AddHandler TextCheckNumber.Validating, AddressOf TextCheckNumber_Validating
            AddHandler TextCheckNumber.Validated, AddressOf TextCheckNumber_Validated
            AddHandler LookupEditBank.EditValueChanged, AddressOf LookupEditBank_EditValueChanged

            If CurrentMode IsNot Nothing Then
                AddHandler CurrentMode.DataChanged, AddressOf CurrentMode_DataChanged
            End If
        End Sub

        Private Sub UnRegisterHandlers()
            RemoveHandler Load, AddressOf VoidForm_Load
            RemoveHandler Button_Cancel.Click, AddressOf Button_Cancel_Click
            RemoveHandler Button_OK.Click, AddressOf Button_OK_Click
            RemoveHandler TextCheckNumber.Validating, AddressOf TextCheckNumber_Validating
            RemoveHandler TextCheckNumber.Validated, AddressOf TextCheckNumber_Validated
            RemoveHandler LookupEditBank.EditValueChanged, AddressOf LookupEditBank_EditValueChanged

            If CurrentMode IsNot Nothing Then
                RemoveHandler CurrentMode.DataChanged, AddressOf CurrentMode_DataChanged
            End If
        End Sub

        Private WithEvents CurrentMode As VoidTemplate = Nothing

        ''' <summary>
        ''' Find the bank number for future access
        ''' </summary>
        Private ReadOnly Property bank() As Int32
            Get
                If LookupEditBank.EditValue Is Nothing OrElse LookupEditBank.EditValue Is DBNull.Value Then Return 0
                Return Convert.ToInt32(LookupEditBank.EditValue)
            End Get
        End Property

#Region " Windows Form Designer generated code "

        'Form overrides dispose to clean up the component list.
        Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
            If disposing Then
                If components IsNot Nothing Then
                    components.Dispose()
                End If
            End If
            MyBase.Dispose(disposing)
        End Sub

        'Required by the Windows Form Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Windows Form Designer
        'It can be modified using the Windows Form Designer.  
        'Do not modify it using the code editor.
        Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents TextCheckNumber As DevExpress.XtraEditors.ButtonEdit
        Friend WithEvents LabelControl3 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents LookupEditBank As DevExpress.XtraEditors.LookUpEdit
        Friend WithEvents Button_OK As DevExpress.XtraEditors.SimpleButton
        Friend WithEvents Button_Cancel As DevExpress.XtraEditors.SimpleButton
        Friend WithEvents ClientVoid1 As DebtPlus.UI.Desktop.Check.Void.ClientVoid
        Friend WithEvents CreditorVoid1 As DebtPlus.UI.Desktop.Check.Void.CreditorVoid
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
            Dim SerializableAppearanceObject1 As DevExpress.Utils.SerializableAppearanceObject = New DevExpress.Utils.SerializableAppearanceObject
            Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl
            Me.TextCheckNumber = New DevExpress.XtraEditors.ButtonEdit
            Me.LabelControl3 = New DevExpress.XtraEditors.LabelControl
            Me.LookupEditBank = New DevExpress.XtraEditors.LookUpEdit
            Me.Button_OK = New DevExpress.XtraEditors.SimpleButton
            Me.Button_Cancel = New DevExpress.XtraEditors.SimpleButton
            Me.CreditorVoid1 = New DebtPlus.UI.Desktop.Check.Void.CreditorVoid
            Me.ClientVoid1 = New DebtPlus.UI.Desktop.Check.Void.ClientVoid
            CType(Me.DxErrorProvider1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.TextCheckNumber.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LookupEditBank.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.SuspendLayout()
            '
            'LabelControl1
            '
            Me.LabelControl1.Location = New System.Drawing.Point(8, 48)
            Me.LabelControl1.Name = "LabelControl1"
            Me.LabelControl1.Size = New System.Drawing.Size(69, 13)
            Me.LabelControl1.TabIndex = 2
            Me.LabelControl1.Text = "&Check Number"
            Me.LabelControl1.ToolTipController = Me.ToolTipController1
            '
            'TextCheckNumber
            '
            Me.TextCheckNumber.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                                               Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.TextCheckNumber.Location = New System.Drawing.Point(96, 48)
            Me.TextCheckNumber.Name = "TextCheckNumber"
            Me.TextCheckNumber.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[False]
            Me.TextCheckNumber.Properties.Appearance.Options.UseTextOptions = True
            Me.TextCheckNumber.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.TextCheckNumber.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Ellipsis, "", -1, True, False, False, DevExpress.XtraEditors.ImageLocation.MiddleCenter, Nothing, New DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), SerializableAppearanceObject1, "", Nothing, Nothing, False)})
            Me.TextCheckNumber.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
            Me.TextCheckNumber.Properties.Mask.BeepOnError = True
            Me.TextCheckNumber.Properties.Mask.EditMask = "[A-Z]?\d+"
            Me.TextCheckNumber.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx
            Me.TextCheckNumber.Properties.Mask.SaveLiteral = False
            Me.TextCheckNumber.Properties.Mask.ShowPlaceHolders = False
            Me.TextCheckNumber.Properties.MaxLength = 50
            Me.TextCheckNumber.Properties.ValidateOnEnterKey = True
            Me.TextCheckNumber.Size = New System.Drawing.Size(104, 20)
            Me.TextCheckNumber.TabIndex = 3
            Me.TextCheckNumber.ToolTip = "Enter the check number that you wish to void here."
            Me.TextCheckNumber.ToolTipController = Me.ToolTipController1
            '
            'LabelControl3
            '
            Me.LabelControl3.Location = New System.Drawing.Point(8, 16)
            Me.LabelControl3.Name = "LabelControl3"
            Me.LabelControl3.Size = New System.Drawing.Size(23, 13)
            Me.LabelControl3.TabIndex = 0
            Me.LabelControl3.Text = "&Bank"
            Me.LabelControl3.ToolTipController = Me.ToolTipController1
            '
            'LookupEditBank
            '
            Me.LookupEditBank.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                                              Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.LookupEditBank.Location = New System.Drawing.Point(96, 16)
            Me.LookupEditBank.Name = "LookupEditBank"
            Me.LookupEditBank.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.LookupEditBank.Properties.NullText = "...Please choose one..."
            Me.LookupEditBank.Size = New System.Drawing.Size(192, 20)
            Me.LookupEditBank.TabIndex = 1
            Me.LookupEditBank.ToolTip = "You must first select the bank account against which the check is written."
            Me.LookupEditBank.ToolTipController = Me.ToolTipController1
            '
            'Button_OK
            '
            Me.Button_OK.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.Button_OK.Enabled = False
            Me.Button_OK.Location = New System.Drawing.Point(328, 8)
            Me.Button_OK.Name = "Button_OK"
            Me.Button_OK.Size = New System.Drawing.Size(75, 23)
            Me.Button_OK.TabIndex = 4
            Me.Button_OK.Text = "Void"
            Me.Button_OK.ToolTipController = Me.ToolTipController1
            '
            'Button_Cancel
            '
            Me.Button_Cancel.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.Button_Cancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
            Me.Button_Cancel.Location = New System.Drawing.Point(328, 48)
            Me.Button_Cancel.Name = "Button_Cancel"
            Me.Button_Cancel.Size = New System.Drawing.Size(75, 23)
            Me.Button_Cancel.TabIndex = 7
            Me.Button_Cancel.Text = "&Quit"
            Me.Button_Cancel.ToolTipController = Me.ToolTipController1
            '
            'CreditorVoid1
            '
            Me.CreditorVoid1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                                              Or System.Windows.Forms.AnchorStyles.Left) _
                                             Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.CreditorVoid1.Location = New System.Drawing.Point(8, 80)
            Me.CreditorVoid1.Name = "CreditorVoid1"
            Me.CreditorVoid1.Size = New System.Drawing.Size(400, 224)
            Me.CreditorVoid1.TabIndex = 6
            '
            'ClientVoid1
            '
            Me.ClientVoid1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                                            Or System.Windows.Forms.AnchorStyles.Left) _
                                           Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.ClientVoid1.Location = New System.Drawing.Point(8, 80)
            Me.ClientVoid1.Name = "ClientVoid1"
            Me.ClientVoid1.Size = New System.Drawing.Size(400, 224)
            Me.ClientVoid1.TabIndex = 5
            '
            'VoidForm
            '
            Me.AcceptButton = Me.Button_OK
            Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
            Me.CancelButton = Me.Button_Cancel
            Me.ClientSize = New System.Drawing.Size(416, 310)
            Me.Controls.Add(Me.CreditorVoid1)
            Me.Controls.Add(Me.ClientVoid1)
            Me.Controls.Add(Me.Button_Cancel)
            Me.Controls.Add(Me.Button_OK)
            Me.Controls.Add(Me.LookupEditBank)
            Me.Controls.Add(Me.LabelControl3)
            Me.Controls.Add(Me.TextCheckNumber)
            Me.Controls.Add(Me.LabelControl1)
            Me.Name = "VoidForm"
            Me.Text = "Void Checks"
            CType(Me.DxErrorProvider1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.TextCheckNumber.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LookupEditBank.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            Me.ResumeLayout(False)
            Me.PerformLayout()

        End Sub

#End Region

        Private Sub VoidForm_Load(ByVal sender As Object, ByVal e As EventArgs)
            UnRegisterHandlers()
            Try
                ClientVoid1.Visible = False
                CreditorVoid1.Visible = False

                ' Load the list of bank accounts
                BankListLookupEdit_Load(LookupEditBank)

                ' Bind the controls
                Button_OK.Enabled = Not HasErrors()

            Finally
                RegisterHandlers()
            End Try
        End Sub

        ''' <summary>
        ''' Load the bank information
        ''' </summary>
        Private Sub BankListLookupEdit_Load(ByRef ctl As DevExpress.XtraEditors.LookUpEdit)

            ' Connection information to the datasource
            Dim cn As SqlClient.SqlConnection = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
            Dim ds As New DataSet
            ' Select the data from the database
            Dim current_cursor As Cursor = System.Windows.Forms.Cursor.Current
            Try
                System.Windows.Forms.Cursor.Current = Cursors.WaitCursor
                Using Command As SqlClient.SqlCommand = New SqlCommand
                    With Command
                        .Connection = cn
                        .CommandText = "SELECT [Default], ActiveFlag, bank as Bank, description as Description, case type WHEN 'R' then 'RPPS' when 'E' then 'ePay' when 'D' then 'Deposit' else 'Checking' end as Type FROM banks WITH (NOLOCK) WHERE type in ('C')"
                        .CommandType = CommandType.Text
                    End With

                    Using da As New SqlClient.SqlDataAdapter(Command)
                        da.Fill(ds, "banks")
                    End Using
                End Using

                ' Bind the data to the list
                Dim tbl As DataTable = ds.Tables("banks")
                Dim vue As DataView = tbl.DefaultView

                ' Bind the office list to the data
                With ctl
                    With .Properties
                        .DataSource = vue
                        .PopulateColumns()
                        .Columns("Default").Visible = False         ' Do not display the default column
                        .Columns("ActiveFlag").Visible = False      ' Do not display the active flag
                        .DisplayMember = "Description"
                        .ValueMember = "Bank"
                        .NullText = ""
                    End With
                    .Properties.PopupWidth = 300

                    ' Set the default item if possible
                    Dim rows() As DataRow = tbl.Select("([Default]<>0) AND ([ActiveFlag]<>0)", String.Empty)
                    If rows.GetUpperBound(0) >= 0 Then
                        .EditValue = rows(0)("bank")
                    End If

                    ' If there is no bank selected and there are banks then select the first bank in the list.
                    If .EditValue Is Nothing AndAlso tbl.Rows.Count > 0 Then
                        .EditValue = tbl.Rows(0).Item("bank")
                    End If

                    AddHandler .EditValueChanging, AddressOf DebtPlus.Data.Validation.LookUpEdit_ActiveTest
                End With

            Catch ex As SqlClient.SqlException
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error reading banks table")

            Finally
                System.Windows.Forms.Cursor.Current = current_cursor
            End Try
        End Sub

        Private Sub Button_Cancel_Click(ByVal sender As Object, ByVal e As EventArgs)
            Close()
        End Sub

        Private Sub Button_OK_Click(ByVal sender As Object, ByVal e As EventArgs)
            UnRegisterHandlers()
            Try
                If CurrentMode IsNot Nothing Then
                    CurrentMode.ProcessVoid()
                    CurrentMode.Visible = False
                    CurrentMode = Nothing
                    DoMessageBeep(MessageBeepEnum.OK)

                    ' Remove the list of checks from the trust_register
                    ds.Tables("registers_trust").Clear()
                End If

                TextCheckNumber.Text = String.Empty
                Button_OK.Enabled = False

                ' Tab to the check number field again.
                TextCheckNumber.Focus()
            Finally
                RegisterHandlers()
            End Try
        End Sub

        Private Sub CurrentMode_DataChanged(ByVal sender As Object, ByVal e As EventArgs)
            Button_OK.Enabled = Not HasErrors()
        End Sub

        Private Function HasErrors() As Boolean
            Dim answer As Boolean = True

            ' If there is no error on the client/creditor mode then look at our own data.
            Do
                If DxErrorProvider1.GetError(TextCheckNumber) <> String.Empty Then Exit Do
                If DxErrorProvider1.GetError(LookupEditBank) <> String.Empty Then Exit Do
                If TextCheckNumber.Text.Trim() = String.Empty Then Exit Do

                ' Look for the error condition in the displayed form
                answer = CurrentMode IsNot Nothing AndAlso CurrentMode.HasErrors
                Exit Do
            Loop

            Return answer
        End Function

        ''' <summary>
        ''' Find the trust register for the check that we wish to void
        ''' </summary>
        Private Function RegistersTrustRow(ByVal CheckNumber As String, ByVal Bank As Int32) As DataRow
            Dim row As DataRow = Nothing
            Dim tbl As DataTable = ds.Tables("registers_trust")

            ' Look for a match in the current tables first
            If tbl IsNot Nothing Then
                Dim rows() As DataRow = tbl.Select(String.Format("([checknum]='{0}') AND ([bank]={1:f0}) AND ([cleared]<>'D')", CheckNumber.Replace("'", "''"), Bank), "date_created desc")
                If rows.GetUpperBound(0) >= 0 Then
                    row = rows(0)
                End If

                rows = tbl.Select(String.Format("([checknum]='{0}') AND ([bank]={1:f0})", CheckNumber.Replace("'", "''"), Bank), "date_created desc")
                If rows.GetUpperBound(0) >= 0 Then
                    row = rows(0)
                End If
            End If

            ' If there is no match then ask the database to supply the closest match to the check number.
            ' We don't care about cleared status since the view will take care of things.
            If row Is Nothing Then
                Try
                    Using cmd As SqlClient.SqlCommand = New SqlCommand
                        With cmd
                            .Connection = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
                            .CommandText = "SELECT [trust_register],[tran_type],[client],[creditor],[check_order],[cleared],[bank],[checknum],[amount],[invoice_register],[sequence_number],[reconciled_date],[bank_xmit_date],[created_by],[date_created] FROM registers_trust WHERE [checknum]=@checknum AND [bank] = @bank"
                            .CommandType = CommandType.Text
                            .Parameters.Add("@checknum", SqlDbType.VarChar, 50).Value = CheckNumber
                            .Parameters.Add("@bank", SqlDbType.Int).Value = Bank
                        End With

                        Using da As New SqlClient.SqlDataAdapter(cmd)
                            da.FillLoadOption = LoadOption.OverwriteChanges
                            da.Fill(ds, "registers_trust")
                        End Using
                    End Using

                    tbl = ds.Tables("registers_trust")
                    With tbl
                        If .PrimaryKey.GetUpperBound(0) < 0 Then
                            .PrimaryKey = New DataColumn() {.Columns("trust_register")}
                        End If
                    End With

                Catch ex As SqlClient.SqlException
                    DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error reading registers_trust table")
                End Try

                ' Look up the row once we have read the table items
                Dim rows() As DataRow = tbl.Select(String.Format("([checknum]='{0}') AND ([bank]={1:f0}) AND ([cleared]<>'D')", CheckNumber.Replace("'", "''"), Bank), "date_created desc")
                If rows.GetUpperBound(0) < 0 Then
                    rows = tbl.Select(String.Format("([checknum]='{0}') AND ([bank]={1:f0})", CheckNumber.Replace("'", "''"), Bank), "date_created desc")
                End If

                If rows.GetUpperBound(0) >= 0 Then
                    row = rows(0)
                End If
            End If

            Return row
        End Function

        ''' <summary>
        ''' Validate the entered check number field
        ''' </summary>
        Private Sub TextCheckNumber_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs)
            UnRegisterHandlers()
            Try
                Dim ErrorMessage As String = String.Empty
                Dim checknum As String = TextCheckNumber.Text

                ' Find the check number. It is a required item but don't lock the control if it is empty incase the user wants to quit.
                If checknum <> String.Empty Then
                    Dim row As DataRow = RegistersTrustRow(checknum, bank)

                    If row Is Nothing Then
                        ErrorMessage = "Invalid check number"
                    Else
                        Select Case Convert.ToString(row("cleared"))
                            Case "V"
                                ErrorMessage = "The check was voided"
                                If row("reconciled_date") IsNot DBNull.Value Then ErrorMessage += " on " + Convert.ToDateTime(row("reconciled_date")).ToShortDateString
                            Case "R"
                                ErrorMessage = "The check was reconciled"
                                If row("reconciled_date") IsNot DBNull.Value Then ErrorMessage += " on " + Convert.ToDateTime(row("reconciled_date")).ToShortDateString
                            Case "D"
                                ErrorMessage = "The check was marked destroyed in printing"
                            Case Else
                        End Select
                    End If

                    ' Look for a valid type to the check
                    If ErrorMessage = String.Empty Then
                        ' Look for a client refund
                        If ClientVoid1.ValidTranType(Convert.ToString(row("tran_type"))) Then
                            CurrentMode = ClientVoid1

                            ' Look for a valid creditor check
                        ElseIf CreditorVoid1.ValidTranType(Convert.ToString(row("tran_type"))) Then
                            CurrentMode = CreditorVoid1
                        End If

                        ' Examine the check to determine the payment information
                        If CurrentMode Is Nothing Then
                            ErrorMessage = "Check type invalid"
                        ElseIf Not CurrentMode.ValidCheck(row) Then
                            ErrorMessage = "Check invalid"
                        End If
                    End If
                End If

                ' Set the error information and accept the change if possible
                DxErrorProvider1.SetError(TextCheckNumber, ErrorMessage)

                ' If there is an error then hide both panels
                If ErrorMessage <> String.Empty Then
                    ClientVoid1.Visible = False
                    CreditorVoid1.Visible = False
                End If

                Button_OK.Enabled = Not HasErrors()
            Finally
                RegisterHandlers()
            End Try
        End Sub

        ''' <summary>
        ''' Enable further processing once we have a valid check
        ''' </summary>
        Private Sub TextCheckNumber_Validated(ByVal sender As Object, ByVal e As EventArgs)

            If DxErrorProvider1.GetError(TextCheckNumber) = String.Empty AndAlso CurrentMode IsNot Nothing Then
                Select Case CurrentMode.Type
                    Case "creditor"
                        With ClientVoid1
                            .Visible = False
                            .TabStop = False
                            .SendToBack()
                        End With

                        With CreditorVoid1
                            .Visible = True
                            .TabStop = True
                            .BringToFront()
                            .Focus()
                        End With

                    Case "client"
                        With CreditorVoid1
                            .Visible = False
                            .TabStop = False
                            .SendToBack()
                        End With

                        With ClientVoid1
                            .Visible = True
                            .TabStop = True
                            .BringToFront()
                            .Focus()
                        End With

                    Case Else
                        Debug.Assert(False)
                End Select
            End If
        End Sub

        Private Sub LookupEditBank_EditValueChanged(ByVal sender As Object, ByVal e As EventArgs)
            Dim ErrorMessage As String = String.Empty

            If LookupEditBank.EditValue Is Nothing Then
                ErrorMessage = "Value Required"
                TextCheckNumber.Enabled = False
            Else
                TextCheckNumber.Enabled = True
            End If

            DxErrorProvider1.SetError(LookupEditBank, ErrorMessage)
            Button_OK.Enabled = Not HasErrors()
        End Sub
    End Class
End Namespace
