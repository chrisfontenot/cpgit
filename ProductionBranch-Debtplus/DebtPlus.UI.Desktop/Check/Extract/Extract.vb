#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}
#End Region

Option Strict Off
Option Explicit On

Imports DebtPlus.Utils
Imports System.Data.SqlClient
Imports System.Windows.Forms

Namespace Check.Extract
    Friend Module Extract

        Friend Enum FieldList As System.Int32
            Field_Zeros = 0
            Field_Empty
            Field_ABA
            Field_AccountNumber
            Field_CheckNumber
            Field_Cleared
            Field_Date
            Field_Amount
            Field_Cents
            Field_Payee
        End Enum

        Friend Sub ExtractChecks(ByVal ap As ArgParser)

            ' Build the list of expressions for the various cleared status values
            Dim my_settings As System.Collections.Specialized.NameValueCollection = DebtPlus.Configuration.Config.GetConfigSettings(DebtPlus.Configuration.Config.ConfigSet.CheckExtract)
            Dim parsing_data As New System.Collections.Specialized.ListDictionary(Comparer.DefaultInvariant)

            For Each key As String In my_settings.Keys
                If key.StartsWith("expr_") Then
                    parsing_data.Add(key, New parsing_expression(key.Substring(5), my_settings.Item(key)))
                End If
            Next

            If ReadBanksData(ap) Then
                ReadTransactions(ap)
                Dim tbl As DataTable = ap.ds.Tables("transactions")
                If tbl IsNot Nothing AndAlso tbl.Rows.Count > 0 Then

                    Dim outputfile As System.IO.StreamWriter = CreateFile(ap, ap.OutputDirectory)
                    If outputfile IsNot Nothing Then
                        Try
                            WriteTransactions(ap, tbl, parsing_data, outputfile)

                        Finally
                            outputfile.Close()
                            outputfile.Dispose()
                        End Try
                    End If
                End If

                ' Finally, we can close our window and terminate
                DebtPlus.Data.Forms.MessageBox.Show(String.Format("{0}{1}{1}The operation is complete", ap.FileName, Environment.NewLine), "Complete", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1)
            End If
        End Sub

        Private Function ReadBanksData(ByVal ap As ArgParser) As Boolean
            Dim answer As Boolean = False

            Using gdr As Repository.GetDataResult = Repository.LookupTables.Banks.GetAll(ap.ds, "banks")
                If Not gdr.Success Then
                    DebtPlus.UI.Common.ErrorHandling.HandleErrors(gdr.ex, "Error reading banks table")
                Else
                    answer = True
                    Dim tbl As DataTable = ap.ds.Tables("banks")
                    Debug.Assert(tbl IsNot Nothing)

                    Dim datarow = tbl.Rows.Find(ap.Bank)
                    If datarow IsNot Nothing Then
                        ap.PrefixLine = If(datarow.IsNull("prefix_line"), String.Empty, Convert.ToString(datarow("prefix_line")).Trim())
                        ap.SuffixLine = If(datarow.IsNull("suffix_line"), String.Empty, Convert.ToString(datarow("suffix_line")).Trim())
                        ap.OutputDirectory = If(datarow.IsNull("output_directory"), String.Empty, Convert.ToString(datarow("output_directory")).Trim())
                    End If
                End If
            End Using

            Return answer
        End Function

        Private Function CreateFile(ByVal ap As ArgParser, ByVal OutputDirectory As String) As System.IO.StreamWriter

            ' If there is a passed name then try to open the file
            If ap.FileName = String.Empty Then

                ' Attempt to generate a working file in the directory
                Dim LocalName As String = String.Format("{0:yyyyMMdd}_{0:HHmmss}.txt", DateTime.Now)

                ' Try to put the file in the output directory
                ap.FileName = System.IO.Path.Combine(OutputDirectory, LocalName)
            End If

            ' Open the file
            Try
                Return New System.IO.StreamWriter(ap.FileName, False, System.Text.Encoding.ASCII, 4096)

            Catch ex As Exception
                DebtPlus.Svc.Common.ErrorHandling.HandleErrors(ex)
            End Try

            ' Ask the user for a new file
            Using frm As New SaveFileDialog()
                With frm
                    .FileName = ap.FileName
                    .Filter = "Text Files (*.txt)|*.txt|All Files (*.*)|*.*"
                    .FilterIndex = 0
                    .InitialDirectory = OutputDirectory
                    .DefaultExt = "txt"
                    .CheckPathExists = True
                    .CheckFileExists = False
                    .AddExtension = True
                    .ValidateNames = True
                    .Title = "Select Output File"
                    If .ShowDialog() = DialogResult.Cancel Then Return Nothing
                    ap.FileName = .FileName
                End With
            End Using

            ' Try to open the output file with the name supplied
            Try
                Return New System.IO.StreamWriter(ap.FileName)

            Catch ex As Exception
                DebtPlus.Data.Forms.MessageBox.Show(ex.ToString(), "Error Opening output file", MessageBoxButtons.OK, MessageBoxIcon.Error)
            End Try

            Return Nothing
        End Function

        Private Sub ReadTransactions(ByVal ap As ArgParser)

            ' Obtain the output text buffer
            Dim cn As SqlClient.SqlConnection = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
            Dim current_cursor As Cursor = Cursor.Current

            ' Create a new command to retrieve the information from the database
            Try
                Cursor.Current = Cursors.WaitCursor
                cn.Open()
                Using ExtractCmd As SqlClient.SqlCommand = New SqlCommand
                    With ExtractCmd
                        .Connection = cn
                        .CommandText = "xpr_bank_extract"
                        .CommandType = CommandType.StoredProcedure
                        .Parameters.Add("@bank", SqlDbType.Int).Value = ap.Bank
                    End With

                    Using da As New SqlClient.SqlDataAdapter(ExtractCmd)
                        da.Fill(ap.ds, "transactions")
                    End Using
                End Using

            Catch ex As SqlClient.SqlException
                Using gdr As New Repository.GetDataResult()
                    gdr.HandleException(ex)
                    DebtPlus.UI.Common.ErrorHandling.HandleErrors(gdr, "error extracting information")
                End Using

            Finally
                Cursor.Current = current_cursor
                If Not cn Is Nothing Then cn.Dispose()
            End Try
        End Sub

        Private Sub WriteTransactions(ByVal ap As ArgParser, ByVal tbl As DataTable, ByVal parsing_data As System.Collections.Specialized.ListDictionary, ByVal OutputFile As System.IO.StreamWriter)

            ' Write the prefix lines if there are any
            If ap.PrefixLine <> String.Empty Then OutputFile.WriteLine(String.Format(ap.PrefixLine, Now))

            ' Process the dataset once it has been loaded
            Dim vue As DataView = tbl.DefaultView
            Dim RowCount As Int32 = 0
            Dim DebitAmt As Decimal = 0D
            Dim CreditAmt As Decimal = 0D

            For Each drv As DataRowView In vue
                Dim ItemArray(10) As Object
                Dim FieldNo As Int32 = 0
                For FieldNo = ItemArray.GetLowerBound(0) To ItemArray.GetUpperBound(0)
                    ItemArray(FieldNo) = String.Empty
                Next

                ItemArray(FieldList.Field_ABA) = 0
                ItemArray(FieldList.Field_AccountNumber) = 0
                ItemArray(FieldList.Field_CheckNumber) = 0
                ItemArray(FieldList.Field_Amount) = 0D
                ItemArray(FieldList.Field_Date) = Date.MinValue

                For FieldNo = tbl.Columns.Count - 1 To 0 Step -1
                    If drv.Item(FieldNo) IsNot DBNull.Value Then
                        Select Case tbl.Columns(FieldNo).ColumnName.ToLower()
                            Case "aba"
                                Dim TempString As String = Convert.ToString(drv(FieldNo)).Trim()
                                Long.TryParse(TempString, ItemArray(FieldList.Field_ABA))

                            Case "account_number"
                                Dim TempString As String = Convert.ToString(drv(FieldNo)).Trim()
                                Long.TryParse(TempString, ItemArray(FieldList.Field_AccountNumber))

                            Case "cleared"
                                ItemArray(FieldList.Field_Cleared) = Convert.ToString(drv(FieldNo)).Trim()

                            Case "checknum"
                                Dim TempString As String = Convert.ToString(drv(FieldNo)).Trim()
                                Long.TryParse(TempString, ItemArray(FieldList.Field_CheckNumber))

                            Case "amount"
                                ItemArray(FieldList.Field_Amount) = Math.Truncate(Convert.ToDecimal(drv(FieldNo)), 2)

                            Case "date_created"
                                ItemArray(FieldList.Field_Date) = Convert.ToDateTime(drv(FieldNo))

                            Case "payee"
                                ItemArray(FieldList.Field_Payee) = Convert.ToString(drv(FieldNo)).Trim()
                            Case Else
                        End Select
                    End If
                Next

                ItemArray(FieldList.Field_Cents) = Convert.ToInt64(ItemArray(FieldList.Field_Amount) * 100.0)
                ItemArray(FieldList.Field_Zeros) = Convert.ToInt64(0)

                Dim Fmt As Object = Nothing
                If parsing_data.Count > 0 Then
                    If ItemArray(FieldList.Field_Cleared).Trim() = "2" Then
                        ItemArray(FieldList.Field_Cleared) = "V"
                    ElseIf ItemArray(FieldList.Field_Cleared) = "I" Then
                        ItemArray(FieldList.Field_Cleared) = ""
                    End If

                    Fmt = parsing_data("expr_" + ItemArray(FieldList.Field_Cleared))
                    If Fmt Is Nothing Then Fmt = parsing_data("expr_")
                    If Fmt Is Nothing Then Fmt = parsing_data.Item(0)
                End If

                RowCount += 1
                If ItemArray(FieldList.Field_Cleared) = "" Then
                    DebitAmt += ItemArray(FieldList.Field_Amount)
                Else
                    CreditAmt += ItemArray(FieldList.Field_Amount)
                End If

                If Fmt Is Nothing Then
                    OutputFile.WriteLine(String.Format("{3:000000000000}I  {4:0000000000}{8:000000000000000}{6:MMddyy}{9,-34:s}", ItemArray))
                Else
                    Dim OutputFormat As String = CType(Fmt, parsing_expression).fmt
                    Dim OutputString As String = String.Format(OutputFormat, ItemArray)
                    OutputFile.WriteLine(OutputString)
                End If
            Next

            ' Write the suffix lines if there are any
            ' If they want stats then include the following fields:
            ' {0} = Number of detail records written
            ' {1} = Total amount of checks written
            ' {2} = Total amount of checks voided
            ' {3} = Net amount (written - voided)
            ' {4} = Total amount of checks written as cents
            ' {5} = Total amount of checks voided as cents
            ' {6} = Net amount (written - voided) as cents
            ' {7} = Total dollar amount of detail records as cents
            If ap.SuffixLine <> String.Empty Then OutputFile.WriteLine(String.Format(ap.SuffixLine, New Object() {RowCount, DebitAmt, CreditAmt, DebitAmt - CreditAmt, Convert.ToInt64(DebitAmt * 100D), Convert.ToInt64(CreditAmt * 100D), Convert.ToInt64((DebitAmt - CreditAmt) * 100), Convert.ToInt64((DebitAmt + CreditAmt) * 100)}))
        End Sub
    End Module

End Namespace