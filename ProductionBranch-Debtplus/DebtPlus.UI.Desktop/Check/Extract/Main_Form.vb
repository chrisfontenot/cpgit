#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}
#End Region

Option Strict Off
Option Explicit On

Imports DebtPlus.Utils

Namespace Check.Extract
    Friend Class Main_Form
        Inherits DebtPlus.Data.Forms.DebtPlusForm

        ''' <summary>
        ''' Main entry to the program
        ''' </summary>
        Friend ap As Check.Extract.ArgParser

        Public Sub New(ByVal ap As ArgParser)
            MyClass.New()
            Me.ap = ap
            AddHandler Me.Load, AddressOf Form1_Load
            AddHandler SimpleButton_OK.Click, AddressOf SimpleButton_OK_Click
        End Sub

#Region "Windows Form Designer generated code "
        Public Sub New()
            MyBase.New()

            'This call is required by the Windows Form Designer.
            InitializeComponent()

        End Sub

        'Form overrides dispose to clean up the component list.
        Protected Overloads Overrides Sub Dispose(ByVal Disposing As Boolean)
            If Disposing Then
                If Not components Is Nothing Then
                    components.Dispose()
                End If
            End If
            MyBase.Dispose(Disposing)
        End Sub

        'Required by the Windows Form Designer
        Private components As System.ComponentModel.IContainer
        Public WithEvents Label1 As DevExpress.XtraEditors.LabelControl
        'NOTE: The following procedure is required by the Windows Form Designer
        'It can be modified using the Windows Form Designer.
        'Do not modify it using the code editor.
        Friend WithEvents SimpleButton_OK As DevExpress.XtraEditors.SimpleButton
        Friend WithEvents SimpleButton_Cancel As DevExpress.XtraEditors.SimpleButton
        Friend WithEvents BankListLookupEdit As DevExpress.XtraEditors.LookUpEdit
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
            Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Main_Form))
            Me.Label1 = New DevExpress.XtraEditors.LabelControl
            Me.BankListLookupEdit = New DevExpress.XtraEditors.LookUpEdit
            Me.SimpleButton_OK = New DevExpress.XtraEditors.SimpleButton
            Me.SimpleButton_Cancel = New DevExpress.XtraEditors.SimpleButton
            CType(Me.DxErrorProvider1, System.ComponentModel.ISupportInitialize).BeginInit()

            CType(Me.BankListLookupEdit.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.SuspendLayout()

            '
            'Label1
            '
            Me.Label1.Appearance.BackColor = System.Drawing.SystemColors.Control
            Me.Label1.Appearance.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.Label1.Appearance.ForeColor = System.Drawing.SystemColors.ControlText
            Me.Label1.Appearance.Options.UseBackColor = True
            Me.Label1.Appearance.Options.UseFont = True
            Me.Label1.Appearance.Options.UseForeColor = True
            Me.Label1.Cursor = System.Windows.Forms.Cursors.Default
            Me.Label1.Location = New System.Drawing.Point(24, 26)
            Me.Label1.Name = "Label1"
            Me.Label1.Size = New System.Drawing.Size(71, 14)
            Me.Label1.TabIndex = 0
            Me.Label1.Text = "Bank Account:"
            '
            'BankListLookupEdit
            '
            Me.BankListLookupEdit.Location = New System.Drawing.Point(112, 24)
            Me.BankListLookupEdit.Name = "BankListLookupEdit"
            Me.BankListLookupEdit.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.BankListLookupEdit.Size = New System.Drawing.Size(192, 20)
            Me.BankListLookupEdit.TabIndex = 4
            '
            'SimpleButton_OK
            '
            Me.SimpleButton_OK.DialogResult = System.Windows.Forms.DialogResult.OK
            Me.SimpleButton_OK.Location = New System.Drawing.Point(71, 64)
            Me.SimpleButton_OK.Name = "SimpleButton_OK"
            Me.SimpleButton_OK.Size = New System.Drawing.Size(75, 23)
            Me.SimpleButton_OK.TabIndex = 5
            Me.SimpleButton_OK.Text = "&OK"
            '
            'SimpleButton_Cancel
            '
            Me.SimpleButton_Cancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
            Me.SimpleButton_Cancel.Location = New System.Drawing.Point(167, 64)
            Me.SimpleButton_Cancel.Name = "SimpleButton_Cancel"
            Me.SimpleButton_Cancel.Size = New System.Drawing.Size(75, 23)
            Me.SimpleButton_Cancel.TabIndex = 6
            Me.SimpleButton_Cancel.Text = "&Cancel"
            '
            'Main_Form
            '
            Me.Appearance.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.Appearance.Options.UseFont = True
            Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
            Me.ClientSize = New System.Drawing.Size(312, 109)
            Me.Controls.Add(Me.SimpleButton_Cancel)
            Me.Controls.Add(Me.SimpleButton_OK)
            Me.Controls.Add(Me.BankListLookupEdit)
            Me.Controls.Add(Me.Label1)
            Me.Cursor = System.Windows.Forms.Cursors.Default
            Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
            Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
            Me.Location = New System.Drawing.Point(4, 23)
            Me.MaximizeBox = False
            Me.MinimizeBox = False
            Me.Name = "Main_Form"
            Me.RightToLeft = System.Windows.Forms.RightToLeft.No
            Me.ToolTipController1.SetSuperTip(Me, Nothing)
            Me.Text = "ARP Extract"
            CType(Me.DxErrorProvider1, System.ComponentModel.ISupportInitialize).EndInit()

            CType(Me.BankListLookupEdit.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            Me.ResumeLayout(False)
            Me.PerformLayout()

        End Sub
#End Region

        ' ARP (Account Reconciliation Program) Extract Utility

        ''' <summary>
        ''' Process the form load event
        ''' </summary>
        Private Sub Form1_Load(ByVal sender As Object, ByVal e As System.EventArgs)
            BankListLookupEdit_Load()
            SimpleButton_OK.Enabled = Not HasErrors()

            ' Bind the controls
            AddHandler BankListLookupEdit.EditValueChanging, AddressOf BankListLookupEdit_EditValueChanging
        End Sub

        ''' <summary>
        ''' Load the bank information
        ''' </summary>
        Private Sub BankListLookupEdit_Load()

            Try
                Dim col As System.Collections.Generic.List(Of DebtPlus.LINQ.bank) = DebtPlus.LINQ.Cache.bank.getList().Where(Function(s) s.ActiveFlag AndAlso s.type = "C").OrderBy(Function(s) s.description).ToList()

                ' Bind the office list to the data
                BankListLookupEdit.Properties.DataSource = col
                BankListLookupEdit.Properties.Columns.AddRange(New DevExpress.XtraEditors.Controls.LookUpColumnInfo() {New DevExpress.XtraEditors.Controls.LookUpColumnInfo("Id", "ID", 25, DevExpress.Utils.FormatType.Numeric, "f0", True, DevExpress.Utils.HorzAlignment.Far), New DevExpress.XtraEditors.Controls.LookUpColumnInfo("description", "Description", 50, DevExpress.Utils.FormatType.None, String.Empty, True, DevExpress.Utils.HorzAlignment.Near)})
                BankListLookupEdit.Properties.DisplayMember = "description"
                BankListLookupEdit.Properties.ValueMember = "Id"
                BankListLookupEdit.Properties.NullText = String.Empty
                BankListLookupEdit.Properties.PopupWidth = BankListLookupEdit.Width + 100           ' Add some extra space for the dropdown width

                ' Set the default bank reference into the controls.
                BankListLookupEdit.EditValue = DebtPlus.LINQ.Cache.bank.getDefault("C")

            Catch ex As Exception
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error reading banks table")
            End Try
        End Sub

        ''' <summary>
        ''' Change in the bank location
        ''' </summary>
        Private Sub BankListLookupEdit_EditValueChanging(ByVal sender As Object, ByVal e As DevExpress.XtraEditors.Controls.ChangingEventArgs)
            SimpleButton_OK.Enabled = Not HasErrors()
        End Sub

        Private Function HasErrors() As Boolean
            If BankListLookupEdit.EditValue Is Nothing Then
                Return True
            End If

            Return False
        End Function

        ''' <summary>
        ''' Process the OK button click event
        ''' </summary>
        Private Sub SimpleButton_OK_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
            ap.Bank = Convert.ToInt32(BankListLookupEdit.EditValue)
        End Sub
    End Class
End Namespace
