#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}
#End Region

Option Compare Binary
Option Explicit On
Option Strict On

Imports System.Windows.Forms

Namespace Check.Extract

    ''' <summary>
    ''' Argument Parsing Class
    ''' </summary>
    Friend Class ArgParser
        Inherits DebtPlus.Utils.ArgParserBase

        Friend ReadOnly ds As New DataSet("ds")
        Friend OutputDirectory As String = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments)
        Friend PrefixLine As String = String.Empty
        Friend SuffixLine As String = String.Empty

        ''' <summary>
        ''' Bank account number
        ''' </summary>
        Private _bank As System.Int32 = -1
        Public Property Bank() As System.Int32
            Get
                Return _bank
            End Get
            Set(ByVal value As System.Int32)
                _bank = value
            End Set
        End Property

        ''' <summary>
        ''' Filename
        ''' </summary>
        Private _FileName As String = String.Empty
        Public Property FileName() As String
            Get
                Return _FileName
            End Get
            Set(ByVal value As String)
                _FileName = value
            End Set
        End Property

        ''' <summary>
        ''' Create an instance of our class
        ''' </summary>
        Public Sub New()
            MyBase.New(New String() {"b", "f"})
        End Sub

        ''' <summary>
        ''' Process a switch on the runline
        ''' </summary>
        Protected Overrides Function OnSwitch(ByVal switchSymbol As String, ByVal switchValue As String) As SwitchStatus
            Dim ss As SwitchStatus = DebtPlus.Utils.ArgParserBase.SwitchStatus.NoError

            Select Case switchSymbol
                Case "b"
                    If Not Int32.TryParse(switchValue, Bank) Then ss = DebtPlus.Utils.ArgParserBase.SwitchStatus.YesError
                Case "f"
                    FileName = switchValue
                Case "?"
                    ss = DebtPlus.Utils.ArgParserBase.SwitchStatus.ShowUsage
                Case Else
                    ss = DebtPlus.Utils.ArgParserBase.SwitchStatus.YesError
            End Select

            Return ss
        End Function

        ''' <summary>
        ''' Generate the command usage information
        ''' </summary>
        Protected Overrides Sub OnUsage(ByVal errorInfo As String)
            'AppendErrorLine(String.Format("Usage: {0} [-b#] [-f name]", fname))
            'AppendErrorLine("       [-b#]     : bank account number (#) to extract")
            'AppendErrorLine("       [-f name] : filename to be used for the extract")
        End Sub

        ''' <summary>
        ''' Process the end of the parse operation
        ''' </summary>
        Protected Overrides Function OnDoneParse() As SwitchStatus
            Dim ss As SwitchStatus = DebtPlus.Utils.ArgParserBase.SwitchStatus.NoError

            If Bank <= 0 Then
                Using frm As New Main_Form(Me)
                    If frm.ShowDialog() <> DialogResult.OK Then ss = DebtPlus.Utils.ArgParserBase.SwitchStatus.YesError
                End Using
            End If

            Return ss
        End Function
    End Class
End Namespace
