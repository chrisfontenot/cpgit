Option Compare Binary
Option Explicit On
Option Strict On

Imports DebtPlus.UI.FormLib.Check
Imports System.Resources
Imports System.Reflection
Imports System.Windows.Forms

Namespace Check.Reconcile.File
    Friend Class ReconcileArgParser
        Inherits DebtPlus.Utils.ArgParserBase

        ' Strings for the file
        Public RM As New ResourceManager("DebtPlus.UI.Desktop.Form_Reconcile_Strings", Assembly.GetExecutingAssembly())

        Private privateFileName As String

        Public ReadOnly Property FileName() As String
            Get
                Return privateFileName
            End Get
        End Property

        Private privateBatch As Int32 = -1

        Public ReadOnly Property Batch() As Int32
            Get
                Return privateBatch
            End Get
        End Property

        Private privateBank As Int32 = -1

        Public ReadOnly Property Bank() As Int32
            Get
                Return privateBank
            End Get
        End Property

        ''' <summary>
        ''' Create an instance of our class
        ''' </summary>
        Public Sub New()
            MyBase.New(New String() {})
        End Sub

        ''' <summary>
        ''' Generate the command usage information
        ''' </summary>
        Protected Overrides Sub OnUsage(ByVal errorInfo As String)
            ' deprecated
        End Sub

        Protected Overrides Function OnDoneParse() As SwitchStatus
            Dim ss As SwitchStatus = DebtPlus.Utils.ArgParserBase.SwitchStatus.NoError

            ' Request the file to be processed
            If Not request_filename() Then
                ss = DebtPlus.Utils.ArgParserBase.SwitchStatus.Quit
            Else

                ' Request the batch ID
                Using frm As New ReconcileBatchForm
                    With frm
                        Dim answer As DialogResult = .ShowDialog()
                        privateBatch = .BatchID
                        If answer <> DialogResult.OK Then
                            ss = DebtPlus.Utils.ArgParserBase.SwitchStatus.Quit
                        End If
                    End With
                End Using
            End If

            Return ss
        End Function

        Public Function request_filename() As Boolean
            Dim dlg As New OpenFileDialog
            Dim answer As DialogResult

            With dlg
                .CheckFileExists = True
                .CheckPathExists = True
                .DefaultExt = RM.GetString("file_open_extnsion")
                .DereferenceLinks = True
                .Filter = RM.GetString("file_open_filter")
                .InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.Personal)
                .Multiselect = False
                .ReadOnlyChecked = True
                .RestoreDirectory = True
                .ShowReadOnly = False
                .Title = RM.GetString("file_open_title")
                .ValidateNames = True
                answer = .ShowDialog()

                privateFileName = .FileName
                .Dispose()
            End With

            Return (answer = DialogResult.OK)
        End Function
    End Class
End Namespace
