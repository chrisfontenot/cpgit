#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}

#End Region

Option Compare Binary
Option Explicit On
Option Strict On

Imports System.Configuration
Imports System.ComponentModel
Imports DebtPlus.Data.Forms
Imports DebtPlus.Utils
Imports System.Collections.Specialized
Imports System.Data.SqlClient
Imports System.Text.RegularExpressions
Imports System.IO
Imports System.Text
Imports System.Windows.Forms

Namespace Check.Reconcile.File
    Friend Class Form_Reconcile
        Inherits DebtPlusForm

        Private ap As ReconcileArgParser = Nothing

        Public Sub New()
            MyBase.New()
            InitializeComponent()
            AddHandler Button_Cancel.Click, AddressOf Button_Cancel_Click
            AddHandler Me.Load, AddressOf Form_Reconcile_Load
            AddHandler bt.DoWork, AddressOf bt_DoWork
            AddHandler bt.RunWorkerCompleted, AddressOf bt_RunWorkerCompleted
        End Sub

        Public Sub New(ByVal ap As ReconcileArgParser)
            MyClass.New()
            Me.ap = ap
        End Sub

        ' Storage for the program
        Public parsing_data As New ListDictionary(Comparer.DefaultInvariant)
        Public batch_hash_total As Decimal = 0D
        Public batch_amount As Decimal = 0D
        Public batch_count As Int32 = 0

        ' Current reconcile batch ID
        Private WithEvents bt As New BackgroundWorker

#Region " Windows Form Designer generated code "

        'Form overrides dispose to clean up the component list.
        Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
            If disposing Then
                If components IsNot Nothing Then
                    components.Dispose()
                End If
            End If
            MyBase.Dispose(disposing)
        End Sub

        'Required by the Windows Form Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Windows Form Designer
        'It can be modified using the Windows Form Designer.  
        'Do not modify it using the code editor.
        Friend WithEvents Button_Cancel As DevExpress.XtraEditors.SimpleButton
        Friend WithEvents Label1 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents Label2 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents Label3 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents Label4 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents Label5 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents Label6 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents Label7 As DevExpress.XtraEditors.LabelControl
        Public WithEvents Label_Reconciling_Item As DevExpress.XtraEditors.LabelControl
        Public WithEvents Label_Transaction_Type As DevExpress.XtraEditors.LabelControl
        Public WithEvents Label_checknum As DevExpress.XtraEditors.LabelControl
        Public WithEvents Label_Date As DevExpress.XtraEditors.LabelControl
        Public WithEvents Label_Amount As DevExpress.XtraEditors.LabelControl
        Public WithEvents Label_Bank As DevExpress.XtraEditors.LabelControl

        <System.Diagnostics.DebuggerStepThrough()>
        Private Sub InitializeComponent()
            Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Form_Reconcile))
            Me.Button_Cancel = New DevExpress.XtraEditors.SimpleButton
            Me.Label1 = New DevExpress.XtraEditors.LabelControl
            Me.Label2 = New DevExpress.XtraEditors.LabelControl
            Me.Label3 = New DevExpress.XtraEditors.LabelControl
            Me.Label4 = New DevExpress.XtraEditors.LabelControl
            Me.Label5 = New DevExpress.XtraEditors.LabelControl
            Me.Label6 = New DevExpress.XtraEditors.LabelControl
            Me.Label7 = New DevExpress.XtraEditors.LabelControl
            Me.Label_Reconciling_Item = New DevExpress.XtraEditors.LabelControl
            Me.Label_Transaction_Type = New DevExpress.XtraEditors.LabelControl
            Me.Label_checknum = New DevExpress.XtraEditors.LabelControl
            Me.Label_Date = New DevExpress.XtraEditors.LabelControl
            Me.Label_Amount = New DevExpress.XtraEditors.LabelControl
            Me.Label_Bank = New DevExpress.XtraEditors.LabelControl
            CType(Me.DxErrorProvider1, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.SuspendLayout()
            '
            'Button_Cancel
            '
            Me.Button_Cancel.Anchor = System.Windows.Forms.AnchorStyles.Bottom
            Me.Button_Cancel.Location = New System.Drawing.Point(152, 223)
            Me.Button_Cancel.Name = "Button_Cancel"
            Me.Button_Cancel.Size = New System.Drawing.Size(75, 25)
            Me.Button_Cancel.TabIndex = 2
            Me.Button_Cancel.Text = "&Cancel"
            Me.Button_Cancel.ToolTip = "Cancel the dialog and return to the previous screen"
            Me.Button_Cancel.ToolTipController = Me.ToolTipController1
            '
            'Label1
            '
            Me.Label1.Location = New System.Drawing.Point(16, 60)
            Me.Label1.Name = "Label1"
            Me.Label1.Size = New System.Drawing.Size(80, 13)
            Me.Label1.TabIndex = 3
            Me.Label1.Text = "Reconciling item:"
            '
            'Label2
            '
            Me.Label2.Location = New System.Drawing.Point(16, 85)
            Me.Label2.Name = "Label2"
            Me.Label2.Size = New System.Drawing.Size(87, 13)
            Me.Label2.TabIndex = 4
            Me.Label2.Text = "Transaction Type:"
            '
            'Label3
            '
            Me.Label3.Location = New System.Drawing.Point(16, 110)
            Me.Label3.Name = "Label3"
            Me.Label3.Size = New System.Drawing.Size(73, 13)
            Me.Label3.TabIndex = 5
            Me.Label3.Text = "Check Number:"
            '
            'Label4
            '
            Me.Label4.Location = New System.Drawing.Point(16, 135)
            Me.Label4.Name = "Label4"
            Me.Label4.Size = New System.Drawing.Size(27, 13)
            Me.Label4.TabIndex = 6
            Me.Label4.Text = "Date:"
            '
            'Label5
            '
            Me.Label5.Location = New System.Drawing.Point(16, 160)
            Me.Label5.Name = "Label5"
            Me.Label5.Size = New System.Drawing.Size(41, 13)
            Me.Label5.TabIndex = 7
            Me.Label5.Text = "Amount:"
            '
            'Label6
            '
            Me.Label6.Location = New System.Drawing.Point(16, 185)
            Me.Label6.Name = "Label6"
            Me.Label6.Size = New System.Drawing.Size(27, 13)
            Me.Label6.TabIndex = 8
            Me.Label6.Text = "Bank:"
            '
            'Label7
            '
            Me.Label7.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                                      Or System.Windows.Forms.AnchorStyles.Right), 
                                     System.Windows.Forms.AnchorStyles)
            Me.Label7.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap
            Me.Label7.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
            Me.Label7.Location = New System.Drawing.Point(5, 12)
            Me.Label7.Name = "Label7"
            Me.Label7.Size = New System.Drawing.Size(359, 32)
            Me.Label7.TabIndex = 9
            Me.Label7.Text = "The system is processing the check reconciliation on the following item. Please l" &
                             "et it continue."
            Me.Label7.UseMnemonic = False
            '
            'Label_Reconciling_Item
            '
            Me.Label_Reconciling_Item.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
            Me.Label_Reconciling_Item.Location = New System.Drawing.Point(128, 60)
            Me.Label_Reconciling_Item.Name = "Label_Reconciling_Item"
            Me.Label_Reconciling_Item.Size = New System.Drawing.Size(236, 13)
            Me.Label_Reconciling_Item.TabIndex = 10
            '
            'Label_Transaction_Type
            '
            Me.Label_Transaction_Type.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
            Me.Label_Transaction_Type.Location = New System.Drawing.Point(128, 85)
            Me.Label_Transaction_Type.Name = "Label_Transaction_Type"
            Me.Label_Transaction_Type.Size = New System.Drawing.Size(236, 13)
            Me.Label_Transaction_Type.TabIndex = 11
            '
            'Label_checknum
            '
            Me.Label_checknum.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
            Me.Label_checknum.Location = New System.Drawing.Point(128, 110)
            Me.Label_checknum.Name = "Label_checknum"
            Me.Label_checknum.Size = New System.Drawing.Size(236, 13)
            Me.Label_checknum.TabIndex = 12
            '
            'Label_Date
            '
            Me.Label_Date.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
            Me.Label_Date.Location = New System.Drawing.Point(128, 135)
            Me.Label_Date.Name = "Label_Date"
            Me.Label_Date.Size = New System.Drawing.Size(236, 13)
            Me.Label_Date.TabIndex = 13
            '
            'Label_Amount
            '
            Me.Label_Amount.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
            Me.Label_Amount.Location = New System.Drawing.Point(128, 160)
            Me.Label_Amount.Name = "Label_Amount"
            Me.Label_Amount.Size = New System.Drawing.Size(236, 13)
            Me.Label_Amount.TabIndex = 14
            '
            'Label_Bank
            '
            Me.Label_Bank.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
            Me.Label_Bank.Location = New System.Drawing.Point(128, 185)
            Me.Label_Bank.Name = "Label_Bank"
            Me.Label_Bank.Size = New System.Drawing.Size(236, 13)
            Me.Label_Bank.TabIndex = 15
            '
            'Form_Reconcile
            '
            Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
            Me.ClientSize = New System.Drawing.Size(376, 264)
            Me.Controls.Add(Me.Label_Bank)
            Me.Controls.Add(Me.Label_Amount)
            Me.Controls.Add(Me.Label_Date)
            Me.Controls.Add(Me.Label_checknum)
            Me.Controls.Add(Me.Label_Transaction_Type)
            Me.Controls.Add(Me.Label_Reconciling_Item)
            Me.Controls.Add(Me.Label7)
            Me.Controls.Add(Me.Label6)
            Me.Controls.Add(Me.Label5)
            Me.Controls.Add(Me.Label4)
            Me.Controls.Add(Me.Label3)
            Me.Controls.Add(Me.Label2)
            Me.Controls.Add(Me.Label1)
            Me.Controls.Add(Me.Button_Cancel)
            Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
            Me.LookAndFeel.UseDefaultLookAndFeel = True
            Me.Name = "Form_Reconcile"
            Me.Text = "Load check reconcile information"
            Me.TopMost = True
            CType(Me.DxErrorProvider1, System.ComponentModel.ISupportInitialize).EndInit()
            Me.ResumeLayout(False)
            Me.PerformLayout()
        End Sub

#End Region

        Private Delegate Sub ValueDelegate(ByVal Value As String)

        Private Sub Set_BankName(ByVal Value As String)
            If Label_Bank.InvokeRequired Then
                Dim ia As IAsyncResult = Label_Bank.BeginInvoke(New ValueDelegate(AddressOf Set_BankName), New Object() {Value})
                Label_Bank.EndInvoke(ia)
            Else
                Label_Bank.Text = Value
                Label_Bank.Invalidate()
            End If
        End Sub

        Private Sub Set_Amount(ByVal Value As String)
            If Label_Amount.InvokeRequired Then
                Dim ia As IAsyncResult = BeginInvoke(New ValueDelegate(AddressOf Set_Amount), New Object() {Value})
                Label_Amount.EndInvoke(ia)
            Else
                Label_Amount.Text = Value
                Label_Amount.Invalidate()
            End If
        End Sub

        Private Sub Set_checknum(ByVal Value As String)
            If Label_checknum.InvokeRequired Then
                Dim ia As IAsyncResult = BeginInvoke(New ValueDelegate(AddressOf Set_checknum), New Object() {Value})
                Label_checknum.EndInvoke(ia)
            Else
                Label_checknum.Text = Value
                Label_checknum.Invalidate()
            End If
        End Sub

        Private Sub Set_Reconciling_Item(ByVal Value As String)
            If Label_Reconciling_Item.InvokeRequired Then
                Dim ia As IAsyncResult = BeginInvoke(New ValueDelegate(AddressOf Set_Reconciling_Item), New Object() {Value})
                Label_Reconciling_Item.EndInvoke(ia)
            Else
                Label_Reconciling_Item.Text = Value
                Label_Reconciling_Item.Invalidate()
            End If
        End Sub

        Private Sub Set_Transaction_Type(ByVal Value As String)
            If Label_Transaction_Type.InvokeRequired Then
                Dim ia As IAsyncResult = BeginInvoke(New ValueDelegate(AddressOf Set_Transaction_Type), New Object() {Value})
                Label_Transaction_Type.EndInvoke(ia)
            Else
                Label_Transaction_Type.Text = Value
                Label_Transaction_Type.Invalidate()
            End If
        End Sub

        Private Sub Set_Date(ByVal Value As String)
            If Label_Date.InvokeRequired Then
                Dim ia As IAsyncResult = BeginInvoke(New ValueDelegate(AddressOf Set_Date), New Object() {Value})
                Label_Date.EndInvoke(ia)
            Else
                Label_Date.Text = Value
                Label_Date.Invalidate()
            End If
        End Sub

        Private Sub Button_Cancel_Click(ByVal sender As Object, ByVal e As EventArgs)
            DialogResult = Windows.Forms.DialogResult.Cancel
        End Sub

        Private Sub Form_Reconcile_Load(ByVal sender As Object, ByVal e As EventArgs)

            ' Build the list of regular expressions for the various types
            ' There may or may not be items. It is ok. If there are no items then the file won't be parsed and loaded. That is the default "fall-back" condition.
            parsing_data = DebtPlus.Configuration.Config.GetRegexExprList(DebtPlus.Configuration.Config.GetConfigSettings(DebtPlus.Configuration.Config.ConfigSet.CheckReconcileFile))

            ' Load the reconciliation file
            bt.RunWorkerAsync()
        End Sub

        Private Sub bt_DoWork(ByVal sender As Object, ByVal e As DoWorkEventArgs)

            ' Read the file
            Dim fso As New FileStream(ap.FileName, FileMode.Open, FileAccess.Read, FileShare.Read)
            Dim src_text As New StreamReader(fso)

            ' Find the hash key if there is one
            Dim hash_dictonary_expr As DebtPlus.Configuration.RegexParser = CType(parsing_data.Item("HASH"), DebtPlus.Configuration.RegexParser)
            Dim hash_rx As Regex = Nothing
            If hash_dictonary_expr IsNot Nothing Then hash_rx = hash_dictonary_expr.rx

            Try
                Using cn As SqlConnection = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
                    cn.Open()

                    Using txn As SqlTransaction = cn.BeginTransaction
                        Do

                            ' Read the next line from the source file. Stop at the EOF condition.
                            Dim source_line As String = src_text.ReadLine()
                            If source_line Is Nothing Then Exit Do

                            ' Determine if the input line is valid
                            Dim valid_line As Boolean = True
                            source_line = source_line.Trim()
                            If source_line = String.Empty Then valid_line = False

                            ' Look for the various other matches for the types. Do not process the HASH entry again.
                            For Each key As String In parsing_data.Keys

                                ' Find the expression for this key.
                                Dim expr As DebtPlus.Configuration.RegexParser = CType(parsing_data.Item(key), DebtPlus.Configuration.RegexParser)

                                ' Determine if the regular expression for this type matches the line.
                                Dim match_expression As Match
                                Dim rx As Regex = expr.rx
                                match_expression = rx.Match(source_line)

                                ' If the line matches the hash string then process the hash line
                                If match_expression.Success Then
                                    valid_line = False
                                    process_line(cn, txn, expr.subtype, match_expression, "R")
                                    Exit For
                                End If
                            Next key
                        Loop

                        ' Close the batch
                        reconcile_batch_close(cn, txn)

                        ' Commit the changes to the database
                        txn.Commit()
                    End Using
                End Using

                ' Display the reconciliation item report for this batch
                print_report()

            Catch ex As Exception
                DebtPlus.Data.Forms.MessageBox.Show(ex.ToString(), ap.RM.GetString("error_dialog_title"), MessageBoxButtons.OK, MessageBoxIcon.Error)
            End Try
        End Sub

        Private Sub print_report()
            Dim rpt As DebtPlus.Reports.Reconcile.Detail.CheckReconcileDetailListReport = New DebtPlus.Reports.Reconcile.Detail.CheckReconcileDetailListReport()
            rpt.Parameter_BatchID = ap.Batch
            rpt.RunReportInSeparateThread()
        End Sub

        Private Function DefaultCheckBank(ByVal cn As SqlConnection, ByVal txn As SqlTransaction) As Int32
            Static LastBank As Int32 = -1
            If LastBank < 0 Then
                Using cmd As SqlCommand = New SqlCommand()
                    cmd.Connection = cn
                    cmd.Transaction = txn
                    cmd.CommandText = "SELECT MIN(bank) FROM banks WHERE [type] = 'C' AND [Default]=1 AND [ActiveFlag]=1 UNION ALL SELECT MIN(bank) FROM banks WHERE [type]='C'"
                    cmd.CommandType = CommandType.Text
                    Dim objAnswer As Object = cmd.ExecuteScalar
                    If objAnswer IsNot Nothing AndAlso objAnswer IsNot DBNull.Value Then LastBank = Convert.ToInt32(objAnswer)
                End Using

                ' From the bank, find the name
                Using cmd As SqlCommand = New SqlCommand
                    cmd.Connection = cn
                    cmd.Transaction = txn
                    cmd.CommandText = "SELECT description FROM banks WITH (NOLOCK) WHERE bank=@bank"
                    cmd.Parameters.Add("@bank", SqlDbType.Int).Value = LastBank

                    Dim objName As Object = cmd.ExecuteScalar()
                    Dim BankName As String = String.Empty
                    If objName IsNot Nothing AndAlso objName IsNot DBNull.Value Then BankName = Convert.ToString(objName)
                    Set_BankName(BankName)
                End Using
            End If

            Return LastBank
        End Function

        Private Function FindBankFromAccount(ByVal cn As SqlConnection, ByVal txn As SqlTransaction, ByVal ABA As String, ByVal AccountNumber As String) As Int32
            Static LastAccountNumber As String = String.Empty
            Static LastBank As Int32 = -1

            ' If the bank account is different then lookup the value
            If LastAccountNumber <> AccountNumber Then LastBank = -1
            If LastBank < 0 Then
                LastAccountNumber = AccountNumber

                Using cmd As SqlCommand = New SqlCommand
                    With cmd
                        .Connection = cn
                        .Transaction = txn
                        .CommandText = "xpr_FindBankFromAccount"
                        .CommandType = CommandType.StoredProcedure
                        .Parameters.Add("RETURN", SqlDbType.Int).Direction = ParameterDirection.ReturnValue
                        .Parameters.Add("@ABA", SqlDbType.VarChar, 10).Value = ABA
                        .Parameters.Add("@AccountNumber", SqlDbType.VarChar, 80).Value = AccountNumber
                        .ExecuteNonQuery()
                        LastBank = Convert.ToInt32(.Parameters(0).Value)
                    End With
                End Using

                ' From the bank, find the name
                Using cmd As SqlCommand = New SqlCommand
                    With cmd
                        .Connection = cn
                        .Transaction = txn
                        .CommandText = "SELECT description FROM banks WITH (NOLOCK) WHERE bank=@bank"
                        .Parameters.Add("@bank", SqlDbType.Int).Value = LastBank

                        Dim objName As Object = .ExecuteScalar()
                        Dim BankName As String = String.Empty
                        If objName IsNot Nothing AndAlso objName IsNot DBNull.Value Then BankName = Convert.ToString(objName)
                        Set_BankName(BankName)
                    End With
                End Using
            End If

            Return LastBank
        End Function

        Private Sub process_line(ByVal cn As SqlConnection, ByVal txn As SqlTransaction, ByVal reconcile_subtype As String, ByVal match As Match, ByVal reconcile_cleared As String)

            ' Locate the fields
            Dim reconcile_aba As String = match.Groups("aba").Value
            Dim reconcile_account As String = match.Groups("account").Value
            Dim reconcile_date As String = match.Groups("date").Value
            Dim reconcile_month As String = match.Groups("month").Value
            Dim reconcile_day As String = match.Groups("day").Value
            Dim reconcile_year As String = match.Groups("year").Value
            Dim reconcile_amount As String = match.Groups("amount").Value
            Dim reconcile_cents As String = match.Groups("cents").Value
            Dim reconcile_checknum As String = match.Groups("check").Value
            Dim reconcile_reference As String = match.Groups("reference").Value

            Dim transaction_date As Date
            Dim transaction_amount As Decimal = 0D
            Dim ErrorText As Object = DBNull.Value
            Dim CheckNum As System.Int64

            ' Convert the input string to a suitable numerical value
            If reconcile_checknum Is Nothing OrElse Not Int64.TryParse(reconcile_checknum.Trim(), Globalization.NumberStyles.None, System.Globalization.CultureInfo.InvariantCulture, CheckNum) Then
                CheckNum = 0
            End If

            ' If there is an account then find the bank with this account
            Dim Bank As Int32 = ap.Bank
            If Bank < 0 AndAlso reconcile_account <> String.Empty Then
                Bank = FindBankFromAccount(cn, txn, reconcile_aba, reconcile_account)
            End If

            If Bank < 0 Then
                Bank = DefaultCheckBank(cn, txn)
            End If

            transaction_date = Date.MinValue
            Try
                ' Determine the appropriate date
                If reconcile_date <> String.Empty Then
                    If Not Date.TryParse(reconcile_date, transaction_date) Then
                        ErrorText = ap.RM.GetString("error_text_invalid_date")
                    End If
                Else
                    If reconcile_month <> String.Empty AndAlso reconcile_day <> String.Empty AndAlso reconcile_year <> String.Empty Then
                        Dim int_year As Int32 = 0
                        Dim int_month As Int32 = 0
                        Dim int_day As Int32 = 0

                        If Int32.TryParse(reconcile_year, int_year) AndAlso
                           Int32.TryParse(reconcile_month, int_month) AndAlso
                           Int32.TryParse(reconcile_day, int_day) Then

                            If int_year < 100 Then
                                If int_year < 80 Then int_year += 100
                                int_year += 1900
                            End If

                            transaction_date = New Date(int_year, int_month, int_day)
                        End If
                    End If
                End If

            Catch ex As Exception
                ErrorText = ap.RM.GetString("error_text_invalid_date")
            End Try

            Try
                ' Find the dollar amount of the transaction
                If reconcile_cents <> String.Empty Then
                    Dim temp_string As New StringBuilder(reconcile_cents)
                    temp_string.Insert(temp_string.Length - 2, ".", 1)
                    reconcile_amount = temp_string.ToString()
                End If

                If Not Decimal.TryParse(reconcile_amount, transaction_amount) Then
                    ErrorText = ap.RM.GetString("error_text_invalid_amount")
                Else
                    transaction_amount = Math.Truncate(transaction_amount, 2)
                End If

            Catch ex As Exception
                ErrorText = ap.RM.GetString("error_text_invalid_amount")
            End Try

            ' Update the statistics
            batch_amount += transaction_amount
            batch_count += 1

            ' Refresh the display
            Set_Amount(String.Format("{0:c2}", transaction_amount))
            Set_checknum(CheckNum.ToString())
            Set_Reconciling_Item(batch_count.ToString())
            Set_Transaction_Type(reconcile_subtype)
            Set_Date(transaction_date.ToShortDateString)

            ' Record the transaction
            reconcile_details_write(cn, txn, ap.Batch, CheckNum, reconcile_cleared, transaction_date, transaction_amount, reconcile_subtype, reconcile_reference, Bank, ErrorText)
        End Sub

        Private Function reconcile_details_write(ByVal cn As SqlConnection, ByVal txn As SqlTransaction, ByVal Batch As Int32, ByVal CheckNum As System.Int64, ByVal Cleared As String, ByVal ReconDate As Date, ByVal ReconAmount As Decimal, ByVal tran_type As String, ByVal Comments As String, ByVal Bank As Int32, ByVal ErrorMessage As Object) As Int32
            Dim answer As Int32 = -1

            ' Complete the issue by inserting the transaction into the detail items.
            Using cmd As SqlCommand = New SqlCommand
                With cmd
                    .CommandText = "xpr_insert_recon_detail"
                    .CommandType = CommandType.StoredProcedure
                    .Connection = cn
                    .Transaction = txn
                    .Parameters.Add("@RETURN_VALUE", SqlDbType.Int).Direction = ParameterDirection.ReturnValue
                    .Parameters.Add("@Batch", SqlDbType.Int).Value = Batch
                    .Parameters.Add("@Bank", SqlDbType.Int).Value = Bank
                    .Parameters.Add("@CheckNum", SqlDbType.BigInt, 0).Value = CheckNum
                    .Parameters.Add("@Cleared", SqlDbType.VarChar, 10).Value = Cleared
                    .Parameters.Add("@ReconDate", SqlDbType.DateTime).Value = ReconDate
                    .Parameters.Add("@ReconAmount", SqlDbType.Money).Value = ReconAmount
                    .Parameters.Add("@Tran_Type", SqlDbType.VarChar, 10).Value = tran_type
                    .Parameters.Add("@Comments", SqlDbType.VarChar, 80).Value = Comments
                    .Parameters.Add("@ErrorMessage", SqlDbType.VarChar, 80).Value = ErrorMessage

                    .ExecuteNonQuery()

                    answer = Convert.ToInt32(.Parameters(0).Value)
                End With
            End Using

            Return answer
        End Function

        Private Sub reconcile_batch_close(ByVal cn As SqlConnection, ByVal txn As SqlTransaction)
            Dim answer As Int32 = -1

            Try
                Using cmd As SqlCommand = New SqlCommand
                    With cmd
                        .CommandText = "xpr_Recon_Close"
                        .CommandType = CommandType.StoredProcedure
                        .Connection = cn
                        .Transaction = txn

                        SqlCommandBuilder.DeriveParameters(cmd)

                        .Parameters(1).Value = ap.Batch
                        .ExecuteNonQuery()

                        answer = Convert.ToInt32(.Parameters(0).Value)
                    End With
                End Using

            Catch ex As SqlException
                DebtPlus.Svc.Common.ErrorHandling.HandleErrors(ex)  ' don't trap if there is an error here.
            End Try
        End Sub

        Private Sub bt_RunWorkerCompleted(ByVal sender As Object, ByVal e As RunWorkerCompletedEventArgs)
            Button_Cancel.Text = "Close"
            Label7.Text = "The operation is complete. Click on the CLOSE button to dismiss this dialog."

            ' Clear the label fields
            Set_Amount(String.Empty)
            Set_checknum(String.Empty)
            Set_Date(String.Empty)
            Set_Reconciling_Item(String.Empty)
            Set_Transaction_Type(String.Empty)
            Set_BankName(String.Empty)
        End Sub
    End Class

    'Public Class Form_Reconcile_Reports
    '    Public report_batch_id As System.Int32

    '    Sub New(ByVal reconcile_batch_id As System.Int32)
    '        MyBase.New()
    '        Me.report_batch_id = reconcile_batch_id
    '    End Sub

    '    Public Sub print_reconcile_batch_report()
    '        Dim rpt As DebtPlus.Interfaces.Reports.IReports = DebtPlus.Reports.LoadReport("DebtPlus.Reports.Reconcile.Detail")
    '        If rpt IsNot Nothing Then
    '            rpt.SetReportParameter("BatchID", report_batch_id)
    '            If rpt.RequestReportParameters = DialogResult.OK Then
    '                rpt.DisplayPreviewDialog()
    '            End If
    '            CType(rpt, IDisposable).Dispose()
    '        End If
    '    End Sub
    'End Class

    Module Tables
        Public Function BanksDepositView(ByRef tbl As DataTable) As DataView
            Dim vue As New DataView(tbl, "type in ('D','C')", "description", DataViewRowState.CurrentRows)
            vue.ApplyDefaultSort = True
            Return vue
        End Function
    End Module
End Namespace
