#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}

#End Region

Option Compare Binary
Option Explicit On
Option Strict On

Imports System.Data.SqlClient
Imports DebtPlus.Data.Forms
Imports DebtPlus.Interfaces.Desktop

Namespace Check.Reconcile.Delete

    Public Class Mainline
        Implements IDesktopMainline

        Public Sub OldAppMain(ByVal args() As String) Implements IDesktopMainline.OldAppMain
            ' This processing does modal dialogs from the start. We need to run it as a new thread so that the menu will
            ' continue to operate.
            Dim thrd As New System.Threading.Thread(New System.Threading.ParameterizedThreadStart(AddressOf UpdateProcedure))
            thrd.SetApartmentState(Threading.ApartmentState.STA)
            thrd.IsBackground = False
            thrd.Name = "ReconcileDelete"
            thrd.Start(args)
        End Sub

        Private Sub UpdateProcedure(ByVal obj As Object)
            Dim args() As String = DirectCast(obj, String())

            Dim ap As New ReconcileArgParser()
            If ap.Parse(args) Then
                IssueCommand(ap)
            End If
        End Sub

        Private Sub IssueCommand(ByVal ap As ReconcileArgParser)
            Dim cn As SqlConnection = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
            Try
                cn.Open()
                Using cmd As SqlCommand = New SqlCommand()
                    With cmd
                        .Connection = cn
                        .CommandText = "xpr_recon_batch_delete"
                        .CommandType = CommandType.StoredProcedure
                        SqlCommandBuilder.DeriveParameters(cmd)
                        .CommandTimeout = 0
                        .Parameters(1).Value = ap.Batch
                        .ExecuteNonQuery()
                    End With
                End Using

                MessageBox.Show("The batch has been Deleted.", "Processing Completed")

            Catch ex As Exception
                Using gdr As New Repository.GetDataResult()
                    gdr.HandleException(ex)
                    DebtPlus.UI.Common.ErrorHandling.HandleErrors(gdr, "Error Deleting batch")
                End Using

            Finally
                If cn IsNot Nothing Then cn.Dispose()
            End Try
        End Sub
    End Class

End Namespace
