Option Compare Binary
Option Explicit On
Option Strict On

Imports DebtPlus.UI.FormLib.Check
Imports System.Windows.Forms

Namespace Check.Reconcile.Post
    Friend Class ReconcileArgParser
        Inherits DebtPlus.Utils.ArgParserBase

        Private privateBatch As System.Int32 = -1
        Public ReadOnly Property Batch() As System.Int32
            Get
                Return privateBatch
            End Get
        End Property

        ''' <summary>
        ''' Create an instance of our class
        ''' </summary>
        Public Sub New()
            MyBase.New(New String() {})
        End Sub

        ''' <summary>
        ''' Generate the command usage information
        ''' </summary>
        Protected Overrides Sub OnUsage(ByVal errorInfo As String)
            ' deprecated
        End Sub

        Protected Overrides Function OnDoneParse() As SwitchStatus
            Dim ss As SwitchStatus = DebtPlus.Utils.ArgParserBase.SwitchStatus.NoError

            ' Request the batch ID
            Using frm As New ReconcileBatchForm
                With frm
                    .ShowNewButton = False
                    Dim answer As System.Windows.Forms.DialogResult = .ShowDialog()
                    privateBatch = .BatchID
                    If answer <> DialogResult.OK Then
                        ss = DebtPlus.Utils.ArgParserBase.SwitchStatus.Quit
                    End If
                End With
            End Using

            Return ss
        End Function
    End Class
End Namespace
