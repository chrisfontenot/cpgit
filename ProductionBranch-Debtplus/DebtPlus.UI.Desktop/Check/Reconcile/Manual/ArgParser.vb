Option Compare Binary
Option Explicit On 
Option Strict On

Imports DebtPlus.UI.FormLib.Check
Imports System.Windows.Forms

Namespace Check.Reconcile.Manual
    Friend Class ArgParser
        Inherits DebtPlus.Utils.ArgParserBase

        ''' <summary>
        ''' Batch ID
        ''' </summary>
        Private _batch As System.Int32 = -1
        Public ReadOnly Property batch() As System.Int32
            Get
                Return _batch
            End Get
        End Property

        ''' <summary>
        ''' Create an instance of our class
        ''' </summary>
        Public Sub New()
            MyBase.New(New String() {"b"})
        End Sub

        ''' <summary>
        ''' Process the switch settings
        ''' </summary>
        Protected Overrides Function OnSwitch(ByVal switchSymbol As String, ByVal switchValue As String) As SwitchStatus
            Dim ss As SwitchStatus = DebtPlus.Utils.ArgParserBase.SwitchStatus.NoError
            Select Case switchSymbol
                Case "b"
                    Dim TempBatch As System.Int32 = 0
                    If Int32.TryParse(switchValue, TempBatch) Then
                        _batch = TempBatch
                    End If
                Case "?"
                    ss = DebtPlus.Utils.ArgParserBase.SwitchStatus.ShowUsage
                Case Else
                    ss = DebtPlus.Utils.ArgParserBase.SwitchStatus.YesError
            End Select
            Return ss
        End Function

        ''' <summary>
        ''' After processing, look for the batch ID
        ''' </summary>
        Protected Overrides Function OnDoneParse() As SwitchStatus
            Dim ss As SwitchStatus = DebtPlus.Utils.ArgParserBase.SwitchStatus.NoError

            ' If there is no batch then ask the user for the batch ID
            If batch <= 0 Then
                Dim answer As System.Windows.Forms.DialogResult

                ' Find the batch to be processed.
                Using frm As New ReconcileBatchForm()
                    With frm
                        answer = .ShowDialog()
                        If answer = DialogResult.OK Then
                            _batch = .BatchID
                        End If
                    End With
                End Using
            End If

            ' If the batch is cancelled then return an error to flush the execution
            If batch < 0 Then ss = DebtPlus.Utils.ArgParserBase.SwitchStatus.YesError
            Return ss
        End Function

        ''' <summary>
        ''' Generate the command usage information
        ''' </summary>
        Protected Overrides Sub OnUsage(ByVal errorInfo As String)
            'AppendErrorLine("Usage: " + fname + " [-b#]")
            'AppendErrorLine("       -b : batch number to process for the inupt")
        End Sub
    End Class
End Namespace
