#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}
#End Region

Option Compare Binary
Option Explicit On 
Option Strict On

Imports DebtPlus.Interfaces.Desktop

Namespace Check.Reconcile.Manual

    Public Class Mainline
        Implements IDesktopMainline

        Public Sub OldAppMain(ByVal args() As String) Implements IDesktopMainline.OldAppMain
            ' This processing does modal dialogs from the start. We need to run it as a new thread so that the menu will
            ' continue to operate.
            Dim thrd As New System.Threading.Thread(New System.Threading.ParameterizedThreadStart(AddressOf UpdateProcedure))
            thrd.SetApartmentState(Threading.ApartmentState.STA)
            thrd.IsBackground = False
            thrd.Name = "ReconcileManual"
            thrd.Start(args)
        End Sub

        Private Sub UpdateProcedure(ByVal obj As Object)
            Dim args() As String = DirectCast(obj, String())
            Dim ap As New ArgParser
            If ap.Parse(args) AndAlso ap.batch > 0 Then
                Using mainForm As New ReconcileItemsForm(ap)
                    mainForm.ShowDialog()
                End Using
            End If
        End Sub
    End Class

End Namespace
