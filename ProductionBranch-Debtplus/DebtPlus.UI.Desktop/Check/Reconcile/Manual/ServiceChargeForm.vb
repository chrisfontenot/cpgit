#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}
#End Region

Option Compare Binary
Option Explicit On
Option Strict On
Namespace Check.Reconcile.Manual

    Friend Class ServiceChargeForm
        Inherits DebtPlus.Data.Forms.DebtPlusForm

        Public Sub New()
            MyBase.New()
            InitializeComponent()
            AddHandler Me.Load, AddressOf frmServiceCharge_Load
            AddHandler CalcEdit1.EditValueChanging, AddressOf CalcEdit1_EditValueChanging
            AddHandler CalcEdit1.Enter, AddressOf CalcEdit1_Enter
        End Sub

        Private _TransactionType As String = System.String.Empty
        Public Property TransactionType() As String
            Get
                Return _TransactionType
            End Get
            Set(ByVal Value As String)
                _TransactionType = Value
            End Set
        End Property

        Public ReadOnly Property Parameter_Date() As Date
            Get
                Return Convert.ToDateTime(DateEdit1.EditValue)
            End Get
        End Property

        Public ReadOnly Property Parameter_Amount() As Decimal
            Get
                Return Convert.ToDecimal(CalcEdit1.EditValue)
            End Get
        End Property

#Region "Windows Form Designer generated code "
        'Form overrides dispose to clean up the component list.
        Protected Overloads Overrides Sub Dispose(ByVal Disposing As Boolean)
            If Disposing Then
                If components IsNot Nothing Then
                    components.Dispose()
                End If
            End If
            MyBase.Dispose(Disposing)
        End Sub
        'Required by the Windows Form Designer
        Private components As System.ComponentModel.IContainer
        Public WithEvents _Label1_2 As DevExpress.XtraEditors.LabelControl
        Public WithEvents _Label1_1 As DevExpress.XtraEditors.LabelControl
        Public WithEvents _Label1_0 As DevExpress.XtraEditors.LabelControl
        'NOTE: The following procedure is required by the Windows Form Designer
        'It can be modified using the Windows Form Designer.
        'Do not modify it using the code editor.
        Friend WithEvents CalcEdit1 As DevExpress.XtraEditors.CalcEdit
        Friend WithEvents DateEdit1 As DevExpress.XtraEditors.DateEdit
        Friend WithEvents Button_OK As DevExpress.XtraEditors.SimpleButton
        Friend WithEvents Button_Cancel As DevExpress.XtraEditors.SimpleButton
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
            Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(ServiceChargeForm))
            Me._Label1_2 = New DevExpress.XtraEditors.LabelControl
            Me._Label1_1 = New DevExpress.XtraEditors.LabelControl
            Me._Label1_0 = New DevExpress.XtraEditors.LabelControl
            Me.CalcEdit1 = New DevExpress.XtraEditors.CalcEdit
            Me.DateEdit1 = New DevExpress.XtraEditors.DateEdit
            Me.Button_OK = New DevExpress.XtraEditors.SimpleButton
            Me.Button_Cancel = New DevExpress.XtraEditors.SimpleButton
            CType(Me.DxErrorProvider1, System.ComponentModel.ISupportInitialize).BeginInit()

            CType(Me.CalcEdit1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.DateEdit1.Properties.VistaTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.DateEdit1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.SuspendLayout()

            '
            '_Label1_2
            '
            Me._Label1_2.Appearance.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me._Label1_2.Appearance.ForeColor = System.Drawing.SystemColors.ControlText
            Me._Label1_2.Appearance.Options.UseFont = True
            Me._Label1_2.Appearance.Options.UseForeColor = True
            Me._Label1_2.Appearance.Options.UseTextOptions = True
            Me._Label1_2.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me._Label1_2.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center
            Me._Label1_2.Cursor = System.Windows.Forms.Cursors.Default
            Me._Label1_2.Location = New System.Drawing.Point(53, 96)
            Me._Label1_2.Name = "_Label1_2"
            Me._Label1_2.Size = New System.Drawing.Size(62, 14)

            Me._Label1_2.TabIndex = 3
            Me._Label1_2.Text = "Item Amount:"
            Me._Label1_2.ToolTipController = Me.ToolTipController1
            '
            '_Label1_1
            '
            Me._Label1_1.Appearance.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me._Label1_1.Appearance.ForeColor = System.Drawing.SystemColors.ControlText
            Me._Label1_1.Appearance.Options.UseFont = True
            Me._Label1_1.Appearance.Options.UseForeColor = True
            Me._Label1_1.Appearance.Options.UseTextOptions = True
            Me._Label1_1.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me._Label1_1.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center
            Me._Label1_1.Cursor = System.Windows.Forms.Cursors.Default
            Me._Label1_1.Location = New System.Drawing.Point(53, 72)
            Me._Label1_1.Name = "_Label1_1"
            Me._Label1_1.Size = New System.Drawing.Size(47, 14)

            Me._Label1_1.TabIndex = 1
            Me._Label1_1.Text = "Item Date:"
            Me._Label1_1.ToolTipController = Me.ToolTipController1
            '
            '_Label1_0
            '
            Me._Label1_0.Appearance.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me._Label1_0.Appearance.ForeColor = System.Drawing.SystemColors.ControlText
            Me._Label1_0.Appearance.Options.UseFont = True
            Me._Label1_0.Appearance.Options.UseForeColor = True
            Me._Label1_0.Appearance.Options.UseTextOptions = True
            Me._Label1_0.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
            Me._Label1_0.Appearance.TextOptions.Trimming = DevExpress.Utils.Trimming.None
            Me._Label1_0.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Top
            Me._Label1_0.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap
            Me._Label1_0.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
            Me._Label1_0.Cursor = System.Windows.Forms.Cursors.Default
            Me._Label1_0.Location = New System.Drawing.Point(8, 12)
            Me._Label1_0.Name = "_Label1_0"
            Me._Label1_0.Size = New System.Drawing.Size(278, 49)

            Me._Label1_0.TabIndex = 0
            Me._Label1_0.Text = "This will allow you to create a reconciled item into your database. Only service " & _
                                "charges, bank errors, or interest items may be created."
            Me._Label1_0.ToolTipController = Me.ToolTipController1
            '
            'CalcEdit1
            '
            Me.CalcEdit1.Location = New System.Drawing.Point(157, 96)
            Me.CalcEdit1.Name = "CalcEdit1"
            Me.CalcEdit1.Properties.Appearance.Options.UseTextOptions = True
            Me.CalcEdit1.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.CalcEdit1.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.CalcEdit1.Properties.DisplayFormat.FormatString = "c2"
            Me.CalcEdit1.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.CalcEdit1.Properties.EditFormat.FormatString = "d2"
            Me.CalcEdit1.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.CalcEdit1.Size = New System.Drawing.Size(88, 20)

            Me.CalcEdit1.TabIndex = 7
            Me.CalcEdit1.ToolTipController = Me.ToolTipController1
            '
            'DateEdit1
            '
            Me.DateEdit1.EditValue = New Date(2005, 10, 11, 0, 0, 0, 0)
            Me.DateEdit1.Location = New System.Drawing.Point(157, 72)
            Me.DateEdit1.Name = "DateEdit1"
            Me.DateEdit1.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.DateEdit1.Properties.VistaTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton})
            Me.DateEdit1.Size = New System.Drawing.Size(88, 20)

            Me.DateEdit1.TabIndex = 8
            Me.DateEdit1.ToolTipController = Me.ToolTipController1
            '
            'Button_OK
            '
            Me.Button_OK.DialogResult = System.Windows.Forms.DialogResult.OK
            Me.Button_OK.Location = New System.Drawing.Point(64, 136)
            Me.Button_OK.Name = "Button_OK"
            Me.Button_OK.Size = New System.Drawing.Size(75, 23)

            Me.Button_OK.TabIndex = 9
            Me.Button_OK.Text = "&OK"
            Me.Button_OK.ToolTipController = Me.ToolTipController1
            '
            'Button_Cancel
            '
            Me.Button_Cancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
            Me.Button_Cancel.Location = New System.Drawing.Point(160, 136)
            Me.Button_Cancel.Name = "Button_Cancel"
            Me.Button_Cancel.Size = New System.Drawing.Size(75, 23)

            Me.Button_Cancel.TabIndex = 10
            Me.Button_Cancel.Text = "&Cancel"
            Me.Button_Cancel.ToolTipController = Me.ToolTipController1
            '
            'ServiceChargeForm
            '
            Me.AcceptButton = Me.Button_OK
            Me.Appearance.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.Appearance.Options.UseFont = True
            Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
            Me.CancelButton = Me.Button_Cancel
            Me.ClientSize = New System.Drawing.Size(298, 176)
            Me.Controls.Add(Me.Button_Cancel)
            Me.Controls.Add(Me.Button_OK)
            Me.Controls.Add(Me.DateEdit1)
            Me.Controls.Add(Me.CalcEdit1)
            Me.Controls.Add(Me._Label1_2)
            Me.Controls.Add(Me._Label1_1)
            Me.Controls.Add(Me._Label1_0)
            Me.Cursor = System.Windows.Forms.Cursors.Default
            Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
            Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
            Me.Location = New System.Drawing.Point(3, 22)
            Me.MaximizeBox = False
            Me.MinimizeBox = False
            Me.Name = "ServiceChargeForm"
            Me.RightToLeft = System.Windows.Forms.RightToLeft.No
            Me.ShowInTaskbar = False
            Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
            Me.ToolTipController1.SetSuperTip(Me, Nothing)
            Me.Text = "Create New Non-Check Item"
            CType(Me.DxErrorProvider1, System.ComponentModel.ISupportInitialize).EndInit()

            CType(Me.CalcEdit1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.DateEdit1.Properties.VistaTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.DateEdit1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            Me.ResumeLayout(False)
            Me.PerformLayout()

        End Sub
#End Region

        Private Sub frmServiceCharge_Load(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs)
            Button_OK.Enabled = False
            Button_Cancel.Enabled = True
            DateEdit1.EditValue = Date.Now
            CalcEdit1.EditValue = 0D
        End Sub

        Private Sub CalcEdit1_EditValueChanging(ByVal sender As Object, ByVal e As DevExpress.XtraEditors.Controls.ChangingEventArgs)
            Dim EditValue As Decimal = 0
            With CalcEdit1

                ' Retrieve the value from the input.
                If .Text <> System.String.Empty Then
                    EditValue = Convert.ToDecimal(e.NewValue)
                End If

                ' Do not allow the value to go negative unless this is a bank error transaction
                If EditValue < 0D AndAlso TransactionType <> "BE" Then
                    Button_OK.Enabled = False
                    e.Cancel = True
                    Beep()
                Else
                    Button_OK.Enabled = True
                End If
            End With
        End Sub

        Private Sub CalcEdit1_Enter(ByVal sender As Object, ByVal e As System.EventArgs)
            CalcEdit1.SelectAll()
        End Sub
    End Class
End Namespace