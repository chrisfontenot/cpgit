#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}

#End Region

Option Strict Off
Option Explicit On

Imports System.ComponentModel
Imports DevExpress.XtraGrid.Columns
Imports DebtPlus.Data.Forms
Imports DebtPlus.Utils
Imports DevExpress.XtraGrid.Views.Grid
Imports DevExpress.XtraBars
Imports System.Data.SqlClient
Imports DevExpress.XtraGrid.Views.Grid.ViewInfo
Imports DebtPlus.Reports
Imports DebtPlus.Interfaces.Reports
Imports System.Windows.Forms

Namespace Check.Reconcile.Manual
    Friend Class ReconcileItemsForm
        Inherits DebtPlusForm

        Private Const STR_RECONCILED As String = "R"
        Private Const STR_UNRECONCILED As String = " "
        Private Const STR_NOTFOUND As String = "NOT FOUND"
        Private Const STR_FOUND As String = "FOUND"

        Private UpdateTable As DataTable = Nothing
        Private RightClickItem As Int32
        Private ReconDate As Date = Now.Date
        Private ReconBatch As Int32
        Private NormalClose As Boolean
        Private bt_reader As New BackgroundWorker()
        Private ds As New DataSet("ds")

        Public Sub New()
            MyBase.New()
            InitializeComponent()
        End Sub

        Public Sub New(ByVal ap As ArgParser)
            MyClass.New()
            ReconBatch = ap.batch
            RegisterHandlers()
        End Sub

        Private Sub RegisterHandlers()
            AddHandler BarButtonItem_File_Exit.ItemClick, AddressOf BarButtonItem_File_Exit_ItemClick
            AddHandler FormClosing, AddressOf ReconcileItemsForm_FormClosing
            AddHandler GotFocus, AddressOf ReconcileItemsForm_GotFocus
            AddHandler Load, AddressOf frmItems_Load
            AddHandler bt_reader.DoWork, AddressOf bt_reader_DoWork
            AddHandler bt_reader.RunWorkerCompleted, AddressOf bt_reader_RunWorkerCompleted
            AddHandler GridView1.Click, AddressOf GridView1_Click
            AddHandler mnuChangeDate.Click, AddressOf mnuChangeDate_Click
            AddHandler mnuChangeItem.Click, AddressOf mnuChangeItem_Click
            AddHandler mnuChangeStatus.Click, AddressOf mnuChangeStatus_Click
            AddHandler mnuCreateItem.Click, AddressOf mnuCreateItem_Click
            AddHandler BarButtonItem_File_Print.ItemClick, AddressOf BarButtonItem_File_Print_ItemClick
            AddHandler Button_OK.Click, AddressOf Button_OK_Click
            AddHandler ContextMenu1.Popup, AddressOf ContextMenu1_Popup
            AddHandler BarStaticItem_recon_date.ItemDoubleClick, AddressOf BarStaticItem_recon_date_ItemDoubleClick
            AddHandler TabControl1.SelectedIndexChanged, AddressOf TabControl1_SelectedIndexChanged

            If UpdateTable IsNot Nothing Then
                AddHandler UpdateTable.RowChanged, AddressOf UpdateTable_RowChanged
            End If
        End Sub

        Private Sub UnRegisterHandlers()
            RemoveHandler BarButtonItem_File_Exit.ItemClick, AddressOf BarButtonItem_File_Exit_ItemClick
            RemoveHandler FormClosing, AddressOf ReconcileItemsForm_FormClosing
            RemoveHandler GotFocus, AddressOf ReconcileItemsForm_GotFocus
            RemoveHandler Load, AddressOf frmItems_Load
            RemoveHandler bt_reader.DoWork, AddressOf bt_reader_DoWork
            RemoveHandler bt_reader.RunWorkerCompleted, AddressOf bt_reader_RunWorkerCompleted
            RemoveHandler GridView1.Click, AddressOf GridView1_Click
            RemoveHandler mnuChangeDate.Click, AddressOf mnuChangeDate_Click
            RemoveHandler mnuChangeItem.Click, AddressOf mnuChangeItem_Click
            RemoveHandler mnuChangeStatus.Click, AddressOf mnuChangeStatus_Click
            RemoveHandler mnuCreateItem.Click, AddressOf mnuCreateItem_Click
            RemoveHandler BarButtonItem_File_Print.ItemClick, AddressOf BarButtonItem_File_Print_ItemClick
            RemoveHandler Button_OK.Click, AddressOf Button_OK_Click
            RemoveHandler ContextMenu1.Popup, AddressOf ContextMenu1_Popup
            RemoveHandler BarStaticItem_recon_date.ItemDoubleClick, AddressOf BarStaticItem_recon_date_ItemDoubleClick
            RemoveHandler TabControl1.SelectedIndexChanged, AddressOf TabControl1_SelectedIndexChanged

            If UpdateTable IsNot Nothing Then
                RemoveHandler UpdateTable.RowChanged, AddressOf UpdateTable_RowChanged
            End If
        End Sub

#Region "Windows Form Designer generated code "

        'Form overrides dispose to clean up the component list.
        Protected Overloads Overrides Sub Dispose(ByVal Disposing As Boolean)
            If Disposing Then
                If components IsNot Nothing Then
                    components.Dispose()
                End If
            End If
            MyBase.Dispose(Disposing)
        End Sub

        'Required by the Windows Form Designer
        Private components As IContainer

        'NOTE: The following procedure is required by the Windows Form Designer
        'It can be modified using the Windows Form Designer.
        'Do not modify it using the code editor.
        Friend WithEvents ContextMenu1 As System.Windows.Forms.ContextMenu
        Friend WithEvents MenuItem1 As System.Windows.Forms.MenuItem
        Public WithEvents mnuChangeItem As System.Windows.Forms.MenuItem
        Public WithEvents mnuChangeDate As System.Windows.Forms.MenuItem
        Public WithEvents mnuCreateItem As System.Windows.Forms.MenuItem
        Public WithEvents mnuChangeStatus As System.Windows.Forms.MenuItem
        Friend WithEvents barManager1 As DevExpress.XtraBars.BarManager
        Friend WithEvents Bar2 As DevExpress.XtraBars.Bar
        Friend WithEvents Bar3 As DevExpress.XtraBars.Bar
        Friend WithEvents barDockControlTop As DevExpress.XtraBars.BarDockControl
        Friend WithEvents barDockControlBottom As DevExpress.XtraBars.BarDockControl
        Friend WithEvents barDockControlLeft As DevExpress.XtraBars.BarDockControl
        Friend WithEvents barDockControlRight As DevExpress.XtraBars.BarDockControl
        Friend WithEvents PanelControl1 As DevExpress.XtraEditors.PanelControl
        Friend WithEvents GridControl1 As DevExpress.XtraGrid.GridControl
        Friend WithEvents GridView1 As GridView
        Friend WithEvents GridColumn_Item As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents GridColumn_Date As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents GridColumn_ReconDate As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents GridColumn_Amount As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents GridColumn_Payee As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents GridColumn_Status As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents GridColumn_TrustRegister As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents GridColumn_ReconDetail As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents Button_OK As DevExpress.XtraEditors.SimpleButton
        Friend WithEvents TabControl1 As System.Windows.Forms.TabControl
        Friend WithEvents TabPage1 As System.Windows.Forms.TabPage
        Friend WithEvents TabPage2 As System.Windows.Forms.TabPage
        Friend WithEvents TabPage3 As System.Windows.Forms.TabPage
        Friend WithEvents TabPage4 As System.Windows.Forms.TabPage
        Friend WithEvents TabPage5 As System.Windows.Forms.TabPage
        Friend WithEvents TabPage6 As System.Windows.Forms.TabPage
        Friend WithEvents TabPage7 As System.Windows.Forms.TabPage
        Friend WithEvents BarStaticItem_status As DevExpress.XtraBars.BarStaticItem
        Friend WithEvents BarStaticItem_recon_count As DevExpress.XtraBars.BarStaticItem
        Friend WithEvents BarStaticItem_recon_amount As DevExpress.XtraBars.BarStaticItem
        Friend WithEvents BarStaticItem_item_count As DevExpress.XtraBars.BarStaticItem
        Friend WithEvents BarStaticItem_recon_date As DevExpress.XtraBars.BarStaticItem
        Friend WithEvents BarSubItem1 As DevExpress.XtraBars.BarSubItem
        Friend WithEvents BarButtonItem_File_Print As DevExpress.XtraBars.BarButtonItem
        Friend WithEvents BarButtonItem_File_Exit As DevExpress.XtraBars.BarButtonItem
        Friend WithEvents TabPage8 As System.Windows.Forms.TabPage

        <System.Diagnostics.DebuggerStepThrough()>
        Private Sub InitializeComponent()
            Me.components = New System.ComponentModel.Container
            Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(ReconcileItemsForm))
            Me.mnuChangeItem = New System.Windows.Forms.MenuItem
            Me.mnuChangeDate = New System.Windows.Forms.MenuItem
            Me.mnuCreateItem = New System.Windows.Forms.MenuItem
            Me.mnuChangeStatus = New System.Windows.Forms.MenuItem
            Me.ContextMenu1 = New System.Windows.Forms.ContextMenu
            Me.MenuItem1 = New System.Windows.Forms.MenuItem
            Me.barManager1 = New DevExpress.XtraBars.BarManager(Me.components)
            Me.Bar2 = New DevExpress.XtraBars.Bar
            Me.BarSubItem1 = New DevExpress.XtraBars.BarSubItem
            Me.BarButtonItem_File_Print = New DevExpress.XtraBars.BarButtonItem
            Me.BarButtonItem_File_Exit = New DevExpress.XtraBars.BarButtonItem
            Me.Bar3 = New DevExpress.XtraBars.Bar
            Me.BarStaticItem_status = New DevExpress.XtraBars.BarStaticItem
            Me.BarStaticItem_recon_count = New DevExpress.XtraBars.BarStaticItem
            Me.BarStaticItem_recon_amount = New DevExpress.XtraBars.BarStaticItem
            Me.BarStaticItem_item_count = New DevExpress.XtraBars.BarStaticItem
            Me.BarStaticItem_recon_date = New DevExpress.XtraBars.BarStaticItem
            Me.barDockControlTop = New DevExpress.XtraBars.BarDockControl
            Me.barDockControlBottom = New DevExpress.XtraBars.BarDockControl
            Me.barDockControlLeft = New DevExpress.XtraBars.BarDockControl
            Me.barDockControlRight = New DevExpress.XtraBars.BarDockControl
            Me.PanelControl1 = New DevExpress.XtraEditors.PanelControl
            Me.GridControl1 = New DevExpress.XtraGrid.GridControl
            Me.GridView1 = New DevExpress.XtraGrid.Views.Grid.GridView
            Me.GridColumn_Item = New DevExpress.XtraGrid.Columns.GridColumn
            Me.GridColumn_Date = New DevExpress.XtraGrid.Columns.GridColumn
            Me.GridColumn_ReconDate = New DevExpress.XtraGrid.Columns.GridColumn
            Me.GridColumn_Amount = New DevExpress.XtraGrid.Columns.GridColumn
            Me.GridColumn_Payee = New DevExpress.XtraGrid.Columns.GridColumn
            Me.GridColumn_Status = New DevExpress.XtraGrid.Columns.GridColumn
            Me.GridColumn_TrustRegister = New DevExpress.XtraGrid.Columns.GridColumn
            Me.GridColumn_ReconDetail = New DevExpress.XtraGrid.Columns.GridColumn
            Me.Button_OK = New DevExpress.XtraEditors.SimpleButton
            Me.TabControl1 = New System.Windows.Forms.TabControl
            Me.TabPage1 = New System.Windows.Forms.TabPage
            Me.TabPage2 = New System.Windows.Forms.TabPage
            Me.TabPage3 = New System.Windows.Forms.TabPage
            Me.TabPage4 = New System.Windows.Forms.TabPage
            Me.TabPage5 = New System.Windows.Forms.TabPage
            Me.TabPage6 = New System.Windows.Forms.TabPage
            Me.TabPage7 = New System.Windows.Forms.TabPage
            Me.TabPage8 = New System.Windows.Forms.TabPage
            CType(Me.DxErrorProvider1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.barManager1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.PanelControl1, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.PanelControl1.SuspendLayout()
            CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.GridView1, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.TabControl1.SuspendLayout()
            Me.SuspendLayout()
            '
            'mnuChangeItem
            '
            Me.mnuChangeItem.Index = 0
            Me.mnuChangeItem.Text = "&Reconcile Selected Item(s)"
            '
            'mnuChangeDate
            '
            Me.mnuChangeDate.Index = 1
            Me.mnuChangeDate.Text = "Change Reconcile &Date"
            '
            'mnuCreateItem
            '
            Me.mnuCreateItem.Index = 2
            Me.mnuCreateItem.Text = "&New Item"
            '
            'mnuChangeStatus
            '
            Me.mnuChangeStatus.Index = 3
            Me.mnuChangeStatus.Text = "Change Status to FOUND"
            '
            'ContextMenu1
            '
            Me.ContextMenu1.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.mnuChangeItem, Me.mnuChangeDate, Me.mnuCreateItem, Me.mnuChangeStatus})
            '
            'MenuItem1
            '
            Me.MenuItem1.Index = -1
            Me.MenuItem1.Text = ""
            '
            'barManager1
            '
            Me.barManager1.Bars.AddRange(New DevExpress.XtraBars.Bar() {Me.Bar2, Me.Bar3})
            Me.barManager1.DockControls.Add(Me.barDockControlTop)
            Me.barManager1.DockControls.Add(Me.barDockControlBottom)
            Me.barManager1.DockControls.Add(Me.barDockControlLeft)
            Me.barManager1.DockControls.Add(Me.barDockControlRight)
            Me.barManager1.Form = Me
            Me.barManager1.Items.AddRange(New DevExpress.XtraBars.BarItem() {Me.BarStaticItem_status, Me.BarStaticItem_recon_count, Me.BarStaticItem_recon_amount, Me.BarStaticItem_item_count, Me.BarStaticItem_recon_date, Me.BarSubItem1, Me.BarButtonItem_File_Print, Me.BarButtonItem_File_Exit})
            Me.barManager1.MainMenu = Me.Bar2
            Me.barManager1.MaxItemId = 8
            Me.barManager1.StatusBar = Me.Bar3
            '
            'Bar2
            '
            Me.Bar2.BarName = "Main menu"
            Me.Bar2.DockCol = 0
            Me.Bar2.DockRow = 0
            Me.Bar2.DockStyle = DevExpress.XtraBars.BarDockStyle.Top
            Me.Bar2.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.BarSubItem1)})
            Me.Bar2.OptionsBar.MultiLine = True
            Me.Bar2.OptionsBar.UseWholeRow = True
            Me.Bar2.Text = "Main menu"
            '
            'BarSubItem1
            '
            Me.BarSubItem1.Caption = "&File"
            Me.BarSubItem1.Id = 5
            Me.BarSubItem1.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.BarButtonItem_File_Print), New DevExpress.XtraBars.LinkPersistInfo(Me.BarButtonItem_File_Exit)})
            Me.BarSubItem1.Name = "BarSubItem1"
            '
            'BarButtonItem_File_Print
            '
            Me.BarButtonItem_File_Print.Caption = "&Print..."
            Me.BarButtonItem_File_Print.Id = 6
            Me.BarButtonItem_File_Print.Name = "BarButtonItem_File_Print"
            '
            'BarButtonItem_File_Exit
            '
            Me.BarButtonItem_File_Exit.Caption = "&Exit"
            Me.BarButtonItem_File_Exit.Id = 7
            Me.BarButtonItem_File_Exit.Name = "BarButtonItem_File_Exit"
            '
            'Bar3
            '
            Me.Bar3.BarName = "Status bar"
            Me.Bar3.CanDockStyle = DevExpress.XtraBars.BarCanDockStyle.Bottom
            Me.Bar3.DockCol = 0
            Me.Bar3.DockRow = 0
            Me.Bar3.DockStyle = DevExpress.XtraBars.BarDockStyle.Bottom
            Me.Bar3.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.BarStaticItem_status), New DevExpress.XtraBars.LinkPersistInfo(Me.BarStaticItem_recon_count), New DevExpress.XtraBars.LinkPersistInfo(Me.BarStaticItem_recon_amount), New DevExpress.XtraBars.LinkPersistInfo(Me.BarStaticItem_item_count), New DevExpress.XtraBars.LinkPersistInfo(Me.BarStaticItem_recon_date)})
            Me.Bar3.OptionsBar.AllowQuickCustomization = False
            Me.Bar3.OptionsBar.DrawDragBorder = False
            Me.Bar3.OptionsBar.DrawSizeGrip = True
            Me.Bar3.OptionsBar.UseWholeRow = True
            Me.Bar3.Text = "Status bar"
            '
            'BarStaticItem_status
            '
            Me.BarStaticItem_status.AutoSize = DevExpress.XtraBars.BarStaticItemSize.Spring
            Me.BarStaticItem_status.Id = 0
            Me.BarStaticItem_status.Name = "BarStaticItem_status"
            Me.BarStaticItem_status.TextAlignment = System.Drawing.StringAlignment.Near
            Me.BarStaticItem_status.Width = 32
            '
            'BarStaticItem_recon_count
            '
            Me.BarStaticItem_recon_count.Caption = "Reconciled 0 items"
            Me.BarStaticItem_recon_count.Id = 1
            Me.BarStaticItem_recon_count.Name = "BarStaticItem_recon_count"
            Me.BarStaticItem_recon_count.TextAlignment = System.Drawing.StringAlignment.Near
            '
            'BarStaticItem_recon_amount
            '
            Me.BarStaticItem_recon_amount.Caption = "for $0.00"
            Me.BarStaticItem_recon_amount.Id = 2
            Me.BarStaticItem_recon_amount.Name = "BarStaticItem_recon_amount"
            Me.BarStaticItem_recon_amount.TextAlignment = System.Drawing.StringAlignment.Near
            '
            'BarStaticItem_item_count
            '
            Me.BarStaticItem_item_count.Caption = "0 items"
            Me.BarStaticItem_item_count.Id = 3
            Me.BarStaticItem_item_count.Name = "BarStaticItem_item_count"
            Me.BarStaticItem_item_count.TextAlignment = System.Drawing.StringAlignment.Near
            '
            'BarStaticItem_recon_date
            '
            Me.BarStaticItem_recon_date.Caption = "MM/dd/yyyy"
            Me.BarStaticItem_recon_date.Id = 4
            Me.BarStaticItem_recon_date.Name = "BarStaticItem_recon_date"
            Me.BarStaticItem_recon_date.TextAlignment = System.Drawing.StringAlignment.Near
            '
            'barDockControlTop
            '
            Me.barDockControlTop.CausesValidation = False
            Me.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top
            Me.barDockControlTop.Location = New System.Drawing.Point(0, 0)
            Me.barDockControlTop.Size = New System.Drawing.Size(588, 22)
            '
            'barDockControlBottom
            '
            Me.barDockControlBottom.CausesValidation = False
            Me.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom
            Me.barDockControlBottom.Location = New System.Drawing.Point(0, 582)
            Me.barDockControlBottom.Size = New System.Drawing.Size(588, 26)
            '
            'barDockControlLeft
            '
            Me.barDockControlLeft.CausesValidation = False
            Me.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left
            Me.barDockControlLeft.Location = New System.Drawing.Point(0, 22)
            Me.barDockControlLeft.Size = New System.Drawing.Size(0, 560)
            '
            'barDockControlRight
            '
            Me.barDockControlRight.CausesValidation = False
            Me.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right
            Me.barDockControlRight.Location = New System.Drawing.Point(588, 22)
            Me.barDockControlRight.Size = New System.Drawing.Size(0, 560)
            '
            'PanelControl1
            '
            Me.PanelControl1.Controls.Add(Me.GridControl1)
            Me.PanelControl1.Controls.Add(Me.Button_OK)
            Me.PanelControl1.Controls.Add(Me.TabControl1)
            Me.PanelControl1.Dock = System.Windows.Forms.DockStyle.Fill
            Me.PanelControl1.Location = New System.Drawing.Point(0, 22)
            Me.PanelControl1.Name = "PanelControl1"
            Me.PanelControl1.Size = New System.Drawing.Size(588, 560)
            Me.PanelControl1.TabIndex = 11
            '
            'GridControl1
            '
            Me.GridControl1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                                             Or System.Windows.Forms.AnchorStyles.Left) _
                                            Or System.Windows.Forms.AnchorStyles.Right),
                                           System.Windows.Forms.AnchorStyles)
            Me.GridControl1.ContextMenu = Me.ContextMenu1
            Me.GridControl1.Location = New System.Drawing.Point(2, 29)
            Me.GridControl1.MainView = Me.GridView1
            Me.GridControl1.Name = "GridControl1"
            Me.GridControl1.Size = New System.Drawing.Size(584, 493)
            Me.GridControl1.TabIndex = 9
            Me.GridControl1.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridView1})
            '
            'GridView1
            '
            Me.GridView1.Appearance.ColumnFilterButton.BackColor = System.Drawing.Color.FromArgb(CType(CType(104, Byte), Int32), CType(CType(184, Byte), Int32), CType(CType(251, Byte), Int32))
            Me.GridView1.Appearance.ColumnFilterButton.BorderColor = System.Drawing.Color.FromArgb(CType(CType(104, Byte), Int32), CType(CType(184, Byte), Int32), CType(CType(251, Byte), Int32))
            Me.GridView1.Appearance.ColumnFilterButton.ForeColor = System.Drawing.Color.White
            Me.GridView1.Appearance.ColumnFilterButton.Options.UseBackColor = True
            Me.GridView1.Appearance.ColumnFilterButton.Options.UseBorderColor = True
            Me.GridView1.Appearance.ColumnFilterButton.Options.UseForeColor = True
            Me.GridView1.Appearance.ColumnFilterButtonActive.BackColor = System.Drawing.Color.FromArgb(CType(CType(170, Byte), Int32), CType(CType(216, Byte), Int32), CType(CType(254, Byte), Int32))
            Me.GridView1.Appearance.ColumnFilterButtonActive.BorderColor = System.Drawing.Color.FromArgb(CType(CType(170, Byte), Int32), CType(CType(216, Byte), Int32), CType(CType(254, Byte), Int32))
            Me.GridView1.Appearance.ColumnFilterButtonActive.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.ColumnFilterButtonActive.Options.UseBackColor = True
            Me.GridView1.Appearance.ColumnFilterButtonActive.Options.UseBorderColor = True
            Me.GridView1.Appearance.ColumnFilterButtonActive.Options.UseForeColor = True
            Me.GridView1.Appearance.Empty.BackColor = System.Drawing.Color.FromArgb(CType(CType(236, Byte), Int32), CType(CType(246, Byte), Int32), CType(CType(255, Byte), Int32))
            Me.GridView1.Appearance.Empty.BackColor2 = System.Drawing.Color.White
            Me.GridView1.Appearance.Empty.Options.UseBackColor = True
            Me.GridView1.Appearance.EvenRow.BackColor = System.Drawing.Color.FromArgb(CType(CType(247, Byte), Int32), CType(CType(251, Byte), Int32), CType(CType(255, Byte), Int32))
            Me.GridView1.Appearance.EvenRow.BorderColor = System.Drawing.Color.FromArgb(CType(CType(247, Byte), Int32), CType(CType(251, Byte), Int32), CType(CType(255, Byte), Int32))
            Me.GridView1.Appearance.EvenRow.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.EvenRow.Options.UseBackColor = True
            Me.GridView1.Appearance.EvenRow.Options.UseBorderColor = True
            Me.GridView1.Appearance.EvenRow.Options.UseForeColor = True
            Me.GridView1.Appearance.FilterCloseButton.BackColor = System.Drawing.Color.FromArgb(CType(CType(104, Byte), Int32), CType(CType(184, Byte), Int32), CType(CType(251, Byte), Int32))
            Me.GridView1.Appearance.FilterCloseButton.BorderColor = System.Drawing.Color.FromArgb(CType(CType(104, Byte), Int32), CType(CType(184, Byte), Int32), CType(CType(251, Byte), Int32))
            Me.GridView1.Appearance.FilterCloseButton.ForeColor = System.Drawing.Color.White
            Me.GridView1.Appearance.FilterCloseButton.Options.UseBackColor = True
            Me.GridView1.Appearance.FilterCloseButton.Options.UseBorderColor = True
            Me.GridView1.Appearance.FilterCloseButton.Options.UseForeColor = True
            Me.GridView1.Appearance.FilterPanel.BackColor = System.Drawing.Color.FromArgb(CType(CType(236, Byte), Int32), CType(CType(246, Byte), Int32), CType(CType(255, Byte), Int32))
            Me.GridView1.Appearance.FilterPanel.BackColor2 = System.Drawing.Color.White
            Me.GridView1.Appearance.FilterPanel.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.FilterPanel.Options.UseBackColor = True
            Me.GridView1.Appearance.FilterPanel.Options.UseForeColor = True
            Me.GridView1.Appearance.FixedLine.BackColor = System.Drawing.Color.FromArgb(CType(CType(59, Byte), Int32), CType(CType(133, Byte), Int32), CType(CType(195, Byte), Int32))
            Me.GridView1.Appearance.FixedLine.Options.UseBackColor = True
            Me.GridView1.Appearance.FocusedCell.BackColor = System.Drawing.Color.White
            Me.GridView1.Appearance.FocusedCell.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.FocusedCell.Options.UseBackColor = True
            Me.GridView1.Appearance.FocusedCell.Options.UseForeColor = True
            Me.GridView1.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb(CType(CType(38, Byte), Int32), CType(CType(109, Byte), Int32), CType(CType(189, Byte), Int32))
            Me.GridView1.Appearance.FocusedRow.BorderColor = System.Drawing.Color.FromArgb(CType(CType(59, Byte), Int32), CType(CType(139, Byte), Int32), CType(CType(206, Byte), Int32))
            Me.GridView1.Appearance.FocusedRow.ForeColor = System.Drawing.Color.White
            Me.GridView1.Appearance.FocusedRow.Options.UseBackColor = True
            Me.GridView1.Appearance.FocusedRow.Options.UseBorderColor = True
            Me.GridView1.Appearance.FocusedRow.Options.UseForeColor = True
            Me.GridView1.Appearance.FooterPanel.BackColor = System.Drawing.Color.FromArgb(CType(CType(104, Byte), Int32), CType(CType(184, Byte), Int32), CType(CType(251, Byte), Int32))
            Me.GridView1.Appearance.FooterPanel.BorderColor = System.Drawing.Color.FromArgb(CType(CType(104, Byte), Int32), CType(CType(184, Byte), Int32), CType(CType(251, Byte), Int32))
            Me.GridView1.Appearance.FooterPanel.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.FooterPanel.Options.UseBackColor = True
            Me.GridView1.Appearance.FooterPanel.Options.UseBorderColor = True
            Me.GridView1.Appearance.FooterPanel.Options.UseForeColor = True
            Me.GridView1.Appearance.GroupButton.BackColor = System.Drawing.Color.FromArgb(CType(CType(104, Byte), Int32), CType(CType(184, Byte), Int32), CType(CType(251, Byte), Int32))
            Me.GridView1.Appearance.GroupButton.BorderColor = System.Drawing.Color.FromArgb(CType(CType(104, Byte), Int32), CType(CType(184, Byte), Int32), CType(CType(251, Byte), Int32))
            Me.GridView1.Appearance.GroupButton.Options.UseBackColor = True
            Me.GridView1.Appearance.GroupButton.Options.UseBorderColor = True
            Me.GridView1.Appearance.GroupFooter.BackColor = System.Drawing.Color.FromArgb(CType(CType(170, Byte), Int32), CType(CType(216, Byte), Int32), CType(CType(254, Byte), Int32))
            Me.GridView1.Appearance.GroupFooter.BorderColor = System.Drawing.Color.FromArgb(CType(CType(170, Byte), Int32), CType(CType(216, Byte), Int32), CType(CType(254, Byte), Int32))
            Me.GridView1.Appearance.GroupFooter.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.GroupFooter.Options.UseBackColor = True
            Me.GridView1.Appearance.GroupFooter.Options.UseBorderColor = True
            Me.GridView1.Appearance.GroupFooter.Options.UseForeColor = True
            Me.GridView1.Appearance.GroupPanel.BackColor = System.Drawing.Color.FromArgb(CType(CType(236, Byte), Int32), CType(CType(246, Byte), Int32), CType(CType(255, Byte), Int32))
            Me.GridView1.Appearance.GroupPanel.BackColor2 = System.Drawing.Color.White
            Me.GridView1.Appearance.GroupPanel.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.GroupPanel.Options.UseBackColor = True
            Me.GridView1.Appearance.GroupPanel.Options.UseForeColor = True
            Me.GridView1.Appearance.GroupRow.BackColor = System.Drawing.Color.FromArgb(CType(CType(170, Byte), Int32), CType(CType(216, Byte), Int32), CType(CType(254, Byte), Int32))
            Me.GridView1.Appearance.GroupRow.BorderColor = System.Drawing.Color.FromArgb(CType(CType(170, Byte), Int32), CType(CType(216, Byte), Int32), CType(CType(254, Byte), Int32))
            Me.GridView1.Appearance.GroupRow.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.GroupRow.Options.UseBackColor = True
            Me.GridView1.Appearance.GroupRow.Options.UseBorderColor = True
            Me.GridView1.Appearance.GroupRow.Options.UseForeColor = True
            Me.GridView1.Appearance.HeaderPanel.BackColor = System.Drawing.Color.FromArgb(CType(CType(139, Byte), Int32), CType(CType(201, Byte), Int32), CType(CType(254, Byte), Int32))
            Me.GridView1.Appearance.HeaderPanel.BorderColor = System.Drawing.Color.FromArgb(CType(CType(139, Byte), Int32), CType(CType(201, Byte), Int32), CType(CType(254, Byte), Int32))
            Me.GridView1.Appearance.HeaderPanel.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.HeaderPanel.Options.UseBackColor = True
            Me.GridView1.Appearance.HeaderPanel.Options.UseBorderColor = True
            Me.GridView1.Appearance.HeaderPanel.Options.UseForeColor = True
            Me.GridView1.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Int32), CType(CType(170, Byte), Int32), CType(CType(225, Byte), Int32))
            Me.GridView1.Appearance.HideSelectionRow.BorderColor = System.Drawing.Color.FromArgb(CType(CType(83, Byte), Int32), CType(CType(155, Byte), Int32), CType(CType(215, Byte), Int32))
            Me.GridView1.Appearance.HideSelectionRow.ForeColor = System.Drawing.Color.FromArgb(CType(CType(236, Byte), Int32), CType(CType(246, Byte), Int32), CType(CType(255, Byte), Int32))
            Me.GridView1.Appearance.HideSelectionRow.Options.UseBackColor = True
            Me.GridView1.Appearance.HideSelectionRow.Options.UseBorderColor = True
            Me.GridView1.Appearance.HideSelectionRow.Options.UseForeColor = True
            Me.GridView1.Appearance.HorzLine.BackColor = System.Drawing.Color.FromArgb(CType(CType(104, Byte), Int32), CType(CType(184, Byte), Int32), CType(CType(251, Byte), Int32))
            Me.GridView1.Appearance.HorzLine.Options.UseBackColor = True
            Me.GridView1.Appearance.OddRow.BackColor = System.Drawing.Color.FromArgb(CType(CType(236, Byte), Int32), CType(CType(246, Byte), Int32), CType(CType(255, Byte), Int32))
            Me.GridView1.Appearance.OddRow.BorderColor = System.Drawing.Color.FromArgb(CType(CType(236, Byte), Int32), CType(CType(246, Byte), Int32), CType(CType(255, Byte), Int32))
            Me.GridView1.Appearance.OddRow.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.OddRow.Options.UseBackColor = True
            Me.GridView1.Appearance.OddRow.Options.UseBorderColor = True
            Me.GridView1.Appearance.OddRow.Options.UseForeColor = True
            Me.GridView1.Appearance.Preview.Font = New System.Drawing.Font("Verdana", 7.5!)
            Me.GridView1.Appearance.Preview.ForeColor = System.Drawing.Color.FromArgb(CType(CType(83, Byte), Int32), CType(CType(155, Byte), Int32), CType(CType(215, Byte), Int32))
            Me.GridView1.Appearance.Preview.Options.UseFont = True
            Me.GridView1.Appearance.Preview.Options.UseForeColor = True
            Me.GridView1.Appearance.Row.BackColor = System.Drawing.Color.FromArgb(CType(CType(247, Byte), Int32), CType(CType(251, Byte), Int32), CType(CType(255, Byte), Int32))
            Me.GridView1.Appearance.Row.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.Row.Options.UseBackColor = True
            Me.GridView1.Appearance.Row.Options.UseForeColor = True
            Me.GridView1.Appearance.RowSeparator.BackColor = System.Drawing.Color.FromArgb(CType(CType(236, Byte), Int32), CType(CType(246, Byte), Int32), CType(CType(255, Byte), Int32))
            Me.GridView1.Appearance.RowSeparator.BackColor2 = System.Drawing.Color.White
            Me.GridView1.Appearance.RowSeparator.Options.UseBackColor = True
            Me.GridView1.Appearance.SelectedRow.BackColor = System.Drawing.Color.FromArgb(CType(CType(83, Byte), Int32), CType(CType(155, Byte), Int32), CType(CType(215, Byte), Int32))
            Me.GridView1.Appearance.SelectedRow.ForeColor = System.Drawing.Color.White
            Me.GridView1.Appearance.SelectedRow.Options.UseBackColor = True
            Me.GridView1.Appearance.SelectedRow.Options.UseForeColor = True
            Me.GridView1.Appearance.TopNewRow.BackColor = System.Drawing.Color.White
            Me.GridView1.Appearance.TopNewRow.Options.UseBackColor = True
            Me.GridView1.Appearance.VertLine.BackColor = System.Drawing.Color.FromArgb(CType(CType(104, Byte), Int32), CType(CType(184, Byte), Int32), CType(CType(251, Byte), Int32))
            Me.GridView1.Appearance.VertLine.Options.UseBackColor = True
            Me.GridView1.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.GridColumn_Item, Me.GridColumn_Date, Me.GridColumn_ReconDate, Me.GridColumn_Amount, Me.GridColumn_Payee, Me.GridColumn_Status, Me.GridColumn_TrustRegister, Me.GridColumn_ReconDetail})
            Me.GridView1.GridControl = Me.GridControl1
            Me.GridView1.Name = "GridView1"
            Me.GridView1.OptionsBehavior.Editable = False
            Me.GridView1.OptionsSelection.InvertSelection = True
            Me.GridView1.OptionsSelection.MultiSelect = True
            Me.GridView1.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never
            Me.GridView1.OptionsView.ShowGroupPanel = False
            Me.GridView1.OptionsView.ShowIndicator = False
            Me.GridView1.SortInfo.Add(GridColumn_Item, DevExpress.Data.ColumnSortOrder.Ascending)
            '
            'GridColumn_Item
            '
            Me.GridColumn_Item.Caption = "Item"
            Me.GridColumn_Item.FieldName = "checknum"
            Me.GridColumn_Item.Name = "GridColumn_Item"
            Me.GridColumn_Item.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.[True]
            Me.GridColumn_Item.Visible = True
            Me.GridColumn_Item.VisibleIndex = 0
            Me.GridColumn_Item.Width = 83
            Me.GridColumn_Item.SortOrder = DevExpress.Data.ColumnSortOrder.Ascending
            '
            'GridColumn_Date
            '
            Me.GridColumn_Date.Caption = "Date"
            Me.GridColumn_Date.DisplayFormat.FormatString = "d"
            Me.GridColumn_Date.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime
            Me.GridColumn_Date.FieldName = "item_date"
            Me.GridColumn_Date.Name = "GridColumn_Date"
            Me.GridColumn_Date.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.[True]
            Me.GridColumn_Date.Visible = True
            Me.GridColumn_Date.VisibleIndex = 1
            Me.GridColumn_Date.Width = 93
            '
            'GridColumn_ReconDate
            '
            Me.GridColumn_ReconDate.Caption = "Recon"
            Me.GridColumn_ReconDate.DisplayFormat.FormatString = "d"
            Me.GridColumn_ReconDate.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime
            Me.GridColumn_ReconDate.FieldName = "recon_date"
            Me.GridColumn_ReconDate.Name = "GridColumn_ReconDate"
            Me.GridColumn_ReconDate.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.[True]
            Me.GridColumn_ReconDate.Visible = True
            Me.GridColumn_ReconDate.VisibleIndex = 2
            Me.GridColumn_ReconDate.Width = 99
            '
            'GridColumn_Amount
            '
            Me.GridColumn_Amount.Caption = "Amount"
            Me.GridColumn_Amount.DisplayFormat.FormatString = "c2"
            Me.GridColumn_Amount.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.GridColumn_Amount.FieldName = "item_amount"
            Me.GridColumn_Amount.Name = "GridColumn_Amount"
            Me.GridColumn_Amount.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.[True]
            Me.GridColumn_Amount.Visible = True
            Me.GridColumn_Amount.VisibleIndex = 3
            Me.GridColumn_Amount.Width = 92
            '
            'GridColumn_Payee
            '
            Me.GridColumn_Payee.Caption = "Payee"
            Me.GridColumn_Payee.FieldName = "payee"
            Me.GridColumn_Payee.Name = "GridColumn_Payee"
            Me.GridColumn_Payee.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.[True]
            Me.GridColumn_Payee.Visible = True
            Me.GridColumn_Payee.VisibleIndex = 4
            Me.GridColumn_Payee.Width = 305
            '
            'GridColumn_Status
            '
            Me.GridColumn_Status.Caption = "Status"
            Me.GridColumn_Status.FieldName = "message"
            Me.GridColumn_Status.Name = "GridColumn_Status"
            Me.GridColumn_Status.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.[True]
            Me.GridColumn_Status.Visible = True
            Me.GridColumn_Status.VisibleIndex = 5
            Me.GridColumn_Status.Width = 118
            '
            'GridColumn_TrustRegister
            '
            Me.GridColumn_TrustRegister.Caption = "TrustRegister"
            Me.GridColumn_TrustRegister.DisplayFormat.FormatString = "f0"
            Me.GridColumn_TrustRegister.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.GridColumn_TrustRegister.FieldName = "trust_register"
            Me.GridColumn_TrustRegister.Name = "GridColumn_TrustRegister"
            Me.GridColumn_TrustRegister.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.[True]
            '
            'GridColumn_ReconDetail
            '
            Me.GridColumn_ReconDetail.Caption = "ReconDetail"
            Me.GridColumn_ReconDetail.DisplayFormat.FormatString = "f0"
            Me.GridColumn_ReconDetail.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.GridColumn_ReconDetail.FieldName = "recon_detail"
            Me.GridColumn_ReconDetail.Name = "GridColumn_ReconDetail"
            Me.GridColumn_ReconDetail.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.[True]
            '
            'Button_OK
            '
            Me.Button_OK.Anchor = System.Windows.Forms.AnchorStyles.Bottom
            Me.Button_OK.Location = New System.Drawing.Point(251, 529)
            Me.Button_OK.Name = "Button_OK"
            Me.Button_OK.Size = New System.Drawing.Size(75, 25)
            Me.Button_OK.TabIndex = 8
            Me.Button_OK.Text = "&OK"
            '
            'TabControl1
            '
            Me.TabControl1.Appearance = System.Windows.Forms.TabAppearance.Buttons
            Me.TabControl1.Controls.Add(Me.TabPage1)
            Me.TabControl1.Controls.Add(Me.TabPage2)
            Me.TabControl1.Controls.Add(Me.TabPage3)
            Me.TabControl1.Controls.Add(Me.TabPage4)
            Me.TabControl1.Controls.Add(Me.TabPage5)
            Me.TabControl1.Controls.Add(Me.TabPage6)
            Me.TabControl1.Controls.Add(Me.TabPage7)
            Me.TabControl1.Controls.Add(Me.TabPage8)
            Me.TabControl1.Dock = System.Windows.Forms.DockStyle.Top
            Me.TabControl1.Location = New System.Drawing.Point(2, 2)
            Me.TabControl1.Name = "TabControl1"
            Me.TabControl1.SelectedIndex = 0
            Me.TabControl1.Size = New System.Drawing.Size(584, 26)
            Me.TabControl1.TabIndex = 7
            '
            'TabPage1
            '
            Me.TabPage1.Location = New System.Drawing.Point(4, 26)
            Me.TabPage1.Name = "TabPage1"
            Me.TabPage1.Size = New System.Drawing.Size(576, 0)
            Me.TabPage1.TabIndex = 0
            Me.TabPage1.Tag = "AD"
            Me.TabPage1.Text = "Checks"
            '
            'TabPage2
            '
            Me.TabPage2.Location = New System.Drawing.Point(4, 26)
            Me.TabPage2.Name = "TabPage2"
            Me.TabPage2.Size = New System.Drawing.Size(576, 0)
            Me.TabPage2.TabIndex = 1
            Me.TabPage2.Tag = "DP"
            Me.TabPage2.Text = "Deposits"
            '
            'TabPage3
            '
            Me.TabPage3.Location = New System.Drawing.Point(4, 26)
            Me.TabPage3.Name = "TabPage3"
            Me.TabPage3.Size = New System.Drawing.Size(576, 0)
            Me.TabPage3.TabIndex = 2
            Me.TabPage3.Tag = "BW"
            Me.TabPage3.Text = "Wire Transfers"
            '
            'TabPage4
            '
            Me.TabPage4.Location = New System.Drawing.Point(4, 26)
            Me.TabPage4.Name = "TabPage4"
            Me.TabPage4.Size = New System.Drawing.Size(576, 0)
            Me.TabPage4.TabIndex = 3
            Me.TabPage4.Tag = "BI"
            Me.TabPage4.Text = "Interest"
            '
            'TabPage5
            '
            Me.TabPage5.Location = New System.Drawing.Point(4, 26)
            Me.TabPage5.Name = "TabPage5"
            Me.TabPage5.Size = New System.Drawing.Size(576, 0)
            Me.TabPage5.TabIndex = 4
            Me.TabPage5.Tag = "SC"
            Me.TabPage5.Text = "Service Charges"
            '
            'TabPage6
            '
            Me.TabPage6.Location = New System.Drawing.Point(4, 26)
            Me.TabPage6.Name = "TabPage6"
            Me.TabPage6.Size = New System.Drawing.Size(576, 0)
            Me.TabPage6.TabIndex = 5
            Me.TabPage6.Tag = "RF"
            Me.TabPage6.Text = "Creditor Refunds"
            '
            'TabPage7
            '
            Me.TabPage7.Location = New System.Drawing.Point(4, 26)
            Me.TabPage7.Name = "TabPage7"
            Me.TabPage7.Size = New System.Drawing.Size(576, 0)
            Me.TabPage7.TabIndex = 6
            Me.TabPage7.Tag = "RR"
            Me.TabPage7.Text = "EFT Rejects"
            '
            'TabPage8
            '
            Me.TabPage8.Location = New System.Drawing.Point(4, 26)
            Me.TabPage8.Name = "TabPage8"
            Me.TabPage8.Size = New System.Drawing.Size(576, 0)
            Me.TabPage8.TabIndex = 7
            Me.TabPage8.Tag = "BE"
            Me.TabPage8.Text = "Errors"
            '
            'ReconcileItemsForm
            '
            Me.Appearance.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.Appearance.Options.UseFont = True
            Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 14.0!)
            Me.ClientSize = New System.Drawing.Size(588, 608)
            Me.Controls.Add(Me.PanelControl1)
            Me.Controls.Add(Me.barDockControlLeft)
            Me.Controls.Add(Me.barDockControlRight)
            Me.Controls.Add(Me.barDockControlBottom)
            Me.Controls.Add(Me.barDockControlTop)
            Me.Cursor = System.Windows.Forms.Cursors.Default
            Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
            Me.Location = New System.Drawing.Point(11, 49)
            Me.LookAndFeel.UseDefaultLookAndFeel = True
            Me.Name = "ReconcileItemsForm"
            Me.StartPosition = System.Windows.Forms.FormStartPosition.WindowsDefaultLocation
            Me.Text = "Reconciliation Items"
            CType(Me.DxErrorProvider1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.barManager1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.PanelControl1, System.ComponentModel.ISupportInitialize).EndInit()
            Me.PanelControl1.ResumeLayout(False)
            CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.GridView1, System.ComponentModel.ISupportInitialize).EndInit()
            Me.TabControl1.ResumeLayout(False)
            Me.ResumeLayout(False)
        End Sub

#End Region

        Private Sub ReconcileItemsForm_FormClosing(ByVal sender As Object, ByVal e As FormClosingEventArgs)

            If Not NormalClose Then

                Select Case DebtPlus.Data.Forms.MessageBox.Show("Do you wish to save the changes?", "Save Changes?", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1)
                    Case DialogResult.Yes
                        SaveChanges()

                    Case DialogResult.No
                    Case DialogResult.Cancel
                        e.Cancel = True

                End Select
            End If
        End Sub

        ''' <summary>
        ''' Handle the form receiving focus
        ''' </summary>
        Private Sub ReconcileItemsForm_GotFocus(ByVal sender As Object, ByVal e As EventArgs)

            ' Give the focus to the grid control so that clicking will work immediately
            GridView1.Focus()
        End Sub

        ''' <summary>
        ''' Process the load event for the form
        ''' </summary>
        Private Sub frmItems_Load(ByVal Sender As Object, ByVal e As EventArgs)
            UnRegisterHandlers()
            Try
                BarStaticItem_recon_date.Caption = ReconDate.ToShortDateString

                ' Select the last item in the list and load its information
                TabControl1.SelectedIndex = TabControl1.TabCount - 1
                load_list()
            Finally
                RegisterHandlers()
            End Try
        End Sub

        ''' <summary>
        ''' The tab is being changed in the tab list
        ''' </summary>
        Private Sub TabControl1_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs)
            load_list()
        End Sub

        ''' <summary>
        ''' Refresh the list of items to be reconciled
        ''' </summary>
        Public Sub load_list()
            With bt_reader
                If Not .IsBusy Then
                    SaveChanges()
                    ds.Clear()
                    .RunWorkerAsync(TransactionType)
                End If
            End With
        End Sub

        ''' <summary>
        ''' Thread to refresh the list of items
        ''' </summary>
        Private Sub bt_reader_DoWork(ByVal sender As Object, ByVal e As DoWorkEventArgs)

            ' Clear the dataset and start with a new copy
            Dim current_cursor As Cursor = Cursor.Current
            Try
                Cursor.Current = Cursors.WaitCursor

                Using cmd As SqlCommand = New SqlCommand
                    With cmd
                        .Connection = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
                        .CommandText = "xpr_recon_item_list"
                        .CommandType = CommandType.StoredProcedure
                        With .Parameters
                            .Add("@recon_batch", SqlDbType.Int).Value = ReconBatch
                            .Add("@tran_type", SqlDbType.VarChar, 10).Value = e.Argument
                        End With
                    End With

                    Using da As New SqlDataAdapter(cmd)
                        da.Fill(ds, "xpr_recon_item_list")
                    End Using
                End Using

                With ds.Tables("xpr_recon_item_list")

                    ' Define the primary key to the table for new additions
                    With .Columns("recon_detail")
                        .AutoIncrement = True
                        .AutoIncrementSeed = -1
                        .AutoIncrementStep = -1
                    End With

                    If Not .Columns.Contains("tran_type") Then
                        .Columns.Add("tran_type", GetType(String))
                    End If

                    ' Add the "cleared" column to the result
                    If Not .Columns.Contains("cleared") Then
                        With .Columns.Add("cleared", GetType(String))
                            .DefaultValue = STR_UNRECONCILED
                        End With
                    End If

                    For Each row As DataRow In ds.Tables(0).Rows
                        row("tran_type") = e.Argument
                        If row("cleared") Is Nothing OrElse row("cleared") Is DBNull.Value Then
                            row("cleared") = STR_UNRECONCILED
                        End If
                        row.AcceptChanges()
                    Next
                End With

            Finally
                Cursor.Current = current_cursor
            End Try

            ' The result is the pointer to the new table with the results
            e.Result = ds.Tables("xpr_recon_item_list")
        End Sub

        ''' <summary>
        ''' Save the changes to the recon table when the itesm are switched
        ''' </summary>
        Private Sub SaveChanges()

            ' The view may be nothing when we first start.
            Dim vue As DataView = TryCast(GridControl1.DataSource, DataView)
            If vue Is Nothing Then Return

            ' From the view, find the current table to be updated.
            Dim tbl As DataTable = vue.Table
            Dim cn As SqlConnection = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
            Dim txn As SqlTransaction = Nothing

            Dim UpdateCmd As SqlCommand = New SqlCommand
            Dim DeleteCmd As SqlCommand = New SqlCommand

            ' Find the items that would normally be added but are now no longer required.
            Dim NewItems() As DataRow = tbl.Select("([recon_detail] is null) AND ([recon_date] is null)", String.Empty, DataViewRowState.ModifiedCurrent)
            For Each row As DataRow In NewItems
                row.AcceptChanges()
            Next

            Try
                cn.Open()
                txn = cn.BeginTransaction

                ' Find the items that do not have a recon_detail item. These need to be added. They say "modified", but "added" is what they need to be.
                Dim AddedRows() As DataRow = tbl.Select("([recon_detail] is null) AND ([recon_date] is not null)", String.Empty, DataViewRowState.ModifiedCurrent)
                If AddedRows.GetUpperBound(0) >= 0 Then
                    Using AddedCmd As SqlCommand = New SqlCommand
                        With AddedCmd
                            .Connection = cn
                            .Transaction = txn
                            .CommandText = "xpr_insert_recon_detail"
                            .CommandType = CommandType.StoredProcedure
                            With .Parameters
                                .Add("@RETURN_VALUE", SqlDbType.Int, 4, "recon_detail").Direction = ParameterDirection.ReturnValue
                                .Add("@Batch", SqlDbType.Int).Value = ReconBatch
                                .Add("@TrustRegister", SqlDbType.Int, 4, "trust_register")
                                .Add("@Cleared", SqlDbType.VarChar, 1, "cleared")
                                .Add("@ReconDate", SqlDbType.DateTime, 8, "recon_date")
                                .Add("@ReconAmount", SqlDbType.Decimal, 8, "item_amount")
                                .Add("@tran_type", SqlDbType.VarChar, 2, "tran_type")
                                .Add("@Comments", SqlDbType.VarChar, 80, "message")

                                ' These parameters are not needed since we specify the trust register directly.
                                .Add("@CheckNumber", SqlDbType.VarChar, 50).Value = DBNull.Value
                                .Add("@Bank", SqlDbType.Int).Value = DBNull.Value
                            End With
                        End With

                        Using da As New SqlDataAdapter
                            da.UpdateCommand = AddedCmd
                            da.AcceptChangesDuringUpdate = True
                            da.ContinueUpdateOnError = True
                            da.Update(AddedRows)
                        End Using
                    End Using
                End If

                ' Do the normal operations at this point
                Using da As New SqlDataAdapter

                    ' Update command
                    With UpdateCmd
                        .Connection = cn
                        .Transaction = txn
                        .CommandText = "UPDATE recon_details SET recon_batch=@recon_batch,amount=@item_amount,cleared=@cleared,recon_date=@recon_date,message=@message WHERE recon_detail=@recon_detail"
                        With .Parameters
                            .Add("@tran_type", SqlDbType.VarChar, 2, "tran_type")
                            .Add("@recon_batch", SqlDbType.Int, 4).Value = ReconBatch
                            .Add("@item_amount", SqlDbType.Decimal, 8, "item_amount")
                            .Add("@cleared", SqlDbType.VarChar, 1, "cleared")
                            .Add("@recon_date", SqlDbType.DateTime, 8, "recon_date")
                            .Add("@message", SqlDbType.VarChar, 80, "message")
                            .Add("@recon_detail", SqlDbType.Int, 4, "recon_detail")
                        End With
                    End With
                    da.UpdateCommand = UpdateCmd

                    ' Delete command
                    With DeleteCmd
                        .Connection = cn
                        .Transaction = txn
                        .CommandText = "DELETE FROM recon_details WHERE recon_detail=@recon_detail"
                        With .Parameters
                            .Add("@recon_detail", SqlDbType.Int, 0, "recon_detail")
                        End With
                    End With
                    da.DeleteCommand = DeleteCmd

                    ' Perform the update on the table with the change
                    da.ContinueUpdateOnError = True
                    da.Update(tbl)

                    ' Do the accept changes on all of the rows so that the ones added without a recon date
                    ' will not be used again.
                    tbl.AcceptChanges()
                End Using

                txn.Commit()
                txn.Dispose()
                txn = Nothing

            Catch ex As SqlException
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error updating recon items")

            Finally
                UpdateCmd.Dispose()
                DeleteCmd.Dispose()

                If txn IsNot Nothing Then
                    Try
                        txn.Rollback()
                    Catch exRollback As Exception
                    End Try
                    txn.Dispose()
                    txn = Nothing
                End If

                If cn IsNot Nothing Then cn.Dispose()
            End Try
        End Sub

        ''' <summary>
        ''' The thread refresh cycle has completed
        ''' </summary>
        Private Sub bt_reader_RunWorkerCompleted(ByVal sender As Object, ByVal e As RunWorkerCompletedEventArgs)
            Dim tbl As DataTable = CType(e.Result, DataTable)

            ' Update the panel with the number of items
            If tbl IsNot Nothing Then

                ' Set the update table for the event handler
                If UpdateTable IsNot Nothing Then
                    RemoveHandler UpdateTable.RowChanged, AddressOf UpdateTable_RowChanged
                End If
                UpdateTable = tbl
                If UpdateTable IsNot Nothing Then
                    AddHandler UpdateTable.RowChanged, AddressOf UpdateTable_RowChanged
                End If

                BarStaticItem_item_count.Caption = String.Format("{0:n0} item(s)", tbl.Rows.Count)

                ' Bind the data to the control
                With GridControl1
                    .DataSource = UpdateTable.DefaultView
                    .RefreshDataSource()
                    UpdateClearedCount()
                End With
            End If
        End Sub

        ''' <summary>
        ''' Group of items to be reconciled
        ''' </summary>
        Private ReadOnly Property TransactionType() As String
            Get
                Return Convert.ToString(TabControl1.SelectedTab.Tag)
            End Get
        End Property

        ''' <summary>
        ''' Process the table changes
        ''' </summary>
        Private Sub UpdateTable_RowChanged(ByVal sender As Object, ByVal e As DataRowChangeEventArgs)

            ' If the row count is the reconciled date then update the cleared counnt
            If e.Action = DataRowAction.Change Then
                UpdateClearedCount()
            End If
        End Sub

        ''' <summary>
        ''' Update the statusbar with the cleared information
        ''' </summary>
        Private Sub UpdateClearedCount()

            Dim vue As DataView = CType(GridControl1.DataSource, DataView)
            Dim objCount As Object = 0
            Dim ReconciledAmount As Object = 0D

            If vue IsNot Nothing Then
                With vue.Table
                    objCount = .Compute("count(item_amount)", "[recon_date] is not null")
                    ReconciledAmount = .Compute("sum(item_amount)", "[recon_date] is not null")
                End With
            End If

            If objCount Is DBNull.Value Then objCount = 0
            If ReconciledAmount Is DBNull.Value Then ReconciledAmount = 0D

            ' Update the status bar
            BarStaticItem_recon_count.Caption = String.Format("Reconciled {0:n0} items", Convert.ToInt32(objCount))
            BarStaticItem_recon_amount.Caption = String.Format("for {0:c}", Convert.ToDecimal(ReconciledAmount))
        End Sub

        ''' <summary>
        ''' Process a click event on an item. Reconcile the item in the proper column.
        ''' </summary>
        Private Sub GridView1_Click(ByVal sender As Object, ByVal e As EventArgs)
            Dim gv As GridView = GridView1
            Dim hi As GridHitInfo = gv.CalcHitInfo((gv.GridControl.PointToClient(MousePosition)))

            ' Look for a click in the reconciled date for an item
            If hi.IsValid AndAlso hi.InRowCell Then
                Dim RowHandle As Int32 = hi.RowHandle

                Dim Col As GridColumn = hi.Column
                If String.Compare(Col.FieldName, "recon_date", False) = 0 Then
                    Dim drv As DataRowView = CType(GridView1.GetRow(RowHandle), DataRowView)
                    drv.BeginEdit()

                    ' There must be a trust register item for this purpose. Create one if not present.
                    If drv("trust_register") Is DBNull.Value AndAlso TransactionType <> "AD" Then
                        Try
                            Using cn As New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
                                cn.Open()
                                Using cmd As SqlCommand = New SqlCommand
                                    With cmd
                                        .Connection = cn
                                        .CommandText = String.Format("xpr_trust_register_create_{0}", TransactionType)
                                        .CommandType = CommandType.StoredProcedure
                                        With .Parameters
                                            .Add("@RETURN_VALUE", SqlDbType.Int, 0).Direction = ParameterDirection.ReturnValue
                                            .Add("@cleared", SqlDbType.VarChar, 2).Value = STR_UNRECONCILED
                                            .Add("@amount", SqlDbType.Money).Value = drv("item_amount")
                                        End With
                                        drv("trust_register") = .Parameters("@RETURN_VALUE").Value
                                    End With
                                End Using
                            End Using

                        Catch ex As SqlException
                            DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error creating trust register element")
                        End Try
                    End If

                    ' Find the row to be updated. If there is no recon item then set the row as "added" to include a new row.
                    If drv("recon_date") Is DBNull.Value Then
                        drv("recon_date") = ReconDate
                        drv("cleared") = STR_RECONCILED
                    Else
                        drv("recon_date") = DBNull.Value
                        drv("cleared") = STR_UNRECONCILED
                    End If

                    drv.EndEdit()
                End If
            End If
        End Sub

        ''' <summary>
        ''' A keypress event occurred on the list.
        ''' </summary>
        Private Sub GridView1_KeyPress(ByVal sender As Object, ByVal e As KeyPressEventArgs)

            ' Control-A : Select all of the rows
            If Asc(e.KeyChar) = 1 Then
                GridView1.SelectAll()
                GridView1.InvalidateRows()
                Return
            End If

            ' Escape : Clear the selection of the rows
            If Asc(e.KeyChar) = 39 Then
                GridView1.ClearSelection()
                GridView1.InvalidateRows()
                Return
            End If
        End Sub

        ''' <summary>
        ''' Change the date for reconciliation
        ''' </summary>
        Public Sub mnuChangeDate_Click(ByVal eventSender As Object, ByVal eventArgs As EventArgs)

            Using frm As New ReconcilationDateForm
                frm.Parameter_FormDate = ReconDate
                frm.ShowDialog()
                ReconDate = frm.Parameter_FormDate
            End Using

            BarStaticItem_recon_date.Caption = ReconDate.ToShortDateString
        End Sub

        ''' <summary>
        ''' Process a change on the menu item
        ''' </summary>
        Public Sub mnuChangeItem_Click(ByVal Sender As Object, ByVal e As EventArgs)
            With GridView1
                .BeginUpdate()

                ' Process the rows. Look for a row with a reconciled date and change the reconciled status
                Dim SelectedRows() As Int32 = .GetSelectedRows()
                For Each RowHandle As Int32 In SelectedRows
                    Dim drv As DataRowView = CType(.GetRow(RowHandle), DataRowView)

                    ' If there is a date then remove it. Otherwise, set it.
                    'If drv("recon_detail") Is System.DBNull.Value AndAlso drv.Row.RowState = DataRowState.Unchanged Then drv.Row.SetAdded()
                    drv.BeginEdit()

                    If drv("recon_date") Is DBNull.Value Then
                        drv("recon_date") = ReconDate
                        drv("cleared") = STR_RECONCILED
                    Else
                        drv("recon_date") = DBNull.Value
                        drv("cleared") = STR_UNRECONCILED
                    End If
                    drv.EndEdit()
                Next

                ' Once all of the rows as addressed, remove the selected status
                .ClearSelection()
                .EndUpdate()
                .InvalidateRows()
            End With
        End Sub

        ''' <summary>
        ''' Change the status of item in the list
        ''' </summary>
        Public Sub mnuChangeStatus_Click(ByVal Sender As Object, ByVal e As EventArgs)
            With GridView1
                .BeginUpdate()

                ' Process the rows. Look for a row with a reconciled date and change the reconciled status
                Dim SelectedRows() As Int32 = .GetSelectedRows()
                For Each RowHandle As Int32 In SelectedRows
                    Dim drv As DataRowView = CType(.GetRow(RowHandle), DataRowView)

                    Dim Message As String = String.Empty
                    If drv("message") IsNot DBNull.Value Then Message = Convert.ToString(drv("message"))
                    If String.Compare(Message, STR_NOTFOUND, False) = 0 Then
                        If drv("recon_detail") Is DBNull.Value Then drv.Row.SetAdded()
                        drv.BeginEdit()
                        drv("message") = STR_FOUND
                        drv.EndEdit()
                    End If
                Next

                ' Once all of the rows as addressed, remove the selected status
                .ClearSelection()
                .EndUpdate()
                .InvalidateRows()
            End With
        End Sub

        ''' <summary>
        ''' Create a new item when desired
        ''' </summary>
        Public Sub mnuCreateItem_Click(ByVal Sender As Object, ByVal e As EventArgs)

            ' We only permit specific items to be created.
            Select Case TransactionType
                Case "SC", "BI", "BE"

                    ' Ask for the date and the amount. If successful then create the new item.
                    Using frm As New ServiceChargeForm
                        If frm.ShowDialog() = DialogResult.OK Then
                            Dim DateCreated As Date = frm.Parameter_Date
                            Dim Amount As Decimal = frm.Parameter_Amount
                            Dim TrustRegister As Int32 = 0

                            Using cm As New DebtPlus.UI.Common.CursorManager
                                Using cn As New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
                                    cn.Open()
                                    Using cmd As SqlCommand = New SqlCommand
                                        cmd.Connection = cn
                                        cmd.CommandType = CommandType.StoredProcedure
                                        cmd.CommandText = String.Format("xpr_trust_register_create_{0}", TransactionType)
                                        cmd.Parameters.Add("@RETURN_VALUE", SqlDbType.Int).Direction = ParameterDirection.ReturnValue
                                        cmd.Parameters.Add("@Amount", SqlDbType.Decimal).Value = Amount
                                        cmd.Parameters.Add("@ItemDate", SqlDbType.DateTime).Value = DateCreated
                                        cmd.Parameters.Add("@Cleared", SqlDbType.VarChar, 1).Value = STR_UNRECONCILED
                                        cmd.ExecuteNonQuery()

                                        Dim objResult As Object = cmd.Parameters("@RETURN_VALUE").Value
                                        If objResult IsNot Nothing AndAlso objResult IsNot DBNull.Value Then
                                            TrustRegister = Convert.ToInt32(objResult)
                                        End If
                                    End Using
                                End Using
                            End Using

                            If TrustRegister > 0 Then
                                Using vue As DataView = CType(GridControl1.DataSource, DataView)
                                    Dim drv As DataRowView = vue.AddNew
                                    drv.BeginEdit()
                                    drv("trust_register") = TrustRegister
                                    drv("tran_type") = TransactionType
                                    drv("amount") = Amount
                                    drv("cleared") = STR_RECONCILED
                                    drv("recon_date") = ReconDate
                                    drv.EndEdit()
                                End Using
                            End If
                        End If
                    End Using

                Case Else
                    DebtPlus.Data.Forms.MessageBox.Show("The item can not be created because it involves normal processing of DebtPlus. You may wish to enter a BANK ERROR or SERVICE CHARGE or BANK INTEREST to adjust the trust register but items such as DEPOSITS or CHECKS may only be created by doing the deposit or disbursement operation", "Sorry, the transaction can not be created", MessageBoxButtons.OK, MessageBoxIcon.Information)
            End Select
        End Sub

        ''' <summary>
        ''' Close the program based upon the menu pick
        ''' </summary>
        Private Sub BarButtonItem_File_Exit_ItemClick(ByVal sender As Object, ByVal e As ItemClickEventArgs)
            DialogResult = DialogResult.OK
            If Not Modal Then
                Close()
            End If
        End Sub

        ''' <summary>
        ''' Print the reconciliation batch report
        ''' </summary>
        Private Sub BarButtonItem_File_Print_ItemClick(ByVal sender As Object, ByVal e As ItemClickEventArgs)

            SaveChanges()

            Dim rpt As New DebtPlus.Reports.Reconcile.Detail.CheckReconcileDetailListReport()
            rpt.Parameter_BatchID = ReconBatch
            If rpt.RequestReportParameters = DialogResult.OK Then
                rpt.RunReportInSeparateThread()
            End If
        End Sub

        ''' <summary>
        ''' Process a double-click on the status panel
        ''' </summary>
        Private Sub BarStaticItem_recon_date_ItemDoubleClick(ByVal sender As Object, ByVal e As ItemClickEventArgs)

            ' Look for a double-click on the date pannel to process the date update
            Using frm As New ReconcilationDateForm
                frm.Parameter_FormDate = ReconDate
                frm.ShowDialog()
                ReconDate = frm.Parameter_FormDate
            End Using

            BarStaticItem_recon_date.Caption = ReconDate.ToShortDateString
        End Sub

        ''' <summary>
        ''' Popup event for the grid control
        ''' </summary>
        Private Sub ContextMenu1_Popup(ByVal sender As Object, ByVal e As EventArgs)

            Try
                ' Find which row the click event occurred
                Dim gv As GridView = GridView1
                Dim hi As GridHitInfo = gv.CalcHitInfo((gv.GridControl.PointToClient(MousePosition)))

                ' Save the handle for the menu pick event.
                RightClickItem = If(hi.IsValid AndAlso hi.InRowCell, hi.RowHandle, -1)

                ' If the row handle is valid then enable the menu items
                mnuChangeItem.Enabled = (RightClickItem >= 0)
                mnuChangeStatus.Enabled = (RightClickItem >= 0)

                ' Change the text for the menu items
                mnuChangeDate.Text = String.Format("Change &Date from {0:d} ...", ReconDate)

                ' If the transaction type is valid then allow the create to work
                Select Case TransactionType
                    Case "BE", "BI", "SC"
                        mnuCreateItem.Enabled = True
                    Case Else
                        mnuCreateItem.Enabled = False
                End Select

                ' Determine if there are items selected
                Dim nStatus As Int32 = 0
                Dim nCount As Int32 = 0
                Dim selectedRows() As Int32 = GridView1.GetSelectedRows()
                If selectedRows IsNot Nothing Then
                    For Each rowHandle As Int32 In selectedRows
                        nCount += 1

                        ' Determine if the selected items are reconciled or not. It is valid to have a mixture.
                        Dim drv As DataRowView = GridView1.GetRow(rowHandle)
                        If drv IsNot Nothing Then
                            If drv("recon_date") Is DBNull.Value Then
                                nStatus = nStatus Or 1
                            Else
                                nStatus = nStatus Or 2
                            End If
                        End If
                    Next
                End If

                ' Based upon the selected items, determine the appropriate message for the function
                With mnuChangeItem
                    Select Case nStatus
                        Case 0
                            .Text = ""
                            .Enabled = False
                            .Visible = False

                        Case 1
                            .Text = If(nCount = 1, String.Format("Reconcile the item to {0:d}", ReconDate), String.Format("Reconcile all items to {0:d}", ReconDate))
                            .Enabled = True
                            .Visible = True

                        Case 2
                            .Text = If(nCount = 1, "Un-Reconcile the item", "Un-Reconcile all items")
                            .Enabled = True
                            .Visible = True

                        Case 3
                            .Text = String.Format("Toggle reconciliation status to {0:d}", ReconDate)
                            .Enabled = True
                            .Visible = True

                        Case Else
                    End Select
                End With

            Catch ex As Exception
                DebtPlus.Data.Forms.MessageBox.Show(ex.ToString(), "Error handling popup menu")
            End Try
        End Sub

        ''' <summary>
        ''' Process the OK button
        ''' </summary>
        Private Sub Button_OK_Click(ByVal sender As Object, ByVal e As EventArgs)
            DialogResult = DialogResult.OK
            If Not Modal Then
                Close()
            End If
        End Sub
    End Class
End Namespace
