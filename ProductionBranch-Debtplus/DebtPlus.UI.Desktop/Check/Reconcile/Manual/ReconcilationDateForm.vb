#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}
#End Region

Option Strict Off
Option Explicit On
Namespace Check.Reconcile.Manual
    Friend Class ReconcilationDateForm
        Inherits DebtPlus.Data.Forms.DebtPlusForm

        Public Sub New()
            MyBase.New()
            InitializeComponent()
            AddHandler MonthCalendar1.DoubleClick, AddressOf MonthCalendar1_DoubleClick
            AddHandler MonthCalendar1.EditDateModified, AddressOf MonthCalendar1_DateSelected
            AddHandler Me.Load, AddressOf ReconcilationDateForm_Load
        End Sub

        Private privateSelectedDate As Date
        Public Property Parameter_FormDate() As Date
            Get
                Return privateSelectedDate
            End Get
            Set(ByVal Value As Date)
                privateSelectedDate = Value
            End Set
        End Property

#Region "Windows Form Designer generated code "

        Protected Overloads Overrides Sub Dispose(ByVal Disposing As Boolean)
            If Disposing Then
                If components IsNot Nothing Then
                    components.Dispose()
                End If
            End If
            MyBase.Dispose(Disposing)
        End Sub

        Private components As System.ComponentModel.IContainer
        'NOTE: The following procedure is required by the Windows Form Designer
        'It can be modified using the Windows Form Designer.
        'Do not modify it using the code editor.
        Friend WithEvents MonthCalendar1 As DevExpress.XtraScheduler.DateNavigator
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
            Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(ReconcilationDateForm))
            Me.MonthCalendar1 = New DevExpress.XtraScheduler.DateNavigator
            CType(Me.DxErrorProvider1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.MonthCalendar1, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.SuspendLayout()
            '
            'MonthCalendar1
            '
            Me.MonthCalendar1.AppearanceCalendar.BackColor = System.Drawing.Color.Cornsilk
            Me.MonthCalendar1.AppearanceCalendar.ForeColor = System.Drawing.SystemColors.WindowText
            Me.MonthCalendar1.AppearanceCalendar.Options.UseBackColor = True
            Me.MonthCalendar1.AppearanceCalendar.Options.UseForeColor = True
            Me.MonthCalendar1.BoldAppointmentDates = False
            Me.MonthCalendar1.DateTime = New Date(2010, 4, 5, 0, 0, 0, 0)
            Me.MonthCalendar1.Font = New System.Drawing.Font("Arial", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.MonthCalendar1.HotDate = Nothing
            Me.MonthCalendar1.Location = New System.Drawing.Point(40, 8)
            Me.MonthCalendar1.Multiselect = False
            Me.MonthCalendar1.Name = "MonthCalendar1"
            Me.MonthCalendar1.Size = New System.Drawing.Size(179, 175)
            Me.MonthCalendar1.TabIndex = 0
            Me.MonthCalendar1.WeekNumberRule = DevExpress.XtraEditors.Controls.WeekNumberRule.FirstDay
            '
            'ReconcilationDateForm
            '
            Me.Appearance.Font = New System.Drawing.Font("Times New Roman", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.Appearance.Options.UseFont = True
            Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
            Me.ClientSize = New System.Drawing.Size(258, 200)
            Me.Controls.Add(Me.MonthCalendar1)
            Me.Cursor = System.Windows.Forms.Cursors.Default
            Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
            Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
            Me.Location = New System.Drawing.Point(3, 22)
            Me.MaximizeBox = False
            Me.MinimizeBox = False
            Me.Name = "ReconcilationDateForm"
            Me.RightToLeft = System.Windows.Forms.RightToLeft.No
            Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
            Me.Text = "Reconciliation Date"
            CType(Me.DxErrorProvider1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.MonthCalendar1, System.ComponentModel.ISupportInitialize).EndInit()
            Me.ResumeLayout(False)

        End Sub
#End Region

        Private InInit As Boolean = True
        Private NewDate As Boolean

        Private Sub MonthCalendar1_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs)
            If NewDate Then
                DialogResult = Windows.Forms.DialogResult.OK
            End If
        End Sub

        Private Sub MonthCalendar1_DateSelected(ByVal sender As Object, ByVal e As System.EventArgs)
            If Not InInit Then
                Parameter_FormDate = MonthCalendar1.DateTime
                NewDate = True
            End If
        End Sub

        Private Sub ReconcilationDateForm_Load(ByVal sender As Object, ByVal e As System.EventArgs)
            InInit = True
            MonthCalendar1.Selection.Clear()
            MonthCalendar1.DateTime = Parameter_FormDate
            NewDate = False
            InInit = False
        End Sub
    End Class
End Namespace
