#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}
#End Region

Option Compare Binary
Option Explicit On 
Option Strict On

Imports System.ComponentModel
Imports DebtPlus.Utils
Imports System.Data.SqlClient
Imports System.Windows.Forms

Namespace Proposals.Delete
    Friend Class DeleteProposalItems
        Inherits DevExpress.XtraEditors.XtraUserControl

        Public Sub New()
            MyBase.New()
            InitializeComponent()
            AddHandler Button_Apply.Click, AddressOf Button_Apply_Click
            AddHandler Button_OK.Click, AddressOf Button_OK_Click
            AddHandler Button_Cancel.Click, AddressOf Button_Cancel_Click
            AddHandler Button_Remove.Click, AddressOf Button_Remove_Click
            AddHandler Button_All.Click, AddressOf Button_All_Click
            AddHandler GridView1.SelectionChanged, AddressOf GridView1_SelectionChanged
            AddHandler Load, AddressOf MyGridControl_Load
            AddHandler GridView1.Layout, AddressOf LayoutChanged
        End Sub

        Public Event OK_Click As System.EventHandler
        Dim ds As New System.Data.DataSet("proposal_items")

        ''' <summary>
        ''' Selected Proposal Batch ID
        ''' </summary>
        Private _batch As System.Int32 = -1
        <Description("Batch ID selected for the transactions"), Browsable(False), Category("DebtPlus"), DefaultValue(GetType(System.Int32), "-1")> _
        Public Property Batch() As System.Int32
            Get
                Return _batch
            End Get
            Set(ByVal Value As System.Int32)
                _batch = Value
            End Set
        End Property

#Region " Windows Form Designer generated code "

        'UserControl overrides dispose to clean up the component list.
        Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
            If disposing Then
                If components IsNot Nothing Then
                    components.Dispose()
                End If
            End If
            MyBase.Dispose(disposing)
        End Sub

        'Required by the Windows Form Designer
        Private components As IContainer

        'NOTE: The following procedure is required by the Windows Form Designer
        'It can be modified using the Windows Form Designer.  
        'Do not modify it using the code editor.
        Friend WithEvents GridControl1 As DevExpress.XtraGrid.GridControl
        Friend WithEvents GridView1 As DevExpress.XtraGrid.Views.Grid.GridView
        Friend WithEvents GridColumn_client_creditor_proposal As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents GridColumn_client As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents GridColumn_creditor As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents GridColumn_account_number As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents GridColumn_creditor_name As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents GridColumn_date_created As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents Button_Remove As DevExpress.XtraEditors.SimpleButton
        Friend WithEvents Button_All As DevExpress.XtraEditors.SimpleButton
        Friend WithEvents Button_Apply As DevExpress.XtraEditors.SimpleButton
        Friend WithEvents Button_Cancel As DevExpress.XtraEditors.SimpleButton
        Friend WithEvents Button_OK As DevExpress.XtraEditors.SimpleButton
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
            Me.GridControl1 = New DevExpress.XtraGrid.GridControl
            Me.GridView1 = New DevExpress.XtraGrid.Views.Grid.GridView
            Me.GridColumn_client_creditor_proposal = New DevExpress.XtraGrid.Columns.GridColumn
            Me.GridColumn_client_creditor_proposal.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True
            Me.GridColumn_client = New DevExpress.XtraGrid.Columns.GridColumn
            Me.GridColumn_client.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True
            Me.GridColumn_creditor = New DevExpress.XtraGrid.Columns.GridColumn
            Me.GridColumn_creditor.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True
            Me.GridColumn_account_number = New DevExpress.XtraGrid.Columns.GridColumn
            Me.GridColumn_account_number.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True
            Me.GridColumn_creditor_name = New DevExpress.XtraGrid.Columns.GridColumn
            Me.GridColumn_creditor_name.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True
            Me.GridColumn_date_created = New DevExpress.XtraGrid.Columns.GridColumn
            Me.GridColumn_date_created.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True
            Me.Button_Remove = New DevExpress.XtraEditors.SimpleButton
            Me.Button_All = New DevExpress.XtraEditors.SimpleButton
            Me.Button_Apply = New DevExpress.XtraEditors.SimpleButton
            Me.Button_Cancel = New DevExpress.XtraEditors.SimpleButton
            Me.Button_OK = New DevExpress.XtraEditors.SimpleButton
            CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.GridView1, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.SuspendLayout()
            '
            'GridControl1
            '
            Me.GridControl1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                        Or System.Windows.Forms.AnchorStyles.Left) _
                        Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.GridControl1.Location = New System.Drawing.Point(0, 0)
            Me.GridControl1.MainView = Me.GridView1
            Me.GridControl1.Name = "GridControl1"
            Me.GridControl1.Size = New System.Drawing.Size(400, 256)
            Me.GridControl1.TabIndex = 0
            Me.GridControl1.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridView1})
            '
            'GridView1
            '
            Me.GridView1.Appearance.ColumnFilterButton.BackColor = System.Drawing.Color.FromArgb(CType(CType(104, Byte), Int32), CType(CType(184, Byte), Int32), CType(CType(251, Byte), Int32))
            Me.GridView1.Appearance.ColumnFilterButton.BorderColor = System.Drawing.Color.FromArgb(CType(CType(104, Byte), Int32), CType(CType(184, Byte), Int32), CType(CType(251, Byte), Int32))
            Me.GridView1.Appearance.ColumnFilterButton.ForeColor = System.Drawing.Color.White
            Me.GridView1.Appearance.ColumnFilterButton.Options.UseBackColor = True
            Me.GridView1.Appearance.ColumnFilterButton.Options.UseBorderColor = True
            Me.GridView1.Appearance.ColumnFilterButton.Options.UseForeColor = True
            Me.GridView1.Appearance.ColumnFilterButtonActive.BackColor = System.Drawing.Color.FromArgb(CType(CType(170, Byte), Int32), CType(CType(216, Byte), Int32), CType(CType(254, Byte), Int32))
            Me.GridView1.Appearance.ColumnFilterButtonActive.BorderColor = System.Drawing.Color.FromArgb(CType(CType(170, Byte), Int32), CType(CType(216, Byte), Int32), CType(CType(254, Byte), Int32))
            Me.GridView1.Appearance.ColumnFilterButtonActive.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.ColumnFilterButtonActive.Options.UseBackColor = True
            Me.GridView1.Appearance.ColumnFilterButtonActive.Options.UseBorderColor = True
            Me.GridView1.Appearance.ColumnFilterButtonActive.Options.UseForeColor = True
            Me.GridView1.Appearance.Empty.BackColor = System.Drawing.Color.FromArgb(CType(CType(236, Byte), Int32), CType(CType(246, Byte), Int32), CType(CType(255, Byte), Int32))
            Me.GridView1.Appearance.Empty.BackColor2 = System.Drawing.Color.White
            Me.GridView1.Appearance.Empty.Options.UseBackColor = True
            Me.GridView1.Appearance.EvenRow.BackColor = System.Drawing.Color.FromArgb(CType(CType(247, Byte), Int32), CType(CType(251, Byte), Int32), CType(CType(255, Byte), Int32))
            Me.GridView1.Appearance.EvenRow.BorderColor = System.Drawing.Color.FromArgb(CType(CType(247, Byte), Int32), CType(CType(251, Byte), Int32), CType(CType(255, Byte), Int32))
            Me.GridView1.Appearance.EvenRow.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.EvenRow.Options.UseBackColor = True
            Me.GridView1.Appearance.EvenRow.Options.UseBorderColor = True
            Me.GridView1.Appearance.EvenRow.Options.UseForeColor = True
            Me.GridView1.Appearance.FilterCloseButton.BackColor = System.Drawing.Color.FromArgb(CType(CType(104, Byte), Int32), CType(CType(184, Byte), Int32), CType(CType(251, Byte), Int32))
            Me.GridView1.Appearance.FilterCloseButton.BorderColor = System.Drawing.Color.FromArgb(CType(CType(104, Byte), Int32), CType(CType(184, Byte), Int32), CType(CType(251, Byte), Int32))
            Me.GridView1.Appearance.FilterCloseButton.ForeColor = System.Drawing.Color.White
            Me.GridView1.Appearance.FilterCloseButton.Options.UseBackColor = True
            Me.GridView1.Appearance.FilterCloseButton.Options.UseBorderColor = True
            Me.GridView1.Appearance.FilterCloseButton.Options.UseForeColor = True
            Me.GridView1.Appearance.FilterPanel.BackColor = System.Drawing.Color.FromArgb(CType(CType(236, Byte), Int32), CType(CType(246, Byte), Int32), CType(CType(255, Byte), Int32))
            Me.GridView1.Appearance.FilterPanel.BackColor2 = System.Drawing.Color.White
            Me.GridView1.Appearance.FilterPanel.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.FilterPanel.Options.UseBackColor = True
            Me.GridView1.Appearance.FilterPanel.Options.UseForeColor = True
            Me.GridView1.Appearance.FixedLine.BackColor = System.Drawing.Color.FromArgb(CType(CType(59, Byte), Int32), CType(CType(133, Byte), Int32), CType(CType(195, Byte), Int32))
            Me.GridView1.Appearance.FixedLine.Options.UseBackColor = True
            Me.GridView1.Appearance.FocusedCell.BackColor = System.Drawing.Color.White
            Me.GridView1.Appearance.FocusedCell.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.FocusedCell.Options.UseBackColor = True
            Me.GridView1.Appearance.FocusedCell.Options.UseForeColor = True
            Me.GridView1.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb(CType(CType(38, Byte), Int32), CType(CType(109, Byte), Int32), CType(CType(189, Byte), Int32))
            Me.GridView1.Appearance.FocusedRow.BorderColor = System.Drawing.Color.FromArgb(CType(CType(59, Byte), Int32), CType(CType(139, Byte), Int32), CType(CType(206, Byte), Int32))
            Me.GridView1.Appearance.FocusedRow.ForeColor = System.Drawing.Color.White
            Me.GridView1.Appearance.FocusedRow.Options.UseBackColor = True
            Me.GridView1.Appearance.FocusedRow.Options.UseBorderColor = True
            Me.GridView1.Appearance.FocusedRow.Options.UseForeColor = True
            Me.GridView1.Appearance.FooterPanel.BackColor = System.Drawing.Color.FromArgb(CType(CType(104, Byte), Int32), CType(CType(184, Byte), Int32), CType(CType(251, Byte), Int32))
            Me.GridView1.Appearance.FooterPanel.BorderColor = System.Drawing.Color.FromArgb(CType(CType(104, Byte), Int32), CType(CType(184, Byte), Int32), CType(CType(251, Byte), Int32))
            Me.GridView1.Appearance.FooterPanel.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.FooterPanel.Options.UseBackColor = True
            Me.GridView1.Appearance.FooterPanel.Options.UseBorderColor = True
            Me.GridView1.Appearance.FooterPanel.Options.UseForeColor = True
            Me.GridView1.Appearance.GroupButton.BackColor = System.Drawing.Color.FromArgb(CType(CType(104, Byte), Int32), CType(CType(184, Byte), Int32), CType(CType(251, Byte), Int32))
            Me.GridView1.Appearance.GroupButton.BorderColor = System.Drawing.Color.FromArgb(CType(CType(104, Byte), Int32), CType(CType(184, Byte), Int32), CType(CType(251, Byte), Int32))
            Me.GridView1.Appearance.GroupButton.Options.UseBackColor = True
            Me.GridView1.Appearance.GroupButton.Options.UseBorderColor = True
            Me.GridView1.Appearance.GroupFooter.BackColor = System.Drawing.Color.FromArgb(CType(CType(170, Byte), Int32), CType(CType(216, Byte), Int32), CType(CType(254, Byte), Int32))
            Me.GridView1.Appearance.GroupFooter.BorderColor = System.Drawing.Color.FromArgb(CType(CType(170, Byte), Int32), CType(CType(216, Byte), Int32), CType(CType(254, Byte), Int32))
            Me.GridView1.Appearance.GroupFooter.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.GroupFooter.Options.UseBackColor = True
            Me.GridView1.Appearance.GroupFooter.Options.UseBorderColor = True
            Me.GridView1.Appearance.GroupFooter.Options.UseForeColor = True
            Me.GridView1.Appearance.GroupPanel.BackColor = System.Drawing.Color.FromArgb(CType(CType(236, Byte), Int32), CType(CType(246, Byte), Int32), CType(CType(255, Byte), Int32))
            Me.GridView1.Appearance.GroupPanel.BackColor2 = System.Drawing.Color.White
            Me.GridView1.Appearance.GroupPanel.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.GroupPanel.Options.UseBackColor = True
            Me.GridView1.Appearance.GroupPanel.Options.UseForeColor = True
            Me.GridView1.Appearance.GroupRow.BackColor = System.Drawing.Color.FromArgb(CType(CType(170, Byte), Int32), CType(CType(216, Byte), Int32), CType(CType(254, Byte), Int32))
            Me.GridView1.Appearance.GroupRow.BorderColor = System.Drawing.Color.FromArgb(CType(CType(170, Byte), Int32), CType(CType(216, Byte), Int32), CType(CType(254, Byte), Int32))
            Me.GridView1.Appearance.GroupRow.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.GroupRow.Options.UseBackColor = True
            Me.GridView1.Appearance.GroupRow.Options.UseBorderColor = True
            Me.GridView1.Appearance.GroupRow.Options.UseForeColor = True
            Me.GridView1.Appearance.HeaderPanel.BackColor = System.Drawing.Color.FromArgb(CType(CType(139, Byte), Int32), CType(CType(201, Byte), Int32), CType(CType(254, Byte), Int32))
            Me.GridView1.Appearance.HeaderPanel.BorderColor = System.Drawing.Color.FromArgb(CType(CType(139, Byte), Int32), CType(CType(201, Byte), Int32), CType(CType(254, Byte), Int32))
            Me.GridView1.Appearance.HeaderPanel.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.HeaderPanel.Options.UseBackColor = True
            Me.GridView1.Appearance.HeaderPanel.Options.UseBorderColor = True
            Me.GridView1.Appearance.HeaderPanel.Options.UseForeColor = True
            Me.GridView1.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Int32), CType(CType(170, Byte), Int32), CType(CType(225, Byte), Int32))
            Me.GridView1.Appearance.HideSelectionRow.BorderColor = System.Drawing.Color.FromArgb(CType(CType(83, Byte), Int32), CType(CType(155, Byte), Int32), CType(CType(215, Byte), Int32))
            Me.GridView1.Appearance.HideSelectionRow.ForeColor = System.Drawing.Color.FromArgb(CType(CType(236, Byte), Int32), CType(CType(246, Byte), Int32), CType(CType(255, Byte), Int32))
            Me.GridView1.Appearance.HideSelectionRow.Options.UseBackColor = True
            Me.GridView1.Appearance.HideSelectionRow.Options.UseBorderColor = True
            Me.GridView1.Appearance.HideSelectionRow.Options.UseForeColor = True
            Me.GridView1.Appearance.HorzLine.BackColor = System.Drawing.Color.FromArgb(CType(CType(104, Byte), Int32), CType(CType(184, Byte), Int32), CType(CType(251, Byte), Int32))
            Me.GridView1.Appearance.HorzLine.Options.UseBackColor = True
            Me.GridView1.Appearance.OddRow.BackColor = System.Drawing.Color.FromArgb(CType(CType(236, Byte), Int32), CType(CType(246, Byte), Int32), CType(CType(255, Byte), Int32))
            Me.GridView1.Appearance.OddRow.BorderColor = System.Drawing.Color.FromArgb(CType(CType(236, Byte), Int32), CType(CType(246, Byte), Int32), CType(CType(255, Byte), Int32))
            Me.GridView1.Appearance.OddRow.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.OddRow.Options.UseBackColor = True
            Me.GridView1.Appearance.OddRow.Options.UseBorderColor = True
            Me.GridView1.Appearance.OddRow.Options.UseForeColor = True
            Me.GridView1.Appearance.Preview.Font = New System.Drawing.Font("Verdana", 7.5!)
            Me.GridView1.Appearance.Preview.ForeColor = System.Drawing.Color.FromArgb(CType(CType(83, Byte), Int32), CType(CType(155, Byte), Int32), CType(CType(215, Byte), Int32))
            Me.GridView1.Appearance.Preview.Options.UseFont = True
            Me.GridView1.Appearance.Preview.Options.UseForeColor = True
            Me.GridView1.Appearance.Row.BackColor = System.Drawing.Color.FromArgb(CType(CType(247, Byte), Int32), CType(CType(251, Byte), Int32), CType(CType(255, Byte), Int32))
            Me.GridView1.Appearance.Row.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.Row.Options.UseBackColor = True
            Me.GridView1.Appearance.Row.Options.UseForeColor = True
            Me.GridView1.Appearance.RowSeparator.BackColor = System.Drawing.Color.FromArgb(CType(CType(236, Byte), Int32), CType(CType(246, Byte), Int32), CType(CType(255, Byte), Int32))
            Me.GridView1.Appearance.RowSeparator.BackColor2 = System.Drawing.Color.White
            Me.GridView1.Appearance.RowSeparator.Options.UseBackColor = True
            Me.GridView1.Appearance.SelectedRow.BackColor = System.Drawing.Color.FromArgb(CType(CType(83, Byte), Int32), CType(CType(155, Byte), Int32), CType(CType(215, Byte), Int32))
            Me.GridView1.Appearance.SelectedRow.ForeColor = System.Drawing.Color.White
            Me.GridView1.Appearance.SelectedRow.Options.UseBackColor = True
            Me.GridView1.Appearance.SelectedRow.Options.UseForeColor = True
            Me.GridView1.Appearance.TopNewRow.BackColor = System.Drawing.Color.White
            Me.GridView1.Appearance.TopNewRow.Options.UseBackColor = True
            Me.GridView1.Appearance.VertLine.BackColor = System.Drawing.Color.FromArgb(CType(CType(104, Byte), Int32), CType(CType(184, Byte), Int32), CType(CType(251, Byte), Int32))
            Me.GridView1.Appearance.VertLine.Options.UseBackColor = True
            Me.GridView1.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.GridColumn_client_creditor_proposal, Me.GridColumn_client, Me.GridColumn_creditor, Me.GridColumn_account_number, Me.GridColumn_creditor_name, Me.GridColumn_date_created})
            Me.GridView1.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus
            Me.GridView1.GridControl = Me.GridControl1
            Me.GridView1.Name = "GridView1"
            Me.GridView1.OptionsBehavior.Editable = False
            Me.GridView1.OptionsLayout.Columns.StoreAllOptions = True
            Me.GridView1.OptionsLayout.Columns.StoreAppearance = True
            Me.GridView1.OptionsLayout.StoreAllOptions = True
            Me.GridView1.OptionsLayout.StoreAppearance = True
            Me.GridView1.OptionsSelection.EnableAppearanceFocusedCell = False
            Me.GridView1.OptionsSelection.MultiSelect = True
            Me.GridView1.OptionsView.ShowGroupPanel = False
            Me.GridView1.OptionsView.ShowIndicator = False
            Me.GridView1.ShowButtonMode = DevExpress.XtraGrid.Views.Base.ShowButtonModeEnum.ShowForFocusedRow
            Me.GridView1.ViewCaption = "This is the view caption area"
            '
            'GridColumn_client_creditor_proposal
            '
            Me.GridColumn_client_creditor_proposal.AppearanceCell.Options.UseTextOptions = True
            Me.GridColumn_client_creditor_proposal.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.GridColumn_client_creditor_proposal.AppearanceHeader.Options.UseTextOptions = True
            Me.GridColumn_client_creditor_proposal.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.GridColumn_client_creditor_proposal.Caption = "ID"
            Me.GridColumn_client_creditor_proposal.CustomizationCaption = "Record ID"
            Me.GridColumn_client_creditor_proposal.DisplayFormat.FormatString = "f0"
            Me.GridColumn_client_creditor_proposal.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.GridColumn_client_creditor_proposal.FieldName = "client_creditor_proposal"
            Me.GridColumn_client_creditor_proposal.GroupFormat.FormatString = "f0"
            Me.GridColumn_client_creditor_proposal.GroupFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.GridColumn_client_creditor_proposal.Name = "GridColumn_client_creditor_proposal"
            Me.GridColumn_client_creditor_proposal.OptionsColumn.AllowEdit = False
            Me.GridColumn_client_creditor_proposal.ToolTip = "Record ID for the proposal record"
            '
            'GridColumn_client
            '
            Me.GridColumn_client.AppearanceCell.Options.UseTextOptions = True
            Me.GridColumn_client.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.GridColumn_client.AppearanceHeader.Options.UseTextOptions = True
            Me.GridColumn_client.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.GridColumn_client.Caption = "Client"
            Me.GridColumn_client.CustomizationCaption = "Client ID"
            Me.GridColumn_client.DisplayFormat.FormatString = "0000000"
            Me.GridColumn_client.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom
            Me.GridColumn_client.FieldName = "client"
            Me.GridColumn_client.GroupFormat.FormatString = "0000000"
            Me.GridColumn_client.GroupFormat.FormatType = DevExpress.Utils.FormatType.Custom
            Me.GridColumn_client.Name = "GridColumn_client"
            Me.GridColumn_client.ToolTip = "Client ID"
            Me.GridColumn_client.Visible = True
            Me.GridColumn_client.VisibleIndex = 0
            Me.GridColumn_client.Width = 61
            '
            'GridColumn_creditor
            '
            Me.GridColumn_creditor.Caption = "Creditor"
            Me.GridColumn_creditor.CustomizationCaption = "Creditor"
            Me.GridColumn_creditor.FieldName = "creditor"
            Me.GridColumn_creditor.Name = "GridColumn_creditor"
            Me.GridColumn_creditor.ToolTip = "Creditor ID"
            Me.GridColumn_creditor.Visible = True
            Me.GridColumn_creditor.VisibleIndex = 1
            Me.GridColumn_creditor.Width = 60
            '
            'GridColumn_account_number
            '
            Me.GridColumn_account_number.Caption = "Account"
            Me.GridColumn_account_number.CustomizationCaption = "Account Number"
            Me.GridColumn_account_number.FieldName = "account_number"
            Me.GridColumn_account_number.Name = "GridColumn_account_number"
            Me.GridColumn_account_number.ToolTip = "Account Number"
            Me.GridColumn_account_number.Visible = True
            Me.GridColumn_account_number.VisibleIndex = 2
            Me.GridColumn_account_number.Width = 116
            '
            'GridColumn_creditor_name
            '
            Me.GridColumn_creditor_name.Caption = "Creditor Name"
            Me.GridColumn_creditor_name.CustomizationCaption = "Creditor Name"
            Me.GridColumn_creditor_name.FieldName = "creditor_name"
            Me.GridColumn_creditor_name.Name = "GridColumn_creditor_name"
            Me.GridColumn_creditor_name.ToolTip = "Creditor Name"
            Me.GridColumn_creditor_name.Visible = True
            Me.GridColumn_creditor_name.VisibleIndex = 3
            Me.GridColumn_creditor_name.Width = 147
            '
            'GridColumn_date_created
            '
            Me.GridColumn_date_created.AppearanceCell.Options.UseTextOptions = True
            Me.GridColumn_date_created.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.GridColumn_date_created.AppearanceHeader.Options.UseTextOptions = True
            Me.GridColumn_date_created.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.GridColumn_date_created.Caption = "Date Created"
            Me.GridColumn_date_created.CustomizationCaption = "Date Created"
            Me.GridColumn_date_created.DisplayFormat.FormatString = "d"
            Me.GridColumn_date_created.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime
            Me.GridColumn_date_created.FieldName = "date_created"
            Me.GridColumn_date_created.GroupFormat.FormatString = "d"
            Me.GridColumn_date_created.GroupFormat.FormatType = DevExpress.Utils.FormatType.DateTime
            Me.GridColumn_date_created.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.DateMonth
            Me.GridColumn_date_created.Name = "GridColumn_date_created"
            Me.GridColumn_date_created.ToolTip = "Date/Time when proposal was created"
            Me.GridColumn_date_created.Visible = True
            Me.GridColumn_date_created.VisibleIndex = 4
            Me.GridColumn_date_created.Width = 101
            '
            'Button_Remove
            '
            Me.Button_Remove.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.Button_Remove.Enabled = False
            Me.Button_Remove.Location = New System.Drawing.Point(408, 101)
            Me.Button_Remove.Name = "Button_Remove"
            Me.Button_Remove.Size = New System.Drawing.Size(75, 23)
            Me.Button_Remove.TabIndex = 3
            Me.Button_Remove.Text = "Remove"
            Me.Button_Remove.ToolTip = "Click here to remove the selected items from the list"
            '
            'Button_All
            '
            Me.Button_All.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.Button_All.Location = New System.Drawing.Point(408, 133)
            Me.Button_All.Name = "Button_All"
            Me.Button_All.Size = New System.Drawing.Size(75, 23)
            Me.Button_All.TabIndex = 4
            Me.Button_All.Tag = "1"
            Me.Button_All.Text = "Select All"
            Me.Button_All.ToolTip = "Select all of the items in the list"
            '
            'Button_Apply
            '
            Me.Button_Apply.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.Button_Apply.Enabled = False
            Me.Button_Apply.Location = New System.Drawing.Point(408, 216)
            Me.Button_Apply.Name = "Button_Apply"
            Me.Button_Apply.Size = New System.Drawing.Size(75, 23)
            Me.Button_Apply.TabIndex = 5
            Me.Button_Apply.Text = "Apply"
            Me.Button_Apply.ToolTip = "Click here to apply the deletions and continue to work this batch"
            '
            'Button_Cancel
            '
            Me.Button_Cancel.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.Button_Cancel.Location = New System.Drawing.Point(408, 40)
            Me.Button_Cancel.Name = "Button_Cancel"
            Me.Button_Cancel.Size = New System.Drawing.Size(75, 23)
            Me.Button_Cancel.TabIndex = 2
            Me.Button_Cancel.Tag = ""
            Me.Button_Cancel.Text = "&Cancel"
            Me.Button_Cancel.ToolTip = "Click here to cancel any deletions (leaving them still in the batch) and return t" & _
                "o the list of batches"
            '
            'Button_OK
            '
            Me.Button_OK.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.Button_OK.Enabled = False
            Me.Button_OK.Location = New System.Drawing.Point(408, 8)
            Me.Button_OK.Name = "Button_OK"
            Me.Button_OK.Size = New System.Drawing.Size(75, 23)
            Me.Button_OK.TabIndex = 1
            Me.Button_OK.Text = "&OK"
            Me.Button_OK.ToolTip = "Click here to apply the changes and return to the list of batches"
            '
            'DeleteProposalItems
            '
            Me.Controls.Add(Me.Button_Cancel)
            Me.Controls.Add(Me.Button_OK)
            Me.Controls.Add(Me.Button_Apply)
            Me.Controls.Add(Me.Button_All)
            Me.Controls.Add(Me.Button_Remove)
            Me.Controls.Add(Me.GridControl1)
            Me.Name = "DeleteProposalItems"
            Me.Size = New System.Drawing.Size(488, 256)
            CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.GridView1, System.ComponentModel.ISupportInitialize).EndInit()
            Me.ResumeLayout(False)

        End Sub

#End Region

        ''' <summary>
        ''' Load the list of batches for the ACH files
        ''' </summary>
        <Browsable(False)> _
        Public Sub Process()
            ds.Clear()

            Dim current_cursor As Cursor = System.Windows.Forms.Cursor.Current
            Try
                System.Windows.Forms.Cursor.Current = Cursors.WaitCursor
                Using cmd As SqlClient.SqlCommand = New SqlCommand
                    With cmd
                        .Connection = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
                        .CommandText = "SELECT client_creditor_proposal, client, creditor, client_creditor, date_created, creditor_name, account_number FROM view_proposal_proof WITH (NOLOCK) WHERE proposal_batch_id=@proposal_batch_id"
                        .Parameters.Add("@proposal_batch_id", SqlDbType.Int).Value = Batch
                    End With

                    Using da As New SqlClient.SqlDataAdapter(cmd)
                        da.Fill(ds, "proposal_items")
                    End Using
                End Using

                ' Bind the data to the grid
                With GridControl1
                    .DataSource = ds.Tables(0).DefaultView
                    .RefreshDataSource()
                End With

                ' Enable the ALL button if there is something to process
                Button_All.Enabled = GridView1.RowCount > 0
                Button_Apply.Enabled = False
                Button_OK.Enabled = False

            Catch ex As SqlClient.SqlException
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error reading Proposal Batch Items")

            Finally
                System.Windows.Forms.Cursor.Current = current_cursor
            End Try
        End Sub

        ''' <summary>
        ''' Process the apply button
        ''' </summary>
        Private Sub Button_Apply_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
            SaveChanges()
        End Sub

        ''' <summary>
        ''' Process the OK button
        ''' </summary>
        Private Sub Button_OK_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
            SaveChanges()
            RaiseEvent OK_Click(Me, EventArgs.Empty)
        End Sub

        ''' <summary>
        ''' Process the CANCEL button
        ''' </summary>
        Private Sub Button_Cancel_Click(ByVal sender As Object, ByVal e As System.EventArgs)
            RaiseEvent OK_Click(Me, EventArgs.Empty)
        End Sub

        ''' <summary>
        ''' Process the CANCEL button
        ''' </summary>
        Private Sub Button_Remove_Click(ByVal sender As Object, ByVal e As System.EventArgs)

            ' Delete the rows from the table. The actual deletion will be done when we do the apply.
            Do While GridView1.SelectedRowsCount > 0
                Dim RowHandle As System.Int32 = GridView1.GetSelectedRows(0)
                Dim row As System.Data.DataRow = GridView1.GetDataRow(RowHandle)
                If row IsNot Nothing Then row.Delete()
            Loop

            ' Disable the apply/ok buttons
            Button_Apply.Enabled = HasChanges()
            Button_OK.Enabled = Button_Apply.Enabled
        End Sub

        ''' <summary>
        ''' Process the Select All/Clear All conditions
        ''' </summary>
        Private Sub Button_All_Click(ByVal sender As Object, ByVal e As System.EventArgs)

            With Button_All
                If Convert.ToInt32(.Tag) = 0 Then
                    GridView1.ClearSelection()
                Else
                    GridView1.SelectAll()
                End If
            End With
        End Sub

        ''' <summary>
        ''' Save any changes to the database
        ''' </summary>
        Friend Sub SaveChanges()
            Dim current_cursor As Cursor = System.Windows.Forms.Cursor.Current
            Try
                ' Build a command to delete the proposals from the batch
                Using cmd As SqlClient.SqlCommand = New SqlCommand
                    With cmd
                        .Connection = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
                        .CommandText = "xpr_proposal_item_remove"
                        .CommandType = CommandType.StoredProcedure
                        .Parameters.Add("@client_creditor_proposal", SqlDbType.Int, 0, "client_creditor_proposal")
                    End With

                    ' Do the delete operation to remove the item(s) from the batch.
                    Using da As New SqlClient.SqlDataAdapter
                        da.DeleteCommand = cmd
                        System.Windows.Forms.Cursor.Current = Cursors.WaitCursor
                        If da.Update(ds.Tables(0)) > 0 Then Microsoft.VisualBasic.Beep()
                    End Using
                End Using

            Catch ex As SqlClient.SqlException
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error clearing proposal from batch")

            Finally
                System.Windows.Forms.Cursor.Current = current_cursor
            End Try

            ' Disable the apply/ok buttons
            Button_Apply.Enabled = HasChanges()
            Button_OK.Enabled = Button_Apply.Enabled
        End Sub

        ''' <summary>
        ''' Determine if the database has any changes
        ''' </summary>
        Private Function HasChanges() As Boolean

            ' Determine if there are any deleted items in the update table
            Using vue As New System.Data.DataView(ds.Tables(0), String.Empty, String.Empty, DataViewRowState.Deleted)
                Return vue.Count > 0
            End Using
        End Function

        ''' <summary>
        ''' If the selection changes then change the select button
        ''' </summary>
        Private Sub GridView1_SelectionChanged(ByVal sender As Object, ByVal e As DevExpress.Data.SelectionChangedEventArgs)

            With Button_All
                If GridView1.GetSelectedRows.GetLength(0) > 0 Then
                    Button_Remove.Enabled = True
                    .Text = "Clear All"
                    .Tag = 0
                    .ToolTip = "Click here to deselect (un-highlight) all proposals in the list"
                Else
                    Button_Remove.Enabled = False
                    .Text = "Select All"
                    .Tag = 1
                    .ToolTip = "Click here to select (highlight) all proposals in the list"
                End If
            End With
        End Sub

        Private InInit As Boolean = True

        ''' <summary>
        ''' Return the directory to the saved layout information
        ''' </summary>
        Protected Overridable Function XMLBasePath() As String
            If DesignMode OrElse ParentForm Is Nothing Then Return String.Empty
            Dim BasePath As String = String.Format("{0}{1}DebtPlus", System.Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData), System.IO.Path.DirectorySeparatorChar)
            Return System.IO.Path.Combine(BasePath, "Proposals.Delete")
        End Function

        ''' <summary>
        ''' When loading the control, restore the layout from the file
        ''' </summary>
        Private Sub MyGridControl_Load(ByVal sender As Object, ByVal e As System.EventArgs)
            ReloadGridControlLayout()
            InInit = False
        End Sub

        ''' <summary>
        ''' Reload the layout of the grid control if needed
        ''' </summary>
        Protected Sub ReloadGridControlLayout()

            ' Find the base path to the saved file location
            Dim PathName As String = XMLBasePath()
            If Not String.IsNullOrEmpty(PathName) Then
                InInit = True
                Try
                    Dim FileName As String = System.IO.Path.Combine(PathName, String.Format("{0}.Grid.xml", Name))
                    If System.IO.File.Exists(FileName) Then
                        GridView1.RestoreLayoutFromXml(FileName)
                    End If
                Catch ex As System.IO.DirectoryNotFoundException
                Catch ex As System.IO.FileNotFoundException

                Finally
                    InInit = False
                End Try
            End If
        End Sub

        ''' <summary>
        ''' If the layout is changed then update the file
        ''' </summary>
        Private Sub LayoutChanged(ByVal Sender As Object, ByVal e As System.EventArgs)
            Dim PathName As String = XMLBasePath()
            If Not InInit AndAlso Not String.IsNullOrEmpty(PathName) Then
                If Not System.IO.Directory.Exists(PathName) Then
                    System.IO.Directory.CreateDirectory(PathName)
                End If

                Dim FileName As String = System.IO.Path.Combine(PathName, String.Format("{0}.Grid.xml", Name))

                GridView1.SaveLayoutToXml(FileName)
            End If
        End Sub
    End Class
End Namespace
