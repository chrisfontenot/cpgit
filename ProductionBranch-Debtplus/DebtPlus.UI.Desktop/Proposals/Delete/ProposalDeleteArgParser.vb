Option Compare Binary
Option Explicit On 
Option Strict On

Namespace Proposals.Delete
    Friend Class ProposalDeleteArgParser
        Inherits DebtPlus.Utils.ArgParserBase

        Public Sub New()
            MyBase.New(New String() {})
        End Sub

        Protected Overrides Sub OnUsage(ByVal errorInfo As String)
            If errorInfo IsNot Nothing Then
                AppendErrorLine(String.Format("Invalid parameter: {0}" + Environment.NewLine, errorInfo))
            End If

            AppendErrorLine("Usage: DebtPlus.Proposals.Delete.exe")
        End Sub
    End Class
End Namespace