#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}
#End Region

Option Compare Binary
Option Explicit On 
Option Strict On

Imports DebtPlus.UI.FormLib.Proposals

Namespace Proposals.Delete
    Friend Class Form_Proposals_Delete
        Inherits DebtPlus.Data.Forms.DebtPlusForm

        Private Batch As System.Int32 = -1

        Public Sub New()
            MyBase.New()
            InitializeComponent()
            AddHandler Me.Load, AddressOf Proposals_Delete_Load
            AddHandler ProposalBatchSelect1.Cancelled, AddressOf ProposalBatchSelect1_Cancelled
            AddHandler ProposalBatchSelect1.Selected, AddressOf ProposalBatchSelect1_Selected
            AddHandler MyBase.Closing, AddressOf Proposals_Delete_Closing
            AddHandler DeleteProposalItems1.OK_Click, AddressOf DeleteProposalItems1_OK_Click
            AddHandler DeleteProposalItems1.Load, AddressOf DeleteProposalItems1_Load
        End Sub

#Region " Windows Form Designer generated code "

        'Form overrides dispose to clean up the component list.
        Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
            If disposing Then
                If components IsNot Nothing Then
                    components.Dispose()
                End If
            End If
            MyBase.Dispose(disposing)
        End Sub

        'Required by the Windows Form Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Windows Form Designer
        'It can be modified using the Windows Form Designer.  
        'Do not modify it using the code editor.
        Friend WithEvents ProposalBatchSelect1 As OpenControl
        Friend WithEvents DeleteProposalItems1 As DeleteProposalItems
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
            Me.ProposalBatchSelect1 = New OpenControl
            Me.DeleteProposalItems1 = New DeleteProposalItems
            CType(Me.DxErrorProvider1, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.SuspendLayout()
            '
            'ProposalBatchSelect1
            '
            Me.ProposalBatchSelect1.Dock = System.Windows.Forms.DockStyle.Fill
            Me.ProposalBatchSelect1.Location = New System.Drawing.Point(0, 0)
            Me.ProposalBatchSelect1.Name = "ProposalBatchSelect1"
            Me.ProposalBatchSelect1.Size = New System.Drawing.Size(577, 266)
            Me.ProposalBatchSelect1.TabIndex = 0
            '
            'DeleteProposalItems1
            '
            Me.DeleteProposalItems1.Dock = System.Windows.Forms.DockStyle.Fill
            Me.DeleteProposalItems1.Location = New System.Drawing.Point(0, 0)
            Me.DeleteProposalItems1.Name = "DeleteProposalItems1"
            Me.DeleteProposalItems1.Size = New System.Drawing.Size(577, 266)
            Me.DeleteProposalItems1.TabIndex = 1
            Me.DeleteProposalItems1.Visible = False
            '
            'Proposals_Delete
            '
            Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
            Me.ClientSize = New System.Drawing.Size(577, 266)
            Me.Controls.Add(Me.DeleteProposalItems1)
            Me.Controls.Add(Me.ProposalBatchSelect1)
            Me.Name = "Proposals_Delete"
            Me.Text = "List Of Proposals In The Batch"
            CType(Me.DxErrorProvider1, System.ComponentModel.ISupportInitialize).EndInit()
            Me.ResumeLayout(False)

        End Sub

#End Region

        Private Sub Proposals_Delete_Load(ByVal sender As System.Object, ByVal e As System.EventArgs)

            ' Request the proposal batch to be processed
            ProposalBatchSelect1.RefreshList()
            Text = "Select the Proposal Batch"
        End Sub

        Private Sub ProposalBatchSelect1_Cancelled(ByVal sender As Object, ByVal e As System.EventArgs)
            Close()
        End Sub

        Private Sub ProposalBatchSelect1_Selected(ByVal sender As Object, ByVal e As System.EventArgs)

            ' Process the information from the sub-control
            Dim batch As System.Int32
            With ProposalBatchSelect1
                batch = .Batch_ID
                .Visible = False
            End With

            ' Start the processing for the batch selection information
            With DeleteProposalItems1
                .Visible = True
                .Batch = batch
                .Process()
            End With

            Text = "Delete Proposals From Batch"
        End Sub

        Private Sub Proposals_Delete_Closing(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs)

            ' If we are closing then apply the changes to the database
            If DeleteProposalItems1.Visible Then DeleteProposalItems1.SaveChanges()

        End Sub

        Private Sub DeleteProposalItems1_OK_Click(ByVal sender As Object, ByVal e As System.EventArgs)
            DeleteProposalItems1.Visible = False
            ProposalBatchSelect1.Visible = True
            Text = "Select the Proposal Batch"
        End Sub

        Private Sub DeleteProposalItems1_Load(ByVal sender As System.Object, ByVal e As System.EventArgs)
            DeleteProposalItems1.Button_Apply.Visible = False
        End Sub
    End Class
End Namespace
