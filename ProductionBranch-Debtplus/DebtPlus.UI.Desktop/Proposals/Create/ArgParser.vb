Option Compare Binary
Option Explicit On 
Option Strict On

Imports System.Windows.Forms

Namespace Proposals.Create

    Public Class ArgParser
        Inherits DebtPlus.Utils.ArgParserBase

        ''' <summary>
        ''' Processing mode values
        ''' </summary>
        Public Enum ModeEnum
            [Unspecified] = 0
            [Create] = 1
            [Close] = 2
        End Enum

        Private _mode As ModeEnum = ModeEnum.Unspecified

        ''' <summary>
        ''' Processing mode
        ''' </summary>
        Public ReadOnly Property Mode() As ModeEnum
            Get
                Return _mode
            End Get
        End Property

        ''' <summary>
        ''' Create an instance of our class
        ''' </summary>
        Public Sub New()
            MyBase.New(New String() {})
        End Sub

        ''' <summary>
        ''' Generate the command usage information
        ''' </summary>
        Protected Overrides Sub OnUsage(ByVal errorInfo As String)
            'AppendErrorLine("Usage: " + fname + " CREATE | CLOSE ")
        End Sub

        ''' <summary>
        ''' The parsing operation is complete
        ''' </summary>
        Protected Overrides Function OnDoneParse() As SwitchStatus
            Dim ss As SwitchStatus = DebtPlus.Utils.ArgParserBase.SwitchStatus.NoError

            If Mode = ModeEnum.Unspecified Then
                With New ModeSelectForm
                    If .ShowDialog() = DialogResult.OK Then _mode = CType(.Parameter_Mode, ModeEnum)
                    .Dispose()
                End With
            End If

            If Mode = ModeEnum.Unspecified Then ss = DebtPlus.Utils.ArgParserBase.SwitchStatus.YesError
            Return ss
        End Function

        ''' <summary>
        ''' Process a non-switch argument
        ''' </summary>
        Protected Overrides Function OnNonSwitch(ByVal value As String) As SwitchStatus
            Dim ss As SwitchStatus = DebtPlus.Utils.ArgParserBase.SwitchStatus.NoError

            Select Case value.ToLower().Trim()
                Case "create"
                    _mode = ModeEnum.Create
                Case "close"
                    _mode = ModeEnum.Close
                Case Else
                    ss = DebtPlus.Utils.ArgParserBase.SwitchStatus.ShowUsage
            End Select

            Return ss
        End Function
    End Class
End Namespace
