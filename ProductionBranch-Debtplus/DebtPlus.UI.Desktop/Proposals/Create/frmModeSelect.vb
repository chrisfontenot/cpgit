#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}
#End Region

Option Compare Binary
Option Strict Off
Option Explicit On 

Namespace Proposals.Create
    Friend Class ModeSelectForm
        Inherits DebtPlus.Data.Forms.DebtPlusForm

        Public Sub New()
            MyBase.New()
            InitializeComponent()
            AddHandler Me.Load, AddressOf ModeSelectForm_Load
        End Sub

#Region "Windows Form Designer generated code "

        'Form overrides dispose to clean up the component list.
        Protected Overloads Overrides Sub Dispose(ByVal Disposing As Boolean)
            If Disposing Then
                If components IsNot Nothing Then
                    components.Dispose()
                End If
            End If
            MyBase.Dispose(Disposing)
        End Sub

        'Required by the Windows Form Designer
        Private components As System.ComponentModel.IContainer
        Public WithEvents Label1 As DevExpress.XtraEditors.LabelControl
        'NOTE: The following procedure is required by the Windows Form Designer
        'It can be modified using the Windows Form Designer.
        'Do not modify it using the code editor.
        Friend WithEvents lst_mode As DevExpress.XtraEditors.ComboBoxEdit
        Friend WithEvents Button_OK As DevExpress.XtraEditors.SimpleButton
        Friend WithEvents Button_Cancel As DevExpress.XtraEditors.SimpleButton
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
            Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(ModeSelectForm))
            Me.Label1 = New DevExpress.XtraEditors.LabelControl
            Me.lst_mode = New DevExpress.XtraEditors.ComboBoxEdit
            Me.Button_OK = New DevExpress.XtraEditors.SimpleButton
            Me.Button_Cancel = New DevExpress.XtraEditors.SimpleButton
            CType(Me.DxErrorProvider1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.lst_mode.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.SuspendLayout()
            '
            'Label1
            '
            Me.Label1.Appearance.BackColor = System.Drawing.SystemColors.Control
            Me.Label1.Appearance.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.Label1.Appearance.ForeColor = System.Drawing.SystemColors.ControlText
            Me.Label1.Appearance.Options.UseBackColor = True
            Me.Label1.Appearance.Options.UseFont = True
            Me.Label1.Appearance.Options.UseForeColor = True
            Me.Label1.Appearance.Options.UseTextOptions = True
            Me.Label1.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
            Me.Label1.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Top
            Me.Label1.Cursor = System.Windows.Forms.Cursors.Default
            Me.Label1.Location = New System.Drawing.Point(16, 12)
            Me.Label1.Name = "Label1"
            Me.Label1.Size = New System.Drawing.Size(215, 14)
            Me.Label1.TabIndex = 3
            Me.Label1.Text = "What type of processing do you wish to do?"
            Me.Label1.ToolTipController = Me.ToolTipController1
            '
            'lst_mode
            '
            Me.lst_mode.Location = New System.Drawing.Point(16, 48)
            Me.lst_mode.Name = "lst_mode"
            Me.lst_mode.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.lst_mode.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
            Me.lst_mode.Size = New System.Drawing.Size(273, 20)
            Me.lst_mode.TabIndex = 4
            Me.lst_mode.ToolTipController = Me.ToolTipController1
            '
            'Button_OK
            '
            Me.Button_OK.DialogResult = System.Windows.Forms.DialogResult.OK
            Me.Button_OK.Location = New System.Drawing.Point(58, 88)
            Me.Button_OK.Name = "Button_OK"
            Me.Button_OK.Size = New System.Drawing.Size(75, 23)
            Me.Button_OK.TabIndex = 5
            Me.Button_OK.Text = "&OK"
            Me.Button_OK.ToolTipController = Me.ToolTipController1
            '
            'Button_Cancel
            '
            Me.Button_Cancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
            Me.Button_Cancel.Location = New System.Drawing.Point(178, 88)
            Me.Button_Cancel.Name = "Button_Cancel"
            Me.Button_Cancel.Size = New System.Drawing.Size(75, 23)
            Me.Button_Cancel.TabIndex = 6
            Me.Button_Cancel.Text = "&Cancel"
            Me.Button_Cancel.ToolTipController = Me.ToolTipController1
            '
            'ModeSelectForm
            '
            Me.AcceptButton = Me.Button_OK
            Me.Appearance.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.Appearance.Options.UseFont = True
            Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
            Me.CancelButton = Me.Button_Cancel
            Me.ClientSize = New System.Drawing.Size(311, 128)
            Me.Controls.Add(Me.Button_Cancel)
            Me.Controls.Add(Me.Button_OK)
            Me.Controls.Add(Me.lst_mode)
            Me.Controls.Add(Me.Label1)
            Me.Cursor = System.Windows.Forms.Cursors.Default
            Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
            Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
            Me.Location = New System.Drawing.Point(3, 22)
            Me.MaximizeBox = False
            Me.MinimizeBox = False
            Me.Name = "ModeSelectForm"
            Me.RightToLeft = System.Windows.Forms.RightToLeft.No
            Me.ToolTipController1.SetSuperTip(Me, Nothing)
            Me.Text = "Select the processing mode"
            CType(Me.DxErrorProvider1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.lst_mode.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            Me.ResumeLayout(False)
            Me.PerformLayout()

        End Sub
#End Region

        Public ReadOnly Property Parameter_Mode() As System.Int32
            Get
                With lst_mode
                    Return Convert.ToInt32(CType(.SelectedItem, DebtPlus.Data.Controls.ComboboxItem).value)
                End With
            End Get
        End Property

        Private Sub ModeSelectForm_Load(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs)

            lst_mode.Properties.Items.Clear()
            lst_mode.Properties.Items.Add(New DebtPlus.Data.Controls.ComboboxItem("Create a new proposal batch", 1))
            lst_mode.Properties.Items.Add(New DebtPlus.Data.Controls.ComboboxItem("Close the batch", 2))
            lst_mode.SelectedIndex = 0

        End Sub
    End Class
End Namespace