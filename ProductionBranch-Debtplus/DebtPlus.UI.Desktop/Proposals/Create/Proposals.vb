#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}
#End Region

Option Compare Binary
Option Strict Off
Option Explicit On

Imports DebtPlus.UI.FormLib.Proposals
Imports System.Data.SqlClient
Imports System.Windows.Forms

Namespace Proposals.Create

    Public Module Proposals

        ''' <summary>
        ''' Process the functions
        ''' </summary>
        Public Sub Process(ByVal ap As ArgParser)

            ' Process the mode
            Select Case ap.Mode
                Case ArgParser.ModeEnum.Unspecified
                    ' Do not do anything. The user cancelled the dialog.

                Case ArgParser.ModeEnum.Create
                    CreateBatch()

                Case ArgParser.ModeEnum.Close
                    CloseBatch()
            End Select
        End Sub

        ''' <summary>
        ''' Parameter values for the reports
        ''' </summary>
        Public Sub CloseBatch()
            Dim BatchListString As New System.Text.StringBuilder
            Dim answer As DialogResult

            Using frm As New OpenForm()
                With frm
                    .MultipleSelectionAllowed = True
                    answer = .ShowDialog()

                    ' Build the list of batch IDs as a comma-seperated value string.
                    If answer = DialogResult.OK Then
                        For Each BatchID As Int32 In .SelectedBatches
                            BatchListString.AppendFormat(",{0:d}", BatchID)
                        Next
                        BatchListString.Remove(0, 1)
                    End If
                End With
            End Using

            ' If successful then set the wait cursor
            If answer = DialogResult.OK Then

                Dim cn As SqlClient.SqlConnection = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
                Dim CurrentCursor As Cursor = Cursor.Current
                Cursor.Current = Cursors.WaitCursor
                Try

                    ' Open the database connection and close the proposal batch(es)
                    cn.Open()
                    With New SqlCommand
                        .Connection = cn
                        .CommandText = "xpr_proposal_close"
                        .CommandType = CommandType.StoredProcedure
                        .Parameters.Add("@proposal_batch_id", SqlDbType.VarChar, 256).Value = BatchListString.ToString()
                        .ExecuteNonQuery()
                    End With

                    ' Error occurred closing the batch. Stop here.
                Catch ex As SqlClient.SqlException
                    Using gdr As New Repository.GetDataResult()
                        gdr.HandleException(ex)
                        DebtPlus.UI.Common.ErrorHandling.HandleErrors(gdr, "Error closing proposal batch")
                    End Using

                Finally
                    ' Close out the database connection.
                    If cn IsNot Nothing Then cn.Dispose()
                    Cursor.Current = CurrentCursor
                End Try
            End If
        End Sub

        ''' <summary>
        ''' Create a new batch
        ''' </summary>
        Public Function CreateBatch() As DialogResult
            Dim answer As DialogResult

            Using frm As New BatchCreateForm()
                answer = frm.ShowDialog()
                If answer = DialogResult.OK Then
                    DebtPlus.Data.Forms.MessageBox.Show("The proposal batch(es) have been created successfully", "Operation complete", MessageBoxButtons.OK, MessageBoxIcon.Information)
                End If
            End Using

            Return answer
        End Function

    End Module
End Namespace
