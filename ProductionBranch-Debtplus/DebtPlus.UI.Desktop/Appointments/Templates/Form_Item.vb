#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}
#End Region

Option Compare Binary
Option Explicit On 
Option Strict On

Imports DebtPlus.UI.Common
Imports DebtPlus.UI.Common.Templates
Imports DebtPlus.Utils
Imports System.Data.SqlClient
Imports System.Windows.Forms

Namespace Appointments.Templates
    Friend Class Form_Item
        Inherits DebtPlus.UI.Common.Templates.ADO.Net.EditTemplateForm

        Private ds As New DataSet("ds")

        Public Sub New()
            MyBase.New()
            InitializeComponent()
        End Sub

        Public Sub New(ByVal ctx As Context, ByVal drv As DataRowView)
            MyBase.New(ctx, drv)
            InitializeComponent()
            AddHandler Me.Load, AddressOf Form_Item_Load
        End Sub

#Region " Windows Form Designer generated code "

        'Form overrides dispose to clean up the component list.
        Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
            If disposing Then
                If components IsNot Nothing Then
                    components.Dispose()
                End If
            End If
            MyBase.Dispose(disposing)
        End Sub

        'Required by the Windows Form Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Windows Form Designer
        'It can be modified using the Windows Form Designer.  
        'Do not modify it using the code editor.
        Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents LabelControl2 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents LabelControl3 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents LabelControl4 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents LabelControl5 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents office_name As DevExpress.XtraEditors.LabelControl
        Friend WithEvents day_of_week As DevExpress.XtraEditors.LabelControl
        Friend WithEvents start_time As DevExpress.XtraEditors.TimeEdit
        Friend WithEvents appt_type As DevExpress.XtraEditors.LookUpEdit
        Friend WithEvents counselors As DevExpress.XtraEditors.CheckedListBoxControl
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
            Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl
            Me.LabelControl2 = New DevExpress.XtraEditors.LabelControl
            Me.LabelControl3 = New DevExpress.XtraEditors.LabelControl
            Me.LabelControl4 = New DevExpress.XtraEditors.LabelControl
            Me.office_name = New DevExpress.XtraEditors.LabelControl
            Me.day_of_week = New DevExpress.XtraEditors.LabelControl
            Me.start_time = New DevExpress.XtraEditors.TimeEdit
            Me.appt_type = New DevExpress.XtraEditors.LookUpEdit
            Me.LabelControl5 = New DevExpress.XtraEditors.LabelControl
            Me.counselors = New DevExpress.XtraEditors.CheckedListBoxControl
            CType(Me.DxErrorProvider1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.start_time.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.appt_type.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.counselors, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.SuspendLayout()
            '
            'SimpleButton_OK
            '
            Me.SimpleButton_OK.Location = New System.Drawing.Point(370, 43)
            Me.SimpleButton_OK.TabIndex = 10
            '
            'SimpleButton_Cancel
            '
            Me.SimpleButton_Cancel.Location = New System.Drawing.Point(370, 86)
            Me.SimpleButton_Cancel.TabIndex = 11
            '
            'LabelControl1
            '
            Me.LabelControl1.Location = New System.Drawing.Point(8, 8)
            Me.LabelControl1.Name = "LabelControl1"
            Me.LabelControl1.Size = New System.Drawing.Size(29, 13)
            Me.LabelControl1.TabIndex = 0
            Me.LabelControl1.Text = "Office"
            Me.LabelControl1.ToolTipController = Me.ToolTipController1
            '
            'LabelControl2
            '
            Me.LabelControl2.Location = New System.Drawing.Point(8, 24)
            Me.LabelControl2.Name = "LabelControl2"
            Me.LabelControl2.Size = New System.Drawing.Size(79, 13)
            Me.LabelControl2.TabIndex = 2
            Me.LabelControl2.Text = "Day of the week"
            Me.LabelControl2.ToolTipController = Me.ToolTipController1
            '
            'LabelControl3
            '
            Me.LabelControl3.Location = New System.Drawing.Point(8, 51)
            Me.LabelControl3.Name = "LabelControl3"
            Me.LabelControl3.Size = New System.Drawing.Size(86, 13)
            Me.LabelControl3.TabIndex = 4
            Me.LabelControl3.Text = "Appointment Time"
            Me.LabelControl3.ToolTipController = Me.ToolTipController1
            '
            'LabelControl4
            '
            Me.LabelControl4.Location = New System.Drawing.Point(8, 75)
            Me.LabelControl4.Name = "LabelControl4"
            Me.LabelControl4.Size = New System.Drawing.Size(88, 13)
            Me.LabelControl4.TabIndex = 6
            Me.LabelControl4.Text = "Appointment Type"
            Me.LabelControl4.ToolTipController = Me.ToolTipController1
            '
            'office_name
            '
            Me.office_name.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                        Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.office_name.Location = New System.Drawing.Point(112, 8)
            Me.office_name.Name = "office_name"
            Me.office_name.Size = New System.Drawing.Size(29, 13)
            Me.office_name.TabIndex = 1
            Me.office_name.Text = "Office"
            Me.office_name.ToolTipController = Me.ToolTipController1
            '
            'day_of_week
            '
            Me.day_of_week.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                        Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.day_of_week.Location = New System.Drawing.Point(112, 24)
            Me.day_of_week.Name = "day_of_week"
            Me.day_of_week.Size = New System.Drawing.Size(29, 13)
            Me.day_of_week.TabIndex = 3
            Me.day_of_week.Text = "Office"
            Me.day_of_week.ToolTipController = Me.ToolTipController1
            '
            'start_time
            '
            Me.start_time.EditValue = New Date(2007, 4, 4, 0, 0, 0, 0)
            Me.start_time.Location = New System.Drawing.Point(112, 48)
            Me.start_time.Name = "start_time"
            Me.start_time.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton})
            Me.start_time.Size = New System.Drawing.Size(100, 20)
            Me.start_time.TabIndex = 5
            Me.start_time.ToolTipController = Me.ToolTipController1
            '
            'appt_type
            '
            Me.appt_type.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                        Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.appt_type.Location = New System.Drawing.Point(112, 72)
            Me.appt_type.Name = "appt_type"
            Me.appt_type.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.appt_type.Properties.Columns.AddRange(New DevExpress.XtraEditors.Controls.LookUpColumnInfo() {New DevExpress.XtraEditors.Controls.LookUpColumnInfo("appt_type", "Type", 10, DevExpress.Utils.FormatType.Numeric, "f0", True, DevExpress.Utils.HorzAlignment.Far), New DevExpress.XtraEditors.Controls.LookUpColumnInfo("appt_name", "Description", 50, DevExpress.Utils.FormatType.None, "", True, DevExpress.Utils.HorzAlignment.[Default], DevExpress.Data.ColumnSortOrder.Ascending), New DevExpress.XtraEditors.Controls.LookUpColumnInfo("appt_duration", "Duration", 10, DevExpress.Utils.FormatType.Numeric, "f0", True, DevExpress.Utils.HorzAlignment.Far)})
            Me.appt_type.Properties.NullText = "All"
            Me.appt_type.Size = New System.Drawing.Size(250, 20)
            Me.appt_type.TabIndex = 7
            Me.appt_type.ToolTipController = Me.ToolTipController1
            '
            'LabelControl5
            '
            Me.LabelControl5.Location = New System.Drawing.Point(8, 96)
            Me.LabelControl5.Name = "LabelControl5"
            Me.LabelControl5.Size = New System.Drawing.Size(53, 13)
            Me.LabelControl5.TabIndex = 8
            Me.LabelControl5.Text = "Counselors"
            Me.LabelControl5.ToolTipController = Me.ToolTipController1
            '
            'counselors
            '
            Me.counselors.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                        Or System.Windows.Forms.AnchorStyles.Left) _
                        Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.counselors.CheckOnClick = True
            Me.counselors.Location = New System.Drawing.Point(112, 96)
            Me.counselors.Name = "counselors"
            Me.counselors.Size = New System.Drawing.Size(250, 116)
            Me.counselors.TabIndex = 9
            Me.counselors.ToolTipController = Me.ToolTipController1
            '
            'Form_Item
            '
            Me.AcceptButton = Nothing
            Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
            Me.CancelButton = Nothing
            Me.ClientSize = New System.Drawing.Size(466, 224)
            Me.Controls.Add(Me.counselors)
            Me.Controls.Add(Me.appt_type)
            Me.Controls.Add(Me.start_time)
            Me.Controls.Add(Me.day_of_week)
            Me.Controls.Add(Me.office_name)
            Me.Controls.Add(Me.LabelControl5)
            Me.Controls.Add(Me.LabelControl4)
            Me.Controls.Add(Me.LabelControl3)
            Me.Controls.Add(Me.LabelControl2)
            Me.Controls.Add(Me.LabelControl1)
            Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Sizable
            Me.Name = "Form_Item"
            Me.Text = "Appointment Template Item"
            Me.Controls.SetChildIndex(Me.LabelControl1, 0)
            Me.Controls.SetChildIndex(Me.LabelControl2, 0)
            Me.Controls.SetChildIndex(Me.LabelControl3, 0)
            Me.Controls.SetChildIndex(Me.LabelControl4, 0)
            Me.Controls.SetChildIndex(Me.LabelControl5, 0)
            Me.Controls.SetChildIndex(Me.office_name, 0)
            Me.Controls.SetChildIndex(Me.day_of_week, 0)
            Me.Controls.SetChildIndex(Me.start_time, 0)
            Me.Controls.SetChildIndex(Me.appt_type, 0)
            Me.Controls.SetChildIndex(Me.counselors, 0)
            Me.Controls.SetChildIndex(Me.SimpleButton_OK, 0)
            Me.Controls.SetChildIndex(Me.SimpleButton_Cancel, 0)
            CType(Me.DxErrorProvider1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.start_time.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.appt_type.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.counselors, System.ComponentModel.ISupportInitialize).EndInit()
            Me.ResumeLayout(False)
            Me.PerformLayout()

        End Sub

#End Region

        ''' <summary>
        ''' Process the form load operation
        ''' </summary>
        Private Sub Form_Item_Load(ByVal sender As Object, ByVal e As EventArgs)

            ' Display the office name
            Dim tbl As DataTable = OfficesTable()
            Using vue As New DataView(tbl, String.Format("office={0:d}", drv("office")), String.Empty, DataViewRowState.CurrentRows)
                Dim office As String
                If vue.Count > 0 Then
                    office = Convert.ToString(vue(0).Item("name"))
                Else
                    office = String.Empty
                End If
                office_name.Text = office
            End Using

            ' Display the day of the week
            tbl = DayOfWeekTable()
            Using vue As New DataView(tbl, String.Format("day_of_week={0:d}", drv("dow")), String.Empty, DataViewRowState.CurrentRows)
                Dim weekday As String
                If vue.Count > 0 Then
                    weekday = Convert.ToString(vue(0).Item("description"))
                Else
                    weekday = String.Empty
                End If
                day_of_week.Text = weekday
            End Using

            ' Set the starting time into the time control
            start_time.EditValue = Date.MinValue.AddMinutes(Convert.ToDouble(drv("start_time")))

            ' Find the table of appointment types
            ' If there is no "All" item then add it.
            tbl = ApptTypesTable()
            If tbl.Rows.Find(-1) Is Nothing Then
                Dim row As DataRow = tbl.NewRow
                row.BeginEdit()
                row("appt_type") = -1
                row("appt_name") = "All"
                row("appt_duration") = 60
                row.EndEdit()
                tbl.Rows.Add(row)
                tbl.AcceptChanges()
            End If

            ' Set the information for the appointment types
            With appt_type
                With .Properties
                    .DataSource = tbl.DefaultView
                    .DisplayMember = "appt_name"
                    .ValueMember = "appt_type"
                    .PopupWidth = appt_type.Width + 120
                End With
                .EditValue = drv("appt_type")
            End With

            ' Load the list of counselors
            Dim current_cursor As Cursor = System.Windows.Forms.Cursor.Current
            Try
                System.Windows.Forms.Cursor.Current = Cursors.WaitCursor
                Using cmd As SqlClient.SqlCommand = New SqlCommand
                    With cmd
                        .Connection = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
                        If Convert.ToInt32(drv("appt_time_template")) < 0 Then
                            .CommandText = "SELECT convert(int,null) AS appt_counselor_template, Counselor, NameID, EmailID, TelephoneID, Person, default, ActiveFlag, Office, Menu_Level, Color, ssn, billing_mode, rate, emp_start_date, emp_end_date, HUD_id, date_created, created_by, name, email, telephone, CONVERT(bit,0) AS checked FROM view_counselor_or_csrs WITH (NOLOCK) WHERE ActiveFlag <> 0 ORDER BY name"
                        Else
                            .CommandText = "SELECT a.appt_counselor_template, co.Counselor, co.NameID, co.EmailID, co.TelephoneID, co.Person, co.default, co.ActiveFlag, co.Office, co.Menu_Level, co.Color, co.ssn, co.billing_mode, co.rate, co.emp_start_date, co.emp_end_date, co.HUD_id, co.date_created, co.created_by, co.name, co.email, co.telephone, CONVERT(bit,CASE WHEN a.appt_counselor_template IS NULL THEN 0 ELSE 1 END) AS checked FROM view_counselor_or_csrs co WITH (NOLOCK) LEFT OUTER JOIN appt_counselor_templates a WITH (NOLOCK) ON co.counselor = a.counselor AND @appt_time_template = a.appt_time_template WHERE ActiveFlag <> 0 ORDER BY co.name"
                            .Parameters.Add("@appt_time_template", SqlDbType.Int).Value = drv("appt_time_template")
                        End If
                    End With

                    Using da As New SqlClient.SqlDataAdapter(cmd)
                        da.Fill(ds, "counselors")
                    End Using
                End Using

                ' Load the table into the checked list item
                With counselors
                    tbl = ds.Tables(0)
                    .Items.Clear()
                    For Each drv As DataRowView In tbl.DefaultView
                        .Items.Add(New MyCheckedControl(drv))
                    Next
                End With

            Catch ex As SqlClient.SqlException
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error reading appt_counselors_template table")

            Finally
                System.Windows.Forms.Cursor.Current = current_cursor
            End Try

            ' Add handlers to detect changes to the controls
            AddHandler start_time.EditValueChanging, AddressOf start_time_EditValueChanging
            AddHandler appt_type.EditValueChanging, AddressOf appt_type_EditValueChanging
            AddHandler counselors.ItemCheck, AddressOf counselors_ItemCheck
        End Sub

        ''' <summary>
        ''' Handle the change in the start time.
        ''' </summary>
        Private Sub start_time_EditValueChanging(ByVal sender As Object, ByVal e As DevExpress.XtraEditors.Controls.ChangingEventArgs)
            Dim TimeOffset As Int32 = -1
            Dim TimeValue As Date = Convert.ToDateTime(e.NewValue)
            Dim Hour As Int32 = TimeValue.Hour
            Dim Minute As Int32 = TimeValue.Minute
            TimeOffset = (Hour * 60) + Minute
            drv("start_time") = TimeOffset

            ' Enable the OK button
            SimpleButton_OK.Enabled = True
        End Sub

        ''' <summary>
        ''' Appointment Type handling
        ''' </summary>
        Private Sub appt_type_EditValueChanging(ByVal sender As Object, ByVal e As DevExpress.XtraEditors.Controls.ChangingEventArgs)

            ' Special case handling if the value is "ALL"
            If e.NewValue Is Nothing OrElse e.NewValue Is DBNull.Value OrElse Convert.ToInt32(e.NewValue) < 0 Then
                drv("appt_type") = DBNull.Value
            Else
                ' Otherwise, just set the appointment type to the value.
                drv("appt_type") = e.NewValue
            End If

            ' Enable the OK button
            SimpleButton_OK.Enabled = True
        End Sub

        ''' <summary>
        ''' The checked status of the counselor is changed
        ''' </summary>
        Private Sub counselors_ItemCheck(ByVal sender As Object, ByVal e As DevExpress.XtraEditors.Controls.ItemCheckEventArgs)

            ' Enable the OK button
            SimpleButton_OK.Enabled = True
        End Sub

        ''' <summary>
        ''' Write the counselor information and create the template item
        ''' </summary>
        Protected Overrides Sub SimpleButton_OK_Click(ByVal sender As Object, ByVal e As EventArgs)
            MyBase.SimpleButton_OK_Click(sender, e)

            ' Open the database connection
            Dim cn As SqlClient.SqlConnection = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
            Dim txn As SqlClient.SqlTransaction = Nothing
            Dim current_cursor As Cursor = System.Windows.Forms.Cursor.Current
            Try
                System.Windows.Forms.Cursor.Current = Cursors.WaitCursor
                cn.Open()
                txn = cn.BeginTransaction

                ' If there is no appointment template then create it
                ' Create the record if one is needed to be created
                Dim ApptTimeTemplate As Int32 = Convert.ToInt32(drv("appt_time_template"))
                If ApptTimeTemplate < 0 Then
                    Using cmd As SqlClient.SqlCommand = New SqlCommand
                        With cmd
                            .Connection = cn
                            .Transaction = txn
                            .CommandText = "SET NOCOUNT ON;INSERT INTO appt_time_templates (office,dow,start_time) VALUES (@office,@dow,@start_time); SELECT scope_identity() AS appt_time_template;SET NOCOUNT OFF;"
                            .Parameters.Add("@office", SqlDbType.Int).Value = drv("office")
                            .Parameters.Add("@dow", SqlDbType.Int).Value = drv("dow")
                            .Parameters.Add("@start_time", SqlDbType.Int).Value = drv("start_time")

                            Dim objAnswer As Object = .ExecuteScalar
                            If objAnswer IsNot Nothing AndAlso objAnswer IsNot DBNull.Value Then
                                ApptTimeTemplate = Convert.ToInt32(objAnswer)
                                drv("appt_time_template") = ApptTimeTemplate
                            End If
                        End With
                    End Using
                End If

                ' Record the counselor items
                If ApptTimeTemplate > 0 Then
                    Dim CheckedCounselors As New System.Text.StringBuilder

                    ' Find the names of the counselors
                    For Each Item As MyCheckedControl In counselors.Items
                        If Item.CheckState = CheckState.Checked Then
                            CheckedCounselors.AppendFormat(",{0}", Convert.ToString(Item.drv.Item("name")).Trim())

                            ' If the item is not recorded as cheched then add it
                            If Not Convert.ToBoolean(Item.drv.Item("checked")) Then
                                Using cmd As SqlClient.SqlCommand = New SqlCommand
                                    With cmd
                                        .Connection = cn
                                        .Transaction = txn
                                        .CommandText = "INSERT INTO appt_counselor_templates(appt_time_template,counselor) VALUES (@appt_time_template,@counselor)"
                                        .Parameters.Add("@appt_time_template", SqlDbType.Int).Value = ApptTimeTemplate
                                        .Parameters.Add("@counselor", SqlDbType.Int).Value = Item.drv.Item("counselor")
                                        .ExecuteNonQuery()
                                    End With
                                End Using
                            End If

                        Else

                            ' If the item is recorded as checked and it is no longer checked then clear it.
                            If Item.drv.Item("appt_counselor_template") IsNot DBNull.Value Then
                                Using cmd As SqlClient.SqlCommand = New SqlCommand
                                    With cmd
                                        .Connection = cn
                                        .Transaction = txn
                                        .CommandText = "DELETE FROM appt_counselor_templates WHERE appt_counselor_template=@appt_counselor_template"
                                        .Parameters.Add("@appt_counselor_template", SqlDbType.Int).Value = Item.drv.Item("appt_counselor_template")
                                        .ExecuteNonQuery()
                                    End With
                                End Using
                            End If
                        End If
                    Next

                    ' Update the master row with the formatted items
                    If CheckedCounselors.Length > 0 Then CheckedCounselors.Remove(0, 1)
                    drv("formatted_names") = CheckedCounselors.ToString()
                    drv("formatted_appt_type") = appt_type.Text

                    ' Accept the changes to the dialog
                    DialogResult = System.Windows.Forms.DialogResult.OK

                    ' Commit the changes to the database
                    txn.Commit()
                    txn = Nothing
                End If

            Catch ex As SqlClient.SqlException
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error updating appt_counselor_templates table")

            Finally
                If txn IsNot Nothing Then
                    Try
                        txn.Rollback()
                    Catch exRollback As Exception
                    End Try
                    txn.Dispose()
                    txn = Nothing
                End If

                If cn IsNot Nothing Then cn.Dispose()

                System.Windows.Forms.Cursor.Current = current_cursor
            End Try
        End Sub
    End Class
End Namespace
