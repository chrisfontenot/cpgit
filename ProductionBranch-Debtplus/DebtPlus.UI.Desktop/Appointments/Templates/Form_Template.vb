#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}
#End Region

Option Compare Binary
Option Explicit On 
Option Strict On

Imports DebtPlus.UI.Common
Imports DebtPlus.Utils
Imports System.Data.SqlClient
Imports DebtPlus.UI.FormLib.Offices
Imports System.Windows.Forms

Namespace Appointments.Templates
    Friend Class Form_Template
        Inherits DebtPlus.Data.Forms.DebtPlusForm

        Private Property ctx As Context = Nothing

        Public Sub New(ByVal ctx As Context)
            MyBase.New()
            Me.ctx = ctx
            InitializeComponent()

            AddHandler lblOffice.Click, AddressOf lblOffice_Click
            AddHandler Me.Load, AddressOf Form_Template_Load
            AddHandler GridView1.FocusedRowChanged, AddressOf GridView1_FocusedRowChanged
            AddHandler GridView1.DoubleClick, AddressOf GridView1_DoubleClick
            AddHandler MyBase.Closing, AddressOf Form_Template_Closing
            AddHandler Button_Edit.Click, AddressOf Button_Edit_Click
            AddHandler ContextMenu1.Popup, AddressOf ContextMenu1_Popup
            AddHandler ContextMenu1_Create.Click, AddressOf ContextMenu1_Create_Click
            AddHandler ContextMenu1_Edit.Click, AddressOf ContextMenu1_Edit_Click
            AddHandler ContextMenu1_Delete.Click, AddressOf ContextMenu1_Delete_Click
            AddHandler Button_Cancel.Click, AddressOf Button_Cancel_Click

            GridColumn_time.DisplayFormat.Format = New TimeFormatProvider
        End Sub

#Region " Windows Form Designer generated code "

        'Form overrides dispose to clean up the component list.
        Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
            If disposing Then
                If components IsNot Nothing Then
                    components.Dispose()
                End If
            End If
            MyBase.Dispose(disposing)
        End Sub

        'Required by the Windows Form Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Windows Form Designer
        'It can be modified using the Windows Form Designer.  
        'Do not modify it using the code editor.
        Friend WithEvents lblOffice As DevExpress.XtraEditors.LabelControl
        Friend WithEvents lblDayOfWeek As DevExpress.XtraEditors.LabelControl
        Friend WithEvents LookUpEdit_Office As DevExpress.XtraEditors.LookUpEdit
        Friend WithEvents LookUpEdit_Weekday As DevExpress.XtraEditors.LookUpEdit
        Friend WithEvents GridControl1 As DevExpress.XtraGrid.GridControl
        Friend WithEvents Button_Cancel As DevExpress.XtraEditors.SimpleButton
        Friend WithEvents GridView1 As DevExpress.XtraGrid.Views.Grid.GridView
        Friend WithEvents GridColumn_time As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents GridColumn_appt_type As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents GridColumn_appt_template_time As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents GridColumn_appt_type_name As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents GridColumn_counselors As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents ContextMenu1 As System.Windows.Forms.ContextMenu
        Friend WithEvents Button_Edit As DevExpress.XtraEditors.SimpleButton
        Friend WithEvents ContextMenu1_Delete As System.Windows.Forms.MenuItem
        Friend WithEvents ContextMenu1_Edit As System.Windows.Forms.MenuItem
        Friend WithEvents ContextMenu1_Create As System.Windows.Forms.MenuItem
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
            Me.GridControl1 = New DevExpress.XtraGrid.GridControl
            Me.ContextMenu1 = New System.Windows.Forms.ContextMenu
            Me.ContextMenu1_Create = New System.Windows.Forms.MenuItem
            Me.ContextMenu1_Edit = New System.Windows.Forms.MenuItem
            Me.ContextMenu1_Delete = New System.Windows.Forms.MenuItem
            Me.GridView1 = New DevExpress.XtraGrid.Views.Grid.GridView
            Me.GridColumn_appt_template_time = New DevExpress.XtraGrid.Columns.GridColumn
            Me.GridColumn_appt_template_time.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True
            Me.GridColumn_appt_type = New DevExpress.XtraGrid.Columns.GridColumn
            Me.GridColumn_appt_type.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True
            Me.GridColumn_time = New DevExpress.XtraGrid.Columns.GridColumn
            Me.GridColumn_time.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True
            Me.GridColumn_appt_type_name = New DevExpress.XtraGrid.Columns.GridColumn
            Me.GridColumn_appt_type_name.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True
            Me.GridColumn_counselors = New DevExpress.XtraGrid.Columns.GridColumn
            Me.GridColumn_counselors.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True
            Me.lblOffice = New DevExpress.XtraEditors.LabelControl
            Me.lblDayOfWeek = New DevExpress.XtraEditors.LabelControl
            Me.LookUpEdit_Office = New DevExpress.XtraEditors.LookUpEdit
            Me.LookUpEdit_Weekday = New DevExpress.XtraEditors.LookUpEdit
            Me.Button_Edit = New DevExpress.XtraEditors.SimpleButton
            Me.Button_Cancel = New DevExpress.XtraEditors.SimpleButton
            CType(Me.DxErrorProvider1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.GridView1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LookUpEdit_Office.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LookUpEdit_Weekday.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.SuspendLayout()
            '
            'GridControl1
            '
            Me.GridControl1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                        Or System.Windows.Forms.AnchorStyles.Left) _
                        Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.GridControl1.ContextMenu = Me.ContextMenu1
            Me.GridControl1.Location = New System.Drawing.Point(0, 56)
            Me.GridControl1.MainView = Me.GridView1
            Me.GridControl1.Name = "GridControl1"
            Me.GridControl1.Size = New System.Drawing.Size(384, 200)
            Me.GridControl1.TabIndex = 4
            Me.GridControl1.ToolTipController = Me.ToolTipController1
            Me.GridControl1.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridView1})
            '
            'ContextMenu1
            '
            Me.ContextMenu1.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.ContextMenu1_Create, Me.ContextMenu1_Edit, Me.ContextMenu1_Delete})
            '
            'ContextMenu1_Create
            '
            Me.ContextMenu1_Create.Index = 0
            Me.ContextMenu1_Create.Text = "&Add"
            '
            'ContextMenu1_Edit
            '
            Me.ContextMenu1_Edit.Index = 1
            Me.ContextMenu1_Edit.Text = "&Change"
            '
            'ContextMenu1_Delete
            '
            Me.ContextMenu1_Delete.Index = 2
            Me.ContextMenu1_Delete.Text = "&Delete"
            '
            'GridView1
            '
            Me.GridView1.Appearance.ColumnFilterButton.BackColor = System.Drawing.Color.FromArgb(CType(CType(104, Byte), Int32), CType(CType(184, Byte), Int32), CType(CType(251, Byte), Int32))
            Me.GridView1.Appearance.ColumnFilterButton.BorderColor = System.Drawing.Color.FromArgb(CType(CType(104, Byte), Int32), CType(CType(184, Byte), Int32), CType(CType(251, Byte), Int32))
            Me.GridView1.Appearance.ColumnFilterButton.ForeColor = System.Drawing.Color.White
            Me.GridView1.Appearance.ColumnFilterButton.Options.UseBackColor = True
            Me.GridView1.Appearance.ColumnFilterButton.Options.UseBorderColor = True
            Me.GridView1.Appearance.ColumnFilterButton.Options.UseForeColor = True
            Me.GridView1.Appearance.ColumnFilterButtonActive.BackColor = System.Drawing.Color.FromArgb(CType(CType(170, Byte), Int32), CType(CType(216, Byte), Int32), CType(CType(254, Byte), Int32))
            Me.GridView1.Appearance.ColumnFilterButtonActive.BorderColor = System.Drawing.Color.FromArgb(CType(CType(170, Byte), Int32), CType(CType(216, Byte), Int32), CType(CType(254, Byte), Int32))
            Me.GridView1.Appearance.ColumnFilterButtonActive.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.ColumnFilterButtonActive.Options.UseBackColor = True
            Me.GridView1.Appearance.ColumnFilterButtonActive.Options.UseBorderColor = True
            Me.GridView1.Appearance.ColumnFilterButtonActive.Options.UseForeColor = True
            Me.GridView1.Appearance.Empty.BackColor = System.Drawing.Color.FromArgb(CType(CType(236, Byte), Int32), CType(CType(246, Byte), Int32), CType(CType(255, Byte), Int32))
            Me.GridView1.Appearance.Empty.BackColor2 = System.Drawing.Color.White
            Me.GridView1.Appearance.Empty.Options.UseBackColor = True
            Me.GridView1.Appearance.EvenRow.BackColor = System.Drawing.Color.FromArgb(CType(CType(247, Byte), Int32), CType(CType(251, Byte), Int32), CType(CType(255, Byte), Int32))
            Me.GridView1.Appearance.EvenRow.BorderColor = System.Drawing.Color.FromArgb(CType(CType(247, Byte), Int32), CType(CType(251, Byte), Int32), CType(CType(255, Byte), Int32))
            Me.GridView1.Appearance.EvenRow.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.EvenRow.Options.UseBackColor = True
            Me.GridView1.Appearance.EvenRow.Options.UseBorderColor = True
            Me.GridView1.Appearance.EvenRow.Options.UseForeColor = True
            Me.GridView1.Appearance.FilterCloseButton.BackColor = System.Drawing.Color.FromArgb(CType(CType(104, Byte), Int32), CType(CType(184, Byte), Int32), CType(CType(251, Byte), Int32))
            Me.GridView1.Appearance.FilterCloseButton.BorderColor = System.Drawing.Color.FromArgb(CType(CType(104, Byte), Int32), CType(CType(184, Byte), Int32), CType(CType(251, Byte), Int32))
            Me.GridView1.Appearance.FilterCloseButton.ForeColor = System.Drawing.Color.White
            Me.GridView1.Appearance.FilterCloseButton.Options.UseBackColor = True
            Me.GridView1.Appearance.FilterCloseButton.Options.UseBorderColor = True
            Me.GridView1.Appearance.FilterCloseButton.Options.UseForeColor = True
            Me.GridView1.Appearance.FilterPanel.BackColor = System.Drawing.Color.FromArgb(CType(CType(236, Byte), Int32), CType(CType(246, Byte), Int32), CType(CType(255, Byte), Int32))
            Me.GridView1.Appearance.FilterPanel.BackColor2 = System.Drawing.Color.White
            Me.GridView1.Appearance.FilterPanel.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.FilterPanel.Options.UseBackColor = True
            Me.GridView1.Appearance.FilterPanel.Options.UseForeColor = True
            Me.GridView1.Appearance.FixedLine.BackColor = System.Drawing.Color.FromArgb(CType(CType(59, Byte), Int32), CType(CType(133, Byte), Int32), CType(CType(195, Byte), Int32))
            Me.GridView1.Appearance.FixedLine.Options.UseBackColor = True
            Me.GridView1.Appearance.FocusedCell.BackColor = System.Drawing.Color.White
            Me.GridView1.Appearance.FocusedCell.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.FocusedCell.Options.UseBackColor = True
            Me.GridView1.Appearance.FocusedCell.Options.UseForeColor = True
            Me.GridView1.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb(CType(CType(38, Byte), Int32), CType(CType(109, Byte), Int32), CType(CType(189, Byte), Int32))
            Me.GridView1.Appearance.FocusedRow.BorderColor = System.Drawing.Color.FromArgb(CType(CType(59, Byte), Int32), CType(CType(139, Byte), Int32), CType(CType(206, Byte), Int32))
            Me.GridView1.Appearance.FocusedRow.ForeColor = System.Drawing.Color.White
            Me.GridView1.Appearance.FocusedRow.Options.UseBackColor = True
            Me.GridView1.Appearance.FocusedRow.Options.UseBorderColor = True
            Me.GridView1.Appearance.FocusedRow.Options.UseForeColor = True
            Me.GridView1.Appearance.FooterPanel.BackColor = System.Drawing.Color.FromArgb(CType(CType(104, Byte), Int32), CType(CType(184, Byte), Int32), CType(CType(251, Byte), Int32))
            Me.GridView1.Appearance.FooterPanel.BorderColor = System.Drawing.Color.FromArgb(CType(CType(104, Byte), Int32), CType(CType(184, Byte), Int32), CType(CType(251, Byte), Int32))
            Me.GridView1.Appearance.FooterPanel.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.FooterPanel.Options.UseBackColor = True
            Me.GridView1.Appearance.FooterPanel.Options.UseBorderColor = True
            Me.GridView1.Appearance.FooterPanel.Options.UseForeColor = True
            Me.GridView1.Appearance.GroupButton.BackColor = System.Drawing.Color.FromArgb(CType(CType(104, Byte), Int32), CType(CType(184, Byte), Int32), CType(CType(251, Byte), Int32))
            Me.GridView1.Appearance.GroupButton.BorderColor = System.Drawing.Color.FromArgb(CType(CType(104, Byte), Int32), CType(CType(184, Byte), Int32), CType(CType(251, Byte), Int32))
            Me.GridView1.Appearance.GroupButton.Options.UseBackColor = True
            Me.GridView1.Appearance.GroupButton.Options.UseBorderColor = True
            Me.GridView1.Appearance.GroupFooter.BackColor = System.Drawing.Color.FromArgb(CType(CType(170, Byte), Int32), CType(CType(216, Byte), Int32), CType(CType(254, Byte), Int32))
            Me.GridView1.Appearance.GroupFooter.BorderColor = System.Drawing.Color.FromArgb(CType(CType(170, Byte), Int32), CType(CType(216, Byte), Int32), CType(CType(254, Byte), Int32))
            Me.GridView1.Appearance.GroupFooter.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.GroupFooter.Options.UseBackColor = True
            Me.GridView1.Appearance.GroupFooter.Options.UseBorderColor = True
            Me.GridView1.Appearance.GroupFooter.Options.UseForeColor = True
            Me.GridView1.Appearance.GroupPanel.BackColor = System.Drawing.Color.FromArgb(CType(CType(236, Byte), Int32), CType(CType(246, Byte), Int32), CType(CType(255, Byte), Int32))
            Me.GridView1.Appearance.GroupPanel.BackColor2 = System.Drawing.Color.White
            Me.GridView1.Appearance.GroupPanel.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.GroupPanel.Options.UseBackColor = True
            Me.GridView1.Appearance.GroupPanel.Options.UseForeColor = True
            Me.GridView1.Appearance.GroupRow.BackColor = System.Drawing.Color.FromArgb(CType(CType(170, Byte), Int32), CType(CType(216, Byte), Int32), CType(CType(254, Byte), Int32))
            Me.GridView1.Appearance.GroupRow.BorderColor = System.Drawing.Color.FromArgb(CType(CType(170, Byte), Int32), CType(CType(216, Byte), Int32), CType(CType(254, Byte), Int32))
            Me.GridView1.Appearance.GroupRow.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.GroupRow.Options.UseBackColor = True
            Me.GridView1.Appearance.GroupRow.Options.UseBorderColor = True
            Me.GridView1.Appearance.GroupRow.Options.UseForeColor = True
            Me.GridView1.Appearance.HeaderPanel.BackColor = System.Drawing.Color.FromArgb(CType(CType(139, Byte), Int32), CType(CType(201, Byte), Int32), CType(CType(254, Byte), Int32))
            Me.GridView1.Appearance.HeaderPanel.BorderColor = System.Drawing.Color.FromArgb(CType(CType(139, Byte), Int32), CType(CType(201, Byte), Int32), CType(CType(254, Byte), Int32))
            Me.GridView1.Appearance.HeaderPanel.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.HeaderPanel.Options.UseBackColor = True
            Me.GridView1.Appearance.HeaderPanel.Options.UseBorderColor = True
            Me.GridView1.Appearance.HeaderPanel.Options.UseForeColor = True
            Me.GridView1.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Int32), CType(CType(170, Byte), Int32), CType(CType(225, Byte), Int32))
            Me.GridView1.Appearance.HideSelectionRow.BorderColor = System.Drawing.Color.FromArgb(CType(CType(83, Byte), Int32), CType(CType(155, Byte), Int32), CType(CType(215, Byte), Int32))
            Me.GridView1.Appearance.HideSelectionRow.ForeColor = System.Drawing.Color.FromArgb(CType(CType(236, Byte), Int32), CType(CType(246, Byte), Int32), CType(CType(255, Byte), Int32))
            Me.GridView1.Appearance.HideSelectionRow.Options.UseBackColor = True
            Me.GridView1.Appearance.HideSelectionRow.Options.UseBorderColor = True
            Me.GridView1.Appearance.HideSelectionRow.Options.UseForeColor = True
            Me.GridView1.Appearance.HorzLine.BackColor = System.Drawing.Color.FromArgb(CType(CType(104, Byte), Int32), CType(CType(184, Byte), Int32), CType(CType(251, Byte), Int32))
            Me.GridView1.Appearance.HorzLine.Options.UseBackColor = True
            Me.GridView1.Appearance.OddRow.BackColor = System.Drawing.Color.FromArgb(CType(CType(236, Byte), Int32), CType(CType(246, Byte), Int32), CType(CType(255, Byte), Int32))
            Me.GridView1.Appearance.OddRow.BorderColor = System.Drawing.Color.FromArgb(CType(CType(236, Byte), Int32), CType(CType(246, Byte), Int32), CType(CType(255, Byte), Int32))
            Me.GridView1.Appearance.OddRow.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.OddRow.Options.UseBackColor = True
            Me.GridView1.Appearance.OddRow.Options.UseBorderColor = True
            Me.GridView1.Appearance.OddRow.Options.UseForeColor = True
            Me.GridView1.Appearance.Preview.Font = New System.Drawing.Font("Verdana", 7.5!)
            Me.GridView1.Appearance.Preview.ForeColor = System.Drawing.Color.FromArgb(CType(CType(83, Byte), Int32), CType(CType(155, Byte), Int32), CType(CType(215, Byte), Int32))
            Me.GridView1.Appearance.Preview.Options.UseFont = True
            Me.GridView1.Appearance.Preview.Options.UseForeColor = True
            Me.GridView1.Appearance.Row.BackColor = System.Drawing.Color.FromArgb(CType(CType(247, Byte), Int32), CType(CType(251, Byte), Int32), CType(CType(255, Byte), Int32))
            Me.GridView1.Appearance.Row.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.Row.Options.UseBackColor = True
            Me.GridView1.Appearance.Row.Options.UseForeColor = True
            Me.GridView1.Appearance.RowSeparator.BackColor = System.Drawing.Color.FromArgb(CType(CType(236, Byte), Int32), CType(CType(246, Byte), Int32), CType(CType(255, Byte), Int32))
            Me.GridView1.Appearance.RowSeparator.BackColor2 = System.Drawing.Color.White
            Me.GridView1.Appearance.RowSeparator.Options.UseBackColor = True
            Me.GridView1.Appearance.SelectedRow.BackColor = System.Drawing.Color.FromArgb(CType(CType(83, Byte), Int32), CType(CType(155, Byte), Int32), CType(CType(215, Byte), Int32))
            Me.GridView1.Appearance.SelectedRow.ForeColor = System.Drawing.Color.White
            Me.GridView1.Appearance.SelectedRow.Options.UseBackColor = True
            Me.GridView1.Appearance.SelectedRow.Options.UseForeColor = True
            Me.GridView1.Appearance.TopNewRow.BackColor = System.Drawing.Color.White
            Me.GridView1.Appearance.TopNewRow.Options.UseBackColor = True
            Me.GridView1.Appearance.VertLine.BackColor = System.Drawing.Color.FromArgb(CType(CType(104, Byte), Int32), CType(CType(184, Byte), Int32), CType(CType(251, Byte), Int32))
            Me.GridView1.Appearance.VertLine.Options.UseBackColor = True
            Me.GridView1.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.GridColumn_appt_template_time, Me.GridColumn_appt_type, Me.GridColumn_time, Me.GridColumn_appt_type_name, Me.GridColumn_counselors})
            Me.GridView1.GridControl = Me.GridControl1
            Me.GridView1.Name = "GridView1"
            Me.GridView1.OptionsBehavior.Editable = False
            Me.GridView1.OptionsView.ShowGroupPanel = False
            Me.GridView1.OptionsView.ShowIndicator = False
            Me.GridView1.SortInfo.AddRange(New DevExpress.XtraGrid.Columns.GridColumnSortInfo() {New DevExpress.XtraGrid.Columns.GridColumnSortInfo(Me.GridColumn_time, DevExpress.Data.ColumnSortOrder.Ascending)})
            '
            'GridColumn_appt_template_time
            '
            Me.GridColumn_appt_template_time.AppearanceCell.Options.UseTextOptions = True
            Me.GridColumn_appt_template_time.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.GridColumn_appt_template_time.AppearanceHeader.Options.UseTextOptions = True
            Me.GridColumn_appt_template_time.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.GridColumn_appt_template_time.Caption = "Record ID"
            Me.GridColumn_appt_template_time.CustomizationCaption = "Record ID"
            Me.GridColumn_appt_template_time.DisplayFormat.FormatString = "f0"
            Me.GridColumn_appt_template_time.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.GridColumn_appt_template_time.FieldName = "appt_time_template"
            Me.GridColumn_appt_template_time.GroupFormat.FormatString = "f0"
            Me.GridColumn_appt_template_time.GroupFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.GridColumn_appt_template_time.Name = "GridColumn_appt_template_time"
            '
            'GridColumn_appt_type
            '
            Me.GridColumn_appt_type.AppearanceCell.Options.UseTextOptions = True
            Me.GridColumn_appt_type.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.GridColumn_appt_type.AppearanceHeader.Options.UseTextOptions = True
            Me.GridColumn_appt_type.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.GridColumn_appt_type.Caption = "Appt Type ID"
            Me.GridColumn_appt_type.CustomizationCaption = "Appt Type ID"
            Me.GridColumn_appt_type.DisplayFormat.FormatString = "f0"
            Me.GridColumn_appt_type.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.GridColumn_appt_type.FieldName = "appt_type"
            Me.GridColumn_appt_type.GroupFormat.FormatString = "f0"
            Me.GridColumn_appt_type.GroupFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.GridColumn_appt_type.Name = "GridColumn_appt_type"
            '
            'GridColumn_time
            '
            Me.GridColumn_time.AppearanceCell.Options.UseTextOptions = True
            Me.GridColumn_time.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.GridColumn_time.AppearanceHeader.Options.UseTextOptions = True
            Me.GridColumn_time.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.GridColumn_time.Caption = "Time"
            Me.GridColumn_time.CustomizationCaption = "Time of day"
            Me.GridColumn_time.DisplayFormat.FormatString = "f0"
            Me.GridColumn_time.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom
            Me.GridColumn_time.FieldName = "start_time"
            Me.GridColumn_time.GroupFormat.FormatString = "f0"
            Me.GridColumn_time.GroupFormat.FormatType = DevExpress.Utils.FormatType.Custom
            Me.GridColumn_time.Name = "GridColumn_time"
            Me.GridColumn_time.Visible = True
            Me.GridColumn_time.VisibleIndex = 0
            Me.GridColumn_time.Width = 102
            '
            'GridColumn_appt_type_name
            '
            Me.GridColumn_appt_type_name.Caption = "Type"
            Me.GridColumn_appt_type_name.CustomizationCaption = "Appointment Type"
            Me.GridColumn_appt_type_name.FieldName = "formatted_appt_type"
            Me.GridColumn_appt_type_name.Name = "GridColumn_appt_type_name"
            Me.GridColumn_appt_type_name.Visible = True
            Me.GridColumn_appt_type_name.VisibleIndex = 1
            Me.GridColumn_appt_type_name.Width = 106
            '
            'GridColumn_counselors
            '
            Me.GridColumn_counselors.Caption = "Counselors"
            Me.GridColumn_counselors.CustomizationCaption = "Counselor Name(s)"
            Me.GridColumn_counselors.FieldName = "formatted_names"
            Me.GridColumn_counselors.Name = "GridColumn_counselors"
            Me.GridColumn_counselors.Visible = True
            Me.GridColumn_counselors.VisibleIndex = 2
            Me.GridColumn_counselors.Width = 389
            '
            'lblOffice
            '
            Me.lblOffice.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Underline)
            Me.lblOffice.Appearance.Options.UseFont = True
            Me.lblOffice.Location = New System.Drawing.Point(8, 11)
            Me.lblOffice.Name = "lblOffice"
            Me.lblOffice.Size = New System.Drawing.Size(33, 13)
            Me.lblOffice.TabIndex = 0
            Me.lblOffice.Text = "Office:"
            Me.lblOffice.ToolTipController = Me.ToolTipController1
            '
            'lblDayOfWeek
            '
            Me.lblDayOfWeek.Location = New System.Drawing.Point(8, 35)
            Me.lblDayOfWeek.Name = "lblDayOfWeek"
            Me.lblDayOfWeek.Size = New System.Drawing.Size(79, 13)
            Me.lblDayOfWeek.TabIndex = 1
            Me.lblDayOfWeek.Text = "Day of the week"
            Me.lblDayOfWeek.ToolTipController = Me.ToolTipController1
            '
            'LookUpEdit_Office
            '
            Me.LookUpEdit_Office.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                        Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.LookUpEdit_Office.Location = New System.Drawing.Point(96, 8)
            Me.LookUpEdit_Office.Name = "LookUpEdit_Office"
            Me.LookUpEdit_Office.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.LookUpEdit_Office.Properties.Columns.AddRange(New DevExpress.XtraEditors.Controls.LookUpColumnInfo() {New DevExpress.XtraEditors.Controls.LookUpColumnInfo("office", "Office", 20, DevExpress.Utils.FormatType.Numeric, "", True, DevExpress.Utils.HorzAlignment.Far), New DevExpress.XtraEditors.Controls.LookUpColumnInfo("name", "Name", 50, DevExpress.Utils.FormatType.None, "", True, DevExpress.Utils.HorzAlignment.[Default], DevExpress.Data.ColumnSortOrder.Ascending), New DevExpress.XtraEditors.Controls.LookUpColumnInfo("formatted_office_type", "Type", 20, DevExpress.Utils.FormatType.None, "", True, DevExpress.Utils.HorzAlignment.[Default], DevExpress.Data.ColumnSortOrder.Ascending)})
            Me.LookUpEdit_Office.Properties.NullText = "...Please Choose one..."
            Me.LookUpEdit_Office.Properties.ShowFooter = False
            Me.LookUpEdit_Office.Size = New System.Drawing.Size(280, 20)
            Me.LookUpEdit_Office.TabIndex = 2
            Me.LookUpEdit_Office.ToolTipController = Me.ToolTipController1
            Me.LookUpEdit_Office.Properties.SortColumnIndex = 1
            '
            'LookUpEdit_Weekday
            '
            Me.LookUpEdit_Weekday.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                        Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.LookUpEdit_Weekday.Location = New System.Drawing.Point(96, 32)
            Me.LookUpEdit_Weekday.Name = "LookUpEdit_Weekday"
            Me.LookUpEdit_Weekday.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.LookUpEdit_Weekday.Properties.Columns.AddRange(New DevExpress.XtraEditors.Controls.LookUpColumnInfo() {New DevExpress.XtraEditors.Controls.LookUpColumnInfo("weekday", "Weekday", 20, DevExpress.Utils.FormatType.Numeric, "f0", False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Data.ColumnSortOrder.Ascending), New DevExpress.XtraEditors.Controls.LookUpColumnInfo("description", "Description", 20, DevExpress.Utils.FormatType.None, "", True, DevExpress.Utils.HorzAlignment.Near)})
            Me.LookUpEdit_Weekday.Properties.NullText = "...Please Choose one..."
            Me.LookUpEdit_Weekday.Properties.ShowFooter = False
            Me.LookUpEdit_Weekday.Properties.ShowHeader = False
            Me.LookUpEdit_Weekday.Properties.ShowLines = False
            Me.LookUpEdit_Weekday.Size = New System.Drawing.Size(280, 20)
            Me.LookUpEdit_Weekday.TabIndex = 3
            Me.LookUpEdit_Weekday.ToolTipController = Me.ToolTipController1
            Me.LookUpEdit_Weekday.Properties.SortColumnIndex = 1
            '
            'Button_Edit
            '
            Me.Button_Edit.Anchor = System.Windows.Forms.AnchorStyles.Bottom
            Me.Button_Edit.Location = New System.Drawing.Point(111, 264)
            Me.Button_Edit.Name = "Button_Edit"
            Me.Button_Edit.Size = New System.Drawing.Size(75, 23)
            Me.Button_Edit.TabIndex = 5
            Me.Button_Edit.Text = "Edit..."
            '
            'Button_Cancel
            '
            Me.Button_Cancel.Anchor = System.Windows.Forms.AnchorStyles.Bottom
            Me.Button_Cancel.Location = New System.Drawing.Point(199, 264)
            Me.Button_Cancel.Name = "Button_Cancel"
            Me.Button_Cancel.Size = New System.Drawing.Size(75, 23)
            Me.Button_Cancel.TabIndex = 6
            Me.Button_Cancel.Text = "&Cancel"
            '
            'Form_Template
            '
            Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
            Me.ClientSize = New System.Drawing.Size(384, 302)
            Me.Controls.Add(Me.Button_Cancel)
            Me.Controls.Add(Me.Button_Edit)
            Me.Controls.Add(Me.GridControl1)
            Me.Controls.Add(Me.LookUpEdit_Weekday)
            Me.Controls.Add(Me.LookUpEdit_Office)
            Me.Controls.Add(Me.lblDayOfWeek)
            Me.Controls.Add(Me.lblOffice)
            Me.Name = "Form_Template"
            Me.Text = "Appointment Template"
            CType(Me.DxErrorProvider1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.GridView1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LookUpEdit_Office.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LookUpEdit_Weekday.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            Me.ResumeLayout(False)
            Me.PerformLayout()

        End Sub

#End Region

        ''' <summary>
        ''' Load the form
        ''' </summary>
        Private Sub Form_Template_Load(ByVal sender As Object, ByVal e As System.EventArgs)

            ' Empty the table of times, etc.
            GridControl1.DataSource = Nothing

            ' Define the days of the week
            Dim vue As DataView = DayOfWeekTable.DefaultView
            With LookUpEdit_Weekday
                With .Properties
                    .DataSource = vue
                    .DisplayMember = "description"
                    .ValueMember = "day_of_week"
                End With
                .Properties.PopupWidth = .Width
                .EditValue = 2  ' Monday
            End With

            ' Load the list of offices
            lookupEdit_office_Load(LookUpEdit_Office)
            AddHandler LookUpEdit_Office.EditValueChanging, AddressOf LookupEdit_EditValueChanging

            ' Add the handlers to reload the grid when the items change
            AddHandler LookUpEdit_Office.EditValueChanged, AddressOf RefreshGrid
            AddHandler LookUpEdit_Weekday.EditValueChanged, AddressOf RefreshGrid

            ' Load the initial selection if there is one
            ReadGrid()
        End Sub

        ''' <summary>
        ''' The focused row changed. Enable the Edit button as needed.
        ''' </summary>
        Private Sub GridView1_FocusedRowChanged(ByVal sender As Object, ByVal e As DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs)
            Button_Edit.Enabled = e.FocusedRowHandle >= 0
        End Sub

        ''' <summary>
        ''' Double click event on the list
        ''' </summary>
        Private Sub GridView1_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs)
            Dim gv As DevExpress.XtraGrid.Views.Grid.GridView = GridView1
            Dim ctl As DevExpress.XtraGrid.GridControl = gv.GridControl
            Dim hi As DevExpress.XtraGrid.Views.Grid.ViewInfo.GridHitInfo = gv.CalcHitInfo((ctl.PointToClient(Windows.Forms.Control.MousePosition)))
            Dim RowHandle As System.Int32 = hi.RowHandle

            ' Find the row view in the dataview object for this row.
            Dim drv As System.Data.DataRowView = Nothing
            If RowHandle >= 0 Then
                gv.FocusedRowHandle = RowHandle
                drv = CType(gv.GetRow(RowHandle), System.Data.DataRowView)
            End If

            ' If the row is defined then edit the row.
            If drv IsNot Nothing Then
                drv.BeginEdit()
                If EditRow(drv) = System.Windows.Forms.DialogResult.OK Then
                    drv.EndEdit()
                Else
                    drv.CancelEdit()
                End If
            End If
        End Sub

        ''' <summary>
        ''' Edit the current record
        ''' </summary>
        Private Sub Button_Edit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
            Dim RowHandle As System.Int32 = GridView1.FocusedRowHandle
            Dim vue As System.Data.DataView = CType(GridControl1.DataSource, System.Data.DataView)

            ' Find the row that is being edited
            Dim drv As System.Data.DataRowView = CType(GridView1.GetRow(RowHandle), System.Data.DataRowView)
            If drv IsNot Nothing Then

                ' Change the item
                drv.BeginEdit()
                If EditRow(drv) = System.Windows.Forms.DialogResult.OK Then
                    drv.EndEdit()
                Else
                    drv.CancelEdit()
                End If

                GridView1.FocusedRowHandle = RowHandle
            End If
        End Sub

        ''' <summary>
        ''' Edit -> menu being shown
        ''' </summary>
        Private Sub ContextMenu1_Popup(ByVal sender As Object, ByVal e As System.EventArgs)
            Dim gv As DevExpress.XtraGrid.Views.Grid.GridView = GridView1
            Dim ctl As DevExpress.XtraGrid.GridControl = gv.GridControl
            Dim hi As DevExpress.XtraGrid.Views.Grid.ViewInfo.GridHitInfo = gv.CalcHitInfo((ctl.PointToClient(Windows.Forms.Control.MousePosition)))
            Dim RowHandle As System.Int32 = hi.RowHandle

            ' Find the row view in the dataview object for this row.
            Dim drv As System.Data.DataRowView = Nothing
            If RowHandle >= 0 Then
                gv.FocusedRowHandle = RowHandle
                drv = CType(gv.GetRow(RowHandle), System.Data.DataRowView)
            End If

            ' If the row is defined then edit the row.
            If drv IsNot Nothing Then
                ContextMenu1_Edit.Enabled = True
                ContextMenu1_Delete.Enabled = True
            Else
                ContextMenu1_Edit.Enabled = False
                ContextMenu1_Delete.Enabled = False
            End If

            ' The create is enabled if there is a view for the control
            ContextMenu1_Create.Enabled = ctl.DataSource IsNot Nothing
        End Sub

        ''' <summary>
        ''' Edit -> Create menu item clicked
        ''' </summary>
        Private Sub ContextMenu1_Create_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
            Dim vue As System.Data.DataView = CType(GridControl1.DataSource, System.Data.DataView)

            ' Add the new row to the dataview
            Dim drv As System.Data.DataRowView = vue.AddNew()
            drv.BeginEdit()
            If EditRow(drv) = System.Windows.Forms.DialogResult.OK Then
                drv.EndEdit()
            Else
                drv.CancelEdit()
            End If
        End Sub

        ''' <summary>
        ''' Edit -> Edit menu item clicked
        ''' </summary>
        Private Sub ContextMenu1_Edit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
            Dim gv As DevExpress.XtraGrid.Views.Grid.GridView = GridView1
            Dim ctl As DevExpress.XtraGrid.GridControl = gv.GridControl
            Dim RowHandle As System.Int32 = GridView1.FocusedRowHandle
            Dim drv As System.Data.DataRowView = Nothing

            If RowHandle >= 0 Then
                gv.FocusedRowHandle = RowHandle
                drv = CType(gv.GetRow(RowHandle), System.Data.DataRowView)
            End If

            ' If the row is defined then edit the row.
            If drv IsNot Nothing Then
                drv.BeginEdit()
                If EditRow(drv) = System.Windows.Forms.DialogResult.OK Then
                    drv.EndEdit()
                Else
                    drv.CancelEdit()
                End If
            End If
        End Sub

        ''' <summary>
        ''' Edit -> Delete menu item clicked
        ''' </summary>
        Private Sub ContextMenu1_Delete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
            Dim gv As DevExpress.XtraGrid.Views.Grid.GridView = GridView1
            Dim ctl As DevExpress.XtraGrid.GridControl = gv.GridControl
            Dim RowHandle As System.Int32 = GridView1.FocusedRowHandle
            Dim drv As System.Data.DataRowView = Nothing

            If RowHandle >= 0 Then
                gv.FocusedRowHandle = RowHandle
                drv = CType(gv.GetRow(RowHandle), System.Data.DataRowView)
            End If

            ' If the row is defined then edit the row.
            If drv IsNot Nothing Then
                If DeleteRow(drv) = System.Windows.Forms.DialogResult.Yes Then
                    drv.Delete()
                End If
            End If
        End Sub

        ''' <summary>
        ''' Handle the edit of the information on the form
        ''' </summary>
        Protected Overridable Function EditRow(ByVal drv As System.Data.DataRowView) As System.Windows.Forms.DialogResult

            ' If this is a new item then we must set the defaults from here. The child form does not have this information.
            If drv.IsNew Then

                ' Initialize the default office and day of the week information
                drv("office") = LookUpEdit_Office.EditValue
                drv("dow") = LookUpEdit_Weekday.EditValue

                ' Find the last time for this office/day.
                Dim start_time As System.Int32 = -1
                Dim start_time_object As Object = CType(GridControl1.DataSource, System.Data.DataView).Table.Compute("max([start_time])", String.Empty)
                If (start_time_object IsNot Nothing) AndAlso (start_time_object IsNot System.DBNull.Value) Then start_time = Convert.ToInt32(start_time_object)

                ' Add one hour to the ending time to start the next value in sequence
                If start_time >= (23 * 60) OrElse start_time < 0 Then
                    start_time = 8 * 60
                Else
                    start_time += 60
                End If
                drv("start_time") = start_time
                drv("created_by") = "me"
                drv("date_created") = Date.Now
            End If

            ' Conduct the form and obtain the new values
            Dim answer As System.Windows.Forms.DialogResult
            Using frm As New Form_Item(ctx, drv)
                answer = frm.ShowDialog()
            End Using

            Return answer
        End Function

        ''' <summary>
        ''' Handle the edit of the information on the form
        ''' </summary>
        Protected Overridable Function DeleteRow(ByVal drv As System.Data.DataRowView) As System.Windows.Forms.DialogResult
            Return DebtPlus.Data.Prompts.RequestConfirmation_Delete()
        End Function

        ''' <summary>
        ''' Handle the Cancel button to close the form
        ''' </summary>
        Private Sub Button_Cancel_Click(ByVal sender As Object, ByVal e As System.EventArgs)
            Close()
        End Sub

        ''' <summary>
        ''' The day of the week or the office changed. Reload the grid.
        ''' </summary>
        Private Sub RefreshGrid(ByVal Sender As Object, ByVal e As System.EventArgs)
            SaveChanges()
            ReadGrid()
        End Sub

        ''' <summary>
        ''' Refresh the grid with the changes
        ''' </summary>
        Private Sub ReadGrid()

            ' Do nothing if there is not a complete picture for the data
            If LookUpEdit_Office.EditValue Is Nothing OrElse LookUpEdit_Weekday.EditValue Is Nothing Then Return

            ' Read the grid information from the database
            Dim ds As New System.Data.DataSet("ds")
            Dim cn As SqlClient.SqlConnection = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
            Dim tbl As System.Data.DataTable
            Dim current_cursor As Cursor = System.Windows.Forms.Cursor.Current
            Try
                System.Windows.Forms.Cursor.Current = Cursors.WaitCursor
                cn.Open()

                ' Read the list of times for this office
                Using cmd As SqlClient.SqlCommand = New SqlCommand
                    With cmd
                        .Connection = cn
                        .CommandText = "SELECT a.appt_time_template, a.office, a.dow, a.start_time, a.appt_type, a.date_created, a.created_by, isnull(t.appt_name,'All') AS formatted_appt_type, dbo.template_counselors ( a.appt_time_template ) AS formatted_names FROM appt_time_templates a WITH (NOLOCK) LEFT OUTER JOIN appt_types t WITH (NOLOCK) ON a.appt_type = t.appt_type WHERE a.office=@office AND a.dow=@dow"
                        .Parameters.Add("@office", SqlDbType.Int).Value = Convert.ToInt32(LookUpEdit_Office.EditValue)
                        .Parameters.Add("@dow", SqlDbType.Int).Value = Convert.ToInt32(LookUpEdit_Weekday.EditValue)
                    End With

                    Using da As New SqlClient.SqlDataAdapter(cmd)
                        da.FillLoadOption = LoadOption.OverwriteChanges
                        da.Fill(ds, "appt_time_templates")
                    End Using
                End Using

                tbl = ds.Tables("appt_time_templates")
                With tbl
                    If .PrimaryKey.GetUpperBound(0) < 0 Then .PrimaryKey = New System.Data.DataColumn() {.Columns("appt_time_template")}
                    With .Columns("appt_time_template")
                        .AutoIncrement = True
                        .AutoIncrementSeed = -1
                        .AutoIncrementStep = -1
                    End With
                    .Columns("start_time").DefaultValue = (8 * 60)
                End With

                ' Bind the dataset to the relation
                With GridControl1
                    .DataSource = tbl.DefaultView
                    .RefreshDataSource()
                End With

            Catch ex As SqlClient.SqlException
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error reading template week information")

            Finally
                If cn IsNot Nothing Then cn.Dispose()
                System.Windows.Forms.Cursor.Current = current_cursor
            End Try
        End Sub

        ''' <summary>
        ''' Save the changes when the form is closed
        ''' </summary>
        Private Sub Form_Template_Closing(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs)
            SaveChanges()
        End Sub

        ''' <summary>
        ''' Save changes made to the tables
        ''' </summary>
        Private Sub SaveChanges()
            Dim vue As System.Data.DataView = CType(GridControl1.DataSource, System.Data.DataView)
            If vue IsNot Nothing Then
                Dim tbl As System.Data.DataTable = vue.Table

                ' The update and insert commands are the same since the insert is performed in the item form.
                Dim UpdateCmd As SqlClient.SqlCommand = New SqlCommand
                Dim DeleteCmd As SqlClient.SqlCommand = New SqlCommand

                With UpdateCmd
                    .Connection = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
                    .CommandText = "UPDATE appt_time_templates SET appt_type=@appt_type, office=@office, dow=@dow, start_time=@start_time WHERE appt_time_template=@appt_time_template"
                    .Parameters.Add("@appt_type", SqlDbType.Int, 0, "appt_type")
                    .Parameters.Add("@office", SqlDbType.Int, 0, "office")
                    .Parameters.Add("@dow", SqlDbType.Int, 0, "dow")
                    .Parameters.Add("@start_time", SqlDbType.Int, 0, "start_time")
                    .Parameters.Add("@appt_time_template", SqlDbType.Int, 0, "appt_time_template")
                End With

                ' Possibly delete items from the database. Remove the counselor references at the same time.
                With DeleteCmd
                    .Connection = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
                    .CommandText = "DELETE FROM appt_counselor_templates WHERE appt_time_template=@appt_time_template; DELETE FROM appt_time_templates WHERE appt_time_template=@appt_time_template;"
                    .Parameters.Add("@appt_time_template", SqlDbType.Int, 0, "appt_time_template")
                End With

                ' Apply the updates to the database
                Dim current_cursor As Cursor = System.Windows.Forms.Cursor.Current
                Try
                    System.Windows.Forms.Cursor.Current = Cursors.WaitCursor
                    Using da As New SqlClient.SqlDataAdapter
                        With da
                            .DeleteCommand = DeleteCmd
                            .InsertCommand = UpdateCmd
                            .UpdateCommand = UpdateCmd
                            Dim RowCount As System.Int32 = .Update(tbl)
                        End With
                    End Using
                    tbl.AcceptChanges()

                Catch ex As Exception
                    DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error updating appt_time_templates table")

                Finally
                    System.Windows.Forms.Cursor.Current = current_cursor
                    DeleteCmd.Dispose()
                    UpdateCmd.Dispose()
                End Try
            End If
        End Sub

        ''' <summary>
        ''' Load the office location information
        ''' </summary>
        Private Sub lookupEdit_office_Load(ByRef ctl As DevExpress.XtraEditors.LookUpEdit)

            ' Bind the office list to the data
            With ctl.Properties
                .DataSource = New System.Data.DataView(OfficesTable, "[ActiveFlag]<>0", "name", DataViewRowState.CurrentRows)
                .DisplayMember = "name"
                .ValueMember = "office"
                .PopupWidth = ctl.Width + 125           ' Add some extra space for the dropdown width

                ' Set the default item if possible
                Dim rows() As System.Data.DataRow = OfficesTable.Select("[Default]<>0 AND [ActiveFlag]<>0", "office")
                If rows.GetUpperBound(0) >= 0 Then
                    ctl.EditValue = Convert.ToInt32(rows(0).Item("office"))
                End If
            End With
        End Sub

        ''' <summary>
        ''' Change in lookup edit informaiton item
        ''' </summary>
        Private Sub LookupEdit_EditValueChanging(ByVal sender As Object, ByVal e As DevExpress.XtraEditors.Controls.ChangingEventArgs)

            ' If the new value is defined then make sure that the item is still active.
            With CType(sender, DevExpress.XtraEditors.LookUpEdit)
                If e.NewValue IsNot Nothing Then

                    ' Find the new row being selected
                    Dim drv As DataRowView = CType(.Properties.GetDataSourceRowByKeyValue(e.NewValue), DataRowView)

                    ' From the office, ensure that the active flag is set
                    Dim col As System.Int32 = CType(.Properties.DataSource, System.Data.DataView).Table.Columns.IndexOf("ActiveFlag")
                    If drv IsNot Nothing AndAlso col >= 0 AndAlso Not Convert.ToBoolean(drv(col)) Then
                        e.Cancel = True
                        .ErrorText = "Not Active"
                    End If
                End If
            End With
        End Sub

        ''' <summary>
        ''' Format the relative time value which we store as minutes since 00:00
        ''' </summary>
        Private Class TimeFormatProvider
            Implements ICustomFormatter
            Implements IFormatProvider

            Public Function Format(ByVal FormatString As String, ByVal arg As Object, ByVal formatProvider As System.IFormatProvider) As String Implements System.ICustomFormatter.Format
                Dim ts As New TimeSpan(Convert.ToInt32(arg) * TimeSpan.TicksPerSecond * 60)
                Dim dt As DateTime = DateTime.MinValue.Add(ts)
                Dim answer As String = String.Format("{0:t}", dt)
                Return answer
            End Function

            Public Function GetFormat(ByVal formatType As System.Type) As Object Implements System.IFormatProvider.GetFormat
                Return Me
            End Function
        End Class

#Region " Offices "

        ''' <summary>
        ''' Process the click event on the offices label
        ''' </summary>
        Private Sub lblOffice_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
            Dim Office As System.Int32 = -1
            If LookUpEdit_Office.EditValue IsNot Nothing Then Office = Convert.ToInt32(LookUpEdit_Office.EditValue)

            ' If there is an office then edit the office
            If Office > 0 Then
                Dim tbl As System.Data.DataTable = OfficesTable()
                If tbl IsNot Nothing Then
                    Dim vue As System.Data.DataView = New System.Data.DataView(tbl, String.Format("[office]={0:d}", Office), String.Empty, DataViewRowState.CurrentRows)

                    ' There should be a row for the office.
                    If vue.Count > 0 Then
                        Dim drv As System.Data.DataRowView = vue(0)

                        ' Edit the office information
                        drv.BeginEdit()
                        Using frm As New OfficeEditForm(drv)
                            With frm

                                ' If successful, then complete the edit and rewrite the offices table
                                If .ShowDialog() = System.Windows.Forms.DialogResult.OK Then
                                    drv.EndEdit()
                                    UpdateOfficesTable()
                                    .SaveZipcodes(Convert.ToInt32(drv("office"))) ' Save the zipcodes once the office is defined.
                                Else
                                    drv.CancelEdit()
                                End If
                            End With
                        End Using
                    End If
                End If
            End If
        End Sub

        ''' <summary>
        ''' Update the offices table when it changes
        ''' </summary>
        Private Sub UpdateOfficesTable()
            Dim current_cursor As Cursor = System.Windows.Forms.Cursor.Current

            Dim UpdateCmd As SqlClient.SqlCommand = Offices_UpdateCommand()
            Dim InsertCmd As SqlClient.SqlCommand = Offices_InsertCommand()
            Dim DeleteCmd As SqlClient.SqlCommand = Offices_DeleteCommand()
            Try
                System.Windows.Forms.Cursor.Current = Cursors.WaitCursor

                Using da As New SqlClient.SqlDataAdapter
                    da.UpdateCommand = UpdateCmd
                    da.InsertCommand = InsertCmd
                    da.DeleteCommand = DeleteCmd
                    Dim RowCount As System.Int32 = da.Update(OfficesTable)
                End Using

            Catch ex As SqlClient.SqlException
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error updating offices table")

            Finally
                System.Windows.Forms.Cursor.Current = current_cursor
                InsertCmd.Dispose()
                DeleteCmd.Dispose()
                UpdateCmd.Dispose()
            End Try
        End Sub

        ''' <summary>
        ''' Return the selection command block
        ''' </summary>
        Private Function Offices_SelectCommand() As SqlClient.SqlCommand

            Dim cmd As SqlClient.SqlCommand = New SqlCommand
            With cmd
                .Connection = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
                .CommandText = "SELECT o.office, o.name, o.type, o.AddressID, o.EmailID, o.TelephoneID, o.AltTelephoneID, o.FAXID, o.Default, o.ActiveFlag, o.hud_hcs_id, o.TimeZoneID, o.nfcc, o.default_till_missed, o.counselor, o.region, o.district, o.directions, o.date_created, o.created_by, m.description as formatted_office_type, dbo.format_city_state_zip(o.city, o.state, o.postalcode) as address3, r.description as formatted_region, d.description as formatted_district, co.name as formatted_counselor FROM offices o WITH (NOLOCK) LEFT OUTER JOIN regions r on o.region = r.region LEFT OUTER JOIN districts d ON o.district = d.district LEFT OUTER JOIN counselors co ON o.counselor = co.counselor LEFT OUTER JOIN OfficeTypes m ON o.type = m.oID"
            End With
            Return cmd
        End Function

        ''' <summary>
        ''' Return the insert command block
        ''' </summary>
        Protected Function Offices_InsertCommand() As SqlClient.SqlCommand

            Dim cmd As SqlClient.SqlCommand = New SqlCommand
            With cmd
                .Connection = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
                .CommandText = "xpr_insert_offices"
                .CommandType = CommandType.StoredProcedure
                .Parameters.Add("@RETURN_VALUE", SqlDbType.Int, 0, "office").Direction = ParameterDirection.ReturnValue

                .Parameters.Add("@name", SqlDbType.VarChar, 80, "name")
                .Parameters.Add("@type", SqlDbType.Int, 0, "type")
                .Parameters.Add("@AddressID", SqlDbType.Int, 0, "AddressID")
                .Parameters.Add("@directions", SqlDbType.Text, Int32.MaxValue, "directions")
                .Parameters.Add("@TelephoneID", SqlDbType.Int, 0, "TelephoneID")
                .Parameters.Add("@FAXID", SqlDbType.Int, 0, "FAXID")
                .Parameters.Add("@default_till_missed", SqlDbType.Int, 0, "default_till_missed")
                .Parameters.Add("@counselor", SqlDbType.Int, 0, "counselor")
                .Parameters.Add("@region", SqlDbType.Int, 0, "region")
                .Parameters.Add("@district", SqlDbType.Int, 0, "district")
                .Parameters.Add("@default", SqlDbType.Bit, 0, "default")
                .Parameters.Add("@activeflag", SqlDbType.Bit, 0, "activeflag")
            End With
            Return cmd
        End Function

        ''' <summary>
        ''' Return the update command block
        ''' </summary>
        Protected Function Offices_UpdateCommand() As SqlClient.SqlCommand

            Dim cmd As SqlClient.SqlCommand = New SqlCommand
            With cmd
                .Connection = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
                .CommandText = "UPDATE offices SET [name]=@name,[type]=@type,[AddressID]=@AddressID,[directions]=@directions,[TelephoneID]=@TelephoneID,[FAXID]=@FAXID,[default_till_missed]=@default_till_missed,[counselor]=@counselor,[region]=@region,[district]=@district,[default]=@default,[activeflag]=@activeflag WHERE office=@office"
                .Parameters.Add("@name", SqlDbType.VarChar, 80, "name")
                .Parameters.Add("@type", SqlDbType.Int, 0, "type")
                .Parameters.Add("@AddressID", SqlDbType.Int, 0, "AddressID")
                .Parameters.Add("@directions", SqlDbType.Text, Int32.MaxValue, "directions")
                .Parameters.Add("@TelephoneID", SqlDbType.Int, 0, "TelephoneID")
                .Parameters.Add("@FAXID", SqlDbType.Int, 0, "FAXID")
                .Parameters.Add("@default_till_missed", SqlDbType.Int, 0, "default_till_missed")
                .Parameters.Add("@counselor", SqlDbType.Int, 0, "counselor")
                .Parameters.Add("@region", SqlDbType.Int, 0, "region")
                .Parameters.Add("@district", SqlDbType.Int, 0, "district")
                .Parameters.Add("@default", SqlDbType.Bit, 0, "default")
                .Parameters.Add("@activeflag", SqlDbType.Bit, 0, "activeflag")
                .Parameters.Add("@office", SqlDbType.Int, 0, "office")
            End With
            Return cmd
        End Function

        ''' <summary>
        ''' Return the delete command block
        ''' </summary>
        Protected Function Offices_DeleteCommand() As SqlClient.SqlCommand
            Dim cmd As SqlClient.SqlCommand = New SqlCommand
            With cmd
                .Connection = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
                .CommandText = "DELETE FROM offices WHERE office=@office"
                .Parameters.Add("@office", SqlDbType.Int, 0, "office")
            End With
            Return cmd
        End Function
#End Region
    End Class
End Namespace
