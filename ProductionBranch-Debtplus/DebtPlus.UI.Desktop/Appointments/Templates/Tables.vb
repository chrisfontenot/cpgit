#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}
#End Region

Option Compare Binary
Option Explicit On 
Option Strict On

Imports DebtPlus.Utils
Imports System.Data.SqlClient
Imports System.Windows.Forms

Namespace Appointments.Templates

    Friend Module Tables

        ' Dataset for the table storage
        Private ReadOnly MyDs As New DataSet("MyDs")
        Friend ds As New DataSet("ds")

#Region " Day Of The Week "

        ''' <summary>
        ''' Days of the week for the template table
        ''' </summary>
        Friend Function DayOfWeekTable() As DataTable
            Dim tbl As DataTable = MyDs.Tables("days_of_week")
            If tbl Is Nothing Then
                tbl = New DataTable("days_of_week")
                With tbl

                    ' Define the table as a list of numbers attached to the local names of the days of the week.
                    ' Numbers range from 0(sunday) through 6(saturday)
                    .Columns.Add("day_of_week", GetType(Int32))
                    .Columns.Add("description", GetType(String))
                    .PrimaryKey = New DataColumn() {.Columns(0)}

                    ' Add the values to the table for the corresponding days of the week
                    .BeginLoadData()
                    For Each dow As DayOfWeek In New Object() {DayOfWeek.Sunday, DayOfWeek.Monday, DayOfWeek.Tuesday, DayOfWeek.Wednesday, DayOfWeek.Thursday, DayOfWeek.Friday, DayOfWeek.Saturday}
                        .Rows.Add(New Object() {Convert.ToInt32(dow) + 1, System.Globalization.CultureInfo.CurrentCulture.DateTimeFormat().GetDayName(dow)})
                    Next
                    .EndLoadData()

                    .AcceptChanges()
                    MyDs.Tables.Add(tbl)
                End With
            End If

            Return tbl
        End Function
#End Region

#Region " Office Types "

        ''' <summary>
        ''' Office Type table
        ''' </summary>
        Friend Function OfficeTypesTable() As DataTable
            Const MyTableName As String = "office_types"
            Dim tbl As DataTable = MyDs.Tables(MyTableName)

            ' If the table is not found then read the information from the database
            If tbl Is Nothing Then
                Dim current_cursor As Cursor = Cursor.Current
                Try
                    Cursor.Current = Cursors.WaitCursor
                    Using cmd As SqlClient.SqlCommand = New SqlCommand
                        With cmd
                            .Connection = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
                            .CommandText = "SELECT oID as type, description FROM OfficeTypes WITH (NOLOCK)"
                        End With

                        ' Ask the database for the information
                        Using da As New SqlClient.SqlDataAdapter(cmd)
                            da.Fill(MyDs, MyTableName)
                        End Using
                    End Using

                    tbl = MyDs.Tables(MyTableName)
                    tbl.PrimaryKey = New DataColumn() {tbl.Columns(0)}

                    ' When we do an update, the update results in zero rows being updated. IGNORE IT.
                Catch ex As DBConcurrencyException
                    DebtPlus.Svc.Common.ErrorHandling.HandleErrors(ex)

                    ' Something happend in trying to talk to the database.
                Catch ex As SqlClient.SqlException
                    Using gdr As New Repository.GetDataResult()
                        gdr.HandleException(ex)
                        DebtPlus.UI.Common.ErrorHandling.HandleErrors(gdr, String.Format("Error reading {0} table", MyTableName))
                    End Using

                Finally
                    Cursor.Current = current_cursor
                End Try
            End If

            Return tbl
        End Function
#End Region

#Region " Offices "

        ''' <summary>
        ''' Offices information table
        ''' </summary>
        Friend Function OfficesTable() As DataTable
            Const MyTableName As String = "offices"
            Dim tbl As DataTable = MyDs.Tables(MyTableName)

            ' If the table is not found then read the information from the database
            If tbl Is Nothing Then
                Dim current_cursor As Cursor = Cursor.Current
                Try
                    Cursor.Current = Cursors.WaitCursor
                    Using cmd As SqlClient.SqlCommand = New SqlCommand
                        With cmd
                            .Connection = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
                            .CommandText = "SELECT o.[office], o.[name], o.[type], o.[AddressID], o.[EmailID], o.[TelephoneID], o.[AltTelephoneID], o.[FAXID], o.[Default], o.[ActiveFlag], o.[hud_hcs_id], o.[TimeZoneID], o.[nfcc], o.[default_till_missed], o.[counselor], o.[region], o.[district], o.[directions], o.[date_created], o.[created_by], m.[description] as formatted_office_type FROM offices o WITH (NOLOCK) LEFT OUTER JOIN OfficeTypes m ON o.type = m.oID WHERE o.ActiveFlag <> 0"
                        End With

                        ' Ask the database for the information
                        Using da As New SqlClient.SqlDataAdapter(cmd)
                            da.Fill(MyDs, MyTableName)
                        End Using
                    End Using

                    tbl = MyDs.Tables(MyTableName)

                    ' When we do an update, the update results in zero rows being updated. IGNORE IT.
                Catch ex As DBConcurrencyException
                    DebtPlus.Svc.Common.ErrorHandling.HandleErrors(ex)

                    ' Something happend in trying to talk to the database.
                Catch ex As SqlClient.SqlException
                    Using gdr As New Repository.GetDataResult()
                        gdr.HandleException(ex)
                        DebtPlus.UI.Common.ErrorHandling.HandleErrors(gdr, String.Format("Error reading {0} table", MyTableName))
                    End Using

                Finally
                    Cursor.Current = current_cursor
                End Try
            End If

            Return tbl
        End Function
#End Region

#Region " Counselors "

        ''' <summary>
        ''' Counselors information table
        ''' </summary>
        Friend Function CounselorsTable() As DataTable
            Const MyTableName As String = "counselors"
            Dim tbl As DataTable = MyDs.Tables(MyTableName)

            ' If the table is not found then read the information from the database
            If tbl Is Nothing Then
                Dim current_cursor As Cursor = Cursor.Current
                Try
                    Cursor.Current = Cursors.WaitCursor
                    Using cmd As SqlClient.SqlCommand = New SqlCommand
                        With cmd
                            .CommandType = CommandType.Text
                            .Connection = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
                            .CommandText = "SELECT [Counselor],[NameID],[EmailID],[TelephoneID],[Person],[Manager],[Note],[Default],[ActiveFlag],[Office],[Menu_Level],[Color],[Image],[SSN],[billing_mode],[rate],[emp_start_date],[emp_end_date],[HUD_id],[CaseCounter],[RxOfficeCounselorID],[date_created],[created_by] FROM counselors WITH (NOLOCK) WHERE ActiveFlag <> 0"
                        End With

                        ' Ask the database for the information
                        Using da As New SqlClient.SqlDataAdapter(cmd)
                            da.Fill(MyDs, MyTableName)
                        End Using
                    End Using

                    tbl = MyDs.Tables(MyTableName)

                    ' When we do an update, the update results in zero rows being updated. IGNORE IT.
                Catch ex As DBConcurrencyException
                    DebtPlus.Svc.Common.ErrorHandling.HandleErrors(ex)

                    ' Something happend in trying to talk to the database.
                Catch ex As SqlClient.SqlException
                    Using gdr As New Repository.GetDataResult()
                        gdr.HandleException(ex)
                        DebtPlus.UI.Common.ErrorHandling.HandleErrors(gdr, String.Format("Error reading {0} table", MyTableName))
                    End Using

                Finally
                    Cursor.Current = current_cursor
                End Try
            End If

            Return tbl
        End Function
#End Region

#Region " Regions "

        ''' <summary>
        ''' region information table
        ''' </summary>
        Friend Function RegionsTable() As DataTable
            Const MyTableName As String = "regions"
            Dim tbl As DataTable = MyDs.Tables(MyTableName)

            ' If the table is not found then read the information from the database
            If tbl Is Nothing Then
                Dim current_cursor As Cursor = Cursor.Current
                Try
                    Cursor.Current = Cursors.WaitCursor
                    Using cmd As SqlClient.SqlCommand = New SqlCommand
                        With cmd
                            .Connection = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
                            .CommandText = "SELECT [region],[description],[ManagerName],[default],[ActiveFlag],[created_by],[date_created] FROM regions WITH (NOLOCK)"
                        End With

                        ' Ask the database for the information
                        Using da As New SqlClient.SqlDataAdapter(cmd)
                            da.Fill(MyDs, MyTableName)
                        End Using
                    End Using

                    tbl = MyDs.Tables(MyTableName)

                    ' When we do an update, the update results in zero rows being updated. IGNORE IT.
                Catch ex As DBConcurrencyException
                    DebtPlus.Svc.Common.ErrorHandling.HandleErrors(ex)

                    ' Something happend in trying to talk to the database.
                Catch ex As SqlClient.SqlException
                    Using gdr As New Repository.GetDataResult()
                        gdr.HandleException(ex)
                        DebtPlus.UI.Common.ErrorHandling.HandleErrors(gdr, String.Format("Error reading {0} table", MyTableName))
                    End Using

                Finally
                    Cursor.Current = current_cursor
                End Try
            End If

            Return tbl
        End Function
#End Region

#Region " Districts "

        ''' <summary>
        ''' district information table
        ''' </summary>
        Friend Function DistrictsTable() As DataTable
            Const MyTableName As String = "districts"
            Dim tbl As DataTable = MyDs.Tables(MyTableName)

            ' If the table is not found then read the information from the database
            If tbl Is Nothing Then
                Dim current_cursor As Cursor = Cursor.Current
                Try
                    Cursor.Current = Cursors.WaitCursor
                    Using cmd As SqlClient.SqlCommand = New SqlCommand
                        With cmd
                            .Connection = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
                            .CommandText = "SELECT [district] as 'District', [description] as 'Description', [ActiveFlag], [default] FROM districts WITH (NOLOCK)"
                        End With

                        ' Ask the database for the information
                        Using da As New SqlClient.SqlDataAdapter(cmd)
                            da.Fill(MyDs, MyTableName)
                        End Using
                    End Using

                    tbl = MyDs.Tables(MyTableName)

                    ' When we do an update, the update results in zero rows being updated. IGNORE IT.
                Catch ex As DBConcurrencyException
                    DebtPlus.Svc.Common.ErrorHandling.HandleErrors(ex)

                    ' Something happend in trying to talk to the database.
                Catch ex As SqlClient.SqlException
                    Using gdr As New Repository.GetDataResult()
                        gdr.HandleException(ex)
                        DebtPlus.UI.Common.ErrorHandling.HandleErrors(gdr, String.Format("Error reading {0} table", MyTableName))
                    End Using

                Finally
                    Cursor.Current = current_cursor
                End Try
            End If

            Return tbl
        End Function
#End Region

#Region " ApptTypes "

        ''' <summary>
        ''' ApptTypes information table
        ''' </summary>
        Friend Function ApptTypesTable() As DataTable
            Const MyTableName As String = "appt_types"
            Dim tbl As DataTable = MyDs.Tables(MyTableName)

            ' If the table is not found then read the information from the database
            If tbl Is Nothing Then
                Dim current_cursor As Cursor = Cursor.Current
                Try
                    Cursor.Current = Cursors.WaitCursor
                    Using cmd As SqlClient.SqlCommand = New SqlCommand
                        With cmd
                            .Connection = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
                            .CommandText = "SELECT [appt_type],[appt_name],[appt_duration],[default],[housing],[credit],[bankruptcy],[initial_appt],[contact_type],[missed_retention_event],[create_letter],[reschedule_letter],[cancel_letter],[bankruptcy_class],[date_created],[created_by] FROM appt_types WITH (NOLOCK)"
                        End With

                        ' Ask the database for the information
                        Using da As New SqlClient.SqlDataAdapter(cmd)
                            da.Fill(MyDs, MyTableName)
                        End Using
                    End Using

                    tbl = MyDs.Tables(MyTableName)
                    tbl.PrimaryKey = New DataColumn() {tbl.Columns("appt_type")}

                    ' When we do an update, the update results in zero rows being updated. IGNORE IT.
                Catch ex As DBConcurrencyException
                    DebtPlus.Svc.Common.ErrorHandling.HandleErrors(ex)

                    ' Something happend in trying to talk to the database.
                Catch ex As SqlClient.SqlException
                    Using gdr As New Repository.GetDataResult()
                        gdr.HandleException(ex)
                        DebtPlus.UI.Common.ErrorHandling.HandleErrors(gdr, String.Format("Error reading {0} table", MyTableName))
                    End Using

                Finally
                    Cursor.Current = current_cursor
                End Try
            End If

            Return tbl
        End Function
#End Region

        ''' <summary>
        ''' Find the default timezone item.
        ''' </summary>
        ''' <returns>The ID of the default item</returns>
        ''' <remarks></remarks>
        Friend Function DefaultTimeZone() As Int32
            Dim answer As Int32 = -1
            Dim tbl As DataTable = TimeZonesTable()

            ' Look first for the item that is marked "Default"
            If tbl IsNot Nothing Then
                Dim rows() As DataRow = tbl.Select("[default]<>false", "oID")
                If rows.GetUpperBound(0) >= 0 Then
                    answer = DebtPlus.Utils.Nulls.DInt(rows(0)("oID"))
                End If

                ' Failing that, take the first item in the table
                If answer <= 0 Then
                    Dim obj As Object = tbl.Compute("min(oID)", String.Empty)
                    answer = DebtPlus.Utils.Nulls.DInt(obj)
                End If
            End If

            ' If there is no default, assume a value.
            If answer <= 0 Then
                answer = 11
            End If

            Return answer
        End Function

        ''' <summary>
        ''' TimeZone information table
        ''' </summary>
        Friend Function TimeZonesTable() As DataTable
            Const MyTableName As String = "TimeZones"
            Dim tbl As DataTable = ds.Tables(MyTableName)

            ' If the table is not found then read the information from the database
            If tbl Is Nothing Then
                Dim cmd As SqlClient.SqlCommand = New SqlCommand
                With cmd
                    .Connection = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
                    .CommandText = "SELECT oID, description, [GMTOffset], [default] FROM TimeZones WITH (NOLOCK)"
                End With

                ' Ask the database for the information
                Dim da As New SqlClient.SqlDataAdapter(cmd)
                Dim current_cursor As Cursor = Cursor.Current
                Try
                    Cursor.Current = Cursors.WaitCursor
                    da.Fill(ds, MyTableName)
                    tbl = ds.Tables(MyTableName)

                    ' When we do an update, the update results in zero rows being updated. IGNORE IT.
                Catch ex As DBConcurrencyException
                    DebtPlus.Svc.Common.ErrorHandling.HandleErrors(ex)

                    ' Something happend in trying to talk to the database.
                Catch ex As SqlClient.SqlException
                    Using gdr As New Repository.GetDataResult()
                        gdr.HandleException(ex)
                        DebtPlus.UI.Common.ErrorHandling.HandleErrors(gdr, String.Format("Error reading {0} table", MyTableName))
                    End Using

                Finally
                    Cursor.Current = current_cursor
                End Try
            End If

            Return tbl
        End Function

    End Module
End Namespace
