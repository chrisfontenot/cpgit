#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}
#End Region

Option Compare Binary
Option Explicit On 
Option Strict On

Imports System.Windows.Forms

Namespace Appointments.Templates
    Friend Module CheckedListControl

        ''' <summary>
        ''' Class to handle the checked status of our indicators
        ''' </summary>
        Friend Class MyCheckedControl
            Inherits DevExpress.XtraEditors.Controls.CheckedListBoxItem

            ''' <summary>
            ''' Datarowview to the current datarow for the indicator
            ''' </summary>
            Public Property drv() As DataRowView

            ''' <summary>
            ''' Create a blank class
            ''' </summary>
            Public Sub New()
                MyBase.New()
            End Sub

            Public Sub New(ByVal value As Object)
                MyBase.New(value)
            End Sub

            Public Sub New(ByVal value As Object, ByVal isChecked As Boolean)
                MyBase.New(value, isChecked)
            End Sub

            Public Sub New(ByVal value As Object, ByVal description As String)
                MyBase.New(value, description)
            End Sub

            Public Sub New(ByVal value As Object, ByVal checkState As CheckState)
                MyBase.New(value, checkState)
            End Sub

            Public Sub New(ByVal value As Object, ByVal description As String, ByVal checkState As CheckState)
                MyBase.New(value, description, checkState)
            End Sub

            Public Sub New(ByVal value As Object, ByVal checkState As CheckState, ByVal enabled As Boolean)
                MyBase.New(value, checkState, enabled)
            End Sub

            Public Sub New(ByVal value As Object, ByVal description As String, ByVal checkState As CheckState, ByVal enabled As Boolean)
                MyBase.New(value, description, checkState, enabled)
            End Sub

            ''' <summary>
            ''' Create the class with my parameters
            ''' </summary>
            Public Sub New(ByVal NewDrv As DataRowView)
                MyClass.New()

                ' Save the value for the display text
                Dim StringName As String = MyBase.ToString()
                If NewDrv IsNot Nothing Then
                    Dim col As Int32 = NewDrv.DataView.Table.Columns.IndexOf("name")
                    If col >= 0 Then
                        If NewDrv(col) IsNot Nothing AndAlso NewDrv(col) IsNot DBNull.Value Then StringName = Convert.ToString(NewDrv(col)).Trim()
                    End If
                End If
                MyBase.Value = StringName

                ' Check the column accordingly
                CheckState = System.Windows.Forms.CheckState.Unchecked
                If NewDrv IsNot Nothing Then

                    ' Find the checked status indicator
                    Dim col As Int32 = NewDrv.DataView.Table.Columns.IndexOf("checked")
                    If col >= 0 Then
                        If NewDrv(col) IsNot Nothing AndAlso NewDrv(col) IsNot DBNull.Value AndAlso Convert.ToBoolean(NewDrv(col)) Then CheckState = System.Windows.Forms.CheckState.Checked
                    End If

                    ' Find the checked status indicator
                    Enabled = True
                    col = NewDrv.DataView.Table.Columns.IndexOf("ActiveFlag")
                    If col >= 0 Then
                        If NewDrv(col) IsNot Nothing AndAlso NewDrv(col) IsNot DBNull.Value AndAlso Not Convert.ToBoolean(NewDrv(col)) Then Enabled = False
                    End If
                End If

                ' After the initialization is complete then store the pointer to the row. This prevents
                ' the initialization from tripping the "changed" logic.
                drv = NewDrv
            End Sub

        End Class
    End Module
End Namespace
