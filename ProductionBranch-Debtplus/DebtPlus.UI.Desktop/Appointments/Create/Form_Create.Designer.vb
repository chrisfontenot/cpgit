﻿Namespace Appointments.Create
    Partial Class Form_Create
        Inherits DebtPlus.Data.Forms.DebtPlusForm

        'Form overrides dispose to clean up the component list.
        Protected Overrides Sub Dispose(ByVal disposing As Boolean)
            Try
                If disposing Then
                    If components IsNot Nothing Then components.Dispose()
                    If bt IsNot Nothing Then bt.Dispose()
                    If ds IsNot Nothing Then ds.Dispose()
                    If busyDialog IsNot Nothing Then busyDialog.Dispose()
                End If

                ' Clear any large objects that don't support Dispose
                tbl = Nothing
                If lstProcessing IsNot Nothing Then
                    lstProcessing.Clear()
                End If

            Finally
                MyBase.Dispose(disposing)
            End Try
        End Sub

        'Required by the Windows Form Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Windows Form Designer
        'It can be modified using the Windows Form Designer.  
        'Do not modify it using the code editor.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
            Me.GridControl1 = New DevExpress.XtraGrid.GridControl
            Me.GridView1 = New DevExpress.XtraGrid.Views.Grid.GridView
            Me.GridColumn_office = New DevExpress.XtraGrid.Columns.GridColumn
            Me.GridColumn_office.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True
            Me.GridColumn_office_name = New DevExpress.XtraGrid.Columns.GridColumn
            Me.GridColumn_office_name.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True
            Me.GridColumn_SU = New DevExpress.XtraGrid.Columns.GridColumn
            Me.GridColumn_SU.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True
            Me.DayOfWeekRepositoryCheckedit = New DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit
            Me.GridColumn_M = New DevExpress.XtraGrid.Columns.GridColumn
            Me.GridColumn_M.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True
            Me.GridColumn_TU = New DevExpress.XtraGrid.Columns.GridColumn
            Me.GridColumn_TU.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True
            Me.GridColumn_W = New DevExpress.XtraGrid.Columns.GridColumn
            Me.GridColumn_W.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True
            Me.GridColumn_TH = New DevExpress.XtraGrid.Columns.GridColumn
            Me.GridColumn_TH.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True
            Me.GridColumn_F = New DevExpress.XtraGrid.Columns.GridColumn
            Me.GridColumn_F.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True
            Me.GridColumn_SA = New DevExpress.XtraGrid.Columns.GridColumn
            Me.GridColumn_SA.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True
            Me.Button_SelectAll = New DevExpress.XtraEditors.SimpleButton
            Me.Button_OK = New DevExpress.XtraEditors.SimpleButton
            Me.Button_Cancel = New DevExpress.XtraEditors.SimpleButton
            Me.DateNavigator1 = New DevExpress.XtraScheduler.DateNavigator
            CType(Me.DxErrorProvider1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.GridView1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.DayOfWeekRepositoryCheckedit, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.DateNavigator1, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.SuspendLayout()
            '
            'GridControl1
            '
            Me.GridControl1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                                             Or System.Windows.Forms.AnchorStyles.Left) _
                                            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.GridControl1.Location = New System.Drawing.Point(8, 8)
            Me.GridControl1.MainView = Me.GridView1
            Me.GridControl1.Name = "GridControl1"
            Me.GridControl1.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.DayOfWeekRepositoryCheckedit})
            Me.GridControl1.Size = New System.Drawing.Size(360, 167)
            Me.GridControl1.TabIndex = 0
            Me.GridControl1.ToolTipController = Me.ToolTipController1
            Me.GridControl1.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridView1})
            '
            'GridView1
            '
            Me.GridView1.Appearance.ColumnFilterButton.BackColor = System.Drawing.Color.FromArgb(CType(CType(104, Byte), Int32), CType(CType(184, Byte), Int32), CType(CType(251, Byte), Int32))
            Me.GridView1.Appearance.ColumnFilterButton.BorderColor = System.Drawing.Color.FromArgb(CType(CType(104, Byte), Int32), CType(CType(184, Byte), Int32), CType(CType(251, Byte), Int32))
            Me.GridView1.Appearance.ColumnFilterButton.ForeColor = System.Drawing.Color.White
            Me.GridView1.Appearance.ColumnFilterButton.Options.UseBackColor = True
            Me.GridView1.Appearance.ColumnFilterButton.Options.UseBorderColor = True
            Me.GridView1.Appearance.ColumnFilterButton.Options.UseForeColor = True
            Me.GridView1.Appearance.ColumnFilterButtonActive.BackColor = System.Drawing.Color.FromArgb(CType(CType(170, Byte), Int32), CType(CType(216, Byte), Int32), CType(CType(254, Byte), Int32))
            Me.GridView1.Appearance.ColumnFilterButtonActive.BorderColor = System.Drawing.Color.FromArgb(CType(CType(170, Byte), Int32), CType(CType(216, Byte), Int32), CType(CType(254, Byte), Int32))
            Me.GridView1.Appearance.ColumnFilterButtonActive.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.ColumnFilterButtonActive.Options.UseBackColor = True
            Me.GridView1.Appearance.ColumnFilterButtonActive.Options.UseBorderColor = True
            Me.GridView1.Appearance.ColumnFilterButtonActive.Options.UseForeColor = True
            Me.GridView1.Appearance.Empty.BackColor = System.Drawing.Color.FromArgb(CType(CType(236, Byte), Int32), CType(CType(246, Byte), Int32), CType(CType(255, Byte), Int32))
            Me.GridView1.Appearance.Empty.BackColor2 = System.Drawing.Color.White
            Me.GridView1.Appearance.Empty.Options.UseBackColor = True
            Me.GridView1.Appearance.EvenRow.BackColor = System.Drawing.Color.FromArgb(CType(CType(247, Byte), Int32), CType(CType(251, Byte), Int32), CType(CType(255, Byte), Int32))
            Me.GridView1.Appearance.EvenRow.BorderColor = System.Drawing.Color.FromArgb(CType(CType(247, Byte), Int32), CType(CType(251, Byte), Int32), CType(CType(255, Byte), Int32))
            Me.GridView1.Appearance.EvenRow.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.EvenRow.Options.UseBackColor = True
            Me.GridView1.Appearance.EvenRow.Options.UseBorderColor = True
            Me.GridView1.Appearance.EvenRow.Options.UseForeColor = True
            Me.GridView1.Appearance.FilterCloseButton.BackColor = System.Drawing.Color.FromArgb(CType(CType(104, Byte), Int32), CType(CType(184, Byte), Int32), CType(CType(251, Byte), Int32))
            Me.GridView1.Appearance.FilterCloseButton.BorderColor = System.Drawing.Color.FromArgb(CType(CType(104, Byte), Int32), CType(CType(184, Byte), Int32), CType(CType(251, Byte), Int32))
            Me.GridView1.Appearance.FilterCloseButton.ForeColor = System.Drawing.Color.White
            Me.GridView1.Appearance.FilterCloseButton.Options.UseBackColor = True
            Me.GridView1.Appearance.FilterCloseButton.Options.UseBorderColor = True
            Me.GridView1.Appearance.FilterCloseButton.Options.UseForeColor = True
            Me.GridView1.Appearance.FilterPanel.BackColor = System.Drawing.Color.FromArgb(CType(CType(236, Byte), Int32), CType(CType(246, Byte), Int32), CType(CType(255, Byte), Int32))
            Me.GridView1.Appearance.FilterPanel.BackColor2 = System.Drawing.Color.White
            Me.GridView1.Appearance.FilterPanel.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.FilterPanel.Options.UseBackColor = True
            Me.GridView1.Appearance.FilterPanel.Options.UseForeColor = True
            Me.GridView1.Appearance.FixedLine.BackColor = System.Drawing.Color.FromArgb(CType(CType(59, Byte), Int32), CType(CType(133, Byte), Int32), CType(CType(195, Byte), Int32))
            Me.GridView1.Appearance.FixedLine.Options.UseBackColor = True
            Me.GridView1.Appearance.FocusedCell.BackColor = System.Drawing.Color.White
            Me.GridView1.Appearance.FocusedCell.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.FocusedCell.Options.UseBackColor = True
            Me.GridView1.Appearance.FocusedCell.Options.UseForeColor = True
            Me.GridView1.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb(CType(CType(38, Byte), Int32), CType(CType(109, Byte), Int32), CType(CType(189, Byte), Int32))
            Me.GridView1.Appearance.FocusedRow.BorderColor = System.Drawing.Color.FromArgb(CType(CType(59, Byte), Int32), CType(CType(139, Byte), Int32), CType(CType(206, Byte), Int32))
            Me.GridView1.Appearance.FocusedRow.ForeColor = System.Drawing.Color.White
            Me.GridView1.Appearance.FocusedRow.Options.UseBackColor = True
            Me.GridView1.Appearance.FocusedRow.Options.UseBorderColor = True
            Me.GridView1.Appearance.FocusedRow.Options.UseForeColor = True
            Me.GridView1.Appearance.FooterPanel.BackColor = System.Drawing.Color.FromArgb(CType(CType(104, Byte), Int32), CType(CType(184, Byte), Int32), CType(CType(251, Byte), Int32))
            Me.GridView1.Appearance.FooterPanel.BorderColor = System.Drawing.Color.FromArgb(CType(CType(104, Byte), Int32), CType(CType(184, Byte), Int32), CType(CType(251, Byte), Int32))
            Me.GridView1.Appearance.FooterPanel.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.FooterPanel.Options.UseBackColor = True
            Me.GridView1.Appearance.FooterPanel.Options.UseBorderColor = True
            Me.GridView1.Appearance.FooterPanel.Options.UseForeColor = True
            Me.GridView1.Appearance.GroupButton.BackColor = System.Drawing.Color.FromArgb(CType(CType(104, Byte), Int32), CType(CType(184, Byte), Int32), CType(CType(251, Byte), Int32))
            Me.GridView1.Appearance.GroupButton.BorderColor = System.Drawing.Color.FromArgb(CType(CType(104, Byte), Int32), CType(CType(184, Byte), Int32), CType(CType(251, Byte), Int32))
            Me.GridView1.Appearance.GroupButton.Options.UseBackColor = True
            Me.GridView1.Appearance.GroupButton.Options.UseBorderColor = True
            Me.GridView1.Appearance.GroupFooter.BackColor = System.Drawing.Color.FromArgb(CType(CType(170, Byte), Int32), CType(CType(216, Byte), Int32), CType(CType(254, Byte), Int32))
            Me.GridView1.Appearance.GroupFooter.BorderColor = System.Drawing.Color.FromArgb(CType(CType(170, Byte), Int32), CType(CType(216, Byte), Int32), CType(CType(254, Byte), Int32))
            Me.GridView1.Appearance.GroupFooter.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.GroupFooter.Options.UseBackColor = True
            Me.GridView1.Appearance.GroupFooter.Options.UseBorderColor = True
            Me.GridView1.Appearance.GroupFooter.Options.UseForeColor = True
            Me.GridView1.Appearance.GroupPanel.BackColor = System.Drawing.Color.FromArgb(CType(CType(236, Byte), Int32), CType(CType(246, Byte), Int32), CType(CType(255, Byte), Int32))
            Me.GridView1.Appearance.GroupPanel.BackColor2 = System.Drawing.Color.White
            Me.GridView1.Appearance.GroupPanel.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.GroupPanel.Options.UseBackColor = True
            Me.GridView1.Appearance.GroupPanel.Options.UseForeColor = True
            Me.GridView1.Appearance.GroupRow.BackColor = System.Drawing.Color.FromArgb(CType(CType(170, Byte), Int32), CType(CType(216, Byte), Int32), CType(CType(254, Byte), Int32))
            Me.GridView1.Appearance.GroupRow.BorderColor = System.Drawing.Color.FromArgb(CType(CType(170, Byte), Int32), CType(CType(216, Byte), Int32), CType(CType(254, Byte), Int32))
            Me.GridView1.Appearance.GroupRow.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.GroupRow.Options.UseBackColor = True
            Me.GridView1.Appearance.GroupRow.Options.UseBorderColor = True
            Me.GridView1.Appearance.GroupRow.Options.UseForeColor = True
            Me.GridView1.Appearance.HeaderPanel.BackColor = System.Drawing.Color.FromArgb(CType(CType(139, Byte), Int32), CType(CType(201, Byte), Int32), CType(CType(254, Byte), Int32))
            Me.GridView1.Appearance.HeaderPanel.BorderColor = System.Drawing.Color.FromArgb(CType(CType(139, Byte), Int32), CType(CType(201, Byte), Int32), CType(CType(254, Byte), Int32))
            Me.GridView1.Appearance.HeaderPanel.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.HeaderPanel.Options.UseBackColor = True
            Me.GridView1.Appearance.HeaderPanel.Options.UseBorderColor = True
            Me.GridView1.Appearance.HeaderPanel.Options.UseForeColor = True
            Me.GridView1.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Int32), CType(CType(170, Byte), Int32), CType(CType(225, Byte), Int32))
            Me.GridView1.Appearance.HideSelectionRow.BorderColor = System.Drawing.Color.FromArgb(CType(CType(83, Byte), Int32), CType(CType(155, Byte), Int32), CType(CType(215, Byte), Int32))
            Me.GridView1.Appearance.HideSelectionRow.ForeColor = System.Drawing.Color.FromArgb(CType(CType(236, Byte), Int32), CType(CType(246, Byte), Int32), CType(CType(255, Byte), Int32))
            Me.GridView1.Appearance.HideSelectionRow.Options.UseBackColor = True
            Me.GridView1.Appearance.HideSelectionRow.Options.UseBorderColor = True
            Me.GridView1.Appearance.HideSelectionRow.Options.UseForeColor = True
            Me.GridView1.Appearance.HorzLine.BackColor = System.Drawing.Color.FromArgb(CType(CType(104, Byte), Int32), CType(CType(184, Byte), Int32), CType(CType(251, Byte), Int32))
            Me.GridView1.Appearance.HorzLine.Options.UseBackColor = True
            Me.GridView1.Appearance.OddRow.BackColor = System.Drawing.Color.FromArgb(CType(CType(236, Byte), Int32), CType(CType(246, Byte), Int32), CType(CType(255, Byte), Int32))
            Me.GridView1.Appearance.OddRow.BorderColor = System.Drawing.Color.FromArgb(CType(CType(236, Byte), Int32), CType(CType(246, Byte), Int32), CType(CType(255, Byte), Int32))
            Me.GridView1.Appearance.OddRow.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.OddRow.Options.UseBackColor = True
            Me.GridView1.Appearance.OddRow.Options.UseBorderColor = True
            Me.GridView1.Appearance.OddRow.Options.UseForeColor = True
            Me.GridView1.Appearance.Preview.Font = New System.Drawing.Font("Verdana", 7.5!)
            Me.GridView1.Appearance.Preview.ForeColor = System.Drawing.Color.FromArgb(CType(CType(83, Byte), Int32), CType(CType(155, Byte), Int32), CType(CType(215, Byte), Int32))
            Me.GridView1.Appearance.Preview.Options.UseFont = True
            Me.GridView1.Appearance.Preview.Options.UseForeColor = True
            Me.GridView1.Appearance.Row.BackColor = System.Drawing.Color.FromArgb(CType(CType(247, Byte), Int32), CType(CType(251, Byte), Int32), CType(CType(255, Byte), Int32))
            Me.GridView1.Appearance.Row.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.Row.Options.UseBackColor = True
            Me.GridView1.Appearance.Row.Options.UseForeColor = True
            Me.GridView1.Appearance.RowSeparator.BackColor = System.Drawing.Color.FromArgb(CType(CType(236, Byte), Int32), CType(CType(246, Byte), Int32), CType(CType(255, Byte), Int32))
            Me.GridView1.Appearance.RowSeparator.BackColor2 = System.Drawing.Color.White
            Me.GridView1.Appearance.RowSeparator.Options.UseBackColor = True
            Me.GridView1.Appearance.SelectedRow.BackColor = System.Drawing.Color.FromArgb(CType(CType(83, Byte), Int32), CType(CType(155, Byte), Int32), CType(CType(215, Byte), Int32))
            Me.GridView1.Appearance.SelectedRow.ForeColor = System.Drawing.Color.White
            Me.GridView1.Appearance.SelectedRow.Options.UseBackColor = True
            Me.GridView1.Appearance.SelectedRow.Options.UseForeColor = True
            Me.GridView1.Appearance.TopNewRow.BackColor = System.Drawing.Color.White
            Me.GridView1.Appearance.TopNewRow.Options.UseBackColor = True
            Me.GridView1.Appearance.VertLine.BackColor = System.Drawing.Color.FromArgb(CType(CType(104, Byte), Int32), CType(CType(184, Byte), Int32), CType(CType(251, Byte), Int32))
            Me.GridView1.Appearance.VertLine.Options.UseBackColor = True
            Me.GridView1.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.GridColumn_office, Me.GridColumn_office_name, Me.GridColumn_SU, Me.GridColumn_M, Me.GridColumn_TU, Me.GridColumn_W, Me.GridColumn_TH, Me.GridColumn_F, Me.GridColumn_SA})
            Me.GridView1.GridControl = Me.GridControl1
            Me.GridView1.Name = "GridView1"
            Me.GridView1.OptionsView.ShowGroupPanel = False
            Me.GridView1.OptionsView.ShowIndicator = False
            Me.GridView1.ShowButtonMode = DevExpress.XtraGrid.Views.Base.ShowButtonModeEnum.ShowAlways
            '
            'GridColumn_office
            '
            Me.GridColumn_office.Caption = "Office ID"
            Me.GridColumn_office.CustomizationCaption = "Office ID"
            Me.GridColumn_office.FieldName = "office"
            Me.GridColumn_office.Name = "GridColumn_office"
            Me.GridColumn_office.OptionsColumn.AllowEdit = False
            '
            'GridColumn_office_name
            '
            Me.GridColumn_office_name.Caption = "Office"
            Me.GridColumn_office_name.CustomizationCaption = "Office Name"
            Me.GridColumn_office_name.FieldName = "office_name"
            Me.GridColumn_office_name.Name = "GridColumn_office_name"
            Me.GridColumn_office_name.OptionsColumn.AllowEdit = False
            Me.GridColumn_office_name.Visible = True
            Me.GridColumn_office_name.VisibleIndex = 0
            Me.GridColumn_office_name.Width = 134
            '
            'GridColumn_SU
            '
            Me.GridColumn_SU.AppearanceCell.Options.UseTextOptions = True
            Me.GridColumn_SU.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
            Me.GridColumn_SU.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
            Me.GridColumn_SU.AppearanceHeader.ForeColor = System.Drawing.Color.Red
            Me.GridColumn_SU.AppearanceHeader.Options.UseFont = True
            Me.GridColumn_SU.AppearanceHeader.Options.UseForeColor = True
            Me.GridColumn_SU.AppearanceHeader.Options.UseTextOptions = True
            Me.GridColumn_SU.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
            Me.GridColumn_SU.Caption = "S"
            Me.GridColumn_SU.ColumnEdit = Me.DayOfWeekRepositoryCheckedit
            Me.GridColumn_SU.CustomizationCaption = "Sunday"
            Me.GridColumn_SU.FieldName = "sun"
            Me.GridColumn_SU.MinWidth = 32
            Me.GridColumn_SU.Name = "GridColumn_SU"
            Me.GridColumn_SU.OptionsColumn.AllowGroup = DevExpress.Utils.DefaultBoolean.[False]
            Me.GridColumn_SU.OptionsColumn.AllowIncrementalSearch = False
            Me.GridColumn_SU.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
            Me.GridColumn_SU.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.[False]
            Me.GridColumn_SU.OptionsColumn.FixedWidth = True
            Me.GridColumn_SU.OptionsColumn.ShowInCustomizationForm = False
            Me.GridColumn_SU.ShowButtonMode = DevExpress.XtraGrid.Views.Base.ShowButtonModeEnum.ShowAlways
            Me.GridColumn_SU.Visible = True
            Me.GridColumn_SU.VisibleIndex = 1
            Me.GridColumn_SU.Width = 32
            '
            'DayOfWeekRepositoryCheckedit
            '
            Me.DayOfWeekRepositoryCheckedit.Caption = ""
            Me.DayOfWeekRepositoryCheckedit.Name = "DayOfWeekRepositoryCheckedit"
            Me.DayOfWeekRepositoryCheckedit.NullStyle = DevExpress.XtraEditors.Controls.StyleIndeterminate.Inactive
            Me.DayOfWeekRepositoryCheckedit.ValueChecked = 1
            Me.DayOfWeekRepositoryCheckedit.ValueUnchecked = 0
            '
            'GridColumn_M
            '
            Me.GridColumn_M.AppearanceCell.Options.UseTextOptions = True
            Me.GridColumn_M.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
            Me.GridColumn_M.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
            Me.GridColumn_M.AppearanceHeader.Options.UseFont = True
            Me.GridColumn_M.AppearanceHeader.Options.UseTextOptions = True
            Me.GridColumn_M.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
            Me.GridColumn_M.Caption = "M"
            Me.GridColumn_M.ColumnEdit = Me.DayOfWeekRepositoryCheckedit
            Me.GridColumn_M.CustomizationCaption = "Monday"
            Me.GridColumn_M.FieldName = "mon"
            Me.GridColumn_M.MinWidth = 32
            Me.GridColumn_M.Name = "GridColumn_M"
            Me.GridColumn_M.OptionsColumn.AllowGroup = DevExpress.Utils.DefaultBoolean.[False]
            Me.GridColumn_M.OptionsColumn.AllowIncrementalSearch = False
            Me.GridColumn_M.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
            Me.GridColumn_M.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.[False]
            Me.GridColumn_M.OptionsColumn.FixedWidth = True
            Me.GridColumn_M.OptionsColumn.ShowInCustomizationForm = False
            Me.GridColumn_M.ShowButtonMode = DevExpress.XtraGrid.Views.Base.ShowButtonModeEnum.ShowAlways
            Me.GridColumn_M.Visible = True
            Me.GridColumn_M.VisibleIndex = 2
            Me.GridColumn_M.Width = 32
            '
            'GridColumn_TU
            '
            Me.GridColumn_TU.AppearanceCell.Options.UseTextOptions = True
            Me.GridColumn_TU.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
            Me.GridColumn_TU.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
            Me.GridColumn_TU.AppearanceHeader.Options.UseFont = True
            Me.GridColumn_TU.AppearanceHeader.Options.UseTextOptions = True
            Me.GridColumn_TU.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
            Me.GridColumn_TU.Caption = "T"
            Me.GridColumn_TU.ColumnEdit = Me.DayOfWeekRepositoryCheckedit
            Me.GridColumn_TU.FieldName = "tue"
            Me.GridColumn_TU.MinWidth = 32
            Me.GridColumn_TU.Name = "GridColumn_TU"
            Me.GridColumn_TU.OptionsColumn.AllowGroup = DevExpress.Utils.DefaultBoolean.[False]
            Me.GridColumn_TU.OptionsColumn.AllowIncrementalSearch = False
            Me.GridColumn_TU.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
            Me.GridColumn_TU.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.[False]
            Me.GridColumn_TU.OptionsColumn.FixedWidth = True
            Me.GridColumn_TU.OptionsColumn.ShowInCustomizationForm = False
            Me.GridColumn_TU.ShowButtonMode = DevExpress.XtraGrid.Views.Base.ShowButtonModeEnum.ShowAlways
            Me.GridColumn_TU.Visible = True
            Me.GridColumn_TU.VisibleIndex = 3
            Me.GridColumn_TU.Width = 32
            '
            'GridColumn_W
            '
            Me.GridColumn_W.AppearanceCell.Options.UseTextOptions = True
            Me.GridColumn_W.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
            Me.GridColumn_W.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
            Me.GridColumn_W.AppearanceHeader.Options.UseFont = True
            Me.GridColumn_W.AppearanceHeader.Options.UseTextOptions = True
            Me.GridColumn_W.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
            Me.GridColumn_W.Caption = "W"
            Me.GridColumn_W.ColumnEdit = Me.DayOfWeekRepositoryCheckedit
            Me.GridColumn_W.FieldName = "wed"
            Me.GridColumn_W.MinWidth = 32
            Me.GridColumn_W.Name = "GridColumn_W"
            Me.GridColumn_W.OptionsColumn.AllowGroup = DevExpress.Utils.DefaultBoolean.[False]
            Me.GridColumn_W.OptionsColumn.AllowIncrementalSearch = False
            Me.GridColumn_W.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
            Me.GridColumn_W.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.[False]
            Me.GridColumn_W.OptionsColumn.FixedWidth = True
            Me.GridColumn_W.OptionsColumn.ShowInCustomizationForm = False
            Me.GridColumn_W.ShowButtonMode = DevExpress.XtraGrid.Views.Base.ShowButtonModeEnum.ShowAlways
            Me.GridColumn_W.Visible = True
            Me.GridColumn_W.VisibleIndex = 4
            Me.GridColumn_W.Width = 32
            '
            'GridColumn_TH
            '
            Me.GridColumn_TH.AppearanceCell.Options.UseTextOptions = True
            Me.GridColumn_TH.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
            Me.GridColumn_TH.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
            Me.GridColumn_TH.AppearanceHeader.Options.UseFont = True
            Me.GridColumn_TH.AppearanceHeader.Options.UseTextOptions = True
            Me.GridColumn_TH.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
            Me.GridColumn_TH.Caption = "T"
            Me.GridColumn_TH.ColumnEdit = Me.DayOfWeekRepositoryCheckedit
            Me.GridColumn_TH.FieldName = "thu"
            Me.GridColumn_TH.MinWidth = 32
            Me.GridColumn_TH.Name = "GridColumn_TH"
            Me.GridColumn_TH.OptionsColumn.AllowGroup = DevExpress.Utils.DefaultBoolean.[False]
            Me.GridColumn_TH.OptionsColumn.AllowIncrementalSearch = False
            Me.GridColumn_TH.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
            Me.GridColumn_TH.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.[False]
            Me.GridColumn_TH.OptionsColumn.FixedWidth = True
            Me.GridColumn_TH.OptionsColumn.ShowInCustomizationForm = False
            Me.GridColumn_TH.ShowButtonMode = DevExpress.XtraGrid.Views.Base.ShowButtonModeEnum.ShowAlways
            Me.GridColumn_TH.Visible = True
            Me.GridColumn_TH.VisibleIndex = 5
            Me.GridColumn_TH.Width = 32
            '
            'GridColumn_F
            '
            Me.GridColumn_F.AppearanceCell.Options.UseTextOptions = True
            Me.GridColumn_F.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
            Me.GridColumn_F.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
            Me.GridColumn_F.AppearanceHeader.Options.UseFont = True
            Me.GridColumn_F.AppearanceHeader.Options.UseTextOptions = True
            Me.GridColumn_F.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
            Me.GridColumn_F.Caption = "F"
            Me.GridColumn_F.ColumnEdit = Me.DayOfWeekRepositoryCheckedit
            Me.GridColumn_F.FieldName = "fri"
            Me.GridColumn_F.MinWidth = 32
            Me.GridColumn_F.Name = "GridColumn_F"
            Me.GridColumn_F.OptionsColumn.AllowGroup = DevExpress.Utils.DefaultBoolean.[False]
            Me.GridColumn_F.OptionsColumn.AllowIncrementalSearch = False
            Me.GridColumn_F.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
            Me.GridColumn_F.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.[False]
            Me.GridColumn_F.OptionsColumn.FixedWidth = True
            Me.GridColumn_F.OptionsColumn.ShowInCustomizationForm = False
            Me.GridColumn_F.ShowButtonMode = DevExpress.XtraGrid.Views.Base.ShowButtonModeEnum.ShowAlways
            Me.GridColumn_F.Visible = True
            Me.GridColumn_F.VisibleIndex = 6
            Me.GridColumn_F.Width = 32
            '
            'GridColumn_SA
            '
            Me.GridColumn_SA.AppearanceCell.Options.UseTextOptions = True
            Me.GridColumn_SA.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
            Me.GridColumn_SA.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
            Me.GridColumn_SA.AppearanceHeader.ForeColor = System.Drawing.Color.Blue
            Me.GridColumn_SA.AppearanceHeader.Options.UseFont = True
            Me.GridColumn_SA.AppearanceHeader.Options.UseForeColor = True
            Me.GridColumn_SA.AppearanceHeader.Options.UseTextOptions = True
            Me.GridColumn_SA.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
            Me.GridColumn_SA.Caption = "S"
            Me.GridColumn_SA.ColumnEdit = Me.DayOfWeekRepositoryCheckedit
            Me.GridColumn_SA.FieldName = "sat"
            Me.GridColumn_SA.MinWidth = 32
            Me.GridColumn_SA.Name = "GridColumn_SA"
            Me.GridColumn_SA.OptionsColumn.AllowGroup = DevExpress.Utils.DefaultBoolean.[False]
            Me.GridColumn_SA.OptionsColumn.AllowIncrementalSearch = False
            Me.GridColumn_SA.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
            Me.GridColumn_SA.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.[False]
            Me.GridColumn_SA.OptionsColumn.FixedWidth = True
            Me.GridColumn_SA.OptionsColumn.ShowInCustomizationForm = False
            Me.GridColumn_SA.ShowButtonMode = DevExpress.XtraGrid.Views.Base.ShowButtonModeEnum.ShowAlways
            Me.GridColumn_SA.Visible = True
            Me.GridColumn_SA.VisibleIndex = 7
            Me.GridColumn_SA.Width = 32
            '
            'Button_SelectAll
            '
            Me.Button_SelectAll.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
            Me.Button_SelectAll.Location = New System.Drawing.Point(16, 188)
            Me.Button_SelectAll.Name = "Button_SelectAll"
            Me.Button_SelectAll.Size = New System.Drawing.Size(75, 23)
            Me.Button_SelectAll.TabIndex = 2
            Me.Button_SelectAll.Tag = "1"
            Me.Button_SelectAll.Text = "Select All"
            Me.Button_SelectAll.ToolTipController = Me.ToolTipController1
            '
            'Button_OK
            '
            Me.Button_OK.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.Button_OK.Location = New System.Drawing.Point(400, 188)
            Me.Button_OK.Name = "Button_OK"
            Me.Button_OK.Size = New System.Drawing.Size(75, 23)
            Me.Button_OK.TabIndex = 3
            Me.Button_OK.Text = "Create"
            Me.Button_OK.ToolTipController = Me.ToolTipController1
            '
            'Button_Cancel
            '
            Me.Button_Cancel.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.Button_Cancel.Location = New System.Drawing.Point(488, 188)
            Me.Button_Cancel.Name = "Button_Cancel"
            Me.Button_Cancel.Size = New System.Drawing.Size(75, 23)
            Me.Button_Cancel.TabIndex = 4
            Me.Button_Cancel.Text = "&Quit"
            Me.Button_Cancel.ToolTipController = Me.ToolTipController1
            '
            'DateNavigator1
            '
            Me.DateNavigator1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                                              Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.DateNavigator1.Location = New System.Drawing.Point(384, 8)
            Me.DateNavigator1.Name = "DateNavigator1"
            Me.DateNavigator1.ShowWeekNumbers = False
            Me.DateNavigator1.Size = New System.Drawing.Size(179, 165)
            Me.DateNavigator1.TabIndex = 5
            '
            'Form_Create
            '
            Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
            Me.ClientSize = New System.Drawing.Size(568, 226)
            Me.Controls.Add(Me.DateNavigator1)
            Me.Controls.Add(Me.Button_Cancel)
            Me.Controls.Add(Me.Button_OK)
            Me.Controls.Add(Me.Button_SelectAll)
            Me.Controls.Add(Me.GridControl1)
            Me.Name = "Form_Create"
            Me.Text = "Create Schedule Items"
            CType(Me.DxErrorProvider1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.GridView1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.DayOfWeekRepositoryCheckedit, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.DateNavigator1, System.ComponentModel.ISupportInitialize).EndInit()
            Me.ResumeLayout(False)
        End Sub

        Private Button_Cancel As DevExpress.XtraEditors.SimpleButton
        Private Button_OK As DevExpress.XtraEditors.SimpleButton
        Private Button_SelectAll As DevExpress.XtraEditors.SimpleButton
        Private DateNavigator1 As DevExpress.XtraScheduler.DateNavigator
        Private DayOfWeekRepositoryCheckedit As DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit
        Private GridColumn_F As DevExpress.XtraGrid.Columns.GridColumn
        Private GridColumn_M As DevExpress.XtraGrid.Columns.GridColumn
        Private GridColumn_office As DevExpress.XtraGrid.Columns.GridColumn
        Private GridColumn_office_name As DevExpress.XtraGrid.Columns.GridColumn
        Private GridColumn_SA As DevExpress.XtraGrid.Columns.GridColumn
        Private GridColumn_SU As DevExpress.XtraGrid.Columns.GridColumn
        Private GridColumn_TH As DevExpress.XtraGrid.Columns.GridColumn
        Private GridColumn_TU As DevExpress.XtraGrid.Columns.GridColumn
        Private GridColumn_W As DevExpress.XtraGrid.Columns.GridColumn
        Private GridControl1 As DevExpress.XtraGrid.GridControl
        Private GridView1 As DevExpress.XtraGrid.Views.Grid.GridView
    End Class
End Namespace