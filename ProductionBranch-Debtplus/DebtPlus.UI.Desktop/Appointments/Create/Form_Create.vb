#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}
#End Region

Option Compare Binary
Option Explicit On 
Option Strict On

Imports DebtPlus.Utils
Imports System.Data.SqlClient
Imports System.Threading
Imports System.Threading.Tasks
Imports System.Windows.Forms

Namespace Appointments.Create

    Friend Class Form_Create

        ' Array of tasks to process
        Dim lstProcessing As System.Collections.Generic.List(Of Task)

        Private WithEvents tbl As System.Data.DataTable
        Private WithEvents bt As System.ComponentModel.BackgroundWorker
        Private ds As System.Data.DataSet
        Private busyDialog As DevExpress.Utils.WaitDialogForm
        Private ProcessedCount As Int32
        Private TotalCount As Int32

        Public Sub New()
            MyBase.New()
            InitializeComponent()

            lstProcessing = New System.Collections.Generic.List(Of Task)()
            tbl = Nothing
            bt = New System.ComponentModel.BackgroundWorker()
            ds = New System.Data.DataSet("ds")
            busyDialog = Nothing
            ProcessedCount = 0
            TotalCount = 0

            RegisterHandlers()
        End Sub

        Private Sub RegisterHandlers()
            AddHandler Load, AddressOf Form_Create_Load
            AddHandler bt.DoWork, AddressOf bt_DoWork
            AddHandler bt.RunWorkerCompleted, AddressOf bt_RunWorkerCompleted
            AddHandler DateNavigator1.EditDateModified, AddressOf DateNavigator1_EditDateModified
            AddHandler DayOfWeekRepositoryCheckedit.EditValueChanging, AddressOf Check_Changing
            AddHandler Button_Cancel.Click, AddressOf Button_Cancel_Click
            AddHandler Button_SelectAll.Click, AddressOf Button_SelectAll_Click
            AddHandler Button_OK.Click, AddressOf Button_OK_Click
        End Sub

        Private Sub UnRegisterHandlers()
            RemoveHandler Load, AddressOf Form_Create_Load
            RemoveHandler bt.DoWork, AddressOf bt_DoWork
            RemoveHandler bt.RunWorkerCompleted, AddressOf bt_RunWorkerCompleted
            RemoveHandler DateNavigator1.EditDateModified, AddressOf DateNavigator1_EditDateModified
            RemoveHandler DayOfWeekRepositoryCheckedit.EditValueChanging, AddressOf Check_Changing
            RemoveHandler Button_Cancel.Click, AddressOf Button_Cancel_Click
            RemoveHandler Button_SelectAll.Click, AddressOf Button_SelectAll_Click
            RemoveHandler Button_OK.Click, AddressOf Button_OK_Click
        End Sub

        ''' <summary>
        ''' Load the form
        ''' </summary>
        Private Sub Form_Create_Load(ByVal sender As Object, ByVal e As System.EventArgs)

            ' Load the list of offices
            Dim current_cursor As Cursor = System.Windows.Forms.Cursor.Current
            Dim cn As SqlConnection = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)

            UnRegisterHandlers()
            Try
                System.Windows.Forms.Cursor.Current = Cursors.WaitCursor
                cn.Open()

                Using cmd As SqlClient.SqlCommand = New SqlCommand
                    cmd.Connection = cn
                    cmd.CommandText = "SELECT DISTINCT o.office, o.name AS office_name, convert(int,0) AS sun, convert(int,0) AS mon, convert(int,0) AS tue, convert(int,0) AS wed, convert(int,0) AS thu, convert(int,0) AS fri, convert(int,0) AS sat FROM offices o WITH (NOLOCK) INNER JOIN appt_time_templates t WITH (NOLOCK) ON o.office = t.office WHERE o.ActiveFlag <> 0"
                    cmd.CommandType = CommandType.Text
                    cmd.CommandTimeout = DebtPlus.LINQ.SQLInfoClass.getDefault().CommandTimeout

                    Using da As New SqlClient.SqlDataAdapter(cmd)
                        da.Fill(ds, "checked_items")
                    End Using
                End Using

                tbl = ds.Tables("checked_items")

                ' Sort the list by the name
                GridView1.SortInfo.Clear()
                GridView1.SortInfo.Add(GridColumn_office_name, DevExpress.Data.ColumnSortOrder.Ascending)

                ' Bind the data to the table
                GridControl1.DataSource = tbl.DefaultView
                GridControl1.RefreshDataSource()

                ' Position the calendar to today
                DateNavigator1.DateTime = DateTime.Now.Date

            Catch ex As SqlClient.SqlException
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error reading appt_time_templates table")

            Finally
                If cn IsNot Nothing Then cn.Dispose()
                System.Windows.Forms.Cursor.Current = current_cursor
                RegisterHandlers()
            End Try

            EnableOK()
        End Sub

        ''' <summary>
        ''' Cancel button :: close the form
        ''' </summary>
        Private Sub Button_Cancel_Click(ByVal sender As Object, ByVal e As System.EventArgs)
            Close()
        End Sub

        ''' <summary>
        ''' Process the SelectAll or ClearAll button (same button)
        ''' </summary>
        Private Sub Button_SelectAll_Click(ByVal sender As Object, ByVal e As System.EventArgs)
            UnRegisterHandlers()
            Try
                ClearSelection()

                ' Invert the button text
                If Convert.ToInt32(Button_SelectAll.Tag) = 1 Then
                    Button_SelectAll.Tag = 0
                    Button_SelectAll.Text = "Clear All"

                    ' Open a database connection
                    Dim cn As SqlClient.SqlConnection = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
                    Dim current_cursor As Cursor = System.Windows.Forms.Cursor.Current
                    Try
                        System.Windows.Forms.Cursor.Current = Cursors.WaitCursor
                        cn.Open()

                        ' Process each row in the table to find the office and the days of the week.
                        For Each drv As System.Data.DataRowView In tbl.DefaultView
                            UpdateRow(cn, drv)
                        Next

                    Catch ex As SqlClient.SqlException
                        DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error reading appt_time_template items")

                    Finally
                        If cn IsNot Nothing Then cn.Dispose()
                        System.Windows.Forms.Cursor.Current = current_cursor
                    End Try

                    ' Accept the changes and set the OK button.
                    tbl.AcceptChanges()
                    EnableOK()
                End If

            Finally
                RegisterHandlers()
            End Try
        End Sub

        ''' <summary>
        ''' Clear the checkboxes on the form
        ''' </summary>
        Private Sub ClearSelection()

            Button_SelectAll.Tag = 1
            Button_SelectAll.Text = "Select All"

            ' Reset the row checkboxes
            For Each row As System.Data.DataRow In tbl.Rows
                row.BeginEdit()
                row.Item("sun") = 0
                row.Item("mon") = 0
                row.Item("tue") = 0
                row.Item("wed") = 0
                row.Item("thu") = 0
                row.Item("fri") = 0
                row.Item("sat") = 0
                row.EndEdit()
            Next

            ' Accept the changes and disable the OK button.
            tbl.AcceptChanges()
            Button_OK.Enabled = False
        End Sub

        ''' <summary>
        ''' Check the days for the current office
        ''' </summary>
        Private Sub UpdateRow(ByRef cn As SqlClient.SqlConnection, ByRef drv As System.Data.DataRowView)
            drv.BeginEdit()

            Using cmd As SqlClient.SqlCommand = New SqlCommand
                cmd.Connection = cn
                cmd.CommandText = "SELECT distinct dow FROM appt_time_templates WHERE office=@office"
                cmd.CommandType = CommandType.Text
                cmd.CommandTimeout = DebtPlus.LINQ.SQLInfoClass.getDefault().CommandTimeout
                cmd.Parameters.Add("@office", SqlDbType.Int).Value = drv("office")

                Using rd = cmd.ExecuteReader(CommandBehavior.SingleResult)

                    ' Select the appropriate checkbox items
                    Do While rd.Read
                        Select Case Convert.ToInt32(rd.GetValue(0))
                            Case 1 : drv("sun") = 1
                            Case 2 : drv("mon") = 1
                            Case 3 : drv("tue") = 1
                            Case 4 : drv("wed") = 1
                            Case 5 : drv("thu") = 1
                            Case 6 : drv("fri") = 1
                            Case 7 : drv("sat") = 1
                        End Select
                    Loop
                End Using
            End Using

            drv.EndEdit()
        End Sub

        ''' <summary>
        ''' Enable or Disable the OK button based upon items checked in the form
        ''' </summary>
        Private Sub EnableOK()
            If tbl IsNot Nothing Then
                Dim objCount As Object = tbl.Compute("count(office)", "sun<>0 OR mon<>0 OR tue<>0 OR wed<>0 OR thu<>0 OR fri<>0 OR sat<>0")
                Button_OK.Enabled = objCount IsNot Nothing AndAlso objCount IsNot System.DBNull.Value AndAlso Convert.ToInt32(objCount) > 0
            End If
        End Sub

        ''' <summary>
        ''' Process the CLICK event on the START button. Start the background thread process going.
        ''' </summary>
        ''' <param name="sender"></param>
        ''' <param name="e"></param>
        ''' <remarks></remarks>
        Private Sub Button_OK_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)

            ' Prevent the form from tripping the button again.
            Button_OK.Enabled = False

            ' Create a busy dialog
            busyDialog = New DevExpress.Utils.WaitDialogForm("Please wait", "Creating Appointments")
            busyDialog.Show()

            Dim CurrentDate As DateTime = DateNavigator1.SelectionStart.Date

            ' Do not allow the date to be in the past.
            If CurrentDate < DateTime.Now.Date Then CurrentDate = DateTime.Now.Date
            Dim EndingDate As DateTime = DateNavigator1.SelectionEnd.Date

            ' Create a thread to handle each day of the range.
            ' The work is all in parallel to speed things up.
            Do While CurrentDate <= EndingDate

                ' Create the task to process the current date
                Dim cls As New ProcessingClass(tbl)
                cls.CurrentDate = CurrentDate
                AddHandler cls.CountChanging, AddressOf cls_CountChanging

                ' the factory to create the task takes an "action" delegate. Ensure that we use the proper sequence for it.
                Dim tsk As Task = Task.Factory.StartNew(New System.Action(AddressOf cls.RunProcedure), TaskCreationOptions.AttachedToParent)
                lstProcessing.Add(tsk)

                ' Process all of the dates in the selected region. Each date has its own thread.
                CurrentDate = CurrentDate.AddDays(1)
            Loop

            ' Make the selection just the first date beyond the range selected
            DateNavigator1.Selection.Clear()
            DateNavigator1.DateTime = CurrentDate

            ' Start a thread that will block until the processing threads have terminated
            ' This will then trip the Completed processing for the background thread to finish the process.
            bt.RunWorkerAsync()
        End Sub

        ''' <summary>
        ''' Background thread to process the function. Allocate sub-threads to do each day and then wait
        ''' for all of them to complete.
        ''' </summary>
        ''' <param name="sender"></param>
        ''' <param name="e"></param>
        ''' <remarks></remarks>
        Private Sub bt_DoWork(sender As Object, e As System.ComponentModel.DoWorkEventArgs)

            ' Wait for all of the processing threads to terminate. They will all wait here and this
            ' thread will be blocked until we complete. That is why it is running as a background thread
            ' because the "foreground" thread needs to be free to run the waiting dialog. When we
            ' complete, the bt_RunWorkerCompleted event will trip and we will deal with the foregorund again.
            If lstProcessing.Count > 0 Then
                Task.WaitAll(lstProcessing.ToArray())
            End If
        End Sub

        ''' <summary>
        ''' The main processing class routines have terminated. Now display the event dialog and enable the exit button
        ''' </summary>
        ''' <param name="sender"></param>
        ''' <param name="e"></param>
        ''' <remarks></remarks>
        Private Sub bt_RunWorkerCompleted(sender As Object, e As System.ComponentModel.RunWorkerCompletedEventArgs)

            ' Marshal to the proper thread. It is ok since the thread is just waiting for the events to happen.
            If busyDialog.InvokeRequired Then
                busyDialog.EndInvoke(busyDialog.BeginInvoke(New System.ComponentModel.RunWorkerCompletedEventHandler(AddressOf bt_RunWorkerCompleted), New Object() {sender, e}))

            Else
                ' Hide the busy dialog once it has served its purpose
                If busyDialog IsNot Nothing Then
                    If busyDialog.Visible Then busyDialog.Close()
                    If Not busyDialog.IsDisposed Then busyDialog.Dispose()
                    busyDialog = Nothing
                End If

                ' Tell the user that the operation is complete
                DebtPlus.Data.Forms.MessageBox.Show(String.Format("{0:n0} Appointment(s) have been created. You may close the window.", TotalCount), "Operation Completed")
                EnableOK()
            End If
        End Sub

        ''' <summary>
        ''' Process the change in the count of appointments that have been created. Each thread maintains
        ''' its own count but we need the grand total count so it is done here. We pick up the change in
        ''' count on each thread and update the count here.
        ''' </summary>
        ''' <param name="sender"></param>
        ''' <param name="e"></param>
        ''' <remarks></remarks>
        Private Sub cls_CountChanging(ByVal sender As Object, ByVal e As CountChangingEventArgs)
            If busyDialog IsNot Nothing Then
                If busyDialog.InvokeRequired Then
                    busyDialog.EndInvoke(busyDialog.BeginInvoke(New CountChangingEventHandler(AddressOf cls_CountChanging), New Object() {sender, e}))
                Else
                    TotalCount += e.NewValue - e.OldValue ' The true count is what was incremented. To get that, subtract the old from the new value.
                    busyDialog.SetCaption(String.Format(String.Format("{0:n0} Appointment(s) created", TotalCount)))
                End If
            End If
        End Sub

        ''' <summary>
        ''' Process the change in a checkbox
        ''' </summary>
        ''' <param name="sender"></param>
        ''' <param name="e"></param>
        ''' <remarks></remarks>
        Private Sub Check_Changing(ByVal sender As Object, ByVal e As DevExpress.XtraEditors.Controls.ChangingEventArgs)

            ' If setting the checkbox then just enable the OK button.
            If Convert.ToInt32(e.NewValue) <> 0 Then
                Button_OK.Enabled = True
            Else

                ' If resetting the checkbox then count the number of checked items. We need at least two currently checked to enable the OK button.
                Dim CheckedCount As System.Int32 = 0
                For Each row As System.Data.DataRow In tbl.Rows
                    If Convert.ToInt32(row("sun")) <> 0 Then CheckedCount += 1
                    If Convert.ToInt32(row("mon")) <> 0 Then CheckedCount += 1
                    If Convert.ToInt32(row("tue")) <> 0 Then CheckedCount += 1
                    If Convert.ToInt32(row("wed")) <> 0 Then CheckedCount += 1
                    If Convert.ToInt32(row("thu")) <> 0 Then CheckedCount += 1
                    If Convert.ToInt32(row("fri")) <> 0 Then CheckedCount += 1
                    If Convert.ToInt32(row("sat")) <> 0 Then CheckedCount += 1
                Next

                ' If the count was only one then we are resetting our one checked item. Disable the OK button.
                Button_OK.Enabled = CheckedCount > 1
            End If
        End Sub

        ''' <summary>
        ''' Process a change in the date selection navigator. Do not allow the value to be too early.
        ''' </summary>
        ''' <param name="sender"></param>
        ''' <param name="e"></param>
        ''' <remarks></remarks>
        Private Sub DateNavigator1_EditDateModified(ByVal sender As Object, ByVal e As System.EventArgs)
            If DateNavigator1.DateTime < DateTime.Now.Date Then
                DateNavigator1.DateTime = DateTime.Now.Date
            End If
        End Sub
    End Class

End Namespace
