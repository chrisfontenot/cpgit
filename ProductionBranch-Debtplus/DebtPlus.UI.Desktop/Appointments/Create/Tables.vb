#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}
#End Region

Option Compare Binary
Option Explicit On 
Option Strict On

Imports DebtPlus.Utils
Imports System.Data.SqlClient
Imports System.Windows.Forms

Namespace Appointments.Create

    Friend Module Tables

        ' Dataset for the table storage
        Private MyDs As New System.Data.DataSet

#Region " Day Of The Week "

        ''' <summary>
        ''' Days of the week for the template table
        ''' </summary>
        Friend Function DayOfWeekTable() As System.Data.DataTable
            Dim tbl As System.Data.DataTable = MyDs.Tables("days_of_week")
            If tbl Is Nothing Then
                tbl = New System.Data.DataTable("days_of_week")
                With tbl

                    ' Define the table as a list of numbers attached to the local names of the days of the week.
                    ' Numbers range from 0(sunday) through 6(saturday)
                    .Columns.Add("day_of_week", GetType(System.Int32))
                    .Columns.Add("description", GetType(String))
                    .PrimaryKey = New DataColumn() {.Columns(0)}

                    ' Add the values to the table for the corresponding days of the week
                    .BeginLoadData()
                    For Each dow As System.DayOfWeek In New Object() {System.DayOfWeek.Sunday, System.DayOfWeek.Monday, System.DayOfWeek.Tuesday, System.DayOfWeek.Wednesday, System.DayOfWeek.Thursday, System.DayOfWeek.Friday, System.DayOfWeek.Saturday}
                        .Rows.Add(New Object() {Convert.ToInt32(dow) + 1, System.Globalization.CultureInfo.CurrentCulture.DateTimeFormat().GetDayName(dow)})
                    Next
                    .EndLoadData()

                    .AcceptChanges()
                    MyDs.Tables.Add(tbl)
                End With
            End If

            Return tbl
        End Function
#End Region

#Region " Office Types "

        ''' <summary>
        ''' Office Type table
        ''' </summary>
        <Obsolete("Use LINQ tables")> _
        Friend Function OfficeTypesTable() As System.Data.DataTable
            Dim MyTableName As String = "office_types"
            Dim tbl As System.Data.DataTable = MyDs.Tables(MyTableName)

            ' If the table is not found then read the information from the database
            If tbl Is Nothing Then
                Dim current_cursor As Cursor = System.Windows.Forms.Cursor.Current
                Try
                    System.Windows.Forms.Cursor.Current = Cursors.WaitCursor
                    Using cmd As SqlClient.SqlCommand = New SqlCommand
                        With cmd
                            .Connection = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
                            .CommandText = "SELECT item_value as office_type, description FROM messages WITH (NOLOCK) WHERE item_type='OFFICE TYPE'"
                        End With

                        ' Ask the database for the information
                        Using da As New SqlClient.SqlDataAdapter(cmd)
                            da.Fill(MyDs, MyTableName)
                        End Using
                    End Using

                    tbl = MyDs.Tables(MyTableName)
                    tbl.PrimaryKey = New System.Data.DataColumn() {tbl.Columns(0)}

                    ' When we do an update, the update results in zero rows being updated. IGNORE IT.
                Catch ex As System.Data.DBConcurrencyException
                    DebtPlus.Svc.Common.ErrorHandling.HandleErrors(ex)

                    ' Something happend in trying to talk to the database.
                Catch ex As SqlClient.SqlException
                    Using gdr As New Repository.GetDataResult()
                        gdr.HandleException(ex)
                        DebtPlus.UI.Common.ErrorHandling.HandleErrors(gdr, "Error reading " + MyTableName + " table")
                    End Using

                Finally
                    System.Windows.Forms.Cursor.Current = current_cursor
                End Try
            End If

            Return tbl
        End Function
#End Region

#Region " Offices "

        ''' <summary>
        ''' Offices information table
        ''' </summary>
        <Obsolete("Use LINQ tables")> _
        Friend Function OfficesTable() As System.Data.DataTable
            Dim MyTableName As String = "offices"
            Dim tbl As System.Data.DataTable = MyDs.Tables(MyTableName)

            ' If the table is not found then read the information from the database
            If tbl Is Nothing Then
                Dim current_cursor As Cursor = System.Windows.Forms.Cursor.Current
                Try
                    System.Windows.Forms.Cursor.Current = Cursors.WaitCursor
                    Using cmd As SqlClient.SqlCommand = New SqlCommand
                        With cmd
                            .Connection = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
                            .CommandText = "SELECT o.office, o.name, o.type, o.AddressID, o.EmailID, o.TelephoneID, o.AltTelephoneID, o.FAXID, o.Default, o.ActiveFlag, o.hud_hcs_id, o.TimeZoneID, o.nfcc, o.default_till_missed, o.counselor, o.region, o.district, o.directions, o.date_created, o.created_by, m.description as formatted_office_type FROM offices o WITH (NOLOCK) LEFT OUTER JOIN OfficeTypes m ON o.type = m.oID WHERE o.ActiveFlag <> 0"
                        End With

                        ' Ask the database for the information
                        Using da As New SqlClient.SqlDataAdapter(cmd)
                            da.Fill(MyDs, MyTableName)
                        End Using
                    End Using

                    tbl = MyDs.Tables(MyTableName)

                    ' When we do an update, the update results in zero rows being updated. IGNORE IT.
                Catch ex As System.Data.DBConcurrencyException
                    DebtPlus.Svc.Common.ErrorHandling.HandleErrors(ex)

                    ' Something happened in trying to talk to the database.
                Catch ex As SqlClient.SqlException
                    Using gdr As New Repository.GetDataResult()
                        gdr.HandleException(ex)
                        DebtPlus.UI.Common.ErrorHandling.HandleErrors(gdr, "Error reading " + MyTableName + " table")
                    End Using

                Finally
                    System.Windows.Forms.Cursor.Current = current_cursor
                End Try
            End If

            Return tbl
        End Function
#End Region

#Region " Counselors "

        ''' <summary>
        ''' Counselors information table
        ''' </summary>
        <Obsolete("Use LINQ tables")> _
        Friend Function CounselorsTable() As System.Data.DataTable
            Dim MyTableName As String = "counselors"
            Dim tbl As System.Data.DataTable = MyDs.Tables(MyTableName)

            ' If the table is not found then read the information from the database
            If tbl Is Nothing Then
                Dim current_cursor As Cursor = System.Windows.Forms.Cursor.Current
                Try
                    System.Windows.Forms.Cursor.Current = Cursors.WaitCursor
                    Using cmd As SqlClient.SqlCommand = New SqlCommand
                        With cmd
                            .CommandType = CommandType.Text
                            .Connection = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
                            .CommandText = "SELECT [Counselor],[NameID],[EmailID],[TelephoneID],[Person],[Manager],[Note],[Default],[ActiveFlag],[Office],[Menu_Level],[Color],[Image],[SSN],[billing_mode],[rate],[emp_start_date],[emp_end_date],[HUD_id],[CaseCounter],[RxOfficeCounselorID],[date_created],[created_by] FROM counselors WITH (NOLOCK) WHERE ActiveFlag <> 0"
                        End With

                        ' Ask the database for the information
                        Using da As New SqlClient.SqlDataAdapter(cmd)
                            da.Fill(MyDs, MyTableName)
                        End Using
                    End Using

                    tbl = MyDs.Tables(MyTableName)

                    ' When we do an update, the update results in zero rows being updated. IGNORE IT.
                Catch ex As System.Data.DBConcurrencyException
                    DebtPlus.Svc.Common.ErrorHandling.HandleErrors(ex)

                    ' Something happend in trying to talk to the database.
                Catch ex As SqlClient.SqlException
                    Using gdr As New Repository.GetDataResult()
                        gdr.HandleException(ex)
                        DebtPlus.UI.Common.ErrorHandling.HandleErrors(gdr, "Error reading " + MyTableName + " table")
                    End Using

                Finally
                    System.Windows.Forms.Cursor.Current = current_cursor
                End Try
            End If

            Return tbl
        End Function
#End Region

#Region " Regions "

        ''' <summary>
        ''' region information table
        ''' </summary>
        <Obsolete("Use LINQ tables")> _
        Friend Function RegionsTable() As System.Data.DataTable
            Dim MyTableName As String = "regions"
            Dim tbl As System.Data.DataTable = MyDs.Tables(MyTableName)

            ' If the table is not found then read the information from the database
            If tbl Is Nothing Then
                Dim current_cursor As Cursor = System.Windows.Forms.Cursor.Current
                Try
                    System.Windows.Forms.Cursor.Current = Cursors.WaitCursor
                    Using cmd As SqlClient.SqlCommand = New SqlCommand
                        With cmd
                            .Connection = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
                            .CommandText = "SELECT region,description,ActiveFlag,[default] FROM regions WITH (NOLOCK)"
                        End With

                        ' Ask the database for the information
                        Using da As New SqlClient.SqlDataAdapter(cmd)
                            da.Fill(MyDs, MyTableName)
                        End Using
                    End Using

                    tbl = MyDs.Tables(MyTableName)

                    ' When we do an update, the update results in zero rows being updated. IGNORE IT.
                Catch ex As System.Data.DBConcurrencyException
                    DebtPlus.Svc.Common.ErrorHandling.HandleErrors(ex)

                    ' Something happend in trying to talk to the database.
                Catch ex As SqlClient.SqlException
                    Using gdr As New Repository.GetDataResult()
                        gdr.HandleException(ex)
                        DebtPlus.UI.Common.ErrorHandling.HandleErrors(gdr, "Error reading " + MyTableName + " table")
                    End Using

                Finally
                    System.Windows.Forms.Cursor.Current = current_cursor
                End Try
            End If

            Return tbl
        End Function
#End Region

#Region " Districts "

        ''' <summary>
        ''' district information table
        ''' </summary>
        <Obsolete("Use LINQ tables")> _
        Friend Function DistrictsTable() As System.Data.DataTable
            Dim MyTableName As String = "districts"
            Dim tbl As System.Data.DataTable = MyDs.Tables(MyTableName)

            ' If the table is not found then read the information from the database
            If tbl Is Nothing Then
                Dim current_cursor As Cursor = System.Windows.Forms.Cursor.Current
                Try
                    System.Windows.Forms.Cursor.Current = Cursors.WaitCursor
                    Using cmd As SqlClient.SqlCommand = New SqlCommand
                        With cmd
                            .Connection = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
                            .CommandText = "SELECT district,description,ActiveFlag,[default] FROM districts WITH (NOLOCK)"
                        End With

                        ' Ask the database for the information
                        Using da As New SqlClient.SqlDataAdapter(cmd)
                            da.Fill(MyDs, MyTableName)
                        End Using
                    End Using
                    tbl = MyDs.Tables(MyTableName)

                    ' When we do an update, the update results in zero rows being updated. IGNORE IT.
                Catch ex As System.Data.DBConcurrencyException
                    DebtPlus.Svc.Common.ErrorHandling.HandleErrors(ex)

                    ' Something happend in trying to talk to the database.
                Catch ex As SqlClient.SqlException
                    Using gdr As New Repository.GetDataResult()
                        gdr.HandleException(ex)
                        DebtPlus.UI.Common.ErrorHandling.HandleErrors(gdr, "Error reading " + MyTableName + " table")
                    End Using

                Finally
                    System.Windows.Forms.Cursor.Current = current_cursor
                End Try
            End If

            Return tbl
        End Function
#End Region

#Region " ApptTypes "

        ''' <summary>
        ''' ApptTypes information table
        ''' </summary>
        <Obsolete("Use LINQ tables")> _
        Friend Function ApptTypesTable() As System.Data.DataTable
            Dim MyTableName As String = "appt_types"
            Dim tbl As System.Data.DataTable = MyDs.Tables(MyTableName)

            ' If the table is not found then read the information from the database
            If tbl Is Nothing Then
                Dim current_cursor As Cursor = System.Windows.Forms.Cursor.Current
                Try
                    System.Windows.Forms.Cursor.Current = Cursors.WaitCursor
                    Using cmd As SqlClient.SqlCommand = New SqlCommand
                        With cmd
                            .Connection = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
                            .CommandText = "SELECT [appt_type],[appt_name],[appt_duration],[default],[housing],[credit],[bankruptcy],[initial_appt],[contact_type],[missed_retention_event],[create_letter],[reschedule_letter],[cancel_letter],[bankruptcy_class],[date_created],[created_by] FROM appt_types WITH (NOLOCK)"
                        End With

                        ' Ask the database for the information
                        Using da As New SqlClient.SqlDataAdapter(cmd)
                            da.Fill(MyDs, MyTableName)
                        End Using
                    End Using

                    tbl = MyDs.Tables(MyTableName)
                    tbl.PrimaryKey = New System.Data.DataColumn() {tbl.Columns("appt_type")}

                    ' When we do an update, the update results in zero rows being updated. IGNORE IT.
                Catch ex As System.Data.DBConcurrencyException
                    DebtPlus.Svc.Common.ErrorHandling.HandleErrors(ex)

                    ' Something happend in trying to talk to the database.
                Catch ex As SqlClient.SqlException
                    Using gdr As New Repository.GetDataResult()
                        gdr.HandleException(ex)
                        DebtPlus.UI.Common.ErrorHandling.HandleErrors(gdr, "Error reading " + MyTableName + " table")
                    End Using

                Finally
                    System.Windows.Forms.Cursor.Current = current_cursor
                End Try
            End If

            Return tbl
        End Function
#End Region
    End Module
End Namespace
