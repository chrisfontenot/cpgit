﻿#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}
#End Region

Option Compare Binary
Option Explicit On
Option Strict On

Imports DebtPlus.Utils
Imports System.Data.SqlClient
Imports System.Threading
Imports System.Threading.Tasks

Namespace Appointments.Create

    Friend Class ProcessingClass
        Implements INotifyCountChanging

        Private tbl As System.Data.DataTable
        Private privateCount As Int32 = 0

        ''' <summary>
        ''' Event to indicate the count is being changed
        ''' </summary>
        ''' <param name="sender"></param>
        ''' <param name="e"></param>
        ''' <remarks></remarks>
        Public Event CountChanging As CountChangingEventHandler Implements INotifyCountChanging.CountChanging

        ''' <summary>
        ''' Raise the CountChanging event
        ''' </summary>
        ''' <param name="PropertyName"></param>
        ''' <param name="OldValue"></param>
        ''' <param name="NewValue"></param>
        ''' <remarks></remarks>
        Protected Sub RaiseCountChanging(ByVal PropertyName As String, ByVal OldValue As Int32, ByVal NewValue As Int32)
            RaiseEvent CountChanging(Me, New CountChangingEventArgs(PropertyName, OldValue, NewValue))
        End Sub

        ''' <summary>
        ''' Initialize the class
        ''' </summary>
        ''' <remarks></remarks>
        Public Sub New()
            MyBase.New()
        End Sub

        ''' <summary>
        ''' Initialize the class
        ''' </summary>
        ''' <param name="tbl"></param>
        ''' <remarks></remarks>
        Public Sub New(ByRef tbl As System.Data.DataTable)
            MyClass.New()
            Me.tbl = tbl
        End Sub

        ''' <summary>
        ''' Number of appointments created by this thread
        ''' </summary>
        ''' <value></value>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Property Count As Int32
            Get
                Return privateCount
            End Get
            Set(value As Int32)
                If privateCount <> value Then
                    RaiseCountChanging("Count", privateCount, value)
                    privateCount = value
                End If
            End Set
        End Property

        ''' <summary>
        ''' Date for the appointments
        ''' </summary>
        ''' <value></value>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Property CurrentDate As DateTime

        ''' <summary>
        ''' Execute the procedure to create the appointment time slots
        ''' </summary>
        ''' <remarks></remarks>
        Public Sub RunProcedure()
            Dim cnReader As SqlClient.SqlConnection = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
            Dim rd As SqlClient.SqlDataReader = Nothing

            Dim txnProcess As SqlClient.SqlTransaction = Nothing
            Dim cnProcess As SqlClient.SqlConnection = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)

            Try
                cnReader.Open()

                ' Find the day of the week
                Dim dow As System.Int32 = Convert.ToInt32(CurrentDate.DayOfWeek) + 1

                ' Find the list of offices that represent this day of the week for the current date
                Dim OfficeList As New System.Text.StringBuilder
                For Each row As System.Data.DataRow In tbl.Rows
                    Select Case dow
                        Case 1 : If Convert.ToInt32(row("sun")) <> 0 Then OfficeList.AppendFormat(",{0:d}", row("office"))
                        Case 2 : If Convert.ToInt32(row("mon")) <> 0 Then OfficeList.AppendFormat(",{0:d}", row("office"))
                        Case 3 : If Convert.ToInt32(row("tue")) <> 0 Then OfficeList.AppendFormat(",{0:d}", row("office"))
                        Case 4 : If Convert.ToInt32(row("wed")) <> 0 Then OfficeList.AppendFormat(",{0:d}", row("office"))
                        Case 5 : If Convert.ToInt32(row("thu")) <> 0 Then OfficeList.AppendFormat(",{0:d}", row("office"))
                        Case 6 : If Convert.ToInt32(row("fri")) <> 0 Then OfficeList.AppendFormat(",{0:d}", row("office"))
                        Case 7 : If Convert.ToInt32(row("sat")) <> 0 Then OfficeList.AppendFormat(",{0:d}", row("office"))
                    End Select
                Next

                If OfficeList.Length > 0 Then
                    OfficeList.Remove(0, 1) ' the beginning comma

                    ' Find the appointments for these offices and this day of the week
                    Using cmd As SqlClient.SqlCommand = New SqlCommand
                        With cmd
                            .Connection = cnReader
                            .CommandText = "SELECT appt_time_template,office,appt_type,start_time FROM appt_time_templates WITH (NOLOCK) WHERE dow=@dow AND office IN (" + OfficeList.ToString() + ") ORDER BY office,dow,start_time"
                            .CommandType = CommandType.Text
                            .CommandTimeout = DebtPlus.LINQ.SQLInfoClass.getDefault().CommandTimeout
                            .Parameters.Add("@dow", SqlDbType.Int).Value = dow

                            rd = .ExecuteReader(CommandBehavior.SingleResult)
                        End With
                    End Using

                    ' Create a second connection to process the transfers
                    If rd IsNot Nothing Then

                        ' Allocate a second connection to do the work.
                        cnProcess = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
                        cnProcess.Open()

                        ' Copy the information to the appoiment booking tables
                        Do While rd.Read()
                            Dim Minutes As System.Int32 = Convert.ToInt32(rd.GetValue(3))
                            Dim start_time As Date = CurrentDate.AddMinutes(Minutes)

                            Dim appt_time As System.Int32 = CreateApptTime(cnProcess, txnProcess, Convert.ToInt32(rd.GetValue(1)), rd.GetValue(2), start_time)
                            If appt_time > 0 Then
                                Count += CopyCounselors(cnProcess, txnProcess, Convert.ToInt32(rd.GetValue(0)), appt_time)
                            End If
                        Loop
                    End If
                End If

            Finally
                If txnProcess IsNot Nothing Then
                    Try
                        txnProcess.Rollback()
                    Catch exRollback As System.Exception
                    End Try
                    txnProcess.Dispose()
                    txnProcess = Nothing
                End If
                If cnProcess IsNot Nothing Then cnProcess.Dispose()

                If rd IsNot Nothing Then rd.Dispose()
                If cnReader IsNot Nothing Then cnReader.Dispose()
            End Try
        End Sub

        ''' <summary>
        ''' Create an appointment time slot
        ''' </summary>
        ''' <param name="cn"></param>
        ''' <param name="txn"></param>
        ''' <param name="office"></param>
        ''' <param name="appt_type"></param>
        ''' <param name="start_time"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Private Function CreateApptTime(ByVal cn As SqlClient.SqlConnection, ByVal txn As SqlClient.SqlTransaction, ByVal office As System.Int32, ByVal appt_type As Object, ByVal start_time As DateTime) As System.Int32
            Dim answer As System.Int32 = -1

            Try
                Using cmd As SqlClient.SqlCommand = New SqlCommand()
                    With cmd
                        .Connection = cn
                        .Transaction = txn
                        .CommandText = "xpr_insert_appt_time"
                        .CommandType = CommandType.StoredProcedure
                        .CommandTimeout = DebtPlus.LINQ.SQLInfoClass.getDefault().CommandTimeout
                        .Parameters.Add("RETURN", SqlDbType.Int).Direction = ParameterDirection.ReturnValue
                        .Parameters.Add("@office", SqlDbType.Int).Value = office
                        .Parameters.Add("@appt_type", SqlDbType.Int).Value = appt_type
                        .Parameters.Add("@start_time", SqlDbType.DateTime).Value = start_time
                        .ExecuteNonQuery()

                        Dim objAnswer As Object = .Parameters(0).Value
                        If (objAnswer IsNot Nothing) AndAlso (objAnswer IsNot System.DBNull.Value) Then answer = Convert.ToInt32(objAnswer)
                    End With
                End Using

            Catch ex As SqlException

                ' Catch the exception that the key is already defined. It is possible to do this. We used to first look
                ' but that is not really cost effective with a large database so we do the insert and catch the error
                ' instead.
                If ex.Message.Contains("Cannot insert duplicate key row in object") Then
                    Return -1
                Else
                    Throw
                End If
            End Try

            Return answer
        End Function

        ''' <summary>
        ''' Copy the list of counselors from the template to the time slots
        ''' </summary>
        ''' <param name="cn"></param>
        ''' <param name="txn"></param>
        ''' <param name="appt_time_template"></param>
        ''' <param name="appt_time"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Private Function CopyCounselors(ByVal cn As SqlClient.SqlConnection, ByVal txn As SqlClient.SqlTransaction, ByVal appt_time_template As System.Int32, ByVal appt_time As System.Int32) As System.Int32
            Dim answer As System.Int32 = 0
            Try
                Using cmd As SqlClient.SqlCommand = New SqlCommand()
                    With cmd
                        .Connection = cn
                        .Transaction = txn
                        .CommandText = "xpr_insert_appt_counselors_copied"
                        .CommandType = CommandType.StoredProcedure
                        .CommandTimeout = DebtPlus.LINQ.SQLInfoClass.getDefault().CommandTimeout
                        .Parameters.Add("RETURN", SqlDbType.Int).Direction = ParameterDirection.ReturnValue
                        .Parameters.Add("@appt_time", SqlDbType.Int).Value = appt_time
                        .Parameters.Add("@appt_time_template", SqlDbType.Int).Value = appt_time_template
                        .ExecuteNonQuery()

                        If .Parameters(0).Value IsNot System.DBNull.Value AndAlso .Parameters(0).Value IsNot Nothing Then
                            answer = Convert.ToInt32(.Parameters(0).Value)
                        End If
                    End With
                End Using

            Catch ex As SqlException
                If ex.Message.Contains("Cannot insert duplicate key row in object") Then
                    Return -1
                Else
                    Throw
                End If
            End Try

            Return answer
        End Function
    End Class
End Namespace
