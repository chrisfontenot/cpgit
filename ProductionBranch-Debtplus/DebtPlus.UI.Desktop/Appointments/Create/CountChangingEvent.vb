﻿Namespace Appointments.Create
    ''' <summary>
    ''' Class for the CountChangingEventHandler routine arguments. They contain both the old and new values along with the property name
    ''' </summary>
    ''' <remarks></remarks>
    Friend Class CountChangingEventArgs
        Inherits System.ComponentModel.PropertyChangingEventArgs
        Public Property OldValue As Int32
        Public Property NewValue As Int32

        Public Sub New(ByVal PropertyName As String)
            MyBase.New(PropertyName)
        End Sub

        Public Sub New(ByVal PropertyName As String, ByVal OldValue As Int32, ByVal NewValue As Int32)
            MyClass.New(PropertyName)
            Me.OldValue = OldValue
            Me.NewValue = NewValue
        End Sub
    End Class

    ''' <summary>
    ''' Delegate to the event for the change in the count of appointments created
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Friend Delegate Sub CountChangingEventHandler(ByVal sender As Object, ByVal e As CountChangingEventArgs)

    ''' <summary>
    ''' Interface to describe the event for the count of appointments created
    ''' </summary>
    ''' <remarks></remarks>
    Friend Interface INotifyCountChanging
        Event CountChanging As CountChangingEventHandler
    End Interface
End Namespace