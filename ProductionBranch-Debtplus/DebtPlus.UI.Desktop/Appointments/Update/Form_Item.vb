#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}
#End Region

Option Compare Binary
Option Explicit On
Option Strict On

Imports DebtPlus.Utils
Imports System.Data.SqlClient
Imports DebtPlus.Utils.Format
Imports System.Windows.Forms
Imports System.Drawing

Namespace Appointments.Update

    Friend Class Form_Item
        Inherits DebtPlus.Data.Forms.DebtPlusForm

        Private Const FORM_NAME As String = "Appointments.Update.Item"
        Private ap As ArgParser
        Private drv As System.Data.DataRowView
        Private appt_time As System.Int32 = -1

        Public Sub New()
            MyBase.New()
            InitializeComponent()
        End Sub

        ''' <summary>
        ''' Create our instance of the class
        ''' </summary>
        Public Sub New(ByVal ap As ArgParser, ByVal drv As System.Data.DataRowView)
            MyClass.New()
            Me.ap = ap
            Me.drv = drv

            AddHandler Load, AddressOf Form_Item_Load
            AddHandler counselors.DrawItem, AddressOf counselors_DrawItem
            AddHandler appt_type.EditValueChanged, AddressOf appt_type_EditValueChanged
        End Sub

#Region " Windows Form Designer generated code "

        'Form overrides dispose to clean up the component list.
        Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
            If disposing Then
                If components IsNot Nothing Then
                    components.Dispose()
                End If
            End If
            MyBase.Dispose(disposing)
        End Sub

        'Required by the Windows Form Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Windows Form Designer
        'It can be modified using the Windows Form Designer.  
        'Do not modify it using the code editor.
        Friend WithEvents GridControl1 As DevExpress.XtraGrid.GridControl
        Friend WithEvents GridView1 As DevExpress.XtraGrid.Views.Grid.GridView
        Friend WithEvents GridColumn_client_appointment As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents GridColumn_client As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents GridColumn_appt_time As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents GridColumn_counselor As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents GridColumn_start_time As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents GridColumn_office As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents GridColumn_appt_type As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents GridColumn_status As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents GridColumn_result As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents GridColumn_duration As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents GridColumn_priority As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents GridColumn_housing As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents GridColumn_post_purchase As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents GridColumn_credit As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents GridColumn_callback_ph As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents GridColumn_confirmation_status As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents GridColumn_date_confirmed As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents GridColumn_date_updated As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents GridColumn_date_created As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents GridColumn_created_by As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents GridColumn_formatted_name As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents GridColumn_active_status As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents GridColumn_formatted_home_ph As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents GridColumn_formatted_work_ph As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents GridColumn_formatted_message_ph As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents GridColumn_formatted_counselor As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents GridColumn_formatted_office As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents GridColumn_formatted_appt_type As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents GridColumn_formatted_status As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents GridColumn_formatted_call_ph As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents GridColumn_formatted_confirmation_status As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents office_name As DevExpress.XtraEditors.LabelControl
        Friend WithEvents LabelControl3 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents LabelControl2 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents Button_OK As DevExpress.XtraEditors.SimpleButton
        Friend WithEvents Button_Cancel As DevExpress.XtraEditors.SimpleButton
        Friend WithEvents counselors As DevExpress.XtraEditors.CheckedListBoxControl
        Friend WithEvents start_time As DevExpress.XtraEditors.TimeEdit
        Friend WithEvents appt_type As DevExpress.XtraEditors.LookUpEdit
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
            Me.GridControl1 = New DevExpress.XtraGrid.GridControl()
            Me.GridView1 = New DevExpress.XtraGrid.Views.Grid.GridView()
            Me.GridColumn_client_appointment = New DevExpress.XtraGrid.Columns.GridColumn()
            Me.GridColumn_client = New DevExpress.XtraGrid.Columns.GridColumn()
            Me.GridColumn_appt_time = New DevExpress.XtraGrid.Columns.GridColumn()
            Me.GridColumn_counselor = New DevExpress.XtraGrid.Columns.GridColumn()
            Me.GridColumn_start_time = New DevExpress.XtraGrid.Columns.GridColumn()
            Me.GridColumn_office = New DevExpress.XtraGrid.Columns.GridColumn()
            Me.GridColumn_appt_type = New DevExpress.XtraGrid.Columns.GridColumn()
            Me.GridColumn_status = New DevExpress.XtraGrid.Columns.GridColumn()
            Me.GridColumn_result = New DevExpress.XtraGrid.Columns.GridColumn()
            Me.GridColumn_duration = New DevExpress.XtraGrid.Columns.GridColumn()
            Me.GridColumn_priority = New DevExpress.XtraGrid.Columns.GridColumn()
            Me.GridColumn_housing = New DevExpress.XtraGrid.Columns.GridColumn()
            Me.GridColumn_post_purchase = New DevExpress.XtraGrid.Columns.GridColumn()
            Me.GridColumn_credit = New DevExpress.XtraGrid.Columns.GridColumn()
            Me.GridColumn_callback_ph = New DevExpress.XtraGrid.Columns.GridColumn()
            Me.GridColumn_confirmation_status = New DevExpress.XtraGrid.Columns.GridColumn()
            Me.GridColumn_date_confirmed = New DevExpress.XtraGrid.Columns.GridColumn()
            Me.GridColumn_date_updated = New DevExpress.XtraGrid.Columns.GridColumn()
            Me.GridColumn_date_created = New DevExpress.XtraGrid.Columns.GridColumn()
            Me.GridColumn_created_by = New DevExpress.XtraGrid.Columns.GridColumn()
            Me.GridColumn_formatted_name = New DevExpress.XtraGrid.Columns.GridColumn()
            Me.GridColumn_active_status = New DevExpress.XtraGrid.Columns.GridColumn()
            Me.GridColumn_formatted_home_ph = New DevExpress.XtraGrid.Columns.GridColumn()
            Me.GridColumn_formatted_work_ph = New DevExpress.XtraGrid.Columns.GridColumn()
            Me.GridColumn_formatted_message_ph = New DevExpress.XtraGrid.Columns.GridColumn()
            Me.GridColumn_formatted_counselor = New DevExpress.XtraGrid.Columns.GridColumn()
            Me.GridColumn_formatted_office = New DevExpress.XtraGrid.Columns.GridColumn()
            Me.GridColumn_formatted_appt_type = New DevExpress.XtraGrid.Columns.GridColumn()
            Me.GridColumn_formatted_status = New DevExpress.XtraGrid.Columns.GridColumn()
            Me.GridColumn_formatted_call_ph = New DevExpress.XtraGrid.Columns.GridColumn()
            Me.GridColumn_formatted_confirmation_status = New DevExpress.XtraGrid.Columns.GridColumn()
            Me.counselors = New DevExpress.XtraEditors.CheckedListBoxControl()
            Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl()
            Me.office_name = New DevExpress.XtraEditors.LabelControl()
            Me.LabelControl3 = New DevExpress.XtraEditors.LabelControl()
            Me.start_time = New DevExpress.XtraEditors.TimeEdit()
            Me.LabelControl2 = New DevExpress.XtraEditors.LabelControl()
            Me.appt_type = New DevExpress.XtraEditors.LookUpEdit()
            Me.Button_OK = New DevExpress.XtraEditors.SimpleButton()
            Me.Button_Cancel = New DevExpress.XtraEditors.SimpleButton()
            CType(Me.DxErrorProvider1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.GridView1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.counselors, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.start_time.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.appt_type.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.SuspendLayout()
            '
            'DefaultLookAndFeel1
            '
            Me.DefaultLookAndFeel1.LookAndFeel.SkinName = "McSkin"
            '
            'GridControl1
            '
            Me.GridControl1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.GridControl1.Location = New System.Drawing.Point(184, 80)
            Me.GridControl1.MainView = Me.GridView1
            Me.GridControl1.Name = "GridControl1"
            Me.GridControl1.Size = New System.Drawing.Size(216, 176)
            Me.GridControl1.TabIndex = 8
            Me.GridControl1.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridView1})
            '
            'GridView1
            '
            Me.GridView1.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.GridColumn_client_appointment, Me.GridColumn_client, Me.GridColumn_appt_time, Me.GridColumn_counselor, Me.GridColumn_start_time, Me.GridColumn_office, Me.GridColumn_appt_type, Me.GridColumn_status, Me.GridColumn_result, Me.GridColumn_duration, Me.GridColumn_priority, Me.GridColumn_housing, Me.GridColumn_post_purchase, Me.GridColumn_credit, Me.GridColumn_callback_ph, Me.GridColumn_confirmation_status, Me.GridColumn_date_confirmed, Me.GridColumn_date_updated, Me.GridColumn_date_created, Me.GridColumn_created_by, Me.GridColumn_formatted_name, Me.GridColumn_active_status, Me.GridColumn_formatted_home_ph, Me.GridColumn_formatted_work_ph, Me.GridColumn_formatted_message_ph, Me.GridColumn_formatted_counselor, Me.GridColumn_formatted_office, Me.GridColumn_formatted_appt_type, Me.GridColumn_formatted_status, Me.GridColumn_formatted_call_ph, Me.GridColumn_formatted_confirmation_status})
            Me.GridView1.GridControl = Me.GridControl1
            Me.GridView1.Name = "GridView1"
            Me.GridView1.OptionsBehavior.Editable = False
            Me.GridView1.OptionsView.ShowGroupPanel = False
            Me.GridView1.OptionsView.ShowIndicator = False
            '
            'GridColumn_client_appointment
            '
            Me.GridColumn_client_appointment.AppearanceCell.Options.UseTextOptions = True
            Me.GridColumn_client_appointment.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.GridColumn_client_appointment.AppearanceHeader.Options.UseTextOptions = True
            Me.GridColumn_client_appointment.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.GridColumn_client_appointment.Caption = "Appointment ID"
            Me.GridColumn_client_appointment.CustomizationCaption = "Appointment ID"
            Me.GridColumn_client_appointment.DisplayFormat.FormatString = "f0"
            Me.GridColumn_client_appointment.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.GridColumn_client_appointment.FieldName = "client_appointment"
            Me.GridColumn_client_appointment.GroupFormat.FormatString = "f0"
            Me.GridColumn_client_appointment.GroupFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.GridColumn_client_appointment.Name = "GridColumn_client_appointment"
            Me.GridColumn_client_appointment.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.[True]
            '
            'GridColumn_client
            '
            Me.GridColumn_client.AppearanceCell.Options.UseTextOptions = True
            Me.GridColumn_client.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.GridColumn_client.AppearanceHeader.Options.UseTextOptions = True
            Me.GridColumn_client.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.GridColumn_client.Caption = "Client"
            Me.GridColumn_client.CustomizationCaption = "Client ID"
            Me.GridColumn_client.DisplayFormat.FormatString = "f0"
            Me.GridColumn_client.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.GridColumn_client.FieldName = "client"
            Me.GridColumn_client.GroupFormat.FormatString = "f0"
            Me.GridColumn_client.GroupFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.GridColumn_client.Name = "GridColumn_client"
            Me.GridColumn_client.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.[True]
            Me.GridColumn_client.Visible = True
            Me.GridColumn_client.VisibleIndex = 0
            Me.GridColumn_client.Width = 139
            '
            'GridColumn_appt_time
            '
            Me.GridColumn_appt_time.AppearanceCell.Options.UseTextOptions = True
            Me.GridColumn_appt_time.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.GridColumn_appt_time.AppearanceHeader.Options.UseTextOptions = True
            Me.GridColumn_appt_time.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.GridColumn_appt_time.Caption = "Appt Time"
            Me.GridColumn_appt_time.CustomizationCaption = "Appt Time ID"
            Me.GridColumn_appt_time.DisplayFormat.FormatString = "f0"
            Me.GridColumn_appt_time.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.GridColumn_appt_time.FieldName = "appt_time"
            Me.GridColumn_appt_time.GroupFormat.FormatString = "f0"
            Me.GridColumn_appt_time.GroupFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.GridColumn_appt_time.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.[Date]
            Me.GridColumn_appt_time.Name = "GridColumn_appt_time"
            Me.GridColumn_appt_time.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.[True]
            '
            'GridColumn_counselor
            '
            Me.GridColumn_counselor.AppearanceCell.Options.UseTextOptions = True
            Me.GridColumn_counselor.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.GridColumn_counselor.AppearanceHeader.Options.UseTextOptions = True
            Me.GridColumn_counselor.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.GridColumn_counselor.Caption = "Counselor"
            Me.GridColumn_counselor.CustomizationCaption = "Counselor ID"
            Me.GridColumn_counselor.DisplayFormat.FormatString = "f0"
            Me.GridColumn_counselor.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.GridColumn_counselor.FieldName = "counselor"
            Me.GridColumn_counselor.GroupFormat.FormatString = "f0"
            Me.GridColumn_counselor.GroupFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.GridColumn_counselor.Name = "GridColumn_counselor"
            Me.GridColumn_counselor.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.[True]
            '
            'GridColumn_start_time
            '
            Me.GridColumn_start_time.AppearanceCell.Options.UseTextOptions = True
            Me.GridColumn_start_time.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.GridColumn_start_time.AppearanceHeader.Options.UseTextOptions = True
            Me.GridColumn_start_time.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.GridColumn_start_time.Caption = "Date/Time"
            Me.GridColumn_start_time.CustomizationCaption = "Starting Time"
            Me.GridColumn_start_time.DisplayFormat.FormatString = "d"
            Me.GridColumn_start_time.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime
            Me.GridColumn_start_time.FieldName = "start_time"
            Me.GridColumn_start_time.GroupFormat.FormatString = "d"
            Me.GridColumn_start_time.GroupFormat.FormatType = DevExpress.Utils.FormatType.DateTime
            Me.GridColumn_start_time.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.[Date]
            Me.GridColumn_start_time.Name = "GridColumn_start_time"
            Me.GridColumn_start_time.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.[True]
            '
            'GridColumn_office
            '
            Me.GridColumn_office.AppearanceCell.Options.UseTextOptions = True
            Me.GridColumn_office.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.GridColumn_office.AppearanceHeader.Options.UseTextOptions = True
            Me.GridColumn_office.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.GridColumn_office.Caption = "Office"
            Me.GridColumn_office.CustomizationCaption = "Office ID"
            Me.GridColumn_office.DisplayFormat.FormatString = "f0"
            Me.GridColumn_office.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.GridColumn_office.FieldName = "office"
            Me.GridColumn_office.GroupFormat.FormatString = "f0"
            Me.GridColumn_office.GroupFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.GridColumn_office.Name = "GridColumn_office"
            Me.GridColumn_office.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.[True]
            '
            'GridColumn_appt_type
            '
            Me.GridColumn_appt_type.AppearanceCell.Options.UseTextOptions = True
            Me.GridColumn_appt_type.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.GridColumn_appt_type.AppearanceHeader.Options.UseTextOptions = True
            Me.GridColumn_appt_type.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.GridColumn_appt_type.Caption = "Appt Type ID"
            Me.GridColumn_appt_type.CustomizationCaption = "Appointment Type ID"
            Me.GridColumn_appt_type.DisplayFormat.FormatString = "f0"
            Me.GridColumn_appt_type.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.GridColumn_appt_type.FieldName = "appt_type"
            Me.GridColumn_appt_type.GroupFormat.FormatString = "f0"
            Me.GridColumn_appt_type.GroupFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.GridColumn_appt_type.Name = "GridColumn_appt_type"
            Me.GridColumn_appt_type.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.[True]
            '
            'GridColumn_status
            '
            Me.GridColumn_status.AppearanceCell.Options.UseTextOptions = True
            Me.GridColumn_status.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.GridColumn_status.AppearanceHeader.Options.UseTextOptions = True
            Me.GridColumn_status.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.GridColumn_status.Caption = "Appt Status"
            Me.GridColumn_status.CustomizationCaption = "Appointment Status ID"
            Me.GridColumn_status.FieldName = "status"
            Me.GridColumn_status.Name = "GridColumn_status"
            Me.GridColumn_status.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.[True]
            '
            'GridColumn_result
            '
            Me.GridColumn_result.Caption = "Appt Result"
            Me.GridColumn_result.CustomizationCaption = "Appointment Result"
            Me.GridColumn_result.FieldName = "result"
            Me.GridColumn_result.Name = "GridColumn_result"
            Me.GridColumn_result.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.[True]
            '
            'GridColumn_duration
            '
            Me.GridColumn_duration.AppearanceCell.Options.UseTextOptions = True
            Me.GridColumn_duration.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.GridColumn_duration.AppearanceHeader.Options.UseTextOptions = True
            Me.GridColumn_duration.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.GridColumn_duration.Caption = "Duration"
            Me.GridColumn_duration.CustomizationCaption = "Duration"
            Me.GridColumn_duration.FieldName = "duration"
            Me.GridColumn_duration.Name = "GridColumn_duration"
            Me.GridColumn_duration.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.[True]
            '
            'GridColumn_priority
            '
            Me.GridColumn_priority.Caption = "Priority"
            Me.GridColumn_priority.CustomizationCaption = "Priority Appointment?"
            Me.GridColumn_priority.DisplayFormat.FormatString = "Y;N;N"
            Me.GridColumn_priority.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.GridColumn_priority.FieldName = "priority"
            Me.GridColumn_priority.Name = "GridColumn_priority"
            Me.GridColumn_priority.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.[True]
            '
            'GridColumn_housing
            '
            Me.GridColumn_housing.Caption = "Housing"
            Me.GridColumn_housing.CustomizationCaption = "Housing Appointment?"
            Me.GridColumn_housing.DisplayFormat.FormatString = "Y;N;N"
            Me.GridColumn_housing.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.GridColumn_housing.FieldName = "housing"
            Me.GridColumn_housing.Name = "GridColumn_housing"
            Me.GridColumn_housing.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.[True]
            '
            'GridColumn_post_purchase
            '
            Me.GridColumn_post_purchase.Caption = "Purchase"
            Me.GridColumn_post_purchase.CustomizationCaption = "Post Purchase Appointment?"
            Me.GridColumn_post_purchase.DisplayFormat.FormatString = "Y;N;N"
            Me.GridColumn_post_purchase.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.GridColumn_post_purchase.FieldName = "post_purchase"
            Me.GridColumn_post_purchase.Name = "GridColumn_post_purchase"
            Me.GridColumn_post_purchase.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.[True]
            '
            'GridColumn_credit
            '
            Me.GridColumn_credit.Caption = "Credit"
            Me.GridColumn_credit.CustomizationCaption = "Credit Appointment?"
            Me.GridColumn_credit.DisplayFormat.FormatString = "Y;N;N"
            Me.GridColumn_credit.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.GridColumn_credit.FieldName = "credit"
            Me.GridColumn_credit.Name = "GridColumn_credit"
            Me.GridColumn_credit.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.[True]
            '
            'GridColumn_callback_ph
            '
            Me.GridColumn_callback_ph.Caption = "Callback #"
            Me.GridColumn_callback_ph.CustomizationCaption = "Callback Phone ID"
            Me.GridColumn_callback_ph.FieldName = "callback_ph"
            Me.GridColumn_callback_ph.Name = "GridColumn_callback_ph"
            Me.GridColumn_callback_ph.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.[True]
            '
            'GridColumn_confirmation_status
            '
            Me.GridColumn_confirmation_status.Caption = "Confirmed"
            Me.GridColumn_confirmation_status.CustomizationCaption = "Confirmation Status ID"
            Me.GridColumn_confirmation_status.FieldName = "confirmation_status"
            Me.GridColumn_confirmation_status.Name = "GridColumn_confirmation_status"
            Me.GridColumn_confirmation_status.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.[True]
            '
            'GridColumn_date_confirmed
            '
            Me.GridColumn_date_confirmed.AppearanceCell.Options.UseTextOptions = True
            Me.GridColumn_date_confirmed.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.GridColumn_date_confirmed.AppearanceHeader.Options.UseTextOptions = True
            Me.GridColumn_date_confirmed.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.GridColumn_date_confirmed.Caption = "Date Confirmed"
            Me.GridColumn_date_confirmed.CustomizationCaption = "Date Confirmed"
            Me.GridColumn_date_confirmed.DisplayFormat.FormatString = "d"
            Me.GridColumn_date_confirmed.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime
            Me.GridColumn_date_confirmed.FieldName = "date_confirmed"
            Me.GridColumn_date_confirmed.GroupFormat.FormatString = "d"
            Me.GridColumn_date_confirmed.GroupFormat.FormatType = DevExpress.Utils.FormatType.DateTime
            Me.GridColumn_date_confirmed.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.[Date]
            Me.GridColumn_date_confirmed.Name = "GridColumn_date_confirmed"
            Me.GridColumn_date_confirmed.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.[True]
            '
            'GridColumn_date_updated
            '
            Me.GridColumn_date_updated.AppearanceCell.Options.UseTextOptions = True
            Me.GridColumn_date_updated.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.GridColumn_date_updated.AppearanceHeader.Options.UseTextOptions = True
            Me.GridColumn_date_updated.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.GridColumn_date_updated.Caption = "Date Updated"
            Me.GridColumn_date_updated.CustomizationCaption = "Date Last Changed"
            Me.GridColumn_date_updated.DisplayFormat.FormatString = "d"
            Me.GridColumn_date_updated.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime
            Me.GridColumn_date_updated.FieldName = "date_updated"
            Me.GridColumn_date_updated.GroupFormat.FormatString = "d"
            Me.GridColumn_date_updated.GroupFormat.FormatType = DevExpress.Utils.FormatType.DateTime
            Me.GridColumn_date_updated.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.[Date]
            Me.GridColumn_date_updated.Name = "GridColumn_date_updated"
            Me.GridColumn_date_updated.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.[True]
            '
            'GridColumn_date_created
            '
            Me.GridColumn_date_created.AppearanceCell.Options.UseTextOptions = True
            Me.GridColumn_date_created.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.GridColumn_date_created.AppearanceHeader.Options.UseTextOptions = True
            Me.GridColumn_date_created.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.GridColumn_date_created.Caption = "Date Made"
            Me.GridColumn_date_created.CustomizationCaption = "Date Appointment Made"
            Me.GridColumn_date_created.DisplayFormat.FormatString = "d"
            Me.GridColumn_date_created.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime
            Me.GridColumn_date_created.FieldName = "date_created"
            Me.GridColumn_date_created.GroupFormat.FormatString = "d"
            Me.GridColumn_date_created.GroupFormat.FormatType = DevExpress.Utils.FormatType.DateTime
            Me.GridColumn_date_created.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.[Date]
            Me.GridColumn_date_created.Name = "GridColumn_date_created"
            Me.GridColumn_date_created.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.[True]
            '
            'GridColumn_created_by
            '
            Me.GridColumn_created_by.Caption = "Creator"
            Me.GridColumn_created_by.CustomizationCaption = "Appointment Creator"
            Me.GridColumn_created_by.FieldName = "created_by"
            Me.GridColumn_created_by.Name = "GridColumn_created_by"
            Me.GridColumn_created_by.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.[True]
            '
            'GridColumn_formatted_name
            '
            Me.GridColumn_formatted_name.Caption = "Name"
            Me.GridColumn_formatted_name.CustomizationCaption = "Client Name"
            Me.GridColumn_formatted_name.FieldName = "formatted_name"
            Me.GridColumn_formatted_name.Name = "GridColumn_formatted_name"
            Me.GridColumn_formatted_name.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.[True]
            Me.GridColumn_formatted_name.Visible = True
            Me.GridColumn_formatted_name.VisibleIndex = 1
            Me.GridColumn_formatted_name.Width = 458
            '
            'GridColumn_active_status
            '
            Me.GridColumn_active_status.Caption = "Active Status"
            Me.GridColumn_active_status.CustomizationCaption = "Active Status"
            Me.GridColumn_active_status.FieldName = "active_status"
            Me.GridColumn_active_status.Name = "GridColumn_active_status"
            Me.GridColumn_active_status.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.[True]
            '
            'GridColumn_formatted_home_ph
            '
            Me.GridColumn_formatted_home_ph.Caption = "Home Ph#"
            Me.GridColumn_formatted_home_ph.CustomizationCaption = "Home/Evening Phone #"
            Me.GridColumn_formatted_home_ph.FieldName = "formatted_home_ph"
            Me.GridColumn_formatted_home_ph.Name = "GridColumn_formatted_home_ph"
            Me.GridColumn_formatted_home_ph.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.[True]
            '
            'GridColumn_formatted_work_ph
            '
            Me.GridColumn_formatted_work_ph.Caption = "Work Ph#"
            Me.GridColumn_formatted_work_ph.CustomizationCaption = "Work/Day Phone #"
            Me.GridColumn_formatted_work_ph.FieldName = "formatted_work_ph"
            Me.GridColumn_formatted_work_ph.Name = "GridColumn_formatted_work_ph"
            Me.GridColumn_formatted_work_ph.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.[True]
            '
            'GridColumn_formatted_message_ph
            '
            Me.GridColumn_formatted_message_ph.Caption = "Msg Ph#"
            Me.GridColumn_formatted_message_ph.CustomizationCaption = "Message Ph#"
            Me.GridColumn_formatted_message_ph.FieldName = "formatted_message_ph"
            Me.GridColumn_formatted_message_ph.Name = "GridColumn_formatted_message_ph"
            Me.GridColumn_formatted_message_ph.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.[True]
            '
            'GridColumn_formatted_counselor
            '
            Me.GridColumn_formatted_counselor.Caption = "Counselor"
            Me.GridColumn_formatted_counselor.CustomizationCaption = "Counselor"
            Me.GridColumn_formatted_counselor.FieldName = "formatted_counselor"
            Me.GridColumn_formatted_counselor.Name = "GridColumn_formatted_counselor"
            Me.GridColumn_formatted_counselor.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.[True]
            '
            'GridColumn_formatted_office
            '
            Me.GridColumn_formatted_office.Caption = "Office"
            Me.GridColumn_formatted_office.CustomizationCaption = "Office"
            Me.GridColumn_formatted_office.FieldName = "formatted_office"
            Me.GridColumn_formatted_office.Name = "GridColumn_formatted_office"
            Me.GridColumn_formatted_office.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.[True]
            '
            'GridColumn_formatted_appt_type
            '
            Me.GridColumn_formatted_appt_type.Caption = "Appt Type"
            Me.GridColumn_formatted_appt_type.CustomizationCaption = "Appointment Type"
            Me.GridColumn_formatted_appt_type.FieldName = "formatted_appt_type"
            Me.GridColumn_formatted_appt_type.Name = "GridColumn_formatted_appt_type"
            Me.GridColumn_formatted_appt_type.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.[True]
            '
            'GridColumn_formatted_status
            '
            Me.GridColumn_formatted_status.Caption = "Appt Status"
            Me.GridColumn_formatted_status.CustomizationCaption = "Appointment Status"
            Me.GridColumn_formatted_status.FieldName = "formatted_status"
            Me.GridColumn_formatted_status.Name = "GridColumn_formatted_status"
            Me.GridColumn_formatted_status.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.[True]
            '
            'GridColumn_formatted_call_ph
            '
            Me.GridColumn_formatted_call_ph.Caption = "Callback Ph#"
            Me.GridColumn_formatted_call_ph.CustomizationCaption = "Callback Phone Number"
            Me.GridColumn_formatted_call_ph.FieldName = "formatted_call_ph"
            Me.GridColumn_formatted_call_ph.Name = "GridColumn_formatted_call_ph"
            Me.GridColumn_formatted_call_ph.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.[True]
            '
            'GridColumn_formatted_confirmation_status
            '
            Me.GridColumn_formatted_confirmation_status.Caption = "Confirmed"
            Me.GridColumn_formatted_confirmation_status.CustomizationCaption = "Confirmation Status"
            Me.GridColumn_formatted_confirmation_status.FieldName = "formatted_confirmation_status"
            Me.GridColumn_formatted_confirmation_status.Name = "GridColumn_formatted_confirmation_status"
            Me.GridColumn_formatted_confirmation_status.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.[True]
            '
            'counselors
            '
            Me.counselors.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
            Me.counselors.CheckOnClick = True
            Me.counselors.Location = New System.Drawing.Point(8, 80)
            Me.counselors.Name = "counselors"
            Me.counselors.Size = New System.Drawing.Size(168, 176)
            Me.counselors.TabIndex = 7
            '
            'LabelControl1
            '
            Me.LabelControl1.Location = New System.Drawing.Point(8, 8)
            Me.LabelControl1.Name = "LabelControl1"
            Me.LabelControl1.Size = New System.Drawing.Size(33, 13)
            Me.LabelControl1.TabIndex = 0
            Me.LabelControl1.Text = "Office:"
            '
            'office_name
            '
            Me.office_name.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.office_name.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
            Me.office_name.Appearance.TextOptions.Trimming = DevExpress.Utils.Trimming.Character
            Me.office_name.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Top
            Me.office_name.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap
            Me.office_name.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
            Me.office_name.Location = New System.Drawing.Point(96, 8)
            Me.office_name.Name = "office_name"
            Me.office_name.Size = New System.Drawing.Size(304, 13)
            Me.office_name.TabIndex = 1
            Me.office_name.Text = "LabelControl2"
            '
            'LabelControl3
            '
            Me.LabelControl3.Location = New System.Drawing.Point(8, 35)
            Me.LabelControl3.Name = "LabelControl3"
            Me.LabelControl3.Size = New System.Drawing.Size(53, 13)
            Me.LabelControl3.TabIndex = 2
            Me.LabelControl3.Text = "Start Time:"
            '
            'start_time
            '
            Me.start_time.EditValue = New Date(2007, 4, 6, 0, 0, 0, 0)
            Me.start_time.Location = New System.Drawing.Point(96, 32)
            Me.start_time.Name = "start_time"
            Me.start_time.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
            Me.start_time.Properties.DisplayFormat.FormatString = "hh:mm tt"
            Me.start_time.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime
            Me.start_time.Properties.EditFormat.FormatString = "hh:m tt"
            Me.start_time.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime
            Me.start_time.Size = New System.Drawing.Size(90, 20)
            Me.start_time.TabIndex = 4
            '
            'LabelControl2
            '
            Me.LabelControl2.Location = New System.Drawing.Point(8, 59)
            Me.LabelControl2.Name = "LabelControl2"
            Me.LabelControl2.Size = New System.Drawing.Size(54, 13)
            Me.LabelControl2.TabIndex = 5
            Me.LabelControl2.Text = "Appt Type:"
            '
            'appt_type
            '
            Me.appt_type.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.appt_type.Location = New System.Drawing.Point(96, 56)
            Me.appt_type.Name = "appt_type"
            Me.appt_type.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.appt_type.Properties.Columns.AddRange(New DevExpress.XtraEditors.Controls.LookUpColumnInfo() {New DevExpress.XtraEditors.Controls.LookUpColumnInfo("appt_type", "Type", 10, DevExpress.Utils.FormatType.Numeric, "f0", True, DevExpress.Utils.HorzAlignment.Far, DevExpress.Data.ColumnSortOrder.None, DevExpress.Utils.DefaultBoolean.[True]), New DevExpress.XtraEditors.Controls.LookUpColumnInfo("appt_name", "Description", 50, DevExpress.Utils.FormatType.None, "", True, DevExpress.Utils.HorzAlignment.[Default], DevExpress.Data.ColumnSortOrder.Ascending, DevExpress.Utils.DefaultBoolean.[True]), New DevExpress.XtraEditors.Controls.LookUpColumnInfo("appt_duration", "Duration", 10, DevExpress.Utils.FormatType.Numeric, "f0", True, DevExpress.Utils.HorzAlignment.Far, DevExpress.Data.ColumnSortOrder.None, DevExpress.Utils.DefaultBoolean.[True])})
            Me.appt_type.Properties.NullText = "All"
            Me.appt_type.Properties.SortColumnIndex = 1
            Me.appt_type.Size = New System.Drawing.Size(304, 20)
            Me.appt_type.TabIndex = 6
            Me.appt_type.ToolTipController = Me.ToolTipController1
            '
            'Button_OK
            '
            Me.Button_OK.Anchor = System.Windows.Forms.AnchorStyles.Bottom
            Me.Button_OK.DialogResult = System.Windows.Forms.DialogResult.OK
            Me.Button_OK.Location = New System.Drawing.Point(123, 264)
            Me.Button_OK.Name = "Button_OK"
            Me.Button_OK.Size = New System.Drawing.Size(75, 23)
            Me.Button_OK.TabIndex = 9
            Me.Button_OK.Text = "&OK"
            '
            'Button_Cancel
            '
            Me.Button_Cancel.Anchor = System.Windows.Forms.AnchorStyles.Bottom
            Me.Button_Cancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
            Me.Button_Cancel.Location = New System.Drawing.Point(211, 264)
            Me.Button_Cancel.Name = "Button_Cancel"
            Me.Button_Cancel.Size = New System.Drawing.Size(75, 23)
            Me.Button_Cancel.TabIndex = 10
            Me.Button_Cancel.Text = "&Cancel"
            '
            'Form_Item
            '
            Me.ClientSize = New System.Drawing.Size(408, 302)
            Me.Controls.Add(Me.Button_Cancel)
            Me.Controls.Add(Me.Button_OK)
            Me.Controls.Add(Me.appt_type)
            Me.Controls.Add(Me.LabelControl2)
            Me.Controls.Add(Me.start_time)
            Me.Controls.Add(Me.LabelControl3)
            Me.Controls.Add(Me.office_name)
            Me.Controls.Add(Me.LabelControl1)
            Me.Controls.Add(Me.counselors)
            Me.Controls.Add(Me.GridControl1)
            Me.Name = "Form_Item"
            Me.Text = "Edit Appointment Information"
            CType(Me.DxErrorProvider1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.GridView1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.counselors, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.start_time.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.appt_type.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            Me.ResumeLayout(False)
            Me.PerformLayout()

        End Sub

#End Region

        ''' <summary>
        ''' Discard any values less than a minute
        ''' </summary>
        Private Shared Function DateAndTime(ByVal InputDate As Date) As Date
            Return InputDate.Date.Add(New TimeSpan(InputDate.Hour, InputDate.Minute, 0))
        End Function

        ''' <summary>
        ''' Process the FORM LOAD event
        ''' </summary>
        Private Sub Form_Item_Load(ByVal sender As System.Object, ByVal e As System.EventArgs)
            Dim cn As SqlClient.SqlConnection = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)

            ' Restore the window size and location
            LoadPlacement(FORM_NAME)

            ' Find the appointment time being edited
            appt_time = Convert.ToInt32(drv("appt_time"))

            ' Add the appointment time if required
            If appt_time < 0 Then
                Try
                    System.Windows.Forms.Cursor.Current = Cursors.WaitCursor
                    cn.Open()

                    Using InsertCmd As SqlClient.SqlCommand = New SqlCommand
                        With InsertCmd
                            .Connection = cn
                            .CommandText = "xpr_insert_appt_time"
                            .CommandType = CommandType.StoredProcedure
                            .Parameters.Add("@RETURN_VALUE", SqlDbType.Int).Direction = ParameterDirection.ReturnValue
                            .Parameters.Add("@start_time", SqlDbType.DateTime).Value = DateAndTime(Convert.ToDateTime(drv("start_time")))
                            .Parameters.Add("@office", SqlDbType.Int).Value = drv("office")
                            .Parameters.Add("@appt_type", SqlDbType.Int).Value = drv("appt_type")
                            .ExecuteNonQuery()
                            Dim objAnswer As Object = .Parameters(0).Value

                            If objAnswer IsNot Nothing AndAlso objAnswer IsNot System.DBNull.Value Then
                                appt_time = Convert.ToInt32(objAnswer)
                                drv("appt_time") = appt_time
                            End If
                        End With
                    End Using

                Catch ex As SqlClient.SqlException
                    Using gdr As New Repository.GetDataResult()
                        gdr.HandleException(ex)
                        DebtPlus.UI.Common.ErrorHandling.HandleErrors(gdr, "Error inserting appointment time entry")
                    End Using
                    DialogResult = System.Windows.Forms.DialogResult.Cancel

                Finally
                    If cn IsNot Nothing Then cn.Dispose()
                End Try
            End If

            ' Load the list of counselors for this appointment type
            With counselors

                ' Find the attribute corresponding to a counselor
                Dim CounselorAttribute As System.Int32 = ap.GetCounselorAttributeID()
                If CounselorAttribute > 0 Then
                    Dim Counselors As System.Data.DataTable = ap.CounselorsTable
                    If Counselors IsNot Nothing Then

                        ' Process the list of people. Reject inactive items.
                        For Each CounselorRow As System.Data.DataRow In Counselors.Rows
                            If Nulls.DBool(CounselorRow("ActiveFlag")) Then
                                Dim CounselorID As System.Int32 = DebtPlus.Utils.Nulls.DInt(CounselorRow("counselor"))

                                ' From the list, take only the ones marked COUNSELOR
                                If ap.CounselorHasAttribute(CounselorID, CounselorAttribute) Then
                                    Dim NewItem As New MySortedCheckedListboxControlItem(CounselorID)

                                    ' Include the counselor name
                                    Dim NameID As System.Int32 = DebtPlus.Utils.Nulls.DInt(CounselorRow("NameID"))
                                    If NameID > 0 Then
                                        Dim NameRow As System.Data.DataRow = ap.GetNamesRow(NameID)
                                        If NameRow IsNot Nothing Then
                                            NewItem.Description = DebtPlus.LINQ.Name.FormatNormalName(Nothing, NameRow("first"), Nothing, NameRow("last"), Nothing)
                                        End If
                                    End If

                                    ' Determine if the item is to be checked
                                    Dim SearchItems() As System.Data.DataRow = ap.ds.Tables("appt_counselors").Select(String.Format("([appt_time]={0:f0}) AND ([counselor]={1:f0} AND ([inactive]=0))", appt_time, CounselorID), String.Empty)
                                    If SearchItems.GetUpperBound(0) >= 0 Then
                                        NewItem.CheckState = CheckState.Checked
                                    End If

                                    ' Finally, add it to the list
                                    .Items.Add(NewItem)
                                End If
                            End If
                        Next
                    End If
                End If

                ' Define the sort order
                .SortOrder = System.Windows.Forms.SortOrder.Ascending
            End With

            ' Bind the client appointments to the display grid
            With GridControl1
                Dim vue As New System.Data.DataView(ap.ds.Tables("client_appointments"), String.Format("[appt_time]={0:f0} AND [status]='P'", appt_time), String.Empty, DataViewRowState.CurrentRows)
                .DataSource = vue

                ' Count the counselor appointments that are listed in the table
                For Each drv As System.Data.DataRowView In vue
                    Dim CounselorID As Int32 = DebtPlus.Utils.Nulls.DInt(drv("counselor"), -1)
                    If CounselorID > 0 Then
                        For Each item As MySortedCheckedListboxControlItem In counselors.Items
                            If CounselorID.CompareTo(item.Value) = 0 Then
                                item.ItemCount += 1
                                Exit For
                            End If
                        Next item
                    End If
                Next drv
                .RefreshDataSource()
            End With

            ' Move the line to the first item shown in the list
            If counselors.ItemCount > 0 Then counselors.SelectedIndex = 0

            ' Refresh the counselor list
            counselors.Refresh()

            ' Find the appointment starting time and date
            start_time.DataBindings.Add("EditValue", drv, "start_time")

            ' Find the office for this appointment. This is display only at this point.
            Dim office As System.Int32
            If drv("office") IsNot Nothing AndAlso drv("office") IsNot System.DBNull.Value Then
                office = Convert.ToInt32(drv("office"))
            Else
                office = -1
            End If
            office_name.Text = String.Empty

            Dim tbl As System.Data.DataTable
            If office > 0 Then
                tbl = ap.OfficesTable()
                If tbl IsNot Nothing Then
                    Dim row As System.Data.DataRow = tbl.Rows.Find(office)
                    If row IsNot Nothing Then
                        office_name.Text = Convert.ToString(row("name")).Trim()
                    End If
                End If
            End If

            ' Find the table of appointment types
            ' If there is no "All" item then add it.
            tbl = ap.ApptTypesTable()
            If tbl.Rows.Find(-1) Is Nothing Then
                Dim row As System.Data.DataRow = tbl.NewRow
                row.BeginEdit()
                row("appt_type") = -1
                row("appt_name") = "All"
                row("appt_duration") = 60
                row.EndEdit()
                tbl.Rows.Add(row)
                tbl.AcceptChanges()
            End If

            With appt_type
                With .Properties
                    .DataSource = tbl.DefaultView
                    .DisplayMember = "appt_name"
                    .ValueMember = "appt_type"
                    .PopupWidth = appt_type.Width + 120
                End With
                .EditValue = drv("appt_type")
                .DataBindings.Add("EditValue", drv, "appt_type")
            End With

            ' Do the best that we can to fit the data in the columns.
            GridView1.BestFitColumns()

            ' Add the handler to process a change in the counselor list
            AddHandler counselors.ItemCheck, AddressOf counselors_ItemCheck
        End Sub

        ''' <summary>
        ''' Set the checked status for the counselors
        ''' </summary>
        Private Sub counselors_ItemCheck(ByVal sender As Object, ByVal e As DevExpress.XtraEditors.Controls.ItemCheckEventArgs)
            Dim item As MySortedCheckedListboxControlItem = CType(CType(sender, DevExpress.XtraEditors.CheckedListBoxControl).Items(e.Index), MySortedCheckedListboxControlItem)
            Dim CounselorID As System.Int32 = DebtPlus.Utils.Nulls.DInt(item.Value)
            Dim Status As Boolean = (e.State = System.Windows.Forms.CheckState.Unchecked)

            ' Attempt to find the counselor row
            Dim tbl As System.Data.DataTable = ap.ds.Tables("appt_counselors")
            If tbl Is Nothing Then Return
            Dim rows() As System.Data.DataRow = tbl.Select(String.Format("([appt_time]={0:f0}) AND ([counselor]={1:f0})", appt_time, CounselorID), String.Empty)
            Dim row As System.Data.DataRow
            If rows.GetUpperBound(0) >= 0 Then
                row = rows(0)
            Else
                row = Nothing
            End If

            ' If there are pending appointments for this counselor then we have problems
            If Not Status Then
                If row Is Nothing Then
                    row = tbl.NewRow
                    row("counselor") = CounselorID
                    row("appt_time") = appt_time
                    row("inactive") = 0
                    tbl.Rows.Add(row)
                End If
            End If

            ' There should be a row at this point. If not then it means that there was not one
            ' and we un-checked the item. This is a normal (but unexpected) item.
            If row Is Nothing Then Return
            row.BeginEdit()

            ' Handle the case where the item is not being checked.
            If Status Then
                Dim vue As New System.Data.DataView(ap.ds.Tables("client_appointments"), String.Format("[appt_time]={0:f0} AND [counselor]={1:f0}", appt_time, CounselorID), String.Empty, DataViewRowState.CurrentRows)

                ' If there is an appointment then ask if the user wants it cancelled.
                If vue.Count > 0 Then
                    If DebtPlus.Data.Forms.MessageBox.Show(String.Format(My.Resources.CautionTheCounselorHasPendingAppointments, Convert.ToInt32(vue.Count)), My.Resources.AreYouSure, MessageBoxButtons.YesNo, MessageBoxIcon.Hand, MessageBoxDefaultButton.Button2) = System.Windows.Forms.DialogResult.No Then
                        CType(sender, DevExpress.XtraEditors.CheckedListBoxControl).Items(e.Index).CheckState = CheckState.Checked
                        row.EndEdit()
                        Return
                    End If

                    ' Cancel the appointments for the clients. Ok. We asked....
                    For Each AptDrv As System.Data.DataRowView In vue
                        AptDrv.Delete()
                    Next
                End If

                ' Ask for the reason that we are cancelling the counselor
                Dim Reason As String = String.Empty
                If DebtPlus.Data.Forms.InputBox.Show(My.Resources.WhyIsTheCounselorBeingRemovedFromThisSchedule, Reason, My.Resources.InactiveStatusRason, String.Empty, 50) = System.Windows.Forms.DialogResult.OK Then
                    row("inactive_reason") = Reason
                End If
            End If

            row("inactive") = Status
            row.EndEdit()
            If row.RowState = DataRowState.Unchanged Then
                row.SetModified()
            End If
        End Sub

        Private Sub appt_type_EditValueChanged(ByVal sender As Object, ByVal e As System.EventArgs)

            ' Obtain the current value of the control
            Dim val As Object = appt_type.EditValue

            ' Change the value to null if indicated
            If val IsNot Nothing AndAlso val IsNot System.DBNull.Value Then
                If Convert.ToInt32(val) < 0 Then appt_type.EditValue = System.DBNull.Value
            End If
        End Sub

        Private Sub counselors_DrawItem(ByVal sender As Object, ByVal e As DevExpress.XtraEditors.ListBoxDrawItemEventArgs)

            ' Change the background color on the item when needed
            If e.Index >= 0 AndAlso e.Index < counselors.Items.Count Then
                If CType(counselors.Items(e.Index), MySortedCheckedListboxControlItem).ItemCount > 0 Then
                    e.Appearance.BackColor = Color.PeachPuff
                End If
            End If

        End Sub

#Region "MySortedCheckedListboxControlItem"
        Private Class MySortedCheckedListboxControlItem
            Inherits DebtPlus.Data.Controls.SortedCheckedListboxControlItem

            Private privateItemCount As Int32
            Public Property ItemCount() As Int32
                Get
                    Return privateItemCount
                End Get
                Set(ByVal value As Int32)
                    privateItemCount = value
                End Set
            End Property
#Region " New "
            Public Sub New()
                MyBase.New()
            End Sub

            Public Sub New(ByVal value As Object)
                MyBase.New(value)
            End Sub

            Public Sub New(ByVal value As Object, ByVal isChecked As Boolean)
                MyBase.New(value, isChecked)
            End Sub

            Public Sub New(ByVal value As Object, ByVal description As String)
                MyBase.New(value, description)
            End Sub

            Public Sub New(ByVal value As Object, ByVal checkState As CheckState)
                MyBase.New(value, checkState)
            End Sub

            Public Sub New(ByVal value As Object, ByVal description As String, ByVal checkState As CheckState)
                MyBase.New(value, description, checkState)
            End Sub

            Public Sub New(ByVal value As Object, ByVal checkState As CheckState, ByVal enabled As Boolean)
                MyBase.New(value, checkState, enabled)
            End Sub

            Public Sub New(ByVal value As Object, ByVal description As String, ByVal checkState As CheckState, ByVal enabled As Boolean)
                MyBase.New(value, description, checkState, enabled)
            End Sub
#End Region
            Public Overrides Function ToString() As String
                If ItemCount > 0 Then
                    Return String.Format("{0} ({1:f0})", Description, ItemCount)
                Else
                    Return Description
                End If
            End Function
        End Class
#End Region
    End Class
End Namespace
