﻿Namespace Appointments.Update
    <Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
    Partial Class Form_RemoveCounselor
        Inherits DebtPlus.Data.Forms.DebtPlusForm

        'Form overrides dispose to clean up the component list.
        <System.Diagnostics.DebuggerNonUserCode()> _
        Protected Overrides Sub Dispose(ByVal disposing As Boolean)
            Try
                If disposing AndAlso components IsNot Nothing Then
                    components.Dispose()
                End If
            Finally
                MyBase.Dispose(disposing)
            End Try
        End Sub

        'Required by the Windows Form Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Windows Form Designer
        'It can be modified using the Windows Form Designer.  
        'Do not modify it using the code editor.
        <System.Diagnostics.DebuggerStepThrough()> _
        Private Sub InitializeComponent()
            Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl
            Me.LayoutControl1 = New DevExpress.XtraLayout.LayoutControl
            Me.DateNavigator1 = New DevExpress.XtraScheduler.DateNavigator
            Me.TextEdit_reason = New DevExpress.XtraEditors.TextEdit
            Me.SimpleButton_Cancel = New DevExpress.XtraEditors.SimpleButton
            Me.SimpleButton_OK = New DevExpress.XtraEditors.SimpleButton
            Me.CheckedListBoxControl1 = New DevExpress.XtraEditors.CheckedListBoxControl
            Me.LayoutControlGroup1 = New DevExpress.XtraLayout.LayoutControlGroup
            Me.LayoutControlItem1 = New DevExpress.XtraLayout.LayoutControlItem
            Me.LayoutControlItem2 = New DevExpress.XtraLayout.LayoutControlItem
            Me.LayoutControlItem3 = New DevExpress.XtraLayout.LayoutControlItem
            Me.LayoutControlItem4 = New DevExpress.XtraLayout.LayoutControlItem
            Me.LayoutControlItem5 = New DevExpress.XtraLayout.LayoutControlItem
            Me.LayoutControlItem6 = New DevExpress.XtraLayout.LayoutControlItem
            Me.EmptySpaceItem1 = New DevExpress.XtraLayout.EmptySpaceItem
            Me.EmptySpaceItem2 = New DevExpress.XtraLayout.EmptySpaceItem
            CType(Me.DxErrorProvider1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControl1, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.LayoutControl1.SuspendLayout()
            CType(Me.DateNavigator1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.TextEdit_reason.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.CheckedListBoxControl1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlGroup1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem2, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem3, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem4, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem5, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem6, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.EmptySpaceItem1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.EmptySpaceItem2, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.SuspendLayout()
            '
            'LabelControl1
            '
            Me.LabelControl1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                                             Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.LabelControl1.Appearance.Options.UseTextOptions = True
            Me.LabelControl1.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
            Me.LabelControl1.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Top
            Me.LabelControl1.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap
            Me.LabelControl1.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
            Me.LabelControl1.Location = New System.Drawing.Point(20, 20)
            Me.LabelControl1.Name = "LabelControl1"
            Me.LabelControl1.Size = New System.Drawing.Size(351, 13)
            Me.LabelControl1.StyleController = Me.LayoutControl1
            Me.LabelControl1.TabIndex = 0
            Me.LabelControl1.Text = "Select the date range and the counselor(s) that you wish to remove from the appoi" & _
                                    "ntment system."
            '
            'LayoutControl1
            '
            Me.LayoutControl1.Controls.Add(Me.DateNavigator1)
            Me.LayoutControl1.Controls.Add(Me.TextEdit_reason)
            Me.LayoutControl1.Controls.Add(Me.SimpleButton_Cancel)
            Me.LayoutControl1.Controls.Add(Me.SimpleButton_OK)
            Me.LayoutControl1.Controls.Add(Me.CheckedListBoxControl1)
            Me.LayoutControl1.Controls.Add(Me.LabelControl1)
            Me.LayoutControl1.Dock = System.Windows.Forms.DockStyle.Fill
            Me.LayoutControl1.Location = New System.Drawing.Point(0, 0)
            Me.LayoutControl1.Name = "LayoutControl1"
            Me.LayoutControl1.Root = Me.LayoutControlGroup1
            Me.LayoutControl1.Size = New System.Drawing.Size(391, 300)
            Me.LayoutControl1.TabIndex = 2
            Me.LayoutControl1.Text = "LayoutControl1"
            '
            'DateNavigator1
            '
            Me.DateNavigator1.DateTime = New Date(2009, 12, 4, 0, 0, 0, 0)
            Me.DateNavigator1.Location = New System.Drawing.Point(12, 45)
            Me.DateNavigator1.Name = "DateNavigator1"
            Me.DateNavigator1.Size = New System.Drawing.Size(237, 190)
            Me.DateNavigator1.TabIndex = 7
            '
            'TextEdit_reason
            '
            Me.TextEdit_reason.Location = New System.Drawing.Point(56, 239)
            Me.TextEdit_reason.Name = "TextEdit_reason"
            Me.TextEdit_reason.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[False]
            Me.TextEdit_reason.Properties.MaxLength = 50
            Me.TextEdit_reason.Size = New System.Drawing.Size(323, 20)
            Me.TextEdit_reason.StyleController = Me.LayoutControl1
            Me.TextEdit_reason.TabIndex = 6
            '
            'SimpleButton_Cancel
            '
            Me.SimpleButton_Cancel.CausesValidation = False
            Me.SimpleButton_Cancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
            Me.SimpleButton_Cancel.Location = New System.Drawing.Point(214, 263)
            Me.SimpleButton_Cancel.MaximumSize = New System.Drawing.Size(75, 25)
            Me.SimpleButton_Cancel.MinimumSize = New System.Drawing.Size(75, 25)
            Me.SimpleButton_Cancel.Name = "SimpleButton_Cancel"
            Me.SimpleButton_Cancel.Size = New System.Drawing.Size(75, 25)
            Me.SimpleButton_Cancel.StyleController = Me.LayoutControl1
            Me.SimpleButton_Cancel.TabIndex = 5
            Me.SimpleButton_Cancel.Text = "&Cancel"
            '
            'SimpleButton_OK
            '
            Me.SimpleButton_OK.DialogResult = System.Windows.Forms.DialogResult.OK
            Me.SimpleButton_OK.Enabled = False
            Me.SimpleButton_OK.Location = New System.Drawing.Point(115, 263)
            Me.SimpleButton_OK.MaximumSize = New System.Drawing.Size(75, 25)
            Me.SimpleButton_OK.MinimumSize = New System.Drawing.Size(75, 25)
            Me.SimpleButton_OK.Name = "SimpleButton_OK"
            Me.SimpleButton_OK.Size = New System.Drawing.Size(75, 25)
            Me.SimpleButton_OK.StyleController = Me.LayoutControl1
            Me.SimpleButton_OK.TabIndex = 4
            Me.SimpleButton_OK.Text = "&OK"
            '
            'CheckedListBoxControl1
            '
            Me.CheckedListBoxControl1.Location = New System.Drawing.Point(253, 63)
            Me.CheckedListBoxControl1.Name = "CheckedListBoxControl1"
            Me.CheckedListBoxControl1.Size = New System.Drawing.Size(126, 172)
            Me.CheckedListBoxControl1.StyleController = Me.LayoutControl1
            Me.CheckedListBoxControl1.TabIndex = 8
            '
            'LayoutControlGroup1
            '
            Me.LayoutControlGroup1.CustomizationFormText = "LayoutControlGroup1"
            Me.LayoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.[True]
            Me.LayoutControlGroup1.GroupBordersVisible = False
            Me.LayoutControlGroup1.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.LayoutControlItem1, Me.LayoutControlItem2, Me.LayoutControlItem3, Me.LayoutControlItem4, Me.LayoutControlItem5, Me.LayoutControlItem6, Me.EmptySpaceItem1, Me.EmptySpaceItem2})
            Me.LayoutControlGroup1.Location = New System.Drawing.Point(0, 0)
            Me.LayoutControlGroup1.Name = "LayoutControlGroup1"
            Me.LayoutControlGroup1.Size = New System.Drawing.Size(391, 300)
            Me.LayoutControlGroup1.Spacing = New DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0)
            Me.LayoutControlGroup1.Text = "LayoutControlGroup1"
            Me.LayoutControlGroup1.TextVisible = False
            '
            'LayoutControlItem1
            '
            Me.LayoutControlItem1.Control = Me.LabelControl1
            Me.LayoutControlItem1.CustomizationFormText = "Selection Message"
            Me.LayoutControlItem1.Location = New System.Drawing.Point(0, 0)
            Me.LayoutControlItem1.Name = "LayoutControlItem1"
            Me.LayoutControlItem1.Padding = New DevExpress.XtraLayout.Utils.Padding(10, 10, 10, 10)
            Me.LayoutControlItem1.Size = New System.Drawing.Size(371, 33)
            Me.LayoutControlItem1.Text = "Selection Message"
            Me.LayoutControlItem1.TextSize = New System.Drawing.Size(0, 0)
            Me.LayoutControlItem1.TextToControlDistance = 0
            Me.LayoutControlItem1.TextVisible = False
            '
            'LayoutControlItem2
            '
            Me.LayoutControlItem2.Control = Me.SimpleButton_OK
            Me.LayoutControlItem2.CustomizationFormText = "OK Button"
            Me.LayoutControlItem2.Location = New System.Drawing.Point(93, 251)
            Me.LayoutControlItem2.Name = "LayoutControlItem2"
            Me.LayoutControlItem2.Size = New System.Drawing.Size(99, 29)
            Me.LayoutControlItem2.Spacing = New DevExpress.XtraLayout.Utils.Padding(10, 10, 0, 0)
            Me.LayoutControlItem2.Text = "OK Button"
            Me.LayoutControlItem2.TextSize = New System.Drawing.Size(0, 0)
            Me.LayoutControlItem2.TextToControlDistance = 0
            Me.LayoutControlItem2.TextVisible = False
            '
            'LayoutControlItem3
            '
            Me.LayoutControlItem3.Control = Me.SimpleButton_Cancel
            Me.LayoutControlItem3.CustomizationFormText = "CANCEL button"
            Me.LayoutControlItem3.Location = New System.Drawing.Point(192, 251)
            Me.LayoutControlItem3.Name = "LayoutControlItem3"
            Me.LayoutControlItem3.Size = New System.Drawing.Size(99, 29)
            Me.LayoutControlItem3.Spacing = New DevExpress.XtraLayout.Utils.Padding(10, 10, 0, 0)
            Me.LayoutControlItem3.Text = "CANCEL button"
            Me.LayoutControlItem3.TextSize = New System.Drawing.Size(0, 0)
            Me.LayoutControlItem3.TextToControlDistance = 0
            Me.LayoutControlItem3.TextVisible = False
            '
            'LayoutControlItem4
            '
            Me.LayoutControlItem4.Control = Me.TextEdit_reason
            Me.LayoutControlItem4.CustomizationFormText = "Reason:"
            Me.LayoutControlItem4.Location = New System.Drawing.Point(0, 227)
            Me.LayoutControlItem4.Name = "LayoutControlItem4"
            Me.LayoutControlItem4.Size = New System.Drawing.Size(371, 24)
            Me.LayoutControlItem4.Text = "Reason:"
            Me.LayoutControlItem4.TextSize = New System.Drawing.Size(40, 13)
            '
            'LayoutControlItem5
            '
            Me.LayoutControlItem5.Control = Me.DateNavigator1
            Me.LayoutControlItem5.CustomizationFormText = "Date Range"
            Me.LayoutControlItem5.Location = New System.Drawing.Point(0, 33)
            Me.LayoutControlItem5.Name = "LayoutControlItem5"
            Me.LayoutControlItem5.Size = New System.Drawing.Size(241, 194)
            Me.LayoutControlItem5.Text = "Date Range"
            Me.LayoutControlItem5.TextSize = New System.Drawing.Size(0, 0)
            Me.LayoutControlItem5.TextToControlDistance = 0
            Me.LayoutControlItem5.TextVisible = False
            '
            'LayoutControlItem6
            '
            Me.LayoutControlItem6.AppearanceItemCaption.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
            Me.LayoutControlItem6.AppearanceItemCaption.Options.UseFont = True
            Me.LayoutControlItem6.Control = Me.CheckedListBoxControl1
            Me.LayoutControlItem6.ControlAlignment = System.Drawing.ContentAlignment.MiddleCenter
            Me.LayoutControlItem6.CustomizationFormText = "Counselor List"
            Me.LayoutControlItem6.Location = New System.Drawing.Point(241, 33)
            Me.LayoutControlItem6.Name = "LayoutControlItem6"
            Me.LayoutControlItem6.Size = New System.Drawing.Size(130, 194)
            Me.LayoutControlItem6.Text = "Counselor List"
            Me.LayoutControlItem6.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize
            Me.LayoutControlItem6.TextLocation = DevExpress.Utils.Locations.Top
            Me.LayoutControlItem6.TextSize = New System.Drawing.Size(79, 13)
            Me.LayoutControlItem6.TextToControlDistance = 5
            '
            'EmptySpaceItem1
            '
            Me.EmptySpaceItem1.CustomizationFormText = "EmptySpaceItem1"
            Me.EmptySpaceItem1.Location = New System.Drawing.Point(291, 251)
            Me.EmptySpaceItem1.Name = "EmptySpaceItem1"
            Me.EmptySpaceItem1.Size = New System.Drawing.Size(80, 29)
            Me.EmptySpaceItem1.Text = "EmptySpaceItem1"
            Me.EmptySpaceItem1.TextSize = New System.Drawing.Size(0, 0)
            '
            'EmptySpaceItem2
            '
            Me.EmptySpaceItem2.CustomizationFormText = "EmptySpaceItem2"
            Me.EmptySpaceItem2.Location = New System.Drawing.Point(0, 251)
            Me.EmptySpaceItem2.Name = "EmptySpaceItem2"
            Me.EmptySpaceItem2.Size = New System.Drawing.Size(93, 29)
            Me.EmptySpaceItem2.Text = "EmptySpaceItem2"
            Me.EmptySpaceItem2.TextSize = New System.Drawing.Size(0, 0)
            '
            'Form_RemoveCounselor
            '
            Me.AcceptButton = Me.SimpleButton_OK
            Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
            Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
            Me.CancelButton = Me.SimpleButton_Cancel
            Me.ClientSize = New System.Drawing.Size(391, 300)
            Me.Controls.Add(Me.LayoutControl1)
            Me.Name = "Form_RemoveCounselor"
            Me.Text = "Remove Counselor(s) from Appointments on Date(s)"
            CType(Me.DxErrorProvider1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControl1, System.ComponentModel.ISupportInitialize).EndInit()
            Me.LayoutControl1.ResumeLayout(False)
            CType(Me.DateNavigator1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.TextEdit_reason.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.CheckedListBoxControl1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlGroup1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem2, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem3, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem4, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem5, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem6, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.EmptySpaceItem1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.EmptySpaceItem2, System.ComponentModel.ISupportInitialize).EndInit()
            Me.ResumeLayout(False)

        End Sub
        Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents LayoutControl1 As DevExpress.XtraLayout.LayoutControl
        Friend WithEvents SimpleButton_Cancel As DevExpress.XtraEditors.SimpleButton
        Friend WithEvents SimpleButton_OK As DevExpress.XtraEditors.SimpleButton
        Friend WithEvents LayoutControlGroup1 As DevExpress.XtraLayout.LayoutControlGroup
        Friend WithEvents LayoutControlItem1 As DevExpress.XtraLayout.LayoutControlItem
        Friend WithEvents LayoutControlItem2 As DevExpress.XtraLayout.LayoutControlItem
        Friend WithEvents LayoutControlItem3 As DevExpress.XtraLayout.LayoutControlItem
        Friend WithEvents TextEdit_reason As DevExpress.XtraEditors.TextEdit
        Friend WithEvents LayoutControlItem4 As DevExpress.XtraLayout.LayoutControlItem
        Friend WithEvents DateNavigator1 As DevExpress.XtraScheduler.DateNavigator
        Friend WithEvents CheckedListBoxControl1 As DevExpress.XtraEditors.CheckedListBoxControl
        Friend WithEvents LayoutControlItem5 As DevExpress.XtraLayout.LayoutControlItem
        Friend WithEvents LayoutControlItem6 As DevExpress.XtraLayout.LayoutControlItem
        Friend WithEvents EmptySpaceItem1 As DevExpress.XtraLayout.EmptySpaceItem
        Friend WithEvents EmptySpaceItem2 As DevExpress.XtraLayout.EmptySpaceItem
    End Class
End Namespace