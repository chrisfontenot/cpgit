#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}
#End Region

Option Compare Binary
Option Explicit On
Option Strict On

Imports DebtPlus.Utils.Format
Imports System.Windows.Forms

Namespace Appointments.Update

    Friend Class Form_Edit
        Inherits DebtPlus.Data.Forms.DebtPlusForm
        Private Const FORM_NAME As String = "Appointments.Update.Edit"

        Private ap As ArgParser

        Public Sub New()
            MyBase.New()
            InitializeComponent()
        End Sub

        Public Sub New(ByVal ap As ArgParser)
            MyClass.New()
            Me.ap = ap
            AddHandler Me.FormClosing, AddressOf Form_Closing
            AddHandler Me.Load, AddressOf Form_Load
            AddHandler GridView1.DoubleClick, AddressOf GridView1_DoubleClick
            AddHandler ContextMenu1.Popup, AddressOf ContextMenu1_Popup
            AddHandler ContextMenu1_Create.Click, AddressOf ContextMenu1_Create_Click
            AddHandler ContextMenu1_Delete.Click, AddressOf ContextMenu1_Delete_Click
            AddHandler BarButtonItem2.ItemClick, AddressOf BarButtonItem2_ItemClick
        End Sub

#Region " Windows Form Designer generated code "

        'Form overrides dispose to clean up the component list.
        Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
            If disposing Then
                If components IsNot Nothing Then
                    components.Dispose()
                End If
            End If
            MyBase.Dispose(disposing)
        End Sub

        'Required by the Windows Form Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Windows Form Designer
        'It can be modified using the Windows Form Designer.  
        'Do not modify it using the code editor.
        Friend WithEvents MonthCalendar1 As MonthCalendar
        Friend WithEvents LookUpEdit_office As DevExpress.XtraEditors.LookUpEdit
        Friend WithEvents ContextMenu1_Delete As MenuItem
        Friend WithEvents ContextMenu1_Edit As MenuItem
        Friend WithEvents ContextMenu1_Create As MenuItem
        Friend WithEvents GridView1 As DevExpress.XtraGrid.Views.Grid.GridView
        Friend WithEvents GridColumn_appt_time As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents GridColumn_type As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents GridColumn_start_time As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents GridColumn_item_count As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents GridColumn_booked_count As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents GridColumn_formatted_appt_type As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents GridColumn_counselors As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents GridControl1 As DevExpress.XtraGrid.GridControl
        Friend WithEvents barManager1 As DevExpress.XtraBars.BarManager
        Friend WithEvents Bar2 As DevExpress.XtraBars.Bar
        Friend WithEvents BarSubItem1 As DevExpress.XtraBars.BarSubItem
        Friend WithEvents barDockControlTop As DevExpress.XtraBars.BarDockControl
        Friend WithEvents barDockControlBottom As DevExpress.XtraBars.BarDockControl
        Friend WithEvents barDockControlLeft As DevExpress.XtraBars.BarDockControl
        Friend WithEvents barDockControlRight As DevExpress.XtraBars.BarDockControl
        Friend WithEvents BarButtonItem1 As DevExpress.XtraBars.BarButtonItem
        Friend WithEvents BarSubItem2 As DevExpress.XtraBars.BarSubItem
        Friend WithEvents BarButtonItem2 As DevExpress.XtraBars.BarButtonItem
        Friend WithEvents LayoutControl1 As DevExpress.XtraLayout.LayoutControl
        Friend WithEvents LayoutControlGroup1 As DevExpress.XtraLayout.LayoutControlGroup
        Friend WithEvents LayoutControlItem1 As DevExpress.XtraLayout.LayoutControlItem
        Friend WithEvents LayoutControlItem2 As DevExpress.XtraLayout.LayoutControlItem
        Friend WithEvents LayoutControlItem3 As DevExpress.XtraLayout.LayoutControlItem
        Friend WithEvents EmptySpaceItem2 As DevExpress.XtraLayout.EmptySpaceItem
        Friend WithEvents EmptySpaceItem1 As DevExpress.XtraLayout.EmptySpaceItem
        Friend WithEvents ContextMenu1 As ContextMenu
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
            Me.components = New System.ComponentModel.Container
            Dim StyleFormatCondition1 As DevExpress.XtraGrid.StyleFormatCondition = New DevExpress.XtraGrid.StyleFormatCondition
            Me.GridColumn_booked_count = New DevExpress.XtraGrid.Columns.GridColumn
            Me.ContextMenu1_Create = New System.Windows.Forms.MenuItem
            Me.ContextMenu1_Edit = New System.Windows.Forms.MenuItem
            Me.ContextMenu1_Delete = New System.Windows.Forms.MenuItem
            Me.MonthCalendar1 = New System.Windows.Forms.MonthCalendar
            Me.LookUpEdit_office = New DevExpress.XtraEditors.LookUpEdit
            Me.LayoutControl1 = New DevExpress.XtraLayout.LayoutControl
            Me.GridControl1 = New DevExpress.XtraGrid.GridControl
            Me.ContextMenu1 = New System.Windows.Forms.ContextMenu
            Me.GridView1 = New DevExpress.XtraGrid.Views.Grid.GridView
            Me.GridColumn_appt_time = New DevExpress.XtraGrid.Columns.GridColumn
            Me.GridColumn_type = New DevExpress.XtraGrid.Columns.GridColumn
            Me.GridColumn_start_time = New DevExpress.XtraGrid.Columns.GridColumn
            Me.GridColumn_item_count = New DevExpress.XtraGrid.Columns.GridColumn
            Me.GridColumn_formatted_appt_type = New DevExpress.XtraGrid.Columns.GridColumn
            Me.GridColumn_counselors = New DevExpress.XtraGrid.Columns.GridColumn
            Me.LayoutControlGroup1 = New DevExpress.XtraLayout.LayoutControlGroup
            Me.LayoutControlItem1 = New DevExpress.XtraLayout.LayoutControlItem
            Me.LayoutControlItem2 = New DevExpress.XtraLayout.LayoutControlItem
            Me.LayoutControlItem3 = New DevExpress.XtraLayout.LayoutControlItem
            Me.EmptySpaceItem2 = New DevExpress.XtraLayout.EmptySpaceItem
            Me.EmptySpaceItem1 = New DevExpress.XtraLayout.EmptySpaceItem
            Me.barManager1 = New DevExpress.XtraBars.BarManager(Me.components)
            Me.Bar2 = New DevExpress.XtraBars.Bar
            Me.BarSubItem2 = New DevExpress.XtraBars.BarSubItem
            Me.BarButtonItem2 = New DevExpress.XtraBars.BarButtonItem
            Me.barDockControlTop = New DevExpress.XtraBars.BarDockControl
            Me.barDockControlBottom = New DevExpress.XtraBars.BarDockControl
            Me.barDockControlLeft = New DevExpress.XtraBars.BarDockControl
            Me.barDockControlRight = New DevExpress.XtraBars.BarDockControl
            Me.BarButtonItem1 = New DevExpress.XtraBars.BarButtonItem
            Me.BarSubItem1 = New DevExpress.XtraBars.BarSubItem
            CType(Me.DxErrorProvider1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LookUpEdit_office.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControl1, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.LayoutControl1.SuspendLayout()
            CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.GridView1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlGroup1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem2, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem3, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.EmptySpaceItem2, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.EmptySpaceItem1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.barManager1, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.SuspendLayout()
            '
            'GridColumn_booked_count
            '
            Me.GridColumn_booked_count.AppearanceCell.Options.UseTextOptions = True
            Me.GridColumn_booked_count.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.GridColumn_booked_count.AppearanceHeader.Options.UseTextOptions = True
            Me.GridColumn_booked_count.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.GridColumn_booked_count.Caption = "Booked"
            Me.GridColumn_booked_count.CustomizationCaption = "# Booked"
            Me.GridColumn_booked_count.DisplayFormat.FormatString = "f0"
            Me.GridColumn_booked_count.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.GridColumn_booked_count.FieldName = "booked"
            Me.GridColumn_booked_count.GroupFormat.FormatString = "f0"
            Me.GridColumn_booked_count.GroupFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.GridColumn_booked_count.Name = "GridColumn_booked_count"
            Me.GridColumn_booked_count.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.[True]
            Me.GridColumn_booked_count.Visible = True
            Me.GridColumn_booked_count.VisibleIndex = 2
            Me.GridColumn_booked_count.Width = 51
            '
            'ContextMenu1_Create
            '
            Me.ContextMenu1_Create.Index = 0
            Me.ContextMenu1_Create.Text = "&Add"
            '
            'ContextMenu1_Edit
            '
            Me.ContextMenu1_Edit.Index = 1
            Me.ContextMenu1_Edit.Text = "&Change"
            '
            'ContextMenu1_Delete
            '
            Me.ContextMenu1_Delete.Index = 2
            Me.ContextMenu1_Delete.Text = "&Delete"
            '
            'MonthCalendar1
            '
            Me.MonthCalendar1.Anchor = System.Windows.Forms.AnchorStyles.None
            Me.MonthCalendar1.AnnuallyBoldedDates = New Date() {New Date(2007, 1, 1, 0, 0, 0, 0), New Date(2007, 12, 25, 0, 0, 0, 0), New Date(2007, 7, 4, 0, 0, 0, 0)}
            Me.MonthCalendar1.Location = New System.Drawing.Point(12, 36)
            Me.MonthCalendar1.MaxSelectionCount = 1
            Me.MonthCalendar1.Name = "MonthCalendar1"
            Me.MonthCalendar1.TabIndex = 2
            Me.MonthCalendar1.TrailingForeColor = System.Drawing.Color.DarkKhaki
            '
            'LookUpEdit_office
            '
            Me.LookUpEdit_office.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                                                 Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.LookUpEdit_office.Location = New System.Drawing.Point(246, 12)
            Me.LookUpEdit_office.Name = "LookUpEdit_office"
            Me.LookUpEdit_office.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.LookUpEdit_office.Properties.Columns.AddRange(New DevExpress.XtraEditors.Controls.LookUpColumnInfo() {New DevExpress.XtraEditors.Controls.LookUpColumnInfo("office", "Office", 20, DevExpress.Utils.FormatType.Numeric, "", True, DevExpress.Utils.HorzAlignment.Far), New DevExpress.XtraEditors.Controls.LookUpColumnInfo("name", "Name", 50, DevExpress.Utils.FormatType.None, "", True, DevExpress.Utils.HorzAlignment.[Default], DevExpress.Data.ColumnSortOrder.Ascending), New DevExpress.XtraEditors.Controls.LookUpColumnInfo("formatted_office_type", "Type", 20, DevExpress.Utils.FormatType.None, "", True, DevExpress.Utils.HorzAlignment.[Default], DevExpress.Data.ColumnSortOrder.Ascending)})
            Me.LookUpEdit_office.Properties.DisplayMember = "name"
            Me.LookUpEdit_office.Properties.NullText = "...Please Choose one..."
            Me.LookUpEdit_office.Properties.ShowFooter = False
            Me.LookUpEdit_office.Properties.ValueMember = "office"
            Me.LookUpEdit_office.Size = New System.Drawing.Size(364, 20)
            Me.LookUpEdit_office.StyleController = Me.LayoutControl1
            Me.LookUpEdit_office.TabIndex = 1
            Me.LookUpEdit_office.ToolTipController = Me.ToolTipController1
            Me.LookUpEdit_office.Properties.SortColumnIndex = 1
            '
            'LayoutControl1
            '
            Me.LayoutControl1.Controls.Add(Me.LookUpEdit_office)
            Me.LayoutControl1.Controls.Add(Me.GridControl1)
            Me.LayoutControl1.Controls.Add(Me.MonthCalendar1)
            Me.LayoutControl1.Dock = System.Windows.Forms.DockStyle.Fill
            Me.LayoutControl1.Location = New System.Drawing.Point(0, 22)
            Me.LayoutControl1.Name = "LayoutControl1"
            Me.LayoutControl1.Root = Me.LayoutControlGroup1
            Me.LayoutControl1.Size = New System.Drawing.Size(622, 366)
            Me.LayoutControl1.TabIndex = 8
            Me.LayoutControl1.Text = "LayoutControl1"
            '
            'GridControl1
            '
            Me.GridControl1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                                             Or System.Windows.Forms.AnchorStyles.Left) _
                                            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.GridControl1.ContextMenu = Me.ContextMenu1
            Me.GridControl1.Location = New System.Drawing.Point(249, 36)
            Me.GridControl1.MainView = Me.GridView1
            Me.GridControl1.Name = "GridControl1"
            Me.GridControl1.Size = New System.Drawing.Size(361, 318)
            Me.GridControl1.TabIndex = 3
            Me.GridControl1.ToolTipController = Me.ToolTipController1
            Me.GridControl1.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridView1})
            '
            'ContextMenu1
            '
            Me.ContextMenu1.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.ContextMenu1_Create, Me.ContextMenu1_Edit, Me.ContextMenu1_Delete})
            '
            'GridView1
            '
            Me.GridView1.Appearance.ColumnFilterButton.BackColor = System.Drawing.Color.FromArgb(CType(CType(104, Byte), Int32), CType(CType(184, Byte), Int32), CType(CType(251, Byte), Int32))
            Me.GridView1.Appearance.ColumnFilterButton.BorderColor = System.Drawing.Color.FromArgb(CType(CType(104, Byte), Int32), CType(CType(184, Byte), Int32), CType(CType(251, Byte), Int32))
            Me.GridView1.Appearance.ColumnFilterButton.ForeColor = System.Drawing.Color.White
            Me.GridView1.Appearance.ColumnFilterButton.Options.UseBackColor = True
            Me.GridView1.Appearance.ColumnFilterButton.Options.UseBorderColor = True
            Me.GridView1.Appearance.ColumnFilterButton.Options.UseForeColor = True
            Me.GridView1.Appearance.ColumnFilterButtonActive.BackColor = System.Drawing.Color.FromArgb(CType(CType(170, Byte), Int32), CType(CType(216, Byte), Int32), CType(CType(254, Byte), Int32))
            Me.GridView1.Appearance.ColumnFilterButtonActive.BorderColor = System.Drawing.Color.FromArgb(CType(CType(170, Byte), Int32), CType(CType(216, Byte), Int32), CType(CType(254, Byte), Int32))
            Me.GridView1.Appearance.ColumnFilterButtonActive.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.ColumnFilterButtonActive.Options.UseBackColor = True
            Me.GridView1.Appearance.ColumnFilterButtonActive.Options.UseBorderColor = True
            Me.GridView1.Appearance.ColumnFilterButtonActive.Options.UseForeColor = True
            Me.GridView1.Appearance.Empty.BackColor = System.Drawing.Color.FromArgb(CType(CType(236, Byte), Int32), CType(CType(246, Byte), Int32), CType(CType(255, Byte), Int32))
            Me.GridView1.Appearance.Empty.BackColor2 = System.Drawing.Color.White
            Me.GridView1.Appearance.Empty.Options.UseBackColor = True
            Me.GridView1.Appearance.EvenRow.BackColor = System.Drawing.Color.FromArgb(CType(CType(247, Byte), Int32), CType(CType(251, Byte), Int32), CType(CType(255, Byte), Int32))
            Me.GridView1.Appearance.EvenRow.BorderColor = System.Drawing.Color.FromArgb(CType(CType(247, Byte), Int32), CType(CType(251, Byte), Int32), CType(CType(255, Byte), Int32))
            Me.GridView1.Appearance.EvenRow.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.EvenRow.Options.UseBackColor = True
            Me.GridView1.Appearance.EvenRow.Options.UseBorderColor = True
            Me.GridView1.Appearance.EvenRow.Options.UseForeColor = True
            Me.GridView1.Appearance.FilterCloseButton.BackColor = System.Drawing.Color.FromArgb(CType(CType(104, Byte), Int32), CType(CType(184, Byte), Int32), CType(CType(251, Byte), Int32))
            Me.GridView1.Appearance.FilterCloseButton.BorderColor = System.Drawing.Color.FromArgb(CType(CType(104, Byte), Int32), CType(CType(184, Byte), Int32), CType(CType(251, Byte), Int32))
            Me.GridView1.Appearance.FilterCloseButton.ForeColor = System.Drawing.Color.White
            Me.GridView1.Appearance.FilterCloseButton.Options.UseBackColor = True
            Me.GridView1.Appearance.FilterCloseButton.Options.UseBorderColor = True
            Me.GridView1.Appearance.FilterCloseButton.Options.UseForeColor = True
            Me.GridView1.Appearance.FilterPanel.BackColor = System.Drawing.Color.FromArgb(CType(CType(236, Byte), Int32), CType(CType(246, Byte), Int32), CType(CType(255, Byte), Int32))
            Me.GridView1.Appearance.FilterPanel.BackColor2 = System.Drawing.Color.White
            Me.GridView1.Appearance.FilterPanel.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.FilterPanel.Options.UseBackColor = True
            Me.GridView1.Appearance.FilterPanel.Options.UseForeColor = True
            Me.GridView1.Appearance.FixedLine.BackColor = System.Drawing.Color.FromArgb(CType(CType(59, Byte), Int32), CType(CType(133, Byte), Int32), CType(CType(195, Byte), Int32))
            Me.GridView1.Appearance.FixedLine.Options.UseBackColor = True
            Me.GridView1.Appearance.FocusedCell.BackColor = System.Drawing.Color.White
            Me.GridView1.Appearance.FocusedCell.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.FocusedCell.Options.UseBackColor = True
            Me.GridView1.Appearance.FocusedCell.Options.UseForeColor = True
            Me.GridView1.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb(CType(CType(38, Byte), Int32), CType(CType(109, Byte), Int32), CType(CType(189, Byte), Int32))
            Me.GridView1.Appearance.FocusedRow.BorderColor = System.Drawing.Color.FromArgb(CType(CType(59, Byte), Int32), CType(CType(139, Byte), Int32), CType(CType(206, Byte), Int32))
            Me.GridView1.Appearance.FocusedRow.ForeColor = System.Drawing.Color.White
            Me.GridView1.Appearance.FocusedRow.Options.UseBackColor = True
            Me.GridView1.Appearance.FocusedRow.Options.UseBorderColor = True
            Me.GridView1.Appearance.FocusedRow.Options.UseForeColor = True
            Me.GridView1.Appearance.FooterPanel.BackColor = System.Drawing.Color.FromArgb(CType(CType(104, Byte), Int32), CType(CType(184, Byte), Int32), CType(CType(251, Byte), Int32))
            Me.GridView1.Appearance.FooterPanel.BorderColor = System.Drawing.Color.FromArgb(CType(CType(104, Byte), Int32), CType(CType(184, Byte), Int32), CType(CType(251, Byte), Int32))
            Me.GridView1.Appearance.FooterPanel.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.FooterPanel.Options.UseBackColor = True
            Me.GridView1.Appearance.FooterPanel.Options.UseBorderColor = True
            Me.GridView1.Appearance.FooterPanel.Options.UseForeColor = True
            Me.GridView1.Appearance.GroupButton.BackColor = System.Drawing.Color.FromArgb(CType(CType(104, Byte), Int32), CType(CType(184, Byte), Int32), CType(CType(251, Byte), Int32))
            Me.GridView1.Appearance.GroupButton.BorderColor = System.Drawing.Color.FromArgb(CType(CType(104, Byte), Int32), CType(CType(184, Byte), Int32), CType(CType(251, Byte), Int32))
            Me.GridView1.Appearance.GroupButton.Options.UseBackColor = True
            Me.GridView1.Appearance.GroupButton.Options.UseBorderColor = True
            Me.GridView1.Appearance.GroupFooter.BackColor = System.Drawing.Color.FromArgb(CType(CType(170, Byte), Int32), CType(CType(216, Byte), Int32), CType(CType(254, Byte), Int32))
            Me.GridView1.Appearance.GroupFooter.BorderColor = System.Drawing.Color.FromArgb(CType(CType(170, Byte), Int32), CType(CType(216, Byte), Int32), CType(CType(254, Byte), Int32))
            Me.GridView1.Appearance.GroupFooter.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.GroupFooter.Options.UseBackColor = True
            Me.GridView1.Appearance.GroupFooter.Options.UseBorderColor = True
            Me.GridView1.Appearance.GroupFooter.Options.UseForeColor = True
            Me.GridView1.Appearance.GroupPanel.BackColor = System.Drawing.Color.FromArgb(CType(CType(236, Byte), Int32), CType(CType(246, Byte), Int32), CType(CType(255, Byte), Int32))
            Me.GridView1.Appearance.GroupPanel.BackColor2 = System.Drawing.Color.White
            Me.GridView1.Appearance.GroupPanel.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.GroupPanel.Options.UseBackColor = True
            Me.GridView1.Appearance.GroupPanel.Options.UseForeColor = True
            Me.GridView1.Appearance.GroupRow.BackColor = System.Drawing.Color.FromArgb(CType(CType(170, Byte), Int32), CType(CType(216, Byte), Int32), CType(CType(254, Byte), Int32))
            Me.GridView1.Appearance.GroupRow.BorderColor = System.Drawing.Color.FromArgb(CType(CType(170, Byte), Int32), CType(CType(216, Byte), Int32), CType(CType(254, Byte), Int32))
            Me.GridView1.Appearance.GroupRow.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.GroupRow.Options.UseBackColor = True
            Me.GridView1.Appearance.GroupRow.Options.UseBorderColor = True
            Me.GridView1.Appearance.GroupRow.Options.UseForeColor = True
            Me.GridView1.Appearance.HeaderPanel.BackColor = System.Drawing.Color.FromArgb(CType(CType(139, Byte), Int32), CType(CType(201, Byte), Int32), CType(CType(254, Byte), Int32))
            Me.GridView1.Appearance.HeaderPanel.BorderColor = System.Drawing.Color.FromArgb(CType(CType(139, Byte), Int32), CType(CType(201, Byte), Int32), CType(CType(254, Byte), Int32))
            Me.GridView1.Appearance.HeaderPanel.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.HeaderPanel.Options.UseBackColor = True
            Me.GridView1.Appearance.HeaderPanel.Options.UseBorderColor = True
            Me.GridView1.Appearance.HeaderPanel.Options.UseForeColor = True
            Me.GridView1.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Int32), CType(CType(170, Byte), Int32), CType(CType(225, Byte), Int32))
            Me.GridView1.Appearance.HideSelectionRow.BorderColor = System.Drawing.Color.FromArgb(CType(CType(83, Byte), Int32), CType(CType(155, Byte), Int32), CType(CType(215, Byte), Int32))
            Me.GridView1.Appearance.HideSelectionRow.ForeColor = System.Drawing.Color.FromArgb(CType(CType(236, Byte), Int32), CType(CType(246, Byte), Int32), CType(CType(255, Byte), Int32))
            Me.GridView1.Appearance.HideSelectionRow.Options.UseBackColor = True
            Me.GridView1.Appearance.HideSelectionRow.Options.UseBorderColor = True
            Me.GridView1.Appearance.HideSelectionRow.Options.UseForeColor = True
            Me.GridView1.Appearance.HorzLine.BackColor = System.Drawing.Color.FromArgb(CType(CType(104, Byte), Int32), CType(CType(184, Byte), Int32), CType(CType(251, Byte), Int32))
            Me.GridView1.Appearance.HorzLine.Options.UseBackColor = True
            Me.GridView1.Appearance.OddRow.BackColor = System.Drawing.Color.FromArgb(CType(CType(236, Byte), Int32), CType(CType(246, Byte), Int32), CType(CType(255, Byte), Int32))
            Me.GridView1.Appearance.OddRow.BorderColor = System.Drawing.Color.FromArgb(CType(CType(236, Byte), Int32), CType(CType(246, Byte), Int32), CType(CType(255, Byte), Int32))
            Me.GridView1.Appearance.OddRow.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.OddRow.Options.UseBackColor = True
            Me.GridView1.Appearance.OddRow.Options.UseBorderColor = True
            Me.GridView1.Appearance.OddRow.Options.UseForeColor = True
            Me.GridView1.Appearance.Preview.Font = New System.Drawing.Font("Verdana", 7.5!)
            Me.GridView1.Appearance.Preview.ForeColor = System.Drawing.Color.FromArgb(CType(CType(83, Byte), Int32), CType(CType(155, Byte), Int32), CType(CType(215, Byte), Int32))
            Me.GridView1.Appearance.Preview.Options.UseFont = True
            Me.GridView1.Appearance.Preview.Options.UseForeColor = True
            Me.GridView1.Appearance.Row.BackColor = System.Drawing.Color.FromArgb(CType(CType(247, Byte), Int32), CType(CType(251, Byte), Int32), CType(CType(255, Byte), Int32))
            Me.GridView1.Appearance.Row.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.Row.Options.UseBackColor = True
            Me.GridView1.Appearance.Row.Options.UseForeColor = True
            Me.GridView1.Appearance.RowSeparator.BackColor = System.Drawing.Color.FromArgb(CType(CType(236, Byte), Int32), CType(CType(246, Byte), Int32), CType(CType(255, Byte), Int32))
            Me.GridView1.Appearance.RowSeparator.BackColor2 = System.Drawing.Color.White
            Me.GridView1.Appearance.RowSeparator.Options.UseBackColor = True
            Me.GridView1.Appearance.SelectedRow.BackColor = System.Drawing.Color.FromArgb(CType(CType(83, Byte), Int32), CType(CType(155, Byte), Int32), CType(CType(215, Byte), Int32))
            Me.GridView1.Appearance.SelectedRow.ForeColor = System.Drawing.Color.White
            Me.GridView1.Appearance.SelectedRow.Options.UseBackColor = True
            Me.GridView1.Appearance.SelectedRow.Options.UseForeColor = True
            Me.GridView1.Appearance.TopNewRow.BackColor = System.Drawing.Color.White
            Me.GridView1.Appearance.TopNewRow.Options.UseBackColor = True
            Me.GridView1.Appearance.VertLine.BackColor = System.Drawing.Color.FromArgb(CType(CType(104, Byte), Int32), CType(CType(184, Byte), Int32), CType(CType(251, Byte), Int32))
            Me.GridView1.Appearance.VertLine.Options.UseBackColor = True
            Me.GridView1.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.GridColumn_appt_time, Me.GridColumn_type, Me.GridColumn_start_time, Me.GridColumn_item_count, Me.GridColumn_booked_count, Me.GridColumn_formatted_appt_type, Me.GridColumn_counselors})
            Me.GridView1.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus
            StyleFormatCondition1.Appearance.BackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Int32), CType(CType(0, Byte), Int32), CType(CType(0, Byte), Int32))
            StyleFormatCondition1.Appearance.BackColor2 = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Int32), CType(CType(0, Byte), Int32), CType(CType(0, Byte), Int32))
            StyleFormatCondition1.Appearance.BorderColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Int32), CType(CType(0, Byte), Int32), CType(CType(0, Byte), Int32))
            StyleFormatCondition1.Appearance.ForeColor = System.Drawing.Color.White
            StyleFormatCondition1.Appearance.Options.UseBackColor = True
            StyleFormatCondition1.Appearance.Options.UseBorderColor = True
            StyleFormatCondition1.Appearance.Options.UseForeColor = True
            StyleFormatCondition1.ApplyToRow = True
            StyleFormatCondition1.Column = Me.GridColumn_booked_count
            StyleFormatCondition1.Condition = DevExpress.XtraGrid.FormatConditionEnum.Greater
            StyleFormatCondition1.Value1 = 0
            Me.GridView1.FormatConditions.AddRange(New DevExpress.XtraGrid.StyleFormatCondition() {StyleFormatCondition1})
            Me.GridView1.GridControl = Me.GridControl1
            Me.GridView1.Name = "GridView1"
            Me.GridView1.OptionsBehavior.Editable = False
            Me.GridView1.OptionsView.ShowGroupPanel = False
            Me.GridView1.OptionsView.ShowIndicator = False
            Me.GridView1.ShowButtonMode = DevExpress.XtraGrid.Views.Base.ShowButtonModeEnum.ShowForFocusedRow
            Me.GridView1.SortInfo.AddRange(New DevExpress.XtraGrid.Columns.GridColumnSortInfo() {New DevExpress.XtraGrid.Columns.GridColumnSortInfo(Me.GridColumn_start_time, DevExpress.Data.ColumnSortOrder.Ascending)})
            '
            'GridColumn_appt_time
            '
            Me.GridColumn_appt_time.AppearanceCell.Options.UseTextOptions = True
            Me.GridColumn_appt_time.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.GridColumn_appt_time.AppearanceHeader.Options.UseTextOptions = True
            Me.GridColumn_appt_time.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.GridColumn_appt_time.Caption = "ID"
            Me.GridColumn_appt_time.CustomizationCaption = "Record ID"
            Me.GridColumn_appt_time.DisplayFormat.FormatString = "f0"
            Me.GridColumn_appt_time.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.GridColumn_appt_time.FieldName = "appt_time"
            Me.GridColumn_appt_time.GroupFormat.FormatString = "f0"
            Me.GridColumn_appt_time.GroupFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.GridColumn_appt_time.Name = "GridColumn_appt_time"
            Me.GridColumn_appt_time.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.[True]
            '
            'GridColumn_type
            '
            Me.GridColumn_type.AppearanceCell.Options.UseTextOptions = True
            Me.GridColumn_type.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.GridColumn_type.AppearanceHeader.Options.UseTextOptions = True
            Me.GridColumn_type.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.GridColumn_type.Caption = "Type ID"
            Me.GridColumn_type.CustomizationCaption = "Type ID"
            Me.GridColumn_type.DisplayFormat.FormatString = "f0"
            Me.GridColumn_type.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.GridColumn_type.FieldName = "appt_type"
            Me.GridColumn_type.GroupFormat.FormatString = "f0"
            Me.GridColumn_type.GroupFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.GridColumn_type.Name = "GridColumn_type"
            Me.GridColumn_type.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.[True]
            '
            'GridColumn_start_time
            '
            Me.GridColumn_start_time.AppearanceCell.Options.UseTextOptions = True
            Me.GridColumn_start_time.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.GridColumn_start_time.AppearanceHeader.Options.UseTextOptions = True
            Me.GridColumn_start_time.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.GridColumn_start_time.Caption = "Time"
            Me.GridColumn_start_time.CustomizationCaption = "Starting Time"
            Me.GridColumn_start_time.DisplayFormat.FormatString = "h:mm tt"
            Me.GridColumn_start_time.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime
            Me.GridColumn_start_time.FieldName = "start_time"
            Me.GridColumn_start_time.GroupFormat.FormatString = "h:mm tt"
            Me.GridColumn_start_time.GroupFormat.FormatType = DevExpress.Utils.FormatType.DateTime
            Me.GridColumn_start_time.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.DateRange
            Me.GridColumn_start_time.Name = "GridColumn_start_time"
            Me.GridColumn_start_time.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.[True]
            Me.GridColumn_start_time.Visible = True
            Me.GridColumn_start_time.VisibleIndex = 0
            Me.GridColumn_start_time.Width = 70
            '
            'GridColumn_item_count
            '
            Me.GridColumn_item_count.AppearanceCell.Options.UseTextOptions = True
            Me.GridColumn_item_count.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.GridColumn_item_count.AppearanceHeader.Options.UseTextOptions = True
            Me.GridColumn_item_count.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.GridColumn_item_count.Caption = "Avail"
            Me.GridColumn_item_count.CustomizationCaption = "# Available"
            Me.GridColumn_item_count.DisplayFormat.FormatString = "f0"
            Me.GridColumn_item_count.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.GridColumn_item_count.FieldName = "avail"
            Me.GridColumn_item_count.GroupFormat.FormatString = "f0"
            Me.GridColumn_item_count.GroupFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.GridColumn_item_count.Name = "GridColumn_item_count"
            Me.GridColumn_item_count.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.[True]
            Me.GridColumn_item_count.Visible = True
            Me.GridColumn_item_count.VisibleIndex = 1
            Me.GridColumn_item_count.Width = 36
            '
            'GridColumn_formatted_appt_type
            '
            Me.GridColumn_formatted_appt_type.AppearanceCell.Options.UseTextOptions = True
            Me.GridColumn_formatted_appt_type.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
            Me.GridColumn_formatted_appt_type.AppearanceHeader.Options.UseTextOptions = True
            Me.GridColumn_formatted_appt_type.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
            Me.GridColumn_formatted_appt_type.Caption = "Type"
            Me.GridColumn_formatted_appt_type.CustomizationCaption = "Appt Type"
            Me.GridColumn_formatted_appt_type.DisplayFormat.FormatString = "d"
            Me.GridColumn_formatted_appt_type.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom
            Me.GridColumn_formatted_appt_type.FieldName = "appt_type"
            Me.GridColumn_formatted_appt_type.FilterMode = DevExpress.XtraGrid.ColumnFilterMode.DisplayText
            Me.GridColumn_formatted_appt_type.GroupFormat.FormatString = "d"
            Me.GridColumn_formatted_appt_type.GroupFormat.FormatType = DevExpress.Utils.FormatType.Custom
            Me.GridColumn_formatted_appt_type.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.DisplayText
            Me.GridColumn_formatted_appt_type.Name = "GridColumn_formatted_appt_type"
            Me.GridColumn_formatted_appt_type.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.[True]
            Me.GridColumn_formatted_appt_type.Visible = True
            Me.GridColumn_formatted_appt_type.VisibleIndex = 3
            Me.GridColumn_formatted_appt_type.Width = 36
            '
            'GridColumn_counselors
            '
            Me.GridColumn_counselors.AppearanceCell.Options.UseTextOptions = True
            Me.GridColumn_counselors.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
            Me.GridColumn_counselors.AppearanceHeader.Options.UseTextOptions = True
            Me.GridColumn_counselors.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
            Me.GridColumn_counselors.Caption = "Counselors"
            Me.GridColumn_counselors.CustomizationCaption = "Counselor Names"
            Me.GridColumn_counselors.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom
            Me.GridColumn_counselors.FieldName = "appt_time"
            Me.GridColumn_counselors.FilterMode = DevExpress.XtraGrid.ColumnFilterMode.DisplayText
            Me.GridColumn_counselors.GroupFormat.FormatType = DevExpress.Utils.FormatType.Custom
            Me.GridColumn_counselors.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.DisplayText
            Me.GridColumn_counselors.Name = "GridColumn_counselors"
            Me.GridColumn_counselors.OptionsColumn.AllowEdit = False
            Me.GridColumn_counselors.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.[True]
            Me.GridColumn_counselors.Visible = True
            Me.GridColumn_counselors.VisibleIndex = 4
            Me.GridColumn_counselors.Width = 153
            '
            'LayoutControlGroup1
            '
            Me.LayoutControlGroup1.CustomizationFormText = "LayoutControlGroup1"
            Me.LayoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.[True]
            Me.LayoutControlGroup1.GroupBordersVisible = False
            Me.LayoutControlGroup1.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.LayoutControlItem1, Me.LayoutControlItem2, Me.LayoutControlItem3, Me.EmptySpaceItem2, Me.EmptySpaceItem1})
            Me.LayoutControlGroup1.Location = New System.Drawing.Point(0, 0)
            Me.LayoutControlGroup1.Name = "LayoutControlGroup1"
            Me.LayoutControlGroup1.Size = New System.Drawing.Size(622, 366)
            Me.LayoutControlGroup1.Text = "LayoutControlGroup1"
            Me.LayoutControlGroup1.TextVisible = False
            '
            'LayoutControlItem1
            '
            Me.LayoutControlItem1.Control = Me.LookUpEdit_office
            Me.LayoutControlItem1.CustomizationFormText = "Office List"
            Me.LayoutControlItem1.Location = New System.Drawing.Point(201, 0)
            Me.LayoutControlItem1.Name = "LayoutControlItem1"
            Me.LayoutControlItem1.Size = New System.Drawing.Size(401, 24)
            Me.LayoutControlItem1.Text = "Office"
            Me.LayoutControlItem1.TextSize = New System.Drawing.Size(29, 13)
            '
            'LayoutControlItem2
            '
            Me.LayoutControlItem2.Control = Me.GridControl1
            Me.LayoutControlItem2.CustomizationFormText = "Appointment Grid"
            Me.LayoutControlItem2.Location = New System.Drawing.Point(237, 24)
            Me.LayoutControlItem2.Name = "LayoutControlItem2"
            Me.LayoutControlItem2.Size = New System.Drawing.Size(365, 322)
            Me.LayoutControlItem2.Text = "Appointment Grid"
            Me.LayoutControlItem2.TextSize = New System.Drawing.Size(0, 0)
            Me.LayoutControlItem2.TextToControlDistance = 0
            Me.LayoutControlItem2.TextVisible = False
            '
            'LayoutControlItem3
            '
            Me.LayoutControlItem3.Control = Me.MonthCalendar1
            Me.LayoutControlItem3.CustomizationFormText = "Month Calendar"
            Me.LayoutControlItem3.Location = New System.Drawing.Point(0, 24)
            Me.LayoutControlItem3.Name = "LayoutControlItem3"
            Me.LayoutControlItem3.Size = New System.Drawing.Size(237, 166)
            Me.LayoutControlItem3.Text = "Month Calendar"
            Me.LayoutControlItem3.TextSize = New System.Drawing.Size(0, 0)
            Me.LayoutControlItem3.TextToControlDistance = 0
            Me.LayoutControlItem3.TextVisible = False
            '
            'EmptySpaceItem2
            '
            Me.EmptySpaceItem2.CustomizationFormText = "EmptySpaceItem2"
            Me.EmptySpaceItem2.Location = New System.Drawing.Point(0, 0)
            Me.EmptySpaceItem2.Name = "EmptySpaceItem2"
            Me.EmptySpaceItem2.Size = New System.Drawing.Size(201, 24)
            Me.EmptySpaceItem2.Text = "EmptySpaceItem2"
            Me.EmptySpaceItem2.TextSize = New System.Drawing.Size(0, 0)
            '
            'EmptySpaceItem1
            '
            Me.EmptySpaceItem1.CustomizationFormText = "EmptySpaceItem1"
            Me.EmptySpaceItem1.Location = New System.Drawing.Point(0, 190)
            Me.EmptySpaceItem1.Name = "EmptySpaceItem1"
            Me.EmptySpaceItem1.Size = New System.Drawing.Size(237, 156)
            Me.EmptySpaceItem1.Text = "EmptySpaceItem1"
            Me.EmptySpaceItem1.TextSize = New System.Drawing.Size(0, 0)
            '
            'barManager1
            '
            Me.barManager1.Bars.AddRange(New DevExpress.XtraBars.Bar() {Me.Bar2})
            Me.barManager1.DockControls.Add(Me.barDockControlTop)
            Me.barManager1.DockControls.Add(Me.barDockControlBottom)
            Me.barManager1.DockControls.Add(Me.barDockControlLeft)
            Me.barManager1.DockControls.Add(Me.barDockControlRight)
            Me.barManager1.Form = Me
            Me.barManager1.Items.AddRange(New DevExpress.XtraBars.BarItem() {Me.BarButtonItem1, Me.BarSubItem1, Me.BarSubItem2, Me.BarButtonItem2})
            Me.barManager1.MainMenu = Me.Bar2
            Me.barManager1.MaxItemId = 4
            '
            'Bar2
            '
            Me.Bar2.BarName = "Main menu"
            Me.Bar2.DockCol = 0
            Me.Bar2.DockRow = 0
            Me.Bar2.DockStyle = DevExpress.XtraBars.BarDockStyle.Top
            Me.Bar2.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.None, Me.BarSubItem2, "", False, False, True, 0)})
            Me.Bar2.OptionsBar.MultiLine = True
            Me.Bar2.OptionsBar.UseWholeRow = True
            Me.Bar2.Text = "Main menu"
            '
            'BarSubItem2
            '
            Me.BarSubItem2.Caption = "Counselors"
            Me.BarSubItem2.Id = 2
            Me.BarSubItem2.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.BarButtonItem2)})
            Me.BarSubItem2.Name = "BarSubItem2"
            '
            'BarButtonItem2
            '
            Me.BarButtonItem2.Caption = "&Delete..."
            Me.BarButtonItem2.Id = 3
            Me.BarButtonItem2.Name = "BarButtonItem2"
            '
            'barDockControlTop
            '
            Me.barDockControlTop.CausesValidation = False
            Me.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top
            Me.barDockControlTop.Location = New System.Drawing.Point(0, 0)
            Me.barDockControlTop.Size = New System.Drawing.Size(622, 22)
            '
            'barDockControlBottom
            '
            Me.barDockControlBottom.CausesValidation = False
            Me.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom
            Me.barDockControlBottom.Location = New System.Drawing.Point(0, 388)
            Me.barDockControlBottom.Size = New System.Drawing.Size(622, 0)
            '
            'barDockControlLeft
            '
            Me.barDockControlLeft.CausesValidation = False
            Me.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left
            Me.barDockControlLeft.Location = New System.Drawing.Point(0, 22)
            Me.barDockControlLeft.Size = New System.Drawing.Size(0, 366)
            '
            'barDockControlRight
            '
            Me.barDockControlRight.CausesValidation = False
            Me.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right
            Me.barDockControlRight.Location = New System.Drawing.Point(622, 22)
            Me.barDockControlRight.Size = New System.Drawing.Size(0, 366)
            '
            'BarButtonItem1
            '
            Me.BarButtonItem1.Caption = "Counselors"
            Me.BarButtonItem1.Id = 0
            Me.BarButtonItem1.Name = "BarButtonItem1"
            '
            'BarSubItem1
            '
            Me.BarSubItem1.Caption = "Counselors"
            Me.BarSubItem1.Id = 1
            Me.BarSubItem1.Name = "BarSubItem1"
            '
            'Form_Edit
            '
            Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
            Me.ClientSize = New System.Drawing.Size(622, 388)
            Me.Controls.Add(Me.LayoutControl1)
            Me.Controls.Add(Me.barDockControlLeft)
            Me.Controls.Add(Me.barDockControlRight)
            Me.Controls.Add(Me.barDockControlBottom)
            Me.Controls.Add(Me.barDockControlTop)
            Me.LookAndFeel.UseDefaultLookAndFeel = True
            Me.Name = "Form_Edit"
            Me.Text = "Edit Generated Appointments"
            CType(Me.DxErrorProvider1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LookUpEdit_office.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControl1, System.ComponentModel.ISupportInitialize).EndInit()
            Me.LayoutControl1.ResumeLayout(False)
            CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.GridView1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlGroup1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem2, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem3, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.EmptySpaceItem2, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.EmptySpaceItem1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.barManager1, System.ComponentModel.ISupportInitialize).EndInit()
            Me.ResumeLayout(False)

        End Sub

#End Region

        ''' <summary>
        ''' Process the FROM CLOSING event
        ''' </summary>
        Private Sub Form_Closing(ByVal sender As Object, ByVal e As FormClosingEventArgs)
            ap.SaveChanges()
        End Sub

        ''' <summary>
        ''' Process the FORM LOAD event
        ''' </summary>
        Private Sub Form_Load(ByVal sender As Object, ByVal e As EventArgs)

            ' Restore the form placement
            LoadPlacement(FORM_NAME)

            With GridColumn_counselors
                With .DisplayFormat
                    .Format = New FormatCounselorNames(ap)
                End With
                With .GroupFormat
                    .Format = New FormatCounselorNames(ap)
                End With
            End With

            With GridColumn_formatted_appt_type
                With .DisplayFormat
                    .Format = New FormatApptTypes(ap)
                End With
                With .GroupFormat
                    .Format = New FormatApptTypes(ap)
                End With
            End With

            ' The earliest date is today.
            With MonthCalendar1
                .MinDate = Now.Date
                .TodayDate = .MinDate
                .SetDate(.MinDate)
                AddHandler .DateSelected, AddressOf Control_EditValueChanged
            End With

            ' Load the list of offices
            With LookUpEdit_office
                With .Properties
                    .DataSource = New DataView(ap.OfficesTable, "[ActiveFlag]<>0", "name", DataViewRowState.CurrentRows)

                    Dim rows() As DataRow = ap.OfficesTable.Select("([ActiveFlag]<>0) AND ([Default]<>0)", "office desc")
                    If rows.GetUpperBound(0) >= 0 Then
                        LookUpEdit_office.EditValue = rows(0)("office")
                    End If
                End With
                AddHandler .EditValueChanging, AddressOf DebtPlus.Data.Validation.LookUpEdit_ActiveTest
                AddHandler .EditValueChanged, AddressOf Control_EditValueChanged
            End With

            ' Load the initial selection if there is one
            ReadGrid()
        End Sub

        ''' <summary>
        ''' Double click event on the list
        ''' </summary>
        Private Sub GridView1_DoubleClick(ByVal sender As Object, ByVal e As EventArgs)
            Dim gv As DevExpress.XtraGrid.Views.Grid.GridView = GridView1
            Dim ctl As DevExpress.XtraGrid.GridControl = gv.GridControl
            Dim hi As DevExpress.XtraGrid.Views.Grid.ViewInfo.GridHitInfo = gv.CalcHitInfo((ctl.PointToClient(Windows.Forms.Control.MousePosition)))
            Dim RowHandle As Int32 = hi.RowHandle

            ' Find the row view in the dataview object for this row.
            Dim drv As DataRowView = Nothing
            If RowHandle >= 0 Then
                gv.FocusedRowHandle = RowHandle
                drv = CType(gv.GetRow(RowHandle), DataRowView)
            End If

            ' If the row is defined then edit the row.
            If drv IsNot Nothing Then
                drv.BeginEdit()
                If EditRow(drv) = System.Windows.Forms.DialogResult.OK Then
                    drv.EndEdit()
                Else
                    drv.CancelEdit()
                End If
            End If
        End Sub

        ''' <summary>
        ''' Edit -> menu being shown
        ''' </summary>
        Private Sub ContextMenu1_Popup(ByVal sender As Object, ByVal e As EventArgs)
            Dim gv As DevExpress.XtraGrid.Views.Grid.GridView = GridView1
            Dim ctl As DevExpress.XtraGrid.GridControl = gv.GridControl
            Dim hi As DevExpress.XtraGrid.Views.Grid.ViewInfo.GridHitInfo = gv.CalcHitInfo((ctl.PointToClient(Windows.Forms.Control.MousePosition)))
            Dim RowHandle As Int32 = hi.RowHandle

            ' Find the row view in the dataview object for this row.
            Dim drv As DataRowView = Nothing
            If RowHandle >= 0 Then
                gv.FocusedRowHandle = RowHandle
                drv = CType(gv.GetRow(RowHandle), DataRowView)
            End If

            ' If the row is defined then edit the row.
            If drv IsNot Nothing Then
                ContextMenu1_Edit.Enabled = True
                ContextMenu1_Delete.Enabled = True
            Else
                ContextMenu1_Edit.Enabled = False
                ContextMenu1_Delete.Enabled = False
            End If

            ' The create is enabled if there is a view for the control
            ContextMenu1_Create.Enabled = ctl.DataSource IsNot Nothing
        End Sub

        ''' <summary>
        ''' Edit -> Create menu item clicked
        ''' </summary>
        Private Sub ContextMenu1_Create_Click(ByVal sender As Object, ByVal e As EventArgs)
            Dim vue As DataView = CType(GridControl1.DataSource, DataView)

            ' Add the new row to the dataview
            Dim drv As DataRowView = vue.AddNew()
            drv.BeginEdit()

            ' Set some defaults for the new row before we edit the value.
            drv("office") = LookUpEdit_office.EditValue
            drv("appt_type") = DBNull.Value

            ' Find the latest time for the appointment times
            Dim SelectionCriteria As String = String.Format("[office]={0:f0} AND [start_time]>='{1:d}' AND [start_time]<'{2:d}'", LookUpEdit_office.EditValue, MonthCalendar1.SelectionStart, MonthCalendar1.SelectionStart.AddDays(1))
            Dim objTemp As Object = ap.ds.Tables("appt_times").Compute("max(start_time)", SelectionCriteria)
            If objTemp Is Nothing OrElse objTemp Is DBNull.Value Then objTemp = New DateTime(1900, 1, 1, 7, 0, 0) ' 7:00AM on 1/1/1900

            ' Add an hour to the time if possible
            Dim EndingTime As Date = Convert.ToDateTime(objTemp)
            If EndingTime.Hour < 23 Then EndingTime = EndingTime.AddHours(1)

            ' Merge the values together with the time and the current date
            drv("start_time") = MonthCalendar1.SelectionStart.Add(EndingTime.TimeOfDay)

            If EditRow(drv) = System.Windows.Forms.DialogResult.OK Then
                drv.EndEdit()
            Else
                drv.CancelEdit()
            End If
        End Sub

        ''' <summary>
        ''' Edit -> Edit menu item clicked
        ''' </summary>
        Private Sub ContextMenu1_Edit_Click(ByVal sender As Object, ByVal e As EventArgs)
            Dim gv As DevExpress.XtraGrid.Views.Grid.GridView = GridView1
            Dim ctl As DevExpress.XtraGrid.GridControl = gv.GridControl
            Dim RowHandle As Int32 = GridView1.FocusedRowHandle
            Dim drv As DataRowView = Nothing

            If RowHandle >= 0 Then
                gv.FocusedRowHandle = RowHandle
                drv = CType(gv.GetRow(RowHandle), DataRowView)
            End If

            ' If the row is defined then edit the row.
            If drv IsNot Nothing Then
                drv.BeginEdit()
                If EditRow(drv) = System.Windows.Forms.DialogResult.OK Then
                    drv.EndEdit()
                Else
                    drv.CancelEdit()
                End If
            End If
        End Sub

        ''' <summary>
        ''' Edit -> Delete menu item clicked
        ''' </summary>
        Private Sub ContextMenu1_Delete_Click(ByVal sender As Object, ByVal e As EventArgs)
            Dim gv As DevExpress.XtraGrid.Views.Grid.GridView = GridView1
            Dim ctl As DevExpress.XtraGrid.GridControl = gv.GridControl
            Dim RowHandle As Int32 = GridView1.FocusedRowHandle
            Dim drv As DataRowView = Nothing

            If RowHandle >= 0 Then
                gv.FocusedRowHandle = RowHandle
                drv = CType(gv.GetRow(RowHandle), DataRowView)
            End If

            ' If the row is defined then edit the row.
            If drv IsNot Nothing Then
                If DeleteRow(drv) = System.Windows.Forms.DialogResult.Yes Then
                    drv.Delete()
                End If
            End If
        End Sub

        ''' <summary>
        ''' Handle the edit of the information on the form
        ''' </summary>
        Protected Overridable Function EditRow(ByVal drv As DataRowView) As DialogResult

            ' Conduct the form and obtain the new values
            Dim answer As DialogResult
            Using frm As New Form_Item(ap, drv)
                answer = frm.ShowDialog()
            End Using

            Return answer
        End Function

        ''' <summary>
        ''' Ask the user for confirmation on the delete operation
        ''' </summary>
        Protected Overridable Function DeleteRow(ByVal drv As DataRowView) As DialogResult

            Dim appt_time As Int32 = DebtPlus.Utils.Nulls.DInt(drv("appt_time"))
            If appt_time <> 0 Then
                Dim tbl As DataTable = ap.ds.Tables("client_appointments")
                If tbl IsNot Nothing Then
                    Using vue As New DataView(tbl, String.Format("[appt_time]={0:f0}", appt_time), String.Empty, DataViewRowState.CurrentRows)
                        Dim AppointmentCount As Int32 = vue.Count
                        If AppointmentCount > 0 Then
                            Dim answer As DialogResult = DebtPlus.Data.Forms.MessageBox.Show(String.Format("CAUTION{0}{0}Deleting schedules here will cancel the {1:f0} pending client appointment(s) for this time slot.{0}Perhaps you should reschedule the appointments first before you delete this time slot.{0}{0}Are you really sure that you wish to delete the appointment time?", Environment.NewLine, AppointmentCount), My.Resources.AreYouSure, MessageBoxButtons.YesNo, MessageBoxIcon.Hand, MessageBoxDefaultButton.Button2)
                            If answer = Windows.Forms.DialogResult.No Then
                                Return answer
                            End If

                            ' Cancel the appointments in the row
                            For Each AppointmentDRV As DataRowView In vue
                                AppointmentDRV.Row.Delete()
                                AppointmentDRV.Row.AcceptChanges()
                            Next
                        End If
                    End Using
                End If

                ' Remove the counselor references as well. The system will actually do the deletion when
                ' we delete the appointment time slot so we don't need to do anything now but remove them
                ' from our tables.
                tbl = ap.ds.Tables("appt_counselors")
                If tbl IsNot Nothing Then
                    Dim Rows() As DataRow = tbl.Select(String.Format("[appt_time]={0:f0}", appt_time))
                    For Each row As DataRow In Rows
                        row.Delete()
                        row.AcceptChanges()
                    Next
                End If
            End If

            Return System.Windows.Forms.DialogResult.Yes
        End Function

        ''' <summary>
        ''' Do updates to the grid when the information changes
        ''' </summary>
        Private Sub Control_EditValueChanged(ByVal sender As Object, ByVal e As EventArgs)
            ReadGrid()
        End Sub

        ''' <summary>
        ''' Refresh the grid with the changes
        ''' </summary>
        Private Sub ReadGrid()

            ' Do nothing if there is not a complete picture for the data
            If LookUpEdit_office.EditValue Is Nothing Then Return

            Dim Office As Int32 = Convert.ToInt32(LookUpEdit_office.EditValue)
            Dim FromDate As DateTime = MonthCalendar1.SelectionStart
            Dim ToDate As DateTime = FromDate

            ap.ReadItems(FromDate, ToDate, Office)

            ' Bind the dataset to the relation
            With GridControl1
                .DataSource = New DataView(ap.ds.Tables("appt_times"), String.Format("(office={0:f0}) and (start_time >= '{1:d}') and (start_time < '{2:d}')", Office, FromDate.Date, ToDate.AddDays(1).Date), String.Empty, DataViewRowState.CurrentRows)
                .RefreshDataSource()
            End With
        End Sub

        Private Class FormatCounselorNames
            Implements IFormatProvider, ICustomFormatter

            Private ap As ArgParser
            Public Sub New(ByVal ap As ArgParser)
                Me.ap = ap
            End Sub

            Public Function Format(ByVal format1 As String, ByVal arg As Object, ByVal formatProvider As IFormatProvider) As String Implements ICustomFormatter.Format
                Dim sb As New System.Text.StringBuilder

                ' Convert the record number to edit to a number and find the list of counselros for that record
                Dim appt_time As Int32 = DebtPlus.Utils.Nulls.DInt(arg)
                Dim tbl As DataTable = ap.ds.Tables("appt_counselors")
                If appt_time > 0 AndAlso tbl IsNot Nothing Then
                    Dim CounselorRows() As DataRow = tbl.Select(String.Format("[appt_time]={0:f0} and [inactive]=0", appt_time))

                    ' For each counselor, find the name
                    For Each CounselorRow As DataRow In CounselorRows
                        Dim CounselorID As Int32 = DebtPlus.Utils.Nulls.DInt(CounselorRow("counselor"))
                        If CounselorID > 0 Then

                            ' Given the counselor id, find the row for the counselor
                            Dim row As DataRow = ap.CounselorsTable.Rows.Find(CounselorID)
                            If row IsNot Nothing Then
                                Dim NameID As Int32 = DebtPlus.Utils.Nulls.DInt(row("NameID"))
                                If NameID > 0 Then

                                    ' Given the address of the name in the tables, find the row and format the name
                                    Dim NameRow As DataRow = ap.GetNamesRow(NameID)
                                    Dim txt As String = DebtPlus.LINQ.Name.FormatNormalName(Nothing, NameRow("first"), Nothing, NameRow("last"), Nothing)
                                    sb.AppendFormat(",{0}", txt)
                                End If
                            End If
                        End If
                    Next
                End If

                ' Remove the leading comma character
                If sb.Length > 0 Then
                    sb.Remove(0, 1)
                End If

                Return sb.ToString()
            End Function

            Public Function GetFormat(ByVal formatType As Type) As Object Implements IFormatProvider.GetFormat
                Return Me
            End Function
        End Class

        Private Class FormatApptTypes
            Implements IFormatProvider, ICustomFormatter

            Private ap As ArgParser
            Public Sub New(ByVal ap As ArgParser)
                Me.ap = ap
            End Sub

            Public Function Format(ByVal format1 As String, ByVal arg As Object, ByVal formatProvider As IFormatProvider) As String Implements ICustomFormatter.Format
                Dim answer As String = String.Empty

                ' Convert the record number to edit to a number and find the list of counselros for that record
                If arg IsNot Nothing AndAlso arg IsNot DBNull.Value Then
                    Dim appt_type As Int32 = Convert.ToInt32(arg)
                    If appt_type > 0 Then
                        Dim tbl As DataTable = ap.ApptTypesTable()
                        If tbl IsNot Nothing Then
                            Dim row As DataRow = tbl.Rows.Find(appt_type)
                            If row IsNot Nothing Then
                                answer = DebtPlus.Utils.Nulls.DStr(row("appt_name"))
                            End If
                        End If
                    End If
                Else
                    answer = "ALL"
                End If

                Return answer
            End Function

            Public Function GetFormat(ByVal formatType As Type) As Object Implements IFormatProvider.GetFormat
                Return Me
            End Function
        End Class

        Private Sub BarButtonItem2_ItemClick(ByVal sender As Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs)
            Using frm As New Form_RemoveCounselor(ap)
                frm.ShowDialog()
            End Using
        End Sub
    End Class
End Namespace
