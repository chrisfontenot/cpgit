#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}
#End Region

Option Compare Binary
Option Explicit On
Option Strict On

Imports DebtPlus.Utils
Imports System.Data.SqlClient
Imports System.Windows.Forms

Namespace Appointments.Update

    Partial Friend Class ArgParser

        ' Dataset for the table storage
        Friend ds As New System.Data.DataSet("ds")

#Region " Day Of The Week "

        ''' <summary>
        ''' Days of the week for the template table
        ''' </summary>
        Friend Function DayOfWeekTable() As System.Data.DataTable
            Return DayOfWeekTable(False)
        End Function

        Friend Function DayOfWeekTable(ByVal Reload As Boolean) As System.Data.DataTable
            Dim tbl As System.Data.DataTable = ds.Tables("days_of_week")
            If tbl Is Nothing Then
                tbl = New System.Data.DataTable("days_of_week")
                With tbl

                    ' Define the table as a list of numbers attached to the local names of the days of the week.
                    ' Numbers range from 0(sunday) through 6(saturday)
                    .Columns.Add("day_of_week", GetType(System.Int32))
                    .Columns.Add("description", GetType(String))
                    .PrimaryKey = New DataColumn() {.Columns(0)}

                    ' Add the values to the table for the corresponding days of the week
                    .BeginLoadData()
                    For Each dow As System.DayOfWeek In New Object() {System.DayOfWeek.Sunday, System.DayOfWeek.Monday, System.DayOfWeek.Tuesday, System.DayOfWeek.Wednesday, System.DayOfWeek.Thursday, System.DayOfWeek.Friday, System.DayOfWeek.Saturday}
                        .Rows.Add(New Object() {Convert.ToInt32(dow) + 1, System.Globalization.CultureInfo.CurrentCulture.DateTimeFormat().GetDayName(dow)})
                    Next
                    .EndLoadData()

                    .AcceptChanges()
                    ds.Tables.Add(tbl)
                End With
            End If

            Return tbl
        End Function
#End Region

#Region " Names "

        ''' <summary>
        ''' Return the row corresponding to the given record ID
        ''' </summary>
        Friend Function GetNamesRow(ByVal NameID As System.Int32) As System.Data.DataRow
            Dim MyTableName As String = "Names"
            Dim tbl As System.Data.DataTable = ds.Tables(MyTableName)
            If tbl IsNot Nothing Then
                Dim row As System.Data.DataRow = tbl.Rows.Find(NameID)
                If row IsNot Nothing Then
                    Return row
                End If
            End If

            tbl = ReadNamesTable(NameID)
            Return tbl.Rows.Find(NameID)
        End Function

        ''' <summary>
        ''' Names table
        ''' </summary>
        Friend Function ReadNamesTable(ByVal NameID As System.Int32) As System.Data.DataTable
            Dim MyTableName As String = "Names"
            Dim tbl As System.Data.DataTable = Nothing

            ' If the table is not found then read the information from the database
            Dim current_cursor As Cursor = System.Windows.Forms.Cursor.Current
            Try
                System.Windows.Forms.Cursor.Current = Cursors.WaitCursor
                Using cmd As SqlClient.SqlCommand = New SqlCommand
                    With cmd
                        .Connection = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
                        .CommandText = "SELECT [Name],[Prefix],[First],[Middle],[Last],[Suffix] FROM Names WITH (NOLOCK) WHERE [Name]=@Name"
                        .CommandType = CommandType.Text
                        .Parameters.Add("@Name", SqlDbType.Int).Value = NameID
                    End With

                    ' Ask the database for the information
                    Using da As New SqlClient.SqlDataAdapter(cmd)
                        da.Fill(ds, MyTableName)
                    End Using
                End Using

                tbl = ds.Tables(MyTableName)
                With tbl
                    If .PrimaryKey.GetUpperBound(0) < 0 Then
                        .PrimaryKey = New System.Data.DataColumn() {.Columns("Name")}
                    End If
                End With

                ' When we do an update, the update results in zero rows being updated. IGNORE IT.
            Catch ex As System.Data.DBConcurrencyException
                DebtPlus.Svc.Common.ErrorHandling.HandleErrors(ex)

                ' Something happend in trying to talk to the database.
            Catch ex As SqlClient.SqlException
                Using gdr As New Repository.GetDataResult()
                    gdr.HandleException(ex)
                    DebtPlus.UI.Common.ErrorHandling.HandleErrors(gdr, "Error reading " + MyTableName + " table")
                End Using

            Finally
                System.Windows.Forms.Cursor.Current = current_cursor
            End Try

            Return tbl
        End Function
#End Region

        ''' <summary>
        ''' Save any pending changes to the database
        ''' </summary>
        Friend Sub SaveChanges()

            ' Save any changes to the database
            Dim cn As SqlClient.SqlConnection = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
            Dim txn As SqlClient.SqlTransaction = Nothing
            Dim current_cursor As Cursor = System.Windows.Forms.Cursor.Current
            Try
                System.Windows.Forms.Cursor.Current = Cursors.WaitCursor
                cn.Open()
                txn = cn.BeginTransaction(IsolationLevel.ReadUncommitted)

                Update_client_appointments(cn, txn)
                Update_appt_times(cn, txn)
                Update_appt_counselors(cn, txn)

                txn.Commit()
                txn.Dispose()
                txn = Nothing

            Catch ex As SqlClient.SqlException
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error updating appt_times table")

            Finally
                If txn IsNot Nothing Then
                    Try
                        txn.Rollback()
                    Catch exRollback As System.Exception
                    End Try
                    txn.Dispose()
                    txn = Nothing
                End If

                If cn IsNot Nothing Then cn.Dispose()

                System.Windows.Forms.Cursor.Current = current_cursor
            End Try
        End Sub

        ''' <summary>
        ''' Save any changes to the grid at this point
        ''' </summary>
        Friend Sub Update_client_appointments(ByVal cn As SqlClient.SqlConnection, ByVal txn As SqlClient.SqlTransaction)

            Dim tbl As System.Data.DataTable = ds.Tables("client_appointments")
            If tbl IsNot Nothing Then
                Using cmd As SqlClient.SqlCommand = New SqlCommand
                    With cmd
                        .Connection = cn
                        .Transaction = txn
                        .CommandText = "xpr_appt_cancel"
                        .CommandType = CommandType.StoredProcedure
                        .Parameters.Add("@appt", SqlDbType.Int, 0, "client_appointment")
                    End With

                    Using da As New SqlClient.SqlDataAdapter
                        da.DeleteCommand = cmd
                        Dim RowsUpdated As System.Int32 = da.Update(tbl)
                    End Using
                End Using
            End If
        End Sub

        ''' <summary>
        ''' Save the appt_counselor changes to the database
        ''' </summary>
        Friend Sub Update_appt_counselors(ByVal cn As SqlClient.SqlConnection, ByVal txn As SqlClient.SqlTransaction)

            ' Add new counselors to the time slot
            Dim tbl As System.Data.DataTable = ds.Tables("appt_counselors")
            If tbl IsNot Nothing Then
                Dim UpdateCmd As SqlClient.SqlCommand = New SqlCommand
                Dim InsertCmd As SqlClient.SqlCommand = New SqlCommand
                Dim DeleteCmd As SqlClient.SqlCommand = New SqlCommand
                Try

                    ' Build the update command object
                    Using da As New SqlClient.SqlDataAdapter
                        With UpdateCmd
                            .Connection = cn
                            .Transaction = txn
                            .CommandText = "UPDATE appt_counselors SET [counselor]=@counselor,[inactive]=@inactive,[inactive_reason]=@inactive_reason WHERE [appt_counselor]=@appt_counselor"
                            .CommandType = CommandType.Text
                            .Parameters.Add("@counselor", SqlDbType.Int, 0, "counselor")
                            .Parameters.Add("@inactive", SqlDbType.Bit, 0, "inactive")
                            .Parameters.Add("@inactive_reason", SqlDbType.VarChar, 80, "inactive_reason")
                            .Parameters.Add("@appt_counselor", SqlDbType.Int, 0, "appt_counselor")
                        End With
                        da.UpdateCommand = UpdateCmd

                        ' Build the insert command object
                        With InsertCmd
                            .Connection = cn
                            .Transaction = txn
                            .CommandText = "xpr_insert_appt_counselor"
                            .CommandType = CommandType.StoredProcedure
                            .Parameters.Add("@RETURN_VALUE", SqlDbType.Int, 0, "appt_counselor").Direction = ParameterDirection.ReturnValue
                            .Parameters.Add("@appt_time", SqlDbType.Int, 0, "appt_time")
                            .Parameters.Add("@counselor", SqlDbType.Int, 0, "counselor")
                            .Parameters.Add("@inactive", SqlDbType.Bit, 0, "inactive")
                            .Parameters.Add("@inactive_reason", SqlDbType.VarChar, 80, "inactive_reason")
                        End With
                        da.InsertCommand = InsertCmd

                        ' Build the deleteCmd command object
                        With DeleteCmd
                            .Connection = cn
                            .Transaction = txn
                            .CommandText = "DELETE from appt_counselors WHERE appt_counselor=@appt_counselor"
                            .CommandType = CommandType.Text
                            .Parameters.Add("@appt_counselor", SqlDbType.Int, 0, "appt_counselor")
                        End With
                        da.DeleteCommand = DeleteCmd

                        ' Issue the table Update
                        Dim RowsUpdated As System.Int32 = da.Update(tbl)
                        RowsUpdated += 0
                    End Using

                Finally
                    InsertCmd.Dispose()
                    UpdateCmd.Dispose()
                    DeleteCmd.Dispose()
                End Try
            End If
        End Sub

        ''' <summary>
        ''' Save the appt_times table changes
        ''' </summary>
        Friend Sub Update_appt_times(ByVal cn As SqlClient.SqlConnection, ByVal txn As SqlClient.SqlTransaction)

            Dim tbl As System.Data.DataTable = ds.Tables("appt_times")
            If tbl IsNot Nothing Then
                Dim UpdateCmd As SqlClient.SqlCommand = New SqlCommand
                'Dim InsertCmd As SqlClient.SqlCommand = New SqlCommand
                Dim DeleteCmd As SqlClient.SqlCommand = New SqlCommand
                Try

                    ' Build the update command object
                    Using da As New SqlClient.SqlDataAdapter
                        With UpdateCmd
                            .Connection = cn
                            .Transaction = txn
                            .CommandText = "UPDATE appt_times SET appt_type=@appt_type,start_time=@start_time WHERE appt_time=@appt_time"
                            .CommandType = CommandType.Text
                            .Parameters.Add("@appt_type", SqlDbType.Int, 0, "appt_type")
                            .Parameters.Add("@start_time", SqlDbType.DateTime, 0, "start_time")
                            .Parameters.Add("@appt_time", SqlDbType.Int, 0, "appt_time")
                        End With
                        da.UpdateCommand = UpdateCmd
#If 0 Then
                    ' Build the insert command object
                    With InsertCmd
                        .Connection = cn
                        .Transaction = txn
                        .CommandText = "xpr_insert_appt_time"
                        .CommandType = CommandType.StoredProcedure
                        .Parameters.Add("@RETURN_VALUE", SqlDbType.Int, 0, "appt_time").Direction = ParameterDirection.ReturnValue
                        .Parameters.Add("@start_time", SqlDbType.DateTime, 0, "start_time")
                        .Parameters.Add("@office", SqlDbType.Int, 0, "office")
                        .Parameters.Add("@appt_type", SqlDbType.Bit, 0, "appt_type")
                    End With
                    'da.InsertCommand = InsertCmd
#End If
                        ' Yes, we really want to do this. Do an update if the table shows "added" since the row is added externally we want to update the row with the proper values.
                        da.InsertCommand = UpdateCmd

                        ' Build the deleteCmd command object
                        With DeleteCmd
                            .Connection = cn
                            .Transaction = txn
                            .CommandText = "DELETE from appt_times WHERE appt_time=@appt_time"
                            .CommandType = CommandType.Text
                            .Parameters.Add("@appt_time", SqlDbType.Int, 0, "appt_time")
                        End With
                        da.DeleteCommand = DeleteCmd

                        ' Issue the table Update
                        Dim RowsUpdated As System.Int32 = da.Update(tbl)
                    End Using

                Finally
                    'InsertCmd.Dispose()
                    UpdateCmd.Dispose()
                    DeleteCmd.Dispose()
                End Try
            End If
        End Sub

        ''' <summary>
        ''' Read the appointment information from the database
        ''' </summary>
        Friend Sub ReadItems(ByVal FromDate As DateTime, ByVal ToDate As DateTime, ByVal Office As System.Int32?)

            ' Read the grid information from the database
            Dim cn As SqlClient.SqlConnection = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
            Dim current_cursor As Cursor = System.Windows.Forms.Cursor.Current
            Try
                System.Windows.Forms.Cursor.Current = Cursors.WaitCursor
                cn.Open()

                ' Read the list of times for this office
                Using cmd As SqlClient.SqlCommand = New SqlCommand
                    With cmd
                        .Connection = cn
                        .CommandText = "SELECT appt_time, office, start_time, appt_type FROM appt_times WITH (NOLOCK) WHERE (start_time >= @from_date) AND (start_time < @to_date)"
                        .CommandType = CommandType.Text
                        .Parameters.Add("@from_date", SqlDbType.DateTime).Value = FromDate.Date
                        .Parameters.Add("@to_date", SqlDbType.DateTime).Value = ToDate.AddDays(1).Date
                        If Office.HasValue Then
                            .CommandText += " AND ([office]=@office)"
                            .Parameters.Add("@office", SqlDbType.Int).Value = Office.Value
                        End If
                    End With

                    Using da As New SqlClient.SqlDataAdapter(cmd)
                        ds.EnforceConstraints = False
                        da.FillLoadOption = LoadOption.PreserveChanges
                        da.Fill(ds, "appt_times")
                    End Using
                End Using

                ' Define the primary key
                With ds.Tables("appt_times")
                    If .PrimaryKey.GetUpperBound(0) < 0 Then
                        .PrimaryKey = New System.Data.DataColumn() {.Columns("appt_time")}
                    End If

                    With .Columns("appt_time")
                        If Not .AutoIncrement Then
                            .AutoIncrement = True
                            .AutoIncrementStep = -1
                            .AutoIncrementSeed = -1
                        End If
                    End With
                End With

                ' Read the counselor items for these times
                Using cmd As SqlClient.SqlCommand = New SqlCommand
                    With cmd
                        .Connection = cn
                        .CommandText = "SELECT appt_counselor,appt_time,counselor,inactive,inactive_reason FROM appt_counselors WHERE appt_time in (select appt_time FROM appt_times WITH (NOLOCK) WHERE (start_time >= @from_date) AND (start_time < @to_date)"
                        .CommandType = CommandType.Text
                        .Parameters.Add("@from_date", SqlDbType.DateTime).Value = FromDate.Date
                        .Parameters.Add("@to_date", SqlDbType.DateTime).Value = ToDate.AddDays(1).Date
                        If Office.HasValue Then
                            .CommandText += " AND ([office]=@office)"
                            .Parameters.Add("@office", SqlDbType.Int).Value = Office.Value
                        End If
                        .CommandText += ")"
                    End With

                    Using da As New SqlClient.SqlDataAdapter(cmd)
                        ds.EnforceConstraints = False
                        da.FillLoadOption = LoadOption.PreserveChanges
                        da.Fill(ds, "appt_counselors")
                    End Using

                    ' Define the primary key
                    With ds.Tables("appt_counselors")
                        If .PrimaryKey.GetUpperBound(0) < 0 Then
                            .PrimaryKey = New System.Data.DataColumn() {.Columns("appt_counselor")}
                        End If

                        With .Columns("appt_counselor")
                            If Not .AutoIncrement Then
                                .AutoIncrement = True
                                .AutoIncrementStep = -1
                                .AutoIncrementSeed = -1
                            End If
                        End With
                        .Columns("inactive").DefaultValue = 0

                        If Not .Columns.Contains("count_inactive") Then
                            .Columns.Add("count_inactive", GetType(Int32), "iif([inactive]=false,1,0)")
                        End If
                    End With
                End Using

                ' Create a relation to count the number of available counselors for a given slot
                If Not ds.Relations.Contains("rel_appt_times_appt_counselors") Then
                    Dim rel_appt_times_appt_counselors As New System.Data.DataRelation("rel_appt_times_appt_counselors", ds.Tables("appt_times").Columns("appt_time"), ds.Tables("appt_counselors").Columns("appt_time"), False)
                    ds.Relations.Add(rel_appt_times_appt_counselors)
                End If

                If Not ds.Tables("appt_times").Columns.Contains("avail") Then
                    ds.Tables("appt_times").Columns.Add("avail", GetType(System.Int32), "sum(Child(rel_appt_times_appt_counselors).count_inactive)")
                End If

                ' Read the pending appointments for these times
                Using cmd As SqlClient.SqlCommand = New SqlCommand
                    With cmd
                        .Connection = cn
                        .CommandText = "SELECT [client_appointment],[client],[appt_time],[counselor],[date_confirmed],[date_updated],[date_created],[created_by],[confirmation_status],[callback_ph],[credit],[post_purchase],[priority],[housing],[duration],[status],[start_time],[formatted_status],[formatted_confirmation_status],[formatted_appt_type],[active_status],[formatted_office],[formatted_counselor],[formatted_home_ph],[formatted_message_ph],[formatted_work_ph],[formatted_name],[appt_office],[appt_start_time] FROM view_pending_appointments WITH (NOLOCK) WHERE ([appt_start_time]>=@from_date) AND ([appt_start_time]<@to_date)"
                        .CommandType = CommandType.Text
                        .Parameters.Add("@from_date", SqlDbType.DateTime).Value = FromDate.Date
                        .Parameters.Add("@to_date", SqlDbType.DateTime).Value = ToDate.AddDays(1).Date

                        If Office.HasValue Then
                            .CommandText += " AND ([appt_office]=@office)"
                            .Parameters.Add("@office", SqlDbType.Int).Value = Office
                        End If
                    End With

                    Using da As New SqlClient.SqlDataAdapter(cmd)
                        ds.EnforceConstraints = False
                        da.FillLoadOption = LoadOption.PreserveChanges
                        da.Fill(ds, "client_appointments")
                    End Using
                End Using

                With ds.Tables("client_appointments")
                    If .PrimaryKey.GetUpperBound(0) < 0 Then
                        .PrimaryKey = New System.Data.DataColumn() {.Columns("client_appointment")}
                    End If
                End With

                ' Create a relation to count the number of booked appointments for a given slot
                If Not ds.Relations.Contains("rel_appt_times_client_appointments") Then
                    Dim rel_appt_times_appt_counselors As New System.Data.DataRelation("rel_appt_times_client_appointments", ds.Tables("appt_times").Columns("appt_time"), ds.Tables("client_appointments").Columns("appt_time"), False)
                    ds.Relations.Add(rel_appt_times_appt_counselors)
                End If

                If Not ds.Tables("appt_times").Columns.Contains("booked") Then
                    ds.Tables("appt_times").Columns.Add("booked", GetType(System.Int32), "count(Child(rel_appt_times_client_appointments).appt_time)")
                End If

            Catch ex As SqlClient.SqlException
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error reading appt_times information")

            Finally
                If cn IsNot Nothing Then cn.Dispose()
                System.Windows.Forms.Cursor.Current = current_cursor
            End Try
        End Sub

#Region " Names "

        ''' <summary>
        ''' Return the row corresponding to the given record ID
        ''' </summary>
        Friend Function GetPeopleRow(ByVal ClientID As System.Int32, ByVal Person As System.Int32) As System.Data.DataRow
            Dim MyTableName As String = "People"
            Dim tbl As System.Data.DataTable = ds.Tables(MyTableName)
            If tbl IsNot Nothing Then
                Dim row As System.Data.DataRow = tbl.Rows.Find(New Object() {ClientID, Person})
                If row IsNot Nothing Then
                    Return row
                End If
            End If

            tbl = ReadPeopleTable(ClientID, String.Format("[person]={0:f0}", Person))
            Return tbl.Rows.Find(New Object() {ClientID, Person})
        End Function

        ''' <summary>
        ''' Names table
        ''' </summary>
        Friend Function ReadPeopleTable(ByVal ClientID As System.Int32, ByVal SelectClause As String) As System.Data.DataTable
            Dim MyTableName As String = "People"
            Dim tbl As System.Data.DataTable = Nothing

            ' If the table is not found then read the information from the database
            Dim current_cursor As Cursor = System.Windows.Forms.Cursor.Current
            Try
                System.Windows.Forms.Cursor.Current = Cursors.WaitCursor
                Using cmd As SqlClient.SqlCommand = New SqlCommand
                    With cmd
                        .Connection = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
                        .CommandText = String.Format("SELECT [Person],[Client],[Relation],[NameID],[WorkTelephoneID],[CellTelephoneID],[EmailID],[Former],[SSN],[Gender],[Race],[Ethnicity],[Disabled],[MilitaryStatusID],[MilitaryServiceID],[MilitaryGradeID],[MilitaryDependentID],[Education],[Birthdate],[FICO_Score],[CreditAgency],[no_fico_score_reason],[gross_income],[net_income],[Frequency],[emp_start_date],[emp_end_date],[employer],[job],[other_job],[bkfileddate],[bkdischarge],[bkchapter],[bkPaymentCurrent],[bkAuthLetterDate],[SPOC_ID],[created_by],[date_created],[POA_Name],[poa] FROM People WITH (NOLOCK) WHERE [client]=@client{0}", SelectClause)
                        .CommandType = CommandType.Text
                        .Parameters.Add("@client", SqlDbType.Int).Value = ClientID
                    End With

                    ' Ask the database for the information
                    Using da As New SqlClient.SqlDataAdapter(cmd)
                        da.Fill(ds, MyTableName)
                    End Using
                End Using

                tbl = ds.Tables(MyTableName)
                With tbl
                    If .PrimaryKey.GetUpperBound(0) < 0 Then
                        .PrimaryKey = New System.Data.DataColumn() {.Columns("person")}
                    End If
                End With

                ' When we do an update, the update results in zero rows being updated. IGNORE IT.
            Catch ex As System.Data.DBConcurrencyException
                DebtPlus.Svc.Common.ErrorHandling.HandleErrors(ex)

                ' Something happend in trying to talk to the database.
            Catch ex As SqlClient.SqlException
                Using gdr As New Repository.GetDataResult()
                    gdr.HandleException(ex)
                    DebtPlus.UI.Common.ErrorHandling.HandleErrors(gdr, "Error reading " + MyTableName + " table")
                End Using

            Finally
                System.Windows.Forms.Cursor.Current = current_cursor
            End Try

            Return tbl
        End Function
#End Region

#Region " Office Types "

        ''' <summary>
        ''' Office Type table
        ''' </summary>
        Friend Function OfficeTypesTable() As System.Data.DataTable
            Dim MyTableName As String = "office_types"
            Dim tbl As System.Data.DataTable = ds.Tables(MyTableName)

            ' If the table is not found then read the information from the database
            If tbl Is Nothing Then
                Dim current_cursor As Cursor = System.Windows.Forms.Cursor.Current
                Try
                    System.Windows.Forms.Cursor.Current = Cursors.WaitCursor
                    Using cmd As SqlClient.SqlCommand = New SqlCommand
                        With cmd
                            .Connection = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
                            .CommandText = "SELECT item_value as office_type, description FROM messages WITH (NOLOCK) WHERE item_type='OFFICE TYPE'"
                            .CommandType = CommandType.Text
                        End With

                        ' Ask the database for the information
                        Using da As New SqlClient.SqlDataAdapter(cmd)
                            da.Fill(ds, MyTableName)
                        End Using
                    End Using
                    tbl = ds.Tables(MyTableName)
                    tbl.PrimaryKey = New System.Data.DataColumn() {tbl.Columns(0)}

                    ' When we do an update, the update results in zero rows being updated. IGNORE IT.
                Catch ex As System.Data.DBConcurrencyException
                    DebtPlus.Svc.Common.ErrorHandling.HandleErrors(ex)

                    ' Something happend in trying to talk to the database.
                Catch ex As SqlClient.SqlException
                    Using gdr As New Repository.GetDataResult()
                        gdr.HandleException(ex)
                        DebtPlus.UI.Common.ErrorHandling.HandleErrors(gdr, "Error reading " + MyTableName + " table")
                    End Using

                Finally
                    System.Windows.Forms.Cursor.Current = current_cursor
                End Try
            End If

            Return tbl
        End Function
#End Region

#Region " Offices "

        ''' <summary>
        ''' Offices information table
        ''' </summary>
        Friend Function OfficesTable() As System.Data.DataTable
            Dim MyTableName As String = "offices"
            Dim tbl As System.Data.DataTable = ds.Tables(MyTableName)

            ' If the table is not found then read the information from the database
            If tbl Is Nothing Then
                Dim current_cursor As Cursor = System.Windows.Forms.Cursor.Current
                Try
                    System.Windows.Forms.Cursor.Current = Cursors.WaitCursor
                    Using cmd As SqlClient.SqlCommand = New SqlCommand
                        With cmd
                            .Connection = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
                            .CommandText = "SELECT [office],[name],[type],[AddressID],[EmailID],[TelephoneID],[AltTelephoneID],[FAXID],[Default],[ActiveFlag],[hud_hcs_id],[TimeZoneID],[nfcc],[default_till_missed],[counselor],[region],[district],[directions],[date_created],[created_by] FROM offices WITH (NOLOCK)"
                            .CommandType = CommandType.Text
                        End With

                        ' Ask the database for the information
                        Using da As New SqlClient.SqlDataAdapter(cmd)
                            da.Fill(ds, MyTableName)
                        End Using
                    End Using

                    tbl = ds.Tables(MyTableName)
                    With tbl
                        If .PrimaryKey.GetUpperBound(0) < 0 Then
                            tbl.PrimaryKey = New System.Data.DataColumn() {.Columns("office")}
                        End If

                        ' Add a relation for the office type
                        Dim rel As New System.Data.DataRelation("offices_office_type", OfficeTypesTable.Columns("office_type"), .Columns("type"))
                        ds.Relations.Add(rel)

                        ' Then add the column to pull the description of the type
                        .Columns.Add("formatted_office_type", GetType(String), "Parent(offices_office_type).description")
                    End With

                    ' When we do an update, the update results in zero rows being updated. IGNORE IT.
                Catch ex As System.Data.DBConcurrencyException
                    DebtPlus.Svc.Common.ErrorHandling.HandleErrors(ex)

                    ' Something happend in trying to talk to the database.
                Catch ex As SqlClient.SqlException
                    Using gdr As New Repository.GetDataResult()
                        gdr.HandleException(ex)
                        DebtPlus.UI.Common.ErrorHandling.HandleErrors(gdr, "Error reading " + MyTableName + " table")
                    End Using

                Finally
                    System.Windows.Forms.Cursor.Current = current_cursor
                End Try
            End If

            Return tbl
        End Function
#End Region

#Region " Counselors "

        ''' <summary>
        ''' Counselors information table
        ''' </summary>
        Friend Function CounselorsTable() As System.Data.DataTable
            Dim MyTableName As String = "counselors"
            Dim tbl As System.Data.DataTable = ds.Tables(MyTableName)

            ' If the table is not found then read the information from the database
            If tbl Is Nothing Then
                Dim current_cursor As Cursor = System.Windows.Forms.Cursor.Current
                Try
                    System.Windows.Forms.Cursor.Current = Cursors.WaitCursor
                    Using cmd As SqlClient.SqlCommand = New SqlCommand
                        With cmd
                            .Connection = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
                            .CommandText = "SELECT [Counselor],[NameID],[EmailID],[TelephoneID],[Person],[Manager],[Note],[Default],[ActiveFlag],[Office],[Menu_Level],[Color],[Image],[SSN],[billing_mode],[rate],[emp_start_date],[emp_end_date],[HUD_id],[CaseCounter],[RxOfficeCounselorID],[date_created],[created_by] FROM counselors WITH (NOLOCK)"
                            .CommandType = CommandType.Text
                        End With

                        ' Ask the database for the information
                        Using da As New SqlClient.SqlDataAdapter(cmd)
                            da.Fill(ds, MyTableName)
                        End Using
                    End Using

                    tbl = ds.Tables(MyTableName)
                    With tbl
                        .PrimaryKey = New System.Data.DataColumn() {.Columns("counselor")}
                    End With

                    ' When we do an update, the update results in zero rows being updated. IGNORE IT.
                Catch ex As System.Data.DBConcurrencyException
                    DebtPlus.Svc.Common.ErrorHandling.HandleErrors(ex)

                    ' Something happend in trying to talk to the database.
                Catch ex As SqlClient.SqlException
                    Using gdr As New Repository.GetDataResult()
                        gdr.HandleException(ex)
                        DebtPlus.UI.Common.ErrorHandling.HandleErrors(gdr, "Error reading " + MyTableName + " table")
                    End Using

                Finally
                    System.Windows.Forms.Cursor.Current = current_cursor
                End Try
            End If

            Return tbl
        End Function
#End Region

#Region " Regions "

        ''' <summary>
        ''' region information table
        ''' </summary>
        Friend Function RegionsTable() As System.Data.DataTable
            Dim MyTableName As String = "regions"
            Dim tbl As System.Data.DataTable = ds.Tables(MyTableName)

            ' If the table is not found then read the information from the database
            If tbl Is Nothing Then
                Dim current_cursor As Cursor = System.Windows.Forms.Cursor.Current
                Try
                    System.Windows.Forms.Cursor.Current = Cursors.WaitCursor
                    Using cmd As SqlClient.SqlCommand = New SqlCommand
                        With cmd
                            .Connection = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
                            .CommandText = "SELECT [region],[description],[ManagerName],[default],[ActiveFlag],[created_by],[date_created] FROM regions WITH (NOLOCK)"
                            .CommandType = CommandType.Text
                        End With

                        ' Ask the database for the information
                        Using da As New SqlClient.SqlDataAdapter(cmd)
                            da.Fill(ds, MyTableName)
                        End Using
                    End Using
                    tbl = ds.Tables(MyTableName)

                    ' When we do an update, the update results in zero rows being updated. IGNORE IT.
                Catch ex As System.Data.DBConcurrencyException
                    DebtPlus.Svc.Common.ErrorHandling.HandleErrors(ex)

                    ' Something happend in trying to talk to the database.
                Catch ex As SqlClient.SqlException
                    Using gdr As New Repository.GetDataResult()
                        gdr.HandleException(ex)
                        DebtPlus.UI.Common.ErrorHandling.HandleErrors(gdr, "Error reading " + MyTableName + " table")
                    End Using

                Finally
                    System.Windows.Forms.Cursor.Current = current_cursor
                End Try
            End If

            Return tbl
        End Function
#End Region

#Region " Districts "

        ''' <summary>
        ''' district information table
        ''' </summary>
        Friend Function DistrictsTable() As System.Data.DataTable
            Dim MyTableName As String = "districts"
            Dim tbl As System.Data.DataTable = ds.Tables(MyTableName)

            ' If the table is not found then read the information from the database
            If tbl Is Nothing Then
                Dim current_cursor As Cursor = System.Windows.Forms.Cursor.Current
                Try
                    System.Windows.Forms.Cursor.Current = Cursors.WaitCursor
                    Using cmd As SqlClient.SqlCommand = New SqlCommand
                        With cmd
                            .Connection = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
                            .CommandText = "SELECT [district],[description],[ManagerName],[Default],[ActiveFlag],[created_by],[date_created] FROM districts WITH (NOLOCK)"
                            .CommandType = CommandType.Text
                        End With

                        ' Ask the database for the information
                        Using da As New SqlClient.SqlDataAdapter(cmd)
                            da.Fill(ds, MyTableName)
                        End Using
                    End Using
                    tbl = ds.Tables(MyTableName)

                    ' When we do an update, the update results in zero rows being updated. IGNORE IT.
                Catch ex As System.Data.DBConcurrencyException
                    DebtPlus.Svc.Common.ErrorHandling.HandleErrors(ex)

                    ' Something happend in trying to talk to the database.
                Catch ex As SqlClient.SqlException
                    Using gdr As New Repository.GetDataResult()
                        gdr.HandleException(ex)
                        DebtPlus.UI.Common.ErrorHandling.HandleErrors(gdr, "Error reading " + MyTableName + " table")
                    End Using

                Finally
                    System.Windows.Forms.Cursor.Current = current_cursor
                End Try
            End If

            Return tbl
        End Function
#End Region

#Region " counselor_attributes "

        ''' <summary>
        ''' counselor_attributes information table
        ''' </summary>
        Friend Function CounselorHasAttribute(ByVal Counselor As System.Int32, ByVal Attribute As System.Int32) As Boolean
            Dim answer As Boolean = False
            Dim tbl As System.Data.DataTable = CounselorAttributesTable()
            If tbl IsNot Nothing Then
                Dim rows() As System.Data.DataRow = tbl.Select(String.Format("[counselor]={0:f0} AND [attribute]={1:f0}", Counselor, Attribute), String.Empty)
                If rows.GetUpperBound(0) >= 0 Then
                    answer = True
                End If
            End If
            Return answer
        End Function

        ''' <summary>
        ''' counselor_attributes information table
        ''' </summary>
        Friend Function CounselorAttributesTable() As System.Data.DataTable
            Dim MyTableName As String = "CounselorAttributes"
            Dim tbl As System.Data.DataTable = ds.Tables(MyTableName)

            ' If the table is not found then read the information from the database
            If tbl Is Nothing Then
                Dim current_cursor As Cursor = System.Windows.Forms.Cursor.Current
                Try
                    System.Windows.Forms.Cursor.Current = Cursors.WaitCursor
                    Using cmd As SqlClient.SqlCommand = New SqlCommand
                        With cmd
                            .Connection = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
                            .CommandText = "SELECT oID,[counselor],[Attribute] FROM counselor_attributes WITH (NOLOCK)"
                            .CommandType = CommandType.Text
                        End With

                        ' Ask the database for the information
                        Using da As New SqlClient.SqlDataAdapter(cmd)
                            da.Fill(ds, MyTableName)
                        End Using
                    End Using
                    tbl = ds.Tables(MyTableName)
                    tbl.PrimaryKey = New System.Data.DataColumn() {tbl.Columns("oID")}

                    ' When we do an update, the update results in zero rows being updated. IGNORE IT.
                Catch ex As System.Data.DBConcurrencyException
                    DebtPlus.Svc.Common.ErrorHandling.HandleErrors(ex)

                    ' Something happend in trying to talk to the database.
                Catch ex As SqlClient.SqlException
                    Using gdr As New Repository.GetDataResult()
                        gdr.HandleException(ex)
                        DebtPlus.UI.Common.ErrorHandling.HandleErrors(gdr, "Error reading " + MyTableName + " table")
                    End Using

                Finally
                    System.Windows.Forms.Cursor.Current = current_cursor
                End Try
            End If

            Return tbl
        End Function
#End Region

#Region " attributetypes "

        ''' <summary>
        ''' ApptTypes information table
        ''' </summary>
        Friend Function AttributeTypesTable() As System.Data.DataTable
            Dim MyTableName As String = "AttributeTypes"
            Dim tbl As System.Data.DataTable = ds.Tables(MyTableName)

            ' If the table is not found then read the information from the database
            If tbl Is Nothing Then
                Dim current_cursor As Cursor = System.Windows.Forms.Cursor.Current
                Try
                    System.Windows.Forms.Cursor.Current = Cursors.WaitCursor
                    Using cmd As SqlClient.SqlCommand = New SqlCommand
                        With cmd
                            .Connection = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
                            .CommandText = "SELECT oID,[Grouping],[Attribute],[ActiveFlag] FROM AttributeTypes WITH (NOLOCK)"
                            .CommandType = CommandType.Text
                        End With

                        ' Ask the database for the information
                        Using da As New SqlClient.SqlDataAdapter(cmd)
                            da.Fill(ds, MyTableName)
                        End Using
                    End Using
                    tbl = ds.Tables(MyTableName)
                    tbl.PrimaryKey = New System.Data.DataColumn() {tbl.Columns("oID")}

                    ' When we do an update, the update results in zero rows being updated. IGNORE IT.
                Catch ex As System.Data.DBConcurrencyException
                    DebtPlus.Svc.Common.ErrorHandling.HandleErrors(ex)

                    ' Something happend in trying to talk to the database.
                Catch ex As SqlClient.SqlException
                    Using gdr As New Repository.GetDataResult()
                        gdr.HandleException(ex)
                        DebtPlus.UI.Common.ErrorHandling.HandleErrors(gdr, "Error reading " + MyTableName + " table")
                    End Using

                Finally
                    System.Windows.Forms.Cursor.Current = current_cursor
                End Try
            End If

            Return tbl
        End Function

        ''' <summary>
        ''' Find the attribute that reflects "COUNSELOR"
        ''' </summary>
        Friend Function GetCounselorAttributeID() As System.Int32
            Dim answer As System.Int32 = -1
            Dim tbl As System.Data.DataTable = AttributeTypesTable()
            If tbl IsNot Nothing Then
                Dim rows() As System.Data.DataRow = tbl.Select("[grouping]='ROLE' AND [attribute]='COUNSELOR'", String.Empty)
                If rows.GetUpperBound(0) >= 0 Then
                    answer = DebtPlus.Utils.Nulls.DInt(rows(0)("oID"))
                End If
            End If
            Return answer
        End Function
#End Region

#Region " ApptTypes "

        ''' <summary>
        ''' ApptTypes information table
        ''' </summary>
        Friend Function ApptTypesTable() As System.Data.DataTable
            Dim MyTableName As String = "appt_types"
            Dim tbl As System.Data.DataTable = ds.Tables(MyTableName)

            ' If the table is not found then read the information from the database
            If tbl Is Nothing Then
                Dim current_cursor As Cursor = System.Windows.Forms.Cursor.Current
                Try
                    System.Windows.Forms.Cursor.Current = Cursors.WaitCursor
                    Using cmd As SqlClient.SqlCommand = New SqlCommand
                        With cmd
                            .Connection = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
                            .CommandText = "SELECT [appt_type],[appt_name],[appt_duration],[default],[housing],[credit],[bankruptcy],[initial_appt],[contact_type],[missed_retention_event],[create_letter],[reschedule_letter],[cancel_letter],[bankruptcy_class],[date_created],[created_by] FROM appt_types WITH (NOLOCK)"
                            .CommandType = CommandType.Text
                        End With

                        ' Ask the database for the information
                        Using da As New SqlClient.SqlDataAdapter(cmd)
                            da.Fill(ds, MyTableName)
                        End Using
                    End Using
                    tbl = ds.Tables(MyTableName)
                    tbl.PrimaryKey = New System.Data.DataColumn() {tbl.Columns("appt_type")}

                    ' When we do an update, the update results in zero rows being updated. IGNORE IT.
                Catch ex As System.Data.DBConcurrencyException
                    DebtPlus.Svc.Common.ErrorHandling.HandleErrors(ex)

                    ' Something happend in trying to talk to the database.
                Catch ex As SqlClient.SqlException
                    Using gdr As New Repository.GetDataResult()
                        gdr.HandleException(ex)
                        DebtPlus.UI.Common.ErrorHandling.HandleErrors(gdr, "Error reading " + MyTableName + " table")
                    End Using

                Finally
                    System.Windows.Forms.Cursor.Current = current_cursor
                End Try
            End If

            Return tbl
        End Function
#End Region
    End Class
End Namespace
