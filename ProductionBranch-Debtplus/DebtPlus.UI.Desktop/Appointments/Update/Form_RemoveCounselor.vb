#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}

#End Region

Imports DevExpress.XtraEditors.Controls
Imports DebtPlus.Utils
Imports System.Data.SqlClient
Imports System.Windows.Forms

Namespace Appointments.Update
    Friend Class Form_RemoveCounselor

        ' Dataset for the counselors list
        Private ds As New DataSet("ds")

        Private ap As ArgParser
        Public Sub New(ByVal ap As ArgParser)
            MyClass.New()
            Me.ap = ap
        End Sub

        Public Sub New()
            MyBase.New()
            InitializeComponent()
            AddHandler Me.Load, AddressOf Form_Load
            AddHandler CheckedListBoxControl1.ItemChecking, AddressOf CheckedListBoxControl1_ItemChecking
            AddHandler SimpleButton_OK.Click, AddressOf SimpleButton_OK_Click
        End Sub

        Private Sub Form_Load(ByVal sender As Object, ByVal e As EventArgs)

            ' Read the list of counselors
            Dim tbl As DataTable = ds.Tables("lst_counselors")
            If tbl Is Nothing Then
                Dim cn As SqlConnection = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
                Try
                    cn.Open()
                    Using cmd As SqlCommand = New SqlCommand
                        With cmd
                            .Connection = cn
                            .CommandText = "lst_counselors"
                            .CommandType = CommandType.StoredProcedure
                        End With

                        Using da As New SqlDataAdapter(cmd)
                            da.Fill(ds, "lst_counselors")
                            tbl = ds.Tables("lst_counselors")
                        End Using
                    End Using

                Catch ex As SqlException
                    Using gdr As New Repository.GetDataResult()
                        gdr.HandleException(ex)
                        DebtPlus.UI.Common.ErrorHandling.HandleErrors(gdr, String.Format("Error reading lst_counselors table"))
                    End Using

                Finally
                    If cn IsNot Nothing Then cn.Dispose()
                End Try
            End If

            With CheckedListBoxControl1
                With .Items
                    .Clear()
                    For Each row As DataRow In tbl.Rows
                        .Add(New CheckedListBoxItem(row("item_key"), Convert.ToString(row("description")), CheckState.Unchecked))
                    Next
                End With
            End With

            ' Define the starting date for the date range
            DateNavigator1.DateTime = Now.Date

            ' Empty the reason
            TextEdit_reason.EditValue = String.Empty

            ' Prevent the OK button from being clicked until a counselor is checked
            SimpleButton_OK.Enabled = False
        End Sub

        Private Sub CheckedListBoxControl1_ItemChecking(ByVal sender As Object, ByVal e As ItemCheckingEventArgs)

            ' If the item is being checked then enable the ok button
            If e.NewValue = CheckState.Checked Then
                SimpleButton_OK.Enabled = True
                Return
            End If

            ' Count the number of checked items. If < 2 then disable the button
            Dim ItemCount As Int32 = 0
            For Each item As CheckedListBoxItem In CheckedListBoxControl1.Items
                If item.CheckState = CheckState.Checked Then
                    ItemCount += 1
                End If
            Next
            SimpleButton_OK.Enabled = (ItemCount >= 2)  ' since we are clearing a check state, there can't be just one item.
        End Sub

        Private Sub SimpleButton_OK_Click(ByVal sender As Object, ByVal e As EventArgs)

            ' Process each date in the list. Find the appointment time for that date
            Dim StartingDate As DateTime = DateNavigator1.SelectionStart.Date
            Dim EndingDate As DateTime = DateNavigator1.SelectionEnd.Date
            ap.ReadItems(StartingDate, EndingDate, Nothing)

            ' Find the appointment times that are effected
            Dim SelectionClause As String = String.Format("[start_time] >= '{0:d}' AND [start_time] < '{1:d}'", StartingDate, EndingDate.AddDays(1))
            Using vue As New DataView(ap.ds.Tables("appt_times"), SelectionClause, String.Empty, DataViewRowState.CurrentRows)
                For Each drv As DataRowView In vue
                    ProcessTime(drv)
                Next
            End Using
        End Sub

        Private Sub ProcessTime(ByRef drv As DataRowView)
            For Each item As CheckedListBoxItem In CheckedListBoxControl1.Items
                If item.CheckState = CheckState.Checked Then
                    Dim Counselor As Int32 = Convert.ToInt32(item.Value)
                    If Counselor > 0 Then
                        ProcessCounselor(drv, Counselor)
                    End If
                End If
            Next
        End Sub

        Private Sub ProcessCounselor(ByRef drv As DataRowView, ByVal Counselor As Int32)

            ' Find the counselor in the list for this appointment time
            Dim SelectionClause As String = String.Format("[appt_time]={0:f0} AND [counselor]={1:f0}", Convert.ToInt32(drv("appt_time")), Counselor)
            Dim rows() As DataRow = ap.ds.Tables("appt_counselors").Select(SelectionClause)
            If rows.GetUpperBound(0) >= 0 Then
                Dim row As DataRow = rows(0)
                row.BeginEdit()
                row("inactive") = True
                row("inactive_reason") = Convert.ToString(TextEdit_reason.Text.Trim())
                row.EndEdit()

                ' Cancel any client appointment assigned to this counselor for the time period
                CancelClientAppointment(Convert.ToInt32(drv("appt_time")), Counselor)
            End If
        End Sub

        Private Sub CancelClientAppointment(ByVal appt_time As Int32, ByVal counselor As Int32)
            Dim SelectionClause As String = String.Format("[appt_time]={0:f0} AND [counselor]={1:f0}", appt_time, counselor)
            Dim rows() As DataRow = ap.ds.Tables("client_appointments").Select(SelectionClause, String.Empty)
            If rows.GetUpperBound(0) > 0 Then
                rows(0).Delete()
            End If
        End Sub
    End Class
End Namespace
