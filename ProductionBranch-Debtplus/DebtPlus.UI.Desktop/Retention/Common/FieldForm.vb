#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}
#End Region

Option Compare Binary
Option Explicit On 
Option Strict On

Namespace Retention.Common
    Friend Class FieldForm
        Inherits DebtPlus.Data.Forms.DebtPlusForm

        Protected NewFieldItem As FieldSelection
        Protected IsSearch As Boolean

        Public Sub New(ByVal search As Boolean)
            MyBase.New()
            InitializeComponent()
            AddHandler Me.Load, AddressOf FieldForm_Load
            AddHandler Button_OK.Click, AddressOf Button_OK_Click
            IsSearch = search
        End Sub

        Public Sub New(ByVal search As Boolean, ByRef NewFieldItem As FieldSelection)
            MyBase.New()
            InitializeComponent()
            Me.NewFieldItem = NewFieldItem
            IsSearch = search
        End Sub

#Region " Windows Form Designer generated code "

        'Form overrides dispose to clean up the component list.
        Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
            If disposing Then
                If components IsNot Nothing Then
                    components.Dispose()
                End If
            End If
            MyBase.Dispose(disposing)
        End Sub

        'Required by the Windows Form Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Windows Form Designer
        'It can be modified using the Windows Form Designer.  
        'Do not modify it using the code editor.
        Protected Friend WithEvents LookUpEdit1 As DevExpress.XtraEditors.LookUpEdit
        Protected Friend WithEvents Button_OK As DevExpress.XtraEditors.SimpleButton
        Protected Friend WithEvents Button_Cancel As DevExpress.XtraEditors.SimpleButton
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
            Me.LookUpEdit1 = New DevExpress.XtraEditors.LookUpEdit
            Me.Button_OK = New DevExpress.XtraEditors.SimpleButton
            Me.Button_Cancel = New DevExpress.XtraEditors.SimpleButton

            CType(Me.LookUpEdit1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.SuspendLayout()

            '
            'LookUpEdit1
            '
            Me.LookUpEdit1.Location = New System.Drawing.Point(8, 8)
            Me.LookUpEdit1.Name = "LookUpEdit1"
            '
            'LookUpEdit1.Properties
            '
            Me.LookUpEdit1.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.LookUpEdit1.Properties.Columns.AddRange(New DevExpress.XtraEditors.Controls.LookUpColumnInfo() {New DevExpress.XtraEditors.Controls.LookUpColumnInfo("description", "Description", 20, DevExpress.Utils.FormatType.None, "", True, DevExpress.Utils.HorzAlignment.Default, DevExpress.Data.ColumnSortOrder.Ascending)})
            Me.LookUpEdit1.Properties.NullText = ""
            Me.LookUpEdit1.Properties.ShowFooter = False
            Me.LookUpEdit1.Properties.ShowHeader = False
            Me.LookUpEdit1.Properties.ShowLines = False
            Me.LookUpEdit1.Size = New System.Drawing.Size(176, 20)
            Me.LookUpEdit1.TabIndex = 0
            Me.LookUpEdit1.ToolTipController = Me.ToolTipController1
            Me.LookUpEdit1.Properties.SortColumnIndex = 1
            '
            'Button_OK
            '
            Me.Button_OK.DialogResult = System.Windows.Forms.DialogResult.OK
            Me.Button_OK.Enabled = False
            Me.Button_OK.Location = New System.Drawing.Point(200, 8)
            Me.Button_OK.Name = "Button_OK"

            Me.Button_OK.TabIndex = 1
            Me.Button_OK.Text = "&OK"
            Me.Button_OK.ToolTipController = Me.ToolTipController1
            '
            'Button_Cancel
            '
            Me.Button_Cancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
            Me.Button_Cancel.Location = New System.Drawing.Point(200, 40)
            Me.Button_Cancel.Name = "Button_Cancel"

            Me.Button_Cancel.TabIndex = 2
            Me.Button_Cancel.Text = "&Cancel"
            Me.Button_Cancel.ToolTipController = Me.ToolTipController1
            '
            'FieldForm
            '
            Me.AcceptButton = Me.Button_OK
            Me.AutoScaleBaseSize = New System.Drawing.Size(5, 14)
            Me.CancelButton = Me.Button_Cancel
            Me.ClientSize = New System.Drawing.Size(292, 78)
            Me.Controls.Add(Me.Button_Cancel)
            Me.Controls.Add(Me.Button_OK)
            Me.Controls.Add(Me.LookUpEdit1)
            Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow
            Me.Name = "FieldForm"
            Me.ShowInTaskbar = False
            Me.StartPosition = System.Windows.Forms.FormStartPosition.Manual
            Me.ToolTipController1.SetSuperTip(Me, Nothing)
            Me.Text = "Select Item"

            CType(Me.LookUpEdit1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            Me.ResumeLayout(False)

        End Sub

#End Region

        Protected Sub FieldForm_Load(ByVal sender As System.Object, ByVal e As System.EventArgs)

            With LookUpEdit1
                With .Properties
                    If IsSearch Then
                        .DataSource = EventsTable.DefaultView
                    Else
                        .DataSource = New System.Data.DataView(EventsTable, "[creation]=True", String.Empty, DataViewRowState.CurrentRows)
                    End If
                    .DisplayMember = "description"
                    .ValueMember = "retention_event"
                End With

                ' Set the value to the first item in the table
                .EditValue = EventsTable().Rows(0)("retention_event")
            End With

            Button_OK.Enabled = Not HasErrors()
        End Sub

        Protected Overridable Sub Button_OK_Click(ByVal sender As Object, ByVal e As System.EventArgs)
            With LookUpEdit1
                NewFieldItem.EventID = CType(.EditValue, Enum_Events)
                NewFieldItem.Caption = Convert.ToString(.Text)
            End With
            DialogResult = System.Windows.Forms.DialogResult.OK
        End Sub

        Protected Overridable Function HasErrors() As Boolean
            Return False
        End Function
    End Class
End Namespace
