Option Compare Binary
Option Explicit On 
Option Strict On

Imports DebtPlus.Utils
Imports System.Data.SqlClient
Imports System.Windows.Forms

Namespace Retention.Common

    Friend Module CommonStorage

        Friend ds As New System.Data.DataSet("ds")

        Friend Function RetentionEventsTable() As System.Data.DataTable
            Dim tbl As System.Data.DataTable = ds.Tables("retention_events")

            If tbl Is Nothing Then
                Dim current_cursor As Cursor = Cursor.Current
                Try
                    Cursor.Current = Cursors.WaitCursor
                    Using cmd As SqlClient.SqlCommand = New SqlCommand
                        With cmd
                            .Connection = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
                            .CommandText = "SELECT retention_event as item_key, description, convert(bit,1) as ActiveFlag, convert(bit,0) as [default], priority, initial_retention_action, expire_type FROM retention_events WITH (NOLOCK) ORDER BY description"
                            .CommandType = CommandType.Text
                        End With

                        Using da As New SqlClient.SqlDataAdapter(cmd)
                            da.Fill(ds, "retention_events")
                        End Using
                    End Using

                    tbl = ds.Tables("retention_events")
                    With tbl
                        .PrimaryKey = New System.Data.DataColumn() {.Columns("retention_event")}
                    End With

                Catch ex As SqlClient.SqlException
                    DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error reading retention_events table")

                Finally
                    Cursor.Current = current_cursor
                End Try
            End If

            Return tbl
        End Function

        Friend Function RetentionActionsTable() As System.Data.DataTable

            Dim tbl As System.Data.DataTable = ds.Tables("retention_actions")
            If tbl Is Nothing Then
                Dim current_cursor As Cursor = Cursor.Current
                Try
                    Cursor.Current = Cursors.WaitCursor
                    Using cmd As SqlClient.SqlCommand = New SqlCommand
                        With cmd
                            .Connection = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
                            .CommandText = "SELECT [retention_action],[description],[letter_code],[date_created],[created_by] FROM retention_actions WITH (NOLOCK) ORDER BY description"
                            .CommandType = CommandType.Text
                        End With

                        Using da As New SqlClient.SqlDataAdapter(cmd)
                            da.Fill(ds, "retention_actions")
                        End Using
                    End Using

                    tbl = ds.Tables("retention_actions")
                    With tbl
                        .PrimaryKey = New System.Data.DataColumn() {.Columns("retention_action")}
                    End With

                Catch ex As SqlClient.SqlException
                    DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error reading retention_actions table")

                Finally
                    Cursor.Current = current_cursor
                End Try
            End If

            Return tbl
        End Function

        Friend Function ActiveStatusTable() As System.Data.DataTable
            Dim tbl As New System.Data.DataTable

            With tbl
                .Columns.Add("active_status", GetType(String))
                .Columns.Add("description", GetType(String))
                .PrimaryKey = New System.Data.DataColumn() {.Columns(0)}

                With .Rows
                    .Add(New Object() {"A", "Active"})
                    .Add(New Object() {"AR", "Active Restart"})
                    .Add(New Object() {"APT", "Appointment"})
                    .Add(New Object() {"CRE", "Created"})
                    .Add(New Object() {"I", "Inactive"})
                    .Add(New Object() {"RDY", "Ready For Review"})
                    .Add(New Object() {"EX", "Terminating"})
                End With
            End With

            Return tbl
        End Function

        Friend Function OfficesTable() As System.Data.DataTable
            Const TableName As String = "offices"
            Dim tbl As System.Data.DataTable = ds.Tables(TableName)
            If tbl Is Nothing Then
                Dim current_cursor As Cursor = Cursor.Current
                Try
                    Cursor.Current = Cursors.WaitCursor
                    Using cmd As SqlClient.SqlCommand = New SqlCommand
                        With cmd
                            .Connection = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
                            .CommandText = "SELECT office as item_key, name as description, ActiveFlag, [default] FROM offices ORDER BY description"
                            .CommandType = CommandType.Text
                        End With

                        Using da As New SqlClient.SqlDataAdapter(cmd)
                            da.Fill(ds, TableName)
                        End Using
                    End Using

                    tbl = ds.Tables(TableName)
                    With tbl
                        If .PrimaryKey.GetUpperBound(0) < 0 Then
                            .PrimaryKey = New System.Data.DataColumn() {.Columns("item_key")}
                        End If
                    End With

                Catch ex As SqlClient.SqlException
                    DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error reading offices table")

                Finally
                    Cursor.Current = current_cursor
                End Try
            End If

            Return tbl
        End Function

        Friend Function RegionsTable() As System.Data.DataTable
            Dim tbl As System.Data.DataTable = ds.Tables("regions")
            If tbl Is Nothing Then
                Dim current_cursor As Cursor = Cursor.Current
                Try
                    Cursor.Current = Cursors.WaitCursor
                    Using cmd As SqlClient.SqlCommand = New SqlCommand
                        With cmd
                            .Connection = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
                            .CommandText = "SELECT region as item_key, description, convert(bit,1) as ActiveFlag, convert(bit,0) as [default] FROM regions ORDER BY description"
                            .CommandType = CommandType.Text
                        End With

                        Using da As New SqlClient.SqlDataAdapter(cmd)
                            da.Fill(ds, "regions")
                        End Using
                    End Using

                    tbl = ds.Tables("regions")
                    With tbl
                        .PrimaryKey = New System.Data.DataColumn() {.Columns("region")}
                    End With

                Catch ex As SqlClient.SqlException
                    DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error reading regions table")

                Finally
                    Cursor.Current = current_cursor
                End Try
            End If

            Return tbl
        End Function

        Friend Function CounselorsTable() As System.Data.DataTable
            Const TableName As String = "counselors"
            Dim tbl As System.Data.DataTable = ds.Tables(TableName)
            If tbl Is Nothing Then
                Dim current_cursor As Cursor = Cursor.Current
                Try
                    Cursor.Current = Cursors.WaitCursor
                    Using cmd As SqlClient.SqlCommand = New SqlCommand
                        With cmd
                            .Connection = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
                            .CommandText = "SELECT counselor as item_key, name as description, ActiveFlag, [default] FROM view_counselors ORDER BY name"
                            .CommandType = CommandType.Text
                        End With

                        Using da As New SqlClient.SqlDataAdapter(cmd)
                            da.Fill(ds, TableName)
                        End Using
                    End Using

                    tbl = ds.Tables(TableName)
                    With tbl
                        If .PrimaryKey.GetUpperBound(0) < 0 Then
                            .PrimaryKey = New System.Data.DataColumn() {.Columns("item_key")}
                        End If
                    End With

                Catch ex As SqlClient.SqlException
                    DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error reading counselors table")

                Finally
                    Cursor.Current = current_cursor
                End Try
            End If

            Return tbl
        End Function

        Friend Function CSRTable() As System.Data.DataTable
            Const TableName As String = "csrs"
            Dim tbl As System.Data.DataTable = ds.Tables(TableName)
            If tbl Is Nothing Then
                Dim current_cursor As Cursor = Cursor.Current
                Try
                    Cursor.Current = Cursors.WaitCursor
                    Using cmd As SqlClient.SqlCommand = New SqlCommand
                        With cmd
                            .Connection = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
                            .CommandText = "SELECT counselor as item_key, name as description, ActiveFlag, [default] FROM view_csrs ORDER BY name"
                            .CommandType = CommandType.Text
                        End With

                        Using da As New SqlClient.SqlDataAdapter(cmd)
                            da.Fill(ds, TableName)
                        End Using
                    End Using

                    tbl = ds.Tables(TableName)
                    With tbl
                        If .PrimaryKey.GetUpperBound(0) < 0 Then
                            .PrimaryKey = New System.Data.DataColumn() {.Columns("item_key")}
                        End If
                    End With

                Catch ex As SqlClient.SqlException
                    DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error reading CSRs table")

                Finally
                    Cursor.Current = current_cursor
                End Try
            End If

            Return tbl
        End Function
    End Module
End Namespace
