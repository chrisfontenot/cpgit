#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}
#End Region

Option Compare Binary
Option Explicit On 
Option Strict On

Imports System.Windows.Forms

Namespace Retention.Common
    Friend Class RelationForm
        Inherits DebtPlus.Data.Forms.DebtPlusForm

        Private FirstField As FieldSelection
        Private IsSearch As Boolean

        Public Sub New(ByVal search As Boolean)
            MyBase.New()
            InitializeComponent()
            AddHandler Me.Load, AddressOf RelationForm_Load
            AddHandler Button_New.Click, AddressOf Button_New_Click
            AddHandler Button_Del.Click, AddressOf Button_Del_Click
            IsSearch = search
        End Sub

        Public Sub New(ByVal search As Boolean, ByVal FirstField As FieldSelection)
            MyClass.New(search)
            Me.FirstField = FirstField
        End Sub

#Region " Windows Form Designer generated code "

        'Form overrides dispose to clean up the component list.
        Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
            If disposing Then
                If components IsNot Nothing Then
                    components.Dispose()
                End If
            End If
            MyBase.Dispose(disposing)
        End Sub

        'Required by the Windows Form Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Windows Form Designer
        'It can be modified using the Windows Form Designer.  
        'Do not modify it using the code editor.
        Friend WithEvents XtraTabControl1 As DevExpress.XtraTab.XtraTabControl
        Friend WithEvents XtraTabPage1 As DevExpress.XtraTab.XtraTabPage
        Friend WithEvents Button_New As DevExpress.XtraEditors.SimpleButton
        Friend WithEvents Button_Del As DevExpress.XtraEditors.SimpleButton
        Friend WithEvents Button_OK As DevExpress.XtraEditors.SimpleButton
        Friend WithEvents Button_Cancel As DevExpress.XtraEditors.SimpleButton
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
            Me.XtraTabControl1 = New DevExpress.XtraTab.XtraTabControl
            Me.XtraTabPage1 = New DevExpress.XtraTab.XtraTabPage
            Me.Button_New = New DevExpress.XtraEditors.SimpleButton
            Me.Button_Del = New DevExpress.XtraEditors.SimpleButton
            Me.Button_OK = New DevExpress.XtraEditors.SimpleButton
            Me.Button_Cancel = New DevExpress.XtraEditors.SimpleButton
            CType(Me.DxErrorProvider1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.XtraTabControl1, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.XtraTabControl1.SuspendLayout()
            Me.SuspendLayout()
            '
            'XtraTabControl1
            '
            Me.XtraTabControl1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                        Or System.Windows.Forms.AnchorStyles.Left) _
                        Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.XtraTabControl1.Location = New System.Drawing.Point(0, 0)
            Me.XtraTabControl1.Name = "XtraTabControl1"
            Me.XtraTabControl1.SelectedTabPage = Me.XtraTabPage1
            Me.XtraTabControl1.Size = New System.Drawing.Size(336, 264)
            Me.XtraTabControl1.TabIndex = 0
            Me.XtraTabControl1.TabPages.AddRange(New DevExpress.XtraTab.XtraTabPage() {Me.XtraTabPage1})
            '
            'XtraTabPage1
            '
            Me.XtraTabPage1.Name = "XtraTabPage1"
            Me.XtraTabPage1.Size = New System.Drawing.Size(329, 235)
            Me.XtraTabPage1.Text = "XtraTabPage1"
            '
            'Button_New
            '
            Me.Button_New.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.Button_New.Location = New System.Drawing.Point(344, 8)
            Me.Button_New.Name = "Button_New"
            Me.Button_New.Size = New System.Drawing.Size(75, 23)
            Me.Button_New.TabIndex = 1
            Me.Button_New.Text = "&New"
            '
            'Button_Del
            '
            Me.Button_Del.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.Button_Del.CausesValidation = False
            Me.Button_Del.Location = New System.Drawing.Point(344, 48)
            Me.Button_Del.Name = "Button_Del"
            Me.Button_Del.Size = New System.Drawing.Size(75, 23)
            Me.Button_Del.TabIndex = 2
            Me.Button_Del.Text = "&Delete"
            '
            'Button_OK
            '
            Me.Button_OK.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
            Me.Button_OK.DialogResult = System.Windows.Forms.DialogResult.OK
            Me.Button_OK.Location = New System.Drawing.Point(16, 272)
            Me.Button_OK.Name = "Button_OK"
            Me.Button_OK.Size = New System.Drawing.Size(75, 23)
            Me.Button_OK.TabIndex = 3
            Me.Button_OK.Text = "&OK"
            '
            'Button_Cancel
            '
            Me.Button_Cancel.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
            Me.Button_Cancel.CausesValidation = False
            Me.Button_Cancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
            Me.Button_Cancel.Location = New System.Drawing.Point(112, 272)
            Me.Button_Cancel.Name = "Button_Cancel"
            Me.Button_Cancel.Size = New System.Drawing.Size(75, 23)
            Me.Button_Cancel.TabIndex = 4
            Me.Button_Cancel.Text = "&Cancel"
            '
            'RelationForm
            '
            Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
            Me.ClientSize = New System.Drawing.Size(432, 302)
            Me.Controls.Add(Me.Button_Cancel)
            Me.Controls.Add(Me.Button_OK)
            Me.Controls.Add(Me.Button_Del)
            Me.Controls.Add(Me.Button_New)
            Me.Controls.Add(Me.XtraTabControl1)
            Me.LookAndFeel.UseDefaultLookAndFeel = True
            Me.Name = "RelationForm"
            Me.Text = "Retention Selection Criteria"
            CType(Me.DxErrorProvider1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.XtraTabControl1, System.ComponentModel.ISupportInitialize).EndInit()
            Me.XtraTabControl1.ResumeLayout(False)
            Me.ResumeLayout(False)

        End Sub

#End Region

        Private Sub RelationForm_Load(ByVal sender As Object, ByVal e As System.EventArgs)

            ' Define the first tab control
            Dim Page As DevExpress.XtraTab.XtraTabPage = XtraTabControl1.TabPages(0)
            With Page
                .Text = FirstField.Caption
                Dim ctl As New FrameControl(FirstField)
                AddHandler ctl.FrameUpdated, AddressOf FrameUpdatedEvent
                .Controls.Add(ctl)
            End With

        End Sub

        Private Sub Button_New_Click(ByVal sender As Object, ByVal e As System.EventArgs)
            Static PageSequence As System.Int32 = 1

            Dim NextField As New FieldSelection
            Using FieldChoice As New FieldForm(IsSearch, NextField)

                With FieldChoice
                    .Location = New System.Drawing.Point(Me.Left + (Me.Width - .Width) \ 2, Me.Top + (Me.Height - .Height) \ 2)

                    ' If successful, add a new page to the control group.
                    If .ShowDialog() = System.Windows.Forms.DialogResult.OK Then

                        ' Create the tab page
                        Dim Page As New DevExpress.XtraTab.XtraTabPage
                        PageSequence += 1
                        Page.Name = String.Format("XtraTabPage{0:d}", PageSequence)
                        Page.Text = NextField.Caption

                        ' In that page, add a new frame control to process the input fields
                        Dim ctl As New FrameControl(NextField)
                        AddHandler ctl.FrameUpdated, AddressOf FrameUpdatedEvent
                        ctl.Name = String.Format("FrameControl{0:d}", PageSequence)
                        ctl.Dock = DockStyle.Fill
                        Page.Controls.Add(ctl)

                        ' Add the page and select it.
                        XtraTabControl1.TabPages.Add(Page)
                        XtraTabControl1.SelectedTabPage = Page
                    End If
                End With
            End Using
        End Sub

        Private Sub Button_Del_Click(ByVal sender As Object, ByVal e As System.EventArgs)

            ' Remove the page from the tab
            Dim Page As DevExpress.XtraTab.XtraTabPage = XtraTabControl1.SelectedTabPage
            If Page IsNot Nothing Then
                XtraTabControl1.TabPages.Remove(Page)
            End If

            ' Disable the OK and DELETE buttons if there are no more pages.
            Button_OK.Enabled = SelectionClause.Valid
            Button_Del.Enabled = XtraTabControl1.TabPages.Count > 0
        End Sub

        Private Sub FrameUpdatedEvent(ByVal Sender As Object, ByVal e As System.EventArgs)
            Button_OK.Enabled = SelectionClause.Valid
        End Sub

        Friend Function SelectionClause() As SelectionClause
            Dim answer As New SelectionClause

            ' Process the tab pages. Find the controls from the pages and envoke their "GetSelectionClause" routine to build the selection string.
            For Each Page As DevExpress.XtraTab.XtraTabPage In XtraTabControl1.TabPages
                Dim ctl As FrameControl = CType(Page.Controls(0), FrameControl)
                Dim item As FieldSelection = ctl.CurrentItem

                ' From the item, find the selection clause
                Dim Clause As SelectionClause = item.GetSelectionClause

                ' IF the clause is not valid then abort and return an empty condition as an error
                If Not Clause.Valid Then
                    answer.Valid = False
                    Return answer
                End If

                ' Combine the tables that we need
                answer.UseClientDepositTable = answer.UseClientDepositTable OrElse Clause.UseClientDepositTable
                answer.UseClientRetentionEventsTable = answer.UseClientRetentionEventsTable OrElse Clause.UseClientRetentionEventsTable
                answer.SelectionString += " AND " + Clause.SelectionString
            Next

            ' Remove the leading " AND " item
            If answer.SelectionString.Length > 0 Then answer.SelectionString = answer.SelectionString.Substring(5)
            answer.Valid = True
            Return answer
        End Function
    End Class
End Namespace
