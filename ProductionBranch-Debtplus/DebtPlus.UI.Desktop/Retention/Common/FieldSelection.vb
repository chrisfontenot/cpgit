#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}
#End Region

Option Compare Binary
Option Explicit On 
Option Strict On

Namespace Retention.Common
    Friend Class FieldSelection
        Inherits Object

        Friend EventID As Enum_Events
        Friend Caption As String = String.Empty
        Friend Relation As Enum_Relations = Enum_Relations.RELATION_IS_ANY_VALUE
        Friend Values As New System.Collections.ArrayList
        Friend FieldName As String = String.Empty
        Friend TabID As String = String.Empty
        Friend Field1 As New FieldItem
        Friend Field2 As New FieldItem
        Private Clause As SelectionClause

        Public Sub New()
            MyBase.new()
        End Sub

        ''' <summary>
        ''' Return a selection clause for the field and values
        ''' </summary>
        Private Function Encode_Value(ByVal Field As FieldItem, Optional ByVal time_value As String = "00:00:00") As String
            Dim answer As String = String.Empty

            If Field Is Nothing OrElse Field.ItemValue Is Nothing Then
                Clause.Valid = False
            Else
                Select Case EventID
                    Case Enum_Events.EVENT_ACTIVE_STATUS   ' Strings
                        answer = "'" + Convert.ToString(Field.ItemValue).Replace("'", "''") + "'"

                    Case Enum_Events.EVENT_ACTION_DATE, Enum_Events.EVENT_EXPECTED_DEPOSIT_DATE, Enum_Events.EVENT_LAST_DEPOSIT_DATE  ' Dates
                        answer = "'" + Convert.ToDateTime(Field.ItemValue).ToShortDateString + " " + time_value + "'"

                    Case Enum_Events.EVENT_COUNSELOR, Enum_Events.EVENT_CSR, Enum_Events.EVENT_REGION, Enum_Events.EVENT_OFFICE, Enum_Events.EVENT_RETENTION_EVENT  ' Table keys
                        answer = Convert.ToInt32(Field.ItemValue).ToString()

                    Case Else
                        Debug.Assert(False, "Invalid EventID")
                End Select
            End If

            Return answer
        End Function

        ''' <summary>
        ''' Return a selection clause for the field and values
        ''' </summary>
        Private Function is_one_of() As String
            Dim strClause As String = String.Empty

            If Values.Count = 0 Then
                Clause.Valid = False
            Else
                For Each item As FieldItem In Values
                    strClause += "," + Encode_Value(item)
                Next item

                strClause = "(" + strClause.Substring(1) + ")"
            End If

            Return strClause
        End Function

        ''' <summary>
        ''' Return a selection clause for the field and values
        ''' </summary>
        Private Function Where_Clause(ByVal strField As String) As String
            Dim answer As String = String.Empty

            Select Case Relation
                Case Enum_Relations.RELATION_IS_ANY_VALUE
                    answer = strField + " is not null"

                Case Enum_Relations.RELATION_IS_BETWEEEN
                    answer = strField + " BETWEEN " + Encode_Value(Field1) + " AND " + Encode_Value(Field2, "23:59:59")

                Case Enum_Relations.RELATION_IS_EQUAL
                    answer = strField + "=" + Encode_Value(Field1)

                Case Enum_Relations.RELATION_IS_NOT_EQUAL
                    answer = strField + "<>" + Encode_Value(Field1)

                Case Enum_Relations.RELATION_IS_GREATER_THAN
                    answer = strField + ">" + Encode_Value(Field1)

                Case Enum_Relations.RELATION_IS_GREATER_THAN_OR_EQUAL
                    answer = strField + ">=" + Encode_Value(Field1)

                Case Enum_Relations.RELATION_IS_LESS_THAN
                    answer = strField + "<" + Encode_Value(Field1)

                Case Enum_Relations.RELATION_IS_LESS_THAN_OR_EQUAL
                    answer = strField + "<=" + Encode_Value(Field1)

                Case Enum_Relations.RELATION_IS_NOT_BETWEEN
                    answer = strField + " NOT BETWEEN " + Encode_Value(Field1) + " AND " + Encode_Value(Field2, "23:59:59")

                Case Enum_Relations.RELATION_IS_NOT_ONE_OF
                    answer = strField + " NOT IN " + is_one_of()

                Case Enum_Relations.RELATION_IS_ONE_OF
                    answer = strField + " IN " + is_one_of()

                Case Else
                    Debug.Assert(False, "Invalid Relation")
            End Select

            Return answer
        End Function

        Friend Function GetSelectionClause() As SelectionClause
            Clause = New SelectionClause

            Try
                Select Case EventID
                    Case Enum_Events.EVENT_ACTIVE_STATUS
                        Clause.SelectionString = Where_Clause("c.active_status")

                    Case Enum_Events.EVENT_EXPECTED_DEPOSIT_DATE
                        Clause.SelectionString = Where_Clause("cd.deposit_date")
                        Clause.UseClientDepositTable = True

                    Case Enum_Events.EVENT_LAST_DEPOSIT_DATE
                        Clause.SelectionString = Where_Clause("c.last_deposit_date")

                    Case Enum_Events.EVENT_OFFICE
                        Clause.SelectionString = Where_Clause("c.office")

                    Case Enum_Events.EVENT_CSR
                        Clause.SelectionString = Where_Clause("c.csr")

                    Case Enum_Events.EVENT_COUNSELOR
                        Clause.SelectionString = Where_Clause("c.counselor")

                    Case Enum_Events.EVENT_REGION
                        Clause.SelectionString = Where_Clause("c.region")

                    Case Enum_Events.EVENT_ACTION_DATE
                        Clause.SelectionString = Where_Clause("vc.last_action_date")
                        Clause.UseClientRetentionEventsTable = True

                    Case Enum_Events.EVENT_RETENTION_EVENT
                        Clause.SelectionString = Where_Clause("vc.retention_event")
                        Clause.UseClientRetentionEventsTable = True

                    Case Else
                        Debug.Assert(False, "Invalid EventID")
                End Select

            Catch ex As Exception
                Clause.Valid = False
            End Try

            Return Clause
        End Function
    End Class
End Namespace
