Option Compare Binary
Option Explicit On 
Option Strict On

Namespace Retention.Common
    Friend Class SelectionClause
        Inherits Object
        Friend SelectionString As String = String.Empty
        Friend UseClientDepositTable As Boolean = False
        Friend UseClientRetentionEventsTable As Boolean = False
        Friend Valid As Boolean = True

        Public Sub New()
            MyBase.new()
        End Sub

        Public Sub New(ByVal SelectionString As String, Optional ByVal UseClientDeposit As Boolean = False, Optional ByVal UseRetentionTable As Boolean = False)
            MyClass.New()
            Me.SelectionString = SelectionString
            Me.UseClientDepositTable = UseClientDeposit
            Me.UseClientRetentionEventsTable = UseRetentionTable
        End Sub
    End Class
End Namespace
