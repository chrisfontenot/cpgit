#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}
#End Region

Option Compare Binary
Option Explicit On 
Option Strict On

Namespace Retention.Common
    Friend Class FrameControl
        Inherits DevExpress.XtraEditors.XtraUserControl
        Public Event FrameUpdated As EventHandler

        Friend CurrentItem As FieldSelection = Nothing
        Public Sub New(ByRef CurrentItem As FieldSelection)
            MyClass.New()
            Me.CurrentItem = CurrentItem

            ' Do not allow the combobox items to be changed if this is a "pick one of the follow" type of input
            Select Case CurrentItem.EventID
                Case Enum_Events.EVENT_ACTIVE_STATUS, Enum_Events.EVENT_OFFICE, Enum_Events.EVENT_RETENTION_EVENT, Enum_Events.EVENT_COUNSELOR, Enum_Events.EVENT_CSR, Enum_Events.EVENT_REGION
                    Value1.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
                    value2.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor

                Case Else
                    Value1.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard
                    value2.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard
            End Select
        End Sub

        Public Sub New()
            MyBase.New()
            InitializeComponent()

            AddHandler relation.SelectedIndexChanged, AddressOf relation_SelectedIndexChanged
            AddHandler Value1.SelectedIndexChanged, AddressOf Value1_SelectedIndexChanged
            AddHandler Value1.TextChanged, AddressOf Value1_TextChanged
            AddHandler value2.SelectedIndexChanged, AddressOf value2_SelectedIndexChanged
            AddHandler value2.TextChanged, AddressOf value2_TextChanged
            AddHandler Button_Add.Click, AddressOf Button_Add_Click
            AddHandler Button_Del.Click, AddressOf Button_Del_Click
            AddHandler ItemList.SelectedIndexChanged, AddressOf ItemList_SelectedIndexChanged
            AddHandler Value1.Validating, AddressOf Value1_Validating
            AddHandler value2.Validating, AddressOf value2_Validating
        End Sub

#Region " Windows Form Designer generated code "

        'UserControl overrides dispose to clean up the component list.
        Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
            If disposing Then
                If components IsNot Nothing Then
                    components.Dispose()
                End If
            End If
            MyBase.Dispose(disposing)
        End Sub

        'Required by the Windows Form Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Windows Form Designer
        'It can be modified using the Windows Form Designer.  
        'Do not modify it using the code editor.
        Friend WithEvents relation As DevExpress.XtraEditors.ComboBoxEdit
        Friend WithEvents Value1 As DevExpress.XtraEditors.ComboBoxEdit
        Friend WithEvents simple_group As DevExpress.XtraEditors.GroupControl
        Friend WithEvents value2 As DevExpress.XtraEditors.ComboBoxEdit
        Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents item_group As DevExpress.XtraEditors.GroupControl
        Friend WithEvents Button_Del As DevExpress.XtraEditors.SimpleButton
        Friend WithEvents Button_Add As DevExpress.XtraEditors.SimpleButton
        Friend WithEvents ItemList As DevExpress.XtraEditors.ListBoxControl
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
            Me.relation = New DevExpress.XtraEditors.ComboBoxEdit
            Me.Value1 = New DevExpress.XtraEditors.ComboBoxEdit
            Me.simple_group = New DevExpress.XtraEditors.GroupControl
            Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl
            Me.value2 = New DevExpress.XtraEditors.ComboBoxEdit
            Me.item_group = New DevExpress.XtraEditors.GroupControl
            Me.Button_Del = New DevExpress.XtraEditors.SimpleButton
            Me.Button_Add = New DevExpress.XtraEditors.SimpleButton
            Me.ItemList = New DevExpress.XtraEditors.ListBoxControl
            CType(Me.relation.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.Value1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.simple_group, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.simple_group.SuspendLayout()
            CType(Me.value2.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.item_group, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.item_group.SuspendLayout()
            CType(Me.ItemList, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.SuspendLayout()
            '
            'relation
            '
            Me.relation.Location = New System.Drawing.Point(8, 8)
            Me.relation.Name = "relation"
            '
            'relation.Properties
            '
            Me.relation.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.relation.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
            Me.relation.Size = New System.Drawing.Size(144, 20)
            Me.relation.TabIndex = 0
            '
            'Value1
            '
            Me.Value1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                        Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.Value1.Location = New System.Drawing.Point(160, 8)
            Me.Value1.Name = "Value1"
            '
            'Value1.Properties
            '
            Me.Value1.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.Value1.Size = New System.Drawing.Size(160, 20)
            Me.Value1.TabIndex = 1
            '
            'simple_group
            '
            Me.simple_group.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                        Or System.Windows.Forms.AnchorStyles.Left) _
                        Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.simple_group.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder
            Me.simple_group.Controls.Add(Me.LabelControl1)
            Me.simple_group.Controls.Add(Me.value2)
            Me.simple_group.Location = New System.Drawing.Point(0, 32)
            Me.simple_group.Name = "simple_group"
            Me.simple_group.ShowCaption = False
            Me.simple_group.Size = New System.Drawing.Size(320, 120)
            Me.simple_group.TabIndex = 2
            Me.simple_group.Text = "simple_group"
            Me.simple_group.Visible = False
            '
            'LabelControl1
            '
            Me.LabelControl1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                        Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.LabelControl1.Appearance.Options.UseTextOptions = True
            Me.LabelControl1.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
            Me.LabelControl1.Location = New System.Drawing.Point(160, 8)
            Me.LabelControl1.Name = "LabelControl1"
            Me.LabelControl1.Size = New System.Drawing.Size(160, 20)
            Me.LabelControl1.TabIndex = 0
            Me.LabelControl1.Text = "and"
            Me.LabelControl1.UseMnemonic = False
            '
            'value2
            '
            Me.value2.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                        Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.value2.Location = New System.Drawing.Point(160, 32)
            Me.value2.Name = "value2"
            '
            'value2.Properties
            '
            Me.value2.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.value2.Size = New System.Drawing.Size(160, 20)
            Me.value2.TabIndex = 1
            '
            'item_group
            '
            Me.item_group.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                        Or System.Windows.Forms.AnchorStyles.Left) _
                        Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.item_group.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder
            Me.item_group.Controls.Add(Me.Button_Del)
            Me.item_group.Controls.Add(Me.Button_Add)
            Me.item_group.Controls.Add(Me.ItemList)
            Me.item_group.Location = New System.Drawing.Point(0, 32)
            Me.item_group.Name = "item_group"
            Me.item_group.ShowCaption = False
            Me.item_group.Size = New System.Drawing.Size(320, 120)
            Me.item_group.TabIndex = 3
            Me.item_group.Text = "GroupControl1"
            Me.item_group.Visible = False
            '
            'Button_Del
            '
            Me.Button_Del.Enabled = False
            Me.Button_Del.Location = New System.Drawing.Point(72, 40)
            Me.Button_Del.Name = "Button_Del"
            Me.Button_Del.TabIndex = 1
            Me.Button_Del.Text = "&Del"
            '
            'Button_Add
            '
            Me.Button_Add.Enabled = False
            Me.Button_Add.Location = New System.Drawing.Point(72, 8)
            Me.Button_Add.Name = "Button_Add"
            Me.Button_Add.TabIndex = 0
            Me.Button_Add.Text = "&Add"
            '
            'ItemList
            '
            Me.ItemList.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                        Or System.Windows.Forms.AnchorStyles.Left) _
                        Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.ItemList.Location = New System.Drawing.Point(160, 8)
            Me.ItemList.Name = "ItemList"
            Me.ItemList.Size = New System.Drawing.Size(160, 104)
            Me.ItemList.TabIndex = 2
            '
            'FrameControl
            '
            Me.Controls.Add(Me.Value1)
            Me.Controls.Add(Me.relation)
            Me.Controls.Add(Me.item_group)
            Me.Controls.Add(Me.simple_group)
            Me.Name = "FrameControl"
            Me.Size = New System.Drawing.Size(320, 150)
            CType(Me.relation.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.Value1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.simple_group, System.ComponentModel.ISupportInitialize).EndInit()
            Me.simple_group.ResumeLayout(False)
            CType(Me.value2.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.item_group, System.ComponentModel.ISupportInitialize).EndInit()
            Me.item_group.ResumeLayout(False)
            CType(Me.ItemList, System.ComponentModel.ISupportInitialize).EndInit()
            Me.ResumeLayout(False)

        End Sub

#End Region

        Protected Overrides Sub OnLoad(ByVal e As System.EventArgs)
            MyBase.OnLoad(e)

            ' Load the list of items for the relations
            With relation
                With .Properties.Items
                    .Add(New DebtPlus.Data.Controls.ComboboxItem("is any value", Enum_Relations.RELATION_IS_ANY_VALUE))
                    .Add(New DebtPlus.Data.Controls.ComboboxItem("is equal to", Enum_Relations.RELATION_IS_EQUAL))
                    .Add(New DebtPlus.Data.Controls.ComboboxItem("is not equal to", Enum_Relations.RELATION_IS_NOT_EQUAL))
                    .Add(New DebtPlus.Data.Controls.ComboboxItem("is one of", Enum_Relations.RELATION_IS_ONE_OF))
                    .Add(New DebtPlus.Data.Controls.ComboboxItem("is not one of", Enum_Relations.RELATION_IS_NOT_ONE_OF))
                    .Add(New DebtPlus.Data.Controls.ComboboxItem("is less than", Enum_Relations.RELATION_IS_LESS_THAN))
                    .Add(New DebtPlus.Data.Controls.ComboboxItem("is less than or equal to", Enum_Relations.RELATION_IS_GREATER_THAN_OR_EQUAL))
                    .Add(New DebtPlus.Data.Controls.ComboboxItem("is greater than", Enum_Relations.RELATION_IS_GREATER_THAN))
                    .Add(New DebtPlus.Data.Controls.ComboboxItem("is greater than or equal to", Enum_Relations.RELATION_IS_GREATER_THAN_OR_EQUAL))
                    .Add(New DebtPlus.Data.Controls.ComboboxItem("is between", Enum_Relations.RELATION_IS_BETWEEEN))
                    .Add(New DebtPlus.Data.Controls.ComboboxItem("is not between", Enum_Relations.RELATION_IS_NOT_BETWEEN))
                End With
                .SelectedIndex = Convert.ToInt32(CurrentItem.Relation)
            End With

            ' Populate the list of items into the item list
            With ItemList
                With .Items
                    .Clear()
                    For Each NewItem As FieldItem In CurrentItem.Values
                        .Add(NewItem)
                    Next
                End With
            End With

            ' If this is a table type of event then load the table contents into the dropdown lists
            Select Case CurrentItem.EventID
                Case Enum_Events.EVENT_OFFICE
                    LoadTableReferences(Me.Value1, OfficesTable().DefaultView, CurrentItem.Field1)
                    LoadTableReferences(Me.value2, OfficesTable().DefaultView, CurrentItem.Field2)

                Case Enum_Events.EVENT_ACTIVE_STATUS
                    LoadTableReferences(Me.Value1, ActiveStatusTable().DefaultView, CurrentItem.Field1)
                    LoadTableReferences(Me.value2, ActiveStatusTable().DefaultView, CurrentItem.Field2)

                Case Enum_Events.EVENT_RETENTION_EVENT
                    LoadTableReferences(Me.Value1, RetentionEventsTable().DefaultView, CurrentItem.Field1)
                    LoadTableReferences(Me.value2, RetentionEventsTable().DefaultView, CurrentItem.Field2)

                Case Enum_Events.EVENT_REGION
                    LoadTableReferences(Me.Value1, RegionsTable().DefaultView, CurrentItem.Field1)
                    LoadTableReferences(Me.value2, RegionsTable().DefaultView, CurrentItem.Field2)

                Case Retention.Enum_Events.EVENT_COUNSELOR
                    Dim vue As New System.Data.DataView(CounselorsTable(), String.Empty, String.Empty, DataViewRowState.CurrentRows)
                    LoadTableReferences(Me.Value1, vue, CurrentItem.Field1)
                    LoadTableReferences(Me.value2, vue, CurrentItem.Field2)

                Case Retention.Enum_Events.EVENT_CSR
                    Dim vue As New System.Data.DataView(CSRTable(), String.Empty, String.Empty, DataViewRowState.CurrentRows)
                    LoadTableReferences(Me.Value1, vue, CurrentItem.Field1)
                    LoadTableReferences(Me.value2, vue, CurrentItem.Field2)
            End Select
        End Sub

        Private Sub LoadTableReferences(ByRef ctl As DevExpress.XtraEditors.ComboBoxEdit, ByVal vue As System.Data.DataView, ByVal SelectedItem As FieldItem)
            With ctl
                With .Properties.Items
                    .Clear()
                    For Each row As System.Data.DataRowView In vue
                        Dim ItemNumber As System.Int32 = .Add(New DebtPlus.Data.Controls.ComboboxItem(Convert.ToString(row(1)), row(0)))
                        If SelectedItem IsNot Nothing Then
                            If SelectedItem.CompareTo(row(1)) = 0 Then ctl.SelectedIndex = ItemNumber
                        End If
                    Next
                End With
            End With
        End Sub

        Private Sub relation_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)

            ' Set the relation in the clause
            CurrentItem.Relation = CType(CType(relation.SelectedItem, DebtPlus.Data.Controls.ComboboxItem).value, Enum_Relations)

            ' Enable/Disable controls based upon the relation
            Select Case CurrentItem.Relation
                Case Enum_Relations.RELATION_IS_ANY_VALUE
                    Me.Value1.Visible = False
                    Me.simple_group.Visible = False
                    Me.item_group.Visible = False

                Case Enum_Relations.RELATION_IS_BETWEEEN, Enum_Relations.RELATION_IS_NOT_BETWEEN
                    Me.Value1.Visible = True
                    Me.simple_group.Visible = True
                    Me.item_group.Visible = False

                Case Enum_Relations.RELATION_IS_EQUAL, Enum_Relations.RELATION_IS_GREATER_THAN, Enum_Relations.RELATION_IS_GREATER_THAN_OR_EQUAL, Enum_Relations.RELATION_IS_LESS_THAN, Enum_Relations.RELATION_IS_LESS_THAN_OR_EQUAL, Enum_Relations.RELATION_IS_NOT_EQUAL
                    Me.Value1.Visible = True
                    Me.simple_group.Visible = False
                    Me.item_group.Visible = False

                Case Enum_Relations.RELATION_IS_NOT_ONE_OF, Enum_Relations.RELATION_IS_ONE_OF
                    Me.Value1.Visible = True
                    Me.simple_group.Visible = False
                    Me.item_group.Visible = True
            End Select

            RaiseEvent FrameUpdated(Me, EventArgs.Empty)
        End Sub

        Private Sub Value1_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
            CurrentItem.Field1.ItemValue = -1
            CurrentItem.Field1.Value = Value1.Text
            If Value1.SelectedIndex >= 0 Then
                CurrentItem.Field1.ItemValue = CType(Value1.SelectedItem, DebtPlus.Data.Controls.ComboboxItem).value
                Button_Add.Enabled = Value1NotInList()
            End If

            RaiseEvent FrameUpdated(Me, EventArgs.Empty)
        End Sub

        Private Sub Value1_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)
            CurrentItem.Field1.Value = Value1.Text.Trim()
            Button_Add.Enabled = (CurrentItem.Field1.Value <> String.Empty) AndAlso Value1NotInList()

            RaiseEvent FrameUpdated(Me, EventArgs.Empty)
        End Sub

        Private Sub value2_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
            CurrentItem.Field2.ItemValue = -1
            CurrentItem.Field2.Value = value2.Text
            If Value1.SelectedIndex >= 0 Then
                CurrentItem.Field2.ItemValue = CType(value2.SelectedItem, DebtPlus.Data.Controls.ComboboxItem).value
            End If

            RaiseEvent FrameUpdated(Me, EventArgs.Empty)
        End Sub

        Private Sub value2_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)
            CurrentItem.Field2.Value = value2.Text
            RaiseEvent FrameUpdated(Me, EventArgs.Empty)
        End Sub

        Private Sub Button_Add_Click(ByVal sender As Object, ByVal e As System.EventArgs)

            ' Make a copy of the current item #1
            Dim NewField As New FieldItem
            With CurrentItem.Field1
                NewField.ItemID = .ItemID
                NewField.ItemValue = .ItemValue
                NewField.Value = .Value
            End With

            ' Add it to the list of items in the group
            CurrentItem.Values.Add(NewField)

            ' Put it in the visible list of item and select it
            ItemList.SelectedItem = ItemList.Items.Add(NewField)
            Button_Add.Enabled = Value1NotInList()

            RaiseEvent FrameUpdated(Me, EventArgs.Empty)
        End Sub

        Private Sub Button_Del_Click(ByVal sender As Object, ByVal e As System.EventArgs)

            ' If there is a selected item to be removed ....
            If ItemList.SelectedIndex >= 0 Then

                ' Find the item that is selected
                Dim ItemSelection As FieldItem = CType(ItemList.SelectedItem, FieldItem)

                ' Find the item in our current list of items for this relation
                Dim IndexItem As System.Int32 = CurrentItem.Values.IndexOf(ItemSelection)

                ' If found then we may delete it. Remove it from both lists.
                If IndexItem >= 0 Then
                    Button_Del.Enabled = False
                    CurrentItem.Values.RemoveAt(IndexItem)
                    ItemList.Items.RemoveAt(ItemList.SelectedIndex)

                    ' Enable the ADD button if the current item is not in the list
                    Button_Add.Enabled = Value1NotInList()

                    RaiseEvent FrameUpdated(Me, EventArgs.Empty)
                End If
            End If
        End Sub

        Private Sub ItemList_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
            Button_Del.Enabled = (ItemList.SelectedIndex >= 0)
        End Sub

        Private Sub Value1_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs)

            ' Start by disabling the add button until we like the data
            Button_Add.Enabled = False

            ' Trim the text around the string and if there is no item then blank the value clause
            CurrentItem.Field1.Value = Value1.Text.Trim()
            If CurrentItem.Field1.Value = String.Empty Then
                CurrentItem.Field1.ItemValue = Nothing
                RaiseEvent FrameUpdated(Me, EventArgs.Empty)
                Return
            End If

            ' If the item is a date then we need to parse the date. If there is an error, it will take an exception.
            Select Case CurrentItem.EventID
                Case Enum_Events.EVENT_EXPECTED_DEPOSIT_DATE, Enum_Events.EVENT_LAST_DEPOSIT_DATE, Enum_Events.EVENT_ACTION_DATE
                    Try
                        CurrentItem.Field1.ItemValue = Date.Parse(Value1.Text)
                        Value1.Text = CurrentItem.Field1.ToString()
                        Button_Add.Enabled = Value1NotInList()
                        RaiseEvent FrameUpdated(Me, EventArgs.Empty)

                    Catch ex As FormatException
                        e.Cancel = True
                        Value1.ErrorText = "Invalid date"

                    Catch ex As ArgumentOutOfRangeException
                        e.Cancel = True
                        Value1.ErrorText = "Invalid date"
                    End Try

                Case Else
                    Button_Add.Enabled = (Value1.SelectedIndex >= 0) AndAlso Value1NotInList()
            End Select

        End Sub

        Private Sub value2_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs)

            ' Trim the text around the string and if there is no item then blank the value clause
            CurrentItem.Field2.Value = value2.Text.Trim()
            If CurrentItem.Field2.Value = String.Empty Then
                CurrentItem.Field2.ItemValue = Nothing
                RaiseEvent FrameUpdated(Me, EventArgs.Empty)
                Return
            End If

            ' If the item is a date then we need to parse the date. If there is an error, it will take an exception.
            Select Case CurrentItem.EventID
                Case Enum_Events.EVENT_EXPECTED_DEPOSIT_DATE, Enum_Events.EVENT_LAST_DEPOSIT_DATE, Enum_Events.EVENT_ACTION_DATE
                    Try
                        CurrentItem.Field2.ItemValue = Date.Parse(value2.Text)
                        value2.Text = CurrentItem.Field2.ToString()
                        RaiseEvent FrameUpdated(Me, EventArgs.Empty)

                    Catch ex As FormatException
                        e.Cancel = True
                        value2.ErrorText = "Invalid date"

                    Catch ex As ArgumentOutOfRangeException
                        e.Cancel = True
                        value2.ErrorText = "Invalid date"
                    End Try
            End Select
        End Sub

        Private Function Value1NotInList() As Boolean

            ' A "NOTHING" is always in the list
            If CurrentItem.Field1.ItemValue Is Nothing Then Return True

            ' Look for a duplicate item to the current selection
            For Each Item As FieldItem In ItemList.Items
                If Item.CompareTo(CurrentItem.Field1) = 0 Then Return False
            Next

            ' The item is not in the list if we can't find it.
            Return True
        End Function
    End Class
End Namespace
