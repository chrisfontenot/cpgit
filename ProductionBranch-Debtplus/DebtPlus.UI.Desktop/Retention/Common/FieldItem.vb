Option Compare Binary
Option Explicit On 
Option Strict On

Namespace Retention.Common
    Friend Class FieldItem
        Inherits Object
        Implements IComparable

        Friend Value As String = String.Empty
        Friend ItemValue As Object = Nothing
        Friend ItemID As System.Int32 = 0

        Public Sub New()
            MyBase.new()
        End Sub

        Public Overrides Function ToString() As String
            If ItemValue IsNot Nothing Then
                If TypeOf ItemValue Is DateTime Then Return String.Format("{0:d}", Convert.ToDateTime(ItemValue))
            End If
            Return Value
        End Function

        Public Function CompareTo(ByVal obj As Object) As System.Int32 Implements System.IComparable.CompareTo

            ' If the input value is nothing then we are always greater
            If obj Is Nothing Then Return 1

            ' If we are comparing to our own type then compare the string values
            If TypeOf obj Is FieldItem Then
                Return String.Compare(CType(obj, FieldItem).ToString(), Me.ToString())
            End If

            ' Handle the case of a string comparison to ourselves
            If TypeOf obj Is String AndAlso TypeOf ItemValue Is String Then Return String.Compare(Convert.ToString(ItemValue), Convert.ToString(obj))

            ' Compare the Int32 value
            If TypeOf obj Is Int32 AndAlso TypeOf ItemValue Is Int32 Then
                If Convert.ToInt32(ItemValue) < Convert.ToInt32(obj) Then Return -1
                If Convert.ToInt32(ItemValue) = Convert.ToInt32(obj) Then Return 0
                Return 1
            End If

            ' We do not compare. So, assume that we are less.
            Return -1
        End Function
    End Class
End Namespace