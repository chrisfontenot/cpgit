Option Compare Binary
Option Explicit On 
Option Strict On

Namespace Retention.Generate
    Friend Class ArgumentParser
        Inherits DebtPlus.Utils.ArgParserBase

        ' Event to create.
        Friend EventToGenerate As System.Int32 = -1
        Friend EventToGenerateTitle As String = String.Empty

        Public Sub New()
            MyBase.New(New String() {})
        End Sub

        Protected Overrides Sub OnUsage(ByVal errorInfo As String)
            ' deprecated
        End Sub
    End Class
End Namespace