#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}
#End Region

Option Compare Binary
Option Explicit On 
Option Strict On

Imports DebtPlus.UI.Desktop.Retention.Common

Namespace Retention.Generate
    Friend Class GetEventForm
        Inherits FieldForm

        ''' <summary>
        ''' Process the creation of the field item
        ''' </summary>
        Private ap As ArgumentParser = Nothing

        Public Sub New(ByVal search As Boolean)
            MyBase.New(search)
            InitializeComponent()
            AddHandler Me.Load, AddressOf GetEventForm_Load
            AddHandler RetentionEvent.EditValueChanged, AddressOf RetentionEvent_EditValueChanged
        End Sub

        Public Sub New(ByVal search As Boolean, ByVal ap As ArgumentParser, ByVal FirstField As FieldSelection)
            MyClass.New(search)
            Me.ap = ap
            MyBase.NewFieldItem = FirstField
        End Sub

#Region " Windows Form Designer generated code "

        'Form overrides dispose to clean up the component list.
        Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
            If disposing Then
                If components IsNot Nothing Then
                    components.Dispose()
                End If
            End If
            MyBase.Dispose(disposing)
        End Sub
        'Required by the Windows Form Designer
        Private components As System.ComponentModel.IContainer
        'NOTE: The following procedure is required by the Windows Form Designer
        'It can be modified using the Windows Form Designer.  
        'Do not modify it using the code editor.
        Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents LabelControl2 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents RetentionEvent As DevExpress.XtraEditors.LookUpEdit
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
            Me.RetentionEvent = New DevExpress.XtraEditors.LookUpEdit
            Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl
            Me.LabelControl2 = New DevExpress.XtraEditors.LabelControl
            CType(Me.LookUpEdit1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()

            CType(Me.RetentionEvent.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.SuspendLayout()
            '
            'Button_OK
            '
            Me.Button_OK.Location = New System.Drawing.Point(72, 80)
            Me.Button_OK.Name = "Button_OK"
            Me.Button_OK.TabIndex = 4
            '
            'LookUpEdit1
            '
            Me.LookUpEdit1.EditValue = 0
            Me.LookUpEdit1.Location = New System.Drawing.Point(112, 48)
            Me.LookUpEdit1.Name = "LookUpEdit1"
            Me.LookUpEdit1.TabIndex = 3
            '
            'Button_Cancel
            '
            Me.Button_Cancel.Location = New System.Drawing.Point(160, 80)
            Me.Button_Cancel.Name = "Button_Cancel"
            Me.Button_Cancel.TabIndex = 5
            '
            'RetentionEvent
            '
            Me.RetentionEvent.Location = New System.Drawing.Point(112, 24)
            Me.RetentionEvent.Name = "RetentionEvent"
            '
            'RetentionEvent.Properties
            '
            Me.RetentionEvent.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.RetentionEvent.Properties.Columns.AddRange(New DevExpress.XtraEditors.Controls.LookUpColumnInfo() {New DevExpress.XtraEditors.Controls.LookUpColumnInfo("item_key", "ID", 20, DevExpress.Utils.FormatType.Numeric, "f0", False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Data.ColumnSortOrder.None), New DevExpress.XtraEditors.Controls.LookUpColumnInfo("description", "Description", 20, DevExpress.Utils.FormatType.None, "", True, DevExpress.Utils.HorzAlignment.Default, DevExpress.Data.ColumnSortOrder.None)})
            Me.RetentionEvent.Properties.NullText = ""
            Me.RetentionEvent.Properties.ShowFooter = False
            Me.RetentionEvent.Properties.ShowHeader = False
            Me.RetentionEvent.Properties.ShowLines = False
            Me.RetentionEvent.Size = New System.Drawing.Size(176, 20)
            Me.RetentionEvent.TabIndex = 1
            Me.RetentionEvent.Properties.SortColumnIndex = 1
            '
            'LabelControl1
            '
            Me.LabelControl1.Location = New System.Drawing.Point(8, 24)
            Me.LabelControl1.Name = "LabelControl1"
            Me.LabelControl1.Size = New System.Drawing.Size(96, 14)
            Me.LabelControl1.TabIndex = 0
            Me.LabelControl1.Text = "Event to Generate"
            '
            'LabelControl2
            '
            Me.LabelControl2.Location = New System.Drawing.Point(8, 48)
            Me.LabelControl2.Name = "LabelControl2"
            Me.LabelControl2.Size = New System.Drawing.Size(75, 16)
            Me.LabelControl2.TabIndex = 2
            Me.LabelControl2.Text = "Client Selection"
            '
            'GetEventForm
            '
            Me.AutoScaleBaseSize = New System.Drawing.Size(5, 14)
            Me.ClientSize = New System.Drawing.Size(306, 112)
            Me.Controls.Add(Me.LabelControl2)
            Me.Controls.Add(Me.LabelControl1)
            Me.Controls.Add(Me.RetentionEvent)
            Me.Name = "GetEventForm"
            Me.ToolTipController1.SetSuperTip(Me, Nothing)
            Me.Text = "Event And Initial Selection Item"
            Me.Controls.SetChildIndex(Me.RetentionEvent, 0)
            Me.Controls.SetChildIndex(Me.LabelControl1, 0)
            Me.Controls.SetChildIndex(Me.LabelControl2, 0)
            Me.Controls.SetChildIndex(Me.LookUpEdit1, 0)
            Me.Controls.SetChildIndex(Me.Button_OK, 0)
            Me.Controls.SetChildIndex(Me.Button_Cancel, 0)
            CType(Me.LookUpEdit1.Properties, System.ComponentModel.ISupportInitialize).EndInit()

            CType(Me.RetentionEvent.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            Me.ResumeLayout(False)

        End Sub
#End Region
        Private Sub GetEventForm_Load(ByVal sender As System.Object, ByVal e As System.EventArgs)

            ' Load the list of retention events to generate
            Dim tbl As System.Data.DataTable = RetentionEventsTable()
            With RetentionEvent
                With .Properties
                    .DataSource = tbl.DefaultView
                    .DisplayMember = "description"
                    .ValueMember = "item_key"
                End With
            End With

            Button_OK.Enabled = Not HasErrors()
        End Sub

        Protected Overrides Function HasErrors() As Boolean
            Return MyBase.HasErrors OrElse ap.EventToGenerate <= 0
        End Function

        Private Sub RetentionEvent_EditValueChanged(ByVal sender As Object, ByVal e As System.EventArgs)
            If RetentionEvent.EditValue IsNot Nothing Then
                ap.EventToGenerate = Convert.ToInt32(RetentionEvent.EditValue)
                ap.EventToGenerateTitle = RetentionEvent.Text
                Button_OK.Enabled = Not HasErrors()
            End If
        End Sub
    End Class
End Namespace
