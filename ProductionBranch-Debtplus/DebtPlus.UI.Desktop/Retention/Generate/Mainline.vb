#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}
#End Region

Option Compare Binary
Option Explicit On 
Option Strict On

Imports DebtPlus.UI.Desktop.Retention.Common
Imports System.Data.SqlClient
Imports DebtPlus.Interfaces.Desktop
Imports System.Windows.Forms

Namespace Retention.Generate

    Public Class Mainline
        Implements IDesktopMainline

        ' This is the creation function. In search, this is True.
        Friend IsSearch As Boolean = False
        Friend Arguments As ArgumentParser = Nothing

        Public Sub OldAppMain(ByVal args() As String) Implements IDesktopMainline.OldAppMain

            Arguments = New ArgumentParser
            If Arguments.Parse(Args) Then

                ' Ask for the first field item
                Dim FirstField As New FieldSelection
                Dim answer As System.Windows.Forms.DialogResult
                Using frm As New GetEventForm(IsSearch, Arguments, FirstField)
                    With frm
                        .StartPosition = FormStartPosition.CenterScreen
                        answer = .ShowDialog()
                    End With
                End Using

                ' If there is a field then run the main application. Otherwise, quit.
                If answer = DialogResult.OK Then
                    Using dlg As New RelationForm(IsSearch, FirstField)
                        dlg.Text = "Generate " + Arguments.EventToGenerateTitle + " Retention Events"
                        answer = dlg.ShowDialog()

                        ' If successful with the dialog then try to create the events
                        If answer = DialogResult.OK Then
                            With dlg
                                Dim Result As SelectionClause = .SelectionClause
                                If Result.Valid Then
                                    Dim Selection As String = Result.SelectionString
                                    If Selection.Length > 0 Then Selection = " WHERE " + Selection
                                    Dim RecordCount As System.Int32 = 0

                                    ' Build the insert command from all of the bits and pieces
                                    Dim CommandText As String = "INSERT INTO client_retention_events (client, retention_event, priority, expire_type) SELECT DISTINCT c.client, " + Arguments.EventToGenerate.ToString() + ", ev.priority,isnull(ev.expire_type,1) FROM clients c INNER JOIN retention_events ev ON ev.retention_event=" + Arguments.EventToGenerate.ToString()
                                    If Result.UseClientDepositTable Then CommandText += " INNER JOIN client_deposits cd ON c.client = cd.client"
                                    If Result.UseClientRetentionEventsTable Then CommandText += " INNER JOIN client_retention_events vc ON vc.client = c.client"
                                    CommandText += Selection

                                    ' Do the record insert operation
                                    Dim cn As SqlClient.SqlConnection = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
                                    Dim current_cursor As Cursor = Cursor.Current
                                    Try
                                        Cursor.Current = Cursors.WaitCursor
                                        cn.Open()
                                        Using cmd As SqlClient.SqlCommand = New SqlCommand
                                            With cmd
                                                .CommandText = CommandText
                                                .CommandTimeout = System.Math.Max(300, DebtPlus.LINQ.SQLInfoClass.getDefault().CommandTimeout)
                                                .Connection = cn
                                                RecordCount = .ExecuteNonQuery
                                            End With
                                        End Using

                                        ' It worked.
                                        DebtPlus.Data.Forms.MessageBox.Show(String.Format("{0:d} clients received the events", RecordCount), "Operation completed successfully", MessageBoxButtons.OK, MessageBoxIcon.Information)

                                    Catch ex As SqlClient.SqlException
                                        DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error Inserting Retention Events")

                                    Finally
                                        If cn IsNot Nothing Then cn.Dispose()
                                        Cursor.Current = current_cursor
                                    End Try
                                End If
                            End With
                        End If
                    End Using
                End If
            End If
        End Sub

    End Class

End Namespace