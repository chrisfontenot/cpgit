#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}
#End Region

Option Compare Binary
Option Explicit On 
Option Strict On

Imports DebtPlus.UI.Desktop.Retention.Common
Imports System.Windows.Forms

Namespace Retention.Review
    Friend Class CounselorOfficeSelectionForm
        Inherits DebtPlus.Data.Forms.DebtPlusForm

        Public Sub New()
            MyBase.New()
            InitializeComponent()
            AddHandler Me.Load, AddressOf CounselorOfficeSelectionForm_Load
            AddHandler office.EditValueChanged, AddressOf office_EditValueChanged
            AddHandler counselor.EditValueChanged, AddressOf office_EditValueChanged
        End Sub

#Region " Windows Form Designer generated code "

        'Form overrides dispose to clean up the component list.
        Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
            If disposing Then
                If components IsNot Nothing Then
                    components.Dispose()
                End If
            End If
            MyBase.Dispose(disposing)
        End Sub

        'Required by the Windows Form Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Windows Form Designer
        'It can be modified using the Windows Form Designer.  
        'Do not modify it using the code editor.
        Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents LabelControl2 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents LabelControl3 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents CheckEdit1 As DevExpress.XtraEditors.CheckEdit
        Friend WithEvents Button_OK As DevExpress.XtraEditors.SimpleButton
        Friend WithEvents Button_Cancel As DevExpress.XtraEditors.SimpleButton
        Friend WithEvents counselor As DevExpress.XtraEditors.LookUpEdit
        Friend WithEvents office As DevExpress.XtraEditors.LookUpEdit
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
            Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl
            Me.LabelControl2 = New DevExpress.XtraEditors.LabelControl
            Me.LabelControl3 = New DevExpress.XtraEditors.LabelControl
            Me.CheckEdit1 = New DevExpress.XtraEditors.CheckEdit
            Me.Button_OK = New DevExpress.XtraEditors.SimpleButton
            Me.Button_Cancel = New DevExpress.XtraEditors.SimpleButton
            Me.counselor = New DevExpress.XtraEditors.LookUpEdit
            Me.office = New DevExpress.XtraEditors.LookUpEdit
            CType(Me.DxErrorProvider1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.CheckEdit1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.counselor.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.office.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.SuspendLayout()
            '
            'LabelControl1
            '
            Me.LabelControl1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                        Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.LabelControl1.Appearance.Options.UseTextOptions = True
            Me.LabelControl1.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
            Me.LabelControl1.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Top
            Me.LabelControl1.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap
            Me.LabelControl1.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
            Me.LabelControl1.Location = New System.Drawing.Point(8, 16)
            Me.LabelControl1.Name = "LabelControl1"
            Me.LabelControl1.Size = New System.Drawing.Size(272, 32)
            Me.LabelControl1.TabIndex = 0
            Me.LabelControl1.Text = "Please choose the counselor and/or office to narrow the search."
            '
            'LabelControl2
            '
            Me.LabelControl2.Location = New System.Drawing.Point(8, 59)
            Me.LabelControl2.Name = "LabelControl2"
            Me.LabelControl2.Size = New System.Drawing.Size(52, 13)
            Me.LabelControl2.TabIndex = 1
            Me.LabelControl2.Text = "&Counselor:"
            '
            'LabelControl3
            '
            Me.LabelControl3.Location = New System.Drawing.Point(8, 83)
            Me.LabelControl3.Name = "LabelControl3"
            Me.LabelControl3.Size = New System.Drawing.Size(33, 13)
            Me.LabelControl3.TabIndex = 2
            Me.LabelControl3.Text = "&Office:"
            '
            'CheckEdit1
            '
            Me.CheckEdit1.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
            Me.CheckEdit1.EditValue = True
            Me.CheckEdit1.Location = New System.Drawing.Point(8, 168)
            Me.CheckEdit1.Name = "CheckEdit1"
            Me.CheckEdit1.Properties.Caption = "Include only &active clients"
            Me.CheckEdit1.Size = New System.Drawing.Size(152, 19)
            Me.CheckEdit1.TabIndex = 3
            '
            'Button_OK
            '
            Me.Button_OK.Anchor = System.Windows.Forms.AnchorStyles.Bottom
            Me.Button_OK.DialogResult = System.Windows.Forms.DialogResult.OK
            Me.Button_OK.Location = New System.Drawing.Point(57, 128)
            Me.Button_OK.Name = "Button_OK"
            Me.Button_OK.Size = New System.Drawing.Size(75, 23)
            Me.Button_OK.TabIndex = 4
            Me.Button_OK.Text = "&OK"
            '
            'Button_Cancel
            '
            Me.Button_Cancel.Anchor = System.Windows.Forms.AnchorStyles.Bottom
            Me.Button_Cancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
            Me.Button_Cancel.Location = New System.Drawing.Point(161, 128)
            Me.Button_Cancel.Name = "Button_Cancel"
            Me.Button_Cancel.Size = New System.Drawing.Size(75, 23)
            Me.Button_Cancel.TabIndex = 5
            Me.Button_Cancel.Text = "&Cancel"
            '
            'counselor
            '
            Me.counselor.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                        Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.counselor.Location = New System.Drawing.Point(96, 56)
            Me.counselor.Name = "counselor"
            Me.counselor.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.counselor.Properties.Columns.AddRange(New DevExpress.XtraEditors.Controls.LookUpColumnInfo() {New DevExpress.XtraEditors.Controls.LookUpColumnInfo("item_key", "ID", 20, DevExpress.Utils.FormatType.Numeric, "f0", False, DevExpress.Utils.HorzAlignment.[Default]), New DevExpress.XtraEditors.Controls.LookUpColumnInfo("description", "Description", 20, DevExpress.Utils.FormatType.None, "", True, DevExpress.Utils.HorzAlignment.[Default], DevExpress.Data.ColumnSortOrder.Ascending)})
            Me.counselor.Properties.NullText = "ANY COUNSELOR"
            Me.counselor.Properties.ShowFooter = False
            Me.counselor.Properties.ShowHeader = False
            Me.counselor.Properties.ShowLines = False
            Me.counselor.Size = New System.Drawing.Size(184, 20)
            Me.counselor.TabIndex = 6
            Me.counselor.Properties.SortColumnIndex = 1
            '
            'office
            '
            Me.office.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                        Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.office.Location = New System.Drawing.Point(96, 80)
            Me.office.Name = "office"
            Me.office.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.office.Properties.Columns.AddRange(New DevExpress.XtraEditors.Controls.LookUpColumnInfo() {New DevExpress.XtraEditors.Controls.LookUpColumnInfo("item_key", "ID", 20, DevExpress.Utils.FormatType.Numeric, "f0", False, DevExpress.Utils.HorzAlignment.[Default]), New DevExpress.XtraEditors.Controls.LookUpColumnInfo("description", "Description", 20, DevExpress.Utils.FormatType.None, "", True, DevExpress.Utils.HorzAlignment.[Default], DevExpress.Data.ColumnSortOrder.Ascending)})
            Me.office.Properties.NullText = "ANY OFFICE"
            Me.office.Properties.ShowFooter = False
            Me.office.Properties.ShowHeader = False
            Me.office.Properties.ShowLines = False
            Me.office.Size = New System.Drawing.Size(184, 20)
            Me.office.TabIndex = 7
            Me.office.Properties.SortColumnIndex = 1
            '
            'CounselorOfficeSelectionForm
            '
            Me.AcceptButton = Me.Button_OK
            Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
            Me.CancelButton = Me.Button_Cancel
            Me.ClientSize = New System.Drawing.Size(292, 198)
            Me.Controls.Add(Me.office)
            Me.Controls.Add(Me.counselor)
            Me.Controls.Add(Me.Button_Cancel)
            Me.Controls.Add(Me.Button_OK)
            Me.Controls.Add(Me.CheckEdit1)
            Me.Controls.Add(Me.LabelControl3)
            Me.Controls.Add(Me.LabelControl2)
            Me.Controls.Add(Me.LabelControl1)
            Me.Name = "CounselorOfficeSelectionForm"
            Me.Text = "Selection Information"
            CType(Me.DxErrorProvider1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.CheckEdit1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.counselor.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.office.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            Me.ResumeLayout(False)
            Me.PerformLayout()

        End Sub

#End Region

        Private Sub CounselorOfficeSelectionForm_Load(ByVal sender As Object, ByVal e As System.EventArgs)

            ' Load the counselor table
            Dim tbl As System.Data.DataTable = CounselorsTable()
            Dim row As System.Data.DataRow = tbl.Rows.Find(-1)
            If row Is Nothing Then
                tbl.Rows.Add(New Object() {-1, "ANY COUNSELOR"})
            End If

            With counselor.Properties
                .DisplayMember = "description"
                .ValueMember = "item_key"
                .DataSource = New System.Data.DataView(tbl, String.Empty, "description", DataViewRowState.CurrentRows)
            End With

            ' Load the offices table
            tbl = OfficesTable()
            row = tbl.Rows.Find(-1)
            If row Is Nothing Then
                tbl.Rows.Add(New Object() {-1, "ANY OFFICE"})
            End If

            With office.Properties
                .DisplayMember = "description"
                .ValueMember = "item_key"
                .DataSource = New System.Data.DataView(tbl, String.Empty, "description", DataViewRowState.CurrentRows)
            End With
        End Sub

        Public ReadOnly Property SelectionClause() As String
            Get
                Dim counselor_string As String = String.Empty
                Dim office_string As String = String.Empty
                Dim active_string As String = String.Empty

                With counselor
                    If .EditValue IsNot Nothing Then counselor_string = String.Format("(vc.[client_counselor]={0:d})", Convert.ToInt32(.EditValue))
                End With

                With office
                    If .EditValue IsNot Nothing Then office_string = String.Format("(vc.[client_office]={0:d})", Convert.ToInt32(.EditValue))
                End With

                With Me.CheckEdit1
                    If .CheckState = CheckState.Checked Then active_string = "(vc.[active_status] IN ('A','AR'))"
                End With

                Dim Result As String = String.Empty
                If counselor_string <> String.Empty Then Result += " AND " + counselor_string
                If office_string <> String.Empty Then Result += " AND " + office_string
                If active_string <> String.Empty Then Result += " AND " + active_string
                Return Result
            End Get
        End Property

        Private Sub office_EditValueChanged(ByVal sender As Object, ByVal e As System.EventArgs)
            With CType(sender, DevExpress.XtraEditors.LookUpEdit)
                If .EditValue IsNot Nothing Then
                    If Convert.ToInt32(.EditValue) < 0 Then .EditValue = Nothing
                End If
            End With
        End Sub
    End Class
End Namespace
