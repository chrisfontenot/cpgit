#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}
#End Region

Option Compare Binary
Option Explicit On 
Option Strict On

Imports System.Windows.Forms
Imports System.Drawing

Namespace Retention.Review
    Friend Class BaseGridControl
        Inherits System.Windows.Forms.UserControl
        Protected ControlRow As System.Int32 = -1

        Public Sub New()
            MyBase.New()
            InitializeComponent()
            AddHandler Me.Load, AddressOf BaseGridControl_Load
        End Sub

#Region " Windows Form Designer generated code "

        'UserControl overrides dispose to clean up the component list.
        Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
            If disposing Then
                If components IsNot Nothing Then
                    components.Dispose()
                End If
            End If
            MyBase.Dispose(disposing)
        End Sub

        'Required by the Windows Form Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Windows Form Designer
        'It can be modified using the Windows Form Designer.  
        'Do not modify it using the code editor.
        Protected WithEvents GridControl1 As DevExpress.XtraGrid.GridControl
        Protected WithEvents GridView1 As DevExpress.XtraGrid.Views.Grid.GridView
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
            Me.GridControl1 = New DevExpress.XtraGrid.GridControl
            Me.GridView1 = New DevExpress.XtraGrid.Views.Grid.GridView
            CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.GridView1, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.SuspendLayout()
            '
            'GridControl1
            '
            Me.GridControl1.Dock = System.Windows.Forms.DockStyle.Fill
            '
            'GridControl1.EmbeddedNavigator
            '
            Me.GridControl1.EmbeddedNavigator.Name = ""
            Me.GridControl1.Location = New System.Drawing.Point(0, 0)
            Me.GridControl1.MainView = Me.GridView1
            Me.GridControl1.Name = "GridControl1"
            Me.GridControl1.Size = New System.Drawing.Size(512, 280)
            Me.GridControl1.TabIndex = 15
            Me.GridControl1.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridView1})
            '
            'GridView1
            '
            Me.GridView1.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus
            Me.GridView1.GridControl = Me.GridControl1
            Me.GridView1.Name = "GridView1"
            Me.GridView1.OptionsBehavior.Editable = False
            Me.GridView1.OptionsView.ShowGroupPanel = False
            Me.GridView1.OptionsView.ShowIndicator = False
            Me.GridView1.ShowButtonMode = DevExpress.XtraGrid.Views.Base.ShowButtonModeEnum.ShowForFocusedRow
            '
            'BaseGridControl
            '
            Me.Controls.Add(Me.GridControl1)
            Me.Name = "BaseGridControl"
            Me.Size = New System.Drawing.Size(512, 280)
            CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.GridView1, System.ComponentModel.ISupportInitialize).EndInit()
            Me.ResumeLayout(False)

        End Sub

#End Region

        ''' <summary>
        ''' Create the context menu for the control
        ''' </summary>
        Protected Overridable Sub DefineMenuItems(ByRef menu As MenuItem.MenuItemCollection)
            With menu
                .Add(New MenuItem("Add", AddressOf MenuItemCreate, System.Windows.Forms.Shortcut.Ins))
                .Add(New MenuItem("Change", AddressOf MenuItemEdit))
                .Add(New MenuItem("Delete", AddressOf MenuItemDelete, System.Windows.Forms.Shortcut.Del))
            End With
        End Sub

        Protected Overridable Function myContextMenu() As System.Windows.Forms.ContextMenu
            Dim menu As New System.Windows.Forms.ContextMenu
            DefineMenuItems(menu.MenuItems)
            AddHandler menu.Popup, AddressOf ContextMenuPopup
            Return menu
        End Function

        ''' <summary>
        ''' Handle the popup function for the context menu
        ''' </summary>
        Protected Overridable Sub ContextMenuPopup(ByVal sender As Object, ByVal e As System.EventArgs)
            HandlePopup(CType(sender, System.Windows.Forms.ContextMenu).MenuItems)
        End Sub

        Private Sub HandlePopup(ByRef MenuItems As System.Windows.Forms.Menu.MenuItemCollection)
            For Each itm As System.Windows.Forms.MenuItem In MenuItems
                Select Case itm.Text.ToLower()
                    Case "create", "add"
                        itm.Enabled = OkToCreate(CType(GridControl1.DataSource, System.Data.DataView))
                    Case "delete", "remove"
                        itm.Enabled = (ControlRow >= 0) AndAlso OkToDelete(CType(GridView1.GetRow(ControlRow), System.Data.DataRowView))
                    Case "edit", "update", "change", "show"
                        itm.Enabled = (ControlRow >= 0) AndAlso OkToEdit(CType(GridView1.GetRow(ControlRow), System.Data.DataRowView))
                End Select
            Next
        End Sub

        ''' <summary>
        ''' Process a CREATE menu item choice
        ''' </summary>
        Protected Sub MenuItemCreate(ByVal sender As Object, ByVal e As System.EventArgs)

            ' Find the datarowview from the input tables.
            Dim vue As DataView = CType(GridControl1.DataSource, DataView)
            OnCreateItem(vue.Table.DefaultView)
        End Sub

        ''' <summary>
        ''' Process an EDIT menu item choice
        ''' </summary>
        Protected Sub MenuItemEdit(ByVal sender As Object, ByVal e As System.EventArgs)

            ' Find the datarowview from the input tables.
            Dim drv As System.Data.DataRowView = Nothing
            If ControlRow >= 0 Then
                Dim vue As DataView = CType(GridControl1.DataSource, DataView)
                drv = CType(GridView1.GetRow(ControlRow), System.Data.DataRowView)
            End If

            If drv IsNot Nothing Then OnEditItem(drv)
        End Sub

        ''' <summary>
        ''' Process a DELETE menu item
        ''' </summary>
        Protected Sub MenuItemDelete(ByVal sender As Object, ByVal e As System.EventArgs)

            ' Find the datarowview from the input tables.
            Dim drv As System.Data.DataRowView = Nothing
            If ControlRow >= 0 Then
                Dim vue As DataView = CType(GridControl1.DataSource, DataView)
                drv = CType(GridView1.GetRow(ControlRow), System.Data.DataRowView)
            End If

            If drv IsNot Nothing Then OnDeleteItem(drv)
        End Sub

        ''' <summary>
        ''' Overridable function to edit the item in the list
        ''' </summary>
        Protected Overridable Function EditItem(ByVal drv As System.Data.DataRowView) As Boolean
            Return False
        End Function

        Protected Overridable Sub OnEditItem(ByVal drv As System.Data.DataRowView)
            drv.BeginEdit()
            If EditItem(drv) Then
                drv.EndEdit()
            Else
                drv.CancelEdit()
            End If
        End Sub

        ''' <summary>
        ''' Overridable function to delete the item in the list
        ''' </summary>
        Protected Overridable Function DeleteItem(ByVal drv As System.Data.DataRowView) As Boolean
            Return False
        End Function

        Protected Overridable Sub OnDeleteItem(ByVal drv As System.Data.DataRowView)
            If DeleteItem(drv) Then drv.Delete()
        End Sub

        ''' <summary>
        ''' Is the item valid to be deleted?
        ''' </summary>
        Protected Overridable Function OkToDelete(ByVal drv As System.Data.DataRowView) As Boolean
            Return True
        End Function

        ''' <summary>
        ''' Is the item valid to be edited?
        ''' </summary>
        Protected Overridable Function OkToEdit(ByVal drv As System.Data.DataRowView) As Boolean
            Return True
        End Function

        ''' <summary>
        ''' Is the item valid to be created?
        ''' </summary>
        Protected Overridable Function OkToCreate(ByVal vue As System.Data.DataView) As Boolean
            Return True
        End Function

        ''' <summary>
        ''' Overridable function to create an item in the list
        ''' </summary>
        Protected Overridable Function CreateItem(ByVal drv As System.Data.DataRowView) As Boolean
            Return False
        End Function

        Protected Overridable Sub OnCreateItem(ByVal vue As System.Data.DataView)
            Dim drv As System.Data.DataRowView = vue.AddNew
            drv.BeginEdit()
            If CreateItem(drv) Then
                drv.EndEdit()
            Else
                drv.CancelEdit()
            End If
        End Sub

        ''' <summary>
        ''' Overridable function to indicate that the row was selected
        ''' </summary>
        Protected Overridable Sub OnSelectItem(ByVal drv As System.Data.DataRowView)
        End Sub

        ''' <summary>
        ''' Process a change in the current row for the control
        ''' </summary>
        Private Sub GridView1_FocusedRowChanged(ByVal sender As Object, ByVal e As DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs)
            Dim gv As DevExpress.XtraGrid.Views.Grid.GridView = CType(sender, DevExpress.XtraGrid.Views.Grid.GridView)
            Dim ctl As DevExpress.XtraGrid.GridControl = gv.GridControl
            ControlRow = e.FocusedRowHandle

            ' Find the datarowview from the input tables.
            Dim drv As System.Data.DataRowView = Nothing
            If ControlRow >= 0 Then
                Dim vue As DataView = CType(ctl.DataSource, DataView)
                drv = CType(gv.GetRow(ControlRow), System.Data.DataRowView)
            End If

            ' Ask the user to do something with the selected row
            OnSelectItem(drv)
        End Sub

        ''' <summary>
        ''' Double Click on an item -- edit it
        ''' </summary>
        Private Sub GridView1_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs)

            ' Find the row targetted as the double-click item
            Dim gv As DevExpress.XtraGrid.Views.Grid.GridView = CType(sender, DevExpress.XtraGrid.Views.Grid.GridView)
            Dim ctl As DevExpress.XtraGrid.GridControl = CType(gv.GridControl, DevExpress.XtraGrid.GridControl)
            Dim hi As DevExpress.XtraGrid.Views.Grid.ViewInfo.GridHitInfo = gv.CalcHitInfo((ctl.PointToClient(Windows.Forms.Control.MousePosition)))

            If hi.InRow Then
                ControlRow = hi.RowHandle

                ' Find the datarowview from the input tables.
                Dim drv As System.Data.DataRowView = Nothing
                If ControlRow >= 0 Then
                    Dim vue As DataView = CType(ctl.DataSource, DataView)
                    drv = CType(gv.GetRow(ControlRow), System.Data.DataRowView)
                End If

                ' If there is a row then perform the edit operation. If successful, complete the changes else roll them back
                If drv IsNot Nothing Then OnEditItem(drv)
            End If
        End Sub

        ''' <summary>
        ''' If we need it, here is a mouse-down procedure for the gridview
        ''' </summary>
        Private Sub GridView1_MouseDown(ByVal sender As System.Object, ByVal e As System.Windows.Forms.MouseEventArgs)
            Dim gv As DevExpress.XtraGrid.Views.Grid.GridView = GridView1
            Dim ctl As DevExpress.XtraGrid.GridControl = CType(gv.GridControl, DevExpress.XtraGrid.GridControl)
            Dim hi As DevExpress.XtraGrid.Views.Grid.ViewInfo.GridHitInfo = gv.CalcHitInfo(New Point(e.X, e.Y))

            ' Remember the position for the popup menu handler.
            If hi.InRow Then
                ControlRow = hi.RowHandle
            Else
                ControlRow = -1
            End If
        End Sub

        ''' <summary>
        ''' Load the control
        ''' </summary>
        Private Sub BaseGridControl_Load(ByVal sender As Object, ByVal e As System.EventArgs)
            GridControl1.ContextMenu = myContextMenu()
            AddHandler GridView1.DoubleClick, AddressOf GridView1_DoubleClick
            AddHandler GridView1.FocusedRowChanged, AddressOf GridView1_FocusedRowChanged
        End Sub
    End Class
End Namespace
