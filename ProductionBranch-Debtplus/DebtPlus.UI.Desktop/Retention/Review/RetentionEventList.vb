#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}
#End Region

Option Compare Binary
Option Explicit On 
Option Strict On

Imports DebtPlus.Utils
Imports System.Data.SqlClient
Imports System.Windows.Forms

Namespace Retention.Review
    Friend Class RetentionEventList
        Inherits BaseGridControl

#Region " Windows Form Designer generated code "

        Public Sub New()
            MyBase.New()
            InitializeComponent()
        End Sub

        'UserControl overrides dispose to clean up the component list.
        Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
            If disposing Then
                If components IsNot Nothing Then
                    components.Dispose()
                End If
            End If
            MyBase.Dispose(disposing)
        End Sub

        'Required by the Windows Form Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Windows Form Designer
        'It can be modified using the Windows Form Designer.  
        'Do not modify it using the code editor.
        Friend WithEvents GridColumn_date_expired As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents GridColumn_message As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents GridColumn_client_retention_event As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents GridColumn_date_created As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents GridColumn_description As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents GridColumn_amount As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents GridColumn_counselor As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents GridColumn_last_action As DevExpress.XtraGrid.Columns.GridColumn
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
            Dim StyleFormatCondition1 As DevExpress.XtraGrid.StyleFormatCondition = New DevExpress.XtraGrid.StyleFormatCondition
            Me.GridColumn_date_expired = New DevExpress.XtraGrid.Columns.GridColumn
            Me.GridColumn_date_expired.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True
            Me.GridColumn_client_retention_event = New DevExpress.XtraGrid.Columns.GridColumn
            Me.GridColumn_client_retention_event.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True
            Me.GridColumn_date_created = New DevExpress.XtraGrid.Columns.GridColumn
            Me.GridColumn_date_created.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True
            Me.GridColumn_description = New DevExpress.XtraGrid.Columns.GridColumn
            Me.GridColumn_description.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True
            Me.GridColumn_message = New DevExpress.XtraGrid.Columns.GridColumn
            Me.GridColumn_message.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True
            Me.GridColumn_amount = New DevExpress.XtraGrid.Columns.GridColumn
            Me.GridColumn_amount.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True
            Me.GridColumn_counselor = New DevExpress.XtraGrid.Columns.GridColumn
            Me.GridColumn_counselor.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True
            Me.GridColumn_last_action = New DevExpress.XtraGrid.Columns.GridColumn
            Me.GridColumn_last_action.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True
            Me.SuspendLayout()
            '
            'GridColumn_date_expired
            '
            Me.GridColumn_date_expired.AppearanceCell.Options.UseTextOptions = True
            Me.GridColumn_date_expired.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.GridColumn_date_expired.AppearanceHeader.Options.UseTextOptions = True
            Me.GridColumn_date_expired.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.GridColumn_date_expired.Caption = "Expired Date"
            Me.GridColumn_date_expired.DisplayFormat.FormatString = "d"
            Me.GridColumn_date_expired.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime
            Me.GridColumn_date_expired.FieldName = "date_expired"
            Me.GridColumn_date_expired.GroupFormat.FormatString = "d"
            Me.GridColumn_date_expired.GroupFormat.FormatType = DevExpress.Utils.FormatType.DateTime
            Me.GridColumn_date_expired.Name = "GridColumn_date_expired"
            '
            'GridColumn_client_retention_event
            '
            Me.GridColumn_client_retention_event.AppearanceCell.Options.UseTextOptions = True
            Me.GridColumn_client_retention_event.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.GridColumn_client_retention_event.AppearanceHeader.Options.UseTextOptions = True
            Me.GridColumn_client_retention_event.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.GridColumn_client_retention_event.Caption = "ID"
            Me.GridColumn_client_retention_event.CustomizationCaption = "Record ID"
            Me.GridColumn_client_retention_event.DisplayFormat.FormatString = "f0"
            Me.GridColumn_client_retention_event.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.GridColumn_client_retention_event.FieldName = "client_retention_event"
            Me.GridColumn_client_retention_event.GroupFormat.FormatString = "f0"
            Me.GridColumn_client_retention_event.GroupFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.GridColumn_client_retention_event.Name = "GridColumn_client_retention_event"
            Me.GridColumn_client_retention_event.Width = 69
            '
            'GridColumn_date_created
            '
            Me.GridColumn_date_created.AppearanceCell.Options.UseTextOptions = True
            Me.GridColumn_date_created.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.GridColumn_date_created.AppearanceHeader.Options.UseTextOptions = True
            Me.GridColumn_date_created.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.GridColumn_date_created.Caption = "Date"
            Me.GridColumn_date_created.DisplayFormat.FormatString = "d"
            Me.GridColumn_date_created.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime
            Me.GridColumn_date_created.FieldName = "date_created"
            Me.GridColumn_date_created.GroupFormat.FormatString = "d"
            Me.GridColumn_date_created.GroupFormat.FormatType = DevExpress.Utils.FormatType.DateTime
            Me.GridColumn_date_created.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.Date
            Me.GridColumn_date_created.Name = "GridColumn_date_created"
            Me.GridColumn_date_created.Visible = True
            Me.GridColumn_date_created.VisibleIndex = 0
            Me.GridColumn_date_created.Width = 80
            Me.GridColumn_date_created.SortIndex = 0
            Me.GridColumn_date_created.SortMode = DevExpress.XtraGrid.ColumnSortMode.Value
            Me.GridColumn_date_created.SortOrder = DevExpress.Data.ColumnSortOrder.Descending
            '
            'GridColumn_description
            '
            Me.GridColumn_description.Caption = "Description"
            Me.GridColumn_description.FieldName = "event_description"
            Me.GridColumn_description.Name = "GridColumn_description"
            Me.GridColumn_description.Visible = True
            Me.GridColumn_description.VisibleIndex = 1
            Me.GridColumn_description.Width = 286
            '
            'GridColumn_message
            '
            Me.GridColumn_message.Caption = "Message"
            Me.GridColumn_message.FieldName = "message"
            Me.GridColumn_message.Name = "GridColumn_message"
            Me.GridColumn_message.Visible = False
            Me.GridColumn_message.VisibleIndex = -1
            Me.GridColumn_message.Width = 286
            '
            'GridColumn_amount
            '
            Me.GridColumn_amount.AppearanceCell.Options.UseTextOptions = True
            Me.GridColumn_amount.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.GridColumn_amount.AppearanceHeader.Options.UseTextOptions = True
            Me.GridColumn_amount.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.GridColumn_amount.Caption = "Amount"
            Me.GridColumn_amount.DisplayFormat.FormatString = "c"
            Me.GridColumn_amount.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.GridColumn_amount.FieldName = "event_amount"
            Me.GridColumn_amount.GroupFormat.FormatString = "c"
            Me.GridColumn_amount.GroupFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.GridColumn_amount.Name = "GridColumn_amount"
            Me.GridColumn_amount.Visible = True
            Me.GridColumn_amount.VisibleIndex = 2
            Me.GridColumn_amount.Width = 64
            '
            'GridColumn_counselor
            '
            Me.GridColumn_counselor.Caption = "Counselor"
            Me.GridColumn_counselor.FieldName = "created_by"
            Me.GridColumn_counselor.Name = "GridColumn_counselor"
            Me.GridColumn_counselor.Visible = True
            Me.GridColumn_counselor.VisibleIndex = 3
            Me.GridColumn_counselor.Width = 60
            '
            'GridColumn_last_action
            '
            Me.GridColumn_last_action.AppearanceCell.Options.UseTextOptions = True
            Me.GridColumn_last_action.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.GridColumn_last_action.AppearanceHeader.Options.UseTextOptions = True
            Me.GridColumn_last_action.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.GridColumn_last_action.Caption = "Lst Action"
            Me.GridColumn_last_action.DisplayFormat.FormatString = "d"
            Me.GridColumn_last_action.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime
            Me.GridColumn_last_action.FieldName = "action_date"
            Me.GridColumn_last_action.GroupFormat.FormatString = "d"
            Me.GridColumn_last_action.GroupFormat.FormatType = DevExpress.Utils.FormatType.DateTime
            Me.GridColumn_last_action.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.Date
            Me.GridColumn_last_action.Name = "GridColumn_last_action"
            Me.GridColumn_last_action.Visible = True
            Me.GridColumn_last_action.VisibleIndex = 4
            Me.GridColumn_last_action.Width = 90
            '
            'GridView1
            '
            Me.GridView1.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.GridColumn_client_retention_event, Me.GridColumn_date_created, Me.GridColumn_description, Me.GridColumn_amount, Me.GridColumn_counselor, Me.GridColumn_last_action, Me.GridColumn_date_expired, Me.GridColumn_message})
            Me.GridView1.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus
            StyleFormatCondition1.Appearance.BackColor = System.Drawing.Color.FromArgb(CType(255, Byte), CType(192, Byte), CType(192, Byte))
            StyleFormatCondition1.Appearance.BackColor2 = System.Drawing.Color.FromArgb(CType(255, Byte), CType(192, Byte), CType(192, Byte))
            StyleFormatCondition1.Appearance.BorderColor = System.Drawing.Color.FromArgb(CType(255, Byte), CType(192, Byte), CType(192, Byte))
            StyleFormatCondition1.Appearance.Options.UseBackColor = True
            StyleFormatCondition1.Appearance.Options.UseBorderColor = True
            StyleFormatCondition1.ApplyToRow = True
            StyleFormatCondition1.Column = Me.GridColumn_date_expired
            StyleFormatCondition1.Condition = DevExpress.XtraGrid.FormatConditionEnum.GreaterOrEqual
            StyleFormatCondition1.Value1 = New Date(1800, 1, 1, 0, 0, 0, 0)
            Me.GridView1.FormatConditions.AddRange(New DevExpress.XtraGrid.StyleFormatCondition() {StyleFormatCondition1})
            Me.GridView1.GridControl = Me.GridControl1
            Me.GridView1.Name = "GridView1"
            Me.GridView1.OptionsBehavior.Editable = False
            Me.GridView1.OptionsView.ShowGroupPanel = False
            Me.GridView1.OptionsView.ShowIndicator = False
            Me.GridView1.OptionsView.ShowPreview = True
            Me.GridView1.ShowButtonMode = DevExpress.XtraGrid.Views.Base.ShowButtonModeEnum.ShowForFocusedRow
            '
            'Preview lines are the message field
            '
            Me.GridView1.OptionsView.ShowPreviewRowLines = DevExpress.Utils.DefaultBoolean.True
            Me.GridView1.OptionsView.RowAutoHeight = True
            Me.GridView1.OptionsView.AutoCalcPreviewLineCount = True
            Me.GridView1.OptionsView.ShowPreview = True
            Me.GridView1.PreviewFieldName = "event_message"
            '
            'RetentionEventList
            '
            Me.Controls.Add(Me.GridControl1)
            Me.Name = "RetentionEventList"
            Me.Size = New System.Drawing.Size(512, 280)
            Me.ResumeLayout(False)

        End Sub

#End Region

        Private client As System.Int32 = -1
        Public Sub Reload(ByVal Client As System.Int32)
            Dim ds As New System.Data.DataSet("ds")

            ' Save the chagnes to the table before the client is changed
            SaveChanges()

            Me.client = Client
            If Client < 0 Then
                GridControl1.DataSource = Nothing
            Else

                ' Build the command to retrieve the list of retention events for this client
                Dim tbl As System.Data.DataTable = ds.Tables("client_retention_events")
                If tbl IsNot Nothing Then tbl.Reset()

                Using cmd As SqlClient.SqlCommand = New SqlCommand
                    With cmd
                        .Connection = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
                        .CommandText = "SELECT v.client_retention_event, v.client, v.client_counselor, v.client_office, v.priority, v.retention_event, v.event_amount, v.event_message, v.date_created, v.created_by, v.name, v.co_name, v.active_status, v.home_ph, v.work_ph, v.message_ph, v.event_description, v.action_date, v.date_expired, v.expire_type, v.expire_date, v.effective_date, convert(varchar(10), v.date_expired, 101) as formatted_expire_date FROM view_retention_client_events v with (nolock) WHERE v.client=@client"
                        .Parameters.Add("@client", SqlDbType.Int).Value = Client
                    End With

                    ' Read the list of retention events for this client
                    Using da As New SqlClient.SqlDataAdapter(cmd)
                        da.Fill(ds, "client_retention_events")
                    End Using
                End Using
                tbl = ds.Tables(0)

                tbl.PrimaryKey = New System.Data.DataColumn() {tbl.Columns("client_retention_event")}
                With tbl.Columns("client_retention_event")
                    .AutoIncrement = True
                    .AutoIncrementSeed = -1
                    .AutoIncrementStep = -1
                End With

                With GridControl1
                    .BeginUpdate()
                    .DataSource = tbl.DefaultView
                    .RefreshDataSource()
                    .EndUpdate()
                End With

            End If
        End Sub

        Protected Overrides Function EditItem(ByVal drv As System.Data.DataRowView) As Boolean
            Dim answer As Boolean
            Using frm As New Form_Retention_Event(drv)
                answer = (frm.ShowDialog() = DialogResult.OK)
            End Using

            Return answer
        End Function

        Protected Overrides Function CreateItem(ByVal drv As System.Data.DataRowView) As Boolean
            drv("client") = client
            drv("created_by") = "me"
            drv("date_created") = Now
            drv("expire_type") = 1
            Return EditItem(drv)
        End Function

        Protected Overrides Function DeleteItem(ByVal drv As System.Data.DataRowView) As Boolean

            ' Items no longer pending are not deletable
            Dim Pending As Boolean = True
            If drv("date_expired") IsNot Nothing AndAlso drv("date_expired") IsNot System.DBNull.Value Then Pending = False
            If Not Pending Then Return False

            ' Ask if the item is to be deleted
            Return DebtPlus.Data.Prompts.RequestConfirmation_Delete = DialogResult.OK
        End Function

        Public Sub SaveChanges()
            Dim vue As System.Data.DataView = CType(GridControl1.DataSource, System.Data.DataView)
            If vue IsNot Nothing Then
                Dim tbl As System.Data.DataTable = vue.Table

                ' Do the deletion items first. Delete the action items as well for any item being deleted
                Dim da As New SqlClient.SqlDataAdapter
                Dim DeleteCmd As SqlClient.SqlCommand = New SqlCommand
                Dim UpdateCmd As SqlClient.SqlCommand = New SqlCommand
                Dim InsertCmd As SqlClient.SqlCommand = New SqlCommand
                Dim current_cursor As Cursor = System.Windows.Forms.Cursor.Current
                Try
                    System.Windows.Forms.Cursor.Current = Cursors.WaitCursor

                    With DeleteCmd
                        .Connection = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
                        .CommandText = "DELETE FROM client_retention_actions WHERE client_retention_event=@client_retention_event; DELETE FROM client_retention_events WHERE client_retention_event=@client_retention_event"
                        .Parameters.Add("@client_retention_event", SqlDbType.Int, CByte(0), "client_retention_event")
                    End With
                    da.DeleteCommand = DeleteCmd

                    ' Do the update of the retention events
                    With UpdateCmd
                        .Connection = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
                        .CommandText = "UPDATE client_retention_events SET retention_event=@retention_event, amount=@amount, priority=@priority, message=@message, expire_date=@expire_date, expire_type=@expire_type WHERE client_retention_event=@client_retention_event"
                        .Parameters.Add("@retention_event", SqlDbType.Int, CByte(0), "retention_event")
                        .Parameters.Add("@amount", SqlDbType.Decimal, CByte(0), "event_amount")
                        .Parameters.Add("@priority", SqlDbType.Int, CByte(0), "priority")
                        .Parameters.Add("@message", SqlDbType.VarChar, 1024, "event_message")
                        .Parameters.Add("@expire_date", SqlDbType.VarChar, 10, "formatted_expire_date")
                        .Parameters.Add("@expire_type", SqlDbType.Int, CByte(0), "expire_type")
                        .Parameters.Add("@client_retention_event", SqlDbType.Int, CByte(0), "client_retention_event")
                    End With
                    da.UpdateCommand = UpdateCmd

                    ' Do the insert of the retention events
                    With InsertCmd
                        .Connection = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
                        .CommandText = "xpr_client_retention_event_create"
                        .CommandType = CommandType.StoredProcedure
                        .Parameters.Add("@client_retention_event", SqlDbType.Int, CByte(0), "client_retention_event").Direction = ParameterDirection.ReturnValue
                        .Parameters.Add("@retention_event", SqlDbType.Int, CByte(0), "retention_event")
                        .Parameters.Add("@amount", SqlDbType.Decimal, CByte(0), "event_amount")
                        .Parameters.Add("@priority", SqlDbType.Int, CByte(0), "priority")
                        .Parameters.Add("@message", SqlDbType.VarChar, 1024, "event_message")
                        .Parameters.Add("@expire_date", SqlDbType.VarChar, 14, "formatted_expire_date")
                        .Parameters.Add("@expire_type", SqlDbType.Int, CByte(0), "expire_type")
                    End With
                    da.InsertCommand = InsertCmd

                    ' Update the table with the retention events
                    da.Update(tbl)

                Catch ex As SqlClient.SqlException
                    DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error updating client_retention_events table")

                Finally
                    da.Dispose()
                    InsertCmd.Dispose()
                    UpdateCmd.Dispose()
                    DeleteCmd.Dispose()
                    System.Windows.Forms.Cursor.Current = current_cursor
                End Try
            End If
        End Sub
    End Class
End Namespace
