#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}

#End Region

Option Compare Binary
Option Explicit On
Option Strict On

Imports System.ComponentModel
Imports DebtPlus.UI.Desktop.Retention.Common
Imports DebtPlus.Data.Forms
Imports System.Text
Imports System.Data.SqlClient
Imports System.Threading
Imports DebtPlus.UI.Client.Service
Imports DebtPlus.Reports
Imports System.Windows.Forms
Imports System.Drawing

Namespace Retention.Review
    Friend Class Form_Review
        Inherits DebtPlusForm

        Protected ControlRow As Int32
        Private ReadOnly ap As ArgumentParser
        Private IsSearch As Boolean

        Public Sub New(ByVal search As Boolean)
            MyBase.New()
            InitializeComponent()
            IsSearch = search
        End Sub

        Public Sub New(ByVal search As Boolean, ByVal Args As ArgumentParser)
            MyBase.New()
            InitializeComponent()

            AddHandler MyBase.Closing, AddressOf Form_Closing
            AddHandler Me.Load, AddressOf Form_Load
            AddHandler DataNavigator1.PositionChanged, AddressOf DataNavigator1_PositionChanged
            AddHandler MenuFileRefresh.ItemClick, AddressOf MenuFileRefresh_Click
            AddHandler MenuFileNew.ItemClick, AddressOf MenuFileNew_Click
            AddHandler MenuFileDepositsSecond.ItemClick, AddressOf MenuFileDepositsSecond_Click
            AddHandler MenuFileDepositsFirst.ItemClick, AddressOf MenuFileDepositsFirst_Click
            AddHandler Button_Cancel.Click, AddressOf ButtonCancel_Click
            AddHandler MenuFileExit.ItemClick, AddressOf ButtonCancel_Click
            AddHandler MenuFile.Popup, AddressOf MenuFile_Popup
            AddHandler MenuFileAppointmentsAny.ItemClick, AddressOf MenuFileAppointmentsAny_Click
            AddHandler MenuFileAppointmentsFirst.ItemClick, AddressOf MenuFileAppointmentsFirst_Click
            AddHandler MenuFileAppointmentsSecond.ItemClick, AddressOf MenuFileAppointmentsSecond_Click
            AddHandler MenuFileDepositsAny.ItemClick, AddressOf MenuFileDepositsAny_Click
            AddHandler ButtonShow.Click, AddressOf ButtonShow_Click
            AddHandler MenuFilePrint.ItemClick, AddressOf MenuFilePrint_Click

            ap = Args
            IsSearch = search
        End Sub

#Region " Windows Form Designer generated code "

        'Form overrides dispose to clean up the component list.
        Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
            If disposing Then
                If components IsNot Nothing Then
                    components.Dispose()
                End If
            End If
            MyBase.Dispose(disposing)
        End Sub

        'Required by the Windows Form Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Windows Form Designer
        'It can be modified using the Windows Form Designer.  
        'Do not modify it using the code editor.
        <System.Diagnostics.DebuggerStepThrough()>
        Private Sub InitializeComponent()
            Me.components = New System.ComponentModel.Container()
            Me.LabelClient = New DevExpress.XtraEditors.LabelControl()
            Me.LayoutControl1 = New DevExpress.XtraLayout.LayoutControl()
            Me.Button_Cancel = New DevExpress.XtraEditors.SimpleButton()
            Me.RetentionEventList1 = New RetentionEventList()
            Me.ButtonShow = New DevExpress.XtraEditors.SimpleButton()
            Me.LabelName = New DevExpress.XtraEditors.LabelControl()
            Me.LabelCoApplicant = New DevExpress.XtraEditors.LabelControl()
            Me.LabelStatus = New DevExpress.XtraEditors.LabelControl()
            Me.LabelHomePhone = New DevExpress.XtraEditors.LabelControl()
            Me.LabelWorkPhone = New DevExpress.XtraEditors.LabelControl()
            Me.DataNavigator1 = New DevExpress.XtraEditors.DataNavigator()
            Me.LabelMsgPhone = New DevExpress.XtraEditors.LabelControl()
            Me.LayoutControlGroup1 = New DevExpress.XtraLayout.LayoutControlGroup()
            Me.LayoutControlItem1 = New DevExpress.XtraLayout.LayoutControlItem()
            Me.LayoutControlItem2 = New DevExpress.XtraLayout.LayoutControlItem()
            Me.LayoutControlItem8 = New DevExpress.XtraLayout.LayoutControlItem()
            Me.LayoutControlItem9 = New DevExpress.XtraLayout.LayoutControlItem()
            Me.LayoutControlItem10 = New DevExpress.XtraLayout.LayoutControlItem()
            Me.LayoutControlItem11 = New DevExpress.XtraLayout.LayoutControlItem()
            Me.LayoutControlItem3 = New DevExpress.XtraLayout.LayoutControlItem()
            Me.LayoutControlItem6 = New DevExpress.XtraLayout.LayoutControlItem()
            Me.LayoutControlItem7 = New DevExpress.XtraLayout.LayoutControlItem()
            Me.EmptySpaceItem1 = New DevExpress.XtraLayout.EmptySpaceItem()
            Me.EmptySpaceItem2 = New DevExpress.XtraLayout.EmptySpaceItem()
            Me.LayoutControlItem4 = New DevExpress.XtraLayout.LayoutControlItem()
            Me.LayoutControlItem5 = New DevExpress.XtraLayout.LayoutControlItem()
            Me.barManager1 = New DevExpress.XtraBars.BarManager(Me.components)
            Me.Bar2 = New DevExpress.XtraBars.Bar()
            Me.MenuFile = New DevExpress.XtraBars.BarSubItem()
            Me.MenuFileDeposits = New DevExpress.XtraBars.BarSubItem()
            Me.MenuFileDepositsAny = New DevExpress.XtraBars.BarCheckItem()
            Me.MenuFileDepositsFirst = New DevExpress.XtraBars.BarCheckItem()
            Me.MenuFileDepositsSecond = New DevExpress.XtraBars.BarCheckItem()
            Me.MenuFileAppointments = New DevExpress.XtraBars.BarSubItem()
            Me.MenuFileAppointmentsAny = New DevExpress.XtraBars.BarCheckItem()
            Me.MenuFileAppointmentsFirst = New DevExpress.XtraBars.BarCheckItem()
            Me.MenuFileAppointmentsSecond = New DevExpress.XtraBars.BarCheckItem()
            Me.MenuFileNew = New DevExpress.XtraBars.BarCheckItem()
            Me.MenuFileRefresh = New DevExpress.XtraBars.BarCheckItem()
            Me.MenuFilePrint = New DevExpress.XtraBars.BarCheckItem()
            Me.MenuFileExit = New DevExpress.XtraBars.BarCheckItem()
            Me.MenuEdit = New DevExpress.XtraBars.BarSubItem()
            Me.MenuEditCut = New DevExpress.XtraBars.BarCheckItem()
            Me.MenuEditCopy = New DevExpress.XtraBars.BarCheckItem()
            Me.MenuEditPaste = New DevExpress.XtraBars.BarCheckItem()
            Me.barDockControlTop = New DevExpress.XtraBars.BarDockControl()
            Me.barDockControlBottom = New DevExpress.XtraBars.BarDockControl()
            Me.barDockControlLeft = New DevExpress.XtraBars.BarDockControl()
            Me.barDockControlRight = New DevExpress.XtraBars.BarDockControl()
            CType(Me.DxErrorProvider1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControl1, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.LayoutControl1.SuspendLayout()
            CType(Me.LayoutControlGroup1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem2, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem8, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem9, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem10, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem11, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem3, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem6, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem7, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.EmptySpaceItem1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.EmptySpaceItem2, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem4, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem5, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.barManager1, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.SuspendLayout()
            '
            'LabelClient
            '
            Me.LabelClient.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
            Me.LabelClient.Location = New System.Drawing.Point(76, 12)
            Me.LabelClient.Name = "LabelClient"
            Me.LabelClient.Size = New System.Drawing.Size(299, 13)
            Me.LabelClient.StyleController = Me.LayoutControl1
            Me.LabelClient.TabIndex = 5
            '
            'LayoutControl1
            '
            Me.LayoutControl1.Controls.Add(Me.LabelClient)
            Me.LayoutControl1.Controls.Add(Me.Button_Cancel)
            Me.LayoutControl1.Controls.Add(Me.RetentionEventList1)
            Me.LayoutControl1.Controls.Add(Me.ButtonShow)
            Me.LayoutControl1.Controls.Add(Me.LabelName)
            Me.LayoutControl1.Controls.Add(Me.LabelCoApplicant)
            Me.LayoutControl1.Controls.Add(Me.LabelStatus)
            Me.LayoutControl1.Controls.Add(Me.LabelHomePhone)
            Me.LayoutControl1.Controls.Add(Me.LabelWorkPhone)
            Me.LayoutControl1.Controls.Add(Me.DataNavigator1)
            Me.LayoutControl1.Controls.Add(Me.LabelMsgPhone)
            Me.LayoutControl1.Dock = System.Windows.Forms.DockStyle.Fill
            Me.LayoutControl1.Location = New System.Drawing.Point(0, 24)
            Me.LayoutControl1.Name = "LayoutControl1"
            Me.LayoutControl1.Root = Me.LayoutControlGroup1
            Me.LayoutControl1.Size = New System.Drawing.Size(544, 438)
            Me.LayoutControl1.TabIndex = 19
            Me.LayoutControl1.Text = "LayoutControl1"
            '
            'Button_Cancel
            '
            Me.Button_Cancel.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.Button_Cancel.Location = New System.Drawing.Point(457, 39)
            Me.Button_Cancel.MaximumSize = New System.Drawing.Size(75, 23)
            Me.Button_Cancel.MinimumSize = New System.Drawing.Size(75, 23)
            Me.Button_Cancel.Name = "Button_Cancel"
            Me.Button_Cancel.Size = New System.Drawing.Size(75, 23)
            Me.Button_Cancel.StyleController = Me.LayoutControl1
            Me.Button_Cancel.TabIndex = 17
            Me.Button_Cancel.Text = "&Cancel"
            '
            'RetentionEventList1
            '
            Me.RetentionEventList1.Location = New System.Drawing.Point(12, 107)
            Me.RetentionEventList1.Name = "RetentionEventList1"
            Me.RetentionEventList1.Size = New System.Drawing.Size(520, 296)
            Me.RetentionEventList1.TabIndex = 18
            '
            'ButtonShow
            '
            Me.ButtonShow.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.ButtonShow.Location = New System.Drawing.Point(457, 12)
            Me.ButtonShow.MaximumSize = New System.Drawing.Size(75, 23)
            Me.ButtonShow.MinimumSize = New System.Drawing.Size(75, 23)
            Me.ButtonShow.Name = "ButtonShow"
            Me.ButtonShow.Size = New System.Drawing.Size(75, 23)
            Me.ButtonShow.StyleController = Me.LayoutControl1
            Me.ButtonShow.TabIndex = 16
            Me.ButtonShow.Text = "&Show..."
            '
            'LabelName
            '
            Me.LabelName.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
            Me.LabelName.Location = New System.Drawing.Point(76, 29)
            Me.LabelName.Name = "LabelName"
            Me.LabelName.Size = New System.Drawing.Size(299, 13)
            Me.LabelName.StyleController = Me.LayoutControl1
            Me.LabelName.TabIndex = 6
            '
            'LabelCoApplicant
            '
            Me.LabelCoApplicant.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
            Me.LabelCoApplicant.Location = New System.Drawing.Point(76, 46)
            Me.LabelCoApplicant.Name = "LabelCoApplicant"
            Me.LabelCoApplicant.Size = New System.Drawing.Size(299, 13)
            Me.LabelCoApplicant.StyleController = Me.LayoutControl1
            Me.LabelCoApplicant.TabIndex = 7
            '
            'LabelStatus
            '
            Me.LabelStatus.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
            Me.LabelStatus.Location = New System.Drawing.Point(76, 63)
            Me.LabelStatus.Name = "LabelStatus"
            Me.LabelStatus.Size = New System.Drawing.Size(99, 13)
            Me.LabelStatus.StyleController = Me.LayoutControl1
            Me.LabelStatus.TabIndex = 8
            '
            'LabelHomePhone
            '
            Me.LabelHomePhone.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
            Me.LabelHomePhone.Location = New System.Drawing.Point(243, 80)
            Me.LabelHomePhone.Name = "LabelHomePhone"
            Me.LabelHomePhone.Size = New System.Drawing.Size(132, 13)
            Me.LabelHomePhone.StyleController = Me.LayoutControl1
            Me.LabelHomePhone.TabIndex = 9
            '
            'LabelWorkPhone
            '
            Me.LabelWorkPhone.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
            Me.LabelWorkPhone.Location = New System.Drawing.Point(243, 63)
            Me.LabelWorkPhone.Name = "LabelWorkPhone"
            Me.LabelWorkPhone.Size = New System.Drawing.Size(132, 13)
            Me.LabelWorkPhone.StyleController = Me.LayoutControl1
            Me.LabelWorkPhone.TabIndex = 13
            '
            'DataNavigator1
            '
            Me.DataNavigator1.Buttons.Append.Visible = False
            Me.DataNavigator1.Buttons.CancelEdit.Visible = False
            Me.DataNavigator1.Buttons.EndEdit.Visible = False
            Me.DataNavigator1.Buttons.Remove.Visible = False
            Me.DataNavigator1.Location = New System.Drawing.Point(12, 407)
            Me.DataNavigator1.Name = "DataNavigator1"
            Me.DataNavigator1.Size = New System.Drawing.Size(520, 19)
            Me.DataNavigator1.StyleController = Me.LayoutControl1
            Me.DataNavigator1.TabIndex = 15
            Me.DataNavigator1.Text = "DataNavigator1"
            Me.DataNavigator1.TextLocation = DevExpress.XtraEditors.NavigatorButtonsTextLocation.Center
            '
            'LabelMsgPhone
            '
            Me.LabelMsgPhone.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
            Me.LabelMsgPhone.Location = New System.Drawing.Point(76, 80)
            Me.LabelMsgPhone.Name = "LabelMsgPhone"
            Me.LabelMsgPhone.Size = New System.Drawing.Size(99, 13)
            Me.LabelMsgPhone.StyleController = Me.LayoutControl1
            Me.LabelMsgPhone.TabIndex = 12
            '
            'LayoutControlGroup1
            '
            Me.LayoutControlGroup1.CustomizationFormText = "LayoutControlGroup1"
            Me.LayoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.[True]
            Me.LayoutControlGroup1.GroupBordersVisible = False
            Me.LayoutControlGroup1.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.LayoutControlItem1, Me.LayoutControlItem2, Me.LayoutControlItem8, Me.LayoutControlItem9, Me.LayoutControlItem10, Me.LayoutControlItem11, Me.LayoutControlItem3, Me.LayoutControlItem6, Me.LayoutControlItem7, Me.EmptySpaceItem1, Me.EmptySpaceItem2, Me.LayoutControlItem4, Me.LayoutControlItem5})
            Me.LayoutControlGroup1.Location = New System.Drawing.Point(0, 0)
            Me.LayoutControlGroup1.Name = "LayoutControlGroup1"
            Me.LayoutControlGroup1.Size = New System.Drawing.Size(544, 438)
            Me.LayoutControlGroup1.Text = "LayoutControlGroup1"
            Me.LayoutControlGroup1.TextVisible = False
            '
            'LayoutControlItem1
            '
            Me.LayoutControlItem1.Control = Me.LabelClient
            Me.LayoutControlItem1.CustomizationFormText = "Client"
            Me.LayoutControlItem1.Location = New System.Drawing.Point(0, 0)
            Me.LayoutControlItem1.Name = "LayoutControlItem1"
            Me.LayoutControlItem1.Size = New System.Drawing.Size(367, 17)
            Me.LayoutControlItem1.Text = "Client"
            Me.LayoutControlItem1.TextSize = New System.Drawing.Size(60, 13)
            '
            'LayoutControlItem2
            '
            Me.LayoutControlItem2.Control = Me.LabelName
            Me.LayoutControlItem2.CustomizationFormText = "Name"
            Me.LayoutControlItem2.Location = New System.Drawing.Point(0, 17)
            Me.LayoutControlItem2.Name = "LayoutControlItem2"
            Me.LayoutControlItem2.Size = New System.Drawing.Size(367, 17)
            Me.LayoutControlItem2.Text = "Name"
            Me.LayoutControlItem2.TextSize = New System.Drawing.Size(60, 13)
            '
            'LayoutControlItem8
            '
            Me.LayoutControlItem8.Control = Me.RetentionEventList1
            Me.LayoutControlItem8.CustomizationFormText = "Events List"
            Me.LayoutControlItem8.Location = New System.Drawing.Point(0, 95)
            Me.LayoutControlItem8.Name = "LayoutControlItem8"
            Me.LayoutControlItem8.Size = New System.Drawing.Size(524, 300)
            Me.LayoutControlItem8.Text = "Events List"
            Me.LayoutControlItem8.TextSize = New System.Drawing.Size(0, 0)
            Me.LayoutControlItem8.TextToControlDistance = 0
            Me.LayoutControlItem8.TextVisible = False
            '
            'LayoutControlItem9
            '
            Me.LayoutControlItem9.Control = Me.DataNavigator1
            Me.LayoutControlItem9.ControlAlignment = System.Drawing.ContentAlignment.BottomCenter
            Me.LayoutControlItem9.CustomizationFormText = "Record Control"
            Me.LayoutControlItem9.ImageAlignment = System.Drawing.ContentAlignment.BottomCenter
            Me.LayoutControlItem9.Location = New System.Drawing.Point(0, 395)
            Me.LayoutControlItem9.MaxSize = New System.Drawing.Size(0, 23)
            Me.LayoutControlItem9.MinSize = New System.Drawing.Size(177, 23)
            Me.LayoutControlItem9.Name = "LayoutControlItem9"
            Me.LayoutControlItem9.Size = New System.Drawing.Size(524, 23)
            Me.LayoutControlItem9.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom
            Me.LayoutControlItem9.Text = "Record Control"
            Me.LayoutControlItem9.TextSize = New System.Drawing.Size(0, 0)
            Me.LayoutControlItem9.TextToControlDistance = 0
            Me.LayoutControlItem9.TextVisible = False
            '
            'LayoutControlItem10
            '
            Me.LayoutControlItem10.Control = Me.ButtonShow
            Me.LayoutControlItem10.CustomizationFormText = "LayoutControlItem10"
            Me.LayoutControlItem10.Location = New System.Drawing.Point(445, 0)
            Me.LayoutControlItem10.Name = "LayoutControlItem10"
            Me.LayoutControlItem10.Size = New System.Drawing.Size(79, 27)
            Me.LayoutControlItem10.Text = "LayoutControlItem10"
            Me.LayoutControlItem10.TextSize = New System.Drawing.Size(0, 0)
            Me.LayoutControlItem10.TextToControlDistance = 0
            Me.LayoutControlItem10.TextVisible = False
            '
            'LayoutControlItem11
            '
            Me.LayoutControlItem11.Control = Me.Button_Cancel
            Me.LayoutControlItem11.CustomizationFormText = "LayoutControlItem11"
            Me.LayoutControlItem11.Location = New System.Drawing.Point(445, 27)
            Me.LayoutControlItem11.Name = "LayoutControlItem11"
            Me.LayoutControlItem11.Size = New System.Drawing.Size(79, 58)
            Me.LayoutControlItem11.Text = "LayoutControlItem11"
            Me.LayoutControlItem11.TextSize = New System.Drawing.Size(0, 0)
            Me.LayoutControlItem11.TextToControlDistance = 0
            Me.LayoutControlItem11.TextVisible = False
            '
            'LayoutControlItem3
            '
            Me.LayoutControlItem3.Control = Me.LabelCoApplicant
            Me.LayoutControlItem3.CustomizationFormText = "CoApplicant"
            Me.LayoutControlItem3.Location = New System.Drawing.Point(0, 34)
            Me.LayoutControlItem3.Name = "LayoutControlItem3"
            Me.LayoutControlItem3.Size = New System.Drawing.Size(367, 17)
            Me.LayoutControlItem3.Text = "CoApplicant"
            Me.LayoutControlItem3.TextSize = New System.Drawing.Size(60, 13)
            '
            'LayoutControlItem6
            '
            Me.LayoutControlItem6.Control = Me.LabelMsgPhone
            Me.LayoutControlItem6.CustomizationFormText = "Msg Phone"
            Me.LayoutControlItem6.Location = New System.Drawing.Point(0, 68)
            Me.LayoutControlItem6.Name = "LayoutControlItem6"
            Me.LayoutControlItem6.Size = New System.Drawing.Size(167, 17)
            Me.LayoutControlItem6.Text = "Msg Phone"
            Me.LayoutControlItem6.TextSize = New System.Drawing.Size(60, 13)
            '
            'LayoutControlItem7
            '
            Me.LayoutControlItem7.Control = Me.LabelWorkPhone
            Me.LayoutControlItem7.CustomizationFormText = "Work Phone"
            Me.LayoutControlItem7.Location = New System.Drawing.Point(167, 51)
            Me.LayoutControlItem7.Name = "LayoutControlItem7"
            Me.LayoutControlItem7.Size = New System.Drawing.Size(200, 17)
            Me.LayoutControlItem7.Text = "Work Phone"
            Me.LayoutControlItem7.TextSize = New System.Drawing.Size(60, 13)
            '
            'EmptySpaceItem1
            '
            Me.EmptySpaceItem1.AllowHotTrack = False
            Me.EmptySpaceItem1.CustomizationFormText = "EmptySpaceItem1"
            Me.EmptySpaceItem1.Location = New System.Drawing.Point(0, 85)
            Me.EmptySpaceItem1.MaxSize = New System.Drawing.Size(0, 10)
            Me.EmptySpaceItem1.MinSize = New System.Drawing.Size(10, 10)
            Me.EmptySpaceItem1.Name = "EmptySpaceItem1"
            Me.EmptySpaceItem1.Size = New System.Drawing.Size(524, 10)
            Me.EmptySpaceItem1.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom
            Me.EmptySpaceItem1.Text = "EmptySpaceItem1"
            Me.EmptySpaceItem1.TextSize = New System.Drawing.Size(0, 0)
            '
            'EmptySpaceItem2
            '
            Me.EmptySpaceItem2.AllowHotTrack = False
            Me.EmptySpaceItem2.CustomizationFormText = "EmptySpaceItem2"
            Me.EmptySpaceItem2.Location = New System.Drawing.Point(367, 0)
            Me.EmptySpaceItem2.Name = "EmptySpaceItem2"
            Me.EmptySpaceItem2.Size = New System.Drawing.Size(78, 85)
            Me.EmptySpaceItem2.Text = "EmptySpaceItem2"
            Me.EmptySpaceItem2.TextSize = New System.Drawing.Size(0, 0)
            '
            'LayoutControlItem4
            '
            Me.LayoutControlItem4.Control = Me.LabelStatus
            Me.LayoutControlItem4.CustomizationFormText = "Status"
            Me.LayoutControlItem4.Location = New System.Drawing.Point(0, 51)
            Me.LayoutControlItem4.Name = "LayoutControlItem4"
            Me.LayoutControlItem4.Size = New System.Drawing.Size(167, 17)
            Me.LayoutControlItem4.Text = "Status"
            Me.LayoutControlItem4.TextSize = New System.Drawing.Size(60, 13)
            '
            'LayoutControlItem5
            '
            Me.LayoutControlItem5.Control = Me.LabelHomePhone
            Me.LayoutControlItem5.CustomizationFormText = "Home Phone"
            Me.LayoutControlItem5.Location = New System.Drawing.Point(167, 68)
            Me.LayoutControlItem5.Name = "LayoutControlItem5"
            Me.LayoutControlItem5.Size = New System.Drawing.Size(200, 17)
            Me.LayoutControlItem5.Text = "Home Phone"
            Me.LayoutControlItem5.TextSize = New System.Drawing.Size(60, 13)
            '
            'barManager1
            '
            Me.barManager1.Bars.AddRange(New DevExpress.XtraBars.Bar() {Me.Bar2})
            Me.barManager1.DockControls.Add(Me.barDockControlTop)
            Me.barManager1.DockControls.Add(Me.barDockControlBottom)
            Me.barManager1.DockControls.Add(Me.barDockControlLeft)
            Me.barManager1.DockControls.Add(Me.barDockControlRight)
            Me.barManager1.Form = Me
            Me.barManager1.Items.AddRange(New DevExpress.XtraBars.BarItem() {Me.MenuFile, Me.MenuEdit, Me.MenuEditCopy, Me.MenuEditPaste, Me.MenuFileDeposits, Me.MenuFileAppointments, Me.MenuFileNew, Me.MenuFileRefresh, Me.MenuFilePrint, Me.MenuFileExit, Me.MenuFileDepositsAny, Me.MenuFileDepositsFirst, Me.MenuFileDepositsSecond, Me.MenuFileAppointmentsAny, Me.MenuFileAppointmentsFirst, Me.MenuFileAppointmentsSecond, Me.MenuEditCut})
            Me.barManager1.MainMenu = Me.Bar2
            Me.barManager1.MaxItemId = 18
            '
            'Bar2
            '
            Me.Bar2.BarName = "Main menu"
            Me.Bar2.DockCol = 0
            Me.Bar2.DockRow = 0
            Me.Bar2.DockStyle = DevExpress.XtraBars.BarDockStyle.Top
            Me.Bar2.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.MenuFile), New DevExpress.XtraBars.LinkPersistInfo(Me.MenuEdit)})
            Me.Bar2.OptionsBar.MultiLine = True
            Me.Bar2.OptionsBar.UseWholeRow = True
            Me.Bar2.Text = "Main menu"
            '
            'MenuFile
            '
            Me.MenuFile.Caption = "&File"
            Me.MenuFile.Id = 0
            Me.MenuFile.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.MenuFileDeposits), New DevExpress.XtraBars.LinkPersistInfo(Me.MenuFileAppointments), New DevExpress.XtraBars.LinkPersistInfo(Me.MenuFileNew, True), New DevExpress.XtraBars.LinkPersistInfo(Me.MenuFileRefresh), New DevExpress.XtraBars.LinkPersistInfo(Me.MenuFilePrint), New DevExpress.XtraBars.LinkPersistInfo(Me.MenuFileExit, True)})
            Me.MenuFile.Name = "MenuFile"
            '
            'MenuFileDeposits
            '
            Me.MenuFileDeposits.Caption = "&Deposits"
            Me.MenuFileDeposits.Id = 5
            Me.MenuFileDeposits.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.MenuFileDepositsAny), New DevExpress.XtraBars.LinkPersistInfo(Me.MenuFileDepositsFirst), New DevExpress.XtraBars.LinkPersistInfo(Me.MenuFileDepositsSecond)})
            Me.MenuFileDeposits.Name = "MenuFileDeposits"
            '
            'MenuFileDepositsAny
            '
            Me.MenuFileDepositsAny.Caption = "Missed &Any Deposit"
            Me.MenuFileDepositsAny.Id = 11
            Me.MenuFileDepositsAny.Name = "MenuFileDepositsAny"
            '
            'MenuFileDepositsFirst
            '
            Me.MenuFileDepositsFirst.Caption = "Missed &First Deposit"
            Me.MenuFileDepositsFirst.Id = 12
            Me.MenuFileDepositsFirst.Name = "MenuFileDepositsFirst"
            '
            'MenuFileDepositsSecond
            '
            Me.MenuFileDepositsSecond.Caption = "Missed &Subsequent Deposit"
            Me.MenuFileDepositsSecond.Id = 13
            Me.MenuFileDepositsSecond.Name = "MenuFileDepositsSecond"
            '
            'MenuFileAppointments
            '
            Me.MenuFileAppointments.Caption = "&Appointments"
            Me.MenuFileAppointments.Id = 6
            Me.MenuFileAppointments.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.MenuFileAppointmentsAny), New DevExpress.XtraBars.LinkPersistInfo(Me.MenuFileAppointmentsFirst), New DevExpress.XtraBars.LinkPersistInfo(Me.MenuFileAppointmentsSecond)})
            Me.MenuFileAppointments.Name = "MenuFileAppointments"
            '
            'MenuFileAppointmentsAny
            '
            Me.MenuFileAppointmentsAny.Caption = "Missed &Any Appointment"
            Me.MenuFileAppointmentsAny.Id = 14
            Me.MenuFileAppointmentsAny.Name = "MenuFileAppointmentsAny"
            '
            'MenuFileAppointmentsFirst
            '
            Me.MenuFileAppointmentsFirst.Caption = "Missed &First Appointment"
            Me.MenuFileAppointmentsFirst.Id = 15
            Me.MenuFileAppointmentsFirst.Name = "MenuFileAppointmentsFirst"
            '
            'MenuFileAppointmentsSecond
            '
            Me.MenuFileAppointmentsSecond.Caption = "Missed &Subsequent Appointment"
            Me.MenuFileAppointmentsSecond.Id = 16
            Me.MenuFileAppointmentsSecond.Name = "MenuFileAppointmentsSecond"
            '
            'MenuFileNew
            '
            Me.MenuFileNew.Caption = "&New ..."
            Me.MenuFileNew.Id = 7
            Me.MenuFileNew.Name = "MenuFileNew"
            '
            'MenuFileRefresh
            '
            Me.MenuFileRefresh.Caption = "&Refresh"
            Me.MenuFileRefresh.Id = 8
            Me.MenuFileRefresh.Name = "MenuFileRefresh"
            '
            'MenuFilePrint
            '
            Me.MenuFilePrint.Caption = "&Print ..."
            Me.MenuFilePrint.Id = 9
            Me.MenuFilePrint.Name = "MenuFilePrint"
            '
            'MenuFileExit
            '
            Me.MenuFileExit.Caption = "&Exit"
            Me.MenuFileExit.Id = 10
            Me.MenuFileExit.Name = "MenuFileExit"
            '
            'MenuEdit
            '
            Me.MenuEdit.Caption = "&Edit"
            Me.MenuEdit.Id = 1
            Me.MenuEdit.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.MenuEditCut), New DevExpress.XtraBars.LinkPersistInfo(Me.MenuEditCopy), New DevExpress.XtraBars.LinkPersistInfo(Me.MenuEditPaste)})
            Me.MenuEdit.Name = "MenuEdit"
            '
            'MenuEditCut
            '
            Me.MenuEditCut.Caption = "Cu&t"
            Me.MenuEditCut.Id = 2
            Me.MenuEditCut.Name = "MenuEditCut"
            '
            'MenuEditCopy
            '
            Me.MenuEditCopy.Caption = "&Copy"
            Me.MenuEditCopy.Id = 3
            Me.MenuEditCopy.Name = "MenuEditCopy"
            '
            'MenuEditPaste
            '
            Me.MenuEditPaste.Caption = "&Paste"
            Me.MenuEditPaste.Id = 4
            Me.MenuEditPaste.Name = "MenuEditPaste"
            '
            'barDockControlTop
            '
            Me.barDockControlTop.CausesValidation = False
            Me.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top
            Me.barDockControlTop.Location = New System.Drawing.Point(0, 0)
            Me.barDockControlTop.Size = New System.Drawing.Size(544, 24)
            '
            'barDockControlBottom
            '
            Me.barDockControlBottom.CausesValidation = False
            Me.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom
            Me.barDockControlBottom.Location = New System.Drawing.Point(0, 462)
            Me.barDockControlBottom.Size = New System.Drawing.Size(544, 0)
            '
            'barDockControlLeft
            '
            Me.barDockControlLeft.CausesValidation = False
            Me.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left
            Me.barDockControlLeft.Location = New System.Drawing.Point(0, 24)
            Me.barDockControlLeft.Size = New System.Drawing.Size(0, 438)
            '
            'barDockControlRight
            '
            Me.barDockControlRight.CausesValidation = False
            Me.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right
            Me.barDockControlRight.Location = New System.Drawing.Point(544, 24)
            Me.barDockControlRight.Size = New System.Drawing.Size(0, 438)
            '
            'Form_Review
            '
            Me.ClientSize = New System.Drawing.Size(544, 462)
            Me.Controls.Add(Me.LayoutControl1)
            Me.Controls.Add(Me.barDockControlLeft)
            Me.Controls.Add(Me.barDockControlRight)
            Me.Controls.Add(Me.barDockControlBottom)
            Me.Controls.Add(Me.barDockControlTop)
            Me.Name = "Form_Review"
            Me.Text = "List of Pending Events"
            CType(Me.DxErrorProvider1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControl1, System.ComponentModel.ISupportInitialize).EndInit()
            Me.LayoutControl1.ResumeLayout(False)
            CType(Me.LayoutControlGroup1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem2, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem8, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem9, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem10, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem11, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem3, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem6, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem7, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.EmptySpaceItem1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.EmptySpaceItem2, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem4, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem5, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.barManager1, System.ComponentModel.ISupportInitialize).EndInit()
            Me.ResumeLayout(False)
        End Sub

        Friend WithEvents DataNavigator1 As DevExpress.XtraEditors.DataNavigator
        Friend WithEvents LabelClient As DevExpress.XtraEditors.LabelControl
        Friend WithEvents LabelName As DevExpress.XtraEditors.LabelControl
        Friend WithEvents LabelCoApplicant As DevExpress.XtraEditors.LabelControl
        Friend WithEvents LabelStatus As DevExpress.XtraEditors.LabelControl
        Friend WithEvents LabelHomePhone As DevExpress.XtraEditors.LabelControl
        Friend WithEvents LabelWorkPhone As DevExpress.XtraEditors.LabelControl
        Friend WithEvents LabelMsgPhone As DevExpress.XtraEditors.LabelControl
        Friend WithEvents ButtonShow As DevExpress.XtraEditors.SimpleButton
        Friend WithEvents Button_Cancel As DevExpress.XtraEditors.SimpleButton
        Friend WithEvents RetentionEventList1 As RetentionEventList
        Friend WithEvents LayoutControl1 As DevExpress.XtraLayout.LayoutControl
        Friend WithEvents LayoutControlGroup1 As DevExpress.XtraLayout.LayoutControlGroup
        Friend WithEvents LayoutControlItem1 As DevExpress.XtraLayout.LayoutControlItem
        Friend WithEvents LayoutControlItem2 As DevExpress.XtraLayout.LayoutControlItem
        Friend WithEvents LayoutControlItem8 As DevExpress.XtraLayout.LayoutControlItem
        Friend WithEvents LayoutControlItem9 As DevExpress.XtraLayout.LayoutControlItem
        Friend WithEvents LayoutControlItem10 As DevExpress.XtraLayout.LayoutControlItem
        Friend WithEvents LayoutControlItem11 As DevExpress.XtraLayout.LayoutControlItem
        Friend WithEvents LayoutControlItem3 As DevExpress.XtraLayout.LayoutControlItem
        Friend WithEvents LayoutControlItem6 As DevExpress.XtraLayout.LayoutControlItem
        Friend WithEvents LayoutControlItem7 As DevExpress.XtraLayout.LayoutControlItem
        Friend WithEvents EmptySpaceItem1 As DevExpress.XtraLayout.EmptySpaceItem
        Friend WithEvents EmptySpaceItem2 As DevExpress.XtraLayout.EmptySpaceItem
        Friend WithEvents LayoutControlItem4 As DevExpress.XtraLayout.LayoutControlItem
        Friend WithEvents LayoutControlItem5 As DevExpress.XtraLayout.LayoutControlItem
        Friend WithEvents barManager1 As DevExpress.XtraBars.BarManager
        Friend WithEvents Bar2 As DevExpress.XtraBars.Bar
        Friend WithEvents MenuFile As DevExpress.XtraBars.BarSubItem
        Friend WithEvents MenuFileDeposits As DevExpress.XtraBars.BarSubItem
        Friend WithEvents MenuFileDepositsAny As DevExpress.XtraBars.BarCheckItem
        Friend WithEvents MenuFileDepositsFirst As DevExpress.XtraBars.BarCheckItem
        Friend WithEvents MenuFileDepositsSecond As DevExpress.XtraBars.BarCheckItem
        Friend WithEvents MenuFileAppointments As DevExpress.XtraBars.BarSubItem
        Friend WithEvents MenuFileAppointmentsAny As DevExpress.XtraBars.BarCheckItem
        Friend WithEvents MenuFileAppointmentsFirst As DevExpress.XtraBars.BarCheckItem
        Friend WithEvents MenuFileAppointmentsSecond As DevExpress.XtraBars.BarCheckItem
        Friend WithEvents MenuFileNew As DevExpress.XtraBars.BarCheckItem
        Friend WithEvents MenuFileRefresh As DevExpress.XtraBars.BarCheckItem
        Friend WithEvents MenuFilePrint As DevExpress.XtraBars.BarCheckItem
        Friend WithEvents MenuFileExit As DevExpress.XtraBars.BarCheckItem
        Friend WithEvents MenuEdit As DevExpress.XtraBars.BarSubItem
        Friend WithEvents MenuEditCut As DevExpress.XtraBars.BarCheckItem
        Friend WithEvents MenuEditCopy As DevExpress.XtraBars.BarCheckItem
        Friend WithEvents MenuEditPaste As DevExpress.XtraBars.BarCheckItem
        Friend WithEvents barDockControlTop As DevExpress.XtraBars.BarDockControl
        Friend WithEvents barDockControlBottom As DevExpress.XtraBars.BarDockControl
        Friend WithEvents barDockControlLeft As DevExpress.XtraBars.BarDockControl
        Friend WithEvents barDockControlRight As DevExpress.XtraBars.BarDockControl

#End Region

        Private Sub ButtonCancel_Click(ByVal sender As Object, ByVal e As EventArgs)
            Close()
        End Sub

        Private Sub MenuFile_Popup(ByVal sender As Object, ByVal e As EventArgs)
            Dim tbl As DataTable = ds.Tables("selected_clients")
            MenuFilePrint.Enabled = tbl IsNot Nothing AndAlso tbl.Rows.Count > 0
        End Sub

        Private Sub MenuFileAppointmentsAny_Click(ByVal sender As Object, ByVal e As EventArgs)
            Dim CounselorString As String = CounselorAndOffice()
            If CounselorString <> "CANCEL" Then

                ' Clear the current cheched status items for the file menus
                'MenuFileDepositsFirst.Checked = False
                'MenuFileDepositsSecond.Checked = False
                'MenuFileDepositsAny.Checked = False
                'MenuFileAppointmentsFirst.Checked = False
                'MenuFileAppointmentsSecond.Checked = False
                'MenuFileAppointmentsAny.Checked = False
                'MenuFileNew.Checked = False

                'MenuFileAppointmentsAny.Checked = True
                MenuSelections("vc.retention_event in (3,4) " + CounselorString, False, False)
            End If
        End Sub

        Private Sub MenuFileAppointmentsFirst_Click(ByVal sender As Object, ByVal e As EventArgs)
            Dim CounselorString As String = CounselorAndOffice()
            If CounselorString <> "CANCEL" Then

                ' Clear the current cheched status items for the file menus
                'MenuFileDepositsFirst.Checked = False
                'MenuFileDepositsSecond.Checked = False
                'MenuFileDepositsAny.Checked = False
                'MenuFileAppointmentsFirst.Checked = False
                'MenuFileAppointmentsSecond.Checked = False
                'MenuFileAppointmentsAny.Checked = False
                'MenuFileNew.Checked = False

                'MenuFileAppointmentsFirst.Checked = True
                MenuSelections("vc.retention_event = 3 " + CounselorString, False, False)
            End If
        End Sub

        Private Sub MenuFileAppointmentsSecond_Click(ByVal sender As Object, ByVal e As EventArgs)
            Dim CounselorString As String = CounselorAndOffice()
            If CounselorString <> "CANCEL" Then

                ' Clear the current cheched status items for the file menus
                'MenuFileDepositsFirst.Checked = False
                'MenuFileDepositsSecond.Checked = False
                'MenuFileDepositsAny.Checked = False
                'MenuFileAppointmentsFirst.Checked = False
                'MenuFileAppointmentsSecond.Checked = False
                'MenuFileAppointmentsAny.Checked = False
                'MenuFileNew.Checked = False

                'MenuFileAppointmentsSecond.Checked = True
                MenuSelections("vc.retention_event = 4 " + CounselorString, False, False)
            End If
        End Sub

        Private Sub MenuFileDepositsAny_Click(ByVal sender As Object, ByVal e As EventArgs)
            Dim CounselorString As String = CounselorAndOffice()
            If CounselorString <> "CANCEL" Then

                ' Clear the current cheched status items for the file menus
                'MenuFileDepositsFirst.Checked = False
                'MenuFileDepositsSecond.Checked = False
                'MenuFileDepositsAny.Checked = False
                'MenuFileAppointmentsFirst.Checked = False
                'MenuFileAppointmentsSecond.Checked = False
                'MenuFileAppointmentsAny.Checked = False
                'MenuFileNew.Checked = False

                'MenuFileDepositsAny.Checked = True
                MenuSelections("vc.retention_event IN (1,2) " + CounselorString, False, False)
            End If
        End Sub

        Private Sub MenuFileDepositsFirst_Click(ByVal sender As Object, ByVal e As EventArgs)
            Dim CounselorString As String = CounselorAndOffice()
            If CounselorString <> "CANCEL" Then

                ' Clear the current cheched status items for the file menus
                'MenuFileDepositsFirst.Checked = False
                'MenuFileDepositsSecond.Checked = False
                'MenuFileDepositsAny.Checked = False
                'MenuFileAppointmentsFirst.Checked = False
                'MenuFileAppointmentsSecond.Checked = False
                'MenuFileAppointmentsAny.Checked = False
                'MenuFileNew.Checked = False

                'MenuFileDepositsFirst.Checked = True
                MenuSelections("vc.retention_event = 1 " + CounselorString, False, False)
            End If
        End Sub

        Private Sub MenuFileDepositsSecond_Click(ByVal sender As Object, ByVal e As EventArgs)
            Dim CounselorString As String = CounselorAndOffice()
            If CounselorString <> "CANCEL" Then

                ' Clear the current cheched status items for the file menus
                'MenuFileDepositsFirst.Checked = False
                'MenuFileDepositsSecond.Checked = False
                'MenuFileDepositsAny.Checked = False
                'MenuFileAppointmentsFirst.Checked = False
                'MenuFileAppointmentsSecond.Checked = False
                'MenuFileAppointmentsAny.Checked = False
                'MenuFileNew.Checked = False

                'MenuFileDepositsSecond.Checked = True
                MenuSelections("vc.retention_event = 2 " + CounselorString, False, False)
            End If
        End Sub

        Private Sub MenuFileNew_Click(ByVal sender As Object, ByVal e As EventArgs)
            Dim NewField As New FieldSelection
            Dim answer As DialogResult
            Dim clause As SelectionClause = Nothing

            ' Ask for a new event id
            Using frm As New FieldForm(IsSearch, NewField)
                frm.Location = New Point(Left + (Width - frm.Width) \ 2, Top + (Height - frm.Height) \ 2)
                answer = frm.ShowDialog()
            End Using

            ' Pass this event id to the relation form to build the selection criteria
            If answer = DialogResult.OK Then
                Using frm_relation As New RelationForm(IsSearch, NewField)
                    With frm_relation
                        .Text = "Find Retention Events"
                        .Location = New Point(Left + (Width - .Width) \ 2, Top + (Height - .Height) \ 2)
                        answer = .ShowDialog()
                        If answer = DialogResult.OK Then
                            clause = .SelectionClause
                        End If
                    End With
                End Using
            End If

            ' If we have a selection clause then request the information from the system
            If clause IsNot Nothing AndAlso clause.Valid Then
                MenuSelections(clause.SelectionString, clause.UseClientDepositTable, clause.UseClientRetentionEventsTable)

                ' Clear the current cheched status items for the file menus
                'MenuFileDepositsFirst.Checked = False
                'MenuFileDepositsSecond.Checked = False
                'MenuFileDepositsAny.Checked = False
                'MenuFileAppointmentsFirst.Checked = False
                'MenuFileAppointmentsSecond.Checked = False
                'MenuFileAppointmentsAny.Checked = False
                'MenuFileNew.Checked = False

                'MenuFileNew.Checked = True
            End If
        End Sub

        Private Sub MenuFileRefresh_Click(ByVal sender As Object, ByVal e As EventArgs)
            MenuSelections()
        End Sub

        Private WithEvents ClientsTable As DataTable
        Private CurrentSelectionClause As String = String.Empty
        Private CurrentUseClientDepositTable As Boolean
        Private CurrentUseClientRetentionEventsTable As Boolean

        Private Sub MenuSelections(ByVal SelectionClause As String, ByVal UseClientDepositTable As Boolean, ByVal UseClientRetentionEventsTable As Boolean)
            CurrentSelectionClause = SelectionClause
            CurrentUseClientDepositTable = UseClientDepositTable
            CurrentUseClientRetentionEventsTable = UseClientRetentionEventsTable
            MenuSelections()
        End Sub

        Private Sub MenuSelections()
            Dim sb As New StringBuilder
            sb.Append("SELECT DISTINCT vc.client FROM view_retention_client_events vc WITH (NOLOCK)")
            sb.Append(" INNER JOIN clients c ON vc.client = c.client")

            If CurrentUseClientDepositTable Then
                sb.Append(" LEFT OUTER JOIN client_deposits cd WITH (NOLOCK) ON vc.client = cd.client")
            End If

            If CurrentUseClientRetentionEventsTable Then
                ' The view contains all that we need now so we don't need this flag now.
            End If

            sb.Append(" WHERE (vc.date_expired IS NULL)")

            If CurrentSelectionClause <> String.Empty Then
                sb.AppendFormat(" AND ({0})", CurrentSelectionClause)
            End If

            sb.Append(" ORDER BY 1")

            MenuSelections(sb.ToString())
        End Sub

        Private Sub MenuSelections(ByVal SelectStatement As String)

            ' Generate a read request to retrieve the client information
            Dim current_cursor As Cursor = Cursor.Current
            Try
                Cursor.Current = Cursors.WaitCursor
                If SelectStatement <> String.Empty Then
                    ClientsTable = ds.Tables("selected_clients")
                    If ClientsTable IsNot Nothing Then
                        ClientsTable.Clear()
                    End If

                    Using cmd As SqlCommand = New SqlCommand
                        With cmd
                            .Connection = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
                            .CommandText = SelectStatement
                            .CommandType = CommandType.Text
                            .CommandTimeout = System.Math.Max(300, DebtPlus.LINQ.SQLInfoClass.getDefault().CommandTimeout)
                        End With

                        Using da As New SqlDataAdapter(cmd)
                            da.Fill(ds, "selected_clients")
                        End Using
                    End Using

                    ClientsTable = ds.Tables("selected_clients")

                    ' Attach the navigator to the dataset
                    DataNavigator1.DataSource = New DataView(ClientsTable, String.Empty, "client", DataViewRowState.CurrentRows)
                    ButtonShow.Enabled = ClientsTable.Rows.Count > 0
                    MenuFileRefresh.Enabled = True
                End If

                DisplayClientInfo()
                RetentionEventList1.Reload(client)

            Catch ex As SqlException
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error reading client list")

            Finally
                Cursor.Current = current_cursor
            End Try
        End Sub

        Private Shared Function CounselorAndOffice() As String
            Dim answer As String = String.Empty

            Using frm As New CounselorOfficeSelectionForm
                If frm.ShowDialog() = DialogResult.OK Then
                    answer = frm.SelectionClause
                Else
                    answer = "CANCEL"
                End If
            End Using

            Return answer
        End Function

        Private Sub DataNavigator1_PositionChanged(ByVal sender As Object, ByVal e As EventArgs)
            RetentionEventList1.Reload(client)
            DisplayClientInfo()
        End Sub

        Private ReadOnly Property client() As Int32
            Get
                ' Find the client that is current selected by the navigator
                Dim CurrentClient As Int32 = -1
                With DataNavigator1
                    Dim vue As DataView = CType(DataNavigator1.DataSource, DataView)
                    If .Position >= 0 Then CurrentClient = Convert.ToInt32(vue(.Position)("client"))
                End With

                Return CurrentClient
            End Get
        End Property

        Private Sub DisplayClientInfo()

            ' Clear the client information
            LabelClient.Text = String.Empty
            LabelCoApplicant.Text = String.Empty
            LabelHomePhone.Text = String.Empty
            LabelMsgPhone.Text = String.Empty
            LabelName.Text = String.Empty
            LabelStatus.Text = String.Empty
            LabelWorkPhone.Text = String.Empty

            ' If there is a client then format the information for the display
            If client >= 0 Then
                LabelClient.Text = String.Format("{0:0000000}", client)

                Dim cn As SqlConnection = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
                Dim rd As SqlDataReader = Nothing
                Dim current_cursor As Cursor = Cursor.Current
                Try
                    Cursor.Current = Cursors.WaitCursor
                    cn.Open()
                    Using cmd As SqlCommand = New SqlCommand
                        With cmd
                            .Connection = cn
                            .CommandText = "SELECT name, coapplicant, active_status, phone, work_ph, work_ext, message_ph FROM view_client_address with (nolock) WHERE client=@client"
                            .Parameters.Add("@client", SqlDbType.Int).Value = client
                            rd = .ExecuteReader(CommandBehavior.CloseConnection Or CommandBehavior.SingleRow)
                        End With
                    End Using

                    If rd.Read Then
                        Dim work_ph As String, work_ext As String

                        If Not rd.IsDBNull(0) Then LabelName.Text = rd.GetString(0).Trim()
                        If Not rd.IsDBNull(1) Then LabelCoApplicant.Text = rd.GetString(1).Trim()
                        If Not rd.IsDBNull(2) Then LabelStatus.Text = rd.GetString(2).Trim()
                        If Not rd.IsDBNull(3) Then LabelHomePhone.Text = rd.GetString(3).Trim()

                        If Not rd.IsDBNull(4) Then
                            work_ph = rd.GetString(4).Trim()
                        Else
                            work_ph = String.Empty
                        End If

                        If Not rd.IsDBNull(5) Then
                            work_ext = rd.GetString(5).Trim()
                        Else
                            work_ext = String.Empty
                        End If

                        If work_ph <> String.Empty AndAlso work_ext <> String.Empty Then
                            LabelWorkPhone.Text = String.Format("{0} Ext. {1}", work_ph, work_ext)
                        Else
                            LabelWorkPhone.Text = work_ph + work_ext
                        End If

                        If Not rd.IsDBNull(6) Then LabelMsgPhone.Text = rd.GetString(6).Trim()
                    End If

                Catch ex As Exception
                    DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error reading client information")

                Finally
                    If rd IsNot Nothing Then rd.Dispose()
                    If cn IsNot Nothing Then cn.Dispose()

                    Cursor.Current = current_cursor
                End Try
            End If
        End Sub

        Private Sub Form_Closing(ByVal sender As Object, ByVal e As CancelEventArgs)
            RetentionEventList1.SaveChanges()
        End Sub

        Private Sub Form_Load(ByVal sender As Object, ByVal e As EventArgs)
            MenuSelections()
            ButtonShow.Enabled = False
            MenuFileRefresh.Enabled = False
        End Sub

        Private Sub ButtonShow_Click(ByVal sender As Object, ByVal e As EventArgs)
            If client >= 0 Then
                With New ClientThread(client)
                    With New Thread(AddressOf .EditClient)
                        .IsBackground = True
                        .SetApartmentState(ApartmentState.STA)
                        .Name = Guid.NewGuid().ToString()
                        .Start()
                    End With
                End With
            End If
        End Sub

        Private Class ClientThread
            Private ReadOnly Client As Int32

            Public Sub New(ByVal Client As Int32)
                Me.Client = Client
            End Sub

            Public Sub EditClient()
                With New Notes.AlertNotes.Client()
                    .ShowAlerts(Client)
                End With

                Using cuc = New ClientUpdateClass()
                    cuc.ShowEditDialog(Client, False)
                End Using
            End Sub
        End Class

        Private Sub MenuFilePrint_Click(ByVal sender As Object, ByVal e As EventArgs)

            ' Construct the selection statement for the report.
            Dim sb As New StringBuilder
            sb.Append("SELECT vc.client_retention_event, vc.client, vc.client_counselor, vc.client_office, vc.priority, vc.retention_event, vc.event_amount, vc.event_message, vc.date_created, vc.created_by, vc.name, vc.co_name, vc.active_status, vc.home_ph, vc.work_ph, vc.message_ph, vc.event_description, vc.action_date, vc.date_expired, vc.expire_type, vc.expire_date, vc.effective_date FROM view_retention_client_events vc WITH (NOLOCK)")
            sb.Append(" INNER JOIN clients c ON vc.client = c.client")

            If CurrentUseClientDepositTable Then
                sb.Append(" LEFT OUTER JOIN client_deposits cd WITH (NOLOCK) ON vc.client = cd.client")
            End If

            If CurrentUseClientRetentionEventsTable Then
                ' We no longer need the raw table. Everything is in the view. So, just don't bother here.
            End If

            sb.Append(" WHERE (vc.date_expired IS NULL)")

            If CurrentSelectionClause <> String.Empty Then
                sb.AppendFormat(" AND ({0})", CurrentSelectionClause)
            End If

            Dim rpt As New DebtPlus.Reports.Retention.List.Clients.ListClientsReport()
            rpt.Parameter_SelectionCriteria = sb.ToString()
            rpt.RunReportInSeparateThread()
        End Sub
    End Class
End Namespace
