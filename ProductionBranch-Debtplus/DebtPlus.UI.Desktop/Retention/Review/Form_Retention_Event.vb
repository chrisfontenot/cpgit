#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}
#End Region

Option Compare Binary
Option Explicit On 
Option Strict On

Imports DebtPlus.UI.Desktop.Retention.Common

Namespace Retention.Review
    Friend Class Form_Retention_Event
        Inherits DebtPlus.Data.Forms.DebtPlusForm

        Protected Friend drv As System.Data.DataRowView = Nothing
        Public Sub New(ByVal drv As System.Data.DataRowView)
            MyClass.New()
            Me.drv = drv

            AddHandler Me.Load, AddressOf Form_Retention_Event_Load
            AddHandler MyBase.Closing, AddressOf Form_Retention_Event_Closing
            AddHandler expire_date.EditValueChanged, AddressOf expire_date_EditValueChanged
            AddHandler retention_event.EditValueChanged, AddressOf retention_event_EditValueChanged
        End Sub

#Region " Windows Form Designer generated code "

        Public Sub New()
            MyBase.New()

            'This call is required by the Windows Form Designer.
            InitializeComponent()

            'Add any initialization after the InitializeComponent() call

        End Sub

        'Form overrides dispose to clean up the component list.
        Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
            If disposing Then
                If components IsNot Nothing Then
                    components.Dispose()
                End If
            End If
            MyBase.Dispose(disposing)
        End Sub

        'Required by the Windows Form Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Windows Form Designer
        'It can be modified using the Windows Form Designer.  
        'Do not modify it using the code editor.
        Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents LabelControl2 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents LabelControl3 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents LabelControl4 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents LabelControl5 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents retention_event As DevExpress.XtraEditors.LookUpEdit
        Friend WithEvents priority As DevExpress.XtraEditors.SpinEdit
        Friend WithEvents Button_OK As DevExpress.XtraEditors.SimpleButton
        Friend WithEvents Button_Cancel As DevExpress.XtraEditors.SimpleButton
        Friend WithEvents RetentionActionList1 As RetentionActionList
        Friend WithEvents amount As DevExpress.XtraEditors.CalcEdit
        Friend WithEvents message As DevExpress.XtraEditors.MemoEdit
        Friend WithEvents expire_type As DevExpress.XtraEditors.GroupControl
        Friend WithEvents expire_date As DevExpress.XtraEditors.DateEdit
        Friend WithEvents expire_type_3 As DevExpress.XtraEditors.CheckEdit
        Friend WithEvents expire_type_2 As DevExpress.XtraEditors.CheckEdit
        Friend WithEvents expire_type_1 As DevExpress.XtraEditors.CheckEdit
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
            Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl()
            Me.LabelControl2 = New DevExpress.XtraEditors.LabelControl()
            Me.LabelControl3 = New DevExpress.XtraEditors.LabelControl()
            Me.LabelControl4 = New DevExpress.XtraEditors.LabelControl()
            Me.LabelControl5 = New DevExpress.XtraEditors.LabelControl()
            Me.retention_event = New DevExpress.XtraEditors.LookUpEdit()
            Me.amount = New DevExpress.XtraEditors.CalcEdit()
            Me.message = New DevExpress.XtraEditors.MemoEdit()
            Me.priority = New DevExpress.XtraEditors.SpinEdit()
            Me.expire_type = New DevExpress.XtraEditors.GroupControl()
            Me.expire_date = New DevExpress.XtraEditors.DateEdit()
            Me.expire_type_3 = New DevExpress.XtraEditors.CheckEdit()
            Me.expire_type_2 = New DevExpress.XtraEditors.CheckEdit()
            Me.expire_type_1 = New DevExpress.XtraEditors.CheckEdit()
            Me.Button_OK = New DevExpress.XtraEditors.SimpleButton()
            Me.Button_Cancel = New DevExpress.XtraEditors.SimpleButton()
            Me.RetentionActionList1 = New RetentionActionList()
            CType(Me.DxErrorProvider1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.retention_event.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.amount.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.message.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.priority.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.expire_type, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.expire_type.SuspendLayout()
            CType(Me.expire_date.Properties.VistaTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.expire_date.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.expire_type_3.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.expire_type_2.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.expire_type_1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.SuspendLayout()
            '
            'LabelControl1
            '
            Me.LabelControl1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.LabelControl1.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
            Me.LabelControl1.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center
            Me.LabelControl1.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap
            Me.LabelControl1.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
            Me.LabelControl1.Location = New System.Drawing.Point(8, 0)
            Me.LabelControl1.Name = "LabelControl1"
            Me.LabelControl1.Size = New System.Drawing.Size(490, 42)
            Me.LabelControl1.TabIndex = 0
            Me.LabelControl1.Text = "This form shows the client retention event. Below are the actions which have been" & _
        " posted against this event."
            Me.LabelControl1.UseMnemonic = False
            '
            'LabelControl2
            '
            Me.LabelControl2.Location = New System.Drawing.Point(24, 40)
            Me.LabelControl2.Name = "LabelControl2"
            Me.LabelControl2.Size = New System.Drawing.Size(28, 13)
            Me.LabelControl2.TabIndex = 1
            Me.LabelControl2.Text = "&Event"
            '
            'LabelControl3
            '
            Me.LabelControl3.Location = New System.Drawing.Point(24, 64)
            Me.LabelControl3.Name = "LabelControl3"
            Me.LabelControl3.Size = New System.Drawing.Size(37, 13)
            Me.LabelControl3.TabIndex = 3
            Me.LabelControl3.Text = "&Amount"
            '
            'LabelControl4
            '
            Me.LabelControl4.Location = New System.Drawing.Point(24, 91)
            Me.LabelControl4.Name = "LabelControl4"
            Me.LabelControl4.Size = New System.Drawing.Size(23, 13)
            Me.LabelControl4.TabIndex = 7
            Me.LabelControl4.Text = "&Note"
            '
            'LabelControl5
            '
            Me.LabelControl5.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.LabelControl5.Location = New System.Drawing.Point(279, 68)
            Me.LabelControl5.Name = "LabelControl5"
            Me.LabelControl5.Size = New System.Drawing.Size(34, 13)
            Me.LabelControl5.TabIndex = 5
            Me.LabelControl5.Text = "Priority"
            '
            'retention_event
            '
            Me.retention_event.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.retention_event.Location = New System.Drawing.Point(104, 40)
            Me.retention_event.Name = "retention_event"
            Me.retention_event.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.retention_event.Properties.Columns.AddRange(New DevExpress.XtraEditors.Controls.LookUpColumnInfo() {New DevExpress.XtraEditors.Controls.LookUpColumnInfo("item_key", "ID", 20, DevExpress.Utils.FormatType.Numeric, "f0", False, DevExpress.Utils.HorzAlignment.[Default]), New DevExpress.XtraEditors.Controls.LookUpColumnInfo("description", "Description", 20, DevExpress.Utils.FormatType.None, "", True, DevExpress.Utils.HorzAlignment.[Default], DevExpress.Data.ColumnSortOrder.Ascending)})
            Me.retention_event.Properties.NullText = "...Please Choose One..."
            Me.retention_event.Properties.ShowFooter = False
            Me.retention_event.Properties.ShowHeader = False
            Me.retention_event.Properties.ShowLines = False
            Me.retention_event.Size = New System.Drawing.Size(255, 20)
            Me.retention_event.TabIndex = 2
            Me.retention_event.Properties.SortColumnIndex = 1
            '
            'amount
            '
            Me.amount.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.amount.Location = New System.Drawing.Point(104, 64)
            Me.amount.Name = "amount"
            Me.amount.Properties.Appearance.Options.UseTextOptions = True
            Me.amount.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.amount.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.amount.Properties.DisplayFormat.FormatString = "c"
            Me.amount.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.amount.Properties.EditFormat.FormatString = "c"
            Me.amount.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.amount.Properties.Mask.BeepOnError = True
            Me.amount.Properties.Mask.EditMask = "c"
            Me.amount.Size = New System.Drawing.Size(99, 20)
            Me.amount.TabIndex = 4
            '
            'message
            '
            Me.message.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.message.Location = New System.Drawing.Point(104, 88)
            Me.message.Name = "message"
            Me.message.Properties.MaxLength = 256
            Me.message.Properties.ScrollBars = System.Windows.Forms.ScrollBars.None
            Me.message.Size = New System.Drawing.Size(255, 40)
            Me.message.TabIndex = 8
            '
            'priority
            '
            Me.priority.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.priority.EditValue = New Decimal(New Int32() {9, 0, 0, 0})
            Me.priority.Location = New System.Drawing.Point(327, 64)
            Me.priority.Name = "priority"
            Me.priority.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
            Me.priority.Properties.IsFloatValue = False
            Me.priority.Properties.Mask.EditMask = "N00"
            Me.priority.Properties.MaxLength = 1
            Me.priority.Properties.MaxValue = New Decimal(New Int32() {9, 0, 0, 0})
            Me.priority.Size = New System.Drawing.Size(32, 20)
            Me.priority.TabIndex = 6
            '
            'expire_type
            '
            Me.expire_type.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.expire_type.Controls.Add(Me.expire_date)
            Me.expire_type.Controls.Add(Me.expire_type_3)
            Me.expire_type.Controls.Add(Me.expire_type_2)
            Me.expire_type.Controls.Add(Me.expire_type_1)
            Me.expire_type.Location = New System.Drawing.Point(8, 128)
            Me.expire_type.Name = "expire_type"
            Me.expire_type.Size = New System.Drawing.Size(495, 80)
            Me.expire_type.TabIndex = 9
            Me.expire_type.Text = "Expires"
            '
            'expire_date
            '
            Me.expire_date.EditValue = Nothing
            Me.expire_date.Location = New System.Drawing.Point(160, 53)
            Me.expire_date.Name = "expire_date"
            Me.expire_date.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
            Me.expire_date.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.expire_date.Properties.VistaTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
            Me.expire_date.Size = New System.Drawing.Size(88, 20)
            Me.expire_date.TabIndex = 3
            '
            'expire_type_3
            '
            Me.expire_type_3.Location = New System.Drawing.Point(8, 54)
            Me.expire_type_3.Name = "expire_type_3"
            Me.expire_type_3.Properties.Caption = "Expires automatically on"
            Me.expire_type_3.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
            Me.expire_type_3.Properties.RadioGroupIndex = 1
            Me.expire_type_3.Size = New System.Drawing.Size(144, 19)
            Me.expire_type_3.TabIndex = 2
            Me.expire_type_3.TabStop = False
            '
            'expire_type_2
            '
            Me.expire_type_2.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.expire_type_2.Location = New System.Drawing.Point(8, 35)
            Me.expire_type_2.Name = "expire_type_2"
            Me.expire_type_2.Properties.Caption = "Expires on next posted deposit of at least the dollar amount indicated above"
            Me.expire_type_2.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
            Me.expire_type_2.Properties.RadioGroupIndex = 1
            Me.expire_type_2.Size = New System.Drawing.Size(463, 19)
            Me.expire_type_2.TabIndex = 1
            Me.expire_type_2.TabStop = False
            '
            'expire_type_1
            '
            Me.expire_type_1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.expire_type_1.EditValue = True
            Me.expire_type_1.Location = New System.Drawing.Point(8, 16)
            Me.expire_type_1.Name = "expire_type_1"
            Me.expire_type_1.Properties.Caption = "Never expires. Must be manually cancelled."
            Me.expire_type_1.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
            Me.expire_type_1.Properties.RadioGroupIndex = 1
            Me.expire_type_1.Size = New System.Drawing.Size(463, 19)
            Me.expire_type_1.TabIndex = 0
            '
            'Button_OK
            '
            Me.Button_OK.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.Button_OK.DialogResult = System.Windows.Forms.DialogResult.OK
            Me.Button_OK.Enabled = False
            Me.Button_OK.Location = New System.Drawing.Point(423, 48)
            Me.Button_OK.Name = "Button_OK"
            Me.Button_OK.Size = New System.Drawing.Size(75, 23)
            Me.Button_OK.TabIndex = 11
            Me.Button_OK.Text = "&OK"
            '
            'Button_Cancel
            '
            Me.Button_Cancel.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.Button_Cancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
            Me.Button_Cancel.Location = New System.Drawing.Point(423, 80)
            Me.Button_Cancel.Name = "Button_Cancel"
            Me.Button_Cancel.Size = New System.Drawing.Size(75, 23)
            Me.Button_Cancel.TabIndex = 12
            Me.Button_Cancel.Text = "&Cancel"
            '
            'RetentionActionList1
            '
            Me.RetentionActionList1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.RetentionActionList1.Location = New System.Drawing.Point(8, 216)
            Me.RetentionActionList1.Name = "RetentionActionList1"
            Me.RetentionActionList1.Size = New System.Drawing.Size(495, 144)
            Me.RetentionActionList1.TabIndex = 10
            '
            'Form_Retention_Event
            '
            Me.AcceptButton = Me.Button_OK
            Me.CancelButton = Me.Button_Cancel
            Me.ClientSize = New System.Drawing.Size(511, 366)
            Me.Controls.Add(Me.RetentionActionList1)
            Me.Controls.Add(Me.Button_Cancel)
            Me.Controls.Add(Me.Button_OK)
            Me.Controls.Add(Me.expire_type)
            Me.Controls.Add(Me.priority)
            Me.Controls.Add(Me.message)
            Me.Controls.Add(Me.amount)
            Me.Controls.Add(Me.retention_event)
            Me.Controls.Add(Me.LabelControl5)
            Me.Controls.Add(Me.LabelControl4)
            Me.Controls.Add(Me.LabelControl3)
            Me.Controls.Add(Me.LabelControl2)
            Me.Controls.Add(Me.LabelControl1)
            Me.Name = "Form_Retention_Event"
            Me.Text = "Retention Event"
            CType(Me.DxErrorProvider1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.retention_event.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.amount.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.message.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.priority.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.expire_type, System.ComponentModel.ISupportInitialize).EndInit()
            Me.expire_type.ResumeLayout(False)
            CType(Me.expire_date.Properties.VistaTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.expire_date.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.expire_type_3.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.expire_type_2.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.expire_type_1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            Me.ResumeLayout(False)
            Me.PerformLayout()

        End Sub

#End Region

        Private ReadOnly Property client_retention_event() As System.Int32
            Get
                Dim answer As System.Int32 = -1
                If drv IsNot Nothing Then
                    If drv("client_retention_event") IsNot Nothing AndAlso drv("client_retention_event") IsNot System.DBNull.Value Then answer = Convert.ToInt32(drv("client_retention_event"))
                End If
                Return answer
            End Get
        End Property

        Private Sub Form_Retention_Event_Load(ByVal sender As System.Object, ByVal e As System.EventArgs)

            ' Define the list of retention events
            Dim tbl As System.Data.DataTable = RetentionEventsTable()
            With retention_event.Properties
                .DataSource = tbl.DefaultView
                .DisplayMember = "description"
                .ValueMember = "item_key"
            End With

            ' Bind the fields to the items on the form
            With retention_event
                .DataBindings.Clear()
                .DataBindings.Add("EditValue", drv, "retention_event")
            End With

            With amount
                .DataBindings.Clear()
                .DataBindings.Add("EditValue", drv, "event_amount")
            End With

            With priority
                .DataBindings.Clear()
                .DataBindings.Add("EditValue", drv, "priority")
            End With

            With message
                .DataBindings.Clear()
                .DataBindings.Add("EditValue", drv, "event_message")
            End With

            With expire_date
                .DataBindings.Clear()
                .DataBindings.Add("EditValue", drv, "expire_date")
            End With

            AddHandler expire_type_1.CheckedChanged, AddressOf expire_type_CheckChanged
            AddHandler expire_type_2.CheckedChanged, AddressOf expire_type_CheckChanged
            AddHandler expire_type_3.CheckedChanged, AddressOf expire_type_CheckChanged

            ' Based upon the expiration type, set the appropriate checkbox
            Select Case Convert.ToInt32(drv("expire_type"))
                Case 1
                    expire_type_1.Checked = True
                Case 2
                    expire_type_2.Checked = True
                Case 3
                    expire_type_3.Checked = True
            End Select

            ' Display the grid information
            With RetentionActionList1
                .Reload(client_retention_event)
            End With

            Button_OK.Enabled = Not HasErrors()
        End Sub

        Private Sub expire_type_CheckChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)

            If expire_type_1.Checked Then
                drv("expire_type") = 1
                expire_date.Enabled = False

            ElseIf expire_type_2.Checked Then
                drv("expire_type") = 2
                expire_date.Enabled = False

            ElseIf expire_type_3.Checked Then
                drv("expire_type") = 3
                expire_date.Enabled = True
            End If

            Button_OK.Enabled = Not HasErrors()
        End Sub

        Private Sub Form_Retention_Event_Closing(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs)
            RetentionActionList1.SaveChanges()
        End Sub

        Private Sub expire_date_EditValueChanged(ByVal sender As Object, ByVal e As System.EventArgs)
            Button_OK.Enabled = Not HasErrors()
        End Sub

        Private Sub retention_event_EditValueChanged(ByVal sender As Object, ByVal e As System.EventArgs)
            Dim Enabled As Boolean = Not HasErrors()
            Button_OK.Enabled = Enabled

            If Not Enabled Then
                drv("event_description") = String.Empty
            Else
                Dim event_row As System.Data.DataRowView = CType(retention_event.Properties.GetDataSourceRowByKeyValue(retention_event.EditValue), System.Data.DataRowView)
                drv("event_description") = event_row("description")
            End If
        End Sub

        Private Function HasErrors() As Boolean
            Dim answer As Boolean = False

            If retention_event.EditValue Is Nothing OrElse retention_event.EditValue Is System.DBNull.Value Then
                answer = True
            Else
                If expire_type_3.Checked Then
                    If expire_date.EditValue Is Nothing OrElse expire_date.EditValue Is System.DBNull.Value Then
                        answer = True
                    End If

                    If Not answer Then
                        If expire_date.DateTime.Date <= DateTime.Now.Date Then
                            answer = True
                        End If
                    End If
                End If
            End If

            Return answer
        End Function
    End Class
End Namespace