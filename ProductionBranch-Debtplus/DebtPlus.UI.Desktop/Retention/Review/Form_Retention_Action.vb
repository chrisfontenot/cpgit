#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}
#End Region

Option Compare Binary
Option Explicit On 
Option Strict On

Imports DebtPlus.UI.Desktop.Retention.Common

Namespace Retention.Review

    Friend Class Form_Retention_Action
        Inherits DebtPlus.Data.Forms.DebtPlusForm

        Public Sub New()
            MyBase.New()
            InitializeComponent()
            AddHandler Me.Load, AddressOf Form_Retention_Action_Load
            AddHandler retention_action.EditValueChanged, AddressOf retention_action_EditValueChanged
        End Sub

        Private drv As System.Data.DataRowView = Nothing
        Public Sub New(ByVal drv As System.Data.DataRowView)
            MyClass.New()
            Me.drv = drv
        End Sub

        Private ReadOnly Property client_retention_action() As System.Int32
            Get
                Dim answer As System.Int32 = -1
                If drv IsNot Nothing Then
                    If drv("client_retention_action") IsNot Nothing AndAlso drv("client_retention_event") IsNot System.DBNull.Value Then answer = Convert.ToInt32(drv("client_retention_action"))
                End If
                Return answer
            End Get
        End Property

#Region " Windows Form Designer generated code "

        'Form overrides dispose to clean up the component list.
        Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
            If disposing Then
                If components IsNot Nothing Then
                    components.Dispose()
                End If
            End If
            MyBase.Dispose(disposing)
        End Sub

        'Required by the Windows Form Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Windows Form Designer
        'It can be modified using the Windows Form Designer.  
        'Do not modify it using the code editor.
        Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents LabelControl2 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents LabelControl3 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents Button_OK As DevExpress.XtraEditors.SimpleButton
        Friend WithEvents Button_Cance As DevExpress.XtraEditors.SimpleButton
        Friend WithEvents retention_action As DevExpress.XtraEditors.LookUpEdit
        Friend WithEvents retention_message As DevExpress.XtraEditors.MemoEdit
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
            Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl()
            Me.LabelControl2 = New DevExpress.XtraEditors.LabelControl()
            Me.LabelControl3 = New DevExpress.XtraEditors.LabelControl()
            Me.retention_action = New DevExpress.XtraEditors.LookUpEdit()
            Me.retention_message = New DevExpress.XtraEditors.MemoEdit()
            Me.Button_OK = New DevExpress.XtraEditors.SimpleButton()
            Me.Button_Cance = New DevExpress.XtraEditors.SimpleButton()
            CType(Me.DxErrorProvider1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.retention_action.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.retention_message.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.SuspendLayout()
            '
            'LabelControl1
            '
            Me.LabelControl1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.LabelControl1.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
            Me.LabelControl1.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center
            Me.LabelControl1.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap
            Me.LabelControl1.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
            Me.LabelControl1.Location = New System.Drawing.Point(8, 3)
            Me.LabelControl1.Name = "LabelControl1"
            Me.LabelControl1.Size = New System.Drawing.Size(447, 39)
            Me.LabelControl1.TabIndex = 0
            Me.LabelControl1.Text = "This form shows the action for the indicated event. Choose the action that you wi" & _
        "sh to post against the current event. You may enter a textual note if you wish t" & _
        "o explain the action."
            Me.LabelControl1.ToolTipController = Me.ToolTipController1
            Me.LabelControl1.UseMnemonic = False
            '
            'LabelControl2
            '
            Me.LabelControl2.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.Horizontal
            Me.LabelControl2.Location = New System.Drawing.Point(8, 48)
            Me.LabelControl2.Name = "LabelControl2"
            Me.LabelControl2.Size = New System.Drawing.Size(30, 13)
            Me.LabelControl2.TabIndex = 1
            Me.LabelControl2.Text = "&Action"
            Me.LabelControl2.ToolTipController = Me.ToolTipController1
            '
            'LabelControl3
            '
            Me.LabelControl3.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.Horizontal
            Me.LabelControl3.Location = New System.Drawing.Point(8, 88)
            Me.LabelControl3.Name = "LabelControl3"
            Me.LabelControl3.Size = New System.Drawing.Size(23, 13)
            Me.LabelControl3.TabIndex = 3
            Me.LabelControl3.Text = "&Note"
            Me.LabelControl3.ToolTipController = Me.ToolTipController1
            '
            'retention_action
            '
            Me.retention_action.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.retention_action.Location = New System.Drawing.Point(88, 48)
            Me.retention_action.Name = "retention_action"
            Me.retention_action.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.retention_action.Properties.Columns.AddRange(New DevExpress.XtraEditors.Controls.LookUpColumnInfo() {New DevExpress.XtraEditors.Controls.LookUpColumnInfo("item_key", "ID", 20, DevExpress.Utils.FormatType.Numeric, "f0", False, DevExpress.Utils.HorzAlignment.[Default]), New DevExpress.XtraEditors.Controls.LookUpColumnInfo("description", "Description", 20, DevExpress.Utils.FormatType.None, "", True, DevExpress.Utils.HorzAlignment.[Default], DevExpress.Data.ColumnSortOrder.Ascending)})
            Me.retention_action.Properties.NullText = ""
            Me.retention_action.Properties.ShowFooter = False
            Me.retention_action.Properties.ShowHeader = False
            Me.retention_action.Properties.ShowLines = False
            Me.retention_action.Size = New System.Drawing.Size(367, 20)
            Me.retention_action.TabIndex = 2
            Me.retention_action.ToolTipController = Me.ToolTipController1
            Me.retention_action.Properties.SortColumnIndex = 1
            '
            'retention_message
            '
            Me.retention_message.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.retention_message.Location = New System.Drawing.Point(88, 80)
            Me.retention_message.Name = "retention_message"
            Me.retention_message.Properties.MaxLength = 256
            Me.retention_message.Size = New System.Drawing.Size(367, 96)
            Me.retention_message.TabIndex = 4
            Me.retention_message.ToolTipController = Me.ToolTipController1
            '
            'Button_OK
            '
            Me.Button_OK.Anchor = System.Windows.Forms.AnchorStyles.Bottom
            Me.Button_OK.DialogResult = System.Windows.Forms.DialogResult.OK
            Me.Button_OK.Location = New System.Drawing.Point(150, 192)
            Me.Button_OK.Name = "Button_OK"
            Me.Button_OK.Size = New System.Drawing.Size(75, 23)
            Me.Button_OK.TabIndex = 5
            Me.Button_OK.Text = "&OK"
            Me.Button_OK.ToolTipController = Me.ToolTipController1
            '
            'Button_Cance
            '
            Me.Button_Cance.Anchor = System.Windows.Forms.AnchorStyles.Bottom
            Me.Button_Cance.DialogResult = System.Windows.Forms.DialogResult.Cancel
            Me.Button_Cance.Location = New System.Drawing.Point(246, 192)
            Me.Button_Cance.Name = "Button_Cance"
            Me.Button_Cance.Size = New System.Drawing.Size(75, 23)
            Me.Button_Cance.TabIndex = 6
            Me.Button_Cance.Text = "&Cancel"
            Me.Button_Cance.ToolTipController = Me.ToolTipController1
            '
            'Form_Retention_Action
            '
            Me.AcceptButton = Me.Button_OK
            Me.CancelButton = Me.Button_Cance
            Me.ClientSize = New System.Drawing.Size(471, 230)
            Me.Controls.Add(Me.Button_Cance)
            Me.Controls.Add(Me.Button_OK)
            Me.Controls.Add(Me.retention_message)
            Me.Controls.Add(Me.retention_action)
            Me.Controls.Add(Me.LabelControl3)
            Me.Controls.Add(Me.LabelControl2)
            Me.Controls.Add(Me.LabelControl1)
            Me.Name = "Form_Retention_Action"
            Me.Text = "Retention Action"
            CType(Me.DxErrorProvider1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.retention_action.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.retention_message.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            Me.ResumeLayout(False)
            Me.PerformLayout()

        End Sub

#End Region

        Private Sub Form_Retention_Action_Load(ByVal sender As System.Object, ByVal e As System.EventArgs)

            ' Define the list of retention events
            Dim tbl As System.Data.DataTable = RetentionActionsTable()
            With retention_action.Properties
                .DataSource = tbl.DefaultView
                .DisplayMember = "description"
                .ValueMember = "retention_action"
            End With

            ' Bind the fields to the items on the form
            retention_message.DataBindings.Clear()
            retention_message.DataBindings.Add("EditValue", drv, "retention_message")

            retention_action.DataBindings.Clear()
            retention_action.DataBindings.Add("EditValue", drv, "retention_action")
        End Sub

        Private Sub retention_action_EditValueChanged(ByVal sender As Object, ByVal e As System.EventArgs)
            Dim Enabled As Boolean = False
            If retention_action.EditValue IsNot Nothing AndAlso retention_action.EditValue IsNot System.DBNull.Value Then Enabled = True
            Button_OK.Enabled = Enabled

            If Enabled Then
                Dim ItemDRV As System.Data.DataRowView = CType(retention_action.Properties.GetDataSourceRowByKeyValue(retention_action.EditValue), System.Data.DataRowView)
                If ItemDRV Is Nothing Then
                    drv("retention_description") = String.Empty
                Else
                    drv("retention_description") = ItemDRV("description")
                End If
            End If
        End Sub
    End Class
End Namespace
