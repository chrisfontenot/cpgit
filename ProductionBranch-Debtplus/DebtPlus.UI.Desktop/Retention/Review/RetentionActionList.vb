#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}
#End Region

Option Compare Binary
Option Explicit On 
Option Strict On

Imports DebtPlus.UI.Desktop.Retention.Common
Imports System.Data.SqlClient
Imports System.Windows.Forms

Namespace Retention.Review
    Friend Class RetentionActionList
        Inherits BaseGridControl

#Region " Windows Form Designer generated code "

        Public Sub New()
            MyBase.New()
            InitializeComponent()
        End Sub

        'UserControl overrides dispose to clean up the component list.
        Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
            If disposing Then
                If components IsNot Nothing Then
                    components.Dispose()
                End If
            End If
            MyBase.Dispose(disposing)
        End Sub

        'Required by the Windows Form Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Windows Form Designer
        'It can be modified using the Windows Form Designer.  
        'Do not modify it using the code editor.
        Friend WithEvents GridColumn_retention_action As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents GridColumn_message As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents GridColumn_date_created As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents GridColumn_created_by As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents GridColumn_action_description As DevExpress.XtraGrid.Columns.GridColumn
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
            Me.GridColumn_retention_action = New DevExpress.XtraGrid.Columns.GridColumn
            Me.GridColumn_retention_action.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True
            Me.GridColumn_message = New DevExpress.XtraGrid.Columns.GridColumn
            Me.GridColumn_message.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True
            Me.GridColumn_date_created = New DevExpress.XtraGrid.Columns.GridColumn
            Me.GridColumn_date_created.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True
            Me.GridColumn_created_by = New DevExpress.XtraGrid.Columns.GridColumn
            Me.GridColumn_created_by.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True
            Me.GridColumn_action_description = New DevExpress.XtraGrid.Columns.GridColumn
            Me.GridColumn_action_description.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True
            Me.SuspendLayout()
            Me.GridView1.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.GridColumn_retention_action, Me.GridColumn_message, Me.GridColumn_date_created, Me.GridColumn_created_by, Me.GridColumn_action_description})
            Me.GridView1.OptionsView.ShowPreview = True
            Me.GridView1.PreviewFieldName = "retention_message"
            '
            'GridColumn_retention_action
            '
            Me.GridColumn_retention_action.Caption = "ID"
            Me.GridColumn_retention_action.FieldName = "client_retention_action"
            Me.GridColumn_retention_action.Name = "GridColumn_retention_action"
            '
            'GridColumn_message
            '
            Me.GridColumn_message.Caption = "Note"
            Me.GridColumn_message.FieldName = "message"
            Me.GridColumn_message.Name = "GridColumn_message"
            '
            'GridColumn_date_created
            '
            Me.GridColumn_date_created.Caption = "Date"
            Me.GridColumn_date_created.FieldName = "date_created"
            Me.GridColumn_date_created.Name = "GridColumn_date_created"
            Me.GridColumn_date_created.Visible = True
            Me.GridColumn_date_created.VisibleIndex = 0
            Me.GridColumn_date_created.Width = 93
            '
            'GridColumn_created_by
            '
            Me.GridColumn_created_by.Caption = "Counselor"
            Me.GridColumn_created_by.FieldName = "created_by"
            Me.GridColumn_created_by.Name = "GridColumn_created_by"
            Me.GridColumn_created_by.Visible = True
            Me.GridColumn_created_by.VisibleIndex = 1
            Me.GridColumn_created_by.Width = 103
            '
            'GridColumn_action_description
            '
            Me.GridColumn_action_description.Caption = "Action"
            Me.GridColumn_action_description.FieldName = "retention_description"
            Me.GridColumn_action_description.Name = "GridColumn_action_description"
            Me.GridColumn_action_description.Visible = True
            Me.GridColumn_action_description.VisibleIndex = 2
            Me.GridColumn_action_description.Width = 240
            '
            Me.GridView1.OptionsView.ShowPreviewRowLines = DevExpress.Utils.DefaultBoolean.True
            Me.GridView1.OptionsView.RowAutoHeight = True
            Me.GridView1.OptionsView.AutoCalcPreviewLineCount = True
            Me.GridView1.OptionsView.ShowPreview = True
            Me.GridView1.PreviewFieldName = "retention_message"
            Me.GridView1.Appearance.Preview.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap
            '
            'RetentionActionList
            '
            Me.Controls.Add(Me.GridControl1)
            Me.Name = "RetentionActionList"
            Me.Size = New System.Drawing.Size(440, 150)
            Me.ResumeLayout(False)

        End Sub

#End Region

        Private client_retention_event As System.Int32 = -1
        Public Sub Reload(ByVal client_retention_event As System.Int32)

            ' Save the existing changes to the table before it is reloaded
            SaveChanges()

            ' Save the new retention event if there is one.
            Me.client_retention_event = client_retention_event
            Dim tbl As System.Data.DataTable = ds.Tables("client_retention_actions")
            If tbl IsNot Nothing Then tbl.Clear()

            Dim current_cursor As Cursor = System.Windows.Forms.Cursor.Current
            Try
                System.Windows.Forms.Cursor.Current = Cursors.WaitCursor
                Using cmd As SqlClient.SqlCommand = New SqlCommand
                    With cmd
                        .Connection = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
                        .CommandText = "SELECT [client_retention_action],[client_retention_event],[retention_action],[date_created],[created_by],[retention_message],[retention_description],[retention_letter_code],[letter_description] FROM view_retention_client_actions WHERE client_retention_event=@client_retention_event"
                        .Parameters.Add("@client_retention_event", SqlDbType.Int).Value = client_retention_event
                    End With
                    Using da As New SqlClient.SqlDataAdapter(cmd)
                        da.Fill(ds, "client_retention_actions")
                    End Using
                End Using

                tbl = ds.Tables("client_retention_actions")
                tbl.PrimaryKey = New System.Data.DataColumn() {tbl.Columns("client_retention_action")}

                With tbl.Columns("client_retention_action")
                    .AutoIncrement = True
                    .AutoIncrementSeed = -1
                    .AutoIncrementStep = -1
                End With

                Dim vue As System.Data.DataView = tbl.DefaultView
                With GridControl1
                    .DataSource = vue
                    .RefreshDataSource()
                End With

            Catch ex As SqlClient.SqlException
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error reading client_retention_actions table")

            Finally
                System.Windows.Forms.Cursor.Current = current_cursor
            End Try
        End Sub

        Protected Overrides Function EditItem(ByVal drv As System.Data.DataRowView) As Boolean
            Dim answer As Boolean = False
            With New Form_Retention_Action(drv)
                If .ShowDialog() = DialogResult.OK Then answer = True

                ' If successful then update the date/time of the last action. It is always "now".
                If answer Then
                    Dim tbl As System.Data.DataTable = ds.Tables("client_retention_events")
                    If tbl IsNot Nothing Then
                        Dim row As System.Data.DataRow = tbl.Rows.Find(client_retention_event)
                        If row IsNot Nothing Then row("action_date") = Now
                    End If
                End If

                .Dispose()
            End With

            Return answer
        End Function

        Protected Overrides Function CreateItem(ByVal drv As System.Data.DataRowView) As Boolean
            drv("client_retention_event") = client_retention_event
            drv("created_by") = "me"
            drv("date_created") = Now

            ' Find the edit operation
            Dim answer As Boolean = EditItem(drv)
            Return answer
        End Function

        Protected Overrides Function DeleteItem(ByVal drv As System.Data.DataRowView) As Boolean
            Return DebtPlus.Data.Prompts.RequestConfirmation_Delete = DialogResult.OK
        End Function

        Public Sub SaveChanges()

            ' Do the deletion items first. Delete the action items as well for any item being deleted
            Dim tbl As System.Data.DataTable = ds.Tables("client_retention_actions")

            If tbl IsNot Nothing Then

                ' Find the items that are added to the list
                Using vue As New System.Data.DataView(tbl, String.Empty, String.Empty, DataViewRowState.Added)
                    For Each drv As System.Data.DataRowView In vue
                        GeneratePossibleLetter(drv)
                    Next
                End Using

                ' Do the database update at this point
                Dim DeleteCmd As SqlClient.SqlCommand = New SqlCommand
                Dim InsertCmd As SqlClient.SqlCommand = New SqlCommand
                Dim UpdateCmd As SqlClient.SqlCommand = New SqlCommand
                Dim current_cursor As Cursor = System.Windows.Forms.Cursor.Current
                Try
                    Using da As New SqlClient.SqlDataAdapter
                        With DeleteCmd
                            .Connection = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
                            .CommandText = "DELETE FROM client_retention_actions WHERE client_retention_action=@client_retention_action"
                            .Parameters.Add("@client_retention_action", SqlDbType.Int, CByte(0), "client_retention_action")
                        End With
                        da.DeleteCommand = DeleteCmd

                        ' Do the update of the retention events
                        With UpdateCmd
                            .Connection = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
                            .CommandText = "UPDATE client_retention_actions SET retention_action=@retention_action, message=@message WHERE client_retention_action=@client_retention_action"
                            .Parameters.Add("@retention_action", SqlDbType.Int, CByte(0), "retention_action")
                            .Parameters.Add("@message", SqlDbType.VarChar, 1024, "message")
                            .Parameters.Add("@client_retention_action", SqlDbType.Int, CByte(0), "client_retention_action")
                        End With
                        da.UpdateCommand = UpdateCmd

                        ' Do the insert of the retention events
                        With InsertCmd
                            .Connection = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
                            .CommandText = "xpr_insert_client_retention_action"
                            .CommandType = CommandType.StoredProcedure
                            .Parameters.Add("@client_retention_action", SqlDbType.Int, CByte(0), "client_retention_action").Direction = ParameterDirection.ReturnValue
                            .Parameters.Add("@client_retention_event", SqlDbType.Int, CByte(0), "client_retention_event")
                            .Parameters.Add("@retention_action", SqlDbType.Int, CByte(0), "retention_action")
                            .Parameters.Add("@message", SqlDbType.VarChar, 1024, "message")
                        End With
                        da.InsertCommand = InsertCmd

                        ' Update the table with the retention events
                        System.Windows.Forms.Cursor.Current = Cursors.WaitCursor
                        da.Update(tbl)
                    End Using

                    tbl.AcceptChanges()

                Catch ex As SqlClient.SqlException
                    DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error updating client_retention_actions table")

                Finally
                    InsertCmd.Dispose()
                    UpdateCmd.Dispose()
                    DeleteCmd.Dispose()
                    System.Windows.Forms.Cursor.Current = current_cursor
                End Try
            End If
        End Sub

        Private Sub GeneratePossibleLetter(ByVal drv As System.Data.DataRowView)
            Dim RetentionAction As Int32 = -1
            If drv("retention_action") IsNot Nothing AndAlso drv("retention_action") IsNot System.DBNull.Value Then
                RetentionAction = Convert.ToInt32(drv("retention_action"))
            End If

            ' Find the client for the event. It is needed for the ClientId letters.
            Dim frm As Form_Retention_Event = CType(Me.ParentForm, Form_Retention_Event)
            Dim retention_event_drv As System.Data.DataRowView = frm.drv
            Dim Client As Int32 = Convert.ToInt32(retention_event_drv("client"))

            ' Generate the letter if needed
            GeneratePossibleLetter(Client, RetentionAction)
        End Sub

        Private Sub GeneratePossibleLetter(ByVal Client As Int32, ByVal RetentionAction As Int32)

            ' Locate the definition for the retention action in the tables
            Dim tbl As System.Data.DataTable = RetentionActionsTable()
            If tbl IsNot Nothing Then
                Dim row As System.Data.DataRow = tbl.Rows.Find(RetentionAction)
                If row IsNot Nothing Then
                    Dim LetterCode As String = DebtPlus.Utils.Nulls.DStr(row("letter_code"))
                    If LetterCode <> String.Empty Then
                        PrintLetter(Client, LetterCode)
                    End If
                End If
            End If
        End Sub

        Private Sub PrintLetter(ByVal Client As Int32, ByVal LetterCode As String)
            Dim letter_class As New LetterThread(Client, LetterCode)
            Dim thrd As New System.Threading.Thread(AddressOf letter_class.Show)
            thrd.SetApartmentState(Threading.ApartmentState.STA)
            thrd.IsBackground = False
            thrd.Name = "LetterThread"
            thrd.Start()
        End Sub

        Private Class LetterThread
            Private ReadOnly Client As Int32
            Private ReadOnly LetterCode As String

            Public Sub New(ByVal Client As Int32, ByVal LetterCode As String)
                Me.Client = Client
                Me.LetterCode = LetterCode
            End Sub

            Public Sub Show()
                Using ltr As New DebtPlus.Letters.Compose.Composition(LetterCode)
                    AddHandler ltr.GetValue, AddressOf LetterGetField
                    ltr.PrintLetter()
                    RemoveHandler ltr.GetValue, AddressOf LetterGetField
                End Using
            End Sub

            Private Sub LetterGetField(ByVal sender As Object, ByVal e As DebtPlus.Events.ParameterValueEventArgs)
                If e.Name = DebtPlus.Events.ParameterValueEventArgs.name_client Then
                    e.Value = Client
                End If
            End Sub
        End Class

    End Class
End Namespace
