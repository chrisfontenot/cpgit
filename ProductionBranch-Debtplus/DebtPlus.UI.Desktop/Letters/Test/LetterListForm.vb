#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}

#End Region

Imports DebtPlus.Letters.Compose
Imports DevExpress.XtraEditors.Controls
Imports DebtPlus.Data.Forms
Imports DebtPlus.Utils
Imports DebtPlus.Events
Imports System.Data.SqlClient
Imports System.Globalization
Imports System.Linq
Imports DebtPlus.LINQ

Namespace Letters.Test
    Friend Class LetterListForm
        Inherits DebtPlusForm

        Private ReadOnly ap As ArgsParser

        Public Sub New()
            MyBase.New()
            InitializeComponent()
        End Sub

        Public Sub New(ByVal ap As ArgsParser)
            MyClass.New()
            Me.ap = ap
            AddHandler Load, AddressOf ReportListForm_Load
            AddHandler SimpleButton_Cancel.Click, AddressOf SimpleButton_Cancel_Click
            AddHandler SimpleButton_OK.Click, AddressOf SimpleButton_OK_Click
        End Sub

        Private ReadOnly _reportName As String = String.Empty

        Public ReadOnly Property ReportName() As String
            Get
                Return _reportName
            End Get
        End Property

        ''' <summary>
        ''' Handle the loading of the form
        ''' </summary>
        Private Sub ReportListForm_Load(ByVal sender As Object, ByVal e As EventArgs)

            LookUpEdit1.Properties.DataSource = DebtPlus.LINQ.Cache.letter_type.getList()
            AddHandler LookUpEdit1.EditValueChanged, AddressOf FormChanged

            TextEdit1.EditValue = String.Empty
            DateEdit1.EditValue = Now.Date
            DateEdit1.EditValue = Nothing
            CalcEdit1.EditValue = 0D

            SimpleButton_OK.Enabled = LookUpEdit1.EditValue IsNot Nothing AndAlso LookUpEdit1.EditValue IsNot DBNull.Value
        End Sub

        ''' <summary>
        ''' Process a change condition on the form
        ''' </summary>
        Private Sub FormChanged(ByVal Sender As Object, ByVal e As EventArgs)
            SimpleButton_OK.Enabled = LookUpEdit1.EditValue IsNot Nothing AndAlso LookUpEdit1.EditValue IsNot DBNull.Value
        End Sub

        ''' <summary>
        ''' Close the form on the CANCEL button
        ''' </summary>
        Private Sub SimpleButton_Cancel_Click(ByVal sender As Object, ByVal e As EventArgs)
            Close()
        End Sub

        ''' <summary>
        ''' Generate the letter when the OK button is clicked
        ''' </summary>
        Private Sub SimpleButton_OK_Click(ByVal sender As Object, ByVal e As EventArgs)
            Using ltr As New MyLetterComposition(Convert.ToInt32(LookUpEdit1.EditValue))
                AddHandler ltr.GetValue, AddressOf Letter_Getvalue
                ltr.PrintLetter()
            End Using
        End Sub

        ''' <summary>
        ''' Process a request for data from the letters
        ''' </summary>
        Private Sub Letter_Getvalue(ByVal Sender As Object, ByVal e As DebtPlus.Events.ParameterValueEventArgs)
            If e.Name = ParameterValueEventArgs.name_client Then
                e.Value = ClientID1.EditValue.GetValueOrDefault(-1)
            ElseIf e.Name = ParameterValueEventArgs.name_creditor Then
                e.Value = CreditorID1.EditValue
            ElseIf e.Name = ParameterValueEventArgs.name_debt Then
                Dim IntValue As Int32
                If Int32.TryParse(TextEdit1.Text.Trim(), NumberStyles.Integer, CultureInfo.InvariantCulture, IntValue) AndAlso IntValue > 0 Then
                    e.Value = IntValue
                End If
            ElseIf e.Name = ParameterValueEventArgs.name_appointment Then
                Dim answer As Int32 = CType(Sender, MyLetterComposition).DefaultClientAppointment
                If answer > 0 Then e.Value = answer
            End If
        End Sub

        ''' <summary>
        ''' Change on the debt number
        ''' </summary>
        Private Sub SpinEdit1_EditValueChanging(ByVal sender As Object, ByVal e As ChangingEventArgs)
            e.Cancel = Convert.ToInt32(e.NewValue) <= 0
        End Sub

        ''' <summary>
        ''' Override the base class to not do some things
        ''' </summary>
        Private Class MyLetterComposition
            Inherits Base

            Public Sub New()
                MyBase.New()
            End Sub

            Public Sub New(ByVal LetterCode As String)
                MyBase.New(LetterCode)
            End Sub

            Public Sub New(ByVal LetterType As Int32)
                MyBase.New(LetterType)
            End Sub

            ''' <summary>
            ''' Display the letter for the user
            ''' </summary>
            Protected Overrides Sub DoPrintLetter(ByVal ShowPrinterDialog As Boolean)
                DoShow()    ' this is too easy.

                ' To test the queue logic, replace the DoShow with these two statements.
                'queue_name = "Testing Letters" ' A name is required for a queue.
                'DoQueue()
            End Sub

            ''' <summary>
            ''' If we are queueing the letter in response to a print button, don't
            ''' </summary>
            Protected Overrides Sub DoQueueRoutine()
                MyBase.DoPrintRoutine(True)
            End Sub

            ''' <summary>
            ''' Write the system note that the letter was printed
            ''' </summary>
            Protected Overrides Sub WriteSystemNote()
                ' Never write a system note so we override this to do nothing.
            End Sub

            ''' <summary>
            ''' Extend the method to find an appointment, even if it is not for this client
            ''' </summary>
            Public Overrides Function DefaultClientAppointment() As Int32

                ' Try the standard logic first
                Dim answer As Int32 = MyBase.DefaultClientAppointment()

                ' If there is no answer then look for any appointment for this client
                If answer <= 0 Then
                    Using bc As New BusinessContext()

                        ' Look for a pending appointment for this client
                        Dim q As client_appointment = (From ca In bc.client_appointments Where ca.client = ClientId And ca.status = "P"c Select ca).FirstOrDefault()
                        If q Is Nothing Then

                            ' If not for this client then take any pending appointment
                            q = (From ca In bc.client_appointments Where ca.status = "P"c Select ca).FirstOrDefault()
                            If q Is Nothing Then

                                ' Failing all else, just take the last appointment in the system
                                q = (From ca In bc.client_appointments Order By ca.Id Take 1 Select ca).FirstOrDefault()
                            End If
                        End If

                        If q IsNot Nothing Then
                            answer = q.Id
                        End If
                    End Using
                End If

                Return answer
            End Function

        End Class
    End Class
End Namespace
