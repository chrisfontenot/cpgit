﻿
Imports DebtPlus.UI.Creditor.Widgets.Controls
Imports DebtPlus.UI.Client.Widgets.Controls
Imports DebtPlus.UI.Creditor.Widgets

Namespace Letters.Test
    <Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
    Partial Class LetterListForm
        Inherits DebtPlus.Data.Forms.DebtPlusForm

        'Form overrides dispose to clean up the component list.
        <System.Diagnostics.DebuggerNonUserCode()> _
        Protected Overrides Sub Dispose(ByVal disposing As Boolean)
            Try
                If disposing AndAlso components IsNot Nothing Then
                    components.Dispose()
                End If
            Finally
                MyBase.Dispose(disposing)
            End Try
        End Sub

        'Required by the Windows Form Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Windows Form Designer
        'It can be modified using the Windows Form Designer.  
        'Do not modify it using the code editor.
        <System.Diagnostics.DebuggerStepThrough()> _
        Private Sub InitializeComponent()
            Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(LetterListForm))
            Dim SerializableAppearanceObject1 As DevExpress.Utils.SerializableAppearanceObject = New DevExpress.Utils.SerializableAppearanceObject
            Dim SerializableAppearanceObject2 As DevExpress.Utils.SerializableAppearanceObject = New DevExpress.Utils.SerializableAppearanceObject
            Me.SimpleButton_OK = New DevExpress.XtraEditors.SimpleButton
            Me.LayoutControl1 = New DevExpress.XtraLayout.LayoutControl
            Me.TextEdit1 = New DevExpress.XtraEditors.TextEdit
            Me.CreditorID1 = New CreditorID
            Me.ClientID1 = New DebtPlus.UI.Client.Widgets.Controls.ClientID
            Me.CalcEdit1 = New DevExpress.XtraEditors.CalcEdit
            Me.LookUpEdit1 = New DevExpress.XtraEditors.LookUpEdit
            Me.DateEdit1 = New DevExpress.XtraEditors.DateEdit
            Me.SimpleButton_Cancel = New DevExpress.XtraEditors.SimpleButton
            Me.LayoutControlGroup1 = New DevExpress.XtraLayout.LayoutControlGroup
            Me.LayoutControlItem1 = New DevExpress.XtraLayout.LayoutControlItem
            Me.LayoutControlItem2 = New DevExpress.XtraLayout.LayoutControlItem
            Me.LayoutControlItem3 = New DevExpress.XtraLayout.LayoutControlItem
            Me.LayoutControlItem5 = New DevExpress.XtraLayout.LayoutControlItem
            Me.LayoutControlItem6 = New DevExpress.XtraLayout.LayoutControlItem
            Me.LayoutControlItem7 = New DevExpress.XtraLayout.LayoutControlItem
            Me.LayoutControlItem8 = New DevExpress.XtraLayout.LayoutControlItem
            Me.LayoutControlItem4 = New DevExpress.XtraLayout.LayoutControlItem
            Me.EmptySpaceItem1 = New DevExpress.XtraLayout.EmptySpaceItem
            Me.EmptySpaceItem2 = New DevExpress.XtraLayout.EmptySpaceItem
            Me.EmptySpaceItem3 = New DevExpress.XtraLayout.EmptySpaceItem
            Me.EmptySpaceItem4 = New DevExpress.XtraLayout.EmptySpaceItem
            Me.EmptySpaceItem5 = New DevExpress.XtraLayout.EmptySpaceItem
            Me.EmptySpaceItem6 = New DevExpress.XtraLayout.EmptySpaceItem
            Me.EmptySpaceItem7 = New DevExpress.XtraLayout.EmptySpaceItem
            Me.EmptySpaceItem8 = New DevExpress.XtraLayout.EmptySpaceItem
            Me.EmptySpaceItem9 = New DevExpress.XtraLayout.EmptySpaceItem
            Me.EmptySpaceItem10 = New DevExpress.XtraLayout.EmptySpaceItem
            CType(Me.DxErrorProvider1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControl1, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.LayoutControl1.SuspendLayout()
            CType(Me.TextEdit1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.CreditorID1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.ClientID1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.CalcEdit1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LookUpEdit1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.DateEdit1.Properties.VistaTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.DateEdit1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlGroup1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem2, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem3, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem5, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem6, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem7, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem8, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem4, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.EmptySpaceItem1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.EmptySpaceItem2, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.EmptySpaceItem3, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.EmptySpaceItem4, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.EmptySpaceItem5, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.EmptySpaceItem6, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.EmptySpaceItem7, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.EmptySpaceItem8, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.EmptySpaceItem9, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.EmptySpaceItem10, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.SuspendLayout()
            '
            'SimpleButton_OK
            '
            Me.SimpleButton_OK.Anchor = System.Windows.Forms.AnchorStyles.Bottom
            Me.SimpleButton_OK.DialogResult = System.Windows.Forms.DialogResult.OK
            Me.SimpleButton_OK.Location = New System.Drawing.Point(73, 182)
            Me.SimpleButton_OK.MaximumSize = New System.Drawing.Size(75, 23)
            Me.SimpleButton_OK.MinimumSize = New System.Drawing.Size(75, 23)
            Me.SimpleButton_OK.Name = "SimpleButton_OK"
            Me.SimpleButton_OK.Size = New System.Drawing.Size(75, 23)
            Me.SimpleButton_OK.StyleController = Me.LayoutControl1
            Me.SimpleButton_OK.TabIndex = 12
            Me.SimpleButton_OK.Text = "OK"
            '
            'LayoutControl1
            '
            Me.LayoutControl1.Controls.Add(Me.TextEdit1)
            Me.LayoutControl1.Controls.Add(Me.CreditorID1)
            Me.LayoutControl1.Controls.Add(Me.ClientID1)
            Me.LayoutControl1.Controls.Add(Me.CalcEdit1)
            Me.LayoutControl1.Controls.Add(Me.LookUpEdit1)
            Me.LayoutControl1.Controls.Add(Me.DateEdit1)
            Me.LayoutControl1.Controls.Add(Me.SimpleButton_Cancel)
            Me.LayoutControl1.Controls.Add(Me.SimpleButton_OK)
            Me.LayoutControl1.Dock = System.Windows.Forms.DockStyle.Fill
            Me.LayoutControl1.Location = New System.Drawing.Point(0, 0)
            Me.LayoutControl1.Name = "LayoutControl1"
            Me.LayoutControl1.Root = Me.LayoutControlGroup1
            Me.LayoutControl1.Size = New System.Drawing.Size(332, 236)
            Me.LayoutControl1.TabIndex = 14
            Me.LayoutControl1.Text = "LayoutControl1"
            '
            'TextEdit1
            '
            Me.TextEdit1.Location = New System.Drawing.Point(98, 84)
            Me.TextEdit1.Name = "TextEdit1"
            Me.TextEdit1.Size = New System.Drawing.Size(103, 20)
            Me.TextEdit1.StyleController = Me.LayoutControl1
            Me.TextEdit1.TabIndex = 6
            '
            'CreditorID1
            '
            Me.CreditorID1.EditValue = Nothing
            Me.CreditorID1.Location = New System.Drawing.Point(98, 60)
            Me.CreditorID1.Name = "CreditorID1"
            Me.CreditorID1.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "", -1, True, True, False, DevExpress.XtraEditors.ImageLocation.MiddleCenter, CType(resources.GetObject("CreditorID1.Properties.Buttons"), System.Drawing.Image), New DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), SerializableAppearanceObject1, "Click here to search for a creditor ID", "search", Nothing, True)})
            Me.CreditorID1.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
            Me.CreditorID1.Properties.Mask.AutoComplete = DevExpress.XtraEditors.Mask.AutoCompleteType.Strong
            Me.CreditorID1.Properties.Mask.BeepOnError = True
            Me.CreditorID1.Properties.Mask.EditMask = "[A-Z]{1,2}[0-9]{4,}"
            Me.CreditorID1.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx
            Me.CreditorID1.Properties.Mask.UseMaskAsDisplayFormat = True
            Me.CreditorID1.Properties.MaxLength = 10
            Me.CreditorID1.Size = New System.Drawing.Size(103, 20)
            Me.CreditorID1.StyleController = Me.LayoutControl1
            Me.CreditorID1.TabIndex = 5
            '
            'ClientID1
            '
            Me.ClientID1.Location = New System.Drawing.Point(98, 36)
            Me.ClientID1.Name = "ClientID1"
            Me.ClientID1.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[False]
            Me.ClientID1.Properties.Appearance.Options.UseTextOptions = True
            Me.ClientID1.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.ClientID1.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "", -1, True, True, False, DevExpress.XtraEditors.ImageLocation.MiddleCenter, CType(resources.GetObject("ClientID1.Properties.Buttons"), System.Drawing.Image), New DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), SerializableAppearanceObject2, "Click here to search for a client ID", "search", Nothing, True)})
            Me.ClientID1.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
            Me.ClientID1.Properties.DisplayFormat.FormatString = "0000000"
            Me.ClientID1.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom
            Me.ClientID1.Properties.EditFormat.FormatString = "f0"
            Me.ClientID1.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.ClientID1.Properties.Mask.BeepOnError = True
            Me.ClientID1.Properties.Mask.EditMask = "\d*"
            Me.ClientID1.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx
            Me.ClientID1.Properties.ValidateOnEnterKey = True
            Me.ClientID1.Size = New System.Drawing.Size(103, 20)
            Me.ClientID1.StyleController = Me.LayoutControl1
            Me.ClientID1.TabIndex = 4
            '
            'CalcEdit1
            '
            Me.CalcEdit1.Location = New System.Drawing.Point(98, 132)
            Me.CalcEdit1.Name = "CalcEdit1"
            Me.CalcEdit1.Properties.Appearance.Options.UseTextOptions = True
            Me.CalcEdit1.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.CalcEdit1.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.CalcEdit1.Properties.DisplayFormat.FormatString = "{0:c}"
            Me.CalcEdit1.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.CalcEdit1.Properties.EditFormat.FormatString = "{0:c}"
            Me.CalcEdit1.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.CalcEdit1.Properties.Mask.BeepOnError = True
            Me.CalcEdit1.Properties.Mask.EditMask = "c"
            Me.CalcEdit1.Properties.Precision = 2
            Me.CalcEdit1.Size = New System.Drawing.Size(103, 20)
            Me.CalcEdit1.StyleController = Me.LayoutControl1
            Me.CalcEdit1.TabIndex = 11
            '
            'LookUpEdit1
            '
            Me.LookUpEdit1.Location = New System.Drawing.Point(98, 12)
            Me.LookUpEdit1.Name = "LookUpEdit1"
            Me.LookUpEdit1.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.LookUpEdit1.Properties.Columns.AddRange(New DevExpress.XtraEditors.Controls.LookUpColumnInfo() {New DevExpress.XtraEditors.Controls.LookUpColumnInfo("Id", "ID", 20, DevExpress.Utils.FormatType.Numeric, "f0", False, DevExpress.Utils.HorzAlignment.[Default]), New DevExpress.XtraEditors.Controls.LookUpColumnInfo("description", "Description", 20, DevExpress.Utils.FormatType.None, "", True, DevExpress.Utils.HorzAlignment.[Default], DevExpress.Data.ColumnSortOrder.Ascending)})
            Me.LookUpEdit1.Properties.DisplayMember = "description"
            Me.LookUpEdit1.Properties.NullText = ""
            Me.LookUpEdit1.Properties.ShowFooter = False
            Me.LookUpEdit1.Properties.ShowHeader = False
            Me.LookUpEdit1.Properties.ValueMember = "Id"
            Me.LookUpEdit1.Size = New System.Drawing.Size(222, 20)
            Me.LookUpEdit1.StyleController = Me.LayoutControl1
            Me.LookUpEdit1.TabIndex = 1
            Me.LookUpEdit1.Properties.SortColumnIndex = 1
            '
            'DateEdit1
            '
            Me.DateEdit1.EditValue = Nothing
            Me.DateEdit1.Location = New System.Drawing.Point(98, 108)
            Me.DateEdit1.Name = "DateEdit1"
            Me.DateEdit1.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.DateEdit1.Properties.VistaTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton})
            Me.DateEdit1.Size = New System.Drawing.Size(103, 20)
            Me.DateEdit1.StyleController = Me.LayoutControl1
            Me.DateEdit1.TabIndex = 9
            '
            'SimpleButton_Cancel
            '
            Me.SimpleButton_Cancel.Anchor = System.Windows.Forms.AnchorStyles.Bottom
            Me.SimpleButton_Cancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
            Me.SimpleButton_Cancel.Location = New System.Drawing.Point(189, 182)
            Me.SimpleButton_Cancel.MaximumSize = New System.Drawing.Size(75, 23)
            Me.SimpleButton_Cancel.MinimumSize = New System.Drawing.Size(75, 23)
            Me.SimpleButton_Cancel.Name = "SimpleButton_Cancel"
            Me.SimpleButton_Cancel.Size = New System.Drawing.Size(75, 23)
            Me.SimpleButton_Cancel.StyleController = Me.LayoutControl1
            Me.SimpleButton_Cancel.TabIndex = 13
            Me.SimpleButton_Cancel.Text = "Quit"
            '
            'LayoutControlGroup1
            '
            Me.LayoutControlGroup1.CustomizationFormText = "LayoutControlGroup1"
            Me.LayoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.[True]
            Me.LayoutControlGroup1.GroupBordersVisible = False
            Me.LayoutControlGroup1.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.LayoutControlItem1, Me.LayoutControlItem2, Me.LayoutControlItem3, Me.LayoutControlItem5, Me.LayoutControlItem6, Me.LayoutControlItem7, Me.LayoutControlItem8, Me.LayoutControlItem4, Me.EmptySpaceItem1, Me.EmptySpaceItem2, Me.EmptySpaceItem3, Me.EmptySpaceItem4, Me.EmptySpaceItem5, Me.EmptySpaceItem6, Me.EmptySpaceItem7, Me.EmptySpaceItem8, Me.EmptySpaceItem9, Me.EmptySpaceItem10})
            Me.LayoutControlGroup1.Location = New System.Drawing.Point(0, 0)
            Me.LayoutControlGroup1.Name = "LayoutControlGroup1"
            Me.LayoutControlGroup1.Size = New System.Drawing.Size(332, 236)
            Me.LayoutControlGroup1.Spacing = New DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0)
            Me.LayoutControlGroup1.Text = "LayoutControlGroup1"
            Me.LayoutControlGroup1.TextVisible = False
            '
            'LayoutControlItem1
            '
            Me.LayoutControlItem1.Control = Me.LookUpEdit1
            Me.LayoutControlItem1.CustomizationFormText = "Letter Type"
            Me.LayoutControlItem1.Location = New System.Drawing.Point(0, 0)
            Me.LayoutControlItem1.Name = "LayoutControlItem1"
            Me.LayoutControlItem1.Size = New System.Drawing.Size(312, 24)
            Me.LayoutControlItem1.Text = "Letter Type"
            Me.LayoutControlItem1.TextSize = New System.Drawing.Size(82, 13)
            '
            'LayoutControlItem2
            '
            Me.LayoutControlItem2.Control = Me.ClientID1
            Me.LayoutControlItem2.CustomizationFormText = "Client"
            Me.LayoutControlItem2.Location = New System.Drawing.Point(0, 24)
            Me.LayoutControlItem2.Name = "LayoutControlItem2"
            Me.LayoutControlItem2.Size = New System.Drawing.Size(193, 24)
            Me.LayoutControlItem2.Text = "Client"
            Me.LayoutControlItem2.TextSize = New System.Drawing.Size(82, 13)
            '
            'LayoutControlItem3
            '
            Me.LayoutControlItem3.Control = Me.CreditorID1
            Me.LayoutControlItem3.CustomizationFormText = "Creditor"
            Me.LayoutControlItem3.Location = New System.Drawing.Point(0, 48)
            Me.LayoutControlItem3.Name = "LayoutControlItem3"
            Me.LayoutControlItem3.Size = New System.Drawing.Size(193, 24)
            Me.LayoutControlItem3.Text = "Creditor"
            Me.LayoutControlItem3.TextSize = New System.Drawing.Size(82, 13)
            '
            'LayoutControlItem5
            '
            Me.LayoutControlItem5.Control = Me.DateEdit1
            Me.LayoutControlItem5.CustomizationFormText = "Followup Date"
            Me.LayoutControlItem5.Location = New System.Drawing.Point(0, 96)
            Me.LayoutControlItem5.Name = "LayoutControlItem5"
            Me.LayoutControlItem5.Size = New System.Drawing.Size(193, 24)
            Me.LayoutControlItem5.Text = "Followup Date"
            Me.LayoutControlItem5.TextSize = New System.Drawing.Size(82, 13)
            '
            'LayoutControlItem6
            '
            Me.LayoutControlItem6.Control = Me.CalcEdit1
            Me.LayoutControlItem6.CustomizationFormText = "Followup Amount"
            Me.LayoutControlItem6.Location = New System.Drawing.Point(0, 120)
            Me.LayoutControlItem6.Name = "LayoutControlItem6"
            Me.LayoutControlItem6.Size = New System.Drawing.Size(193, 24)
            Me.LayoutControlItem6.Text = "Followup Amount"
            Me.LayoutControlItem6.TextSize = New System.Drawing.Size(82, 13)
            '
            'LayoutControlItem7
            '
            Me.LayoutControlItem7.Control = Me.SimpleButton_OK
            Me.LayoutControlItem7.ControlAlignment = System.Drawing.ContentAlignment.MiddleCenter
            Me.LayoutControlItem7.CustomizationFormText = "LayoutControlItem7"
            Me.LayoutControlItem7.Location = New System.Drawing.Point(61, 170)
            Me.LayoutControlItem7.MaxSize = New System.Drawing.Size(79, 27)
            Me.LayoutControlItem7.MinSize = New System.Drawing.Size(79, 27)
            Me.LayoutControlItem7.Name = "LayoutControlItem7"
            Me.LayoutControlItem7.Size = New System.Drawing.Size(79, 27)
            Me.LayoutControlItem7.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom
            Me.LayoutControlItem7.Text = "LayoutControlItem7"
            Me.LayoutControlItem7.TextSize = New System.Drawing.Size(0, 0)
            Me.LayoutControlItem7.TextToControlDistance = 0
            Me.LayoutControlItem7.TextVisible = False
            '
            'LayoutControlItem8
            '
            Me.LayoutControlItem8.Control = Me.SimpleButton_Cancel
            Me.LayoutControlItem8.ControlAlignment = System.Drawing.ContentAlignment.MiddleCenter
            Me.LayoutControlItem8.CustomizationFormText = "LayoutControlItem8"
            Me.LayoutControlItem8.Location = New System.Drawing.Point(177, 170)
            Me.LayoutControlItem8.Name = "LayoutControlItem8"
            Me.LayoutControlItem8.Size = New System.Drawing.Size(79, 27)
            Me.LayoutControlItem8.Text = "LayoutControlItem8"
            Me.LayoutControlItem8.TextSize = New System.Drawing.Size(0, 0)
            Me.LayoutControlItem8.TextToControlDistance = 0
            Me.LayoutControlItem8.TextVisible = False
            '
            'LayoutControlItem4
            '
            Me.LayoutControlItem4.Control = Me.TextEdit1
            Me.LayoutControlItem4.CustomizationFormText = "Debt Record Number"
            Me.LayoutControlItem4.Location = New System.Drawing.Point(0, 72)
            Me.LayoutControlItem4.Name = "LayoutControlItem4"
            Me.LayoutControlItem4.Size = New System.Drawing.Size(193, 24)
            Me.LayoutControlItem4.Text = "Debt Record #"
            Me.LayoutControlItem4.TextSize = New System.Drawing.Size(82, 13)
            '
            'EmptySpaceItem1
            '
            Me.EmptySpaceItem1.CustomizationFormText = "EmptySpaceItem1"
            Me.EmptySpaceItem1.Location = New System.Drawing.Point(0, 144)
            Me.EmptySpaceItem1.Name = "EmptySpaceItem1"
            Me.EmptySpaceItem1.Size = New System.Drawing.Size(312, 26)
            Me.EmptySpaceItem1.Text = "EmptySpaceItem1"
            Me.EmptySpaceItem1.TextSize = New System.Drawing.Size(0, 0)
            '
            'EmptySpaceItem2
            '
            Me.EmptySpaceItem2.CustomizationFormText = "EmptySpaceItem2"
            Me.EmptySpaceItem2.Location = New System.Drawing.Point(0, 170)
            Me.EmptySpaceItem2.Name = "EmptySpaceItem2"
            Me.EmptySpaceItem2.Size = New System.Drawing.Size(61, 27)
            Me.EmptySpaceItem2.Text = "EmptySpaceItem2"
            Me.EmptySpaceItem2.TextSize = New System.Drawing.Size(0, 0)
            '
            'EmptySpaceItem3
            '
            Me.EmptySpaceItem3.CustomizationFormText = "EmptySpaceItem3"
            Me.EmptySpaceItem3.Location = New System.Drawing.Point(256, 170)
            Me.EmptySpaceItem3.Name = "EmptySpaceItem3"
            Me.EmptySpaceItem3.Size = New System.Drawing.Size(56, 27)
            Me.EmptySpaceItem3.Text = "EmptySpaceItem3"
            Me.EmptySpaceItem3.TextSize = New System.Drawing.Size(0, 0)
            '
            'EmptySpaceItem4
            '
            Me.EmptySpaceItem4.CustomizationFormText = "EmptySpaceItem4"
            Me.EmptySpaceItem4.Location = New System.Drawing.Point(140, 170)
            Me.EmptySpaceItem4.Name = "EmptySpaceItem4"
            Me.EmptySpaceItem4.Size = New System.Drawing.Size(37, 27)
            Me.EmptySpaceItem4.Text = "EmptySpaceItem4"
            Me.EmptySpaceItem4.TextSize = New System.Drawing.Size(0, 0)
            '
            'EmptySpaceItem5
            '
            Me.EmptySpaceItem5.CustomizationFormText = "EmptySpaceItem5"
            Me.EmptySpaceItem5.Location = New System.Drawing.Point(0, 197)
            Me.EmptySpaceItem5.Name = "EmptySpaceItem5"
            Me.EmptySpaceItem5.Size = New System.Drawing.Size(312, 19)
            Me.EmptySpaceItem5.Text = "EmptySpaceItem5"
            Me.EmptySpaceItem5.TextSize = New System.Drawing.Size(0, 0)
            '
            'EmptySpaceItem6
            '
            Me.EmptySpaceItem6.CustomizationFormText = "EmptySpaceItem6"
            Me.EmptySpaceItem6.Location = New System.Drawing.Point(193, 24)
            Me.EmptySpaceItem6.Name = "EmptySpaceItem6"
            Me.EmptySpaceItem6.Size = New System.Drawing.Size(119, 24)
            Me.EmptySpaceItem6.Text = "EmptySpaceItem6"
            Me.EmptySpaceItem6.TextSize = New System.Drawing.Size(0, 0)
            '
            'EmptySpaceItem7
            '
            Me.EmptySpaceItem7.CustomizationFormText = "EmptySpaceItem7"
            Me.EmptySpaceItem7.Location = New System.Drawing.Point(193, 48)
            Me.EmptySpaceItem7.Name = "EmptySpaceItem7"
            Me.EmptySpaceItem7.Size = New System.Drawing.Size(119, 24)
            Me.EmptySpaceItem7.Text = "EmptySpaceItem7"
            Me.EmptySpaceItem7.TextSize = New System.Drawing.Size(0, 0)
            '
            'EmptySpaceItem8
            '
            Me.EmptySpaceItem8.CustomizationFormText = "EmptySpaceItem8"
            Me.EmptySpaceItem8.Location = New System.Drawing.Point(193, 72)
            Me.EmptySpaceItem8.Name = "EmptySpaceItem8"
            Me.EmptySpaceItem8.Size = New System.Drawing.Size(119, 24)
            Me.EmptySpaceItem8.Text = "EmptySpaceItem8"
            Me.EmptySpaceItem8.TextSize = New System.Drawing.Size(0, 0)
            '
            'EmptySpaceItem9
            '
            Me.EmptySpaceItem9.CustomizationFormText = "EmptySpaceItem9"
            Me.EmptySpaceItem9.Location = New System.Drawing.Point(193, 96)
            Me.EmptySpaceItem9.Name = "EmptySpaceItem9"
            Me.EmptySpaceItem9.Size = New System.Drawing.Size(119, 24)
            Me.EmptySpaceItem9.Text = "EmptySpaceItem9"
            Me.EmptySpaceItem9.TextSize = New System.Drawing.Size(0, 0)
            '
            'EmptySpaceItem10
            '
            Me.EmptySpaceItem10.CustomizationFormText = "EmptySpaceItem10"
            Me.EmptySpaceItem10.Location = New System.Drawing.Point(193, 120)
            Me.EmptySpaceItem10.Name = "EmptySpaceItem10"
            Me.EmptySpaceItem10.Size = New System.Drawing.Size(119, 24)
            Me.EmptySpaceItem10.Text = "EmptySpaceItem10"
            Me.EmptySpaceItem10.TextSize = New System.Drawing.Size(0, 0)
            '
            'LetterListForm
            '
            Me.AcceptButton = Me.SimpleButton_OK
            Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
            Me.CancelButton = Me.SimpleButton_Cancel
            Me.ClientSize = New System.Drawing.Size(332, 236)
            Me.Controls.Add(Me.LayoutControl1)
            Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
            Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
            Me.MaximizeBox = False
            Me.MinimizeBox = False
            Me.Name = "LetterListForm"
            Me.Text = "Letter Selection and Parameters"
            CType(Me.DxErrorProvider1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControl1, System.ComponentModel.ISupportInitialize).EndInit()
            Me.LayoutControl1.ResumeLayout(False)
            CType(Me.TextEdit1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.CreditorID1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.ClientID1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.CalcEdit1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LookUpEdit1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.DateEdit1.Properties.VistaTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.DateEdit1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlGroup1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem2, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem3, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem5, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem6, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem7, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem8, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem4, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.EmptySpaceItem1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.EmptySpaceItem2, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.EmptySpaceItem3, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.EmptySpaceItem4, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.EmptySpaceItem5, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.EmptySpaceItem6, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.EmptySpaceItem7, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.EmptySpaceItem8, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.EmptySpaceItem9, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.EmptySpaceItem10, System.ComponentModel.ISupportInitialize).EndInit()
            Me.ResumeLayout(False)

        End Sub

        'NOTE: The following procedure is required by the Windows Form Designer
        'It can be modified using the Windows Form Designer.  
        'Do not modify it using the code editor.
        Friend WithEvents SimpleButton_OK As DevExpress.XtraEditors.SimpleButton
        Friend WithEvents SimpleButton_Cancel As DevExpress.XtraEditors.SimpleButton
        Friend WithEvents DateEdit1 As DevExpress.XtraEditors.DateEdit
        Friend WithEvents CalcEdit1 As DevExpress.XtraEditors.CalcEdit
        Friend WithEvents LookUpEdit1 As DevExpress.XtraEditors.LookUpEdit
        Friend WithEvents LayoutControl1 As DevExpress.XtraLayout.LayoutControl
        Friend WithEvents TextEdit1 As DevExpress.XtraEditors.TextEdit
        Friend WithEvents CreditorID1 As CreditorID
        Friend WithEvents ClientID1 As DebtPlus.UI.Client.Widgets.Controls.ClientID
        Friend WithEvents LayoutControlGroup1 As DevExpress.XtraLayout.LayoutControlGroup
        Friend WithEvents LayoutControlItem1 As DevExpress.XtraLayout.LayoutControlItem
        Friend WithEvents LayoutControlItem2 As DevExpress.XtraLayout.LayoutControlItem
        Friend WithEvents LayoutControlItem3 As DevExpress.XtraLayout.LayoutControlItem
        Friend WithEvents LayoutControlItem5 As DevExpress.XtraLayout.LayoutControlItem
        Friend WithEvents LayoutControlItem6 As DevExpress.XtraLayout.LayoutControlItem
        Friend WithEvents LayoutControlItem7 As DevExpress.XtraLayout.LayoutControlItem
        Friend WithEvents LayoutControlItem8 As DevExpress.XtraLayout.LayoutControlItem
        Friend WithEvents LayoutControlItem4 As DevExpress.XtraLayout.LayoutControlItem
        Friend WithEvents EmptySpaceItem1 As DevExpress.XtraLayout.EmptySpaceItem
        Friend WithEvents EmptySpaceItem2 As DevExpress.XtraLayout.EmptySpaceItem
        Friend WithEvents EmptySpaceItem3 As DevExpress.XtraLayout.EmptySpaceItem
        Friend WithEvents EmptySpaceItem4 As DevExpress.XtraLayout.EmptySpaceItem
        Friend WithEvents EmptySpaceItem5 As DevExpress.XtraLayout.EmptySpaceItem
        Friend WithEvents EmptySpaceItem6 As DevExpress.XtraLayout.EmptySpaceItem
        Friend WithEvents EmptySpaceItem7 As DevExpress.XtraLayout.EmptySpaceItem
        Friend WithEvents EmptySpaceItem8 As DevExpress.XtraLayout.EmptySpaceItem
        Friend WithEvents EmptySpaceItem9 As DevExpress.XtraLayout.EmptySpaceItem
        Friend WithEvents EmptySpaceItem10 As DevExpress.XtraLayout.EmptySpaceItem
    End Class
End Namespace
