Option Compare Binary
Option Explicit On 
Option Strict On

Namespace Letters.Test
    Friend Class ArgsParser
        Inherits DebtPlus.Utils.ArgParserBase

        Public Sub New()
            MyBase.New(New String() {"d", "p"})
        End Sub

        Protected Overrides Sub OnUsage(ByVal errorInfo As String)
            ' deprecated
        End Sub

        Protected Overrides Function OnDoneParse() As SwitchStatus
            Dim ss As SwitchStatus = DebtPlus.Utils.ArgParserBase.SwitchStatus.NoError
            Return ss
        End Function
    End Class
End Namespace