﻿Namespace Letters.Print
    <Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
    Partial Class MainForm
        Inherits DebtPlus.Data.Forms.DebtPlusForm

        'Form overrides dispose to clean up the component list.
        <System.Diagnostics.DebuggerNonUserCode()> _
        Protected Overrides Sub Dispose(ByVal disposing As Boolean)
            Try
                If disposing AndAlso components IsNot Nothing Then
                    components.Dispose()
                End If
            Finally
                MyBase.Dispose(disposing)
            End Try
        End Sub

        'Required by the Windows Form Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Windows Form Designer
        'It can be modified using the Windows Form Designer.  
        'Do not modify it using the code editor.
        <System.Diagnostics.DebuggerStepThrough()> _
        Private Sub InitializeComponent()
            Me.GridControl1 = New DevExpress.XtraGrid.GridControl
            Me.GridView1 = New DevExpress.XtraGrid.Views.Grid.GridView
            Me.GridColumn_selected = New DevExpress.XtraGrid.Columns.GridColumn
            Me.GridColumn_selected.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True
            Me.CheckEdit = New DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit
            Me.GridColumn_description = New DevExpress.XtraGrid.Columns.GridColumn
            Me.GridColumn_description.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True
            Me.GridColumnStatus = New DevExpress.XtraGrid.Columns.GridColumn
            Me.GridColumnStatus.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True
            Me.GridColumn3 = New DevExpress.XtraGrid.Columns.GridColumn
            Me.GridColumn3.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True
            Me.GridColumn_letter_type = New DevExpress.XtraGrid.Columns.GridColumn
            Me.GridColumn_letter_type.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True
            Me.Button_Print = New DevExpress.XtraEditors.SimpleButton
            Me.Button_SelectAll = New DevExpress.XtraEditors.SimpleButton
            Me.Button_ClearAll = New DevExpress.XtraEditors.SimpleButton
            Me.Button_Cancel = New DevExpress.XtraEditors.SimpleButton
            CType(Me.DxErrorProvider1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.GridView1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.CheckEdit, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.SuspendLayout()
            '
            'GridControl1
            '
            Me.GridControl1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                        Or System.Windows.Forms.AnchorStyles.Left) _
                        Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.GridControl1.Location = New System.Drawing.Point(0, 0)
            Me.GridControl1.MainView = Me.GridView1
            Me.GridControl1.Name = "GridControl1"
            Me.GridControl1.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.CheckEdit})
            Me.GridControl1.Size = New System.Drawing.Size(457, 267)
            Me.GridControl1.TabIndex = 0
            Me.GridControl1.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridView1})
            '
            'GridView1
            '
            Me.GridView1.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.GridColumn_selected, Me.GridColumn_description, Me.GridColumnStatus, Me.GridColumn3, Me.GridColumn_letter_type})
            Me.GridView1.GridControl = Me.GridControl1
            Me.GridView1.Name = "GridView1"
            Me.GridView1.OptionsView.ShowGroupPanel = False
            Me.GridView1.OptionsView.ShowIndicator = False
            '
            'GridColumn_selected
            '
            Me.GridColumn_selected.Caption = "selected"
            Me.GridColumn_selected.ColumnEdit = Me.CheckEdit
            Me.GridColumn_selected.CustomizationCaption = "Checked"
            Me.GridColumn_selected.FieldName = "selected"
            Me.GridColumn_selected.Name = "GridColumn_selected"
            Me.GridColumn_selected.OptionsColumn.AllowMove = False
            Me.GridColumn_selected.OptionsColumn.AllowSize = False
            Me.GridColumn_selected.OptionsColumn.ShowCaption = False
            Me.GridColumn_selected.OptionsColumn.ShowInCustomizationForm = False
            Me.GridColumn_selected.Visible = True
            Me.GridColumn_selected.VisibleIndex = 0
            Me.GridColumn_selected.Width = 20
            '
            'CheckEdit
            '
            Me.CheckEdit.AutoHeight = False
            Me.CheckEdit.Name = "CheckEdit"
            '
            'GridColumn_description
            '
            Me.GridColumn_description.Caption = "Description"
            Me.GridColumn_description.CustomizationCaption = "Description"
            Me.GridColumn_description.FieldName = "description"
            Me.GridColumn_description.Name = "GridColumn_description"
            Me.GridColumn_description.OptionsColumn.AllowEdit = False
            Me.GridColumn_description.Visible = True
            Me.GridColumn_description.VisibleIndex = 1
            Me.GridColumn_description.Width = 305
            '
            'GridColumnStatus
            '
            Me.GridColumnStatus.Caption = "Status"
            Me.GridColumnStatus.FieldName = "status"
            Me.GridColumnStatus.Name = "GridColumnStatus"
            Me.GridColumnStatus.Visible = True
            Me.GridColumnStatus.VisibleIndex = 2
            Me.GridColumnStatus.Width = 70
            '
            'GridColumn3
            '
            Me.GridColumn3.AppearanceCell.Options.UseTextOptions = True
            Me.GridColumn3.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.GridColumn3.AppearanceHeader.Options.UseTextOptions = True
            Me.GridColumn3.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.GridColumn3.Caption = "Count"
            Me.GridColumn3.CustomizationCaption = "Count of letters"
            Me.GridColumn3.DisplayFormat.FormatString = "n0"
            Me.GridColumn3.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.GridColumn3.FieldName = "count"
            Me.GridColumn3.GroupFormat.FormatString = "n0"
            Me.GridColumn3.GroupFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.GridColumn3.Name = "GridColumn3"
            Me.GridColumn3.OptionsColumn.AllowEdit = False
            Me.GridColumn3.Visible = True
            Me.GridColumn3.VisibleIndex = 3
            Me.GridColumn3.Width = 58
            '
            'GridColumn_letter_type
            '
            Me.GridColumn_letter_type.AppearanceCell.Options.UseTextOptions = True
            Me.GridColumn_letter_type.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.GridColumn_letter_type.AppearanceHeader.Options.UseTextOptions = True
            Me.GridColumn_letter_type.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.GridColumn_letter_type.Caption = "Type"
            Me.GridColumn_letter_type.CustomizationCaption = "Letter Type"
            Me.GridColumn_letter_type.DisplayFormat.FormatString = "f0"
            Me.GridColumn_letter_type.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.GridColumn_letter_type.FieldName = "letter_type"
            Me.GridColumn_letter_type.Name = "GridColumn_letter_type"
            Me.GridColumn_letter_type.OptionsColumn.AllowEdit = False
            '
            'Button_Print
            '
            Me.Button_Print.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.Button_Print.Enabled = False
            Me.Button_Print.Location = New System.Drawing.Point(463, 12)
            Me.Button_Print.Name = "Button_Print"
            Me.Button_Print.Size = New System.Drawing.Size(75, 23)
            Me.Button_Print.TabIndex = 1
            Me.Button_Print.Text = "Print..."
            '
            'Button_SelectAll
            '
            Me.Button_SelectAll.Anchor = System.Windows.Forms.AnchorStyles.Right
            Me.Button_SelectAll.Location = New System.Drawing.Point(463, 101)
            Me.Button_SelectAll.Name = "Button_SelectAll"
            Me.Button_SelectAll.Size = New System.Drawing.Size(75, 23)
            Me.Button_SelectAll.TabIndex = 2
            Me.Button_SelectAll.Text = "Select All"
            '
            'Button_ClearAll
            '
            Me.Button_ClearAll.Anchor = System.Windows.Forms.AnchorStyles.Right
            Me.Button_ClearAll.Location = New System.Drawing.Point(463, 130)
            Me.Button_ClearAll.Name = "Button_ClearAll"
            Me.Button_ClearAll.Size = New System.Drawing.Size(75, 23)
            Me.Button_ClearAll.TabIndex = 3
            Me.Button_ClearAll.Text = "Clear All"
            '
            'Button_Cancel
            '
            Me.Button_Cancel.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.Button_Cancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
            Me.Button_Cancel.Location = New System.Drawing.Point(463, 231)
            Me.Button_Cancel.Name = "Button_Cancel"
            Me.Button_Cancel.Size = New System.Drawing.Size(75, 23)
            Me.Button_Cancel.TabIndex = 4
            Me.Button_Cancel.Text = "Quit"
            '
            'MainForm
            '
            Me.AcceptButton = Me.Button_Print
            Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
            Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
            Me.CancelButton = Me.Button_Cancel
            Me.ClientSize = New System.Drawing.Size(550, 266)
            Me.Controls.Add(Me.Button_Cancel)
            Me.Controls.Add(Me.Button_ClearAll)
            Me.Controls.Add(Me.Button_SelectAll)
            Me.Controls.Add(Me.Button_Print)
            Me.Controls.Add(Me.GridControl1)
            Me.Name = "MainForm"
            Me.Text = "Print Queued Letters"
            CType(Me.DxErrorProvider1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.GridView1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.CheckEdit, System.ComponentModel.ISupportInitialize).EndInit()
            Me.ResumeLayout(False)

        End Sub
        Friend WithEvents GridControl1 As DevExpress.XtraGrid.GridControl
        Friend WithEvents GridView1 As DevExpress.XtraGrid.Views.Grid.GridView
        Friend WithEvents Button_Print As DevExpress.XtraEditors.SimpleButton
        Friend WithEvents Button_SelectAll As DevExpress.XtraEditors.SimpleButton
        Friend WithEvents Button_ClearAll As DevExpress.XtraEditors.SimpleButton
        Friend WithEvents Button_Cancel As DevExpress.XtraEditors.SimpleButton
        Friend WithEvents GridColumn_selected As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents CheckEdit As DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit
        Friend WithEvents GridColumn_description As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents GridColumn3 As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents GridColumn_letter_type As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents GridColumnStatus As DevExpress.XtraGrid.Columns.GridColumn
    End Class
End Namespace
