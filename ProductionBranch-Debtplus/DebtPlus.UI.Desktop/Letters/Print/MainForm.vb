#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}
#End Region
Imports DebtPlus.Utils
Imports System.Data.SqlClient
Imports System.Windows.Forms

Namespace Letters.Print
    Friend Class MainForm

        Public Sub New()
            MyBase.New()
            InitializeComponent()
        End Sub

        ''' <summary>
        ''' Create an instance of our class
        ''' </summary>
        Public Sub New(ByVal ap As ArgParser)
            MyClass.New()
            RegisterHandlers()
        End Sub

        Private Sub RegisterHandlers()
            AddHandler Load, AddressOf MainForm_Load
            AddHandler Button_Cancel.Click, AddressOf Button_Cancel_Click
            AddHandler Button_ClearAll.Click, AddressOf Button_ClearAll_Click
            AddHandler Button_SelectAll.Click, AddressOf Button_SelectAll_Click
            AddHandler Button_Print.Click, AddressOf Button_Print_Click
            AddHandler CheckEdit.EditValueChanging, AddressOf CheckEdit_EditValueChanging
        End Sub

        Private Sub UnRegisterHandlers()
            RemoveHandler Load, AddressOf MainForm_Load
            RemoveHandler Button_Cancel.Click, AddressOf Button_Cancel_Click
            RemoveHandler Button_ClearAll.Click, AddressOf Button_ClearAll_Click
            RemoveHandler Button_SelectAll.Click, AddressOf Button_SelectAll_Click
            RemoveHandler Button_Print.Click, AddressOf Button_Print_Click
            RemoveHandler CheckEdit.EditValueChanging, AddressOf CheckEdit_EditValueChanging
        End Sub

        ''' <summary>
        ''' Process the close request for the form.
        ''' </summary>
        Private Class PrinterManager
            Private Declare Auto Function GetDefaultPrinter Lib "winspool.drv" (ByVal pszBuffer As String, ByRef pcchBuffer As Int32) As Boolean
            Private Declare Auto Function SetDefaultPrinter_API Lib "winspool.drv" Alias "SetDefaultPrinter" (ByVal pszPrinter As String) As Boolean

            <System.Security.SecuritySafeCritical()> _
            Private Shared Function DoGetDefaultPrinter(ByVal pszBuffer As String, ByRef ppchBuffer As Int32) As Boolean
                Return GetDefaultPrinter(pszBuffer, ppchBuffer)
            End Function

            Private Const ERROR_FILE_NOT_FOUND As Int32 = 2
            Private Const ERROR_INSUFFICIENT_BUFFER As Int32 = 122

            <System.Security.SecuritySafeCritical()> _
            Private Shared Function DoSetDefaultPrinter_API(ByVal pszBuffer As String) As Boolean
                Return SetDefaultPrinter_API(pszBuffer)
            End Function

            ''' <summary>
            ''' Process the close request for the form.
            ''' </summary>
            Public Shared Sub SetDefaultPrinter(ByVal PrinterName As String)
                If Not DoSetDefaultPrinter_API(PrinterName) Then
                    Throw New System.ComponentModel.Win32Exception
                End If
            End Sub

            ''' <summary>
            ''' Process the close request for the form.
            ''' </summary>
            Public Shared Function GetDefaultPrinter() As String
                Dim n As Int32 = 128

                Dim s As New String(" "c, n)
                If DoGetDefaultPrinter(s, n) Then
                    Return Microsoft.VisualBasic.Strings.Left(s, n - 1)   ' toss the trailing null character
                End If

                Dim LastError As Int32 = System.Runtime.InteropServices.Marshal.GetLastWin32Error()
                If LastError = ERROR_FILE_NOT_FOUND Then
                    Throw New System.ComponentModel.Win32Exception(LastError, "There is no default printer.")
                End If

                If LastError <> ERROR_INSUFFICIENT_BUFFER Then
                    Throw New System.ComponentModel.Win32Exception
                End If

                s = New String(" "c, n)
                If DoGetDefaultPrinter(s, n) Then
                    Return Microsoft.VisualBasic.Strings.Left(s, n - 1)
                Else
                    Throw New System.ComponentModel.Win32Exception
                End If
            End Function
        End Class

        Friend ds As New DataSet("ds")
        Friend vue As DataView

        ''' <summary>
        ''' Reload the letter queue information
        ''' </summary>
        Dim WithEvents MainTable As DataTable
        Private Sub RefreshData()
            Dim current_cursor As Cursor = Cursor.Current

            ' Disable the other buttons until we find that we have something to do
            Button_ClearAll.Enabled = False
            Button_SelectAll.Enabled = False

            Try
                Cursor.Current = Cursors.WaitCursor

                Using cmd As SqlClient.SqlCommand = New SqlCommand
                    cmd.Connection = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
                    cmd.CommandText = "SELECT convert(bit,0) as selected, letter_type, queue_name as 'description', count(*) AS 'count', convert(varchar(80),'Pending') as 'status' FROM letter_queue WITH (NOLOCK) WHERE date_printed IS NULL GROUP BY letter_type, queue_name"
                    cmd.CommandType = CommandType.Text

                    Using da As New SqlClient.SqlDataAdapter(cmd)
                        da.Fill(ds, "letter_queue")
                        MainTable = ds.Tables("letter_queue")
                    End Using
                End Using

                GridControl1.DataSource = MainTable.DefaultView
                GridControl1.RefreshDataSource()

            Catch ex As SqlClient.SqlException
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error reading letter queue items")

            Finally
                Cursor.Current = current_cursor
            End Try

            ' Enable the select buttons if there are items
            If MainTable IsNot Nothing Then
                If MainTable.Rows.Count > 0 Then
                    Button_ClearAll.Enabled = True
                    Button_SelectAll.Enabled = True
                End If
            End If

            Button_Print.Enabled = False
        End Sub

        ''' <summary>
        ''' Process the form load event
        ''' </summary>
        Private Sub MainForm_Load(ByVal sender As Object, ByVal e As EventArgs)
            RefreshData()
        End Sub

        ''' <summary>
        ''' When QUIT is clicked, close the application down
        ''' </summary>
        Private Sub Button_Cancel_Click(ByVal sender As Object, ByVal e As EventArgs)
            Close()
        End Sub

        ''' <summary>
        ''' Process the CLEAR ALL button
        ''' </summary>
        Private Sub Button_ClearAll_Click(ByVal sender As Object, ByVal e As EventArgs)
            If MainTable IsNot Nothing Then
                For Each row As DataRow In MainTable.Rows
                    row.BeginEdit()
                    row("selected") = 0
                    row.EndEdit()
                Next
            End If
            Button_Print.Enabled = False
        End Sub

        ''' <summary>
        ''' Process the SELECT ALL button
        ''' </summary>
        Private Sub Button_SelectAll_Click(ByVal sender As Object, ByVal e As EventArgs)
            Dim Enabled As Boolean = False
            If MainTable IsNot Nothing Then
                For Each row As DataRow In MainTable.Rows
                    row.BeginEdit()
                    row("selected") = 1
                    row.EndEdit()
                    Enabled = True
                Next
            End If
            Button_Print.Enabled = Enabled
        End Sub

        ''' <summary>
        ''' Process the PRINT button
        ''' </summary>
        Private PrintThread As System.Threading.Thread

        Private Sub Button_Print_Click(ByVal sender As Object, ByVal e As EventArgs)
            Button_Print.Enabled = False

            PrintThread = New System.Threading.Thread(AddressOf PrintThreadTopHandler)
            PrintThread.SetApartmentState(Threading.ApartmentState.STA)
            PrintThread.Name = "Print Thread"
            PrintThread.IsBackground = False
            PrintThread.Start()
        End Sub

        Private Delegate Sub PrintThreadCompletionDelegate()
        Private Sub PrintThreadCompletion()
            If InvokeRequired() Then
                Dim ia As IAsyncResult = BeginInvoke(New PrintThreadCompletionDelegate(AddressOf PrintThreadCompletion))
                EndInvoke(ia)
            Else
                PrintThread = Nothing
            End If
        End Sub

        Private Sub PrintThreadTopHandler()
            Try
                PrintThreadHandler()
            Finally
                PrintThreadCompletion()
            End Try
        End Sub

        Private Sub PrintThreadHandler()
            Dim PrinterName As String

            ' Load the default printer into the printing component
            Dim PrintSettings As New System.Drawing.Printing.PrinterSettings()
            PrintSettings.PrinterName = PrinterManager.GetDefaultPrinter
            PrintSettings.PrintToFile = False
            PrintSettings.PrintRange = System.Drawing.Printing.PrintRange.AllPages
            PrintSettings.Copies = 1
            PrintSettings.Duplex = System.Drawing.Printing.Duplex.Default
            PrintSettings.FromPage = 1
            PrintSettings.ToPage = PrintSettings.MaximumPage

            ' Allow the user to change the printer location
            Using frm As New PrintDialog
                frm.PrinterSettings = PrintSettings
                frm.AllowCurrentPage = False
                frm.AllowPrintToFile = False
                frm.AllowSelection = False
                frm.PrintToFile = False
                frm.UseEXDialog = True

                If frm.ShowDialog() = Windows.Forms.DialogResult.Cancel Then Return
                PrinterName = frm.PrinterSettings.PrinterName
            End Using

            Dim vue As DataView = CType(GridControl1.DataSource, DataView)

            For Each row As DataRow In vue.Table.Select("[selected]<>false", String.Empty)
                row.BeginEdit()
                row("status") = "PRINTING"
                row.EndEdit()
            Next

            ' Process the letter types
            Using SelectVue As New DataView(vue.Table, "[selected]<>0", "letter_type", DataViewRowState.CurrentRows)

                For Each drv As DataRowView In SelectVue
                    PrintLetterType(Convert.ToInt32(drv("letter_type")), PrinterName)

                    drv.Row.BeginEdit()
                    drv.Row("selected") = False
                    drv.Row("status") = "PRINTED"
                    drv.Row.EndEdit()
                Next
            End Using
        End Sub

        ''' <summary>
        ''' Print a specific letter type
        ''' </summary>
        Private Sub PrintLetterType(ByVal LetterType As Int32, ByVal PrinterName As String)
            Dim StatusForm As New DevExpress.Utils.WaitDialogForm("Printing Letters")
            StatusForm.Show()
            Try
                Using dsLetterType As New DataSet("dsLetterType")

                    StatusForm.Caption = "Reading letter text"
                    Application.DoEvents()

                    ' For the letter type, read the pending letter text from the database
                    Using cmd As SqlClient.SqlCommand = New SqlCommand
                        cmd.Connection = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
                        cmd.CommandText = "SELECT letter_queue, isnull(queue_name,'') as queue_name, date_printed, isnull(top_margin, 0.10) as top_margin, isnull(left_margin,0.10) as left_margin, isnull(bottom_margin,0.10) as bottom_margin, isnull(right_margin,0.10) as right_margin, isnull(priority,0) as priority, isnull(sort_order,'ZZZZZ') as sort_order, formatted_letter FROM letter_queue WITH (NOLOCK) WHERE letter_type=@letter_type AND date_printed IS NULL"
                        cmd.CommandType = CommandType.Text
                        cmd.CommandTimeout = 0
                        cmd.Parameters.Add("@letter_type", SqlDbType.Int).Value = LetterType

                        Using da As New SqlClient.SqlDataAdapter(cmd)
                            da.AcceptChangesDuringFill = True
                            da.FillLoadOption = LoadOption.OverwriteChanges
                            da.Fill(dsLetterType, "letter_queue_items")
                        End Using
                    End Using

                    StatusForm.Caption = "Imaging Letters"
                    Application.DoEvents()

                    ' Create the report for the letters
                    Using LetterView As New DataView(dsLetterType.Tables("letter_queue_items"), String.Empty, "sort_order", DataViewRowState.CurrentRows)
                        If LetterView.Count > 0 Then
                            Using ps As New DevExpress.XtraPrinting.PrintingSystem

                                For Each drv As DataRowView In LetterView
                                    ps.Pages.AddRange(GetDocumentLink(drv).PrintingSystem.Pages)

                                    ' Mark the item as having been printed
                                    drv.BeginEdit()
                                    drv("date_printed") = Now
                                    drv.EndEdit()
                                Next

                                ' Set the name of the document for the spooler and suppress the warnings.
                                ps.Document.Name = DebtPlus.Utils.Nulls.DStr(LetterView(0)("queue_name"))
                                ps.ShowMarginsWarning = False
                                ps.ShowPrintStatusDialog = False

                                StatusForm.Caption = "Printing Letters"
                                Application.DoEvents()
                                ps.Print(PrinterName)
                            End Using
                        End If
                    End Using

                    StatusForm.Caption = "Updating print status"
                    Application.DoEvents()

                    ' Attempt to update the database with the changed information
                    Using cmd As SqlClient.SqlCommand = New SqlCommand
                        cmd.Connection = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
                        cmd.CommandText = "xpr_letter_mark_printed"
                        cmd.CommandType = CommandType.StoredProcedure
                        cmd.Parameters.Add("@letter_queue", SqlDbType.Int, 0, "letter_queue")

                        ' Do the update operation
                        Using da As New SqlClient.SqlDataAdapter()
                            da.UpdateCommand = cmd
                            Dim RowsUpdated As Int32 = da.Update(dsLetterType.Tables("letter_queue_items"))
                        End Using
                    End Using
                End Using

            Catch ex As System.Data.SqlClient.SqlException
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error reading letters to print")

            Finally
                If StatusForm IsNot Nothing Then
                    StatusForm.Close()
                    StatusForm.Dispose()
                End If
            End Try
        End Sub

        ''' <summary>
        ''' Get the document link for the new letter template object
        ''' </summary>
        Private Shared Function GetDocumentLink(ByVal drv As DataRowView) As DevExpress.XtraPrinting.PrintableComponentLink
            Dim FormattedLetterText As String = Convert.ToString(drv("formatted_letter"))
            Dim ab As Byte() = (New System.Text.ASCIIEncoding).GetBytes(FormattedLetterText)

            ' Create a document that represents the letter image
            Dim LetterServer As New DevExpress.XtraRichEdit.RichEditDocumentServer()
            Using stm As New System.IO.MemoryStream(ab)
                LetterServer.LoadDocument(stm, DevExpress.XtraRichEdit.DocumentFormat.Rtf)
            End Using

            ' Create a Component link to include the document
            Dim lk1 As New DevExpress.XtraPrinting.PrintableComponentLink(New DevExpress.XtraPrinting.PrintingSystem()) With {.Component = LetterServer}

            ' Create the document object and return the linkage to it.
            lk1.CreateDocument()
            Return lk1
        End Function

        ''' <summary>
        ''' Handle the select item being changed
        ''' </summary>
        Private Sub CheckEdit_EditValueChanging(ByVal sender As Object, ByVal e As DevExpress.XtraEditors.Controls.ChangingEventArgs)

            ' If we are setting the item then enable the print button
            If Convert.ToInt32(e.NewValue) <> 0 Then
                Button_Print.Enabled = True
                Return
            End If

            ' The item is being reset. There must be at least TWO selected to enable the print mode
            Dim SelectCount As Int32 = 0
            For Each row As DataRow In MainTable.Rows
                If Convert.ToInt32(row("selected")) <> 0 Then
                    SelectCount += 1
                End If
            Next

            ' We need at least two since we are clearing one of them
            Button_Print.Enabled = (SelectCount > 1)
        End Sub
    End Class
End Namespace
