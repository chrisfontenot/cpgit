Option Compare Binary
Option Explicit On
Option Strict On

Imports System.Reflection
Imports System.Windows.Forms

Namespace Reports.Print
    Friend Class ArgsParser
        Inherits DebtPlus.Utils.ArgParserBase

        Private ReadOnly _params As New ArrayList()
        Public ReadOnly Property Params() As ArrayList
            Get
                Return _params
            End Get
        End Property

        Public Property Report() As String

        Private ReadOnly _conf As Specialized.NameValueCollection = DebtPlus.Configuration.Config.GetConfigSettings(DebtPlus.Configuration.Config.ConfigSet.ReportsPrint)
        Public ReadOnly Property Config() As Specialized.NameValueCollection
            Get
                Return _conf
            End Get
        End Property

        Public Sub New()
            MyBase.New(New String() {"p"})
        End Sub

        Protected Overrides Function OnSwitch(ByVal switchName As String, ByVal switchValue As String) As SwitchStatus
            Dim ss As SwitchStatus = DebtPlus.Utils.ArgParserBase.SwitchStatus.NoError

            Select Case switchName
                Case "p"
                    Dim paramValue As String = switchValue.Trim()
                    If paramValue <> String.Empty Then
                        Dim splitValues() As String = paramValue.Split("="c)
                        If splitValues.GetUpperBound(0) = 1 AndAlso splitValues(0).Length > 0 AndAlso splitValues(1).Length > 0 Then
                            Params.Add(New Parameters(splitValues(0), splitValues(1)))
                            Exit Select
                        End If
                    End If
                    ss = DebtPlus.Utils.ArgParserBase.SwitchStatus.YesError

                Case "?"
                    ss = DebtPlus.Utils.ArgParserBase.SwitchStatus.ShowUsage

                Case Else
                    ss = DebtPlus.Utils.ArgParserBase.SwitchStatus.YesError
            End Select

            Return ss
        End Function

        Protected Overrides Function OnNonSwitch(ByVal switchValue As String) As SwitchStatus
            Dim ss As SwitchStatus = DebtPlus.Utils.ArgParserBase.SwitchStatus.NoError

            If Report <> String.Empty Then
                AppendErrorLine("Only one report may be specified on the command line")
                ss = DebtPlus.Utils.ArgParserBase.SwitchStatus.YesError
            Else
                Report = switchValue
            End If

            Return ss
        End Function

        Protected Overrides Sub OnUsage(ByVal errorInfo As String)
            'AppendErrorLine(String.Format("Usage: {0} [-o Filename] [-dDirectoryName ...] [-pNAME=VALUE ...] [-e] [report]", fname))
            'AppendErrorLine("       -o : Open the report file for editing")
            'AppendErrorLine("       -e : Edit the report definition")
            'AppendErrorLine("       -p : Specifiy a parameter for the report")
        End Sub

        Protected Overrides Function OnDoneParse() As SwitchStatus
            Dim ss As SwitchStatus = DebtPlus.Utils.ArgParserBase.SwitchStatus.NoError

            If Report = String.Empty AndAlso ss <> DebtPlus.Utils.ArgParserBase.SwitchStatus.Quit Then
                AppendErrorLine("A report file must be specified on the command line or entered on the dialog.")
                ss = DebtPlus.Utils.ArgParserBase.SwitchStatus.ShowUsage
            End If

            Return ss
        End Function
    End Class
End Namespace
