#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}

#End Region

Option Compare Binary
Option Explicit On
Option Strict On

Imports DebtPlus.Reports
Imports DebtPlus.Interfaces.Desktop
Imports System.Windows.Forms

Namespace Reports.Print
    Public Class Mainline
        Implements IDesktopMainline

        Public Sub OldAppMain(ByVal args() As String) Implements IDesktopMainline.OldAppMain
            Dim m_ap As New ArgsParser()
            Try
                If m_ap.Parse(args) Then
                    Dim rpt As DebtPlus.Interfaces.Reports.IReports = DebtPlus.Reports.ReportLoader.LoadReport(m_ap.Report)
                    If rpt Is Nothing Then
                        DebtPlus.Data.Forms.MessageBox.Show(String.Format("Unable to run the report at {0}", m_ap.Report), "Report Loading Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                    Else
                        rpt.AllowParameterChangesByUser = True
                        rpt.RunReportInSeparateThread()
                    End If
                End If

            Catch ex As Exception
                Using gdr As New Repository.GetDataResult()
                    gdr.HandleException(ex)
                    DebtPlus.UI.Common.ErrorHandling.HandleErrors(gdr, String.Format("Unable to run the report at {0}", m_ap.Report))
                End Using
            End Try
        End Sub
    End Class
End Namespace
