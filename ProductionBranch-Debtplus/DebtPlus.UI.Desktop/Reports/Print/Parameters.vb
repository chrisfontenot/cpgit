Option Compare Binary
Option Explicit On 
Option Strict On

Namespace Reports.Print
    Friend Class Parameters
        Public name As String
        Public value As String
        Public Sub New(ByVal name As String, ByVal value As String)
            Me.name = name
            Me.value = value
        End Sub
    End Class
End Namespace