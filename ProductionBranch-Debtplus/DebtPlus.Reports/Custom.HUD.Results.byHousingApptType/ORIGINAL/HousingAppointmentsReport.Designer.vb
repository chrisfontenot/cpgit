﻿Namespace Custom.HUD.Results.byHousingApptType
    <Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
    Partial Class HousingReport
        Inherits DebtPlus.Reports.Template.DatedTemplateXtraReportClass

        'XtraReport overrides dispose to clean up the component list.
        <System.Diagnostics.DebuggerNonUserCode()> _
        Protected Overrides Sub Dispose(ByVal disposing As Boolean)
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
            MyBase.Dispose(disposing)
        End Sub

        'Required by the Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Designer
        'It can be modified using the Designer.  
        'Do not modify it using the code editor.
        <System.Diagnostics.DebuggerStepThrough()> _
        Private Sub InitializeComponent()
            Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(HousingReport))
            Me.XrPanel1 = New DevExpress.XtraReports.UI.XRPanel()
            Me.XrLabel11 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel10 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel13 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel12 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel7 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel6 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel9 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel8 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel5 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel3 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel1 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel2 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_CLientName = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_ClientID = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_CounselorName = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_AppointmentType = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_StartTimeDT = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_InterviewType = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_InterviewDT = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_ResultType = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_ResultDT = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_TerminationType = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_TerminationDT = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_HousingDuration = New DevExpress.XtraReports.UI.XRLabel()
            CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
            '
            'PageHeader
            '
            Me.PageHeader.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrPanel1})
            Me.PageHeader.HeightF = 140.0!
            Me.PageHeader.Controls.SetChildIndex(Me.XrPanel_AgencyAddress, 0)
            Me.PageHeader.Controls.SetChildIndex(Me.XrLabel_Title, 0)
            Me.PageHeader.Controls.SetChildIndex(Me.XrLine_Title, 0)
            Me.PageHeader.Controls.SetChildIndex(Me.XrLabel_Subtitle, 0)
            Me.PageHeader.Controls.SetChildIndex(Me.XrPageInfo1, 0)
            Me.PageHeader.Controls.SetChildIndex(Me.XrPanel1, 0)
            '
            'XrLabel_Title
            '
            Me.XrLabel_Title.LocationFloat = New DevExpress.Utils.PointFloat(8.0!, 7.999992!)
            Me.XrLabel_Title.SizeF = New System.Drawing.SizeF(734.0!, 42.0!)
            Me.XrLabel_Title.StylePriority.UseFont = False
            '
            'XrPageInfo_PageNumber
            '
            Me.XrPageInfo_PageNumber.LocationFloat = New DevExpress.Utils.PointFloat(917.0!, 3.000005!)
            Me.XrPageInfo_PageNumber.StylePriority.UseTextAlignment = False
            '
            'XrPanel_AgencyAddress
            '
            Me.XrPanel_AgencyAddress.LocationFloat = New DevExpress.Utils.PointFloat(742.0!, 0.0!)
            '
            'XRLabel_Agency_Name
            '
            Me.XRLabel_Agency_Name.StylePriority.UseTextAlignment = False
            '
            'XrLine_Title
            '
            Me.XrLine_Title.SizeF = New System.Drawing.SizeF(734.0!, 8.0!)
            '
            'XrLabel_Agency_Address3
            '
            Me.XrLabel_Agency_Address3.StylePriority.UseFont = False
            Me.XrLabel_Agency_Address3.StylePriority.UseTextAlignment = False
            '
            'XrLabel_Agency_Address1
            '
            Me.XrLabel_Agency_Address1.StylePriority.UseFont = False
            Me.XrLabel_Agency_Address1.StylePriority.UseTextAlignment = False
            '
            'XrLabel_Agency_Phone
            '
            Me.XrLabel_Agency_Phone.StylePriority.UseFont = False
            Me.XrLabel_Agency_Phone.StylePriority.UseTextAlignment = False
            '
            'XrLabel_Agency_Address2
            '
            Me.XrLabel_Agency_Address2.StylePriority.UseFont = False
            Me.XrLabel_Agency_Address2.StylePriority.UseTextAlignment = False
            '
            'Detail
            '
            Me.Detail.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel_HousingDuration, Me.XrLabel_TerminationDT, Me.XrLabel_TerminationType, Me.XrLabel_ResultDT, Me.XrLabel_ResultType, Me.XrLabel_InterviewDT, Me.XrLabel_InterviewType, Me.XrLabel_StartTimeDT, Me.XrLabel_AppointmentType, Me.XrLabel_CounselorName, Me.XrLabel_ClientID, Me.XrLabel_CLientName})
            Me.Detail.HeightF = 15.0!
            '
            'XrPanel1
            '
            Me.XrPanel1.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel11, Me.XrLabel10, Me.XrLabel13, Me.XrLabel12, Me.XrLabel7, Me.XrLabel6, Me.XrLabel9, Me.XrLabel8, Me.XrLabel5, Me.XrLabel3, Me.XrLabel1, Me.XrLabel2})
            Me.XrPanel1.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 113.0!)
            Me.XrPanel1.Name = "XrPanel1"
            Me.XrPanel1.SizeF = New System.Drawing.SizeF(1050.0!, 17.0!)
            Me.XrPanel1.StyleName = "XrControlStyle_HeaderPannel"
            '
            'XrLabel11
            '
            Me.XrLabel11.LocationFloat = New DevExpress.Utils.PointFloat(721.9092!, 1.0!)
            Me.XrLabel11.Name = "XrLabel11"
            Me.XrLabel11.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel11.SizeF = New System.Drawing.SizeF(80.99994!, 14.99998!)
            Me.XrLabel11.StylePriority.UsePadding = False
            Me.XrLabel11.StylePriority.UseTextAlignment = False
            Me.XrLabel11.Text = "RESULT DT"
            Me.XrLabel11.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel10
            '
            Me.XrLabel10.LocationFloat = New DevExpress.Utils.PointFloat(804.2728!, 1.0!)
            Me.XrLabel10.Name = "XrLabel10"
            Me.XrLabel10.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel10.SizeF = New System.Drawing.SizeF(80.99994!, 14.99998!)
            Me.XrLabel10.StylePriority.UsePadding = False
            Me.XrLabel10.StylePriority.UseTextAlignment = False
            Me.XrLabel10.Text = "TERM TYP"
            Me.XrLabel10.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            '
            'XrLabel13
            '
            Me.XrLabel13.LocationFloat = New DevExpress.Utils.PointFloat(969.0001!, 0.9999911!)
            Me.XrLabel13.Name = "XrLabel13"
            Me.XrLabel13.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel13.SizeF = New System.Drawing.SizeF(80.99994!, 14.99998!)
            Me.XrLabel13.StylePriority.UsePadding = False
            Me.XrLabel13.StylePriority.UseTextAlignment = False
            Me.XrLabel13.Text = "MINS"
            Me.XrLabel13.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel12
            '
            Me.XrLabel12.LocationFloat = New DevExpress.Utils.PointFloat(886.6365!, 1.0!)
            Me.XrLabel12.Name = "XrLabel12"
            Me.XrLabel12.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel12.SizeF = New System.Drawing.SizeF(80.99994!, 14.99998!)
            Me.XrLabel12.StylePriority.UsePadding = False
            Me.XrLabel12.StylePriority.UseTextAlignment = False
            Me.XrLabel12.Text = "TERM DT"
            Me.XrLabel12.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel7
            '
            Me.XrLabel7.LocationFloat = New DevExpress.Utils.PointFloat(474.8183!, 1.0!)
            Me.XrLabel7.Name = "XrLabel7"
            Me.XrLabel7.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel7.SizeF = New System.Drawing.SizeF(80.99994!, 14.99998!)
            Me.XrLabel7.StylePriority.UsePadding = False
            Me.XrLabel7.StylePriority.UseTextAlignment = False
            Me.XrLabel7.Text = "IV TYP"
            Me.XrLabel7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            '
            'XrLabel6
            '
            Me.XrLabel6.LocationFloat = New DevExpress.Utils.PointFloat(392.4547!, 1.0!)
            Me.XrLabel6.Name = "XrLabel6"
            Me.XrLabel6.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel6.SizeF = New System.Drawing.SizeF(80.99994!, 14.99998!)
            Me.XrLabel6.StylePriority.UsePadding = False
            Me.XrLabel6.StylePriority.UseTextAlignment = False
            Me.XrLabel6.Text = "APT DT"
            Me.XrLabel6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel9
            '
            Me.XrLabel9.LocationFloat = New DevExpress.Utils.PointFloat(639.5455!, 1.0!)
            Me.XrLabel9.Name = "XrLabel9"
            Me.XrLabel9.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel9.SizeF = New System.Drawing.SizeF(80.99994!, 14.99998!)
            Me.XrLabel9.StylePriority.UsePadding = False
            Me.XrLabel9.StylePriority.UseTextAlignment = False
            Me.XrLabel9.Text = "RESULT"
            Me.XrLabel9.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            '
            'XrLabel8
            '
            Me.XrLabel8.LocationFloat = New DevExpress.Utils.PointFloat(557.1819!, 1.0!)
            Me.XrLabel8.Name = "XrLabel8"
            Me.XrLabel8.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel8.SizeF = New System.Drawing.SizeF(80.99994!, 14.99998!)
            Me.XrLabel8.StylePriority.UsePadding = False
            Me.XrLabel8.StylePriority.UseTextAlignment = False
            Me.XrLabel8.Text = "IV DDT"
            Me.XrLabel8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel5
            '
            Me.XrLabel5.LocationFloat = New DevExpress.Utils.PointFloat(227.7274!, 1.0!)
            Me.XrLabel5.Name = "XrLabel5"
            Me.XrLabel5.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel5.SizeF = New System.Drawing.SizeF(80.99994!, 14.99998!)
            Me.XrLabel5.StylePriority.UsePadding = False
            Me.XrLabel5.StylePriority.UseTextAlignment = False
            Me.XrLabel5.Text = "CNSLR"
            Me.XrLabel5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            '
            'XrLabel3
            '
            Me.XrLabel3.LocationFloat = New DevExpress.Utils.PointFloat(310.091!, 1.0!)
            Me.XrLabel3.Name = "XrLabel3"
            Me.XrLabel3.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel3.SizeF = New System.Drawing.SizeF(80.99994!, 15.0!)
            Me.XrLabel3.StylePriority.UsePadding = False
            Me.XrLabel3.StylePriority.UseTextAlignment = False
            Me.XrLabel3.Text = "APT TYP"
            Me.XrLabel3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            '
            'XrLabel1
            '
            Me.XrLabel1.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 1.000023!)
            Me.XrLabel1.Name = "XrLabel1"
            Me.XrLabel1.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel1.SizeF = New System.Drawing.SizeF(80.99994!, 14.99999!)
            Me.XrLabel1.StylePriority.UsePadding = False
            Me.XrLabel1.StylePriority.UseTextAlignment = False
            Me.XrLabel1.Text = "CLIENT"
            Me.XrLabel1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrLabel1.WordWrap = False
            '
            'XrLabel2
            '
            Me.XrLabel2.LocationFloat = New DevExpress.Utils.PointFloat(82.36363!, 1.0!)
            Me.XrLabel2.Name = "XrLabel2"
            Me.XrLabel2.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel2.SizeF = New System.Drawing.SizeF(144.0001!, 14.99999!)
            Me.XrLabel2.StylePriority.UsePadding = False
            Me.XrLabel2.StylePriority.UseTextAlignment = False
            Me.XrLabel2.Text = "NAME"
            Me.XrLabel2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            Me.XrLabel2.WordWrap = False
            '
            'XrLabel_CLientName
            '
            Me.XrLabel_CLientName.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "ClientName")})
            Me.XrLabel_CLientName.LocationFloat = New DevExpress.Utils.PointFloat(82.36362!, 0.0!)
            Me.XrLabel_CLientName.Multiline = True
            Me.XrLabel_CLientName.Name = "XrLabel_CLientName"
            Me.XrLabel_CLientName.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_CLientName.Scripts.OnBeforePrint = "XrLabel_name_BeforePrint"
            Me.XrLabel_CLientName.SizeF = New System.Drawing.SizeF(144.0001!, 14.99999!)
            Me.XrLabel_CLientName.StylePriority.UsePadding = False
            Me.XrLabel_CLientName.StylePriority.UseTextAlignment = False
            Me.XrLabel_CLientName.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            Me.XrLabel_CLientName.WordWrap = False
            '
            'XrLabel_ClientID
            '
            Me.XrLabel_ClientID.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
            Me.XrLabel_ClientID.Name = "XrLabel_ClientID"
            Me.XrLabel_ClientID.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_ClientID.Scripts.OnBeforePrint = "XrLabel_ClientID_BeforePrint"
            Me.XrLabel_ClientID.SizeF = New System.Drawing.SizeF(80.99994!, 14.99999!)
            Me.XrLabel_ClientID.StylePriority.UsePadding = False
            Me.XrLabel_ClientID.StylePriority.UseTextAlignment = False
            Me.XrLabel_ClientID.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrLabel_ClientID.WordWrap = False
            '
            'XrLabel_CounselorName
            '
            Me.XrLabel_CounselorName.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "CounselorName")})
            Me.XrLabel_CounselorName.LocationFloat = New DevExpress.Utils.PointFloat(227.7274!, 0.00001589457!)
            Me.XrLabel_CounselorName.Name = "XrLabel_CounselorName"
            Me.XrLabel_CounselorName.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_CounselorName.SizeF = New System.Drawing.SizeF(80.99994!, 14.99998!)
            Me.XrLabel_CounselorName.StylePriority.UsePadding = False
            Me.XrLabel_CounselorName.StylePriority.UseTextAlignment = False
            Me.XrLabel_CounselorName.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            Me.XrLabel_CounselorName.WordWrap = False
            '
            'XrLabel_AppointmentType
            '
            Me.XrLabel_AppointmentType.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "AppointmentType")})
            Me.XrLabel_AppointmentType.LocationFloat = New DevExpress.Utils.PointFloat(310.091!, 0.00001589457!)
            Me.XrLabel_AppointmentType.Name = "XrLabel_AppointmentType"
            Me.XrLabel_AppointmentType.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_AppointmentType.SizeF = New System.Drawing.SizeF(80.99994!, 14.99998!)
            Me.XrLabel_AppointmentType.StylePriority.UsePadding = False
            Me.XrLabel_AppointmentType.StylePriority.UseTextAlignment = False
            Me.XrLabel_AppointmentType.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            Me.XrLabel_AppointmentType.WordWrap = False
            '
            'XrLabel_StartTimeDT
            '
            Me.XrLabel_StartTimeDT.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "StartTimeDT", "{0:d}")})
            Me.XrLabel_StartTimeDT.LocationFloat = New DevExpress.Utils.PointFloat(392.4547!, 0.00001589457!)
            Me.XrLabel_StartTimeDT.Name = "XrLabel_StartTimeDT"
            Me.XrLabel_StartTimeDT.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_StartTimeDT.SizeF = New System.Drawing.SizeF(80.99994!, 14.99998!)
            Me.XrLabel_StartTimeDT.StylePriority.UsePadding = False
            Me.XrLabel_StartTimeDT.StylePriority.UseTextAlignment = False
            Me.XrLabel_StartTimeDT.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrLabel_StartTimeDT.WordWrap = False
            '
            'XrLabel_InterviewType
            '
            Me.XrLabel_InterviewType.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "InterviewType")})
            Me.XrLabel_InterviewType.LocationFloat = New DevExpress.Utils.PointFloat(474.8183!, 0.00001589457!)
            Me.XrLabel_InterviewType.Name = "XrLabel_InterviewType"
            Me.XrLabel_InterviewType.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_InterviewType.SizeF = New System.Drawing.SizeF(80.99994!, 14.99998!)
            Me.XrLabel_InterviewType.StylePriority.UsePadding = False
            Me.XrLabel_InterviewType.StylePriority.UseTextAlignment = False
            Me.XrLabel_InterviewType.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            Me.XrLabel_InterviewType.WordWrap = False
            '
            'XrLabel_InterviewDT
            '
            Me.XrLabel_InterviewDT.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "InterviewDT", "{0:d}")})
            Me.XrLabel_InterviewDT.LocationFloat = New DevExpress.Utils.PointFloat(557.1819!, 0.00001589457!)
            Me.XrLabel_InterviewDT.Name = "XrLabel_InterviewDT"
            Me.XrLabel_InterviewDT.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_InterviewDT.SizeF = New System.Drawing.SizeF(80.99994!, 14.99998!)
            Me.XrLabel_InterviewDT.StylePriority.UsePadding = False
            Me.XrLabel_InterviewDT.StylePriority.UseTextAlignment = False
            Me.XrLabel_InterviewDT.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrLabel_InterviewDT.WordWrap = False
            '
            'XrLabel_ResultType
            '
            Me.XrLabel_ResultType.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "ResultType")})
            Me.XrLabel_ResultType.LocationFloat = New DevExpress.Utils.PointFloat(639.5455!, 0.00001589457!)
            Me.XrLabel_ResultType.Name = "XrLabel_ResultType"
            Me.XrLabel_ResultType.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_ResultType.SizeF = New System.Drawing.SizeF(80.99994!, 14.99998!)
            Me.XrLabel_ResultType.StylePriority.UsePadding = False
            Me.XrLabel_ResultType.StylePriority.UseTextAlignment = False
            Me.XrLabel_ResultType.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            Me.XrLabel_ResultType.WordWrap = False
            '
            'XrLabel_ResultDT
            '
            Me.XrLabel_ResultDT.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "ResultDT", "{0:d}")})
            Me.XrLabel_ResultDT.LocationFloat = New DevExpress.Utils.PointFloat(721.9092!, 0.00001589457!)
            Me.XrLabel_ResultDT.Name = "XrLabel_ResultDT"
            Me.XrLabel_ResultDT.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_ResultDT.SizeF = New System.Drawing.SizeF(80.99994!, 14.99998!)
            Me.XrLabel_ResultDT.StylePriority.UsePadding = False
            Me.XrLabel_ResultDT.StylePriority.UseTextAlignment = False
            Me.XrLabel_ResultDT.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrLabel_ResultDT.WordWrap = False
            '
            'XrLabel_TerminationType
            '
            Me.XrLabel_TerminationType.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "TerminationType")})
            Me.XrLabel_TerminationType.LocationFloat = New DevExpress.Utils.PointFloat(804.2728!, 0.00001589457!)
            Me.XrLabel_TerminationType.Name = "XrLabel_TerminationType"
            Me.XrLabel_TerminationType.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_TerminationType.SizeF = New System.Drawing.SizeF(80.99994!, 14.99998!)
            Me.XrLabel_TerminationType.StylePriority.UsePadding = False
            Me.XrLabel_TerminationType.StylePriority.UseTextAlignment = False
            Me.XrLabel_TerminationType.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            Me.XrLabel_TerminationType.WordWrap = False
            '
            'XrLabel_TerminationDT
            '
            Me.XrLabel_TerminationDT.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "TerminationDT", "{0:d}")})
            Me.XrLabel_TerminationDT.LocationFloat = New DevExpress.Utils.PointFloat(886.6365!, 0.00001589457!)
            Me.XrLabel_TerminationDT.Name = "XrLabel_TerminationDT"
            Me.XrLabel_TerminationDT.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_TerminationDT.SizeF = New System.Drawing.SizeF(80.99994!, 14.99998!)
            Me.XrLabel_TerminationDT.StylePriority.UsePadding = False
            Me.XrLabel_TerminationDT.StylePriority.UseTextAlignment = False
            Me.XrLabel_TerminationDT.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrLabel_TerminationDT.WordWrap = False
            '
            'XrLabel_HousingDuration
            '
            Me.XrLabel_HousingDuration.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "HousingDuration", "{0:f0}")})
            Me.XrLabel_HousingDuration.LocationFloat = New DevExpress.Utils.PointFloat(969.0001!, 0.0!)
            Me.XrLabel_HousingDuration.Name = "XrLabel_HousingDuration"
            Me.XrLabel_HousingDuration.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_HousingDuration.SizeF = New System.Drawing.SizeF(80.99994!, 14.99998!)
            Me.XrLabel_HousingDuration.StylePriority.UsePadding = False
            Me.XrLabel_HousingDuration.StylePriority.UseTextAlignment = False
            Me.XrLabel_HousingDuration.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrLabel_HousingDuration.WordWrap = False
            '
            'HousingReport
            '
            Me.Bands.AddRange(New DevExpress.XtraReports.UI.Band() {Me.TopMarginBand1, Me.Detail, Me.BottomMarginBand1, Me.PageHeader, Me.PageFooter})
            Me.Landscape = True
            Me.PageHeight = 850
            Me.PageWidth = 1100
            Me.ScriptLanguage = DevExpress.XtraReports.ScriptLanguage.VisualBasic
            Me.ScriptReferencesString = resources.GetString("$this.ScriptReferencesString")
            Me.Scripts.OnBeforePrint = "Report_BeforePrint"
            Me.ScriptsSource = resources.GetString("$this.ScriptsSource")
            Me.StyleSheet.AddRange(New DevExpress.XtraReports.UI.XRControlStyle() {Me.XrControlStyle_Header, Me.XrControlStyle_Totals, Me.XrControlStyle_HeaderPannel, Me.XrControlStyle_GroupHeader})
            Me.Version = "11.2"
            CType(Me, System.ComponentModel.ISupportInitialize).EndInit()

        End Sub
        Friend WithEvents XrPanel1 As DevExpress.XtraReports.UI.XRPanel
        Friend WithEvents XrLabel5 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel3 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel1 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel2 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_ClientID As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_CLientName As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_AppointmentType As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_CounselorName As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_TerminationDT As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_TerminationType As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_ResultDT As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_ResultType As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_InterviewDT As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_InterviewType As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_StartTimeDT As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_HousingDuration As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel11 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel10 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel13 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel12 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel7 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel6 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel9 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel8 As DevExpress.XtraReports.UI.XRLabel
    End Class
End Namespace
