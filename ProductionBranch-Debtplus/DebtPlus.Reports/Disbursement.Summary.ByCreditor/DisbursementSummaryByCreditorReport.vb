#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}

#End Region

Option Compare Binary
Option Explicit On
Option Strict On

Imports System.ComponentModel
Imports DevExpress.XtraReports.UI
Imports System.Globalization
Imports DebtPlus.Reports.Template.Forms
Imports System.Data.SqlClient
Imports System.Text
Imports System.Threading
Imports DebtPlus.Interfaces.Reports

Namespace Disbursement.SummaryByCreditor
    Public Class DisbursementSummaryByCreditorReport
        Inherits DebtPlus.Reports.Template.TemplateXtraReportClass

        Public Sub New()
            MyBase.New()
            InitializeComponent()

            ' Bind the fields
            XrLabel_Date.DataBindings.Add("Text", Nothing, "date_created", "{0:d}")
            XrLabel_checknum.DataBindings.Add("Text", Nothing, "checknum")
            XrLabel_Creditor.DataBindings.Add("Text", Nothing, "creditor")
            XrLabel_CreditorName.DataBindings.Add("Text", Nothing, "creditor_name")
            XrLabel_Gross.DataBindings.Add("Text", Nothing, "gross", "{0:c}")
            XrLabel_Deducted.DataBindings.Add("Text", Nothing, "deducted", "{0:c}")
            XrLabel_Billed.DataBindings.Add("Text", Nothing, "billed", "{0:c}")
            XrLabel_Net.DataBindings.Add("Text", Nothing, "net", "{0:c}")
            XrLabel_total_gross.DataBindings.Add("Text", Nothing, "gross")
            XrLabel_total_deduct.DataBindings.Add("Text", Nothing, "deducted")
            XrLabel_total_billed.DataBindings.Add("Text", Nothing, "billed")
            XrLabel_total_net.DataBindings.Add("Text", Nothing, "net")

            XrLabel_Date.DataBindings.Add("Tag", Nothing, "id")
            XrLabel_checknum.DataBindings.Add("Tag", Nothing, "id")
            XrLabel_Creditor.DataBindings.Add("Tag", Nothing, "id")
            XrLabel_CreditorName.DataBindings.Add("Tag", Nothing, "id")
            XrLabel_Gross.DataBindings.Add("Tag", Nothing, "id")
            XrLabel_Deducted.DataBindings.Add("Tag", Nothing, "id")
            XrLabel_Billed.DataBindings.Add("Tag", Nothing, "id")
            XrLabel_Net.DataBindings.Add("Tag", Nothing, "id")

            AddHandler BeforePrint, AddressOf Report_BeforePrint
            AddHandler XrLabel_Date.PreviewClick, AddressOf Preview_Click
            AddHandler XrLabel_checknum.PreviewClick, AddressOf Preview_Click
            AddHandler XrLabel_Creditor.PreviewClick, AddressOf Preview_Click
            AddHandler XrLabel_CreditorName.PreviewClick, AddressOf Preview_Click
            AddHandler XrLabel_Gross.PreviewClick, AddressOf Preview_Click
            AddHandler XrLabel_Deducted.PreviewClick, AddressOf Preview_Click
            AddHandler XrLabel_Billed.PreviewClick, AddressOf Preview_Click
            AddHandler XrLabel_Net.PreviewClick, AddressOf Preview_Click
        End Sub

        Private _Parameter_BatchID As Int32 = -1

        Public Property Parameter_BatchID() As Int32
            Get
                Return _Parameter_BatchID
            End Get
            Set(ByVal Value As Int32)
                _Parameter_BatchID = Value
            End Set
        End Property

        Public Overrides Sub SetReportParameter(ByVal Parameter As String, ByVal Value As Object)
            Select Case Parameter
                Case "BatchID"
                    Parameter_BatchID = Convert.ToInt32(Value, CultureInfo.InvariantCulture)
                Case Else
                    MyBase.SetReportParameter(Parameter, Value)
            End Select
        End Sub

        Public Overrides ReadOnly Property ReportTitle() As String
            Get
                Return "Disbursement Summary by Creditor"
            End Get
        End Property

        Private _Report_Subtitle As String = String.Empty

        Public Overrides ReadOnly Property ReportSubtitle() As String
            Get
                Return _Report_Subtitle
            End Get
        End Property

        Public Overrides Function NeedParameters() As Boolean
            Return MyBase.NeedParameters() OrElse (Parameter_BatchID <= 0)
        End Function

        Public Overrides Function RequestReportParameters() As DialogResult
            Dim answer As DialogResult = DialogResult.OK
            If NeedParameters() Then
                With New DisbursementParametersForm()
                    .Text = ReportTitle + " Report Parameters"
                    answer = .ShowDialog()
                    Parameter_BatchID = .Parameter_BatchID
                    .Dispose()
                End With
            End If
            Return answer
        End Function

        Private Sub Report_BeforePrint(ByVal sender As Object, ByVal e As EventArgs)
            Dim ds As New DataSet("ds")

            _Report_Subtitle = String.Format("This report is for disbursement batch #{0:f0}", Parameter_BatchID)

            ' Read the table from the system
            Using cmd As SqlCommand = New System.Data.SqlClient.SqlCommand()
                With cmd
                    .Connection = New System.Data.SqlClient.SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
                    .CommandText = CommandText()
                    .Parameters.Add("@disbursement_register", SqlDbType.Int).Value = Parameter_BatchID
                    .CommandTimeout = 0
                End With

                Using da As New SqlDataAdapter(cmd)
                    da.Fill(ds, "registers_trust")

                    Dim tbl As DataTable = ds.Tables("registers_trust")
                    If tbl IsNot Nothing Then
                        With tbl
                            .PrimaryKey = New DataColumn() {.Columns("id")}
                            .Columns.Add("net", GetType(Decimal), "[gross]-[deducted]")
                        End With

                        DataSource = New DataView(tbl, String.Empty, "bank, len_checknum, checknum, creditor, date_created", DataViewRowState.CurrentRows)
                    End If
                End Using
            End Using
        End Sub

        Private Function CommandText() As String
            Dim answer As New StringBuilder

            answer.Append("select	convert(varchar(50),newid()) as id,")
            answer.Append("         tr.trust_register, ")
            answer.Append(" 		rcc.creditor_register as creditor_register, ")
            answer.Append(" 		tr.bank, ")
            answer.Append(" 		tr.checknum, ")
            answer.Append(" 		rcc.creditor, ")
            answer.Append(" 		cr.creditor_name, ")
            answer.Append(" 		tr.date_created, ")
            answer.Append(" 		SUM(rcc.debit_amt) as gross, ")
            answer.Append(" 		SUM(case when rcc.creditor_type = 'D' then rcc.fairshare_amt else 0 end) as deducted, ")
            answer.Append(" 		SUM(case when rcc.creditor_type in ('D','N') then 0 else rcc.fairshare_amt end) as billed, ")
            answer.Append(" 		len(tr.checknum) as len_checknum ")
            answer.Append("from     registers_client_creditor rcc ")
            answer.Append("inner join registers_trust tr on rcc.trust_register = tr.trust_register ")
            answer.Append("inner join creditors cr on rcc.creditor = cr.creditor ")
            answer.Append("WHERE    rcc.tran_type in ('AD','BW','MD','CM') ")
            answer.Append("AND      rcc.disbursement_register=@disbursement_register ")
            answer.Append("AND      cr.creditor_id <> 0 ")
            answer.Append("group by tr.trust_register, tr.bank, tr.checknum, rcc.creditor, cr.creditor_name, tr.date_created, rcc.creditor_register; ")

            Return answer.ToString()
        End Function

        Private Sub Preview_Click(ByVal sender As Object, ByVal e As PreviewMouseEventArgs)
            Dim vue As DataView = CType(DataSource, DataView)
            Dim tbl As DataTable = vue.Table

            Dim id As String = CType(e.Brick.Value, String)
            If Not String.IsNullOrEmpty(id) Then
                Dim row As DataRow = tbl.Rows.Find(id)
                Dim creditor_register As Int32 = -1
                If row IsNot Nothing Then
                    If row("creditor_register") IsNot DBNull.Value Then creditor_register = Convert.ToInt32(row("creditor_register"))
                End If

                If creditor_register > 0 Then
                    DetailReport(row)
                End If
            End If
        End Sub

        Private Sub DetailReport(ByVal row As DataRow)
            Dim thrd As New Thread(New ParameterizedThreadStart(AddressOf MyRunReport))
            With thrd
                .SetApartmentState(ApartmentState.STA)
                .IsBackground = True
                .Name = "CreditorVoucherReport"
                .Start(row)
            End With
        End Sub

        Private Sub MyRunReport(ByVal obj As Object)
            Dim row As DataRow = CType(obj, DataRow)
            Dim iRep As DebtPlus.Reports.Creditor.CheckVoucher.CheckVoucherReport = New DebtPlus.Reports.Creditor.CheckVoucher.CheckVoucherReport()

            If row("trust_register") IsNot DBNull.Value Then iRep.Parameter_TrustRegister = Convert.ToInt32(row("trust_register"))
            If row("checknum") IsNot DBNull.Value Then iRep.Parameter_checknum = Convert.ToInt32(row("checknum"))
            If row("bank") IsNot DBNull.Value Then iRep.Parameter_Bank = Convert.ToInt32(row("bank"))
            If row("creditor") IsNot DBNull.Value Then iRep.Parameter_Creditor = Convert.ToString(row("creditor"))
            iRep.DisplayPreviewDialog()
        End Sub
    End Class
End Namespace