#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}

#End Region

Option Compare Binary
Option Explicit On
Option Strict On

Imports DebtPlus.Reports.Template.Forms
Imports System.Data.SqlClient

Namespace Operations.Referrals
    Public Class OperationsReferralsParametersForm
        Inherits DateReportParametersForm

        Public Sub New()
            MyBase.New()
            InitializeComponent()
            AddHandler Me.Load, AddressOf ParametersForm_Load
        End Sub

        ''' <summary>
        ''' Parameter for the office number
        ''' </summary>
        Public ReadOnly Property Parameter_Office() As Int32
            Get
                Return Convert.ToInt32(LookUpEdit1.EditValue)
            End Get
        End Property

        ''' <summary>
        ''' Look for some error condition to prevent the OK button from being used
        ''' </summary>
        Protected Overrides Function HasErrors() As Boolean
            Dim Answer As Boolean = MyBase.HasErrors()
            If Not Answer Then
                If LookUpEdit1.EditValue Is Nothing OrElse LookUpEdit1.EditValue Is DBNull.Value Then Answer = True
            End If

            Return Answer
        End Function

#Region " Windows Form Designer generated code "

        'Form overrides dispose to clean up the component list.
        Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
            If disposing Then
                If components IsNot Nothing Then
                    components.Dispose()
                End If
            End If
            MyBase.Dispose(disposing)
        End Sub

        'Required by the Windows Form Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Windows Form Designer
        'It can be modified using the Windows Form Designer.  
        'Do not modify it using the code editor.
        Friend WithEvents Label1 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents LookUpEdit1 As DevExpress.XtraEditors.LookUpEdit

        <System.Diagnostics.DebuggerStepThrough()>
        Private Sub InitializeComponent()
            Dim ComboboxItem1 As DebtPlus.Data.Controls.ComboboxItem = New DebtPlus.Data.Controls.ComboboxItem
            Me.Label1 = New DevExpress.XtraEditors.LabelControl
            Me.LookUpEdit1 = New DevExpress.XtraEditors.LookUpEdit
            CType(Me.XrGroup_param_08_1, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.XrGroup_param_08_1.SuspendLayout()
            CType(Me.XrCombo_param_08_1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.XrDate_param_08_2.Properties.VistaTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.XrDate_param_08_2.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.XrDate_param_08_1.Properties.VistaTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.XrDate_param_08_1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.DxErrorProvider1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LookUpEdit1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.SuspendLayout()
            '
            'XrGroup_param_08_1
            '
            Me.ToolTipController1.SetSuperTip(Me.XrGroup_param_08_1, Nothing)
            '
            'XrCombo_param_08_1
            '
            ComboboxItem1.tag = Nothing
            ComboboxItem1.value = 12
            Me.XrCombo_param_08_1.EditValue = ComboboxItem1
            '
            'XrDate_param_08_2
            '
            Me.XrDate_param_08_2.EditValue = New Date(2007, 2, 4, 0, 0, 0, 0)
            Me.XrDate_param_08_2.Properties.VistaTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton})
            '
            'XrDate_param_08_1
            '
            Me.XrDate_param_08_1.EditValue = New Date(2007, 2, 4, 0, 0, 0, 0)
            Me.XrDate_param_08_1.Properties.VistaTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton})
            '
            'ButtonOK
            '
            Me.ButtonOK.TabIndex = 3
            '
            'ButtonCancel
            '
            Me.ButtonCancel.TabIndex = 4
            '
            'Label1
            '
            Me.Label1.Appearance.Options.UseTextOptions = True
            Me.Label1.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
            Me.Label1.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center
            Me.Label1.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.Horizontal
            Me.Label1.Location = New System.Drawing.Point(8, 129)
            Me.Label1.Name = "Label1"
            Me.Label1.Size = New System.Drawing.Size(33, 13)
            Me.Label1.TabIndex = 1
            Me.Label1.Text = "Office:"
            Me.Label1.ToolTipController = Me.ToolTipController1
            '
            'LookUpEdit1
            '
            Me.LookUpEdit1.Location = New System.Drawing.Point(48, 127)
            Me.LookUpEdit1.Name = "LookUpEdit1"
            Me.LookUpEdit1.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.LookUpEdit1.Properties.Columns.AddRange(New DevExpress.XtraEditors.Controls.LookUpColumnInfo() {New DevExpress.XtraEditors.Controls.LookUpColumnInfo("item_key", "ID", 5, DevExpress.Utils.FormatType.Numeric, "f0", True, DevExpress.Utils.HorzAlignment.[Default], DevExpress.Data.ColumnSortOrder.None), New DevExpress.XtraEditors.Controls.LookUpColumnInfo("description", "Name", 20, DevExpress.Utils.FormatType.None, "", True, DevExpress.Utils.HorzAlignment.[Default], DevExpress.Data.ColumnSortOrder.Descending), New DevExpress.XtraEditors.Controls.LookUpColumnInfo("TypeName", "Type", 15)})
            Me.LookUpEdit1.Properties.DisplayMember = "description"
            Me.LookUpEdit1.Properties.NullText = ""
            Me.LookUpEdit1.Properties.ShowFooter = False
            Me.LookUpEdit1.Properties.ValueMember = "item_key"
            Me.LookUpEdit1.Size = New System.Drawing.Size(280, 20)
            Me.LookUpEdit1.TabIndex = 2
            Me.LookUpEdit1.ToolTipController = Me.ToolTipController1
            Me.LookUpEdit1.Properties.SortColumnIndex = 1
            '
            'ParametersForm
            '
            Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
            Me.ClientSize = New System.Drawing.Size(336, 160)
            Me.Controls.Add(Me.LookUpEdit1)
            Me.Controls.Add(Me.Label1)
            Me.Name = "ParametersForm"
            Me.ToolTipController1.SetSuperTip(Me, Nothing)
            Me.Controls.SetChildIndex(Me.Label1, 0)
            Me.Controls.SetChildIndex(Me.LookUpEdit1, 0)
            Me.Controls.SetChildIndex(Me.ButtonOK, 0)
            Me.Controls.SetChildIndex(Me.ButtonCancel, 0)
            Me.Controls.SetChildIndex(Me.XrGroup_param_08_1, 0)
            CType(Me.XrGroup_param_08_1, System.ComponentModel.ISupportInitialize).EndInit()
            Me.XrGroup_param_08_1.ResumeLayout(False)
            Me.XrGroup_param_08_1.PerformLayout()
            CType(Me.XrCombo_param_08_1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.XrDate_param_08_2.Properties.VistaTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.XrDate_param_08_2.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.XrDate_param_08_1.Properties.VistaTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.XrDate_param_08_1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.DxErrorProvider1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LookUpEdit1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            Me.ResumeLayout(False)
            Me.PerformLayout()
        End Sub

#End Region

        ''' <summary>
        ''' Process the initial loading of this form
        ''' </summary>
        Private Sub ParametersForm_Load(ByVal sender As Object, ByVal e As EventArgs)
            OfficeListLookupEdit_Load()
            ButtonOK.Enabled = Not HasErrors()
        End Sub

        ''' <summary>
        ''' Load the office location information
        ''' </summary>
        Private Sub OfficeListLookupEdit_Load()
            Dim ds As New DataSet("ds")

            ' Select the data from the database
            Dim cmd As SqlCommand = New System.Data.SqlClient.SqlCommand()
            With cmd
                .Connection = New System.Data.SqlClient.SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
                .CommandText = "lst_offices"
                .CommandType = CommandType.StoredProcedure
            End With

            Dim da As New SqlDataAdapter(cmd)
            da.Fill(ds)
            Dim tbl As DataTable = ds.Tables(0)

            ' Add the "All offices" to the list of offices
            Dim row As DataRow = tbl.NewRow
            With row
                .Item("TypeName") = String.Empty
                .Item("item_key") = -1
                .Item("description") = "All Offices"
                .Item("Default") = False
                .Item("ActiveFlag") = True
            End With
            tbl.Rows.InsertAt(row, 0)

            ' Bind the office list to the data
            LookUpEdit1.Properties.DataSource = tbl
            LookUpEdit1.Properties.PopupWidth = LookUpEdit1.Width + 125           ' Add some extra space for the dropdown width

            ' Set the default item if possible
            If tbl.Columns.Contains("Default") Then
                Dim default_vue As New DataView(tbl, "[Default]<>0", String.Empty, DataViewRowState.CurrentRows)
                If default_vue.Count > 0 Then
                    LookUpEdit1.EditValue = default_vue(0).Item("item_key")
                End If
            End If

            AddHandler LookUpEdit1.EditValueChanging, AddressOf DebtPlus.Data.Validation.LookUpEdit_ActiveTest
            AddHandler LookUpEdit1.EditValueChanged, AddressOf FormChanged
        End Sub

    End Class
End Namespace
