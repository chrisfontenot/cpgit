#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}

#End Region

Imports DebtPlus.Reports.Template

Imports DebtPlus.Utils
Imports System.IO
Imports System.Reflection
Imports DevExpress.XtraReports.UI

Namespace Operations.Referrals
    Public Class ReferralsReportClass
        Inherits DatedTemplateXtraReportClass

        Public Sub New()
            MyBase.New()

            ' Name of the report file
            Const ReportName As String = "DebtPlus.Reports.Operations.Referrals.repx"

            ' See if there is a report reference that we can use
            Dim Fname As String = Path.Combine(DebtPlus.Configuration.Config.ReportsDirectory, ReportName)
            Dim UseDefault As Boolean = True
            Try
                If (Not DebtPlus.Configuration.Config.LocalReportsOnly) AndAlso System.IO.File.Exists(Fname) Then
                    LoadLayout(Fname)
                    UseDefault = False
                End If

            Catch ex As Exception
                DebtPlus.Svc.Common.ErrorHandling.HandleErrors(ex)
            End Try

            ' Load the standard report definition if we need a new item
            If UseDefault Then
                Dim asm As Assembly = Assembly.GetExecutingAssembly                                           ' changed
                Using ios As Stream = asm.GetManifestResourceStream(String.Format("DebtPlus.Reports.{0}", ReportName)) ' changed
                    If ios IsNot Nothing Then LoadLayout(ios) '                                                                       changed
                End Using
            End If

            ' Set the script references as needed
            ScriptReferences = DebtPlus.Utils.AssemblyTools.GetAllSiblingAssemblies()

            ' Update the subreports as well
            For Each BandPtr As Band In Bands
                For Each CtlPtr As XRControl In BandPtr.Controls
                    Dim Rpt As XRSubreport = TryCast(CtlPtr, XRSubreport)
                    If Rpt IsNot Nothing Then
                        Dim RptSrc As XtraReport = Rpt.ReportSource
                        If RptSrc IsNot Nothing Then
                            RptSrc.ScriptReferences = ScriptReferences
                        End If
                    End If
                Next
            Next
        End Sub


        ''' <summary>
        '''     Office number
        ''' </summary>
        Public Property Parameter_Office() As Int32
            Get
                Dim parm As DevExpress.XtraReports.Parameters.Parameter = FindParameter("ParameterOffice")
                If parm Is Nothing Then Return -1
                Return Convert.ToInt32(parm.Value)
            End Get
            Set(ByVal Value As Int32)
                SetParameter("ParameterOffice", GetType(Int32), Value, "Office ID", False)
            End Set
        End Property


        ''' <summary>
        '''     Set a report parameter by name
        ''' </summary>
        Public Overrides Sub SetReportParameter(ByVal Parameter As String, ByVal Value As Object)
            Select Case Parameter
                Case "Office"
                    Parameter_Office = If(Object.Equals(Value, System.DBNull.Value), -1, Convert.ToInt32(Value))
                Case Else
                    MyBase.SetReportParameter(Parameter, Value)
            End Select
        End Sub


        ''' <summary>
        '''     Ask for the parameters
        ''' </summary>
        Public Overrides Function RequestReportParameters() As DialogResult
            Dim answer As DialogResult = DialogResult.OK
            If NeedParameters() Then
                Using frm As New OperationsReferralsParametersForm()
                    answer = frm.ShowDialog()
                    Parameter_FromDate = frm.Parameter_FromDate
                    Parameter_ToDate = frm.Parameter_ToDate
                    Parameter_Office = If(Object.Equals(frm.Parameter_Office, System.DBNull.Value), -1, Convert.ToInt32(frm.Parameter_Office))
                End Using
            End If
            Return answer
        End Function


        ''' <summary>
        '''     Title associated with the report
        ''' </summary>
        Public Overrides ReadOnly Property ReportTitle() As String
            Get
                Return "Referrals/Cause Report"
            End Get
        End Property


        ''' <summary>
        '''     SubTitle associated with the report
        ''' </summary>
        Public Overrides ReadOnly Property ReportSubTitle() As String
            Get
                Dim Answer As String = MyBase.ReportSubTitle
                If Parameter_Office > 0 Then Answer += String.Format(" and office #{0:f0}", Convert.ToInt32(Parameter_Office))
                Return Answer
            End Get
        End Property
    End Class
End Namespace
