#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}
#End Region

Imports DevExpress.XtraReports.UI

Namespace Operations.Referrals
    Friend Class ReferralsReport_ReferredTo

        Public Sub New()
            MyBase.New()
            InitializeComponent()
            ' AddHandler BeforePrint, AddressOf ReferralsReport_ReferredTo_BeforePrint
        End Sub

        Dim ds As New System.Data.DataSet("ds")

        Private Sub ReferralsReport_ReferredTo_BeforePrint(sender As Object, e As System.Drawing.Printing.PrintEventArgs)
            Const TableName As String = "rpt_referrals_referred_to"
            Dim rpt As DevExpress.XtraReports.UI.XtraReport = CType(sender, DevExpress.XtraReports.UI.XtraReport)
            Dim MasterRpt As DevExpress.XtraReports.UI.XtraReport = CType(rpt.MasterReport, DevExpress.XtraReports.UI.XtraReport)
            If MasterRpt IsNot Nothing Then
                Dim ParameterFromDate As DateTime = Convert.ToDateTime(MasterRpt.Parameters("ParameterFromDate").Value)
                Dim ParameterToDate As DateTime = Convert.ToDateTime(MasterRpt.Parameters("ParameterToDate").Value)
                Dim ParameterOffice As Int32 = Convert.ToInt32(MasterRpt.Parameters("ParameterOffice").Value)

                '-- Bind the report source
                Dim sqlInfo As DebtPlus.LINQ.SQLInfoClass = DebtPlus.LINQ.SQLInfoClass.getDefault()
                Using dlg As DevExpress.Utils.WaitDialogForm = New DevExpress.Utils.WaitDialogForm("Reading Information For Referral Targets")
                    Using cn As System.Data.SqlClient.SqlConnection = New System.Data.SqlClient.SqlConnection(sqlInfo.ConnectionString)
                        cn.Open()
                        Using cmd As System.Data.SqlClient.SqlCommand = New System.Data.SqlClient.SqlCommand()
                            cmd.Connection = cn
                            cmd.CommandText = "rpt_referrals_referred_to"
                            cmd.CommandType = System.Data.CommandType.StoredProcedure
                            cmd.Parameters.Add("@FromDate", System.Data.SqlDbType.DateTime).Value = ParameterFromDate
                            cmd.Parameters.Add("@ToDate", System.Data.SqlDbType.DateTime).Value = ParameterToDate
                            If ParameterOffice >= 0 Then
                                cmd.Parameters.Add("@Office", System.Data.SqlDbType.Int).Value = ParameterOffice
                            End If

                            Using da As New System.Data.SqlClient.SqlDataAdapter(cmd)
                                da.Fill(ds, TableName)
                                rpt.DataSource = ds.Tables(TableName).DefaultView
                            End Using
                        End Using
                    End Using

                    dlg.Close()
                End Using
            End If
        End Sub
    End Class
End Namespace

