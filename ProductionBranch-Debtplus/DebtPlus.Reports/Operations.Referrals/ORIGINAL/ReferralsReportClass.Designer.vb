﻿Namespace Operations.Referrals
    Partial Class ReferralsReportClass
        Inherits DebtPlus.Reports.Template.DatedTemplateXtraReportClass

        'Component overrides dispose to clean up the component list.
        Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
            If disposing Then
                If components IsNot Nothing Then
                    components.Dispose()
                End If
            End If
            MyBase.Dispose(disposing)
        End Sub

        'Required by the Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Designer
        'It can be modified using the Designer.
        'Do not modify it using the code editor.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
            Me.components = New System.ComponentModel.Container()
            Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(ReferralsReportClass))
            Me.Subreport_FinancialProblems = New DevExpress.XtraReports.UI.XRSubreport()
            Me.ReferralsReport_FinancialProblems1 = New DebtPlus.Reports.Operations.Referrals.ReferralsReport_FinancialProblems()
            Me.Subreport_ReferredTo = New DevExpress.XtraReports.UI.XRSubreport()
            Me.ReferralsReport_ReferredTo1 = New DebtPlus.Reports.Operations.Referrals.ReferralsReport_ReferredTo()
            Me.Subreport_ReferredBy = New DevExpress.XtraReports.UI.XRSubreport()
            Me.ReferralsReport_ReferredBy1 = New DebtPlus.Reports.Operations.Referrals.ReferralsReport_ReferredBy()
            Me.ParameterOffice = New DevExpress.XtraReports.Parameters.Parameter()
            CType(Me.ReferralsReport_FinancialProblems1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.ReferralsReport_ReferredTo1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.ReferralsReport_ReferredBy1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
            '
            'Detail
            '
            Me.Detail.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.Subreport_FinancialProblems, Me.Subreport_ReferredTo, Me.Subreport_ReferredBy})
            Me.Detail.HeightF = 100.0!
            '
            'XrLabel_Title
            '
            Me.XrLabel_Title.StylePriority.UseFont = False
            '
            'XrPageInfo_PageNumber
            '
            Me.XrPageInfo_PageNumber.StylePriority.UseTextAlignment = False
            '
            'XRLabel_Agency_Name
            '
            Me.XRLabel_Agency_Name.StylePriority.UseTextAlignment = False
            '
            'XrLabel_Agency_Address3
            '
            Me.XrLabel_Agency_Address3.StylePriority.UseFont = False
            Me.XrLabel_Agency_Address3.StylePriority.UseTextAlignment = False
            '
            'XrLabel_Agency_Address1
            '
            Me.XrLabel_Agency_Address1.StylePriority.UseFont = False
            Me.XrLabel_Agency_Address1.StylePriority.UseTextAlignment = False
            '
            'XrLabel_Agency_Phone
            '
            Me.XrLabel_Agency_Phone.StylePriority.UseFont = False
            Me.XrLabel_Agency_Phone.StylePriority.UseTextAlignment = False
            '
            'XrLabel_Agency_Address2
            '
            Me.XrLabel_Agency_Address2.StylePriority.UseFont = False
            Me.XrLabel_Agency_Address2.StylePriority.UseTextAlignment = False
            '
            'Subreport_FinancialProblems
            '
            Me.Subreport_FinancialProblems.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 67.00001!)
            Me.Subreport_FinancialProblems.Name = "Subreport_FinancialProblems"
            Me.Subreport_FinancialProblems.ReportSource = Me.ReferralsReport_FinancialProblems1
            Me.Subreport_FinancialProblems.SizeF = New System.Drawing.SizeF(741.0!, 23.0!)
            '
            'Subreport_ReferredTo
            '
            Me.Subreport_ReferredTo.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 32.99999!)
            Me.Subreport_ReferredTo.Name = "Subreport_ReferredTo"
            Me.Subreport_ReferredTo.ReportSource = Me.ReferralsReport_ReferredTo1
            Me.Subreport_ReferredTo.SizeF = New System.Drawing.SizeF(740.9999!, 23.0!)
            '
            'Subreport_ReferredBy
            '
            Me.Subreport_ReferredBy.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
            Me.Subreport_ReferredBy.Name = "Subreport_ReferredBy"
            Me.Subreport_ReferredBy.ReportSource = Me.ReferralsReport_ReferredBy1
            Me.Subreport_ReferredBy.SizeF = New System.Drawing.SizeF(741.0!, 23.0!)
            '
            'ParameterOffice
            '
            Me.ParameterOffice.Description = "Office ID"
            Me.ParameterOffice.Name = "ParameterOffice"
            Me.ParameterOffice.Type = GetType(Integer)
            Me.ParameterOffice.Value = -1
            Me.ParameterOffice.Visible = False
            '
            'ReferralsReportClass
            '
            Me.Bands.AddRange(New DevExpress.XtraReports.UI.Band() {Me.Detail, Me.PageHeader, Me.PageFooter, Me.TopMarginBand1, Me.BottomMarginBand1})
            Me.Parameters.AddRange(New DevExpress.XtraReports.Parameters.Parameter() {Me.ParameterOffice})
            Me.ScriptLanguage = DevExpress.XtraReports.ScriptLanguage.VisualBasic
            Me.ScriptReferencesString = resources.GetString("$this.ScriptReferencesString")
            Me.StyleSheet.AddRange(New DevExpress.XtraReports.UI.XRControlStyle() {Me.XrControlStyle_Header, Me.XrControlStyle_Totals, Me.XrControlStyle_HeaderPannel, Me.XrControlStyle_GroupHeader})
            Me.Version = "11.2"
            CType(Me.ReferralsReport_FinancialProblems1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.ReferralsReport_ReferredTo1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.ReferralsReport_ReferredBy1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me, System.ComponentModel.ISupportInitialize).EndInit()

        End Sub

        Friend WithEvents Subreport_ReferredBy As DevExpress.XtraReports.UI.XRSubreport
        Friend WithEvents Subreport_ReferredTo As DevExpress.XtraReports.UI.XRSubreport
        Friend WithEvents Subreport_FinancialProblems As DevExpress.XtraReports.UI.XRSubreport
        Private WithEvents ReferralsReport_FinancialProblems1 As DebtPlus.Reports.Operations.Referrals.ReferralsReport_FinancialProblems
        Private WithEvents ReferralsReport_ReferredTo1 As DebtPlus.Reports.Operations.Referrals.ReferralsReport_ReferredTo
        Private WithEvents ReferralsReport_ReferredBy1 As DebtPlus.Reports.Operations.Referrals.ReferralsReport_ReferredBy
        Friend WithEvents ParameterOffice As DevExpress.XtraReports.Parameters.Parameter
    End Class
End Namespace
