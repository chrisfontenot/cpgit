#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}
#End Region

Option Compare Binary
Option Explicit On
Option Strict On

Imports DebtPlus.Reports.Template

Namespace ACH.Transactions
    Public Class AchTransactionsReport
        Inherits TemplateXtraReportClass

        ''' <summary>
        ''' Create an instance of our report
        ''' </summary>
        Public Sub New()
            MyBase.New()
            InitializeComponent()
            AddHandler Me.BeforePrint, AddressOf ACHTransactionsReport_BeforePrint
            AddHandler XrLabel_ClientID.BeforePrint, AddressOf XrLabel_ClientID_BeforePrint
            AddHandler XrLabel_TraceID.BeforePrint, AddressOf XrLabel_TraceID_BeforePrint
            AddHandler XrLabel_TraceID.AfterPrint, AddressOf XrLabel_TraceID_AfterPrint
            AddHandler XrLabel_Summary_Errors.BeforePrint, AddressOf XrLabel_Summary_Errors_BeforePrint
            AddHandler XrLabel_Summary_HOLD.BeforePrint, AddressOf XrLabel_Summary_HOLD_BeforePrint
            AddHandler XrLabel_Summary_InsufficentFunds.BeforePrint, AddressOf XrLabel_Summary_InsufficentFunds_BeforePrint
            AddHandler XrLabel_Summary_Prenotes.BeforePrint, AddressOf XrLabel_Summary_Prenotes_BeforePrint
            AddHandler XrLabel_Summary_StopPayments.BeforePrint, AddressOf XrLabel_Summary_StopPayments_BeforePrint
            ReportFilter.IsEnabled = True
        End Sub

        Friend WithEvents XrControlStyle1 As DevExpress.XtraReports.UI.XRControlStyle

        ''' <summary>
        ''' Return the report title
        ''' </summary>
        Public Overrides ReadOnly Property ReportTitle() As String
            Get
                Return "ACH Transactions"
            End Get
        End Property

        ''' <summary>
        ''' Parameter for the deposit batch number
        ''' </summary>
        Private _Parameter_DepositBatchID As System.Int32 = -1
        Public Property Parameter_DepositBatchID() As System.Int32
            Get
                Return _Parameter_DepositBatchID
            End Get
            Set(ByVal Value As System.Int32)
                _Parameter_DepositBatchID = Value
            End Set
        End Property

        ''' <summary>
        ''' Subroutine to set the parameter(s)
        ''' </summary>
        Public Overrides Sub SetReportParameter(ByVal Parameter As String, ByVal Value As Object)
            Select Case Parameter
                Case "DepositBatchID"
                    _Parameter_DepositBatchID = Convert.ToInt32(Value)
                Case Else
                    MyBase.SetReportParameter(Parameter, Value)
            End Select
        End Sub

#Region " Component Designer generated code "

        'Component overrides dispose to clean up the component list.
        Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
            If disposing Then
                If components IsNot Nothing Then
                    components.Dispose()
                End If
            End If
            MyBase.Dispose(disposing)
        End Sub

        'Required by the Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Designer
        'It can be modified using the Designer.
        'Do not modify it using the code editor.
        Friend WithEvents XrLabel1 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel2 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel3 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_Header_DatePosted As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_Header_DateCreated As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_Header_Status As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel4 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_Header_Pull_Schedules As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel6 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_Header_Effective_Date As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_Header_Settlement_Date As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel9 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrPanel1 As DevExpress.XtraReports.UI.XRPanel
        Friend WithEvents XrLabel5 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel7 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel8 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel10 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel11 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel12 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel13 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents ReportFooter As DevExpress.XtraReports.UI.ReportFooterBand
        Friend WithEvents XrLabel14 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLine1 As DevExpress.XtraReports.UI.XRLine
        Friend WithEvents XrLabel_Total_Amount As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_Amount As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_TraceID As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_Transaction As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_Routing As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_Account As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_Auth As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_ClientID As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_ClientName As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel15 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLine2 As DevExpress.XtraReports.UI.XRLine
        Friend WithEvents XrLabel16 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_Summary_Prenotes As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel18 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_Summary_Errors As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel20 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_Summary_StopPayments As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel22 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_Summary_InsufficentFunds As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_Summary_HOLD As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel25 As DevExpress.XtraReports.UI.XRLabel
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
            Dim XrSummary1 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary()
            Me.XrControlStyle1 = New DevExpress.XtraReports.UI.XRControlStyle()
            Me.XrLabel1 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel2 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel3 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_Header_DatePosted = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_Header_DateCreated = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_Header_Status = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel4 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_Header_Pull_Schedules = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel6 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_Header_Effective_Date = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_Header_Settlement_Date = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel9 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrPanel1 = New DevExpress.XtraReports.UI.XRPanel()
            Me.XrLabel13 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel12 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel11 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel10 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel8 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel7 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel5 = New DevExpress.XtraReports.UI.XRLabel()
            Me.ReportFooter = New DevExpress.XtraReports.UI.ReportFooterBand()
            Me.XrLabel25 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_Summary_HOLD = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_Summary_InsufficentFunds = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel22 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_Summary_StopPayments = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel20 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_Summary_Errors = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel18 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_Summary_Prenotes = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel16 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLine2 = New DevExpress.XtraReports.UI.XRLine()
            Me.XrLabel15 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_Total_Amount = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLine1 = New DevExpress.XtraReports.UI.XRLine()
            Me.XrLabel14 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_Amount = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_TraceID = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_Transaction = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_Routing = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_Account = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_Auth = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_ClientID = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_ClientName = New DevExpress.XtraReports.UI.XRLabel()
            CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
            '
            'Detail
            '
            Me.Detail.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel_ClientName, Me.XrLabel_ClientID, Me.XrLabel_Auth, Me.XrLabel_Account, Me.XrLabel_Routing, Me.XrLabel_Transaction, Me.XrLabel_TraceID, Me.XrLabel_Amount})
            Me.Detail.HeightF = 17.0!
            '
            'PageHeader
            '
            Me.PageHeader.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrPanel1, Me.XrLabel9, Me.XrLabel_Header_Settlement_Date, Me.XrLabel_Header_Effective_Date, Me.XrLabel6, Me.XrLabel_Header_Pull_Schedules, Me.XrLabel4, Me.XrLabel_Header_Status, Me.XrLabel_Header_DateCreated, Me.XrLabel_Header_DatePosted, Me.XrLabel3, Me.XrLabel2, Me.XrLabel1})
            Me.PageHeader.HeightF = 174.0!
            Me.PageHeader.Controls.SetChildIndex(Me.XrLabel1, 0)
            Me.PageHeader.Controls.SetChildIndex(Me.XrLabel2, 0)
            Me.PageHeader.Controls.SetChildIndex(Me.XrLabel3, 0)
            Me.PageHeader.Controls.SetChildIndex(Me.XrLabel_Header_DatePosted, 0)
            Me.PageHeader.Controls.SetChildIndex(Me.XrLabel_Header_DateCreated, 0)
            Me.PageHeader.Controls.SetChildIndex(Me.XrLabel_Header_Status, 0)
            Me.PageHeader.Controls.SetChildIndex(Me.XrLabel4, 0)
            Me.PageHeader.Controls.SetChildIndex(Me.XrLabel_Header_Pull_Schedules, 0)
            Me.PageHeader.Controls.SetChildIndex(Me.XrLabel6, 0)
            Me.PageHeader.Controls.SetChildIndex(Me.XrLabel_Header_Effective_Date, 0)
            Me.PageHeader.Controls.SetChildIndex(Me.XrLabel_Header_Settlement_Date, 0)
            Me.PageHeader.Controls.SetChildIndex(Me.XrLabel9, 0)
            Me.PageHeader.Controls.SetChildIndex(Me.XrPanel1, 0)
            Me.PageHeader.Controls.SetChildIndex(Me.XrPanel_AgencyAddress, 0)
            Me.PageHeader.Controls.SetChildIndex(Me.XrLabel_Title, 0)
            Me.PageHeader.Controls.SetChildIndex(Me.XrLine_Title, 0)
            Me.PageHeader.Controls.SetChildIndex(Me.XrLabel_Subtitle, 0)
            Me.PageHeader.Controls.SetChildIndex(Me.XrPageInfo1, 0)
            '
            'XrLabel_Title
            '
            Me.XrLabel_Title.StylePriority.UseFont = False
            '
            'XrLabel_Subtitle
            '
            Me.XrLabel_Subtitle.LocationFloat = New DevExpress.Utils.PointFloat(233.0!, 67.0!)
            Me.XrLabel_Subtitle.SizeF = New System.Drawing.SizeF(200.0!, 17.0!)
            Me.XrLabel_Subtitle.Visible = False
            '
            'XrPageInfo_PageNumber
            '
            Me.XrPageInfo_PageNumber.StylePriority.UseTextAlignment = False
            '
            'XRLabel_Agency_Name
            '
            Me.XRLabel_Agency_Name.StylePriority.UseTextAlignment = False
            '
            'XrLabel_Agency_Address3
            '
            Me.XrLabel_Agency_Address3.StylePriority.UseFont = False
            Me.XrLabel_Agency_Address3.StylePriority.UseTextAlignment = False
            '
            'XrLabel_Agency_Address1
            '
            Me.XrLabel_Agency_Address1.StylePriority.UseFont = False
            Me.XrLabel_Agency_Address1.StylePriority.UseTextAlignment = False
            '
            'XrLabel_Agency_Phone
            '
            Me.XrLabel_Agency_Phone.StylePriority.UseFont = False
            Me.XrLabel_Agency_Phone.StylePriority.UseTextAlignment = False
            '
            'XrLabel_Agency_Address2
            '
            Me.XrLabel_Agency_Address2.StylePriority.UseFont = False
            Me.XrLabel_Agency_Address2.StylePriority.UseTextAlignment = False
            '
            'XrControlStyle1
            '
            Me.XrControlStyle1.BackColor = System.Drawing.Color.Transparent
            Me.XrControlStyle1.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrControlStyle1.ForeColor = System.Drawing.Color.White
            Me.XrControlStyle1.Name = "XrControlStyle1"
            Me.XrControlStyle1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel1
            '
            Me.XrLabel1.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel1.ForeColor = System.Drawing.Color.Teal
            Me.XrLabel1.LocationFloat = New DevExpress.Utils.PointFloat(8.0!, 92.0!)
            Me.XrLabel1.Name = "XrLabel1"
            Me.XrLabel1.SizeF = New System.Drawing.SizeF(59.0!, 16.0!)
            Me.XrLabel1.Text = "Status:"
            '
            'XrLabel2
            '
            Me.XrLabel2.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel2.ForeColor = System.Drawing.Color.Teal
            Me.XrLabel2.LocationFloat = New DevExpress.Utils.PointFloat(8.0!, 108.0!)
            Me.XrLabel2.Name = "XrLabel2"
            Me.XrLabel2.SizeF = New System.Drawing.SizeF(92.0!, 16.0!)
            Me.XrLabel2.Text = "Date Created:"
            '
            'XrLabel3
            '
            Me.XrLabel3.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel3.ForeColor = System.Drawing.Color.Teal
            Me.XrLabel3.LocationFloat = New DevExpress.Utils.PointFloat(8.0!, 125.0!)
            Me.XrLabel3.Name = "XrLabel3"
            Me.XrLabel3.SizeF = New System.Drawing.SizeF(92.0!, 16.0!)
            Me.XrLabel3.Text = "Date Posted:"
            '
            'XrLabel_Header_DatePosted
            '
            Me.XrLabel_Header_DatePosted.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel_Header_DatePosted.ForeColor = System.Drawing.Color.Teal
            Me.XrLabel_Header_DatePosted.LocationFloat = New DevExpress.Utils.PointFloat(100.0!, 125.0!)
            Me.XrLabel_Header_DatePosted.Name = "XrLabel_Header_DatePosted"
            Me.XrLabel_Header_DatePosted.SizeF = New System.Drawing.SizeF(92.0!, 16.0!)
            Me.XrLabel_Header_DatePosted.Text = "MM/dd/yyyy"
            '
            'XrLabel_Header_DateCreated
            '
            Me.XrLabel_Header_DateCreated.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel_Header_DateCreated.ForeColor = System.Drawing.Color.Teal
            Me.XrLabel_Header_DateCreated.LocationFloat = New DevExpress.Utils.PointFloat(100.0!, 108.0!)
            Me.XrLabel_Header_DateCreated.Name = "XrLabel_Header_DateCreated"
            Me.XrLabel_Header_DateCreated.SizeF = New System.Drawing.SizeF(92.0!, 16.0!)
            Me.XrLabel_Header_DateCreated.Text = "MM/dd/yyyy"
            '
            'XrLabel_Header_Status
            '
            Me.XrLabel_Header_Status.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel_Header_Status.ForeColor = System.Drawing.Color.Teal
            Me.XrLabel_Header_Status.LocationFloat = New DevExpress.Utils.PointFloat(100.0!, 92.0!)
            Me.XrLabel_Header_Status.Name = "XrLabel_Header_Status"
            Me.XrLabel_Header_Status.SizeF = New System.Drawing.SizeF(92.0!, 16.0!)
            Me.XrLabel_Header_Status.Text = "OPEN"
            '
            'XrLabel4
            '
            Me.XrLabel4.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel4.ForeColor = System.Drawing.Color.Teal
            Me.XrLabel4.LocationFloat = New DevExpress.Utils.PointFloat(200.0!, 125.0!)
            Me.XrLabel4.Name = "XrLabel4"
            Me.XrLabel4.SizeF = New System.Drawing.SizeF(108.0!, 16.0!)
            Me.XrLabel4.Text = "Pull Schedule(s):"
            '
            'XrLabel_Header_Pull_Schedules
            '
            Me.XrLabel_Header_Pull_Schedules.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel_Header_Pull_Schedules.ForeColor = System.Drawing.Color.Teal
            Me.XrLabel_Header_Pull_Schedules.LocationFloat = New DevExpress.Utils.PointFloat(308.0!, 125.0!)
            Me.XrLabel_Header_Pull_Schedules.Name = "XrLabel_Header_Pull_Schedules"
            Me.XrLabel_Header_Pull_Schedules.SizeF = New System.Drawing.SizeF(442.0!, 16.0!)
            '
            'XrLabel6
            '
            Me.XrLabel6.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel6.ForeColor = System.Drawing.Color.Teal
            Me.XrLabel6.LocationFloat = New DevExpress.Utils.PointFloat(200.0!, 92.0!)
            Me.XrLabel6.Name = "XrLabel6"
            Me.XrLabel6.SizeF = New System.Drawing.SizeF(133.0!, 16.0!)
            Me.XrLabel6.Text = "Effective Entry Date:"
            '
            'XrLabel_Header_Effective_Date
            '
            Me.XrLabel_Header_Effective_Date.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel_Header_Effective_Date.ForeColor = System.Drawing.Color.Teal
            Me.XrLabel_Header_Effective_Date.LocationFloat = New DevExpress.Utils.PointFloat(333.0!, 92.0!)
            Me.XrLabel_Header_Effective_Date.Name = "XrLabel_Header_Effective_Date"
            Me.XrLabel_Header_Effective_Date.SizeF = New System.Drawing.SizeF(92.0!, 16.0!)
            Me.XrLabel_Header_Effective_Date.Text = "MM/dd/yyyy"
            '
            'XrLabel_Header_Settlement_Date
            '
            Me.XrLabel_Header_Settlement_Date.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel_Header_Settlement_Date.ForeColor = System.Drawing.Color.Teal
            Me.XrLabel_Header_Settlement_Date.LocationFloat = New DevExpress.Utils.PointFloat(333.0!, 108.0!)
            Me.XrLabel_Header_Settlement_Date.Name = "XrLabel_Header_Settlement_Date"
            Me.XrLabel_Header_Settlement_Date.SizeF = New System.Drawing.SizeF(92.0!, 16.0!)
            Me.XrLabel_Header_Settlement_Date.Text = "MM/dd/yyyy"
            '
            'XrLabel9
            '
            Me.XrLabel9.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel9.ForeColor = System.Drawing.Color.Teal
            Me.XrLabel9.LocationFloat = New DevExpress.Utils.PointFloat(200.0!, 108.0!)
            Me.XrLabel9.Name = "XrLabel9"
            Me.XrLabel9.SizeF = New System.Drawing.SizeF(133.0!, 16.0!)
            Me.XrLabel9.Text = "Settlement Date:"
            '
            'XrPanel1
            '
            Me.XrPanel1.BackColor = System.Drawing.Color.Teal
            Me.XrPanel1.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel13, Me.XrLabel12, Me.XrLabel11, Me.XrLabel10, Me.XrLabel8, Me.XrLabel7, Me.XrLabel5})
            Me.XrPanel1.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 150.0!)
            Me.XrPanel1.Name = "XrPanel1"
            Me.XrPanel1.SizeF = New System.Drawing.SizeF(800.0!, 17.0!)
            '
            'XrLabel13
            '
            Me.XrLabel13.BorderColor = System.Drawing.Color.Transparent
            Me.XrLabel13.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel13.ForeColor = System.Drawing.Color.White
            Me.XrLabel13.LocationFloat = New DevExpress.Utils.PointFloat(717.0!, 0.0!)
            Me.XrLabel13.Name = "XrLabel13"
            Me.XrLabel13.SizeF = New System.Drawing.SizeF(73.0!, 17.0!)
            Me.XrLabel13.Text = "AMOUNT"
            Me.XrLabel13.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel12
            '
            Me.XrLabel12.BorderColor = System.Drawing.Color.Transparent
            Me.XrLabel12.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel12.ForeColor = System.Drawing.Color.White
            Me.XrLabel12.LocationFloat = New DevExpress.Utils.PointFloat(475.0!, 0.0!)
            Me.XrLabel12.Name = "XrLabel12"
            Me.XrLabel12.SizeF = New System.Drawing.SizeF(183.0!, 17.0!)
            Me.XrLabel12.Text = "Client ID AND NAME"
            Me.XrLabel12.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            '
            'XrLabel11
            '
            Me.XrLabel11.BorderColor = System.Drawing.Color.Transparent
            Me.XrLabel11.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel11.ForeColor = System.Drawing.Color.White
            Me.XrLabel11.LocationFloat = New DevExpress.Utils.PointFloat(375.0!, 0.0!)
            Me.XrLabel11.Name = "XrLabel11"
            Me.XrLabel11.SizeF = New System.Drawing.SizeF(91.0!, 17.0!)
            Me.XrLabel11.Text = "AUTH"
            Me.XrLabel11.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            '
            'XrLabel10
            '
            Me.XrLabel10.BorderColor = System.Drawing.Color.Transparent
            Me.XrLabel10.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel10.ForeColor = System.Drawing.Color.White
            Me.XrLabel10.LocationFloat = New DevExpress.Utils.PointFloat(283.0!, 0.0!)
            Me.XrLabel10.Name = "XrLabel10"
            Me.XrLabel10.SizeF = New System.Drawing.SizeF(75.0!, 17.0!)
            Me.XrLabel10.Text = "ACCOUNT"
            Me.XrLabel10.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            '
            'XrLabel8
            '
            Me.XrLabel8.BorderColor = System.Drawing.Color.Transparent
            Me.XrLabel8.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel8.ForeColor = System.Drawing.Color.White
            Me.XrLabel8.LocationFloat = New DevExpress.Utils.PointFloat(200.0!, 0.0!)
            Me.XrLabel8.Name = "XrLabel8"
            Me.XrLabel8.SizeF = New System.Drawing.SizeF(75.0!, 17.0!)
            Me.XrLabel8.Text = "ROUTING"
            Me.XrLabel8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            '
            'XrLabel7
            '
            Me.XrLabel7.BorderColor = System.Drawing.Color.Transparent
            Me.XrLabel7.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel7.ForeColor = System.Drawing.Color.White
            Me.XrLabel7.LocationFloat = New DevExpress.Utils.PointFloat(83.0!, 0.0!)
            Me.XrLabel7.Name = "XrLabel7"
            Me.XrLabel7.SizeF = New System.Drawing.SizeF(108.0!, 17.0!)
            Me.XrLabel7.Text = "TRANSACTION"
            Me.XrLabel7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            '
            'XrLabel5
            '
            Me.XrLabel5.BorderColor = System.Drawing.Color.Transparent
            Me.XrLabel5.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel5.ForeColor = System.Drawing.Color.White
            Me.XrLabel5.LocationFloat = New DevExpress.Utils.PointFloat(8.0!, 0.0!)
            Me.XrLabel5.Name = "XrLabel5"
            Me.XrLabel5.SizeF = New System.Drawing.SizeF(71.0!, 17.0!)
            Me.XrLabel5.Text = "TRACE ID"
            Me.XrLabel5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'ReportFooter
            '
            Me.ReportFooter.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel25, Me.XrLabel_Summary_HOLD, Me.XrLabel_Summary_InsufficentFunds, Me.XrLabel22, Me.XrLabel_Summary_StopPayments, Me.XrLabel20, Me.XrLabel_Summary_Errors, Me.XrLabel18, Me.XrLabel_Summary_Prenotes, Me.XrLabel16, Me.XrLine2, Me.XrLabel15, Me.XrLabel_Total_Amount, Me.XrLine1, Me.XrLabel14})
            Me.ReportFooter.HeightF = 189.75!
            Me.ReportFooter.Name = "ReportFooter"
            '
            'XrLabel25
            '
            Me.XrLabel25.LocationFloat = New DevExpress.Utils.PointFloat(8.0!, 168.0!)
            Me.XrLabel25.Name = "XrLabel25"
            Me.XrLabel25.SizeF = New System.Drawing.SizeF(209.0!, 17.0!)
            Me.XrLabel25.Text = "Number of accounts put on HOLD"
            '
            'XrLabel_Summary_HOLD
            '
            Me.XrLabel_Summary_HOLD.LocationFloat = New DevExpress.Utils.PointFloat(217.0!, 168.0!)
            Me.XrLabel_Summary_HOLD.Name = "XrLabel_Summary_HOLD"
            Me.XrLabel_Summary_HOLD.SizeF = New System.Drawing.SizeF(125.0!, 17.0!)
            Me.XrLabel_Summary_HOLD.Text = "0"
            Me.XrLabel_Summary_HOLD.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel_Summary_InsufficentFunds
            '
            Me.XrLabel_Summary_InsufficentFunds.LocationFloat = New DevExpress.Utils.PointFloat(217.0!, 151.0!)
            Me.XrLabel_Summary_InsufficentFunds.Name = "XrLabel_Summary_InsufficentFunds"
            Me.XrLabel_Summary_InsufficentFunds.SizeF = New System.Drawing.SizeF(125.0!, 17.0!)
            Me.XrLabel_Summary_InsufficentFunds.Text = "0"
            Me.XrLabel_Summary_InsufficentFunds.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel22
            '
            Me.XrLabel22.LocationFloat = New DevExpress.Utils.PointFloat(8.0!, 151.0!)
            Me.XrLabel22.Name = "XrLabel22"
            Me.XrLabel22.SizeF = New System.Drawing.SizeF(184.0!, 17.0!)
            Me.XrLabel22.Text = "Number of Insufficient Funds"
            '
            'XrLabel_Summary_StopPayments
            '
            Me.XrLabel_Summary_StopPayments.LocationFloat = New DevExpress.Utils.PointFloat(217.0!, 134.0!)
            Me.XrLabel_Summary_StopPayments.Name = "XrLabel_Summary_StopPayments"
            Me.XrLabel_Summary_StopPayments.SizeF = New System.Drawing.SizeF(125.0!, 17.0!)
            Me.XrLabel_Summary_StopPayments.Text = "0"
            Me.XrLabel_Summary_StopPayments.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel20
            '
            Me.XrLabel20.LocationFloat = New DevExpress.Utils.PointFloat(8.0!, 134.0!)
            Me.XrLabel20.Name = "XrLabel20"
            Me.XrLabel20.SizeF = New System.Drawing.SizeF(184.0!, 17.0!)
            Me.XrLabel20.Text = "Number of Stop Payments"
            '
            'XrLabel_Summary_Errors
            '
            Me.XrLabel_Summary_Errors.LocationFloat = New DevExpress.Utils.PointFloat(217.0!, 117.0!)
            Me.XrLabel_Summary_Errors.Name = "XrLabel_Summary_Errors"
            Me.XrLabel_Summary_Errors.SizeF = New System.Drawing.SizeF(125.0!, 17.0!)
            Me.XrLabel_Summary_Errors.Text = "0"
            Me.XrLabel_Summary_Errors.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel18
            '
            Me.XrLabel18.LocationFloat = New DevExpress.Utils.PointFloat(8.0!, 117.0!)
            Me.XrLabel18.Name = "XrLabel18"
            Me.XrLabel18.SizeF = New System.Drawing.SizeF(184.0!, 17.0!)
            Me.XrLabel18.Text = "Number of Account Errors"
            '
            'XrLabel_Summary_Prenotes
            '
            Me.XrLabel_Summary_Prenotes.LocationFloat = New DevExpress.Utils.PointFloat(217.0!, 100.0!)
            Me.XrLabel_Summary_Prenotes.Name = "XrLabel_Summary_Prenotes"
            Me.XrLabel_Summary_Prenotes.SizeF = New System.Drawing.SizeF(125.0!, 17.0!)
            Me.XrLabel_Summary_Prenotes.Text = "0"
            Me.XrLabel_Summary_Prenotes.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel16
            '
            Me.XrLabel16.LocationFloat = New DevExpress.Utils.PointFloat(8.0!, 100.0!)
            Me.XrLabel16.Name = "XrLabel16"
            Me.XrLabel16.SizeF = New System.Drawing.SizeF(184.0!, 17.0!)
            Me.XrLabel16.Text = "Number of Prenote Requests"
            '
            'XrLine2
            '
            Me.XrLine2.LineWidth = 2
            Me.XrLine2.LocationFloat = New DevExpress.Utils.PointFloat(8.0!, 83.0!)
            Me.XrLine2.Name = "XrLine2"
            Me.XrLine2.SizeF = New System.Drawing.SizeF(333.0!, 9.0!)
            '
            'XrLabel15
            '
            Me.XrLabel15.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel15.LocationFloat = New DevExpress.Utils.PointFloat(8.0!, 67.0!)
            Me.XrLabel15.Name = "XrLabel15"
            Me.XrLabel15.SizeF = New System.Drawing.SizeF(117.0!, 16.0!)
            Me.XrLabel15.Text = "Statistics"
            '
            'XrLabel_Total_Amount
            '
            Me.XrLabel_Total_Amount.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "amount")})
            Me.XrLabel_Total_Amount.LocationFloat = New DevExpress.Utils.PointFloat(640.0!, 25.0!)
            Me.XrLabel_Total_Amount.Name = "XrLabel_Total_Amount"
            Me.XrLabel_Total_Amount.SizeF = New System.Drawing.SizeF(150.0!, 16.0!)
            XrSummary1.FormatString = "{0:c2}"
            XrSummary1.Running = DevExpress.XtraReports.UI.SummaryRunning.Report
            Me.XrLabel_Total_Amount.Summary = XrSummary1
            Me.XrLabel_Total_Amount.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLine1
            '
            Me.XrLine1.ForeColor = System.Drawing.Color.Teal
            Me.XrLine1.LineWidth = 2
            Me.XrLine1.LocationFloat = New DevExpress.Utils.PointFloat(8.0!, 17.0!)
            Me.XrLine1.Name = "XrLine1"
            Me.XrLine1.SizeF = New System.Drawing.SizeF(784.0!, 8.0!)
            '
            'XrLabel14
            '
            Me.XrLabel14.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.XrLabel14.ForeColor = System.Drawing.Color.Teal
            Me.XrLabel14.LocationFloat = New DevExpress.Utils.PointFloat(8.0!, 25.0!)
            Me.XrLabel14.Name = "XrLabel14"
            Me.XrLabel14.SizeF = New System.Drawing.SizeF(200.0!, 17.0!)
            Me.XrLabel14.Text = "GRAND TOTAL AMOUNT"
            '
            'XrLabel_Amount
            '
            Me.XrLabel_Amount.CanGrow = False
            Me.XrLabel_Amount.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "amount", "{0:c2}")})
            Me.XrLabel_Amount.KeepTogether = True
            Me.XrLabel_Amount.LocationFloat = New DevExpress.Utils.PointFloat(725.0!, 0.0!)
            Me.XrLabel_Amount.Name = "XrLabel_Amount"
            Me.XrLabel_Amount.SizeF = New System.Drawing.SizeF(65.0!, 16.0!)
            Me.XrLabel_Amount.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel_TraceID
            '
            Me.XrLabel_TraceID.CanGrow = False
            Me.XrLabel_TraceID.KeepTogether = True
            Me.XrLabel_TraceID.LocationFloat = New DevExpress.Utils.PointFloat(8.0!, 0.0!)
            Me.XrLabel_TraceID.Name = "XrLabel_TraceID"
            Me.XrLabel_TraceID.SizeF = New System.Drawing.SizeF(71.0!, 17.0!)
            Me.XrLabel_TraceID.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel_Transaction
            '
            Me.XrLabel_Transaction.CanGrow = False
            Me.XrLabel_Transaction.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "transaction_code")})
            Me.XrLabel_Transaction.KeepTogether = True
            Me.XrLabel_Transaction.LocationFloat = New DevExpress.Utils.PointFloat(83.0!, 0.0!)
            Me.XrLabel_Transaction.Name = "XrLabel_Transaction"
            Me.XrLabel_Transaction.SizeF = New System.Drawing.SizeF(108.0!, 17.0!)
            Me.XrLabel_Transaction.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            '
            'XrLabel_Routing
            '
            Me.XrLabel_Routing.CanGrow = False
            Me.XrLabel_Routing.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "ach_routing_number")})
            Me.XrLabel_Routing.KeepTogether = True
            Me.XrLabel_Routing.LocationFloat = New DevExpress.Utils.PointFloat(200.0!, 0.0!)
            Me.XrLabel_Routing.Name = "XrLabel_Routing"
            Me.XrLabel_Routing.SizeF = New System.Drawing.SizeF(75.0!, 17.0!)
            Me.XrLabel_Routing.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            '
            'XrLabel_Account
            '
            Me.XrLabel_Account.CanGrow = False
            Me.XrLabel_Account.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "ach_account_number")})
            Me.XrLabel_Account.KeepTogether = True
            Me.XrLabel_Account.LocationFloat = New DevExpress.Utils.PointFloat(283.0!, 0.0!)
            Me.XrLabel_Account.Name = "XrLabel_Account"
            Me.XrLabel_Account.SizeF = New System.Drawing.SizeF(83.0!, 17.0!)
            Me.XrLabel_Account.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            '
            'XrLabel_Auth
            '
            Me.XrLabel_Auth.CanGrow = False
            Me.XrLabel_Auth.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "ach_authentication_code")})
            Me.XrLabel_Auth.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel_Auth.ForeColor = System.Drawing.Color.DarkRed
            Me.XrLabel_Auth.KeepTogether = True
            Me.XrLabel_Auth.LocationFloat = New DevExpress.Utils.PointFloat(383.0!, 0.0!)
            Me.XrLabel_Auth.Name = "XrLabel_Auth"
            Me.XrLabel_Auth.SizeF = New System.Drawing.SizeF(83.0!, 17.0!)
            Me.XrLabel_Auth.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            '
            'XrLabel_ClientID
            '
            Me.XrLabel_ClientID.CanGrow = False
            Me.XrLabel_ClientID.KeepTogether = True
            Me.XrLabel_ClientID.LocationFloat = New DevExpress.Utils.PointFloat(467.0!, 0.0!)
            Me.XrLabel_ClientID.Name = "XrLabel_ClientID"
            Me.XrLabel_ClientID.SizeF = New System.Drawing.SizeF(66.0!, 17.0!)
            Me.XrLabel_ClientID.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel_ClientName
            '
            Me.XrLabel_ClientName.CanGrow = False
            Me.XrLabel_ClientName.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "client_name")})
            Me.XrLabel_ClientName.KeepTogether = True
            Me.XrLabel_ClientName.LocationFloat = New DevExpress.Utils.PointFloat(533.0!, 0.0!)
            Me.XrLabel_ClientName.Name = "XrLabel_ClientName"
            Me.XrLabel_ClientName.SizeF = New System.Drawing.SizeF(183.0!, 17.0!)
            Me.XrLabel_ClientName.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            '
            'ACHTransactionsReport
            '
            Me.Bands.AddRange(New DevExpress.XtraReports.UI.Band() {Me.Detail, Me.PageHeader, Me.PageFooter, Me.TopMarginBand1, Me.BottomMarginBand1, Me.ReportFooter})
            Me.StyleSheet.AddRange(New DevExpress.XtraReports.UI.XRControlStyle() {Me.XrControlStyle_Header, Me.XrControlStyle_Totals, Me.XrControlStyle_HeaderPannel, Me.XrControlStyle_GroupHeader, Me.XrControlStyle1})
            Me.Version = "11.2"
            Me.Controls.SetChildIndex(Me.ReportFooter, 0)
            Me.Controls.SetChildIndex(Me.BottomMarginBand1, 0)
            Me.Controls.SetChildIndex(Me.TopMarginBand1, 0)
            Me.Controls.SetChildIndex(Me.PageFooter, 0)
            Me.Controls.SetChildIndex(Me.PageHeader, 0)
            Me.Controls.SetChildIndex(Me.Detail, 0)
            CType(Me, System.ComponentModel.ISupportInitialize).EndInit()

        End Sub

#End Region

        ''' <summary>
        ''' Function to read the parameters from the user
        ''' </summary>
        Public Overrides Function RequestReportParameters() As System.Windows.Forms.DialogResult
            Dim answer As System.Windows.Forms.DialogResult = DialogResult.OK
            If Parameter_DepositBatchID < 1 Then
                With New AchTransactionsParametersForm()
                    .Parameter_DepositBatchID = Parameter_DepositBatchID
                    answer = .ShowDialog()
                    If answer = DialogResult.OK Then
                        Parameter_DepositBatchID = .Parameter_DepositBatchID
                    End If
                    .Dispose()
                End With
            End If
            Return answer
        End Function

        ' Read the list of the transactions for this batch
        Dim summary_prenotes As System.Int32 = 0
        Dim summary_errors As System.Int32 = 0
        Dim summary_stoppayments As System.Int32 = 0
        Dim Summary_InsufficentFunds As System.Int32 = 0
        Dim summary_hold As System.Int32 = 0

        Dim ds As New System.Data.DataSet("ds")

        ''' <summary>
        ''' Configure the report's datasource as needed
        ''' </summary>
        Private Sub ACHTransactionsReport_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs)
            Const TableName As String = "report_details"
            Dim tbl As System.Data.DataTable = ds.Tables(TableName)

            ' Always read the batch header
            ReadBatchHeader()

            ' If there is no dataset then read it again.
            If tbl Is Nothing Then

                ' Generate the dataset for the results
                Using cmd As SqlClient.SqlCommand = New System.Data.SqlClient.SqlCommand()
                    With cmd
                        .Connection = New System.Data.SqlClient.SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
                        .CommandText = "rpt_ACH_Transaction_Detail"
                        .CommandType = CommandType.StoredProcedure

                        .Parameters.Add("@FileID", SqlDbType.Int).Value = Parameter_DepositBatchID
                        .CommandTimeout = 0
                    End With

                    Using da As New SqlClient.SqlDataAdapter(cmd)
                        da.Fill(ds, TableName)
                        tbl = ds.Tables(TableName)
                    End Using
                End Using
            End If

            If tbl IsNot Nothing Then
                DataSource = New System.Data.DataView(tbl, ReportFilter.ViewFilter, String.Empty, DataViewRowState.CurrentRows)

                summary_prenotes = 0
                summary_errors = 0
                summary_stoppayments = 0
                Summary_InsufficentFunds = 0
                summary_hold = 0
            End If

        End Sub

        ''' <summary>
        ''' Read the header information for the batch
        ''' </summary>
        Private Sub ReadBatchHeader()
            Dim cn As SqlClient.SqlConnection = New System.Data.SqlClient.SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
            Dim rd As SqlClient.SqlDataReader = Nothing

            Dim header_status As String = System.String.Empty
            Dim header_date_created As String = System.String.Empty
            Dim header_date_posted As String = System.String.Empty
            Dim header_effective_date As String = System.String.Empty
            Dim header_settlement_date As String = System.String.Empty
            Dim header_pull_schedules As String = System.String.Empty

            Try
                cn.Open()
                With New System.Data.SqlClient.SqlCommand()
                    .Connection = cn
                    .CommandText = "rpt_ACH_Transaction_Header"
                    .CommandType = CommandType.StoredProcedure
                    .Parameters.Add(New SqlClient.SqlParameter("@FileID", SqlDbType.Int)).Value = Parameter_DepositBatchID
                    rd = .ExecuteReader(CommandBehavior.SingleRow Or CommandBehavior.CloseConnection)
                End With

                If rd.Read Then
                    Dim col As System.Int32
                    col = rd.GetOrdinal("status") : If Not rd.IsDBNull(col) Then header_status = rd.GetString(col).Trim()
                    col = rd.GetOrdinal("date_created") : If Not rd.IsDBNull(col) Then header_date_created = rd.GetDateTime(col).ToShortDateString
                    col = rd.GetOrdinal("date_posted") : If Not rd.IsDBNull(col) Then header_date_posted = rd.GetDateTime(col).ToShortDateString
                    col = rd.GetOrdinal("effective_entry_date") : If Not rd.IsDBNull(col) Then header_effective_date = rd.GetDateTime(col).ToShortDateString
                    col = rd.GetOrdinal("settlement_date") : If Not rd.IsDBNull(col) Then header_settlement_date = rd.GetDateTime(col).ToShortDateString
                    col = rd.GetOrdinal("pull_date") : If Not rd.IsDBNull(col) Then header_pull_schedules = Convert.ToString(rd.GetValue(col)).Trim()
                End If
                rd.Close()

            Finally
                If rd IsNot Nothing Then rd.Dispose()
                If cn IsNot Nothing Then cn.Dispose()
            End Try

            ' Supply the headers
            XrLabel_Header_DateCreated.Text = header_date_created
            XrLabel_Header_DatePosted.Text = header_date_posted
            XrLabel_Header_Effective_Date.Text = header_effective_date
            XrLabel_Header_Pull_Schedules.Text = header_pull_schedules
            XrLabel_Header_Settlement_Date.Text = header_settlement_date
        End Sub

        Private Sub XrLabel_ClientID_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs)
            With XrLabel_ClientID
                .Text = String.Format("{0:0000000}", Convert.ToInt32(GetCurrentColumnValue("client")))
            End With
        End Sub

        Private Sub XrLabel_TraceID_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs)
            Dim objTraceID As Object = GetCurrentColumnValue("trace_number")
            With XrLabel_TraceID
                If objTraceID Is Nothing OrElse (objTraceID Is System.DBNull.Value) Then
                    .Text = System.String.Empty
                Else
                    Dim strTraceID As String = Convert.ToString(objTraceID).Trim()
                    If strTraceID.Length > 7 Then strTraceID = strTraceID.Substring(strTraceID.Length - 7)
                    .Text = strTraceID
                End If
            End With
        End Sub

        Private Sub XrLabel_TraceID_AfterPrint(ByVal sender As Object, ByVal e As System.EventArgs)

            ' Determine the error codes
            Dim objTemp As Object

            ' Look at the transaction code for a prenote operation
            objTemp = GetCurrentColumnValue("transaction_code")
            If objTemp IsNot Nothing AndAlso objTemp IsNot System.DBNull.Value Then
                If Convert.ToString(objTemp).ToLower().EndsWith("pre") Then summary_prenotes += 1
            End If

            ' Look at the serious errors to count the number of clients placed on a hold status
            objTemp = GetCurrentColumnValue("serious_error")
            If objTemp IsNot Nothing AndAlso objTemp IsNot System.DBNull.Value Then
                If Convert.ToBoolean(objTemp) Then summary_hold += 1
            End If

            ' Look at the authentication code to determine the value for the error counts
            objTemp = GetCurrentColumnValue("authentication_code")
            If objTemp Is Nothing OrElse (objTemp Is System.DBNull.Value) Then Return
            If Convert.ToString(objTemp).Trim() = System.String.Empty Then Return

            ' Count the number of errors
            Select Case Convert.ToString(objTemp).Substring(0, 3)
                Case "C01", "C02", "C03", "C04", "R02"
                    summary_errors += 1
                Case "R01"
                    Summary_InsufficentFunds += 1
                Case "R08", "R10"
                    summary_stoppayments += 1
            End Select
        End Sub

        Private Sub XrLabel_Summary_Errors_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs)
            With XrLabel_Summary_Errors
                .Text = System.String.Format("{0:d}", summary_errors)
            End With
        End Sub

        Private Sub XrLabel_Summary_HOLD_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs)
            With XrLabel_Summary_HOLD
                .Text = System.String.Format("{0:d}", summary_hold)
            End With
        End Sub

        Private Sub XrLabel_Summary_InsufficentFunds_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs)
            With XrLabel_Summary_InsufficentFunds
                .Text = System.String.Format("{0:d}", Summary_InsufficentFunds)
            End With
        End Sub

        Private Sub XrLabel_Summary_Prenotes_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs)
            With XrLabel_Summary_Prenotes
                .Text = System.String.Format("{0:d}", summary_prenotes)
            End With
        End Sub

        Private Sub XrLabel_Summary_StopPayments_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs)
            With XrLabel_Summary_StopPayments
                .Text = System.String.Format("{0:d}", summary_stoppayments)
            End With
        End Sub
    End Class

End Namespace
