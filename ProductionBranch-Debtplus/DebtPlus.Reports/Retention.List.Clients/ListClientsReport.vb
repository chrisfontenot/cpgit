#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}
#End Region

Imports System.Reflection
Imports System.IO
Imports DebtPlus.Utils

Namespace Retention.List.Clients
    Public Class ListClientsReport
        Inherits Template.TemplateXtraReportClass

        Public Sub New()
            MyBase.new()

            Const ReportName As String = "DebtPlus.Reports.Retention.List.Clients.repx"

            ' See if there is a report reference that we can use
            Dim Fname As String = System.IO.Path.Combine(DebtPlus.Configuration.Config.ReportsDirectory, ReportName)
            Dim UseDefault As Boolean = True
            Try
                If (Not DebtPlus.Configuration.Config.LocalReportsOnly) AndAlso System.IO.File.Exists(Fname) Then
                    LoadLayout(Fname)
                    UseDefault = False
                End If
            Catch ex As Exception
            End Try

            ' Load the standard report definition if we need a new item
            If UseDefault Then
                Dim asm As Assembly = Assembly.GetExecutingAssembly                                                    ' changed
                Using ios As Stream = asm.GetManifestResourceStream(String.Format("DebtPlus.Reports.{0}", ReportName)) ' changed
                    If ios IsNot Nothing Then LoadLayout(ios) ' changed
                End Using
            End If

            ' Set the script references as needed
            ScriptReferences = DebtPlus.Utils.AssemblyTools.GetAllSiblingAssemblies()

            ' Copy the script references to the subreports as well
            For Each Band As DevExpress.XtraReports.UI.Band In Bands
                For Each ctl As DevExpress.XtraReports.UI.XRControl In Band.Controls
                    Dim rpt As DevExpress.XtraReports.UI.XRSubreport = TryCast(ctl, DevExpress.XtraReports.UI.XRSubreport)
                    If rpt IsNot Nothing Then
                        Dim SubRpt As DevExpress.XtraReports.UI.XtraReport = TryCast(rpt.ReportSource, DevExpress.XtraReports.UI.XtraReport)
                        If SubRpt IsNot Nothing Then
                            SubRpt.ScriptReferences = ScriptReferences
                        End If
                    End If
                Next
            Next

            ' Ensure that the creditor parameter is empty
            Parameter_SelectionCriteria = String.Empty

            ' Enable the select expert
            ReportFilter.IsEnabled = True
        End Sub


        ''' <summary>
        ''' Process the report title information
        ''' </summary>
        Public Overrides ReadOnly Property ReportTitle() As String
            Get
                Return "Client Retention Events"
            End Get
        End Property

        Public Property Parameter_SelectionCriteria() As String
            Get
                Return CType(Parameters("ParameterSelectionCriteria").Value, String)
            End Get
            Set(ByVal Value As String)
                Parameters("ParameterSelectionCriteria").Value = Value
            End Set
        End Property

        Public Overrides Sub SetReportParameter(ByVal Parameter As String, ByVal Value As Object)
            Select Case Parameter
                Case "SelectionCriteria"
                    Parameter_SelectionCriteria = Convert.ToString(Value)
                Case Else
                    MyBase.SetReportParameter(Parameter, Value)
            End Select
        End Sub
    End Class
End Namespace
