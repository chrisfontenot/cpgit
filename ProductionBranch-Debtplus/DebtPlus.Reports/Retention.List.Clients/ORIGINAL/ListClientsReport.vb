#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}
#End Region

Option Compare Binary
Option Explicit On 
Option Strict On

Imports DevExpress.XtraReports.UI

Namespace Retention.List.Clients
    Public Class ListClientsReport

        ''' <summary>
        '''     Create an instance of our report
        ''' </summary>
        Public Sub New()
            MyBase.New()
            InitializeComponent()

            '-- Register the event handlers
            AddHandler BeforePrint, AddressOf ListClientsReport_BeforePrint
            AddHandler XrSubreport_action.BeforePrint, AddressOf XrSubreport_action_BeforePrint
            AddHandler XrTableCell_client_id.BeforePrint, AddressOf XrTableCell_client_id_BeforePrint

            '-- Enable the report filter
            ReportFilter.IsEnabled = True
        End Sub


        ''' <summary>
        '''     Process the report title information
        ''' </summary>
        Public Overrides ReadOnly Property ReportTitle() As String
            Get
                Return "Client Retention Events"
            End Get
        End Property


        ''' <summary>
        '''     Selection criteria for the events
        ''' </summary>
        Public Property Parameter_SelectionCriteria() As String
            Get
                Return CType(Parameters("ParameterSelectionCriteria").Value, String)
            End Get
            Set(ByVal Value As String)
                Parameters("ParameterSelectionCriteria").Value = Value
            End Set
        End Property

        Public Overrides Sub SetReportParameter(ByVal Parameter As String, ByVal Value As Object)
            Select Case Parameter
                Case "SelectionCriteria"
                    Parameter_SelectionCriteria = Convert.ToString(Value)
                Case Else
                    MyBase.SetReportParameter(Parameter, Value)
            End Select
        End Sub

        Private ReadOnly ds As New System.Data.DataSet("ds")

        ''' <summary>
        '''     Generate the report information
        ''' </summary>
        Private Sub ListClientsReport_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs)
            Const TableName As String = "events"
            Dim rpt As DevExpress.XtraReports.UI.XtraReport = CType(sender, DevExpress.XtraReports.UI.XtraReport)
            Dim tbl As System.Data.DataTable = ds.Tables(TableName)

            If tbl Is Nothing Then
                Dim sqlInfo As DebtPlus.LINQ.SQLInfoClass = DebtPlus.LINQ.SQLInfoClass.getDefault()
                Using cn As New System.Data.SqlClient.SqlConnection(sqlInfo.ConnectionString)
                    cn.Open()

                    Dim SelectionCritiera As String = Convert.ToString(rpt.Parameters("ParameterSelectionCriteria").Value).Trim
                    Using cmd As New System.Data.SqlClient.SqlCommand()
                        cmd.Connection = cn
                        If SelectionCritiera <> String.Empty Then
                            cmd.CommandText = SelectionCritiera
                        Else
                            cmd.CommandText = "SELECT TOP 100 * FROM view_retention_client_events"
                        End If
                        cmd.CommandType = System.Data.CommandType.Text

                        Using da As New System.Data.SqlClient.SqlDataAdapter(cmd)
                            da.Fill(ds, TableName)
                            tbl = ds.Tables(TableName)
                        End Using
                    End Using
                End Using
            End If

            If tbl IsNot Nothing Then
                rpt.DataSource = New System.Data.DataView(tbl, CType(rpt, DebtPlus.Interfaces.Reports.IReportFilter).ReportFilter.ViewFilter, "client, date_created desc", System.Data.DataViewRowState.CurrentRows)
                For Each calc As DevExpress.XtraReports.UI.CalculatedField In rpt.CalculatedFields
                    calc.Assign(rpt.DataSource, rpt.DataMember)
                Next
            End If
        End Sub

        ''' <summary>
        '''     Format the client ID and name
        ''' </summary>
        Private Sub XrTableCell_client_id_BeforePrint(ByVal sender As System.Object, ByVal e As System.Drawing.Printing.PrintEventArgs)
            With CType(sender, DevExpress.XtraReports.UI.XRLabel)
                Dim rpt As DevExpress.XtraReports.UI.XtraReport = CType(.Report, DevExpress.XtraReports.UI.XtraReport)
                .Text = DebtPlus.Utils.Format.Client.FormatClientID(rpt.GetCurrentColumnValue("client"))
            End With
        End Sub

        ''' <summary>
        '''     Print the detail information as a subreport
        ''' </summary>
        Private Sub XrSubreport_action_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs)
            With CType(sender, DevExpress.XtraReports.UI.XRSubreport)
                Dim MasterRpt As DevExpress.XtraReports.UI.XtraReport = CType(.Report, DevExpress.XtraReports.UI.XtraReport)
                Dim rpt As DevExpress.XtraReports.UI.XtraReport = CType(.ReportSource, DevExpress.XtraReports.UI.XtraReport)
                Dim tbl As System.Data.DataTable = ds.Tables("actions")
                If tbl IsNot Nothing Then ds.Tables.Remove(tbl)

                '-- Process the items from the report, not the pre-scan list
                Dim CurrentEvent As Object = MasterRpt.GetCurrentColumnValue("client_retention_event")
                If CurrentEvent IsNot Nothing Then
                    Dim sqlInfo As DebtPlus.LINQ.SQLInfoClass = DebtPlus.LINQ.SQLInfoClass.getDefault()
                    Using cn As New System.Data.SqlClient.SqlConnection(sqlInfo.ConnectionString)
                        cn.Open()
                        Using cmd As New System.Data.SqlClient.SqlCommand()
                            cmd.Connection = cn
                            cmd.CommandText = "SELECT * FROM view_retention_client_actions WHERE client_retention_event=@client_retention_event"
                            cmd.CommandType = System.Data.CommandType.Text
                            cmd.Parameters.Add("@client_retention_event", System.Data.SqlDbType.Int).Value = CurrentEvent

                            Using da As New System.Data.SqlClient.SqlDataAdapter(cmd)
                                da.Fill(ds, "actions")
                                tbl = ds.Tables("actions")
                                If tbl.Rows.Count > 0 Then
                                    rpt.DataSource = New System.Data.DataView(tbl, String.Format("[client_retention_event]={0:f0}", CurrentEvent), "date_created", System.Data.DataViewRowState.CurrentRows)
                                    Return
                                End If
                            End Using
                        End Using
                    End Using
                End If
            End With

            e.Cancel = True
        End Sub
    End Class
End Namespace
