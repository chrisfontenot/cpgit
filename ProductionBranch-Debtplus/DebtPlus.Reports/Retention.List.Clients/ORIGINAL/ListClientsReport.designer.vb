Namespace Retention.List.Clients
    <Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
    Partial Class ListClientsReport
        Inherits DebtPlus.Reports.Template.TemplateXtraReportClass

        'Component overrides dispose to clean up the component list.
        Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
            If disposing Then
                If components IsNot Nothing Then
                    components.Dispose()
                End If
            End If
            MyBase.Dispose(disposing)
        End Sub

        'Required by the Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Designer
        'It can be modified using the Designer.
        'Do not modify it using the code editor.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
            Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(ListClientsReport))
            Me.GroupHeader1 = New DevExpress.XtraReports.UI.GroupHeaderBand()
            Me.XrLabel_created_by = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_date_created = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_event_amount = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_event_description = New DevExpress.XtraReports.UI.XRLabel()
            Me.GroupHeader2 = New DevExpress.XtraReports.UI.GroupHeaderBand()
            Me.XrPanel1 = New DevExpress.XtraReports.UI.XRPanel()
            Me.XrLabel3a = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel4a = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel5a = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel2a = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel1a = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrTable1 = New DevExpress.XtraReports.UI.XRTable()
            Me.XrTableRow1 = New DevExpress.XtraReports.UI.XRTableRow()
            Me.XrTableCell1 = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableCell_client_id = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableCell4 = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableCell_active_status = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableRow2 = New DevExpress.XtraReports.UI.XRTableRow()
            Me.XrTableCell5 = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableCell_name = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableCell7 = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableCell_home_ph = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableRow4 = New DevExpress.XtraReports.UI.XRTableRow()
            Me.XrTableCell13 = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableCell_coname = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableCell15 = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableCell_work_ph = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableRow3 = New DevExpress.XtraReports.UI.XRTableRow()
            Me.XrTableCell9 = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableCell10 = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableCell11 = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableCell_message_ph = New DevExpress.XtraReports.UI.XRTableCell()
            Me.GroupFooter1 = New DevExpress.XtraReports.UI.GroupFooterBand()
            Me.XrLine1 = New DevExpress.XtraReports.UI.XRLine()
            Me.ParameterSelectionCriteria = New DevExpress.XtraReports.Parameters.Parameter()
            Me.XrSubreport_action = New DevExpress.XtraReports.UI.XRSubreport()
            Me.EventActionList1 = New DebtPlus.Reports.Retention.List.Clients.EventActionList()
            CType(Me.XrTable1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.EventActionList1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
            '
            'PageHeader
            '
            Me.PageHeader.HeightF = 89.125!
            '
            'XrLabel_Title
            '
            Me.XrLabel_Title.StylePriority.UseFont = False
            '
            'XrLabel_Subtitle
            '
            Me.XrLabel_Subtitle.LocationFloat = New DevExpress.Utils.PointFloat(249.6667!, 67.00001!)
            Me.XrLabel_Subtitle.SizeF = New System.Drawing.SizeF(227.75!, 17.0!)
            Me.XrLabel_Subtitle.Visible = False
            '
            'XrPageInfo_PageNumber
            '
            Me.XrPageInfo_PageNumber.LocationFloat = New DevExpress.Utils.PointFloat(917.0!, 8.0!)
            Me.XrPageInfo_PageNumber.StylePriority.UseTextAlignment = False
            '
            'XrPanel_AgencyAddress
            '
            Me.XrPanel_AgencyAddress.LocationFloat = New DevExpress.Utils.PointFloat(742.0!, 0.0!)
            '
            'XRLabel_Agency_Name
            '
            Me.XRLabel_Agency_Name.StylePriority.UseTextAlignment = False
            '
            'XrLabel_Agency_Address3
            '
            Me.XrLabel_Agency_Address3.StylePriority.UseFont = False
            Me.XrLabel_Agency_Address3.StylePriority.UseTextAlignment = False
            '
            'XrLabel_Agency_Address1
            '
            Me.XrLabel_Agency_Address1.StylePriority.UseFont = False
            Me.XrLabel_Agency_Address1.StylePriority.UseTextAlignment = False
            '
            'XrLabel_Agency_Phone
            '
            Me.XrLabel_Agency_Phone.StylePriority.UseFont = False
            Me.XrLabel_Agency_Phone.StylePriority.UseTextAlignment = False
            '
            'XrLabel_Agency_Address2
            '
            Me.XrLabel_Agency_Address2.StylePriority.UseFont = False
            Me.XrLabel_Agency_Address2.StylePriority.UseTextAlignment = False
            '
            'Detail
            '
            Me.Detail.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrSubreport_action, Me.XrLabel_event_amount, Me.XrLabel_event_description, Me.XrLabel_date_created, Me.XrLabel_created_by})
            Me.Detail.HeightF = 15.0!
            Me.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            '
            'GroupHeader1
            '
            Me.GroupHeader1.GroupFields.AddRange(New DevExpress.XtraReports.UI.GroupField() {New DevExpress.XtraReports.UI.GroupField("client_retention_event", DevExpress.XtraReports.UI.XRColumnSortOrder.Ascending)})
            Me.GroupHeader1.HeightF = 0.0!
            Me.GroupHeader1.Name = "GroupHeader1"
            '
            'XrLabel_created_by
            '
            Me.XrLabel_created_by.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "created_by")})
            Me.XrLabel_created_by.LocationFloat = New DevExpress.Utils.PointFloat(292.7083!, 0.0!)
            Me.XrLabel_created_by.Name = "XrLabel_created_by"
            Me.XrLabel_created_by.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_created_by.SizeF = New System.Drawing.SizeF(123.9583!, 15.0!)
            Me.XrLabel_created_by.StylePriority.UseTextAlignment = False
            Me.XrLabel_created_by.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
            '
            'XrLabel_date_created
            '
            Me.XrLabel_date_created.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "date_created", "{0:d}")})
            Me.XrLabel_date_created.LocationFloat = New DevExpress.Utils.PointFloat(416.6666!, 0.0!)
            Me.XrLabel_date_created.Name = "XrLabel_date_created"
            Me.XrLabel_date_created.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_date_created.SizeF = New System.Drawing.SizeF(86.79169!, 14.99999!)
            Me.XrLabel_date_created.StylePriority.UseTextAlignment = False
            Me.XrLabel_date_created.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
            '
            'XrLabel_event_amount
            '
            Me.XrLabel_event_amount.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "event_amount", "{0:c}")})
            Me.XrLabel_event_amount.LocationFloat = New DevExpress.Utils.PointFloat(503.4583!, 0.0!)
            Me.XrLabel_event_amount.Name = "XrLabel_event_amount"
            Me.XrLabel_event_amount.Padding = New DevExpress.XtraPrinting.PaddingInfo(4, 4, 0, 0, 100.0!)
            Me.XrLabel_event_amount.SizeF = New System.Drawing.SizeF(73.95834!, 14.99999!)
            Me.XrLabel_event_amount.StylePriority.UsePadding = False
            Me.XrLabel_event_amount.StylePriority.UseTextAlignment = False
            Me.XrLabel_event_amount.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
            '
            'XrLabel_event_description
            '
            Me.XrLabel_event_description.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "event_description")})
            Me.XrLabel_event_description.LocationFloat = New DevExpress.Utils.PointFloat(12.5!, 0.0!)
            Me.XrLabel_event_description.Name = "XrLabel_event_description"
            Me.XrLabel_event_description.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_event_description.SizeF = New System.Drawing.SizeF(280.2083!, 14.99999!)
            Me.XrLabel_event_description.StylePriority.UseTextAlignment = False
            Me.XrLabel_event_description.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
            '
            'GroupHeader2
            '
            Me.GroupHeader2.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrPanel1, Me.XrTable1})
            Me.GroupHeader2.GroupFields.AddRange(New DevExpress.XtraReports.UI.GroupField() {New DevExpress.XtraReports.UI.GroupField("client", DevExpress.XtraReports.UI.XRColumnSortOrder.Ascending)})
            Me.GroupHeader2.HeightF = 97.91666!
            Me.GroupHeader2.Level = 1
            Me.GroupHeader2.Name = "GroupHeader2"
            Me.GroupHeader2.PageBreak = DevExpress.XtraReports.UI.PageBreak.BeforeBand
            Me.GroupHeader2.RepeatEveryPage = True
            '
            'XrPanel1
            '
            Me.XrPanel1.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel3a, Me.XrLabel4a, Me.XrLabel5a, Me.XrLabel2a, Me.XrLabel1a})
            Me.XrPanel1.LocationFloat = New DevExpress.Utils.PointFloat(12.5!, 70.5!)
            Me.XrPanel1.Name = "XrPanel1"
            Me.XrPanel1.SizeF = New System.Drawing.SizeF(1034.0!, 17.0!)
            Me.XrPanel1.StyleName = "XrControlStyle_HeaderPannel"
            '
            'XrLabel3a
            '
            Me.XrLabel3a.LocationFloat = New DevExpress.Utils.PointFloat(404.17!, 1.0!)
            Me.XrLabel3a.Name = "XrLabel3a"
            Me.XrLabel3a.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
            Me.XrLabel3a.SizeF = New System.Drawing.SizeF(86.79169!, 14.99999!)
            Me.XrLabel3a.StylePriority.UsePadding = False
            Me.XrLabel3a.Text = "DATE"
            '
            'XrLabel4a
            '
            Me.XrLabel4a.LocationFloat = New DevExpress.Utils.PointFloat(490.96!, 1.0!)
            Me.XrLabel4a.Name = "XrLabel4a"
            Me.XrLabel4a.Padding = New DevExpress.XtraPrinting.PaddingInfo(4, 4, 0, 0, 100.0!)
            Me.XrLabel4a.SizeF = New System.Drawing.SizeF(73.95834!, 14.99999!)
            Me.XrLabel4a.StylePriority.UsePadding = False
            Me.XrLabel4a.Text = "AMOUNT"
            '
            'XrLabel5a
            '
            Me.XrLabel5a.LocationFloat = New DevExpress.Utils.PointFloat(564.92!, 1.0!)
            Me.XrLabel5a.Name = "XrLabel5a"
            Me.XrLabel5a.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
            Me.XrLabel5a.SizeF = New System.Drawing.SizeF(459.0833!, 14.99999!)
            Me.XrLabel5a.StylePriority.UsePadding = False
            Me.XrLabel5a.StylePriority.UseTextAlignment = False
            Me.XrLabel5a.Text = "ACTION"
            Me.XrLabel5a.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
            '
            'XrLabel2a
            '
            Me.XrLabel2a.LocationFloat = New DevExpress.Utils.PointFloat(280.21!, 1.0!)
            Me.XrLabel2a.Name = "XrLabel2a"
            Me.XrLabel2a.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
            Me.XrLabel2a.SizeF = New System.Drawing.SizeF(123.9583!, 14.99999!)
            Me.XrLabel2a.StylePriority.UsePadding = False
            Me.XrLabel2a.StylePriority.UseTextAlignment = False
            Me.XrLabel2a.Text = "CREATED BY"
            Me.XrLabel2a.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
            '
            'XrLabel1a
            '
            Me.XrLabel1a.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 1.0!)
            Me.XrLabel1a.Name = "XrLabel1a"
            Me.XrLabel1a.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
            Me.XrLabel1a.SizeF = New System.Drawing.SizeF(280.2083!, 14.99999!)
            Me.XrLabel1a.StylePriority.UsePadding = False
            Me.XrLabel1a.StylePriority.UseTextAlignment = False
            Me.XrLabel1a.Text = "DESCRIPTION"
            Me.XrLabel1a.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
            '
            'XrTable1
            '
            Me.XrTable1.LocationFloat = New DevExpress.Utils.PointFloat(12.5!, 0.0!)
            Me.XrTable1.Name = "XrTable1"
            Me.XrTable1.Padding = New DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 100.0!)
            Me.XrTable1.Rows.AddRange(New DevExpress.XtraReports.UI.XRTableRow() {Me.XrTableRow1, Me.XrTableRow2, Me.XrTableRow4, Me.XrTableRow3})
            Me.XrTable1.SizeF = New System.Drawing.SizeF(1032.0!, 60.0!)
            Me.XrTable1.StylePriority.UsePadding = False
            '
            'XrTableRow1
            '
            Me.XrTableRow1.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.XrTableCell1, Me.XrTableCell_client_id, Me.XrTableCell4, Me.XrTableCell_active_status})
            Me.XrTableRow1.Name = "XrTableRow1"
            Me.XrTableRow1.Weight = 1.0R
            '
            'XrTableCell1
            '
            Me.XrTableCell1.Name = "XrTableCell1"
            Me.XrTableCell1.Padding = New DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 100.0!)
            Me.XrTableCell1.StylePriority.UsePadding = False
            Me.XrTableCell1.StylePriority.UseTextAlignment = False
            Me.XrTableCell1.Text = "CLIENT:"
            Me.XrTableCell1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
            Me.XrTableCell1.Weight = 0.40952043755109924R
            '
            'XrTableCell_client_id
            '
            Me.XrTableCell_client_id.Name = "XrTableCell_client_id"
            Me.XrTableCell_client_id.Scripts.OnBeforePrint = "XrTableCell_client_id_BeforePrint"
            Me.XrTableCell_client_id.Weight = 1.30244667585506R
            '
            'XrTableCell4
            '
            Me.XrTableCell4.Name = "XrTableCell4"
            Me.XrTableCell4.Padding = New DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 100.0!)
            Me.XrTableCell4.StylePriority.UsePadding = False
            Me.XrTableCell4.StylePriority.UseTextAlignment = False
            Me.XrTableCell4.Text = "ACTIVE STATUS:"
            Me.XrTableCell4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
            Me.XrTableCell4.Weight = 0.581758366074673R
            '
            'XrTableCell_active_status
            '
            Me.XrTableCell_active_status.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "active_status")})
            Me.XrTableCell_active_status.Name = "XrTableCell_active_status"
            Me.XrTableCell_active_status.Weight = 0.70627452051916784R
            '
            'XrTableRow2
            '
            Me.XrTableRow2.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.XrTableCell5, Me.XrTableCell_name, Me.XrTableCell7, Me.XrTableCell_home_ph})
            Me.XrTableRow2.Name = "XrTableRow2"
            Me.XrTableRow2.Weight = 1.0R
            '
            'XrTableCell5
            '
            Me.XrTableCell5.Name = "XrTableCell5"
            Me.XrTableCell5.Padding = New DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 100.0!)
            Me.XrTableCell5.StylePriority.UsePadding = False
            Me.XrTableCell5.StylePriority.UseTextAlignment = False
            Me.XrTableCell5.Text = "NAME:"
            Me.XrTableCell5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
            Me.XrTableCell5.Weight = 0.40952043755109924R
            '
            'XrTableCell_name
            '
            Me.XrTableCell_name.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "name")})
            Me.XrTableCell_name.Name = "XrTableCell_name"
            Me.XrTableCell_name.Weight = 1.30244667585506R
            '
            'XrTableCell7
            '
            Me.XrTableCell7.Name = "XrTableCell7"
            Me.XrTableCell7.Padding = New DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 100.0!)
            Me.XrTableCell7.StylePriority.UsePadding = False
            Me.XrTableCell7.StylePriority.UseTextAlignment = False
            Me.XrTableCell7.Text = "PHONE:"
            Me.XrTableCell7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
            Me.XrTableCell7.Weight = 0.581758366074673R
            '
            'XrTableCell_home_ph
            '
            Me.XrTableCell_home_ph.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "home_ph")})
            Me.XrTableCell_home_ph.Name = "XrTableCell_home_ph"
            Me.XrTableCell_home_ph.Weight = 0.70627452051916784R
            '
            'XrTableRow4
            '
            Me.XrTableRow4.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.XrTableCell13, Me.XrTableCell_coname, Me.XrTableCell15, Me.XrTableCell_work_ph})
            Me.XrTableRow4.Name = "XrTableRow4"
            Me.XrTableRow4.Weight = 1.0R
            '
            'XrTableCell13
            '
            Me.XrTableCell13.Name = "XrTableCell13"
            Me.XrTableCell13.Padding = New DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 100.0!)
            Me.XrTableCell13.StylePriority.UsePadding = False
            Me.XrTableCell13.StylePriority.UseTextAlignment = False
            Me.XrTableCell13.Text = "CO-APP:"
            Me.XrTableCell13.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
            Me.XrTableCell13.Weight = 0.40952043755109924R
            '
            'XrTableCell_coname
            '
            Me.XrTableCell_coname.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "coname")})
            Me.XrTableCell_coname.Name = "XrTableCell_coname"
            Me.XrTableCell_coname.Weight = 1.30244667585506R
            '
            'XrTableCell15
            '
            Me.XrTableCell15.Name = "XrTableCell15"
            Me.XrTableCell15.Padding = New DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 100.0!)
            Me.XrTableCell15.StylePriority.UsePadding = False
            Me.XrTableCell15.StylePriority.UseTextAlignment = False
            Me.XrTableCell15.Text = "WORK PH:"
            Me.XrTableCell15.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
            Me.XrTableCell15.Weight = 0.581758366074673R
            '
            'XrTableCell_work_ph
            '
            Me.XrTableCell_work_ph.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "work_ph")})
            Me.XrTableCell_work_ph.Name = "XrTableCell_work_ph"
            Me.XrTableCell_work_ph.Weight = 0.70627452051916784R
            '
            'XrTableRow3
            '
            Me.XrTableRow3.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.XrTableCell9, Me.XrTableCell10, Me.XrTableCell11, Me.XrTableCell_message_ph})
            Me.XrTableRow3.Name = "XrTableRow3"
            Me.XrTableRow3.Weight = 1.0R
            '
            'XrTableCell9
            '
            Me.XrTableCell9.Name = "XrTableCell9"
            Me.XrTableCell9.Padding = New DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 100.0!)
            Me.XrTableCell9.StylePriority.UsePadding = False
            Me.XrTableCell9.StylePriority.UseTextAlignment = False
            Me.XrTableCell9.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
            Me.XrTableCell9.Weight = 0.40952043755109924R
            '
            'XrTableCell10
            '
            Me.XrTableCell10.Name = "XrTableCell10"
            Me.XrTableCell10.Weight = 1.30244667585506R
            '
            'XrTableCell11
            '
            Me.XrTableCell11.Name = "XrTableCell11"
            Me.XrTableCell11.Padding = New DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 100.0!)
            Me.XrTableCell11.StylePriority.UsePadding = False
            Me.XrTableCell11.StylePriority.UseTextAlignment = False
            Me.XrTableCell11.Text = "MSG PH:"
            Me.XrTableCell11.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
            Me.XrTableCell11.Weight = 0.581758366074673R
            '
            'XrTableCell_message_ph
            '
            Me.XrTableCell_message_ph.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "message_ph")})
            Me.XrTableCell_message_ph.Name = "XrTableCell_message_ph"
            Me.XrTableCell_message_ph.Weight = 0.70627452051916784R
            '
            'GroupFooter1
            '
            Me.GroupFooter1.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLine1})
            Me.GroupFooter1.HeightF = 23.5417!
            Me.GroupFooter1.Name = "GroupFooter1"
            '
            'XrLine1
            '
            Me.XrLine1.ForeColor = System.Drawing.Color.Maroon
            Me.XrLine1.LineWidth = 2
            Me.XrLine1.LocationFloat = New DevExpress.Utils.PointFloat(337.5!, 10.0!)
            Me.XrLine1.Name = "XrLine1"
            Me.XrLine1.SizeF = New System.Drawing.SizeF(372.9167!, 4.166666!)
            Me.XrLine1.StylePriority.UseForeColor = False
            '
            'ParameterSelectionCriteria
            '
            Me.ParameterSelectionCriteria.Description = "Selection Criteria"
            Me.ParameterSelectionCriteria.Name = "ParameterSelectionCriteria"
            Me.ParameterSelectionCriteria.Value = ""
            Me.ParameterSelectionCriteria.Visible = False
            '
            'XrSubreport_action
            '
            Me.XrSubreport_action.LocationFloat = New DevExpress.Utils.PointFloat(577.4167!, 0.0!)
            Me.XrSubreport_action.Name = "XrSubreport_action"
            Me.XrSubreport_action.ReportSource = Me.EventActionList1
            Me.XrSubreport_action.Scripts.OnBeforePrint = "XrSubreport_action_BeforePrint"
            Me.XrSubreport_action.SizeF = New System.Drawing.SizeF(464.5833!, 14.99999!)
            '
            'ListClientsReport
            '
            Me.Bands.AddRange(New DevExpress.XtraReports.UI.Band() {Me.Detail, Me.PageHeader, Me.PageFooter, Me.TopMarginBand1, Me.BottomMarginBand1, Me.GroupHeader1, Me.GroupHeader2, Me.GroupFooter1})
            Me.Landscape = True
            Me.PageHeight = 850
            Me.PageWidth = 1100
            Me.Parameters.AddRange(New DevExpress.XtraReports.Parameters.Parameter() {Me.ParameterSelectionCriteria})
            Me.ReportPrintOptions.PrintOnEmptyDataSource = False
            Me.RequestParameters = False
            Me.ScriptLanguage = DevExpress.XtraReports.ScriptLanguage.VisualBasic
            Me.ScriptReferencesString = resources.GetString("$this.ScriptReferencesString")
            Me.Scripts.OnBeforePrint = "ListClientsReport_BeforePrint"
            Me.ScriptsSource = resources.GetString("$this.ScriptsSource")
            Me.StyleSheet.AddRange(New DevExpress.XtraReports.UI.XRControlStyle() {Me.XrControlStyle_Header, Me.XrControlStyle_Totals, Me.XrControlStyle_HeaderPannel, Me.XrControlStyle_GroupHeader})
            Me.Version = "11.2"
            Me.Controls.SetChildIndex(Me.GroupFooter1, 0)
            Me.Controls.SetChildIndex(Me.GroupHeader2, 0)
            Me.Controls.SetChildIndex(Me.GroupHeader1, 0)
            Me.Controls.SetChildIndex(Me.BottomMarginBand1, 0)
            Me.Controls.SetChildIndex(Me.TopMarginBand1, 0)
            Me.Controls.SetChildIndex(Me.PageFooter, 0)
            Me.Controls.SetChildIndex(Me.PageHeader, 0)
            Me.Controls.SetChildIndex(Me.Detail, 0)
            CType(Me.XrTable1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.EventActionList1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me, System.ComponentModel.ISupportInitialize).EndInit()

        End Sub
        Friend WithEvents XrLabel_event_amount As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_event_description As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_date_created As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_created_by As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents GroupHeader1 As DevExpress.XtraReports.UI.GroupHeaderBand
        Friend WithEvents GroupHeader2 As DevExpress.XtraReports.UI.GroupHeaderBand
        Friend WithEvents XrPanel1 As DevExpress.XtraReports.UI.XRPanel
        Friend WithEvents XrLabel3a As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel4a As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel5a As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel2a As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel1a As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrTable1 As DevExpress.XtraReports.UI.XRTable
        Friend WithEvents XrTableRow1 As DevExpress.XtraReports.UI.XRTableRow
        Friend WithEvents XrTableCell1 As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents XrTableCell_client_id As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents XrTableCell4 As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents XrTableCell_active_status As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents XrTableRow2 As DevExpress.XtraReports.UI.XRTableRow
        Friend WithEvents XrTableCell5 As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents XrTableCell_name As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents XrTableCell7 As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents XrTableCell_home_ph As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents XrTableRow4 As DevExpress.XtraReports.UI.XRTableRow
        Friend WithEvents XrTableCell13 As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents XrTableCell_coname As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents XrTableCell15 As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents XrTableCell_work_ph As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents XrTableRow3 As DevExpress.XtraReports.UI.XRTableRow
        Friend WithEvents XrTableCell9 As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents XrTableCell10 As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents XrTableCell11 As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents XrTableCell_message_ph As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents GroupFooter1 As DevExpress.XtraReports.UI.GroupFooterBand
        Friend WithEvents XrLine1 As DevExpress.XtraReports.UI.XRLine
        Friend WithEvents XrSubreport_action As DevExpress.XtraReports.UI.XRSubreport
        Friend WithEvents ParameterSelectionCriteria As DevExpress.XtraReports.Parameters.Parameter
        Private WithEvents EventActionList1 As DebtPlus.Reports.Retention.List.Clients.EventActionList
    End Class
End Namespace
