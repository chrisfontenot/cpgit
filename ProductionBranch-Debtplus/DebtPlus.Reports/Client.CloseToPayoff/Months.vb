#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}
#End Region

Option Compare Binary
Option Explicit On
Option Strict On

Namespace Client.CloseToPayoff
    Friend Module Months

        ''' <summary>
        ''' Translate the month value to a suitable string
        ''' </summary>
        Friend Function MonthsString(ByVal Months As System.Int32) As String
            Dim Answer As String = String.Empty

            Select Case Months
                Case 0
                    Answer = "Less than $100"
                Case 1
                    Answer = "One to Two Months"
                Case 2
                    Answer = "Two to Three Months"
                Case 3
                    Answer = "Three to Four Months"
                Case 4
                    Answer = "Four to Five Months"
                Case 5
                    Answer = "Five to Six Months"
                Case 6
                    Answer = "Six to Seven Months"
                Case 7
                    Answer = "Seven to Eight Months"
                Case Else
                    Answer = "More than Eight Months"
            End Select

            Return Answer
        End Function
    End Module
End Namespace
