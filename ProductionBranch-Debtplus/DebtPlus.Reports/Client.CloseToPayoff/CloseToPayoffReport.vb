#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}

#End Region

Option Compare Binary
Option Explicit On
Option Strict On

Imports DebtPlus.Reports.Template
Imports System.Drawing.Printing
Imports DebtPlus.Reports.Template.Forms
Imports System.Data.SqlClient
Imports System.Threading
Imports DebtPlus.Interfaces.Reports

Namespace Client.CloseToPayoff
    Public Class CloseToPayoffReport
        Inherits TemplateXtraReportClass

        Public Sub New()
            MyBase.New()
            InitializeComponent()

            AddHandler BeforePrint, AddressOf Report_BeforePrint
            AddHandler XrLabel_Type.BeforePrint, AddressOf XrLabel_Type_BeforePrint
            AddHandler XrLabel_Count.PreviewClick, AddressOf XrLabel_Count_PreviewClick
            AddHandler XrLabel_Balance.PreviewClick, AddressOf XrLabel_Count_PreviewClick
            AddHandler XrLabel_Type.PreviewClick, AddressOf XrLabel_Count_PreviewClick

            ReportFilter.IsEnabled = True
        End Sub

        Public Property Parameter_Counselor() As Int32
            Get
                Return DebtPlus.Utils.Nulls.DInt(Parameters("ParameterCounselor").Value, -1)
            End Get
            Set(ByVal Value As Int32)
                Parameters("ParameterCounselor").Value = Value
            End Set
        End Property

        Public Overrides Sub SetReportParameter(ByVal Parameter As String, ByVal Value As Object)
            Select Case Parameter
                Case "Counselor"
                    If Not (TypeOf Value Is Int32) Then
                        Parameter_Counselor = -1
                    Else
                        Parameter_Counselor = Convert.ToInt32(Value)
                    End If
                Case Else
                    MyBase.SetReportParameter(Parameter, Value)
            End Select
        End Sub

        Public Overrides Function RequestReportParameters() As DialogResult
            Dim answer As DialogResult
            Using frm As New CounselorParametersForm(True)
                answer = frm.ShowDialog()
                SetReportParameter("Counselor", frm.Parameter_Counselor)
            End Using
            Return answer
        End Function

        Public Overrides ReadOnly Property ReportTitle() As String
            Get
                Return "Close to Payoff"
            End Get
        End Property

        Public Overrides ReadOnly Property ReportSubTitle() As String
            Get
                If Parameter_Counselor <= 0 Then
                    Return "This report shows all counselors"
                Else
                    Return String.Format("This report shows counselor # {0:f0}", Parameter_Counselor)
                End If
            End Get
        End Property

#Region " Component Designer generated code "

        'Component overrides dispose to clean up the component list.
        Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
            If disposing Then
                If components IsNot Nothing Then
                    components.Dispose()
                End If
            End If
            MyBase.Dispose(disposing)
        End Sub

        'Required by the Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Designer
        'It can be modified using the Designer.
        'Do not modify it using the code editor.
        Friend WithEvents GroupHeader1 As DevExpress.XtraReports.UI.GroupHeaderBand
        Friend WithEvents GroupFooter1 As DevExpress.XtraReports.UI.GroupFooterBand
        Friend WithEvents ReportFooter As DevExpress.XtraReports.UI.ReportFooterBand
        Friend WithEvents XrLabel8 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_Type As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_Count As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_Balance As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_Total_Balance As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_Total_Count As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLine1 As DevExpress.XtraReports.UI.XRLine
        Friend WithEvents XrLabel_detail As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrPanel1 As DevExpress.XtraReports.UI.XRPanel
        Friend WithEvents XrLabel3 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel2 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel1 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents ParameterCounselor As DevExpress.XtraReports.Parameters.Parameter

        <System.Diagnostics.DebuggerStepThrough()>
        Private Sub InitializeComponent()
            Dim XrSummary1 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary()
            Dim XrSummary2 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary()
            Dim XrSummary3 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary()
            Dim XrSummary4 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary()
            Me.GroupHeader1 = New DevExpress.XtraReports.UI.GroupHeaderBand()
            Me.GroupFooter1 = New DevExpress.XtraReports.UI.GroupFooterBand()
            Me.XrLabel_Balance = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_Type = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_Count = New DevExpress.XtraReports.UI.XRLabel()
            Me.ReportFooter = New DevExpress.XtraReports.UI.ReportFooterBand()
            Me.XrLine1 = New DevExpress.XtraReports.UI.XRLine()
            Me.XrLabel_Total_Count = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_Total_Balance = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel8 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_detail = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrPanel1 = New DevExpress.XtraReports.UI.XRPanel()
            Me.XrLabel3 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel2 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel1 = New DevExpress.XtraReports.UI.XRLabel()
            Me.ParameterCounselor = New DevExpress.XtraReports.Parameters.Parameter()
            CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
            '
            'Detail
            '
            Me.Detail.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel_detail})
            Me.Detail.HeightF = 17.0!
            Me.Detail.Visible = False
            '
            'PageHeader
            '
            Me.PageHeader.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrPanel1})
            Me.PageHeader.HeightF = 141.0!
            Me.PageHeader.Controls.SetChildIndex(Me.XrPanel1, 0)
            Me.PageHeader.Controls.SetChildIndex(Me.XrPanel_AgencyAddress, 0)
            Me.PageHeader.Controls.SetChildIndex(Me.XrLabel_Title, 0)
            Me.PageHeader.Controls.SetChildIndex(Me.XrLine_Title, 0)
            Me.PageHeader.Controls.SetChildIndex(Me.XrLabel_Subtitle, 0)
            Me.PageHeader.Controls.SetChildIndex(Me.XrPageInfo1, 0)
            '
            'XrLabel_Title
            '
            Me.XrLabel_Title.StylePriority.UseFont = False
            '
            'XrPageInfo_PageNumber
            '
            Me.XrPageInfo_PageNumber.StylePriority.UseTextAlignment = False
            '
            'XRLabel_Agency_Name
            '
            Me.XRLabel_Agency_Name.StylePriority.UseTextAlignment = False
            '
            'XrLabel_Agency_Address3
            '
            Me.XrLabel_Agency_Address3.StylePriority.UseFont = False
            Me.XrLabel_Agency_Address3.StylePriority.UseTextAlignment = False
            '
            'XrLabel_Agency_Address1
            '
            Me.XrLabel_Agency_Address1.StylePriority.UseFont = False
            Me.XrLabel_Agency_Address1.StylePriority.UseTextAlignment = False
            '
            'XrLabel_Agency_Phone
            '
            Me.XrLabel_Agency_Phone.StylePriority.UseFont = False
            Me.XrLabel_Agency_Phone.StylePriority.UseTextAlignment = False
            '
            'XrLabel_Agency_Address2
            '
            Me.XrLabel_Agency_Address2.StylePriority.UseFont = False
            Me.XrLabel_Agency_Address2.StylePriority.UseTextAlignment = False
            '
            'GroupHeader1
            '
            Me.GroupHeader1.GroupFields.AddRange(New DevExpress.XtraReports.UI.GroupField() {New DevExpress.XtraReports.UI.GroupField("months_to_payoff", DevExpress.XtraReports.UI.XRColumnSortOrder.Ascending)})
            Me.GroupHeader1.HeightF = 0.0!
            Me.GroupHeader1.Name = "GroupHeader1"
            Me.GroupHeader1.RepeatEveryPage = True
            '
            'GroupFooter1
            '
            Me.GroupFooter1.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel_Balance, Me.XrLabel_Type, Me.XrLabel_Count})
            Me.GroupFooter1.HeightF = 15.0!
            Me.GroupFooter1.Name = "GroupFooter1"
            '
            'XrLabel_Balance
            '
            Me.XrLabel_Balance.CanGrow = False
            Me.XrLabel_Balance.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "balance"), New DevExpress.XtraReports.UI.XRBinding("Tag", Nothing, "months_to_payoff")})
            Me.XrLabel_Balance.LocationFloat = New DevExpress.Utils.PointFloat(567.0!, 0.0!)
            Me.XrLabel_Balance.Name = "XrLabel_Balance"
            Me.XrLabel_Balance.SizeF = New System.Drawing.SizeF(179.0!, 15.0!)
            XrSummary1.FormatString = "{0:c}"
            XrSummary1.Running = DevExpress.XtraReports.UI.SummaryRunning.Group
            Me.XrLabel_Balance.Summary = XrSummary1
            Me.XrLabel_Balance.Text = "XrLabel_Balance"
            Me.XrLabel_Balance.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
            Me.XrLabel_Balance.WordWrap = False
            '
            'XrLabel_Type
            '
            Me.XrLabel_Type.CanGrow = False
            Me.XrLabel_Type.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Tag", Nothing, "months_to_payoff")})
            Me.XrLabel_Type.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
            Me.XrLabel_Type.Name = "XrLabel_Type"
            Me.XrLabel_Type.SizeF = New System.Drawing.SizeF(417.0!, 15.0!)
            Me.XrLabel_Type.WordWrap = False
            '
            'XrLabel_Count
            '
            Me.XrLabel_Count.CanGrow = False
            Me.XrLabel_Count.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "months_to_payoff"), New DevExpress.XtraReports.UI.XRBinding("Tag", Nothing, "months_to_payoff")})
            Me.XrLabel_Count.LocationFloat = New DevExpress.Utils.PointFloat(425.0!, 0.0!)
            Me.XrLabel_Count.Name = "XrLabel_Count"
            Me.XrLabel_Count.SizeF = New System.Drawing.SizeF(122.0!, 15.0!)
            XrSummary2.FormatString = "{0:n0}"
            XrSummary2.Func = DevExpress.XtraReports.UI.SummaryFunc.Count
            XrSummary2.Running = DevExpress.XtraReports.UI.SummaryRunning.Group
            Me.XrLabel_Count.Summary = XrSummary2
            Me.XrLabel_Count.Text = "XrLabel_Count"
            Me.XrLabel_Count.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
            Me.XrLabel_Count.WordWrap = False
            '
            'ReportFooter
            '
            Me.ReportFooter.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLine1, Me.XrLabel_Total_Count, Me.XrLabel_Total_Balance, Me.XrLabel8})
            Me.ReportFooter.HeightF = 32.0!
            Me.ReportFooter.Name = "ReportFooter"
            '
            'XrLine1
            '
            Me.XrLine1.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 8.0!)
            Me.XrLine1.Name = "XrLine1"
            Me.XrLine1.SizeF = New System.Drawing.SizeF(750.0!, 9.0!)
            '
            'XrLabel_Total_Count
            '
            Me.XrLabel_Total_Count.CanGrow = False
            Me.XrLabel_Total_Count.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "months_to_payoff")})
            Me.XrLabel_Total_Count.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel_Total_Count.LocationFloat = New DevExpress.Utils.PointFloat(425.0!, 17.0!)
            Me.XrLabel_Total_Count.Name = "XrLabel_Total_Count"
            Me.XrLabel_Total_Count.SizeF = New System.Drawing.SizeF(122.0!, 15.0!)
            XrSummary3.FormatString = "{0:n0}"
            XrSummary3.Func = DevExpress.XtraReports.UI.SummaryFunc.Count
            XrSummary3.Running = DevExpress.XtraReports.UI.SummaryRunning.Report
            Me.XrLabel_Total_Count.Summary = XrSummary3
            Me.XrLabel_Total_Count.Text = "XrLabel5"
            Me.XrLabel_Total_Count.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
            Me.XrLabel_Total_Count.WordWrap = False
            '
            'XrLabel_Total_Balance
            '
            Me.XrLabel_Total_Balance.CanGrow = False
            Me.XrLabel_Total_Balance.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "balance")})
            Me.XrLabel_Total_Balance.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel_Total_Balance.LocationFloat = New DevExpress.Utils.PointFloat(567.0!, 17.0!)
            Me.XrLabel_Total_Balance.Name = "XrLabel_Total_Balance"
            Me.XrLabel_Total_Balance.SizeF = New System.Drawing.SizeF(179.0!, 15.0!)
            XrSummary4.FormatString = "{0:c}"
            XrSummary4.Running = DevExpress.XtraReports.UI.SummaryRunning.Report
            Me.XrLabel_Total_Balance.Summary = XrSummary4
            Me.XrLabel_Total_Balance.Text = "XrLabel6"
            Me.XrLabel_Total_Balance.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
            Me.XrLabel_Total_Balance.WordWrap = False
            '
            'XrLabel8
            '
            Me.XrLabel8.CanGrow = False
            Me.XrLabel8.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel8.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 17.0!)
            Me.XrLabel8.Name = "XrLabel8"
            Me.XrLabel8.SizeF = New System.Drawing.SizeF(417.0!, 15.0!)
            Me.XrLabel8.Text = "Totals"
            Me.XrLabel8.WordWrap = False
            '
            'XrLabel_detail
            '
            Me.XrLabel_detail.CanGrow = False
            Me.XrLabel_detail.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "months_to_payoff"), New DevExpress.XtraReports.UI.XRBinding("Tag", Nothing, "months_to_payoff")})
            Me.XrLabel_detail.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
            Me.XrLabel_detail.Name = "XrLabel_detail"
            Me.XrLabel_detail.SizeF = New System.Drawing.SizeF(100.0!, 17.0!)
            Me.XrLabel_detail.WordWrap = False
            '
            'XrPanel1
            '
            Me.XrPanel1.BackColor = System.Drawing.Color.Teal
            Me.XrPanel1.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel3, Me.XrLabel2, Me.XrLabel1})
            Me.XrPanel1.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 125.0!)
            Me.XrPanel1.Name = "XrPanel1"
            Me.XrPanel1.SizeF = New System.Drawing.SizeF(750.0!, 16.0!)
            '
            'XrLabel3
            '
            Me.XrLabel3.CanGrow = False
            Me.XrLabel3.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel3.ForeColor = System.Drawing.Color.White
            Me.XrLabel3.LocationFloat = New DevExpress.Utils.PointFloat(567.0!, 0.0!)
            Me.XrLabel3.Name = "XrLabel3"
            Me.XrLabel3.SizeF = New System.Drawing.SizeF(179.0!, 15.0!)
            Me.XrLabel3.Text = "BALANCE"
            Me.XrLabel3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
            Me.XrLabel3.WordWrap = False
            '
            'XrLabel2
            '
            Me.XrLabel2.CanGrow = False
            Me.XrLabel2.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel2.ForeColor = System.Drawing.Color.White
            Me.XrLabel2.LocationFloat = New DevExpress.Utils.PointFloat(433.0!, 0.0!)
            Me.XrLabel2.Name = "XrLabel2"
            Me.XrLabel2.SizeF = New System.Drawing.SizeF(114.0!, 15.0!)
            Me.XrLabel2.Text = "COUNT"
            Me.XrLabel2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
            Me.XrLabel2.WordWrap = False
            '
            'XrLabel1
            '
            Me.XrLabel1.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel1.ForeColor = System.Drawing.Color.White
            Me.XrLabel1.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
            Me.XrLabel1.Name = "XrLabel1"
            Me.XrLabel1.SizeF = New System.Drawing.SizeF(417.0!, 15.0!)
            Me.XrLabel1.Text = "CATEGORY"
            '
            'ParameterCounselor
            '
            Me.ParameterCounselor.Description = "Counselor ID"
            Me.ParameterCounselor.Name = "ParameterCounselor"
            Me.ParameterCounselor.Type = GetType(Int32)
            Me.ParameterCounselor.Value = -1
            Me.ParameterCounselor.Visible = False
            '
            'CloseToPayoffReport
            '
            Me.Bands.AddRange(New DevExpress.XtraReports.UI.Band() {Me.Detail, Me.PageHeader, Me.PageFooter, Me.TopMarginBand1, Me.BottomMarginBand1, Me.GroupHeader1, Me.GroupFooter1, Me.ReportFooter})
            Me.Parameters.AddRange(New DevExpress.XtraReports.Parameters.Parameter() {Me.ParameterCounselor})
            Me.RequestParameters = False
            Me.StyleSheet.AddRange(New DevExpress.XtraReports.UI.XRControlStyle() {Me.XrControlStyle_Header, Me.XrControlStyle_Totals, Me.XrControlStyle_HeaderPannel, Me.XrControlStyle_GroupHeader})
            Me.Version = "11.2"
            Me.Controls.SetChildIndex(Me.ReportFooter, 0)
            Me.Controls.SetChildIndex(Me.GroupFooter1, 0)
            Me.Controls.SetChildIndex(Me.GroupHeader1, 0)
            Me.Controls.SetChildIndex(Me.BottomMarginBand1, 0)
            Me.Controls.SetChildIndex(Me.TopMarginBand1, 0)
            Me.Controls.SetChildIndex(Me.PageFooter, 0)
            Me.Controls.SetChildIndex(Me.PageHeader, 0)
            Me.Controls.SetChildIndex(Me.Detail, 0)
            CType(Me, System.ComponentModel.ISupportInitialize).EndInit()
        End Sub

#End Region

        Dim ds As New DataSet("ds")
        Const TableName As String = "rpt_close_to_payoff"

        Private Sub Report_BeforePrint(ByVal sender As Object, ByVal e As EventArgs)
            Dim rpt As DevExpress.XtraReports.UI.XtraReport = CType(sender, DevExpress.XtraReports.UI.XtraReport)

            Dim tbl As DataTable = ds.Tables(TableName)
            If tbl Is Nothing Then
                Dim cmd As New SqlCommand()
                With cmd
                    .Connection = New System.Data.SqlClient.SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
                    .CommandText = "rpt_close_to_payoff"
                    .CommandType = CommandType.StoredProcedure
                    .CommandTimeout = 0

                    If Parameter_Counselor > 0 Then
                        .Parameters.Add("@counselor", SqlDbType.Int).Value = Parameter_Counselor
                    End If
                    .CommandTimeout = 0
                End With

                Using da As New SqlDataAdapter(cmd)
                    da.Fill(ds, TableName)
                    tbl = ds.Tables(TableName)
                End Using
            End If

            If tbl IsNot Nothing Then
                rpt.DataSource = New DataView(tbl, CType(rpt, DebtPlus.Interfaces.Reports.IReportFilter).ReportFilter.ViewFilter, "months_to_payoff, client", DataViewRowState.CurrentRows)
                For Each calc As DevExpress.XtraReports.UI.CalculatedField In rpt.CalculatedFields
                    calc.Assign(rpt.DataSource, rpt.DataMember)
                Next
            End If
        End Sub

        Private Sub XrLabel_Count_PreviewClick(ByVal sender As Object, ByVal e As DevExpress.XtraReports.UI.PreviewMouseEventArgs)
            With CType(sender, DevExpress.XtraReports.UI.XRLabel)
                If e.Brick.Value IsNot Nothing AndAlso e.Brick.Value IsNot DBNull.Value Then
                    Dim Months As Int32 = Convert.ToInt32(e.Brick.Value)
                    If Months >= 0 Then
                        Dim thrd As New Thread(New ParameterizedThreadStart(AddressOf ShowReportThread))
                        With thrd
                            .SetApartmentState(ApartmentState.STA)
                            .Name = String.Format("subreport months {0:f0}", Months)
                            .IsBackground = True
                            .Start(Months)
                        End With
                    End If
                End If
            End With
        End Sub

        Dim MasterSelection As String
        Dim MonthSelection As String

        Private Sub ShowReportThread(ByVal param As Object)
            Dim months As Int32 = Convert.ToInt32(param)

            MasterSelection = ReportFilter.ViewFilter
            If MasterSelection <> String.Empty Then MasterSelection = String.Format(" AND ({0})", MasterSelection)
            MonthSelection = String.Format(" AND ([months_to_payoff]={0:f0})", months)

            Using rpt As New DetailSubreport()
                AddHandler rpt.GetSubTitleString, AddressOf GetSubReportSubTitle
                AddHandler rpt.BeforePrint, AddressOf SubReport_BeforePrint
                Using frm As New PrintPreviewForm(rpt)
                    frm.ShowDialog()
                End Using
                RemoveHandler rpt.GetSubTitleString, AddressOf GetSubReportSubTitle
                RemoveHandler rpt.BeforePrint, AddressOf SubReport_BeforePrint
            End Using
        End Sub

        Private Sub SubReport_BeforePrint(ByVal sender As Object, ByVal e As EventArgs)
            Dim rpt As DevExpress.XtraReports.UI.XtraReport = CType(sender, DevExpress.XtraReports.UI.XtraReport)
            With rpt
                Dim SubReportSelection As String = CType(rpt, DebtPlus.Interfaces.Reports.IReportFilter).ReportFilter.ViewFilter
                If SubReportSelection <> String.Empty Then SubReportSelection = String.Format(" AND {0}", SubReportSelection)
                Dim Selection As String = MasterSelection + MonthSelection + SubReportSelection
                If Selection <> String.Empty Then Selection = Selection.Substring(5)

                .DataSource = New DataView(ds.Tables(TableName), Selection, "months_to_payoff, client", DataViewRowState.CurrentRows)
            End With
        End Sub

        Private Sub GetSubReportSubTitle(ByVal Sender As Object, ByVal e As TitleStringEventArgs)
            e.Title = ReportSubTitle
        End Sub

        Private Sub XrLabel_Type_BeforePrint(ByVal sender As Object, ByVal e As PrintEventArgs)
            With CType(sender, DevExpress.XtraReports.UI.XRLabel)
                .Text = MonthsString(DebtPlus.Utils.Nulls.DInt(GetCurrentColumnValue("months_to_payoff")))
            End With
        End Sub
    End Class
End Namespace