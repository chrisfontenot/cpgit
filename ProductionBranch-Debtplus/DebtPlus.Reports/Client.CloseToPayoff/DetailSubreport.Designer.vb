Namespace Client.CloseToPayoff
    <Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
    Partial Class DetailSubreport

        'XtraReport overrides dispose to clean up the component list.
        <System.Diagnostics.DebuggerNonUserCode()> _
        Protected Overrides Sub Dispose(ByVal disposing As Boolean)
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
            MyBase.Dispose(disposing)
        End Sub

        'Required by the Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Designer
        'It can be modified using the Designer.  
        'Do not modify it using the code editor.
        <System.Diagnostics.DebuggerStepThrough()> _
        Private Sub InitializeComponent()
            Dim XrSummary1 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary()
            Me.XrLabel_months_to_payoff = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrPanel1 = New DevExpress.XtraReports.UI.XRPanel()
            Me.XrLabel4 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel3 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel2 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel1 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_balance = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_unverified_debts = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_disbursement = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_client_id_and_name = New DevExpress.XtraReports.UI.XRLabel()
            Me.ReportFooter = New DevExpress.XtraReports.UI.ReportFooterBand()
            Me.XrLabel10 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_total_balance = New DevExpress.XtraReports.UI.XRLabel()
            Me.GroupHeader1 = New DevExpress.XtraReports.UI.GroupHeaderBand()
            CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
            '
            'Detail
            '
            Me.Detail.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel_client_id_and_name, Me.XrLabel_disbursement, Me.XrLabel_unverified_debts, Me.XrLabel_balance})
            Me.Detail.HeightF = 15.0!
            '
            'PageHeader
            '
            Me.PageHeader.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrPanel1})
            Me.PageHeader.HeightF = 151.0!
            Me.PageHeader.Controls.SetChildIndex(Me.XrPanel1, 0)
            Me.PageHeader.Controls.SetChildIndex(Me.XrPanel_AgencyAddress, 0)
            Me.PageHeader.Controls.SetChildIndex(Me.XrLabel_Title, 0)
            Me.PageHeader.Controls.SetChildIndex(Me.XrLine_Title, 0)
            Me.PageHeader.Controls.SetChildIndex(Me.XrLabel_Subtitle, 0)
            Me.PageHeader.Controls.SetChildIndex(Me.XrPageInfo1, 0)
            '
            'XrLabel_Title
            '
            Me.XrLabel_Title.StylePriority.UseFont = False
            '
            'XrPageInfo_PageNumber
            '
            Me.XrPageInfo_PageNumber.StylePriority.UseTextAlignment = False
            '
            'XRLabel_Agency_Name
            '
            Me.XRLabel_Agency_Name.StylePriority.UseTextAlignment = False
            '
            'XrLabel_Agency_Address3
            '
            Me.XrLabel_Agency_Address3.StylePriority.UseFont = False
            Me.XrLabel_Agency_Address3.StylePriority.UseTextAlignment = False
            '
            'XrLabel_Agency_Address1
            '
            Me.XrLabel_Agency_Address1.StylePriority.UseFont = False
            Me.XrLabel_Agency_Address1.StylePriority.UseTextAlignment = False
            '
            'XrLabel_Agency_Phone
            '
            Me.XrLabel_Agency_Phone.StylePriority.UseFont = False
            Me.XrLabel_Agency_Phone.StylePriority.UseTextAlignment = False
            '
            'XrLabel_Agency_Address2
            '
            Me.XrLabel_Agency_Address2.StylePriority.UseFont = False
            Me.XrLabel_Agency_Address2.StylePriority.UseTextAlignment = False
            '
            'XrLabel_months_to_payoff
            '
            Me.XrLabel_months_to_payoff.Font = New System.Drawing.Font("Times New Roman", 14.0!, System.Drawing.FontStyle.Bold)
            Me.XrLabel_months_to_payoff.ForeColor = System.Drawing.Color.Maroon
            Me.XrLabel_months_to_payoff.LocationFloat = New DevExpress.Utils.PointFloat(8.0!, 8.0!)
            Me.XrLabel_months_to_payoff.Name = "XrLabel_months_to_payoff"
            Me.XrLabel_months_to_payoff.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_months_to_payoff.SizeF = New System.Drawing.SizeF(742.0!, 25.0!)
            Me.XrLabel_months_to_payoff.StylePriority.UseFont = False
            Me.XrLabel_months_to_payoff.StylePriority.UseForeColor = False
            Me.XrLabel_months_to_payoff.Text = "XrLabel_months_to_payoff"
            '
            'XrPanel1
            '
            Me.XrPanel1.BackColor = System.Drawing.Color.Teal
            Me.XrPanel1.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel4, Me.XrLabel3, Me.XrLabel2, Me.XrLabel1})
            Me.XrPanel1.LocationFloat = New DevExpress.Utils.PointFloat(8.0!, 125.0!)
            Me.XrPanel1.Name = "XrPanel1"
            Me.XrPanel1.SizeF = New System.Drawing.SizeF(750.0!, 16.0!)
            '
            'XrLabel4
            '
            Me.XrLabel4.CanGrow = False
            Me.XrLabel4.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel4.ForeColor = System.Drawing.Color.White
            Me.XrLabel4.LocationFloat = New DevExpress.Utils.PointFloat(308.0!, 0.0!)
            Me.XrLabel4.Name = "XrLabel4"
            Me.XrLabel4.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel4.SizeF = New System.Drawing.SizeF(178.0!, 15.0!)
            Me.XrLabel4.StylePriority.UseTextAlignment = False
            Me.XrLabel4.Text = "UNVERIFIED DEBTS"
            Me.XrLabel4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter
            Me.XrLabel4.WordWrap = False
            '
            'XrLabel3
            '
            Me.XrLabel3.CanGrow = False
            Me.XrLabel3.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel3.ForeColor = System.Drawing.Color.White
            Me.XrLabel3.LocationFloat = New DevExpress.Utils.PointFloat(617.0!, 0.0!)
            Me.XrLabel3.Name = "XrLabel3"
            Me.XrLabel3.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel3.SizeF = New System.Drawing.SizeF(129.0!, 15.0!)
            Me.XrLabel3.Text = "BALANCE"
            Me.XrLabel3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
            Me.XrLabel3.WordWrap = False
            '
            'XrLabel2
            '
            Me.XrLabel2.CanGrow = False
            Me.XrLabel2.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel2.ForeColor = System.Drawing.Color.White
            Me.XrLabel2.LocationFloat = New DevExpress.Utils.PointFloat(492.0!, 0.0!)
            Me.XrLabel2.Name = "XrLabel2"
            Me.XrLabel2.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel2.SizeF = New System.Drawing.SizeF(114.0!, 15.0!)
            Me.XrLabel2.Text = "DISBURSEMENT"
            Me.XrLabel2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
            Me.XrLabel2.WordWrap = False
            '
            'XrLabel1
            '
            Me.XrLabel1.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel1.ForeColor = System.Drawing.Color.White
            Me.XrLabel1.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
            Me.XrLabel1.Name = "XrLabel1"
            Me.XrLabel1.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel1.SizeF = New System.Drawing.SizeF(200.0!, 15.0!)
            Me.XrLabel1.Text = "Client ID AND NAME"
            '
            'XrLabel_balance
            '
            Me.XrLabel_balance.CanGrow = False
            Me.XrLabel_balance.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "balance", "{0:c}")})
            Me.XrLabel_balance.Font = New System.Drawing.Font("Times New Roman", 9.75!)
            Me.XrLabel_balance.ForeColor = System.Drawing.SystemColors.ControlText
            Me.XrLabel_balance.LocationFloat = New DevExpress.Utils.PointFloat(625.0!, 0.0!)
            Me.XrLabel_balance.Name = "XrLabel_balance"
            Me.XrLabel_balance.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_balance.SizeF = New System.Drawing.SizeF(129.0!, 15.0!)
            Me.XrLabel_balance.StylePriority.UseFont = False
            Me.XrLabel_balance.StylePriority.UseForeColor = False
            Me.XrLabel_balance.Text = "$0.00"
            Me.XrLabel_balance.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
            Me.XrLabel_balance.WordWrap = False
            '
            'XrLabel_unverified_debts
            '
            Me.XrLabel_unverified_debts.CanGrow = False
            Me.XrLabel_unverified_debts.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "unverified", "{0:n0}")})
            Me.XrLabel_unverified_debts.Font = New System.Drawing.Font("Times New Roman", 9.75!)
            Me.XrLabel_unverified_debts.ForeColor = System.Drawing.SystemColors.ControlText
            Me.XrLabel_unverified_debts.LocationFloat = New DevExpress.Utils.PointFloat(358.0!, 0.0!)
            Me.XrLabel_unverified_debts.Name = "XrLabel_unverified_debts"
            Me.XrLabel_unverified_debts.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_unverified_debts.SizeF = New System.Drawing.SizeF(46.0!, 15.0!)
            Me.XrLabel_unverified_debts.StylePriority.UseFont = False
            Me.XrLabel_unverified_debts.StylePriority.UseForeColor = False
            Me.XrLabel_unverified_debts.StylePriority.UseTextAlignment = False
            Me.XrLabel_unverified_debts.Text = "0"
            Me.XrLabel_unverified_debts.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
            Me.XrLabel_unverified_debts.WordWrap = False
            '
            'XrLabel_disbursement
            '
            Me.XrLabel_disbursement.CanGrow = False
            Me.XrLabel_disbursement.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "disbursement", "{0:c}")})
            Me.XrLabel_disbursement.Font = New System.Drawing.Font("Times New Roman", 9.75!)
            Me.XrLabel_disbursement.ForeColor = System.Drawing.SystemColors.ControlText
            Me.XrLabel_disbursement.LocationFloat = New DevExpress.Utils.PointFloat(417.0!, 0.0!)
            Me.XrLabel_disbursement.Name = "XrLabel_disbursement"
            Me.XrLabel_disbursement.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_disbursement.SizeF = New System.Drawing.SizeF(197.0!, 15.0!)
            Me.XrLabel_disbursement.StylePriority.UseFont = False
            Me.XrLabel_disbursement.StylePriority.UseForeColor = False
            Me.XrLabel_disbursement.Text = "$0.00"
            Me.XrLabel_disbursement.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
            Me.XrLabel_disbursement.WordWrap = False
            '
            'XrLabel_client_id_and_name
            '
            Me.XrLabel_client_id_and_name.Font = New System.Drawing.Font("Times New Roman", 9.75!)
            Me.XrLabel_client_id_and_name.ForeColor = System.Drawing.SystemColors.ControlText
            Me.XrLabel_client_id_and_name.LocationFloat = New DevExpress.Utils.PointFloat(8.0!, 0.0!)
            Me.XrLabel_client_id_and_name.Name = "XrLabel_client_id_and_name"
            Me.XrLabel_client_id_and_name.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_client_id_and_name.SizeF = New System.Drawing.SizeF(342.0!, 15.0!)
            Me.XrLabel_client_id_and_name.StylePriority.UseFont = False
            Me.XrLabel_client_id_and_name.StylePriority.UseForeColor = False
            Me.XrLabel_client_id_and_name.Text = "Client ID AND NAME"
            '
            'ReportFooter
            '
            Me.ReportFooter.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel10, Me.XrLabel_total_balance})
            Me.ReportFooter.HeightF = 31.0!
            Me.ReportFooter.Name = "ReportFooter"
            '
            'XrLabel10
            '
            Me.XrLabel10.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel10.ForeColor = System.Drawing.SystemColors.ControlText
            Me.XrLabel10.LocationFloat = New DevExpress.Utils.PointFloat(8.0!, 8.0!)
            Me.XrLabel10.Name = "XrLabel10"
            Me.XrLabel10.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel10.SizeF = New System.Drawing.SizeF(342.0!, 15.0!)
            Me.XrLabel10.StylePriority.UseFont = False
            Me.XrLabel10.StylePriority.UseForeColor = False
            Me.XrLabel10.Text = "TOTALS"
            '
            'XrLabel_total_balance
            '
            Me.XrLabel_total_balance.CanGrow = False
            Me.XrLabel_total_balance.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "balance")})
            Me.XrLabel_total_balance.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel_total_balance.ForeColor = System.Drawing.SystemColors.ControlText
            Me.XrLabel_total_balance.LocationFloat = New DevExpress.Utils.PointFloat(625.0!, 8.0!)
            Me.XrLabel_total_balance.Name = "XrLabel_total_balance"
            Me.XrLabel_total_balance.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_total_balance.SizeF = New System.Drawing.SizeF(129.0!, 15.0!)
            Me.XrLabel_total_balance.StylePriority.UseFont = False
            Me.XrLabel_total_balance.StylePriority.UseForeColor = False
            XrSummary1.FormatString = "{0:c}"
            XrSummary1.Running = DevExpress.XtraReports.UI.SummaryRunning.Report
            Me.XrLabel_total_balance.Summary = XrSummary1
            Me.XrLabel_total_balance.Text = "$0.00"
            Me.XrLabel_total_balance.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
            Me.XrLabel_total_balance.WordWrap = False
            '
            'GroupHeader1
            '
            Me.GroupHeader1.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel_months_to_payoff})
            Me.GroupHeader1.GroupFields.AddRange(New DevExpress.XtraReports.UI.GroupField() {New DevExpress.XtraReports.UI.GroupField("months_to_payoff", DevExpress.XtraReports.UI.XRColumnSortOrder.Ascending)})
            Me.GroupHeader1.GroupUnion = DevExpress.XtraReports.UI.GroupUnion.WithFirstDetail
            Me.GroupHeader1.HeightF = 42.0!
            Me.GroupHeader1.Name = "GroupHeader1"
            Me.GroupHeader1.RepeatEveryPage = True
            '
            'DetailSubreport
            '
            Me.Bands.AddRange(New DevExpress.XtraReports.UI.Band() {Me.Detail, Me.PageHeader, Me.PageFooter, Me.ReportFooter, Me.GroupHeader1, Me.TopMarginBand1, Me.BottomMarginBand1})
            Me.StyleSheet.AddRange(New DevExpress.XtraReports.UI.XRControlStyle() {Me.XrControlStyle_Header, Me.XrControlStyle_Totals, Me.XrControlStyle_HeaderPannel, Me.XrControlStyle_GroupHeader})
            Me.Version = "11.2"
            Me.Controls.SetChildIndex(Me.BottomMarginBand1, 0)
            Me.Controls.SetChildIndex(Me.TopMarginBand1, 0)
            Me.Controls.SetChildIndex(Me.GroupHeader1, 0)
            Me.Controls.SetChildIndex(Me.ReportFooter, 0)
            Me.Controls.SetChildIndex(Me.PageFooter, 0)
            Me.Controls.SetChildIndex(Me.PageHeader, 0)
            Me.Controls.SetChildIndex(Me.Detail, 0)
            CType(Me, System.ComponentModel.ISupportInitialize).EndInit()

        End Sub
        Protected Friend WithEvents XrLabel_months_to_payoff As DevExpress.XtraReports.UI.XRLabel
        Protected Friend WithEvents XrPanel1 As DevExpress.XtraReports.UI.XRPanel
        Protected Friend WithEvents XrLabel3 As DevExpress.XtraReports.UI.XRLabel
        Protected Friend WithEvents XrLabel2 As DevExpress.XtraReports.UI.XRLabel
        Protected Friend WithEvents XrLabel1 As DevExpress.XtraReports.UI.XRLabel
        Protected Friend WithEvents XrLabel_client_id_and_name As DevExpress.XtraReports.UI.XRLabel
        Protected Friend WithEvents XrLabel_disbursement As DevExpress.XtraReports.UI.XRLabel
        Protected Friend WithEvents XrLabel_unverified_debts As DevExpress.XtraReports.UI.XRLabel
        Protected Friend WithEvents XrLabel_balance As DevExpress.XtraReports.UI.XRLabel
        Protected Friend WithEvents XrLabel4 As DevExpress.XtraReports.UI.XRLabel
        Protected Friend WithEvents ReportFooter As DevExpress.XtraReports.UI.ReportFooterBand
        Protected Friend WithEvents XrLabel10 As DevExpress.XtraReports.UI.XRLabel
        Protected Friend WithEvents XrLabel_total_balance As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents GroupHeader1 As DevExpress.XtraReports.UI.GroupHeaderBand
    End Class
End Namespace
