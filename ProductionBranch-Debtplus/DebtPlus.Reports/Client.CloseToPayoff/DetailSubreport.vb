#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}
#End Region
Imports DebtPlus.Reports.Template

Namespace Client.CloseToPayoff

    Public Class DetailSubreport
        Inherits TemplateXtraReportClass

        Protected ParameterCounselor As Object = Nothing
        Protected ParameterMonthsToPayoff As System.Int32

        Public Sub New()
            MyBase.New()
            InitializeComponent()

            AddHandler XrLabel_client_id_and_name.BeforePrint, AddressOf XrLabel_client_id_and_name_BeforePrint
            AddHandler XrLabel_months_to_payoff.BeforePrint, AddressOf XrLabel_months_to_payoff_BeforePrint

            ReportFilter.IsEnabled = True
        End Sub

        Public Overrides ReadOnly Property ReportTitle() As String
            Get
                Return "Close to Payoff"
            End Get
        End Property

        Protected Sub XrLabel_client_id_and_name_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs)
            XrLabel_client_id_and_name.Text = "[" + String.Format("{0:0000000}", GetCurrentColumnValue("client")) + "] " + DebtPlus.Utils.Nulls.DStr(Me.GetCurrentColumnValue("name"))
        End Sub

        Private Sub XrLabel_months_to_payoff_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs)
            Dim months As System.Int32 = DebtPlus.Utils.Nulls.DInt(Me.GetCurrentColumnValue("months_to_payoff"))
            XrLabel_months_to_payoff.Text = MonthsString(months)
        End Sub
    End Class
End Namespace