#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}
#End Region

Option Compare Binary
Option Explicit On
Option Strict On

Namespace Counselor.Prod.Detail
    Public Class CounselorProductivityDetailReport

        ''' <summary>
        ''' Create an instance of our report
        ''' </summary>
        Public Sub New()
            MyBase.New()
            InitializeComponent()
            AddHandler BeforePrint, AddressOf CounselorProductivityDetailReport_BeforePrint
            AddHandler XrLabel_client.BeforePrint, AddressOf XrLabel_client_BeforePrint

            ParameterClientStatus.Value = String.Empty

            ReportFilter.IsEnabled = True
        End Sub


        ''' <summary>
        ''' Report Title information
        ''' </summary>
        Public Overrides ReadOnly Property ReportTitle() As String
            Get
                Return "Counselor Productivity"
            End Get
        End Property

        ''' <summary>
        ''' Parameter for the report result type
        ''' </summary>
        Public Property Parameter_ClientStatus() As String
            Get
                Dim parm As DevExpress.XtraReports.Parameters.Parameter = FindParameter("ParameterClientStatus")
                If parm Is Nothing Then Return String.Empty
                Return Convert.ToString(parm.Value)
            End Get
            Set(ByVal value As String)
                SetParameter("ParameterClientStatus", GetType(String), value, "Client Status", False)
            End Set
        End Property

        ''' <summary>
        ''' Set the report parameters
        ''' </summary>
        Public Overrides Sub SetReportParameter(ByVal Parameter As String, ByVal Value As Object)
            If Parameter = "ClientStatus" Then
                Parameter_ClientStatus = Convert.ToString(Value)
            Else
                MyBase.SetReportParameter(Parameter, Value)
            End If
        End Sub

        ''' <summary>
        ''' Request the parameters for the report
        ''' </summary>
        Public Overrides Function RequestReportParameters() As System.Windows.Forms.DialogResult
            Dim Answer As System.Windows.Forms.DialogResult = DialogResult.OK
            If NeedParameters() Then
                Using frm As New Dated_AppoinmentResult_ParametersForm()
                    Answer = frm.ShowDialog()
                    Parameter_ClientStatus = frm.Parameter_ClientStatus
                    Parameter_FromDate = frm.Parameter_FromDate
                    Parameter_ToDate = frm.Parameter_ToDate
                End Using
            End If
            Return Answer
        End Function

        Private ReadOnly ds As New System.Data.DataSet("ds")
        ''' <summary>
        ''' Initialize the report
        ''' </summary>
        Private Sub CounselorProductivityDetailReport_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs)
            Const TableName As String = "rpt_counselor_productivity"
            Dim rpt As DevExpress.XtraReports.UI.XtraReport = CType(sender, DevExpress.XtraReports.UI.XtraReport)
            Dim tbl As System.Data.DataTable = ds.Tables(TableName)

            If tbl Is Nothing Then
                Dim sqlInfo As DebtPlus.LINQ.SQLInfoClass = DebtPlus.LINQ.SQLInfoClass.getDefault()
                Try
                    Using cn As System.Data.SqlClient.SqlConnection = New System.Data.SqlClient.SqlConnection(sqlInfo.ConnectionString)
                        cn.Open()
                        Using cmd As New System.Data.SqlClient.SqlCommand()
                            cmd.Connection = cn
                            cmd.CommandText = "rpt_counselor_productivity"
                            cmd.CommandType = System.Data.CommandType.StoredProcedure

                            '-- Find the parameters for the stored procedure on the database
                            System.Data.SqlClient.SqlCommandBuilder.DeriveParameters(cmd)

                            '-- Supply the date parameter values
                            cmd.Parameters(1).Value = rpt.Parameters("ParameterFromDate").Value
                            cmd.Parameters(2).Value = rpt.Parameters("ParameterToDate").Value

                            cmd.CommandTimeout = 0

                            Using da As New System.Data.SqlClient.SqlDataAdapter(cmd)
                                da.Fill(ds, TableName)
                                tbl = ds.Tables(TableName)
                            End Using
                        End Using
                    End Using

                Catch ex As System.Data.SqlClient.SqlException
                    Using gdr As New DebtPlus.Repository.GetDataResult()
                        gdr.HandleException(ex)
                        DebtPlus.UI.Common.ErrorHandling.HandleErrors(gdr, "Error reading data")
                    End Using
                End Try
            End If

            If tbl IsNot Nothing Then

                '-- Perform a selection of the data if needed
                Dim SelectionClause As String = CType(rpt, DebtPlus.Interfaces.Reports.IReportFilter).ReportFilter.ViewFilter
                Dim ClientStatus As String = Convert.ToString(rpt.Parameters("ParameterClientStatus").Value)

                If ClientStatus <> String.Empty Then
                    If SelectionClause <> String.Empty Then SelectionClause = String.Format("({0}) AND ", SelectionClause)
                    SelectionClause += String.Format("([result]='{0}')", ClientStatus.Replace("'", "''"))
                End If

                rpt.DataSource = New System.Data.DataView(tbl, SelectionClause, "office,start_time", System.Data.DataViewRowState.CurrentRows)
            End If
        End Sub

        ''' <summary>
        ''' Should we print the field on the form?
        ''' </summary>
        Private Sub XrLabel_client_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs)
            Dim lbl As DevExpress.XtraReports.UI.XRLabel = CType(sender, DevExpress.XtraReports.UI.XRLabel)
            Dim rpt As DevExpress.XtraReports.UI.XtraReport = CType(lbl.Report, DevExpress.XtraReports.UI.XtraReport)
            Dim Client As System.Int32 = DebtPlus.Utils.Nulls.DInt(rpt.GetCurrentColumnValue("client"))
            Dim Fname As String = DebtPlus.Utils.Nulls.DStr(rpt.GetCurrentColumnValue("firstname"))
            Dim Lname As String = DebtPlus.Utils.Nulls.DStr(rpt.GetCurrentColumnValue("lastname"))
            lbl.Text = String.Format("{0} {1} {2}", DebtPlus.Utils.Format.Client.FormatClientID(Client), Fname, Lname)
        End Sub
    End Class
End Namespace
