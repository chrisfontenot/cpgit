#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}
#End Region

Namespace Counselor.Prod.Detail
    Public Class Dated_AppoinmentResult_ParametersForm

        Private ds As New System.Data.DataSet("ds")

        Public Sub New()
            MyBase.New()
            InitializeComponent()
            AddHandler Me.Load, AddressOf ParametersForm_Load
        End Sub

        Private Sub ParametersForm_Load(ByVal sender As Object, ByVal e As System.EventArgs)
            Using cmd As SqlClient.SqlCommand = New System.Data.SqlClient.SqlCommand()
                With cmd
                    .Connection = New System.Data.SqlClient.SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
                    .CommandText = "SELECT [client_status],[description],[default] FROM ClientStatusTypes WHERE ActiveFlag=1"
                    .CommandType = CommandType.Text
                End With

                Using da As New SqlClient.SqlDataAdapter(cmd)
                    da.Fill(ds, "ClientStatusTypes")
                End Using
            End Using

            Dim tbl As System.Data.DataTable = ds.Tables("ClientStatusTypes")
            With tbl
                Dim row As System.Data.DataRow = tbl.NewRow()
                row("description") = "[All Items]"
                row("client_status") = String.Empty
                row("Default") = False
                tbl.Rows.Add(row)
                tbl.AcceptChanges()
            End With

            LookUpEdit_Result.Properties.DataSource = New System.Data.DataView(ds.Tables("ClientStatusTypes"), String.Empty, "description", DataViewRowState.CurrentRows)
            LookUpEdit_Result.EditValue = String.Empty
        End Sub

        Public ReadOnly Property Parameter_ClientStatus() As String
            Get
                Return Convert.ToString(LookUpEdit_Result.EditValue)
            End Get
        End Property
    End Class
End Namespace