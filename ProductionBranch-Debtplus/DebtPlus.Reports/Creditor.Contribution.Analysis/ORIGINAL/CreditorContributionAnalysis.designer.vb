Namespace Creditor.Contribution.Analysis
    <Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
    Partial Class CreditorContributionAnalysis
        Inherits DebtPlus.Reports.Template.TemplateXtraReportClass

        'Component overrides dispose to clean up the component list.
        <System.Diagnostics.DebuggerNonUserCode()> _
        Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
            If disposing Then
                If components IsNot Nothing Then
                    components.Dispose()
                End If
            End If
            MyBase.Dispose(disposing)
        End Sub

        'Required by the Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Designer
        'It can be modified using the Designer.
        'Do not modify it using the code editor.
        <System.Diagnostics.DebuggerStepThrough()> _
        Private Sub InitializeComponent()
            Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(CreditorContributionAnalysis))
            Me.XrPanel1 = New DevExpress.XtraReports.UI.XRPanel()
            Me.XrLabel17 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel16 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel15 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel14 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel12 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel11 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel10 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel9 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel8 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel7 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel5 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel3 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel2 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_creditor_name2 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_contact_name2 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_zipcode2 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel6 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_phone2 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrPanel2 = New DevExpress.XtraReports.UI.XRPanel()
            Me.XrLabel_all_payments = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_qtd_clients = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_ytd_clients = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_all_clients = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_last_year_fairshare = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_this_year_fairshare = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_mtd_clients = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_ytd_payments = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_qtd_payments = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_mtd_payments = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_creditor = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_creditor_name = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_contact_name = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_zipcode = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_phone = New DevExpress.XtraReports.UI.XRLabel()
            CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
            '
            'PageHeader
            '
            Me.PageHeader.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrPanel1})
            Me.PageHeader.HeightF = 181.2916!
            Me.PageHeader.Controls.SetChildIndex(Me.XrPanel1, 0)
            Me.PageHeader.Controls.SetChildIndex(Me.XrPanel_AgencyAddress, 0)
            Me.PageHeader.Controls.SetChildIndex(Me.XrLabel_Title, 0)
            Me.PageHeader.Controls.SetChildIndex(Me.XrLine_Title, 0)
            Me.PageHeader.Controls.SetChildIndex(Me.XrLabel_Subtitle, 0)
            Me.PageHeader.Controls.SetChildIndex(Me.XrPageInfo1, 0)
            '
            'XrLabel_Title
            '
            Me.XrLabel_Title.LocationFloat = New DevExpress.Utils.PointFloat(8.0!, 7.999992!)
            Me.XrLabel_Title.SizeF = New System.Drawing.SizeF(470.125!, 42.0!)
            Me.XrLabel_Title.StylePriority.UseFont = False
            '
            'XrLabel_Subtitle
            '
            Me.XrLabel_Subtitle.SizeF = New System.Drawing.SizeF(725.0!, 17.0!)
            '
            'XrPageInfo_PageNumber
            '
            Me.XrPageInfo_PageNumber.LocationFloat = New DevExpress.Utils.PointFloat(655.0!, 8.0!)
            Me.XrPageInfo_PageNumber.StylePriority.UseTextAlignment = False
            '
            'XrPanel_AgencyAddress
            '
            Me.XrPanel_AgencyAddress.LocationFloat = New DevExpress.Utils.PointFloat(492.0!, 0.0!)
            '
            'XRLabel_Agency_Name
            '
            Me.XRLabel_Agency_Name.StylePriority.UseTextAlignment = False
            '
            'XrLine_Title
            '
            Me.XrLine_Title.SizeF = New System.Drawing.SizeF(470.125!, 8.0!)
            '
            'XrLabel_Agency_Address3
            '
            Me.XrLabel_Agency_Address3.StylePriority.UseFont = False
            Me.XrLabel_Agency_Address3.StylePriority.UseTextAlignment = False
            '
            'XrLabel_Agency_Address1
            '
            Me.XrLabel_Agency_Address1.StylePriority.UseFont = False
            Me.XrLabel_Agency_Address1.StylePriority.UseTextAlignment = False
            '
            'XrLabel_Agency_Phone
            '
            Me.XrLabel_Agency_Phone.StylePriority.UseFont = False
            Me.XrLabel_Agency_Phone.StylePriority.UseTextAlignment = False
            '
            'XrLabel_Agency_Address2
            '
            Me.XrLabel_Agency_Address2.StylePriority.UseFont = False
            Me.XrLabel_Agency_Address2.StylePriority.UseTextAlignment = False
            '
            'Detail
            '
            Me.Detail.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrPanel2})
            Me.Detail.HeightF = 50.45834!
            '
            'XrPanel1
            '
            Me.XrPanel1.BackColor = System.Drawing.Color.Teal
            Me.XrPanel1.CanGrow = False
            Me.XrPanel1.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel17, Me.XrLabel16, Me.XrLabel15, Me.XrLabel14, Me.XrLabel12, Me.XrLabel11, Me.XrLabel10, Me.XrLabel9, Me.XrLabel8, Me.XrLabel7, Me.XrLabel5, Me.XrLabel3, Me.XrLabel2, Me.XrLabel_creditor_name2, Me.XrLabel_contact_name2, Me.XrLabel_zipcode2, Me.XrLabel6, Me.XrLabel_phone2})
            Me.XrPanel1.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 125.0!)
            Me.XrPanel1.Name = "XrPanel1"
            Me.XrPanel1.SizeF = New System.Drawing.SizeF(798.0!, 53.0!)
            Me.XrPanel1.StyleName = "XrControlStyle_HeaderPannel"
            Me.XrPanel1.StylePriority.UseBackColor = False
            '
            'XrLabel17
            '
            Me.XrLabel17.CanGrow = False
            Me.XrLabel17.LocationFloat = New DevExpress.Utils.PointFloat(275.0!, 34.0!)
            Me.XrLabel17.Name = "XrLabel17"
            Me.XrLabel17.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel17.SizeF = New System.Drawing.SizeF(73.0!, 17.0!)
            Me.XrLabel17.StylePriority.UseTextAlignment = False
            Me.XrLabel17.Text = "ALL"
            Me.XrLabel17.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel16
            '
            Me.XrLabel16.CanGrow = False
            Me.XrLabel16.LocationFloat = New DevExpress.Utils.PointFloat(418.0!, 34.0!)
            Me.XrLabel16.Name = "XrLabel16"
            Me.XrLabel16.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel16.SizeF = New System.Drawing.SizeF(77.00006!, 17.0!)
            Me.XrLabel16.StylePriority.UseTextAlignment = False
            Me.XrLabel16.Text = "QTD"
            Me.XrLabel16.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel15
            '
            Me.XrLabel15.CanGrow = False
            Me.XrLabel15.LocationFloat = New DevExpress.Utils.PointFloat(495.0!, 34.0!)
            Me.XrLabel15.Name = "XrLabel15"
            Me.XrLabel15.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel15.SizeF = New System.Drawing.SizeF(77.0!, 17.0!)
            Me.XrLabel15.StylePriority.UseTextAlignment = False
            Me.XrLabel15.Text = "YTD"
            Me.XrLabel15.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel14
            '
            Me.XrLabel14.CanGrow = False
            Me.XrLabel14.LocationFloat = New DevExpress.Utils.PointFloat(572.0001!, 34.0!)
            Me.XrLabel14.Name = "XrLabel14"
            Me.XrLabel14.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel14.SizeF = New System.Drawing.SizeF(77.0!, 17.0!)
            Me.XrLabel14.StylePriority.UseTextAlignment = False
            Me.XrLabel14.Text = "ALL"
            Me.XrLabel14.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel12
            '
            Me.XrLabel12.CanGrow = False
            Me.XrLabel12.LocationFloat = New DevExpress.Utils.PointFloat(666.0!, 34.0!)
            Me.XrLabel12.Name = "XrLabel12"
            Me.XrLabel12.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel12.SizeF = New System.Drawing.SizeF(50.0!, 17.0!)
            Me.XrLabel12.StylePriority.UseTextAlignment = False
            Me.XrLabel12.Text = "PRIOR"
            Me.XrLabel12.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel11
            '
            Me.XrLabel11.CanGrow = False
            Me.XrLabel11.LocationFloat = New DevExpress.Utils.PointFloat(735.0!, 34.0!)
            Me.XrLabel11.Name = "XrLabel11"
            Me.XrLabel11.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel11.SizeF = New System.Drawing.SizeF(47.0!, 17.0!)
            Me.XrLabel11.StylePriority.UseTextAlignment = False
            Me.XrLabel11.Text = "CUR"
            Me.XrLabel11.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel10
            '
            Me.XrLabel10.CanGrow = False
            Me.XrLabel10.LocationFloat = New DevExpress.Utils.PointFloat(358.0!, 34.0!)
            Me.XrLabel10.Name = "XrLabel10"
            Me.XrLabel10.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel10.SizeF = New System.Drawing.SizeF(60.0!, 17.0!)
            Me.XrLabel10.StylePriority.UseTextAlignment = False
            Me.XrLabel10.Text = "MTD"
            Me.XrLabel10.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel9
            '
            Me.XrLabel9.CanGrow = False
            Me.XrLabel9.LocationFloat = New DevExpress.Utils.PointFloat(186.0!, 34.0!)
            Me.XrLabel9.Name = "XrLabel9"
            Me.XrLabel9.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel9.SizeF = New System.Drawing.SizeF(73.0!, 17.0!)
            Me.XrLabel9.StylePriority.UseTextAlignment = False
            Me.XrLabel9.Text = "YTD"
            Me.XrLabel9.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel8
            '
            Me.XrLabel8.CanGrow = False
            Me.XrLabel8.LocationFloat = New DevExpress.Utils.PointFloat(96.99999!, 34.0!)
            Me.XrLabel8.Name = "XrLabel8"
            Me.XrLabel8.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel8.SizeF = New System.Drawing.SizeF(73.0!, 17.0!)
            Me.XrLabel8.StylePriority.UseTextAlignment = False
            Me.XrLabel8.Text = "QTD"
            Me.XrLabel8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel7
            '
            Me.XrLabel7.CanGrow = False
            Me.XrLabel7.LocationFloat = New DevExpress.Utils.PointFloat(7.999992!, 34.0!)
            Me.XrLabel7.Name = "XrLabel7"
            Me.XrLabel7.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel7.SizeF = New System.Drawing.SizeF(73.0!, 17.0!)
            Me.XrLabel7.StylePriority.UseTextAlignment = False
            Me.XrLabel7.Text = "MTD"
            Me.XrLabel7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel5
            '
            Me.XrLabel5.BorderColor = System.Drawing.Color.White
            Me.XrLabel5.Borders = DevExpress.XtraPrinting.BorderSide.Bottom
            Me.XrLabel5.BorderWidth = 2
            Me.XrLabel5.LocationFloat = New DevExpress.Utils.PointFloat(666.0!, 17.0!)
            Me.XrLabel5.Name = "XrLabel5"
            Me.XrLabel5.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel5.SizeF = New System.Drawing.SizeF(116.0!, 17.0!)
            Me.XrLabel5.StylePriority.UseBorderColor = False
            Me.XrLabel5.StylePriority.UseBorders = False
            Me.XrLabel5.StylePriority.UseBorderWidth = False
            Me.XrLabel5.StylePriority.UseTextAlignment = False
            Me.XrLabel5.Text = "PERCENT"
            Me.XrLabel5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
            '
            'XrLabel3
            '
            Me.XrLabel3.BorderColor = System.Drawing.Color.White
            Me.XrLabel3.Borders = DevExpress.XtraPrinting.BorderSide.Bottom
            Me.XrLabel3.BorderWidth = 2
            Me.XrLabel3.LocationFloat = New DevExpress.Utils.PointFloat(358.0!, 17.0!)
            Me.XrLabel3.Name = "XrLabel3"
            Me.XrLabel3.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel3.SizeF = New System.Drawing.SizeF(291.0!, 17.0!)
            Me.XrLabel3.StylePriority.UseBorderColor = False
            Me.XrLabel3.StylePriority.UseBorders = False
            Me.XrLabel3.StylePriority.UseBorderWidth = False
            Me.XrLabel3.StylePriority.UseTextAlignment = False
            Me.XrLabel3.Text = "CLIENTS"
            Me.XrLabel3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
            '
            'XrLabel2
            '
            Me.XrLabel2.CanGrow = False
            Me.XrLabel2.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 1.0!)
            Me.XrLabel2.Name = "XrLabel2"
            Me.XrLabel2.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel2.SizeF = New System.Drawing.SizeF(86.0!, 17.0!)
            Me.XrLabel2.StylePriority.UseTextAlignment = False
            Me.XrLabel2.Text = "CREDITOR"
            Me.XrLabel2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            '
            'XrLabel_creditor_name2
            '
            Me.XrLabel_creditor_name2.CanGrow = False
            Me.XrLabel_creditor_name2.LocationFloat = New DevExpress.Utils.PointFloat(87.0!, 1.0!)
            Me.XrLabel_creditor_name2.Name = "XrLabel_creditor_name2"
            Me.XrLabel_creditor_name2.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_creditor_name2.SizeF = New System.Drawing.SizeF(190.0!, 17.0!)
            Me.XrLabel_creditor_name2.StylePriority.UseTextAlignment = False
            Me.XrLabel_creditor_name2.Text = "NAME"
            Me.XrLabel_creditor_name2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            '
            'XrLabel_contact_name2
            '
            Me.XrLabel_contact_name2.LocationFloat = New DevExpress.Utils.PointFloat(391.0!, 1.0!)
            Me.XrLabel_contact_name2.Name = "XrLabel_contact_name2"
            Me.XrLabel_contact_name2.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 0, 0, 0, 100.0!)
            Me.XrLabel_contact_name2.SizeF = New System.Drawing.SizeF(190.0!, 17.0!)
            Me.XrLabel_contact_name2.StylePriority.UsePadding = False
            Me.XrLabel_contact_name2.StylePriority.UseTextAlignment = False
            Me.XrLabel_contact_name2.Text = "CONTACT NAME"
            Me.XrLabel_contact_name2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            '
            'XrLabel_zipcode2
            '
            Me.XrLabel_zipcode2.LocationFloat = New DevExpress.Utils.PointFloat(291.0!, 1.0!)
            Me.XrLabel_zipcode2.Name = "XrLabel_zipcode2"
            Me.XrLabel_zipcode2.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_zipcode2.SizeF = New System.Drawing.SizeF(91.0!, 17.0!)
            Me.XrLabel_zipcode2.StylePriority.UseTextAlignment = False
            Me.XrLabel_zipcode2.Text = "ZIPCODE"
            Me.XrLabel_zipcode2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            '
            'XrLabel6
            '
            Me.XrLabel6.BorderColor = System.Drawing.Color.White
            Me.XrLabel6.Borders = DevExpress.XtraPrinting.BorderSide.Bottom
            Me.XrLabel6.BorderWidth = 2
            Me.XrLabel6.LocationFloat = New DevExpress.Utils.PointFloat(8.0!, 17.0!)
            Me.XrLabel6.Name = "XrLabel6"
            Me.XrLabel6.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel6.SizeF = New System.Drawing.SizeF(340.0!, 17.0!)
            Me.XrLabel6.StylePriority.UseBorderColor = False
            Me.XrLabel6.StylePriority.UseBorders = False
            Me.XrLabel6.StylePriority.UseBorderWidth = False
            Me.XrLabel6.StylePriority.UseTextAlignment = False
            Me.XrLabel6.Text = "PAYMENTS"
            Me.XrLabel6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
            '
            'XrLabel_phone2
            '
            Me.XrLabel_phone2.LocationFloat = New DevExpress.Utils.PointFloat(591.0!, 1.0!)
            Me.XrLabel_phone2.Name = "XrLabel_phone2"
            Me.XrLabel_phone2.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_phone2.SizeF = New System.Drawing.SizeF(190.0!, 17.0!)
            Me.XrLabel_phone2.StylePriority.UseTextAlignment = False
            Me.XrLabel_phone2.Text = "PHONE"
            Me.XrLabel_phone2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            '
            'XrPanel2
            '
            Me.XrPanel2.BackColor = System.Drawing.Color.Transparent
            Me.XrPanel2.CanGrow = False
            Me.XrPanel2.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel_all_payments, Me.XrLabel_qtd_clients, Me.XrLabel_ytd_clients, Me.XrLabel_all_clients, Me.XrLabel_last_year_fairshare, Me.XrLabel_this_year_fairshare, Me.XrLabel_mtd_clients, Me.XrLabel_ytd_payments, Me.XrLabel_qtd_payments, Me.XrLabel_mtd_payments, Me.XrLabel_creditor, Me.XrLabel_creditor_name, Me.XrLabel_contact_name, Me.XrLabel_zipcode, Me.XrLabel_phone})
            Me.XrPanel2.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
            Me.XrPanel2.Name = "XrPanel2"
            Me.XrPanel2.SizeF = New System.Drawing.SizeF(798.0!, 34.0!)
            '
            'XrLabel_all_payments
            '
            Me.XrLabel_all_payments.CanGrow = False
            Me.XrLabel_all_payments.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "all_payments", "{0:c}")})
            Me.XrLabel_all_payments.LocationFloat = New DevExpress.Utils.PointFloat(259.0!, 17.0!)
            Me.XrLabel_all_payments.Name = "XrLabel_all_payments"
            Me.XrLabel_all_payments.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_all_payments.SizeF = New System.Drawing.SizeF(89.0!, 17.0!)
            Me.XrLabel_all_payments.StylePriority.UseTextAlignment = False
            Me.XrLabel_all_payments.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel_qtd_clients
            '
            Me.XrLabel_qtd_clients.CanGrow = False
            Me.XrLabel_qtd_clients.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "qtd_clients", "{0:f0}")})
            Me.XrLabel_qtd_clients.LocationFloat = New DevExpress.Utils.PointFloat(418.0!, 17.0!)
            Me.XrLabel_qtd_clients.Name = "XrLabel_qtd_clients"
            Me.XrLabel_qtd_clients.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_qtd_clients.SizeF = New System.Drawing.SizeF(77.00006!, 17.0!)
            Me.XrLabel_qtd_clients.StylePriority.UseTextAlignment = False
            Me.XrLabel_qtd_clients.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel_ytd_clients
            '
            Me.XrLabel_ytd_clients.CanGrow = False
            Me.XrLabel_ytd_clients.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "ytd_clients", "{0:f0}")})
            Me.XrLabel_ytd_clients.LocationFloat = New DevExpress.Utils.PointFloat(495.0!, 17.0!)
            Me.XrLabel_ytd_clients.Name = "XrLabel_ytd_clients"
            Me.XrLabel_ytd_clients.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_ytd_clients.SizeF = New System.Drawing.SizeF(77.0!, 17.0!)
            Me.XrLabel_ytd_clients.StylePriority.UseTextAlignment = False
            Me.XrLabel_ytd_clients.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel_all_clients
            '
            Me.XrLabel_all_clients.CanGrow = False
            Me.XrLabel_all_clients.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "all_clients", "{0:f0}")})
            Me.XrLabel_all_clients.LocationFloat = New DevExpress.Utils.PointFloat(572.0001!, 17.0!)
            Me.XrLabel_all_clients.Name = "XrLabel_all_clients"
            Me.XrLabel_all_clients.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_all_clients.SizeF = New System.Drawing.SizeF(77.0!, 17.0!)
            Me.XrLabel_all_clients.StylePriority.UseTextAlignment = False
            Me.XrLabel_all_clients.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel_last_year_fairshare
            '
            Me.XrLabel_last_year_fairshare.CanGrow = False
            Me.XrLabel_last_year_fairshare.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "last_year_fairshare", "{0:p}")})
            Me.XrLabel_last_year_fairshare.LocationFloat = New DevExpress.Utils.PointFloat(655.0!, 17.0!)
            Me.XrLabel_last_year_fairshare.Name = "XrLabel_last_year_fairshare"
            Me.XrLabel_last_year_fairshare.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_last_year_fairshare.SizeF = New System.Drawing.SizeF(61.0!, 17.0!)
            Me.XrLabel_last_year_fairshare.StylePriority.UseTextAlignment = False
            Me.XrLabel_last_year_fairshare.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel_this_year_fairshare
            '
            Me.XrLabel_this_year_fairshare.CanGrow = False
            Me.XrLabel_this_year_fairshare.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "this_year_fairshare", "{0:p}")})
            Me.XrLabel_this_year_fairshare.LocationFloat = New DevExpress.Utils.PointFloat(716.0!, 17.0!)
            Me.XrLabel_this_year_fairshare.Name = "XrLabel_this_year_fairshare"
            Me.XrLabel_this_year_fairshare.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_this_year_fairshare.SizeF = New System.Drawing.SizeF(66.0!, 17.0!)
            Me.XrLabel_this_year_fairshare.StylePriority.UseTextAlignment = False
            Me.XrLabel_this_year_fairshare.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel_mtd_clients
            '
            Me.XrLabel_mtd_clients.CanGrow = False
            Me.XrLabel_mtd_clients.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "mtd_clients", "{0:f0}")})
            Me.XrLabel_mtd_clients.LocationFloat = New DevExpress.Utils.PointFloat(358.0!, 17.0!)
            Me.XrLabel_mtd_clients.Name = "XrLabel_mtd_clients"
            Me.XrLabel_mtd_clients.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_mtd_clients.SizeF = New System.Drawing.SizeF(60.0!, 17.0!)
            Me.XrLabel_mtd_clients.StylePriority.UseTextAlignment = False
            Me.XrLabel_mtd_clients.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel_ytd_payments
            '
            Me.XrLabel_ytd_payments.CanGrow = False
            Me.XrLabel_ytd_payments.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "ytd_payments", "{0:c}")})
            Me.XrLabel_ytd_payments.LocationFloat = New DevExpress.Utils.PointFloat(170.0!, 17.0!)
            Me.XrLabel_ytd_payments.Name = "XrLabel_ytd_payments"
            Me.XrLabel_ytd_payments.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_ytd_payments.SizeF = New System.Drawing.SizeF(89.00002!, 17.0!)
            Me.XrLabel_ytd_payments.StylePriority.UseTextAlignment = False
            Me.XrLabel_ytd_payments.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel_qtd_payments
            '
            Me.XrLabel_qtd_payments.CanGrow = False
            Me.XrLabel_qtd_payments.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "qtd_payments", "{0:c}")})
            Me.XrLabel_qtd_payments.LocationFloat = New DevExpress.Utils.PointFloat(87.00002!, 17.0!)
            Me.XrLabel_qtd_payments.Name = "XrLabel_qtd_payments"
            Me.XrLabel_qtd_payments.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_qtd_payments.SizeF = New System.Drawing.SizeF(82.99999!, 17.0!)
            Me.XrLabel_qtd_payments.StylePriority.UseTextAlignment = False
            Me.XrLabel_qtd_payments.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel_mtd_payments
            '
            Me.XrLabel_mtd_payments.CanGrow = False
            Me.XrLabel_mtd_payments.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "mtd_payments", "{0:c}")})
            Me.XrLabel_mtd_payments.LocationFloat = New DevExpress.Utils.PointFloat(8.0!, 17.0!)
            Me.XrLabel_mtd_payments.Name = "XrLabel_mtd_payments"
            Me.XrLabel_mtd_payments.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_mtd_payments.SizeF = New System.Drawing.SizeF(73.0!, 17.0!)
            Me.XrLabel_mtd_payments.StylePriority.UseTextAlignment = False
            Me.XrLabel_mtd_payments.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel_creditor
            '
            Me.XrLabel_creditor.CanGrow = False
            Me.XrLabel_creditor.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "creditor")})
            Me.XrLabel_creditor.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
            Me.XrLabel_creditor.Name = "XrLabel_creditor"
            Me.XrLabel_creditor.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_creditor.SizeF = New System.Drawing.SizeF(86.0!, 17.0!)
            Me.XrLabel_creditor.StylePriority.UseTextAlignment = False
            Me.XrLabel_creditor.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            '
            'XrLabel_creditor_name
            '
            Me.XrLabel_creditor_name.CanGrow = False
            Me.XrLabel_creditor_name.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "creditor_name")})
            Me.XrLabel_creditor_name.LocationFloat = New DevExpress.Utils.PointFloat(87.0!, 0.0!)
            Me.XrLabel_creditor_name.Name = "XrLabel_creditor_name"
            Me.XrLabel_creditor_name.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_creditor_name.SizeF = New System.Drawing.SizeF(190.0!, 17.0!)
            Me.XrLabel_creditor_name.StylePriority.UseTextAlignment = False
            Me.XrLabel_creditor_name.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            '
            'XrLabel_contact_name
            '
            Me.XrLabel_contact_name.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "contact_name")})
            Me.XrLabel_contact_name.LocationFloat = New DevExpress.Utils.PointFloat(391.0!, 0.0!)
            Me.XrLabel_contact_name.Name = "XrLabel_contact_name"
            Me.XrLabel_contact_name.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 0, 0, 0, 100.0!)
            Me.XrLabel_contact_name.SizeF = New System.Drawing.SizeF(190.0!, 17.0!)
            Me.XrLabel_contact_name.StylePriority.UsePadding = False
            Me.XrLabel_contact_name.StylePriority.UseTextAlignment = False
            Me.XrLabel_contact_name.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            '
            'XrLabel_zipcode
            '
            Me.XrLabel_zipcode.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "zipcode")})
            Me.XrLabel_zipcode.LocationFloat = New DevExpress.Utils.PointFloat(291.0!, 0.0!)
            Me.XrLabel_zipcode.Name = "XrLabel_zipcode"
            Me.XrLabel_zipcode.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_zipcode.SizeF = New System.Drawing.SizeF(91.0!, 17.0!)
            Me.XrLabel_zipcode.StylePriority.UseTextAlignment = False
            Me.XrLabel_zipcode.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            '
            'XrLabel_phone
            '
            Me.XrLabel_phone.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "phone")})
            Me.XrLabel_phone.LocationFloat = New DevExpress.Utils.PointFloat(591.0!, 0.0!)
            Me.XrLabel_phone.Name = "XrLabel_phone"
            Me.XrLabel_phone.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_phone.SizeF = New System.Drawing.SizeF(190.0!, 17.0!)
            Me.XrLabel_phone.StylePriority.UseTextAlignment = False
            Me.XrLabel_phone.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            '
            'CreditorContributionAnalysis
            '
            Me.Bands.AddRange(New DevExpress.XtraReports.UI.Band() {Me.TopMarginBand1, Me.Detail, Me.BottomMarginBand1, Me.PageHeader, Me.PageFooter})
            Me.DesignerOptions.ShowExportWarnings = False
            Me.DesignerOptions.ShowPrintingWarnings = False
            Me.ScriptLanguage = DevExpress.XtraReports.ScriptLanguage.VisualBasic
            Me.ScriptReferencesString = resources.GetString("$this.ScriptReferencesString")
            Me.Scripts.OnBeforePrint = "CreditorContributionAnalysis_BeforePrint"
            Me.ScriptsSource = resources.GetString("$this.ScriptsSource")
            Me.StyleSheet.AddRange(New DevExpress.XtraReports.UI.XRControlStyle() {Me.XrControlStyle_Header, Me.XrControlStyle_Totals, Me.XrControlStyle_HeaderPannel, Me.XrControlStyle_GroupHeader})
            Me.Version = "11.2"
            CType(Me, System.ComponentModel.ISupportInitialize).EndInit()

        End Sub

        Friend WithEvents XrPanel1 As DevExpress.XtraReports.UI.XRPanel
        Friend WithEvents XrLabel_zipcode2 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel6 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_contact_name2 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_creditor_name2 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_phone2 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel2 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel5 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel3 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel17 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel16 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel15 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel14 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel12 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel11 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel10 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel9 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel8 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel7 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrPanel2 As DevExpress.XtraReports.UI.XRPanel
        Friend WithEvents XrLabel_all_payments As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_qtd_clients As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_ytd_clients As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_all_clients As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_last_year_fairshare As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_this_year_fairshare As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_mtd_clients As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_ytd_payments As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_qtd_payments As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_mtd_payments As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_creditor As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_creditor_name As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_contact_name As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_zipcode As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_phone As DevExpress.XtraReports.UI.XRLabel
    End Class
End Namespace
