#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}

#End Region


Imports System.Data.SqlClient
Imports DevExpress.Utils

Namespace Creditor.Contribution.Analysis
    Public Class CreditorContributionAnalysisParametersForm
        Inherits Template.Forms.DateReportParametersForm

        Public Sub New()
            MyBase.New()
            InitializeComponent()
            AddHandler Me.Load, AddressOf ReportParametersForm_Load
        End Sub


        Public Property Parameter_Referral() As Int32
            Get
                If LookUpEdit_referral.EditValue Is Nothing OrElse LookUpEdit_referral.EditValue Is DBNull.Value Then Return -1
                Return Convert.ToInt32(LookUpEdit_referral.EditValue)
            End Get
            Set(ByVal value As Int32)
                LookUpEdit_referral.EditValue = value
            End Set
        End Property

        Dim ds As New DataSet("ds")

        Private Sub ReportParametersForm_Load(ByVal sender As Object, ByVal e As EventArgs)
            Const tableName As String = "referred_by"
            Dim tbl As DataTable
            Using cmd As SqlCommand = New System.Data.SqlClient.SqlCommand()
                With cmd
                    .Connection = New System.Data.SqlClient.SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
                    .CommandText = "lst_referred_by"
                    .CommandType = CommandType.StoredProcedure
                End With

                Using da As New SqlDataAdapter(cmd)
                    da.Fill(ds, tableName)
                    tbl = ds.Tables(tableName)
                    With tbl
                        Dim row As DataRow = tbl.NewRow()
                        row("item_key") = -1
                        row("description") = "[All Items]"
                        tbl.Rows.Add(row)
                        row.AcceptChanges()
                    End With
                End Using
            End Using

            ' Load the list of possible items
            LookUpEdit_referral.Properties.DataSource = tbl.DefaultView
            LookUpEdit_referral.EditValue = -1
            LookUpEdit_referral.Properties.AllowNullInput = DefaultBoolean.False
        End Sub
    End Class
End Namespace