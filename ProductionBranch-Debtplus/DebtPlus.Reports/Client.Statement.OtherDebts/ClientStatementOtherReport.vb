#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}
#End Region
Imports DebtPlus.Reports.Template
Imports System.IO
Imports DebtPlus.Utils

Namespace Client.Statement.OtherDebts

    Public Class ClientStatementOtherReport
        Inherits BaseXtraReportClass

        Public Sub New()
            MyBase.New()
            InitializeComponent()
        End Sub

        Private Sub InitializeComponent()
            Const ReportName As String = "DebtPlus.Reports.Client.Statement.OtherDebts.repx"

            ' See if there is a report reference that we can use
            Dim Fname As String = System.IO.Path.Combine(DebtPlus.Configuration.Config.ReportsDirectory, ReportName)
            Dim UseDefault As Boolean = True
            Try
                If (Not DebtPlus.Configuration.Config.LocalReportsOnly) AndAlso System.IO.File.Exists(Fname) Then
                    LoadLayout(Fname)
                    UseDefault = False
                End If
            Catch ex As Exception
            End Try

            ' Load the standard report definition if we need a new item
            If UseDefault Then
                Dim asm As Reflection.Assembly = Reflection.Assembly.GetExecutingAssembly
                Using ios As Stream = asm.GetManifestResourceStream(String.Format("DebtPlus.Reports.{0}", ReportName))
                    If ios IsNot Nothing Then
                        LoadLayout(ios)
                    End If
                End Using
            End If

            ' Set the script references as needed
            ScriptReferences = DebtPlus.Utils.AssemblyTools.GetAllSiblingAssemblies()
        End Sub
    End Class
End Namespace