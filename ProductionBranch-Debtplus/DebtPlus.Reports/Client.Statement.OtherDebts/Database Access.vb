﻿#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}
#End Region

Namespace Client.Statement.OtherDebts
    Public Module Database_Access
        Public Function get_view_client_addresses(ByVal clientID As Int32) As DebtPlus.LINQ.view_client_address

            Try
                Using bc As New DebtPlus.LINQ.BusinessContext()
                    Return (From v In bc.view_client_addresses Where v.client = clientID Select v).FirstOrDefault()
                End Using

            Catch ex As System.Data.SqlClient.SqlException
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error reading report data")
            End Try

            Return Nothing
        End Function

        Public Function GetCreditorList(ByRef colRecords As System.Collections.Generic.List(Of DebtPlus.LINQ.rpt_other_fee_statementResult), ByVal clientID As Int32) As String
            If colRecords IsNot Nothing Then
                Dim lst As New System.Collections.Generic.List(Of String)
                colRecords.FindAll(Function(s) s.client = clientID).ForEach(Sub(s) lst.Add(s.creditor))
                Return String.Join(", ", lst.ToArray())
            End If
            Return String.Empty
        End Function
    End Module
End Namespace
