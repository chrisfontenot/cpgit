#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}
#End Region

Public Class ClientStatementOtherReport

    ''' <summary>
    '''     Create an instance of our report
    ''' </summary>
    Public Sub New()
        MyBase.New()
        InitializeComponent()

        'AddHandler BeforePrint, AddressOf Report_BeforePrint
        'AddHandler XrTableCell_creditor.BeforePrint, AddressOf XrTableCell_creditor_BeforePrint
        'AddHandler XrBarCode_PostalCode.BeforePrint, AddressOf XrBarCode_PostalCode_BeforePrint
        'AddHandler XrLabel_Address.BeforePrint, AddressOf XrLabel_Address_BeforePrint
        'AddHandler XrTableCell_client.BeforePrint, AddressOf XrTableCell_client_BeforePrint
    End Sub

    Dim ds As New System.Data.DataSet("ds")
    Private Sub Report_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs)
        Dim rpt As DevExpress.XtraReports.UI.XtraReport = CType(sender, DevExpress.XtraReports.UI.XtraReport)
        Dim sqlInfo As DebtPlus.LINQ.SQLInfoClass = DebtPlus.LINQ.SQLInfoClass.getDefault()
        Const TableName As String = "rpt_other_fee_statement"
        Dim tbl As System.Data.DataTable = ds.Tables(TableName)
        If tbl Is Nothing Then
            Dim cn As System.Data.SqlClient.SqlConnection = New System.Data.SqlClient.SqlConnection(sqlInfo.ConnectionString)
            Try
                cn.Open()
                Using cmd As New System.Data.SqlClient.SqlCommand()
                    With cmd
                        .Connection = cn
                        .CommandType = System.Data.CommandType.StoredProcedure
                        .CommandText = "rpt_other_fee_statement"
                        .CommandTimeout = 0
                    End With

                    Using da As New System.Data.SqlClient.SqlDataAdapter(cmd)
                        da.Fill(ds, TableName)
                        tbl = ds.Tables(TableName)
                    End Using
                End Using

            Catch ex As System.Data.SqlClient.SqlException
                Dim gdr As New DebtPlus.Repository.GetDataResult() : gdr.HandleException(ex) : DebtPlus.UI.Common.ErrorHandling.HandleErrors(gdr, "Error reading report data")

            Finally
                If cn IsNot Nothing Then
                    If cn.State <> System.Data.ConnectionState.Closed Then cn.Close()
                    cn.Dispose()
                End If
            End Try
        End If

        If tbl IsNot Nothing Then
            rpt.DataSource = tbl.DefaultView
        End If
    End Sub

    Private Function view_client_address(ByVal ctl As DevExpress.XtraReports.UI.XRControl, ByVal client As System.Int32) As System.Data.DataRow
        Dim rpt As DevExpress.XtraReports.UI.XtraReport = CType(ctl.Report, DevExpress.XtraReports.UI.XtraReport)
        Dim vue As System.Data.DataView = TryCast(rpt.DataSource, System.Data.DataView)
        If vue IsNot Nothing Then
            Dim ds As System.Data.DataSet = vue.Table.DataSet
            Dim tbl As System.Data.DataTable = ds.Tables("view_client_address")

            If tbl IsNot Nothing Then
                Dim row As System.Data.DataRow = tbl.Rows.Find(client)
                If row IsNot Nothing Then Return row
            End If

            '-- Read the client information
            Dim sqlInfo As DebtPlus.LINQ.SQLInfoClass = DebtPlus.LINQ.SQLInfoClass.getDefault()
            Dim cn As System.Data.SqlClient.SqlConnection = New System.Data.SqlClient.SqlConnection(sqlInfo.ConnectionString)
            Try
                cn.Open()
                Using cmd As New System.Data.SqlClient.SqlCommand()
                    With cmd
                        .Connection = cn
                        .CommandType = System.Data.CommandType.Text
                        .CommandText = "SELECT [client],[name],[addr1],[addr2],[addr3],[zipcode] FROM view_client_address WHERE [client]=@client"
                        .Parameters.Add("@client", System.Data.SqlDbType.Int).Value = client
                    End With

                    Using da As New System.Data.SqlClient.SqlDataAdapter(cmd)
                        da.Fill(ds, "view_client_address")
                        tbl = ds.Tables("view_client_address")
                        With tbl
                            If .PrimaryKey.GetUpperBound(0) < 0 Then .PrimaryKey = New System.Data.DataColumn() {.Columns("client")}
                        End With
                    End Using
                End Using

            Catch ex As System.Data.SqlClient.SqlException
                Dim gdr As New DebtPlus.Repository.GetDataResult() : gdr.HandleException(ex) : DebtPlus.UI.Common.ErrorHandling.HandleErrors(gdr, "Error reading client address")

            Finally
                If cn IsNot Nothing Then
                    If cn.State <> System.Data.ConnectionState.Closed Then cn.Close()
                    cn.Dispose()
                End If
            End Try

            If tbl IsNot Nothing Then
                Return tbl.Rows.Find(client)
            End If
        End If

        Return Nothing
    End Function

    Private Sub XrBarCode_PostalCode_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs)
        With CType(sender, DevExpress.XtraReports.UI.XRBarCode)
            Dim rpt As DevExpress.XtraReports.UI.XtraReport = CType(.Report, DevExpress.XtraReports.UI.XtraReport)
            Dim client As System.Int32 = Convert.ToInt32(rpt.GetCurrentColumnValue("client"))
            Dim row As System.Data.DataRow = view_client_address(CType(sender, DevExpress.XtraReports.UI.XRControl), client)

            Dim Postalcode As String = String.Empty
            If row IsNot Nothing Then

                '-- Remove extra junk from the zipcode field and ensure that it is the proper size
                Postalcode = DebtPlus.Format.Strings.DigitsOnly(DebtPlus.Utils.Nulls.DStr(row("zipcode")))
                Select Case Postalcode.Length
                    Case 5, 9
                    Case Else
                        Postalcode = String.Empty
                End Select
            End If

            '-- Set the text and if there is no text then hide the value
            .Text = Postalcode
            e.Cancel = (Postalcode = String.Empty)
        End With
    End Sub

    Private Sub XrLabel_Address_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs)
        With CType(sender, DevExpress.XtraReports.UI.XRLabel)
            Dim rpt As DevExpress.XtraReports.UI.XtraReport = CType(.Report, DevExpress.XtraReports.UI.XtraReport)
            Dim client As System.Int32 = Convert.ToInt32(rpt.GetCurrentColumnValue("client"))
            Dim row As System.Data.DataRow = view_client_address(CType(sender, DevExpress.XtraReports.UI.XRControl), client)

            Dim sb As New System.Text.StringBuilder

            '-- Start with the client ID
            sb.Append(Environment.NewLine)
            sb.Append(DebtPlus.Utils.Format.Client.FormatClientID(client))

            '-- Merge the other fields in the address together to form the address block
            If row IsNot Nothing Then
                For Each key As String In New String() {"name", "addr1", "addr2", "addr3"}
                    Dim value As String = DebtPlus.Utils.Nulls.DStr(row(key)).Trim
                    If value <> String.Empty Then
                        sb.Append(Environment.NewLine)
                        sb.Append(value)
                    End If
                Next
                If sb.Length > 0 Then sb.Remove(0, 2)
            End If

            .Text = sb.ToString
        End With
    End Sub

    Private Sub XrTableCell_client_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs)
        With CType(sender, DevExpress.XtraReports.UI.XRLabel)
            Dim rpt As DevExpress.XtraReports.UI.XtraReport = CType(.Report, DevExpress.XtraReports.UI.XtraReport)
            .Text = DebtPlus.Utils.Format.Client.FormatClientID(rpt.GetCurrentColumnValue("client"))
        End With
    End Sub

    Private Sub XrTableCell_creditor_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs)
        With CType(sender, DevExpress.XtraReports.UI.XRTableCell)
            Dim rpt As DevExpress.XtraReports.UI.XtraReport = CType(.Report, DevExpress.XtraReports.UI.XtraReport)
            Dim vue As System.Data.DataView = TryCast(rpt.DataSource, System.Data.DataView)
            If vue IsNot Nothing Then

                '-- Find the list of distinct creditors for this client.
                Dim lst As New System.Collections.Generic.List(Of String)
                For Each drv As System.Data.DataRowView In vue
                    Dim CurrentCreditor As String = DebtPlus.Utils.Nulls.DStr(drv("creditor"))
                    If Not lst.Contains(CurrentCreditor) Then
                        lst.Add(CurrentCreditor)
                    End If
                Next

                '-- Turn the list into a string seperated by commas
                .Text = String.Join(", ", lst.ToArray)
            End If
        End With
    End Sub
End Class
