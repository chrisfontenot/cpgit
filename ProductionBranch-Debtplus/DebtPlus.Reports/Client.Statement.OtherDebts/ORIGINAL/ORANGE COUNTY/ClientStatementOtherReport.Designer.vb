<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Public Class ClientStatementOtherReport
    Inherits DebtPlus.Reports.Template.BaseXtraReportClass

    'XtraReport overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Designer
    'It can be modified using the Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim XrSummary1 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary
        Dim XrSummary2 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary
        Dim PostNetGenerator1 As DevExpress.XtraPrinting.BarCode.PostNetGenerator = New DevExpress.XtraPrinting.BarCode.PostNetGenerator
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(ClientStatementOtherReport))
        Me.XrLabel_creditor_name = New DevExpress.XtraReports.UI.XRLabel
        Me.XrLabel_orig_balance = New DevExpress.XtraReports.UI.XRLabel
        Me.XrLabel_balance = New DevExpress.XtraReports.UI.XRLabel
        Me.XrLabel_last_disbursement_date = New DevExpress.XtraReports.UI.XRLabel
        Me.XrLabel_last_disbursement_amount = New DevExpress.XtraReports.UI.XRLabel
        Me.GroupFooter1 = New DevExpress.XtraReports.UI.GroupFooterBand
        Me.XrLabel_total_balance = New DevExpress.XtraReports.UI.XRLabel
        Me.XrLabel_total_orig_balance = New DevExpress.XtraReports.UI.XRLabel
        Me.XrLabel10 = New DevExpress.XtraReports.UI.XRLabel
        Me.GroupHeader1 = New DevExpress.XtraReports.UI.GroupHeaderBand
        Me.XrPanel_ClientAddress = New DevExpress.XtraReports.UI.XRPanel
        Me.XrBarCode_PostalCode = New DevExpress.XtraReports.UI.XRBarCode
        Me.XrLabel_Address = New DevExpress.XtraReports.UI.XRLabel
        Me.XrPageInfo1 = New DevExpress.XtraReports.UI.XRPageInfo
        Me.XrLabel1 = New DevExpress.XtraReports.UI.XRLabel
        Me.XrLabel2 = New DevExpress.XtraReports.UI.XRLabel
        Me.XrPanel1 = New DevExpress.XtraReports.UI.XRPanel
        Me.XrLabel3 = New DevExpress.XtraReports.UI.XRLabel
        Me.XrLabel4 = New DevExpress.XtraReports.UI.XRLabel
        Me.XrLabel5 = New DevExpress.XtraReports.UI.XRLabel
        Me.XrLabel6 = New DevExpress.XtraReports.UI.XRLabel
        Me.PageHeader = New DevExpress.XtraReports.UI.PageHeaderBand
        Me.XrRichTextBox1 = New DevExpress.XtraReports.UI.XRRichText
        Me.XrControlStyle_Header = New DevExpress.XtraReports.UI.XRControlStyle
        Me.XrControlStyle_Totals = New DevExpress.XtraReports.UI.XRControlStyle
        Me.XrControlStyle_HeaderPannel = New DevExpress.XtraReports.UI.XRControlStyle
        Me.XrControlStyle_GroupHeader = New DevExpress.XtraReports.UI.XRControlStyle
        Me.GroupFooter2 = New DevExpress.XtraReports.UI.GroupFooterBand
        Me.XrLabel7 = New DevExpress.XtraReports.UI.XRLabel
        Me.XrLine1 = New DevExpress.XtraReports.UI.XRLine
        Me.XrLabel8 = New DevExpress.XtraReports.UI.XRLabel
        Me.XrTable1 = New DevExpress.XtraReports.UI.XRTable
        Me.XrTableRow1 = New DevExpress.XtraReports.UI.XRTableRow
        Me.XrTableCell1 = New DevExpress.XtraReports.UI.XRTableCell
        Me.XrTableCell_client = New DevExpress.XtraReports.UI.XRTableCell
        Me.XrTableCell2 = New DevExpress.XtraReports.UI.XRTableCell
        Me.XrTableCell_client_name = New DevExpress.XtraReports.UI.XRTableCell
        Me.XrTableRow2 = New DevExpress.XtraReports.UI.XRTableRow
        Me.XrTableCell4 = New DevExpress.XtraReports.UI.XRTableCell
        Me.XrTableCell_creditor = New DevExpress.XtraReports.UI.XRTableCell
        Me.XrLabel11 = New DevExpress.XtraReports.UI.XRLabel
        Me.GroupHeader2 = New DevExpress.XtraReports.UI.GroupHeaderBand
        CType(Me.XrRichTextBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.XrTable1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
        '
        'Detail
        '
        Me.Detail.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel_creditor_name, Me.XrLabel_orig_balance, Me.XrLabel_balance, Me.XrLabel_last_disbursement_date, Me.XrLabel_last_disbursement_amount})
        Me.Detail.HeightF = 15.0!
        Me.Detail.Name = "Detail"
        Me.Detail.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
        Me.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'XrLabel_creditor_name
        '
        Me.XrLabel_creditor_name.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "creditor_name")})
        Me.XrLabel_creditor_name.LocationFloat = New DevExpress.Utils.PointFloat(41.0!, 0.0!)
        Me.XrLabel_creditor_name.Name = "XrLabel_creditor_name"
        Me.XrLabel_creditor_name.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel_creditor_name.SizeF = New System.Drawing.SizeF(284.0!, 15.0!)
        Me.XrLabel_creditor_name.WordWrap = False
        '
        'XrLabel_orig_balance
        '
        Me.XrLabel_orig_balance.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "orig_balance", "{0:c}")})
        Me.XrLabel_orig_balance.LocationFloat = New DevExpress.Utils.PointFloat(333.0!, 0.0!)
        Me.XrLabel_orig_balance.Name = "XrLabel_orig_balance"
        Me.XrLabel_orig_balance.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel_orig_balance.SizeF = New System.Drawing.SizeF(82.0!, 15.0!)
        Me.XrLabel_orig_balance.StylePriority.UseTextAlignment = False
        Me.XrLabel_orig_balance.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
        '
        'XrLabel_balance
        '
        Me.XrLabel_balance.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "balance", "{0:c}")})
        Me.XrLabel_balance.ForeColor = System.Drawing.Color.Red
        Me.XrLabel_balance.LocationFloat = New DevExpress.Utils.PointFloat(425.0!, 0.0!)
        Me.XrLabel_balance.Name = "XrLabel_balance"
        Me.XrLabel_balance.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel_balance.SizeF = New System.Drawing.SizeF(108.0!, 15.0!)
        Me.XrLabel_balance.StylePriority.UseForeColor = False
        Me.XrLabel_balance.StylePriority.UseTextAlignment = False
        Me.XrLabel_balance.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
        '
        'XrLabel_last_disbursement_date
        '
        Me.XrLabel_last_disbursement_date.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "last_disbursement_date", "{0:d}")})
        Me.XrLabel_last_disbursement_date.LocationFloat = New DevExpress.Utils.PointFloat(542.0!, 0.0!)
        Me.XrLabel_last_disbursement_date.Name = "XrLabel_last_disbursement_date"
        Me.XrLabel_last_disbursement_date.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel_last_disbursement_date.SizeF = New System.Drawing.SizeF(82.0!, 15.0!)
        Me.XrLabel_last_disbursement_date.StylePriority.UseTextAlignment = False
        Me.XrLabel_last_disbursement_date.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
        '
        'XrLabel_last_disbursement_amount
        '
        Me.XrLabel_last_disbursement_amount.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "last_disbursement_amount", "{0:c}")})
        Me.XrLabel_last_disbursement_amount.LocationFloat = New DevExpress.Utils.PointFloat(635.0!, 0.0!)
        Me.XrLabel_last_disbursement_amount.Name = "XrLabel_last_disbursement_amount"
        Me.XrLabel_last_disbursement_amount.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel_last_disbursement_amount.SizeF = New System.Drawing.SizeF(90.0!, 15.0!)
        Me.XrLabel_last_disbursement_amount.StylePriority.UseTextAlignment = False
        Me.XrLabel_last_disbursement_amount.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        '
        'GroupFooter1
        '
        Me.GroupFooter1.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel_total_balance, Me.XrLabel_total_orig_balance, Me.XrLabel10})
        Me.GroupFooter1.HeightF = 41.0!
        Me.GroupFooter1.Name = "GroupFooter1"
        '
        'XrLabel_total_balance
        '
        Me.XrLabel_total_balance.BorderColor = System.Drawing.Color.Red
        Me.XrLabel_total_balance.Borders = DevExpress.XtraPrinting.BorderSide.Bottom
        Me.XrLabel_total_balance.BorderWidth = 2
        Me.XrLabel_total_balance.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "balance")})
        Me.XrLabel_total_balance.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
        Me.XrLabel_total_balance.ForeColor = System.Drawing.Color.Red
        Me.XrLabel_total_balance.LocationFloat = New DevExpress.Utils.PointFloat(425.0!, 8.0!)
        Me.XrLabel_total_balance.Name = "XrLabel_total_balance"
        Me.XrLabel_total_balance.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel_total_balance.SizeF = New System.Drawing.SizeF(108.0!, 15.0!)
        Me.XrLabel_total_balance.StylePriority.UseBorderColor = False
        Me.XrLabel_total_balance.StylePriority.UseBorders = False
        Me.XrLabel_total_balance.StylePriority.UseBorderWidth = False
        Me.XrLabel_total_balance.StylePriority.UseFont = False
        Me.XrLabel_total_balance.StylePriority.UseForeColor = False
        Me.XrLabel_total_balance.StylePriority.UseTextAlignment = False
        XrSummary1.FormatString = "{0:c}"
        XrSummary1.Running = DevExpress.XtraReports.UI.SummaryRunning.Group
        Me.XrLabel_total_balance.Summary = XrSummary1
        Me.XrLabel_total_balance.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
        '
        'XrLabel_total_orig_balance
        '
        Me.XrLabel_total_orig_balance.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "orig_balance")})
        Me.XrLabel_total_orig_balance.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
        Me.XrLabel_total_orig_balance.LocationFloat = New DevExpress.Utils.PointFloat(333.0!, 8.0!)
        Me.XrLabel_total_orig_balance.Name = "XrLabel_total_orig_balance"
        Me.XrLabel_total_orig_balance.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel_total_orig_balance.SizeF = New System.Drawing.SizeF(82.0!, 15.0!)
        Me.XrLabel_total_orig_balance.StylePriority.UseFont = False
        Me.XrLabel_total_orig_balance.StylePriority.UseTextAlignment = False
        XrSummary2.FormatString = "{0:c}"
        XrSummary2.Running = DevExpress.XtraReports.UI.SummaryRunning.Group
        Me.XrLabel_total_orig_balance.Summary = XrSummary2
        Me.XrLabel_total_orig_balance.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
        '
        'XrLabel10
        '
        Me.XrLabel10.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
        Me.XrLabel10.LocationFloat = New DevExpress.Utils.PointFloat(42.0!, 8.0!)
        Me.XrLabel10.Name = "XrLabel10"
        Me.XrLabel10.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel10.SizeF = New System.Drawing.SizeF(208.0!, 15.0!)
        Me.XrLabel10.StylePriority.UseFont = False
        Me.XrLabel10.Text = "TOTALS"
        '
        'GroupHeader1
        '
        Me.GroupHeader1.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrPanel_ClientAddress, Me.XrPageInfo1, Me.XrLabel1, Me.XrLabel2, Me.XrPanel1})
        Me.GroupHeader1.GroupFields.AddRange(New DevExpress.XtraReports.UI.GroupField() {New DevExpress.XtraReports.UI.GroupField("client", DevExpress.XtraReports.UI.XRColumnSortOrder.Ascending)})
        Me.GroupHeader1.GroupUnion = DevExpress.XtraReports.UI.GroupUnion.WithFirstDetail
        Me.GroupHeader1.HeightF = 300.0!
        Me.GroupHeader1.Name = "GroupHeader1"
        Me.GroupHeader1.PageBreak = DevExpress.XtraReports.UI.PageBreak.BeforeBand
        Me.GroupHeader1.RepeatEveryPage = True
        '
        'XrPanel_ClientAddress
        '
        Me.XrPanel_ClientAddress.AnchorVertical = DevExpress.XtraReports.UI.VerticalAnchorStyles.Top
        Me.XrPanel_ClientAddress.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrBarCode_PostalCode, Me.XrLabel_Address})
        Me.XrPanel_ClientAddress.LocationFloat = New DevExpress.Utils.PointFloat(25.0!, 17.0!)
        Me.XrPanel_ClientAddress.Name = "XrPanel_ClientAddress"
        Me.XrPanel_ClientAddress.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
        Me.XrPanel_ClientAddress.SizeF = New System.Drawing.SizeF(575.0!, 100.0!)
        '
        'XrBarCode_PostalCode
        '
        Me.XrBarCode_PostalCode.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
        Me.XrBarCode_PostalCode.Name = "XrBarCode_PostalCode"
        Me.XrBarCode_PostalCode.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 10, 0, 4, 100.0!)
        Me.XrBarCode_PostalCode.Scripts.OnBeforePrint = "XrBarCode_PostalCode_BeforePrint"
        Me.XrBarCode_PostalCode.ShowText = False
        Me.XrBarCode_PostalCode.SizeF = New System.Drawing.SizeF(567.0!, 17.0!)
        Me.XrBarCode_PostalCode.StylePriority.UsePadding = False
        Me.XrBarCode_PostalCode.Symbology = PostNetGenerator1
        Me.XrBarCode_PostalCode.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'XrLabel_Address
        '
        Me.XrLabel_Address.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 17.0!)
        Me.XrLabel_Address.Multiline = True
        Me.XrLabel_Address.Name = "XrLabel_Address"
        Me.XrLabel_Address.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel_Address.Scripts.OnBeforePrint = "XrLabel_Address_BeforePrint"
        Me.XrLabel_Address.SizeF = New System.Drawing.SizeF(567.0!, 17.0!)
        Me.XrLabel_Address.Text = "XrLabel_Address"
        Me.XrLabel_Address.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        Me.XrLabel_Address.WordWrap = False
        '
        'XrPageInfo1
        '
        Me.XrPageInfo1.Format = "{0:dddd, MMMM d, yyyy}"
        Me.XrPageInfo1.LocationFloat = New DevExpress.Utils.PointFloat(25.0!, 142.0!)
        Me.XrPageInfo1.Name = "XrPageInfo1"
        Me.XrPageInfo1.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrPageInfo1.PageInfo = DevExpress.XtraPrinting.PageInfo.DateTime
        Me.XrPageInfo1.SizeF = New System.Drawing.SizeF(300.0!, 25.0!)
        '
        'XrLabel1
        '
        Me.XrLabel1.LocationFloat = New DevExpress.Utils.PointFloat(25.0!, 183.0!)
        Me.XrLabel1.Name = "XrLabel1"
        Me.XrLabel1.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel1.SizeF = New System.Drawing.SizeF(375.0!, 17.0!)
        Me.XrLabel1.Text = "Dear [client_name]:"
        '
        'XrLabel2
        '
        Me.XrLabel2.LocationFloat = New DevExpress.Utils.PointFloat(25.0!, 217.0!)
        Me.XrLabel2.Multiline = True
        Me.XrLabel2.Name = "XrLabel2"
        Me.XrLabel2.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel2.SizeF = New System.Drawing.SizeF(717.0!, 42.0!)
        Me.XrLabel2.Text = resources.GetString("XrLabel2.Text")
        '
        'XrPanel1
        '
        Me.XrPanel1.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel3, Me.XrLabel4, Me.XrLabel5, Me.XrLabel6})
        Me.XrPanel1.LocationFloat = New DevExpress.Utils.PointFloat(8.0!, 275.0!)
        Me.XrPanel1.Name = "XrPanel1"
        Me.XrPanel1.SizeF = New System.Drawing.SizeF(722.0!, 17.0!)
        Me.XrPanel1.StyleName = "XrControlStyle_HeaderPannel"
        '
        'XrLabel3
        '
        Me.XrLabel3.LocationFloat = New DevExpress.Utils.PointFloat(33.0!, 1.0!)
        Me.XrLabel3.Name = "XrLabel3"
        Me.XrLabel3.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel3.SizeF = New System.Drawing.SizeF(208.0!, 15.0!)
        Me.XrLabel3.StylePriority.UseTextAlignment = False
        Me.XrLabel3.Text = "NAME"
        Me.XrLabel3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        '
        'XrLabel4
        '
        Me.XrLabel4.LocationFloat = New DevExpress.Utils.PointFloat(534.0!, 0.0!)
        Me.XrLabel4.Name = "XrLabel4"
        Me.XrLabel4.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel4.SizeF = New System.Drawing.SizeF(184.0!, 15.0!)
        Me.XrLabel4.StylePriority.UseTextAlignment = False
        Me.XrLabel4.Text = "LAST PAID"
        Me.XrLabel4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
        '
        'XrLabel5
        '
        Me.XrLabel5.LocationFloat = New DevExpress.Utils.PointFloat(417.0!, 0.0!)
        Me.XrLabel5.Name = "XrLabel5"
        Me.XrLabel5.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel5.SizeF = New System.Drawing.SizeF(108.0!, 15.0!)
        Me.XrLabel5.StylePriority.UseTextAlignment = False
        Me.XrLabel5.Text = "TOTAL DUE"
        Me.XrLabel5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
        '
        'XrLabel6
        '
        Me.XrLabel6.LocationFloat = New DevExpress.Utils.PointFloat(325.0!, 0.0!)
        Me.XrLabel6.Name = "XrLabel6"
        Me.XrLabel6.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel6.SizeF = New System.Drawing.SizeF(82.0!, 15.0!)
        Me.XrLabel6.StylePriority.UseTextAlignment = False
        Me.XrLabel6.Text = "ORIGINAL"
        Me.XrLabel6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
        '
        'PageHeader
        '
        Me.PageHeader.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrRichTextBox1})
        Me.PageHeader.HeightF = 108.0!
        Me.PageHeader.Name = "PageHeader"
        '
        'XrRichTextBox1
        '
        Me.XrRichTextBox1.AnchorVertical = DevExpress.XtraReports.UI.VerticalAnchorStyles.Top
        Me.XrRichTextBox1.BackColor = System.Drawing.Color.Transparent
        Me.XrRichTextBox1.LocationFloat = New DevExpress.Utils.PointFloat(25.0!, 0.0!)
        Me.XrRichTextBox1.Name = "XrRichTextBox1"
        Me.XrRichTextBox1.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
        Me.XrRichTextBox1.SerializableRtfString = resources.GetString("XrRichTextBox1.SerializableRtfString")
        Me.XrRichTextBox1.SizeF = New System.Drawing.SizeF(725.0!, 108.0!)
        Me.XrRichTextBox1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'XrControlStyle_Header
        '
        Me.XrControlStyle_Header.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.XrControlStyle_Header.ForeColor = System.Drawing.Color.Teal
        Me.XrControlStyle_Header.Name = "XrControlStyle_Header"
        Me.XrControlStyle_Header.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
        Me.XrControlStyle_Header.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        '
        'XrControlStyle_Totals
        '
        Me.XrControlStyle_Totals.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.XrControlStyle_Totals.Name = "XrControlStyle_Totals"
        Me.XrControlStyle_Totals.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
        Me.XrControlStyle_Totals.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
        '
        'XrControlStyle_HeaderPannel
        '
        Me.XrControlStyle_HeaderPannel.BackColor = System.Drawing.Color.Teal
        Me.XrControlStyle_HeaderPannel.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.XrControlStyle_HeaderPannel.ForeColor = System.Drawing.Color.White
        Me.XrControlStyle_HeaderPannel.Name = "XrControlStyle_HeaderPannel"
        Me.XrControlStyle_HeaderPannel.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
        Me.XrControlStyle_HeaderPannel.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
        '
        'XrControlStyle_GroupHeader
        '
        Me.XrControlStyle_GroupHeader.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.XrControlStyle_GroupHeader.ForeColor = System.Drawing.Color.Maroon
        Me.XrControlStyle_GroupHeader.Name = "XrControlStyle_GroupHeader"
        Me.XrControlStyle_GroupHeader.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        '
        'GroupFooter2
        '
        Me.GroupFooter2.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel7, Me.XrLine1, Me.XrLabel8, Me.XrTable1, Me.XrLabel11})
        Me.GroupFooter2.HeightF = 199.0!
        Me.GroupFooter2.Level = 1
        Me.GroupFooter2.Name = "GroupFooter2"
        Me.GroupFooter2.PrintAtBottom = True
        '
        'XrLabel7
        '
        Me.XrLabel7.BorderColor = System.Drawing.Color.Gray
        Me.XrLabel7.Borders = DevExpress.XtraPrinting.BorderSide.Bottom
        Me.XrLabel7.Font = New System.Drawing.Font("Times New Roman", 7.0!, System.Drawing.FontStyle.Italic)
        Me.XrLabel7.ForeColor = System.Drawing.Color.Gray
        Me.XrLabel7.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 8.0!)
        Me.XrLabel7.Name = "XrLabel7"
        Me.XrLabel7.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel7.SizeF = New System.Drawing.SizeF(750.0!, 17.0!)
        Me.XrLabel7.StylePriority.UseBorderColor = False
        Me.XrLabel7.StylePriority.UseBorders = False
        Me.XrLabel7.StylePriority.UseFont = False
        Me.XrLabel7.StylePriority.UseForeColor = False
        Me.XrLabel7.StylePriority.UseTextAlignment = False
        Me.XrLabel7.Text = "(Please tear off and return bottom portion with payment)"
        Me.XrLabel7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
        '
        'XrLine1
        '
        Me.XrLine1.LocationFloat = New DevExpress.Utils.PointFloat(625.0!, 108.0!)
        Me.XrLine1.Name = "XrLine1"
        Me.XrLine1.SizeF = New System.Drawing.SizeF(119.0!, 25.0!)
        '
        'XrLabel8
        '
        Me.XrLabel8.LocationFloat = New DevExpress.Utils.PointFloat(383.0!, 92.0!)
        Me.XrLabel8.Name = "XrLabel8"
        Me.XrLabel8.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel8.SizeF = New System.Drawing.SizeF(233.0!, 33.0!)
        Me.XrLabel8.Text = "Enclosed please find my check or money order in the amount of $"
        '
        'XrTable1
        '
        Me.XrTable1.LocationFloat = New DevExpress.Utils.PointFloat(33.0!, 42.0!)
        Me.XrTable1.Name = "XrTable1"
        Me.XrTable1.Rows.AddRange(New DevExpress.XtraReports.UI.XRTableRow() {Me.XrTableRow1, Me.XrTableRow2})
        Me.XrTable1.SizeF = New System.Drawing.SizeF(717.0!, 30.0!)
        '
        'XrTableRow1
        '
        Me.XrTableRow1.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.XrTableCell1, Me.XrTableCell_client, Me.XrTableCell2, Me.XrTableCell_client_name})
        Me.XrTableRow1.Name = "XrTableRow1"
        Me.XrTableRow1.Weight = 0.6
        '
        'XrTableCell1
        '
        Me.XrTableCell1.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
        Me.XrTableCell1.Name = "XrTableCell1"
        Me.XrTableCell1.StylePriority.UseFont = False
        Me.XrTableCell1.Text = "Client #"
        Me.XrTableCell1.Weight = 1
        '
        'XrTableCell_client
        '
        Me.XrTableCell_client.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
        Me.XrTableCell_client.Name = "XrTableCell_client"
        Me.XrTableCell_client.Scripts.OnBeforePrint = "XrTableCell_client_BeforePrint"
        Me.XrTableCell_client.StylePriority.UseFont = False
        Me.XrTableCell_client.Weight = 1
        '
        'XrTableCell2
        '
        Me.XrTableCell2.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
        Me.XrTableCell2.Name = "XrTableCell2"
        Me.XrTableCell2.StylePriority.UseFont = False
        Me.XrTableCell2.Text = "Client's Name"
        Me.XrTableCell2.Weight = 1
        '
        'XrTableCell_client_name
        '
        Me.XrTableCell_client_name.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
        Me.XrTableCell_client_name.Name = "XrTableCell_client_name"
        Me.XrTableCell_client_name.StylePriority.UseFont = False
        Me.XrTableCell_client_name.Text = "[client_name]"
        Me.XrTableCell_client_name.Weight = 4.17
        '
        'XrTableRow2
        '
        Me.XrTableRow2.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.XrTableCell4, Me.XrTableCell_creditor})
        Me.XrTableRow2.Name = "XrTableRow2"
        Me.XrTableRow2.Weight = 0.59999999999999987
        '
        'XrTableCell4
        '
        Me.XrTableCell4.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
        Me.XrTableCell4.Name = "XrTableCell4"
        Me.XrTableCell4.StylePriority.UseFont = False
        Me.XrTableCell4.Text = "Cred #"
        Me.XrTableCell4.Weight = 1
        '
        'XrTableCell_creditor
        '
        Me.XrTableCell_creditor.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
        Me.XrTableCell_creditor.Name = "XrTableCell_creditor"
        Me.XrTableCell_creditor.Scripts.OnBeforePrint = "XrTableCell_creditor_BeforePrint"
        Me.XrTableCell_creditor.StylePriority.UseFont = False
        Me.XrTableCell_creditor.Weight = 6.17
        '
        'XrLabel11
        '
        Me.XrLabel11.LocationFloat = New DevExpress.Utils.PointFloat(33.0!, 92.0!)
        Me.XrLabel11.Multiline = True
        Me.XrLabel11.Name = "XrLabel11"
        Me.XrLabel11.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel11.SizeF = New System.Drawing.SizeF(333.0!, 100.0!)
        Me.XrLabel11.Text = "Payments should be mailed to:" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Attn: Payment Processing" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "CCS of Orange County" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & _
            "1920 Old Tustin Ave" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Santa Ana CA 92711"
        '
        'GroupHeader2
        '
        Me.GroupHeader2.GroupFields.AddRange(New DevExpress.XtraReports.UI.GroupField() {New DevExpress.XtraReports.UI.GroupField("client", DevExpress.XtraReports.UI.XRColumnSortOrder.Ascending)})
        Me.GroupHeader2.HeightF = 0.0!
        Me.GroupHeader2.Level = 1
        Me.GroupHeader2.Name = "GroupHeader2"
        '
        'ClientStatementOtherReport
        '
        Me.Bands.AddRange(New DevExpress.XtraReports.UI.Band() {Me.Detail, Me.GroupFooter1, Me.GroupHeader1, Me.PageHeader, Me.GroupFooter2, Me.GroupHeader2})
        Me.PrintOnEmptyDataSource = False
        Me.ScriptLanguage = DevExpress.XtraReports.ScriptLanguage.VisualBasic
        Me.ScriptReferencesString = resources.GetString("$this.ScriptReferencesString")
        Me.Scripts.OnBeforePrint = "Report_BeforePrint"
        Me.ScriptsSource = resources.GetString("$this.ScriptsSource")
        Me.StyleSheet.AddRange(New DevExpress.XtraReports.UI.XRControlStyle() {Me.XrControlStyle_Header, Me.XrControlStyle_Totals, Me.XrControlStyle_HeaderPannel, Me.XrControlStyle_GroupHeader})
        Me.Version = "11.1"
        CType(Me.XrRichTextBox1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.XrTable1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me, System.ComponentModel.ISupportInitialize).EndInit()

    End Sub

    Friend WithEvents GroupFooter1 As DevExpress.XtraReports.UI.GroupFooterBand
    Friend WithEvents GroupHeader1 As DevExpress.XtraReports.UI.GroupHeaderBand
    Friend WithEvents XrPanel_ClientAddress As DevExpress.XtraReports.UI.XRPanel
    Friend WithEvents XrBarCode_PostalCode As DevExpress.XtraReports.UI.XRBarCode
    Friend WithEvents XrLabel_Address As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrPageInfo1 As DevExpress.XtraReports.UI.XRPageInfo
    Friend WithEvents XrLabel1 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel2 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrPanel1 As DevExpress.XtraReports.UI.XRPanel
    Friend WithEvents PageHeader As DevExpress.XtraReports.UI.PageHeaderBand
    Friend WithEvents XrRichTextBox1 As DevExpress.XtraReports.UI.XRRichText
    Friend WithEvents XrControlStyle_Header As DevExpress.XtraReports.UI.XRControlStyle
    Friend WithEvents XrControlStyle_Totals As DevExpress.XtraReports.UI.XRControlStyle
    Friend WithEvents XrControlStyle_HeaderPannel As DevExpress.XtraReports.UI.XRControlStyle
    Friend WithEvents XrControlStyle_GroupHeader As DevExpress.XtraReports.UI.XRControlStyle
    Friend WithEvents XrLabel3 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel4 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel5 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel6 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel_creditor_name As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel_orig_balance As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel_balance As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel_last_disbursement_date As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel_last_disbursement_amount As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel_total_balance As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel_total_orig_balance As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel10 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents GroupFooter2 As DevExpress.XtraReports.UI.GroupFooterBand
    Friend WithEvents GroupHeader2 As DevExpress.XtraReports.UI.GroupHeaderBand
    Friend WithEvents XrLabel7 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLine1 As DevExpress.XtraReports.UI.XRLine
    Friend WithEvents XrLabel8 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrTable1 As DevExpress.XtraReports.UI.XRTable
    Friend WithEvents XrTableRow1 As DevExpress.XtraReports.UI.XRTableRow
    Friend WithEvents XrTableCell1 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents XrTableCell_client As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents XrTableCell2 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents XrTableCell_client_name As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents XrTableRow2 As DevExpress.XtraReports.UI.XRTableRow
    Friend WithEvents XrTableCell4 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents XrTableCell_creditor As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents XrLabel11 As DevExpress.XtraReports.UI.XRLabel
End Class
