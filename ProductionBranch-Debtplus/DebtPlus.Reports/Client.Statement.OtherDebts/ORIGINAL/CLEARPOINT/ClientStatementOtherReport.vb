#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}
#End Region

Namespace Client.Statement.OtherDebts
    Public Class ClientStatementOtherReport

        ''' <summary>
        ''' Create an instance of our report
        ''' </summary>
        Public Sub New()
            MyBase.New()
            InitializeComponent()

            ' Set the script references as needed
            ScriptReferences = DebtPlus.Utils.AssemblyTools.GetAllSiblingAssemblies()

            'AddHandler BeforePrint, AddressOf ClientStatementOtherReport_BeforePrint
            'AddHandler XrTableCell_creditor.BeforePrint, AddressOf XrTableCell_creditor_BeforePrint
            'AddHandler XrBarCode_PostalCode.BeforePrint, AddressOf XrBarCode_PostalCode_BeforePrint
            'AddHandler XrLabel_Address.BeforePrint, AddressOf XrLabel_Address_BeforePrint
            'AddHandler XrTableCell_client.BeforePrint, AddressOf XrTableCell_client_BeforePrint
        End Sub

        Private colRecords As System.Collections.Generic.List(Of DebtPlus.LINQ.rpt_other_fee_statementResult)

        Private Sub ClientStatementOtherReport_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs)
            Dim rpt As DevExpress.XtraReports.UI.XtraReport = CType(sender, DevExpress.XtraReports.UI.XtraReport)

            If colRecords Is Nothing Then
                Try
                    Using bc As New DebtPlus.LINQ.BusinessContext()
                        colRecords = bc.rpt_other_fee_statement()
                    End Using

                Catch ex As System.Data.SqlClient.SqlException
                    Using gdr As New DebtPlus.Repository.GetDataResult()
                        gdr.HandleException(ex)
                        DebtPlus.UI.Common.ErrorHandling.HandleErrors(gdr, "Error reading report data")
                    End Using
                End Try
            End If

            rpt.DataSource = colRecords
        End Sub

        Private cached_view_client_address As DebtPlus.LINQ.view_client_address
        Private Function view_client_address(ByVal clientID As Int32) As DebtPlus.LINQ.view_client_address

            ' Return the cached copy first
            If cached_view_client_address IsNot Nothing AndAlso cached_view_client_address.client = clientID Then
                Return cached_view_client_address
            End If

            ' Retrieve the values from the database
            cached_view_client_address = DebtPlus.Reports.Client.Statement.OtherDebts.Database_Access.get_view_client_addresses(clientID)

            ' If the value now matches then return the result
            If cached_view_client_address IsNot Nothing AndAlso cached_view_client_address.client = clientID Then
                Return cached_view_client_address
            End If

            ' Return a no-match value
            Return Nothing
        End Function

        Private Sub XrBarCode_PostalCode_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs)
            Dim bar As DevExpress.XtraReports.UI.XRBarCode = CType(sender, DevExpress.XtraReports.UI.XRBarCode)
            Dim rpt As DevExpress.XtraReports.UI.XtraReport = CType(bar.Report, DevExpress.XtraReports.UI.XtraReport)
            Dim client As System.Int32 = Convert.ToInt32(rpt.GetCurrentColumnValue("client"))

            ' Find the client row
            Dim vc As DebtPlus.LINQ.view_client_address = view_client_address(client)
            If vc IsNot Nothing Then
                Dim Postalcode As String = DebtPlus.Utils.Format.Strings.DigitsOnly(vc.zipcode)

                ' Process the postal-code of exactly the proper length
                Select Case Postalcode.Length
                    Case 5, 9
                        bar.Text = Postalcode
                        e.Cancel = False
                        Return

                    Case Else
                        Exit Select
                End Select
            End If

            bar.Text = String.Empty
            e.Cancel = True
        End Sub

        Private Sub XrLabel_Address_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs)
            Dim lbl As DevExpress.XtraReports.UI.XRLabel = CType(sender, DevExpress.XtraReports.UI.XRLabel)
            Dim rpt As DevExpress.XtraReports.UI.XtraReport = CType(lbl.Report, DevExpress.XtraReports.UI.XtraReport)
            Dim client As System.Int32 = Convert.ToInt32(rpt.GetCurrentColumnValue("client"))

            ' Find the client row
            Dim vc As DebtPlus.LINQ.view_client_address = view_client_address(client)
            Dim sb As New System.Text.StringBuilder()

            '-- Start with the client ID
            sb.Append(Environment.NewLine)
            sb.Append(DebtPlus.Utils.Format.Client.FormatClientID(client))

            If vc IsNot Nothing Then
                For Each value As String In New String() {vc.name, vc.addr1, vc.addr2, vc.addr3}
                    If Not String.IsNullOrEmpty(value) Then
                        sb.Append(Environment.NewLine)
                        sb.Append(value)
                    End If
                Next
            End If

            sb.Remove(0, 2)
            lbl.Text = sb.ToString()
        End Sub

        Private Sub XrTableCell_client_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs)
            Dim lbl As DevExpress.XtraReports.UI.XRLabel = CType(sender, DevExpress.XtraReports.UI.XRLabel)
            Dim rpt As DevExpress.XtraReports.UI.XtraReport = CType(lbl.Report, DevExpress.XtraReports.UI.XtraReport)
            Dim client As System.Int32 = Convert.ToInt32(rpt.GetCurrentColumnValue("client"))
            lbl.Text = DebtPlus.Utils.Format.Client.FormatClientID(client)
        End Sub

        Private Sub XrTableCell_creditor_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs)
            Dim cell As DevExpress.XtraReports.UI.XRTableCell = CType(sender, DevExpress.XtraReports.UI.XRTableCell)
            Dim rpt As DevExpress.XtraReports.UI.XtraReport = CType(cell.Report, DevExpress.XtraReports.UI.XtraReport)
            Dim client As Int32 = Convert.ToInt32(rpt.GetCurrentColumnValue("client"))
            cell.Text = DebtPlus.Reports.Client.Statement.OtherDebts.Database_Access.GetCreditorList(colRecords, client)
        End Sub
    End Class
End Namespace
