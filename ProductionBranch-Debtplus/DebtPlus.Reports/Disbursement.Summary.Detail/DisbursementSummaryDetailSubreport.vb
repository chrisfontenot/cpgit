#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}

#End Region

Option Compare Binary
Option Explicit On
Option Strict On

Imports System.ComponentModel
Imports DebtPlus.Reports.Template

Imports System.Drawing.Printing
Imports DevExpress.XtraReports.UI

Namespace Disbursement.SummaryDetail
    Public Class DisbursementSummaryDetailSubreport
        Inherits TemplateXtraReportClass

        ''' <summary>
        ''' Create an instance of our report
        ''' </summary>
        Public Sub New()
            MyBase.New()
            InitializeComponent()

            XrLabel_account_number.DataBindings.Add("Text", Nothing, "account_number")
            XrLabel_check_date.DataBindings.Add("Text", Nothing, "check_date", "{0:d}")
            XrLabel_checknum.DataBindings.Add("Text", Nothing, "checknum")

            XrLabel_billed.DataBindings.Add("Text", Nothing, "billed", "{0:c}")
            XrLabel_deducted.DataBindings.Add("Text", Nothing, "deducted", "{0:c}")
            XrLabel_gross.DataBindings.Add("Text", Nothing, "gross", "{0:c}")
            XrLabel_net.DataBindings.Add("Text", Nothing, "net", "{0:c}")

            XrLabel_total_billed.DataBindings.Add("Text", Nothing, "billed")
            XrLabel_total_deducted.DataBindings.Add("Text", Nothing, "deducted")
            XrLabel_total_gross.DataBindings.Add("Text", Nothing, "gross")
            XrLabel_total_net.DataBindings.Add("Text", Nothing, "net")

            XrLabel_group_billed.DataBindings.Add("Text", Nothing, "billed")
            XrLabel_group_deducted.DataBindings.Add("Text", Nothing, "deducted")
            XrLabel_group_gross.DataBindings.Add("Text", Nothing, "gross")
            XrLabel_group_net.DataBindings.Add("Text", Nothing, "net")

            AddHandler BeforePrint, AddressOf MyBase_BeforePrint
        End Sub

        Public Overrides ReadOnly Property ReportTitle() As String
            Get
                Return "Disbursement Summary And Detatil"
            End Get
        End Property

        Protected vue As DataView

        Public Sub New(ByVal vue As DataView, ByVal Subtitle As String)
            MyClass.New()
            Me.vue = vue
            Me.privateSubtitle = Subtitle
        End Sub

        Protected privateSubtitle As String

        Public Overrides ReadOnly Property ReportSubTitle() As String
            Get
                Return privateSubtitle
            End Get
        End Property

        Protected Overridable Sub MyBase_BeforePrint(ByVal sender As Object, ByVal e As PrintEventArgs)
            DataSource = vue

            ' Create a calculated field, and add it to the report's collection
            Dim calcField As New CalculatedField(DataSource, DataMember)
            CalculatedFields.Add(calcField)

            ' Define its name, field type and expression.
            calcField.Name = "modified_check_date"
            calcField.FieldType = FieldType.DateTime
            calcField.Expression = "GetDate([check_date])"

            ' Define the calculated field as 
            ' a grouping criteria for the group header band.
            Dim groupField As New GroupField()
            groupField.FieldName = "modified_check_date"
            groupField.SortOrder = XRColumnSortOrder.Ascending
            GroupHeader1.GroupFields.Add(groupField)
        End Sub
    End Class
End Namespace
