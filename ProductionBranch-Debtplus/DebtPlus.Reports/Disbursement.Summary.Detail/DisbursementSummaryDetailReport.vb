#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}

#End Region

Option Compare Binary
Option Explicit On
Option Strict On

Imports System.ComponentModel
Imports DebtPlus.Reports.Template

Imports System.Drawing.Printing
Imports DevExpress.XtraReports.UI
Imports DevExpress.LookAndFeel
Imports System.Data.SqlClient
Imports System.Threading

Namespace Disbursement.SummaryDetail
    Public Class DroppedClientsReportClass
        Inherits DatedTemplateXtraReportClass

        Public Sub New()
            MyBase.New()
            InitializeComponent()
            AddHandler Me.BeforePrint, AddressOf Report_BeforePrint

            ' Populate the columns on the Report
            With XrLabel_billed
                .DataBindings.Clear()
                .DataBindings.Add("Text", Nothing, "billed", "{0:c}")
                .DataBindings.Add("Tag", Nothing, "creditor")
                AddHandler .PreviewClick, AddressOf Report_PreviewClick
            End With

            With XrLabel_deducted
                .DataBindings.Clear()
                .DataBindings.Add("Text", Nothing, "deducted", "{0:c}")
                .DataBindings.Add("Tag", Nothing, "creditor")
                AddHandler .PreviewClick, AddressOf Report_PreviewClick
            End With

            With XrLabel_gross
                .DataBindings.Clear()
                .DataBindings.Add("Text", Nothing, "gross", "{0:c}")
                .DataBindings.Add("Tag", Nothing, "creditor")
                AddHandler .PreviewClick, AddressOf Report_PreviewClick
            End With

            With XrLabel_net
                .DataBindings.Clear()
                .DataBindings.Add("Text", Nothing, "net", "{0:c}")
                .DataBindings.Add("Tag", Nothing, "creditor")
                AddHandler .PreviewClick, AddressOf Report_PreviewClick
            End With

            With XrLabel_total_billed
                .DataBindings.Clear()
                .DataBindings.Add("Text", Nothing, "billed")
            End With

            With XrLabel_total_deducted
                .DataBindings.Clear()
                .DataBindings.Add("Text", Nothing, "deducted")
            End With

            With XrLabel_total_gross
                .DataBindings.Clear()
                .DataBindings.Add("Text", Nothing, "gross")
            End With

            With XrLabel_total_net
                .DataBindings.Clear()
                .DataBindings.Add("Text", Nothing, "net")
            End With

            With XrLabel_creditor
                .DataBindings.Clear()
                .DataBindings.Add("Tag", Nothing, "creditor")
                AddHandler .PreviewClick, AddressOf Report_PreviewClick
            End With

            ' Break the report by the type
            Dim GroupFields1 As New GroupField("creditor")
            GroupHeader1.GroupFields.Add(GroupFields1)
        End Sub

        Public Overrides ReadOnly Property ReportTitle() As String
            Get
                Return "Disbursement Summary and Detail"
            End Get
        End Property

#Region " Component Designer generated code "

        'Component overrides dispose to clean up the component list.
        Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
            If disposing Then
                If components IsNot Nothing Then
                    components.Dispose()
                End If
                ds.Dispose()
            End If
            MyBase.Dispose(disposing)
        End Sub

        Friend WithEvents XrPanel1 As DevExpress.XtraReports.UI.XRPanel
        Friend WithEvents XrLabel3 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel1 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel2 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel4 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel6 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents ReportFooter As DevExpress.XtraReports.UI.ReportFooterBand
        Friend WithEvents XrLabel_total_net As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_total_gross As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_total_deducted As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_total_billed As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel15 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLine1 As DevExpress.XtraReports.UI.XRLine
        Friend WithEvents GroupHeader1 As DevExpress.XtraReports.UI.GroupHeaderBand
        Friend WithEvents GroupFooter1 As DevExpress.XtraReports.UI.GroupFooterBand
        Friend WithEvents XrLabel_creditor As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_billed As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_deducted As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_gross As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_net As DevExpress.XtraReports.UI.XRLabel

        'Required by the Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Designer
        'It can be modified using the Designer.
        'Do not modify it using the code editor.
        <System.Diagnostics.DebuggerStepThrough()>
        Private Sub InitializeComponent()
            Dim XrSummary1 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary
            Dim XrSummary2 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary
            Dim XrSummary3 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary
            Dim XrSummary4 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary
            Dim XrSummary5 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary
            Dim XrSummary6 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary
            Dim XrSummary7 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary
            Dim XrSummary8 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary
            Me.XrPanel1 = New DevExpress.XtraReports.UI.XRPanel
            Me.XrLabel3 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel1 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel2 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel4 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel6 = New DevExpress.XtraReports.UI.XRLabel
            Me.ReportFooter = New DevExpress.XtraReports.UI.ReportFooterBand
            Me.XrLabel_total_net = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_total_gross = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_total_deducted = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_total_billed = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel15 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLine1 = New DevExpress.XtraReports.UI.XRLine
            Me.GroupHeader1 = New DevExpress.XtraReports.UI.GroupHeaderBand
            Me.GroupFooter1 = New DevExpress.XtraReports.UI.GroupFooterBand
            Me.XrLabel_creditor = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_billed = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_deducted = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_gross = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_net = New DevExpress.XtraReports.UI.XRLabel
            CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
            '
            'Detail
            '
            Me.Detail.Height = 0
            '
            'PageHeader
            '
            Me.PageHeader.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrPanel1})
            Me.PageHeader.Height = 154
            Me.PageHeader.Controls.SetChildIndex(Me.XrPanel1, 0)
            Me.PageHeader.Controls.SetChildIndex(Me.XrPanel_AgencyAddress, 0)
            Me.PageHeader.Controls.SetChildIndex(Me.XrLabel_Title, 0)
            Me.PageHeader.Controls.SetChildIndex(Me.XrLine_Title, 0)
            Me.PageHeader.Controls.SetChildIndex(Me.XrLabel_Subtitle, 0)
            Me.PageHeader.Controls.SetChildIndex(Me.XrPageInfo1, 0)
            '
            'XrPanel1
            '
            Me.XrPanel1.BackColor = System.Drawing.Color.Teal
            Me.XrPanel1.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel3, Me.XrLabel1, Me.XrLabel2, Me.XrLabel4, Me.XrLabel6})
            Me.XrPanel1.Location = New System.Drawing.Point(0, 133)
            Me.XrPanel1.Name = "XrPanel1"
            Me.XrPanel1.Size = New System.Drawing.Size(742, 17)
            '
            'XrLabel3
            '
            Me.XrLabel3.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.XrLabel3.ForeColor = System.Drawing.Color.White
            Me.XrLabel3.Location = New System.Drawing.Point(1, 1)
            Me.XrLabel3.Name = "XrLabel3"
            Me.XrLabel3.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel3.Size = New System.Drawing.Size(341, 15)
            Me.XrLabel3.StylePriority.UseTextAlignment = False
            Me.XrLabel3.Text = "CREDITOR ID AND NAME"
            Me.XrLabel3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            '
            'XrLabel1
            '
            Me.XrLabel1.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.XrLabel1.ForeColor = System.Drawing.Color.White
            Me.XrLabel1.Location = New System.Drawing.Point(642, 0)
            Me.XrLabel1.Name = "XrLabel1"
            Me.XrLabel1.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel1.Size = New System.Drawing.Size(100, 15)
            Me.XrLabel1.StylePriority.UseTextAlignment = False
            Me.XrLabel1.Text = "NET"
            Me.XrLabel1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel2
            '
            Me.XrLabel2.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.XrLabel2.ForeColor = System.Drawing.Color.White
            Me.XrLabel2.Location = New System.Drawing.Point(542, 0)
            Me.XrLabel2.Name = "XrLabel2"
            Me.XrLabel2.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel2.Size = New System.Drawing.Size(100, 15)
            Me.XrLabel2.StylePriority.UseTextAlignment = False
            Me.XrLabel2.Text = "BILL"
            Me.XrLabel2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel4
            '
            Me.XrLabel4.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.XrLabel4.ForeColor = System.Drawing.Color.White
            Me.XrLabel4.Location = New System.Drawing.Point(342, 0)
            Me.XrLabel4.Name = "XrLabel4"
            Me.XrLabel4.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel4.Size = New System.Drawing.Size(100, 15)
            Me.XrLabel4.StylePriority.UseTextAlignment = False
            Me.XrLabel4.Text = "GROSS"
            Me.XrLabel4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel6
            '
            Me.XrLabel6.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.XrLabel6.ForeColor = System.Drawing.Color.White
            Me.XrLabel6.Location = New System.Drawing.Point(442, 0)
            Me.XrLabel6.Name = "XrLabel6"
            Me.XrLabel6.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel6.Size = New System.Drawing.Size(100, 15)
            Me.XrLabel6.StylePriority.UseTextAlignment = False
            Me.XrLabel6.Text = "DEDUCT"
            Me.XrLabel6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'ReportFooter
            '
            Me.ReportFooter.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel_total_net, Me.XrLabel_total_gross, Me.XrLabel_total_deducted, Me.XrLabel_total_billed, Me.XrLabel15, Me.XrLine1})
            Me.ReportFooter.Height = 42
            Me.ReportFooter.Name = "ReportFooter"
            '
            'XrLabel_total_net
            '
            Me.XrLabel_total_net.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.XrLabel_total_net.ForeColor = System.Drawing.SystemColors.ControlText
            Me.XrLabel_total_net.Location = New System.Drawing.Point(642, 17)
            Me.XrLabel_total_net.Name = "XrLabel_total_net"
            Me.XrLabel_total_net.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_total_net.Size = New System.Drawing.Size(100, 15)
            Me.XrLabel_total_net.StylePriority.UseForeColor = False
            Me.XrLabel_total_net.StylePriority.UseTextAlignment = False
            XrSummary1.FormatString = "{0:c}"
            XrSummary1.Running = DevExpress.XtraReports.UI.SummaryRunning.Report
            Me.XrLabel_total_net.Summary = XrSummary1
            Me.XrLabel_total_net.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel_total_gross
            '
            Me.XrLabel_total_gross.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.XrLabel_total_gross.ForeColor = System.Drawing.SystemColors.ControlText
            Me.XrLabel_total_gross.Location = New System.Drawing.Point(342, 17)
            Me.XrLabel_total_gross.Name = "XrLabel_total_gross"
            Me.XrLabel_total_gross.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_total_gross.Size = New System.Drawing.Size(100, 15)
            Me.XrLabel_total_gross.StylePriority.UseForeColor = False
            Me.XrLabel_total_gross.StylePriority.UseTextAlignment = False
            XrSummary2.FormatString = "{0:c}"
            XrSummary2.Running = DevExpress.XtraReports.UI.SummaryRunning.Report
            Me.XrLabel_total_gross.Summary = XrSummary2
            Me.XrLabel_total_gross.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel_total_deducted
            '
            Me.XrLabel_total_deducted.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.XrLabel_total_deducted.ForeColor = System.Drawing.SystemColors.ControlText
            Me.XrLabel_total_deducted.Location = New System.Drawing.Point(442, 17)
            Me.XrLabel_total_deducted.Name = "XrLabel_total_deducted"
            Me.XrLabel_total_deducted.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_total_deducted.Size = New System.Drawing.Size(100, 15)
            Me.XrLabel_total_deducted.StylePriority.UseForeColor = False
            Me.XrLabel_total_deducted.StylePriority.UseTextAlignment = False
            XrSummary3.FormatString = "{0:c}"
            XrSummary3.Running = DevExpress.XtraReports.UI.SummaryRunning.Report
            Me.XrLabel_total_deducted.Summary = XrSummary3
            Me.XrLabel_total_deducted.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel_total_billed
            '
            Me.XrLabel_total_billed.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.XrLabel_total_billed.ForeColor = System.Drawing.SystemColors.ControlText
            Me.XrLabel_total_billed.Location = New System.Drawing.Point(542, 17)
            Me.XrLabel_total_billed.Name = "XrLabel_total_billed"
            Me.XrLabel_total_billed.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_total_billed.Size = New System.Drawing.Size(100, 15)
            Me.XrLabel_total_billed.StylePriority.UseForeColor = False
            Me.XrLabel_total_billed.StylePriority.UseTextAlignment = False
            XrSummary4.FormatString = "{0:c}"
            XrSummary4.Running = DevExpress.XtraReports.UI.SummaryRunning.Report
            Me.XrLabel_total_billed.Summary = XrSummary4
            Me.XrLabel_total_billed.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel15
            '
            Me.XrLabel15.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.XrLabel15.ForeColor = System.Drawing.SystemColors.ControlText
            Me.XrLabel15.Location = New System.Drawing.Point(1, 17)
            Me.XrLabel15.Name = "XrLabel15"
            Me.XrLabel15.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel15.Size = New System.Drawing.Size(341, 15)
            Me.XrLabel15.StylePriority.UseForeColor = False
            Me.XrLabel15.StylePriority.UseTextAlignment = False
            Me.XrLabel15.Text = "TOTALS"
            Me.XrLabel15.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            '
            'XrLine1
            '
            Me.XrLine1.Location = New System.Drawing.Point(0, 8)
            Me.XrLine1.Name = "XrLine1"
            Me.XrLine1.Size = New System.Drawing.Size(742, 8)
            '
            'GroupHeader1
            '
            Me.GroupHeader1.Height = 0
            Me.GroupHeader1.Name = "GroupHeader1"
            '
            'GroupFooter1
            '
            Me.GroupFooter1.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel_creditor, Me.XrLabel_billed, Me.XrLabel_deducted, Me.XrLabel_gross, Me.XrLabel_net})
            Me.GroupFooter1.GroupUnion = DevExpress.XtraReports.UI.GroupFooterUnion.WithLastDetail
            Me.GroupFooter1.Height = 15
            Me.GroupFooter1.Name = "GroupFooter1"
            '
            'XrLabel_creditor
            '
            Me.XrLabel_creditor.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.XrLabel_creditor.ForeColor = System.Drawing.SystemColors.ControlText
            Me.XrLabel_creditor.Location = New System.Drawing.Point(0, 0)
            Me.XrLabel_creditor.Name = "XrLabel_creditor"
            Me.XrLabel_creditor.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_creditor.Size = New System.Drawing.Size(341, 15)
            Me.XrLabel_creditor.StylePriority.UseFont = False
            Me.XrLabel_creditor.StylePriority.UseForeColor = False
            Me.XrLabel_creditor.StylePriority.UseTextAlignment = False
            Me.XrLabel_creditor.Text = "[[creditor]] [creditor_name]"
            Me.XrLabel_creditor.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            Me.XrLabel_creditor.WordWrap = False
            '
            'XrLabel_billed
            '
            Me.XrLabel_billed.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.XrLabel_billed.ForeColor = System.Drawing.SystemColors.ControlText
            Me.XrLabel_billed.Location = New System.Drawing.Point(541, 0)
            Me.XrLabel_billed.Name = "XrLabel_billed"
            Me.XrLabel_billed.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_billed.Size = New System.Drawing.Size(100, 15)
            Me.XrLabel_billed.StylePriority.UseFont = False
            Me.XrLabel_billed.StylePriority.UseForeColor = False
            Me.XrLabel_billed.StylePriority.UseTextAlignment = False
            XrSummary5.FormatString = "{0:c}"
            XrSummary5.Running = DevExpress.XtraReports.UI.SummaryRunning.Group
            Me.XrLabel_billed.Summary = XrSummary5
            Me.XrLabel_billed.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel_deducted
            '
            Me.XrLabel_deducted.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.XrLabel_deducted.ForeColor = System.Drawing.SystemColors.ControlText
            Me.XrLabel_deducted.Location = New System.Drawing.Point(441, 0)
            Me.XrLabel_deducted.Name = "XrLabel_deducted"
            Me.XrLabel_deducted.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_deducted.Size = New System.Drawing.Size(100, 15)
            Me.XrLabel_deducted.StylePriority.UseFont = False
            Me.XrLabel_deducted.StylePriority.UseForeColor = False
            Me.XrLabel_deducted.StylePriority.UseTextAlignment = False
            XrSummary6.FormatString = "{0:c}"
            XrSummary6.Running = DevExpress.XtraReports.UI.SummaryRunning.Group
            Me.XrLabel_deducted.Summary = XrSummary6
            Me.XrLabel_deducted.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel_gross
            '
            Me.XrLabel_gross.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.XrLabel_gross.ForeColor = System.Drawing.SystemColors.ControlText
            Me.XrLabel_gross.Location = New System.Drawing.Point(341, 0)
            Me.XrLabel_gross.Name = "XrLabel_gross"
            Me.XrLabel_gross.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_gross.Size = New System.Drawing.Size(100, 15)
            Me.XrLabel_gross.StylePriority.UseFont = False
            Me.XrLabel_gross.StylePriority.UseForeColor = False
            Me.XrLabel_gross.StylePriority.UseTextAlignment = False
            XrSummary7.FormatString = "{0:c}"
            XrSummary7.Running = DevExpress.XtraReports.UI.SummaryRunning.Group
            Me.XrLabel_gross.Summary = XrSummary7
            Me.XrLabel_gross.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel_net
            '
            Me.XrLabel_net.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.XrLabel_net.ForeColor = System.Drawing.SystemColors.ControlText
            Me.XrLabel_net.Location = New System.Drawing.Point(641, 0)
            Me.XrLabel_net.Name = "XrLabel_net"
            Me.XrLabel_net.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_net.Size = New System.Drawing.Size(100, 15)
            Me.XrLabel_net.StylePriority.UseFont = False
            Me.XrLabel_net.StylePriority.UseForeColor = False
            Me.XrLabel_net.StylePriority.UseTextAlignment = False
            XrSummary8.FormatString = "{0:c}"
            XrSummary8.Running = DevExpress.XtraReports.UI.SummaryRunning.Group
            Me.XrLabel_net.Summary = XrSummary8
            Me.XrLabel_net.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'DroppedClientsReportClass
            '
            Me.Bands.AddRange(New DevExpress.XtraReports.UI.Band() {Me.Detail, Me.PageHeader, Me.PageFooter, Me.ReportFooter, Me.GroupHeader1, Me.GroupFooter1})
            Me.Version = "9.2"
            Me.Controls.SetChildIndex(Me.GroupFooter1, 0)
            Me.Controls.SetChildIndex(Me.GroupHeader1, 0)
            Me.Controls.SetChildIndex(Me.ReportFooter, 0)
            Me.Controls.SetChildIndex(Me.PageFooter, 0)
            Me.Controls.SetChildIndex(Me.PageHeader, 0)
            Me.Controls.SetChildIndex(Me.Detail, 0)
            CType(Me, System.ComponentModel.ISupportInitialize).EndInit()
        End Sub

#End Region

        Dim ds As New DataSet("ds")
        Protected Overridable Sub Report_BeforePrint(ByVal sender As Object, ByVal e As PrintEventArgs)

            Dim cn As New System.Data.SqlClient.SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
            Dim tbl As DataTable = ds.Tables("rpt_disbursement_detail")
            If tbl Is Nothing Then
                Try
                    cn.Open()
                    Using cmd As New System.Data.SqlClient.SqlCommand()
                        With cmd
                            .Connection = cn
                            .CommandType = CommandType.StoredProcedure
                            .CommandText = "rpt_disbursement_detail"
                            System.Data.SqlClient.SqlCommandBuilder.DeriveParameters(cmd)

                            .CommandTimeout = 0
                            .Parameters(1).Value = Parameter_FromDate
                            .Parameters(2).Value = Parameter_ToDate
                        End With

                        Using da As New SqlDataAdapter(cmd)
                            da.Fill(ds, "rpt_disbursement_detail")
                            tbl = ds.Tables("rpt_disbursement_detail")
                        End Using
                    End Using

                Catch ex As System.Data.SqlClient.SqlException
                    Using gdr As New Repository.GetDataResult()
                        gdr.HandleException(ex)
                        DebtPlus.UI.Common.ErrorHandling.HandleErrors(gdr, "Error reading information for report")
                    End Using

                Finally
                    cn.Dispose()
                End Try
            End If

            If tbl IsNot Nothing Then
                If Not tbl.Columns.Contains("net") Then
                    tbl.Columns.Add("net", GetType(Decimal), "[gross] - [deducted]")
                End If

                ' Create the default view for the summary table
                DataSource = New DataView(tbl, String.Empty, "creditor", DataViewRowState.CurrentRows)
            End If
        End Sub

        Protected Overridable Sub Report_PreviewClick(ByVal sender As Object, ByVal e As PreviewMouseEventArgs)
            Dim tbl As DataTable = CType(DataSource, DataView).Table
            Dim creditor As Object = e.Brick.Value

            If creditor IsNot Nothing Then
                Dim creditor_name As String = String.Empty
                Dim NameRow() As DataRow = tbl.Select(String.Format("[creditor]='{0}'", creditor), String.Empty)
                If NameRow.GetUpperBound(0) >= 0 Then
                    creditor_name = DebtPlus.Utils.Nulls.DStr(NameRow(0)("creditor_name"))
                End If

                Dim cls As New SubReportThread(New DefaultLookAndFeel().LookAndFeel, tbl, DebtPlus.Utils.Nulls.DStr(creditor), String.Format("{0} for creditor [{1}] {2}", ReportSubtitle, creditor, creditor_name))

                Dim thrd As New Thread(AddressOf cls.ShowReport)
                thrd.IsBackground = True
                thrd.SetApartmentState(ApartmentState.STA)
                thrd.Name = "Subreport"
                thrd.Start()
            End If
        End Sub

        Protected Class SubReportThread
            Private tbl As DataTable
            Private Creditor As String
            Private Subtitle As String
            Private LookAndFeel As UserLookAndFeel

            Public Sub New(ByVal LookAndFeel As UserLookAndFeel, ByVal tbl As DataTable, ByVal creditor As String, ByVal subtitle As String)
                Me.tbl = tbl
                Me.Subtitle = subtitle
                Me.Creditor = creditor
            End Sub

            Public Sub ShowReport()
                Using vue As New DataView(tbl, String.Format("[creditor]='{0}'", Creditor), "check_date, client", DataViewRowState.CurrentRows)
                    Using rpt As New DisbursementSummaryDetailSubreport(vue, Subtitle)
                        rpt.CreateDocument()
                        rpt.DisplayPreviewDialog(LookAndFeel)
                    End Using
                End Using
            End Sub
        End Class
    End Class
End Namespace