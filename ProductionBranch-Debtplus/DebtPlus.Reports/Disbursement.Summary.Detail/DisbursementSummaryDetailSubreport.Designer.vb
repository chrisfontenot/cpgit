Namespace Disbursement.SummaryDetail
    <Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
    Partial Class DisbursementSummaryDetailSubreport

        'XtraReport overrides dispose to clean up the component list.
        <System.Diagnostics.DebuggerNonUserCode()> _
        Protected Overrides Sub Dispose(ByVal disposing As Boolean)
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
            MyBase.Dispose(disposing)
        End Sub

        'Required by the Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Designer
        'It can be modified using the Designer.  
        'Do not modify it using the code editor.
        <System.Diagnostics.DebuggerStepThrough()> _
        Private Sub InitializeComponent()
            Dim XrSummary1 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary
            Dim XrSummary2 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary
            Dim XrSummary3 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary
            Dim XrSummary4 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary
            Dim XrSummary5 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary
            Dim XrSummary6 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary
            Dim XrSummary7 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary
            Dim XrSummary8 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary
            Me.XrPanel1 = New DevExpress.XtraReports.UI.XRPanel
            Me.XrLabel9 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel4 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel2 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel6 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel3 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel1 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel7 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_account_number = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_net = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_billed = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_deducted = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_gross = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_creditor = New DevExpress.XtraReports.UI.XRLabel
            Me.ReportFooter = New DevExpress.XtraReports.UI.ReportFooterBand
            Me.XrLine1 = New DevExpress.XtraReports.UI.XRLine
            Me.XrLabel_total_billed = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_total_deducted = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_total_gross = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel15 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_total_net = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_check_date = New DevExpress.XtraReports.UI.XRLabel
            Me.GroupHeader1 = New DevExpress.XtraReports.UI.GroupHeaderBand
            Me.GroupFooter1 = New DevExpress.XtraReports.UI.GroupFooterBand
            Me.XrLabel_group_gross = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_group_billed = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_group_deducted = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_group_net = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel8 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_checknum = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel5 = New DevExpress.XtraReports.UI.XRLabel
            CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
            '
            'Detail
            '
            Me.Detail.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel_checknum, Me.XrLabel_account_number, Me.XrLabel_net, Me.XrLabel_billed, Me.XrLabel_deducted, Me.XrLabel_gross, Me.XrLabel_creditor, Me.XrLabel_check_date})
            Me.Detail.HeightF = 15.0!
            '
            'PageHeader
            '
            Me.PageHeader.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrPanel1})
            Me.PageHeader.HeightF = 158.0!
            Me.PageHeader.Controls.SetChildIndex(Me.XrPanel1, 0)
            Me.PageHeader.Controls.SetChildIndex(Me.XrPanel_AgencyAddress, 0)
            Me.PageHeader.Controls.SetChildIndex(Me.XrLabel_Title, 0)
            Me.PageHeader.Controls.SetChildIndex(Me.XrLine_Title, 0)
            Me.PageHeader.Controls.SetChildIndex(Me.XrLabel_Subtitle, 0)
            Me.PageHeader.Controls.SetChildIndex(Me.XrPageInfo1, 0)
            '
            'XrLabel_Title
            '
            Me.XrLabel_Title.SizeF = New System.Drawing.SizeF(692.0!, 42.0!)
            Me.XrLabel_Title.StylePriority.UseFont = False
            '
            'XrLabel_Subtitle
            '
            Me.XrLabel_Subtitle.LocationFloat = New DevExpress.Utils.PointFloat(8.0!, 92.0!)
            '
            'XrPageInfo_PageNumber
            '
            Me.XrPageInfo_PageNumber.LocationFloat = New DevExpress.Utils.PointFloat(917.0!, 8.0!)
            Me.XrPageInfo_PageNumber.StylePriority.UseTextAlignment = False
            '
            'XrPanel_AgencyAddress
            '
            Me.XrPanel_AgencyAddress.LocationFloat = New DevExpress.Utils.PointFloat(742.0!, 0.0!)
            '
            'XRLabel_Agency_Name
            '
            Me.XRLabel_Agency_Name.StylePriority.UseTextAlignment = False
            '
            'XrLine_Title
            '
            Me.XrLine_Title.SizeF = New System.Drawing.SizeF(692.0!, 8.0!)
            '
            'XrLabel_Agency_Address3
            '
            Me.XrLabel_Agency_Address3.StylePriority.UseFont = False
            Me.XrLabel_Agency_Address3.StylePriority.UseTextAlignment = False
            '
            'XrLabel_Agency_Address1
            '
            Me.XrLabel_Agency_Address1.StylePriority.UseFont = False
            Me.XrLabel_Agency_Address1.StylePriority.UseTextAlignment = False
            '
            'XrLabel_Agency_Phone
            '
            Me.XrLabel_Agency_Phone.StylePriority.UseFont = False
            Me.XrLabel_Agency_Phone.StylePriority.UseTextAlignment = False
            '
            'XrLabel_Agency_Address2
            '
            Me.XrLabel_Agency_Address2.StylePriority.UseFont = False
            Me.XrLabel_Agency_Address2.StylePriority.UseTextAlignment = False
            '
            'XrPanel1
            '
            Me.XrPanel1.BackColor = System.Drawing.Color.Teal
            Me.XrPanel1.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel5, Me.XrLabel9, Me.XrLabel4, Me.XrLabel2, Me.XrLabel6, Me.XrLabel3, Me.XrLabel1, Me.XrLabel7})
            Me.XrPanel1.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 133.0!)
            Me.XrPanel1.Name = "XrPanel1"
            Me.XrPanel1.SizeF = New System.Drawing.SizeF(1042.0!, 17.0!)
            '
            'XrLabel9
            '
            Me.XrLabel9.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.XrLabel9.ForeColor = System.Drawing.Color.White
            Me.XrLabel9.LocationFloat = New DevExpress.Utils.PointFloat(85.4166!, 0.0!)
            Me.XrLabel9.Name = "XrLabel9"
            Me.XrLabel9.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel9.SizeF = New System.Drawing.SizeF(122.9167!, 14.99998!)
            Me.XrLabel9.StylePriority.UseTextAlignment = False
            Me.XrLabel9.Text = "CHECK NUMBER"
            Me.XrLabel9.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel4
            '
            Me.XrLabel4.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.XrLabel4.ForeColor = System.Drawing.Color.White
            Me.XrLabel4.LocationFloat = New DevExpress.Utils.PointFloat(583.0!, 0.0!)
            Me.XrLabel4.Name = "XrLabel4"
            Me.XrLabel4.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel4.SizeF = New System.Drawing.SizeF(118.0!, 15.0!)
            Me.XrLabel4.StylePriority.UseTextAlignment = False
            Me.XrLabel4.Text = "ACCT"
            Me.XrLabel4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            '
            'XrLabel2
            '
            Me.XrLabel2.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.XrLabel2.ForeColor = System.Drawing.Color.White
            Me.XrLabel2.LocationFloat = New DevExpress.Utils.PointFloat(733.0!, 0.0!)
            Me.XrLabel2.Name = "XrLabel2"
            Me.XrLabel2.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel2.SizeF = New System.Drawing.SizeF(75.0!, 15.0!)
            Me.XrLabel2.StylePriority.UseTextAlignment = False
            Me.XrLabel2.Text = "GROSS"
            Me.XrLabel2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel6
            '
            Me.XrLabel6.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.XrLabel6.ForeColor = System.Drawing.Color.White
            Me.XrLabel6.LocationFloat = New DevExpress.Utils.PointFloat(808.0!, 0.0!)
            Me.XrLabel6.Name = "XrLabel6"
            Me.XrLabel6.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel6.SizeF = New System.Drawing.SizeF(75.0!, 15.0!)
            Me.XrLabel6.StylePriority.UseTextAlignment = False
            Me.XrLabel6.Text = "DEDUCT"
            Me.XrLabel6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel3
            '
            Me.XrLabel3.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.XrLabel3.ForeColor = System.Drawing.Color.White
            Me.XrLabel3.LocationFloat = New DevExpress.Utils.PointFloat(883.0!, 0.0!)
            Me.XrLabel3.Name = "XrLabel3"
            Me.XrLabel3.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel3.SizeF = New System.Drawing.SizeF(75.0!, 15.0!)
            Me.XrLabel3.StylePriority.UseTextAlignment = False
            Me.XrLabel3.Text = "BILLED"
            Me.XrLabel3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel1
            '
            Me.XrLabel1.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.XrLabel1.ForeColor = System.Drawing.Color.White
            Me.XrLabel1.LocationFloat = New DevExpress.Utils.PointFloat(958.0!, 0.0!)
            Me.XrLabel1.Name = "XrLabel1"
            Me.XrLabel1.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel1.SizeF = New System.Drawing.SizeF(75.0!, 15.0!)
            Me.XrLabel1.StylePriority.UseTextAlignment = False
            Me.XrLabel1.Text = "NET"
            Me.XrLabel1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel7
            '
            Me.XrLabel7.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.XrLabel7.ForeColor = System.Drawing.Color.White
            Me.XrLabel7.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
            Me.XrLabel7.Name = "XrLabel7"
            Me.XrLabel7.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel7.SizeF = New System.Drawing.SizeF(84.375!, 14.99998!)
            Me.XrLabel7.StylePriority.UseTextAlignment = False
            Me.XrLabel7.Text = "DATE"
            Me.XrLabel7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel_account_number
            '
            Me.XrLabel_account_number.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.XrLabel_account_number.ForeColor = System.Drawing.SystemColors.ControlText
            Me.XrLabel_account_number.LocationFloat = New DevExpress.Utils.PointFloat(583.0!, 0.0!)
            Me.XrLabel_account_number.Name = "XrLabel_account_number"
            Me.XrLabel_account_number.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_account_number.SizeF = New System.Drawing.SizeF(150.0!, 15.0!)
            Me.XrLabel_account_number.StylePriority.UseFont = False
            Me.XrLabel_account_number.StylePriority.UseForeColor = False
            Me.XrLabel_account_number.StylePriority.UseTextAlignment = False
            Me.XrLabel_account_number.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            '
            'XrLabel_net
            '
            Me.XrLabel_net.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.XrLabel_net.ForeColor = System.Drawing.SystemColors.ControlText
            Me.XrLabel_net.LocationFloat = New DevExpress.Utils.PointFloat(958.0!, 0.0!)
            Me.XrLabel_net.Name = "XrLabel_net"
            Me.XrLabel_net.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_net.SizeF = New System.Drawing.SizeF(75.0!, 15.0!)
            Me.XrLabel_net.StylePriority.UseFont = False
            Me.XrLabel_net.StylePriority.UseForeColor = False
            Me.XrLabel_net.StylePriority.UseTextAlignment = False
            Me.XrLabel_net.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel_billed
            '
            Me.XrLabel_billed.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.XrLabel_billed.ForeColor = System.Drawing.SystemColors.ControlText
            Me.XrLabel_billed.LocationFloat = New DevExpress.Utils.PointFloat(883.0!, 0.0!)
            Me.XrLabel_billed.Name = "XrLabel_billed"
            Me.XrLabel_billed.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_billed.SizeF = New System.Drawing.SizeF(75.0!, 15.0!)
            Me.XrLabel_billed.StylePriority.UseFont = False
            Me.XrLabel_billed.StylePriority.UseForeColor = False
            Me.XrLabel_billed.StylePriority.UseTextAlignment = False
            Me.XrLabel_billed.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel_deducted
            '
            Me.XrLabel_deducted.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.XrLabel_deducted.ForeColor = System.Drawing.SystemColors.ControlText
            Me.XrLabel_deducted.LocationFloat = New DevExpress.Utils.PointFloat(808.0!, 0.0!)
            Me.XrLabel_deducted.Name = "XrLabel_deducted"
            Me.XrLabel_deducted.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_deducted.SizeF = New System.Drawing.SizeF(75.0!, 15.0!)
            Me.XrLabel_deducted.StylePriority.UseFont = False
            Me.XrLabel_deducted.StylePriority.UseForeColor = False
            Me.XrLabel_deducted.StylePriority.UseTextAlignment = False
            Me.XrLabel_deducted.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel_gross
            '
            Me.XrLabel_gross.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.XrLabel_gross.ForeColor = System.Drawing.SystemColors.ControlText
            Me.XrLabel_gross.LocationFloat = New DevExpress.Utils.PointFloat(733.0!, 0.0!)
            Me.XrLabel_gross.Name = "XrLabel_gross"
            Me.XrLabel_gross.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_gross.SizeF = New System.Drawing.SizeF(75.0!, 15.0!)
            Me.XrLabel_gross.StylePriority.UseFont = False
            Me.XrLabel_gross.StylePriority.UseForeColor = False
            Me.XrLabel_gross.StylePriority.UseTextAlignment = False
            Me.XrLabel_gross.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel_creditor
            '
            Me.XrLabel_creditor.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.XrLabel_creditor.ForeColor = System.Drawing.SystemColors.ControlText
            Me.XrLabel_creditor.LocationFloat = New DevExpress.Utils.PointFloat(220.8333!, 0.0!)
            Me.XrLabel_creditor.Name = "XrLabel_creditor"
            Me.XrLabel_creditor.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_creditor.ProcessDuplicates = DevExpress.XtraReports.UI.ValueSuppressType.Suppress
            Me.XrLabel_creditor.SizeF = New System.Drawing.SizeF(350.375!, 14.99999!)
            Me.XrLabel_creditor.StylePriority.UseFont = False
            Me.XrLabel_creditor.StylePriority.UseForeColor = False
            Me.XrLabel_creditor.StylePriority.UseTextAlignment = False
            Me.XrLabel_creditor.Text = "[client] [name]"
            Me.XrLabel_creditor.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            '
            'ReportFooter
            '
            Me.ReportFooter.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLine1, Me.XrLabel_total_billed, Me.XrLabel_total_deducted, Me.XrLabel_total_gross, Me.XrLabel15, Me.XrLabel_total_net})
            Me.ReportFooter.HeightF = 42.0!
            Me.ReportFooter.Name = "ReportFooter"
            '
            'XrLine1
            '
            Me.XrLine1.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
            Me.XrLine1.Name = "XrLine1"
            Me.XrLine1.SizeF = New System.Drawing.SizeF(1033.0!, 8.0!)
            '
            'XrLabel_total_billed
            '
            Me.XrLabel_total_billed.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.XrLabel_total_billed.ForeColor = System.Drawing.SystemColors.ControlText
            Me.XrLabel_total_billed.LocationFloat = New DevExpress.Utils.PointFloat(883.0!, 17.0!)
            Me.XrLabel_total_billed.Name = "XrLabel_total_billed"
            Me.XrLabel_total_billed.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_total_billed.SizeF = New System.Drawing.SizeF(75.0!, 15.0!)
            Me.XrLabel_total_billed.StylePriority.UseForeColor = False
            Me.XrLabel_total_billed.StylePriority.UseTextAlignment = False
            XrSummary1.FormatString = "{0:c}"
            XrSummary1.Running = DevExpress.XtraReports.UI.SummaryRunning.Report
            Me.XrLabel_total_billed.Summary = XrSummary1
            Me.XrLabel_total_billed.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel_total_deducted
            '
            Me.XrLabel_total_deducted.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.XrLabel_total_deducted.ForeColor = System.Drawing.SystemColors.ControlText
            Me.XrLabel_total_deducted.LocationFloat = New DevExpress.Utils.PointFloat(808.0!, 17.0!)
            Me.XrLabel_total_deducted.Name = "XrLabel_total_deducted"
            Me.XrLabel_total_deducted.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_total_deducted.SizeF = New System.Drawing.SizeF(75.0!, 15.0!)
            Me.XrLabel_total_deducted.StylePriority.UseForeColor = False
            Me.XrLabel_total_deducted.StylePriority.UseTextAlignment = False
            XrSummary2.FormatString = "{0:c}"
            XrSummary2.Running = DevExpress.XtraReports.UI.SummaryRunning.Report
            Me.XrLabel_total_deducted.Summary = XrSummary2
            Me.XrLabel_total_deducted.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel_total_gross
            '
            Me.XrLabel_total_gross.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.XrLabel_total_gross.ForeColor = System.Drawing.SystemColors.ControlText
            Me.XrLabel_total_gross.LocationFloat = New DevExpress.Utils.PointFloat(733.0!, 17.0!)
            Me.XrLabel_total_gross.Name = "XrLabel_total_gross"
            Me.XrLabel_total_gross.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_total_gross.SizeF = New System.Drawing.SizeF(75.0!, 15.0!)
            Me.XrLabel_total_gross.StylePriority.UseForeColor = False
            Me.XrLabel_total_gross.StylePriority.UseTextAlignment = False
            XrSummary3.FormatString = "{0:c}"
            XrSummary3.Running = DevExpress.XtraReports.UI.SummaryRunning.Report
            Me.XrLabel_total_gross.Summary = XrSummary3
            Me.XrLabel_total_gross.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel15
            '
            Me.XrLabel15.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.XrLabel15.ForeColor = System.Drawing.SystemColors.ControlText
            Me.XrLabel15.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 17.0!)
            Me.XrLabel15.Name = "XrLabel15"
            Me.XrLabel15.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel15.SizeF = New System.Drawing.SizeF(341.0!, 15.0!)
            Me.XrLabel15.StylePriority.UseForeColor = False
            Me.XrLabel15.StylePriority.UseTextAlignment = False
            Me.XrLabel15.Text = "TOTALS"
            Me.XrLabel15.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            '
            'XrLabel_total_net
            '
            Me.XrLabel_total_net.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.XrLabel_total_net.ForeColor = System.Drawing.SystemColors.ControlText
            Me.XrLabel_total_net.LocationFloat = New DevExpress.Utils.PointFloat(958.0!, 17.0!)
            Me.XrLabel_total_net.Name = "XrLabel_total_net"
            Me.XrLabel_total_net.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_total_net.SizeF = New System.Drawing.SizeF(75.0!, 15.0!)
            Me.XrLabel_total_net.StylePriority.UseForeColor = False
            Me.XrLabel_total_net.StylePriority.UseTextAlignment = False
            XrSummary4.FormatString = "{0:c}"
            XrSummary4.Running = DevExpress.XtraReports.UI.SummaryRunning.Report
            Me.XrLabel_total_net.Summary = XrSummary4
            Me.XrLabel_total_net.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel_check_date
            '
            Me.XrLabel_check_date.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.XrLabel_check_date.ForeColor = System.Drawing.SystemColors.ControlText
            Me.XrLabel_check_date.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
            Me.XrLabel_check_date.Name = "XrLabel_check_date"
            Me.XrLabel_check_date.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_check_date.SizeF = New System.Drawing.SizeF(84.375!, 15.0!)
            Me.XrLabel_check_date.StylePriority.UseFont = False
            Me.XrLabel_check_date.StylePriority.UseForeColor = False
            Me.XrLabel_check_date.StylePriority.UseTextAlignment = False
            Me.XrLabel_check_date.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'GroupHeader1
            '
            Me.GroupHeader1.HeightF = 0.0!
            Me.GroupHeader1.Name = "GroupHeader1"
            '
            'GroupFooter1
            '
            Me.GroupFooter1.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel_group_gross, Me.XrLabel_group_billed, Me.XrLabel_group_deducted, Me.XrLabel_group_net, Me.XrLabel8})
            Me.GroupFooter1.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.GroupFooter1.HeightF = 35.0!
            Me.GroupFooter1.Name = "GroupFooter1"
            Me.GroupFooter1.StylePriority.UseFont = False
            '
            'XrLabel_group_gross
            '
            Me.XrLabel_group_gross.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.XrLabel_group_gross.ForeColor = System.Drawing.SystemColors.ControlText
            Me.XrLabel_group_gross.LocationFloat = New DevExpress.Utils.PointFloat(733.0!, 8.0!)
            Me.XrLabel_group_gross.Name = "XrLabel_group_gross"
            Me.XrLabel_group_gross.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_group_gross.SizeF = New System.Drawing.SizeF(75.0!, 15.0!)
            Me.XrLabel_group_gross.StylePriority.UseFont = False
            Me.XrLabel_group_gross.StylePriority.UseForeColor = False
            Me.XrLabel_group_gross.StylePriority.UseTextAlignment = False
            XrSummary5.FormatString = "{0:c}"
            XrSummary5.Running = DevExpress.XtraReports.UI.SummaryRunning.Group
            Me.XrLabel_group_gross.Summary = XrSummary5
            Me.XrLabel_group_gross.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel_group_billed
            '
            Me.XrLabel_group_billed.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.XrLabel_group_billed.ForeColor = System.Drawing.SystemColors.ControlText
            Me.XrLabel_group_billed.LocationFloat = New DevExpress.Utils.PointFloat(883.0!, 8.0!)
            Me.XrLabel_group_billed.Name = "XrLabel_group_billed"
            Me.XrLabel_group_billed.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_group_billed.SizeF = New System.Drawing.SizeF(75.0!, 15.0!)
            Me.XrLabel_group_billed.StylePriority.UseFont = False
            Me.XrLabel_group_billed.StylePriority.UseForeColor = False
            Me.XrLabel_group_billed.StylePriority.UseTextAlignment = False
            XrSummary6.FormatString = "{0:c}"
            XrSummary6.Running = DevExpress.XtraReports.UI.SummaryRunning.Group
            Me.XrLabel_group_billed.Summary = XrSummary6
            Me.XrLabel_group_billed.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel_group_deducted
            '
            Me.XrLabel_group_deducted.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.XrLabel_group_deducted.ForeColor = System.Drawing.SystemColors.ControlText
            Me.XrLabel_group_deducted.LocationFloat = New DevExpress.Utils.PointFloat(808.0!, 8.0!)
            Me.XrLabel_group_deducted.Name = "XrLabel_group_deducted"
            Me.XrLabel_group_deducted.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_group_deducted.SizeF = New System.Drawing.SizeF(75.0!, 15.0!)
            Me.XrLabel_group_deducted.StylePriority.UseFont = False
            Me.XrLabel_group_deducted.StylePriority.UseForeColor = False
            Me.XrLabel_group_deducted.StylePriority.UseTextAlignment = False
            XrSummary7.FormatString = "{0:c}"
            XrSummary7.Running = DevExpress.XtraReports.UI.SummaryRunning.Group
            Me.XrLabel_group_deducted.Summary = XrSummary7
            Me.XrLabel_group_deducted.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel_group_net
            '
            Me.XrLabel_group_net.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.XrLabel_group_net.ForeColor = System.Drawing.SystemColors.ControlText
            Me.XrLabel_group_net.LocationFloat = New DevExpress.Utils.PointFloat(958.0!, 8.0!)
            Me.XrLabel_group_net.Name = "XrLabel_group_net"
            Me.XrLabel_group_net.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_group_net.SizeF = New System.Drawing.SizeF(75.0!, 15.0!)
            Me.XrLabel_group_net.StylePriority.UseFont = False
            Me.XrLabel_group_net.StylePriority.UseForeColor = False
            Me.XrLabel_group_net.StylePriority.UseTextAlignment = False
            XrSummary8.FormatString = "{0:c}"
            XrSummary8.Running = DevExpress.XtraReports.UI.SummaryRunning.Group
            Me.XrLabel_group_net.Summary = XrSummary8
            Me.XrLabel_group_net.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel8
            '
            Me.XrLabel8.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.XrLabel8.ForeColor = System.Drawing.SystemColors.ControlText
            Me.XrLabel8.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 8.0!)
            Me.XrLabel8.Name = "XrLabel8"
            Me.XrLabel8.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel8.SizeF = New System.Drawing.SizeF(341.0!, 15.0!)
            Me.XrLabel8.StylePriority.UseForeColor = False
            Me.XrLabel8.StylePriority.UseTextAlignment = False
            Me.XrLabel8.Text = "SUB-TOTALS"
            Me.XrLabel8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            '
            'XrLabel_checknum
            '
            Me.XrLabel_checknum.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.XrLabel_checknum.ForeColor = System.Drawing.SystemColors.ControlText
            Me.XrLabel_checknum.LocationFloat = New DevExpress.Utils.PointFloat(84.375!, 0.0!)
            Me.XrLabel_checknum.Name = "XrLabel_checknum"
            Me.XrLabel_checknum.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_checknum.SizeF = New System.Drawing.SizeF(123.9583!, 15.0!)
            Me.XrLabel_checknum.StylePriority.UseFont = False
            Me.XrLabel_checknum.StylePriority.UseForeColor = False
            Me.XrLabel_checknum.StylePriority.UseTextAlignment = False
            Me.XrLabel_checknum.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel5
            '
            Me.XrLabel5.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.XrLabel5.ForeColor = System.Drawing.Color.White
            Me.XrLabel5.LocationFloat = New DevExpress.Utils.PointFloat(220.8333!, 0.0!)
            Me.XrLabel5.Name = "XrLabel5"
            Me.XrLabel5.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel5.SizeF = New System.Drawing.SizeF(146.5417!, 14.99998!)
            Me.XrLabel5.StylePriority.UseTextAlignment = False
            Me.XrLabel5.Text = "CLIENT AND NAME"
            Me.XrLabel5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            '
            'DisbursementSummaryDetailSubreport
            '
            Me.Bands.AddRange(New DevExpress.XtraReports.UI.Band() {Me.Detail, Me.PageHeader, Me.PageFooter, Me.ReportFooter, Me.GroupHeader1, Me.GroupFooter1, Me.TopMarginBand1, Me.BottomMarginBand1})
            Me.Landscape = True
            Me.PageHeight = 850
            Me.PageWidth = 1100
            Me.StyleSheet.AddRange(New DevExpress.XtraReports.UI.XRControlStyle() {Me.XrControlStyle_Header, Me.XrControlStyle_Totals, Me.XrControlStyle_HeaderPannel, Me.XrControlStyle_GroupHeader})
            Me.Version = "10.1"
            Me.Controls.SetChildIndex(Me.BottomMarginBand1, 0)
            Me.Controls.SetChildIndex(Me.TopMarginBand1, 0)
            Me.Controls.SetChildIndex(Me.GroupFooter1, 0)
            Me.Controls.SetChildIndex(Me.GroupHeader1, 0)
            Me.Controls.SetChildIndex(Me.ReportFooter, 0)
            Me.Controls.SetChildIndex(Me.PageFooter, 0)
            Me.Controls.SetChildIndex(Me.PageHeader, 0)
            Me.Controls.SetChildIndex(Me.Detail, 0)
            CType(Me, System.ComponentModel.ISupportInitialize).EndInit()

        End Sub
        Friend WithEvents XrPanel1 As DevExpress.XtraReports.UI.XRPanel
        Friend WithEvents XrLabel4 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel2 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel6 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel3 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel1 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_account_number As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_net As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_billed As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_deducted As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_gross As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_creditor As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents ReportFooter As DevExpress.XtraReports.UI.ReportFooterBand
        Friend WithEvents XrLine1 As DevExpress.XtraReports.UI.XRLine
        Friend WithEvents XrLabel_total_billed As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_total_deducted As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_total_gross As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel15 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_total_net As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_check_date As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel7 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents GroupHeader1 As DevExpress.XtraReports.UI.GroupHeaderBand
        Friend WithEvents GroupFooter1 As DevExpress.XtraReports.UI.GroupFooterBand
        Friend WithEvents XrLabel_group_gross As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_group_billed As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_group_deducted As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_group_net As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel8 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_checknum As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel9 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel5 As DevExpress.XtraReports.UI.XRLabel
    End Class
End Namespace
