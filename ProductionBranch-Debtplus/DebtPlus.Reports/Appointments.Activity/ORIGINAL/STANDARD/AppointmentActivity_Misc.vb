#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}
#End Region

Option Compare Binary
Option Explicit On 
Option Strict On

Imports DevExpress.XtraReports.UI
Namespace Appointments.Activity
    Public Class AppointmentActivity_Misc
        Inherits DevExpress.XtraReports.UI.XtraReport

        Public Sub New(ByVal Container As System.ComponentModel.IContainer)
            MyClass.New()
            Container.Add(Me)
        End Sub

        Public Sub New()
            MyBase.New()
            InitializeComponent()
        End Sub

#Region " Component Designer generated code "

        'Component overrides dispose to clean up the component list.
        Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
            If disposing Then
                If components IsNot Nothing Then
                    components.Dispose()
                End If
            End If
            MyBase.Dispose(disposing)
        End Sub

        'Required by the Component Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Component Designer
        'It can be modified using the Component Designer.
        'Do not modify it using the code editor.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
            Me.Detail = New DevExpress.XtraReports.UI.DetailBand
            Me.XrLabel_YTD = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_QTD = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_MTD = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_Description = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_HeaderLabel = New DevExpress.XtraReports.UI.XRLabel
            Me.ReportHeader = New DevExpress.XtraReports.UI.ReportHeaderBand
            Me.PageHeader = New DevExpress.XtraReports.UI.PageHeaderBand
            Me.XrLabel4 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel3 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel2 = New DevExpress.XtraReports.UI.XRLabel
            Me.TopMarginBand1 = New DevExpress.XtraReports.UI.TopMarginBand
            Me.BottomMarginBand1 = New DevExpress.XtraReports.UI.BottomMarginBand
            CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
            '
            'Detail
            '
            Me.Detail.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel_YTD, Me.XrLabel_QTD, Me.XrLabel_MTD, Me.XrLabel_Description})
            Me.Detail.HeightF = 15.0!
            Me.Detail.Name = "Detail"
            Me.Detail.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
            Me.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
            '
            'XrLabel_YTD
            '
            Me.XrLabel_YTD.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "YTDCount", "{0:n0}")})
            Me.XrLabel_YTD.Font = New System.Drawing.Font("Times New Roman", 9.75!)
            Me.XrLabel_YTD.LocationFloat = New DevExpress.Utils.PointFloat(497.8334!, 0.0!)
            Me.XrLabel_YTD.Name = "XrLabel_YTD"
            Me.XrLabel_YTD.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_YTD.SizeF = New System.Drawing.SizeF(101.0!, 15.0!)
            Me.XrLabel_YTD.Text = "0"
            Me.XrLabel_YTD.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
            '
            'XrLabel_QTD
            '
            Me.XrLabel_QTD.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "QTDCount", "{0:n0}")})
            Me.XrLabel_QTD.Font = New System.Drawing.Font("Times New Roman", 9.75!)
            Me.XrLabel_QTD.LocationFloat = New DevExpress.Utils.PointFloat(396.8333!, 0.0!)
            Me.XrLabel_QTD.Name = "XrLabel_QTD"
            Me.XrLabel_QTD.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_QTD.SizeF = New System.Drawing.SizeF(101.0!, 15.0!)
            Me.XrLabel_QTD.Text = "0"
            Me.XrLabel_QTD.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
            '
            'XrLabel_MTD
            '
            Me.XrLabel_MTD.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "MTDCount", "{0:n0}")})
            Me.XrLabel_MTD.Font = New System.Drawing.Font("Times New Roman", 9.75!)
            Me.XrLabel_MTD.LocationFloat = New DevExpress.Utils.PointFloat(295.8333!, 0.0!)
            Me.XrLabel_MTD.Name = "XrLabel_MTD"
            Me.XrLabel_MTD.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_MTD.SizeF = New System.Drawing.SizeF(101.0!, 15.0!)
            Me.XrLabel_MTD.Text = "0"
            Me.XrLabel_MTD.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
            '
            'XrLabel_Description
            '
            Me.XrLabel_Description.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "Description")})
            Me.XrLabel_Description.Font = New System.Drawing.Font("Times New Roman", 9.75!)
            Me.XrLabel_Description.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
            Me.XrLabel_Description.Name = "XrLabel_Description"
            Me.XrLabel_Description.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_Description.SizeF = New System.Drawing.SizeF(295.8333!, 14.99999!)
            Me.XrLabel_Description.Text = "Status Code"
            Me.XrLabel_Description.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
            '
            'XrLabel_HeaderLabel
            '
            Me.XrLabel_HeaderLabel.CanGrow = False
            Me.XrLabel_HeaderLabel.Font = New System.Drawing.Font("Times New Roman", 16.0!, System.Drawing.FontStyle.Bold)
            Me.XrLabel_HeaderLabel.ForeColor = System.Drawing.Color.Teal
            Me.XrLabel_HeaderLabel.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 7.999992!)
            Me.XrLabel_HeaderLabel.Name = "XrLabel_HeaderLabel"
            Me.XrLabel_HeaderLabel.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_HeaderLabel.SizeF = New System.Drawing.SizeF(598.8334!, 33.0!)
            Me.XrLabel_HeaderLabel.Text = "2. Miscellaneous Appointment Conversion Information"
            Me.XrLabel_HeaderLabel.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
            '
            'ReportHeader
            '
            Me.ReportHeader.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel_HeaderLabel})
            Me.ReportHeader.HeightF = 44.0!
            Me.ReportHeader.Name = "ReportHeader"
            Me.ReportHeader.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
            Me.ReportHeader.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
            '
            'PageHeader
            '
            Me.PageHeader.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel4, Me.XrLabel3, Me.XrLabel2})
            Me.PageHeader.HeightF = 15.0!
            Me.PageHeader.Name = "PageHeader"
            Me.PageHeader.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
            Me.PageHeader.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
            '
            'XrLabel4
            '
            Me.XrLabel4.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel4.LocationFloat = New DevExpress.Utils.PointFloat(497.8334!, 0.0!)
            Me.XrLabel4.Name = "XrLabel4"
            Me.XrLabel4.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel4.SizeF = New System.Drawing.SizeF(101.0!, 15.0!)
            Me.XrLabel4.Text = "YTD"
            Me.XrLabel4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
            '
            'XrLabel3
            '
            Me.XrLabel3.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel3.LocationFloat = New DevExpress.Utils.PointFloat(396.8333!, 0.0!)
            Me.XrLabel3.Name = "XrLabel3"
            Me.XrLabel3.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel3.SizeF = New System.Drawing.SizeF(101.0!, 15.0!)
            Me.XrLabel3.Text = "QTD"
            Me.XrLabel3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
            '
            'XrLabel2
            '
            Me.XrLabel2.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel2.LocationFloat = New DevExpress.Utils.PointFloat(295.8333!, 0.0!)
            Me.XrLabel2.Name = "XrLabel2"
            Me.XrLabel2.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel2.SizeF = New System.Drawing.SizeF(101.0!, 15.0!)
            Me.XrLabel2.Text = "MTD"
            Me.XrLabel2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
            '
            'TopMarginBand1
            '
            Me.TopMarginBand1.HeightF = 0.0!
            Me.TopMarginBand1.Name = "TopMarginBand1"
            '
            'BottomMarginBand1
            '
            Me.BottomMarginBand1.HeightF = 0.0!
            Me.BottomMarginBand1.Name = "BottomMarginBand1"
            '
            'AppointmentActivity_Misc
            '
            Me.Bands.AddRange(New DevExpress.XtraReports.UI.Band() {Me.Detail, Me.ReportHeader, Me.PageHeader, Me.TopMarginBand1, Me.BottomMarginBand1})
            Me.Margins = New System.Drawing.Printing.Margins(100, 100, 0, 0)
            Me.Version = "10.1"
            CType(Me, System.ComponentModel.ISupportInitialize).EndInit()

        End Sub
        Protected Friend WithEvents Detail As DevExpress.XtraReports.UI.DetailBand
        Protected Friend WithEvents XrLabel_HeaderLabel As DevExpress.XtraReports.UI.XRLabel
        Protected Friend WithEvents ReportHeader As DevExpress.XtraReports.UI.ReportHeaderBand
        Protected Friend WithEvents PageHeader As DevExpress.XtraReports.UI.PageHeaderBand
        Protected Friend WithEvents XrLabel2 As DevExpress.XtraReports.UI.XRLabel
        Protected Friend WithEvents XrLabel3 As DevExpress.XtraReports.UI.XRLabel
        Protected Friend WithEvents XrLabel4 As DevExpress.XtraReports.UI.XRLabel
        Protected Friend WithEvents XrLabel_MTD As DevExpress.XtraReports.UI.XRLabel
        Protected Friend WithEvents XrLabel_QTD As DevExpress.XtraReports.UI.XRLabel
        Protected Friend WithEvents XrLabel_YTD As DevExpress.XtraReports.UI.XRLabel
        Protected Friend WithEvents TopMarginBand1 As DevExpress.XtraReports.UI.TopMarginBand
        Protected Friend WithEvents BottomMarginBand1 As DevExpress.XtraReports.UI.BottomMarginBand
        Protected Friend WithEvents XrLabel_Description As DevExpress.XtraReports.UI.XRLabel
#End Region

    End Class
End Namespace
