#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}
#End Region

Option Compare Binary
Option Explicit On 
Option Strict On
Imports DevExpress.XtraReports.UI

Namespace Appointments.Activity
    Public Class AppointmentActivity_ByStatus
        Inherits AppointmentActivity_Misc

        Public Sub New(ByVal Container As System.ComponentModel.IContainer)
            MyClass.New()
            Container.Add(Me)
        End Sub

        Public Sub New()
            MyBase.New()
            InitializeComponent()
        End Sub

#Region " Component Designer generated code "

        'Component overrides dispose to clean up the component list.
        Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
            If disposing Then
                If components IsNot Nothing Then
                    components.Dispose()
                End If
            End If
            MyBase.Dispose(disposing)
        End Sub

        'Required by the Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Designer
        'It can be modified using the Designer.
        'Do not modify it using the code editor.
        Friend WithEvents ReportFooter As DevExpress.XtraReports.UI.ReportFooterBand
        Protected Friend WithEvents XrLine1 As DevExpress.XtraReports.UI.XRLine
        Protected Friend WithEvents XrLabel_Total_YTD As DevExpress.XtraReports.UI.XRLabel
        Protected Friend WithEvents XrLabel_Total_QTD As DevExpress.XtraReports.UI.XRLabel
        Protected Friend WithEvents XrLabel_Total_MTD As DevExpress.XtraReports.UI.XRLabel
        Protected Friend WithEvents XrLabel12 As DevExpress.XtraReports.UI.XRLabel
        Protected Friend WithEvents XrLabel1 As DevExpress.XtraReports.UI.XRLabel
        Protected Friend WithEvents XrLabel_Description_Title As DevExpress.XtraReports.UI.XRLabel
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
            Dim XrSummary1 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary
            Dim XrSummary2 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary
            Dim XrSummary3 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary
            Me.ReportFooter = New DevExpress.XtraReports.UI.ReportFooterBand
            Me.XrLabel12 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_Total_MTD = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLine1 = New DevExpress.XtraReports.UI.XRLine
            Me.XrLabel_Total_QTD = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_Total_YTD = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel1 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_Description_Title = New DevExpress.XtraReports.UI.XRLabel
            CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
            '
            'XrLabel_HeaderLabel
            '
            Me.XrLabel_HeaderLabel.Text = "1. Appointments by the Status Code"
            '
            'PageHeader
            '
            Me.PageHeader.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel_Description_Title})
            Me.PageHeader.HeightF = 15.0!
            Me.PageHeader.Controls.SetChildIndex(Me.XrLabel_Description_Title, 0)
            Me.PageHeader.Controls.SetChildIndex(Me.XrLabel2, 0)
            Me.PageHeader.Controls.SetChildIndex(Me.XrLabel3, 0)
            Me.PageHeader.Controls.SetChildIndex(Me.XrLabel4, 0)
            '
            'ReportFooter
            '
            Me.ReportFooter.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel12, Me.XrLabel_Total_MTD, Me.XrLine1, Me.XrLabel_Total_QTD, Me.XrLabel_Total_YTD})
            Me.ReportFooter.HeightF = 48.0!
            Me.ReportFooter.Name = "ReportFooter"
            '
            'XrLabel12
            '
            Me.XrLabel12.CanGrow = False
            Me.XrLabel12.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel12.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 23.00002!)
            Me.XrLabel12.Name = "XrLabel12"
            Me.XrLabel12.SizeF = New System.Drawing.SizeF(292.8333!, 14.99999!)
            Me.XrLabel12.Text = "TOTALS"
            Me.XrLabel12.WordWrap = False
            '
            'XrLabel_Total_MTD
            '
            Me.XrLabel_Total_MTD.CanGrow = False
            Me.XrLabel_Total_MTD.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "MTDCount")})
            Me.XrLabel_Total_MTD.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel_Total_MTD.LocationFloat = New DevExpress.Utils.PointFloat(295.8335!, 23.00002!)
            Me.XrLabel_Total_MTD.Name = "XrLabel_Total_MTD"
            Me.XrLabel_Total_MTD.SizeF = New System.Drawing.SizeF(101.0!, 15.0!)
            XrSummary1.FormatString = "{0:#,0}"
            XrSummary1.Running = DevExpress.XtraReports.UI.SummaryRunning.Report
            Me.XrLabel_Total_MTD.Summary = XrSummary1
            Me.XrLabel_Total_MTD.Text = "0"
            Me.XrLabel_Total_MTD.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
            Me.XrLabel_Total_MTD.WordWrap = False
            '
            'XrLine1
            '
            Me.XrLine1.LineWidth = 2
            Me.XrLine1.LocationFloat = New DevExpress.Utils.PointFloat(298.83!, 5.0!)
            Me.XrLine1.Name = "XrLine1"
            Me.XrLine1.SizeF = New System.Drawing.SizeF(300.0!, 9.0!)
            '
            'XrLabel_Total_QTD
            '
            Me.XrLabel_Total_QTD.CanGrow = False
            Me.XrLabel_Total_QTD.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "QTDCount")})
            Me.XrLabel_Total_QTD.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel_Total_QTD.LocationFloat = New DevExpress.Utils.PointFloat(396.8335!, 23.00002!)
            Me.XrLabel_Total_QTD.Name = "XrLabel_Total_QTD"
            Me.XrLabel_Total_QTD.SizeF = New System.Drawing.SizeF(101.0!, 15.0!)
            XrSummary2.FormatString = "{0:#,0}"
            XrSummary2.Running = DevExpress.XtraReports.UI.SummaryRunning.Report
            Me.XrLabel_Total_QTD.Summary = XrSummary2
            Me.XrLabel_Total_QTD.Text = "0"
            Me.XrLabel_Total_QTD.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
            Me.XrLabel_Total_QTD.WordWrap = False
            '
            'XrLabel_Total_YTD
            '
            Me.XrLabel_Total_YTD.CanGrow = False
            Me.XrLabel_Total_YTD.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "YTDCount")})
            Me.XrLabel_Total_YTD.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel_Total_YTD.LocationFloat = New DevExpress.Utils.PointFloat(497.8334!, 23.00002!)
            Me.XrLabel_Total_YTD.Name = "XrLabel_Total_YTD"
            Me.XrLabel_Total_YTD.SizeF = New System.Drawing.SizeF(101.0!, 15.0!)
            XrSummary3.FormatString = "{0:#,0}"
            XrSummary3.Running = DevExpress.XtraReports.UI.SummaryRunning.Report
            Me.XrLabel_Total_YTD.Summary = XrSummary3
            Me.XrLabel_Total_YTD.Text = "0"
            Me.XrLabel_Total_YTD.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
            Me.XrLabel_Total_YTD.WordWrap = False
            '
            'XrLabel1
            '
            Me.XrLabel1.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel1.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
            Me.XrLabel1.Name = "XrLabel1"
            Me.XrLabel1.SizeF = New System.Drawing.SizeF(250.0!, 15.0!)
            Me.XrLabel1.Text = "Appointment Status"
            '
            'XrLabel_Description_Title
            '
            Me.XrLabel_Description_Title.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel_Description_Title.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
            Me.XrLabel_Description_Title.Name = "XrLabel_Description_Title"
            Me.XrLabel_Description_Title.SizeF = New System.Drawing.SizeF(295.8333!, 15.0!)
            Me.XrLabel_Description_Title.Text = "Appointment Status"
            Me.XrLabel_Description_Title.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            '
            'AppointmentActivity_ByStatus
            '
            Me.Bands.AddRange(New DevExpress.XtraReports.UI.Band() {Me.Detail, Me.ReportHeader, Me.PageHeader, Me.TopMarginBand1, Me.BottomMarginBand1, Me.ReportFooter})
            Me.Version = "10.1"
            Me.Controls.SetChildIndex(Me.ReportFooter, 0)
            Me.Controls.SetChildIndex(Me.BottomMarginBand1, 0)
            Me.Controls.SetChildIndex(Me.TopMarginBand1, 0)
            Me.Controls.SetChildIndex(Me.PageHeader, 0)
            Me.Controls.SetChildIndex(Me.ReportHeader, 0)
            Me.Controls.SetChildIndex(Me.Detail, 0)
            CType(Me, System.ComponentModel.ISupportInitialize).EndInit()

        End Sub
#End Region

    End Class
End Namespace
