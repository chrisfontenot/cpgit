#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}
#End Region

Option Compare Binary
Option Explicit On 
Option Strict On

Imports DebtPlus.Reports
Imports DevExpress.XtraReports.UI

Namespace Appointments.Activity
    Public Class AppointmentActivityClass
        Inherits DebtPlus.Reports.Template.DatedTemplateXtraReportClass

        Public Sub New()
            MyBase.New()
            InitializeComponent()

            'AddHandler BeforePrint, AddressOf AppointmentActivityClass_BeforePrint
            'AddHandler Subreport_ByStatus.BeforePrint, AddressOf ByStatus_BeforePrint
            'AddHandler Subreport_ByType.BeforePrint, AddressOf ByType_BeforePrint
            'AddHandler Subreport_ByResult.BeforePrint, AddressOf ByResult_BeforePrint
            'AddHandler Subreport_Misc.BeforePrint, AddressOf ByMisc_BeforePrint

            ReportFilter.IsEnabled = True
        End Sub

        ''' <summary>
        ''' Determine if we need to ask for parameters
        ''' </summary>
        Public Overrides Function NeedParameters() As Boolean
            Return Not ToDateDefined
        End Function

        ''' <summary>
        ''' Title associated with the report
        ''' </summary>
        Public Overrides ReadOnly Property ReportTitle() As String
            Get
                Return "Appointment Activity"
            End Get
        End Property

        ''' <summary>
        ''' SubTitle associated with the report
        ''' </summary>
        Public Overrides ReadOnly Property ReportSubTitle() As String
            Get
                Dim Description As String
                Dim office As System.Int32 = -1
                Dim objValue As Object = GetCurrentColumnValue("office")
                If objValue IsNot Nothing AndAlso objValue IsNot System.DBNull.Value Then
                    office = Convert.ToInt32(objValue, System.Globalization.CultureInfo.InvariantCulture)
                End If

                If office <= 0 Then
                    Description = "all offices"
                Else
                    Dim row As System.Data.DataRow = GetOfficeRow(Me, office)
                    If row Is Nothing Then
                        Description = String.Format("unknown office #{0:f0}", office)
                    Else
                        Description = String.Empty
                        If row("description") IsNot Nothing AndAlso row("description") IsNot System.DBNull.Value Then
                            Description = String.Format("the {0} office", Convert.ToString(row("description"), System.Globalization.CultureInfo.InvariantCulture).Trim)
                        End If
                    End If
                End If

                Return System.String.Format("This report covers the ending date of {0:d} for {1}", Parameter_ToDate, Description)
            End Get
        End Property

        ''' <summary>
        ''' Request the parameters for the report
        ''' </summary>
        Public Overrides Function RequestReportParameters() As System.Windows.Forms.DialogResult
            Dim Answer As System.Windows.Forms.DialogResult = DialogResult.OK

            If NeedParameters() Then
                Using frm As New AppointmentActivityParametersForm()
                    Answer = frm.ShowDialog
                    Parameter_ToDate = frm.Parameter_ToDate
                End Using
            End If

            '-- The answer should be OK if we want to show the report.
            Return Answer
        End Function

        Dim ds As New System.Data.DataSet("ds")

        ''' <summary>
        ''' Bind the dataset to the report
        ''' </summary>
        Private Sub AppointmentActivityClass_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs)
            Dim rpt As DevExpress.XtraReports.UI.XtraReport = CType(sender, DevExpress.XtraReports.UI.XtraReport)
            Const TableName As String = "rpt_AppointmentActivity"

            '-- Create a dataset for the report fields
            Dim tbl As System.Data.DataTable = ds.Tables(TableName)
            If tbl Is Nothing Then
                Dim sqlInfo As DebtPlus.LINQ.SQLInfoClass = DebtPlus.LINQ.SQLInfoClass.getDefault()
                Using cn As New System.Data.SqlClient.SqlConnection(sqlInfo.ConnectionString)
                    cn.Open()
                    Using cmd As New System.Data.SqlClient.SqlCommand()
                        cmd.Connection = New System.Data.SqlClient.SqlConnection(sqlInfo.ConnectionString)
                        cmd.CommandText = "rpt_AppointmentActivity"
                        cmd.CommandType = System.Data.CommandType.StoredProcedure
                        cmd.CommandTimeout = 0

                        Using da As New System.Data.SqlClient.SqlDataAdapter(cmd)
                            da.Fill(ds, TableName)
                            tbl = ds.Tables(TableName)

                            rpt.DataSource = New System.Data.DataView(tbl, CType(sender, DebtPlus.Interfaces.Reports.IReportFilter).ReportFilter.ViewFilter, String.Empty, System.Data.DataViewRowState.CurrentRows)
                        End Using
                    End Using
                End Using
            End If
        End Sub

        Private Function GetOfficeRow(ByRef rpt As DevExpress.XtraReports.UI.XtraReport, ByVal Office As System.Int32) As System.Data.DataRow
            Dim tbl As System.Data.DataTable = ds.Tables("offices")
            If tbl IsNot Nothing Then
                Dim row As System.Data.DataRow = tbl.Rows.Find(Office)
                If row IsNot Nothing Then
                    Return row
                End If
            End If

            '-- Read the office row
            Dim sqlInfo As DebtPlus.LINQ.SQLInfoClass = DebtPlus.LINQ.SQLInfoClass.getDefault()
            Using cn As New System.Data.SqlClient.SqlConnection(sqlInfo.ConnectionString)
                cn.Open()
                Using cmd As New System.Data.SqlClient.SqlCommand()
                    cmd.Connection = cn
                    cmd.CommandText = "SELECT office,name as description FROM offices WHERE office=@office"
                    cmd.CommandType = System.Data.CommandType.Text
                    cmd.Parameters.Add("@office", System.Data.SqlDbType.Int).Value = Office

                    Using da As New System.Data.SqlClient.SqlDataAdapter(cmd)
                        da.Fill(ds, "offices")
                        tbl = ds.Tables("offices")
                        If tbl.PrimaryKey.GetUpperBound(0) < 0 Then
                            tbl.PrimaryKey = New System.Data.DataColumn() {tbl.Columns("office")}
                        End If

                        Return tbl.Rows.Find(Office)
                    End Using
                End Using
            End Using
        End Function

        Private Sub ByStatus_BeforePrint(ByVal Sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs)
            Const TableName As String = "rpt_AppointmentActivity_ByStatus"
            Dim subRpt As DevExpress.XtraReports.UI.XRSubreport = CType(Sender, DevExpress.XtraReports.UI.XRSubreport)
            Dim rpt As DevExpress.XtraReports.UI.XtraReport = CType(subRpt.ReportSource, DevExpress.XtraReports.UI.XtraReport)
            Dim MasterRpt As DevExpress.XtraReports.UI.XtraReport = CType(rpt.MasterReport, DevExpress.XtraReports.UI.XtraReport)
            Dim sqlInfo As DebtPlus.LINQ.SQLInfoClass = DebtPlus.LINQ.SQLInfoClass.getDefault()
            Dim ds As System.Data.DataSet = CType(MasterRpt.DataSource, System.Data.DataView).Table.DataSet
            Dim Office As System.Int32 = Convert.ToInt32(MasterRpt.GetCurrentColumnValue("office"), System.Globalization.CultureInfo.InvariantCulture)

            '-- Read the dataset for the information
            Dim tbl As System.Data.DataTable = ds.Tables(TableName)
            If tbl IsNot Nothing Then
                tbl.Clear()
            End If

            Using cn As System.Data.SqlClient.SqlConnection = New System.Data.SqlClient.SqlConnection(sqlInfo.ConnectionString)
                cn.Open()
                Using cmd As New System.Data.SqlClient.SqlCommand()
                    cmd.Connection = cn
                    cmd.CommandText = TableName
                    cmd.CommandType = System.Data.CommandType.StoredProcedure
                    System.Data.SqlClient.SqlCommandBuilder.DeriveParameters(cmd)

                    cmd.Parameters(1).Value = MasterRpt.Parameters("ParameterToDate").Value
                    cmd.Parameters(2).Value = Office

                    Using da As New System.Data.SqlClient.SqlDataAdapter(cmd)
                        da.Fill(ds, TableName)
                        tbl = ds.Tables(TableName)
                    End Using
                End Using
            End Using

            If tbl.Rows.Count > 0 Then
                rpt.DataSource = tbl.DefaultView
            Else
                e.Cancel = True
            End If
        End Sub

        Private Sub ByResult_BeforePrint(ByVal Sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs)
            Const TableName As String = "rpt_AppointmentActivity_ByResult"
            Dim subRpt As DevExpress.XtraReports.UI.XRSubreport = CType(Sender, DevExpress.XtraReports.UI.XRSubreport)
            Dim rpt As DevExpress.XtraReports.UI.XtraReport = CType(subRpt.ReportSource, DevExpress.XtraReports.UI.XtraReport)
            Dim MasterRpt As DevExpress.XtraReports.UI.XtraReport = CType(rpt.MasterReport, DevExpress.XtraReports.UI.XtraReport)
            Dim sqlInfo As DebtPlus.LINQ.SQLInfoClass = DebtPlus.LINQ.SQLInfoClass.getDefault()
            Dim ds As System.Data.DataSet = CType(MasterRpt.DataSource, System.Data.DataView).Table.DataSet
            Dim Office As System.Int32 = Convert.ToInt32(MasterRpt.GetCurrentColumnValue("office"), System.Globalization.CultureInfo.InvariantCulture)

            '-- Read the dataset for the information
            Dim tbl As System.Data.DataTable = ds.Tables(TableName)
            If tbl IsNot Nothing Then
                tbl.Clear()
            End If

            Using cn As System.Data.SqlClient.SqlConnection = New System.Data.SqlClient.SqlConnection(sqlInfo.ConnectionString)
                cn.Open()
                Using cmd As New System.Data.SqlClient.SqlCommand()
                    cmd.Connection = cn
                    cmd.CommandText = TableName
                    cmd.CommandType = System.Data.CommandType.StoredProcedure
                    System.Data.SqlClient.SqlCommandBuilder.DeriveParameters(cmd)

                    cmd.Parameters(1).Value = MasterRpt.Parameters("ParameterToDate").Value
                    cmd.Parameters(2).Value = Office

                    Using da As New System.Data.SqlClient.SqlDataAdapter(cmd)
                        da.Fill(ds, TableName)
                        tbl = ds.Tables(TableName)
                    End Using
                End Using
            End Using

            If tbl.Rows.Count > 0 Then
                rpt.DataSource = tbl.DefaultView
            Else
                e.Cancel = True
            End If
        End Sub

        Private Sub ByType_BeforePrint(ByVal Sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs)
            Const TableName As String = "rpt_AppointmentActivity_ByType"
            Dim subRpt As DevExpress.XtraReports.UI.XRSubreport = CType(Sender, DevExpress.XtraReports.UI.XRSubreport)
            Dim rpt As DevExpress.XtraReports.UI.XtraReport = CType(subRpt.ReportSource, DevExpress.XtraReports.UI.XtraReport)
            Dim MasterRpt As DevExpress.XtraReports.UI.XtraReport = CType(rpt.MasterReport, DevExpress.XtraReports.UI.XtraReport)
            Dim sqlInfo As DebtPlus.LINQ.SQLInfoClass = DebtPlus.LINQ.SQLInfoClass.getDefault()
            Dim ds As System.Data.DataSet = CType(MasterRpt.DataSource, System.Data.DataView).Table.DataSet
            Dim Office As System.Int32 = Convert.ToInt32(MasterRpt.GetCurrentColumnValue("office"), System.Globalization.CultureInfo.InvariantCulture)

            '-- Read the dataset for the information
            Dim tbl As System.Data.DataTable = ds.Tables(TableName)
            If tbl IsNot Nothing Then
                tbl.Clear()
            End If

            Using cn As System.Data.SqlClient.SqlConnection = New System.Data.SqlClient.SqlConnection(sqlInfo.ConnectionString)
                cn.Open()
                Using cmd As New System.Data.SqlClient.SqlCommand()
                    cmd.Connection = cn
                    cmd.CommandText = TableName
                    cmd.CommandType = System.Data.CommandType.StoredProcedure
                    System.Data.SqlClient.SqlCommandBuilder.DeriveParameters(cmd)

                    cmd.Parameters(1).Value = MasterRpt.Parameters("ParameterToDate").Value
                    cmd.Parameters(2).Value = Office

                    Using da As New System.Data.SqlClient.SqlDataAdapter(cmd)
                        da.Fill(ds, TableName)
                        tbl = ds.Tables(TableName)
                    End Using
                End Using
            End Using

            If tbl.Rows.Count > 0 Then
                rpt.DataSource = tbl.DefaultView
            Else
                e.Cancel = True
            End If
        End Sub

        Private Sub ByMisc_BeforePrint(ByVal Sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs)
            Const TableName As String = "rpt_AppointmentActivity_Misc"
            Dim subRpt As DevExpress.XtraReports.UI.XRSubreport = CType(Sender, DevExpress.XtraReports.UI.XRSubreport)
            Dim rpt As DevExpress.XtraReports.UI.XtraReport = CType(subRpt.ReportSource, DevExpress.XtraReports.UI.XtraReport)
            Dim MasterRpt As DevExpress.XtraReports.UI.XtraReport = CType(rpt.MasterReport, DevExpress.XtraReports.UI.XtraReport)
            Dim sqlInfo As DebtPlus.LINQ.SQLInfoClass = DebtPlus.LINQ.SQLInfoClass.getDefault()
            Dim ds As System.Data.DataSet = CType(MasterRpt.DataSource, System.Data.DataView).Table.DataSet
            Dim Office As System.Int32 = Convert.ToInt32(MasterRpt.GetCurrentColumnValue("office"), System.Globalization.CultureInfo.InvariantCulture)

            '-- Read the dataset for the information
            Dim tbl As System.Data.DataTable = ds.Tables(TableName)
            If tbl IsNot Nothing Then
                tbl.Clear()
            End If

            Using cn As System.Data.SqlClient.SqlConnection = New System.Data.SqlClient.SqlConnection(sqlInfo.ConnectionString)
                cn.Open()
                Using cmd As New System.Data.SqlClient.SqlCommand()
                    cmd.Connection = cn
                    cmd.CommandText = TableName
                    cmd.CommandType = System.Data.CommandType.StoredProcedure
                    System.Data.SqlClient.SqlCommandBuilder.DeriveParameters(cmd)

                    cmd.Parameters(1).Value = MasterRpt.Parameters("ParameterToDate").Value
                    cmd.Parameters(2).Value = Office

                    Using da As New System.Data.SqlClient.SqlDataAdapter(cmd)
                        da.Fill(ds, TableName)
                        tbl = ds.Tables(TableName)
                    End Using
                End Using
            End Using

            If tbl.Rows.Count > 0 Then
                rpt.DataSource = tbl.DefaultView
            Else
                e.Cancel = True
            End If
        End Sub

#Region " Component Designer generated code "

        'Component overrides dispose to clean up the component list.
        Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
            If disposing Then
                If components IsNot Nothing Then
                    components.Dispose()
                End If
                ds.Dispose()
            End If
            MyBase.Dispose(disposing)
        End Sub

        'Required by the Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Designer
        'It can be modified using the Designer.
        'Do not modify it using the code editor.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
            Me.components = New System.ComponentModel.Container()
            Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(AppointmentActivityClass))
            Me.XrLabel_Office = New DevExpress.XtraReports.UI.XRLabel()
            Me.Subreport_Misc = New DevExpress.XtraReports.UI.XRSubreport()
            Me.AppointmentActivity_Misc1 = New DebtPlus.Reports.Appointments.Activity.AppointmentActivity_Misc(Me.components)
            Me.Subreport_ByResult = New DevExpress.XtraReports.UI.XRSubreport()
            Me.AppointmentActivity_ByResult1 = New DebtPlus.Reports.Appointments.Activity.AppointmentActivity_ByResult(Me.components)
            Me.Subreport_ByType = New DevExpress.XtraReports.UI.XRSubreport()
            Me.AppointmentActivity_ByType1 = New DebtPlus.Reports.Appointments.Activity.AppointmentActivity_ByType(Me.components)
            Me.Subreport_ByStatus = New DevExpress.XtraReports.UI.XRSubreport()
            Me.AppointmentActivity_ByStatus1 = New DebtPlus.Reports.Appointments.Activity.AppointmentActivity_ByStatus(Me.components)
            CType(Me.AppointmentActivity_Misc1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.AppointmentActivity_ByResult1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.AppointmentActivity_ByType1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.AppointmentActivity_ByStatus1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
            '
            'XrLabel_Title
            '
            Me.XrLabel_Title.StylePriority.UseFont = False
            '
            'XrPageInfo_PageNumber
            '
            Me.XrPageInfo_PageNumber.StylePriority.UseTextAlignment = False
            '
            'XRLabel_Agency_Name
            '
            Me.XRLabel_Agency_Name.StylePriority.UseTextAlignment = False
            '
            'XrLabel_Agency_Address3
            '
            Me.XrLabel_Agency_Address3.StylePriority.UseFont = False
            Me.XrLabel_Agency_Address3.StylePriority.UseTextAlignment = False
            '
            'XrLabel_Agency_Address1
            '
            Me.XrLabel_Agency_Address1.StylePriority.UseFont = False
            Me.XrLabel_Agency_Address1.StylePriority.UseTextAlignment = False
            '
            'XrLabel_Agency_Phone
            '
            Me.XrLabel_Agency_Phone.StylePriority.UseFont = False
            Me.XrLabel_Agency_Phone.StylePriority.UseTextAlignment = False
            '
            'XrLabel_Agency_Address2
            '
            Me.XrLabel_Agency_Address2.StylePriority.UseFont = False
            Me.XrLabel_Agency_Address2.StylePriority.UseTextAlignment = False
            '
            'Detail
            '
            Me.Detail.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.Subreport_Misc, Me.Subreport_ByResult, Me.Subreport_ByType, Me.XrLabel_Office, Me.Subreport_ByStatus})
            Me.Detail.HeightF = 148.0!
            Me.Detail.PageBreak = DevExpress.XtraReports.UI.PageBreak.AfterBand
            '
            'XrLabel_Office
            '
            Me.XrLabel_Office.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "office")})
            Me.XrLabel_Office.LocationFloat = New DevExpress.Utils.PointFloat(683.0!, 0.0!)
            Me.XrLabel_Office.Name = "XrLabel_Office"
            Me.XrLabel_Office.SizeF = New System.Drawing.SizeF(117.0!, 17.0!)
            Me.XrLabel_Office.Visible = False
            '
            'Subreport_Misc
            '
            Me.Subreport_Misc.LocationFloat = New DevExpress.Utils.PointFloat(50.0!, 42.00001!)
            Me.Subreport_Misc.Name = "Subreport_Misc"
            Me.Subreport_Misc.ReportSource = Me.AppointmentActivity_Misc1
            Me.Subreport_Misc.Scripts.OnBeforePrint = "Subreport_Misc_BeforePrint"
            Me.Subreport_Misc.SizeF = New System.Drawing.SizeF(501.0417!, 23.0!)
            '
            'Subreport_ByResult
            '
            Me.Subreport_ByResult.LocationFloat = New DevExpress.Utils.PointFloat(50.0!, 125.0!)
            Me.Subreport_ByResult.Name = "Subreport_ByResult"
            Me.Subreport_ByResult.ReportSource = Me.AppointmentActivity_ByResult1
            Me.Subreport_ByResult.Scripts.OnBeforePrint = "Subreport_ByResult_BeforePrint"
            Me.Subreport_ByResult.SizeF = New System.Drawing.SizeF(501.0417!, 23.00002!)
            '
            'Subreport_ByType
            '
            Me.Subreport_ByType.LocationFloat = New DevExpress.Utils.PointFloat(50.0!, 82.99999!)
            Me.Subreport_ByType.Name = "Subreport_ByType"
            Me.Subreport_ByType.ReportSource = Me.AppointmentActivity_ByType1
            Me.Subreport_ByType.Scripts.OnBeforePrint = "Subreport_ByType_BeforePrint"
            Me.Subreport_ByType.SizeF = New System.Drawing.SizeF(501.0417!, 23.00002!)
            '
            'Subreport_ByStatus
            '
            Me.Subreport_ByStatus.LocationFloat = New DevExpress.Utils.PointFloat(50.0!, 0.0!)
            Me.Subreport_ByStatus.Name = "Subreport_ByStatus"
            Me.Subreport_ByStatus.ReportSource = Me.AppointmentActivity_ByStatus1
            Me.Subreport_ByStatus.Scripts.OnBeforePrint = "Subreport_ByStatus_BeforePrint"
            Me.Subreport_ByStatus.SizeF = New System.Drawing.SizeF(501.0417!, 23.0!)
            '
            'AppointmentActivityClass
            '
            Me.Bands.AddRange(New DevExpress.XtraReports.UI.Band() {Me.TopMarginBand1, Me.Detail, Me.BottomMarginBand1, Me.PageHeader, Me.PageFooter})
            Me.ReportPrintOptions.PrintOnEmptyDataSource = False
            Me.ScriptLanguage = DevExpress.XtraReports.ScriptLanguage.VisualBasic
            Me.ScriptReferencesString = resources.GetString("$this.ScriptReferencesString")
            Me.Scripts.OnBeforePrint = "AppointmentActivityClass_BeforePrint"
            Me.ScriptsSource = resources.GetString("$this.ScriptsSource")
            Me.StyleSheet.AddRange(New DevExpress.XtraReports.UI.XRControlStyle() {Me.XrControlStyle_Header, Me.XrControlStyle_Totals, Me.XrControlStyle_HeaderPannel, Me.XrControlStyle_GroupHeader})
            Me.Version = "11.2"
            CType(Me.AppointmentActivity_Misc1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.AppointmentActivity_ByResult1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.AppointmentActivity_ByType1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.AppointmentActivity_ByStatus1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me, System.ComponentModel.ISupportInitialize).EndInit()

        End Sub
        Protected Friend WithEvents XrLabel_Office As DevExpress.XtraReports.UI.XRLabel
        Protected Friend WithEvents Subreport_ByStatus As DevExpress.XtraReports.UI.XRSubreport
        Protected Friend WithEvents Subreport_ByType As DevExpress.XtraReports.UI.XRSubreport
        Protected Friend WithEvents Subreport_ByResult As DevExpress.XtraReports.UI.XRSubreport
        Protected Friend WithEvents Subreport_Misc As DevExpress.XtraReports.UI.XRSubreport
        Private WithEvents AppointmentActivity_ByStatus1 As DebtPlus.Reports.Appointments.Activity.AppointmentActivity_ByStatus
        Private WithEvents AppointmentActivity_ByType1 As DebtPlus.Reports.Appointments.Activity.AppointmentActivity_ByType
        Private WithEvents AppointmentActivity_ByResult1 As DebtPlus.Reports.Appointments.Activity.AppointmentActivity_ByResult
        Private WithEvents AppointmentActivity_Misc1 As DebtPlus.Reports.Appointments.Activity.AppointmentActivity_Misc
#End Region
    End Class
End Namespace
