#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}
#End Region

Option Compare Binary
Option Explicit On
Option Strict On

Imports DebtPlus.Reports.Template.Forms

Namespace Appointments.Activity

    Friend Class AppointmentActivityParametersForm
        Inherits ReportParametersForm

        Public ReadOnly Property Parameter_ToDate() As Date
            Get
                Return Convert.ToDateTime(DateEdit_Ending.EditValue)
            End Get
        End Property

        Public Sub New()
            MyBase.New()
            InitializeComponent()
            AddHandler Me.Load, AddressOf DateReportParametersForm_Load
        End Sub

        Public Sub New(ByVal FromDate As Date, ByVal ToDate As Date)
            MyClass.New()
            DateEdit_Ending.EditValue = ToDate
        End Sub

#Region " Windows Form Designer generated code "

        'Form overrides dispose to clean up the component list.
        Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
            If disposing Then
                If components IsNot Nothing Then
                    components.Dispose()
                End If
            End If
            MyBase.Dispose(disposing)
        End Sub

        'Required by the Windows Form Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Windows Form Designer
        'It can be modified using the Windows Form Designer.  
        'Do not modify it using the code editor.
        Protected Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
        Protected Friend WithEvents ComboboxEdit_DateRange As DevExpress.XtraEditors.ComboBoxEdit
        Protected Friend WithEvents DateEdit_Ending As DevExpress.XtraEditors.DateEdit
        Protected Friend WithEvents Label3 As DevExpress.XtraEditors.LabelControl
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
            Me.GroupBox1 = New System.Windows.Forms.GroupBox
            Me.ComboboxEdit_DateRange = New DevExpress.XtraEditors.ComboBoxEdit
            Me.DateEdit_Ending = New DevExpress.XtraEditors.DateEdit
            Me.Label3 = New DevExpress.XtraEditors.LabelControl

            Me.GroupBox1.SuspendLayout()
            CType(Me.ComboboxEdit_DateRange.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.DateEdit_Ending.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.SuspendLayout()
            '
            'ButtonOK
            '
            Me.ButtonOK.Location = New System.Drawing.Point(248, 17)
            Me.ButtonOK.Name = "ButtonOK"
            Me.ButtonOK.Size = New System.Drawing.Size(80, 26)
            '
            'ButtonCancel
            '
            Me.ButtonCancel.Location = New System.Drawing.Point(248, 52)
            Me.ButtonCancel.Name = "ButtonCancel"
            Me.ButtonCancel.Size = New System.Drawing.Size(80, 26)
            '
            'GroupBox1
            '
            Me.GroupBox1.Controls.Add(Me.ComboboxEdit_DateRange)
            Me.GroupBox1.Controls.Add(Me.DateEdit_Ending)
            Me.GroupBox1.Controls.Add(Me.Label3)
            Me.GroupBox1.Location = New System.Drawing.Point(8, 9)
            Me.GroupBox1.Name = "GroupBox1"
            Me.GroupBox1.Size = New System.Drawing.Size(232, 86)
            Me.GroupBox1.TabIndex = 82
            Me.GroupBox1.TabStop = False
            Me.GroupBox1.Text = " Date Range "
            '
            'ComboboxEdit_DateRange
            '
            Me.ComboboxEdit_DateRange.EditValue = ""
            Me.ComboboxEdit_DateRange.Location = New System.Drawing.Point(16, 26)
            Me.ComboboxEdit_DateRange.Name = "ComboboxEdit_DateRange"
            '
            'ComboboxEdit_DateRange.Properties
            '
            Me.ComboboxEdit_DateRange.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.ComboboxEdit_DateRange.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
            Me.ComboboxEdit_DateRange.Size = New System.Drawing.Size(184, 20)
            Me.ComboboxEdit_DateRange.TabIndex = 0
            Me.ComboboxEdit_DateRange.ToolTip = "Select a standard period for the date ranges"
            Me.ComboboxEdit_DateRange.ToolTipController = Me.ToolTipController1
            '
            'DateEdit_Ending
            '
            Me.DateEdit_Ending.EditValue = New Date(2005, 12, 24, 0, 0, 0, 0)
            Me.DateEdit_Ending.Location = New System.Drawing.Point(112, 52)
            Me.DateEdit_Ending.Name = "DateEdit_Ending"
            '
            'DateEdit_Ending.Properties
            '
            Me.DateEdit_Ending.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.DateEdit_Ending.Properties.Enabled = False
            Me.DateEdit_Ending.Properties.NullDate = ""
            Me.DateEdit_Ending.Size = New System.Drawing.Size(88, 20)
            Me.DateEdit_Ending.TabIndex = 4
            Me.DateEdit_Ending.ToolTip = "Enter the ending date for the report"
            Me.DateEdit_Ending.ToolTipController = Me.ToolTipController1
            '
            'Label3
            '
            Me.Label3.Location = New System.Drawing.Point(16, 54)
            Me.Label3.Name = "Label3"
            Me.Label3.Size = New System.Drawing.Size(88, 17)
            Me.Label3.TabIndex = 3
            Me.Label3.Text = "&Ending Period:"
            '
            'ParametersForm
            '
            Me.AutoScaleBaseSize = New System.Drawing.Size(5, 14)
            Me.ClientSize = New System.Drawing.Size(336, 103)
            Me.Controls.Add(Me.GroupBox1)
            Me.Name = "ParametersForm"
            Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
            Me.Controls.SetChildIndex(Me.ButtonOK, 0)
            Me.Controls.SetChildIndex(Me.ButtonCancel, 0)
            Me.Controls.SetChildIndex(Me.GroupBox1, 0)

            Me.GroupBox1.ResumeLayout(False)
            CType(Me.ComboboxEdit_DateRange.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.DateEdit_Ending.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            Me.ResumeLayout(False)

        End Sub

#End Region

        Private Sub DateReportParametersForm_Load(ByVal sender As Object, ByVal e As System.EventArgs)

            With ComboboxEdit_DateRange
                With .Properties
                    With .Items
                        .Clear()
                        .Add(New DebtPlus.Data.Controls.ComboboxItem("This month", 1, False))
                        .Add(New DebtPlus.Data.Controls.ComboboxItem("Last month", 2, False))
                        .Add(New DebtPlus.Data.Controls.ComboboxItem("Two months ago", 3, False))
                        .Add(New DebtPlus.Data.Controls.ComboboxItem("Three months ago", 4, False))
                        .Add(New DebtPlus.Data.Controls.ComboboxItem("Specific Date Range", 5, True))
                    End With
                End With

                ' Select the default of "today"
                .SelectedIndex = 0
                DateEdit_Ending.EditValue = Today
            End With

            With DateEdit_Ending
                .Properties.ShowClear = False
                .Properties.ValidateOnEnterKey = True
            End With

            ' Add the parameters
            AddHandler DateEdit_Ending.Validating, AddressOf DateEdit_Validating
            AddHandler ComboboxEdit_DateRange.EditValueChanged, AddressOf ComboBoxEdit_DateRange_EditValueChanged

            ' Enable or disable the OK button
            EnableOK()
        End Sub

        Protected Friend Sub DateEdit_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs)
            Dim ErrorMessage As String = System.String.Empty

            ' Try to convert the date
            With CType(sender, DevExpress.XtraEditors.DateEdit)
                Try
                    Dim TestDate As Date = Date.Parse(.Text)
                Catch
                    ErrorMessage = "Invalid Date. Use a format like MM/DD/YYYY"
                    e.Cancel = True
                End Try
            End With

            ' Set the error text
            DxErrorProvider1.SetError(CType(sender, System.Windows.Forms.Control), ErrorMessage)

            ' Enable or disable the OK button
            EnableOK()
        End Sub

        Protected Overrides Function HasErrors() As Boolean

            ' Look for any error condition. Disable the OK button if the form is not complete.
            Do
                If DxErrorProvider1.GetError(DateEdit_Ending) <> System.String.Empty Then Exit Do
                If DxErrorProvider1.GetError(ComboboxEdit_DateRange) <> System.String.Empty Then Exit Do

                ' There is no shown error.
                Return False
            Loop

            ' There is a pending error. Disable the OK button.
            Return True
        End Function

        Private Sub EnableOK()
            ButtonOK.Enabled = Not HasErrors()
        End Sub

        Protected Friend Overridable Sub ComboBoxEdit_DateRange_EditValueChanged(ByVal sender As Object, ByVal e As System.EventArgs)
            With CType(sender, DevExpress.XtraEditors.ComboBoxEdit)
                Dim Item As DebtPlus.Data.Controls.ComboboxItem = CType(.SelectedItem, DebtPlus.Data.Controls.ComboboxItem)

                ' Enable or disable the date fields
                DateEdit_Ending.Enabled = Item.active

                ' Determine the relative dates for the period
                Select Case Convert.ToInt32(Item.value)
                    Case 1      ' This month
                        DateEdit_Ending.EditValue = Today

                    Case 2      ' Last month
                        DateEdit_Ending.EditValue = (New Date(Today.Year, Today.Month, 1)).AddDays(-1)

                    Case 3      ' Two months ago
                        DateEdit_Ending.EditValue = (New Date(Today.Year, Today.Month, 1)).AddMonths(-1).AddDays(-1)

                    Case 4      ' Three months ago
                        DateEdit_Ending.EditValue = (New Date(Today.Year, Today.Month, 1)).AddMonths(-2).AddDays(-1)

                    Case 5      ' Date entry

                    Case Else
                        Debug.Assert(False)
                End Select
            End With
        End Sub
    End Class
End Namespace
