#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}
#End Region

Imports DebtPlus.Reports.Template
Imports System.IO
Imports DebtPlus.Utils

Namespace Appointments.Activity
    Public Class AppointmentActivityClass
        Inherits DatedTemplateXtraReportClass

        Dim ds As New System.Data.DataSet("ds")

        ''' <summary>
        '''     Create an instance of our report
        ''' </summary>
        Public Sub New()
            MyBase.New()

            ' Name of the report file
            Const ReportName As String = "DebtPlus.Reports.Appointments.Activity.repx"

            ' See if there is a report reference that we can use
            Dim Fname As String = System.IO.Path.Combine(DebtPlus.Configuration.Config.ReportsDirectory, ReportName)
            Dim UseDefault As Boolean = True
            Try
                If (Not DebtPlus.Configuration.Config.LocalReportsOnly) AndAlso System.IO.File.Exists(Fname) Then
                    LoadLayout(Fname)
                    UseDefault = False
                End If

            Catch ex As System.Exception
                DebtPlus.Svc.Common.ErrorHandling.HandleErrors(ex)
            End Try

            ' Load the standard report definition if we need a new item
            If UseDefault Then
                Dim asm As Reflection.Assembly = Reflection.Assembly.GetExecutingAssembly                                           ' changed
                Using ios As Stream = asm.GetManifestResourceStream(String.Format("DebtPlus.Reports.{0}", ReportName))              ' changed
                    If ios IsNot Nothing Then LoadLayout(ios) '                                                                       changed
                End Using
            End If

            ' Set the script references as needed
            ScriptReferences = DebtPlus.Utils.AssemblyTools.GetAllSiblingAssemblies()

            ' Update the subreports as well
            For Each BandPtr As DevExpress.XtraReports.UI.Band In Bands
                For Each CtlPtr As DevExpress.XtraReports.UI.XRControl In BandPtr.Controls
                    Dim Rpt As DevExpress.XtraReports.UI.XRSubreport = TryCast(CtlPtr, DevExpress.XtraReports.UI.XRSubreport)
                    If Rpt IsNot Nothing Then
                        Dim RptSrc As DevExpress.XtraReports.UI.XtraReport = TryCast(Rpt.ReportSource, DevExpress.XtraReports.UI.XtraReport)
                        If RptSrc IsNot Nothing Then
                            RptSrc.ScriptReferences = ScriptReferences
                        End If
                    End If
                Next
            Next

            ReportFilter.IsEnabled = True
        End Sub


        ''' <summary>
        '''     Determine if we need to ask for parameters
        ''' </summary>
        Public Overrides Function NeedParameters() As Boolean
            Return Not ToDateDefined
        End Function


        ''' <summary>
        '''     Title associated with the report
        ''' </summary>
        Public Overrides ReadOnly Property ReportTitle() As String
            Get
                Return "Appointment Activity"
            End Get
        End Property

        ''' <summary>
        '''     SubTitle associated with the report
        ''' </summary>
        Public Overrides ReadOnly Property ReportSubTitle() As String
            Get
                Dim Description As String
                Dim office As System.Int32 = -1
                Dim objValue As Object = GetCurrentColumnValue("office")
                If objValue IsNot Nothing AndAlso objValue IsNot System.DBNull.Value Then
                    office = Convert.ToInt32(objValue, System.Globalization.CultureInfo.InvariantCulture)
                End If

                If office <= 0 Then
                    Description = "all offices"
                Else
                    Dim o As DebtPlus.LINQ.office = DebtPlus.LINQ.Cache.office.getList().Find(Function(s) s.Id = office)
                    If o IsNot Nothing Then
                        Description = String.Format("the {0} office", o.name, System.Globalization.CultureInfo.InvariantCulture)
                    Else
                        Description = String.Format("unknown office #{0:f0}", office)
                    End If
                End If

                Return System.String.Format("This report covers the ending date of {0:d} for {1}", Parameter_ToDate, Description)
            End Get
        End Property

        ''' <summary>
        '''     Request the parameters for the report
        ''' </summary>
        Public Overrides Function RequestReportParameters() As System.Windows.Forms.DialogResult
            Dim answer As System.Windows.Forms.DialogResult = DialogResult.OK

            If NeedParameters() Then
                Using DialogForm As New AppointmentActivityParametersForm()
                    With DialogForm
                        answer = .ShowDialog()
                        Parameter_ToDate = .Parameter_ToDate
                    End With
                End Using
            End If

            ' The answer should be OK if we want to show the report.
            Return answer
        End Function
    End Class
End Namespace
