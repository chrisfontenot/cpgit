#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}

#End Region

Option Compare Binary
Option Explicit On
Option Strict On

Imports System.ComponentModel
Imports DevExpress.Data
Imports DevExpress.XtraEditors.Controls
Imports DebtPlus.Reports.Template

Imports System.Drawing.Printing
Imports DevExpress.XtraReports.Parameters
Imports System.Text
Imports System.Data.SqlClient
Imports DevExpress.XtraEditors
Imports DevExpress.Utils

Namespace Operations.Demographics
    Public Class DemographicsReport
        Inherits DatedTemplateXtraReportClass

        Protected ds As New DataSet("ds")


        ''' <summary>
        ''' Create an instance of our report
        ''' </summary>
        Public Sub New()
            MyBase.New()
            InitializeComponent()
            AddHandler Me.BeforePrint, AddressOf Report_RefreshDataReport
            AddHandler Me.ParametersRequestBeforeShow, AddressOf DemographicsReport_ParametersRequestBeforeShow
        End Sub

#Region " Component Designer generated code "

        'Component overrides dispose to clean up the component list.
        Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
            If disposing Then
                If components IsNot Nothing Then
                    components.Dispose()
                End If
            End If
            MyBase.Dispose(disposing)
        End Sub

        'Required by the Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Designer
        'It can be modified using the Designer.
        'Do not modify it using the code editor.
        Protected Friend WithEvents XrLabel1 As DevExpress.XtraReports.UI.XRLabel
        Protected Friend WithEvents XrLabel2 As DevExpress.XtraReports.UI.XRLabel
        Protected Friend WithEvents XrLabel3 As DevExpress.XtraReports.UI.XRLabel
        Protected Friend WithEvents XrLabel4 As DevExpress.XtraReports.UI.XRLabel
        Protected Friend WithEvents XrLabel5 As DevExpress.XtraReports.UI.XRLabel
        Protected Friend WithEvents XrLabel6 As DevExpress.XtraReports.UI.XRLabel
        Protected Friend WithEvents XrLabel7 As DevExpress.XtraReports.UI.XRLabel
        Protected Friend WithEvents XrLabel8 As DevExpress.XtraReports.UI.XRLabel
        Protected Friend WithEvents XrLabel_Description As DevExpress.XtraReports.UI.XRLabel
        Protected Friend WithEvents XrLabel_Ready As DevExpress.XtraReports.UI.XRLabel
        Protected Friend WithEvents XrLabel_Proposal As DevExpress.XtraReports.UI.XRLabel
        Protected Friend WithEvents XrLabel_Pending As DevExpress.XtraReports.UI.XRLabel
        Protected Friend WithEvents XrLabel_Inactive As DevExpress.XtraReports.UI.XRLabel
        Protected Friend WithEvents XrLabel_Exit As DevExpress.XtraReports.UI.XRLabel
        Protected Friend WithEvents XrLabel_Created As DevExpress.XtraReports.UI.XRLabel
        Protected Friend WithEvents XrLabel_Appt As DevExpress.XtraReports.UI.XRLabel
        Protected Friend WithEvents XrLabel_Active As DevExpress.XtraReports.UI.XRLabel
        Protected Friend WithEvents XrLabel_Workshop As DevExpress.XtraReports.UI.XRLabel
        Protected Friend WithEvents GroupFooter1 As DevExpress.XtraReports.UI.GroupFooterBand
        Protected Friend WithEvents GroupHeader1 As DevExpress.XtraReports.UI.GroupHeaderBand
        Protected Friend WithEvents XrLabel10 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents ParameterApptType As DevExpress.XtraReports.Parameters.Parameter
        Friend WithEvents ParameterRegion As DevExpress.XtraReports.Parameters.Parameter
        Friend WithEvents ParameterOffice As DevExpress.XtraReports.Parameters.Parameter
        Protected Friend WithEvents XrLabel9 As DevExpress.XtraReports.UI.XRLabel

        <System.Diagnostics.DebuggerStepThrough()>
        Private Sub InitializeComponent()
            Me.XrLabel1 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel2 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel3 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel4 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel5 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel6 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel7 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel8 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel9 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_Workshop = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_Active = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_Appt = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_Created = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_Exit = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_Inactive = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_Pending = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_Proposal = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_Ready = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_Description = New DevExpress.XtraReports.UI.XRLabel
            Me.GroupFooter1 = New DevExpress.XtraReports.UI.GroupFooterBand
            Me.XrLabel10 = New DevExpress.XtraReports.UI.XRLabel
            Me.GroupHeader1 = New DevExpress.XtraReports.UI.GroupHeaderBand
            Me.ParameterApptType = New DevExpress.XtraReports.Parameters.Parameter
            Me.ParameterRegion = New DevExpress.XtraReports.Parameters.Parameter
            Me.ParameterOffice = New DevExpress.XtraReports.Parameters.Parameter
            CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
            '
            'Detail
            '
            Me.Detail.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel_Description, Me.XrLabel_Ready, Me.XrLabel_Proposal, Me.XrLabel_Pending, Me.XrLabel_Inactive, Me.XrLabel_Exit, Me.XrLabel_Created, Me.XrLabel_Appt, Me.XrLabel_Active, Me.XrLabel_Workshop})
            Me.Detail.HeightF = 15.0!
            '
            'PageHeader
            '
            Me.PageHeader.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel9, Me.XrLabel8, Me.XrLabel7, Me.XrLabel6, Me.XrLabel5, Me.XrLabel4, Me.XrLabel3, Me.XrLabel2, Me.XrLabel1})
            Me.PageHeader.HeightF = 150.0!
            Me.PageHeader.Controls.SetChildIndex(Me.XrLabel1, 0)
            Me.PageHeader.Controls.SetChildIndex(Me.XrLabel2, 0)
            Me.PageHeader.Controls.SetChildIndex(Me.XrLabel3, 0)
            Me.PageHeader.Controls.SetChildIndex(Me.XrLabel4, 0)
            Me.PageHeader.Controls.SetChildIndex(Me.XrLabel5, 0)
            Me.PageHeader.Controls.SetChildIndex(Me.XrLabel6, 0)
            Me.PageHeader.Controls.SetChildIndex(Me.XrLabel7, 0)
            Me.PageHeader.Controls.SetChildIndex(Me.XrLabel8, 0)
            Me.PageHeader.Controls.SetChildIndex(Me.XrLabel9, 0)
            Me.PageHeader.Controls.SetChildIndex(Me.XrPanel_AgencyAddress, 0)
            Me.PageHeader.Controls.SetChildIndex(Me.XrLabel_Title, 0)
            Me.PageHeader.Controls.SetChildIndex(Me.XrLine_Title, 0)
            Me.PageHeader.Controls.SetChildIndex(Me.XrLabel_Subtitle, 0)
            Me.PageHeader.Controls.SetChildIndex(Me.XrPageInfo1, 0)
            '
            'PageFooter
            '
            Me.PageFooter.HeightF = 25.0!
            '
            'XrLabel_Title
            '
            Me.XrLabel_Title.StylePriority.UseFont = False
            '
            'XrPageInfo_PageNumber
            '
            Me.XrPageInfo_PageNumber.LocationFloat = New DevExpress.Utils.PointFloat(908.0!, 8.0!)
            Me.XrPageInfo_PageNumber.SizeF = New System.Drawing.SizeF(142.0!, 17.0!)
            Me.XrPageInfo_PageNumber.StylePriority.UseTextAlignment = False
            '
            'XrPanel_AgencyAddress
            '
            Me.XrPanel_AgencyAddress.LocationFloat = New DevExpress.Utils.PointFloat(750.0!, 0.0!)
            Me.XrPanel_AgencyAddress.SizeF = New System.Drawing.SizeF(300.0!, 108.0!)
            '
            'XRLabel_Agency_Name
            '
            Me.XRLabel_Agency_Name.StylePriority.UseTextAlignment = False
            '
            'XrLabel_Agency_Address3
            '
            Me.XrLabel_Agency_Address3.StylePriority.UseFont = False
            Me.XrLabel_Agency_Address3.StylePriority.UseTextAlignment = False
            '
            'XrLabel_Agency_Address1
            '
            Me.XrLabel_Agency_Address1.StylePriority.UseFont = False
            Me.XrLabel_Agency_Address1.StylePriority.UseTextAlignment = False
            '
            'XrLabel_Agency_Phone
            '
            Me.XrLabel_Agency_Phone.StylePriority.UseFont = False
            Me.XrLabel_Agency_Phone.StylePriority.UseTextAlignment = False
            '
            'XrLabel_Agency_Address2
            '
            Me.XrLabel_Agency_Address2.StylePriority.UseFont = False
            Me.XrLabel_Agency_Address2.StylePriority.UseTextAlignment = False
            '
            'XrLabel1
            '
            Me.XrLabel1.CanGrow = False
            Me.XrLabel1.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.XrLabel1.ForeColor = System.Drawing.SystemColors.Highlight
            Me.XrLabel1.LocationFloat = New DevExpress.Utils.PointFloat(233.0!, 125.0!)
            Me.XrLabel1.Name = "XrLabel1"
            Me.XrLabel1.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel1.SizeF = New System.Drawing.SizeF(82.0!, 15.0!)
            Me.XrLabel1.Text = "Active"
            Me.XrLabel1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrLabel1.WordWrap = False
            '
            'XrLabel2
            '
            Me.XrLabel2.CanGrow = False
            Me.XrLabel2.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.XrLabel2.ForeColor = System.Drawing.SystemColors.Highlight
            Me.XrLabel2.LocationFloat = New DevExpress.Utils.PointFloat(322.0!, 125.0!)
            Me.XrLabel2.Name = "XrLabel2"
            Me.XrLabel2.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel2.SizeF = New System.Drawing.SizeF(82.0!, 15.0!)
            Me.XrLabel2.Text = "Appt"
            Me.XrLabel2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrLabel2.WordWrap = False
            '
            'XrLabel3
            '
            Me.XrLabel3.CanGrow = False
            Me.XrLabel3.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.XrLabel3.ForeColor = System.Drawing.SystemColors.Highlight
            Me.XrLabel3.LocationFloat = New DevExpress.Utils.PointFloat(411.0!, 125.0!)
            Me.XrLabel3.Name = "XrLabel3"
            Me.XrLabel3.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel3.SizeF = New System.Drawing.SizeF(82.0!, 15.0!)
            Me.XrLabel3.Text = "Created"
            Me.XrLabel3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrLabel3.WordWrap = False
            '
            'XrLabel4
            '
            Me.XrLabel4.CanGrow = False
            Me.XrLabel4.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.XrLabel4.ForeColor = System.Drawing.SystemColors.Highlight
            Me.XrLabel4.LocationFloat = New DevExpress.Utils.PointFloat(767.0!, 125.0!)
            Me.XrLabel4.Name = "XrLabel4"
            Me.XrLabel4.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel4.SizeF = New System.Drawing.SizeF(82.0!, 15.0!)
            Me.XrLabel4.Text = "Proposal"
            Me.XrLabel4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrLabel4.WordWrap = False
            '
            'XrLabel5
            '
            Me.XrLabel5.CanGrow = False
            Me.XrLabel5.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.XrLabel5.ForeColor = System.Drawing.SystemColors.Highlight
            Me.XrLabel5.LocationFloat = New DevExpress.Utils.PointFloat(856.0!, 125.0!)
            Me.XrLabel5.Name = "XrLabel5"
            Me.XrLabel5.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel5.SizeF = New System.Drawing.SizeF(82.0!, 15.0!)
            Me.XrLabel5.Text = "Ready"
            Me.XrLabel5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrLabel5.WordWrap = False
            '
            'XrLabel6
            '
            Me.XrLabel6.CanGrow = False
            Me.XrLabel6.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.XrLabel6.ForeColor = System.Drawing.SystemColors.Highlight
            Me.XrLabel6.LocationFloat = New DevExpress.Utils.PointFloat(589.0!, 125.0!)
            Me.XrLabel6.Name = "XrLabel6"
            Me.XrLabel6.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel6.SizeF = New System.Drawing.SizeF(82.0!, 15.0!)
            Me.XrLabel6.Text = "Inactive"
            Me.XrLabel6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrLabel6.WordWrap = False
            '
            'XrLabel7
            '
            Me.XrLabel7.CanGrow = False
            Me.XrLabel7.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.XrLabel7.ForeColor = System.Drawing.SystemColors.Highlight
            Me.XrLabel7.LocationFloat = New DevExpress.Utils.PointFloat(500.0!, 125.0!)
            Me.XrLabel7.Name = "XrLabel7"
            Me.XrLabel7.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel7.SizeF = New System.Drawing.SizeF(82.0!, 15.0!)
            Me.XrLabel7.Text = "Exit"
            Me.XrLabel7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrLabel7.WordWrap = False
            '
            'XrLabel8
            '
            Me.XrLabel8.CanGrow = False
            Me.XrLabel8.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.XrLabel8.ForeColor = System.Drawing.SystemColors.Highlight
            Me.XrLabel8.LocationFloat = New DevExpress.Utils.PointFloat(945.0!, 125.0!)
            Me.XrLabel8.Name = "XrLabel8"
            Me.XrLabel8.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel8.SizeF = New System.Drawing.SizeF(82.0!, 15.0!)
            Me.XrLabel8.Text = "Workshop"
            Me.XrLabel8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrLabel8.WordWrap = False
            '
            'XrLabel9
            '
            Me.XrLabel9.CanGrow = False
            Me.XrLabel9.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.XrLabel9.ForeColor = System.Drawing.SystemColors.Highlight
            Me.XrLabel9.LocationFloat = New DevExpress.Utils.PointFloat(678.0!, 125.0!)
            Me.XrLabel9.Name = "XrLabel9"
            Me.XrLabel9.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel9.SizeF = New System.Drawing.SizeF(82.0!, 15.0!)
            Me.XrLabel9.Text = "Pending"
            Me.XrLabel9.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrLabel9.WordWrap = False
            '
            'XrLabel_Workshop
            '
            Me.XrLabel_Workshop.CanGrow = False
            Me.XrLabel_Workshop.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "v_workshop")})
            Me.XrLabel_Workshop.LocationFloat = New DevExpress.Utils.PointFloat(945.0!, 0.0!)
            Me.XrLabel_Workshop.Name = "XrLabel_Workshop"
            Me.XrLabel_Workshop.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_Workshop.SizeF = New System.Drawing.SizeF(82.0!, 15.0!)
            Me.XrLabel_Workshop.Text = "XrLabel_Workshop"
            Me.XrLabel_Workshop.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrLabel_Workshop.WordWrap = False
            '
            'XrLabel_Active
            '
            Me.XrLabel_Active.CanGrow = False
            Me.XrLabel_Active.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "v_active")})
            Me.XrLabel_Active.LocationFloat = New DevExpress.Utils.PointFloat(233.0!, 0.0!)
            Me.XrLabel_Active.Name = "XrLabel_Active"
            Me.XrLabel_Active.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_Active.SizeF = New System.Drawing.SizeF(82.0!, 15.0!)
            Me.XrLabel_Active.Text = "XrLabel_Active"
            Me.XrLabel_Active.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrLabel_Active.WordWrap = False
            '
            'XrLabel_Appt
            '
            Me.XrLabel_Appt.CanGrow = False
            Me.XrLabel_Appt.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "v_appt")})
            Me.XrLabel_Appt.LocationFloat = New DevExpress.Utils.PointFloat(322.0!, 0.0!)
            Me.XrLabel_Appt.Name = "XrLabel_Appt"
            Me.XrLabel_Appt.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_Appt.SizeF = New System.Drawing.SizeF(82.0!, 15.0!)
            Me.XrLabel_Appt.Text = "XrLabel_Appt"
            Me.XrLabel_Appt.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrLabel_Appt.WordWrap = False
            '
            'XrLabel_Created
            '
            Me.XrLabel_Created.CanGrow = False
            Me.XrLabel_Created.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "v_created")})
            Me.XrLabel_Created.LocationFloat = New DevExpress.Utils.PointFloat(411.0!, 0.0!)
            Me.XrLabel_Created.Name = "XrLabel_Created"
            Me.XrLabel_Created.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_Created.SizeF = New System.Drawing.SizeF(82.0!, 15.0!)
            Me.XrLabel_Created.Text = "XrLabel_Created"
            Me.XrLabel_Created.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrLabel_Created.WordWrap = False
            '
            'XrLabel_Exit
            '
            Me.XrLabel_Exit.CanGrow = False
            Me.XrLabel_Exit.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "v_exit")})
            Me.XrLabel_Exit.LocationFloat = New DevExpress.Utils.PointFloat(500.0!, 0.0!)
            Me.XrLabel_Exit.Name = "XrLabel_Exit"
            Me.XrLabel_Exit.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_Exit.SizeF = New System.Drawing.SizeF(82.0!, 15.0!)
            Me.XrLabel_Exit.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrLabel_Exit.WordWrap = False
            '
            'XrLabel_Inactive
            '
            Me.XrLabel_Inactive.CanGrow = False
            Me.XrLabel_Inactive.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "v_inactive")})
            Me.XrLabel_Inactive.LocationFloat = New DevExpress.Utils.PointFloat(589.0!, 0.0!)
            Me.XrLabel_Inactive.Name = "XrLabel_Inactive"
            Me.XrLabel_Inactive.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_Inactive.SizeF = New System.Drawing.SizeF(82.0!, 15.0!)
            Me.XrLabel_Inactive.Text = "XrLabel_Inactive"
            Me.XrLabel_Inactive.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrLabel_Inactive.WordWrap = False
            '
            'XrLabel_Pending
            '
            Me.XrLabel_Pending.CanGrow = False
            Me.XrLabel_Pending.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "v_pending")})
            Me.XrLabel_Pending.LocationFloat = New DevExpress.Utils.PointFloat(678.0!, 0.0!)
            Me.XrLabel_Pending.Name = "XrLabel_Pending"
            Me.XrLabel_Pending.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_Pending.SizeF = New System.Drawing.SizeF(82.0!, 15.0!)
            Me.XrLabel_Pending.Text = "XrLabel_Pending"
            Me.XrLabel_Pending.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrLabel_Pending.WordWrap = False
            '
            'XrLabel_Proposal
            '
            Me.XrLabel_Proposal.CanGrow = False
            Me.XrLabel_Proposal.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "v_proposal")})
            Me.XrLabel_Proposal.LocationFloat = New DevExpress.Utils.PointFloat(767.0!, 0.0!)
            Me.XrLabel_Proposal.Name = "XrLabel_Proposal"
            Me.XrLabel_Proposal.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_Proposal.SizeF = New System.Drawing.SizeF(82.0!, 15.0!)
            Me.XrLabel_Proposal.Text = "XrLabel_Proposal"
            Me.XrLabel_Proposal.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrLabel_Proposal.WordWrap = False
            '
            'XrLabel_Ready
            '
            Me.XrLabel_Ready.CanGrow = False
            Me.XrLabel_Ready.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "v_ready")})
            Me.XrLabel_Ready.LocationFloat = New DevExpress.Utils.PointFloat(856.0!, 0.0!)
            Me.XrLabel_Ready.Name = "XrLabel_Ready"
            Me.XrLabel_Ready.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_Ready.SizeF = New System.Drawing.SizeF(82.0!, 15.0!)
            Me.XrLabel_Ready.Text = "XrLabel_Ready"
            Me.XrLabel_Ready.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrLabel_Ready.WordWrap = False
            '
            'XrLabel_Description
            '
            Me.XrLabel_Description.CanGrow = False
            Me.XrLabel_Description.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "description")})
            Me.XrLabel_Description.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
            Me.XrLabel_Description.Name = "XrLabel_Description"
            Me.XrLabel_Description.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_Description.SizeF = New System.Drawing.SizeF(232.0!, 15.0!)
            Me.XrLabel_Description.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            Me.XrLabel_Description.WordWrap = False
            '
            'GroupFooter1
            '
            Me.GroupFooter1.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel10})
            Me.GroupFooter1.HeightF = 17.0!
            Me.GroupFooter1.Name = "GroupFooter1"
            '
            'XrLabel10
            '
            Me.XrLabel10.BorderColor = System.Drawing.Color.Transparent
            Me.XrLabel10.ForeColor = System.Drawing.Color.Transparent
            Me.XrLabel10.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
            Me.XrLabel10.Name = "XrLabel10"
            Me.XrLabel10.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel10.SizeF = New System.Drawing.SizeF(208.0!, 17.0!)
            Me.XrLabel10.StylePriority.UseBorderColor = False
            Me.XrLabel10.StylePriority.UseForeColor = False
            Me.XrLabel10.Text = "XrLabel10"
            '
            'GroupHeader1
            '
            Me.GroupHeader1.GroupFields.AddRange(New DevExpress.XtraReports.UI.GroupField() {New DevExpress.XtraReports.UI.GroupField("group", DevExpress.XtraReports.UI.XRColumnSortOrder.Ascending)})
            Me.GroupHeader1.HeightF = 0.0!
            Me.GroupHeader1.Name = "GroupHeader1"
            '
            'ParameterApptType
            '
            Me.ParameterApptType.Description = "Appointment Type"
            Me.ParameterApptType.Name = "ParameterApptType"
            Me.ParameterApptType.Type = GetType(System.Int32)
            Me.ParameterApptType.Value = 0
            Me.ParameterApptType.Visible = False
            '
            'ParameterRegion
            '
            Me.ParameterRegion.Description = "Region"
            Me.ParameterRegion.Name = "ParameterRegion"
            Me.ParameterRegion.Type = GetType(System.Int32)
            Me.ParameterRegion.Value = 0
            Me.ParameterRegion.Visible = False
            '
            'ParameterOffice
            '
            Me.ParameterOffice.Description = "Office"
            Me.ParameterOffice.Name = "ParameterOffice"
            Me.ParameterOffice.Type = GetType(System.Int32)
            Me.ParameterOffice.Value = 0
            Me.ParameterOffice.Visible = False
            '
            'DemographicsReport
            '
            Me.Bands.AddRange(New DevExpress.XtraReports.UI.Band() {Me.Detail, Me.PageHeader, Me.PageFooter, Me.TopMarginBand1, Me.BottomMarginBand1, Me.GroupFooter1, Me.GroupHeader1})
            Me.Landscape = True
            Me.PageHeight = 850
            Me.PageWidth = 1100
            Me.Parameters.AddRange(New DevExpress.XtraReports.Parameters.Parameter() {Me.ParameterApptType, Me.ParameterRegion, Me.ParameterOffice})
            Me.StyleSheet.AddRange(New DevExpress.XtraReports.UI.XRControlStyle() {Me.XrControlStyle_Header, Me.XrControlStyle_Totals, Me.XrControlStyle_HeaderPannel, Me.XrControlStyle_GroupHeader})
            Me.Version = "10.1"
            Me.Controls.SetChildIndex(Me.GroupHeader1, 0)
            Me.Controls.SetChildIndex(Me.GroupFooter1, 0)
            Me.Controls.SetChildIndex(Me.BottomMarginBand1, 0)
            Me.Controls.SetChildIndex(Me.TopMarginBand1, 0)
            Me.Controls.SetChildIndex(Me.PageFooter, 0)
            Me.Controls.SetChildIndex(Me.PageHeader, 0)
            Me.Controls.SetChildIndex(Me.Detail, 0)
            CType(Me, System.ComponentModel.ISupportInitialize).EndInit()
        End Sub

#End Region


        ''' <summary>
        ''' Report Title
        ''' </summary>
        Public Overrides ReadOnly Property ReportTitle() As String
            Get
                Return "Client Demographics"
            End Get
        End Property


        ''' <summary>
        ''' Report SubTitle
        ''' </summary>
        Public Overrides ReadOnly Property ReportSubTitle() As String
            Get
                Dim sb As New StringBuilder
                Dim DateText As String = "clients who changed active status"
                If ParameterApptType.Value IsNot DBNull.Value Then DateText = "appointments"

                Dim strFrom As String = String.Empty
                Dim strTo As String = String.Empty

                If Parameter_ToDate <> DateTime.MaxValue Then
                    If Parameter_ToDate.Date = Now.Date Then
                        strTo = "today"
                    Else
                        strTo = Parameter_ToDate.ToShortDateString()
                    End If
                End If

                If Parameter_FromDate <> DateTime.MinValue Then
                    If Parameter_FromDate.Date = DateTime.Now.Date Then
                        strFrom = "today"
                    Else
                        strFrom = Parameter_FromDate.ToShortDateString()
                    End If
                End If

                If strFrom <> String.Empty AndAlso strTo <> String.Empty Then
                    sb.Append(Environment.NewLine)
                    If strFrom = strTo Then
                        sb.AppendFormat("- {0} on {1}", DateText, strTo)
                    Else
                        sb.AppendFormat("- {0} between {1} and {2}", DateText, strFrom, strTo)
                    End If
                ElseIf strFrom <> String.Empty Then
                    sb.Append(Environment.NewLine)
                    sb.AppendFormat("- {0} from {1}", DateText, strFrom)
                ElseIf strTo <> String.Empty Then
                    sb.Append(Environment.NewLine)
                    sb.AppendFormat("- {0} prior to {1}", DateText, strTo)
                End If

                If ParameterOffice.Value IsNot DBNull.Value Then
                    sb.Append(Environment.NewLine)
                    sb.AppendFormat("- in the {0} office", GetOfficeName(ParameterOffice.Value))
                End If

                If ParameterRegion.Value IsNot DBNull.Value Then
                    sb.Append(Environment.NewLine)
                    sb.AppendFormat("- in the {0} region", GetRegionName(ParameterRegion.Value))
                End If

                If ParameterApptType.Value IsNot DBNull.Value Then
                    sb.Append(Environment.NewLine)
                    sb.AppendFormat("- for appointment type {0}", GetAppointmentTypeName(ParameterApptType.Value))
                End If

                If sb.Length = 0 Then
                    sb.Append("This report is for all clients")
                Else
                    sb.Insert(0, "This report is for the following criteria")
                End If

                Return sb.ToString()
            End Get
        End Property

        Private Function GetOfficesTable() As DataTable
            Const TableName As String = "lst_offices"
            Dim tbl As DataTable = ds.Tables(TableName)
            If tbl Is Nothing Then
                Using cmd As SqlCommand = New System.Data.SqlClient.SqlCommand()
                    With cmd
                        .Connection = New System.Data.SqlClient.SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
                        .CommandText = TableName
                        .CommandType = CommandType.StoredProcedure
                    End With

                    Using da As New SqlDataAdapter(cmd)
                        da.Fill(ds, TableName)
                        tbl = ds.Tables(TableName)
                        With tbl
                            .PrimaryKey = New DataColumn() {.Columns("item_key")}
                            Dim row As DataRow = tbl.NewRow
                            With row
                                .Item("item_key") = -1
                                .Item("description") = "[All offices]"
                                .Item("default") = False
                                .Item("ActiveFlag") = True
                            End With
                            .Rows.InsertAt(row, 0)
                        End With
                    End Using
                End Using
            End If

            Return tbl
        End Function

        Private Function GetOfficeName(ByVal Office As Object) As String
            Dim Answer As String = String.Empty
            Dim tbl As DataTable = GetOfficesTable()
            If tbl IsNot Nothing Then
                Dim row As DataRow = tbl.Rows.Find(Office)
                If row IsNot Nothing AndAlso row("description") IsNot DBNull.Value Then
                    Answer = Convert.ToString(row("description")).Trim()
                End If
            End If

            Return Answer
        End Function

        Private Function GetRegionsTable() As DataTable
            Const TableName As String = "lst_regions"
            Dim tbl As DataTable = ds.Tables(TableName)
            If tbl Is Nothing Then
                Using cmd As SqlCommand = New System.Data.SqlClient.SqlCommand()
                    With cmd
                        .Connection = New System.Data.SqlClient.SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
                        .CommandText = TableName
                        .CommandType = CommandType.StoredProcedure
                    End With

                    Using da As New SqlDataAdapter(cmd)
                        da.Fill(ds, TableName)
                        tbl = ds.Tables(TableName)
                        With tbl
                            .PrimaryKey = New DataColumn() {.Columns("item_key")}
                            Dim row As DataRow = tbl.NewRow
                            With row
                                .Item("item_key") = -1
                                .Item("description") = "[All regions]"
                                .Item("default") = False
                                .Item("ActiveFlag") = True
                            End With
                            .Rows.InsertAt(row, 0)
                        End With
                    End Using
                End Using
            End If

            Return tbl
        End Function

        Private Function GetRegionName(ByVal Office As Object) As String
            Dim Answer As String = String.Empty
            Dim tbl As DataTable = GetRegionsTable()
            If tbl IsNot Nothing Then
                Dim row As DataRow = tbl.Rows.Find(Office)
                If row IsNot Nothing AndAlso row("description") IsNot DBNull.Value Then
                    Answer = Convert.ToString(row("description")).Trim()
                End If
            End If

            Return Answer
        End Function

        Private Function GetAppointmentTypesTable() As DataTable
            Const TableName As String = "lst_appt_types"
            Dim tbl As DataTable = ds.Tables(TableName)
            If tbl Is Nothing Then
                Using cmd As SqlCommand = New System.Data.SqlClient.SqlCommand()
                    With cmd
                        .Connection = New System.Data.SqlClient.SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
                        .CommandText = TableName
                        .CommandType = CommandType.StoredProcedure
                    End With

                    Using da As New SqlDataAdapter(cmd)
                        da.Fill(ds, TableName)
                        tbl = ds.Tables(TableName)
                        With tbl
                            .PrimaryKey = New DataColumn() {.Columns("item_key")}
                            Dim row As DataRow = tbl.NewRow
                            With row
                                .Item("item_key") = -1
                                .Item("description") = "[No Type Specified]"
                                .Item("default") = False
                                .Item("ActiveFlag") = True
                            End With
                            .Rows.InsertAt(row, 0)
                        End With
                    End Using
                End Using
            End If

            Return tbl
        End Function

        Private Function GetAppointmentTypeName(ByVal Office As Object) As String
            Dim Answer As String = String.Empty
            Dim tbl As DataTable = GetAppointmentTypesTable()
            If tbl IsNot Nothing Then
                Dim row As DataRow = tbl.Rows.Find(Office)
                If row IsNot Nothing AndAlso row("description") IsNot DBNull.Value Then
                    Answer = Convert.ToString(row("description")).Trim()
                End If
            End If

            Return Answer
        End Function


        ''' <summary>
        ''' Request the report parameters
        ''' </summary>
        Public Overrides Function RequestReportParameters() As DialogResult
            Dim answer As DialogResult = DialogResult.OK
            If NeedParameters() Then
                Using frm As New OperationsDemographicsParametersForm(True)
                    With frm
                        answer = .ShowDialog()
                        Parameter_FromDate = .Parameter_FromDate
                        Parameter_ToDate = .Parameter_ToDate
                        ParameterOffice.Value = .Parameter_Office
                        ParameterApptType.Value = .Parameter_ApptType
                        ParameterRegion.Value = .Parameter_Region
                    End With
                End Using
            End If

            Return answer
        End Function


        ''' <summary>
        ''' Retrieve the input table to be formatted
        ''' </summary>
        Protected Overridable Function GetInputTable() As DataTable
            Const TableName As String = "raw_data"
            Dim InputTable As DataTable = ds.Tables(TableName)
            If InputTable IsNot Nothing Then
                InputTable.Clear()
                InputTable = Nothing
            End If

            Dim current_cursor As Cursor = Cursor.Current
            Dim cn As SqlConnection = New System.Data.SqlClient.SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)

            Try
                Cursor.Current = Cursors.WaitCursor
                cn.Open()
                Using cmd As SqlCommand = CType(New System.Data.SqlClient.SqlCommand(), SqlCommand)
                    With cmd
                        .Connection = cn
                        .CommandText = "rpt_demographics_ytd"
                        .CommandType = CommandType.StoredProcedure
                        SqlCommandBuilder.DeriveParameters(cmd)

                        .CommandTimeout = 0

                        Dim v As Object = Parameter_FromDate
                        If v Is Nothing OrElse v Is DBNull.Value OrElse Convert.ToDateTime(v).Date = New DateTime(1900, 1, 1) Then v = DBNull.Value
                        .Parameters(1).Value = v

                        v = Parameter_ToDate
                        If v Is Nothing OrElse v Is DBNull.Value OrElse Convert.ToDateTime(v).Date = New DateTime(2099, 12, 31) Then v = DBNull.Value
                        .Parameters(2).Value = v

                        v = Me.ParameterRegion.Value
                        If v Is Nothing OrElse v Is DBNull.Value OrElse Convert.ToInt32(v) <= 0 Then v = DBNull.Value
                        .Parameters(3).Value = v

                        v = Me.ParameterApptType.Value
                        If v Is Nothing OrElse v Is DBNull.Value OrElse Convert.ToInt32(v) <= 0 Then v = DBNull.Value
                        .Parameters(4).Value = v

                        v = Me.ParameterOffice.Value
                        If v Is Nothing OrElse v Is DBNull.Value OrElse Convert.ToInt32(v) <= 0 Then v = DBNull.Value
                        .Parameters(5).Value = v
                    End With

                    Using da As New SqlDataAdapter(cmd)
                        da.Fill(ds, TableName)
                        InputTable = ds.Tables(TableName)
                    End Using
                End Using

            Finally
                Cursor.Current = current_cursor
            End Try

            Return InputTable
        End Function


        ''' <summary>
        ''' Bind the report to the dataset
        ''' </summary>
        Protected Sub Report_RefreshDataReport(ByVal sender As Object, ByVal e As PrintEventArgs)
            Const TableName As String = "demographics"

            ' Build a new table with the values
            Dim tbl As DataTable = ds.Tables(TableName)
            If tbl IsNot Nothing Then
                ds.Tables.Remove(TableName)
            End If

            tbl = New DataTable("demographics")
            With tbl

                ' The first column is the sequence number
                Dim col As New DataColumn("ID", GetType(Int32))
                With col
                    .AutoIncrement = True
                    .AutoIncrementSeed = 1
                    .AutoIncrementStep = 1
                End With
                .Columns.Add(col)

                ' Add the other columns
                col = New DataColumn("description", GetType(String))
                col.DefaultValue = String.Empty
                .Columns.Add(col)

                col = New DataColumn("group", GetType(String))
                col.DefaultValue = String.Empty
                .Columns.Add(col)

                col = New DataColumn("v_active", GetType(String))
                col.DefaultValue = String.Empty
                .Columns.Add(col)

                col = New DataColumn("v_appt", GetType(String))
                col.DefaultValue = String.Empty
                .Columns.Add(col)

                col = New DataColumn("v_created", GetType(String))
                col.DefaultValue = String.Empty
                .Columns.Add(col)

                col = New DataColumn("v_exit", GetType(String))
                col.DefaultValue = String.Empty
                .Columns.Add(col)

                col = New DataColumn("v_inactive", GetType(String))
                col.DefaultValue = String.Empty
                .Columns.Add(col)

                col = New DataColumn("v_pending", GetType(String))
                col.DefaultValue = String.Empty
                .Columns.Add(col)

                col = New DataColumn("v_proposal", GetType(String))
                col.DefaultValue = String.Empty
                .Columns.Add(col)

                col = New DataColumn("v_ready", GetType(String))
                col.DefaultValue = String.Empty
                .Columns.Add(col)

                col = New DataColumn("v_workshop", GetType(String))
                col.DefaultValue = String.Empty
                .Columns.Add(col)

                ' Set a primary key to the table for the description and group code
                .PrimaryKey = New DataColumn() {.Columns("description"), .Columns("group")}
            End With
            ds.Tables.Add(tbl)

            ' Load the table with the descriptions and group fields
            For Each drv As DataRowView In New DataView(GetInputTable(), String.Empty, "description", DataViewRowState.CurrentRows)

                ' Get the current value to be printed
                Dim ytd_value As Double = 0.0
                If drv("ytd_value") IsNot Nothing AndAlso drv("ytd_value") IsNot DBNull.Value Then ytd_value = Convert.ToDouble(drv("ytd_value"))

                Dim GroupValue As String = String.Empty
                If drv("group") IsNot Nothing AndAlso drv("group") IsNot DBNull.Value Then GroupValue = Convert.ToString(drv("group"))

                Dim DescriptionValue As String = String.Empty
                If drv("description") IsNot Nothing AndAlso drv("description") IsNot DBNull.Value Then DescriptionValue = Convert.ToString(drv("description"))

                ' From the group code, determine the formatting type
                Dim Formatting As String = String.Empty
                If GroupValue.Length > 2 Then Formatting = GroupValue.Substring(2, 1)

                ' Translate the ytd value to the corresponding string
                Dim ytd_string As String = String.Empty
                Select Case Formatting
                    Case "#"
                        ytd_string = String.Format("{0:n0}", Convert.ToInt32(ytd_value))

                    Case "." ' Averages of Int32 values
                        ytd_string = String.Format("{0:f2}", ytd_value)

                    Case "$" ' Dollar amount
                        ytd_string = String.Format("{0:c}", Convert.ToDecimal(ytd_value))

                    Case "%" ' Percentages
                        ytd_string = String.Format("{0:P}", ytd_value / 100.0)

                    Case Else ' Something else ???
                        ytd_string = ytd_value.ToString()
                End Select

                ' Find the row corresponding to this item
                Dim row As DataRow = tbl.Rows.Find(New Object() {DescriptionValue, GroupValue})

                ' Add a row if one is not in the table
                If row Is Nothing Then
                    row = tbl.NewRow
                    tbl.Rows.Add(row)

                    row.BeginEdit()
                    row("description") = DescriptionValue
                    row("group") = GroupValue
                Else
                    row.BeginEdit()
                End If

                ' Add the value corresponding to the active status
                Select Case Convert.ToString(drv("active_status")).ToLower()
                    Case "a", "ar", "active"
                        row("v_active") = ytd_string
                    Case "apt"
                        row("v_appt") = ytd_string
                    Case "cre"
                        row("v_created") = ytd_string
                    Case "ex"
                        row("v_exit") = ytd_string
                    Case "i"
                        row("v_inactive") = ytd_string
                    Case "pnd"
                        row("v_pending") = ytd_string
                    Case "pro"
                        row("v_proposal") = ytd_string
                    Case "rdy"
                        row("v_ready") = ytd_string
                    Case "wks"
                        row("v_workshop") = ytd_string
                End Select
                row.EndEdit()

                ' Accept the changes to this table since we are not going to update it
                tbl.AcceptChanges()
            Next

            ' Form a view of the information once the tables have been updated
            DataSource = New DataView(tbl, String.Empty, "group, ID", DataViewRowState.CurrentRows)
        End Sub

        Private Sub DemographicsReport_ParametersRequestBeforeShow(ByVal sender As Object, ByVal e As ParametersRequestEventArgs)
            For Each pi As ParameterInfo In e.ParametersInformation
                If pi.Parameter.Name = "ParameterOffice" Then
                    pi.Editor = GetOfficeEditor()
                ElseIf pi.Parameter.Name = "ParameterApptType" Then
                    pi.Editor = GetAppointmentTypeEditor()
                ElseIf pi.Parameter.Name = "ParameterRegion" Then
                    pi.Editor = GetRegionsEditor()
                End If
            Next
        End Sub

        Private Function GetOfficeEditor() As Control
            Dim ctl As New LookUpEdit
            With ctl
                With .Properties
                    .DataSource = GetOfficesTable.DefaultView
                    .AllowNullInput = DefaultBoolean.[True]
                    .Columns.AddRange(New LookUpColumnInfo() {New LookUpColumnInfo("item_key", "ID", 20, FormatType.Numeric, "f0", False, HorzAlignment.[Default]), New LookUpColumnInfo("description", "Description", 20, FormatType.None, "", True, HorzAlignment.[Default], ColumnSortOrder.Ascending)})
                    .DisplayMember = "description"
                    .NullText = "[All Offices]"
                    .ShowFooter = False
                    .ShowHeader = False
                    .ValueMember = "item_key"
                End With
            End With
            AddHandler ctl.EditValueChanged, AddressOf EditValueChanged
            Return ctl
        End Function

        Private Function GetRegionsEditor() As Control
            Dim ctl As New LookUpEdit
            With ctl
                With .Properties
                    .DataSource = GetRegionsTable.DefaultView
                    .AllowNullInput = DefaultBoolean.[True]
                    .Columns.AddRange(New LookUpColumnInfo() {New LookUpColumnInfo("item_key", "ID", 20, FormatType.Numeric, "f0", False, HorzAlignment.[Default]), New LookUpColumnInfo("description", "Description", 20, FormatType.None, "", True, HorzAlignment.[Default], ColumnSortOrder.Ascending)})
                    .DisplayMember = "description"
                    .NullText = "[All Regions]"
                    .ShowFooter = False
                    .ShowHeader = False
                    .ValueMember = "item_key"
                End With
            End With
            AddHandler ctl.EditValueChanged, AddressOf EditValueChanged
            Return ctl
        End Function

        Private Function GetAppointmentTypeEditor() As Control
            Dim ctl As New LookUpEdit
            With ctl
                With .Properties
                    .DataSource = GetAppointmentTypesTable.DefaultView
                    .AllowNullInput = DefaultBoolean.[True]
                    .Columns.AddRange(New LookUpColumnInfo() {New LookUpColumnInfo("item_key", "ID", 20, FormatType.Numeric, "f0", False, HorzAlignment.[Default]), New LookUpColumnInfo("description", "Description", 20, FormatType.None, "", True, HorzAlignment.[Default], ColumnSortOrder.Ascending)})
                    .DisplayMember = "description"
                    .NullText = "[No Type Specified]"
                    .ShowFooter = False
                    .ShowHeader = False
                    .ValueMember = "item_key"
                End With
            End With
            AddHandler ctl.EditValueChanged, AddressOf EditValueChanged
            Return ctl
        End Function

        Private Sub EditValueChanged(ByVal Sender As Object, ByVal e As EventArgs)
            With CType(Sender, LookUpEdit)
                If .EditValue IsNot Nothing AndAlso .EditValue IsNot DBNull.Value Then
                    If Convert.ToInt32(.EditValue) < 0 Then
                        .EditValue = DBNull.Value
                    End If
                End If
            End With
        End Sub
    End Class
End Namespace
