﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form1
    Inherits DebtPlus.Reports.Template.Forms.DateReportParametersForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim ComboboxItem2 As DebtPlus.Data.Controls.ComboboxItem = New DebtPlus.Data.Controls.ComboboxItem()
        Me.LabelControl3 = New DevExpress.XtraEditors.LabelControl()
        Me.LookUpEditApptType = New DevExpress.XtraEditors.LookUpEdit()
        Me.LookUpEditRegion = New DevExpress.XtraEditors.LookUpEdit()
        Me.LabelControl2 = New DevExpress.XtraEditors.LabelControl()
        Me.XrOffice_param_07_1 = New DevExpress.XtraEditors.LookUpEdit()
        Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl()
        CType(Me.XrGroup_param_08_1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.XrGroup_param_08_1.SuspendLayout()
        CType(Me.XrCombo_param_08_1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.XrDate_param_08_2.Properties.VistaTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.XrDate_param_08_2.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.XrDate_param_08_1.Properties.VistaTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.XrDate_param_08_1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DxErrorProvider1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LookUpEditApptType.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LookUpEditRegion.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.XrOffice_param_07_1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'XrCombo_param_08_1
        '
        ComboboxItem2.tag = Nothing
        ComboboxItem2.value = DebtPlus.Utils.DateRange.Today
        Me.XrCombo_param_08_1.EditValue = ComboboxItem2
        '
        'XrDate_param_08_2
        '
        Me.XrDate_param_08_2.EditValue = New Date(2015, 7, 22, 0, 0, 0, 0)
        '
        'XrDate_param_08_1
        '
        Me.XrDate_param_08_1.EditValue = New Date(2015, 7, 22, 0, 0, 0, 0)
        '
        'ButtonOK
        '
        Me.ButtonOK.TabIndex = 7
        '
        'ButtonCancel
        '
        Me.ButtonCancel.TabIndex = 8
        '
        'LabelControl3
        '
        Me.LabelControl3.Location = New System.Drawing.Point(9, 182)
        Me.LabelControl3.Name = "LabelControl3"
        Me.LabelControl3.Size = New System.Drawing.Size(88, 13)
        Me.LabelControl3.TabIndex = 5
        Me.LabelControl3.Text = "Appointment Type"
        '
        'LookUpEditApptType
        '
        Me.LookUpEditApptType.Location = New System.Drawing.Point(116, 179)
        Me.LookUpEditApptType.Name = "LookUpEditApptType"
        Me.LookUpEditApptType.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.LookUpEditApptType.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.LookUpEditApptType.Properties.Columns.AddRange(New DevExpress.XtraEditors.Controls.LookUpColumnInfo() {New DevExpress.XtraEditors.Controls.LookUpColumnInfo("item_key", "ID", 20, DevExpress.Utils.FormatType.Numeric, "f0", False, DevExpress.Utils.HorzAlignment.[Default]), New DevExpress.XtraEditors.Controls.LookUpColumnInfo("description", "Description", 20, DevExpress.Utils.FormatType.None, "", True, DevExpress.Utils.HorzAlignment.[Default], DevExpress.Data.ColumnSortOrder.Ascending)})
        Me.LookUpEditApptType.Properties.DisplayMember = "description"
        Me.LookUpEditApptType.Properties.NullText = "[No Type Specified]"
        Me.LookUpEditApptType.Properties.ShowFooter = False
        Me.LookUpEditApptType.Properties.ShowHeader = False
        Me.LookUpEditApptType.Properties.SortColumnIndex = 1
        Me.LookUpEditApptType.Properties.ValueMember = "item_key"
        Me.LookUpEditApptType.Size = New System.Drawing.Size(208, 20)
        Me.LookUpEditApptType.TabIndex = 6
        Me.LookUpEditApptType.ToolTip = "Choose an appointment type if you wish to restrict items to having this appointme" & _
    "nt (in the date ranage abvoe) only"
        '
        'LookUpEditRegion
        '
        Me.LookUpEditRegion.Location = New System.Drawing.Point(116, 153)
        Me.LookUpEditRegion.Name = "LookUpEditRegion"
        Me.LookUpEditRegion.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.LookUpEditRegion.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.LookUpEditRegion.Properties.Columns.AddRange(New DevExpress.XtraEditors.Controls.LookUpColumnInfo() {New DevExpress.XtraEditors.Controls.LookUpColumnInfo("item_key", "ID", 20, DevExpress.Utils.FormatType.Numeric, "f0", False, DevExpress.Utils.HorzAlignment.[Default]), New DevExpress.XtraEditors.Controls.LookUpColumnInfo("description", "Description", 20, DevExpress.Utils.FormatType.None, "", True, DevExpress.Utils.HorzAlignment.[Default], DevExpress.Data.ColumnSortOrder.Ascending)})
        Me.LookUpEditRegion.Properties.DisplayMember = "description"
        Me.LookUpEditRegion.Properties.NullText = "[All Regions]"
        Me.LookUpEditRegion.Properties.ShowFooter = False
        Me.LookUpEditRegion.Properties.ShowHeader = False
        Me.LookUpEditRegion.Properties.SortColumnIndex = 1
        Me.LookUpEditRegion.Properties.ValueMember = "item_key"
        Me.LookUpEditRegion.Size = New System.Drawing.Size(208, 20)
        Me.LookUpEditRegion.TabIndex = 4
        Me.LookUpEditRegion.ToolTip = "Choose a region if you wish to restrict items to this region only"
        '
        'LabelControl2
        '
        Me.LabelControl2.Location = New System.Drawing.Point(8, 156)
        Me.LabelControl2.Name = "LabelControl2"
        Me.LabelControl2.Size = New System.Drawing.Size(33, 13)
        Me.LabelControl2.TabIndex = 3
        Me.LabelControl2.Text = "Region"
        '
        'XrOffice_param_07_1
        '
        Me.XrOffice_param_07_1.Location = New System.Drawing.Point(116, 127)
        Me.XrOffice_param_07_1.Name = "XrOffice_param_07_1"
        Me.XrOffice_param_07_1.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.XrOffice_param_07_1.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.XrOffice_param_07_1.Properties.Columns.AddRange(New DevExpress.XtraEditors.Controls.LookUpColumnInfo() {New DevExpress.XtraEditors.Controls.LookUpColumnInfo("item_key", "ID", 20, DevExpress.Utils.FormatType.Numeric, "f0", False, DevExpress.Utils.HorzAlignment.[Default]), New DevExpress.XtraEditors.Controls.LookUpColumnInfo("description", "Description", 20, DevExpress.Utils.FormatType.None, "", True, DevExpress.Utils.HorzAlignment.[Default], DevExpress.Data.ColumnSortOrder.Ascending)})
        Me.XrOffice_param_07_1.Properties.DisplayMember = "description"
        Me.XrOffice_param_07_1.Properties.NullText = "[All Offices]"
        Me.XrOffice_param_07_1.Properties.ShowFooter = False
        Me.XrOffice_param_07_1.Properties.ShowHeader = False
        Me.XrOffice_param_07_1.Properties.SortColumnIndex = 1
        Me.XrOffice_param_07_1.Properties.ValueMember = "item_key"
        Me.XrOffice_param_07_1.Size = New System.Drawing.Size(208, 20)
        Me.XrOffice_param_07_1.TabIndex = 2
        Me.XrOffice_param_07_1.ToolTip = "Choose an office if you wish to restrict items to this region only"
        '
        'LabelControl1
        '
        Me.LabelControl1.Location = New System.Drawing.Point(8, 130)
        Me.LabelControl1.Name = "LabelControl1"
        Me.LabelControl1.Size = New System.Drawing.Size(29, 13)
        Me.LabelControl1.TabIndex = 1
        Me.LabelControl1.Text = "Office"
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(336, 206)
        Me.Controls.Add(Me.XrOffice_param_07_1)
        Me.Controls.Add(Me.LabelControl1)
        Me.Controls.Add(Me.LabelControl3)
        Me.Controls.Add(Me.LookUpEditApptType)
        Me.Controls.Add(Me.LookUpEditRegion)
        Me.Controls.Add(Me.LabelControl2)
        Me.Name = "Form1"
        Me.Controls.SetChildIndex(Me.ButtonOK, 0)
        Me.Controls.SetChildIndex(Me.ButtonCancel, 0)
        Me.Controls.SetChildIndex(Me.XrGroup_param_08_1, 0)
        Me.Controls.SetChildIndex(Me.LabelControl2, 0)
        Me.Controls.SetChildIndex(Me.LookUpEditRegion, 0)
        Me.Controls.SetChildIndex(Me.LookUpEditApptType, 0)
        Me.Controls.SetChildIndex(Me.LabelControl3, 0)
        Me.Controls.SetChildIndex(Me.LabelControl1, 0)
        Me.Controls.SetChildIndex(Me.XrOffice_param_07_1, 0)
        CType(Me.XrGroup_param_08_1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.XrGroup_param_08_1.ResumeLayout(False)
        Me.XrGroup_param_08_1.PerformLayout()
        CType(Me.XrCombo_param_08_1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.XrDate_param_08_2.Properties.VistaTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.XrDate_param_08_2.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.XrDate_param_08_1.Properties.VistaTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.XrDate_param_08_1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DxErrorProvider1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LookUpEditApptType.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LookUpEditRegion.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.XrOffice_param_07_1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents LabelControl3 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LookUpEditApptType As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents LookUpEditRegion As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents LabelControl2 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents XrOffice_param_07_1 As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
End Class
