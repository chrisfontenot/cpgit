﻿#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}
#End Region

Imports DebtPlus.Reports.Template.Forms
Imports System.Data.SqlClient
Imports DevExpress.Utils

Public Class Form1

    ' TRUE if "[All Offices]" is in the list
    Protected Property EnableAllOffices As Boolean = True

    Private sqlInfo As DebtPlus.LINQ.SQLInfoClass = DebtPlus.LINQ.SQLInfoClass.getDefault()
    Private ds As New DataSet("ds")

    Public Sub New()
        MyBase.New()
        InitializeComponent()
    End Sub

    Public Sub New(ByVal EnableAllOffices As Boolean)
        MyClass.New()
        MyClass.EnableAllOffices = EnableAllOffices
    End Sub

    Public Sub New(ByVal defaultItem As DebtPlus.Utils.DateRange, ByVal EnableAllOffices As Boolean)
        MyBase.New(defaultItem)
        InitializeComponent()
        MyClass.EnableAllOffices = EnableAllOffices
    End Sub

    ''' <summary>
    ''' Return the office selected in the form
    ''' </summary>
    Public ReadOnly Property Parameter_Office() As Object
        Get
            With XrOffice_param_07_1
                If .EditValue IsNot Nothing AndAlso .EditValue IsNot DBNull.Value Then
                    If Convert.ToInt32(.EditValue) > 0 Then Return Convert.ToInt32(.EditValue)
                End If

                ' If all counselors is not permitted then return -1
                If Not EnableAllOffices Then Return -1
                Return DBNull.Value
            End With
        End Get
    End Property

    ''' <summary>
    ''' Process the form load event
    ''' </summary>
    Private Sub DateCounselorReportParametersForm_Load(ByVal sender As Object, ByVal e As EventArgs) _
        Handles MyBase.Load
        OfficeDropDown_Load()
        ButtonOK.Enabled = Not HasErrors()
    End Sub

    ''' <summary>
    ''' Load the list of offices
    ''' </summary>
    Private ds As New DataSet("ds")

    Protected Sub OfficeDropDown_Load()

        Const tableName As String = "offices"
        Dim tbl As DataTable = ds.Tables(tableName)
        If tbl Is Nothing Then
            Using (New CursorManager())
                Dim gdr As Repository.GetDataResult = Repository.LookupTables.Offices.GetAll3(ds, tableName)
                If Not gdr.Success Then
                    DebtPlus.UI.Common.ErrorHandling.HandleErrors(gdr)
                    Return
                End If
            End Using
        End If

        tbl = ds.Tables(tableName)
        With tbl
            .PrimaryKey = New DataColumn() {.Columns("item_key")}
        End With

        With XrOffice_param_07_1
            .Properties.DataSource = New DataView(tbl, String.Empty, "description, item_key", DataViewRowState.CurrentRows)
            If tbl.Columns.Contains("default") Then
                Dim rows() As DataRow = tbl.Select("[Default]<>0 AND [ActiveFlag]<>0", String.Empty)
                If rows.GetUpperBound(0) >= 0 Then
                    .EditValue = rows(0)("item_key")
                End If
            End If

            If EnableAllOffices Then
                With tbl
                    Dim row As DataRow = tbl.NewRow
                    With row
                        .Item("item_key") = -1
                        .Item("description") = "[All Offices]"
                        .Item("default") = False
                        .Item("ActiveFlag") = True
                    End With
                    .Rows.InsertAt(row, 0)
                End With
                .Properties.AllowNullInput = DefaultBoolean.True
                .EditValue = -1
            Else
                .Properties.AllowNullInput = DefaultBoolean.False
            End If

            AddHandler .EditValueChanging, AddressOf DebtPlus.Data.Validation.LookUpEdit_ActiveTest
            AddHandler .EditValueChanged, AddressOf FormChanged
        End With
    End Sub

    ''' <summary>
    ''' Process the change in the form contents
    ''' </summary>
    Protected Overrides Sub FormChanged(ByVal Sender As Object, ByVal e As EventArgs)

        ' Replace the magic marker with the empty condition (This will recurse into our procedure again)
        With CType(Sender, LookUpEdit)
            If .EditValue IsNot Nothing AndAlso .EditValue IsNot DBNull.Value Then
                If Convert.ToInt32(.EditValue) < 0 Then
                    .EditValue = DBNull.Value
                End If
            End If
        End With

        MyBase.FormChanged(Sender, e)
    End Sub

    ''' <summary>
    ''' Look for errors in the counselor list
    ''' </summary>
    Protected Overrides Function HasErrors() As Boolean
        Return MyBase.HasErrors() OrElse (Not EnableAllOffices AndAlso XrOffice_param_07_1.EditValue Is DBNull.Value)
    End Function
























    ''' <summary>
    ''' Return the region selected in the form
    ''' </summary>
    Public ReadOnly Property Parameter_Region() As Object
        Get
            With LookUpEditRegion
                If .EditValue IsNot Nothing AndAlso .EditValue IsNot DBNull.Value Then
                    If Convert.ToInt32(.EditValue) > 0 Then Return Convert.ToInt32(.EditValue)
                End If

                ' If all counselors is not permitted then return -1
                If Not EnableAllOffices Then Return -1
                Return DBNull.Value
            End With
        End Get
    End Property


    ''' <summary>
    ''' Return the appointment type selected in the form
    ''' </summary>
    Public ReadOnly Property Parameter_ApptType() As Object
        Get
            With LookUpEditApptType
                If .EditValue IsNot Nothing AndAlso .EditValue IsNot DBNull.Value Then
                    If Convert.ToInt32(.EditValue) > 0 Then Return Convert.ToInt32(.EditValue)
                End If

                ' If all counselors is not permitted then return -1
                If Not EnableAllOffices Then Return -1
                Return DBNull.Value
            End With
        End Get
    End Property

    Private Sub ParametersForm_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load
        If Not DesignMode Then
            RegionDropDown_Load()
            ApptTypeDropDown_Load()
            ButtonOK.Enabled = Not HasErrors()
        End If
    End Sub


    ''' <summary>
    ''' Load the list of regions
    ''' </summary>
    Private Sub RegionDropDown_Load()
        Const TableName As String = "lst_regions"
        Dim tbl As DataTable = ds.Tables(TableName)
        If tbl Is Nothing Then
            Using cmd As SqlCommand = New System.Data.SqlClient.SqlCommand()
                With cmd
                    .Connection = New System.Data.SqlClient.SqlConnection(sqlInfo.ConnectionString)
                    .CommandText = TableName
                    .CommandType = CommandType.StoredProcedure
                End With

                Using da As New SqlDataAdapter(cmd)
                    da.Fill(ds, TableName)
                    tbl = ds.Tables(TableName)
                End Using
            End Using

            With tbl
                For Each col As DataColumn In .Columns
                    col.AllowDBNull = True
                Next

                .Columns("item_key").AllowDBNull = False
                .PrimaryKey = New DataColumn() {.Columns("item_key")}
            End With
        End If

        With LookUpEditRegion
            .Properties.DataSource = New DataView(tbl, String.Empty, "description, item_key", DataViewRowState.CurrentRows)
            If tbl.Columns.Contains("default") Then
                Dim rows() As DataRow = tbl.Select("[Default]<>0 AND [ActiveFlag]<>0", String.Empty)
                If rows.GetUpperBound(0) >= 0 Then
                    .EditValue = rows(0)("item_key")
                End If
            End If

            If EnableAllOffices Then
                With tbl
                    Dim row As DataRow = tbl.NewRow
                    With row
                        .Item("item_key") = -1
                        .Item("description") = "[All regions]"
                        .Item("default") = False
                        .Item("ActiveFlag") = True
                    End With
                    .Rows.InsertAt(row, 0)
                End With
                .Properties.AllowNullInput = DefaultBoolean.True
                .EditValue = -1
            Else
                .Properties.AllowNullInput = DefaultBoolean.False
            End If

            AddHandler .EditValueChanging, AddressOf DebtPlus.Data.Validation.LookUpEdit_ActiveTest
            AddHandler .EditValueChanged, AddressOf FormChanged
        End With
    End Sub

    ''' <summary>
    ''' Load the list of appt_types
    ''' </summary>
    Private Sub ApptTypeDropDown_Load()
        Const TableName As String = "lst_appt_types"
        Dim tbl As DataTable = ds.Tables(TableName)
        If tbl Is Nothing Then
            Using cmd As SqlCommand = New System.Data.SqlClient.SqlCommand()
                With cmd
                    .Connection = New System.Data.SqlClient.SqlConnection(sqlInfo.ConnectionString)
                    .CommandText = TableName
                    .CommandType = CommandType.StoredProcedure
                End With

                Using da As New SqlDataAdapter(cmd)
                    da.Fill(ds, TableName)
                    tbl = ds.Tables(TableName)
                End Using
            End Using

            With tbl
                For Each col As DataColumn In .Columns
                    col.AllowDBNull = True
                Next

                .Columns("item_key").AllowDBNull = False
                .PrimaryKey = New DataColumn() {.Columns("item_key")}
            End With
        End If

        With LookUpEditApptType
            .Properties.DataSource = New DataView(tbl, String.Empty, "description, item_key", DataViewRowState.CurrentRows)
            If tbl.Columns.Contains("default") Then
                Dim rows() As DataRow = tbl.Select("[Default]<>0 AND [ActiveFlag]<>0", String.Empty)
                If rows.GetUpperBound(0) >= 0 Then
                    .EditValue = rows(0)("item_key")
                End If
            End If

            If EnableAllOffices Then
                With tbl
                    Dim row As DataRow = tbl.NewRow
                    With row
                        .Item("item_key") = -1
                        .Item("description") = "[No type specified]"
                        .Item("default") = False
                        .Item("ActiveFlag") = True
                    End With
                    .Rows.InsertAt(row, 0)
                End With
                .Properties.AllowNullInput = DefaultBoolean.True
                .EditValue = -1
            Else
                .Properties.AllowNullInput = DefaultBoolean.False
            End If

            AddHandler .EditValueChanging, AddressOf DebtPlus.Data.Validation.LookUpEdit_ActiveTest
            AddHandler .EditValueChanged, AddressOf FormChanged
        End With
    End Sub
End Class