﻿Namespace Operations.Demographics
    <Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
    Partial Class OperationsDemographicsParametersForm

        'Form overrides dispose to clean up the component list.
        <System.Diagnostics.DebuggerNonUserCode()> _
        Protected Overrides Sub Dispose(ByVal disposing As Boolean)
            If disposing Then
                If components IsNot Nothing Then
                    components.Dispose()
                End If
                ds.Dispose()
            End If
            MyBase.Dispose(disposing)
        End Sub

        'Required by the Windows Form Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Windows Form Designer
        'It can be modified using the Windows Form Designer.  
        'Do not modify it using the code editor.
        <System.Diagnostics.DebuggerStepThrough()> _
        Private Sub InitializeComponent()
            Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(OperationsDemographicsParametersForm))
            Dim SuperToolTip1 As DevExpress.Utils.SuperToolTip = New DevExpress.Utils.SuperToolTip
            Dim ToolTipTitleItem1 As DevExpress.Utils.ToolTipTitleItem = New DevExpress.Utils.ToolTipTitleItem
            Dim ToolTipItem1 As DevExpress.Utils.ToolTipItem = New DevExpress.Utils.ToolTipItem
            Dim ComboboxItem1 As DebtPlus.Data.Controls.ComboboxItem = New DebtPlus.Data.Controls.ComboboxItem
            Me.LabelControl2 = New DevExpress.XtraEditors.LabelControl
            Me.LookUpEditRegion = New DevExpress.XtraEditors.LookUpEdit
            Me.LabelControl3 = New DevExpress.XtraEditors.LabelControl
            Me.LookUpEditApptType = New DevExpress.XtraEditors.LookUpEdit
            CType(Me.XrOffice_param_07_1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.XrGroup_param_08_1, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.XrGroup_param_08_1.SuspendLayout()
            CType(Me.XrCombo_param_08_1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.XrDate_param_08_2.Properties.VistaTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.XrDate_param_08_2.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.XrDate_param_08_1.Properties.VistaTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.XrDate_param_08_1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.DxErrorProvider1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LookUpEditRegion.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LookUpEditApptType.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.SuspendLayout()
            '
            'XrOffice_param_07_1
            '
            Me.XrOffice_param_07_1.EditValue = CType(resources.GetObject("XrOffice_param_07_1.EditValue"), Object)
            Me.XrOffice_param_07_1.Location = New System.Drawing.Point(120, 127)
            Me.XrOffice_param_07_1.Size = New System.Drawing.Size(208, 20)
            Me.XrOffice_param_07_1.TabIndex = 2
            Me.XrOffice_param_07_1.ToolTip = "Choose an office if you wish to restrict items to this office only"
            '
            'LabelControl1
            '
            Me.LabelControl1.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center
            Me.LabelControl1.TabIndex = 1
            '
            'XrGroup_param_08_1
            '
            SuperToolTip1.AllowHtmlText = DevExpress.Utils.DefaultBoolean.[True]
            ToolTipTitleItem1.Text = "Date Range"
            ToolTipItem1.LeftIndent = 6
            ToolTipItem1.Text = resources.GetString("ToolTipItem1.Text")
            SuperToolTip1.Items.Add(ToolTipTitleItem1)
            SuperToolTip1.Items.Add(ToolTipItem1)
            Me.ToolTipController1.SetSuperTip(Me.XrGroup_param_08_1, SuperToolTip1)
            Me.ToolTipController1.SetTitle(Me.XrGroup_param_08_1, "Date Range")
            '
            'XrCombo_param_08_1
            '
            ComboboxItem1.tag = Nothing
            ComboboxItem1.value = DebtPlus.Utils.DateRange.Today
            Me.XrCombo_param_08_1.EditValue = ComboboxItem1
            '
            'XrDate_param_08_2
            '
            Me.XrDate_param_08_2.Properties.VistaTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton, New DevExpress.XtraEditors.Controls.EditorButton, New DevExpress.XtraEditors.Controls.EditorButton, New DevExpress.XtraEditors.Controls.EditorButton, New DevExpress.XtraEditors.Controls.EditorButton, New DevExpress.XtraEditors.Controls.EditorButton, New DevExpress.XtraEditors.Controls.EditorButton, New DevExpress.XtraEditors.Controls.EditorButton})
            '
            'XrDate_param_08_1
            '
            Me.XrDate_param_08_1.Properties.VistaTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton, New DevExpress.XtraEditors.Controls.EditorButton, New DevExpress.XtraEditors.Controls.EditorButton, New DevExpress.XtraEditors.Controls.EditorButton, New DevExpress.XtraEditors.Controls.EditorButton, New DevExpress.XtraEditors.Controls.EditorButton, New DevExpress.XtraEditors.Controls.EditorButton, New DevExpress.XtraEditors.Controls.EditorButton})
            '
            'ButtonOK
            '
            Me.ButtonOK.TabIndex = 7
            '
            'ButtonCancel
            '
            Me.ButtonCancel.TabIndex = 8
            '
            'LabelControl2
            '
            Me.LabelControl2.Location = New System.Drawing.Point(12, 158)
            Me.LabelControl2.Name = "LabelControl2"
            Me.LabelControl2.Size = New System.Drawing.Size(33, 13)
            Me.LabelControl2.TabIndex = 3
            Me.LabelControl2.Text = "Region"
            '
            'LookUpEditRegion
            '
            Me.LookUpEditRegion.Location = New System.Drawing.Point(120, 155)
            Me.LookUpEditRegion.Name = "LookUpEditRegion"
            Me.LookUpEditRegion.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
            Me.LookUpEditRegion.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.LookUpEditRegion.Properties.Columns.AddRange(New DevExpress.XtraEditors.Controls.LookUpColumnInfo() {New DevExpress.XtraEditors.Controls.LookUpColumnInfo("item_key", "ID", 20, DevExpress.Utils.FormatType.Numeric, "f0", False, DevExpress.Utils.HorzAlignment.[Default]), New DevExpress.XtraEditors.Controls.LookUpColumnInfo("description", "Description", 20, DevExpress.Utils.FormatType.None, "", True, DevExpress.Utils.HorzAlignment.[Default], DevExpress.Data.ColumnSortOrder.Ascending)})
            Me.LookUpEditRegion.Properties.DisplayMember = "description"
            Me.LookUpEditRegion.Properties.NullText = "[All Regions]"
            Me.LookUpEditRegion.Properties.ShowFooter = False
            Me.LookUpEditRegion.Properties.ShowHeader = False
            Me.LookUpEditRegion.Properties.SortColumnIndex = 1
            Me.LookUpEditRegion.Properties.ValueMember = "item_key"
            Me.LookUpEditRegion.Size = New System.Drawing.Size(208, 20)
            Me.LookUpEditRegion.TabIndex = 4
            Me.LookUpEditRegion.ToolTip = "Choose a region if you wish to restrict items to this region only"
            '
            'LabelControl3
            '
            Me.LabelControl3.Location = New System.Drawing.Point(13, 184)
            Me.LabelControl3.Name = "LabelControl3"
            Me.LabelControl3.Size = New System.Drawing.Size(88, 13)
            Me.LabelControl3.TabIndex = 5
            Me.LabelControl3.Text = "Appointment Type"
            '
            'LookUpEditApptType
            '
            Me.LookUpEditApptType.Location = New System.Drawing.Point(120, 181)
            Me.LookUpEditApptType.Name = "LookUpEditApptType"
            Me.LookUpEditApptType.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
            Me.LookUpEditApptType.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.LookUpEditApptType.Properties.Columns.AddRange(New DevExpress.XtraEditors.Controls.LookUpColumnInfo() {New DevExpress.XtraEditors.Controls.LookUpColumnInfo("item_key", "ID", 20, DevExpress.Utils.FormatType.Numeric, "f0", False, DevExpress.Utils.HorzAlignment.[Default]), New DevExpress.XtraEditors.Controls.LookUpColumnInfo("description", "Description", 20, DevExpress.Utils.FormatType.None, "", True, DevExpress.Utils.HorzAlignment.[Default], DevExpress.Data.ColumnSortOrder.Ascending)})
            Me.LookUpEditApptType.Properties.DisplayMember = "description"
            Me.LookUpEditApptType.Properties.NullText = "[No Type Specified]"
            Me.LookUpEditApptType.Properties.ShowFooter = False
            Me.LookUpEditApptType.Properties.ShowHeader = False
            Me.LookUpEditApptType.Properties.SortColumnIndex = 1
            Me.LookUpEditApptType.Properties.ValueMember = "item_key"
            Me.LookUpEditApptType.Size = New System.Drawing.Size(208, 20)
            Me.LookUpEditApptType.TabIndex = 6
            Me.LookUpEditApptType.ToolTip = "Choose an appointment type if you wish to restrict items to having this appointme" & _
                "nt (in the date ranage abvoe) only"
            '
            'ParametersForm
            '
            Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
            Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
            Me.ClientSize = New System.Drawing.Size(336, 213)
            Me.Controls.Add(Me.LabelControl3)
            Me.Controls.Add(Me.LookUpEditApptType)
            Me.Controls.Add(Me.LookUpEditRegion)
            Me.Controls.Add(Me.LabelControl2)
            Me.Name = "ParametersForm"
            Me.Controls.SetChildIndex(Me.XrOffice_param_07_1, 0)
            Me.Controls.SetChildIndex(Me.LabelControl1, 0)
            Me.Controls.SetChildIndex(Me.LabelControl2, 0)
            Me.Controls.SetChildIndex(Me.LookUpEditRegion, 0)
            Me.Controls.SetChildIndex(Me.LookUpEditApptType, 0)
            Me.Controls.SetChildIndex(Me.LabelControl3, 0)
            Me.Controls.SetChildIndex(Me.ButtonOK, 0)
            Me.Controls.SetChildIndex(Me.ButtonCancel, 0)
            Me.Controls.SetChildIndex(Me.XrGroup_param_08_1, 0)
            Me.Controls.SetChildIndex(Me.LabelControl1, 0)
            Me.Controls.SetChildIndex(Me.XrOffice_param_07_1, 0)
            CType(Me.XrOffice_param_07_1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.XrGroup_param_08_1, System.ComponentModel.ISupportInitialize).EndInit()
            Me.XrGroup_param_08_1.ResumeLayout(False)
            Me.XrGroup_param_08_1.PerformLayout()
            CType(Me.XrCombo_param_08_1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.XrDate_param_08_2.Properties.VistaTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.XrDate_param_08_2.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.XrDate_param_08_1.Properties.VistaTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.XrDate_param_08_1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.DxErrorProvider1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LookUpEditRegion.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LookUpEditApptType.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            Me.ResumeLayout(False)
            Me.PerformLayout()

        End Sub
        Friend WithEvents LabelControl2 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents LookUpEditRegion As DevExpress.XtraEditors.LookUpEdit
        Friend WithEvents LabelControl3 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents LookUpEditApptType As DevExpress.XtraEditors.LookUpEdit
    End Class
End Namespace
