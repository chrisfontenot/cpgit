#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}
#End Region

Namespace Client.Housing
    Public Class Transactions
        Implements DebtPlus.Interfaces.Client.IClient

        ''' <summary>
        ''' Create an instance of our report
        ''' </summary>
        Public Sub New()
            MyBase.New()
            InitializeComponent()

            AddHandler BeforePrint, AddressOf Transactions_BeforePrint
            AddHandler XrLabel_ClientAddress.BeforePrint, AddressOf XrLabel_ClientAddress_BeforePrint
            AddHandler XrLabel_ClientName.BeforePrint, AddressOf XrLabel_ClientName_BeforePrint
            AddHandler XrLabel_ClientID.BeforePrint, AddressOf XrLabel_ClientID_BeforePrint

            '-- Remove the parameter values
            ClientID = -1
        End Sub

        Public Property ClientID As Int32 Implements DebtPlus.Interfaces.Client.IClient.ClientId
            Get
                Return Parameter_Client
            End Get
            Set(value As Int32)
                Parameter_Client = value
            End Set
        End Property

        Public Property Parameter_Client() As System.Int32
            Get
                Dim parm As DevExpress.XtraReports.Parameters.Parameter = FindParameter("ParameterClient")
                If parm Is Nothing Then Return -1
                Return Convert.ToInt32(parm.Value)
            End Get
            Set(ByVal value As System.Int32)
                SetParameter("ParameterClient", GetType(Int32), value, "Client ID", False)
            End Set
        End Property

        Public Overrides Sub SetReportParameter(ByVal Parameter As String, ByVal Value As Object)
            If Parameter = "Client" Then
                ClientID = Convert.ToInt32(Value)
            Else
                MyBase.SetReportParameter(Parameter, Value)
            End If
        End Sub

        Public Overrides Function NeedParameters() As Boolean
            Return MyBase.NeedParameters() OrElse Parameter_Client < 0
        End Function

        Public Overrides ReadOnly Property ReportTitle() As String
            Get
                Return "Housing Interviews By Client"
            End Get
        End Property

        Public Overrides Function RequestReportParameters() As System.Windows.Forms.DialogResult
            Dim Answer As System.Windows.Forms.DialogResult = DialogResult.OK
            If NeedParameters() Then
                Using frm As New DebtPlus.Reports.Template.Forms.ClientParametersForm()
                    Answer = frm.ShowDialog
                    Parameter_Client = frm.Parameter_Client
                End Using
            End If
            Return Answer
        End Function

        Dim ds As New System.Data.DataSet("ds")
        Private Sub Transactions_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs)
            Dim rpt As DevExpress.XtraReports.UI.XtraReport = CType(sender, DevExpress.XtraReports.UI.XtraReport)
            Dim sqlInfo As DebtPlus.LINQ.SQLInfoClass = DebtPlus.LINQ.SQLInfoClass.getDefault()

            Try
                Using cn As System.Data.SqlClient.SqlConnection = New System.Data.SqlClient.SqlConnection(sqlInfo.ConnectionString)
                    cn.Open()

                    '-- Read the client name and address for the address field
                    Using cmd As New System.Data.SqlClient.SqlCommand()
                        cmd.Connection = cn
                        cmd.CommandText = "SELECT name, addr1, addr2, addr3 FROM view_client_address WITH (NOLOCK) WHERE client = @client"
                        cmd.CommandType = System.Data.CommandType.Text
                        cmd.Parameters.Add("@client", System.Data.SqlDbType.Int).Value = rpt.Parameters("ParameterClient").Value
                        cmd.CommandTimeout = 0

                        Using da As New System.Data.SqlClient.SqlDataAdapter(cmd)
                            da.Fill(ds, "address")
                        End Using
                    End Using

                    Using cmd As New System.Data.SqlClient.SqlCommand()
                        cmd.Connection = cn
                        cmd.CommandText = "rpt_housing_interviews_by_client"
                        cmd.CommandType = System.Data.CommandType.StoredProcedure
                        cmd.Parameters.Add("@client", System.Data.SqlDbType.Int).Value = rpt.Parameters("ParameterClient").Value
                        cmd.CommandTimeout = 0

                        Using da As New System.Data.SqlClient.SqlDataAdapter(cmd)
                            da.Fill(ds, "transactions")

                            Dim tbl As System.Data.DataTable = ds.Tables("transactions")
                            rpt.DataSource = New System.Data.DataView(tbl, String.Empty, "ID, interview_date", System.Data.DataViewRowState.CurrentRows)
                        End Using
                    End Using
                End Using

            Catch ex As System.Data.SqlClient.SqlException
                Using gdr As New DebtPlus.Repository.GetDataResult()
                    gdr.HandleException(ex)
                    DebtPlus.UI.Common.ErrorHandling.HandleErrors(gdr, "Error reading report information")
                End Using
            End Try
        End Sub

        Private Sub XrLabel_ClientAddress_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs)
            Dim lbl As DevExpress.XtraReports.UI.XRLabel = CType(sender, DevExpress.XtraReports.UI.XRLabel)
            Dim rpt As DevExpress.XtraReports.UI.XtraReport = CType(lbl.Report, DevExpress.XtraReports.UI.XtraReport)
            Dim tbl As System.Data.DataTable = ds.Tables("address")
            If tbl.Rows.Count > 0 Then
                Dim row As System.Data.DataRow = tbl.Rows(0)

                Dim sb As New System.Text.StringBuilder
                If row("addr1") IsNot System.DBNull.Value Then
                    Dim txt As String = Convert.ToString(row("addr1")).Trim
                    If txt <> String.Empty Then
                        sb.Append(Environment.NewLine)
                        sb.Append(txt)
                    End If
                End If

                If row("addr2") IsNot System.DBNull.Value Then
                    Dim txt As String = Convert.ToString(row("addr2")).Trim
                    If txt <> String.Empty Then
                        sb.Append(Environment.NewLine)
                        sb.Append(txt)
                    End If
                End If

                If row("addr3") IsNot System.DBNull.Value Then
                    Dim txt As String = Convert.ToString(row("addr3")).Trim
                    If txt <> String.Empty Then
                        sb.Append(Environment.NewLine)
                        sb.Append(txt)
                    End If
                End If

                If sb.Length > 0 Then sb.Remove(0, 2)
                lbl.Text = sb.ToString()
            End If
        End Sub

        Private Sub XrLabel_ClientID_BeforePrint(sender As Object, e As System.Drawing.Printing.PrintEventArgs) Handles XrLabel_ClientID.BeforePrint
            Dim lbl As DevExpress.XtraReports.UI.XRLabel = CType(sender, DevExpress.XtraReports.UI.XRLabel)
            Dim rpt As DevExpress.XtraReports.UI.XtraReport = CType(lbl.Report, DevExpress.XtraReports.UI.XtraReport)
            lbl.Text = DebtPlus.Utils.Format.Client.FormatClientID(rpt.Parameters("ParameterClient").Value)
        End Sub

        Private Sub XrLabel_ClientName_BeforePrint(sender As Object, e As System.Drawing.Printing.PrintEventArgs)
            Dim lbl As DevExpress.XtraReports.UI.XRLabel = CType(sender, DevExpress.XtraReports.UI.XRLabel)
            Dim rpt As DevExpress.XtraReports.UI.XtraReport = CType(lbl.Report, DevExpress.XtraReports.UI.XtraReport)
            Dim tbl As System.Data.DataTable = ds.Tables("address")
            If tbl.Rows.Count > 0 Then
                Dim row As System.Data.DataRow = tbl.Rows(0)
                If row("name") IsNot System.DBNull.Value Then
                    lbl.Text = Convert.ToString(row("name")).Trim
                End If
            End If
        End Sub
    End Class
End Namespace
