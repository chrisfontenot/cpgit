Namespace Client.Housing
    <Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
    Partial Class Transactions
        Inherits DebtPlus.Reports.Template.TemplateXtraReportClass

        'XtraReport overrides dispose to clean up the component list.
        <System.Diagnostics.DebuggerNonUserCode()> _
        Protected Overrides Sub Dispose(ByVal disposing As Boolean)
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
            MyBase.Dispose(disposing)
        End Sub

        'Required by the Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Designer
        'It can be modified using the Designer.  
        'Do not modify it using the code editor.
        <System.Diagnostics.DebuggerStepThrough()> _
        Private Sub InitializeComponent()
            Dim XrSummary1 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary()
            Dim XrSummary2 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary()
            Dim XrSummary3 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary()
            Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Transactions))
            Me.XrPanel1 = New DevExpress.XtraReports.UI.XRPanel()
            Me.XrLabel_ClientAddress = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_ClientName = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_ClientID = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrPanel2 = New DevExpress.XtraReports.UI.XRPanel()
            Me.XrLabel7 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel8 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel1 = New DevExpress.XtraReports.UI.XRLabel()
            Me.GroupHeader1 = New DevExpress.XtraReports.UI.GroupHeaderBand()
            Me.XrLabel_hud_grant = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel6 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel5 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel4 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_result_description = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_result_date = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_interview_description = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_interview_date = New DevExpress.XtraReports.UI.XRLabel()
            Me.GroupFooter1 = New DevExpress.XtraReports.UI.GroupFooterBand()
            Me.XrLine1 = New DevExpress.XtraReports.UI.XRLine()
            Me.XrLabel_termination_description = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel9 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_termination_date = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel2 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_group_minutes = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_minutes = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_minutes_counselor = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_minutes_source = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_minutes_date = New DevExpress.XtraReports.UI.XRLabel()
            Me.ParameterClient = New DevExpress.XtraReports.Parameters.Parameter()
            CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
            '
            'PageHeader
            '
            Me.PageHeader.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrPanel2, Me.XrPanel1})
            Me.PageHeader.HeightF = 308.0!
            Me.PageHeader.Controls.SetChildIndex(Me.XrPanel1, 0)
            Me.PageHeader.Controls.SetChildIndex(Me.XrPanel2, 0)
            Me.PageHeader.Controls.SetChildIndex(Me.XrPanel_AgencyAddress, 0)
            Me.PageHeader.Controls.SetChildIndex(Me.XrLabel_Title, 0)
            Me.PageHeader.Controls.SetChildIndex(Me.XrLine_Title, 0)
            Me.PageHeader.Controls.SetChildIndex(Me.XrLabel_Subtitle, 0)
            Me.PageHeader.Controls.SetChildIndex(Me.XrPageInfo1, 0)
            '
            'XrLabel_Title
            '
            Me.XrLabel_Title.StylePriority.UseFont = False
            '
            'XrPageInfo_PageNumber
            '
            Me.XrPageInfo_PageNumber.StylePriority.UseTextAlignment = False
            '
            'XRLabel_Agency_Name
            '
            Me.XRLabel_Agency_Name.StylePriority.UseTextAlignment = False
            '
            'XrLabel_Agency_Address3
            '
            Me.XrLabel_Agency_Address3.StylePriority.UseFont = False
            Me.XrLabel_Agency_Address3.StylePriority.UseTextAlignment = False
            '
            'XrLabel_Agency_Address1
            '
            Me.XrLabel_Agency_Address1.StylePriority.UseFont = False
            Me.XrLabel_Agency_Address1.StylePriority.UseTextAlignment = False
            '
            'XrLabel_Agency_Phone
            '
            Me.XrLabel_Agency_Phone.StylePriority.UseFont = False
            Me.XrLabel_Agency_Phone.StylePriority.UseTextAlignment = False
            '
            'XrLabel_Agency_Address2
            '
            Me.XrLabel_Agency_Address2.StylePriority.UseFont = False
            Me.XrLabel_Agency_Address2.StylePriority.UseTextAlignment = False
            '
            'Detail
            '
            Me.Detail.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel_minutes_date, Me.XrLabel_minutes_source, Me.XrLabel_minutes_counselor, Me.XrLabel_minutes})
            Me.Detail.HeightF = 15.0!
            '
            'XrPanel1
            '
            Me.XrPanel1.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel_ClientAddress, Me.XrLabel_ClientName, Me.XrLabel_ClientID})
            Me.XrPanel1.LocationFloat = New DevExpress.Utils.PointFloat(8.0!, 133.0!)
            Me.XrPanel1.Name = "XrPanel1"
            Me.XrPanel1.SizeF = New System.Drawing.SizeF(300.0!, 132.0!)
            '
            'XrLabel_ClientAddress
            '
            Me.XrLabel_ClientAddress.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 30.0!)
            Me.XrLabel_ClientAddress.Multiline = True
            Me.XrLabel_ClientAddress.Name = "XrLabel_ClientAddress"
            Me.XrLabel_ClientAddress.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_ClientAddress.Scripts.OnBeforePrint = "XrLabel_ClientAddress_BeforePrint"
            Me.XrLabel_ClientAddress.SizeF = New System.Drawing.SizeF(300.0!, 67.0!)
            '
            'XrLabel_ClientName
            '
            Me.XrLabel_ClientName.CanGrow = False
            Me.XrLabel_ClientName.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel_ClientName.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 15.0!)
            Me.XrLabel_ClientName.Multiline = True
            Me.XrLabel_ClientName.Name = "XrLabel_ClientName"
            Me.XrLabel_ClientName.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_ClientName.Scripts.OnBeforePrint = "XrLabel_ClientName_BeforePrint"
            Me.XrLabel_ClientName.SizeF = New System.Drawing.SizeF(300.0!, 15.0!)
            Me.XrLabel_ClientName.StylePriority.UseFont = False
            '
            'XrLabel_ClientID
            '
            Me.XrLabel_ClientID.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel_ClientID.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
            Me.XrLabel_ClientID.Multiline = True
            Me.XrLabel_ClientID.Name = "XrLabel_ClientID"
            Me.XrLabel_ClientID.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_ClientID.Scripts.OnBeforePrint = "XrLabel_ClientID_BeforePrint"
            Me.XrLabel_ClientID.SizeF = New System.Drawing.SizeF(300.0!, 15.0!)
            Me.XrLabel_ClientID.StylePriority.UseFont = False
            '
            'XrPanel2
            '
            Me.XrPanel2.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel7, Me.XrLabel8, Me.XrLabel1})
            Me.XrPanel2.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 283.0!)
            Me.XrPanel2.Name = "XrPanel2"
            Me.XrPanel2.SizeF = New System.Drawing.SizeF(800.0!, 20.0!)
            Me.XrPanel2.StyleName = "XrControlStyle_HeaderPannel"
            '
            'XrLabel7
            '
            Me.XrLabel7.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
            Me.XrLabel7.Name = "XrLabel7"
            Me.XrLabel7.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel7.SizeF = New System.Drawing.SizeF(170.12!, 20.0!)
            Me.XrLabel7.StylePriority.UseTextAlignment = False
            Me.XrLabel7.Text = "GRANT ID"
            Me.XrLabel7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            '
            'XrLabel8
            '
            Me.XrLabel8.LocationFloat = New DevExpress.Utils.PointFloat(506.67!, 0.0!)
            Me.XrLabel8.Name = "XrLabel8"
            Me.XrLabel8.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel8.SizeF = New System.Drawing.SizeF(251.33!, 18.0!)
            Me.XrLabel8.StylePriority.UseTextAlignment = False
            Me.XrLabel8.Text = "RESULTS"
            Me.XrLabel8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            '
            'XrLabel1
            '
            Me.XrLabel1.LocationFloat = New DevExpress.Utils.PointFloat(170.12!, 1.0!)
            Me.XrLabel1.Name = "XrLabel1"
            Me.XrLabel1.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel1.SizeF = New System.Drawing.SizeF(170.88!, 18.0!)
            Me.XrLabel1.StylePriority.UseTextAlignment = False
            Me.XrLabel1.Text = "PURPOSE OF VISIT"
            Me.XrLabel1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            '
            'GroupHeader1
            '
            Me.GroupHeader1.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel_hud_grant, Me.XrLabel6, Me.XrLabel5, Me.XrLabel4, Me.XrLabel_result_description, Me.XrLabel_result_date, Me.XrLabel_interview_description, Me.XrLabel_interview_date})
            Me.GroupHeader1.GroupFields.AddRange(New DevExpress.XtraReports.UI.GroupField() {New DevExpress.XtraReports.UI.GroupField("ID", DevExpress.XtraReports.UI.XRColumnSortOrder.Ascending)})
            Me.GroupHeader1.HeightF = 47.0!
            Me.GroupHeader1.Name = "GroupHeader1"
            Me.GroupHeader1.StylePriority.UseTextAlignment = False
            Me.GroupHeader1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            '
            'XrLabel_hud_grant
            '
            Me.XrLabel_hud_grant.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "hud_grant_description")})
            Me.XrLabel_hud_grant.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
            Me.XrLabel_hud_grant.Name = "XrLabel_hud_grant"
            Me.XrLabel_hud_grant.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 10, 0, 0, 100.0!)
            Me.XrLabel_hud_grant.SizeF = New System.Drawing.SizeF(170.12!, 15.0!)
            Me.XrLabel_hud_grant.StylePriority.UsePadding = False
            '
            'XrLabel6
            '
            Me.XrLabel6.LocationFloat = New DevExpress.Utils.PointFloat(350.0!, 25.0!)
            Me.XrLabel6.Name = "XrLabel6"
            Me.XrLabel6.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel6.SizeF = New System.Drawing.SizeF(67.0!, 15.0!)
            Me.XrLabel6.StylePriority.UseTextAlignment = False
            Me.XrLabel6.Text = "MINS"
            Me.XrLabel6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel5
            '
            Me.XrLabel5.LocationFloat = New DevExpress.Utils.PointFloat(66.0!, 25.0!)
            Me.XrLabel5.Name = "XrLabel5"
            Me.XrLabel5.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel5.SizeF = New System.Drawing.SizeF(275.0!, 15.0!)
            Me.XrLabel5.Text = "TRANSACTION"
            '
            'XrLabel4
            '
            Me.XrLabel4.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 25.0!)
            Me.XrLabel4.Name = "XrLabel4"
            Me.XrLabel4.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel4.SizeF = New System.Drawing.SizeF(58.0!, 15.0!)
            Me.XrLabel4.StylePriority.UseTextAlignment = False
            Me.XrLabel4.Text = "SOURCE"
            Me.XrLabel4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
            '
            'XrLabel_result_description
            '
            Me.XrLabel_result_description.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "result_description")})
            Me.XrLabel_result_description.LocationFloat = New DevExpress.Utils.PointFloat(589.67!, 0.0!)
            Me.XrLabel_result_description.Name = "XrLabel_result_description"
            Me.XrLabel_result_description.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_result_description.SizeF = New System.Drawing.SizeF(200.33!, 15.0!)
            Me.XrLabel_result_description.WordWrap = False
            '
            'XrLabel_result_date
            '
            Me.XrLabel_result_date.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "result_date", "{0:d}")})
            Me.XrLabel_result_date.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel_result_date.LocationFloat = New DevExpress.Utils.PointFloat(506.67!, 0.0!)
            Me.XrLabel_result_date.Name = "XrLabel_result_date"
            Me.XrLabel_result_date.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_result_date.SizeF = New System.Drawing.SizeF(83.0!, 15.0!)
            Me.XrLabel_result_date.StylePriority.UseFont = False
            Me.XrLabel_result_date.StylePriority.UseTextAlignment = False
            Me.XrLabel_result_date.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            '
            'XrLabel_interview_description
            '
            Me.XrLabel_interview_description.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "interview_description")})
            Me.XrLabel_interview_description.LocationFloat = New DevExpress.Utils.PointFloat(253.12!, 0.0!)
            Me.XrLabel_interview_description.Name = "XrLabel_interview_description"
            Me.XrLabel_interview_description.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_interview_description.SizeF = New System.Drawing.SizeF(241.0!, 15.0!)
            Me.XrLabel_interview_description.WordWrap = False
            '
            'XrLabel_interview_date
            '
            Me.XrLabel_interview_date.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "interview_date", "{0:d}")})
            Me.XrLabel_interview_date.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel_interview_date.LocationFloat = New DevExpress.Utils.PointFloat(170.12!, 0.0!)
            Me.XrLabel_interview_date.Name = "XrLabel_interview_date"
            Me.XrLabel_interview_date.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_interview_date.SizeF = New System.Drawing.SizeF(83.0!, 15.0!)
            Me.XrLabel_interview_date.StylePriority.UseFont = False
            Me.XrLabel_interview_date.StylePriority.UseTextAlignment = False
            Me.XrLabel_interview_date.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            '
            'GroupFooter1
            '
            Me.GroupFooter1.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLine1, Me.XrLabel_termination_description, Me.XrLabel9, Me.XrLabel_termination_date, Me.XrLabel2, Me.XrLabel_group_minutes})
            Me.GroupFooter1.HeightF = 36.0!
            Me.GroupFooter1.Name = "GroupFooter1"
            '
            'XrLine1
            '
            Me.XrLine1.BorderWidth = 1
            Me.XrLine1.ForeColor = System.Drawing.Color.Teal
            Me.XrLine1.LineWidth = 2
            Me.XrLine1.LocationFloat = New DevExpress.Utils.PointFloat(142.0!, 0.0!)
            Me.XrLine1.Name = "XrLine1"
            Me.XrLine1.SizeF = New System.Drawing.SizeF(276.0!, 17.0!)
            Me.XrLine1.StylePriority.UseBorderWidth = False
            Me.XrLine1.StylePriority.UseForeColor = False
            '
            'XrLabel_termination_description
            '
            Me.XrLabel_termination_description.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "termination_description")})
            Me.XrLabel_termination_description.Font = New System.Drawing.Font("Times New Roman", 9.75!)
            Me.XrLabel_termination_description.LocationFloat = New DevExpress.Utils.PointFloat(617.0!, 17.0!)
            Me.XrLabel_termination_description.Name = "XrLabel_termination_description"
            Me.XrLabel_termination_description.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_termination_description.SizeF = New System.Drawing.SizeF(183.0!, 15.0!)
            Me.XrLabel_termination_description.StylePriority.UseFont = False
            Me.XrLabel_termination_description.StylePriority.UseTextAlignment = False
            XrSummary1.FormatString = "{0:n0}"
            Me.XrLabel_termination_description.Summary = XrSummary1
            Me.XrLabel_termination_description.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            Me.XrLabel_termination_description.WordWrap = False
            '
            'XrLabel9
            '
            Me.XrLabel9.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel9.LocationFloat = New DevExpress.Utils.PointFloat(433.0!, 17.0!)
            Me.XrLabel9.Name = "XrLabel9"
            Me.XrLabel9.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel9.SizeF = New System.Drawing.SizeF(83.0!, 15.0!)
            Me.XrLabel9.StylePriority.UseFont = False
            Me.XrLabel9.Text = "Terminated:"
            '
            'XrLabel_termination_date
            '
            Me.XrLabel_termination_date.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "termination_date", "{0:d}")})
            Me.XrLabel_termination_date.Font = New System.Drawing.Font("Times New Roman", 9.75!)
            Me.XrLabel_termination_date.LocationFloat = New DevExpress.Utils.PointFloat(525.0!, 17.0!)
            Me.XrLabel_termination_date.Name = "XrLabel_termination_date"
            Me.XrLabel_termination_date.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_termination_date.SizeF = New System.Drawing.SizeF(82.0!, 15.0!)
            Me.XrLabel_termination_date.StylePriority.UseFont = False
            Me.XrLabel_termination_date.StylePriority.UseTextAlignment = False
            XrSummary2.FormatString = "{0:d}"
            Me.XrLabel_termination_date.Summary = XrSummary2
            Me.XrLabel_termination_date.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            '
            'XrLabel2
            '
            Me.XrLabel2.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel2.LocationFloat = New DevExpress.Utils.PointFloat(142.0!, 17.0!)
            Me.XrLabel2.Name = "XrLabel2"
            Me.XrLabel2.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel2.SizeF = New System.Drawing.SizeF(133.0!, 15.0!)
            Me.XrLabel2.StylePriority.UseFont = False
            Me.XrLabel2.Text = "TOTAL MINUTES"
            '
            'XrLabel_group_minutes
            '
            Me.XrLabel_group_minutes.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "minutes")})
            Me.XrLabel_group_minutes.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel_group_minutes.LocationFloat = New DevExpress.Utils.PointFloat(335.0!, 17.0!)
            Me.XrLabel_group_minutes.Name = "XrLabel_group_minutes"
            Me.XrLabel_group_minutes.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_group_minutes.SizeF = New System.Drawing.SizeF(82.0!, 15.0!)
            Me.XrLabel_group_minutes.StylePriority.UseFont = False
            Me.XrLabel_group_minutes.StylePriority.UseTextAlignment = False
            XrSummary3.FormatString = "{0:n0}"
            XrSummary3.Running = DevExpress.XtraReports.UI.SummaryRunning.Group
            Me.XrLabel_group_minutes.Summary = XrSummary3
            Me.XrLabel_group_minutes.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel_minutes
            '
            Me.XrLabel_minutes.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "minutes", "{0:n0}")})
            Me.XrLabel_minutes.LocationFloat = New DevExpress.Utils.PointFloat(350.0!, 0.0!)
            Me.XrLabel_minutes.Name = "XrLabel_minutes"
            Me.XrLabel_minutes.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_minutes.SizeF = New System.Drawing.SizeF(68.0!, 15.0!)
            Me.XrLabel_minutes.StylePriority.UseTextAlignment = False
            Me.XrLabel_minutes.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel_minutes_counselor
            '
            Me.XrLabel_minutes_counselor.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "minutes_counselor")})
            Me.XrLabel_minutes_counselor.LocationFloat = New DevExpress.Utils.PointFloat(142.0!, 0.0!)
            Me.XrLabel_minutes_counselor.Name = "XrLabel_minutes_counselor"
            Me.XrLabel_minutes_counselor.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_minutes_counselor.SizeF = New System.Drawing.SizeF(200.0!, 15.0!)
            '
            'XrLabel_minutes_source
            '
            Me.XrLabel_minutes_source.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "minutes_source")})
            Me.XrLabel_minutes_source.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
            Me.XrLabel_minutes_source.Name = "XrLabel_minutes_source"
            Me.XrLabel_minutes_source.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_minutes_source.SizeF = New System.Drawing.SizeF(58.0!, 15.0!)
            Me.XrLabel_minutes_source.StylePriority.UseTextAlignment = False
            Me.XrLabel_minutes_source.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
            '
            'XrLabel_minutes_date
            '
            Me.XrLabel_minutes_date.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "minutes_date", "{0:d}")})
            Me.XrLabel_minutes_date.LocationFloat = New DevExpress.Utils.PointFloat(67.0!, 0.0!)
            Me.XrLabel_minutes_date.Name = "XrLabel_minutes_date"
            Me.XrLabel_minutes_date.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_minutes_date.SizeF = New System.Drawing.SizeF(67.0!, 15.0!)
            '
            'ParameterClient
            '
            Me.ParameterClient.Name = "ParameterClient"
            Me.ParameterClient.Type = GetType(Integer)
            Me.ParameterClient.Visible = False
            '
            'Transactions
            '
            Me.Bands.AddRange(New DevExpress.XtraReports.UI.Band() {Me.TopMarginBand1, Me.Detail, Me.BottomMarginBand1, Me.PageHeader, Me.PageFooter, Me.GroupHeader1, Me.GroupFooter1})
            Me.Parameters.AddRange(New DevExpress.XtraReports.Parameters.Parameter() {Me.ParameterClient})
            Me.RequestParameters = False
            Me.ScriptLanguage = DevExpress.XtraReports.ScriptLanguage.VisualBasic
            Me.ScriptReferencesString = resources.GetString("$this.ScriptReferencesString")
            Me.Scripts.OnBeforePrint = "Housing_Transactions_BeforePrint"
            Me.ScriptsSource = resources.GetString("$this.ScriptsSource")
            Me.StyleSheet.AddRange(New DevExpress.XtraReports.UI.XRControlStyle() {Me.XrControlStyle_Header, Me.XrControlStyle_Totals, Me.XrControlStyle_HeaderPannel, Me.XrControlStyle_GroupHeader})
            Me.Version = "11.2"
            Me.Controls.SetChildIndex(Me.GroupFooter1, 0)
            Me.Controls.SetChildIndex(Me.GroupHeader1, 0)
            Me.Controls.SetChildIndex(Me.PageFooter, 0)
            Me.Controls.SetChildIndex(Me.PageHeader, 0)
            Me.Controls.SetChildIndex(Me.BottomMarginBand1, 0)
            Me.Controls.SetChildIndex(Me.Detail, 0)
            Me.Controls.SetChildIndex(Me.TopMarginBand1, 0)
            CType(Me, System.ComponentModel.ISupportInitialize).EndInit()

        End Sub
        Friend WithEvents XrPanel1 As DevExpress.XtraReports.UI.XRPanel
        Friend WithEvents XrLabel_ClientID As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_ClientAddress As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_ClientName As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrPanel2 As DevExpress.XtraReports.UI.XRPanel
        Friend WithEvents XrLabel8 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel1 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents GroupHeader1 As DevExpress.XtraReports.UI.GroupHeaderBand
        Friend WithEvents XrLabel_result_description As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_result_date As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_interview_description As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_interview_date As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_minutes_source As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_minutes_counselor As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_minutes As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel6 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel5 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel4 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents GroupFooter1 As DevExpress.XtraReports.UI.GroupFooterBand
        Friend WithEvents XrLabel_minutes_date As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_termination_description As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel9 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_termination_date As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel2 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_group_minutes As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLine1 As DevExpress.XtraReports.UI.XRLine
        Friend WithEvents ParameterClient As DevExpress.XtraReports.Parameters.Parameter
        Friend WithEvents XrLabel_hud_grant As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel7 As DevExpress.XtraReports.UI.XRLabel
    End Class
End Namespace
