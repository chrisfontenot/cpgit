#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}
#End Region

Option Compare Binary
Option Explicit On
Option Strict On

Imports DebtPlus.UI.Common

Namespace Trust.DailyDeposits.ByBatch
    Public Class TopLevelReport
        Inherits DebtPlus.Reports.Template.DatedTemplateXtraReportClass

        ''' <summary>
        ''' Create an instance of the class
        ''' </summary>
        Public Sub New()
            MyBase.New()
            InitializeComponent()
            AddHandler BeforePrint, AddressOf Report_BeforePrint
            AddHandler XrLabel_amount.PreviewClick, AddressOf Field_PreviewClick
            AddHandler XrLabel_date_created.PreviewClick, AddressOf Field_PreviewClick
        End Sub

        ''' <summary>
        ''' Title associated with the report
        ''' </summary>
        Public Overrides ReadOnly Property ReportTitle() As String
            Get
                Return "Deposit Register"
            End Get
        End Property

        Dim ds As New System.Data.DataSet("ds")

        ''' <summary>
        ''' Bind the dataset to the report
        ''' </summary>
        Private Sub Report_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs)
            Dim rpt As DevExpress.XtraReports.UI.XtraReport = CType(sender, DevExpress.XtraReports.UI.XtraReport)
            Dim cn As SqlClient.SqlConnection = New System.Data.SqlClient.SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
            Using cm As New CursorManager()
                Try
                    cn.Open()

                    Using cmd As SqlClient.SqlCommand = New System.Data.SqlClient.SqlCommand()
                        With cmd
                            .Connection = cn
                            .CommandText = "SELECT convert(datetime, convert(varchar(10), tr.date_created, 101)) as date_created, sum(tr.amount) as amount FROM registers_trust tr WITH (NOLOCK) WHERE tr.date_created >= @From_date AND tr.date_created < @To_date AND tr.tran_type = 'DP' GROUP BY convert(datetime, convert(varchar(10), tr.date_created, 101))"
                            .Parameters.Add("@from_date", SqlDbType.DateTime).Value = rpt.Parameters("ParameterFromDate").Value
                            .Parameters.Add("@to_date", SqlDbType.DateTime).Value = Convert.ToDateTime(rpt.Parameters("ParameterToDate").Value).Date.AddDays(1)
                            .CommandTimeout = 0
                        End With

                        Using da As New SqlClient.SqlDataAdapter(cmd)
                            da.Fill(ds, "registers_trust")
                        End Using
                    End Using
                    rpt.DataSource = ds.Tables("registers_trust").DefaultView

                Catch ex As SqlClient.SqlException
                    Using gdr As New Repository.GetDataResult()
                        gdr.HandleException(ex)
                        DebtPlus.UI.Common.ErrorHandling.HandleErrors(gdr, "Error reading report information")
                    End Using

                Finally
                    If cn IsNot Nothing Then cn.Dispose()
                End Try
            End Using
        End Sub

        Private Sub Field_PreviewClick(ByVal sender As Object, ByVal e As DevExpress.XtraReports.UI.PreviewMouseEventArgs)
            Dim obj As Object = e.Brick.Value
            If obj IsNot Nothing AndAlso obj IsNot System.DBNull.Value Then
                Dim date_created As DateTime = Convert.ToDateTime(obj)
                Dim thrd As New System.Threading.Thread(New System.Threading.ParameterizedThreadStart(AddressOf SubReportProcedure))
                With thrd
                    .IsBackground = True
                    .SetApartmentState(Threading.ApartmentState.STA)
                    .Start(date_created)
                End With
            End If
        End Sub

        Private Shared Sub SubReportProcedure(ByVal obj As Object)
            Dim date_created As DateTime = CType(obj, DateTime)

            Using frm As New DepositBatchListing()
                frm.Parameters("ParameterItemDate").Value = date_created
                frm.DisplayPreviewDialog()
            End Using
        End Sub
    End Class
End Namespace
