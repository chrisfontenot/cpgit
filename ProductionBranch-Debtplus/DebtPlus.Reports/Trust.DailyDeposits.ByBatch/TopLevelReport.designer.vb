﻿Namespace Trust.DailyDeposits.ByBatch
    Partial Class TopLevelReport

        'Component overrides dispose to clean up the component list.
        Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
            If disposing Then
                If components IsNot Nothing Then
                    components.Dispose()
                End If
            End If
            MyBase.Dispose(disposing)
        End Sub

        'Required by the Component Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Component Designer
        'It can be modified using the Component Designer.
        'Do not modify it using the code editor.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
            Dim XrSummary1 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary
            Me.XrPanel1 = New DevExpress.XtraReports.UI.XRPanel
            Me.XrLabel8 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel6 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_date_created = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_amount = New DevExpress.XtraReports.UI.XRLabel
            Me.ReportFooter = New DevExpress.XtraReports.UI.ReportFooterBand
            Me.XrLabel5 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_total_amount = New DevExpress.XtraReports.UI.XRLabel
            CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
            '
            'Detail
            '
            Me.Detail.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel_amount, Me.XrLabel_date_created})
            Me.Detail.HeightF = 18.00002!
            '
            'PageHeader
            '
            Me.PageHeader.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrPanel1})
            Me.PageHeader.HeightF = 157.0!
            Me.PageHeader.Controls.SetChildIndex(Me.XrPanel1, 0)
            Me.PageHeader.Controls.SetChildIndex(Me.XrPanel_AgencyAddress, 0)
            Me.PageHeader.Controls.SetChildIndex(Me.XrLabel_Title, 0)
            Me.PageHeader.Controls.SetChildIndex(Me.XrLine_Title, 0)
            Me.PageHeader.Controls.SetChildIndex(Me.XrLabel_Subtitle, 0)
            Me.PageHeader.Controls.SetChildIndex(Me.XrPageInfo1, 0)
            '
            'XrLabel_Title
            '
            Me.XrLabel_Title.StylePriority.UseFont = False
            '
            'XrPageInfo_PageNumber
            '
            Me.XrPageInfo_PageNumber.StylePriority.UseTextAlignment = False
            '
            'XRLabel_Agency_Name
            '
            Me.XRLabel_Agency_Name.StylePriority.UseTextAlignment = False
            '
            'XrLabel_Agency_Address3
            '
            Me.XrLabel_Agency_Address3.StylePriority.UseFont = False
            Me.XrLabel_Agency_Address3.StylePriority.UseTextAlignment = False
            '
            'XrLabel_Agency_Address1
            '
            Me.XrLabel_Agency_Address1.StylePriority.UseFont = False
            Me.XrLabel_Agency_Address1.StylePriority.UseTextAlignment = False
            '
            'XrLabel_Agency_Phone
            '
            Me.XrLabel_Agency_Phone.StylePriority.UseFont = False
            Me.XrLabel_Agency_Phone.StylePriority.UseTextAlignment = False
            '
            'XrLabel_Agency_Address2
            '
            Me.XrLabel_Agency_Address2.StylePriority.UseFont = False
            Me.XrLabel_Agency_Address2.StylePriority.UseTextAlignment = False
            '
            'XrPanel1
            '
            Me.XrPanel1.BackColor = System.Drawing.Color.Teal
            Me.XrPanel1.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel8, Me.XrLabel6})
            Me.XrPanel1.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 125.0!)
            Me.XrPanel1.Name = "XrPanel1"
            Me.XrPanel1.SizeF = New System.Drawing.SizeF(750.0!, 17.0!)
            Me.XrPanel1.StyleName = "XrControlStyle_HeaderPannel"
            '
            'XrLabel8
            '
            Me.XrLabel8.LocationFloat = New DevExpress.Utils.PointFloat(425.0!, 1.0!)
            Me.XrLabel8.Name = "XrLabel8"
            Me.XrLabel8.SizeF = New System.Drawing.SizeF(100.0!, 15.0!)
            Me.XrLabel8.StylePriority.UseTextAlignment = False
            Me.XrLabel8.Text = "AMOUNT"
            Me.XrLabel8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel6
            '
            Me.XrLabel6.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 1.0!)
            Me.XrLabel6.Name = "XrLabel6"
            Me.XrLabel6.SizeF = New System.Drawing.SizeF(148.62!, 15.0!)
            Me.XrLabel6.StylePriority.UseTextAlignment = False
            Me.XrLabel6.Text = "CREATED"
            Me.XrLabel6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel_date_created
            '
            Me.XrLabel_date_created.CanGrow = False
            Me.XrLabel_date_created.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "date_created", "{0:d}"), New DevExpress.XtraReports.UI.XRBinding("Tag", Nothing, "date_created")})
            Me.XrLabel_date_created.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 1.000023!)
            Me.XrLabel_date_created.Name = "XrLabel_date_created"
            Me.XrLabel_date_created.SizeF = New System.Drawing.SizeF(148.625!, 17.0!)
            Me.XrLabel_date_created.StylePriority.UseTextAlignment = False
            Me.XrLabel_date_created.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel_amount
            '
            Me.XrLabel_amount.CanGrow = False
            Me.XrLabel_amount.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "amount", "{0:c}"), New DevExpress.XtraReports.UI.XRBinding("Tag", Nothing, "date_created")})
            Me.XrLabel_amount.LocationFloat = New DevExpress.Utils.PointFloat(368.7501!, 0.0!)
            Me.XrLabel_amount.Name = "XrLabel_amount"
            Me.XrLabel_amount.SizeF = New System.Drawing.SizeF(156.2499!, 17.00001!)
            Me.XrLabel_amount.StylePriority.UseTextAlignment = False
            Me.XrLabel_amount.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'ReportFooter
            '
            Me.ReportFooter.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel5, Me.XrLabel_total_amount})
            Me.ReportFooter.HeightF = 36.79168!
            Me.ReportFooter.Name = "ReportFooter"
            Me.ReportFooter.StyleName = "XrControlStyle_Totals"
            '
            'XrLabel5
            '
            Me.XrLabel5.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 10.00001!)
            Me.XrLabel5.Name = "XrLabel5"
            Me.XrLabel5.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel5.SizeF = New System.Drawing.SizeF(296.875!, 17.0!)
            Me.XrLabel5.StylePriority.UseTextAlignment = False
            Me.XrLabel5.Text = "TOTAL"
            Me.XrLabel5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
            '
            'XrLabel_total_amount
            '
            Me.XrLabel_total_amount.CanGrow = False
            Me.XrLabel_total_amount.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "amount")})
            Me.XrLabel_total_amount.LocationFloat = New DevExpress.Utils.PointFloat(368.7501!, 10.00001!)
            Me.XrLabel_total_amount.Name = "XrLabel_total_amount"
            Me.XrLabel_total_amount.SizeF = New System.Drawing.SizeF(156.25!, 17.0!)
            Me.XrLabel_total_amount.StylePriority.UseTextAlignment = False
            XrSummary1.FormatString = "{0:c}"
            XrSummary1.Running = DevExpress.XtraReports.UI.SummaryRunning.Report
            Me.XrLabel_total_amount.Summary = XrSummary1
            Me.XrLabel_total_amount.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'TopLevelReport
            '
            Me.Bands.AddRange(New DevExpress.XtraReports.UI.Band() {Me.Detail, Me.PageHeader, Me.PageFooter, Me.ReportFooter, Me.TopMarginBand1, Me.BottomMarginBand1})
            Me.ScriptLanguage = DevExpress.XtraReports.ScriptLanguage.VisualBasic
            Me.StyleSheet.AddRange(New DevExpress.XtraReports.UI.XRControlStyle() {Me.XrControlStyle_Header, Me.XrControlStyle_Totals, Me.XrControlStyle_HeaderPannel, Me.XrControlStyle_GroupHeader})
            Me.Version = "10.2"
            Me.Controls.SetChildIndex(Me.BottomMarginBand1, 0)
            Me.Controls.SetChildIndex(Me.TopMarginBand1, 0)
            Me.Controls.SetChildIndex(Me.ReportFooter, 0)
            Me.Controls.SetChildIndex(Me.PageFooter, 0)
            Me.Controls.SetChildIndex(Me.PageHeader, 0)
            Me.Controls.SetChildIndex(Me.Detail, 0)
            CType(Me, System.ComponentModel.ISupportInitialize).EndInit()

        End Sub
        Friend WithEvents XrPanel1 As DevExpress.XtraReports.UI.XRPanel
        Friend WithEvents XrLabel6 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel8 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents ReportFooter As DevExpress.XtraReports.UI.ReportFooterBand
        Friend WithEvents XrLabel_date_created As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_amount As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel5 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_total_amount As DevExpress.XtraReports.UI.XRLabel
    End Class

End Namespace