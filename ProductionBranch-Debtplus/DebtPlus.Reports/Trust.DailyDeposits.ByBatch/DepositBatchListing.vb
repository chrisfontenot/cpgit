#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}
#End Region

Imports DebtPlus.Interfaces.Reports

Namespace Trust.DailyDeposits.ByBatch
    Public Class DepositBatchListing
        Inherits DebtPlus.Reports.Template.TemplateXtraReportClass

        Public Sub New()
            MyBase.new()
            InitializeComponent()
            AddHandler Me.BeforePrint, AddressOf DepositBatchListing_BeforePrint

            ' Handlers for click events on the detail fields
            AddHandler XrLabel_date_created.PreviewClick, AddressOf Field_PreviewClick
            AddHandler XrLabel_created_by.PreviewClick, AddressOf Field_PreviewClick
            AddHandler XrLabel_sequence_number.PreviewClick, AddressOf Field_PreviewClick
            AddHandler XrLabel_amount.PreviewClick, AddressOf Field_PreviewClick

            ReportFilter.IsEnabled = True
        End Sub

        Public Overrides ReadOnly Property ReportSubTitle() As String
            Get
                Return String.Format("Report for deposits made on {0:d}", CType(Parameters("ParameterItemDate").Value, DateTime))
            End Get
        End Property

        Public Overrides ReadOnly Property ReportTitle() As String
            Get
                Return "Deposit Register"
            End Get
        End Property

        Private ds As New System.Data.DataSet("ds")
        Private Sub DepositBatchListing_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs)
            Const TableName As String = "registers_trust"
            Dim tbl As DataTable = ds.Tables(TableName)

            If tbl Is Nothing Then
                Using cmd As SqlClient.SqlCommand = New System.Data.SqlClient.SqlCommand()
                    With cmd
                        .Connection = New System.Data.SqlClient.SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
                        .CommandText = "SELECT tr.bank, b.description as bank_name, tr.trust_register, tr.date_created, dbo.format_counselor_name(tr.created_by) as created_by, tr.amount, tr.sequence_number FROM registers_trust tr WITH (NOLOCK) LEFT OUTER JOIN banks b WITH (NOLOCK) ON tr.bank = b.bank WHERE tr.tran_type = 'DP' and tr.date_created >= @from_date and tr.date_created < @to_date"
                        .Parameters.Add("@from_date", SqlDbType.DateTime).Value = CType(ParameterItemDate.Value, DateTime).Date
                        .Parameters.Add("@to_date", SqlDbType.DateTime).Value = CType(ParameterItemDate.Value, DateTime).Date.AddDays(1)
                    End With

                    Using da As New SqlClient.SqlDataAdapter(cmd)
                        da.Fill(ds, TableName)
                        tbl = ds.Tables(TableName)
                    End Using
                End Using
            End If

            If tbl IsNot Nothing Then
                DataSource = New System.Data.DataView(tbl, ReportFilter.ViewFilter, "bank, date_created, trust_register", DataViewRowState.CurrentRows)
            End If
        End Sub

        Private Sub Field_PreviewClick(ByVal sender As Object, ByVal e As DevExpress.XtraReports.UI.PreviewMouseEventArgs)
            Dim obj As Object = e.Brick.Value
            If obj IsNot Nothing AndAlso obj IsNot System.DBNull.Value Then
                Dim trust_register As Int32 = Convert.ToInt32(obj)
                Dim thrd As New System.Threading.Thread(New System.Threading.ParameterizedThreadStart(AddressOf SubReportProcedure))
                With thrd
                    .IsBackground = True
                    .SetApartmentState(Threading.ApartmentState.STA)
                    .Start(trust_register)
                End With
            End If
        End Sub

        Private Shared Sub SubReportProcedure(ByVal obj As Object)
            Dim trust_register As Int32 = CType(obj, Int32)
            Using rpt As DebtPlus.Reports.Deposits.Batch.ByTrustRegister.DepositBatchReport = New DebtPlus.Reports.Deposits.Batch.ByTrustRegister.DepositBatchReport()
                rpt.Parameter_DepositBatchID = trust_register
                rpt.Parameter_SortString = "client"
                rpt.DisplayPreviewDialog()
            End Using

        End Sub
    End Class

End Namespace