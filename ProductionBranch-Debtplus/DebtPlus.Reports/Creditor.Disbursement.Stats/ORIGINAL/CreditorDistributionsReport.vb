#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}
#End Region

Option Compare Binary
Option Explicit On 
Option Strict On

Imports DevExpress.XtraReports.UI

Namespace Creditor.Disbursement.Stats
    Public Class CreditorDistributionsReport

        ''' <summary>
        ''' Create an instance of our report
        ''' </summary>
        Public Sub New()
            MyBase.New()
            InitializeComponent()
            AddHandler Me.BeforePrint, AddressOf CreditorDistributionsReport_BeforePrint
        End Sub

        Public Overrides ReadOnly Property ReportTitle() As String
            Get
                Return "Creditor Disbursement Stats"
            End Get
        End Property

        Dim ds As New System.Data.DataSet("ds")
        Private Sub CreditorDistributionsReport_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs)
            Const TableName As String = "view_creditor_totals"
            Dim Rpt As DevExpress.XtraReports.UI.XtraReport = CType(sender, DevExpress.XtraReports.UI.XtraReport)
            Dim sqlInfo As DebtPlus.LINQ.SQLInfoClass = DebtPlus.LINQ.SQLInfoClass.getDefault()

            Dim tbl As System.Data.DataTable = ds.Tables(TableName)
            If tbl Is Nothing Then
                Try
                    Using cn As System.Data.SqlClient.SqlConnection = New System.Data.SqlClient.SqlConnection(sqlInfo.ConnectionString)
                        cn.Open()

                        '-- First, read the disbursement information
                        Using cmd As New System.Data.SqlClient.SqlCommand()
                            cmd.Connection = cn
                            cmd.CommandText = "SELECT [creditor], [creditor_name], [distrib_mtd], [distrib_ytd], [contrib_mtd_billed], [contrib_ytd_billed], [contrib_mtd_received], [contrib_ytd_received], [type], [creditor_id] FROM [view_creditor_totals]"
                            cmd.CommandType = System.Data.CommandType.Text
                            cmd.CommandTimeout = 0

                            Using da As New System.Data.SqlClient.SqlDataAdapter(cmd)
                                da.Fill(ds, TableName)
                                tbl = ds.Tables(TableName)
                            End Using
                        End Using
                    End Using

                Catch ex As System.Data.SqlClient.SqlException
                    Using gdr As New DebtPlus.Repository.GetDataResult()
                        gdr.HandleException(ex)
                        DebtPlus.UI.Common.ErrorHandling.HandleErrors(gdr, "Error reading database information")
                    End Using
                End Try
            End If

            If tbl IsNot Nothing Then
                Rpt.DataSource = New System.Data.DataView(tbl, String.Empty, "type, creditor_id", System.Data.DataViewRowState.CurrentRows)
            End If
        End Sub
    End Class
End Namespace
