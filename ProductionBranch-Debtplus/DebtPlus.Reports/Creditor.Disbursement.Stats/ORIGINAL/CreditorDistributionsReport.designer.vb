Namespace Creditor.Disbursement.Stats
    Partial Class CreditorDistributionsReport
        Inherits DebtPlus.Reports.Template.TemplateXtraReportClass

        'Component overrides dispose to clean up the component list.
        Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
            If disposing Then
                If components IsNot Nothing Then
                    components.Dispose()
                End If
                ds.Dispose()
            End If
            MyBase.Dispose(disposing)
        End Sub

        'Required by the Designer
        Private components As System.ComponentModel.IContainer

        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
            Dim XrSummary1 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary()
            Dim XrSummary2 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary()
            Dim XrSummary3 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary()
            Dim XrSummary4 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary()
            Dim XrSummary5 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary()
            Dim XrSummary6 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary()
            Dim XrSummary7 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary()
            Dim XrSummary8 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary()
            Dim XrSummary9 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary()
            Dim XrSummary10 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary()
            Dim XrSummary11 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary()
            Dim XrSummary12 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary()
            Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(CreditorDistributionsReport))
            Me.XrLabel_creditor = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrPanel1 = New DevExpress.XtraReports.UI.XRPanel()
            Me.XrLabel10 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel15 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel3 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel8 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel7 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel5 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel2 = New DevExpress.XtraReports.UI.XRLabel()
            Me.GroupHeader1 = New DevExpress.XtraReports.UI.GroupHeaderBand()
            Me.GroupFooter1 = New DevExpress.XtraReports.UI.GroupFooterBand()
            Me.XrLabel24 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_group_contrib_ytd_received = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_group_contrib_mtd_received = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_group_contrib_ytd_billed = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_group_distrib_mtd = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_group_distrib_ytd = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_group_contrib_mtd_billed = New DevExpress.XtraReports.UI.XRLabel()
            Me.ReportFooter = New DevExpress.XtraReports.UI.ReportFooterBand()
            Me.XrLabel_total_contrib_ytd_received = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_total_contrib_mtd_received = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_total_contrib_ytd_billed = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_total_distrib_mtd = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_total_distrib_ytd = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_total_contrib_mtd_billed = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel1 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel4 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel6 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel9 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_distrib_mtd = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_distrib_ytd = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_contrib_mtd_billed = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_contrib_ytd_billed = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_contrib_mtd_received = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_contrib_ytd_received = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_creditor_name = New DevExpress.XtraReports.UI.XRLabel()
            CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
            '
            'PageHeader
            '
            Me.PageHeader.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel9, Me.XrLabel6, Me.XrLabel4, Me.XrPanel1})
            Me.PageHeader.HeightF = 160.0!
            Me.PageHeader.Controls.SetChildIndex(Me.XrPanel1, 0)
            Me.PageHeader.Controls.SetChildIndex(Me.XrPanel_AgencyAddress, 0)
            Me.PageHeader.Controls.SetChildIndex(Me.XrLabel_Title, 0)
            Me.PageHeader.Controls.SetChildIndex(Me.XrLine_Title, 0)
            Me.PageHeader.Controls.SetChildIndex(Me.XrLabel_Subtitle, 0)
            Me.PageHeader.Controls.SetChildIndex(Me.XrPageInfo1, 0)
            Me.PageHeader.Controls.SetChildIndex(Me.XrLabel4, 0)
            Me.PageHeader.Controls.SetChildIndex(Me.XrLabel6, 0)
            Me.PageHeader.Controls.SetChildIndex(Me.XrLabel9, 0)
            '
            'XrLabel_Title
            '
            Me.XrLabel_Title.StylePriority.UseFont = False
            '
            'XrPageInfo_PageNumber
            '
            Me.XrPageInfo_PageNumber.StylePriority.UseTextAlignment = False
            '
            'XRLabel_Agency_Name
            '
            Me.XRLabel_Agency_Name.StylePriority.UseTextAlignment = False
            '
            'XrLabel_Agency_Address3
            '
            Me.XrLabel_Agency_Address3.StylePriority.UseFont = False
            Me.XrLabel_Agency_Address3.StylePriority.UseTextAlignment = False
            '
            'XrLabel_Agency_Address1
            '
            Me.XrLabel_Agency_Address1.StylePriority.UseFont = False
            Me.XrLabel_Agency_Address1.StylePriority.UseTextAlignment = False
            '
            'XrLabel_Agency_Phone
            '
            Me.XrLabel_Agency_Phone.StylePriority.UseFont = False
            Me.XrLabel_Agency_Phone.StylePriority.UseTextAlignment = False
            '
            'XrLabel_Agency_Address2
            '
            Me.XrLabel_Agency_Address2.StylePriority.UseFont = False
            Me.XrLabel_Agency_Address2.StylePriority.UseTextAlignment = False
            '
            'Detail
            '
            Me.Detail.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel_creditor_name, Me.XrLabel_contrib_ytd_billed, Me.XrLabel_contrib_mtd_received, Me.XrLabel_contrib_ytd_received, Me.XrLabel_distrib_mtd, Me.XrLabel_distrib_ytd, Me.XrLabel_contrib_mtd_billed, Me.XrLabel_creditor})
            Me.Detail.HeightF = 16.0!
            '
            'XrLabel_creditor
            '
            Me.XrLabel_creditor.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "creditor")})
            Me.XrLabel_creditor.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
            Me.XrLabel_creditor.Name = "XrLabel_creditor"
            Me.XrLabel_creditor.SizeF = New System.Drawing.SizeF(64.24999!, 14.99999!)
            Me.XrLabel_creditor.StylePriority.UseTextAlignment = False
            Me.XrLabel_creditor.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            Me.XrLabel_creditor.WordWrap = False
            '
            'XrPanel1
            '
            Me.XrPanel1.BackColor = System.Drawing.Color.Teal
            Me.XrPanel1.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel10, Me.XrLabel15, Me.XrLabel3, Me.XrLabel8, Me.XrLabel7, Me.XrLabel5, Me.XrLabel2})
            Me.XrPanel1.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 134.0!)
            Me.XrPanel1.Name = "XrPanel1"
            Me.XrPanel1.SizeF = New System.Drawing.SizeF(799.0!, 17.0!)
            Me.XrPanel1.StyleName = "XrControlStyle_HeaderPannel"
            '
            'XrLabel10
            '
            Me.XrLabel10.LocationFloat = New DevExpress.Utils.PointFloat(200.0!, 1.0!)
            Me.XrLabel10.Name = "XrLabel10"
            Me.XrLabel10.SizeF = New System.Drawing.SizeF(83.0!, 15.0!)
            Me.XrLabel10.Text = "MTD"
            '
            'XrLabel15
            '
            Me.XrLabel15.LocationFloat = New DevExpress.Utils.PointFloat(500.0!, 1.0!)
            Me.XrLabel15.Name = "XrLabel15"
            Me.XrLabel15.SizeF = New System.Drawing.SizeF(91.0!, 15.0!)
            Me.XrLabel15.Text = "YTD"
            '
            'XrLabel3
            '
            Me.XrLabel3.LocationFloat = New DevExpress.Utils.PointFloat(291.0!, 1.0!)
            Me.XrLabel3.Name = "XrLabel3"
            Me.XrLabel3.SizeF = New System.Drawing.SizeF(100.0!, 15.0!)
            Me.XrLabel3.Text = "YTD"
            '
            'XrLabel8
            '
            Me.XrLabel8.LocationFloat = New DevExpress.Utils.PointFloat(591.0!, 1.0!)
            Me.XrLabel8.Name = "XrLabel8"
            Me.XrLabel8.SizeF = New System.Drawing.SizeF(91.0!, 15.0!)
            Me.XrLabel8.Text = "MTD"
            '
            'XrLabel7
            '
            Me.XrLabel7.LocationFloat = New DevExpress.Utils.PointFloat(404.0!, 1.0!)
            Me.XrLabel7.Name = "XrLabel7"
            Me.XrLabel7.SizeF = New System.Drawing.SizeF(87.0!, 15.0!)
            Me.XrLabel7.Text = "MTD"
            '
            'XrLabel5
            '
            Me.XrLabel5.LocationFloat = New DevExpress.Utils.PointFloat(692.0!, 1.0!)
            Me.XrLabel5.Name = "XrLabel5"
            Me.XrLabel5.SizeF = New System.Drawing.SizeF(98.0!, 15.0!)
            Me.XrLabel5.Text = "YTD"
            '
            'XrLabel2
            '
            Me.XrLabel2.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 1.0!)
            Me.XrLabel2.Name = "XrLabel2"
            Me.XrLabel2.SizeF = New System.Drawing.SizeF(122.9167!, 15.0!)
            Me.XrLabel2.Text = "CREDITOR"
            '
            'GroupHeader1
            '
            Me.GroupHeader1.GroupFields.AddRange(New DevExpress.XtraReports.UI.GroupField() {New DevExpress.XtraReports.UI.GroupField("type", DevExpress.XtraReports.UI.XRColumnSortOrder.Ascending)})
            Me.GroupHeader1.HeightF = 0.0!
            Me.GroupHeader1.Name = "GroupHeader1"
            '
            'GroupFooter1
            '
            Me.GroupFooter1.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel24, Me.XrLabel_group_contrib_ytd_received, Me.XrLabel_group_contrib_mtd_received, Me.XrLabel_group_contrib_ytd_billed, Me.XrLabel_group_distrib_mtd, Me.XrLabel_group_distrib_ytd, Me.XrLabel_group_contrib_mtd_billed})
            Me.GroupFooter1.HeightF = 39.0!
            Me.GroupFooter1.Name = "GroupFooter1"
            Me.GroupFooter1.StyleName = "XrControlStyle_Totals"
            '
            'XrLabel24
            '
            Me.XrLabel24.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
            Me.XrLabel24.Name = "XrLabel24"
            Me.XrLabel24.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel24.SizeF = New System.Drawing.SizeF(100.0!, 17.00001!)
            Me.XrLabel24.Text = "SUB-TOTALS"
            '
            'XrLabel_group_contrib_ytd_received
            '
            Me.XrLabel_group_contrib_ytd_received.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "contrib_ytd_received")})
            Me.XrLabel_group_contrib_ytd_received.LocationFloat = New DevExpress.Utils.PointFloat(692.0!, 0.0!)
            Me.XrLabel_group_contrib_ytd_received.Name = "XrLabel_group_contrib_ytd_received"
            Me.XrLabel_group_contrib_ytd_received.SizeF = New System.Drawing.SizeF(98.0!, 15.0!)
            XrSummary1.FormatString = "{0:c}"
            XrSummary1.Running = DevExpress.XtraReports.UI.SummaryRunning.Group
            Me.XrLabel_group_contrib_ytd_received.Summary = XrSummary1
            Me.XrLabel_group_contrib_ytd_received.WordWrap = False
            '
            'XrLabel_group_contrib_mtd_received
            '
            Me.XrLabel_group_contrib_mtd_received.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "contrib_mtd_received")})
            Me.XrLabel_group_contrib_mtd_received.LocationFloat = New DevExpress.Utils.PointFloat(591.0!, 0.0!)
            Me.XrLabel_group_contrib_mtd_received.Name = "XrLabel_group_contrib_mtd_received"
            Me.XrLabel_group_contrib_mtd_received.SizeF = New System.Drawing.SizeF(91.0!, 15.0!)
            XrSummary2.FormatString = "{0:c}"
            XrSummary2.Running = DevExpress.XtraReports.UI.SummaryRunning.Group
            Me.XrLabel_group_contrib_mtd_received.Summary = XrSummary2
            Me.XrLabel_group_contrib_mtd_received.WordWrap = False
            '
            'XrLabel_group_contrib_ytd_billed
            '
            Me.XrLabel_group_contrib_ytd_billed.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "contrib_ytd_billed")})
            Me.XrLabel_group_contrib_ytd_billed.LocationFloat = New DevExpress.Utils.PointFloat(500.0!, 0.0!)
            Me.XrLabel_group_contrib_ytd_billed.Name = "XrLabel_group_contrib_ytd_billed"
            Me.XrLabel_group_contrib_ytd_billed.SizeF = New System.Drawing.SizeF(91.0!, 15.0!)
            XrSummary3.FormatString = "{0:c}"
            XrSummary3.Running = DevExpress.XtraReports.UI.SummaryRunning.Group
            Me.XrLabel_group_contrib_ytd_billed.Summary = XrSummary3
            Me.XrLabel_group_contrib_ytd_billed.WordWrap = False
            '
            'XrLabel_group_distrib_mtd
            '
            Me.XrLabel_group_distrib_mtd.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "distrib_mtd")})
            Me.XrLabel_group_distrib_mtd.LocationFloat = New DevExpress.Utils.PointFloat(157.2917!, 0.0!)
            Me.XrLabel_group_distrib_mtd.Name = "XrLabel_group_distrib_mtd"
            Me.XrLabel_group_distrib_mtd.SizeF = New System.Drawing.SizeF(125.7083!, 14.99999!)
            XrSummary4.FormatString = "{0:c}"
            XrSummary4.Running = DevExpress.XtraReports.UI.SummaryRunning.Group
            Me.XrLabel_group_distrib_mtd.Summary = XrSummary4
            Me.XrLabel_group_distrib_mtd.WordWrap = False
            '
            'XrLabel_group_distrib_ytd
            '
            Me.XrLabel_group_distrib_ytd.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "distrib_ytd")})
            Me.XrLabel_group_distrib_ytd.LocationFloat = New DevExpress.Utils.PointFloat(291.0!, 0.0!)
            Me.XrLabel_group_distrib_ytd.Name = "XrLabel_group_distrib_ytd"
            Me.XrLabel_group_distrib_ytd.SizeF = New System.Drawing.SizeF(100.0!, 15.0!)
            XrSummary5.FormatString = "{0:c}"
            XrSummary5.Running = DevExpress.XtraReports.UI.SummaryRunning.Group
            Me.XrLabel_group_distrib_ytd.Summary = XrSummary5
            Me.XrLabel_group_distrib_ytd.WordWrap = False
            '
            'XrLabel_group_contrib_mtd_billed
            '
            Me.XrLabel_group_contrib_mtd_billed.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "contrib_mtd_billed")})
            Me.XrLabel_group_contrib_mtd_billed.LocationFloat = New DevExpress.Utils.PointFloat(404.0!, 0.0!)
            Me.XrLabel_group_contrib_mtd_billed.Name = "XrLabel_group_contrib_mtd_billed"
            Me.XrLabel_group_contrib_mtd_billed.SizeF = New System.Drawing.SizeF(87.0!, 15.0!)
            XrSummary6.FormatString = "{0:c}"
            XrSummary6.Running = DevExpress.XtraReports.UI.SummaryRunning.Group
            Me.XrLabel_group_contrib_mtd_billed.Summary = XrSummary6
            Me.XrLabel_group_contrib_mtd_billed.WordWrap = False
            '
            'ReportFooter
            '
            Me.ReportFooter.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel_total_contrib_ytd_received, Me.XrLabel_total_contrib_mtd_received, Me.XrLabel_total_contrib_ytd_billed, Me.XrLabel_total_distrib_mtd, Me.XrLabel_total_distrib_ytd, Me.XrLabel_total_contrib_mtd_billed, Me.XrLabel1})
            Me.ReportFooter.HeightF = 41.0!
            Me.ReportFooter.Name = "ReportFooter"
            Me.ReportFooter.StyleName = "XrControlStyle_Totals"
            '
            'XrLabel_total_contrib_ytd_received
            '
            Me.XrLabel_total_contrib_ytd_received.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "contrib_ytd_received")})
            Me.XrLabel_total_contrib_ytd_received.LocationFloat = New DevExpress.Utils.PointFloat(692.0!, 11.99999!)
            Me.XrLabel_total_contrib_ytd_received.Name = "XrLabel_total_contrib_ytd_received"
            Me.XrLabel_total_contrib_ytd_received.SizeF = New System.Drawing.SizeF(98.0!, 15.0!)
            XrSummary7.FormatString = "{0:c}"
            XrSummary7.Running = DevExpress.XtraReports.UI.SummaryRunning.Report
            Me.XrLabel_total_contrib_ytd_received.Summary = XrSummary7
            Me.XrLabel_total_contrib_ytd_received.WordWrap = False
            '
            'XrLabel_total_contrib_mtd_received
            '
            Me.XrLabel_total_contrib_mtd_received.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "contrib_mtd_received")})
            Me.XrLabel_total_contrib_mtd_received.LocationFloat = New DevExpress.Utils.PointFloat(591.0!, 11.99999!)
            Me.XrLabel_total_contrib_mtd_received.Name = "XrLabel_total_contrib_mtd_received"
            Me.XrLabel_total_contrib_mtd_received.SizeF = New System.Drawing.SizeF(91.0!, 15.0!)
            XrSummary8.FormatString = "{0:c}"
            XrSummary8.Running = DevExpress.XtraReports.UI.SummaryRunning.Report
            Me.XrLabel_total_contrib_mtd_received.Summary = XrSummary8
            Me.XrLabel_total_contrib_mtd_received.WordWrap = False
            '
            'XrLabel_total_contrib_ytd_billed
            '
            Me.XrLabel_total_contrib_ytd_billed.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "contrib_ytd_billed")})
            Me.XrLabel_total_contrib_ytd_billed.LocationFloat = New DevExpress.Utils.PointFloat(500.0!, 11.99999!)
            Me.XrLabel_total_contrib_ytd_billed.Name = "XrLabel_total_contrib_ytd_billed"
            Me.XrLabel_total_contrib_ytd_billed.SizeF = New System.Drawing.SizeF(91.0!, 15.0!)
            XrSummary9.FormatString = "{0:c}"
            XrSummary9.Running = DevExpress.XtraReports.UI.SummaryRunning.Report
            Me.XrLabel_total_contrib_ytd_billed.Summary = XrSummary9
            Me.XrLabel_total_contrib_ytd_billed.WordWrap = False
            '
            'XrLabel_total_distrib_mtd
            '
            Me.XrLabel_total_distrib_mtd.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "distrib_mtd")})
            Me.XrLabel_total_distrib_mtd.LocationFloat = New DevExpress.Utils.PointFloat(157.2917!, 11.99999!)
            Me.XrLabel_total_distrib_mtd.Name = "XrLabel_total_distrib_mtd"
            Me.XrLabel_total_distrib_mtd.SizeF = New System.Drawing.SizeF(125.7083!, 15.0!)
            XrSummary10.FormatString = "{0:c}"
            XrSummary10.Running = DevExpress.XtraReports.UI.SummaryRunning.Report
            Me.XrLabel_total_distrib_mtd.Summary = XrSummary10
            Me.XrLabel_total_distrib_mtd.WordWrap = False
            '
            'XrLabel_total_distrib_ytd
            '
            Me.XrLabel_total_distrib_ytd.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "distrib_ytd")})
            Me.XrLabel_total_distrib_ytd.LocationFloat = New DevExpress.Utils.PointFloat(291.0!, 11.99999!)
            Me.XrLabel_total_distrib_ytd.Name = "XrLabel_total_distrib_ytd"
            Me.XrLabel_total_distrib_ytd.SizeF = New System.Drawing.SizeF(100.0!, 15.0!)
            XrSummary11.FormatString = "{0:c}"
            XrSummary11.Running = DevExpress.XtraReports.UI.SummaryRunning.Report
            Me.XrLabel_total_distrib_ytd.Summary = XrSummary11
            Me.XrLabel_total_distrib_ytd.WordWrap = False
            '
            'XrLabel_total_contrib_mtd_billed
            '
            Me.XrLabel_total_contrib_mtd_billed.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "contrib_mtd_billed")})
            Me.XrLabel_total_contrib_mtd_billed.LocationFloat = New DevExpress.Utils.PointFloat(404.0!, 11.99999!)
            Me.XrLabel_total_contrib_mtd_billed.Name = "XrLabel_total_contrib_mtd_billed"
            Me.XrLabel_total_contrib_mtd_billed.SizeF = New System.Drawing.SizeF(87.0!, 15.0!)
            XrSummary12.FormatString = "{0:c}"
            XrSummary12.Running = DevExpress.XtraReports.UI.SummaryRunning.Report
            Me.XrLabel_total_contrib_mtd_billed.Summary = XrSummary12
            Me.XrLabel_total_contrib_mtd_billed.WordWrap = False
            '
            'XrLabel1
            '
            Me.XrLabel1.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 10.0!)
            Me.XrLabel1.Name = "XrLabel1"
            Me.XrLabel1.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel1.SizeF = New System.Drawing.SizeF(100.0!, 17.0!)
            Me.XrLabel1.Text = "TOTALS"
            '
            'XrLabel4
            '
            Me.XrLabel4.LocationFloat = New DevExpress.Utils.PointFloat(200.0!, 119.0!)
            Me.XrLabel4.Name = "XrLabel4"
            Me.XrLabel4.SizeF = New System.Drawing.SizeF(191.0!, 15.0!)
            Me.XrLabel4.StyleName = "XrControlStyle_HeaderPannel"
            Me.XrLabel4.StylePriority.UseTextAlignment = False
            Me.XrLabel4.Text = "Distributions"
            Me.XrLabel4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter
            '
            'XrLabel6
            '
            Me.XrLabel6.LocationFloat = New DevExpress.Utils.PointFloat(404.0!, 119.0!)
            Me.XrLabel6.Name = "XrLabel6"
            Me.XrLabel6.SizeF = New System.Drawing.SizeF(187.0!, 15.0!)
            Me.XrLabel6.StyleName = "XrControlStyle_HeaderPannel"
            Me.XrLabel6.StylePriority.UseTextAlignment = False
            Me.XrLabel6.Text = "Billed"
            Me.XrLabel6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter
            '
            'XrLabel9
            '
            Me.XrLabel9.LocationFloat = New DevExpress.Utils.PointFloat(608.0!, 119.0!)
            Me.XrLabel9.Name = "XrLabel9"
            Me.XrLabel9.SizeF = New System.Drawing.SizeF(191.0!, 15.0!)
            Me.XrLabel9.StyleName = "XrControlStyle_HeaderPannel"
            Me.XrLabel9.StylePriority.UseTextAlignment = False
            Me.XrLabel9.Text = "Contributions Received"
            Me.XrLabel9.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter
            '
            'XrLabel_distrib_mtd
            '
            Me.XrLabel_distrib_mtd.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "distrib_mtd", "{0:c}")})
            Me.XrLabel_distrib_mtd.LocationFloat = New DevExpress.Utils.PointFloat(200.0!, 0.0!)
            Me.XrLabel_distrib_mtd.Name = "XrLabel_distrib_mtd"
            Me.XrLabel_distrib_mtd.SizeF = New System.Drawing.SizeF(83.0!, 15.0!)
            Me.XrLabel_distrib_mtd.StylePriority.UseTextAlignment = False
            Me.XrLabel_distrib_mtd.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrLabel_distrib_mtd.WordWrap = False
            '
            'XrLabel_distrib_ytd
            '
            Me.XrLabel_distrib_ytd.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "distrib_ytd", "{0:c}")})
            Me.XrLabel_distrib_ytd.LocationFloat = New DevExpress.Utils.PointFloat(291.0!, 0.0!)
            Me.XrLabel_distrib_ytd.Name = "XrLabel_distrib_ytd"
            Me.XrLabel_distrib_ytd.SizeF = New System.Drawing.SizeF(100.0!, 15.0!)
            Me.XrLabel_distrib_ytd.StylePriority.UseTextAlignment = False
            Me.XrLabel_distrib_ytd.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrLabel_distrib_ytd.WordWrap = False
            '
            'XrLabel_contrib_mtd_billed
            '
            Me.XrLabel_contrib_mtd_billed.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "contrib_mtd_billed", "{0:c}")})
            Me.XrLabel_contrib_mtd_billed.LocationFloat = New DevExpress.Utils.PointFloat(404.0!, 0.0!)
            Me.XrLabel_contrib_mtd_billed.Name = "XrLabel_contrib_mtd_billed"
            Me.XrLabel_contrib_mtd_billed.SizeF = New System.Drawing.SizeF(87.0!, 15.0!)
            Me.XrLabel_contrib_mtd_billed.StylePriority.UseTextAlignment = False
            Me.XrLabel_contrib_mtd_billed.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrLabel_contrib_mtd_billed.WordWrap = False
            '
            'XrLabel_contrib_ytd_billed
            '
            Me.XrLabel_contrib_ytd_billed.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "contrib_ytd_billed", "{0:c}")})
            Me.XrLabel_contrib_ytd_billed.LocationFloat = New DevExpress.Utils.PointFloat(500.0!, 0.0!)
            Me.XrLabel_contrib_ytd_billed.Name = "XrLabel_contrib_ytd_billed"
            Me.XrLabel_contrib_ytd_billed.SizeF = New System.Drawing.SizeF(91.0!, 15.0!)
            Me.XrLabel_contrib_ytd_billed.StylePriority.UseTextAlignment = False
            Me.XrLabel_contrib_ytd_billed.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrLabel_contrib_ytd_billed.WordWrap = False
            '
            'XrLabel_contrib_mtd_received
            '
            Me.XrLabel_contrib_mtd_received.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "contrib_mtd_received", "{0:c}")})
            Me.XrLabel_contrib_mtd_received.LocationFloat = New DevExpress.Utils.PointFloat(591.0!, 0.0!)
            Me.XrLabel_contrib_mtd_received.Name = "XrLabel_contrib_mtd_received"
            Me.XrLabel_contrib_mtd_received.SizeF = New System.Drawing.SizeF(91.0!, 15.0!)
            Me.XrLabel_contrib_mtd_received.StylePriority.UseTextAlignment = False
            Me.XrLabel_contrib_mtd_received.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrLabel_contrib_mtd_received.WordWrap = False
            '
            'XrLabel_contrib_ytd_received
            '
            Me.XrLabel_contrib_ytd_received.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "contrib_ytd_received", "{0:c}")})
            Me.XrLabel_contrib_ytd_received.LocationFloat = New DevExpress.Utils.PointFloat(692.0!, 0.0!)
            Me.XrLabel_contrib_ytd_received.Name = "XrLabel_contrib_ytd_received"
            Me.XrLabel_contrib_ytd_received.SizeF = New System.Drawing.SizeF(98.0!, 15.0!)
            Me.XrLabel_contrib_ytd_received.StylePriority.UseTextAlignment = False
            Me.XrLabel_contrib_ytd_received.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrLabel_contrib_ytd_received.WordWrap = False
            '
            'XrLabel_creditor_name
            '
            Me.XrLabel_creditor_name.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "creditor_name")})
            Me.XrLabel_creditor_name.LocationFloat = New DevExpress.Utils.PointFloat(64.24999!, 0.0!)
            Me.XrLabel_creditor_name.Name = "XrLabel_creditor_name"
            Me.XrLabel_creditor_name.SizeF = New System.Drawing.SizeF(135.75!, 14.99999!)
            Me.XrLabel_creditor_name.StylePriority.UseTextAlignment = False
            Me.XrLabel_creditor_name.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            Me.XrLabel_creditor_name.WordWrap = False
            '
            'CreditorDistributionsReport
            '
            Me.Bands.AddRange(New DevExpress.XtraReports.UI.Band() {Me.Detail, Me.PageHeader, Me.PageFooter, Me.GroupHeader1, Me.GroupFooter1, Me.ReportFooter, Me.TopMarginBand1, Me.BottomMarginBand1})
            Me.RequestParameters = False
            Me.ScriptLanguage = DevExpress.XtraReports.ScriptLanguage.VisualBasic
            Me.ScriptReferencesString = resources.GetString("$this.ScriptReferencesString")
            Me.Scripts.OnBeforePrint = "CreditorDistributionsReport_BeforePrint"
            Me.ScriptsSource = resources.GetString("$this.ScriptsSource")
            Me.StyleSheet.AddRange(New DevExpress.XtraReports.UI.XRControlStyle() {Me.XrControlStyle_Header, Me.XrControlStyle_Totals, Me.XrControlStyle_HeaderPannel, Me.XrControlStyle_GroupHeader})
            Me.Version = "11.2"
            Me.Controls.SetChildIndex(Me.BottomMarginBand1, 0)
            Me.Controls.SetChildIndex(Me.TopMarginBand1, 0)
            Me.Controls.SetChildIndex(Me.ReportFooter, 0)
            Me.Controls.SetChildIndex(Me.GroupFooter1, 0)
            Me.Controls.SetChildIndex(Me.GroupHeader1, 0)
            Me.Controls.SetChildIndex(Me.PageFooter, 0)
            Me.Controls.SetChildIndex(Me.PageHeader, 0)
            Me.Controls.SetChildIndex(Me.Detail, 0)
            CType(Me, System.ComponentModel.ISupportInitialize).EndInit()

        End Sub
        Friend WithEvents XrLabel_creditor As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrPanel1 As DevExpress.XtraReports.UI.XRPanel
        Friend WithEvents XrLabel2 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel5 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel7 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel8 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents GroupHeader1 As DevExpress.XtraReports.UI.GroupHeaderBand
        Friend WithEvents GroupFooter1 As DevExpress.XtraReports.UI.GroupFooterBand
        Friend WithEvents ReportFooter As DevExpress.XtraReports.UI.ReportFooterBand
        Friend WithEvents XrLabel1 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel3 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel9 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel6 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel4 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel15 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel10 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_contrib_ytd_billed As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_contrib_mtd_received As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_contrib_ytd_received As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_distrib_mtd As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_distrib_ytd As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_contrib_mtd_billed As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_creditor_name As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_group_contrib_ytd_received As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_group_contrib_mtd_received As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_group_contrib_ytd_billed As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_group_distrib_mtd As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_group_distrib_ytd As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_group_contrib_mtd_billed As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_total_contrib_ytd_received As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_total_contrib_mtd_received As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_total_contrib_ytd_billed As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_total_distrib_mtd As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_total_distrib_ytd As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_total_contrib_mtd_billed As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel24 As DevExpress.XtraReports.UI.XRLabel
    End Class
End Namespace
