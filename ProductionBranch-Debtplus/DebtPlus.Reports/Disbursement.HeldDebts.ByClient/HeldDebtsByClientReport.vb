#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}

#End Region

Imports System.ComponentModel
Imports DebtPlus.Reports.Template

Imports System.Drawing.Printing
Imports DebtPlus.Reports.Template.Forms
Imports System.Data.SqlClient

Namespace Disbursement.HeldDebts.ByClient
    Public Class HeldDebtsByClientReport
        Inherits TemplateXtraReportClass

        Public Sub New(ByVal Container As IContainer)
            MyClass.New()
            Container.Add(Me)
        End Sub

        Public Sub New()
            MyBase.New()
            InitializeComponent()
            AddHandler Me.BeforePrint, AddressOf Report_BeforePrint
            AddHandler XrLabel_client.BeforePrint, AddressOf XrLabel_client_BeforePrint
        End Sub

        Public Overrides ReadOnly Property ReportTitle() As String
            Get
                Return "Held Debts By Client"
            End Get
        End Property

        Private CounselorDefined As Boolean = False
        Private privateCounselor As Object = DBNull.Value

        Public Property Parameter_Counselor() As Object
            Get
                Return privateCounselor
            End Get
            Set(ByVal value As Object)
                privateCounselor = value
                CounselorDefined = True
            End Set
        End Property

        Public Overrides Function NeedParameters() As Boolean
            Return Not CounselorDefined
        End Function

        Public Overrides Function RequestReportParameters() As DialogResult
            Dim answer As DialogResult = DialogResult.OK
            If NeedParameters() Then
                Using frm As New CounselorParametersForm(True)
                    answer = frm.ShowDialog()
                    Parameter_Counselor = frm.Parameter_Counselor
                End Using
            End If
            Return answer
        End Function

        Public Overrides ReadOnly Property ReportSubTitle() As String
            Get
                Dim value As Int32 = -1
                If Parameter_Counselor IsNot Nothing AndAlso Parameter_Counselor IsNot DBNull.Value Then
                    Try
                        value = Convert.ToInt32(Parameter_Counselor)
                    Catch ex As Exception
                        value = - 1
                    End Try
                End If
                If value <= 0 Then Return "This report is for all counselors"
                Return String.Format("This report is for counselor #{0:f0}", value)
            End Get
        End Property

        Dim ds As New DataSet("ds")

        Private Sub Report_BeforePrint(ByVal sender As Object, ByVal e As PrintEventArgs)
            Dim tbl As DataTable = ds.Tables("rpt_HeldDebtsByClient")

            If tbl Is Nothing Then
                Dim cn As SqlConnection = New System.Data.SqlClient.SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
                Try
                    cn.Open()
                    Using cmd As SqlCommand = New System.Data.SqlClient.SqlCommand()
                        With cmd
                            .Connection = cn
                            .CommandText = "rpt_HeldDebtsByClient"
                            .CommandType = CommandType.StoredProcedure
                            SqlCommandBuilder.DeriveParameters(cmd)
                            .Parameters(1).Value = Parameter_Counselor
                            .CommandTimeout = 0
                        End With

                        Using da As New SqlDataAdapter(cmd)
                            da.Fill(ds, "rpt_HeldDebtsByClient")
                            tbl = ds.Tables("rpt_HeldDebtsByClient")
                        End Using
                    End Using

                Catch ex As SqlException
                    DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error reading report information")

                Finally
                    If cn IsNot Nothing Then cn.Dispose()
                End Try
            End If

            If tbl IsNot Nothing Then
                Dim vue As New DataView(tbl, String.Empty, "client, account_number", DataViewRowState.CurrentRows)
                DataSource = vue

                With XrLabel_account_number
                    .DataBindings.Clear()
                    .DataBindings.Add("Text", vue, "account_number")
                End With

                With XrLabel_disbursement_factor
                    .DataBindings.Clear()
                    .DataBindings.Add("Text", vue, "disbursement_factor", "{0:c}")
                End With

                With XrLabel_total_disbursement_factor
                    .DataBindings.Clear()
                    .DataBindings.Add("Text", vue, "disbursement_factor")
                End With

                With XrLabel_sched_payment
                    .DataBindings.Clear()
                    .DataBindings.Add("Text", vue, "sched_payment", "{0:c}")
                End With

                With XrLabel_total_sched_payment
                    .DataBindings.Clear()
                    .DataBindings.Add("Text", vue, "sched_payment")
                End With

                With XrLabel_balance
                    .DataBindings.Clear()
                    .DataBindings.Add("Text", vue, "balance", "{0:c}")
                End With

                With XrLabel_total_balance
                    .DataBindings.Clear()
                    .DataBindings.Add("Text", vue, "balance")
                End With

                With XrLabel_creditor
                    .DataBindings.Clear()
                    .DataBindings.Add("Text", vue, "creditor")
                End With
            End If
        End Sub

        Private Sub XrLabel_client_BeforePrint(ByVal sender As Object, ByVal e As PrintEventArgs)
            XrLabel_client.Text = String.Format("{0:0000000}", Convert.ToInt32(GetCurrentColumnValue("client"))) + " " + DebtPlus.Utils.Nulls.DStr(GetCurrentColumnValue("name"))
        End Sub
    End Class

End Namespace
