#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}
#End Region

Namespace Client.Disclosures
    Public Class DisclosureReport
        Inherits DebtPlus.Reports.Template.BaseXtraReportClass
        Implements DebtPlus.Interfaces.Client.IClient

        ''' <summary>
        ''' Create an instance of our report
        ''' </summary>
        Public Sub New()
            MyClass.New(False)
        End Sub

        Public Sub New(ByVal ShowWaitingDialog As Boolean)
            MyClass.New(ShowWaitingDialog, String.Empty)
        End Sub

        Public Sub New(ByVal ShowWaitingDialog As Boolean, ByVal ReportDefinition As String)
            MyBase.New(ShowWaitingDialog)
            InitializeComponent()

            AddHandler XrRichTextDetail.BeforePrint, AddressOf XrRichTextDetail_BeforePrint

            '-- Set the report definition text
            Dim DocumentName As String = String.Empty
            Dim Fields() As String = ReportDefinition.Split(";"c)
            If Fields.GetUpperBound(0) < 1 Then
                Throw New ApplicationException("The entry in the reports table is not valid for this report. It needs the name of the RTF file.")
            End If

            DocumentName = Fields(1).Trim
            If DocumentName = String.Empty Then
                Throw New ApplicationException("The entry in the reports table is not valid for this report. The report name can not be empty.")
            End If

            '-- Set the report information
            Parameters("ParameterReportText").Value = DocumentName
            Parameters("ParameterClient").Value = -1

            '-- Ensure that the DevExpress parameter dialog is not used on the report.
            RequestParameters = False
            For Each parm As DevExpress.XtraReports.Parameters.Parameter In Parameters
                parm.Visible = False
            Next

            '-- Set the script references to the current (running) values.
            ScriptReferences = DebtPlus.Utils.AssemblyTools.GetAllSiblingAssemblies()

            '-- Copy the script references to the subreports as well
            For Each Band As DevExpress.XtraReports.UI.Band In Bands
                For Each ctl As DevExpress.XtraReports.UI.XRControl In Band.Controls
                    Dim subReport As DevExpress.XtraReports.UI.XRSubreport = TryCast(ctl, DevExpress.XtraReports.UI.XRSubreport)
                    If subReport IsNot Nothing Then
                        Dim rpt As DevExpress.XtraReports.UI.XtraReport = subReport.ReportSource
                        If rpt IsNot Nothing Then
                            rpt.ScriptReferences = ScriptReferences
                        End If
                    End If
                Next
            Next
        End Sub

        Private Sub InitializeComponent()
            Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(DisclosureReport))
            Me.PageHeader = New DevExpress.XtraReports.UI.PageHeaderBand()
            Me.XrRichText1 = New DevExpress.XtraReports.UI.XRRichText()
            Me.XrRichTextDetail = New DevExpress.XtraReports.UI.XRRichText()
            Me.PageFooter = New DevExpress.XtraReports.UI.PageFooterBand()
            Me.XrPictureBox5 = New DevExpress.XtraReports.UI.XRPictureBox()
            Me.XrPictureBox2 = New DevExpress.XtraReports.UI.XRPictureBox()
            Me.XrLabel1 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrPictureBox4 = New DevExpress.XtraReports.UI.XRPictureBox()
            Me.XrPictureBox3 = New DevExpress.XtraReports.UI.XRPictureBox()
            Me.ParameterClient = New DevExpress.XtraReports.Parameters.Parameter()
            Me.ParameterReportText = New DevExpress.XtraReports.Parameters.Parameter()
            CType(Me.XrRichText1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.XrRichTextDetail, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
            '
            'Detail
            '
            Me.Detail.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrRichTextDetail})
            Me.Detail.HeightF = 23.0!
            '
            'PageHeader
            '
            Me.PageHeader.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrRichText1})
            Me.PageHeader.HeightF = 91.66666!
            Me.PageHeader.Name = "PageHeader"
            '
            'XrRichText1
            '
            Me.XrRichText1.LocationFloat = New DevExpress.Utils.PointFloat(200.0!, 0.0!)
            Me.XrRichText1.Name = "XrRichText1"
            Me.XrRichText1.SerializableRtfString = resources.GetString("XrRichText1.SerializableRtfString")
            Me.XrRichText1.SizeF = New System.Drawing.SizeF(402.0833!, 91.66665!)
            '
            'XrRichTextDetail
            '
            Me.XrRichTextDetail.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
            Me.XrRichTextDetail.Name = "XrRichTextDetail"
            Me.XrRichTextDetail.Scripts.OnBeforePrint = "XrRichTextDetail_BeforePrint"
            Me.XrRichTextDetail.SerializableRtfString = resources.GetString("XrRichTextDetail.SerializableRtfString")
            Me.XrRichTextDetail.SizeF = New System.Drawing.SizeF(800.0!, 23.0!)
            '
            'PageFooter
            '
            Me.PageFooter.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrPictureBox5, Me.XrPictureBox2, Me.XrLabel1, Me.XrPictureBox4, Me.XrPictureBox3})
            Me.PageFooter.Name = "PageFooter"
            '
            'XrPictureBox5
            '
            Me.XrPictureBox5.Image = CType(resources.GetObject("XrPictureBox5.Image"), System.Drawing.Image)
            Me.XrPictureBox5.LocationFloat = New DevExpress.Utils.PointFloat(615.4584!, 8.95834!)
            Me.XrPictureBox5.Name = "XrPictureBox5"
            Me.XrPictureBox5.SizeF = New System.Drawing.SizeF(79.0!, 74.0!)
            Me.XrPictureBox5.Sizing = DevExpress.XtraPrinting.ImageSizeMode.ZoomImage
            '
            'XrPictureBox2
            '
            Me.XrPictureBox2.Image = CType(resources.GetObject("XrPictureBox2.Image"), System.Drawing.Image)
            Me.XrPictureBox2.LocationFloat = New DevExpress.Utils.PointFloat(47.5!, 10.0!)
            Me.XrPictureBox2.Name = "XrPictureBox2"
            Me.XrPictureBox2.SizeF = New System.Drawing.SizeF(46.0!, 74.0!)
            Me.XrPictureBox2.Sizing = DevExpress.XtraPrinting.ImageSizeMode.ZoomImage
            '
            'XrLabel1
            '
            Me.XrLabel1.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.XrLabel1.LocationFloat = New DevExpress.Utils.PointFloat(237.5!, 27.68752!)
            Me.XrLabel1.Name = "XrLabel1"
            Me.XrLabel1.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel1.SizeF = New System.Drawing.SizeF(325.0!, 36.54167!)
            Me.XrLabel1.StylePriority.UseFont = False
            Me.XrLabel1.StylePriority.UseTextAlignment = False
            Me.XrLabel1.Text = "www.ClearPointFinancialSolutions.org"
            Me.XrLabel1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
            Me.XrLabel1.WordWrap = False
            '
            'XrPictureBox4
            '
            Me.XrPictureBox4.Image = CType(resources.GetObject("XrPictureBox4.Image"), System.Drawing.Image)
            Me.XrPictureBox4.LocationFloat = New DevExpress.Utils.PointFloat(715.0!, 10.00001!)
            Me.XrPictureBox4.Name = "XrPictureBox4"
            Me.XrPictureBox4.SizeF = New System.Drawing.SizeF(75.0!, 74.0!)
            Me.XrPictureBox4.Sizing = DevExpress.XtraPrinting.ImageSizeMode.ZoomImage
            '
            'XrPictureBox3
            '
            Me.XrPictureBox3.Image = CType(resources.GetObject("XrPictureBox3.Image"), System.Drawing.Image)
            Me.XrPictureBox3.LocationFloat = New DevExpress.Utils.PointFloat(120.0417!, 8.95834!)
            Me.XrPictureBox3.Name = "XrPictureBox3"
            Me.XrPictureBox3.SizeF = New System.Drawing.SizeF(46.0!, 74.0!)
            Me.XrPictureBox3.Sizing = DevExpress.XtraPrinting.ImageSizeMode.ZoomImage
            '
            'ParameterClient
            '
            Me.ParameterClient.Description = "Client ID"
            Me.ParameterClient.Name = "ParameterClient"
            Me.ParameterClient.Type = GetType(Integer)
            Me.ParameterClient.Visible = False
            '
            'ParameterReportText
            '
            Me.ParameterReportText.Description = "Report Text"
            Me.ParameterReportText.Name = "ParameterReportText"
            Me.ParameterReportText.Visible = False
            '
            'DisclosureReport
            '
            Me.Bands.AddRange(New DevExpress.XtraReports.UI.Band() {Me.TopMarginBand1, Me.Detail, Me.BottomMarginBand1, Me.PageHeader, Me.PageFooter})
            Me.Parameters.AddRange(New DevExpress.XtraReports.Parameters.Parameter() {Me.ParameterClient, Me.ParameterReportText})
            Me.RequestParameters = False
            Me.ScriptLanguage = DevExpress.XtraReports.ScriptLanguage.VisualBasic
            Me.ScriptReferencesString = resources.GetString("$this.ScriptReferencesString")
            Me.Scripts.OnBeforePrint = "DisclosureReport_BeforePrint"
            Me.ScriptsSource = resources.GetString("$this.ScriptsSource")
            Me.Version = "11.2"
            Me.Controls.SetChildIndex(Me.PageFooter, 0)
            Me.Controls.SetChildIndex(Me.PageHeader, 0)
            Me.Controls.SetChildIndex(Me.BottomMarginBand1, 0)
            Me.Controls.SetChildIndex(Me.Detail, 0)
            Me.Controls.SetChildIndex(Me.TopMarginBand1, 0)
            CType(Me.XrRichText1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.XrRichTextDetail, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me, System.ComponentModel.ISupportInitialize).EndInit()

        End Sub
        Friend WithEvents PageFooter As DevExpress.XtraReports.UI.PageFooterBand
        Friend WithEvents XrRichText1 As DevExpress.XtraReports.UI.XRRichText
        Friend WithEvents XrRichTextDetail As DevExpress.XtraReports.UI.XRRichText
        Friend WithEvents XrPictureBox5 As DevExpress.XtraReports.UI.XRPictureBox
        Friend WithEvents XrPictureBox2 As DevExpress.XtraReports.UI.XRPictureBox
        Friend WithEvents XrLabel1 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrPictureBox4 As DevExpress.XtraReports.UI.XRPictureBox
        Friend WithEvents XrPictureBox3 As DevExpress.XtraReports.UI.XRPictureBox

        Friend WithEvents ParameterClient As DevExpress.XtraReports.Parameters.Parameter
        Friend WithEvents ParameterReportText As DevExpress.XtraReports.Parameters.Parameter
        Friend WithEvents PageHeader As DevExpress.XtraReports.UI.PageHeaderBand

        Public Property ClientID As Int32 Implements DebtPlus.Interfaces.Client.IClient.ClientId
            Get
                Return Parameter_Client
            End Get
            Set(value As Int32)
                Parameter_Client = value
            End Set
        End Property

        ''' <summary>
        ''' Client ID. This is the primary key to the clients table.
        ''' </summary>
        Public Property Parameter_Client() As System.Int32
            Get
                Dim parm As DevExpress.XtraReports.Parameters.Parameter = FindParameter("ParameterClient")
                If parm Is Nothing Then Return -1
                Return Convert.ToInt32(parm.Value)
            End Get
            Set(ByVal value As System.Int32)
                SetParameter("ParameterClient", GetType(Int32), value, "Client ID", False)
            End Set
        End Property

        ''' <summary>
        ''' Does the report need to ask for parameters? If so, we request the parameters for the report
        ''' </summary>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Overrides Function NeedParameters() As Boolean
            Return MyBase.NeedParameters() OrElse (Parameter_Client <= 0)
        End Function

        ''' <summary>
        ''' This is the title shown on the title bar for the preview dialog. It has no other purpose for the client packets
        ''' </summary>
        ''' <value></value>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Overrides ReadOnly Property ReportTitle() As String
            Get
                Return "Client Disclosure"
            End Get
        End Property

        ''' <summary>
        ''' Common interface to set a parameter value.
        ''' </summary>
        ''' <param name="Parameter">Name of the parameter to be set.</param>
        ''' <param name="Value">Value associated with that parameter</param>
        ''' <remarks></remarks>
        Public Overrides Sub SetReportParameter(ByVal Parameter As String, ByVal Value As Object)
            Select Case Parameter
                Case "Client"
                    Parameter_Client = Convert.ToInt32(Value)
                Case Else
                    MyBase.SetReportParameter(Parameter, Value)
            End Select
        End Sub

        ''' <summary>
        ''' Ask for the report parameters if they are needed
        ''' </summary>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Overrides Function RequestReportParameters() As System.Windows.Forms.DialogResult
            Dim Answer As System.Windows.Forms.DialogResult = DialogResult.OK
            If NeedParameters() Then
                Using frm As New DebtPlus.Reports.Template.Forms.ClientParametersForm()
                    Answer = frm.ShowDialog
                    Parameter_Client = frm.Parameter_Client
                End Using
            End If
            Return Answer
        End Function

        ' ******************************************************** MOVED TO SCRIPTS ******************************************************************

        Private Sub XrRichTextDetail_BeforePrint(sender As Object, e As System.Drawing.Printing.PrintEventArgs)
            Try
                With CType(sender, DevExpress.XtraReports.UI.XRRichText)
                    Dim rpt As DevExpress.XtraReports.UI.XtraReport = CType(.Report, DevExpress.XtraReports.UI.XtraReport)
                    Dim DocumentName As String = rpt.Parameters("ParameterReportText").Value

                    If Not System.IO.Path.IsPathRooted(DocumentName) Then
                        If DocumentName.ToLower.StartsWith("[letters]/") Then
                            DocumentName = System.IO.Path.Combine(DebtPlus.Configuration.Config.LettersDirectory(), DocumentName.Substring(10))
                        ElseIf DocumentName.ToLower.StartsWith("[reports]/") Then
                            DocumentName = System.IO.Path.Combine(DebtPlus.Configuration.Config.ReportsDirectory(), DocumentName.Substring(10))
                        Else
                            DocumentName = System.IO.Path.Combine(DebtPlus.Configuration.Config.ReportsDirectory(), DocumentName)
                        End If
                    End If

                    '-- Try to determine the type of the file from the extension
                    Dim FileType As DevExpress.XtraReports.UI.XRRichTextStreamType
                    Select Case System.IO.Path.GetExtension(DocumentName).ToLower
                        Case ".htm", ".html"
                            FileType = DevExpress.XtraReports.UI.XRRichTextStreamType.HtmlText
                        Case ".rtf"
                            FileType = DevExpress.XtraReports.UI.XRRichTextStreamType.RtfText
                        Case ".xml"
                            FileType = DevExpress.XtraReports.UI.XRRichTextStreamType.XmlText
                        Case Else
                            FileType = DevExpress.XtraReports.UI.XRRichTextStreamType.PlainText
                    End Select

                    '-- Load the text into the document before it is printed
                    .LoadFile(DocumentName, FileType)
                End With

            Catch ex As Exception
                Using gdr As New DebtPlus.Repository.GetDataResult()
                    gdr.HandleException(ex)
                    DebtPlus.UI.Common.ErrorHandling.HandleErrors(gdr, "Error loading text document")
                End Using
            End Try
        End Sub
    End Class
End Namespace
