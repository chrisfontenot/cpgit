#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}
#End Region

Option Compare Binary
Option Explicit On
Option Strict On

Imports DebtPlus.Interfaces.Client
Imports DebtPlus.Reports.Template
Imports System.IO
Imports DebtPlus.Utils

Namespace Client.Disclosures

    Public Class DisclosureReport
        Inherits BaseXtraReportClass
        Implements IClient

        Public Sub New()
            MyBase.New()
            InitializeComponent()
        End Sub

        Public Sub New(ByVal ReportDefinition As String)
            MyBase.New()
            InitializeComponent()

            ' Set the report definition text
            'Dim DocumentName As String = String.Empty
            'Dim Fields() As String = ReportDefinition.Split(";"c)
            'If Fields.GetUpperBound(0) < 1 Then
            '    Throw New ApplicationException("The entry in the reports table is not valid for this report. It needs the name of the RTF file.")
            'End If

            'DocumentName = Fields(1).Trim()
            'If DocumentName = String.Empty Then
            '    Throw New ApplicationException("The entry in the reports table is not valid for this report. The report name can not be empty.")
            'End If

            ' Set the report information
            Parameters("ParameterReportText").Value = ReportDefinition
        End Sub

        Private Sub InitializeComponent()
            Const ReportName As String = "DebtPlus.Reports.Client.Disclosures.repx"

            ' See if there is a report reference that we can use
            Dim Fname As String = System.IO.Path.Combine(DebtPlus.Configuration.Config.ReportsDirectory, ReportName)
            Dim UseDefault As Boolean = True
            Try
                If (Not DebtPlus.Configuration.Config.LocalReportsOnly) AndAlso System.IO.File.Exists(Fname) Then
                    LoadLayout(Fname)
                    UseDefault = False
                End If

            Catch ex As Exception
                DebtPlus.Svc.Common.ErrorHandling.HandleErrors(ex)
            End Try

            ' Load the standard report definition if we need a new item
            If UseDefault Then
                Dim asm As Reflection.Assembly = Reflection.Assembly.GetExecutingAssembly                                           ' changed
                Using ios As Stream = asm.GetManifestResourceStream(String.Format("DebtPlus.Reports.{0}", ReportName))              ' changed
                    If ios IsNot Nothing Then LoadLayout(ios) '                                                                       changed
                End Using
            End If

            ' Set the script references as needed
            ScriptReferences = DebtPlus.Utils.AssemblyTools.GetAllSiblingAssemblies()

            ' Update the subreports as well
            For Each BandPtr As DevExpress.XtraReports.UI.Band In Bands
                For Each CtlPtr As DevExpress.XtraReports.UI.XRControl In BandPtr.Controls
                    Dim Rpt As DevExpress.XtraReports.UI.XRSubreport = TryCast(CtlPtr, DevExpress.XtraReports.UI.XRSubreport)
                    If Rpt IsNot Nothing Then
                        Dim RptSrc As DevExpress.XtraReports.UI.XtraReport = TryCast(Rpt.ReportSource, DevExpress.XtraReports.UI.XtraReport)
                        If RptSrc IsNot Nothing Then
                            RptSrc.ScriptReferences = ScriptReferences
                        End If
                    End If
                Next
            Next

            ' Disable the select expert
            ReportFilter.IsEnabled = False

            Parameters("ParameterClient").Value = -1
            Parameters("ParameterReportText").Value = String.Empty
        End Sub

        ''' <summary>
        ''' Client ID. This is the primary key to the clients table.
        ''' </summary>
        Public Property ClientId() As System.Int32 Implements IClient.ClientId
            Get
                Return Convert.ToInt32(Parameters("ParameterClient").Value)
            End Get
            Set(ByVal value As System.Int32)
                Parameters("ParameterClient").Value = value
            End Set
        End Property

        ''' <summary>
        ''' Does the report need to ask for parameters? If so, we request the parameters for the reprot
        ''' </summary>
        Public Overrides Function NeedParameters() As Boolean
            Return MyBase.NeedParameters() OrElse (ClientId <= 0)
        End Function

        ''' <summary>
        ''' This is the title shown on the titla bar for the preview dialog. It has no other purpose for the client packets
        ''' </summary>
        Public Overrides ReadOnly Property ReportTitle() As String
            Get
                Return "Client Disclosure"
            End Get
        End Property

        ''' <summary>
        ''' Common interface to set a parameter value.
        ''' </summary>
        ''' <param name="Parameter">Name of the parameter to be set.</param>
        ''' <param name="Value">Value associated with that parameter</param>
        Public Overrides Sub SetReportParameter(ByVal Parameter As String, ByVal Value As Object)
            Select Case Parameter
                Case "Client"
                    ClientId = Convert.ToInt32(Value)
                Case Else
                    MyBase.SetReportParameter(Parameter, Value)
            End Select
        End Sub

        ''' <summary>
        ''' Ask for the report parameters if they are needed
        ''' </summary>
        Public Overrides Function RequestReportParameters() As System.Windows.Forms.DialogResult
            Dim answer As System.Windows.Forms.DialogResult = DialogResult.OK
            If NeedParameters() Then
                With New DebtPlus.Reports.Template.Forms.ClientParametersForm()
                    answer = .ShowDialog()
                    ClientId = .Parameter_Client
                    .Dispose()
                End With
            End If
            Return answer
        End Function
    End Class
End Namespace