Namespace Custom.EOM.DropsByDistrict
    <Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
    Partial Class DetailReport

        'XtraReport overrides dispose to clean up the component list.
        <System.Diagnostics.DebuggerNonUserCode()> _
        Protected Overrides Sub Dispose(ByVal disposing As Boolean)
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
            MyBase.Dispose(disposing)
        End Sub

        'Required by the Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Designer
        'It can be modified using the Designer.  
        'Do not modify it using the code editor.
        <System.Diagnostics.DebuggerStepThrough()> _
        Private Sub InitializeComponent()
            Me.XrPanel1 = New DevExpress.XtraReports.UI.XRPanel
            Me.XrLabel5 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel4 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel3 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel2 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel1 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_client = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_name = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_active_status = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_active_status_date = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_drop_date = New DevExpress.XtraReports.UI.XRLabel
            CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
            '
            'Detail
            '
            Me.Detail.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel_drop_date, Me.XrLabel_active_status_date, Me.XrLabel_active_status, Me.XrLabel_name, Me.XrLabel_client})
            Me.Detail.HeightF = 15.00003!
            '
            'PageHeader
            '
            Me.PageHeader.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrPanel1})
            Me.PageHeader.HeightF = 134.0417!
            Me.PageHeader.Controls.SetChildIndex(Me.XrPanel_AgencyAddress, 0)
            Me.PageHeader.Controls.SetChildIndex(Me.XrLabel_Title, 0)
            Me.PageHeader.Controls.SetChildIndex(Me.XrLine_Title, 0)
            Me.PageHeader.Controls.SetChildIndex(Me.XrLabel_Subtitle, 0)
            Me.PageHeader.Controls.SetChildIndex(Me.XrPageInfo1, 0)
            Me.PageHeader.Controls.SetChildIndex(Me.XrPanel1, 0)
            '
            'XrLabel_Title
            '
            Me.XrLabel_Title.StylePriority.UseFont = False
            '
            'XrPageInfo_PageNumber
            '
            Me.XrPageInfo_PageNumber.StylePriority.UseTextAlignment = False
            '
            'XRLabel_Agency_Name
            '
            Me.XRLabel_Agency_Name.StylePriority.UseTextAlignment = False
            '
            'XrLabel_Agency_Address3
            '
            Me.XrLabel_Agency_Address3.StylePriority.UseFont = False
            Me.XrLabel_Agency_Address3.StylePriority.UseTextAlignment = False
            '
            'XrLabel_Agency_Address1
            '
            Me.XrLabel_Agency_Address1.StylePriority.UseFont = False
            Me.XrLabel_Agency_Address1.StylePriority.UseTextAlignment = False
            '
            'XrLabel_Agency_Phone
            '
            Me.XrLabel_Agency_Phone.StylePriority.UseFont = False
            Me.XrLabel_Agency_Phone.StylePriority.UseTextAlignment = False
            '
            'XrLabel_Agency_Address2
            '
            Me.XrLabel_Agency_Address2.StylePriority.UseFont = False
            Me.XrLabel_Agency_Address2.StylePriority.UseTextAlignment = False
            '
            'XrPanel1
            '
            Me.XrPanel1.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel5, Me.XrLabel4, Me.XrLabel3, Me.XrLabel2, Me.XrLabel1})
            Me.XrPanel1.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 107.0417!)
            Me.XrPanel1.Name = "XrPanel1"
            Me.XrPanel1.SizeF = New System.Drawing.SizeF(795.3747!, 17.0!)
            Me.XrPanel1.StyleName = "XrControlStyle_HeaderPannel"
            '
            'XrLabel5
            '
            Me.XrLabel5.LocationFloat = New DevExpress.Utils.PointFloat(701.67!, 1.0!)
            Me.XrLabel5.Name = "XrLabel5"
            Me.XrLabel5.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel5.SizeF = New System.Drawing.SizeF(83.70833!, 15.00001!)
            Me.XrLabel5.StylePriority.UseTextAlignment = False
            Me.XrLabel5.Text = "DROP DATE"
            Me.XrLabel5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel4
            '
            Me.XrLabel4.LocationFloat = New DevExpress.Utils.PointFloat(540.8748!, 1.000015!)
            Me.XrLabel4.Name = "XrLabel4"
            Me.XrLabel4.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel4.SizeF = New System.Drawing.SizeF(160.7916!, 15.0!)
            Me.XrLabel4.StylePriority.UseTextAlignment = False
            Me.XrLabel4.Text = "ACTIVE STATUS DATE"
            Me.XrLabel4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel3
            '
            Me.XrLabel3.LocationFloat = New DevExpress.Utils.PointFloat(405.0831!, 1.000023!)
            Me.XrLabel3.Name = "XrLabel3"
            Me.XrLabel3.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel3.SizeF = New System.Drawing.SizeF(135.7917!, 14.99999!)
            Me.XrLabel3.StylePriority.UseTextAlignment = False
            Me.XrLabel3.Text = "ACTIVE STATUS"
            Me.XrLabel3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
            '
            'XrLabel2
            '
            Me.XrLabel2.LocationFloat = New DevExpress.Utils.PointFloat(321.3748!, 1.0!)
            Me.XrLabel2.Name = "XrLabel2"
            Me.XrLabel2.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel2.SizeF = New System.Drawing.SizeF(83.70833!, 15.00001!)
            Me.XrLabel2.StylePriority.UseTextAlignment = False
            Me.XrLabel2.Text = "CLIENT"
            Me.XrLabel2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel1
            '
            Me.XrLabel1.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 1.0!)
            Me.XrLabel1.Name = "XrLabel1"
            Me.XrLabel1.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel1.SizeF = New System.Drawing.SizeF(213.5!, 15.0!)
            Me.XrLabel1.StylePriority.UseTextAlignment = False
            Me.XrLabel1.Text = "NAME"
            Me.XrLabel1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            '
            'XrLabel_client
            '
            Me.XrLabel_client.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "client", "{0:0000000}")})
            Me.XrLabel_client.LocationFloat = New DevExpress.Utils.PointFloat(321.3748!, 0.0!)
            Me.XrLabel_client.Name = "XrLabel_client"
            Me.XrLabel_client.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_client.SizeF = New System.Drawing.SizeF(83.70833!, 15.00001!)
            Me.XrLabel_client.StylePriority.UseTextAlignment = False
            Me.XrLabel_client.Text = "COUNT"
            Me.XrLabel_client.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel_name
            '
            Me.XrLabel_name.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "name")})
            Me.XrLabel_name.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.00003178914!)
            Me.XrLabel_name.Name = "XrLabel_name"
            Me.XrLabel_name.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_name.SizeF = New System.Drawing.SizeF(310.375!, 15.0!)
            Me.XrLabel_name.StylePriority.UseTextAlignment = False
            Me.XrLabel_name.Text = "DISTRICT"
            Me.XrLabel_name.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            '
            'XrLabel_active_status
            '
            Me.XrLabel_active_status.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "active_status")})
            Me.XrLabel_active_status.LocationFloat = New DevExpress.Utils.PointFloat(405.0831!, 0.0!)
            Me.XrLabel_active_status.Name = "XrLabel_active_status"
            Me.XrLabel_active_status.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_active_status.SizeF = New System.Drawing.SizeF(135.7917!, 15.00001!)
            Me.XrLabel_active_status.StylePriority.UseTextAlignment = False
            Me.XrLabel_active_status.Text = "COUNT"
            Me.XrLabel_active_status.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
            '
            'XrLabel_active_status_date
            '
            Me.XrLabel_active_status_date.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "active_status_date", "{0:d}")})
            Me.XrLabel_active_status_date.LocationFloat = New DevExpress.Utils.PointFloat(540.8748!, 0.0!)
            Me.XrLabel_active_status_date.Name = "XrLabel_active_status_date"
            Me.XrLabel_active_status_date.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_active_status_date.SizeF = New System.Drawing.SizeF(150.8335!, 15.00001!)
            Me.XrLabel_active_status_date.StylePriority.UseTextAlignment = False
            Me.XrLabel_active_status_date.Text = "COUNT"
            Me.XrLabel_active_status_date.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel_drop_date
            '
            Me.XrLabel_drop_date.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "drop_date", "{0:d}")})
            Me.XrLabel_drop_date.LocationFloat = New DevExpress.Utils.PointFloat(701.6699!, 0.0!)
            Me.XrLabel_drop_date.Name = "XrLabel_drop_date"
            Me.XrLabel_drop_date.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_drop_date.SizeF = New System.Drawing.SizeF(83.70477!, 15.00001!)
            Me.XrLabel_drop_date.StylePriority.UseTextAlignment = False
            Me.XrLabel_drop_date.Text = "COUNT"
            Me.XrLabel_drop_date.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'DetailReport
            '
            Me.Bands.AddRange(New DevExpress.XtraReports.UI.Band() {Me.Detail, Me.PageHeader, Me.PageFooter, Me.TopMarginBand1, Me.BottomMarginBand1})
            Me.StyleSheet.AddRange(New DevExpress.XtraReports.UI.XRControlStyle() {Me.XrControlStyle_Header, Me.XrControlStyle_Totals, Me.XrControlStyle_HeaderPannel, Me.XrControlStyle_GroupHeader})
            Me.Version = "11.1"
            CType(Me, System.ComponentModel.ISupportInitialize).EndInit()

        End Sub
        Friend WithEvents XrPanel1 As DevExpress.XtraReports.UI.XRPanel
        Friend WithEvents XrLabel2 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel1 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_drop_date As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_active_status_date As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_active_status As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_name As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_client As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel5 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel4 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel3 As DevExpress.XtraReports.UI.XRLabel
    End Class
End Namespace
