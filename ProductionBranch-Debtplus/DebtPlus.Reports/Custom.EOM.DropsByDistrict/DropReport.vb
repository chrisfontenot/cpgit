#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}
#End Region

Option Compare Binary
Option Explicit On
Option Strict On
Namespace Custom.EOM.DropsByDistrict

    Public Class DropReport
        Inherits DebtPlus.Reports.Template.DatedTemplateXtraReportClass

        Public Sub New()
            MyBase.New()
            InitializeComponent()
            AddHandler BeforePrint, AddressOf Report_BeforePrint
            AddHandler XrLabel_group_count.PreviewClick, AddressOf XrLabel_PreviewClick
            AddHandler XrLabel_group_district.PreviewClick, AddressOf XrLabel_PreviewClick
        End Sub

        Public Overrides ReadOnly Property ReportTitle() As String
            Get
                Return "Client Drops By District"
            End Get
        End Property

        Private ds As New System.Data.DataSet("ds")
        Private Sub Report_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs)
            Const TableName As String = "report_custom_Dropped_DMP_by_District"

            Dim tbl As System.Data.DataTable
            Dim rpt As DevExpress.XtraReports.UI.XtraReport = CType(sender, DevExpress.XtraReports.UI.XtraReport)
            Dim cn As New System.Data.SqlClient.SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
            Try
                cn.Open()
                Using cmd As New System.Data.SqlClient.SqlCommand()
                    With cmd
                        .Connection = cn
                        .CommandText = "SELECT [client],[active_status],[active_status_date],[name],[drop_date],[description],[district] FROM report_custom_Dropped_DMP_by_District WHERE drop_date >= @from_date AND drop_date < @to_date ORDER BY district, client"
                        .CommandType = System.Data.CommandType.Text
                        .Parameters.Add("@from_date", System.Data.SqlDbType.DateTime).Value = Convert.ToDateTime(rpt.Parameters("ParameterFromDate").Value).Date
                        .Parameters.Add("@to_date", System.Data.SqlDbType.DateTime).Value = Convert.ToDateTime(rpt.Parameters("ParameterToDate").Value).Date.AddDays(1)
                        .CommandTimeout = 0
                    End With

                    Using da As New System.Data.SqlClient.SqlDataAdapter(cmd)
                        da.Fill(ds, TableName)
                        tbl = ds.Tables(TableName)
                    End Using
                End Using

                If tbl IsNot Nothing Then
                    rpt.DataSource = tbl.DefaultView
                    For Each calc As DevExpress.XtraReports.UI.CalculatedField In rpt.CalculatedFields
                        calc.Assign(rpt.DataSource, rpt.DataMember)
                    Next
                End If

            Catch ex As System.Data.SqlClient.SqlException
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error reading report information")

            Finally
                If cn IsNot Nothing Then cn.Dispose()
            End Try
        End Sub

        Private Sub XrLabel_PreviewClick(ByVal sender As Object, ByVal e As DevExpress.XtraReports.UI.PreviewMouseEventArgs)
            Dim district As Int32 = DebtPlus.Utils.Nulls.DInt(e.Brick.Value)
            If district > 0 Then
                Dim rpt As DevExpress.XtraReports.UI.XtraReport = CType(CType(sender, DevExpress.XtraReports.UI.XRControl).Report, DevExpress.XtraReports.UI.XtraReport)
                Dim vue As System.Data.DataView = TryCast(rpt.DataSource, System.Data.DataView)
                If vue IsNot Nothing Then
                    Dim tbl As System.Data.DataTable = vue.Table
                    Dim NewVue As New System.Data.DataView(tbl, String.Format("[district]={0:f0}", district), "name, client", DataViewRowState.CurrentRows)
                    If NewVue.Count <= 0 Then
                        NewVue.Dispose()
                    Else
                        Dim thrd As New System.Threading.Thread(New System.Threading.ParameterizedThreadStart(AddressOf PreviewHandler))
                        With thrd
                            .SetApartmentState(Threading.ApartmentState.STA)
                            .Name = "Detail Report"
                            .IsBackground = True
                            .Start(New Object() {NewVue, String.Format("{0} FOR district '{1}'", ReportSubTitle, NewVue(0)("description"))})
                        End With
                    End If
                End If
            End If
        End Sub

        Private Sub PreviewHandler(ByVal obj As Object)
            Dim objList() As Object = CType(obj, Object())
            Dim Subtitle As String = CType(objList(1), String)
            Using NewVue As System.Data.DataView = CType(objList(0), System.Data.DataView)
                Using rpt As New DetailReport
                    rpt.SetSubtitle(Subtitle)
                    rpt.DataSource = NewVue
                    rpt.CreateDocument()
                    rpt.DisplayPreviewDialog()
                End Using
            End Using
        End Sub
    End Class
End Namespace