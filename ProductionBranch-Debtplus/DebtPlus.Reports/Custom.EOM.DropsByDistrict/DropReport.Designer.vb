Namespace Custom.EOM.DropsByDistrict
    <Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
    Partial Class DropReport

        'XtraReport overrides dispose to clean up the component list.
        <System.Diagnostics.DebuggerNonUserCode()> _
        Protected Overrides Sub Dispose(ByVal disposing As Boolean)
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
            MyBase.Dispose(disposing)
        End Sub

        'Required by the Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Designer
        'It can be modified using the Designer.  
        'Do not modify it using the code editor.
        <System.Diagnostics.DebuggerStepThrough()> _
        Private Sub InitializeComponent()
            Dim XrSummary1 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary
            Dim XrSummary2 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary
            Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(DropReport))
            Me.XrPanel1 = New DevExpress.XtraReports.UI.XRPanel
            Me.XrLabel2 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel1 = New DevExpress.XtraReports.UI.XRLabel
            Me.GroupHeader1 = New DevExpress.XtraReports.UI.GroupHeaderBand
            Me.GroupFooter1 = New DevExpress.XtraReports.UI.GroupFooterBand
            Me.XrLabel_group_district = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_group_count = New DevExpress.XtraReports.UI.XRLabel
            Me.ReportFooter = New DevExpress.XtraReports.UI.ReportFooterBand
            Me.XrLabel6 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_total_count = New DevExpress.XtraReports.UI.XRLabel
            CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
            '
            'Detail
            '
            Me.Detail.HeightF = 0.0!
            '
            'PageHeader
            '
            Me.PageHeader.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrPanel1})
            Me.PageHeader.HeightF = 135.8333!
            Me.PageHeader.Controls.SetChildIndex(Me.XrPanel_AgencyAddress, 0)
            Me.PageHeader.Controls.SetChildIndex(Me.XrLabel_Title, 0)
            Me.PageHeader.Controls.SetChildIndex(Me.XrLine_Title, 0)
            Me.PageHeader.Controls.SetChildIndex(Me.XrLabel_Subtitle, 0)
            Me.PageHeader.Controls.SetChildIndex(Me.XrPageInfo1, 0)
            Me.PageHeader.Controls.SetChildIndex(Me.XrPanel1, 0)
            '
            'PageFooter
            '
            Me.PageFooter.HeightF = 29.99999!
            '
            'XrLabel_Title
            '
            Me.XrLabel_Title.LocationFloat = New DevExpress.Utils.PointFloat(7.999992!, 7.999992!)
            Me.XrLabel_Title.SizeF = New System.Drawing.SizeF(441.6667!, 42.0!)
            Me.XrLabel_Title.StylePriority.UseFont = False
            '
            'XrPageInfo_PageNumber
            '
            Me.XrPageInfo_PageNumber.LocationFloat = New DevExpress.Utils.PointFloat(662.3747!, 8.0!)
            Me.XrPageInfo_PageNumber.StylePriority.UseTextAlignment = False
            '
            'XrPanel_AgencyAddress
            '
            Me.XrPanel_AgencyAddress.LocationFloat = New DevExpress.Utils.PointFloat(487.3747!, 0.0!)
            '
            'XRLabel_Agency_Name
            '
            Me.XRLabel_Agency_Name.StylePriority.UseTextAlignment = False
            '
            'XrLine_Title
            '
            Me.XrLine_Title.LocationFloat = New DevExpress.Utils.PointFloat(7.999992!, 50.0!)
            Me.XrLine_Title.SizeF = New System.Drawing.SizeF(441.6666!, 8.0!)
            '
            'XrLabel_Agency_Address3
            '
            Me.XrLabel_Agency_Address3.StylePriority.UseFont = False
            Me.XrLabel_Agency_Address3.StylePriority.UseTextAlignment = False
            '
            'XrLabel_Agency_Address1
            '
            Me.XrLabel_Agency_Address1.StylePriority.UseFont = False
            Me.XrLabel_Agency_Address1.StylePriority.UseTextAlignment = False
            '
            'XrLabel_Agency_Phone
            '
            Me.XrLabel_Agency_Phone.StylePriority.UseFont = False
            Me.XrLabel_Agency_Phone.StylePriority.UseTextAlignment = False
            '
            'XrLabel_Agency_Address2
            '
            Me.XrLabel_Agency_Address2.StylePriority.UseFont = False
            Me.XrLabel_Agency_Address2.StylePriority.UseTextAlignment = False
            '
            'XrPanel1
            '
            Me.XrPanel1.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel2, Me.XrLabel1})
            Me.XrPanel1.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 113.0!)
            Me.XrPanel1.Name = "XrPanel1"
            Me.XrPanel1.SizeF = New System.Drawing.SizeF(795.3747!, 17.0!)
            Me.XrPanel1.StyleName = "XrControlStyle_HeaderPannel"
            '
            'XrLabel2
            '
            Me.XrLabel2.LocationFloat = New DevExpress.Utils.PointFloat(318.5715!, 1.0!)
            Me.XrLabel2.Name = "XrLabel2"
            Me.XrLabel2.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel2.SizeF = New System.Drawing.SizeF(83.70833!, 15.00001!)
            Me.XrLabel2.StylePriority.UseTextAlignment = False
            Me.XrLabel2.Text = "COUNT"
            Me.XrLabel2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel1
            '
            Me.XrLabel1.LocationFloat = New DevExpress.Utils.PointFloat(11.0!, 1.0!)
            Me.XrLabel1.Name = "XrLabel1"
            Me.XrLabel1.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel1.SizeF = New System.Drawing.SizeF(213.5!, 15.0!)
            Me.XrLabel1.StylePriority.UseTextAlignment = False
            Me.XrLabel1.Text = "DISTRICT"
            Me.XrLabel1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            '
            'GroupHeader1
            '
            Me.GroupHeader1.GroupFields.AddRange(New DevExpress.XtraReports.UI.GroupField() {New DevExpress.XtraReports.UI.GroupField("district", DevExpress.XtraReports.UI.XRColumnSortOrder.Ascending)})
            Me.GroupHeader1.HeightF = 0.0!
            Me.GroupHeader1.Name = "GroupHeader1"
            '
            'GroupFooter1
            '
            Me.GroupFooter1.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel_group_district, Me.XrLabel_group_count})
            Me.GroupFooter1.HeightF = 15.00003!
            Me.GroupFooter1.Name = "GroupFooter1"
            '
            'XrLabel_group_district
            '
            Me.XrLabel_group_district.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "description"), New DevExpress.XtraReports.UI.XRBinding("Tag", Nothing, "district")})
            Me.XrLabel_group_district.LocationFloat = New DevExpress.Utils.PointFloat(11.0!, 0.0!)
            Me.XrLabel_group_district.Name = "XrLabel_group_district"
            Me.XrLabel_group_district.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_group_district.Scripts.OnPreviewClick = "XrLabel_PreviewClick"
            Me.XrLabel_group_district.SizeF = New System.Drawing.SizeF(294.75!, 15.0!)
            Me.XrLabel_group_district.StylePriority.UseTextAlignment = False
            Me.XrLabel_group_district.Text = "DISTRICT"
            Me.XrLabel_group_district.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            '
            'XrLabel_group_count
            '
            Me.XrLabel_group_count.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "client"), New DevExpress.XtraReports.UI.XRBinding("Tag", Nothing, "district")})
            Me.XrLabel_group_count.LocationFloat = New DevExpress.Utils.PointFloat(318.5715!, 0.0!)
            Me.XrLabel_group_count.Name = "XrLabel_group_count"
            Me.XrLabel_group_count.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_group_count.Scripts.OnPreviewClick = "XrLabel_PreviewClick"
            Me.XrLabel_group_count.SizeF = New System.Drawing.SizeF(83.70833!, 15.00001!)
            Me.XrLabel_group_count.StylePriority.UseTextAlignment = False
            XrSummary1.FormatString = "{0:n0}"
            XrSummary1.Func = DevExpress.XtraReports.UI.SummaryFunc.Count
            XrSummary1.Running = DevExpress.XtraReports.UI.SummaryRunning.Group
            Me.XrLabel_group_count.Summary = XrSummary1
            Me.XrLabel_group_count.Text = "COUNT"
            Me.XrLabel_group_count.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'ReportFooter
            '
            Me.ReportFooter.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel6, Me.XrLabel_total_count})
            Me.ReportFooter.HeightF = 23.33337!
            Me.ReportFooter.Name = "ReportFooter"
            Me.ReportFooter.StyleName = "XrControlStyle_Totals"
            '
            'XrLabel6
            '
            Me.XrLabel6.LocationFloat = New DevExpress.Utils.PointFloat(11.0!, 8.333359!)
            Me.XrLabel6.Name = "XrLabel6"
            Me.XrLabel6.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel6.SizeF = New System.Drawing.SizeF(213.5!, 15.0!)
            Me.XrLabel6.StylePriority.UseTextAlignment = False
            Me.XrLabel6.Text = "TOTAL"
            Me.XrLabel6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            '
            'XrLabel_total_count
            '
            Me.XrLabel_total_count.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "client")})
            Me.XrLabel_total_count.LocationFloat = New DevExpress.Utils.PointFloat(318.5715!, 8.333359!)
            Me.XrLabel_total_count.Name = "XrLabel_total_count"
            Me.XrLabel_total_count.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_total_count.SizeF = New System.Drawing.SizeF(83.70833!, 15.00001!)
            Me.XrLabel_total_count.StylePriority.UseTextAlignment = False
            XrSummary2.FormatString = "{0:n0}"
            XrSummary2.Func = DevExpress.XtraReports.UI.SummaryFunc.Count
            XrSummary2.Running = DevExpress.XtraReports.UI.SummaryRunning.Report
            Me.XrLabel_total_count.Summary = XrSummary2
            Me.XrLabel_total_count.Text = "COUNT"
            Me.XrLabel_total_count.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'DropReport
            '
            Me.Bands.AddRange(New DevExpress.XtraReports.UI.Band() {Me.Detail, Me.PageHeader, Me.PageFooter, Me.TopMarginBand1, Me.BottomMarginBand1, Me.GroupHeader1, Me.GroupFooter1, Me.ReportFooter})
            Me.ScriptLanguage = DevExpress.XtraReports.ScriptLanguage.VisualBasic
            Me.ScriptReferencesString = resources.GetString("$this.ScriptReferencesString")
            Me.StyleSheet.AddRange(New DevExpress.XtraReports.UI.XRControlStyle() {Me.XrControlStyle_Header, Me.XrControlStyle_Totals, Me.XrControlStyle_HeaderPannel, Me.XrControlStyle_GroupHeader})
            Me.Version = "11.1"
            Me.Controls.SetChildIndex(Me.ReportFooter, 0)
            Me.Controls.SetChildIndex(Me.GroupFooter1, 0)
            Me.Controls.SetChildIndex(Me.GroupHeader1, 0)
            Me.Controls.SetChildIndex(Me.BottomMarginBand1, 0)
            Me.Controls.SetChildIndex(Me.TopMarginBand1, 0)
            Me.Controls.SetChildIndex(Me.PageFooter, 0)
            Me.Controls.SetChildIndex(Me.PageHeader, 0)
            Me.Controls.SetChildIndex(Me.Detail, 0)
            CType(Me, System.ComponentModel.ISupportInitialize).EndInit()

        End Sub

        Friend WithEvents XrPanel1 As DevExpress.XtraReports.UI.XRPanel
        Friend WithEvents XrLabel1 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel2 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents GroupHeader1 As DevExpress.XtraReports.UI.GroupHeaderBand
        Friend WithEvents GroupFooter1 As DevExpress.XtraReports.UI.GroupFooterBand
        Friend WithEvents XrLabel_group_district As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_group_count As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents ReportFooter As DevExpress.XtraReports.UI.ReportFooterBand
        Friend WithEvents XrLabel6 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_total_count As DevExpress.XtraReports.UI.XRLabel
    End Class
End Namespace
