#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}
#End Region

Imports DevExpress.XtraReports.UI

Public Class CreditorInvoiceReport
    Inherits Template.BaseXtraReportClass

    Public Sub New()
        MyBase.New()
        InitializeComponent()

        '-- Ensure that the creditor parameter is empty
        Parameter_Creditor = String.Empty

        '-- Add the handlers for the pannels
        'AddHandler Me.BeforePrint, AddressOf Report_BeforePrint
        'AddHandler XrPanel_Aging.BeforePrint, AddressOf XrPanel_Aging_BeforePrint
        'AddHandler XrLabel_Creditor_Address.BeforePrint, AddressOf XrLabel_Creditor_Address_BeforePrint
        'AddHandler Text_Footer.PrintOnPage, AddressOf Text_Footer_PrintOnPage
        'AddHandler Subreport_InvoiceStatus.BeforePrint, AddressOf Subreport_InvoiceStatus_BeforePrint
        'AddHandler Subreport_CheckDetail.BeforePrint, AddressOf Subreport_CheckDetail_BeforePrint
        'AddHandler XrBarCode_PostalCode.BeforePrint, AddressOf XrBarCode_PostalCode_BeforePrint
    End Sub

    Public Property Parameter_Creditor() As String
        Get
            Return CType(Parameters("ParameterCreditor").Value, String)
        End Get
        Set(ByVal Value As String)
            Parameters("ParameterCreditor").Value = Value
        End Set
    End Property

    Public Overrides Sub SetReportParameter(ByVal Parameter As String, ByVal Value As Object)
        Select Case Parameter
            Case "Creditor"
                Parameter_Creditor = CStr(Value)
            Case Else
                MyBase.SetReportParameter(Parameter, Value)
        End Select
    End Sub

    Public Overrides Function NeedParameters() As Boolean
        Return MyBase.NeedParameters() OrElse Parameter_Creditor = String.Empty
    End Function

    Public Overrides Function RequestReportParameters() As System.Windows.Forms.DialogResult
        Dim Answer As System.Windows.Forms.DialogResult = DialogResult.OK
        If NeedParameters() Then
            With New DebtPlus.Reports.Template.Forms.CreditorParametersForm()
                Answer = .ShowDialog
                Parameter_Creditor = .Parameter_Creditor
                .Dispose()
            End With
        End If
        Return Answer
    End Function

#Region " Component Designer generated code "

    'Component overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If components IsNot Nothing Then
                components.Dispose()
            End If
            ds.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub
    Friend WithEvents XrLabel3 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel5 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel1 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrRichText1 As DevExpress.XtraReports.UI.XRRichText

    'Required by the Component Designer
    Private components As System.ComponentModel.IContainer

    '<System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(CreditorInvoiceReport))
        Dim PostNetGenerator1 As DevExpress.XtraPrinting.BarCode.PostNetGenerator = New DevExpress.XtraPrinting.BarCode.PostNetGenerator
        Me.Subreport_CheckDetail = New DevExpress.XtraReports.UI.XRSubreport
        Me.CheckDetailReport1 = New DebtPlus.Reports.Creditor.Invoice.CheckDetailReport(Me.components)
        Me.Subreport_InvoiceStatus = New DevExpress.XtraReports.UI.XRSubreport
        Me.InvoiceStatusReport1 = New DebtPlus.Reports.Creditor.Invoice.InvoiceStatusReport(Me.components)
        Me.PageHeader = New DevExpress.XtraReports.UI.PageHeaderBand
        Me.XrLabel1 = New DevExpress.XtraReports.UI.XRLabel
        Me.XrLabel_CreditorName = New DevExpress.XtraReports.UI.XRLabel
        Me.PageFooter = New DevExpress.XtraReports.UI.PageFooterBand
        Me.XrPageInfo1 = New DevExpress.XtraReports.UI.XRPageInfo
        Me.Text_Footer = New DevExpress.XtraReports.UI.XRLabel
        Me.ReportHeader = New DevExpress.XtraReports.UI.ReportHeaderBand
        Me.XrRichText1 = New DevExpress.XtraReports.UI.XRRichText
        Me.XrLabel5 = New DevExpress.XtraReports.UI.XRLabel
        Me.XrLabel3 = New DevExpress.XtraReports.UI.XRLabel
        Me.XrPanel_CreditorAddress = New DevExpress.XtraReports.UI.XRPanel
        Me.XrLabel_Creditor_Address = New DevExpress.XtraReports.UI.XRLabel
        Me.XrBarCode_PostalCode = New DevExpress.XtraReports.UI.XRBarCode
        Me.XrLabel_PoNumber = New DevExpress.XtraReports.UI.XRLabel
        Me.XrPanel_Aging = New DevExpress.XtraReports.UI.XRPanel
        Me.XrLabel_Aging_Net120 = New DevExpress.XtraReports.UI.XRLabel
        Me.XrLabel17 = New DevExpress.XtraReports.UI.XRLabel
        Me.XrLabel_Aging_net90 = New DevExpress.XtraReports.UI.XRLabel
        Me.XrLabel15 = New DevExpress.XtraReports.UI.XRLabel
        Me.XrLabel_Aging_Net60 = New DevExpress.XtraReports.UI.XRLabel
        Me.XrLabel13 = New DevExpress.XtraReports.UI.XRLabel
        Me.XrLabel_Aging_Net30 = New DevExpress.XtraReports.UI.XRLabel
        Me.XrLabel11 = New DevExpress.XtraReports.UI.XRLabel
        Me.XrLabel_Aging_Current = New DevExpress.XtraReports.UI.XRLabel
        Me.XrLabel9 = New DevExpress.XtraReports.UI.XRLabel
        Me.XrLine1 = New DevExpress.XtraReports.UI.XRLine
        Me.XrLabel8 = New DevExpress.XtraReports.UI.XRLabel
        Me.XrLabel7 = New DevExpress.XtraReports.UI.XRLabel
        Me.XrLabel_Aging_Total = New DevExpress.XtraReports.UI.XRLabel
        Me.XrLabel_Aging_Creditor = New DevExpress.XtraReports.UI.XRLabel
        Me.XrLabel4 = New DevExpress.XtraReports.UI.XRLabel
        Me.XrLabel_Aging_Date = New DevExpress.XtraReports.UI.XRLabel
        Me.XrLabel2 = New DevExpress.XtraReports.UI.XRLabel
        Me.ParameterCreditor = New DevExpress.XtraReports.Parameters.Parameter
        CType(Me.CheckDetailReport1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.InvoiceStatusReport1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.XrRichText1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
        '
        'Detail
        '
        Me.Detail.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.Subreport_CheckDetail, Me.Subreport_InvoiceStatus})
        Me.Detail.HeightF = 46.0!
        Me.Detail.Name = "Detail"
        Me.Detail.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
        Me.Detail.PageBreak = DevExpress.XtraReports.UI.PageBreak.AfterBand
        Me.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'Subreport_CheckDetail
        '
        Me.Subreport_CheckDetail.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 23.0!)
        Me.Subreport_CheckDetail.Name = "Subreport_CheckDetail"
        Me.Subreport_CheckDetail.ReportSource = Me.CheckDetailReport1
        Me.Subreport_CheckDetail.Scripts.OnBeforePrint = "Subreport_CheckDetail_BeforePrint"
        Me.Subreport_CheckDetail.SizeF = New System.Drawing.SizeF(800.0!, 23.0!)
        '
        'Subreport_InvoiceStatus
        '
        Me.Subreport_InvoiceStatus.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
        Me.Subreport_InvoiceStatus.Name = "Subreport_InvoiceStatus"
        Me.Subreport_InvoiceStatus.ReportSource = Me.InvoiceStatusReport1
        Me.Subreport_InvoiceStatus.Scripts.OnBeforePrint = "Subreport_InvoiceStatus_BeforePrint"
        Me.Subreport_InvoiceStatus.SizeF = New System.Drawing.SizeF(800.0!, 23.0!)
        '
        'PageHeader
        '
        Me.PageHeader.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel1, Me.XrLabel_CreditorName})
        Me.PageHeader.HeightF = 64.0!
        Me.PageHeader.Name = "PageHeader"
        Me.PageHeader.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
        Me.PageHeader.PrintOn = DevExpress.XtraReports.UI.PrintOnPages.NotWithReportHeader
        Me.PageHeader.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'XrLabel1
        '
        Me.XrLabel1.Font = New System.Drawing.Font("Times New Roman", 10.0!, System.Drawing.FontStyle.Bold)
        Me.XrLabel1.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
        Me.XrLabel1.Name = "XrLabel1"
        Me.XrLabel1.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel1.SizeF = New System.Drawing.SizeF(789.9999!, 22.99999!)
        Me.XrLabel1.StylePriority.UseFont = False
        Me.XrLabel1.StylePriority.UseTextAlignment = False
        Me.XrLabel1.Text = "Consumer Credit Counseling Service of Orange County"
        Me.XrLabel1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
        '
        'XrLabel_CreditorName
        '
        Me.XrLabel_CreditorName.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.XrLabel_CreditorName.BorderColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.XrLabel_CreditorName.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.XrLabel_CreditorName.BorderWidth = 1
        Me.XrLabel_CreditorName.CanGrow = False
        Me.XrLabel_CreditorName.Font = New System.Drawing.Font("Times New Roman", 14.0!, System.Drawing.FontStyle.Bold)
        Me.XrLabel_CreditorName.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.XrLabel_CreditorName.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 28.00001!)
        Me.XrLabel_CreditorName.Name = "XrLabel_CreditorName"
        Me.XrLabel_CreditorName.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel_CreditorName.SizeF = New System.Drawing.SizeF(789.9999!, 26.00002!)
        Me.XrLabel_CreditorName.StylePriority.UseFont = False
        Me.XrLabel_CreditorName.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter
        '
        'PageFooter
        '
        Me.PageFooter.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrPageInfo1, Me.Text_Footer})
        Me.PageFooter.HeightF = 40.99998!
        Me.PageFooter.Name = "PageFooter"
        Me.PageFooter.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
        Me.PageFooter.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'XrPageInfo1
        '
        Me.XrPageInfo1.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
        Me.XrPageInfo1.ForeColor = System.Drawing.Color.Teal
        Me.XrPageInfo1.Format = "Page {0}"
        Me.XrPageInfo1.LocationFloat = New DevExpress.Utils.PointFloat(700.0!, 8.0!)
        Me.XrPageInfo1.Name = "XrPageInfo1"
        Me.XrPageInfo1.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrPageInfo1.PageInfo = DevExpress.XtraPrinting.PageInfo.Number
        Me.XrPageInfo1.SizeF = New System.Drawing.SizeF(100.0!, 17.0!)
        Me.XrPageInfo1.StylePriority.UseFont = False
        Me.XrPageInfo1.StylePriority.UseForeColor = False
        Me.XrPageInfo1.StylePriority.UseTextAlignment = False
        Me.XrPageInfo1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
        '
        'Text_Footer
        '
        Me.Text_Footer.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.Text_Footer.BorderColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Text_Footer.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.Text_Footer.BorderWidth = 1
        Me.Text_Footer.CanGrow = False
        Me.Text_Footer.Font = New System.Drawing.Font("Times New Roman", 10.0!)
        Me.Text_Footer.ForeColor = System.Drawing.Color.FromArgb(CType(CType(128, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(128, Byte), Integer))
        Me.Text_Footer.KeepTogether = True
        Me.Text_Footer.LocationFloat = New DevExpress.Utils.PointFloat(187.5!, 7.999992!)
        Me.Text_Footer.Multiline = True
        Me.Text_Footer.Name = "Text_Footer"
        Me.Text_Footer.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.Text_Footer.Scripts.OnPrintOnPage = "Text_Footer_PrintOnPage"
        Me.Text_Footer.SizeF = New System.Drawing.SizeF(429.1667!, 32.99999!)
        Me.Text_Footer.Text = "PLEASE RETURN THIS STATEMENT WITH YOUR REMITTANCE" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "THANK YOU"
        Me.Text_Footer.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter
        '
        'ReportHeader
        '
        Me.ReportHeader.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrRichText1, Me.XrLabel5, Me.XrLabel3, Me.XrPanel_CreditorAddress, Me.XrLabel_PoNumber, Me.XrPanel_Aging})
        Me.ReportHeader.HeightF = 369.625!
        Me.ReportHeader.Name = "ReportHeader"
        '
        'XrRichText1
        '
        Me.XrRichText1.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
        Me.XrRichText1.Name = "XrRichText1"
        Me.XrRichText1.SerializableRtfString = resources.GetString("XrRichText1.SerializableRtfString")
        Me.XrRichText1.SizeF = New System.Drawing.SizeF(790.0!, 100.0!)
        '
        'XrLabel5
        '
        Me.XrLabel5.Borders = DevExpress.XtraPrinting.BorderSide.Top
        Me.XrLabel5.LocationFloat = New DevExpress.Utils.PointFloat(2.000077!, 346.625!)
        Me.XrLabel5.Name = "XrLabel5"
        Me.XrLabel5.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel5.SizeF = New System.Drawing.SizeF(789.9999!, 23.0!)
        Me.XrLabel5.StylePriority.UseBorders = False
        Me.XrLabel5.StylePriority.UseTextAlignment = False
        Me.XrLabel5.Text = "Please return the top portion with your remittance"
        Me.XrLabel5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter
        '
        'XrLabel3
        '
        Me.XrLabel3.Font = New System.Drawing.Font("Times New Roman", 8.0!, System.Drawing.FontStyle.Bold)
        Me.XrLabel3.LocationFloat = New DevExpress.Utils.PointFloat(7.999992!, 125.0!)
        Me.XrLabel3.Name = "XrLabel3"
        Me.XrLabel3.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel3.SizeF = New System.Drawing.SizeF(461.4583!, 16.75!)
        Me.XrLabel3.StylePriority.UseFont = False
        Me.XrLabel3.Text = "Contribution Request: Payments distributed through DebtPlus Debt Management Progr" & _
            "ams"
        '
        'XrPanel_CreditorAddress
        '
        Me.XrPanel_CreditorAddress.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel_Creditor_Address, Me.XrBarCode_PostalCode})
        Me.XrPanel_CreditorAddress.LocationFloat = New DevExpress.Utils.PointFloat(75.0!, 220.0!)
        Me.XrPanel_CreditorAddress.Name = "XrPanel_CreditorAddress"
        Me.XrPanel_CreditorAddress.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
        Me.XrPanel_CreditorAddress.SizeF = New System.Drawing.SizeF(300.0!, 100.0!)
        '
        'XrLabel_Creditor_Address
        '
        Me.XrLabel_Creditor_Address.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 17.00002!)
        Me.XrLabel_Creditor_Address.Multiline = True
        Me.XrLabel_Creditor_Address.Name = "XrLabel_Creditor_Address"
        Me.XrLabel_Creditor_Address.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel_Creditor_Address.Scripts.OnBeforePrint = "XrLabel_Creditor_Address_BeforePrint"
        Me.XrLabel_Creditor_Address.SizeF = New System.Drawing.SizeF(300.0!, 82.99998!)
        Me.XrLabel_Creditor_Address.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'XrBarCode_PostalCode
        '
        Me.XrBarCode_PostalCode.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
        Me.XrBarCode_PostalCode.Name = "XrBarCode_PostalCode"
        Me.XrBarCode_PostalCode.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
        Me.XrBarCode_PostalCode.Scripts.OnBeforePrint = "XrBarCode_PostalCode_BeforePrint"
        Me.XrBarCode_PostalCode.ShowText = False
        Me.XrBarCode_PostalCode.SizeF = New System.Drawing.SizeF(300.0!, 16.0!)
        Me.XrBarCode_PostalCode.Symbology = PostNetGenerator1
        Me.XrBarCode_PostalCode.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'XrLabel_PoNumber
        '
        Me.XrLabel_PoNumber.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.XrLabel_PoNumber.BorderColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.XrLabel_PoNumber.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.XrLabel_PoNumber.BorderWidth = 1
        Me.XrLabel_PoNumber.Font = New System.Drawing.Font("Times New Roman", 10.0!)
        Me.XrLabel_PoNumber.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.XrLabel_PoNumber.LocationFloat = New DevExpress.Utils.PointFloat(75.0!, 157.0!)
        Me.XrLabel_PoNumber.Name = "XrLabel_PoNumber"
        Me.XrLabel_PoNumber.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel_PoNumber.SizeF = New System.Drawing.SizeF(400.0!, 19.0!)
        Me.XrLabel_PoNumber.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'XrPanel_Aging
        '
        Me.XrPanel_Aging.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel_Aging_Net120, Me.XrLabel17, Me.XrLabel_Aging_net90, Me.XrLabel15, Me.XrLabel_Aging_Net60, Me.XrLabel13, Me.XrLabel_Aging_Net30, Me.XrLabel11, Me.XrLabel_Aging_Current, Me.XrLabel9, Me.XrLine1, Me.XrLabel8, Me.XrLabel7, Me.XrLabel_Aging_Total, Me.XrLabel_Aging_Creditor, Me.XrLabel4, Me.XrLabel_Aging_Date, Me.XrLabel2})
        Me.XrPanel_Aging.LocationFloat = New DevExpress.Utils.PointFloat(507.0!, 142.0!)
        Me.XrPanel_Aging.Name = "XrPanel_Aging"
        Me.XrPanel_Aging.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
        Me.XrPanel_Aging.Scripts.OnBeforePrint = "XrPanel_Aging_BeforePrint"
        Me.XrPanel_Aging.SizeF = New System.Drawing.SizeF(283.0!, 185.0!)
        '
        'XrLabel_Aging_Net120
        '
        Me.XrLabel_Aging_Net120.Font = New System.Drawing.Font("Times New Roman", 9.75!)
        Me.XrLabel_Aging_Net120.LocationFloat = New DevExpress.Utils.PointFloat(150.0!, 168.0!)
        Me.XrLabel_Aging_Net120.Name = "XrLabel_Aging_Net120"
        Me.XrLabel_Aging_Net120.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel_Aging_Net120.SizeF = New System.Drawing.SizeF(125.0!, 17.0!)
        Me.XrLabel_Aging_Net120.Text = "$0.00"
        Me.XrLabel_Aging_Net120.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'XrLabel17
        '
        Me.XrLabel17.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 168.0!)
        Me.XrLabel17.Name = "XrLabel17"
        Me.XrLabel17.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel17.SizeF = New System.Drawing.SizeF(142.0!, 17.0!)
        Me.XrLabel17.Text = "120 and Over"
        Me.XrLabel17.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'XrLabel_Aging_net90
        '
        Me.XrLabel_Aging_net90.Font = New System.Drawing.Font("Times New Roman", 9.75!)
        Me.XrLabel_Aging_net90.LocationFloat = New DevExpress.Utils.PointFloat(150.0!, 151.0!)
        Me.XrLabel_Aging_net90.Name = "XrLabel_Aging_net90"
        Me.XrLabel_Aging_net90.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel_Aging_net90.SizeF = New System.Drawing.SizeF(125.0!, 17.0!)
        Me.XrLabel_Aging_net90.Text = "$0.00"
        Me.XrLabel_Aging_net90.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'XrLabel15
        '
        Me.XrLabel15.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 151.0!)
        Me.XrLabel15.Name = "XrLabel15"
        Me.XrLabel15.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel15.SizeF = New System.Drawing.SizeF(142.0!, 17.0!)
        Me.XrLabel15.Text = "90 Days"
        Me.XrLabel15.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'XrLabel_Aging_Net60
        '
        Me.XrLabel_Aging_Net60.Font = New System.Drawing.Font("Times New Roman", 9.75!)
        Me.XrLabel_Aging_Net60.LocationFloat = New DevExpress.Utils.PointFloat(150.0!, 134.0!)
        Me.XrLabel_Aging_Net60.Name = "XrLabel_Aging_Net60"
        Me.XrLabel_Aging_Net60.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel_Aging_Net60.SizeF = New System.Drawing.SizeF(125.0!, 17.0!)
        Me.XrLabel_Aging_Net60.Text = "$0.00"
        Me.XrLabel_Aging_Net60.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'XrLabel13
        '
        Me.XrLabel13.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 134.0!)
        Me.XrLabel13.Name = "XrLabel13"
        Me.XrLabel13.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel13.SizeF = New System.Drawing.SizeF(142.0!, 17.0!)
        Me.XrLabel13.Text = "60 Days"
        Me.XrLabel13.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'XrLabel_Aging_Net30
        '
        Me.XrLabel_Aging_Net30.Font = New System.Drawing.Font("Times New Roman", 9.75!)
        Me.XrLabel_Aging_Net30.LocationFloat = New DevExpress.Utils.PointFloat(150.0!, 117.0!)
        Me.XrLabel_Aging_Net30.Name = "XrLabel_Aging_Net30"
        Me.XrLabel_Aging_Net30.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel_Aging_Net30.SizeF = New System.Drawing.SizeF(125.0!, 17.0!)
        Me.XrLabel_Aging_Net30.Text = "$0.00"
        Me.XrLabel_Aging_Net30.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'XrLabel11
        '
        Me.XrLabel11.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 117.0!)
        Me.XrLabel11.Name = "XrLabel11"
        Me.XrLabel11.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel11.SizeF = New System.Drawing.SizeF(142.0!, 17.0!)
        Me.XrLabel11.Text = "30 Days"
        Me.XrLabel11.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'XrLabel_Aging_Current
        '
        Me.XrLabel_Aging_Current.Font = New System.Drawing.Font("Times New Roman", 9.75!)
        Me.XrLabel_Aging_Current.LocationFloat = New DevExpress.Utils.PointFloat(150.0!, 100.0!)
        Me.XrLabel_Aging_Current.Name = "XrLabel_Aging_Current"
        Me.XrLabel_Aging_Current.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel_Aging_Current.SizeF = New System.Drawing.SizeF(125.0!, 17.0!)
        Me.XrLabel_Aging_Current.Text = "$0.00"
        Me.XrLabel_Aging_Current.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'XrLabel9
        '
        Me.XrLabel9.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 100.0!)
        Me.XrLabel9.Name = "XrLabel9"
        Me.XrLabel9.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel9.SizeF = New System.Drawing.SizeF(142.0!, 17.0!)
        Me.XrLabel9.Text = "Current Amount Due:"
        Me.XrLabel9.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'XrLine1
        '
        Me.XrLine1.LineWidth = 2
        Me.XrLine1.LocationFloat = New DevExpress.Utils.PointFloat(150.0!, 82.99998!)
        Me.XrLine1.Name = "XrLine1"
        Me.XrLine1.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
        Me.XrLine1.SizeF = New System.Drawing.SizeF(125.0!, 9.000015!)
        '
        'XrLabel8
        '
        Me.XrLabel8.LocationFloat = New DevExpress.Utils.PointFloat(17.0!, 75.0!)
        Me.XrLabel8.Name = "XrLabel8"
        Me.XrLabel8.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel8.SizeF = New System.Drawing.SizeF(117.0!, 17.0!)
        Me.XrLabel8.Text = "Amount Enclosed:"
        Me.XrLabel8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'XrLabel7
        '
        Me.XrLabel7.LocationFloat = New DevExpress.Utils.PointFloat(17.0!, 34.0!)
        Me.XrLabel7.Name = "XrLabel7"
        Me.XrLabel7.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel7.SizeF = New System.Drawing.SizeF(117.0!, 17.0!)
        Me.XrLabel7.Text = "Total Amount Due:"
        Me.XrLabel7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'XrLabel_Aging_Total
        '
        Me.XrLabel_Aging_Total.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
        Me.XrLabel_Aging_Total.LocationFloat = New DevExpress.Utils.PointFloat(150.0!, 34.00002!)
        Me.XrLabel_Aging_Total.Name = "XrLabel_Aging_Total"
        Me.XrLabel_Aging_Total.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel_Aging_Total.SizeF = New System.Drawing.SizeF(125.0!, 17.0!)
        Me.XrLabel_Aging_Total.Text = "$0.00"
        Me.XrLabel_Aging_Total.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'XrLabel_Aging_Creditor
        '
        Me.XrLabel_Aging_Creditor.LocationFloat = New DevExpress.Utils.PointFloat(183.0!, 17.00001!)
        Me.XrLabel_Aging_Creditor.Name = "XrLabel_Aging_Creditor"
        Me.XrLabel_Aging_Creditor.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel_Aging_Creditor.SizeF = New System.Drawing.SizeF(92.0!, 17.0!)
        Me.XrLabel_Aging_Creditor.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'XrLabel4
        '
        Me.XrLabel4.LocationFloat = New DevExpress.Utils.PointFloat(17.0!, 17.0!)
        Me.XrLabel4.Name = "XrLabel4"
        Me.XrLabel4.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel4.SizeF = New System.Drawing.SizeF(92.0!, 17.0!)
        Me.XrLabel4.Text = "Creditor:"
        Me.XrLabel4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'XrLabel_Aging_Date
        '
        Me.XrLabel_Aging_Date.LocationFloat = New DevExpress.Utils.PointFloat(183.0!, 0.0!)
        Me.XrLabel_Aging_Date.Name = "XrLabel_Aging_Date"
        Me.XrLabel_Aging_Date.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel_Aging_Date.SizeF = New System.Drawing.SizeF(92.0!, 17.0!)
        Me.XrLabel_Aging_Date.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'XrLabel2
        '
        Me.XrLabel2.LocationFloat = New DevExpress.Utils.PointFloat(17.0!, 0.0!)
        Me.XrLabel2.Name = "XrLabel2"
        Me.XrLabel2.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel2.SizeF = New System.Drawing.SizeF(92.0!, 17.0!)
        Me.XrLabel2.Text = "Billing Date:"
        Me.XrLabel2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'ParameterCreditor
        '
        Me.ParameterCreditor.Description = "Creditor ID"
        Me.ParameterCreditor.Name = "ParameterCreditor"
        Me.ParameterCreditor.Value = ""
        '
        'CreditorInvoiceReport
        '
        Me.Bands.AddRange(New DevExpress.XtraReports.UI.Band() {Me.PageHeader, Me.PageFooter, Me.ReportHeader})
        Me.Font = New System.Drawing.Font("Times New Roman", 10.0!)
        Me.PageWidth = 852
        Me.PaperKind = System.Drawing.Printing.PaperKind.Custom
        Me.Parameters.AddRange(New DevExpress.XtraReports.Parameters.Parameter() {Me.ParameterCreditor})
        Me.RequestParameters = False
        Me.ScriptLanguage = DevExpress.XtraReports.ScriptLanguage.VisualBasic
        Me.ScriptReferencesString = "C:\Documents and Settings\Al Longyear\My Documents\Visual Studio 2008\Projects\De" & _
            "btPlus\Executables\DebtPlus.Data.dll"
        Me.Scripts.OnBeforePrint = "CreditorInvoiceReport_BeforePrint"
        Me.ScriptsSource = resources.GetString("$this.ScriptsSource")
        Me.Version = "10.2"
        CType(Me.CheckDetailReport1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.InvoiceStatusReport1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.XrRichText1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me, System.ComponentModel.ISupportInitialize).EndInit()

    End Sub
    'NOTE: The following procedure is required by the Component Designer
    'It can be modified using the Component Designer.
    'Do not modify it using the code editor.

    Friend WithEvents PageHeader As DevExpress.XtraReports.UI.PageHeaderBand
    Friend WithEvents PageFooter As DevExpress.XtraReports.UI.PageFooterBand
    Friend WithEvents Subreport_InvoiceStatus As DevExpress.XtraReports.UI.XRSubreport
    Friend WithEvents Subreport_CheckDetail As DevExpress.XtraReports.UI.XRSubreport
    Friend WithEvents Text_Footer As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrPageInfo1 As DevExpress.XtraReports.UI.XRPageInfo
    Friend WithEvents XrLabel_CreditorName As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents ReportHeader As DevExpress.XtraReports.UI.ReportHeaderBand
    Friend WithEvents XrPanel_CreditorAddress As DevExpress.XtraReports.UI.XRPanel
    Friend WithEvents XrLabel_Creditor_Address As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrBarCode_PostalCode As DevExpress.XtraReports.UI.XRBarCode
    Friend WithEvents XrLabel_PoNumber As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrPanel_Aging As DevExpress.XtraReports.UI.XRPanel
    Friend WithEvents XrLabel_Aging_Net120 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel17 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel_Aging_net90 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel15 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel_Aging_Net60 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel13 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel_Aging_Net30 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel11 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel_Aging_Current As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel9 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLine1 As DevExpress.XtraReports.UI.XRLine
    Friend WithEvents XrLabel8 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel7 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel_Aging_Total As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel_Aging_Creditor As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel4 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel_Aging_Date As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel2 As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents CheckDetailReport1 As DebtPlus.Reports.Creditor.Invoice.CheckDetailReport
    Private WithEvents InvoiceStatusReport1 As DebtPlus.Reports.Creditor.Invoice.InvoiceStatusReport
    Friend WithEvents ParameterCreditor As DevExpress.XtraReports.Parameters.Parameter
#End Region

    Public ds As New System.Data.DataSet("ds")

    Private Sub Report_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs)
        Dim rpt As DevExpress.XtraReports.UI.XtraReport = CType(sender, DevExpress.XtraReports.UI.XtraReport)
        Dim sqlInfo As DebtPlus.LINQ.SQLInfoClass = DebtPlus.LINQ.SQLInfoClass.getDefault()

        With rpt
            Dim tbl As System.Data.DataTable = ds.Tables("rpt_creditor_invoice")
            If tbl IsNot Nothing Then
                tbl.Clear()
            End If

            '-- Define the parameters for the report
            Dim cn As System.Data.SqlClient.SqlConnection = New System.Data.SqlClient.SqlConnection(sqlInfo.ConnectionString)
            Using cmd As New System.Data.SqlClient.SqlCommand()
                With cmd
                    .Connection = cn
                    .CommandText = "rpt_creditor_invoice"
                    .CommandType = System.Data.CommandType.StoredProcedure
                    .CommandTimeout = 0
                    .Parameters.Add("@creditor", System.Data.SqlDbType.VarChar, 10).Value = rpt.Parameters("ParameterCreditor").Value
                End With

                '-- Read the datasource
                Using da As New System.Data.SqlClient.SqlDataAdapter(cmd)
                    da.Fill(ds, "rpt_creditor_invoice")
                    tbl = ds.Tables("rpt_creditor_invoice")
                End Using
            End Using

            '-- Find the datasource
            rpt.DataSource = tbl.DefaultView

            '-- Set the PO Number
            Dim NameString As String = String.Empty
            Dim ctl As DevExpress.XtraReports.UI.XRLabel
            If tbl.Rows.Count > 0 Then
                Dim row As System.Data.DataRow = tbl.Rows(0)
                If row("creditor_name") IsNot System.DBNull.Value Then NameString = Convert.ToString(row("creditor_name"), System.Globalization.CultureInfo.InvariantCulture).Trim

                ctl = TryCast(rpt.FindControl("XrLabel_PoNumber", True), DevExpress.XtraReports.UI.XRLabel)
                If ctl IsNot Nothing Then
                    If row("po_number") IsNot System.DBNull.Value Then ctl.Text = "P.O. Number: " + Convert.ToString(row("po_number"), System.Globalization.CultureInfo.InvariantCulture)
                End If
            End If

            ctl = TryCast(rpt.FindControl("XrLabel_CreditorName", True), DevExpress.XtraReports.UI.XRLabel)
            If ctl IsNot Nothing Then
                If NameString <> String.Empty Then
                    ctl.Text = String.Format("{0} ({1})", NameString, CType(rpt.Parameters("ParameterCreditor").Value, String))
                Else
                    ctl.Text = CType(rpt.Parameters("ParameterCreditor").Value, String)
                End If
            End If
        End With
    End Sub

    Private Sub Subreport_InvoiceStatus_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs)
        With CType(sender, DevExpress.XtraReports.UI.XRSubreport)
            Dim rpt As DevExpress.XtraReports.UI.XtraReport = CType(.ReportSource, DevExpress.XtraReports.UI.XtraReport)
            Dim MasterRpt As DevExpress.XtraReports.UI.XtraReport = CType(.Report, DevExpress.XtraReports.UI.XtraReport)
            Dim sqlInfo As DebtPlus.LINQ.SQLInfoClass = DebtPlus.LINQ.SQLInfoClass.getDefault()

            Dim ds As New System.Data.DataSet("ds")
            Dim tbl As System.Data.DataTable

            Dim cn As System.Data.SqlClient.SqlConnection = New System.Data.SqlClient.SqlConnection(sqlInfo.ConnectionString)
            Try
                cn.Open()
                Using cmd As New System.Data.SqlClient.SqlCommand()
                    With cmd
                        .Connection = cn
                        .CommandText = "rpt_creditor_invoice_detail"
                        .CommandType = System.Data.CommandType.StoredProcedure
                        .Parameters.Add("@creditor", System.Data.SqlDbType.VarChar, 80).Value = MasterRpt.Parameters("ParameterCreditor").Value
                        .CommandTimeout = 0
                    End With

                    Using da As New System.Data.SqlClient.SqlDataAdapter(cmd)
                        da.Fill(ds, "rpt_creditor_invoice_detail")
                        tbl = ds.Tables("rpt_creditor_invoice_detail")
                        With tbl
                            If Not .Columns.Contains("net_amt") Then
                                .Columns.Add(New System.Data.DataColumn("net_amt", GetType(Decimal), "isnull([inv_amt],0)-isnull([adj_amt],0)-isnull([pmt_amt],0)"))
                            End If
                        End With
                    End Using
                End Using

                rpt.DataSource = tbl.DefaultView

            Finally
                If cn IsNot Nothing Then
                    If cn.State <> System.Data.ConnectionState.Closed Then cn.Close()
                    cn.Dispose()
                End If
            End Try
        End With
    End Sub

    Private Sub Subreport_CheckDetail_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs)
        With CType(sender, DevExpress.XtraReports.UI.XRSubreport)
            Dim MasterReport As DevExpress.XtraReports.UI.XtraReport = CType(.Report, DevExpress.XtraReports.UI.XtraReport)
            Dim sqlInfo As DebtPlus.LINQ.SQLInfoClass = DebtPlus.LINQ.SQLInfoClass.getDefault()
            Dim SubReport As DevExpress.XtraReports.UI.XtraReport = CType(.ReportSource, DevExpress.XtraReports.UI.XtraReport)

            '-- Clear the current transactions for the invoice
            Dim tbl As System.Data.DataTable = ds.Tables("rpt_creditor_invoice_current")
            If tbl IsNot Nothing Then
                tbl.Clear()
            End If

            '-- Obtain the invoice data
            Using cmd As New System.Data.SqlClient.SqlCommand()
                With cmd
                    .Connection = New System.Data.SqlClient.SqlConnection(sqlInfo.ConnectionString)
                    .CommandText = "rpt_creditor_invoice_current"
                    .CommandType = System.Data.CommandType.StoredProcedure
                    .Parameters.Add("@creditor", System.Data.SqlDbType.VarChar, 80).Value = MasterReport.Parameters("ParameterCreditor").Value
                    .CommandTimeout = 0
                End With

                Using da As New System.Data.SqlClient.SqlDataAdapter(cmd)
                    da.Fill(ds, "rpt_creditor_invoice_current")
                    tbl = ds.Tables("rpt_creditor_invoice_current")
                End Using
            End Using

            If tbl.Rows.Count = 0 Then
                e.Cancel = True
            Else
                SubReport.DataSource = tbl.DefaultView
            End If
        End With
    End Sub

    Private Sub XrLabel_Creditor_Address_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs)
        With CType(sender, DevExpress.XtraReports.UI.XRLabel)
            Dim MasterReport As DevExpress.XtraReports.UI.XtraReport = CType(.Report, DevExpress.XtraReports.UI.XtraReport)
            Dim sqlInfo As DebtPlus.LINQ.SQLInfoClass = DebtPlus.LINQ.SQLInfoClass.getDefault()
            Dim ds As System.Data.DataSet = CType(MasterReport.DataSource, System.Data.DataView).Table.DataSet
            Dim tbl As System.Data.DataTable = ds.Tables("creditor_addresses")
            Dim row As System.Data.DataRow = Nothing
            Dim Creditor As String = Convert.ToString(MasterReport.Parameters("ParameterCreditor").Value)

            '-- If there is a table then find the row
            If tbl IsNot Nothing Then
                row = tbl.Rows.Find(Creditor)
            End If

            If row Is Nothing Then
                Dim cn As System.Data.SqlClient.SqlConnection = New System.Data.SqlClient.SqlConnection(sqlInfo.ConnectionString)
                Try
                    cn.Open()
                    Using cmd As New System.Data.SqlClient.SqlCommand()
                        With cmd
                            .Connection = cn
                            .CommandText = "rpt_CreditorAddress_I"
                            .CommandType = System.Data.CommandType.StoredProcedure
                            .Parameters.Add("@creditor", System.Data.SqlDbType.VarChar, 10).Value = Creditor
                            .CommandTimeout = 0
                        End With

                        Using da As New System.Data.SqlClient.SqlDataAdapter(cmd)
                            da.Fill(ds, "rpt_CreditorAddress_I")
                            tbl = ds.Tables("rpt_CreditorAddress_I")
                            With tbl
                                If .PrimaryKey.GetUpperBound(0) < 0 Then
                                    .PrimaryKey = New System.Data.DataColumn() {.Columns("creditor")}
                                End If
                            End With
                        End Using
                    End Using

                    If tbl IsNot Nothing Then
                        row = tbl.Rows.Find(Creditor)
                    End If

                Catch ex As System.Data.SqlClient.SqlException
                    Dim gdr As New DebtPlus.Repository.GetDataResult() : gdr.HandleException(ex) : DebtPlus.UI.Common.ErrorHandling.HandleErrors(gdr, "Error reading creditor address")
                End Try
            End If

            Dim text_block As New System.Text.StringBuilder
            If row IsNot Nothing Then
                For Each NameString As String In New String() {"addr1", "addr2", "addr3", "addr4", "addr5", "addr6", "addr7"}
                    Dim Value As String
                    If row(NameString) IsNot Nothing AndAlso row(NameString) IsNot System.DBNull.Value Then
                        Value = Convert.ToString(row(NameString)).Trim
                        If Value <> String.Empty Then
                            text_block.Append(Environment.NewLine)
                            text_block.Append(Value)
                        End If
                    End If
                Next
            End If

            If text_block.Length > 0 Then
                text_block.Remove(0, 2)
            End If

            .Text = text_block.ToString
        End With
    End Sub

    Private Sub XrBarCode_PostalCode_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs)
        With CType(sender, DevExpress.XtraReports.UI.XRBarCode)
            Dim MasterReport As DevExpress.XtraReports.UI.XtraReport = CType(.Report, DevExpress.XtraReports.UI.XtraReport)
            Dim sqlInfo As DebtPlus.LINQ.SQLInfoClass = DebtPlus.LINQ.SQLInfoClass.getDefault()
            Dim ds As System.Data.DataSet = CType(MasterReport.DataSource, System.Data.DataView).Table.DataSet
            Dim tbl As System.Data.DataTable = ds.Tables("rpt_CreditorAddress_I")
            Dim row As System.Data.DataRow = Nothing
            Dim Creditor As String = Convert.ToString(MasterReport.Parameters("ParameterCreditor").Value)

            '-- If there is a table then find the row
            If tbl IsNot Nothing Then
                row = tbl.Rows.Find(Creditor)
            End If

            If row Is Nothing Then
                Dim cn As System.Data.SqlClient.SqlConnection = New System.Data.SqlClient.SqlConnection(sqlInfo.ConnectionString)
                Try
                    cn.Open()
                    Using cmd As New System.Data.SqlClient.SqlCommand()
                        With cmd
                            .Connection = cn
                            .CommandText = "rpt_CreditorAddress_I"
                            .CommandType = System.Data.CommandType.StoredProcedure
                            .Parameters.Add("@creditor", System.Data.SqlDbType.VarChar, 10).Value = Creditor
                            .CommandTimeout = 0
                        End With

                        Using da As New System.Data.SqlClient.SqlDataAdapter(cmd)
                            da.Fill(ds, "rpt_CreditorAddress_I")
                            tbl = ds.Tables("rpt_CreditorAddress_I")
                            With tbl
                                If .PrimaryKey.GetUpperBound(0) < 0 Then
                                    .PrimaryKey = New System.Data.DataColumn() {.Columns("creditor")}
                                End If
                            End With
                        End Using
                    End Using

                    If tbl IsNot Nothing Then
                        row = tbl.Rows.Find(Creditor)
                    End If

                Catch ex As System.Data.SqlClient.SqlException
                    Dim gdr As New DebtPlus.Repository.GetDataResult() : gdr.HandleException(ex) : DebtPlus.UI.Common.ErrorHandling.HandleErrors(gdr, "Error reading creditor address")
                End Try
            End If

            Dim text_block As String = String.Empty
            If row IsNot Nothing Then
                If row("zipcode") IsNot Nothing AndAlso row("zipcode") IsNot System.DBNull.Value Then
                    text_block = Convert.ToString(row("zipcode"))
                End If
                text_block = System.Text.RegularExpressions.Regex.Replace(text_block, "[^0-9]", String.Empty)
            End If

            If text_block = String.Empty Then
                e.Cancel = True
            Else
                .Text = text_block
            End If
        End With
    End Sub

    Private Sub XrPanel_Aging_BeforePrint(ByVal sender As System.Object, ByVal e As System.Drawing.Printing.PrintEventArgs)
        With CType(sender, DevExpress.XtraReports.UI.XRPanel)
            Dim MasterReport As DevExpress.XtraReports.UI.XtraReport = CType(.Report, DevExpress.XtraReports.UI.XtraReport)
            Dim sqlInfo As DebtPlus.LINQ.SQLInfoClass = DebtPlus.LINQ.SQLInfoClass.getDefault()
            Dim Creditor As String = Convert.ToString(MasterReport.Parameters("ParameterCreditor").Value)

            Dim cn As System.Data.SqlClient.SqlConnection = New System.Data.SqlClient.SqlConnection(sqlInfo.ConnectionString)
            Dim rd As System.Data.SqlClient.SqlDataReader = Nothing

            Try
                cn.Open()

                Using cmd As New System.Data.SqlClient.SqlCommand()
                    With cmd
                        .Connection = cn
                        .CommandText = "rpt_creditor_invoice_aging"
                        .CommandType = System.Data.CommandType.StoredProcedure
                        .Parameters.Add("@creditor", System.Data.SqlDbType.VarChar, 10).Value = Creditor
                        .CommandTimeout = 0
                        rd = .ExecuteReader(System.Data.CommandBehavior.CloseConnection Or System.Data.CommandBehavior.SingleRow)
                    End With
                End Using

                If rd IsNot Nothing AndAlso rd.Read Then
                    .FindControl("XrLabel_Aging_Date", True).Text = rd.GetDateTime(rd.GetOrdinal("billing_date")).ToShortDateString
                    .FindControl("XrLabel_Aging_Creditor", True).Text = rd.GetString(rd.GetOrdinal("creditor"))
                    .FindControl("XrLabel_Aging_Current", True).Text = System.String.Format("{0:c}", rd.GetDecimal(rd.GetOrdinal("current")))
                    .FindControl("XrLabel_Aging_Net30", True).Text = System.String.Format("{0:c}", rd.GetDecimal(rd.GetOrdinal("net_30")))
                    .FindControl("XrLabel_Aging_Net60", True).Text = System.String.Format("{0:c}", rd.GetDecimal(rd.GetOrdinal("net_60")))
                    .FindControl("XrLabel_Aging_net90", True).Text = System.String.Format("{0:c}", rd.GetDecimal(rd.GetOrdinal("net_90")))
                    .FindControl("XrLabel_Aging_Net120", True).Text = System.String.Format("{0:c}", rd.GetDecimal(rd.GetOrdinal("net_120")))
                    .FindControl("XrLabel_Aging_Total", True).Text = System.String.Format("{0:c}", rd.GetDecimal(rd.GetOrdinal("total_due")))
                End If

            Finally
                If rd IsNot Nothing AndAlso Not rd.IsClosed Then rd.Close()
                If cn IsNot Nothing Then
                    If cn.State <> System.Data.ConnectionState.Closed Then cn.Close()
                    cn.Dispose()
                End If
            End Try
        End With
    End Sub

    Private Sub Text_Footer_PrintOnPage(ByVal sender As Object, ByVal e As DevExpress.XtraReports.UI.PrintOnPageEventArgs)
        e.Cancel = (e.PageIndex <> 0)
    End Sub
End Class
