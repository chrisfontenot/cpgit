#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}
#End Region

Imports DevExpress.XtraReports.UI

Public Class CheckDetailReport
    Inherits DevExpress.XtraReports.UI.XtraReport

	Private sqlInfo As New DebtPlus.Utils.SqlInfoClass()
    Private ds As New System.Data.DataSet("ds")

    Public Sub New()
        MyBase.New()
        InitializeComponent()

        '-- Bind the fields so that they may be scripted later
        Me.XrLabel_Date.DataBindings.Add("Text", Nothing, "date_created", "{0:d}")
        Me.XrLabel_Account.DataBindings.Add("Text", Nothing, "account_number")
        Me.XrLabel_Check.DataBindings.Add("Text", Nothing, "check_number")
        Me.XrLabel_Name.DataBindings.Add("Text", Nothing, "client_name")
        Me.XrLabel_Payment.DataBindings.Add("Text", Nothing, "amount", "{0:c}")
        Me.XrLabel_Fairshare.DataBindings.Add("Text", Nothing, "billed", "{0:c}")
        Me.XrLabel_Total_Fairshare.DataBindings.Add("Text", Nothing, "billed")
        Me.XrLabel_Total_Payment.DataBindings.Add("Text", Nothing, "amount")

        'AddHandler XrLabel_Client.BeforePrint, AddressOf XrLabel_Client_BeforePrint
        'AddHandler XrLabel_Invoice_Number.BeforePrint, AddressOf XrLabel_Invoice_Number_BeforePrint
        'AddHandler XrLabel_EFT.BeforePrint, AddressOf XRLabel_EFT_BeforePrint
        'AddHandler XrTableCell_Invoice_Subtotal_2.BeforePrint, AddressOf XrTableCell_Invoice_Subtotal_2_BeforePrint
        'AddHandler XrTableCell_Adjustment_Subtotal_2.BeforePrint, AddressOf XrTableCell_Adjustment_Subtotal_2_BeforePrint
        'AddHandler XrTableCell_Payment_Subtotal_2.BeforePrint, AddressOf XrTableCell_Payment_Subtotal_2_BeforePrint
        'AddHandler XrTableCell_Net_Subtotal_2.BeforePrint, AddressOf XrTableCell_Net_Subtotal_2_BeforePrint
        'AddHandler XrTableRow_AdjustmentTotal.BeforePrint, AddressOf XrTableRow_AdjustmentTotal_PrintOnPage
        'AddHandler XrTableRow_InvoiceSubtotal.BeforePrint, AddressOf XrTableRow_AdjustmentTotal_PrintOnPage
        'AddHandler XrTableRow_Payments.PrintOnPage, AddressOf XrTableRow_AdjustmentTotal_PrintOnPage
    End Sub

#Region " Component Designer generated code "

    'Component overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If components IsNot Nothing Then
                components.Dispose()
            End If
            ds.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Component Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Component Designer
    'It can be modified using the Component Designer.
    'Do not modify it using the code editor.
    Friend WithEvents DetailBand1 As DevExpress.XtraReports.UI.DetailBand
    Friend WithEvents GroupHeader1 As DevExpress.XtraReports.UI.GroupHeaderBand
    Friend WithEvents XrPanel1 As DevExpress.XtraReports.UI.XRPanel
    Friend WithEvents XrLabel2 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel3 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel4 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel5 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel6 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel7 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel8 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents GroupFooter_CheckTotals As DevExpress.XtraReports.UI.GroupFooterBand
    Friend WithEvents XrLabel9 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLine1 As DevExpress.XtraReports.UI.XRLine
    Friend WithEvents XrLabel_Total_Payment As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel_Total_Fairshare As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel_EFT As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrPanel_Detail As DevExpress.XtraReports.UI.XRPanel
    Friend WithEvents XrLabel_Fairshare As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel_Account As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel_Client As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel_Payment As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel_Name As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel_Check As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel_Date As DevExpress.XtraReports.UI.XRLabel
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim XrSummary1 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary
        Dim XrSummary2 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(CheckDetailReport))
        Me.DetailBand1 = New DevExpress.XtraReports.UI.DetailBand
        Me.XrPanel_Detail = New DevExpress.XtraReports.UI.XRPanel
        Me.XrLabel_Date = New DevExpress.XtraReports.UI.XRLabel
        Me.XrLabel_Check = New DevExpress.XtraReports.UI.XRLabel
        Me.XrLabel_Name = New DevExpress.XtraReports.UI.XRLabel
        Me.XrLabel_Payment = New DevExpress.XtraReports.UI.XRLabel
        Me.XrLabel_Client = New DevExpress.XtraReports.UI.XRLabel
        Me.XrLabel_Account = New DevExpress.XtraReports.UI.XRLabel
        Me.XrLabel_Fairshare = New DevExpress.XtraReports.UI.XRLabel
        Me.GroupHeader1 = New DevExpress.XtraReports.UI.GroupHeaderBand
        Me.XrPanel1 = New DevExpress.XtraReports.UI.XRPanel
        Me.XrLabel8 = New DevExpress.XtraReports.UI.XRLabel
        Me.XrLabel7 = New DevExpress.XtraReports.UI.XRLabel
        Me.XrLabel6 = New DevExpress.XtraReports.UI.XRLabel
        Me.XrLabel5 = New DevExpress.XtraReports.UI.XRLabel
        Me.XrLabel4 = New DevExpress.XtraReports.UI.XRLabel
        Me.XrLabel3 = New DevExpress.XtraReports.UI.XRLabel
        Me.XrLabel2 = New DevExpress.XtraReports.UI.XRLabel
        Me.GroupFooter_CheckTotals = New DevExpress.XtraReports.UI.GroupFooterBand
        Me.XrLabel_EFT = New DevExpress.XtraReports.UI.XRLabel
        Me.XrLabel_Total_Fairshare = New DevExpress.XtraReports.UI.XRLabel
        Me.XrLabel_Total_Payment = New DevExpress.XtraReports.UI.XRLabel
        Me.XrLine1 = New DevExpress.XtraReports.UI.XRLine
        Me.XrLabel9 = New DevExpress.XtraReports.UI.XRLabel
        Me.XrLabel_Invoice_Number = New DevExpress.XtraReports.UI.XRLabel
        Me.XrLabel1 = New DevExpress.XtraReports.UI.XRLabel
        Me.GroupHeader2 = New DevExpress.XtraReports.UI.GroupHeaderBand
        Me.GroupFooter_InvoiceTotals = New DevExpress.XtraReports.UI.GroupFooterBand
        Me.XrTable1 = New DevExpress.XtraReports.UI.XRTable
        Me.XrTableRow_InvoiceSubtotal = New DevExpress.XtraReports.UI.XRTableRow
        Me.XrTableCell1 = New DevExpress.XtraReports.UI.XRTableCell
        Me.XrTableCell_Invoice_Subtotal_2 = New DevExpress.XtraReports.UI.XRTableCell
        Me.XrTableRow_AdjustmentTotal = New DevExpress.XtraReports.UI.XRTableRow
        Me.XrTableCell5 = New DevExpress.XtraReports.UI.XRTableCell
        Me.XrTableCell_Payment_Subtotal_2 = New DevExpress.XtraReports.UI.XRTableCell
        Me.XrTableRow_Payments = New DevExpress.XtraReports.UI.XRTableRow
        Me.XrTableCell2 = New DevExpress.XtraReports.UI.XRTableCell
        Me.XrTableCell_Adjustment_Subtotal_2 = New DevExpress.XtraReports.UI.XRTableCell
        Me.XrTableRow_Total = New DevExpress.XtraReports.UI.XRTableRow
        Me.XrTableCell7 = New DevExpress.XtraReports.UI.XRTableCell
        Me.XrTableCell_Net_Subtotal_2 = New DevExpress.XtraReports.UI.XRTableCell
        Me.TopMarginBand1 = New DevExpress.XtraReports.UI.TopMarginBand
        Me.BottomMarginBand1 = New DevExpress.XtraReports.UI.BottomMarginBand
        CType(Me.XrTable1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
        '
        'DetailBand1
        '
        Me.DetailBand1.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrPanel_Detail})
        Me.DetailBand1.HeightF = 15.0!
        Me.DetailBand1.Name = "DetailBand1"
        Me.DetailBand1.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
        Me.PrintOnEmptyDataSource = False
        Me.DetailBand1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'XrPanel_Detail
        '
        Me.XrPanel_Detail.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel_Date, Me.XrLabel_Check, Me.XrLabel_Name, Me.XrLabel_Payment, Me.XrLabel_Client, Me.XrLabel_Account, Me.XrLabel_Fairshare})
        Me.XrPanel_Detail.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
        Me.XrPanel_Detail.Name = "XrPanel_Detail"
        Me.XrPanel_Detail.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
        Me.XrPanel_Detail.SizeF = New System.Drawing.SizeF(792.0!, 15.0!)
        '
        'XrLabel_Date
        '
        Me.XrLabel_Date.CanGrow = False
        Me.XrLabel_Date.LocationFloat = New DevExpress.Utils.PointFloat(91.0!, 0.0!)
        Me.XrLabel_Date.Name = "XrLabel_Date"
        Me.XrLabel_Date.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel_Date.SizeF = New System.Drawing.SizeF(66.0!, 15.0!)
        Me.XrLabel_Date.Text = "Date"
        Me.XrLabel_Date.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
        '
        'XrLabel_Check
        '
        Me.XrLabel_Check.CanGrow = False
        Me.XrLabel_Check.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
        Me.XrLabel_Check.Name = "XrLabel_Check"
        Me.XrLabel_Check.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel_Check.ProcessDuplicates = DevExpress.XtraReports.UI.ValueSuppressType.Suppress
        Me.XrLabel_Check.ProcessNullValues = DevExpress.XtraReports.UI.ValueSuppressType.Suppress
        Me.XrLabel_Check.SizeF = New System.Drawing.SizeF(85.0!, 15.0!)
        Me.XrLabel_Check.Text = "Check"
        Me.XrLabel_Check.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
        '
        'XrLabel_Name
        '
        Me.XrLabel_Name.CanGrow = False
        Me.XrLabel_Name.LocationFloat = New DevExpress.Utils.PointFloat(409.625!, 0.0!)
        Me.XrLabel_Name.Name = "XrLabel_Name"
        Me.XrLabel_Name.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel_Name.SizeF = New System.Drawing.SizeF(188.9166!, 15.0!)
        Me.XrLabel_Name.Text = "Name"
        Me.XrLabel_Name.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        Me.XrLabel_Name.WordWrap = False
        '
        'XrLabel_Payment
        '
        Me.XrLabel_Payment.CanGrow = False
        Me.XrLabel_Payment.LocationFloat = New DevExpress.Utils.PointFloat(610.0!, 0.0!)
        Me.XrLabel_Payment.Name = "XrLabel_Payment"
        Me.XrLabel_Payment.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel_Payment.SizeF = New System.Drawing.SizeF(72.0!, 15.0!)
        Me.XrLabel_Payment.Text = "Payment"
        Me.XrLabel_Payment.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
        '
        'XrLabel_Client
        '
        Me.XrLabel_Client.LocationFloat = New DevExpress.Utils.PointFloat(166.0!, 0.0!)
        Me.XrLabel_Client.Name = "XrLabel_Client"
        Me.XrLabel_Client.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel_Client.Scripts.OnBeforePrint = "XrLabel_Client_BeforePrint"
        Me.XrLabel_Client.SizeF = New System.Drawing.SizeF(77.79166!, 15.0!)
        Me.XrLabel_Client.Text = "Client"
        Me.XrLabel_Client.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
        Me.XrLabel_Client.WordWrap = False
        '
        'XrLabel_Account
        '
        Me.XrLabel_Account.CanGrow = False
        Me.XrLabel_Account.LocationFloat = New DevExpress.Utils.PointFloat(255.9167!, 0.0!)
        Me.XrLabel_Account.Name = "XrLabel_Account"
        Me.XrLabel_Account.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel_Account.SizeF = New System.Drawing.SizeF(141.0!, 15.0!)
        Me.XrLabel_Account.Text = "Account"
        Me.XrLabel_Account.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        '
        'XrLabel_Fairshare
        '
        Me.XrLabel_Fairshare.CanGrow = False
        Me.XrLabel_Fairshare.LocationFloat = New DevExpress.Utils.PointFloat(691.0!, 0.0!)
        Me.XrLabel_Fairshare.Name = "XrLabel_Fairshare"
        Me.XrLabel_Fairshare.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel_Fairshare.SizeF = New System.Drawing.SizeF(91.0!, 15.0!)
        Me.XrLabel_Fairshare.Text = "Fairshare"
        Me.XrLabel_Fairshare.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
        '
        'GroupHeader1
        '
        Me.GroupHeader1.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrPanel1})
        Me.GroupHeader1.GroupFields.AddRange(New DevExpress.XtraReports.UI.GroupField() {New DevExpress.XtraReports.UI.GroupField("check_number", DevExpress.XtraReports.UI.XRColumnSortOrder.Ascending)})
        Me.GroupHeader1.HeightF = 34.0!
        Me.GroupHeader1.Name = "GroupHeader1"
        Me.GroupHeader1.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
        Me.GroupHeader1.RepeatEveryPage = True
        Me.GroupHeader1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'XrPanel1
        '
        Me.XrPanel1.BackColor = System.Drawing.Color.Teal
        Me.XrPanel1.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel8, Me.XrLabel7, Me.XrLabel6, Me.XrLabel5, Me.XrLabel4, Me.XrLabel3, Me.XrLabel2})
        Me.XrPanel1.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 10.0!)
        Me.XrPanel1.Name = "XrPanel1"
        Me.XrPanel1.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
        Me.XrPanel1.SizeF = New System.Drawing.SizeF(792.0!, 18.0!)
        '
        'XrLabel8
        '
        Me.XrLabel8.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
        Me.XrLabel8.ForeColor = System.Drawing.Color.White
        Me.XrLabel8.LocationFloat = New DevExpress.Utils.PointFloat(692.0!, 0.0!)
        Me.XrLabel8.Name = "XrLabel8"
        Me.XrLabel8.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel8.SizeF = New System.Drawing.SizeF(91.0!, 17.0!)
        Me.XrLabel8.Text = "FAIRSHARE"
        Me.XrLabel8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
        Me.XrLabel8.WordWrap = False
        '
        'XrLabel7
        '
        Me.XrLabel7.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
        Me.XrLabel7.ForeColor = System.Drawing.Color.White
        Me.XrLabel7.LocationFloat = New DevExpress.Utils.PointFloat(606.0!, 0.0!)
        Me.XrLabel7.Name = "XrLabel7"
        Me.XrLabel7.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel7.SizeF = New System.Drawing.SizeF(76.0!, 17.0!)
        Me.XrLabel7.Text = "PAYMENT"
        Me.XrLabel7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
        Me.XrLabel7.WordWrap = False
        '
        'XrLabel6
        '
        Me.XrLabel6.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
        Me.XrLabel6.ForeColor = System.Drawing.Color.White
        Me.XrLabel6.LocationFloat = New DevExpress.Utils.PointFloat(409.6251!, 0.0!)
        Me.XrLabel6.Name = "XrLabel6"
        Me.XrLabel6.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel6.SizeF = New System.Drawing.SizeF(91.0!, 17.0!)
        Me.XrLabel6.Text = "NAME"
        Me.XrLabel6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        Me.XrLabel6.WordWrap = False
        '
        'XrLabel5
        '
        Me.XrLabel5.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
        Me.XrLabel5.ForeColor = System.Drawing.Color.White
        Me.XrLabel5.LocationFloat = New DevExpress.Utils.PointFloat(255.9167!, 0.0!)
        Me.XrLabel5.Name = "XrLabel5"
        Me.XrLabel5.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel5.SizeF = New System.Drawing.SizeF(91.0!, 17.0!)
        Me.XrLabel5.Text = "ACCOUNT"
        Me.XrLabel5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        Me.XrLabel5.WordWrap = False
        '
        'XrLabel4
        '
        Me.XrLabel4.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
        Me.XrLabel4.ForeColor = System.Drawing.Color.White
        Me.XrLabel4.LocationFloat = New DevExpress.Utils.PointFloat(184.7916!, 0.0!)
        Me.XrLabel4.Name = "XrLabel4"
        Me.XrLabel4.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel4.SizeF = New System.Drawing.SizeF(59.0!, 17.0!)
        Me.XrLabel4.Text = "CLIENT"
        Me.XrLabel4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
        Me.XrLabel4.WordWrap = False
        '
        'XrLabel3
        '
        Me.XrLabel3.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
        Me.XrLabel3.ForeColor = System.Drawing.Color.White
        Me.XrLabel3.LocationFloat = New DevExpress.Utils.PointFloat(98.0!, 0.0!)
        Me.XrLabel3.Name = "XrLabel3"
        Me.XrLabel3.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel3.SizeF = New System.Drawing.SizeF(59.0!, 17.0!)
        Me.XrLabel3.Text = "DATE"
        Me.XrLabel3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
        Me.XrLabel3.WordWrap = False
        '
        'XrLabel2
        '
        Me.XrLabel2.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
        Me.XrLabel2.ForeColor = System.Drawing.Color.White
        Me.XrLabel2.LocationFloat = New DevExpress.Utils.PointFloat(24.0!, 0.0!)
        Me.XrLabel2.Name = "XrLabel2"
        Me.XrLabel2.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel2.SizeF = New System.Drawing.SizeF(59.0!, 17.0!)
        Me.XrLabel2.Text = "CHECK"
        Me.XrLabel2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
        Me.XrLabel2.WordWrap = False
        '
        'GroupFooter_CheckTotals
        '
        Me.GroupFooter_CheckTotals.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel_EFT, Me.XrLabel_Total_Fairshare, Me.XrLabel_Total_Payment, Me.XrLine1, Me.XrLabel9})
        Me.GroupFooter_CheckTotals.HeightF = 52.0!
        Me.GroupFooter_CheckTotals.Name = "GroupFooter_CheckTotals"
        Me.GroupFooter_CheckTotals.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
        Me.GroupFooter_CheckTotals.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'XrLabel_EFT
        '
        Me.XrLabel_EFT.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
        Me.XrLabel_EFT.ForeColor = System.Drawing.Color.Red
        Me.XrLabel_EFT.LocationFloat = New DevExpress.Utils.PointFloat(8.0!, 25.0!)
        Me.XrLabel_EFT.Name = "XrLabel_EFT"
        Me.XrLabel_EFT.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel_EFT.Scripts.OnBeforePrint = "XrLabel_EFT_BeforePrint"
        Me.XrLabel_EFT.SizeF = New System.Drawing.SizeF(342.0!, 17.0!)
        Me.XrLabel_EFT.Text = "THIS PAYMENT WAS MADE ELECTRONICALLY"
        Me.XrLabel_EFT.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
        '
        'XrLabel_Total_Fairshare
        '
        Me.XrLabel_Total_Fairshare.CanGrow = False
        Me.XrLabel_Total_Fairshare.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
        Me.XrLabel_Total_Fairshare.LocationFloat = New DevExpress.Utils.PointFloat(692.0!, 25.0!)
        Me.XrLabel_Total_Fairshare.Name = "XrLabel_Total_Fairshare"
        Me.XrLabel_Total_Fairshare.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel_Total_Fairshare.SizeF = New System.Drawing.SizeF(91.0!, 17.0!)
        XrSummary1.FormatString = "{0:c}"
        XrSummary1.Running = DevExpress.XtraReports.UI.SummaryRunning.Group
        Me.XrLabel_Total_Fairshare.Summary = XrSummary1
        Me.XrLabel_Total_Fairshare.Text = "$0.00"
        Me.XrLabel_Total_Fairshare.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
        '
        'XrLabel_Total_Payment
        '
        Me.XrLabel_Total_Payment.CanGrow = False
        Me.XrLabel_Total_Payment.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
        Me.XrLabel_Total_Payment.LocationFloat = New DevExpress.Utils.PointFloat(558.0!, 25.0!)
        Me.XrLabel_Total_Payment.Name = "XrLabel_Total_Payment"
        Me.XrLabel_Total_Payment.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel_Total_Payment.SizeF = New System.Drawing.SizeF(125.0!, 17.0!)
        XrSummary2.FormatString = "{0:c}"
        XrSummary2.Running = DevExpress.XtraReports.UI.SummaryRunning.Group
        Me.XrLabel_Total_Payment.Summary = XrSummary2
        Me.XrLabel_Total_Payment.Text = "$0.00"
        Me.XrLabel_Total_Payment.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
        '
        'XrLine1
        '
        Me.XrLine1.ForeColor = System.Drawing.Color.Blue
        Me.XrLine1.LineWidth = 2
        Me.XrLine1.LocationFloat = New DevExpress.Utils.PointFloat(409.625!, 7.999992!)
        Me.XrLine1.Name = "XrLine1"
        Me.XrLine1.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
        Me.XrLine1.SizeF = New System.Drawing.SizeF(373.3749!, 9.0!)
        '
        'XrLabel9
        '
        Me.XrLabel9.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
        Me.XrLabel9.ForeColor = System.Drawing.Color.Blue
        Me.XrLabel9.LocationFloat = New DevExpress.Utils.PointFloat(409.6251!, 25.0!)
        Me.XrLabel9.Name = "XrLabel9"
        Me.XrLabel9.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel9.SizeF = New System.Drawing.SizeF(117.0!, 17.0!)
        Me.XrLabel9.Text = "CHECK TOTALS"
        Me.XrLabel9.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'XrLabel_Invoice_Number
        '
        Me.XrLabel_Invoice_Number.Font = New System.Drawing.Font("Times New Roman", 14.0!, System.Drawing.FontStyle.Bold)
        Me.XrLabel_Invoice_Number.LocationFloat = New DevExpress.Utils.PointFloat(332.0!, 25.00002!)
        Me.XrLabel_Invoice_Number.Name = "XrLabel_Invoice_Number"
        Me.XrLabel_Invoice_Number.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel_Invoice_Number.Scripts.OnBeforePrint = "XrLabel_Invoice_Number_BeforePrint"
        Me.XrLabel_Invoice_Number.SizeF = New System.Drawing.SizeF(225.0!, 25.0!)
        Me.XrLabel_Invoice_Number.Text = "A0010-5050205"
        Me.XrLabel_Invoice_Number.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft
        '
        'XrLabel1
        '
        Me.XrLabel1.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 29.0!)
        Me.XrLabel1.Name = "XrLabel1"
        Me.XrLabel1.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel1.SizeF = New System.Drawing.SizeF(325.0!, 17.0!)
        Me.XrLabel1.Text = "The following detail information refers to invoice number:"
        Me.XrLabel1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft
        '
        'GroupHeader2
        '
        Me.GroupHeader2.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel_Invoice_Number, Me.XrLabel1})
        Me.GroupHeader2.GroupFields.AddRange(New DevExpress.XtraReports.UI.GroupField() {New DevExpress.XtraReports.UI.GroupField("invoice_number", DevExpress.XtraReports.UI.XRColumnSortOrder.Ascending)})
        Me.GroupHeader2.HeightF = 50.00002!
        Me.GroupHeader2.Level = 1
        Me.GroupHeader2.Name = "GroupHeader2"
        Me.GroupHeader2.RepeatEveryPage = True
        '
        'GroupFooter_InvoiceTotals
        '
        Me.GroupFooter_InvoiceTotals.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrTable1})
        Me.GroupFooter_InvoiceTotals.HeightF = 70.00001!
        Me.GroupFooter_InvoiceTotals.Level = 1
        Me.GroupFooter_InvoiceTotals.Name = "GroupFooter_InvoiceTotals"
        '
        'XrTable1
        '
        Me.XrTable1.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
        Me.XrTable1.LocationFloat = New DevExpress.Utils.PointFloat(274.9999!, 10.00001!)
        Me.XrTable1.Name = "XrTable1"
        Me.XrTable1.Rows.AddRange(New DevExpress.XtraReports.UI.XRTableRow() {Me.XrTableRow_InvoiceSubtotal, Me.XrTableRow_AdjustmentTotal, Me.XrTableRow_Payments, Me.XrTableRow_Total})
        Me.XrTable1.SizeF = New System.Drawing.SizeF(508.0!, 60.0!)
        Me.XrTable1.StylePriority.UseFont = False
        '
        'XrTableRow_InvoiceSubtotal
        '
        Me.XrTableRow_InvoiceSubtotal.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.XrTableCell1, Me.XrTableCell_Invoice_Subtotal_2})
        Me.XrTableRow_InvoiceSubtotal.Name = "XrTableRow_InvoiceSubtotal"
        Me.XrTableRow_InvoiceSubtotal.Scripts.OnPrintOnPage = "XrTableRow_InvoiceSubtotal_PrintOnPage"
        Me.XrTableRow_InvoiceSubtotal.Weight = 0.70805145564537519
        '
        'XrTableCell1
        '
        Me.XrTableCell1.Name = "XrTableCell1"
        Me.XrTableCell1.StylePriority.UseTextAlignment = False
        Me.XrTableCell1.Text = "INVOICE  SUBTOTAL (fairshare from the check totals above)"
        Me.XrTableCell1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        Me.XrTableCell1.Weight = 3.414803561500428
        '
        'XrTableCell_Invoice_Subtotal_2
        '
        Me.XrTableCell_Invoice_Subtotal_2.Name = "XrTableCell_Invoice_Subtotal_2"
        Me.XrTableCell_Invoice_Subtotal_2.Scripts.OnBeforePrint = "XrTableCell_Invoice_Subtotal_2_BeforePrint"
        Me.XrTableCell_Invoice_Subtotal_2.StylePriority.UseTextAlignment = False
        Me.XrTableCell_Invoice_Subtotal_2.Text = "$0.00"
        Me.XrTableCell_Invoice_Subtotal_2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
        Me.XrTableCell_Invoice_Subtotal_2.Weight = 0.7551958281480099
        '
        'XrTableRow_AdjustmentTotal
        '
        Me.XrTableRow_AdjustmentTotal.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.XrTableCell5, Me.XrTableCell_Payment_Subtotal_2})
        Me.XrTableRow_AdjustmentTotal.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
        Me.XrTableRow_AdjustmentTotal.ForeColor = System.Drawing.Color.Red
        Me.XrTableRow_AdjustmentTotal.Name = "XrTableRow_AdjustmentTotal"
        Me.XrTableRow_AdjustmentTotal.Scripts.OnPrintOnPage = "XrTableRow_AdjustmentTotal_PrintOnPage"
        Me.XrTableRow_AdjustmentTotal.StylePriority.UseFont = False
        Me.XrTableRow_AdjustmentTotal.StylePriority.UseForeColor = False
        Me.XrTableRow_AdjustmentTotal.Weight = 0.70805145564537519
        '
        'XrTableCell5
        '
        Me.XrTableCell5.CanShrink = True
        Me.XrTableCell5.Name = "XrTableCell5"
        Me.XrTableCell5.Padding = New DevExpress.XtraPrinting.PaddingInfo(50, 0, 0, 0, 100.0!)
        Me.XrTableCell5.StylePriority.UsePadding = False
        Me.XrTableCell5.StylePriority.UseTextAlignment = False
        Me.XrTableCell5.Text = "Less amount received to date on this invoice"
        Me.XrTableCell5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        Me.XrTableCell5.Weight = 3.414803561500428
        '
        'XrTableCell_Payment_Subtotal_2
        '
        Me.XrTableCell_Payment_Subtotal_2.CanShrink = True
        Me.XrTableCell_Payment_Subtotal_2.Name = "XrTableCell_Payment_Subtotal_2"
        Me.XrTableCell_Payment_Subtotal_2.Scripts.OnBeforePrint = "XrTableCell_Payment_Subtotal_2_BeforePrint"
        Me.XrTableCell_Payment_Subtotal_2.StylePriority.UseTextAlignment = False
        Me.XrTableCell_Payment_Subtotal_2.Text = "$0.00"
        Me.XrTableCell_Payment_Subtotal_2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
        Me.XrTableCell_Payment_Subtotal_2.Weight = 0.7551958281480099
        '
        'XrTableRow_Payments
        '
        Me.XrTableRow_Payments.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.XrTableCell2, Me.XrTableCell_Adjustment_Subtotal_2})
        Me.XrTableRow_Payments.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
        Me.XrTableRow_Payments.ForeColor = System.Drawing.Color.Red
        Me.XrTableRow_Payments.Name = "XrTableRow_Payments"
        Me.XrTableRow_Payments.Scripts.OnPrintOnPage = "XrTableRow_Payments_PrintOnPage"
        Me.XrTableRow_Payments.StylePriority.UseFont = False
        Me.XrTableRow_Payments.StylePriority.UseForeColor = False
        Me.XrTableRow_Payments.Weight = 0.70805151298699465
        '
        'XrTableCell2
        '
        Me.XrTableCell2.CanShrink = True
        Me.XrTableCell2.Name = "XrTableCell2"
        Me.XrTableCell2.Padding = New DevExpress.XtraPrinting.PaddingInfo(50, 0, 0, 0, 100.0!)
        Me.XrTableCell2.StylePriority.UsePadding = False
        Me.XrTableCell2.StylePriority.UseTextAlignment = False
        Me.XrTableCell2.Text = "Less adjustments to this invoice"
        Me.XrTableCell2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        Me.XrTableCell2.Weight = 3.414803561500428
        '
        'XrTableCell_Adjustment_Subtotal_2
        '
        Me.XrTableCell_Adjustment_Subtotal_2.CanShrink = True
        Me.XrTableCell_Adjustment_Subtotal_2.Name = "XrTableCell_Adjustment_Subtotal_2"
        Me.XrTableCell_Adjustment_Subtotal_2.Scripts.OnBeforePrint = "XrTableCell_Adjustment_Subtotal_2_BeforePrint"
        Me.XrTableCell_Adjustment_Subtotal_2.StylePriority.UseTextAlignment = False
        Me.XrTableCell_Adjustment_Subtotal_2.Text = "$0.00"
        Me.XrTableCell_Adjustment_Subtotal_2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
        Me.XrTableCell_Adjustment_Subtotal_2.Weight = 0.7551958281480099
        '
        'XrTableRow_Total
        '
        Me.XrTableRow_Total.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.XrTableCell7, Me.XrTableCell_Net_Subtotal_2})
        Me.XrTableRow_Total.Name = "XrTableRow_Total"
        Me.XrTableRow_Total.Weight = 0.70805155954482824
        '
        'XrTableCell7
        '
        Me.XrTableCell7.Name = "XrTableCell7"
        Me.XrTableCell7.StylePriority.UseTextAlignment = False
        Me.XrTableCell7.Text = "INVOICE TOTAL (fairshare total less payments and adjustments)"
        Me.XrTableCell7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        Me.XrTableCell7.Weight = 3.4148030604835689
        '
        'XrTableCell_Net_Subtotal_2
        '
        Me.XrTableCell_Net_Subtotal_2.Name = "XrTableCell_Net_Subtotal_2"
        Me.XrTableCell_Net_Subtotal_2.Scripts.OnBeforePrint = "XrTableCell_Net_Subtotal_2_BeforePrint"
        Me.XrTableCell_Net_Subtotal_2.StylePriority.UseTextAlignment = False
        Me.XrTableCell_Net_Subtotal_2.Text = "$0.00"
        Me.XrTableCell_Net_Subtotal_2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
        Me.XrTableCell_Net_Subtotal_2.Weight = 0.7551963291648689
        '
        'TopMarginBand1
        '
        Me.TopMarginBand1.HeightF = 0.0!
        Me.TopMarginBand1.Name = "TopMarginBand1"
        '
        'BottomMarginBand1
        '
        Me.BottomMarginBand1.HeightF = 0.0!
        Me.BottomMarginBand1.Name = "BottomMarginBand1"
        '
        'CheckDetailReport
        '
        Me.Bands.AddRange(New DevExpress.XtraReports.UI.Band() {Me.DetailBand1, Me.GroupHeader1, Me.GroupFooter_CheckTotals, Me.GroupHeader2, Me.GroupFooter_InvoiceTotals, Me.TopMarginBand1, Me.BottomMarginBand1})
        Me.Margins = New System.Drawing.Printing.Margins(0, 0, 0, 0)
        Me.ScriptLanguage = DevExpress.XtraReports.ScriptLanguage.VisualBasic
        Me.ScriptReferencesString = "C:\Documents and Settings\Al Longyear\My Documents\Visual Studio 2008\Projects\De" & _
            "btPlus\Executables\DebtPlus.Data.dll"
        Me.ScriptsSource = resources.GetString("$this.ScriptsSource")
        Me.Version = "10.1"
        CType(Me.XrTable1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me, System.ComponentModel.ISupportInitialize).EndInit()
    End Sub
	
    Friend WithEvents XrLabel_Invoice_Number As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel1 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents GroupHeader2 As DevExpress.XtraReports.UI.GroupHeaderBand
    Friend WithEvents GroupFooter_InvoiceTotals As DevExpress.XtraReports.UI.GroupFooterBand
    Friend WithEvents XrTable1 As DevExpress.XtraReports.UI.XRTable
    Friend WithEvents XrTableRow_InvoiceSubtotal As DevExpress.XtraReports.UI.XRTableRow
    Friend WithEvents XrTableCell1 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents XrTableCell_Invoice_Subtotal_2 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents XrTableRow_AdjustmentTotal As DevExpress.XtraReports.UI.XRTableRow
    Friend WithEvents XrTableCell5 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents XrTableCell_Payment_Subtotal_2 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents XrTableRow_Payments As DevExpress.XtraReports.UI.XRTableRow
    Friend WithEvents XrTableCell2 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents XrTableCell_Adjustment_Subtotal_2 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents XrTableRow_Total As DevExpress.XtraReports.UI.XRTableRow
    Friend WithEvents XrTableCell7 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents XrTableCell_Net_Subtotal_2 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents TopMarginBand1 As DevExpress.XtraReports.UI.TopMarginBand
    Friend WithEvents BottomMarginBand1 As DevExpress.XtraReports.UI.BottomMarginBand
#End Region

    Private Sub XrLabel_Client_BeforePrint(ByVal sender As System.Object, ByVal e As System.Drawing.Printing.PrintEventArgs)
        With CType(sender, DevExpress.XtraReports.UI.XRLabel)
            .Text = DebtPlus.Utils.Format.Client.FormatClientID(GetCurrentColumnValue("client"))
        End With
    End Sub

    Private Sub XrLabel_Invoice_Number_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs)
        With CType(sender, DevExpress.XtraReports.UI.XRLabel)
            Dim SubReport As DevExpress.XtraReports.UI.XtraReport = .Report
            Dim MasterReport As DevExpress.XtraReports.UI.XtraReport = SubReport.MasterReport
            Dim creditor As String = Convert.ToString(MasterReport.Parameters("ParameterCreditor").Value)

            Dim invoice_number As System.Int32 = invoice_register
            .Text = System.String.Format("{0}-{1:f0}", Creditor, invoice_number)
        End With
    End Sub

    Private Sub XRLabel_EFT_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs)
        With CType(sender, DevExpress.XtraReports.UI.XRLabel)
            Dim tran_type As String = ""
            If GetCurrentColumnValue("tran_type") IsNot Nothing AndAlso GetCurrentColumnValue("tran_type") IsNot System.DBNull.Value Then tran_type = Convert.ToString(GetCurrentColumnValue("tran_type")).Trim
            Dim IsVisible As Boolean = tran_type.StartsWith("BW")
            If IsVisible Then
                .Visible = True
            Else
                .Visible = False
                e.Cancel = True
            End If
        End With
    End Sub

    Private Sub XrTableCell_Invoice_Subtotal_2_BeforePrint(ByVal Sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs)
        With CType(Sender, DevExpress.XtraReports.UI.XRTableCell)
            .Text = String.Format("{0:c}", invoice_amount)
        End With
    End Sub

    Private Sub XrTableCell_Adjustment_Subtotal_2_BeforePrint(ByVal Sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs)
        With CType(Sender, DevExpress.XtraReports.UI.XRTableCell)
            .Text = String.Format("{0:c}", invoice_adjustments)
        End With
    End Sub

    Private Sub XrTableCell_Payment_Subtotal_2_BeforePrint(ByVal Sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs)
        With CType(Sender, DevExpress.XtraReports.UI.XRTableCell)
            .Text = String.Format("{0:c}", invoice_payments)
        End With
    End Sub

    Private Sub XrTableCell_Net_Subtotal_2_BeforePrint(ByVal Sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs)
        With CType(Sender, DevExpress.XtraReports.UI.XRTableCell)
            .Text = String.Format("{0:c}", net_invoice)
        End With
    End Sub

    Private Sub XrTableRow_AdjustmentTotal_PrintOnPage(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs)
        e.Cancel = CancelPrintSummary()
    End Sub

    Private ReadOnly Property invoice_register() As System.Int32
        Get
            Dim answer As System.Int32 = 0
            If GetCurrentColumnValue("invoice_number") IsNot Nothing AndAlso GetCurrentColumnValue("invoice_number") IsNot System.DBNull.Value Then answer = Convert.ToInt32(GetCurrentColumnValue("invoice_number"))
            Return answer
        End Get
    End Property

    Private Function invoice_adjustments() As Decimal
        Dim row As System.Data.DataRowView = CType(GetCurrentRow(), System.Data.DataRowView)
        Dim Answer As Object = row("adj_amount")
        If Answer Is System.DBNull.Value Then Return 0D
        Return Convert.ToDecimal(Answer)
    End Function

    Private Function invoice_payments() As Decimal
        Dim row As System.Data.DataRowView = CType(GetCurrentRow(), System.Data.DataRowView)
        Dim Answer As Object = row("pmt_amount")
        If Answer Is System.DBNull.Value Then Return 0D
        Return Convert.ToDecimal(Answer)
    End Function

    Private Function invoice_amount() As Decimal
        Dim tbl As System.Data.DataTable = CType(GetCurrentRow(), System.Data.DataRowView).Row.Table
        Dim Answer As Object = tbl.Compute("sum(billed)", String.Format("[invoice_number]={0:f0}", invoice_register))
        If Answer Is System.DBNull.Value Then Return 0D
        Return Convert.ToDecimal(Answer)
    End Function

    Private Function net_invoice() As Decimal
        Return invoice_amount() - invoice_adjustments() - invoice_payments()
    End Function

    Private Function CancelPrintSummary() As Boolean
        Return (invoice_adjustments() <= 0D) AndAlso (invoice_payments() <= 0D)
    End Function
End Class
