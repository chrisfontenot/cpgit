#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}
#End Region

Option Compare Binary
Option Explicit On 
Option Strict On

Imports DebtPlus.Reports
Imports DevExpress.XtraReports.UI

Namespace Creditor.Invoice
    Public Class InvoiceStatusReport
        Inherits DebtPlus.Reports.Template.BaseXtraReportClass

        ''' <summary>
        '''     Create an instance of our report
        ''' </summary>
        Public Sub New()
            MyBase.New()
            InitializeComponent()

            ' Functions have been moved to the scripts
            'RegisterHandlers()
        End Sub

        Private Sub RegisterHandlers()
            AddHandler Me.BeforePrint, AddressOf InvoiceStatusReport_BeforePrint
        End Sub

        Private Sub UnRegisterHandlers()
            AddHandler Me.BeforePrint, AddressOf InvoiceStatusReport_BeforePrint
        End Sub

#Region " Component Designer generated code "

        'Component overrides dispose to clean up the component list.
        Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
            If disposing Then
                If components IsNot Nothing Then
                    components.Dispose()
                End If
            End If
            MyBase.Dispose(disposing)
        End Sub

        'Required by the Component Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Component Designer
        'It can be modified using the Component Designer.
        'Do not modify it using the code editor.

        Friend WithEvents PageHeader As DevExpress.XtraReports.UI.PageHeaderBand
        Friend WithEvents XrLabel_Invoice As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_InvDate As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_InvAmount As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_PmtDate As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_PmtAmount As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_AdjDate As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel7 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel8 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel9 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel10 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel11 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel12 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLine1 As DevExpress.XtraReports.UI.XRLine
        Friend WithEvents XrLabel13 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_NetTitle As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel1_AdjAmount As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_Net As DevExpress.XtraReports.UI.XRLabel
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
            Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(InvoiceStatusReport))
            Me.XrLabel_Net = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel1_AdjAmount = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_AdjDate = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_PmtAmount = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_PmtDate = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_InvAmount = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_InvDate = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_Invoice = New DevExpress.XtraReports.UI.XRLabel()
            Me.PageHeader = New DevExpress.XtraReports.UI.PageHeaderBand()
            Me.XrLabel_NetTitle = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel13 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLine1 = New DevExpress.XtraReports.UI.XRLine()
            Me.XrLabel12 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel11 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel10 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel9 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel8 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel7 = New DevExpress.XtraReports.UI.XRLabel()
            CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
            '
            'Detail
            '
            Me.Detail.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel_Net, Me.XrLabel1_AdjAmount, Me.XrLabel_AdjDate, Me.XrLabel_PmtAmount, Me.XrLabel_PmtDate, Me.XrLabel_InvAmount, Me.XrLabel_InvDate, Me.XrLabel_Invoice})
            Me.Detail.HeightF = 17.0!
            Me.Detail.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
            Me.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
            '
            'XrLabel_Net
            '
            Me.XrLabel_Net.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "net_amt", "{0:c}")})
            Me.XrLabel_Net.LocationFloat = New DevExpress.Utils.PointFloat(661.0!, 0.0!)
            Me.XrLabel_Net.Name = "XrLabel_Net"
            Me.XrLabel_Net.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_Net.SizeF = New System.Drawing.SizeF(83.0!, 17.0!)
            Me.XrLabel_Net.Text = "$0.00"
            Me.XrLabel_Net.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel1_AdjAmount
            '
            Me.XrLabel1_AdjAmount.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "adj_amt", "{0:c}")})
            Me.XrLabel1_AdjAmount.LocationFloat = New DevExpress.Utils.PointFloat(578.0!, 0.0!)
            Me.XrLabel1_AdjAmount.Name = "XrLabel1_AdjAmount"
            Me.XrLabel1_AdjAmount.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel1_AdjAmount.SizeF = New System.Drawing.SizeF(72.0!, 15.0!)
            Me.XrLabel1_AdjAmount.Text = "$0.00"
            Me.XrLabel1_AdjAmount.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel_AdjDate
            '
            Me.XrLabel_AdjDate.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "adj_date", "{0:d}")})
            Me.XrLabel_AdjDate.LocationFloat = New DevExpress.Utils.PointFloat(492.0!, 0.0!)
            Me.XrLabel_AdjDate.Name = "XrLabel_AdjDate"
            Me.XrLabel_AdjDate.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_AdjDate.SizeF = New System.Drawing.SizeF(75.0!, 15.0!)
            Me.XrLabel_AdjDate.Text = "MM/DD/YY"
            Me.XrLabel_AdjDate.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel_PmtAmount
            '
            Me.XrLabel_PmtAmount.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "pmt_amt", "{0:c}")})
            Me.XrLabel_PmtAmount.LocationFloat = New DevExpress.Utils.PointFloat(409.0!, 0.0!)
            Me.XrLabel_PmtAmount.Name = "XrLabel_PmtAmount"
            Me.XrLabel_PmtAmount.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_PmtAmount.SizeF = New System.Drawing.SizeF(72.0!, 15.0!)
            Me.XrLabel_PmtAmount.Text = "$0.00"
            Me.XrLabel_PmtAmount.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel_PmtDate
            '
            Me.XrLabel_PmtDate.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "pmt_date", "{0:d}")})
            Me.XrLabel_PmtDate.LocationFloat = New DevExpress.Utils.PointFloat(323.0!, 0.0!)
            Me.XrLabel_PmtDate.Name = "XrLabel_PmtDate"
            Me.XrLabel_PmtDate.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_PmtDate.SizeF = New System.Drawing.SizeF(75.0!, 15.0!)
            Me.XrLabel_PmtDate.Text = "MM/DD/YY"
            Me.XrLabel_PmtDate.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel_InvAmount
            '
            Me.XrLabel_InvAmount.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "inv_amt", "{0:c}")})
            Me.XrLabel_InvAmount.LocationFloat = New DevExpress.Utils.PointFloat(229.0!, 0.0!)
            Me.XrLabel_InvAmount.Name = "XrLabel_InvAmount"
            Me.XrLabel_InvAmount.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_InvAmount.SizeF = New System.Drawing.SizeF(83.0!, 15.0!)
            Me.XrLabel_InvAmount.Text = "$0.00"
            Me.XrLabel_InvAmount.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel_InvDate
            '
            Me.XrLabel_InvDate.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "inv_date", "{0:d}")})
            Me.XrLabel_InvDate.LocationFloat = New DevExpress.Utils.PointFloat(136.0!, 0.0!)
            Me.XrLabel_InvDate.Name = "XrLabel_InvDate"
            Me.XrLabel_InvDate.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_InvDate.SizeF = New System.Drawing.SizeF(82.0!, 17.0!)
            Me.XrLabel_InvDate.Text = "MM/DD/YY"
            Me.XrLabel_InvDate.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel_Invoice
            '
            Me.XrLabel_Invoice.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "invoice")})
            Me.XrLabel_Invoice.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
            Me.XrLabel_Invoice.Name = "XrLabel_Invoice"
            Me.XrLabel_Invoice.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_Invoice.SizeF = New System.Drawing.SizeF(125.0!, 15.0!)
            Me.XrLabel_Invoice.Text = "000000000"
            Me.XrLabel_Invoice.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'PageHeader
            '
            Me.PageHeader.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel_NetTitle, Me.XrLabel13, Me.XrLine1, Me.XrLabel12, Me.XrLabel11, Me.XrLabel10, Me.XrLabel9, Me.XrLabel8, Me.XrLabel7})
            Me.PageHeader.HeightF = 34.0!
            Me.PageHeader.Name = "PageHeader"
            Me.PageHeader.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
            Me.PageHeader.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
            '
            'XrLabel_NetTitle
            '
            Me.XrLabel_NetTitle.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel_NetTitle.LocationFloat = New DevExpress.Utils.PointFloat(661.0!, 8.0!)
            Me.XrLabel_NetTitle.Name = "XrLabel_NetTitle"
            Me.XrLabel_NetTitle.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_NetTitle.SizeF = New System.Drawing.SizeF(83.0!, 17.0!)
            Me.XrLabel_NetTitle.Text = "Net"
            Me.XrLabel_NetTitle.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel13
            '
            Me.XrLabel13.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel13.LocationFloat = New DevExpress.Utils.PointFloat(578.0!, 8.0!)
            Me.XrLabel13.Name = "XrLabel13"
            Me.XrLabel13.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel13.SizeF = New System.Drawing.SizeF(72.0!, 17.0!)
            Me.XrLabel13.Text = "Adj Amt"
            Me.XrLabel13.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLine1
            '
            Me.XrLine1.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 25.0!)
            Me.XrLine1.Name = "XrLine1"
            Me.XrLine1.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
            Me.XrLine1.SizeF = New System.Drawing.SizeF(742.0!, 9.0!)
            '
            'XrLabel12
            '
            Me.XrLabel12.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel12.LocationFloat = New DevExpress.Utils.PointFloat(498.0!, 8.0!)
            Me.XrLabel12.Name = "XrLabel12"
            Me.XrLabel12.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel12.SizeF = New System.Drawing.SizeF(69.0!, 17.0!)
            Me.XrLabel12.Text = "Adj Date"
            Me.XrLabel12.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel11
            '
            Me.XrLabel11.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel11.LocationFloat = New DevExpress.Utils.PointFloat(143.0!, 8.0!)
            Me.XrLabel11.Name = "XrLabel11"
            Me.XrLabel11.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel11.SizeF = New System.Drawing.SizeF(75.0!, 17.0!)
            Me.XrLabel11.Text = "Inv Date"
            Me.XrLabel11.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel10
            '
            Me.XrLabel10.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel10.LocationFloat = New DevExpress.Utils.PointFloat(330.0!, 8.0!)
            Me.XrLabel10.Name = "XrLabel10"
            Me.XrLabel10.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel10.SizeF = New System.Drawing.SizeF(68.0!, 17.0!)
            Me.XrLabel10.Text = "Pmt Date"
            Me.XrLabel10.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel9
            '
            Me.XrLabel9.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel9.LocationFloat = New DevExpress.Utils.PointFloat(58.0!, 8.0!)
            Me.XrLabel9.Name = "XrLabel9"
            Me.XrLabel9.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel9.SizeF = New System.Drawing.SizeF(67.0!, 17.0!)
            Me.XrLabel9.Text = "Invoice"
            Me.XrLabel9.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel8
            '
            Me.XrLabel8.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel8.LocationFloat = New DevExpress.Utils.PointFloat(236.0!, 8.0!)
            Me.XrLabel8.Name = "XrLabel8"
            Me.XrLabel8.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel8.SizeF = New System.Drawing.SizeF(76.0!, 17.0!)
            Me.XrLabel8.Text = "Inv Amt"
            Me.XrLabel8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel7
            '
            Me.XrLabel7.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel7.LocationFloat = New DevExpress.Utils.PointFloat(409.0!, 8.0!)
            Me.XrLabel7.Name = "XrLabel7"
            Me.XrLabel7.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel7.SizeF = New System.Drawing.SizeF(72.0!, 15.0!)
            Me.XrLabel7.Text = "Pmt Amt"
            Me.XrLabel7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'InvoiceStatusReport
            '
            Me.Bands.AddRange(New DevExpress.XtraReports.UI.Band() {Me.TopMarginBand1, Me.Detail, Me.BottomMarginBand1, Me.PageHeader})
            Me.Margins = New System.Drawing.Printing.Margins(100, 4, 25, 25)
            Me.ReportPrintOptions.PrintOnEmptyDataSource = False
            Me.ScriptLanguage = DevExpress.XtraReports.ScriptLanguage.VisualBasic
            Me.ScriptsSource = resources.GetString("$this.ScriptsSource")
            Me.Version = "11.2"
            Me.Controls.SetChildIndex(Me.PageHeader, 0)
            Me.Controls.SetChildIndex(Me.BottomMarginBand1, 0)
            Me.Controls.SetChildIndex(Me.Detail, 0)
            Me.Controls.SetChildIndex(Me.TopMarginBand1, 0)
            CType(Me, System.ComponentModel.ISupportInitialize).EndInit()

        End Sub

#End Region

        Protected ReadOnly ds As New System.Data.DataSet("ds")

        Private Sub InvoiceStatusReport_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs)
            Dim rpt As DevExpress.XtraReports.UI.XtraReport = CType(sender, DevExpress.XtraReports.UI.XtraReport)
            Dim MasterRpt As DevExpress.XtraReports.UI.XtraReport = CType(rpt.MasterReport, DevExpress.XtraReports.UI.XtraReport)
            Dim Creditor As String = Convert.ToString(MasterRpt.Parameters("ParameterCreditor").Value)

            '-- Erase the current data
            Dim tbl As System.Data.DataTable = ds.Tables("rpt_creditor_invoice_detail")
            If tbl IsNot Nothing Then
                tbl.Clear()
            End If

            Using cn As SqlClient.SqlConnection = New System.Data.SqlClient.SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
                cn.Open()
                Using cmd As New System.Data.SqlClient.SqlCommand()
                    cmd.Connection = cn
                    cmd.CommandText = "rpt_creditor_invoice_detail"
                    cmd.CommandType = System.Data.CommandType.StoredProcedure
                    cmd.Parameters.Add("@creditor", System.Data.SqlDbType.VarChar, 80).Value = Creditor
                    cmd.CommandTimeout = 0

                    Using da As New System.Data.SqlClient.SqlDataAdapter(cmd)
                        da.Fill(ds, "rpt_creditor_invoice_detail")
                        tbl = ds.Tables("rpt_creditor_invoice_detail")
                        If Not tbl.Columns.Contains("net_amt") Then
                            tbl.Columns.Add(New System.Data.DataColumn("net_amt", GetType(Decimal), "isnull([inv_amt],0)-isnull([adj_amt],0)-isnull([pmt_amt],0)"))
                        End If
                    End Using
                End Using
            End Using

            rpt.DataSource = tbl.DefaultView
        End Sub
    End Class
End Namespace
