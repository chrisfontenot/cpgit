#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}
#End Region

Imports DevExpress.XtraReports.UI

Namespace Creditor.Invoice
    Public Class CreditorInvoiceReport
        Inherits Template.BaseXtraReportClass
        Implements DebtPlus.Interfaces.Creditor.ICreditor

        Public Sub New()
            MyBase.New()
            InitializeComponent()

            '-- Ensure that the creditor parameter is empty
            Parameter_Creditor = String.Empty

            '-- Add the handlers for the panels
            RegisterHandlers()
        End Sub

        Private Sub RegisterHandlers()
            AddHandler BeforePrint, AddressOf CreditorInvoiceReport_BeforePrint
            AddHandler XrPanel_Aging.BeforePrint, AddressOf XrPanel_Aging_BeforePrint
            AddHandler XrLabel_Creditor_Address.BeforePrint, AddressOf XrLabel_Creditor_Address_BeforePrint
            AddHandler Text_Footer.PrintOnPage, AddressOf Text_Footer_PrintOnPage
            AddHandler Subreport_InvoiceStatus.BeforePrint, AddressOf Subreport_InvoiceStatus_BeforePrint
            AddHandler Subreport_CheckDetail.BeforePrint, AddressOf Subreport_CheckDetail_BeforePrint
            AddHandler XrBarCode_PostalCode.BeforePrint, AddressOf XrBarCode_PostalCode_BeforePrint
        End Sub

        Private Sub UnRegisterHandlers()
            RemoveHandler BeforePrint, AddressOf CreditorInvoiceReport_BeforePrint
            RemoveHandler XrPanel_Aging.BeforePrint, AddressOf XrPanel_Aging_BeforePrint
            RemoveHandler XrLabel_Creditor_Address.BeforePrint, AddressOf XrLabel_Creditor_Address_BeforePrint
            RemoveHandler Text_Footer.PrintOnPage, AddressOf Text_Footer_PrintOnPage
            RemoveHandler Subreport_InvoiceStatus.BeforePrint, AddressOf Subreport_InvoiceStatus_BeforePrint
            RemoveHandler Subreport_CheckDetail.BeforePrint, AddressOf Subreport_CheckDetail_BeforePrint
            RemoveHandler XrBarCode_PostalCode.BeforePrint, AddressOf XrBarCode_PostalCode_BeforePrint
        End Sub

        Public Property Creditor As String Implements DebtPlus.Interfaces.Creditor.ICreditor.Creditor
            Get
                Return Parameter_Creditor
            End Get
            Set(value As String)
                Parameter_Creditor = value
            End Set
        End Property

        Public Property Parameter_Creditor() As String
            Get
                Dim parm As DevExpress.XtraReports.Parameters.Parameter = FindParameter("ParameterCreditor")
                If parm Is Nothing Then Return String.Empty
                Return Convert.ToString(parm.Value)
            End Get
            Set(ByVal Value As String)
                SetParameter("ParameterCreditor", GetType(String), Value, "Creditor Label", False)
            End Set
        End Property

        Public Overrides Sub SetReportParameter(ByVal Parameter As String, ByVal Value As Object)
            Select Case Parameter
                Case "Creditor"
                    Parameter_Creditor = Convert.ToString(Value)
                Case Else
                    MyBase.SetReportParameter(Parameter, Value)
            End Select
        End Sub

        Public Overrides Function NeedParameters() As Boolean
            Return MyBase.NeedParameters() OrElse Parameter_Creditor = String.Empty
        End Function

        Public Overrides Function RequestReportParameters() As System.Windows.Forms.DialogResult
            Dim Answer As System.Windows.Forms.DialogResult = DialogResult.OK
            If NeedParameters() Then
                Using frm As New DebtPlus.Reports.Template.Forms.CreditorParametersForm()
                    Answer = frm.ShowDialog()
                    Parameter_Creditor = frm.Parameter_Creditor
                End Using
            End If
            Return Answer
        End Function

#Region " Component Designer generated code "

        'Component overrides dispose to clean up the component list.
        Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
            If disposing Then
                If components IsNot Nothing Then
                    components.Dispose()
                End If
                ds.Dispose()
            End If
            MyBase.Dispose(disposing)
        End Sub
        Friend WithEvents XrPictureBox1 As DevExpress.XtraReports.UI.XRPictureBox
        Friend WithEvents XrLabel_Creditor_Address As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrBarCode_PostalCode As DevExpress.XtraReports.UI.XRBarCode

        'Required by the Component Designer
        Private components As System.ComponentModel.IContainer

        '<System.Diagnostics.DebuggerStepThrough()> _
        Private Sub InitializeComponent()
            Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(CreditorInvoiceReport))
            Dim PostNetGenerator1 As DevExpress.XtraPrinting.BarCode.PostNetGenerator = New DevExpress.XtraPrinting.BarCode.PostNetGenerator()
            Me.PageHeader = New DevExpress.XtraReports.UI.PageHeaderBand()
            Me.XrLabel_CreditorName = New DevExpress.XtraReports.UI.XRLabel()
            Me.Text2 = New DevExpress.XtraReports.UI.XRLabel()
            Me.PageFooter = New DevExpress.XtraReports.UI.PageFooterBand()
            Me.XrPageInfo1 = New DevExpress.XtraReports.UI.XRPageInfo()
            Me.Text_Footer = New DevExpress.XtraReports.UI.XRLabel()
            Me.ReportHeader = New DevExpress.XtraReports.UI.ReportHeaderBand()
            Me.XrPictureBox1 = New DevExpress.XtraReports.UI.XRPictureBox()
            Me.XrPanel_CreditorAddress = New DevExpress.XtraReports.UI.XRPanel()
            Me.XrLabel_Creditor_Address = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrBarCode_PostalCode = New DevExpress.XtraReports.UI.XRBarCode()
            Me.Text7 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_PoNumber = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrPanel_Aging = New DevExpress.XtraReports.UI.XRPanel()
            Me.XrLabel_Aging_Net120 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel17 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_Aging_net90 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel15 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_Aging_Net60 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel13 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_Aging_Net30 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel11 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_Aging_Current = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel9 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLine1 = New DevExpress.XtraReports.UI.XRLine()
            Me.XrLabel8 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel7 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_Aging_Total = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_Aging_Creditor = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel4 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_Aging_Date = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel2 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrRichTextBox1 = New DevExpress.XtraReports.UI.XRRichText()
            Me.ParameterCreditor = New DevExpress.XtraReports.Parameters.Parameter()
            Me.Subreport_CheckDetail = New DevExpress.XtraReports.UI.XRSubreport()
            Me.CheckDetailReport1 = New DebtPlus.Reports.Creditor.Invoice.CheckDetailReport()
            Me.Subreport_InvoiceStatus = New DevExpress.XtraReports.UI.XRSubreport()
            Me.InvoiceStatusReport1 = New DebtPlus.Reports.Creditor.Invoice.InvoiceStatusReport()
            CType(Me.XrRichTextBox1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.CheckDetailReport1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.InvoiceStatusReport1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
            '
            'Detail
            '
            Me.Detail.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.Subreport_CheckDetail, Me.Subreport_InvoiceStatus})
            Me.Detail.HeightF = 46.0!
            Me.Detail.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
            Me.Detail.PageBreak = DevExpress.XtraReports.UI.PageBreak.AfterBand
            Me.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
            '
            'PageHeader
            '
            Me.PageHeader.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel_CreditorName, Me.Text2})
            Me.PageHeader.HeightF = 64.0!
            Me.PageHeader.Name = "PageHeader"
            Me.PageHeader.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
            Me.PageHeader.PrintOn = DevExpress.XtraReports.UI.PrintOnPages.NotWithReportHeader
            Me.PageHeader.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
            '
            'XrLabel_CreditorName
            '
            Me.XrLabel_CreditorName.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
            Me.XrLabel_CreditorName.BorderColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer))
            Me.XrLabel_CreditorName.Borders = DevExpress.XtraPrinting.BorderSide.None
            Me.XrLabel_CreditorName.BorderWidth = 1
            Me.XrLabel_CreditorName.CanGrow = False
            Me.XrLabel_CreditorName.Font = New System.Drawing.Font("Times New Roman", 14.0!, System.Drawing.FontStyle.Bold)
            Me.XrLabel_CreditorName.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer))
            Me.XrLabel_CreditorName.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 17.0!)
            Me.XrLabel_CreditorName.Name = "XrLabel_CreditorName"
            Me.XrLabel_CreditorName.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_CreditorName.SizeF = New System.Drawing.SizeF(800.0!, 26.0!)
            Me.XrLabel_CreditorName.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter
            '
            'Text2
            '
            Me.Text2.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
            Me.Text2.BorderColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer))
            Me.Text2.Borders = DevExpress.XtraPrinting.BorderSide.None
            Me.Text2.BorderWidth = 1
            Me.Text2.CanGrow = False
            Me.Text2.Font = New System.Drawing.Font("Times New Roman", 10.0!)
            Me.Text2.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer))
            Me.Text2.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
            Me.Text2.Name = "Text2"
            Me.Text2.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.Text2.SizeF = New System.Drawing.SizeF(800.0!, 16.0!)
            Me.Text2.Text = "ClearPoint Credit Counseling Solutions"
            Me.Text2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter
            '
            'PageFooter
            '
            Me.PageFooter.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrPageInfo1, Me.Text_Footer})
            Me.PageFooter.HeightF = 42.0!
            Me.PageFooter.Name = "PageFooter"
            Me.PageFooter.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
            Me.PageFooter.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
            '
            'XrPageInfo1
            '
            Me.XrPageInfo1.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrPageInfo1.ForeColor = System.Drawing.Color.Teal
            Me.XrPageInfo1.Format = "Page {0}"
            Me.XrPageInfo1.LocationFloat = New DevExpress.Utils.PointFloat(700.0!, 8.0!)
            Me.XrPageInfo1.Name = "XrPageInfo1"
            Me.XrPageInfo1.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrPageInfo1.PageInfo = DevExpress.XtraPrinting.PageInfo.Number
            Me.XrPageInfo1.SizeF = New System.Drawing.SizeF(100.0!, 17.0!)
            Me.XrPageInfo1.StylePriority.UseFont = False
            Me.XrPageInfo1.StylePriority.UseForeColor = False
            Me.XrPageInfo1.StylePriority.UseTextAlignment = False
            Me.XrPageInfo1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'Text_Footer
            '
            Me.Text_Footer.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
            Me.Text_Footer.BorderColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer))
            Me.Text_Footer.Borders = DevExpress.XtraPrinting.BorderSide.None
            Me.Text_Footer.BorderWidth = 1
            Me.Text_Footer.CanGrow = False
            Me.Text_Footer.Font = New System.Drawing.Font("Times New Roman", 10.0!)
            Me.Text_Footer.ForeColor = System.Drawing.Color.FromArgb(CType(CType(128, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(128, Byte), Integer))
            Me.Text_Footer.KeepTogether = True
            Me.Text_Footer.LocationFloat = New DevExpress.Utils.PointFloat(100.0!, 8.0!)
            Me.Text_Footer.Multiline = True
            Me.Text_Footer.Name = "Text_Footer"
            Me.Text_Footer.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.Text_Footer.Scripts.OnPrintOnPage = "Text_Footer_PrintOnPage"
            Me.Text_Footer.SizeF = New System.Drawing.SizeF(600.0!, 33.0!)
            Me.Text_Footer.Text = "PLEASE RETURN THIS STATEMENT WITH YOUR REMITTANCE" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "THANK YOU"
            Me.Text_Footer.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter
            '
            'ReportHeader
            '
            Me.ReportHeader.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrPictureBox1, Me.XrPanel_CreditorAddress, Me.Text7, Me.XrLabel_PoNumber, Me.XrPanel_Aging, Me.XrRichTextBox1})
            Me.ReportHeader.HeightF = 345.0!
            Me.ReportHeader.Name = "ReportHeader"
            '
            'XrPictureBox1
            '
            Me.XrPictureBox1.Image = CType(resources.GetObject("XrPictureBox1.Image"), System.Drawing.Image)
            Me.XrPictureBox1.LocationFloat = New DevExpress.Utils.PointFloat(10.00001!, 0.0!)
            Me.XrPictureBox1.Name = "XrPictureBox1"
            Me.XrPictureBox1.SizeF = New System.Drawing.SizeF(177.0417!, 98.0!)
            '
            'XrPanel_CreditorAddress
            '
            Me.XrPanel_CreditorAddress.Borders = DevExpress.XtraPrinting.BorderSide.None
            Me.XrPanel_CreditorAddress.BorderWidth = 1
            Me.XrPanel_CreditorAddress.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel_Creditor_Address, Me.XrBarCode_PostalCode})
            Me.XrPanel_CreditorAddress.LocationFloat = New DevExpress.Utils.PointFloat(68.0!, 200.0!)
            Me.XrPanel_CreditorAddress.Name = "XrPanel_CreditorAddress"
            Me.XrPanel_CreditorAddress.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
            Me.XrPanel_CreditorAddress.SizeF = New System.Drawing.SizeF(307.2917!, 130.0!)
            Me.XrPanel_CreditorAddress.StylePriority.UseBorders = False
            Me.XrPanel_CreditorAddress.StylePriority.UseBorderWidth = False
            '
            'XrLabel_Creditor_Address
            '
            Me.XrLabel_Creditor_Address.Borders = DevExpress.XtraPrinting.BorderSide.None
            Me.XrLabel_Creditor_Address.BorderWidth = 1
            Me.XrLabel_Creditor_Address.LocationFloat = New DevExpress.Utils.PointFloat(5.0!, 25.0!)
            Me.XrLabel_Creditor_Address.Multiline = True
            Me.XrLabel_Creditor_Address.Name = "XrLabel_Creditor_Address"
            Me.XrLabel_Creditor_Address.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_Creditor_Address.Scripts.OnBeforePrint = "XrLabel_Creditor_Address_BeforePrint"
            Me.XrLabel_Creditor_Address.SizeF = New System.Drawing.SizeF(300.0!, 100.0!)
            Me.XrLabel_Creditor_Address.StylePriority.UseBorders = False
            Me.XrLabel_Creditor_Address.StylePriority.UseBorderWidth = False
            Me.XrLabel_Creditor_Address.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
            '
            'XrBarCode_PostalCode
            '
            Me.XrBarCode_PostalCode.Borders = DevExpress.XtraPrinting.BorderSide.None
            Me.XrBarCode_PostalCode.BorderWidth = 1
            Me.XrBarCode_PostalCode.LocationFloat = New DevExpress.Utils.PointFloat(5.0!, 5.0!)
            Me.XrBarCode_PostalCode.Name = "XrBarCode_PostalCode"
            Me.XrBarCode_PostalCode.Padding = New DevExpress.XtraPrinting.PaddingInfo(10, 10, 0, 0, 100.0!)
            Me.XrBarCode_PostalCode.Scripts.OnBeforePrint = "XrBarCode_PostalCode_BeforePrint"
            Me.XrBarCode_PostalCode.ShowText = False
            Me.XrBarCode_PostalCode.SizeF = New System.Drawing.SizeF(300.0!, 16.0!)
            Me.XrBarCode_PostalCode.StylePriority.UseBorders = False
            Me.XrBarCode_PostalCode.StylePriority.UseBorderWidth = False
            Me.XrBarCode_PostalCode.StylePriority.UsePadding = False
            Me.XrBarCode_PostalCode.Symbology = PostNetGenerator1
            Me.XrBarCode_PostalCode.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
            '
            'Text7
            '
            Me.Text7.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
            Me.Text7.BorderColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer))
            Me.Text7.Borders = DevExpress.XtraPrinting.BorderSide.None
            Me.Text7.BorderWidth = 1
            Me.Text7.CanGrow = False
            Me.Text7.Font = New System.Drawing.Font("Times New Roman", 8.0!, System.Drawing.FontStyle.Bold)
            Me.Text7.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer))
            Me.Text7.LocationFloat = New DevExpress.Utils.PointFloat(10.00001!, 116.0!)
            Me.Text7.Name = "Text7"
            Me.Text7.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.Text7.SizeF = New System.Drawing.SizeF(469.0!, 15.0!)
            Me.Text7.Text = "Contribution Request: Payments distributed through CCCS Debt Management Programs"
            Me.Text7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
            '
            'XrLabel_PoNumber
            '
            Me.XrLabel_PoNumber.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
            Me.XrLabel_PoNumber.BorderColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer))
            Me.XrLabel_PoNumber.Borders = DevExpress.XtraPrinting.BorderSide.None
            Me.XrLabel_PoNumber.BorderWidth = 1
            Me.XrLabel_PoNumber.Font = New System.Drawing.Font("Times New Roman", 10.0!)
            Me.XrLabel_PoNumber.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer))
            Me.XrLabel_PoNumber.LocationFloat = New DevExpress.Utils.PointFloat(10.00001!, 156.75!)
            Me.XrLabel_PoNumber.Name = "XrLabel_PoNumber"
            Me.XrLabel_PoNumber.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_PoNumber.SizeF = New System.Drawing.SizeF(400.0!, 19.0!)
            Me.XrLabel_PoNumber.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
            '
            'XrPanel_Aging
            '
            Me.XrPanel_Aging.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel_Aging_Net120, Me.XrLabel17, Me.XrLabel_Aging_net90, Me.XrLabel15, Me.XrLabel_Aging_Net60, Me.XrLabel13, Me.XrLabel_Aging_Net30, Me.XrLabel11, Me.XrLabel_Aging_Current, Me.XrLabel9, Me.XrLine1, Me.XrLabel8, Me.XrLabel7, Me.XrLabel_Aging_Total, Me.XrLabel_Aging_Creditor, Me.XrLabel4, Me.XrLabel_Aging_Date, Me.XrLabel2})
            Me.XrPanel_Aging.LocationFloat = New DevExpress.Utils.PointFloat(508.0!, 156.75!)
            Me.XrPanel_Aging.Name = "XrPanel_Aging"
            Me.XrPanel_Aging.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
            Me.XrPanel_Aging.Scripts.OnBeforePrint = "XrPanel_Aging_BeforePrint"
            Me.XrPanel_Aging.SizeF = New System.Drawing.SizeF(283.0!, 185.0!)
            '
            'XrLabel_Aging_Net120
            '
            Me.XrLabel_Aging_Net120.Font = New System.Drawing.Font("Times New Roman", 9.75!)
            Me.XrLabel_Aging_Net120.LocationFloat = New DevExpress.Utils.PointFloat(150.0!, 168.0!)
            Me.XrLabel_Aging_Net120.Name = "XrLabel_Aging_Net120"
            Me.XrLabel_Aging_Net120.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_Aging_Net120.SizeF = New System.Drawing.SizeF(125.0!, 17.0!)
            Me.XrLabel_Aging_Net120.Text = "$0.00"
            Me.XrLabel_Aging_Net120.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
            '
            'XrLabel17
            '
            Me.XrLabel17.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 168.0!)
            Me.XrLabel17.Name = "XrLabel17"
            Me.XrLabel17.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel17.SizeF = New System.Drawing.SizeF(142.0!, 17.0!)
            Me.XrLabel17.Text = "120 and Over"
            Me.XrLabel17.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
            '
            'XrLabel_Aging_net90
            '
            Me.XrLabel_Aging_net90.Font = New System.Drawing.Font("Times New Roman", 9.75!)
            Me.XrLabel_Aging_net90.LocationFloat = New DevExpress.Utils.PointFloat(150.0!, 151.0!)
            Me.XrLabel_Aging_net90.Name = "XrLabel_Aging_net90"
            Me.XrLabel_Aging_net90.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_Aging_net90.SizeF = New System.Drawing.SizeF(125.0!, 17.0!)
            Me.XrLabel_Aging_net90.Text = "$0.00"
            Me.XrLabel_Aging_net90.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
            '
            'XrLabel15
            '
            Me.XrLabel15.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 151.0!)
            Me.XrLabel15.Name = "XrLabel15"
            Me.XrLabel15.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel15.SizeF = New System.Drawing.SizeF(142.0!, 17.0!)
            Me.XrLabel15.Text = "90 Days"
            Me.XrLabel15.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
            '
            'XrLabel_Aging_Net60
            '
            Me.XrLabel_Aging_Net60.Font = New System.Drawing.Font("Times New Roman", 9.75!)
            Me.XrLabel_Aging_Net60.LocationFloat = New DevExpress.Utils.PointFloat(150.0!, 134.0!)
            Me.XrLabel_Aging_Net60.Name = "XrLabel_Aging_Net60"
            Me.XrLabel_Aging_Net60.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_Aging_Net60.SizeF = New System.Drawing.SizeF(125.0!, 17.0!)
            Me.XrLabel_Aging_Net60.Text = "$0.00"
            Me.XrLabel_Aging_Net60.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
            '
            'XrLabel13
            '
            Me.XrLabel13.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 134.0!)
            Me.XrLabel13.Name = "XrLabel13"
            Me.XrLabel13.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel13.SizeF = New System.Drawing.SizeF(142.0!, 17.0!)
            Me.XrLabel13.Text = "60 Days"
            Me.XrLabel13.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
            '
            'XrLabel_Aging_Net30
            '
            Me.XrLabel_Aging_Net30.Font = New System.Drawing.Font("Times New Roman", 9.75!)
            Me.XrLabel_Aging_Net30.LocationFloat = New DevExpress.Utils.PointFloat(150.0!, 117.0!)
            Me.XrLabel_Aging_Net30.Name = "XrLabel_Aging_Net30"
            Me.XrLabel_Aging_Net30.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_Aging_Net30.SizeF = New System.Drawing.SizeF(125.0!, 17.0!)
            Me.XrLabel_Aging_Net30.Text = "$0.00"
            Me.XrLabel_Aging_Net30.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
            '
            'XrLabel11
            '
            Me.XrLabel11.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 117.0!)
            Me.XrLabel11.Name = "XrLabel11"
            Me.XrLabel11.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel11.SizeF = New System.Drawing.SizeF(142.0!, 17.0!)
            Me.XrLabel11.Text = "30 Days"
            Me.XrLabel11.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
            '
            'XrLabel_Aging_Current
            '
            Me.XrLabel_Aging_Current.Font = New System.Drawing.Font("Times New Roman", 9.75!)
            Me.XrLabel_Aging_Current.LocationFloat = New DevExpress.Utils.PointFloat(150.0!, 100.0!)
            Me.XrLabel_Aging_Current.Name = "XrLabel_Aging_Current"
            Me.XrLabel_Aging_Current.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_Aging_Current.SizeF = New System.Drawing.SizeF(125.0!, 17.0!)
            Me.XrLabel_Aging_Current.Text = "$0.00"
            Me.XrLabel_Aging_Current.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
            '
            'XrLabel9
            '
            Me.XrLabel9.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 100.0!)
            Me.XrLabel9.Name = "XrLabel9"
            Me.XrLabel9.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel9.SizeF = New System.Drawing.SizeF(142.0!, 17.0!)
            Me.XrLabel9.Text = "Current Amount Due:"
            Me.XrLabel9.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
            '
            'XrLine1
            '
            Me.XrLine1.LineWidth = 2
            Me.XrLine1.LocationFloat = New DevExpress.Utils.PointFloat(150.0!, 82.99998!)
            Me.XrLine1.Name = "XrLine1"
            Me.XrLine1.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
            Me.XrLine1.SizeF = New System.Drawing.SizeF(125.0!, 9.000015!)
            '
            'XrLabel8
            '
            Me.XrLabel8.LocationFloat = New DevExpress.Utils.PointFloat(17.0!, 75.0!)
            Me.XrLabel8.Name = "XrLabel8"
            Me.XrLabel8.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel8.SizeF = New System.Drawing.SizeF(117.0!, 17.0!)
            Me.XrLabel8.Text = "Amount Enclosed:"
            Me.XrLabel8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
            '
            'XrLabel7
            '
            Me.XrLabel7.LocationFloat = New DevExpress.Utils.PointFloat(17.0!, 34.0!)
            Me.XrLabel7.Name = "XrLabel7"
            Me.XrLabel7.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel7.SizeF = New System.Drawing.SizeF(117.0!, 17.0!)
            Me.XrLabel7.Text = "Total Amount Due:"
            Me.XrLabel7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
            '
            'XrLabel_Aging_Total
            '
            Me.XrLabel_Aging_Total.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel_Aging_Total.LocationFloat = New DevExpress.Utils.PointFloat(150.0!, 34.00002!)
            Me.XrLabel_Aging_Total.Name = "XrLabel_Aging_Total"
            Me.XrLabel_Aging_Total.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_Aging_Total.SizeF = New System.Drawing.SizeF(125.0!, 17.0!)
            Me.XrLabel_Aging_Total.Text = "$0.00"
            Me.XrLabel_Aging_Total.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
            '
            'XrLabel_Aging_Creditor
            '
            Me.XrLabel_Aging_Creditor.LocationFloat = New DevExpress.Utils.PointFloat(183.0!, 17.00001!)
            Me.XrLabel_Aging_Creditor.Name = "XrLabel_Aging_Creditor"
            Me.XrLabel_Aging_Creditor.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_Aging_Creditor.SizeF = New System.Drawing.SizeF(92.0!, 17.0!)
            Me.XrLabel_Aging_Creditor.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
            '
            'XrLabel4
            '
            Me.XrLabel4.LocationFloat = New DevExpress.Utils.PointFloat(17.0!, 17.0!)
            Me.XrLabel4.Name = "XrLabel4"
            Me.XrLabel4.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel4.SizeF = New System.Drawing.SizeF(92.0!, 17.0!)
            Me.XrLabel4.Text = "Creditor:"
            Me.XrLabel4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
            '
            'XrLabel_Aging_Date
            '
            Me.XrLabel_Aging_Date.LocationFloat = New DevExpress.Utils.PointFloat(183.0!, 0.0!)
            Me.XrLabel_Aging_Date.Name = "XrLabel_Aging_Date"
            Me.XrLabel_Aging_Date.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_Aging_Date.SizeF = New System.Drawing.SizeF(92.0!, 17.0!)
            Me.XrLabel_Aging_Date.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
            '
            'XrLabel2
            '
            Me.XrLabel2.LocationFloat = New DevExpress.Utils.PointFloat(17.0!, 0.0!)
            Me.XrLabel2.Name = "XrLabel2"
            Me.XrLabel2.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel2.SizeF = New System.Drawing.SizeF(92.0!, 17.0!)
            Me.XrLabel2.Text = "Billing Date:"
            Me.XrLabel2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
            '
            'XrRichTextBox1
            '
            Me.XrRichTextBox1.Font = New System.Drawing.Font("Times New Roman", 9.0!)
            Me.XrRichTextBox1.LocationFloat = New DevExpress.Utils.PointFloat(202.0833!, 0.0!)
            Me.XrRichTextBox1.Name = "XrRichTextBox1"
            Me.XrRichTextBox1.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 0, 0, 0, 100.0!)
            Me.XrRichTextBox1.SerializableRtfString = resources.GetString("XrRichTextBox1.SerializableRtfString")
            Me.XrRichTextBox1.SizeF = New System.Drawing.SizeF(372.9167!, 105.0!)
            Me.XrRichTextBox1.StylePriority.UseFont = False
            Me.XrRichTextBox1.StylePriority.UsePadding = False
            Me.XrRichTextBox1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
            '
            'ParameterCreditor
            '
            Me.ParameterCreditor.Description = "Creditor ID"
            Me.ParameterCreditor.Name = "ParameterCreditor"
            Me.ParameterCreditor.Value = ""
            Me.ParameterCreditor.Visible = False
            '
            'Subreport_CheckDetail
            '
            Me.Subreport_CheckDetail.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 23.0!)
            Me.Subreport_CheckDetail.Name = "Subreport_CheckDetail"
            Me.Subreport_CheckDetail.ReportSource = Me.CheckDetailReport1
            Me.Subreport_CheckDetail.Scripts.OnBeforePrint = "Subreport_CheckDetail_BeforePrint"
            Me.Subreport_CheckDetail.SizeF = New System.Drawing.SizeF(800.0!, 23.0!)
            '
            'Subreport_InvoiceStatus
            '
            Me.Subreport_InvoiceStatus.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
            Me.Subreport_InvoiceStatus.Name = "Subreport_InvoiceStatus"
            Me.Subreport_InvoiceStatus.ReportSource = Me.InvoiceStatusReport1
            Me.Subreport_InvoiceStatus.Scripts.OnBeforePrint = "Subreport_InvoiceStatus_BeforePrint"
            Me.Subreport_InvoiceStatus.SizeF = New System.Drawing.SizeF(800.0!, 23.0!)
            '
            'CreditorInvoiceReport
            '
            Me.Bands.AddRange(New DevExpress.XtraReports.UI.Band() {Me.TopMarginBand1, Me.Detail, Me.BottomMarginBand1, Me.PageHeader, Me.PageFooter, Me.ReportHeader})
            Me.PageWidth = 852
            Me.PaperKind = System.Drawing.Printing.PaperKind.Custom
            Me.Parameters.AddRange(New DevExpress.XtraReports.Parameters.Parameter() {Me.ParameterCreditor})
            Me.RequestParameters = False
            Me.ScriptLanguage = DevExpress.XtraReports.ScriptLanguage.VisualBasic
            Me.ScriptReferencesString = "C:\Documents and Settings\Al Longyear\My Documents\Visual Studio 2008\Projects\De" & _
                "btPlus\Executables\DebtPlus.Data.dll"
            Me.Scripts.OnBeforePrint = "CreditorInvoiceReport_BeforePrint"
            Me.ScriptsSource = resources.GetString("$this.ScriptsSource")
            Me.Version = "11.2"
            Me.Controls.SetChildIndex(Me.ReportHeader, 0)
            Me.Controls.SetChildIndex(Me.PageFooter, 0)
            Me.Controls.SetChildIndex(Me.PageHeader, 0)
            Me.Controls.SetChildIndex(Me.BottomMarginBand1, 0)
            Me.Controls.SetChildIndex(Me.Detail, 0)
            Me.Controls.SetChildIndex(Me.TopMarginBand1, 0)
            CType(Me.XrRichTextBox1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.CheckDetailReport1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.InvoiceStatusReport1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me, System.ComponentModel.ISupportInitialize).EndInit()

        End Sub
        'NOTE: The following procedure is required by the Component Designer
        'It can be modified using the Component Designer.
        'Do not modify it using the code editor.

        Friend WithEvents PageHeader As DevExpress.XtraReports.UI.PageHeaderBand
        Friend WithEvents PageFooter As DevExpress.XtraReports.UI.PageFooterBand
        Friend WithEvents Subreport_InvoiceStatus As DevExpress.XtraReports.UI.XRSubreport
        Friend WithEvents Subreport_CheckDetail As DevExpress.XtraReports.UI.XRSubreport
        Friend WithEvents Text_Footer As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrPageInfo1 As DevExpress.XtraReports.UI.XRPageInfo
        Friend WithEvents XrLabel_CreditorName As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents Text2 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents ReportHeader As DevExpress.XtraReports.UI.ReportHeaderBand
        Friend WithEvents XrPanel_CreditorAddress As DevExpress.XtraReports.UI.XRPanel
        Friend WithEvents Text7 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_PoNumber As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrPanel_Aging As DevExpress.XtraReports.UI.XRPanel
        Friend WithEvents XrLabel_Aging_Net120 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel17 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_Aging_net90 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel15 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_Aging_Net60 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel13 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_Aging_Net30 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel11 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_Aging_Current As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel9 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLine1 As DevExpress.XtraReports.UI.XRLine
        Friend WithEvents XrLabel8 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel7 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_Aging_Total As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_Aging_Creditor As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel4 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_Aging_Date As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel2 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrRichTextBox1 As DevExpress.XtraReports.UI.XRRichText
        Private WithEvents CheckDetailReport1 As DebtPlus.Reports.Creditor.Invoice.CheckDetailReport
        Private WithEvents InvoiceStatusReport1 As DebtPlus.Reports.Creditor.Invoice.InvoiceStatusReport
        Friend WithEvents ParameterCreditor As DevExpress.XtraReports.Parameters.Parameter
#End Region

        Public ds As New System.Data.DataSet("ds")

        Private Sub CreditorInvoiceReport_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs)

            Dim MasterReport As DevExpress.XtraReports.UI.XtraReport = CType(sender, DevExpress.XtraReports.UI.XtraReport)

            With MasterReport
                Dim tbl As System.Data.DataTable = ds.Tables("rpt_creditor_invoice")
                If tbl IsNot Nothing Then
                    tbl.Clear()
                End If

                '-- Define the parameters for the report
                Dim cn As System.Data.SqlClient.SqlConnection = New System.Data.SqlClient.SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
                Using cmd As System.Data.SqlClient.SqlCommand = New System.Data.SqlClient.SqlCommand()
                    With cmd
                        .Connection = cn
                        .CommandText = "rpt_creditor_invoice"
                        .CommandType = System.Data.CommandType.StoredProcedure
                        .CommandTimeout = 0
                        .Parameters.Add("@creditor", System.Data.SqlDbType.VarChar, 10).Value = MasterReport.Parameters("ParameterCreditor").Value
                    End With

                    '-- Read the datasource
                    Using da As New System.Data.SqlClient.SqlDataAdapter(cmd)
                        da.Fill(ds, "rpt_creditor_invoice")
                        tbl = ds.Tables("rpt_creditor_invoice")
                    End Using
                End Using

                '-- Find the datasource
                Dim vue As System.Data.DataView = tbl.DefaultView
                .DataSource = vue

                '-- Set the PO Number
                Dim NameString As String = String.Empty
                If vue.Count > 0 Then
                    With XrLabel_PoNumber
                        If vue(0)("po_number") IsNot System.DBNull.Value Then .Text = "P.O. Number: " + Convert.ToString(vue(0)("po_number"))
                    End With

                    If vue(0)("creditor_name") IsNot System.DBNull.Value Then NameString = Convert.ToString(vue(0)("creditor_name")).Trim
                End If

                With XrLabel_CreditorName
                    If NameString <> String.Empty Then
                        .Text = NameString + " (" + CType(MasterReport.Parameters("ParameterCreditor").Value, String) + ")"
                    Else
                        .Text = CType(MasterReport.Parameters("ParameterCreditor").Value, String)
                    End If
                End With
            End With
        End Sub

        Private Sub Subreport_InvoiceStatus_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs)

            With CType(sender, DevExpress.XtraReports.UI.XRSubreport)
                Dim MasterReport As DevExpress.XtraReports.UI.XtraReport = CType(.Report, DevExpress.XtraReports.UI.XtraReport)
                Dim SubReport As DevExpress.XtraReports.UI.XtraReport = CType(.ReportSource, DevExpress.XtraReports.UI.XtraReport)

                '-- Erase the current data
                Dim tbl As System.Data.DataTable = ds.Tables("rpt_creditor_invoice_detail")
                If tbl IsNot Nothing Then
                    tbl.Clear()
                End If

                Try
                    Using cn As System.Data.SqlClient.SqlConnection = New System.Data.SqlClient.SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
                        cn.Open()
                        Using cmd As System.Data.SqlClient.SqlCommand = New System.Data.SqlClient.SqlCommand()
                            With cmd
                                .Connection = cn
                                .CommandText = "rpt_creditor_invoice_detail"
                                .CommandType = System.Data.CommandType.StoredProcedure
                                .Parameters.Add("@creditor", System.Data.SqlDbType.VarChar, 80).Value = MasterReport.Parameters("ParameterCreditor").Value
                                .CommandTimeout = 0
                            End With

                            Using da As New System.Data.SqlClient.SqlDataAdapter(cmd)
                                da.Fill(ds, "rpt_creditor_invoice_detail")
                                tbl = ds.Tables("rpt_creditor_invoice_detail")
                                With tbl
                                    If Not .Columns.Contains("net_amt") Then
                                        .Columns.Add(New System.Data.DataColumn("net_amt", GetType(Decimal), "isnull([inv_amt],0)-isnull([adj_amt],0)-isnull([pmt_amt],0)"))
                                    End If
                                End With
                            End Using
                        End Using

                        If tbl.Rows.Count <= 0 Then
                            e.Cancel = True
                        Else
                            SubReport.DataSource = tbl.DefaultView
                        End If
                    End Using

                Catch ex As System.Data.SqlClient.SqlException
                    DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error reading invoice_detail")
                End Try
            End With

        End Sub

        Private Sub Subreport_CheckDetail_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs)

            With CType(sender, DevExpress.XtraReports.UI.XRSubreport)
                Dim MasterReport As DevExpress.XtraReports.UI.XtraReport = CType(.Report, DevExpress.XtraReports.UI.XtraReport)
                Dim SubReport As DevExpress.XtraReports.UI.XtraReport = CType(.ReportSource, DevExpress.XtraReports.UI.XtraReport)

                '-- Clear the current transactions for the invoice
                Dim tbl As System.Data.DataTable = ds.Tables("rpt_creditor_invoice_current")
                If tbl IsNot Nothing Then
                    tbl.Clear()
                End If

                '-- Obtain the invoice data
                Using cn As System.Data.SqlClient.SqlConnection = New System.Data.SqlClient.SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
                    cn.Open()
                    Using cmd As System.Data.SqlClient.SqlCommand = New System.Data.SqlClient.SqlCommand()
                        With cmd
                            .Connection = cn
                            .CommandText = "rpt_creditor_invoice_current"
                            .CommandType = System.Data.CommandType.StoredProcedure
                            .Parameters.Add("@creditor", System.Data.SqlDbType.VarChar, 80).Value = MasterReport.Parameters("ParameterCreditor").Value
                            .CommandTimeout = 0
                        End With

                        Using da As New System.Data.SqlClient.SqlDataAdapter(cmd)
                            da.Fill(ds, "rpt_creditor_invoice_current")
                            tbl = ds.Tables("rpt_creditor_invoice_current")
                        End Using
                    End Using
                End Using

                If tbl.Rows.Count = 0 Then
                    e.Cancel = True
                Else
                    SubReport.DataSource = tbl.DefaultView
                End If
            End With
        End Sub

        Private Sub XrLabel_Creditor_Address_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs)

            With CType(sender, DevExpress.XtraReports.UI.XRLabel)
                Dim MasterReport As DevExpress.XtraReports.UI.XtraReport = CType(.Report, DevExpress.XtraReports.UI.XtraReport)
                Dim ds As System.Data.DataSet = CType(MasterReport.DataSource, System.Data.DataView).Table.DataSet
                Dim tbl As System.Data.DataTable = ds.Tables("creditor_addresses")
                Dim row As System.Data.DataRow = Nothing
                Dim Creditor As String = Convert.ToString(MasterReport.Parameters("ParameterCreditor").Value)

                '-- If there is a table then find the row
                If tbl IsNot Nothing Then
                    row = tbl.Rows.Find(Creditor)
                End If

                If row Is Nothing Then
                    Try
                        Using cn As System.Data.SqlClient.SqlConnection = New System.Data.SqlClient.SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
                            cn.Open()
                            Using cmd As System.Data.SqlClient.SqlCommand = New System.Data.SqlClient.SqlCommand()
                                With cmd
                                    .Connection = cn
                                    .CommandText = "rpt_CreditorAddress_I"
                                    .CommandType = System.Data.CommandType.StoredProcedure
                                    .Parameters.Add("@creditor", System.Data.SqlDbType.VarChar, 10).Value = Creditor
                                    .CommandTimeout = 0
                                End With

                                Using da As New System.Data.SqlClient.SqlDataAdapter(cmd)
                                    da.Fill(ds, "rpt_CreditorAddress_I")
                                    tbl = ds.Tables("rpt_CreditorAddress_I")
                                    With tbl
                                        If .PrimaryKey.GetUpperBound(0) < 0 Then
                                            .PrimaryKey = New System.Data.DataColumn() {.Columns("creditor")}
                                        End If
                                    End With
                                End Using
                            End Using
                        End Using

                        If tbl IsNot Nothing Then
                            row = tbl.Rows.Find(Creditor)
                        End If

                    Catch ex As System.Data.SqlClient.SqlException
                        DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error reading creditor address")
                    End Try
                End If

                Dim text_block As New System.Text.StringBuilder
                If row IsNot Nothing Then
                    For Each NameString As String In New String() {"addr1", "addr2", "addr3", "addr4", "addr5", "addr6", "addr7"}
                        Dim Value As String
                        If row(NameString) IsNot Nothing AndAlso row(NameString) IsNot System.DBNull.Value Then
                            Value = Convert.ToString(row(NameString)).Trim
                            If Value <> String.Empty Then
                                text_block.Append(Environment.NewLine)
                                text_block.Append(Value)
                            End If
                        End If
                    Next
                End If

                If text_block.Length > 0 Then
                    text_block.Remove(0, 2)
                End If

                .Text = text_block.ToString
            End With

        End Sub

        Private Sub XrBarCode_PostalCode_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs)

            With CType(sender, DevExpress.XtraReports.UI.XRBarCode)
                Dim MasterReport As DevExpress.XtraReports.UI.XtraReport = CType(.Report, DevExpress.XtraReports.UI.XtraReport)
                Dim ds As System.Data.DataSet = CType(MasterReport.DataSource, System.Data.DataView).Table.DataSet
                Dim tbl As System.Data.DataTable = ds.Tables("rpt_CreditorAddress_I")
                Dim row As System.Data.DataRow = Nothing
                Dim Creditor As String = Convert.ToString(MasterReport.Parameters("ParameterCreditor").Value)

                '-- If there is a table then find the row
                If tbl IsNot Nothing Then
                    row = tbl.Rows.Find(Creditor)
                End If

                If row Is Nothing Then
                    Try
                        Using cn As System.Data.SqlClient.SqlConnection = New System.Data.SqlClient.SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
                            cn.Open()
                            Using cmd As System.Data.SqlClient.SqlCommand = New System.Data.SqlClient.SqlCommand()
                                With cmd
                                    .Connection = cn
                                    .CommandText = "rpt_CreditorAddress_I"
                                    .CommandType = System.Data.CommandType.StoredProcedure
                                    .Parameters.Add("@creditor", System.Data.SqlDbType.VarChar, 10).Value = Creditor
                                    .CommandTimeout = 0
                                End With

                                Using da As New System.Data.SqlClient.SqlDataAdapter(cmd)
                                    da.Fill(ds, "rpt_CreditorAddress_I")
                                    tbl = ds.Tables("rpt_CreditorAddress_I")
                                    With tbl
                                        If .PrimaryKey.GetUpperBound(0) < 0 Then
                                            .PrimaryKey = New System.Data.DataColumn() {.Columns("creditor")}
                                        End If
                                    End With
                                End Using
                            End Using
                        End Using

                        If tbl IsNot Nothing Then
                            row = tbl.Rows.Find(Creditor)
                        End If

                    Catch ex As System.Data.SqlClient.SqlException
                        DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error reading creditor address")
                    End Try
                End If

                Dim text_block As String = String.Empty
                If row IsNot Nothing Then
                    If row("zipcode") IsNot Nothing AndAlso row("zipcode") IsNot System.DBNull.Value Then
                        text_block = Convert.ToString(row("zipcode"))
                    End If
                    text_block = System.Text.RegularExpressions.Regex.Replace(text_block, "[^0-9]", String.Empty)
                End If

                If text_block = String.Empty Then
                    e.Cancel = True
                Else
                    .Text = text_block
                End If
            End With

        End Sub

        Private Sub XrPanel_Aging_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs)

            With CType(sender, DevExpress.XtraReports.UI.XRPanel)
                Dim MasterReport As DevExpress.XtraReports.UI.XtraReport = CType(.Report, DevExpress.XtraReports.UI.XtraReport)
                Dim Creditor As String = Convert.ToString(MasterReport.Parameters("ParameterCreditor").Value)

                Dim rd As System.Data.SqlClient.SqlDataReader = Nothing

                Try
                    Using cn As System.Data.SqlClient.SqlConnection = New System.Data.SqlClient.SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
                        cn.Open()

                        Using cmd As System.Data.SqlClient.SqlCommand = New System.Data.SqlClient.SqlCommand()
                            With cmd
                                .Connection = cn
                                .CommandText = "rpt_creditor_invoice_aging"
                                .CommandType = System.Data.CommandType.StoredProcedure
                                .Parameters.Add("@creditor", System.Data.SqlDbType.VarChar, 10).Value = Creditor
                                .CommandTimeout = 0
                                rd = .ExecuteReader(System.Data.CommandBehavior.CloseConnection Or System.Data.CommandBehavior.SingleRow)
                            End With
                        End Using

                        If rd IsNot Nothing AndAlso rd.Read Then
                            .FindControl("XrLabel_Aging_Date", True).Text = rd.GetDateTime(rd.GetOrdinal("billing_date")).ToShortDateString
                            .FindControl("XrLabel_Aging_Creditor", True).Text = rd.GetString(rd.GetOrdinal("creditor"))
                            .FindControl("XrLabel_Aging_Current", True).Text = System.String.Format("{0:c}", rd.GetDecimal(rd.GetOrdinal("current")))
                            .FindControl("XrLabel_Aging_Net30", True).Text = System.String.Format("{0:c}", rd.GetDecimal(rd.GetOrdinal("net_30")))
                            .FindControl("XrLabel_Aging_Net60", True).Text = System.String.Format("{0:c}", rd.GetDecimal(rd.GetOrdinal("net_60")))
                            .FindControl("XrLabel_Aging_net90", True).Text = System.String.Format("{0:c}", rd.GetDecimal(rd.GetOrdinal("net_90")))
                            .FindControl("XrLabel_Aging_Net120", True).Text = System.String.Format("{0:c}", rd.GetDecimal(rd.GetOrdinal("net_120")))
                            .FindControl("XrLabel_Aging_Total", True).Text = System.String.Format("{0:c}", rd.GetDecimal(rd.GetOrdinal("total_due")))
                        End If
                    End Using

                Finally
                    If rd IsNot Nothing AndAlso Not rd.IsClosed Then rd.Close()
                End Try
            End With
        End Sub

        Private Sub Text_Footer_PrintOnPage(ByVal sender As Object, ByVal e As DevExpress.XtraReports.UI.PrintOnPageEventArgs)
            e.Cancel = (e.PageIndex <> 0)
        End Sub
    End Class
End Namespace
