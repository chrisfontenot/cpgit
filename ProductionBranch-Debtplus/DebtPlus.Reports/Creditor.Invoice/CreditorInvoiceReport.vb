#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}
#End Region

Imports DebtPlus.Interfaces.Creditor
Imports DebtPlus.Reports.Template
Imports System.IO
Imports DebtPlus.Utils

Namespace Creditor.Invoice
    Public Class CreditorInvoiceReport
        Inherits BaseXtraReportClass
        Implements ICreditor

        Public Sub New()
            MyBase.New()
            InitializeComponent()

            ' Set the script references as needed
            ScriptReferences = DebtPlus.Utils.AssemblyTools.GetAllSiblingAssemblies()

            ' Copy the script references to the sub-reports as well
            For Each Band As DevExpress.XtraReports.UI.Band In Me.Bands
                For Each ctl As DevExpress.XtraReports.UI.XRControl In Band.Controls
                    Dim rpt As DevExpress.XtraReports.UI.XRSubreport = TryCast(ctl, DevExpress.XtraReports.UI.XRSubreport)
                    If rpt IsNot Nothing Then
                        Dim SubRpt As DevExpress.XtraReports.UI.XtraReport = TryCast(rpt.ReportSource, DevExpress.XtraReports.UI.XtraReport)
                        If SubRpt IsNot Nothing Then
                            SubRpt.ScriptReferences = DebtPlus.Utils.AssemblyTools.GetAllSiblingAssemblies()
                        End If
                    End If
                Next
            Next

            ' Ensure that the creditor parameter is empty
            Parameter_Creditor = String.Empty
        End Sub

        Private Sub InitializeComponent()
            Const ReportName As String = "DebtPlus.Reports.Creditor.Invoice.repx"

            ' See if there is a report reference that we can use
            Dim Fname As String = System.IO.Path.Combine(DebtPlus.Configuration.Config.ReportsDirectory, ReportName)
            Try
                If System.IO.File.Exists(Fname) Then
                    LoadLayout(Fname)
                    Return
                End If
            Catch ex As Exception
            End Try

            ' Load the standard report definition if we need a new item
                Dim asm As Reflection.Assembly = Reflection.Assembly.GetExecutingAssembly
                Using ios As Stream = asm.GetManifestResourceStream(String.Format("DebtPlus.Reports.{0}", ReportName))
                    If ios IsNot Nothing Then
                        LoadLayout(ios)

                        ' Set the watermark to say "SAMPLE" in big blue letters.
                        ' This is not saved with the file if the customized item is loaded.
                        With Watermark
                            .Font = New System.Drawing.Font("Arial", 80.0!, System.Drawing.FontStyle.Bold)
                            .ForeColor = System.Drawing.Color.DeepSkyBlue
                            .ShowBehind = False
                            .Text = "SAMPLE"
                            .TextTransparency = 139
                        End With
                    End If
                End Using
        End Sub

        Public Property Parameter_Creditor() As String Implements ICreditor.Creditor
            Get
                Return CType(Parameters("ParameterCreditor").Value, String)
            End Get
            Set(ByVal Value As String)
                Parameters("ParameterCreditor").Value = Value
            End Set
        End Property

        Public Overrides Sub SetReportParameter(ByVal Parameter As String, ByVal Value As Object)
            Select Case Parameter
                Case "Creditor"
                    Parameter_Creditor = Convert.ToString(Value)
                Case Else
                    MyBase.SetReportParameter(Parameter, Value)
            End Select
        End Sub

        Public Overrides Function NeedParameters() As Boolean
            Return MyBase.NeedParameters() OrElse Parameter_Creditor = String.Empty
        End Function

        Public Overrides Function RequestReportParameters() As System.Windows.Forms.DialogResult
            Dim answer As System.Windows.Forms.DialogResult = DialogResult.OK
            If NeedParameters() Then
                Using frm As New DebtPlus.Reports.Template.Forms.CreditorParametersForm()
                    answer = frm.ShowDialog
                    Parameter_Creditor = frm.Parameter_Creditor
                End Using
            End If
            Return answer
        End Function
    End Class
End Namespace