#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}
#End Region

Option Compare Binary
Option Explicit On 
Option Strict On

Namespace Retention.MissingStatements
    Public Class MissingStatementsReport
        Inherits DebtPlus.Reports.Template.DatedTemplateXtraReportClass

        ''' <summary>
        ''' Create an instance of our report
        ''' </summary>
        Public Sub New()
            MyBase.New()
            InitializeComponent()

            AddHandler BeforePrint, AddressOf MissingStatementsReport_BeforePrint
            ReportFilter.IsEnabled = True
        End Sub


        ''' <summary>
        ''' Process the report title information
        ''' </summary>
        Public Overrides ReadOnly Property ReportTitle() As String
            Get
                Return "Missing Creditor Statements"
            End Get
        End Property


        ''' <summary>
        ''' Process the report subtitle information
        ''' </summary>
        Public Overrides ReadOnly Property ReportSubTitle() As String
            Get
                Return System.String.Format("This report covers missing statements since {0:d}", Parameter_FromDate)
            End Get
        End Property

#Region " Component Designer generated code "

        'Component overrides dispose to clean up the component list.
        Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
            If disposing Then
                If components IsNot Nothing Then
                    components.Dispose()
                End If
            End If
            MyBase.Dispose(disposing)
        End Sub

        'Required by the Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Designer
        'It can be modified using the Designer.
        'Do not modify it using the code editor.
        Friend WithEvents GroupHeader1 As DevExpress.XtraReports.UI.GroupHeaderBand
        Friend WithEvents XrLabel_Group_Counselor As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrPanel1 As DevExpress.XtraReports.UI.XRPanel
        Friend WithEvents XrLabel1 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel6 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel3 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel4 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel2 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel7 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel5 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrPanel2 As DevExpress.XtraReports.UI.XRPanel
        Friend WithEvents XrLabel_Client As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_Creditor As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_AccountNumber As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_Phone As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_Statement As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_Balance As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_Date As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_Verified As DevExpress.XtraReports.UI.XRLabel
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
            Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(MissingStatementsReport))
            Me.GroupHeader1 = New DevExpress.XtraReports.UI.GroupHeaderBand()
            Me.XrPanel1 = New DevExpress.XtraReports.UI.XRPanel()
            Me.XrLabel5 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel7 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel2 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel4 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel3 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel6 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel1 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_Group_Counselor = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrPanel2 = New DevExpress.XtraReports.UI.XRPanel()
            Me.XrLabel_Verified = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_Date = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_Balance = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_Statement = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_Phone = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_AccountNumber = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_Creditor = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_Client = New DevExpress.XtraReports.UI.XRLabel()
            CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
            '
            'PageHeader
            '
            Me.PageHeader.HeightF = 111.0!
            '
            'XrLabel_Title
            '
            Me.XrLabel_Title.StylePriority.UseFont = False
            '
            'XrPageInfo_PageNumber
            '
            Me.XrPageInfo_PageNumber.LocationFloat = New DevExpress.Utils.PointFloat(917.0!, 8.0!)
            Me.XrPageInfo_PageNumber.StylePriority.UseTextAlignment = False
            '
            'XrPanel_AgencyAddress
            '
            Me.XrPanel_AgencyAddress.LocationFloat = New DevExpress.Utils.PointFloat(742.0!, 0.0!)
            '
            'XRLabel_Agency_Name
            '
            Me.XRLabel_Agency_Name.StylePriority.UseTextAlignment = False
            '
            'XrLabel_Agency_Address3
            '
            Me.XrLabel_Agency_Address3.StylePriority.UseFont = False
            Me.XrLabel_Agency_Address3.StylePriority.UseTextAlignment = False
            '
            'XrLabel_Agency_Address1
            '
            Me.XrLabel_Agency_Address1.StylePriority.UseFont = False
            Me.XrLabel_Agency_Address1.StylePriority.UseTextAlignment = False
            '
            'XrLabel_Agency_Phone
            '
            Me.XrLabel_Agency_Phone.StylePriority.UseFont = False
            Me.XrLabel_Agency_Phone.StylePriority.UseTextAlignment = False
            '
            'XrLabel_Agency_Address2
            '
            Me.XrLabel_Agency_Address2.StylePriority.UseFont = False
            Me.XrLabel_Agency_Address2.StylePriority.UseTextAlignment = False
            '
            'Detail
            '
            Me.Detail.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrPanel2})
            Me.Detail.HeightF = 15.0!
            Me.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            '
            'GroupHeader1
            '
            Me.GroupHeader1.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrPanel1, Me.XrLabel_Group_Counselor})
            Me.GroupHeader1.GroupFields.AddRange(New DevExpress.XtraReports.UI.GroupField() {New DevExpress.XtraReports.UI.GroupField("counselor", DevExpress.XtraReports.UI.XRColumnSortOrder.Ascending)})
            Me.GroupHeader1.HeightF = 69.0!
            Me.GroupHeader1.Name = "GroupHeader1"
            Me.GroupHeader1.PageBreak = DevExpress.XtraReports.UI.PageBreak.BeforeBand
            Me.GroupHeader1.RepeatEveryPage = True
            '
            'XrPanel1
            '
            Me.XrPanel1.BackColor = System.Drawing.Color.Teal
            Me.XrPanel1.BorderColor = System.Drawing.Color.Teal
            Me.XrPanel1.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel5, Me.XrLabel7, Me.XrLabel2, Me.XrLabel4, Me.XrLabel3, Me.XrLabel6, Me.XrLabel1})
            Me.XrPanel1.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 44.0!)
            Me.XrPanel1.Name = "XrPanel1"
            Me.XrPanel1.SizeF = New System.Drawing.SizeF(1050.0!, 17.0!)
            '
            'XrLabel5
            '
            Me.XrLabel5.BorderColor = System.Drawing.Color.Transparent
            Me.XrLabel5.CanGrow = False
            Me.XrLabel5.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel5.ForeColor = System.Drawing.Color.White
            Me.XrLabel5.LocationFloat = New DevExpress.Utils.PointFloat(777.0!, 1.0!)
            Me.XrLabel5.Name = "XrLabel5"
            Me.XrLabel5.SizeF = New System.Drawing.SizeF(72.0!, 15.0!)
            Me.XrLabel5.Text = "BALANCE"
            Me.XrLabel5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel7
            '
            Me.XrLabel7.BorderColor = System.Drawing.Color.Transparent
            Me.XrLabel7.CanGrow = False
            Me.XrLabel7.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel7.ForeColor = System.Drawing.Color.White
            Me.XrLabel7.LocationFloat = New DevExpress.Utils.PointFloat(950.0!, 1.0!)
            Me.XrLabel7.Name = "XrLabel7"
            Me.XrLabel7.SizeF = New System.Drawing.SizeF(100.0!, 15.0!)
            Me.XrLabel7.Text = "HOME PHONE"
            Me.XrLabel7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel2
            '
            Me.XrLabel2.BorderColor = System.Drawing.Color.Transparent
            Me.XrLabel2.CanGrow = False
            Me.XrLabel2.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel2.ForeColor = System.Drawing.Color.White
            Me.XrLabel2.LocationFloat = New DevExpress.Utils.PointFloat(250.0!, 1.0!)
            Me.XrLabel2.Name = "XrLabel2"
            Me.XrLabel2.SizeF = New System.Drawing.SizeF(275.0!, 15.0!)
            Me.XrLabel2.Text = "CREDITOR"
            Me.XrLabel2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            '
            'XrLabel4
            '
            Me.XrLabel4.BorderColor = System.Drawing.Color.Transparent
            Me.XrLabel4.CanGrow = False
            Me.XrLabel4.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel4.ForeColor = System.Drawing.Color.White
            Me.XrLabel4.LocationFloat = New DevExpress.Utils.PointFloat(683.0!, 1.0!)
            Me.XrLabel4.Name = "XrLabel4"
            Me.XrLabel4.SizeF = New System.Drawing.SizeF(83.0!, 15.0!)
            Me.XrLabel4.Text = "LAST STMT"
            Me.XrLabel4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel3
            '
            Me.XrLabel3.BorderColor = System.Drawing.Color.Transparent
            Me.XrLabel3.CanGrow = False
            Me.XrLabel3.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel3.ForeColor = System.Drawing.Color.White
            Me.XrLabel3.LocationFloat = New DevExpress.Utils.PointFloat(533.0!, 1.0!)
            Me.XrLabel3.Name = "XrLabel3"
            Me.XrLabel3.SizeF = New System.Drawing.SizeF(141.0!, 15.0!)
            Me.XrLabel3.Text = "ACCOUNT"
            Me.XrLabel3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            '
            'XrLabel6
            '
            Me.XrLabel6.BorderColor = System.Drawing.Color.Transparent
            Me.XrLabel6.CanGrow = False
            Me.XrLabel6.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel6.ForeColor = System.Drawing.Color.White
            Me.XrLabel6.LocationFloat = New DevExpress.Utils.PointFloat(866.0!, 1.0!)
            Me.XrLabel6.Name = "XrLabel6"
            Me.XrLabel6.SizeF = New System.Drawing.SizeF(75.0!, 15.0!)
            Me.XrLabel6.Text = "LAST DISB"
            Me.XrLabel6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel1
            '
            Me.XrLabel1.BorderColor = System.Drawing.Color.Transparent
            Me.XrLabel1.CanGrow = False
            Me.XrLabel1.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel1.ForeColor = System.Drawing.Color.White
            Me.XrLabel1.LocationFloat = New DevExpress.Utils.PointFloat(8.0!, 1.0!)
            Me.XrLabel1.Name = "XrLabel1"
            Me.XrLabel1.SizeF = New System.Drawing.SizeF(233.0!, 15.0!)
            Me.XrLabel1.Text = "CLIENT"
            Me.XrLabel1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            '
            'XrLabel_Group_Counselor
            '
            Me.XrLabel_Group_Counselor.CanGrow = False
            Me.XrLabel_Group_Counselor.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "counselor_name", "Counselor: {0}")})
            Me.XrLabel_Group_Counselor.Font = New System.Drawing.Font("Times New Roman", 14.0!, System.Drawing.FontStyle.Bold)
            Me.XrLabel_Group_Counselor.ForeColor = System.Drawing.Color.Maroon
            Me.XrLabel_Group_Counselor.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 8.0!)
            Me.XrLabel_Group_Counselor.Name = "XrLabel_Group_Counselor"
            Me.XrLabel_Group_Counselor.SizeF = New System.Drawing.SizeF(1050.0!, 25.0!)
            Me.XrLabel_Group_Counselor.Text = "Counselor: {0}"
            Me.XrLabel_Group_Counselor.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            '
            'XrPanel2
            '
            Me.XrPanel2.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel_Verified, Me.XrLabel_Date, Me.XrLabel_Balance, Me.XrLabel_Statement, Me.XrLabel_Phone, Me.XrLabel_AccountNumber, Me.XrLabel_Creditor, Me.XrLabel_Client})
            Me.XrPanel2.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
            Me.XrPanel2.Name = "XrPanel2"
            Me.XrPanel2.SizeF = New System.Drawing.SizeF(1050.0!, 15.0!)
            '
            'XrLabel_Verified
            '
            Me.XrLabel_Verified.CanGrow = False
            Me.XrLabel_Verified.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "verified")})
            Me.XrLabel_Verified.LocationFloat = New DevExpress.Utils.PointFloat(849.0!, 0.0!)
            Me.XrLabel_Verified.Name = "XrLabel_Verified"
            Me.XrLabel_Verified.SizeF = New System.Drawing.SizeF(15.0!, 15.0!)
            Me.XrLabel_Verified.Text = "V"
            Me.XrLabel_Verified.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            Me.XrLabel_Verified.WordWrap = False
            '
            'XrLabel_Date
            '
            Me.XrLabel_Date.BorderColor = System.Drawing.Color.Transparent
            Me.XrLabel_Date.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "last_disursed", "{0:d}")})
            Me.XrLabel_Date.LocationFloat = New DevExpress.Utils.PointFloat(866.0!, 0.0!)
            Me.XrLabel_Date.Name = "XrLabel_Date"
            Me.XrLabel_Date.SizeF = New System.Drawing.SizeF(75.0!, 15.0!)
            Me.XrLabel_Date.Text = "CLIENT"
            Me.XrLabel_Date.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrLabel_Date.WordWrap = False
            '
            'XrLabel_Balance
            '
            Me.XrLabel_Balance.BorderColor = System.Drawing.Color.Transparent
            Me.XrLabel_Balance.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "balance", "{0:c2}")})
            Me.XrLabel_Balance.LocationFloat = New DevExpress.Utils.PointFloat(774.0!, 0.0!)
            Me.XrLabel_Balance.Name = "XrLabel_Balance"
            Me.XrLabel_Balance.SizeF = New System.Drawing.SizeF(75.0!, 15.0!)
            Me.XrLabel_Balance.Text = "CLIENT"
            Me.XrLabel_Balance.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrLabel_Balance.WordWrap = False
            '
            'XrLabel_Statement
            '
            Me.XrLabel_Statement.BorderColor = System.Drawing.Color.Transparent
            Me.XrLabel_Statement.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "last_stmt_date", "{0:d}")})
            Me.XrLabel_Statement.LocationFloat = New DevExpress.Utils.PointFloat(683.0!, 0.0!)
            Me.XrLabel_Statement.Name = "XrLabel_Statement"
            Me.XrLabel_Statement.SizeF = New System.Drawing.SizeF(83.0!, 15.0!)
            Me.XrLabel_Statement.Text = "CLIENT"
            Me.XrLabel_Statement.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrLabel_Statement.WordWrap = False
            '
            'XrLabel_Phone
            '
            Me.XrLabel_Phone.BorderColor = System.Drawing.Color.Transparent
            Me.XrLabel_Phone.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "home_ph")})
            Me.XrLabel_Phone.LocationFloat = New DevExpress.Utils.PointFloat(950.0!, 0.0!)
            Me.XrLabel_Phone.Name = "XrLabel_Phone"
            Me.XrLabel_Phone.SizeF = New System.Drawing.SizeF(100.0!, 15.0!)
            Me.XrLabel_Phone.Text = "CLIENT"
            Me.XrLabel_Phone.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrLabel_Phone.WordWrap = False
            '
            'XrLabel_AccountNumber
            '
            Me.XrLabel_AccountNumber.BorderColor = System.Drawing.Color.Transparent
            Me.XrLabel_AccountNumber.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "account_number")})
            Me.XrLabel_AccountNumber.LocationFloat = New DevExpress.Utils.PointFloat(533.0!, 0.0!)
            Me.XrLabel_AccountNumber.Name = "XrLabel_AccountNumber"
            Me.XrLabel_AccountNumber.SizeF = New System.Drawing.SizeF(141.0!, 15.0!)
            Me.XrLabel_AccountNumber.Text = "CLIENT"
            Me.XrLabel_AccountNumber.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            Me.XrLabel_AccountNumber.WordWrap = False
            '
            'XrLabel_Creditor
            '
            Me.XrLabel_Creditor.BorderColor = System.Drawing.Color.Transparent
            Me.XrLabel_Creditor.LocationFloat = New DevExpress.Utils.PointFloat(250.0!, 0.0!)
            Me.XrLabel_Creditor.Name = "XrLabel_Creditor"
            Me.XrLabel_Creditor.SizeF = New System.Drawing.SizeF(275.0!, 15.0!)
            Me.XrLabel_Creditor.Text = "[creditor] [creditor_name]"
            Me.XrLabel_Creditor.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            Me.XrLabel_Creditor.WordWrap = False
            '
            'XrLabel_Client
            '
            Me.XrLabel_Client.BorderColor = System.Drawing.Color.Transparent
            Me.XrLabel_Client.LocationFloat = New DevExpress.Utils.PointFloat(8.0!, 0.0!)
            Me.XrLabel_Client.Name = "XrLabel_Client"
            Me.XrLabel_Client.SizeF = New System.Drawing.SizeF(233.0!, 15.0!)
            Me.XrLabel_Client.Text = "[client!0000000] [client_name]"
            Me.XrLabel_Client.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            Me.XrLabel_Client.WordWrap = False
            '
            'MissingStatementsReport
            '
            Me.Bands.AddRange(New DevExpress.XtraReports.UI.Band() {Me.Detail, Me.PageHeader, Me.PageFooter, Me.TopMarginBand1, Me.BottomMarginBand1, Me.GroupHeader1})
            Me.Landscape = True
            Me.PageHeight = 850
            Me.PageWidth = 1100
            Me.ScriptLanguage = DevExpress.XtraReports.ScriptLanguage.VisualBasic
            Me.ScriptReferencesString = resources.GetString("$this.ScriptReferencesString")
            Me.Scripts.OnBeforePrint = "MissingStatementsReport_BeforePrint"
            Me.ScriptsSource = resources.GetString("$this.ScriptsSource")
            Me.StyleSheet.AddRange(New DevExpress.XtraReports.UI.XRControlStyle() {Me.XrControlStyle_Header, Me.XrControlStyle_Totals, Me.XrControlStyle_HeaderPannel, Me.XrControlStyle_GroupHeader})
            Me.Version = "11.2"
            Me.Controls.SetChildIndex(Me.GroupHeader1, 0)
            Me.Controls.SetChildIndex(Me.BottomMarginBand1, 0)
            Me.Controls.SetChildIndex(Me.TopMarginBand1, 0)
            Me.Controls.SetChildIndex(Me.PageFooter, 0)
            Me.Controls.SetChildIndex(Me.PageHeader, 0)
            Me.Controls.SetChildIndex(Me.Detail, 0)
            CType(Me, System.ComponentModel.ISupportInitialize).EndInit()

        End Sub

#End Region

        ''' <summary>
        ''' Request the parameters for the report
        ''' </summary>
        Public Overrides Function RequestReportParameters() As System.Windows.Forms.DialogResult
            Dim Answer As System.Windows.Forms.DialogResult

            Using frm As New ParametersForm()
                Answer = frm.ShowDialog
                Parameter_FromDate = frm.Parameter_FromDate
            End Using

            Return Answer
        End Function

        Dim ds As New System.Data.DataSet("ds")

        ''' <summary>
        ''' Generate the report information
        ''' </summary>
        Private Sub MissingStatementsReport_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs)
            Dim rpt As DevExpress.XtraReports.UI.XtraReport = CType(sender, DevExpress.XtraReports.UI.XtraReport)
            Const TableName As String = "rpt_Missing_Creditor_Statements"
            Dim tbl As System.Data.DataTable = ds.Tables(TableName)

            If tbl Is Nothing Then
                Dim sqlInfo As DebtPlus.LINQ.SQLInfoClass = DebtPlus.LINQ.SQLInfoClass.getDefault()
                Using cmd As New System.Data.SqlClient.SqlCommand()
                    With cmd
                        .Connection = New System.Data.SqlClient.SqlConnection(sqlInfo.ConnectionString)
                        .CommandText = "rpt_Missing_Creditor_Statements"
                        .CommandType = System.Data.CommandType.StoredProcedure
                        .Parameters.Add("@FromDate", System.Data.SqlDbType.DateTime).Value = rpt.Parameters("ParameterFromDate").Value
                        .CommandTimeout = 0
                    End With

                    Using da As New System.Data.SqlClient.SqlDataAdapter(cmd)
                        da.Fill(ds, TableName)
                        tbl = ds.Tables(TableName)
                    End Using
                End Using
            End If

            '-- Bind the report to the dataset
            If tbl IsNot Nothing Then
                rpt.DataSource = New System.Data.DataView(tbl, CType(rpt, DebtPlus.Interfaces.Reports.IReportFilter).ReportFilter.ViewFilter, String.Empty, System.Data.DataViewRowState.CurrentRows)
                For Each calc As DevExpress.XtraReports.UI.CalculatedField In rpt.CalculatedFields
                    calc.Assign(rpt.DataSource, rpt.DataMember)
                Next
            End If
        End Sub
    End Class
End Namespace
