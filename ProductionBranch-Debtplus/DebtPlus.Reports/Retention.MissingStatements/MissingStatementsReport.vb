#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}
#End Region

Option Compare Binary
Option Explicit On
Option Strict On

Imports System.IO
Imports System.Reflection
Imports DebtPlus.Utils

Imports DevExpress.XtraReports.UI

Namespace Retention.MissingStatements
    Public Class MissingStatementsReport
        Inherits Template.DatedTemplateXtraReportClass

        ''' <summary>
        '''     Create an instance of our report
        ''' </summary>
        Public Sub New()
            MyBase.New()

            Const ReportName As String = "DebtPlus.Reports.Retention.MissingStatements.repx"

            ' See if there is a report reference that we can use
            Dim Fname As String = System.IO.Path.Combine(DebtPlus.Configuration.Config.ReportsDirectory, ReportName)
            Dim UseDefault As Boolean = True
            Try
                If (Not DebtPlus.Configuration.Config.LocalReportsOnly) AndAlso System.IO.File.Exists(Fname) Then
                    LoadLayout(Fname)
                    UseDefault = False
                End If
            Catch ex As Exception
            End Try

            ' Load the standard report definition if we need a new item
            If UseDefault Then
                Dim asm As Assembly = Assembly.GetExecutingAssembly                                                    ' changed
                Using ios As Stream = asm.GetManifestResourceStream(String.Format("DebtPlus.Reports.{0}", ReportName)) ' changed
                    If ios IsNot Nothing Then LoadLayout(ios) ' changed
                End Using
            End If

            ' Set the script references as needed
            ScriptReferences = DebtPlus.Utils.AssemblyTools.GetAllSiblingAssemblies()

            ' Copy the script references to the subreports as well
            For Each Band As Band In Bands
                For Each ctl As XRControl In Band.Controls
                    Dim rpt As XRSubreport = TryCast(ctl, XRSubreport)
                    If rpt IsNot Nothing Then
                        Dim SubRpt As XtraReport = TryCast(rpt.ReportSource, XtraReport)
                        If SubRpt IsNot Nothing Then
                            SubRpt.ScriptReferences = ScriptReferences
                        End If
                    End If
                Next
            Next

            ' Enable the select expert
            ReportFilter.IsEnabled = True
        End Sub


        ''' <summary>
        '''     Request the parameters for the report
        ''' </summary>
        Public Overrides Function RequestReportParameters() As System.Windows.Forms.DialogResult
            Dim answer As System.Windows.Forms.DialogResult
            Using frm As New ParametersForm
                answer = frm.ShowDialog()
                If answer = DialogResult.OK Then Parameter_FromDate = frm.Parameter_FromDate
            End Using
            Return answer
        End Function


        ''' <summary>
        '''     Process the report title information
        ''' </summary>
        Public Overrides ReadOnly Property ReportTitle() As String
            Get
                Return "Missing Creditor Statements"
            End Get
        End Property


        ''' <summary>
        '''     Process the report subtitle information
        ''' </summary>
        Public Overrides ReadOnly Property ReportSubTitle() As String
            Get
                Return System.String.Format("This report covers missing statements since {0:d}", Parameter_FromDate)
            End Get
        End Property
    End Class
End Namespace
