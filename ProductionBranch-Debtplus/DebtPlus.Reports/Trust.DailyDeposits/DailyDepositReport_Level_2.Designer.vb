﻿Namespace Trust.DailyDeposits
    <Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
    Partial Class DailyDepositReport_Level_2

        'XtraReport overrides dispose to clean up the component list.
        <System.Diagnostics.DebuggerNonUserCode()> _
        Protected Overrides Sub Dispose(ByVal disposing As Boolean)
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
            MyBase.Dispose(disposing)
        End Sub

        'Required by the Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Designer
        'It can be modified using the Designer.  
        'Do not modify it using the code editor.
        <System.Diagnostics.DebuggerStepThrough()> _
        Private Sub InitializeComponent()
            Dim XrSummary1 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary
            Dim XrSummary2 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary
            Dim XrSummary3 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary
            Me.XrPanel1 = New DevExpress.XtraReports.UI.XRPanel
            Me.XrLabel10 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel9 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel1 = New DevExpress.XtraReports.UI.XRLabel
            Me.GroupHeader_Type = New DevExpress.XtraReports.UI.GroupHeaderBand
            Me.XrLabel12 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_type = New DevExpress.XtraReports.UI.XRLabel
            Me.GroupFooter_Type = New DevExpress.XtraReports.UI.GroupFooterBand
            Me.XrLabel8 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_total_amount = New DevExpress.XtraReports.UI.XRLabel
            Me.ReportFooter = New DevExpress.XtraReports.UI.ReportFooterBand
            Me.XrLabel6 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel5 = New DevExpress.XtraReports.UI.XRLabel
            Me.CalculatedField_Date = New DevExpress.XtraReports.UI.CalculatedField
            Me.XrLabel_amount = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_bank_name = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_date = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel2 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel3 = New DevExpress.XtraReports.UI.XRLabel
            Me.GroupHeader_Batch = New DevExpress.XtraReports.UI.GroupHeaderBand
            Me.XrLabel_batch = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel11 = New DevExpress.XtraReports.UI.XRLabel
            Me.GroupFooter_Batch = New DevExpress.XtraReports.UI.GroupFooterBand
            Me.XrLabel14 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel13 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_reference = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_name = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_client = New DevExpress.XtraReports.UI.XRLabel
            CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
            '
            'Detail
            '
            Me.Detail.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel_client, Me.XrLabel_name, Me.XrLabel_reference, Me.XrLabel_amount})
            Me.Detail.HeightF = 17.08333!
            '
            'PageHeader
            '
            Me.PageHeader.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel3, Me.XrLabel2, Me.XrLabel_date, Me.XrLabel_bank_name, Me.XrPanel1})
            Me.PageHeader.HeightF = 167.7916!
            Me.PageHeader.Controls.SetChildIndex(Me.XrPanel_AgencyAddress, 0)
            Me.PageHeader.Controls.SetChildIndex(Me.XrLabel_Title, 0)
            Me.PageHeader.Controls.SetChildIndex(Me.XrLine_Title, 0)
            Me.PageHeader.Controls.SetChildIndex(Me.XrLabel_Subtitle, 0)
            Me.PageHeader.Controls.SetChildIndex(Me.XrPageInfo1, 0)
            Me.PageHeader.Controls.SetChildIndex(Me.XrPanel1, 0)
            Me.PageHeader.Controls.SetChildIndex(Me.XrLabel_bank_name, 0)
            Me.PageHeader.Controls.SetChildIndex(Me.XrLabel_date, 0)
            Me.PageHeader.Controls.SetChildIndex(Me.XrLabel2, 0)
            Me.PageHeader.Controls.SetChildIndex(Me.XrLabel3, 0)
            '
            'XrLabel_Title
            '
            Me.XrLabel_Title.StylePriority.UseFont = False
            '
            'XrPageInfo_PageNumber
            '
            Me.XrPageInfo_PageNumber.StylePriority.UseTextAlignment = False
            '
            'XRLabel_Agency_Name
            '
            Me.XRLabel_Agency_Name.StylePriority.UseTextAlignment = False
            '
            'XrLabel_Agency_Address3
            '
            Me.XrLabel_Agency_Address3.StylePriority.UseFont = False
            Me.XrLabel_Agency_Address3.StylePriority.UseTextAlignment = False
            '
            'XrLabel_Agency_Address1
            '
            Me.XrLabel_Agency_Address1.StylePriority.UseFont = False
            Me.XrLabel_Agency_Address1.StylePriority.UseTextAlignment = False
            '
            'XrLabel_Agency_Phone
            '
            Me.XrLabel_Agency_Phone.StylePriority.UseFont = False
            Me.XrLabel_Agency_Phone.StylePriority.UseTextAlignment = False
            '
            'XrLabel_Agency_Address2
            '
            Me.XrLabel_Agency_Address2.StylePriority.UseFont = False
            Me.XrLabel_Agency_Address2.StylePriority.UseTextAlignment = False
            '
            'XrPanel1
            '
            Me.XrPanel1.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel10, Me.XrLabel9, Me.XrLabel1})
            Me.XrPanel1.LocationFloat = New DevExpress.Utils.PointFloat(0.00003178914!, 150.7916!)
            Me.XrPanel1.Name = "XrPanel1"
            Me.XrPanel1.SizeF = New System.Drawing.SizeF(700.0!, 17.0!)
            Me.XrPanel1.StyleName = "XrControlStyle_HeaderPannel"
            '
            'XrLabel10
            '
            Me.XrLabel10.LocationFloat = New DevExpress.Utils.PointFloat(516.6665!, 0.0!)
            Me.XrLabel10.Name = "XrLabel10"
            Me.XrLabel10.Padding = New DevExpress.XtraPrinting.PaddingInfo(10, 10, 0, 0, 100.0!)
            Me.XrLabel10.SizeF = New System.Drawing.SizeF(173.3335!, 15.0!)
            Me.XrLabel10.StylePriority.UsePadding = False
            Me.XrLabel10.StylePriority.UseTextAlignment = False
            Me.XrLabel10.Text = "REFERENCE"
            Me.XrLabel10.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
            '
            'XrLabel9
            '
            Me.XrLabel9.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
            Me.XrLabel9.Name = "XrLabel9"
            Me.XrLabel9.Padding = New DevExpress.XtraPrinting.PaddingInfo(10, 10, 0, 0, 100.0!)
            Me.XrLabel9.SizeF = New System.Drawing.SizeF(416.6665!, 15.0!)
            Me.XrLabel9.StylePriority.UsePadding = False
            Me.XrLabel9.StylePriority.UseTextAlignment = False
            Me.XrLabel9.Text = "CLIENT ID AND NAME"
            Me.XrLabel9.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            '
            'XrLabel1
            '
            Me.XrLabel1.LocationFloat = New DevExpress.Utils.PointFloat(424.6665!, 1.0!)
            Me.XrLabel1.Name = "XrLabel1"
            Me.XrLabel1.Padding = New DevExpress.XtraPrinting.PaddingInfo(10, 10, 0, 0, 100.0!)
            Me.XrLabel1.SizeF = New System.Drawing.SizeF(92.0!, 15.0!)
            Me.XrLabel1.StylePriority.UsePadding = False
            Me.XrLabel1.StylePriority.UseTextAlignment = False
            Me.XrLabel1.Text = "AMOUNT"
            Me.XrLabel1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
            '
            'GroupHeader_Type
            '
            Me.GroupHeader_Type.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel12, Me.XrLabel_type})
            Me.GroupHeader_Type.GroupFields.AddRange(New DevExpress.XtraReports.UI.GroupField() {New DevExpress.XtraReports.UI.GroupField("type", DevExpress.XtraReports.UI.XRColumnSortOrder.Ascending)})
            Me.GroupHeader_Type.HeightF = 36.24999!
            Me.GroupHeader_Type.Name = "GroupHeader_Type"
            Me.GroupHeader_Type.StyleName = "XrControlStyle_GroupHeader"
            '
            'XrLabel12
            '
            Me.XrLabel12.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 14.99999!)
            Me.XrLabel12.Name = "XrLabel12"
            Me.XrLabel12.Padding = New DevExpress.XtraPrinting.PaddingInfo(10, 10, 0, 0, 100.0!)
            Me.XrLabel12.SizeF = New System.Drawing.SizeF(182.2917!, 14.99999!)
            Me.XrLabel12.StylePriority.UsePadding = False
            Me.XrLabel12.Text = "* DEPOSIT TYPE:"
            '
            'XrLabel_type
            '
            Me.XrLabel_type.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "type")})
            Me.XrLabel_type.LocationFloat = New DevExpress.Utils.PointFloat(182.2916!, 14.99999!)
            Me.XrLabel_type.Name = "XrLabel_type"
            Me.XrLabel_type.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_type.SizeF = New System.Drawing.SizeF(287.8333!, 14.99999!)
            '
            'GroupFooter_Type
            '
            Me.GroupFooter_Type.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel8, Me.XrLabel_total_amount})
            Me.GroupFooter_Type.HeightF = 14.99999!
            Me.GroupFooter_Type.Name = "GroupFooter_Type"
            Me.GroupFooter_Type.StyleName = "XrControlStyle_Totals"
            '
            'XrLabel8
            '
            Me.XrLabel8.LocationFloat = New DevExpress.Utils.PointFloat(0.00003178914!, 0.0!)
            Me.XrLabel8.Name = "XrLabel8"
            Me.XrLabel8.Padding = New DevExpress.XtraPrinting.PaddingInfo(10, 10, 0, 0, 100.0!)
            Me.XrLabel8.SizeF = New System.Drawing.SizeF(257.665!, 14.99999!)
            Me.XrLabel8.StylePriority.UsePadding = False
            Me.XrLabel8.StylePriority.UseTextAlignment = False
            Me.XrLabel8.Text = "* DEPOSIT TYPE SUB-TOTAL:"
            Me.XrLabel8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            '
            'XrLabel_total_amount
            '
            Me.XrLabel_total_amount.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "amount")})
            Me.XrLabel_total_amount.LocationFloat = New DevExpress.Utils.PointFloat(316.6665!, 0.0!)
            Me.XrLabel_total_amount.Name = "XrLabel_total_amount"
            Me.XrLabel_total_amount.Padding = New DevExpress.XtraPrinting.PaddingInfo(10, 10, 0, 0, 100.0!)
            Me.XrLabel_total_amount.SizeF = New System.Drawing.SizeF(200.0!, 14.99999!)
            Me.XrLabel_total_amount.StylePriority.UsePadding = False
            Me.XrLabel_total_amount.StylePriority.UseTextAlignment = False
            XrSummary1.FormatString = "{0:c}"
            XrSummary1.Running = DevExpress.XtraReports.UI.SummaryRunning.Group
            Me.XrLabel_total_amount.Summary = XrSummary1
            Me.XrLabel_total_amount.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
            '
            'ReportFooter
            '
            Me.ReportFooter.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel6, Me.XrLabel5})
            Me.ReportFooter.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.ReportFooter.HeightF = 17.08334!
            Me.ReportFooter.Name = "ReportFooter"
            Me.ReportFooter.StyleName = "XrControlStyle_Totals"
            Me.ReportFooter.StylePriority.UseFont = False
            '
            'XrLabel6
            '
            Me.XrLabel6.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
            Me.XrLabel6.Name = "XrLabel6"
            Me.XrLabel6.Padding = New DevExpress.XtraPrinting.PaddingInfo(10, 10, 0, 0, 100.0!)
            Me.XrLabel6.SizeF = New System.Drawing.SizeF(257.665!, 17.08333!)
            Me.XrLabel6.StylePriority.UsePadding = False
            Me.XrLabel6.StylePriority.UseTextAlignment = False
            Me.XrLabel6.Text = "*** DAILY TOTAL:"
            Me.XrLabel6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            '
            'XrLabel5
            '
            Me.XrLabel5.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "amount")})
            Me.XrLabel5.LocationFloat = New DevExpress.Utils.PointFloat(316.6665!, 0.0!)
            Me.XrLabel5.Name = "XrLabel5"
            Me.XrLabel5.Padding = New DevExpress.XtraPrinting.PaddingInfo(10, 10, 0, 0, 100.0!)
            Me.XrLabel5.SizeF = New System.Drawing.SizeF(200.0!, 17.08334!)
            Me.XrLabel5.StylePriority.UsePadding = False
            Me.XrLabel5.StylePriority.UseTextAlignment = False
            XrSummary2.FormatString = "{0:c}"
            XrSummary2.Running = DevExpress.XtraReports.UI.SummaryRunning.Report
            Me.XrLabel5.Summary = XrSummary2
            Me.XrLabel5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
            '
            'CalculatedField_Date
            '
            Me.CalculatedField_Date.Expression = "GetDate([date])"
            Me.CalculatedField_Date.Name = "CalculatedField_Date"
            '
            'XrLabel_amount
            '
            Me.XrLabel_amount.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "amount", "{0:c}")})
            Me.XrLabel_amount.LocationFloat = New DevExpress.Utils.PointFloat(424.6665!, 0.0!)
            Me.XrLabel_amount.Name = "XrLabel_amount"
            Me.XrLabel_amount.Padding = New DevExpress.XtraPrinting.PaddingInfo(10, 10, 0, 0, 100.0!)
            Me.XrLabel_amount.SizeF = New System.Drawing.SizeF(92.00003!, 17.08333!)
            Me.XrLabel_amount.StylePriority.UsePadding = False
            Me.XrLabel_amount.StylePriority.UseTextAlignment = False
            Me.XrLabel_amount.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
            '
            'XrLabel_bank_name
            '
            Me.XrLabel_bank_name.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "bank_name")})
            Me.XrLabel_bank_name.LocationFloat = New DevExpress.Utils.PointFloat(108.0!, 102.0!)
            Me.XrLabel_bank_name.Name = "XrLabel_bank_name"
            Me.XrLabel_bank_name.Padding = New DevExpress.XtraPrinting.PaddingInfo(10, 10, 0, 0, 100.0!)
            Me.XrLabel_bank_name.SizeF = New System.Drawing.SizeF(343.7483!, 17.08333!)
            Me.XrLabel_bank_name.StyleName = "XrControlStyle_Header"
            Me.XrLabel_bank_name.StylePriority.UsePadding = False
            '
            'XrLabel_date
            '
            Me.XrLabel_date.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "date", "{0:d}")})
            Me.XrLabel_date.LocationFloat = New DevExpress.Utils.PointFloat(108.0!, 119.0833!)
            Me.XrLabel_date.Name = "XrLabel_date"
            Me.XrLabel_date.Padding = New DevExpress.XtraPrinting.PaddingInfo(10, 10, 0, 0, 100.0!)
            Me.XrLabel_date.SizeF = New System.Drawing.SizeF(149.6651!, 17.08333!)
            Me.XrLabel_date.StyleName = "XrControlStyle_Header"
            Me.XrLabel_date.StylePriority.UsePadding = False
            Me.XrLabel_date.StylePriority.UseTextAlignment = False
            Me.XrLabel_date.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
            '
            'XrLabel2
            '
            Me.XrLabel2.LocationFloat = New DevExpress.Utils.PointFloat(8.0!, 102.0!)
            Me.XrLabel2.Name = "XrLabel2"
            Me.XrLabel2.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel2.SizeF = New System.Drawing.SizeF(100.0!, 17.08333!)
            Me.XrLabel2.StyleName = "XrControlStyle_Header"
            Me.XrLabel2.Text = "Bank Account:"
            '
            'XrLabel3
            '
            Me.XrLabel3.LocationFloat = New DevExpress.Utils.PointFloat(8.0!, 119.0833!)
            Me.XrLabel3.Name = "XrLabel3"
            Me.XrLabel3.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel3.SizeF = New System.Drawing.SizeF(97.99998!, 17.08333!)
            Me.XrLabel3.StyleName = "XrControlStyle_Header"
            Me.XrLabel3.Text = "Deposit Date:"
            '
            'GroupHeader_Batch
            '
            Me.GroupHeader_Batch.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel_batch, Me.XrLabel11})
            Me.GroupHeader_Batch.GroupFields.AddRange(New DevExpress.XtraReports.UI.GroupField() {New DevExpress.XtraReports.UI.GroupField("batch_id", DevExpress.XtraReports.UI.XRColumnSortOrder.Ascending)})
            Me.GroupHeader_Batch.HeightF = 29.99999!
            Me.GroupHeader_Batch.Level = 1
            Me.GroupHeader_Batch.Name = "GroupHeader_Batch"
            Me.GroupHeader_Batch.StyleName = "XrControlStyle_GroupHeader"
            '
            'XrLabel_batch
            '
            Me.XrLabel_batch.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "batch_id")})
            Me.XrLabel_batch.LocationFloat = New DevExpress.Utils.PointFloat(182.2916!, 14.99999!)
            Me.XrLabel_batch.Name = "XrLabel_batch"
            Me.XrLabel_batch.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_batch.SizeF = New System.Drawing.SizeF(287.8333!, 14.99999!)
            '
            'XrLabel11
            '
            Me.XrLabel11.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 14.99999!)
            Me.XrLabel11.Name = "XrLabel11"
            Me.XrLabel11.Padding = New DevExpress.XtraPrinting.PaddingInfo(10, 10, 0, 0, 100.0!)
            Me.XrLabel11.SizeF = New System.Drawing.SizeF(182.2917!, 14.99999!)
            Me.XrLabel11.StylePriority.UsePadding = False
            Me.XrLabel11.Text = "** DEPOSIT BATCH ID:"
            '
            'GroupFooter_Batch
            '
            Me.GroupFooter_Batch.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel14, Me.XrLabel13})
            Me.GroupFooter_Batch.HeightF = 14.99999!
            Me.GroupFooter_Batch.Level = 1
            Me.GroupFooter_Batch.Name = "GroupFooter_Batch"
            Me.GroupFooter_Batch.StyleName = "XrControlStyle_Totals"
            '
            'XrLabel14
            '
            Me.XrLabel14.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "amount"), New DevExpress.XtraReports.UI.XRBinding("Tag", Nothing, "key")})
            Me.XrLabel14.LocationFloat = New DevExpress.Utils.PointFloat(316.6665!, 0.0!)
            Me.XrLabel14.Name = "XrLabel14"
            Me.XrLabel14.Padding = New DevExpress.XtraPrinting.PaddingInfo(10, 10, 0, 0, 100.0!)
            Me.XrLabel14.SizeF = New System.Drawing.SizeF(200.0!, 14.99999!)
            Me.XrLabel14.StylePriority.UsePadding = False
            Me.XrLabel14.StylePriority.UseTextAlignment = False
            XrSummary3.FormatString = "{0:c}"
            XrSummary3.Running = DevExpress.XtraReports.UI.SummaryRunning.Group
            Me.XrLabel14.Summary = XrSummary3
            Me.XrLabel14.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
            '
            'XrLabel13
            '
            Me.XrLabel13.LocationFloat = New DevExpress.Utils.PointFloat(0.00003178914!, 0.0!)
            Me.XrLabel13.Name = "XrLabel13"
            Me.XrLabel13.Padding = New DevExpress.XtraPrinting.PaddingInfo(10, 10, 0, 0, 100.0!)
            Me.XrLabel13.SizeF = New System.Drawing.SizeF(257.665!, 14.99999!)
            Me.XrLabel13.StylePriority.UsePadding = False
            Me.XrLabel13.StylePriority.UseTextAlignment = False
            Me.XrLabel13.Text = "** DEPOSIT BATCH ID SUB-TOTAL:"
            Me.XrLabel13.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            '
            'XrLabel_reference
            '
            Me.XrLabel_reference.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "reference")})
            Me.XrLabel_reference.LocationFloat = New DevExpress.Utils.PointFloat(516.6666!, 0.0!)
            Me.XrLabel_reference.Name = "XrLabel_reference"
            Me.XrLabel_reference.Padding = New DevExpress.XtraPrinting.PaddingInfo(10, 10, 0, 0, 100.0!)
            Me.XrLabel_reference.SizeF = New System.Drawing.SizeF(173.3334!, 17.08333!)
            Me.XrLabel_reference.StylePriority.UsePadding = False
            Me.XrLabel_reference.StylePriority.UseTextAlignment = False
            Me.XrLabel_reference.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
            '
            'XrLabel_name
            '
            Me.XrLabel_name.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "name")})
            Me.XrLabel_name.LocationFloat = New DevExpress.Utils.PointFloat(91.6667!, 0.0!)
            Me.XrLabel_name.Name = "XrLabel_name"
            Me.XrLabel_name.Padding = New DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 100.0!)
            Me.XrLabel_name.SizeF = New System.Drawing.SizeF(332.9999!, 17.08333!)
            Me.XrLabel_name.StylePriority.UsePadding = False
            '
            'XrLabel_client
            '
            Me.XrLabel_client.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "client", "{0:0000000}")})
            Me.XrLabel_client.LocationFloat = New DevExpress.Utils.PointFloat(0.00003178914!, 0.0!)
            Me.XrLabel_client.Name = "XrLabel_client"
            Me.XrLabel_client.Padding = New DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 100.0!)
            Me.XrLabel_client.SizeF = New System.Drawing.SizeF(91.66667!, 17.08333!)
            Me.XrLabel_client.StylePriority.UsePadding = False
            Me.XrLabel_client.StylePriority.UseTextAlignment = False
            Me.XrLabel_client.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
            '
            'Level_2
            '
            Me.Bands.AddRange(New DevExpress.XtraReports.UI.Band() {Me.Detail, Me.PageHeader, Me.PageFooter, Me.TopMarginBand1, Me.BottomMarginBand1, Me.GroupHeader_Type, Me.GroupFooter_Type, Me.ReportFooter, Me.GroupHeader_Batch, Me.GroupFooter_Batch})
            Me.CalculatedFields.AddRange(New DevExpress.XtraReports.UI.CalculatedField() {Me.CalculatedField_Date})
            Me.StyleSheet.AddRange(New DevExpress.XtraReports.UI.XRControlStyle() {Me.XrControlStyle_Header, Me.XrControlStyle_Totals, Me.XrControlStyle_HeaderPannel, Me.XrControlStyle_GroupHeader})
            Me.Version = "11.1"
            Me.Controls.SetChildIndex(Me.GroupFooter_Batch, 0)
            Me.Controls.SetChildIndex(Me.GroupHeader_Batch, 0)
            Me.Controls.SetChildIndex(Me.ReportFooter, 0)
            Me.Controls.SetChildIndex(Me.GroupFooter_Type, 0)
            Me.Controls.SetChildIndex(Me.GroupHeader_Type, 0)
            Me.Controls.SetChildIndex(Me.BottomMarginBand1, 0)
            Me.Controls.SetChildIndex(Me.TopMarginBand1, 0)
            Me.Controls.SetChildIndex(Me.PageFooter, 0)
            Me.Controls.SetChildIndex(Me.PageHeader, 0)
            Me.Controls.SetChildIndex(Me.Detail, 0)
            CType(Me, System.ComponentModel.ISupportInitialize).EndInit()

        End Sub
        Friend WithEvents XrPanel1 As DevExpress.XtraReports.UI.XRPanel
        Friend WithEvents XrLabel1 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents GroupHeader_Type As DevExpress.XtraReports.UI.GroupHeaderBand
        Friend WithEvents GroupFooter_Type As DevExpress.XtraReports.UI.GroupFooterBand
        Friend WithEvents XrLabel_total_amount As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents ReportFooter As DevExpress.XtraReports.UI.ReportFooterBand
        Friend WithEvents XrLabel6 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel5 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents CalculatedField_Date As DevExpress.XtraReports.UI.CalculatedField
        Friend WithEvents XrLabel_amount As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel3 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel2 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_date As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_bank_name As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel10 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel9 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents GroupHeader_Batch As DevExpress.XtraReports.UI.GroupHeaderBand
        Friend WithEvents GroupFooter_Batch As DevExpress.XtraReports.UI.GroupFooterBand
        Friend WithEvents XrLabel_batch As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel11 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel12 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_type As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_reference As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel8 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_client As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_name As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel14 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel13 As DevExpress.XtraReports.UI.XRLabel
    End Class
End Namespace