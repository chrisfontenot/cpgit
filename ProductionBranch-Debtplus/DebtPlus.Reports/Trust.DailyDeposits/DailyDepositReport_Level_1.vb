#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}
#End Region

Namespace Trust.DailyDeposits

    Public Class DailyDepositReport_Level_1
        Inherits DebtPlus.Reports.Template.DatedTemplateXtraReportClass

        Public Sub New()
            MyBase.New()
            InitializeComponent()

            AddHandler XrLabel_bank_name.PreviewClick, AddressOf Field_PreviewClick
            AddHandler XrLabel_date.PreviewClick, AddressOf Field_PreviewClick
            AddHandler XrLabel_total_amount.PreviewClick, AddressOf Field_PreviewClick

            ' Ensure that the parameters are not displayed
            For Each p As DevExpress.XtraReports.Parameters.Parameter In Parameters
                p.Visible = False
            Next
            Me.RequestParameters = False

        End Sub

        ' Supply the subtitle to the report
        Private privateReportSubtitle As String = String.Empty
        Public Overrides ReadOnly Property ReportSubTitle() As String
            Get
                Return privateReportSubtitle
            End Get
        End Property

        ' Set the title
        Public Sub SetReportTitle(ByVal TitleString As String)
            privateReportTitle = TitleString
        End Sub

        ' Supply the subtitle to the report
        Private privateReportTitle As String = String.Empty
        Public Overrides ReadOnly Property ReportTitle() As String
            Get
                Return privateReportTitle
            End Get
        End Property

        ' Set the subtitle
        Public Sub SetReportSubTitle(ByVal TitleString As String)
            privateReportSubtitle = TitleString
        End Sub

        Private Sub Field_PreviewClick(ByVal sender As Object, ByVal e As DevExpress.XtraReports.UI.PreviewMouseEventArgs)
            With New ThreadClass
                Dim key As Int32 = Convert.ToInt32(e.Brick.Value)
                Dim tbl As System.Data.DataTable = CType(Me.DataSource, System.Data.DataView).Table
                Dim row As System.Data.DataRow = tbl.Rows.Find(key)
                If row IsNot Nothing Then
                    .MasterRpt = CType(CType(sender, DevExpress.XtraReports.UI.XRControl).Report, DevExpress.XtraReports.UI.XtraReport)
                    .bank = Convert.ToInt32(row("bank"))
                    .DepositDate = Convert.ToDateTime(row("date"))

                    .ReportSubTitle = ReportSubTitle
                    .ReportTitle = ReportTitle

                    With New System.Threading.Thread(AddressOf .ThreadProc)
                        .SetApartmentState(Threading.ApartmentState.STA)
                        .IsBackground = True
                        .Start()
                    End With
                End If
            End With
        End Sub

        Private Class ThreadClass
            Public MasterRpt As DevExpress.XtraReports.UI.XtraReport
            Public vue As System.Data.DataView
            Public bank As Int32
            Public DepositDate As DateTime
            Public ReportTitle As String
            Public ReportSubTitle As String

            Public Sub ThreadProc()
                vue = CType(MasterRpt.DataSource, System.Data.DataView)

                Using NewRpt As New DailyDepositReport_Level_2()
                    With NewRpt
                        .SetReportTitle(ReportTitle)
                        .SetReportSubTitle(ReportSubTitle)

                        AddHandler .BeforePrint, AddressOf Report_BeforePrint

                        .CreateDocument()
                        .DisplayPreviewDialog()
                    End With
                End Using
            End Sub

            Private Sub Report_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs)
                Dim rpt As DevExpress.XtraReports.UI.XtraReport = CType(sender, DevExpress.XtraReports.UI.XtraReport)
                If vue IsNot Nothing Then
                    Dim SelectionClause As String = vue.RowFilter
                    If SelectionClause <> String.Empty Then
                        SelectionClause = String.Format("({0}) AND ", SelectionClause)
                    End If
                    SelectionClause += String.Format("([date]>='{0:d}' AND [date]<'{1:d}')", DepositDate.Date, DepositDate.AddDays(1).Date)
                    rpt.DataSource = New System.Data.DataView(vue.Table, SelectionClause, "batch_id, type, client", System.Data.DataViewRowState.CurrentRows)

                    ' Set the calculated fields
                    For Each fld As DevExpress.XtraReports.UI.CalculatedField In rpt.CalculatedFields
                        fld.Assign(rpt.DataSource, rpt.DataMember)
                    Next
                End If
            End Sub
        End Class
    End Class

End Namespace