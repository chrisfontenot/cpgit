#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}
#End Region

Namespace Trust.DailyDeposits

    Public Class DailyDepositReport_Level_2
        Inherits DebtPlus.Reports.Template.DatedTemplateXtraReportClass

        Public Sub New()
            MyBase.New()
            InitializeComponent()

            ' Ensure that the parameters are not displayed
            For Each p As DevExpress.XtraReports.Parameters.Parameter In Parameters
                p.Visible = False
            Next
            Me.RequestParameters = False

        End Sub

        ' Supply the subtitle to the report
        Private privateReportSubtitle As String = String.Empty
        Public Overrides ReadOnly Property ReportSubTitle() As String
            Get
                Return privateReportSubtitle
            End Get
        End Property

        ' Set the title
        Public Sub SetReportTitle(ByVal TitleString As String)
            privateReportTitle = TitleString
        End Sub

        ' Supply the subtitle to the report
        Private privateReportTitle As String = String.Empty
        Public Overrides ReadOnly Property ReportTitle() As String
            Get
                Return privateReportTitle
            End Get
        End Property

        ' Set the subtitle
        Public Sub SetReportSubTitle(ByVal TitleString As String)
            privateReportSubtitle = TitleString
        End Sub
    End Class

End Namespace