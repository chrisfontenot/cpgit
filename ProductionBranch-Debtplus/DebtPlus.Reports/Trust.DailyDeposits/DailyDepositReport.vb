#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}
#End Region

Option Compare Binary
Option Explicit On 
Option Strict On



Namespace Trust.DailyDeposits
    Public Class DailyDepositReport
        Inherits DebtPlus.Reports.Template.DatedTemplateXtraReportClass

        Public Sub New()
            MyBase.New()
            Parameters.Clear()
            InitializeComponent()
            AddHandler BeforePrint, AddressOf Report_BeforePrint
            AddHandler XrLabel_bank_name.PreviewClick, AddressOf Field_PreviewClick
            AddHandler XrLabel_total_amount.PreviewClick, AddressOf Field_PreviewClick

            ' Enable the select expert
            ReportFilter.IsEnabled = True
        End Sub

        Public Overrides ReadOnly Property ReportTitle() As String
            Get
                Return "Daily Deposits Listing"
            End Get
        End Property

        Public Property Parameter_Counselor() As Object
            Get
                Return Parameters("ParameterCounselor").Value
            End Get
            Set(ByVal value As Object)
                SetParameter("ParameterCounselor", GetType(Int32), value, "Counselor", True)
            End Set
        End Property

        Public Overrides Sub SetReportParameter(ByVal Parameter As String, ByVal Value As Object)
            If Parameter = "Counselor" Then
                Parameter_Counselor = Value
            Else
                MyBase.SetReportParameter(Parameter, Value)
            End If
        End Sub

#Region " Component Designer generated code "

        'Component overrides dispose to clean up the component list.
        Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
            If disposing Then
                If components IsNot Nothing Then
                    components.Dispose()
                End If
            End If
            MyBase.Dispose(disposing)
        End Sub
        Friend WithEvents XrPanel1 As DevExpress.XtraReports.UI.XRPanel
        Friend WithEvents XrLabel3 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel1 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents GroupHeader1 As DevExpress.XtraReports.UI.GroupHeaderBand
        Friend WithEvents GroupFooter1 As DevExpress.XtraReports.UI.GroupFooterBand
        Friend WithEvents XrLabel_bank_name As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_total_amount As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents ReportFooter As DevExpress.XtraReports.UI.ReportFooterBand
        Friend WithEvents XrLabel4 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel2 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents ParameterCounselor As DevExpress.XtraReports.Parameters.Parameter

        'Required by the Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Designer
        'It can be modified using the Designer.
        'Do not modify it using the code editor.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
            Dim XrSummary1 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary
            Dim XrSummary2 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary
            Me.XrPanel1 = New DevExpress.XtraReports.UI.XRPanel
            Me.XrLabel3 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel1 = New DevExpress.XtraReports.UI.XRLabel
            Me.GroupHeader1 = New DevExpress.XtraReports.UI.GroupHeaderBand
            Me.GroupFooter1 = New DevExpress.XtraReports.UI.GroupFooterBand
            Me.XrLabel_bank_name = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_total_amount = New DevExpress.XtraReports.UI.XRLabel
            Me.ReportFooter = New DevExpress.XtraReports.UI.ReportFooterBand
            Me.XrLabel4 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel2 = New DevExpress.XtraReports.UI.XRLabel
            Me.ParameterCounselor = New DevExpress.XtraReports.Parameters.Parameter
            CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
            '
            'Detail
            '
            Me.Detail.HeightF = 0.0!
            '
            'PageHeader
            '
            Me.PageHeader.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrPanel1})
            Me.PageHeader.HeightF = 137.8333!
            Me.PageHeader.Controls.SetChildIndex(Me.XrPanel_AgencyAddress, 0)
            Me.PageHeader.Controls.SetChildIndex(Me.XrLabel_Title, 0)
            Me.PageHeader.Controls.SetChildIndex(Me.XrLine_Title, 0)
            Me.PageHeader.Controls.SetChildIndex(Me.XrLabel_Subtitle, 0)
            Me.PageHeader.Controls.SetChildIndex(Me.XrPageInfo1, 0)
            Me.PageHeader.Controls.SetChildIndex(Me.XrPanel1, 0)
            '
            'XrLabel_Title
            '
            Me.XrLabel_Title.StylePriority.UseFont = False
            '
            'XrPageInfo_PageNumber
            '
            Me.XrPageInfo_PageNumber.StylePriority.UseTextAlignment = False
            '
            'XRLabel_Agency_Name
            '
            Me.XRLabel_Agency_Name.StylePriority.UseTextAlignment = False
            '
            'XrLabel_Agency_Address3
            '
            Me.XrLabel_Agency_Address3.StylePriority.UseFont = False
            Me.XrLabel_Agency_Address3.StylePriority.UseTextAlignment = False
            '
            'XrLabel_Agency_Address1
            '
            Me.XrLabel_Agency_Address1.StylePriority.UseFont = False
            Me.XrLabel_Agency_Address1.StylePriority.UseTextAlignment = False
            '
            'XrLabel_Agency_Phone
            '
            Me.XrLabel_Agency_Phone.StylePriority.UseFont = False
            Me.XrLabel_Agency_Phone.StylePriority.UseTextAlignment = False
            '
            'XrLabel_Agency_Address2
            '
            Me.XrLabel_Agency_Address2.StylePriority.UseFont = False
            Me.XrLabel_Agency_Address2.StylePriority.UseTextAlignment = False
            '
            'XrPanel1
            '
            Me.XrPanel1.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel3, Me.XrLabel1})
            Me.XrPanel1.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 120.8333!)
            Me.XrPanel1.Name = "XrPanel1"
            Me.XrPanel1.SizeF = New System.Drawing.SizeF(700.0!, 17.0!)
            Me.XrPanel1.StyleName = "XrControlStyle_HeaderPannel"
            '
            'XrLabel3
            '
            Me.XrLabel3.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.000007629395!)
            Me.XrLabel3.Name = "XrLabel3"
            Me.XrLabel3.Padding = New DevExpress.XtraPrinting.PaddingInfo(10, 10, 0, 0, 100.0!)
            Me.XrLabel3.SizeF = New System.Drawing.SizeF(176.0417!, 14.99999!)
            Me.XrLabel3.StylePriority.UsePadding = False
            Me.XrLabel3.StylePriority.UseTextAlignment = False
            Me.XrLabel3.Text = "BANK ACCOUNT NAME"
            Me.XrLabel3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            '
            'XrLabel1
            '
            Me.XrLabel1.LocationFloat = New DevExpress.Utils.PointFloat(500.0!, 1.0!)
            Me.XrLabel1.Name = "XrLabel1"
            Me.XrLabel1.Padding = New DevExpress.XtraPrinting.PaddingInfo(10, 10, 0, 0, 100.0!)
            Me.XrLabel1.SizeF = New System.Drawing.SizeF(200.0!, 15.0!)
            Me.XrLabel1.StylePriority.UsePadding = False
            Me.XrLabel1.StylePriority.UseTextAlignment = False
            Me.XrLabel1.Text = "AMOUNT"
            Me.XrLabel1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
            '
            'GroupHeader1
            '
            Me.GroupHeader1.GroupFields.AddRange(New DevExpress.XtraReports.UI.GroupField() {New DevExpress.XtraReports.UI.GroupField("bank", DevExpress.XtraReports.UI.XRColumnSortOrder.Ascending)})
            Me.GroupHeader1.HeightF = 0.0!
            Me.GroupHeader1.Name = "GroupHeader1"
            '
            'GroupFooter1
            '
            Me.GroupFooter1.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel_bank_name, Me.XrLabel_total_amount})
            Me.GroupFooter1.HeightF = 17.08334!
            Me.GroupFooter1.Name = "GroupFooter1"
            '
            'XrLabel_bank_name
            '
            Me.XrLabel_bank_name.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "bank_name"), New DevExpress.XtraReports.UI.XRBinding("Tag", Nothing, "bank")})
            Me.XrLabel_bank_name.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
            Me.XrLabel_bank_name.Name = "XrLabel_bank_name"
            Me.XrLabel_bank_name.Padding = New DevExpress.XtraPrinting.PaddingInfo(10, 10, 0, 0, 100.0!)
            Me.XrLabel_bank_name.SizeF = New System.Drawing.SizeF(487.5!, 17.08334!)
            Me.XrLabel_bank_name.StylePriority.UsePadding = False
            Me.XrLabel_bank_name.StylePriority.UseTextAlignment = False
            Me.XrLabel_bank_name.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
            '
            'XrLabel_total_amount
            '
            Me.XrLabel_total_amount.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "amount"), New DevExpress.XtraReports.UI.XRBinding("Tag", Nothing, "bank")})
            Me.XrLabel_total_amount.LocationFloat = New DevExpress.Utils.PointFloat(500.0!, 0.0!)
            Me.XrLabel_total_amount.Name = "XrLabel_total_amount"
            Me.XrLabel_total_amount.Padding = New DevExpress.XtraPrinting.PaddingInfo(10, 10, 0, 0, 100.0!)
            Me.XrLabel_total_amount.SizeF = New System.Drawing.SizeF(200.0!, 17.08334!)
            Me.XrLabel_total_amount.StylePriority.UsePadding = False
            Me.XrLabel_total_amount.StylePriority.UseTextAlignment = False
            XrSummary1.FormatString = "{0:c}"
            XrSummary1.Running = DevExpress.XtraReports.UI.SummaryRunning.Group
            Me.XrLabel_total_amount.Summary = XrSummary1
            Me.XrLabel_total_amount.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
            '
            'ReportFooter
            '
            Me.ReportFooter.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel4, Me.XrLabel2})
            Me.ReportFooter.HeightF = 17.08334!
            Me.ReportFooter.Name = "ReportFooter"
            '
            'XrLabel4
            '
            Me.XrLabel4.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
            Me.XrLabel4.Name = "XrLabel4"
            Me.XrLabel4.Padding = New DevExpress.XtraPrinting.PaddingInfo(10, 10, 0, 0, 100.0!)
            Me.XrLabel4.SizeF = New System.Drawing.SizeF(200.0!, 17.08334!)
            Me.XrLabel4.StylePriority.UsePadding = False
            Me.XrLabel4.Text = "TOTALS"
            '
            'XrLabel2
            '
            Me.XrLabel2.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "amount")})
            Me.XrLabel2.LocationFloat = New DevExpress.Utils.PointFloat(500.0!, 0.0!)
            Me.XrLabel2.Name = "XrLabel2"
            Me.XrLabel2.Padding = New DevExpress.XtraPrinting.PaddingInfo(10, 10, 0, 0, 100.0!)
            Me.XrLabel2.SizeF = New System.Drawing.SizeF(200.0!, 17.08334!)
            Me.XrLabel2.StylePriority.UsePadding = False
            Me.XrLabel2.StylePriority.UseTextAlignment = False
            XrSummary2.FormatString = "{0:c}"
            XrSummary2.Running = DevExpress.XtraReports.UI.SummaryRunning.Report
            Me.XrLabel2.Summary = XrSummary2
            Me.XrLabel2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
            '
            'ParameterCounselor
            '
            Me.ParameterCounselor.Name = "ParameterCounselor"
            Me.ParameterCounselor.Type = GetType(Int32)
            Me.ParameterCounselor.Value = -1
            Me.ParameterCounselor.Visible = False
            '
            'DailyDepositReport
            '
            Me.Bands.AddRange(New DevExpress.XtraReports.UI.Band() {Me.Detail, Me.PageHeader, Me.PageFooter, Me.TopMarginBand1, Me.BottomMarginBand1, Me.GroupHeader1, Me.GroupFooter1, Me.ReportFooter})
            Me.StyleSheet.AddRange(New DevExpress.XtraReports.UI.XRControlStyle() {Me.XrControlStyle_Header, Me.XrControlStyle_Totals, Me.XrControlStyle_HeaderPannel, Me.XrControlStyle_GroupHeader})
            Me.Version = "11.1"
            Me.Controls.SetChildIndex(Me.ReportFooter, 0)
            Me.Controls.SetChildIndex(Me.GroupFooter1, 0)
            Me.Controls.SetChildIndex(Me.GroupHeader1, 0)
            Me.Controls.SetChildIndex(Me.BottomMarginBand1, 0)
            Me.Controls.SetChildIndex(Me.TopMarginBand1, 0)
            Me.Controls.SetChildIndex(Me.PageFooter, 0)
            Me.Controls.SetChildIndex(Me.PageHeader, 0)
            Me.Controls.SetChildIndex(Me.Detail, 0)
            CType(Me, System.ComponentModel.ISupportInitialize).EndInit()

        End Sub
#End Region

        Public ReadOnly ds As New DataSet("ds")

        Public Overrides Function RequestReportParameters() As DialogResult
            Dim answer As DialogResult = DialogResult.OK
            If NeedParameters() Then
                Using frm As New DebtPlus.Reports.Template.Forms.DatedCounselorParametersForm(True)
                    answer = frm.ShowDialog()
                    Parameter_Counselor = frm.Parameter_Counselor
                    Parameter_FromDate = frm.Parameter_FromDate
                    Parameter_ToDate = frm.Parameter_ToDate
                End Using
            End If
            Return answer
        End Function

        Private Sub Report_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs)
            Const TableName As String = "rpt_DailyDeposits"
            Dim rpt As DevExpress.XtraReports.UI.XtraReport = CType(sender, DevExpress.XtraReports.UI.XtraReport)
            Dim MasterRpt As DevExpress.XtraReports.UI.XtraReport = rpt

            Dim tbl As DataTable = ds.Tables(TableName)
            If tbl Is Nothing Then
                Dim cn As SqlClient.SqlConnection = New System.Data.SqlClient.SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
                Try
                    cn.Open()
                    Using cmd As SqlClient.SqlCommand = DirectCast(New System.Data.SqlClient.SqlCommand(), System.Data.SqlClient.SqlCommand)
                        With cmd
                            .CommandText = "rpt_DailyDeposits"
                            .CommandType = CommandType.StoredProcedure
                            .Connection = cn

                            System.Data.SqlClient.SqlCommandBuilder.DeriveParameters(cmd)

                            ' The first two are a "given"....
                            .Parameters(1).Value = MasterRpt.Parameters("ParameterFromDate").Value
                            .Parameters(2).Value = MasterRpt.Parameters("ParameterToDate").Value

                            ' Include the counselor if there is a slot for it.
                            If (.Parameters.Contains("@counselor") OrElse .Parameters.Contains("@Counselor")) Then
                                .Parameters("@counselor").Value = MasterRpt.Parameters("ParameterCounselor").Value
                            End If

                            .CommandTimeout = 0
                        End With

                        Using da As New System.Data.SqlClient.SqlDataAdapter(cmd)
                            da.Fill(ds, TableName)
                            tbl = ds.Tables(TableName)

                            ' Generate a key id for each row so that we may find the item chosen
                            tbl.Columns.Add("key", GetType(Int32))
                            For RowID As Int32 = tbl.Rows.Count - 1 To 0 Step -1
                                tbl.Rows(RowID)("key") = RowID
                            Next

                            ' Give the table the primary key
                            tbl.PrimaryKey = New DataColumn() {tbl.Columns("key")}
                        End Using
                    End Using

                Catch ex As System.Data.SqlClient.SqlException
                    DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex)

                Finally
                    If cn IsNot Nothing Then cn.Dispose()
                End Try
            End If

            If tbl IsNot Nothing Then
                rpt.DataSource = New DataView(tbl, ReportFilter.ViewFilter, "bank", DataViewRowState.CurrentRows)

                ' Set the calculated fields
                For Each fld As DevExpress.XtraReports.UI.CalculatedField In rpt.CalculatedFields
                    fld.Assign(rpt.DataSource, rpt.DataMember)
                Next
            End If
        End Sub

        Private Sub Field_PreviewClick(ByVal sender As Object, ByVal e As DevExpress.XtraReports.UI.PreviewMouseEventArgs)
            With New ThreadClass
                .MasterRpt = CType(CType(sender, DevExpress.XtraReports.UI.XRControl).Report, DevExpress.XtraReports.UI.XtraReport)
                .bank = Convert.ToInt32(e.Brick.Value)
                .ReportSubTitle = ReportSubtitle
                .ReportTitle = ReportTitle

                With New System.Threading.Thread(AddressOf .ThreadProc)
                    .SetApartmentState(Threading.ApartmentState.STA)
                    .IsBackground = True
                    .Start()
                End With
            End With
        End Sub

        Private Class ThreadClass
            Public MasterRpt As DevExpress.XtraReports.UI.XtraReport
            Public vue As DataView
            Public bank As Int32
            Public ReportTitle As String
            Public ReportSubTitle As String

            Public Sub ThreadProc()
                vue = CType(MasterRpt.DataSource, DataView)

                Using NewRpt As New DailyDepositReport_Level_1()
                    With NewRpt
                        .SetReportTitle(ReportTitle)
                        .SetReportSubTitle(ReportSubTitle)

                        AddHandler .BeforePrint, AddressOf Report_BeforePrint

                        .CreateDocument()
                        .DisplayPreviewDialog()
                    End With
                End Using
            End Sub

            Private Sub Report_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs)
                Dim rpt As DevExpress.XtraReports.UI.XtraReport = CType(sender, DevExpress.XtraReports.UI.XtraReport)

                If vue IsNot Nothing Then
                    Dim SelectionClause As String = vue.RowFilter
                    If SelectionClause <> String.Empty Then
                        SelectionClause = String.Format("({0}) AND ", SelectionClause)
                    End If
                    SelectionClause += String.Format("([bank]={0:f0})", bank)
                    rpt.DataSource = New DataView(vue.Table, SelectionClause, "date", DataViewRowState.CurrentRows)

                    ' Set the calculated fields
                    For Each fld As DevExpress.XtraReports.UI.CalculatedField In rpt.CalculatedFields
                        fld.Assign(rpt.DataSource, rpt.DataMember)
                    Next
                End If
            End Sub
        End Class
    End Class

End Namespace
