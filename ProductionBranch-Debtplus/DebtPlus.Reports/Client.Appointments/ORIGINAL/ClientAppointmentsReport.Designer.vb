﻿Namespace Client.Appointments
    <Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
    Partial Class ClientAppointmentsReport
        Inherits DebtPlus.Reports.Template.TemplateXtraReportClass

        'XtraReport overrides dispose to clean up the component list.
        <System.Diagnostics.DebuggerNonUserCode()> _
        Protected Overrides Sub Dispose(ByVal disposing As Boolean)
            Try
                If disposing Then
                    If components IsNot Nothing Then components.Dispose()
                End If
                components = Nothing
            Finally
                MyBase.Dispose(disposing)
            End Try
        End Sub

        'Required by the Designer
        Private components As System.ComponentModel.IContainer = Nothing

        'NOTE: The following procedure is required by the Designer
        'It can be modified using the Designer.
        'Do not modify it using the code editor.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
            Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(ClientAppointmentsReport))
            Me.ParameterClient = New DevExpress.XtraReports.Parameters.Parameter()
            Me.ParameterPendingOnly = New DevExpress.XtraReports.Parameters.Parameter()
            Me.XrLabel_appt_confirmed_status = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_appt_time = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_appt_type = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_counselor = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_office = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_status = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabelPartnerCode = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabelReferral = New DevExpress.XtraReports.UI.XRLabel()
            Me.GroupHeader1 = New DevExpress.XtraReports.UI.GroupHeaderBand()
            Me.GroupHeader2 = New DevExpress.XtraReports.UI.GroupHeaderBand()
            Me.XrPanel1 = New DevExpress.XtraReports.UI.XRPanel()
            Me.XrLabelRef = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel7 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel5 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel4 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel3 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_hdr_appt_time = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel8 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel1 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrSubreport_ClientNameAndAddress = New DevExpress.XtraReports.UI.XRSubreport()
            Me.ClientNameAndAddressReport = New DebtPlus.Reports.Client.Appointments.ClientNameAndAddress()
            CType(Me.ClientNameAndAddressReport, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
            '
            'PageHeader
            '
            Me.PageHeader.HeightF = 110.0!
            '
            'XrLabel_Title
            '
            Me.XrLabel_Title.StylePriority.UseFont = False
            '
            'XrLabel_Subtitle
            '
            Me.XrLabel_Subtitle.LocationFloat = New DevExpress.Utils.PointFloat(7.999992!, 85.00001!)
            Me.XrLabel_Subtitle.SizeF = New System.Drawing.SizeF(1055.0!, 17.0!)
            '
            'XrPageInfo_PageNumber
            '
            Me.XrPageInfo_PageNumber.LocationFloat = New DevExpress.Utils.PointFloat(930.0001!, 8.0!)
            Me.XrPageInfo_PageNumber.StylePriority.UseTextAlignment = False
            '
            'XrPanel_AgencyAddress
            '
            Me.XrPanel_AgencyAddress.LocationFloat = New DevExpress.Utils.PointFloat(755.0!, 0.0!)
            '
            'XRLabel_Agency_Name
            '
            Me.XRLabel_Agency_Name.StylePriority.UseTextAlignment = False
            '
            'XrLabel_Agency_Address3
            '
            Me.XrLabel_Agency_Address3.StylePriority.UseFont = False
            Me.XrLabel_Agency_Address3.StylePriority.UseTextAlignment = False
            '
            'XrLabel_Agency_Address1
            '
            Me.XrLabel_Agency_Address1.StylePriority.UseFont = False
            Me.XrLabel_Agency_Address1.StylePriority.UseTextAlignment = False
            '
            'XrLabel_Agency_Phone
            '
            Me.XrLabel_Agency_Phone.StylePriority.UseFont = False
            Me.XrLabel_Agency_Phone.StylePriority.UseTextAlignment = False
            '
            'XrLabel_Agency_Address2
            '
            Me.XrLabel_Agency_Address2.StylePriority.UseFont = False
            Me.XrLabel_Agency_Address2.StylePriority.UseTextAlignment = False
            '
            'Detail
            '
            Me.Detail.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabelReferral, Me.XrLabelPartnerCode, Me.XrLabel_appt_confirmed_status, Me.XrLabel_office, Me.XrLabel_counselor, Me.XrLabel_status, Me.XrLabel_appt_type, Me.XrLabel_appt_time})
            Me.Detail.HeightF = 16.0!
            '
            'ParameterClient
            '
            Me.ParameterClient.Description = "Client ID"
            Me.ParameterClient.Name = "ParameterClient"
            Me.ParameterClient.Type = GetType(Integer)
            Me.ParameterClient.Value = 0
            Me.ParameterClient.Visible = False
            '
            'ParameterPendingOnly
            '
            Me.ParameterPendingOnly.Description = "Include Pending Appts"
            Me.ParameterPendingOnly.Name = "ParameterPendingOnly"
            Me.ParameterPendingOnly.Type = GetType(Boolean)
            Me.ParameterPendingOnly.Value = False
            '
            'XrLabel_appt_confirmed_status
            '
            Me.XrLabel_appt_confirmed_status.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "appt_confirmed_status")})
            Me.XrLabel_appt_confirmed_status.LocationFloat = New DevExpress.Utils.PointFloat(951.875!, 0.0!)
            Me.XrLabel_appt_confirmed_status.Name = "XrLabel_appt_confirmed_status"
            Me.XrLabel_appt_confirmed_status.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_appt_confirmed_status.SizeF = New System.Drawing.SizeF(111.125!, 16.0!)
            '
            'XrLabel_appt_time
            '
            Me.XrLabel_appt_time.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "appt_time", "{0:M/d/yy hh:mm tt}")})
            Me.XrLabel_appt_time.LocationFloat = New DevExpress.Utils.PointFloat(10.00001!, 0.0!)
            Me.XrLabel_appt_time.Name = "XrLabel_appt_time"
            Me.XrLabel_appt_time.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_appt_time.SizeF = New System.Drawing.SizeF(115.0!, 16.0!)
            Me.XrLabel_appt_time.StylePriority.UseTextAlignment = False
            Me.XrLabel_appt_time.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
            '
            'XrLabel_appt_type
            '
            Me.XrLabel_appt_type.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "appt_type")})
            Me.XrLabel_appt_type.LocationFloat = New DevExpress.Utils.PointFloat(125.0!, 0.0!)
            Me.XrLabel_appt_type.Name = "XrLabel_appt_type"
            Me.XrLabel_appt_type.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_appt_type.SizeF = New System.Drawing.SizeF(193.0412!, 16.0!)
            '
            'XrLabel_counselor
            '
            Me.XrLabel_counselor.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "counselor")})
            Me.XrLabel_counselor.LocationFloat = New DevExpress.Utils.PointFloat(378.8333!, 0.0!)
            Me.XrLabel_counselor.Name = "XrLabel_counselor"
            Me.XrLabel_counselor.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_counselor.SizeF = New System.Drawing.SizeF(97.58331!, 16.0!)
            '
            'XrLabel_office
            '
            Me.XrLabel_office.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "appt_office")})
            Me.XrLabel_office.LocationFloat = New DevExpress.Utils.PointFloat(771.2916!, 0.0!)
            Me.XrLabel_office.Name = "XrLabel_office"
            Me.XrLabel_office.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_office.SizeF = New System.Drawing.SizeF(180.5834!, 16.0!)
            '
            'XrLabel_status
            '
            Me.XrLabel_status.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "status")})
            Me.XrLabel_status.LocationFloat = New DevExpress.Utils.PointFloat(318.0411!, 0.0!)
            Me.XrLabel_status.Name = "XrLabel_status"
            Me.XrLabel_status.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_status.SizeF = New System.Drawing.SizeF(60.79181!, 16.0!)
            Me.XrLabel_status.StylePriority.UseTextAlignment = False
            Me.XrLabel_status.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter
            '
            'XrLabelPartnerCode
            '
            Me.XrLabelPartnerCode.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "partner_code")})
            Me.XrLabelPartnerCode.LocationFloat = New DevExpress.Utils.PointFloat(476.4166!, 0.0!)
            Me.XrLabelPartnerCode.Name = "XrLabelPartnerCode"
            Me.XrLabelPartnerCode.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabelPartnerCode.SizeF = New System.Drawing.SizeF(116.0416!, 16.0!)
            '
            'XrLabelReferral
            '
            Me.XrLabelReferral.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "referral")})
            Me.XrLabelReferral.LocationFloat = New DevExpress.Utils.PointFloat(592.4583!, 0.0!)
            Me.XrLabelReferral.Name = "XrLabelReferral"
            Me.XrLabelReferral.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabelReferral.SizeF = New System.Drawing.SizeF(178.8333!, 16.0!)
            '
            'GroupHeader1
            '
            Me.GroupHeader1.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrPanel1})
            Me.GroupHeader1.HeightF = 25.0!
            Me.GroupHeader1.Name = "GroupHeader1"
            Me.GroupHeader1.RepeatEveryPage = True
            '
            'GroupHeader2
            '
            Me.GroupHeader2.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrSubreport_ClientNameAndAddress})
            Me.GroupHeader2.HeightF = 240.625!
            Me.GroupHeader2.Level = 1
            Me.GroupHeader2.Name = "GroupHeader2"
            '
            'XrPanel1
            '
            Me.XrPanel1.BackColor = System.Drawing.Color.Teal
            Me.XrPanel1.BorderColor = System.Drawing.Color.Teal
            Me.XrPanel1.CanGrow = False
            Me.XrPanel1.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabelRef, Me.XrLabel7, Me.XrLabel5, Me.XrLabel4, Me.XrLabel3, Me.XrLabel_hdr_appt_time, Me.XrLabel8, Me.XrLabel1})
            Me.XrPanel1.KeepTogether = False
            Me.XrPanel1.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
            Me.XrPanel1.Name = "XrPanel1"
            Me.XrPanel1.SizeF = New System.Drawing.SizeF(1071.0!, 18.00006!)
            Me.XrPanel1.StylePriority.UseBackColor = False
            Me.XrPanel1.StylePriority.UseBorderColor = False
            '
            'XrLabelRef
            '
            Me.XrLabelRef.BackColor = System.Drawing.Color.Transparent
            Me.XrLabelRef.BorderColor = System.Drawing.Color.Transparent
            Me.XrLabelRef.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabelRef.ForeColor = System.Drawing.Color.White
            Me.XrLabelRef.LocationFloat = New DevExpress.Utils.PointFloat(592.4582!, 1.0!)
            Me.XrLabelRef.Name = "XrLabelRef"
            Me.XrLabelRef.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabelRef.SizeF = New System.Drawing.SizeF(178.8334!, 15.0!)
            Me.XrLabelRef.StylePriority.UseBackColor = False
            Me.XrLabelRef.StylePriority.UseBorderColor = False
            Me.XrLabelRef.StylePriority.UseFont = False
            Me.XrLabelRef.StylePriority.UseForeColor = False
            Me.XrLabelRef.StylePriority.UseTextAlignment = False
            Me.XrLabelRef.Text = "REFERRAL"
            Me.XrLabelRef.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            '
            'XrLabel7
            '
            Me.XrLabel7.BackColor = System.Drawing.Color.Transparent
            Me.XrLabel7.BorderColor = System.Drawing.Color.Transparent
            Me.XrLabel7.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel7.ForeColor = System.Drawing.Color.White
            Me.XrLabel7.LocationFloat = New DevExpress.Utils.PointFloat(951.875!, 1.0!)
            Me.XrLabel7.Name = "XrLabel7"
            Me.XrLabel7.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel7.SizeF = New System.Drawing.SizeF(111.125!, 15.0!)
            Me.XrLabel7.StylePriority.UseBackColor = False
            Me.XrLabel7.StylePriority.UseBorderColor = False
            Me.XrLabel7.StylePriority.UseFont = False
            Me.XrLabel7.StylePriority.UseForeColor = False
            Me.XrLabel7.StylePriority.UseTextAlignment = False
            Me.XrLabel7.Text = "CONFIRMED"
            Me.XrLabel7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            '
            'XrLabel5
            '
            Me.XrLabel5.BackColor = System.Drawing.Color.Transparent
            Me.XrLabel5.BorderColor = System.Drawing.Color.Transparent
            Me.XrLabel5.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel5.ForeColor = System.Drawing.Color.White
            Me.XrLabel5.LocationFloat = New DevExpress.Utils.PointFloat(771.2916!, 1.0!)
            Me.XrLabel5.Name = "XrLabel5"
            Me.XrLabel5.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel5.SizeF = New System.Drawing.SizeF(180.5834!, 15.0!)
            Me.XrLabel5.StylePriority.UseBackColor = False
            Me.XrLabel5.StylePriority.UseBorderColor = False
            Me.XrLabel5.StylePriority.UseFont = False
            Me.XrLabel5.StylePriority.UseForeColor = False
            Me.XrLabel5.StylePriority.UseTextAlignment = False
            Me.XrLabel5.Text = "OFFICE"
            Me.XrLabel5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            '
            'XrLabel4
            '
            Me.XrLabel4.BackColor = System.Drawing.Color.Transparent
            Me.XrLabel4.BorderColor = System.Drawing.Color.Transparent
            Me.XrLabel4.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel4.ForeColor = System.Drawing.Color.White
            Me.XrLabel4.LocationFloat = New DevExpress.Utils.PointFloat(378.8331!, 1.0!)
            Me.XrLabel4.Name = "XrLabel4"
            Me.XrLabel4.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel4.SizeF = New System.Drawing.SizeF(97.58331!, 15.0!)
            Me.XrLabel4.StylePriority.UseBackColor = False
            Me.XrLabel4.StylePriority.UseBorderColor = False
            Me.XrLabel4.StylePriority.UseFont = False
            Me.XrLabel4.StylePriority.UseForeColor = False
            Me.XrLabel4.StylePriority.UseTextAlignment = False
            Me.XrLabel4.Text = "COUNSELOR"
            Me.XrLabel4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            '
            'XrLabel3
            '
            Me.XrLabel3.BackColor = System.Drawing.Color.Transparent
            Me.XrLabel3.BorderColor = System.Drawing.Color.Transparent
            Me.XrLabel3.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel3.ForeColor = System.Drawing.Color.White
            Me.XrLabel3.LocationFloat = New DevExpress.Utils.PointFloat(318.0411!, 1.0!)
            Me.XrLabel3.Name = "XrLabel3"
            Me.XrLabel3.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel3.SizeF = New System.Drawing.SizeF(60.79199!, 15.0!)
            Me.XrLabel3.StylePriority.UseBackColor = False
            Me.XrLabel3.StylePriority.UseBorderColor = False
            Me.XrLabel3.StylePriority.UseFont = False
            Me.XrLabel3.StylePriority.UseForeColor = False
            Me.XrLabel3.StylePriority.UseTextAlignment = False
            Me.XrLabel3.Text = "STATUS"
            Me.XrLabel3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
            '
            'XrLabel_hdr_appt_time
            '
            Me.XrLabel_hdr_appt_time.BackColor = System.Drawing.Color.Transparent
            Me.XrLabel_hdr_appt_time.BorderColor = System.Drawing.Color.Transparent
            Me.XrLabel_hdr_appt_time.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel_hdr_appt_time.ForeColor = System.Drawing.Color.White
            Me.XrLabel_hdr_appt_time.LocationFloat = New DevExpress.Utils.PointFloat(10.00001!, 1.0!)
            Me.XrLabel_hdr_appt_time.Name = "XrLabel_hdr_appt_time"
            Me.XrLabel_hdr_appt_time.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_hdr_appt_time.SizeF = New System.Drawing.SizeF(115.0!, 15.0!)
            Me.XrLabel_hdr_appt_time.StylePriority.UseBackColor = False
            Me.XrLabel_hdr_appt_time.StylePriority.UseBorderColor = False
            Me.XrLabel_hdr_appt_time.StylePriority.UseFont = False
            Me.XrLabel_hdr_appt_time.StylePriority.UseForeColor = False
            Me.XrLabel_hdr_appt_time.StylePriority.UseTextAlignment = False
            Me.XrLabel_hdr_appt_time.Text = "DATE/TIME"
            Me.XrLabel_hdr_appt_time.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            '
            'XrLabel8
            '
            Me.XrLabel8.BackColor = System.Drawing.Color.Transparent
            Me.XrLabel8.BorderColor = System.Drawing.Color.Transparent
            Me.XrLabel8.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel8.ForeColor = System.Drawing.Color.White
            Me.XrLabel8.LocationFloat = New DevExpress.Utils.PointFloat(125.0!, 1.0!)
            Me.XrLabel8.Name = "XrLabel8"
            Me.XrLabel8.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel8.SizeF = New System.Drawing.SizeF(193.0412!, 15.0!)
            Me.XrLabel8.StylePriority.UseBackColor = False
            Me.XrLabel8.StylePriority.UseBorderColor = False
            Me.XrLabel8.StylePriority.UseFont = False
            Me.XrLabel8.StylePriority.UseForeColor = False
            Me.XrLabel8.StylePriority.UseTextAlignment = False
            Me.XrLabel8.Text = "TYPE"
            Me.XrLabel8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            '
            'XrLabel1
            '
            Me.XrLabel1.BackColor = System.Drawing.Color.Transparent
            Me.XrLabel1.BorderColor = System.Drawing.Color.Transparent
            Me.XrLabel1.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel1.ForeColor = System.Drawing.Color.White
            Me.XrLabel1.LocationFloat = New DevExpress.Utils.PointFloat(476.4164!, 1.0!)
            Me.XrLabel1.Multiline = True
            Me.XrLabel1.Name = "XrLabel1"
            Me.XrLabel1.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel1.SizeF = New System.Drawing.SizeF(116.0417!, 15.0!)
            Me.XrLabel1.StylePriority.UseBackColor = False
            Me.XrLabel1.StylePriority.UseBorderColor = False
            Me.XrLabel1.StylePriority.UseFont = False
            Me.XrLabel1.StylePriority.UseForeColor = False
            Me.XrLabel1.StylePriority.UseTextAlignment = False
            Me.XrLabel1.Text = "PARTNER"
            Me.XrLabel1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            '
            'XrSubreport_ClientNameAndAddress
            '
            Me.XrSubreport_ClientNameAndAddress.CanShrink = True
            Me.XrSubreport_ClientNameAndAddress.LocationFloat = New DevExpress.Utils.PointFloat(75.0!, 110.0!)
            Me.XrSubreport_ClientNameAndAddress.Name = "XrSubreport_ClientNameAndAddress"
            Me.XrSubreport_ClientNameAndAddress.ReportSource = Me.ClientNameAndAddressReport
            Me.XrSubreport_ClientNameAndAddress.SizeF = New System.Drawing.SizeF(300.0!, 100.0!)
            '
            'ClientAppointmentsReport
            '
            Me.Bands.AddRange(New DevExpress.XtraReports.UI.Band() {Me.TopMarginBand1, Me.Detail, Me.BottomMarginBand1, Me.PageHeader, Me.PageFooter, Me.GroupHeader1, Me.GroupHeader2})
            Me.Landscape = True
            Me.Margins = New System.Drawing.Printing.Margins(25, 4, 25, 25)
            Me.PageHeight = 850
            Me.PageWidth = 1100
            Me.Parameters.AddRange(New DevExpress.XtraReports.Parameters.Parameter() {Me.ParameterClient, Me.ParameterPendingOnly})
            Me.RequestParameters = False
            Me.ScriptLanguage = DevExpress.XtraReports.ScriptLanguage.VisualBasic
            Me.ScriptReferencesString = resources.GetString("$this.ScriptReferencesString")
            Me.Scripts.OnBeforePrint = "ClientAppointmentsReport_BeforePrint"
            Me.ScriptsSource = resources.GetString("$this.ScriptsSource")
            Me.StyleSheet.AddRange(New DevExpress.XtraReports.UI.XRControlStyle() {Me.XrControlStyle_Header, Me.XrControlStyle_Totals, Me.XrControlStyle_HeaderPannel, Me.XrControlStyle_GroupHeader})
            Me.Version = "11.2"
            Me.Controls.SetChildIndex(Me.GroupHeader2, 0)
            Me.Controls.SetChildIndex(Me.GroupHeader1, 0)
            Me.Controls.SetChildIndex(Me.PageFooter, 0)
            Me.Controls.SetChildIndex(Me.PageHeader, 0)
            Me.Controls.SetChildIndex(Me.BottomMarginBand1, 0)
            Me.Controls.SetChildIndex(Me.Detail, 0)
            Me.Controls.SetChildIndex(Me.TopMarginBand1, 0)
            CType(Me.ClientNameAndAddressReport, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me, System.ComponentModel.ISupportInitialize).EndInit()

        End Sub

        Private ParameterClient As DevExpress.XtraReports.Parameters.Parameter
        Private ParameterPendingOnly As DevExpress.XtraReports.Parameters.Parameter
        Private XrLabel_appt_confirmed_status As DevExpress.XtraReports.UI.XRLabel
        Private XrLabel_appt_time As DevExpress.XtraReports.UI.XRLabel
        Private XrLabel_appt_type As DevExpress.XtraReports.UI.XRLabel
        Private XrLabel_counselor As DevExpress.XtraReports.UI.XRLabel
        Private XrLabel_office As DevExpress.XtraReports.UI.XRLabel
        Private XrLabel_status As DevExpress.XtraReports.UI.XRLabel
        Private XrLabelPartnerCode As DevExpress.XtraReports.UI.XRLabel
        Private XrLabelReferral As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents GroupHeader1 As DevExpress.XtraReports.UI.GroupHeaderBand
        Private WithEvents XrPanel1 As DevExpress.XtraReports.UI.XRPanel
        Private WithEvents XrLabelRef As DevExpress.XtraReports.UI.XRLabel
        Private WithEvents XrLabel7 As DevExpress.XtraReports.UI.XRLabel
        Private WithEvents XrLabel5 As DevExpress.XtraReports.UI.XRLabel
        Private WithEvents XrLabel4 As DevExpress.XtraReports.UI.XRLabel
        Private WithEvents XrLabel3 As DevExpress.XtraReports.UI.XRLabel
        Private WithEvents XrLabel_hdr_appt_time As DevExpress.XtraReports.UI.XRLabel
        Private WithEvents XrLabel8 As DevExpress.XtraReports.UI.XRLabel
        Private WithEvents XrLabel1 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents GroupHeader2 As DevExpress.XtraReports.UI.GroupHeaderBand
        Private WithEvents XrSubreport_ClientNameAndAddress As DevExpress.XtraReports.UI.XRSubreport
        Private WithEvents ClientNameAndAddressReport As DebtPlus.Reports.Client.Appointments.ClientNameAndAddress
    End Class
End Namespace
