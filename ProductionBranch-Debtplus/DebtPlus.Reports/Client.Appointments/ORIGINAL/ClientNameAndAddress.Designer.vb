Namespace Client.Appointments
    <Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
    Partial Class ClientNameAndAddress
        Inherits DevExpress.XtraReports.UI.XtraReport

        'XtraReport overrides dispose to clean up the component list.
        <System.Diagnostics.DebuggerNonUserCode()> _
        Protected Overrides Sub Dispose(ByVal disposing As Boolean)
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
            MyBase.Dispose(disposing)
        End Sub

        'Required by the Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Designer
        'It can be modified using the Designer.  
        'Do not modify it using the code editor.
        <System.Diagnostics.DebuggerStepThrough()> _
        Private Sub InitializeComponent()
            Dim PostNetGenerator1 As DevExpress.XtraPrinting.BarCode.PostNetGenerator = New DevExpress.XtraPrinting.BarCode.PostNetGenerator()
            Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(ClientNameAndAddress))
            Me.Detail = New DevExpress.XtraReports.UI.DetailBand()
            Me.AddressInformation = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrBarCode_Zipcode = New DevExpress.XtraReports.UI.XRBarCode()
            Me.TopMarginBand1 = New DevExpress.XtraReports.UI.TopMarginBand()
            Me.BottomMarginBand1 = New DevExpress.XtraReports.UI.BottomMarginBand()
            CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
            '
            'Detail
            '
            Me.Detail.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.AddressInformation, Me.XrBarCode_Zipcode})
            Me.Detail.Name = "Detail"
            Me.Detail.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
            Me.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
            '
            'AddressInformation
            '
            Me.AddressInformation.CanGrow = False
            Me.AddressInformation.CanShrink = True
            Me.AddressInformation.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 17.0!)
            Me.AddressInformation.Multiline = True
            Me.AddressInformation.Name = "AddressInformation"
            Me.AddressInformation.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.AddressInformation.SizeF = New System.Drawing.SizeF(300.0!, 83.0!)
            Me.AddressInformation.WordWrap = False
            '
            'XrBarCode_Zipcode
            '
            Me.XrBarCode_Zipcode.BorderColor = System.Drawing.Color.Black
            Me.XrBarCode_Zipcode.Borders = DevExpress.XtraPrinting.BorderSide.None
            Me.XrBarCode_Zipcode.ForeColor = System.Drawing.Color.Black
            Me.XrBarCode_Zipcode.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
            Me.XrBarCode_Zipcode.Module = 3.0!
            Me.XrBarCode_Zipcode.Name = "XrBarCode_Zipcode"
            Me.XrBarCode_Zipcode.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
            Me.XrBarCode_Zipcode.Scripts.OnBeforePrint = "XrBarCode_Zipcode_BeforePrint"
            Me.XrBarCode_Zipcode.ShowText = False
            Me.XrBarCode_Zipcode.SizeF = New System.Drawing.SizeF(300.0!, 15.0!)
            Me.XrBarCode_Zipcode.StylePriority.UseBorderColor = False
            Me.XrBarCode_Zipcode.StylePriority.UseBorders = False
            Me.XrBarCode_Zipcode.StylePriority.UseForeColor = False
            Me.XrBarCode_Zipcode.Symbology = PostNetGenerator1
            Me.XrBarCode_Zipcode.Visible = False
            '
            'TopMarginBand1
            '
            Me.TopMarginBand1.HeightF = 0.0!
            Me.TopMarginBand1.Name = "TopMarginBand1"
            '
            'BottomMarginBand1
            '
            Me.BottomMarginBand1.HeightF = 0.0!
            Me.BottomMarginBand1.Name = "BottomMarginBand1"
            '
            'ClientNameAndAddress
            '
            Me.Bands.AddRange(New DevExpress.XtraReports.UI.Band() {Me.Detail, Me.TopMarginBand1, Me.BottomMarginBand1})
            Me.Margins = New System.Drawing.Printing.Margins(100, 100, 0, 0)
            Me.ScriptLanguage = DevExpress.XtraReports.ScriptLanguage.VisualBasic
            Me.ScriptReferencesString = resources.GetString("$this.ScriptReferencesString")
            Me.Scripts.OnBeforePrint = "ClientNameAndAddress_BeforePrint"
            Me.ScriptsSource = resources.GetString("$this.ScriptsSource")
            Me.Version = "11.2"
            CType(Me, System.ComponentModel.ISupportInitialize).EndInit()

        End Sub
        Friend WithEvents Detail As DevExpress.XtraReports.UI.DetailBand
        Protected Friend WithEvents XrBarCode_Zipcode As DevExpress.XtraReports.UI.XRBarCode
        Friend WithEvents AddressInformation As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents TopMarginBand1 As DevExpress.XtraReports.UI.TopMarginBand
        Friend WithEvents BottomMarginBand1 As DevExpress.XtraReports.UI.BottomMarginBand
    End Class
End Namespace
