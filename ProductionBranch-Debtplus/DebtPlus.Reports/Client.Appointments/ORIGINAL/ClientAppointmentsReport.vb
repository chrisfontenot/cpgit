#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}
#End Region

Option Compare Binary
Option Explicit On 
Option Strict On

Imports DebtPlus.Reports
Imports DevExpress.XtraReports.UI

Namespace Client.Appointments
    Public Class ClientAppointmentsReport
        Implements DebtPlus.Interfaces.Client.IClient

        ''' <summary>
        ''' Create an instance of our report
        ''' </summary>
        Public Sub New()
            MyBase.New()
            InitializeComponent()

            ' Clear the parameters
            Parameter_Client = -1
            Parameter_PendingOnly = False

            AddHandler Me.BeforePrint, AddressOf ClientAppointmentsReport_BeforePrint

            '-- Enable the select expert
            ReportFilter.IsEnabled = True

            ' Set the script references as needed
            ScriptReferences = DebtPlus.Utils.AssemblyTools.GetAllSiblingAssemblies()

            ' Update the subreports as well
            For Each BandPtr As DevExpress.XtraReports.UI.Band In Bands
                For Each CtlPtr As DevExpress.XtraReports.UI.XRControl In BandPtr.Controls
                    Dim Rpt As DevExpress.XtraReports.UI.XRSubreport = TryCast(CtlPtr, DevExpress.XtraReports.UI.XRSubreport)
                    If Rpt IsNot Nothing Then
                        Dim RptSrc As DevExpress.XtraReports.UI.XtraReport = TryCast(Rpt.ReportSource, DevExpress.XtraReports.UI.XtraReport)
                        If RptSrc IsNot Nothing Then
                            RptSrc.ScriptReferences = ScriptReferences
                        End If
                    End If
                Next
            Next
        End Sub

        Public Overrides ReadOnly Property ReportTitle() As String
            Get
                Return "Client Appointments"
            End Get
        End Property

        Public Overrides ReadOnly Property ReportSubTitle() As String
            Get
                Return String.Empty
            End Get
        End Property

        Public Property Parameter_PendingOnly() As Boolean
            Get
                Dim parm As DevExpress.XtraReports.Parameters.Parameter = FindParameter("ParameterPendingOnly")
                If parm Is Nothing Then Return False
                Return Convert.ToBoolean(parm.Value)
            End Get
            Set(ByVal value As Boolean)
                SetParameter("ParameterPendingOnly", GetType(Boolean), value, "Pending items only?", False)
            End Set
        End Property

        Public Property ClientID() As System.Int32 Implements DebtPlus.Interfaces.Client.IClient.ClientId
            Get
                Return Parameter_Client
            End Get
            Set(value As System.Int32)
                Parameter_Client = value
            End Set
        End Property

        Public Property Parameter_Client() As System.Int32
            Get
                Dim parm As DevExpress.XtraReports.Parameters.Parameter = FindParameter("ParameterClient")
                If parm Is Nothing Then Return -1
                Return Convert.ToInt32(parm.Value)
            End Get
            Set(ByVal value As System.Int32)
                SetParameter("ParameterClient", GetType(Int32), value, "Client ID", False)
            End Set
        End Property

        Public Overrides Sub SetReportParameter(ByVal Parameter As String, ByVal Value As Object)
            If Parameter = "Client" Then
                Parameter_Client = Convert.ToInt32(Value)
            ElseIf Parameter = "PendingOnly" Then
                Parameter_PendingOnly = CBool(Value)
            Else
                MyBase.SetReportParameter(Parameter, Value)
            End If
        End Sub

        Public Overrides Function NeedParameters() As Boolean
            Return MyBase.NeedParameters() OrElse Parameter_Client < 0
        End Function

        Public Overrides Function RequestReportParameters() As System.Windows.Forms.DialogResult
            Dim Answer As System.Windows.Forms.DialogResult = DialogResult.OK

            If NeedParameters() Then
                Using frm As New DebtPlus.Reports.Template.Forms.ClientParametersForm()
                    With frm
                        .Parameter_Client = Parameter_Client
                        Answer = .ShowDialog
                        Parameter_Client = .Parameter_Client
                    End With
                End Using
            End If

            Return Answer
        End Function

        Dim ds As New System.Data.DataSet("ds")

        Private Sub ClientAppointmentsReport_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs)
            Dim rpt As DevExpress.XtraReports.UI.XtraReport = CType(sender, DevExpress.XtraReports.UI.XtraReport)
            Const TableName As String = "view_client_appointments"

            Dim tbl As System.Data.DataTable = ds.Tables(TableName)
            If tbl Is Nothing Then
                Dim sqlInfo As DebtPlus.LINQ.SQLInfoClass = DebtPlus.LINQ.SQLInfoClass.getDefault()
                Try
                    Using cm As New DebtPlus.UI.Common.CursorManager()
                        Using cn As System.Data.SqlClient.SqlConnection = New System.Data.SqlClient.SqlConnection(sqlInfo.ConnectionString)
                            cn.Open()
                            Using cmd As New System.Data.SqlClient.SqlCommand()
                                cmd.Connection = cn

                                cmd.CommandText = "SELECT [appt_confirmed_status],[appt_office],[appt_time],[appt_type],[client],[counselor],[partner_code],[referral],[status] FROM [view_client_appointments] WITH (NOLOCK) WHERE [client]=@client"
                                If Convert.ToBoolean(rpt.Parameters("ParameterPendingOnly").Value) Then
                                    cmd.CommandText += " AND [status]='PE'"
                                End If

                                cmd.CommandType = System.Data.CommandType.Text
                                cmd.Parameters.Add("@client", System.Data.SqlDbType.Int).Value = rpt.Parameters("ParameterClient").Value
                                cmd.CommandTimeout = 0

                                Using da As New System.Data.SqlClient.SqlDataAdapter(cmd)
                                    da.Fill(ds, TableName)
                                    tbl = ds.Tables(TableName)
                                End Using
                            End Using
                        End Using
                    End Using

                Catch ex As System.Data.SqlClient.SqlException
                    DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex)
                End Try
            End If

            If tbl IsNot Nothing Then
                rpt.DataSource = New System.Data.DataView(tbl, CType(rpt, DebtPlus.Interfaces.Reports.IReportFilter).ReportFilter.ViewFilter, "appt_time", System.Data.DataViewRowState.CurrentRows)
                For Each fld As DevExpress.XtraReports.UI.CalculatedField In rpt.CalculatedFields
                    fld.Assign(rpt.DataSource, rpt.DataMember)
                Next
            End If
        End Sub
    End Class
End Namespace
