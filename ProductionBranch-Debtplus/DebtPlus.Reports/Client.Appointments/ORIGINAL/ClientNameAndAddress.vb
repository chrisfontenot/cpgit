#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}
#End Region

Namespace Client.Appointments
    Friend Class ClientNameAndAddress

        Public Sub New()
            MyBase.New()
            InitializeComponent()
            AddHandler Me.BeforePrint, AddressOf ClientNameAndAddress_BeforePrint
            AddHandler XrBarCode_Zipcode.BeforePrint, AddressOf XrBarCode_Zipcode_BeforePrint
        End Sub

        Private ds As New System.Data.DataSet("ds")

        Private Sub ClientNameAndAddress_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs)
            Dim rpt As DevExpress.XtraReports.UI.XtraReport = CType(sender, DevExpress.XtraReports.UI.XtraReport)
            Dim MasterRpt As DevExpress.XtraReports.UI.XtraReport = CType(rpt.MasterReport, DevExpress.XtraReports.UI.XtraReport)
            Dim Client As System.Int32 = Convert.ToInt32(MasterRpt.Parameters("ParameterClient").Value, System.Globalization.CultureInfo.InvariantCulture)

            '-- If we have not been initialized then do not do the report
            If Client >= 0 Then

                Dim sb As New System.Text.StringBuilder
                Dim row As System.Data.DataRow = GetClientRow(Client)
                If row IsNot Nothing Then

                    '-- Format the Client ID before the name
                    sb.AppendFormat("{0:0000000}", Client)

                    '-- Set the zipcode information
                    Dim BarCodeCtl As DevExpress.XtraReports.UI.XRBarCode = CType(rpt.FindControl("XrBarCode_Zipcode", True), DevExpress.XtraReports.UI.XRBarCode)
                    If BarCodeCtl IsNot Nothing Then
                        BarCodeCtl.Text = Convert.ToString(row("zipcode"), System.Globalization.CultureInfo.InvariantCulture).Trim
                    End If

                    For Each key As String In New String() {"name", "addr1", "addr2", "addr3"}
                        Dim Obj As Object = row(key)
                        Dim Value As String = String.Empty
                        If Obj IsNot Nothing AndAlso Obj IsNot System.DBNull.Value Then Value = Convert.ToString(Obj, System.Globalization.CultureInfo.InvariantCulture).Trim

                        If Value <> String.Empty Then
                            sb.Append(Environment.NewLine)
                            sb.Append(Value)
                        End If
                    Next
                End If

                '-- Set the address information into the report
                Dim ctl As DevExpress.XtraReports.UI.XRLabel = CType(rpt.FindControl("AddressInformation", True), DevExpress.XtraReports.UI.XRLabel)
                If ctl IsNot Nothing Then
                    ctl.Text = sb.ToString()
                End If
            End If
        End Sub

        Private Function GetClientRow(ByVal Client As System.Int32) As System.Data.DataRow
            Dim current_cursor As System.Windows.Forms.Cursor = System.Windows.Forms.Cursor.Current
            Dim row As System.Data.DataRow = Nothing

            Const TableName As String = "view_client_address"
            Try
                Dim tbl As System.Data.DataTable = ds.Tables(TableName)
                If tbl IsNot Nothing Then
                    row = tbl.Rows.Find(Client)
                End If

                If row Is Nothing Then
                    System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.WaitCursor
                    Dim sqlInfo As DebtPlus.LINQ.SQLInfoClass = DebtPlus.LINQ.SQLInfoClass.getDefault()
                    Using cn As System.Data.SqlClient.SqlConnection = New System.Data.SqlClient.SqlConnection(sqlInfo.ConnectionString)
                        cn.Open()
                        Using cmd As New System.Data.SqlClient.SqlCommand()
                            cmd.Connection = cn
                            cmd.CommandText = "SELECT TOP 1 client, isnull(zipcode,'') as zipcode, isnull(name,'') as name, isnull(addr1,'') as addr1, isnull(addr2,'') as addr2, isnull(addr3,'') as addr3 FROM view_client_address WHERE client=@client"
                            cmd.CommandType = System.Data.CommandType.Text
                            cmd.Parameters.Add("@client", System.Data.SqlDbType.Int).Value = Client

                            Using da As New System.Data.SqlClient.SqlDataAdapter(cmd)
                                da.Fill(ds, TableName)
                                tbl = ds.Tables(TableName)
                                tbl.PrimaryKey = New System.Data.DataColumn() {tbl.Columns("client")}
                            End Using
                        End Using
                    End Using

                    row = tbl.Rows.Find(Client)
                End If

            Catch ex As System.Data.SqlClient.SqlException
                Using gdr As New DebtPlus.Repository.GetDataResult()
                    gdr.HandleException(ex)
                    DebtPlus.UI.Common.ErrorHandling.HandleErrors(gdr, "Error reading client name and address")
                End Using
            End Try

            Return row
        End Function

        Private Sub XrBarCode_Zipcode_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs)
            Dim bar As DevExpress.XtraReports.UI.XRBarCode = CType(sender, DevExpress.XtraReports.UI.XRBarCode)
            If Not bar.Visible OrElse bar.Text = String.Empty Then
                e.Cancel = True
            End If
        End Sub
    End Class
End Namespace
