#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}

#End Region


Imports DevExpress.XtraReports.UI
Imports System.Drawing.Printing

Namespace Operations.Client.Contributions.ByZipcode
    Public Class DetailReport

        Public Sub New()
            MyBase.New()
            InitializeComponent()
            AddHandler XrLabel_detail_name.BeforePrint, AddressOf XrLabel_detail_name_BeforePrint
        End Sub

        Private vue As DataView = Nothing

        Public Sub New(ByVal vue As DataView)
            MyClass.New()
            Me.vue = vue
            Me.OfficeName = Convert.ToString(vue(0)("office_name")).Trim()
        End Sub

        Private OfficeName As String = String.Empty
        Private privateSubtitle As String = String.Empty

        Public Overrides ReadOnly Property ReportSubTitle() As String
            Get
                Return privateSubtitle
            End Get
        End Property

        Public Sub SetReportSubtitle(ByVal SubtitleString As String)
            privateSubtitle = SubtitleString
        End Sub

        Protected Overrides Sub SetReportDataSource()

            XrLabel_fairshare_bill.DataBindings.Add("Text", Nothing, "fairshare_bill", "{0:c}")
            XrLabel_fairshare_deduct.DataBindings.Add("Text", Nothing, "fairshare_deduct", "{0:c}")
            XrLabel_paf_amount.DataBindings.Add("Text", Nothing, "paf_amount", "{0:c}")
            XrLabel_fee_amount.DataBindings.Add("Text", Nothing, "fee_amount", "{0:c}")
            XrLabel_subtotal.DataBindings.Add("Text", Nothing, "total", "{0:c}")
            XrLabel_zipcode.DataBindings.Add("Text", Nothing, "zipcode")
            XrLabel_active_status.DataBindings.Add("Text", Nothing, "active_status")

            DataSource = vue
        End Sub

        Protected Overrides Sub Field_PreviewClick(ByVal sender As Object, ByVal e As PreviewMouseEventArgs)
        End Sub

        Private Sub XrLabel_detail_name_BeforePrint(ByVal sender As Object, ByVal e As PrintEventArgs)
            With CType(sender, XRLabel)
                .Text = String.Format("{0:0000000}", GetCurrentColumnValue("client")) + " " + DebtPlus.Utils.Nulls.DStr(GetCurrentColumnValue("client_name"))
            End With
        End Sub
    End Class
End Namespace