Namespace Operations.Client.Contributions.ByZipcode
    <Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
    Partial Class DetailReport
        Inherits ClientByZipcodeReport

        'XtraReport overrides dispose to clean up the component list.
        <System.Diagnostics.DebuggerNonUserCode()> _
        Protected Overrides Sub Dispose(ByVal disposing As Boolean)
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
            MyBase.Dispose(disposing)
        End Sub

        'Required by the Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Designer
        'It can be modified using the Designer.  
        'Do not modify it using the code editor.
        <System.Diagnostics.DebuggerStepThrough()> _
        Private Sub InitializeComponent()
            Me.XrLabel_detail_name = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_fee_amount = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_paf_amount = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_fairshare_deduct = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_fairshare_bill = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_subtotal = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel7 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel8 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_active_status = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_zipcode = New DevExpress.XtraReports.UI.XRLabel
            CType(Me.ds, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
            '
            'XrPanel1
            '
            Me.XrPanel1.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel8, Me.XrLabel7})
            Me.XrPanel1.Controls.SetChildIndex(Me.XrLabel1, 0)
            Me.XrPanel1.Controls.SetChildIndex(Me.XrLabel2, 0)
            Me.XrPanel1.Controls.SetChildIndex(Me.XrLabel3, 0)
            Me.XrPanel1.Controls.SetChildIndex(Me.XrLabel4, 0)
            Me.XrPanel1.Controls.SetChildIndex(Me.XrLabel5, 0)
            Me.XrPanel1.Controls.SetChildIndex(Me.XrLabel6, 0)
            Me.XrPanel1.Controls.SetChildIndex(Me.XrLabel7, 0)
            Me.XrPanel1.Controls.SetChildIndex(Me.XrLabel8, 0)
            '
            'XrLabel1
            '
            Me.XrLabel1.LocationFloat = New DevExpress.Utils.PointFloat(7.999992!, 1.0!)
            Me.XrLabel1.SizeF = New System.Drawing.SizeF(290.9583!, 14.99998!)
            Me.XrLabel1.StylePriority.UseTextAlignment = False
            Me.XrLabel1.Text = "CLIENT ID AND NAME"
            '
            'XrLabel_group_fairshare_bill
            '
            Me.XrLabel_group_fairshare_bill.StylePriority.UseTextAlignment = False
            '
            'XrLabel_group_fairshare_deduct
            '
            Me.XrLabel_group_fairshare_deduct.StylePriority.UseTextAlignment = False
            '
            'XrLabel_group_paf_amount
            '
            Me.XrLabel_group_paf_amount.StylePriority.UseTextAlignment = False
            '
            'XrLabel_group_fee_amount
            '
            Me.XrLabel_group_fee_amount.StylePriority.UseTextAlignment = False
            '
            'XrLabel_group_subtotal
            '
            Me.XrLabel_group_subtotal.StylePriority.UseTextAlignment = False
            '
            'GroupHeader1
            '
            Me.GroupHeader1.Visible = False
            '
            'GroupFooter1
            '
            Me.GroupFooter1.Visible = False
            '
            'XrLabel_total_fairshare_deduct
            '
            Me.XrLabel_total_fairshare_deduct.StylePriority.UseTextAlignment = False
            '
            'XrLabel_total_paf_amount
            '
            Me.XrLabel_total_paf_amount.StylePriority.UseTextAlignment = False
            '
            'XrLabel_total_fee_amount
            '
            Me.XrLabel_total_fee_amount.StylePriority.UseTextAlignment = False
            '
            'XrLabel13
            '
            Me.XrLabel13.StylePriority.UseTextAlignment = False
            '
            'XrLabel_total
            '
            Me.XrLabel_total.StylePriority.UseTextAlignment = False
            '
            'XrLabel_total_fairshare_bill
            '
            Me.XrLabel_total_fairshare_bill.StylePriority.UseTextAlignment = False
            '
            'Detail
            '
            Me.Detail.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel_zipcode, Me.XrLabel_active_status, Me.XrLabel_subtotal, Me.XrLabel_fairshare_bill, Me.XrLabel_fairshare_deduct, Me.XrLabel_paf_amount, Me.XrLabel_fee_amount, Me.XrLabel_detail_name})
            Me.Detail.HeightF = 15.0!
            '
            'XrLabel_Title
            '
            Me.XrLabel_Title.StylePriority.UseFont = False
            '
            'XrPageInfo_PageNumber
            '
            Me.XrPageInfo_PageNumber.StylePriority.UseTextAlignment = False
            '
            'XRLabel_Agency_Name
            '
            Me.XRLabel_Agency_Name.StylePriority.UseTextAlignment = False
            '
            'XrLabel_Agency_Address3
            '
            Me.XrLabel_Agency_Address3.StylePriority.UseFont = False
            Me.XrLabel_Agency_Address3.StylePriority.UseTextAlignment = False
            '
            'XrLabel_Agency_Address1
            '
            Me.XrLabel_Agency_Address1.StylePriority.UseFont = False
            Me.XrLabel_Agency_Address1.StylePriority.UseTextAlignment = False
            '
            'XrLabel_Agency_Phone
            '
            Me.XrLabel_Agency_Phone.StylePriority.UseFont = False
            Me.XrLabel_Agency_Phone.StylePriority.UseTextAlignment = False
            '
            'XrLabel_Agency_Address2
            '
            Me.XrLabel_Agency_Address2.StylePriority.UseFont = False
            Me.XrLabel_Agency_Address2.StylePriority.UseTextAlignment = False
            '
            'XrLabel_detail_name
            '
            Me.XrLabel_detail_name.LocationFloat = New DevExpress.Utils.PointFloat(10.00001!, 0.0!)
            Me.XrLabel_detail_name.Name = "XrLabel_detail_name"
            Me.XrLabel_detail_name.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_detail_name.SizeF = New System.Drawing.SizeF(356.6683!, 14.99999!)
            Me.XrLabel_detail_name.StylePriority.UseTextAlignment = False
            Me.XrLabel_detail_name.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            '
            'XrLabel_fee_amount
            '
            Me.XrLabel_fee_amount.LocationFloat = New DevExpress.Utils.PointFloat(502.125!, 0.0!)
            Me.XrLabel_fee_amount.Name = "XrLabel_fee_amount"
            Me.XrLabel_fee_amount.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_fee_amount.SizeF = New System.Drawing.SizeF(91.0!, 15.0!)
            Me.XrLabel_fee_amount.StylePriority.UseTextAlignment = False
            Me.XrLabel_fee_amount.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrLabel_fee_amount.WordWrap = False
            '
            'XrLabel_paf_amount
            '
            Me.XrLabel_paf_amount.LocationFloat = New DevExpress.Utils.PointFloat(593.1249!, 0.0!)
            Me.XrLabel_paf_amount.Name = "XrLabel_paf_amount"
            Me.XrLabel_paf_amount.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_paf_amount.SizeF = New System.Drawing.SizeF(100.0!, 15.0!)
            Me.XrLabel_paf_amount.StylePriority.UseTextAlignment = False
            Me.XrLabel_paf_amount.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrLabel_paf_amount.WordWrap = False
            '
            'XrLabel_fairshare_deduct
            '
            Me.XrLabel_fairshare_deduct.LocationFloat = New DevExpress.Utils.PointFloat(693.1249!, 0.0!)
            Me.XrLabel_fairshare_deduct.Name = "XrLabel_fairshare_deduct"
            Me.XrLabel_fairshare_deduct.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_fairshare_deduct.SizeF = New System.Drawing.SizeF(100.0!, 15.0!)
            Me.XrLabel_fairshare_deduct.StylePriority.UseTextAlignment = False
            Me.XrLabel_fairshare_deduct.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrLabel_fairshare_deduct.WordWrap = False
            '
            'XrLabel_fairshare_bill
            '
            Me.XrLabel_fairshare_bill.LocationFloat = New DevExpress.Utils.PointFloat(793.1249!, 0.0!)
            Me.XrLabel_fairshare_bill.Name = "XrLabel_fairshare_bill"
            Me.XrLabel_fairshare_bill.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_fairshare_bill.SizeF = New System.Drawing.SizeF(100.0!, 15.0!)
            Me.XrLabel_fairshare_bill.StylePriority.UseTextAlignment = False
            Me.XrLabel_fairshare_bill.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrLabel_fairshare_bill.WordWrap = False
            '
            'XrLabel_subtotal
            '
            Me.XrLabel_subtotal.LocationFloat = New DevExpress.Utils.PointFloat(893.1252!, 0.0!)
            Me.XrLabel_subtotal.Name = "XrLabel_subtotal"
            Me.XrLabel_subtotal.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_subtotal.SizeF = New System.Drawing.SizeF(100.0!, 15.0!)
            Me.XrLabel_subtotal.StylePriority.UseTextAlignment = False
            Me.XrLabel_subtotal.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrLabel_subtotal.WordWrap = False
            '
            'XrLabel7
            '
            Me.XrLabel7.LocationFloat = New DevExpress.Utils.PointFloat(377.47!, 1.0!)
            Me.XrLabel7.Name = "XrLabel7"
            Me.XrLabel7.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel7.SizeF = New System.Drawing.SizeF(67.70834!, 14.99998!)
            Me.XrLabel7.StylePriority.UseTextAlignment = False
            Me.XrLabel7.Text = "ZIPCODE"
            Me.XrLabel7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            Me.XrLabel7.WordWrap = False
            '
            'XrLabel8
            '
            Me.XrLabel8.LocationFloat = New DevExpress.Utils.PointFloat(455.99!, 1.0!)
            Me.XrLabel8.Name = "XrLabel8"
            Me.XrLabel8.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel8.SizeF = New System.Drawing.SizeF(35.33002!, 14.99998!)
            Me.XrLabel8.StylePriority.UseTextAlignment = False
            Me.XrLabel8.Text = "STS"
            Me.XrLabel8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            Me.XrLabel8.WordWrap = False
            '
            'XrLabel_active_status
            '
            Me.XrLabel_active_status.LocationFloat = New DevExpress.Utils.PointFloat(455.9889!, 0.0!)
            Me.XrLabel_active_status.Name = "XrLabel_active_status"
            Me.XrLabel_active_status.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_active_status.SizeF = New System.Drawing.SizeF(35.33002!, 14.99998!)
            Me.XrLabel_active_status.StylePriority.UseTextAlignment = False
            Me.XrLabel_active_status.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            Me.XrLabel_active_status.WordWrap = False
            '
            'XrLabel_zipcode
            '
            Me.XrLabel_zipcode.LocationFloat = New DevExpress.Utils.PointFloat(377.4744!, 0.0!)
            Me.XrLabel_zipcode.Name = "XrLabel_zipcode"
            Me.XrLabel_zipcode.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_zipcode.SizeF = New System.Drawing.SizeF(67.70834!, 14.99998!)
            Me.XrLabel_zipcode.StylePriority.UseTextAlignment = False
            Me.XrLabel_zipcode.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            Me.XrLabel_zipcode.WordWrap = False
            '
            'DetailReport
            '
            Me.Bands.AddRange(New DevExpress.XtraReports.UI.Band() {Me.Detail, Me.PageHeader, Me.PageFooter, Me.GroupHeader1, Me.GroupFooter1, Me.ReportFooter, Me.TopMarginBand1, Me.BottomMarginBand1})
            Me.StyleSheet.AddRange(New DevExpress.XtraReports.UI.XRControlStyle() {Me.XrControlStyle_Header, Me.XrControlStyle_Totals, Me.XrControlStyle_HeaderPannel, Me.XrControlStyle_GroupHeader})
            Me.Version = "10.1"
            CType(Me.ds, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me, System.ComponentModel.ISupportInitialize).EndInit()

        End Sub
        Friend WithEvents XrLabel_detail_name As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_subtotal As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_fairshare_bill As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_fairshare_deduct As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_paf_amount As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_fee_amount As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel7 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel8 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_zipcode As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_active_status As DevExpress.XtraReports.UI.XRLabel
    End Class
End Namespace