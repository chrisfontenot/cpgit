#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}

#End Region

Imports System.ComponentModel
Imports DebtPlus.Reports.Template

Imports System.Drawing.Printing
Imports System.Data.SqlClient
Imports System.Threading
Imports DebtPlus.Interfaces.Reports

Namespace Operations.Client.Contributions.ByZipcode
    Public Class ClientByZipcodeReport
        Inherits DatedTemplateXtraReportClass


        ''' <summary>
        ''' Create an instance of our report
        ''' </summary>
        Public Sub New()
            MyBase.New()
            InitializeComponent()

            AddHandler Me.BeforePrint, AddressOf Report_BeforePrint
            AddHandler XrLabel_group_detail_name.PreviewClick, AddressOf Field_PreviewClick
            AddHandler XrLabel_group_fairshare_bill.PreviewClick, AddressOf Field_PreviewClick
            AddHandler XrLabel_group_paf_amount.PreviewClick, AddressOf Field_PreviewClick
            AddHandler XrLabel_group_subtotal.PreviewClick, AddressOf Field_PreviewClick
            AddHandler XrLabel_group_fairshare_deduct.PreviewClick, AddressOf Field_PreviewClick
            AddHandler XrLabel_group_fee_amount.PreviewClick, AddressOf Field_PreviewClick

            XrLabel_total_fairshare_bill.DataBindings.Add("Text", Nothing, "fairshare_bill")
            XrLabel_total_fairshare_deduct.DataBindings.Add("Text", Nothing, "fairshare_deduct")
            XrLabel_total_fee_amount.DataBindings.Add("Text", Nothing, "fee_amount")
            XrLabel_total_paf_amount.DataBindings.Add("Text", Nothing, "paf_amount")
            XrLabel_total.DataBindings.Add("Text", Nothing, "total")
        End Sub

        Public Overrides ReadOnly Property ReportTitle() As String
            Get
                Return "Client Contributions by Office"
            End Get
        End Property

#Region " Component Designer generated code "

        'Component overrides dispose to clean up the component list.
        Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
            If disposing Then
                If components IsNot Nothing Then
                    components.Dispose()
                End If
                ds.Dispose()
            End If
            MyBase.Dispose(disposing)
        End Sub

        Protected Friend WithEvents XrPanel1 As DevExpress.XtraReports.UI.XRPanel
        Protected Friend WithEvents XrLabel1 As DevExpress.XtraReports.UI.XRLabel
        Protected Friend WithEvents XrLabel6 As DevExpress.XtraReports.UI.XRLabel
        Protected Friend WithEvents XrLabel5 As DevExpress.XtraReports.UI.XRLabel
        Protected Friend WithEvents XrLabel4 As DevExpress.XtraReports.UI.XRLabel
        Protected Friend WithEvents XrLabel3 As DevExpress.XtraReports.UI.XRLabel
        Protected Friend WithEvents XrLabel2 As DevExpress.XtraReports.UI.XRLabel
        Protected Friend WithEvents XrLabel_group_fairshare_bill As DevExpress.XtraReports.UI.XRLabel
        Protected Friend WithEvents XrLabel_group_fairshare_deduct As DevExpress.XtraReports.UI.XRLabel
        Protected Friend WithEvents XrLabel_group_paf_amount As DevExpress.XtraReports.UI.XRLabel
        Protected Friend WithEvents XrLabel_group_fee_amount As DevExpress.XtraReports.UI.XRLabel
        Protected Friend WithEvents XrLabel_group_subtotal As DevExpress.XtraReports.UI.XRLabel
        Protected Friend WithEvents XrLabel_group_detail_name As DevExpress.XtraReports.UI.XRLabel
        Protected Friend WithEvents GroupHeader1 As DevExpress.XtraReports.UI.GroupHeaderBand
        Protected Friend WithEvents GroupFooter1 As DevExpress.XtraReports.UI.GroupFooterBand
        Protected Friend WithEvents ReportFooter As DevExpress.XtraReports.UI.ReportFooterBand
        Protected Friend WithEvents XrLabel_total_fairshare_deduct As DevExpress.XtraReports.UI.XRLabel
        Protected Friend WithEvents XrLabel_total_paf_amount As DevExpress.XtraReports.UI.XRLabel
        Protected Friend WithEvents XrLabel_total_fee_amount As DevExpress.XtraReports.UI.XRLabel
        Protected Friend WithEvents XrLabel13 As DevExpress.XtraReports.UI.XRLabel
        Protected Friend WithEvents XrLabel_total As DevExpress.XtraReports.UI.XRLabel
        Protected Friend WithEvents XrLabel_total_fairshare_bill As DevExpress.XtraReports.UI.XRLabel

        'Required by the Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Designer
        'It can be modified using the Designer.
        'Do not modify it using the code editor.
        <System.Diagnostics.DebuggerStepThrough()>
        Private Sub InitializeComponent()
            Dim XrSummary1 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary
            Dim XrSummary2 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary
            Dim XrSummary3 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary
            Dim XrSummary4 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary
            Dim XrSummary5 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary
            Dim XrSummary6 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary
            Dim XrSummary7 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary
            Dim XrSummary8 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary
            Dim XrSummary9 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary
            Dim XrSummary10 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary
            Me.XrPanel1 = New DevExpress.XtraReports.UI.XRPanel
            Me.XrLabel6 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel5 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel4 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel3 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel2 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel1 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_group_fee_amount = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_group_paf_amount = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_group_fairshare_deduct = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_group_fairshare_bill = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_group_subtotal = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_group_detail_name = New DevExpress.XtraReports.UI.XRLabel
            Me.GroupHeader1 = New DevExpress.XtraReports.UI.GroupHeaderBand
            Me.GroupFooter1 = New DevExpress.XtraReports.UI.GroupFooterBand
            Me.ReportFooter = New DevExpress.XtraReports.UI.ReportFooterBand
            Me.XrLabel_total_fairshare_deduct = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_total_paf_amount = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_total_fee_amount = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel13 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_total = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_total_fairshare_bill = New DevExpress.XtraReports.UI.XRLabel
            CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
            '
            'Detail
            '
            Me.Detail.HeightF = 0.0!
            '
            'PageHeader
            '
            Me.PageHeader.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrPanel1})
            Me.PageHeader.HeightF = 162.0!
            Me.PageHeader.Controls.SetChildIndex(Me.XrPanel_AgencyAddress, 0)
            Me.PageHeader.Controls.SetChildIndex(Me.XrLabel_Title, 0)
            Me.PageHeader.Controls.SetChildIndex(Me.XrLine_Title, 0)
            Me.PageHeader.Controls.SetChildIndex(Me.XrLabel_Subtitle, 0)
            Me.PageHeader.Controls.SetChildIndex(Me.XrPageInfo1, 0)
            Me.PageHeader.Controls.SetChildIndex(Me.XrPanel1, 0)
            '
            'XrLabel_Title
            '
            Me.XrLabel_Title.StylePriority.UseFont = False
            '
            'XrLabel_Subtitle
            '
            Me.XrLabel_Subtitle.LocationFloat = New DevExpress.Utils.PointFloat(7.999992!, 85.00001!)
            Me.XrLabel_Subtitle.SizeF = New System.Drawing.SizeF(995.1251!, 17.00001!)
            '
            'XrPageInfo_PageNumber
            '
            Me.XrPageInfo_PageNumber.LocationFloat = New DevExpress.Utils.PointFloat(862.125!, 8.0!)
            Me.XrPageInfo_PageNumber.StylePriority.UseTextAlignment = False
            '
            'XrPanel_AgencyAddress
            '
            Me.XrPanel_AgencyAddress.LocationFloat = New DevExpress.Utils.PointFloat(695.125!, 0.0!)
            '
            'XRLabel_Agency_Name
            '
            Me.XRLabel_Agency_Name.StylePriority.UseTextAlignment = False
            '
            'XrLabel_Agency_Address3
            '
            Me.XrLabel_Agency_Address3.StylePriority.UseFont = False
            Me.XrLabel_Agency_Address3.StylePriority.UseTextAlignment = False
            '
            'XrLabel_Agency_Address1
            '
            Me.XrLabel_Agency_Address1.StylePriority.UseFont = False
            Me.XrLabel_Agency_Address1.StylePriority.UseTextAlignment = False
            '
            'XrLabel_Agency_Phone
            '
            Me.XrLabel_Agency_Phone.StylePriority.UseFont = False
            Me.XrLabel_Agency_Phone.StylePriority.UseTextAlignment = False
            '
            'XrLabel_Agency_Address2
            '
            Me.XrLabel_Agency_Address2.StylePriority.UseFont = False
            Me.XrLabel_Agency_Address2.StylePriority.UseTextAlignment = False
            '
            'XrPanel1
            '
            Me.XrPanel1.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel6, Me.XrLabel5, Me.XrLabel4, Me.XrLabel3, Me.XrLabel2, Me.XrLabel1})
            Me.XrPanel1.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 132.0!)
            Me.XrPanel1.Name = "XrPanel1"
            Me.XrPanel1.SizeF = New System.Drawing.SizeF(1003.125!, 17.0!)
            Me.XrPanel1.StyleName = "XrControlStyle_HeaderPannel"
            '
            'XrLabel6
            '
            Me.XrLabel6.LocationFloat = New DevExpress.Utils.PointFloat(902.1251!, 2.000014!)
            Me.XrLabel6.Name = "XrLabel6"
            Me.XrLabel6.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel6.SizeF = New System.Drawing.SizeF(91.0!, 15.0!)
            Me.XrLabel6.Text = "TOTAL"
            '
            'XrLabel5
            '
            Me.XrLabel5.LocationFloat = New DevExpress.Utils.PointFloat(802.125!, 0.9999911!)
            Me.XrLabel5.Name = "XrLabel5"
            Me.XrLabel5.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel5.SizeF = New System.Drawing.SizeF(91.0!, 15.0!)
            Me.XrLabel5.Text = "FS BILL"
            '
            'XrLabel4
            '
            Me.XrLabel4.LocationFloat = New DevExpress.Utils.PointFloat(602.1251!, 0.9999911!)
            Me.XrLabel4.Name = "XrLabel4"
            Me.XrLabel4.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel4.SizeF = New System.Drawing.SizeF(91.0!, 15.0!)
            Me.XrLabel4.Text = "MONTHLY"
            '
            'XrLabel3
            '
            Me.XrLabel3.LocationFloat = New DevExpress.Utils.PointFloat(702.1251!, 0.9999911!)
            Me.XrLabel3.Name = "XrLabel3"
            Me.XrLabel3.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel3.SizeF = New System.Drawing.SizeF(91.0!, 15.0!)
            Me.XrLabel3.Text = "FS DEDUCT"
            '
            'XrLabel2
            '
            Me.XrLabel2.LocationFloat = New DevExpress.Utils.PointFloat(502.125!, 2.000014!)
            Me.XrLabel2.Name = "XrLabel2"
            Me.XrLabel2.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel2.SizeF = New System.Drawing.SizeF(91.0!, 15.0!)
            Me.XrLabel2.Text = "FEES"
            '
            'XrLabel1
            '
            Me.XrLabel1.LocationFloat = New DevExpress.Utils.PointFloat(7.999992!, 1.0!)
            Me.XrLabel1.Name = "XrLabel1"
            Me.XrLabel1.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel1.SizeF = New System.Drawing.SizeF(392.0!, 14.99998!)
            Me.XrLabel1.StylePriority.UseTextAlignment = False
            Me.XrLabel1.Text = "OFFICE"
            Me.XrLabel1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            '
            'XrLabel_group_fee_amount
            '
            Me.XrLabel_group_fee_amount.LocationFloat = New DevExpress.Utils.PointFloat(493.125!, 0.0!)
            Me.XrLabel_group_fee_amount.Name = "XrLabel_group_fee_amount"
            Me.XrLabel_group_fee_amount.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_group_fee_amount.SizeF = New System.Drawing.SizeF(100.0!, 15.0!)
            Me.XrLabel_group_fee_amount.StylePriority.UseTextAlignment = False
            XrSummary1.FormatString = "{0:c}"
            XrSummary1.Running = DevExpress.XtraReports.UI.SummaryRunning.Group
            Me.XrLabel_group_fee_amount.Summary = XrSummary1
            Me.XrLabel_group_fee_amount.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
            '
            'XrLabel_group_paf_amount
            '
            Me.XrLabel_group_paf_amount.LocationFloat = New DevExpress.Utils.PointFloat(593.1251!, 0.0!)
            Me.XrLabel_group_paf_amount.Name = "XrLabel_group_paf_amount"
            Me.XrLabel_group_paf_amount.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_group_paf_amount.SizeF = New System.Drawing.SizeF(100.0!, 15.0!)
            Me.XrLabel_group_paf_amount.StylePriority.UseTextAlignment = False
            XrSummary2.FormatString = "{0:c}"
            XrSummary2.Running = DevExpress.XtraReports.UI.SummaryRunning.Group
            Me.XrLabel_group_paf_amount.Summary = XrSummary2
            Me.XrLabel_group_paf_amount.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
            '
            'XrLabel_group_fairshare_deduct
            '
            Me.XrLabel_group_fairshare_deduct.LocationFloat = New DevExpress.Utils.PointFloat(693.1251!, 0.0!)
            Me.XrLabel_group_fairshare_deduct.Name = "XrLabel_group_fairshare_deduct"
            Me.XrLabel_group_fairshare_deduct.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_group_fairshare_deduct.SizeF = New System.Drawing.SizeF(100.0!, 15.0!)
            Me.XrLabel_group_fairshare_deduct.StylePriority.UseTextAlignment = False
            XrSummary3.FormatString = "{0:c}"
            XrSummary3.Running = DevExpress.XtraReports.UI.SummaryRunning.Group
            Me.XrLabel_group_fairshare_deduct.Summary = XrSummary3
            Me.XrLabel_group_fairshare_deduct.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
            '
            'XrLabel_group_fairshare_bill
            '
            Me.XrLabel_group_fairshare_bill.LocationFloat = New DevExpress.Utils.PointFloat(793.125!, 0.0!)
            Me.XrLabel_group_fairshare_bill.Name = "XrLabel_group_fairshare_bill"
            Me.XrLabel_group_fairshare_bill.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_group_fairshare_bill.SizeF = New System.Drawing.SizeF(100.0!, 15.0!)
            Me.XrLabel_group_fairshare_bill.StylePriority.UseTextAlignment = False
            XrSummary4.FormatString = "{0:c}"
            XrSummary4.Running = DevExpress.XtraReports.UI.SummaryRunning.Group
            Me.XrLabel_group_fairshare_bill.Summary = XrSummary4
            Me.XrLabel_group_fairshare_bill.Text = " "
            Me.XrLabel_group_fairshare_bill.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
            '
            'XrLabel_group_subtotal
            '
            Me.XrLabel_group_subtotal.LocationFloat = New DevExpress.Utils.PointFloat(893.1251!, 0.0!)
            Me.XrLabel_group_subtotal.Name = "XrLabel_group_subtotal"
            Me.XrLabel_group_subtotal.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_group_subtotal.SizeF = New System.Drawing.SizeF(100.0!, 15.0!)
            Me.XrLabel_group_subtotal.StylePriority.UseTextAlignment = False
            XrSummary5.FormatString = "{0:c}"
            XrSummary5.Running = DevExpress.XtraReports.UI.SummaryRunning.Group
            Me.XrLabel_group_subtotal.Summary = XrSummary5
            Me.XrLabel_group_subtotal.Text = " "
            Me.XrLabel_group_subtotal.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
            '
            'XrLabel_group_detail_name
            '
            Me.XrLabel_group_detail_name.LocationFloat = New DevExpress.Utils.PointFloat(9.999998!, 0.0!)
            Me.XrLabel_group_detail_name.Name = "XrLabel_group_detail_name"
            Me.XrLabel_group_detail_name.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_group_detail_name.SizeF = New System.Drawing.SizeF(483.125!, 14.99999!)
            '
            'GroupHeader1
            '
            Me.GroupHeader1.HeightF = 0.0!
            Me.GroupHeader1.Name = "GroupHeader1"
            '
            'GroupFooter1
            '
            Me.GroupFooter1.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel_group_detail_name, Me.XrLabel_group_paf_amount, Me.XrLabel_group_fairshare_deduct, Me.XrLabel_group_fairshare_bill, Me.XrLabel_group_subtotal, Me.XrLabel_group_fee_amount})
            Me.GroupFooter1.HeightF = 15.0!
            Me.GroupFooter1.Name = "GroupFooter1"
            '
            'ReportFooter
            '
            Me.ReportFooter.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel_total_fairshare_deduct, Me.XrLabel_total_paf_amount, Me.XrLabel_total_fee_amount, Me.XrLabel13, Me.XrLabel_total, Me.XrLabel_total_fairshare_bill})
            Me.ReportFooter.HeightF = 32.0!
            Me.ReportFooter.Name = "ReportFooter"
            Me.ReportFooter.StyleName = "XrControlStyle_Totals"
            '
            'XrLabel_total_fairshare_deduct
            '
            Me.XrLabel_total_fairshare_deduct.LocationFloat = New DevExpress.Utils.PointFloat(693.1251!, 10.00001!)
            Me.XrLabel_total_fairshare_deduct.Name = "XrLabel_total_fairshare_deduct"
            Me.XrLabel_total_fairshare_deduct.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_total_fairshare_deduct.SizeF = New System.Drawing.SizeF(100.0!, 15.0!)
            Me.XrLabel_total_fairshare_deduct.StylePriority.UseTextAlignment = False
            XrSummary6.FormatString = "{0:c}"
            XrSummary6.Running = DevExpress.XtraReports.UI.SummaryRunning.Report
            Me.XrLabel_total_fairshare_deduct.Summary = XrSummary6
            Me.XrLabel_total_fairshare_deduct.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
            '
            'XrLabel_total_paf_amount
            '
            Me.XrLabel_total_paf_amount.LocationFloat = New DevExpress.Utils.PointFloat(593.1251!, 10.00001!)
            Me.XrLabel_total_paf_amount.Name = "XrLabel_total_paf_amount"
            Me.XrLabel_total_paf_amount.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_total_paf_amount.SizeF = New System.Drawing.SizeF(100.0!, 15.0!)
            Me.XrLabel_total_paf_amount.StylePriority.UseTextAlignment = False
            XrSummary7.FormatString = "{0:c}"
            XrSummary7.Running = DevExpress.XtraReports.UI.SummaryRunning.Report
            Me.XrLabel_total_paf_amount.Summary = XrSummary7
            Me.XrLabel_total_paf_amount.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
            '
            'XrLabel_total_fee_amount
            '
            Me.XrLabel_total_fee_amount.LocationFloat = New DevExpress.Utils.PointFloat(493.125!, 10.00001!)
            Me.XrLabel_total_fee_amount.Name = "XrLabel_total_fee_amount"
            Me.XrLabel_total_fee_amount.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_total_fee_amount.SizeF = New System.Drawing.SizeF(100.0!, 15.0!)
            Me.XrLabel_total_fee_amount.StylePriority.UseTextAlignment = False
            XrSummary8.FormatString = "{0:c}"
            XrSummary8.Running = DevExpress.XtraReports.UI.SummaryRunning.Report
            Me.XrLabel_total_fee_amount.Summary = XrSummary8
            Me.XrLabel_total_fee_amount.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
            '
            'XrLabel13
            '
            Me.XrLabel13.LocationFloat = New DevExpress.Utils.PointFloat(7.999992!, 10.00001!)
            Me.XrLabel13.Name = "XrLabel13"
            Me.XrLabel13.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel13.SizeF = New System.Drawing.SizeF(392.0!, 15.0!)
            Me.XrLabel13.StylePriority.UseTextAlignment = False
            Me.XrLabel13.Text = "TOTALS"
            Me.XrLabel13.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
            '
            'XrLabel_total
            '
            Me.XrLabel_total.LocationFloat = New DevExpress.Utils.PointFloat(893.1251!, 10.00001!)
            Me.XrLabel_total.Name = "XrLabel_total"
            Me.XrLabel_total.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_total.SizeF = New System.Drawing.SizeF(100.0!, 15.0!)
            Me.XrLabel_total.StylePriority.UseTextAlignment = False
            XrSummary9.FormatString = "{0:c}"
            XrSummary9.Running = DevExpress.XtraReports.UI.SummaryRunning.Report
            Me.XrLabel_total.Summary = XrSummary9
            Me.XrLabel_total.Text = " "
            Me.XrLabel_total.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
            '
            'XrLabel_total_fairshare_bill
            '
            Me.XrLabel_total_fairshare_bill.LocationFloat = New DevExpress.Utils.PointFloat(793.125!, 10.00001!)
            Me.XrLabel_total_fairshare_bill.Name = "XrLabel_total_fairshare_bill"
            Me.XrLabel_total_fairshare_bill.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_total_fairshare_bill.SizeF = New System.Drawing.SizeF(100.0!, 15.0!)
            Me.XrLabel_total_fairshare_bill.StylePriority.UseTextAlignment = False
            XrSummary10.FormatString = "{0:c}"
            XrSummary10.Running = DevExpress.XtraReports.UI.SummaryRunning.Report
            Me.XrLabel_total_fairshare_bill.Summary = XrSummary10
            Me.XrLabel_total_fairshare_bill.Text = " "
            Me.XrLabel_total_fairshare_bill.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
            '
            'ClientByZipcodeReport
            '
            Me.Bands.AddRange(New DevExpress.XtraReports.UI.Band() {Me.Detail, Me.PageHeader, Me.PageFooter, Me.GroupHeader1, Me.GroupFooter1, Me.ReportFooter, Me.TopMarginBand1, Me.BottomMarginBand1})
            Me.Landscape = True
            Me.PageHeight = 850
            Me.PageWidth = 1100
            Me.StyleSheet.AddRange(New DevExpress.XtraReports.UI.XRControlStyle() {Me.XrControlStyle_Header, Me.XrControlStyle_Totals, Me.XrControlStyle_HeaderPannel, Me.XrControlStyle_GroupHeader})
            Me.Version = "10.1"
            Me.Controls.SetChildIndex(Me.BottomMarginBand1, 0)
            Me.Controls.SetChildIndex(Me.TopMarginBand1, 0)
            Me.Controls.SetChildIndex(Me.ReportFooter, 0)
            Me.Controls.SetChildIndex(Me.GroupFooter1, 0)
            Me.Controls.SetChildIndex(Me.GroupHeader1, 0)
            Me.Controls.SetChildIndex(Me.PageFooter, 0)
            Me.Controls.SetChildIndex(Me.PageHeader, 0)
            Me.Controls.SetChildIndex(Me.Detail, 0)
            CType(Me, System.ComponentModel.ISupportInitialize).EndInit()
        End Sub

#End Region

        Protected ds As New DataSet("ds")

        Protected Overridable Sub SetReportDataSource()
            Const TableName As String = "rpt_client_contributions_zipcode"

            Dim tbl As DataTable = ds.Tables(TableName)
            If tbl IsNot Nothing Then
                tbl.Clear()
                tbl = Nothing
            End If

            Dim cn As SqlConnection = New System.Data.SqlClient.SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
            Try
                cn.Open()
                Using cmd As SqlCommand = New System.Data.SqlClient.SqlCommand()
                    With cmd
                        .Connection = cn
                        .CommandText = TableName
                        .CommandType = CommandType.StoredProcedure

                        SqlCommandBuilder.DeriveParameters(cmd)
                        .Parameters(1).Value = Parameter_FromDate
                        .Parameters(2).Value = Parameter_ToDate
                        .CommandTimeout = 0

                        Using da As New SqlDataAdapter(cmd)
                            da.Fill(ds, TableName)
                            tbl = ds.Tables(TableName)

                            With tbl
                                If Not .Columns.Contains("total") Then
                                    .Columns.Add("total", GetType(Decimal), "[fee_amount]+[paf_amount]+[fairshare_deduct]+[fairshare_bill]")
                                End If
                            End With
                        End Using
                    End With
                End Using

            Catch ex As SqlException
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error reading report transactions")

            Finally
                If cn IsNot Nothing Then cn.Dispose()
            End Try

            If tbl IsNot Nothing Then

                ' Bind the office name to the detail field on the main report
                XrLabel_group_detail_name.DataBindings.Clear()
                XrLabel_group_detail_name.DataBindings.Add("Text", Nothing, "office_name")
                XrLabel_group_detail_name.DataBindings.Add("Tag", Nothing, "office")

                XrLabel_group_fairshare_bill.DataBindings.Clear()
                XrLabel_group_fairshare_bill.DataBindings.Add("Text", Nothing, "fairshare_bill")
                XrLabel_group_fairshare_bill.DataBindings.Add("Tag", Nothing, "office")

                XrLabel_group_fairshare_deduct.DataBindings.Clear()
                XrLabel_group_fairshare_deduct.DataBindings.Add("Text", Nothing, "fairshare_deduct")
                XrLabel_group_fairshare_deduct.DataBindings.Add("Tag", Nothing, "office")

                XrLabel_group_fee_amount.DataBindings.Clear()
                XrLabel_group_fee_amount.DataBindings.Add("Text", Nothing, "fee_amount")
                XrLabel_group_fee_amount.DataBindings.Add("Tag", Nothing, "office")

                XrLabel_group_paf_amount.DataBindings.Clear()
                XrLabel_group_paf_amount.DataBindings.Add("Text", Nothing, "paf_amount")
                XrLabel_group_paf_amount.DataBindings.Add("Tag", Nothing, "office")

                XrLabel_group_subtotal.DataBindings.Clear()
                XrLabel_group_subtotal.DataBindings.Add("Text", Nothing, "total")
                XrLabel_group_subtotal.DataBindings.Add("Tag", Nothing, "office")

                ' Set the grouing field to the office
                GroupHeader1.GroupFields.Clear()
                GroupHeader1.GroupFields.Add(New DevExpress.XtraReports.UI.GroupField("office"))

                ' Bind the report source
                DataSource = New DataView(tbl, String.Empty, "office", DataViewRowState.CurrentRows)
            End If
        End Sub

        Protected Sub Report_BeforePrint(ByVal sender As Object, ByVal e As PrintEventArgs)
            SetReportDataSource()

            For Each calc As DevExpress.XtraReports.UI.CalculatedField In CalculatedFields
                calc.Assign(DataSource, DataMember)
            Next
        End Sub

        Protected Overridable Sub Field_PreviewClick(ByVal sender As Object, ByVal e As DevExpress.XtraReports.UI.PreviewMouseEventArgs)
            Dim Office As Int32 = -1
            If e.Brick.Value IsNot Nothing AndAlso e.Brick.Value IsNot DBNull.Value Then
                Office = Convert.ToInt32(e.Brick.Value)
            End If

            If Office > 0 Then
                Dim NewVue As New DataView(CType(DataSource, DataView).Table, String.Format("[office]={0:f0}", Office), "client", DataViewRowState.CurrentRows)
                If NewVue.Count > 0 Then
                    Dim thrd As New Thread(New ParameterizedThreadStart(AddressOf PreviewThread))
                    With thrd
                        .SetApartmentState(ApartmentState.STA)
                        .IsBackground = True
                        .Start(NewVue)
                    End With
                End If
            End If
        End Sub

        Private Sub PreviewThread(ByVal obj As Object)
            Dim vue As DataView = CType(obj, DataView)
            Using rpt As New DetailReport(vue)
                rpt.SetReportSubtitle(String.Format("{0} and office '{1}'", ReportSubTitle, DebtPlus.Utils.Nulls.DStr(vue(0)("office_name"))))
                rpt.Parameters.Clear()
                CType(rpt, IReports).DisplayPreviewDialog()
            End Using
        End Sub
    End Class
End Namespace
