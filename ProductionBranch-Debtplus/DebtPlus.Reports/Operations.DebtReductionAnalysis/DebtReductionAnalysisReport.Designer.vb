Namespace Operations.DebtReductionAnalysis
    <Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
    Partial Class DebtReductionAnalysisReport

        'XtraReport overrides dispose to clean up the component list.
        <System.Diagnostics.DebuggerNonUserCode()> _
        Protected Overrides Sub Dispose(ByVal disposing As Boolean)
            If disposing Then
                If components IsNot Nothing Then
                    components.Dispose()
                End If
                ds.Dispose()
            End If
            MyBase.Dispose(disposing)
        End Sub

        'Required by the Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Designer
        'It can be modified using the Designer.  
        'Do not modify it using the code editor.
        <System.Diagnostics.DebuggerStepThrough()> _
        Private Sub InitializeComponent()
            Me.GroupHeader1 = New DevExpress.XtraReports.UI.GroupHeaderBand
            Me.XrLabel_office = New DevExpress.XtraReports.UI.XRLabel
            Me.ReportFooter = New DevExpress.XtraReports.UI.ReportFooterBand
            Me.XrLine1 = New DevExpress.XtraReports.UI.XRLine
            Me.XrTable1 = New DevExpress.XtraReports.UI.XRTable
            Me.XrTableRow1 = New DevExpress.XtraReports.UI.XRTableRow
            Me.XrTableCell1 = New DevExpress.XtraReports.UI.XRTableCell
            Me.XrTableCell2 = New DevExpress.XtraReports.UI.XRTableCell
            Me.XrTableCell3 = New DevExpress.XtraReports.UI.XRTableCell
            Me.XrTableRow4 = New DevExpress.XtraReports.UI.XRTableRow
            Me.XrTableCell10 = New DevExpress.XtraReports.UI.XRTableCell
            Me.XrTableCell_active_orig_balance = New DevExpress.XtraReports.UI.XRTableCell
            Me.XrTableCell_all_orig_balance = New DevExpress.XtraReports.UI.XRTableCell
            Me.XrTableRow3 = New DevExpress.XtraReports.UI.XRTableRow
            Me.XrTableCell7 = New DevExpress.XtraReports.UI.XRTableCell
            Me.XrTableCell_active_payments = New DevExpress.XtraReports.UI.XRTableCell
            Me.XrTableCell_all_payments = New DevExpress.XtraReports.UI.XRTableCell
            Me.XrTableRow2 = New DevExpress.XtraReports.UI.XRTableRow
            Me.XrTableCell4 = New DevExpress.XtraReports.UI.XRTableCell
            Me.XrTableCell_active_current_balance = New DevExpress.XtraReports.UI.XRTableCell
            Me.XrTableCell_all_current_balance = New DevExpress.XtraReports.UI.XRTableCell
            Me.XrPanel1 = New DevExpress.XtraReports.UI.XRPanel
            Me.XrLabel9 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel8 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel7 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel6 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel5 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel4 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel3 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel2 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel1 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_percentage = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_client_name = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_start_date = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_active_status = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_drop_date = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_orig_balance_adjustment = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_total_payments = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_current_balance = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_client = New DevExpress.XtraReports.UI.XRLabel
            CType(Me.XrTable1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
            '
            'Detail
            '
            Me.Detail.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel_total_payments, Me.XrLabel_orig_balance_adjustment, Me.XrLabel_client, Me.XrLabel_current_balance, Me.XrLabel_drop_date, Me.XrLabel_client_name, Me.XrLabel_percentage, Me.XrLabel_active_status, Me.XrLabel_start_date})
            Me.Detail.HeightF = 15.0!
            '
            'PageHeader
            '
            Me.PageHeader.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrPanel1})
            Me.PageHeader.HeightF = 147.0!
            Me.PageHeader.Controls.SetChildIndex(Me.XrPanel_AgencyAddress, 0)
            Me.PageHeader.Controls.SetChildIndex(Me.XrLabel_Title, 0)
            Me.PageHeader.Controls.SetChildIndex(Me.XrLine_Title, 0)
            Me.PageHeader.Controls.SetChildIndex(Me.XrLabel_Subtitle, 0)
            Me.PageHeader.Controls.SetChildIndex(Me.XrPageInfo1, 0)
            Me.PageHeader.Controls.SetChildIndex(Me.XrPanel1, 0)
            '
            'XrLabel_Title
            '
            Me.XrLabel_Title.StylePriority.UseFont = False
            '
            'XrPageInfo_PageNumber
            '
            Me.XrPageInfo_PageNumber.StylePriority.UseTextAlignment = False
            '
            'XRLabel_Agency_Name
            '
            Me.XRLabel_Agency_Name.StylePriority.UseTextAlignment = False
            '
            'XrLabel_Agency_Address3
            '
            Me.XrLabel_Agency_Address3.StylePriority.UseFont = False
            Me.XrLabel_Agency_Address3.StylePriority.UseTextAlignment = False
            '
            'XrLabel_Agency_Address1
            '
            Me.XrLabel_Agency_Address1.StylePriority.UseFont = False
            Me.XrLabel_Agency_Address1.StylePriority.UseTextAlignment = False
            '
            'XrLabel_Agency_Phone
            '
            Me.XrLabel_Agency_Phone.StylePriority.UseFont = False
            Me.XrLabel_Agency_Phone.StylePriority.UseTextAlignment = False
            '
            'XrLabel_Agency_Address2
            '
            Me.XrLabel_Agency_Address2.StylePriority.UseFont = False
            Me.XrLabel_Agency_Address2.StylePriority.UseTextAlignment = False
            '
            'GroupHeader1
            '
            Me.GroupHeader1.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel_office})
            Me.GroupHeader1.GroupUnion = DevExpress.XtraReports.UI.GroupUnion.WithFirstDetail
            Me.GroupHeader1.HeightF = 35.0!
            Me.GroupHeader1.Name = "GroupHeader1"
            Me.GroupHeader1.RepeatEveryPage = True
            Me.GroupHeader1.StyleName = "XrControlStyle_GroupHeader"
            '
            'XrLabel_office
            '
            Me.XrLabel_office.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 6.00001!)
            Me.XrLabel_office.Name = "XrLabel_office"
            Me.XrLabel_office.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_office.SizeF = New System.Drawing.SizeF(799.0!, 22.99999!)
            Me.XrLabel_office.Text = "XrLabel_office"
            '
            'ReportFooter
            '
            Me.ReportFooter.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLine1, Me.XrTable1})
            Me.ReportFooter.Name = "ReportFooter"
            Me.ReportFooter.StyleName = "XrControlStyle_Totals"
            '
            'XrLine1
            '
            Me.XrLine1.ForeColor = System.Drawing.Color.Teal
            Me.XrLine1.LineWidth = 2
            Me.XrLine1.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
            Me.XrLine1.Name = "XrLine1"
            Me.XrLine1.SizeF = New System.Drawing.SizeF(799.0!, 22.99998!)
            Me.XrLine1.StylePriority.UseForeColor = False
            '
            'XrTable1
            '
            Me.XrTable1.LocationFloat = New DevExpress.Utils.PointFloat(205.5833!, 22.99998!)
            Me.XrTable1.Name = "XrTable1"
            Me.XrTable1.Padding = New DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 100.0!)
            Me.XrTable1.Rows.AddRange(New DevExpress.XtraReports.UI.XRTableRow() {Me.XrTableRow1, Me.XrTableRow4, Me.XrTableRow3, Me.XrTableRow2})
            Me.XrTable1.SizeF = New System.Drawing.SizeF(535.4167!, 70.0!)
            Me.XrTable1.StylePriority.UsePadding = False
            '
            'XrTableRow1
            '
            Me.XrTableRow1.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.XrTableCell1, Me.XrTableCell2, Me.XrTableCell3})
            Me.XrTableRow1.Name = "XrTableRow1"
            Me.XrTableRow1.Weight = 0.7093596249080889
            '
            'XrTableCell1
            '
            Me.XrTableCell1.Name = "XrTableCell1"
            Me.XrTableCell1.StylePriority.UseTextAlignment = False
            Me.XrTableCell1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
            Me.XrTableCell1.Weight = 1.218209561085001
            '
            'XrTableCell2
            '
            Me.XrTableCell2.Name = "XrTableCell2"
            Me.XrTableCell2.Text = "ACTIVE CLIENTS ONLY"
            Me.XrTableCell2.Weight = 0.98614825151260765
            '
            'XrTableCell3
            '
            Me.XrTableCell3.Name = "XrTableCell3"
            Me.XrTableCell3.Text = "ALL CLIENTS"
            Me.XrTableCell3.Weight = 0.79564218740239134
            '
            'XrTableRow4
            '
            Me.XrTableRow4.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.XrTableCell10, Me.XrTableCell_active_orig_balance, Me.XrTableCell_all_orig_balance})
            Me.XrTableRow4.Name = "XrTableRow4"
            Me.XrTableRow4.Weight = 0.42561578573977454
            '
            'XrTableCell10
            '
            Me.XrTableCell10.Name = "XrTableCell10"
            Me.XrTableCell10.StylePriority.UseTextAlignment = False
            Me.XrTableCell10.Text = "Total of origional Balances"
            Me.XrTableCell10.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
            Me.XrTableCell10.Weight = 1.2182095610850008
            '
            'XrTableCell_active_orig_balance
            '
            Me.XrTableCell_active_orig_balance.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.XrTableCell_active_orig_balance.Name = "XrTableCell_active_orig_balance"
            Me.XrTableCell_active_orig_balance.StylePriority.UseFont = False
            Me.XrTableCell_active_orig_balance.Weight = 0.98614825151260765
            '
            'XrTableCell_all_orig_balance
            '
            Me.XrTableCell_all_orig_balance.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.XrTableCell_all_orig_balance.Name = "XrTableCell_all_orig_balance"
            Me.XrTableCell_all_orig_balance.StylePriority.UseFont = False
            Me.XrTableCell_all_orig_balance.Weight = 0.79564218740239134
            '
            'XrTableRow3
            '
            Me.XrTableRow3.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.XrTableCell7, Me.XrTableCell_active_payments, Me.XrTableCell_all_payments})
            Me.XrTableRow3.Name = "XrTableRow3"
            Me.XrTableRow3.Weight = 0.42561581620034294
            '
            'XrTableCell7
            '
            Me.XrTableCell7.Name = "XrTableCell7"
            Me.XrTableCell7.StylePriority.UseTextAlignment = False
            Me.XrTableCell7.Text = "Total of Payments"
            Me.XrTableCell7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
            Me.XrTableCell7.Weight = 1.2182097320784089
            '
            'XrTableCell_active_payments
            '
            Me.XrTableCell_active_payments.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.XrTableCell_active_payments.Name = "XrTableCell_active_payments"
            Me.XrTableCell_active_payments.StylePriority.UseFont = False
            Me.XrTableCell_active_payments.Weight = 0.98614808051919978
            '
            'XrTableCell_all_payments
            '
            Me.XrTableCell_all_payments.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.XrTableCell_all_payments.Name = "XrTableCell_all_payments"
            Me.XrTableCell_all_payments.StylePriority.UseFont = False
            Me.XrTableCell_all_payments.Weight = 0.79564218740239134
            '
            'XrTableRow2
            '
            Me.XrTableRow2.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.XrTableCell4, Me.XrTableCell_active_current_balance, Me.XrTableCell_all_current_balance})
            Me.XrTableRow2.Name = "XrTableRow2"
            Me.XrTableRow2.Weight = 0.42561578914041981
            '
            'XrTableCell4
            '
            Me.XrTableCell4.Name = "XrTableCell4"
            Me.XrTableCell4.StylePriority.UseTextAlignment = False
            Me.XrTableCell4.Text = "Total of Current Balances"
            Me.XrTableCell4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
            Me.XrTableCell4.Weight = 1.2182095610850008
            '
            'XrTableCell_active_current_balance
            '
            Me.XrTableCell_active_current_balance.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.XrTableCell_active_current_balance.Name = "XrTableCell_active_current_balance"
            Me.XrTableCell_active_current_balance.StylePriority.UseFont = False
            Me.XrTableCell_active_current_balance.Weight = 0.98614825151260765
            '
            'XrTableCell_all_current_balance
            '
            Me.XrTableCell_all_current_balance.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.XrTableCell_all_current_balance.Name = "XrTableCell_all_current_balance"
            Me.XrTableCell_all_current_balance.StylePriority.UseFont = False
            Me.XrTableCell_all_current_balance.Weight = 0.79564218740239134
            '
            'XrPanel1
            '
            Me.XrPanel1.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel9, Me.XrLabel8, Me.XrLabel7, Me.XrLabel6, Me.XrLabel5, Me.XrLabel4, Me.XrLabel3, Me.XrLabel2, Me.XrLabel1})
            Me.XrPanel1.LocationFloat = New DevExpress.Utils.PointFloat(8.0!, 119.0!)
            Me.XrPanel1.Name = "XrPanel1"
            Me.XrPanel1.SizeF = New System.Drawing.SizeF(792.0!, 17.0!)
            Me.XrPanel1.StyleName = "XrControlStyle_HeaderPannel"
            '
            'XrLabel9
            '
            Me.XrLabel9.LocationFloat = New DevExpress.Utils.PointFloat(66.0!, 1.0!)
            Me.XrLabel9.Name = "XrLabel9"
            Me.XrLabel9.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
            Me.XrLabel9.SizeF = New System.Drawing.SizeF(190.0!, 15.0!)
            Me.XrLabel9.StylePriority.UsePadding = False
            Me.XrLabel9.StylePriority.UseTextAlignment = False
            Me.XrLabel9.Text = "NAME"
            Me.XrLabel9.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            '
            'XrLabel8
            '
            Me.XrLabel8.LocationFloat = New DevExpress.Utils.PointFloat(533.0!, 1.0!)
            Me.XrLabel8.Name = "XrLabel8"
            Me.XrLabel8.SizeF = New System.Drawing.SizeF(91.0!, 15.0!)
            Me.XrLabel8.Text = "PAYMENTS"
            '
            'XrLabel7
            '
            Me.XrLabel7.LocationFloat = New DevExpress.Utils.PointFloat(450.0!, 1.0!)
            Me.XrLabel7.Name = "XrLabel7"
            Me.XrLabel7.SizeF = New System.Drawing.SizeF(83.0!, 15.0!)
            Me.XrLabel7.Text = "ADJ ORIG"
            '
            'XrLabel6
            '
            Me.XrLabel6.LocationFloat = New DevExpress.Utils.PointFloat(733.0!, 1.0!)
            Me.XrLabel6.Name = "XrLabel6"
            Me.XrLabel6.SizeF = New System.Drawing.SizeF(58.0!, 15.0!)
            Me.XrLabel6.Text = "PCT"
            '
            'XrLabel5
            '
            Me.XrLabel5.LocationFloat = New DevExpress.Utils.PointFloat(633.0!, 1.0!)
            Me.XrLabel5.Name = "XrLabel5"
            Me.XrLabel5.SizeF = New System.Drawing.SizeF(100.0!, 15.0!)
            Me.XrLabel5.Text = "BALANCE"
            '
            'XrLabel4
            '
            Me.XrLabel4.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 1.0!)
            Me.XrLabel4.Name = "XrLabel4"
            Me.XrLabel4.SizeF = New System.Drawing.SizeF(58.0!, 15.0!)
            Me.XrLabel4.Text = "CLIENT"
            '
            'XrLabel3
            '
            Me.XrLabel3.LocationFloat = New DevExpress.Utils.PointFloat(333.0!, 1.0!)
            Me.XrLabel3.Name = "XrLabel3"
            Me.XrLabel3.SizeF = New System.Drawing.SizeF(33.0!, 15.0!)
            Me.XrLabel3.Text = "STS"
            '
            'XrLabel2
            '
            Me.XrLabel2.LocationFloat = New DevExpress.Utils.PointFloat(366.0!, 1.0!)
            Me.XrLabel2.Name = "XrLabel2"
            Me.XrLabel2.SizeF = New System.Drawing.SizeF(75.0!, 15.0!)
            Me.XrLabel2.Text = "DROPPED"
            '
            'XrLabel1
            '
            Me.XrLabel1.LocationFloat = New DevExpress.Utils.PointFloat(258.0!, 1.0!)
            Me.XrLabel1.Name = "XrLabel1"
            Me.XrLabel1.SizeF = New System.Drawing.SizeF(75.0!, 15.0!)
            Me.XrLabel1.Text = "START DT"
            '
            'XrLabel_percentage
            '
            Me.XrLabel_percentage.LocationFloat = New DevExpress.Utils.PointFloat(741.0!, 0.0!)
            Me.XrLabel_percentage.Name = "XrLabel_percentage"
            Me.XrLabel_percentage.SizeF = New System.Drawing.SizeF(58.0!, 14.99999!)
            Me.XrLabel_percentage.StylePriority.UseTextAlignment = False
            Me.XrLabel_percentage.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
            '
            'XrLabel_client_name
            '
            Me.XrLabel_client_name.LocationFloat = New DevExpress.Utils.PointFloat(74.0!, 0.0!)
            Me.XrLabel_client_name.Name = "XrLabel_client_name"
            Me.XrLabel_client_name.SizeF = New System.Drawing.SizeF(190.0!, 15.0!)
            Me.XrLabel_client_name.StylePriority.UseTextAlignment = False
            Me.XrLabel_client_name.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            '
            'XrLabel_start_date
            '
            Me.XrLabel_start_date.LocationFloat = New DevExpress.Utils.PointFloat(266.0!, 0.0!)
            Me.XrLabel_start_date.Name = "XrLabel_start_date"
            Me.XrLabel_start_date.SizeF = New System.Drawing.SizeF(75.0!, 15.0!)
            Me.XrLabel_start_date.StylePriority.UseTextAlignment = False
            Me.XrLabel_start_date.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
            '
            'XrLabel_active_status
            '
            Me.XrLabel_active_status.LocationFloat = New DevExpress.Utils.PointFloat(341.0!, 0.0!)
            Me.XrLabel_active_status.Name = "XrLabel_active_status"
            Me.XrLabel_active_status.SizeF = New System.Drawing.SizeF(33.0!, 15.0!)
            Me.XrLabel_active_status.StylePriority.UseTextAlignment = False
            Me.XrLabel_active_status.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
            '
            'XrLabel_drop_date
            '
            Me.XrLabel_drop_date.LocationFloat = New DevExpress.Utils.PointFloat(374.0!, 0.0!)
            Me.XrLabel_drop_date.Name = "XrLabel_drop_date"
            Me.XrLabel_drop_date.SizeF = New System.Drawing.SizeF(75.0!, 15.0!)
            Me.XrLabel_drop_date.StylePriority.UseTextAlignment = False
            Me.XrLabel_drop_date.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
            '
            'XrLabel_orig_balance_adjustment
            '
            Me.XrLabel_orig_balance_adjustment.LocationFloat = New DevExpress.Utils.PointFloat(458.0!, 0.0!)
            Me.XrLabel_orig_balance_adjustment.Name = "XrLabel_orig_balance_adjustment"
            Me.XrLabel_orig_balance_adjustment.SizeF = New System.Drawing.SizeF(83.0!, 15.0!)
            Me.XrLabel_orig_balance_adjustment.StylePriority.UseTextAlignment = False
            Me.XrLabel_orig_balance_adjustment.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
            '
            'XrLabel_total_payments
            '
            Me.XrLabel_total_payments.LocationFloat = New DevExpress.Utils.PointFloat(541.0!, 0.0!)
            Me.XrLabel_total_payments.Name = "XrLabel_total_payments"
            Me.XrLabel_total_payments.SizeF = New System.Drawing.SizeF(91.0!, 15.0!)
            Me.XrLabel_total_payments.StylePriority.UseTextAlignment = False
            Me.XrLabel_total_payments.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
            '
            'XrLabel_current_balance
            '
            Me.XrLabel_current_balance.LocationFloat = New DevExpress.Utils.PointFloat(641.0!, 0.0!)
            Me.XrLabel_current_balance.Name = "XrLabel_current_balance"
            Me.XrLabel_current_balance.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_current_balance.SizeF = New System.Drawing.SizeF(100.0!, 15.0!)
            Me.XrLabel_current_balance.StylePriority.UseTextAlignment = False
            Me.XrLabel_current_balance.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
            '
            'XrLabel_client
            '
            Me.XrLabel_client.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
            Me.XrLabel_client.Name = "XrLabel_client"
            Me.XrLabel_client.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_client.SizeF = New System.Drawing.SizeF(66.0!, 14.99999!)
            Me.XrLabel_client.StylePriority.UseTextAlignment = False
            Me.XrLabel_client.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
            '
            'DebtReductionAnalysisReport
            '
            Me.Bands.AddRange(New DevExpress.XtraReports.UI.Band() {Me.Detail, Me.PageHeader, Me.PageFooter, Me.TopMarginBand1, Me.BottomMarginBand1, Me.GroupHeader1, Me.ReportFooter})
            Me.ScriptLanguage = DevExpress.XtraReports.ScriptLanguage.VisualBasic
            Me.StyleSheet.AddRange(New DevExpress.XtraReports.UI.XRControlStyle() {Me.XrControlStyle_Header, Me.XrControlStyle_Totals, Me.XrControlStyle_HeaderPannel, Me.XrControlStyle_GroupHeader})
            Me.Version = "10.1"
            Me.Controls.SetChildIndex(Me.ReportFooter, 0)
            Me.Controls.SetChildIndex(Me.GroupHeader1, 0)
            Me.Controls.SetChildIndex(Me.BottomMarginBand1, 0)
            Me.Controls.SetChildIndex(Me.TopMarginBand1, 0)
            Me.Controls.SetChildIndex(Me.PageFooter, 0)
            Me.Controls.SetChildIndex(Me.PageHeader, 0)
            Me.Controls.SetChildIndex(Me.Detail, 0)
            CType(Me.XrTable1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me, System.ComponentModel.ISupportInitialize).EndInit()

        End Sub
        Friend WithEvents XrPanel1 As DevExpress.XtraReports.UI.XRPanel
        Friend WithEvents XrLabel1 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents GroupHeader1 As DevExpress.XtraReports.UI.GroupHeaderBand
        Friend WithEvents XrLabel_office As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents ReportFooter As DevExpress.XtraReports.UI.ReportFooterBand
        Friend WithEvents XrLabel9 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel8 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel7 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel6 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel5 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel4 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel3 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel2 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_total_payments As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_orig_balance_adjustment As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_client As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_current_balance As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_drop_date As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_client_name As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_percentage As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_active_status As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_start_date As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrTable1 As DevExpress.XtraReports.UI.XRTable
        Friend WithEvents XrTableRow1 As DevExpress.XtraReports.UI.XRTableRow
        Friend WithEvents XrTableCell1 As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents XrTableCell2 As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents XrTableCell3 As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents XrTableRow4 As DevExpress.XtraReports.UI.XRTableRow
        Friend WithEvents XrTableCell10 As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents XrTableCell_active_orig_balance As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents XrTableCell_all_orig_balance As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents XrTableRow3 As DevExpress.XtraReports.UI.XRTableRow
        Friend WithEvents XrTableCell7 As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents XrTableCell_active_payments As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents XrTableCell_all_payments As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents XrTableRow2 As DevExpress.XtraReports.UI.XRTableRow
        Friend WithEvents XrTableCell4 As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents XrTableCell_active_current_balance As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents XrTableCell_all_current_balance As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents XrLine1 As DevExpress.XtraReports.UI.XRLine
    End Class
End Namespace
