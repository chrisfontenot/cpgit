#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}

#End Region

Imports System.ComponentModel
Imports DebtPlus.Reports.Template

Imports System.Drawing.Printing
Imports DevExpress.XtraReports.UI
Imports System.Data.SqlClient

Namespace Operations.DebtReductionAnalysis
    Public Class DebtReductionAnalysisReport
        Inherits DatedTemplateXtraReportClass

        Public Sub New()
            MyBase.New()
            InitializeComponent()
            AddHandler Me.BeforePrint, AddressOf Report_BeforePrint

            XrLabel_active_status.DataBindings.Add("Text", Nothing, "active_status")
            XrLabel_client_name.DataBindings.Add("Text", Nothing, "client_name")
            XrLabel_current_balance.DataBindings.Add("Text", Nothing, "current_balance", "{0:c}")
            XrLabel_drop_date.DataBindings.Add("Text", Nothing, "drop_date", "{0:d}")
            XrLabel_office.DataBindings.Add("Text", Nothing, "office")
            XrLabel_orig_balance_adjustment.DataBindings.Add("Text", Nothing, "orig_balance_adjustment", "{0:c}")
            XrLabel_percentage.DataBindings.Add("Text", Nothing, "percentage", "{0:p0}")
            XrLabel_start_date.DataBindings.Add("Text", Nothing, "start_date", "{0:d}")
            XrLabel_total_payments.DataBindings.Add("Text", Nothing, "total_payments", "{0:c}")

            GroupHeader1.GroupFields.Add(New GroupField("office"))

            AddHandler XrLabel_client.BeforePrint, AddressOf XrLabel_client_BeforePrint
            AddHandler XrTable1.BeforePrint, AddressOf XrTable1_BeforePrint
        End Sub

        Public Overrides ReadOnly Property ReportTitle() As String
            Get
                Return "Debt Reduction Analysis"
            End Get
        End Property

        Private ds As New DataSet("ds")

        Private Sub Report_BeforePrint(ByVal sender As Object, ByVal e As PrintEventArgs)
            Const TableName As String = "rpt_debt_reduction_analysis"

            Dim tbl As DataTable = ds.Tables(TableName)
            If tbl IsNot Nothing Then
                tbl.Clear()
                tbl = Nothing
            End If

            Dim cn As SqlConnection = New System.Data.SqlClient.SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
            Try
                cn.Open()
                Using cmd As SqlCommand = New System.Data.SqlClient.SqlCommand()
                    With cmd
                        .Connection = cn
                        .CommandText = TableName
                        .CommandType = CommandType.StoredProcedure
                        SqlCommandBuilder.DeriveParameters(cmd)

                        .CommandTimeout = 0
                        .Parameters(1).Value = Parameter_FromDate
                        .Parameters(2).Value = Parameter_ToDate
                    End With

                    Using da As New SqlDataAdapter(cmd)
                        da.Fill(ds, TableName)
                        tbl = ds.Tables(TableName)

                        With tbl
                            If Not .Columns.Contains("percentage") Then
                                .Columns.Add("percentage", GetType(Double), "iif([orig_balance_adjustment]>0,([orig_balance_adjustment]-[current_balance])/[orig_balance_adjustment],0)")
                            End If
                        End With
                    End Using
                End Using

            Catch ex As Exception
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error reading report information")

            Finally
                If cn IsNot Nothing Then cn.Dispose()
            End Try

            If tbl IsNot Nothing Then
                DataSource = New DataView(tbl, String.Empty, "office, client", DataViewRowState.CurrentRows)
            End If
        End Sub

        Private Sub XrLabel_client_BeforePrint(ByVal sender As Object, ByVal e As PrintEventArgs)
            With CType(sender, XRLabel)
                .Text = String.Format("{0:0000000}", GetCurrentColumnValue("client"))
            End With
        End Sub

        Private Sub XrTable1_BeforePrint(ByVal sender As Object, ByVal e As PrintEventArgs)
            With CType(sender, XRTable)
                Dim tbl As DataTable = CType(.Report.DataSource, DataView).Table

                ' Active current balance
                With CType(.FindControl("XrTableCell_active_current_balance", True), XRTableCell)
                    Dim Value As Object = tbl.Compute("sum(current_balance)", "[active_status]='A' OR [active_status]='AR'")
                    If Value Is Nothing OrElse Value Is DBNull.Value Then Value = 0D
                    .Text = String.Format("{0:c}", Value)
                End With

                ' Active origional balance
                With CType(.FindControl("XrTableCell_active_orig_balance", True), XRTableCell)
                    Dim Value As Object = tbl.Compute("sum(orig_balance)", "[active_status]='A' OR [active_status]='AR'")
                    If Value Is Nothing OrElse Value Is DBNull.Value Then Value = 0D
                    .Text = String.Format("{0:c}", Value)
                End With

                ' Active payments
                With CType(.FindControl("XrTableCell_active_payments", True), XRTableCell)
                    Dim Value As Object = tbl.Compute("sum(total_payments)", "[active_status]='A' OR [active_status]='AR'")
                    If Value Is Nothing OrElse Value Is DBNull.Value Then Value = 0D
                    .Text = String.Format("{0:c}", Value)
                End With

                ' current balance
                With CType(.FindControl("XrTableCell_all_current_balance", True), XRTableCell)
                    Dim Value As Object = tbl.Compute("sum(current_balance)", String.Empty)
                    If Value Is Nothing OrElse Value Is DBNull.Value Then Value = 0D
                    .Text = String.Format("{0:c}", Value)
                End With

                ' origional balance
                With CType(.FindControl("XrTableCell_all_orig_balance", True), XRTableCell)
                    Dim Value As Object = tbl.Compute("sum(orig_balance)", String.Empty)
                    If Value Is Nothing OrElse Value Is DBNull.Value Then Value = 0D
                    .Text = String.Format("{0:c}", Value)
                End With

                ' payments
                With CType(.FindControl("XrTableCell_all_payments", True), XRTableCell)
                    Dim Value As Object = tbl.Compute("sum(total_payments)", String.Empty)
                    If Value Is Nothing OrElse Value Is DBNull.Value Then Value = 0D
                    .Text = String.Format("{0:c}", Value)
                End With

            End With
        End Sub
    End Class
End Namespace