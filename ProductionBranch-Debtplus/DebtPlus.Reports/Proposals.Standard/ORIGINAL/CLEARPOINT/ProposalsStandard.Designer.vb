Namespace Proposals.Standard
    <Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
    Partial Class ProposalsStandardReport
        Inherits DebtPlus.Reports.Template.BaseXtraReportClass

        'XtraReport overrides dispose to clean up the component list.
        <System.Diagnostics.DebuggerNonUserCode()> _
        Protected Overrides Sub Dispose(ByVal disposing As Boolean)
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
            MyBase.Dispose(disposing)
        End Sub

        'Required by the Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Designer
        'It can be modified using the Designer.  
        'Do not modify it using the code editor.
        <System.Diagnostics.DebuggerStepThrough()> _
        Private Sub InitializeComponent()
            Dim PostNetGenerator1 As DevExpress.XtraPrinting.BarCode.PostNetGenerator = New DevExpress.XtraPrinting.BarCode.PostNetGenerator()
            Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(ProposalsStandardReport))
            Me.XrControlStyle1 = New DevExpress.XtraReports.UI.XRControlStyle()
            Me.XrLabel_Creditor_L = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel1 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel3 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrPictureBox1 = New DevExpress.XtraReports.UI.XRPictureBox()
            Me.XrLabel2 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrPanel_CreditorAddress = New DevExpress.XtraReports.UI.XRPanel()
            Me.XrLabel_Creditor_Address = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrBarCode_PostalCode = New DevExpress.XtraReports.UI.XRBarCode()
            Me.XrLabel_contribution = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_counselor_name = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel27 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel25 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel24 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel23 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel22 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_account_number = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_DebtID = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel26 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel20 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel18 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel17 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel16 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel19 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_message = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel13 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel5 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_ClientNameAddress = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrPageInfo_Date = New DevExpress.XtraReports.UI.XRPageInfo()
            Me.XrLabel4 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrTable1 = New DevExpress.XtraReports.UI.XRTable()
            Me.XrTableRow1 = New DevExpress.XtraReports.UI.XRTableRow()
            Me.XrTableCell1 = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableCell_total_creditors = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableCell2 = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableCell_creditor_payment = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableRow2 = New DevExpress.XtraReports.UI.XRTableRow()
            Me.XrTableCell5 = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableCell_total_debt = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableCell7 = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableCell_creditor_debt = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableRow3 = New DevExpress.XtraReports.UI.XRTableRow()
            Me.XrTableCell9 = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableCell_disbursement_day = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableCell11 = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableCell_first_disbursement = New DevExpress.XtraReports.UI.XRTableCell()
            Me.PageHeader = New DevExpress.XtraReports.UI.PageHeaderBand()
            Me.XrPictureBox2 = New DevExpress.XtraReports.UI.XRPictureBox()
            Me.XrLabel6 = New DevExpress.XtraReports.UI.XRLabel()
            CType(Me.XrTable1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
            '
            'Detail
            '
            Me.Detail.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrPanel_CreditorAddress, Me.XrLabel_contribution, Me.XrLabel_counselor_name, Me.XrLabel27, Me.XrLabel25, Me.XrLabel24, Me.XrLabel23, Me.XrLabel22, Me.XrLabel_account_number, Me.XrLabel_DebtID, Me.XrLabel26, Me.XrLabel20, Me.XrLabel18, Me.XrLabel17, Me.XrLabel16, Me.XrLabel19, Me.XrLabel_message, Me.XrLabel13, Me.XrLabel5, Me.XrLabel_ClientNameAddress, Me.XrPageInfo_Date, Me.XrLabel4, Me.XrTable1})
            Me.Detail.HeightF = 865.0!
            Me.Detail.PageBreak = DevExpress.XtraReports.UI.PageBreak.AfterBand
            '
            'XrControlStyle1
            '
            Me.XrControlStyle1.BackColor = System.Drawing.Color.Teal
            Me.XrControlStyle1.BorderColor = System.Drawing.Color.Teal
            Me.XrControlStyle1.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.XrControlStyle1.ForeColor = System.Drawing.Color.White
            Me.XrControlStyle1.Name = "XrControlStyle1"
            Me.XrControlStyle1.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
            Me.XrControlStyle1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            '
            'XrLabel_Creditor_L
            '
            Me.XrLabel_Creditor_L.LocationFloat = New DevExpress.Utils.PointFloat(8.0!, 158.0!)
            Me.XrLabel_Creditor_L.Multiline = True
            Me.XrLabel_Creditor_L.Name = "XrLabel_Creditor_L"
            Me.XrLabel_Creditor_L.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_Creditor_L.SizeF = New System.Drawing.SizeF(308.0!, 100.0!)
            Me.XrLabel_Creditor_L.Text = "XrLabel_Creditor_L"
            '
            'XrLabel1
            '
            Me.XrLabel1.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
            Me.XrLabel1.Name = "XrLabel1"
            Me.XrLabel1.SizeF = New System.Drawing.SizeF(100.0!, 23.0!)
            '
            'XrLabel3
            '
            Me.XrLabel3.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
            Me.XrLabel3.Name = "XrLabel3"
            Me.XrLabel3.SizeF = New System.Drawing.SizeF(100.0!, 23.0!)
            '
            'XrPictureBox1
            '
            Me.XrPictureBox1.Image = CType(resources.GetObject("XrPictureBox1.Image"), System.Drawing.Image)
            Me.XrPictureBox1.LocationFloat = New DevExpress.Utils.PointFloat(16.0!, 8.0!)
            Me.XrPictureBox1.Name = "XrPictureBox1"
            Me.XrPictureBox1.SizeF = New System.Drawing.SizeF(176.0!, 100.0!)
            Me.XrPictureBox1.Sizing = DevExpress.XtraPrinting.ImageSizeMode.StretchImage
            '
            'XrLabel2
            '
            Me.XrLabel2.LocationFloat = New DevExpress.Utils.PointFloat(208.0!, 8.0!)
            Me.XrLabel2.Multiline = True
            Me.XrLabel2.Name = "XrLabel2"
            Me.XrLabel2.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel2.SizeF = New System.Drawing.SizeF(317.0!, 100.0!)
            Me.XrLabel2.StylePriority.UseTextAlignment = False
            Me.XrLabel2.Text = "ClearPoint Financial Solutions, Inc." & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "8000 Franklin Farms Drive" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Richmond, VA 232" & _
        "29-5004" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "877-877-1995" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Fax 804-282-9215"
            Me.XrLabel2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            '
            'XrPanel_CreditorAddress
            '
            Me.XrPanel_CreditorAddress.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel_Creditor_Address, Me.XrBarCode_PostalCode})
            Me.XrPanel_CreditorAddress.LocationFloat = New DevExpress.Utils.PointFloat(46.54!, 109.75!)
            Me.XrPanel_CreditorAddress.Name = "XrPanel_CreditorAddress"
            Me.XrPanel_CreditorAddress.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
            Me.XrPanel_CreditorAddress.Scripts.OnBeforePrint = "XrPanel_CreditorAddress_BeforePrint"
            Me.XrPanel_CreditorAddress.SizeF = New System.Drawing.SizeF(300.0!, 117.0!)
            '
            'XrLabel_Creditor_Address
            '
            Me.XrLabel_Creditor_Address.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 17.00001!)
            Me.XrLabel_Creditor_Address.Multiline = True
            Me.XrLabel_Creditor_Address.Name = "XrLabel_Creditor_Address"
            Me.XrLabel_Creditor_Address.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_Creditor_Address.SizeF = New System.Drawing.SizeF(300.0!, 100.0!)
            Me.XrLabel_Creditor_Address.Text = "XrLabel_Creditor_Address"
            Me.XrLabel_Creditor_Address.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
            '
            'XrBarCode_PostalCode
            '
            Me.XrBarCode_PostalCode.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.9999911!)
            Me.XrBarCode_PostalCode.Name = "XrBarCode_PostalCode"
            Me.XrBarCode_PostalCode.Padding = New DevExpress.XtraPrinting.PaddingInfo(10, 10, 0, 0, 100.0!)
            Me.XrBarCode_PostalCode.ShowText = False
            Me.XrBarCode_PostalCode.SizeF = New System.Drawing.SizeF(300.0!, 16.0!)
            Me.XrBarCode_PostalCode.Symbology = PostNetGenerator1
            Me.XrBarCode_PostalCode.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
            '
            'XrLabel_contribution
            '
            Me.XrLabel_contribution.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel_contribution.LocationFloat = New DevExpress.Utils.PointFloat(7.999992!, 654.1666!)
            Me.XrLabel_contribution.Name = "XrLabel_contribution"
            Me.XrLabel_contribution.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_contribution.Scripts.OnBeforePrint = "XrLabel_contribution_BeforePrint"
            Me.XrLabel_contribution.SizeF = New System.Drawing.SizeF(333.0!, 17.0!)
            Me.XrLabel_contribution.StylePriority.UseFont = False
            Me.XrLabel_contribution.Text = "contribution"
            '
            'XrLabel_counselor_name
            '
            Me.XrLabel_counselor_name.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "counselor_name")})
            Me.XrLabel_counselor_name.LocationFloat = New DevExpress.Utils.PointFloat(7.999992!, 831.0!)
            Me.XrLabel_counselor_name.Name = "XrLabel_counselor_name"
            Me.XrLabel_counselor_name.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_counselor_name.SizeF = New System.Drawing.SizeF(333.0!, 17.0!)
            Me.XrLabel_counselor_name.Text = "counselor_name"
            '
            'XrLabel27
            '
            Me.XrLabel27.Borders = DevExpress.XtraPrinting.BorderSide.Top
            Me.XrLabel27.BorderWidth = 2
            Me.XrLabel27.LocationFloat = New DevExpress.Utils.PointFloat(7.999992!, 848.0!)
            Me.XrLabel27.Name = "XrLabel27"
            Me.XrLabel27.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel27.SizeF = New System.Drawing.SizeF(333.0!, 17.0!)
            Me.XrLabel27.StylePriority.UseBorders = False
            Me.XrLabel27.StylePriority.UseBorderWidth = False
            Me.XrLabel27.Text = "Counselor"
            '
            'XrLabel25
            '
            Me.XrLabel25.Borders = DevExpress.XtraPrinting.BorderSide.Top
            Me.XrLabel25.BorderWidth = 2
            Me.XrLabel25.LocationFloat = New DevExpress.Utils.PointFloat(7.999992!, 799.0!)
            Me.XrLabel25.Name = "XrLabel25"
            Me.XrLabel25.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel25.SizeF = New System.Drawing.SizeF(333.0!, 17.0!)
            Me.XrLabel25.StylePriority.UseBorders = False
            Me.XrLabel25.StylePriority.UseBorderWidth = False
            Me.XrLabel25.Text = "Date"
            '
            'XrLabel24
            '
            Me.XrLabel24.Borders = DevExpress.XtraPrinting.BorderSide.Top
            Me.XrLabel24.BorderWidth = 2
            Me.XrLabel24.LocationFloat = New DevExpress.Utils.PointFloat(7.999992!, 767.0!)
            Me.XrLabel24.Name = "XrLabel24"
            Me.XrLabel24.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel24.SizeF = New System.Drawing.SizeF(333.0!, 17.0!)
            Me.XrLabel24.StylePriority.UseBorders = False
            Me.XrLabel24.StylePriority.UseBorderWidth = False
            Me.XrLabel24.Text = "Telephone Number"
            '
            'XrLabel23
            '
            Me.XrLabel23.Borders = DevExpress.XtraPrinting.BorderSide.Top
            Me.XrLabel23.BorderWidth = 2
            Me.XrLabel23.LocationFloat = New DevExpress.Utils.PointFloat(7.999992!, 735.0!)
            Me.XrLabel23.Name = "XrLabel23"
            Me.XrLabel23.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel23.SizeF = New System.Drawing.SizeF(333.0!, 17.0!)
            Me.XrLabel23.StylePriority.UseBorders = False
            Me.XrLabel23.StylePriority.UseBorderWidth = False
            Me.XrLabel23.Text = "Authorized Signature/Title or Company Stamp"
            '
            'XrLabel22
            '
            Me.XrLabel22.Borders = DevExpress.XtraPrinting.BorderSide.Top
            Me.XrLabel22.BorderWidth = 2
            Me.XrLabel22.LocationFloat = New DevExpress.Utils.PointFloat(7.999992!, 671.1666!)
            Me.XrLabel22.Name = "XrLabel22"
            Me.XrLabel22.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel22.SizeF = New System.Drawing.SizeF(333.0!, 17.0!)
            Me.XrLabel22.StylePriority.UseBorders = False
            Me.XrLabel22.StylePriority.UseBorderWidth = False
            Me.XrLabel22.Text = "Contribution Status"
            '
            'XrLabel_account_number
            '
            Me.XrLabel_account_number.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "account_number")})
            Me.XrLabel_account_number.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel_account_number.LocationFloat = New DevExpress.Utils.PointFloat(417.0!, 654.1666!)
            Me.XrLabel_account_number.Name = "XrLabel_account_number"
            Me.XrLabel_account_number.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_account_number.SizeF = New System.Drawing.SizeF(333.0!, 17.0!)
            Me.XrLabel_account_number.StylePriority.UseFont = False
            Me.XrLabel_account_number.Text = "account number"
            '
            'XrLabel_DebtID
            '
            Me.XrLabel_DebtID.LocationFloat = New DevExpress.Utils.PointFloat(417.0!, 831.0!)
            Me.XrLabel_DebtID.Name = "XrLabel_DebtID"
            Me.XrLabel_DebtID.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_DebtID.Scripts.OnBeforePrint = "XrLabel_DebtID_BeforePrint"
            Me.XrLabel_DebtID.SizeF = New System.Drawing.SizeF(333.0!, 17.0!)
            Me.XrLabel_DebtID.Text = "DebtID"
            '
            'XrLabel26
            '
            Me.XrLabel26.Borders = DevExpress.XtraPrinting.BorderSide.Top
            Me.XrLabel26.BorderWidth = 2
            Me.XrLabel26.LocationFloat = New DevExpress.Utils.PointFloat(417.0!, 848.0!)
            Me.XrLabel26.Name = "XrLabel26"
            Me.XrLabel26.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel26.SizeF = New System.Drawing.SizeF(333.0!, 17.0!)
            Me.XrLabel26.StylePriority.UseBorders = False
            Me.XrLabel26.StylePriority.UseBorderWidth = False
            Me.XrLabel26.Text = "Agency Information"
            '
            'XrLabel20
            '
            Me.XrLabel20.Borders = DevExpress.XtraPrinting.BorderSide.Top
            Me.XrLabel20.BorderWidth = 2
            Me.XrLabel20.LocationFloat = New DevExpress.Utils.PointFloat(417.0!, 799.0!)
            Me.XrLabel20.Name = "XrLabel20"
            Me.XrLabel20.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel20.SizeF = New System.Drawing.SizeF(333.0!, 17.0!)
            Me.XrLabel20.StylePriority.UseBorders = False
            Me.XrLabel20.StylePriority.UseBorderWidth = False
            Me.XrLabel20.Text = "We do not consent to the proposal. Explain on reverse side"
            '
            'XrLabel18
            '
            Me.XrLabel18.Borders = DevExpress.XtraPrinting.BorderSide.Top
            Me.XrLabel18.BorderWidth = 2
            Me.XrLabel18.LocationFloat = New DevExpress.Utils.PointFloat(417.0!, 767.0!)
            Me.XrLabel18.Name = "XrLabel18"
            Me.XrLabel18.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel18.SizeF = New System.Drawing.SizeF(333.0!, 17.0!)
            Me.XrLabel18.StylePriority.UseBorders = False
            Me.XrLabel18.StylePriority.UseBorderWidth = False
            Me.XrLabel18.Text = "Date of Last Payment/Delinquency"
            '
            'XrLabel17
            '
            Me.XrLabel17.Borders = DevExpress.XtraPrinting.BorderSide.Top
            Me.XrLabel17.BorderWidth = 2
            Me.XrLabel17.LocationFloat = New DevExpress.Utils.PointFloat(417.0!, 735.0!)
            Me.XrLabel17.Name = "XrLabel17"
            Me.XrLabel17.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel17.SizeF = New System.Drawing.SizeF(333.0!, 17.0!)
            Me.XrLabel17.StylePriority.UseBorders = False
            Me.XrLabel17.StylePriority.UseBorderWidth = False
            Me.XrLabel17.Text = "% Interest Rate, if any"
            '
            'XrLabel16
            '
            Me.XrLabel16.Borders = DevExpress.XtraPrinting.BorderSide.Top
            Me.XrLabel16.BorderWidth = 2
            Me.XrLabel16.LocationFloat = New DevExpress.Utils.PointFloat(417.0!, 703.0!)
            Me.XrLabel16.Name = "XrLabel16"
            Me.XrLabel16.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel16.SizeF = New System.Drawing.SizeF(333.0!, 17.0!)
            Me.XrLabel16.StylePriority.UseBorders = False
            Me.XrLabel16.StylePriority.UseBorderWidth = False
            Me.XrLabel16.Text = "Balance"
            '
            'XrLabel19
            '
            Me.XrLabel19.Borders = DevExpress.XtraPrinting.BorderSide.Top
            Me.XrLabel19.BorderWidth = 2
            Me.XrLabel19.LocationFloat = New DevExpress.Utils.PointFloat(417.0!, 671.1666!)
            Me.XrLabel19.Name = "XrLabel19"
            Me.XrLabel19.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel19.SizeF = New System.Drawing.SizeF(333.0!, 17.0!)
            Me.XrLabel19.StylePriority.UseBorders = False
            Me.XrLabel19.StylePriority.UseBorderWidth = False
            Me.XrLabel19.Text = "Client's Account Number"
            '
            'XrLabel_message
            '
            Me.XrLabel_message.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "proposal_message")})
            Me.XrLabel_message.Font = New System.Drawing.Font("Times New Roman", 9.0!)
            Me.XrLabel_message.LocationFloat = New DevExpress.Utils.PointFloat(7.999992!, 617.6666!)
            Me.XrLabel_message.Multiline = True
            Me.XrLabel_message.Name = "XrLabel_message"
            Me.XrLabel_message.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_message.Scripts.OnBeforePrint = "XrLabel_message_BeforePrint"
            Me.XrLabel_message.Scripts.OnPrintOnPage = "XrLabel_message_PrintOnPage"
            Me.XrLabel_message.SizeF = New System.Drawing.SizeF(742.0!, 24.0!)
            Me.XrLabel_message.StylePriority.UseFont = False
            Me.XrLabel_message.Text = "XrLabel_proposal_message"
            '
            'XrLabel13
            '
            Me.XrLabel13.Font = New System.Drawing.Font("Times New Roman", 9.0!)
            Me.XrLabel13.LocationFloat = New DevExpress.Utils.PointFloat(7.99996!, 483.3333!)
            Me.XrLabel13.Multiline = True
            Me.XrLabel13.Name = "XrLabel13"
            Me.XrLabel13.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel13.SizeF = New System.Drawing.SizeF(741.9999!, 121.8332!)
            Me.XrLabel13.StylePriority.UseFont = False
            Me.XrLabel13.Text = resources.GetString("XrLabel13.Text")
            '
            'XrLabel5
            '
            Me.XrLabel5.Font = New System.Drawing.Font("Times New Roman", 9.0!)
            Me.XrLabel5.LocationFloat = New DevExpress.Utils.PointFloat(7.99996!, 231.9583!)
            Me.XrLabel5.Multiline = True
            Me.XrLabel5.Name = "XrLabel5"
            Me.XrLabel5.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel5.SizeF = New System.Drawing.SizeF(741.9999!, 151.3749!)
            Me.XrLabel5.StylePriority.UseFont = False
            Me.XrLabel5.Text = resources.GetString("XrLabel5.Text")
            '
            'XrLabel_ClientNameAddress
            '
            Me.XrLabel_ClientNameAddress.Font = New System.Drawing.Font("Times New Roman", 9.0!)
            Me.XrLabel_ClientNameAddress.LocationFloat = New DevExpress.Utils.PointFloat(467.0!, 47.25002!)
            Me.XrLabel_ClientNameAddress.Multiline = True
            Me.XrLabel_ClientNameAddress.Name = "XrLabel_ClientNameAddress"
            Me.XrLabel_ClientNameAddress.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_ClientNameAddress.Scripts.OnBeforePrint = "XrLabel_ClientNameAddress_BeforePrint"
            Me.XrLabel_ClientNameAddress.SizeF = New System.Drawing.SizeF(283.0!, 100.0!)
            Me.XrLabel_ClientNameAddress.StylePriority.UseFont = False
            Me.XrLabel_ClientNameAddress.Text = "XrLabel_ClientNameAddress"
            '
            'XrPageInfo_Date
            '
            Me.XrPageInfo_Date.Font = New System.Drawing.Font("Times New Roman", 9.0!)
            Me.XrPageInfo_Date.Format = "{0:MMMM dd, yyyy}"
            Me.XrPageInfo_Date.LocationFloat = New DevExpress.Utils.PointFloat(7.999992!, 7.999992!)
            Me.XrPageInfo_Date.Name = "XrPageInfo_Date"
            Me.XrPageInfo_Date.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrPageInfo_Date.PageInfo = DevExpress.XtraPrinting.PageInfo.DateTime
            Me.XrPageInfo_Date.SizeF = New System.Drawing.SizeF(300.0!, 17.00001!)
            Me.XrPageInfo_Date.StylePriority.UseFont = False
            '
            'XrLabel4
            '
            Me.XrLabel4.Font = New System.Drawing.Font("Times New Roman", 9.0!)
            Me.XrLabel4.LocationFloat = New DevExpress.Utils.PointFloat(467.0!, 7.999992!)
            Me.XrLabel4.Name = "XrLabel4"
            Me.XrLabel4.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel4.SizeF = New System.Drawing.SizeF(283.0!, 17.00001!)
            Me.XrLabel4.StylePriority.UseFont = False
            Me.XrLabel4.Text = "NFCC #  20003"
            '
            'XrTable1
            '
            Me.XrTable1.Font = New System.Drawing.Font("Times New Roman", 9.0!)
            Me.XrTable1.LocationFloat = New DevExpress.Utils.PointFloat(137.5001!, 395.8333!)
            Me.XrTable1.Name = "XrTable1"
            Me.XrTable1.Rows.AddRange(New DevExpress.XtraReports.UI.XRTableRow() {Me.XrTableRow1, Me.XrTableRow2, Me.XrTableRow3})
            Me.XrTable1.SizeF = New System.Drawing.SizeF(516.9999!, 75.0!)
            Me.XrTable1.StylePriority.UseFont = False
            '
            'XrTableRow1
            '
            Me.XrTableRow1.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.XrTableCell1, Me.XrTableCell_total_creditors, Me.XrTableCell2, Me.XrTableCell_creditor_payment})
            Me.XrTableRow1.Name = "XrTableRow1"
            Me.XrTableRow1.StylePriority.UseTextAlignment = False
            Me.XrTableRow1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            Me.XrTableRow1.Weight = 1.0R
            '
            'XrTableCell1
            '
            Me.XrTableCell1.Font = New System.Drawing.Font("Times New Roman", 9.0!, System.Drawing.FontStyle.Bold)
            Me.XrTableCell1.Name = "XrTableCell1"
            Me.XrTableCell1.Padding = New DevExpress.XtraPrinting.PaddingInfo(10, 0, 0, 0, 100.0!)
            Me.XrTableCell1.StylePriority.UseFont = False
            Me.XrTableCell1.StylePriority.UsePadding = False
            Me.XrTableCell1.Text = "Total Creditors:"
            Me.XrTableCell1.Weight = 1.25R
            '
            'XrTableCell_total_creditors
            '
            Me.XrTableCell_total_creditors.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "total_creditors", "{0:f0}")})
            Me.XrTableCell_total_creditors.Name = "XrTableCell_total_creditors"
            Me.XrTableCell_total_creditors.StylePriority.UseTextAlignment = False
            Me.XrTableCell_total_creditors.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrTableCell_total_creditors.Weight = 1.17R
            '
            'XrTableCell2
            '
            Me.XrTableCell2.Font = New System.Drawing.Font("Times New Roman", 9.0!, System.Drawing.FontStyle.Bold)
            Me.XrTableCell2.Name = "XrTableCell2"
            Me.XrTableCell2.Padding = New DevExpress.XtraPrinting.PaddingInfo(10, 0, 0, 0, 100.0!)
            Me.XrTableCell2.StylePriority.UseFont = False
            Me.XrTableCell2.StylePriority.UsePadding = False
            Me.XrTableCell2.Text = "Proposed Payment:"
            Me.XrTableCell2.Weight = 1.6600006103515628R
            '
            'XrTableCell_creditor_payment
            '
            Me.XrTableCell_creditor_payment.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "creditor_payment", "{0:c}")})
            Me.XrTableCell_creditor_payment.Name = "XrTableCell_creditor_payment"
            Me.XrTableCell_creditor_payment.StylePriority.UseTextAlignment = False
            Me.XrTableCell_creditor_payment.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrTableCell_creditor_payment.Weight = 1.0899987792968746R
            '
            'XrTableRow2
            '
            Me.XrTableRow2.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.XrTableCell5, Me.XrTableCell_total_debt, Me.XrTableCell7, Me.XrTableCell_creditor_debt})
            Me.XrTableRow2.Name = "XrTableRow2"
            Me.XrTableRow2.StylePriority.UseTextAlignment = False
            Me.XrTableRow2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            Me.XrTableRow2.Weight = 1.0R
            '
            'XrTableCell5
            '
            Me.XrTableCell5.Font = New System.Drawing.Font("Times New Roman", 9.0!, System.Drawing.FontStyle.Bold)
            Me.XrTableCell5.Name = "XrTableCell5"
            Me.XrTableCell5.Padding = New DevExpress.XtraPrinting.PaddingInfo(10, 0, 0, 0, 100.0!)
            Me.XrTableCell5.StylePriority.UseFont = False
            Me.XrTableCell5.StylePriority.UsePadding = False
            Me.XrTableCell5.Text = "Total Debt:"
            Me.XrTableCell5.Weight = 1.25R
            '
            'XrTableCell_total_debt
            '
            Me.XrTableCell_total_debt.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "total_debt", "{0:c}")})
            Me.XrTableCell_total_debt.Name = "XrTableCell_total_debt"
            Me.XrTableCell_total_debt.StylePriority.UseTextAlignment = False
            Me.XrTableCell_total_debt.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrTableCell_total_debt.Weight = 1.17R
            '
            'XrTableCell7
            '
            Me.XrTableCell7.Font = New System.Drawing.Font("Times New Roman", 9.0!, System.Drawing.FontStyle.Bold)
            Me.XrTableCell7.Name = "XrTableCell7"
            Me.XrTableCell7.Padding = New DevExpress.XtraPrinting.PaddingInfo(10, 0, 0, 0, 100.0!)
            Me.XrTableCell7.StylePriority.UseFont = False
            Me.XrTableCell7.StylePriority.UsePadding = False
            Me.XrTableCell7.Text = "Your Account Balance:"
            Me.XrTableCell7.Weight = 1.6600006103515628R
            '
            'XrTableCell_creditor_debt
            '
            Me.XrTableCell_creditor_debt.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "creditor_debt", "{0:c}")})
            Me.XrTableCell_creditor_debt.Name = "XrTableCell_creditor_debt"
            Me.XrTableCell_creditor_debt.StylePriority.UseTextAlignment = False
            Me.XrTableCell_creditor_debt.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrTableCell_creditor_debt.Weight = 1.0899987792968746R
            '
            'XrTableRow3
            '
            Me.XrTableRow3.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.XrTableCell9, Me.XrTableCell_disbursement_day, Me.XrTableCell11, Me.XrTableCell_first_disbursement})
            Me.XrTableRow3.Name = "XrTableRow3"
            Me.XrTableRow3.StylePriority.UseTextAlignment = False
            Me.XrTableRow3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            Me.XrTableRow3.Weight = 1.0R
            '
            'XrTableCell9
            '
            Me.XrTableCell9.Font = New System.Drawing.Font("Times New Roman", 9.0!, System.Drawing.FontStyle.Bold)
            Me.XrTableCell9.Name = "XrTableCell9"
            Me.XrTableCell9.Padding = New DevExpress.XtraPrinting.PaddingInfo(10, 0, 0, 0, 100.0!)
            Me.XrTableCell9.StylePriority.UseFont = False
            Me.XrTableCell9.StylePriority.UsePadding = False
            Me.XrTableCell9.Text = "Start Date:"
            Me.XrTableCell9.Weight = 1.25R
            '
            'XrTableCell_disbursement_day
            '
            Me.XrTableCell_disbursement_day.Name = "XrTableCell_disbursement_day"
            Me.XrTableCell_disbursement_day.Scripts.OnBeforePrint = "XrTableCell_disbursement_day_BeforePrint"
            Me.XrTableCell_disbursement_day.StylePriority.UseTextAlignment = False
            Me.XrTableCell_disbursement_day.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrTableCell_disbursement_day.Weight = 1.17R
            '
            'XrTableCell11
            '
            Me.XrTableCell11.Font = New System.Drawing.Font("Times New Roman", 9.0!, System.Drawing.FontStyle.Bold)
            Me.XrTableCell11.Name = "XrTableCell11"
            Me.XrTableCell11.Padding = New DevExpress.XtraPrinting.PaddingInfo(10, 0, 0, 0, 100.0!)
            Me.XrTableCell11.StylePriority.UseFont = False
            Me.XrTableCell11.StylePriority.UsePadding = False
            Me.XrTableCell11.Text = "Beginning Date:"
            Me.XrTableCell11.Weight = 1.6600006103515628R
            '
            'XrTableCell_first_disbursement
            '
            Me.XrTableCell_first_disbursement.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "first_disbursement", "{0:d}")})
            Me.XrTableCell_first_disbursement.Name = "XrTableCell_first_disbursement"
            Me.XrTableCell_first_disbursement.StylePriority.UseTextAlignment = False
            Me.XrTableCell_first_disbursement.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrTableCell_first_disbursement.Weight = 1.0899987792968746R
            '
            'PageHeader
            '
            Me.PageHeader.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrPictureBox2, Me.XrLabel6})
            Me.PageHeader.HeightF = 115.0!
            Me.PageHeader.Name = "PageHeader"
            '
            'XrPictureBox2
            '
            Me.XrPictureBox2.Image = CType(resources.GetObject("XrPictureBox2.Image"), System.Drawing.Image)
            Me.XrPictureBox2.LocationFloat = New DevExpress.Utils.PointFloat(16.0!, 8.0!)
            Me.XrPictureBox2.Name = "XrPictureBox2"
            Me.XrPictureBox2.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 2, 2, 100.0!)
            Me.XrPictureBox2.SizeF = New System.Drawing.SizeF(198.0!, 100.0!)
            Me.XrPictureBox2.StylePriority.UsePadding = False
            '
            'XrLabel6
            '
            Me.XrLabel6.LocationFloat = New DevExpress.Utils.PointFloat(244.0!, 7.999992!)
            Me.XrLabel6.Multiline = True
            Me.XrLabel6.Name = "XrLabel6"
            Me.XrLabel6.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel6.SizeF = New System.Drawing.SizeF(393.0417!, 100.0!)
            Me.XrLabel6.StylePriority.UseTextAlignment = False
            Me.XrLabel6.Text = "Consumer Credit Counseling Service of Greater Atlanta, Inc." & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "ClearPoint Credit Co" & _
        "unseling Solutions" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "8000 Franklin Farms Drive" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Richmond, VA 23229-5004" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Telephon" & _
        "e: 877-877-1995" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Fax: 855-895-1718" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
            Me.XrLabel6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            '
            'ProposalsStandardReport
            '
            Me.Bands.AddRange(New DevExpress.XtraReports.UI.Band() {Me.TopMarginBand1, Me.Detail, Me.BottomMarginBand1, Me.PageHeader})
            Me.RequestParameters = False
            Me.ScriptLanguage = DevExpress.XtraReports.ScriptLanguage.VisualBasic
            Me.Scripts.OnBeforePrint = "ProposalsStandardReport_BeforePrint"
            Me.ScriptsSource = resources.GetString("$this.ScriptsSource")
            Me.StyleSheet.AddRange(New DevExpress.XtraReports.UI.XRControlStyle() {Me.XrControlStyle1})
            Me.Version = "11.2"
            Me.Controls.SetChildIndex(Me.PageHeader, 0)
            Me.Controls.SetChildIndex(Me.BottomMarginBand1, 0)
            Me.Controls.SetChildIndex(Me.Detail, 0)
            Me.Controls.SetChildIndex(Me.TopMarginBand1, 0)
            CType(Me.XrTable1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me, System.ComponentModel.ISupportInitialize).EndInit()

        End Sub
        Friend WithEvents XrControlStyle1 As DevExpress.XtraReports.UI.XRControlStyle
        Friend WithEvents XrLabel_Creditor_L As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel3 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel1 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel2 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_ClientNameAddress As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrPageInfo_Date As DevExpress.XtraReports.UI.XRPageInfo
        Friend WithEvents XrLabel4 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel5 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel13 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_message As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel26 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel20 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel18 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel17 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel16 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel19 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_contribution As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_counselor_name As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel27 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel25 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel24 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel23 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel22 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_account_number As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_DebtID As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrPanel_CreditorAddress As DevExpress.XtraReports.UI.XRPanel
        Friend WithEvents XrLabel_Creditor_Address As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrBarCode_PostalCode As DevExpress.XtraReports.UI.XRBarCode
        Friend WithEvents XrPictureBox1 As DevExpress.XtraReports.UI.XRPictureBox
        Friend WithEvents XrTable1 As DevExpress.XtraReports.UI.XRTable
        Friend WithEvents XrTableRow1 As DevExpress.XtraReports.UI.XRTableRow
        Friend WithEvents XrTableCell1 As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents XrTableCell_total_creditors As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents XrTableCell2 As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents XrTableCell_creditor_payment As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents XrTableRow2 As DevExpress.XtraReports.UI.XRTableRow
        Friend WithEvents XrTableCell5 As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents XrTableCell_total_debt As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents XrTableCell7 As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents XrTableCell_creditor_debt As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents XrTableRow3 As DevExpress.XtraReports.UI.XRTableRow
        Friend WithEvents XrTableCell9 As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents XrTableCell_disbursement_day As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents XrTableCell11 As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents XrTableCell_first_disbursement As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents PageHeader As DevExpress.XtraReports.UI.PageHeaderBand
        Friend WithEvents XrPictureBox2 As DevExpress.XtraReports.UI.XRPictureBox
        Friend WithEvents XrLabel6 As DevExpress.XtraReports.UI.XRLabel
    End Class
End Namespace
