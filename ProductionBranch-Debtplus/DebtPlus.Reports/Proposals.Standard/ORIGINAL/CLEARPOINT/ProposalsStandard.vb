#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}
#End Region

Namespace Proposals.Standard
    Public Class ProposalsStandardReport

        ''' <summary>
        '''     Create an instance of our report
        ''' </summary>
        Public Sub New()
            MyBase.New()
            InitializeComponent()

            ' Set the script references as needed
            ScriptReferences = DebtPlus.Utils.AssemblyTools.GetAllSiblingAssemblies()

            ' Erase the parameters in case they were set in the report definition.
            Parameter_Proposal = -1
            Parameter_Batch = -1

            '-- Add the preview formatting routine addresses
            'AddHandler BeforePrint, AddressOf ProposalsStandardReport_BeforePrint
            'AddHandler XrLabel_ClientNameAddress.BeforePrint, AddressOf XrLabel_ClientNameAddress_BeforePrint
            'AddHandler XrPanel_CreditorAddress.BeforePrint, AddressOf XrPanel_CreditorAddress_BeforePrint
            'AddHandler XrLabel_DebtID.BeforePrint, AddressOf XrLabel_DebtID_BeforePrint
            'AddHandler XrLabel_contribution.BeforePrint, AddressOf XrLabel_contribution_BeforePrint
            'AddHandler XrTableCell_disbursement_day.BeforePrint, AddressOf XrTableCell_disbursement_day_BeforePrint
        End Sub

        Public Property Parameter_Batch() As System.Int32
            Get
                Dim param As DevExpress.XtraReports.Parameters.Parameter = FindParameter("Parameter_Batch")
                Return If(param Is Nothing, -1, Convert.ToInt32(param.Value))
            End Get
            Set(ByVal value As System.Int32)
                SetParameter("Parameter_Batch", GetType(Int32), value, "Batch ID", False)
            End Set
        End Property

        Public Property Parameter_Proposal() As System.Int32
            Get
                Dim param As DevExpress.XtraReports.Parameters.Parameter = FindParameter("Parameter_Proposal")
                Return If(param Is Nothing, -1, Convert.ToInt32(param.Value))
            End Get
            Set(ByVal value As System.Int32)
                SetParameter("Parameter_Proposal", GetType(Int32), value, "Proposal ID", False)
            End Set
        End Property

        Public Overrides Function NeedParameters() As Boolean
            Return Parameter_Batch <= 0 AndAlso Parameter_Proposal <= 0
        End Function

        Public Overrides Function RequestReportParameters() As System.Windows.Forms.DialogResult
            Dim Answer As System.Windows.Forms.DialogResult = DialogResult.OK
            If NeedParameters() Then
                Using frm As New DebtPlus.Reports.Template.Forms.ProposalParametersForm()
                    Answer = frm.ShowDialog()
                    Parameter_Batch = frm.Parameter_BatchID
                End Using
            End If
            Return Answer
        End Function

        '**************************************** MOVED TO SCRIPTS *********************************************
        Dim ds As New System.Data.DataSet("ds")

        Private Sub ProposalsStandardReport_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs)
            Dim rpt As DevExpress.XtraReports.UI.XtraReport = CType(sender, DevExpress.XtraReports.UI.XtraReport)
            Dim ProposalID As System.Int32 = CType(rpt.Parameters("Parameter_Proposal").Value, System.Int32)
            Dim BatchID As System.Int32 = CType(rpt.Parameters("Parameter_Batch").Value, System.Int32)

            Dim sqlInfo As DebtPlus.LINQ.SQLInfoClass = DebtPlus.LINQ.SQLInfoClass.getDefault()
            Using cn As System.Data.SqlClient.SqlConnection = New System.Data.SqlClient.SqlConnection(sqlInfo.ConnectionString)
                cn.Open()
                Using cmd As New System.Data.SqlClient.SqlCommand()
                    cmd.Connection = cn
                    cmd.Connection = New System.Data.SqlClient.SqlConnection(sqlInfo.ConnectionString)
                    cmd.CommandType = System.Data.CommandType.StoredProcedure
                    cmd.CommandText = "rpt_proposal_standard"
                    cmd.CommandTimeout = 0

                    If ProposalID > 0 Then
                        cmd.Parameters.Add("@client_creditor_proposal", System.Data.SqlDbType.Int).Value = ProposalID
                        cmd.Parameters.Add("@proposal_mode", System.Data.SqlDbType.Int).Value = 0
                    Else
                        cmd.Parameters.Add("@client_creditor_proposal", System.Data.SqlDbType.Int).Value = BatchID
                        cmd.Parameters.Add("@proposal_mode", System.Data.SqlDbType.Int).Value = 2
                    End If
                    cmd.CommandTimeout = 0

                    Using da As New System.Data.SqlClient.SqlDataAdapter(cmd)
                        da.Fill(ds, "rpt_proposal_standard")
                    End Using
                End Using
            End Using

            rpt.DataSource = New System.Data.DataView(ds.Tables("rpt_proposal_standard"), String.Empty, "creditor, client, client_creditor", System.Data.DataViewRowState.CurrentRows)
        End Sub

        Private Sub XrLabel_contribution_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs)
            Dim lbl As DevExpress.XtraReports.UI.XRLabel = CType(Sender, DevExpress.XtraReports.UI.XRLabel)
            Dim rpt As DevExpress.XtraReports.UI.XtraReportBase = lbl.Report
            Dim creditor_type As String = DebtPlus.Utils.Nulls.DStr(rpt.GetCurrentColumnValue("creditor_type"))
            Dim Answer As String

            Dim fairshare_rate As Double = DebtPlus.Utils.Nulls.DDbl(rpt.GetCurrentColumnValue("fairshare_rate"))
            While fairshare_rate >= 100.0!
                fairshare_rate /= 100.0!
            End While
            If fairshare_rate <= 0.0! Then creditor_type = "N"

            '-- Convert the contribution type to a string
            Select Case creditor_type.ToUpper
                Case "D"
                    Answer = String.Format("DEDUCT AT {0:p}", fairshare_rate)
                Case "N"
                    Answer = "NO CONTRIBUTION"
                Case Else
                    Answer = String.Format("INVOICE AT {0:p}", fairshare_rate)
            End Select

            lbl.Text = Answer
        End Sub

        Private Sub XrTableCell_disbursement_day_BeforePrint(ByVal Sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs)
            Dim DayOfMonth As Integer = 1
            Dim cell As DevExpress.XtraReports.UI.XRTableCell = CType(Sender, DevExpress.XtraReports.UI.XRTableCell)
            Dim rpt As DevExpress.XtraReports.UI.XtraReportBase = cell.Report
            Dim Answer As String

            '-- Find the day of the month for the disbursement.
            Dim Disbursement_Date As Object = rpt.GetCurrentColumnValue("first_disbursement")
            If Disbursement_Date IsNot Nothing AndAlso Disbursement_Date IsNot System.DBNull.Value Then
                If TypeOf Disbursement_Date Is System.DateTime OrElse TypeOf Disbursement_Date Is Date Then
                    DayOfMonth = Convert.ToDateTime(Disbursement_Date, System.Globalization.CultureInfo.InvariantCulture).Day
                Else
                    DayOfMonth = Convert.ToInt32(Disbursement_Date, System.Globalization.CultureInfo.InvariantCulture)
                End If
            End If

            '-- From the 10th to the 19th it is always "th"
            If DayOfMonth > 10 AndAlso DayOfMonth < 20 Then
                Answer = String.Format("{0:f0}th", DayOfMonth)
            Else

                '-- Otherwise look at the relative day
                Select Case DayOfMonth Mod 10
                    Case 1
                        Answer = String.Format("{0:f0}st", DayOfMonth)
                    Case 2
                        Answer = String.Format("{0:f0}nd", DayOfMonth)
                    Case 3
                        Answer = String.Format("{0:f0}rd", DayOfMonth)
                    Case Else
                        Answer = String.Format("{0:f0}th", DayOfMonth)
                End Select
            End If

            cell.Text = Answer
        End Sub

        Private Sub XrLabel_DebtID_BeforePrint(ByVal sender As Object, ByVal e As System.EventArgs)
            Dim lbl As DevExpress.XtraReports.UI.XRLabel = CType(sender, DevExpress.XtraReports.UI.XRLabel)
            Dim rpt As DevExpress.XtraReports.UI.XtraReportBase = lbl.Report

            Dim Client As Object = rpt.GetCurrentColumnValue("client")
            Dim Creditor As Object = rpt.GetCurrentColumnValue("creditor")
            Dim Answer As String = String.Empty

            If Client IsNot System.DBNull.Value AndAlso Creditor IsNot System.DBNull.Value Then
                Answer = DebtPlus.Utils.Format.Client.FormatClientID(Client) + "*" + Convert.ToString(Creditor, System.Globalization.CultureInfo.InvariantCulture)
            End If

            lbl.Text = Answer
        End Sub

        Private Sub XrLabel_ClientNameAddress_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs)
            Dim lbl As DevExpress.XtraReports.UI.XRLabel = CType(sender, DevExpress.XtraReports.UI.XRLabel)
            Dim rpt As DevExpress.XtraReports.UI.XtraReportBase = lbl.Report
            lbl.Text = GetClientNameAndAddressByID(DebtPlus.Utils.Nulls.DInt(rpt.GetCurrentColumnValue("client_creditor_proposal")))
        End Sub

        Private Function GetClientNameAndAddressByID(ByVal client_creditor_proposal As System.Int32) As String
            Dim sb As New System.Text.StringBuilder

            If client_creditor_proposal >= 0 Then
                Const TableName As String = "rpt_Proposal_AddressInformation"
                Dim row As System.Data.DataRow = Nothing
                Dim tbl As System.Data.DataTable = ds.Tables(TableName)
                If tbl IsNot Nothing Then row = tbl.Rows.Find(client_creditor_proposal)

                If row Is Nothing Then
                    Dim sqlInfo As DebtPlus.LINQ.SQLInfoClass = DebtPlus.LINQ.SQLInfoClass.getDefault()
                    Using cn As System.Data.SqlClient.SqlConnection = New System.Data.SqlClient.SqlConnection(sqlInfo.ConnectionString)
                        cn.Open()
                        Using cmd As New System.Data.SqlClient.SqlCommand()
                            cmd.Connection = cn
                            cmd.Connection = New System.Data.SqlClient.SqlConnection(sqlInfo.ConnectionString)
                            cmd.CommandType = System.Data.CommandType.StoredProcedure
                            cmd.CommandText = "rpt_Proposal_AddressInformation"
                            cmd.Parameters.Add("@client_creditor_proposal", System.Data.SqlDbType.Int).Value = client_creditor_proposal

                            Using da As New System.Data.SqlClient.SqlDataAdapter(cmd)
                                da.Fill(ds, TableName)
                            End Using
                        End Using
                    End Using

                    tbl = ds.Tables(TableName)
                    If tbl.PrimaryKey.GetUpperBound(0) < 0 Then
                        tbl.PrimaryKey = New System.Data.DataColumn() {tbl.Columns("client_creditor_proposal")}
                    End If
                    row = tbl.Rows.Find(client_creditor_proposal)
                End If

                If row IsNot Nothing Then
                    sb.Append(System.Environment.NewLine)
                    sb.AppendFormat("Re: Client # {0}", DebtPlus.Utils.Format.Client.FormatClientID(row("client")))

                    For Each FldID As String In New String() {"name", "address1", "address2", "address3"}
                        Dim Value As Object = row(FldID)
                        If Value IsNot Nothing AndAlso Value IsNot System.DBNull.Value Then
                            Dim strValue As String = Convert.ToString(Value, System.Globalization.CultureInfo.InvariantCulture).Trim
                            If strValue <> String.Empty Then
                                sb.Append(System.Environment.NewLine)
                                sb.Append(strValue)
                            End If
                        End If
                    Next

                    If sb.Length > 0 Then sb.Remove(0, 2)
                End If
            End If

            Return sb.ToString()
        End Function

        ''' <summary>
        '''     Format the creditor ID before it is printed
        ''' </summary>
        Private Sub XrPanel_CreditorAddress_BeforePrint(ByVal sender As System.Object, ByVal e As System.Drawing.Printing.PrintEventArgs)
            Dim panel As DevExpress.XtraReports.UI.XRPanel = CType(sender, DevExpress.XtraReports.UI.XRPanel)
            Dim rpt As DevExpress.XtraReports.UI.XtraReportBase = panel.Report

            Dim text_block As New System.Text.StringBuilder
            Dim postalcode As String = System.String.Empty
            Dim row As System.Data.DataRow = Nothing

            '-- Find the creditor
            Dim Creditor As String = String.Empty
            If rpt.GetCurrentColumnValue("creditor") IsNot Nothing AndAlso rpt.GetCurrentColumnValue("creditor") IsNot System.DBNull.Value Then
                Creditor = Convert.ToString(rpt.GetCurrentColumnValue("creditor"), System.Globalization.CultureInfo.InvariantCulture).Trim
            End If

            If Creditor <> String.Empty Then
                Const TableName As String = "rpt_CreditorAddress_L"
                Dim tbl As System.Data.DataTable = ds.Tables(TableName)
                If tbl IsNot Nothing Then
                    row = tbl.Rows.Find(Creditor)
                End If

                If row Is Nothing Then
                    Dim sqlInfo As DebtPlus.LINQ.SQLInfoClass = DebtPlus.LINQ.SQLInfoClass.getDefault()
                    Using cn As System.Data.SqlClient.SqlConnection = New System.Data.SqlClient.SqlConnection(sqlInfo.ConnectionString)
                        cn.Open()
                        Using cmd As New System.Data.SqlClient.SqlCommand()
                            cmd.Connection = cn
                            cmd.CommandText = "rpt_CreditorAddress_L"
                            cmd.CommandType = System.Data.CommandType.StoredProcedure
                            cmd.Parameters.Add("@creditor", System.Data.SqlDbType.VarChar, 10).Value = rpt.GetCurrentColumnValue("creditor")
                            Using da As New System.Data.SqlClient.SqlDataAdapter(cmd)
                                da.Fill(ds, TableName)
                            End Using
                        End Using
                    End Using

                    tbl = ds.Tables(TableName)
                    If tbl.PrimaryKey.GetUpperBound(0) < 0 Then
                        tbl.PrimaryKey = New System.Data.DataColumn() {tbl.Columns("creditor")}
                    End If
                    row = tbl.Rows.Find(Creditor)
                End If
            End If

            If row IsNot Nothing Then
                '-- We need the postalcode by itself. Find it first.
                If row("zipcode") IsNot Nothing AndAlso row("zipcode") IsNot System.DBNull.Value Then
                    postalcode = Convert.ToString(row("zipcode"), System.Globalization.CultureInfo.InvariantCulture).Trim
                End If

                '-- Build the address string for the report.
                For Each NameString As String In New String() {"addr1", "addr2", "addr3", "addr4", "addr5", "addr6", "addr7"}
                    Dim Value As String = String.Empty
                    Dim objValue As Object = row(NameString)
                    If objValue IsNot Nothing AndAlso objValue IsNot System.DBNull.Value Then
                        Value = Convert.ToString(objValue, System.Globalization.CultureInfo.InvariantCulture).Trim
                    End If
                    If Value <> System.String.Empty Then
                        text_block.Append(System.Environment.NewLine)
                        text_block.Append(Value)
                    End If
                Next NameString
            End If
            If text_block.Length > 0 Then text_block.Remove(0, 2)

            '-- Set the address string
            Dim ctlText As DevExpress.XtraReports.UI.XRLabel = TryCast(rpt.FindControl("XrLabel_Creditor_Address", True), DevExpress.XtraReports.UI.XRLabel)
            If ctlText IsNot Nothing Then
                ctlText.Text = text_block.ToString
            End If

            '-- Set the postalcode into the postnet field
            Dim ctlBarcode As DevExpress.XtraReports.UI.XRBarCode = TryCast(rpt.FindControl("XrBarCode_PostalCode", True), DevExpress.XtraReports.UI.XRBarCode)
            If ctlBarcode IsNot Nothing Then
                If postalcode <> System.String.Empty Then
                    ctlBarcode.Text = DebtPlus.Utils.Format.Strings.DigitsOnly(postalcode)
                    ctlBarcode.Visible = True
                Else
                    ctlBarcode.Text = String.Empty
                    ctlBarcode.Visible = False
                End If
            End If
        End Sub
    End Class
End Namespace
