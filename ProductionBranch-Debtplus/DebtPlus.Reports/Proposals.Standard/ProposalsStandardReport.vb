#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}
#End Region

Namespace Proposals.Standard
    Public Class ProposalsStandardReport
        Inherits DebtPlus.Reports.Template.TemplateXtraReportClass

        Public Sub New()
            MyBase.New()
            InitializeComponent()

            ' Set the script references as needed
            ScriptReferences = DebtPlus.Utils.AssemblyTools.GetAllSiblingAssemblies()

            ' Erase the parameters in case they were set in the report definition.
            Parameter_Proposal = -1
            Parameter_Batch = -1
        End Sub

        Public Property Parameter_Batch() As System.Int32
            Get
                Dim param As DevExpress.XtraReports.Parameters.Parameter = FindParameter("Parameter_Batch")
                Return If(param Is Nothing, -1, Convert.ToInt32(param.Value))
            End Get
            Set(ByVal value As System.Int32)
                SetParameter("Parameter_Batch", GetType(Int32), value, "Batch ID", False)
            End Set
        End Property

        Public Property Parameter_Proposal() As System.Int32
            Get
                Dim param As DevExpress.XtraReports.Parameters.Parameter = FindParameter("Parameter_Proposal")
                Return If(param Is Nothing, -1, Convert.ToInt32(param.Value))
            End Get
            Set(ByVal value As System.Int32)
                SetParameter("Parameter_Proposal", GetType(Int32), value, "Proposal ID", False)
            End Set
        End Property

        Public Overrides Function NeedParameters() As Boolean
            Return Parameter_Batch <= 0 AndAlso Parameter_Proposal <= 0
        End Function

        Public Overrides Function RequestReportParameters() As System.Windows.Forms.DialogResult
            Dim Answer As System.Windows.Forms.DialogResult = DialogResult.OK
            If NeedParameters() Then
                Using frm As New DebtPlus.Reports.Template.Forms.ProposalParametersForm()
                    frm.ShowClosedBatches = False
                    Answer = frm.ShowDialog()
                    Parameter_Batch = frm.Parameter_BatchID
                End Using
            End If
            Return Answer
        End Function

        Private Sub InitializeComponent()
            ' Name of the report file
            Const ReportName As String = "DebtPlus.Reports.Proposals.Standard.repx"

            ' See if there is a report reference that we can use
            Dim Fname As String = System.IO.Path.Combine(DebtPlus.Configuration.Config.ReportsDirectory, ReportName)
            Dim UseDefault As Boolean = True
            Try
                If (Not DebtPlus.Configuration.Config.LocalReportsOnly) AndAlso System.IO.File.Exists(Fname) Then
                    LoadLayout(Fname)
                    UseDefault = False
                End If

            Catch ex As System.Exception
                DebtPlus.Svc.Common.ErrorHandling.HandleErrors(ex)
            End Try

            ' Load the standard report definition if we need a new item
            If UseDefault Then
                Dim asm As Reflection.Assembly = Reflection.Assembly.GetExecutingAssembly                                           ' changed
                Using ios As System.IO.Stream = asm.GetManifestResourceStream(String.Format("DebtPlus.Reports.{0}", ReportName))              ' changed
                    If ios IsNot Nothing Then
                        LoadLayout(ios)

                        ' Stamp the report "SAMPLE" if loaded by default
                        With Watermark
                            .Font = New System.Drawing.Font("Arial", 80.0!, System.Drawing.FontStyle.Bold)
                            .ForeColor = System.Drawing.Color.DeepSkyBlue
                            .ShowBehind = False
                            .Text = "SAMPLE"
                            .TextTransparency = 139
                        End With
                    End If
                End Using
            End If
        End Sub
    End Class
End Namespace
