#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}
#End Region

Imports DebtPlus.Reports.Template
Imports DebtPlus.Utils
Imports System.IO
Imports System.Reflection
Imports DevExpress.XtraReports.UI

Namespace Forms.Disclosure
    Public Class DisclosuresReport
        Inherits BaseXtraReportClass

        ''' <summary>
        ''' Create an instance of our report
        ''' </summary>
        Public Sub New()
            MyClass.New(String.Empty)
        End Sub

        Public Sub New(ByVal ReportDefinition As String)
            MyBase.New()
            InitializeComponent()
            AddHandler Me.BeforePrint, AddressOf AuthorizationReport_BeforePrint

            '-- Split the names along the semi-colon
            Parameters("ParameterReportName").Value = String.Empty
            Dim Fields() As String = ReportDefinition.Split(";"c)
            If Fields.GetUpperBound(0) < 1 Then
                Parameters("ParameterReportName").Value = ReportDefinition.Trim()
            Else
                Parameters("ParameterReportName").Value = Fields(1).Trim()
            End If

            '-- The name of our Letter text is the 2nd parameter in the field list
            If Convert.ToString(Parameters("ParameterReportName").Value) = String.Empty Then
                Throw New ApplicationException("The entry in the reports table is not valid for this report. The report name can not be empty.")
            End If

            '-- Set the script references to the current (running) values.
            ScriptReferences = DebtPlus.Utils.AssemblyTools.GetAllSiblingAssemblies

            '-- Copy the script references to the sub-reports as well
            For Each Band As DevExpress.XtraReports.UI.Band In Bands
                For Each ctl As DevExpress.XtraReports.UI.XRControl In Band.Controls
                    Dim subReport As DevExpress.XtraReports.UI.XRSubreport = TryCast(ctl, DevExpress.XtraReports.UI.XRSubreport)
                    If subReport IsNot Nothing Then
                        Dim rpt As DevExpress.XtraReports.UI.XtraReport = subReport.ReportSource
                        If rpt IsNot Nothing Then
                            rpt.ScriptReferences = ScriptReferences
                        End If
                    End If
                Next
            Next
        End Sub

        Public Overrides Function NeedParameters() As Boolean
            Return False
        End Function

        Public Overrides ReadOnly Property ReportTitle As String
            Get
                Return "Document Text"
            End Get
        End Property

        '***************************************** MOVED TO SCRIPTS **************************************************************

        Private Sub AuthorizationReport_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs)
            Dim rpt As DevExpress.XtraReports.UI.XtraReport = CType(sender, DevExpress.XtraReports.UI.XtraReport)
            Dim sqlInfo As DebtPlus.LINQ.SQLInfoClass = DebtPlus.LINQ.SQLInfoClass.getDefault()
            Dim ReportName As String = Convert.ToString(rpt.Parameters("ParameterReportName").Value)

            '-- Look at the string to determine if we are to start with the reports or letters directories
            If ReportName.ToLower.StartsWith("[reports]/") OrElse ReportName.ToLower.StartsWith("[reports]\") Then
                ReportName = System.IO.Path.Combine(DebtPlus.Configuration.Config.ReportsDirectory(), ReportName.Substring(10))
            ElseIf ReportName.ToLower.StartsWith("[letters]/") OrElse ReportName.ToLower.StartsWith("[letters]\") Then
                ReportName = System.IO.Path.Combine(DebtPlus.Configuration.Config.LettersDirectory(), ReportName.Substring(10))

                '-- If the name is relative then assume that it is relative to the reports share since we are, after all, a report.
            ElseIf Not System.IO.Path.IsPathRooted(ReportName) Then
                ReportName = System.IO.Path.Combine(DebtPlus.Configuration.Config.ReportsDirectory(), ReportName)
            End If

            ' The file must exist. If it does not then the original message will remain saying that there is an error.
            Dim LetterControl As DevExpress.XtraReports.UI.XRRichText = CType(rpt.FindControl("XrRichText1", True), DevExpress.XtraReports.UI.XRRichText)
            If System.IO.File.Exists(ReportName) AndAlso LetterControl IsNot Nothing Then

                '-- Look at the extension to determine the type of the document that we want to load
                Dim Success As Boolean = False
                Dim StreamType As DevExpress.XtraReports.UI.XRRichTextStreamType
                Try
                    Dim Extension As String = System.IO.Path.GetExtension(ReportName).ToLower
                    Select Case Extension
                        Case ".rtf"
                            StreamType = DevExpress.XtraReports.UI.XRRichTextStreamType.RtfText
                        Case ".htm", ".html"
                            StreamType = DevExpress.XtraReports.UI.XRRichTextStreamType.HtmlText
                        Case Else
                            StreamType = DevExpress.XtraReports.UI.XRRichTextStreamType.PlainText
                    End Select

                    '-- Try to load the file with the text
                    LetterControl.LoadFile(ReportName, StreamType)
                    Success = True

                Catch ex As Exception
                    DebtPlus.Svc.Common.ErrorHandling.HandleErrors(ex)
                End Try

                ' If the file could not be loaded and it is not already text then try to load it as text
                If Not Success AndAlso StreamType <> DevExpress.XtraReports.UI.XRRichTextStreamType.PlainText Then
                    Try
                        LetterControl.LoadFile(ReportName, DevExpress.XtraReports.UI.XRRichTextStreamType.PlainText)

                    Catch ex As Exception
                        Using gdr As New Repository.GetDataResult()
                            gdr.HandleException(ex)
                            DebtPlus.UI.Common.ErrorHandling.HandleErrors(gdr, "Error loading RTF text")
                        End Using
                    End Try
                End If
            End If
        End Sub
    End Class
End Namespace
