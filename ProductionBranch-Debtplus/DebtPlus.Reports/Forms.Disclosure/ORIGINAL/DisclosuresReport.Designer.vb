Namespace Forms.Disclosure
    <Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
    Partial Public Class DisclosuresReport
        Inherits DebtPlus.Reports.Template.BaseXtraReportClass

        'XtraReport overrides dispose to clean up the component list.
        <System.Diagnostics.DebuggerNonUserCode()> _
        Protected Overrides Sub Dispose(ByVal disposing As Boolean)
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
            MyBase.Dispose(disposing)
        End Sub

        'Required by the Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Designer
        'It can be modified using the Designer.  
        'Do not modify it using the code editor.
        <System.Diagnostics.DebuggerStepThrough()> _
        Private Sub InitializeComponent()
            Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(DisclosuresReport))
            Me.XrRichText1 = New DevExpress.XtraReports.UI.XRRichText()
            Me.XrPictureBox1 = New DevExpress.XtraReports.UI.XRPictureBox()
            Me.PageHeader = New DevExpress.XtraReports.UI.PageHeaderBand()
            Me.XrRichText2 = New DevExpress.XtraReports.UI.XRRichText()
            Me.PageFooter = New DevExpress.XtraReports.UI.PageFooterBand()
            Me.XrPictureBox5 = New DevExpress.XtraReports.UI.XRPictureBox()
            Me.XrPictureBox4 = New DevExpress.XtraReports.UI.XRPictureBox()
            Me.XrPictureBox3 = New DevExpress.XtraReports.UI.XRPictureBox()
            Me.XrPictureBox2 = New DevExpress.XtraReports.UI.XRPictureBox()
            Me.XrLabel_page_footer = New DevExpress.XtraReports.UI.XRLabel()
            Me.ParameterReportName = New DevExpress.XtraReports.Parameters.Parameter()
            CType(Me.XrRichText1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.XrRichText2, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
            '
            'Detail
            '
            Me.Detail.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrRichText1})
            Me.Detail.HeightF = 40.08333!
            '
            'XrRichText1
            '
            Me.XrRichText1.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
            Me.XrRichText1.Name = "XrRichText1"
            Me.XrRichText1.Padding = New DevExpress.XtraPrinting.PaddingInfo(10, 10, 10, 10, 100.0!)
            Me.XrRichText1.SerializableRtfString = resources.GetString("XrRichText1.SerializableRtfString")
            Me.XrRichText1.SizeF = New System.Drawing.SizeF(767.0!, 40.08333!)
            Me.XrRichText1.StylePriority.UsePadding = False
            '
            'XrPictureBox1
            '
            Me.XrPictureBox1.Image = CType(resources.GetObject("XrPictureBox1.Image"), System.Drawing.Image)
            Me.XrPictureBox1.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
            Me.XrPictureBox1.Name = "XrPictureBox1"
            Me.XrPictureBox1.SizeF = New System.Drawing.SizeF(198.0!, 108.0!)
            Me.XrPictureBox1.Sizing = DevExpress.XtraPrinting.ImageSizeMode.ZoomImage
            '
            'PageHeader
            '
            Me.PageHeader.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrRichText2, Me.XrPictureBox1})
            Me.PageHeader.HeightF = 120.5!
            Me.PageHeader.Name = "PageHeader"
            '
            'XrRichText2
            '
            Me.XrRichText2.LocationFloat = New DevExpress.Utils.PointFloat(372.9165!, 0.0!)
            Me.XrRichText2.Name = "XrRichText2"
            Me.XrRichText2.Padding = New DevExpress.XtraPrinting.PaddingInfo(3, 3, 3, 3, 100.0!)
            Me.XrRichText2.SerializableRtfString = resources.GetString("XrRichText2.SerializableRtfString")
            Me.XrRichText2.SizeF = New System.Drawing.SizeF(394.0835!, 88.62503!)
            Me.XrRichText2.StylePriority.UsePadding = False
            '
            'PageFooter
            '
            Me.PageFooter.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrPictureBox5, Me.XrPictureBox4, Me.XrPictureBox3, Me.XrPictureBox2, Me.XrLabel_page_footer})
            Me.PageFooter.HeightF = 77.0!
            Me.PageFooter.Name = "PageFooter"
            '
            'XrPictureBox5
            '
            Me.XrPictureBox5.Image = CType(resources.GetObject("XrPictureBox5.Image"), System.Drawing.Image)
            Me.XrPictureBox5.LocationFloat = New DevExpress.Utils.PointFloat(690.0!, 4.999996!)
            Me.XrPictureBox5.Name = "XrPictureBox5"
            Me.XrPictureBox5.SizeF = New System.Drawing.SizeF(72.0!, 72.0!)
            Me.XrPictureBox5.Sizing = DevExpress.XtraPrinting.ImageSizeMode.ZoomImage
            '
            'XrPictureBox4
            '
            Me.XrPictureBox4.Image = CType(resources.GetObject("XrPictureBox4.Image"), System.Drawing.Image)
            Me.XrPictureBox4.LocationFloat = New DevExpress.Utils.PointFloat(590.0!, 4.999996!)
            Me.XrPictureBox4.Name = "XrPictureBox4"
            Me.XrPictureBox4.SizeF = New System.Drawing.SizeF(77.0!, 72.0!)
            Me.XrPictureBox4.Sizing = DevExpress.XtraPrinting.ImageSizeMode.ZoomImage
            '
            'XrPictureBox3
            '
            Me.XrPictureBox3.Image = CType(resources.GetObject("XrPictureBox3.Image"), System.Drawing.Image)
            Me.XrPictureBox3.LocationFloat = New DevExpress.Utils.PointFloat(100.0!, 0.0!)
            Me.XrPictureBox3.Name = "XrPictureBox3"
            Me.XrPictureBox3.SizeF = New System.Drawing.SizeF(47.0!, 70.0!)
            Me.XrPictureBox3.Sizing = DevExpress.XtraPrinting.ImageSizeMode.ZoomImage
            '
            'XrPictureBox2
            '
            Me.XrPictureBox2.Image = CType(resources.GetObject("XrPictureBox2.Image"), System.Drawing.Image)
            Me.XrPictureBox2.LocationFloat = New DevExpress.Utils.PointFloat(27.08333!, 0.0!)
            Me.XrPictureBox2.Name = "XrPictureBox2"
            Me.XrPictureBox2.SizeF = New System.Drawing.SizeF(47.0!, 70.0!)
            Me.XrPictureBox2.Sizing = DevExpress.XtraPrinting.ImageSizeMode.ZoomImage
            '
            'XrLabel_page_footer
            '
            Me.XrLabel_page_footer.Font = New System.Drawing.Font("Garamond", 9.0!, System.Drawing.FontStyle.Bold)
            Me.XrLabel_page_footer.LocationFloat = New DevExpress.Utils.PointFloat(237.5!, 12.5!)
            Me.XrLabel_page_footer.Name = "XrLabel_page_footer"
            Me.XrLabel_page_footer.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_page_footer.SizeF = New System.Drawing.SizeF(337.1666!, 50.0!)
            Me.XrLabel_page_footer.StylePriority.UseFont = False
            Me.XrLabel_page_footer.StylePriority.UseTextAlignment = False
            Me.XrLabel_page_footer.Text = "www.ClearPointFinancialSolutions.org"
            Me.XrLabel_page_footer.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
            '
            'ParameterReportName
            '
            Me.ParameterReportName.Name = "ParameterReportName"
            Me.ParameterReportName.Visible = False
            '
            'DisclosuresReport
            '
            Me.Bands.AddRange(New DevExpress.XtraReports.UI.Band() {Me.TopMarginBand1, Me.Detail, Me.BottomMarginBand1, Me.PageHeader, Me.PageFooter})
            Me.Margins = New System.Drawing.Printing.Margins(26, 25, 25, 25)
            Me.Parameters.AddRange(New DevExpress.XtraReports.Parameters.Parameter() {Me.ParameterReportName})
            Me.RequestParameters = False
            Me.ScriptLanguage = DevExpress.XtraReports.ScriptLanguage.VisualBasic
            Me.ScriptReferencesString = resources.GetString("$this.ScriptReferencesString")
            Me.Scripts.OnBeforePrint = "AuthorizationReport_BeforePrint"
            Me.ScriptsSource = resources.GetString("$this.ScriptsSource")
            Me.ShowPreviewMarginLines = False
            Me.Version = "11.2"
            Me.Controls.SetChildIndex(Me.PageFooter, 0)
            Me.Controls.SetChildIndex(Me.PageHeader, 0)
            Me.Controls.SetChildIndex(Me.BottomMarginBand1, 0)
            Me.Controls.SetChildIndex(Me.Detail, 0)
            Me.Controls.SetChildIndex(Me.TopMarginBand1, 0)
            CType(Me.XrRichText1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.XrRichText2, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me, System.ComponentModel.ISupportInitialize).EndInit()

        End Sub

        Friend WithEvents XrPictureBox1 As DevExpress.XtraReports.UI.XRPictureBox
        Friend WithEvents PageHeader As DevExpress.XtraReports.UI.PageHeaderBand
        Friend WithEvents PageFooter As DevExpress.XtraReports.UI.PageFooterBand
        Friend WithEvents XrLabel_page_footer As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrRichText1 As DevExpress.XtraReports.UI.XRRichText
        Friend WithEvents XrPictureBox5 As DevExpress.XtraReports.UI.XRPictureBox
        Friend WithEvents XrPictureBox4 As DevExpress.XtraReports.UI.XRPictureBox
        Friend WithEvents XrPictureBox3 As DevExpress.XtraReports.UI.XRPictureBox
        Friend WithEvents XrPictureBox2 As DevExpress.XtraReports.UI.XRPictureBox
        Friend WithEvents ParameterReportName As DevExpress.XtraReports.Parameters.Parameter
        Friend WithEvents XrRichText2 As DevExpress.XtraReports.UI.XRRichText
    End Class
End Namespace
