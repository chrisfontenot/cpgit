#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}
#End Region

Imports DebtPlus.Reports.Template

Imports DebtPlus.Utils
Imports System.IO
Imports System.Reflection
Imports DevExpress.XtraReports.UI

Namespace Forms.Disclosure
    Public Class DisclosuresReport
        Inherits BaseXtraReportClass

        ''' <summary>
        ''' Create an instance of our report
        ''' </summary>
        Public Sub New()
            MyClass.New(String.Empty)
        End Sub

        Public Sub New(ByVal ReportDefinition As String)
            MyBase.New()
            InitializeComponent()

            '-- Split the names along the semi-colon
            Parameters("ParameterReportName").Value = String.Empty
            Dim Fields() As String = ReportDefinition.Split(";"c)
            If Fields.GetUpperBound(0) < 1 Then
                Parameters("ParameterReportName").Value = ReportDefinition.Trim()
            Else
                Parameters("ParameterReportName").Value = Fields(1).Trim()
            End If

            '-- The name of our Letter text is the 2nd parameter in the field list
            If Convert.ToString(Parameters("ParameterReportName").Value) = String.Empty Then
                Throw New ApplicationException("The entry in the reports table is not valid for this report. The report name can not be empty.")
            End If

            '-- Set the script references to the current (running) values.
            ScriptReferences = DebtPlus.Utils.AssemblyTools.GetAllSiblingAssemblies

            '-- Copy the script references to the sub-reports as well
            For Each Band As DevExpress.XtraReports.UI.Band In Bands
                For Each ctl As DevExpress.XtraReports.UI.XRControl In Band.Controls
                    Dim subReport As DevExpress.XtraReports.UI.XRSubreport = TryCast(ctl, DevExpress.XtraReports.UI.XRSubreport)
                    If subReport IsNot Nothing Then
                        Dim rpt As DevExpress.XtraReports.UI.XtraReport = subReport.ReportSource
                        If rpt IsNot Nothing Then
                            rpt.ScriptReferences = ScriptReferences
                        End If
                    End If
                Next
            Next
        End Sub

        Public Overrides Function NeedParameters() As Boolean
            Return False
        End Function

        Public Overrides ReadOnly Property ReportTitle As String
            Get
                Return "Document Text"
            End Get
        End Property

        Public Sub InitializeComponent()

            ' Name of the report file
            Const ReportName As String = "DebtPlus.Reports.Forms.Disclosure.repx"

            ' See if there is a report reference that we can use
            Dim Fname As String = Path.Combine(DebtPlus.Configuration.Config.ReportsDirectory, ReportName)
            Dim UseDefault As Boolean = True
            Try
                If (Not DebtPlus.Configuration.Config.LocalReportsOnly) AndAlso System.IO.File.Exists(Fname) Then
                    LoadLayout(Fname)
                    UseDefault = False
                End If

            Catch ex As Exception
                DebtPlus.Svc.Common.ErrorHandling.HandleErrors(ex)
            End Try

            ' Load the standard report definition if we need a new item
            If UseDefault Then
                Dim asm As Assembly = Assembly.GetExecutingAssembly                                           ' changed
                Using ios As Stream = asm.GetManifestResourceStream(String.Format("DebtPlus.Reports.{0}", ReportName)) ' changed
                    If ios IsNot Nothing Then LoadLayout(ios) '                                                                       changed
                End Using
            End If
        End Sub
    End Class
End Namespace
