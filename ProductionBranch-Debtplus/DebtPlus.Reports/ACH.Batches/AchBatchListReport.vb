#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}
#End Region

Option Compare Binary
Option Explicit On
Option Strict On

Imports DebtPlus.Reports.Template

Namespace ACH.Batches

    Public Class AchBatchListReport
        Inherits TemplateXtraReportClass

        Public Sub New()
            MyBase.New()
            InitializeComponent()
            AddHandler BeforePrint, AddressOf Report_BeforePrint

            ReportFilter.IsEnabled = True
        End Sub

        Private Const DAY_COUNT As System.Int32 = 90

        Public Overrides ReadOnly Property ReportTitle() As String
            Get
                Return "ACH Batches"
            End Get
        End Property

        Public Overrides ReadOnly Property ReportSubTitle() As String
            Get
                Return String.Format("This report shows the ACH batches which have been created in the past {0:f0} days", DAY_COUNT)
            End Get
        End Property

        Dim ds As New System.Data.DataSet("ds")

        Private Sub Report_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs)
            Dim rpt As DevExpress.XtraReports.UI.XtraReport = CType(sender, DevExpress.XtraReports.UI.XtraReport)

            Const TableName As String = "view_ach_files"
            Dim tbl As DataTable = ds.Tables(TableName)

            If tbl Is Nothing Then
                Using cmd As New System.Data.SqlClient.SqlCommand()
                    With cmd
                        .CommandText = String.Format("SELECT [ach_file], [pull_date], [effective_date], [settlement_date], [date_created], [created_by], [date_posted] FROM view_ach_files WITH (NOLOCK) WHERE [date_created] >= convert(varchar(10), dateadd(d, -{0:f0}, getdate()), 101)", DAY_COUNT)
                        .CommandType = System.Data.CommandType.Text
                        .Connection = New System.Data.SqlClient.SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
                    End With

                    Using da As New System.Data.SqlClient.SqlDataAdapter(cmd)
                        da.Fill(ds, TableName)
                        tbl = ds.Tables(TableName)

                        With tbl
                            If Not .Columns.Contains("status") Then
                                .Columns.Add("status", GetType(String), "iif([date_created] is null,'INVALID',iif([date_posted] is null,'OPEN','CLOSED'))")
                            End If
                        End With
                    End Using
                End Using
            End If

            rpt.DataSource = New System.Data.DataView(tbl, ReportFilter.ViewFilter, String.Empty, DataViewRowState.CurrentRows)
        End Sub
    End Class
End Namespace