Namespace ACH.Batches
    <Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
    Partial Class AchBatchListReport

        'Component overrides dispose to clean up the component list.
        <System.Diagnostics.DebuggerNonUserCode()> _
        Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
            If disposing Then
                If components IsNot Nothing Then
                    components.Dispose()
                End If
            End If
            MyBase.Dispose(disposing)
        End Sub

        'Required by the Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Designer
        'It can be modified using the Designer.
        'Do not modify it using the code editor.
        <System.Diagnostics.DebuggerStepThrough()> _
        Private Sub InitializeComponent()
            Me.XrPanel1 = New DevExpress.XtraReports.UI.XRPanel
            Me.XrLabel7 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel3 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel2 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel1 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel4 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel8 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel5 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_ach_file = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_date_posted = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_settlement_date = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_effective_date = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_pull_date = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_status = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_date_created = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_created_by = New DevExpress.XtraReports.UI.XRLabel
            CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
            '
            'Detail
            '
            Me.Detail.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel_ach_file, Me.XrLabel_date_posted, Me.XrLabel_settlement_date, Me.XrLabel_effective_date, Me.XrLabel_pull_date, Me.XrLabel_status, Me.XrLabel_date_created, Me.XrLabel_created_by})
            Me.Detail.HeightF = 15.0!
            '
            'PageHeader
            '
            Me.PageHeader.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrPanel1})
            Me.PageHeader.HeightF = 142.0!
            Me.PageHeader.Controls.SetChildIndex(Me.XrPanel1, 0)
            Me.PageHeader.Controls.SetChildIndex(Me.XrPanel_AgencyAddress, 0)
            Me.PageHeader.Controls.SetChildIndex(Me.XrLabel_Title, 0)
            Me.PageHeader.Controls.SetChildIndex(Me.XrLine_Title, 0)
            Me.PageHeader.Controls.SetChildIndex(Me.XrLabel_Subtitle, 0)
            Me.PageHeader.Controls.SetChildIndex(Me.XrPageInfo1, 0)
            '
            'XrLabel_Title
            '
            Me.XrLabel_Title.StylePriority.UseFont = False
            '
            'XrPageInfo_PageNumber
            '
            Me.XrPageInfo_PageNumber.StylePriority.UseTextAlignment = False
            '
            'XRLabel_Agency_Name
            '
            Me.XRLabel_Agency_Name.StylePriority.UseTextAlignment = False
            '
            'XrLabel_Agency_Address3
            '
            Me.XrLabel_Agency_Address3.StylePriority.UseFont = False
            Me.XrLabel_Agency_Address3.StylePriority.UseTextAlignment = False
            '
            'XrLabel_Agency_Address1
            '
            Me.XrLabel_Agency_Address1.StylePriority.UseFont = False
            Me.XrLabel_Agency_Address1.StylePriority.UseTextAlignment = False
            '
            'XrLabel_Agency_Phone
            '
            Me.XrLabel_Agency_Phone.StylePriority.UseFont = False
            Me.XrLabel_Agency_Phone.StylePriority.UseTextAlignment = False
            '
            'XrLabel_Agency_Address2
            '
            Me.XrLabel_Agency_Address2.StylePriority.UseFont = False
            Me.XrLabel_Agency_Address2.StylePriority.UseTextAlignment = False
            '
            'XrPanel1
            '
            Me.XrPanel1.BackColor = System.Drawing.Color.Teal
            Me.XrPanel1.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel7, Me.XrLabel3, Me.XrLabel2, Me.XrLabel1, Me.XrLabel4, Me.XrLabel8, Me.XrLabel5})
            Me.XrPanel1.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 117.0!)
            Me.XrPanel1.Name = "XrPanel1"
            Me.XrPanel1.SizeF = New System.Drawing.SizeF(800.0!, 17.0!)
            Me.XrPanel1.StyleName = "XrControlStyle_HeaderPannel"
            '
            'XrLabel7
            '
            Me.XrLabel7.LocationFloat = New DevExpress.Utils.PointFloat(600.0!, 0.0!)
            Me.XrLabel7.Name = "XrLabel7"
            Me.XrLabel7.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel7.SizeF = New System.Drawing.SizeF(200.0!, 15.0!)
            Me.XrLabel7.StylePriority.UseTextAlignment = False
            Me.XrLabel7.Text = "CREATED"
            Me.XrLabel7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
            Me.XrLabel7.WordWrap = False
            '
            'XrLabel3
            '
            Me.XrLabel3.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
            Me.XrLabel3.Name = "XrLabel3"
            Me.XrLabel3.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
            Me.XrLabel3.SizeF = New System.Drawing.SizeF(75.0!, 15.0!)
            Me.XrLabel3.StylePriority.UsePadding = False
            Me.XrLabel3.Text = "ID"
            Me.XrLabel3.WordWrap = False
            '
            'XrLabel2
            '
            Me.XrLabel2.LocationFloat = New DevExpress.Utils.PointFloat(500.0!, 0.0!)
            Me.XrLabel2.Name = "XrLabel2"
            Me.XrLabel2.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
            Me.XrLabel2.SizeF = New System.Drawing.SizeF(100.0!, 15.0!)
            Me.XrLabel2.StylePriority.UsePadding = False
            Me.XrLabel2.Text = "POSTED"
            Me.XrLabel2.WordWrap = False
            '
            'XrLabel1
            '
            Me.XrLabel1.LocationFloat = New DevExpress.Utils.PointFloat(83.0!, 0.0!)
            Me.XrLabel1.Name = "XrLabel1"
            Me.XrLabel1.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel1.SizeF = New System.Drawing.SizeF(100.0!, 15.0!)
            Me.XrLabel1.StylePriority.UseTextAlignment = False
            Me.XrLabel1.Text = "STATUS"
            Me.XrLabel1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
            Me.XrLabel1.WordWrap = False
            '
            'XrLabel4
            '
            Me.XrLabel4.LocationFloat = New DevExpress.Utils.PointFloat(400.0!, 0.0!)
            Me.XrLabel4.Name = "XrLabel4"
            Me.XrLabel4.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel4.SizeF = New System.Drawing.SizeF(100.0!, 15.0!)
            Me.XrLabel4.StylePriority.UseTextAlignment = False
            Me.XrLabel4.Text = "SETTLEMENT"
            Me.XrLabel4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            Me.XrLabel4.WordWrap = False
            '
            'XrLabel8
            '
            Me.XrLabel8.LocationFloat = New DevExpress.Utils.PointFloat(200.0!, 0.0!)
            Me.XrLabel8.Name = "XrLabel8"
            Me.XrLabel8.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
            Me.XrLabel8.SizeF = New System.Drawing.SizeF(100.0!, 15.0!)
            Me.XrLabel8.StylePriority.UsePadding = False
            Me.XrLabel8.Text = "PULL"
            Me.XrLabel8.WordWrap = False
            '
            'XrLabel5
            '
            Me.XrLabel5.LocationFloat = New DevExpress.Utils.PointFloat(300.0!, 0.0!)
            Me.XrLabel5.Name = "XrLabel5"
            Me.XrLabel5.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
            Me.XrLabel5.SizeF = New System.Drawing.SizeF(100.0!, 15.0!)
            Me.XrLabel5.StylePriority.UsePadding = False
            Me.XrLabel5.Text = "EFFECTIVE"
            Me.XrLabel5.WordWrap = False
            '
            'XrLabel_ach_file
            '
            Me.XrLabel_ach_file.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
            Me.XrLabel_ach_file.Name = "XrLabel_ach_file"
            Me.XrLabel_ach_file.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_ach_file.SizeF = New System.Drawing.SizeF(75.0!, 15.0!)
            Me.XrLabel_ach_file.StylePriority.UseTextAlignment = False
            Me.XrLabel_ach_file.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrLabel_ach_file.WordWrap = False
            '
            'XrLabel_date_posted
            '
            Me.XrLabel_date_posted.LocationFloat = New DevExpress.Utils.PointFloat(500.0!, 0.0!)
            Me.XrLabel_date_posted.Name = "XrLabel_date_posted"
            Me.XrLabel_date_posted.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_date_posted.SizeF = New System.Drawing.SizeF(100.0!, 15.0!)
            Me.XrLabel_date_posted.StylePriority.UseTextAlignment = False
            Me.XrLabel_date_posted.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrLabel_date_posted.WordWrap = False
            '
            'XrLabel_settlement_date
            '
            Me.XrLabel_settlement_date.LocationFloat = New DevExpress.Utils.PointFloat(400.0!, 0.0!)
            Me.XrLabel_settlement_date.Name = "XrLabel_settlement_date"
            Me.XrLabel_settlement_date.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_settlement_date.SizeF = New System.Drawing.SizeF(100.0!, 15.0!)
            Me.XrLabel_settlement_date.StylePriority.UseTextAlignment = False
            Me.XrLabel_settlement_date.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            Me.XrLabel_settlement_date.WordWrap = False
            '
            'XrLabel_effective_date
            '
            Me.XrLabel_effective_date.LocationFloat = New DevExpress.Utils.PointFloat(300.0!, 0.0!)
            Me.XrLabel_effective_date.Name = "XrLabel_effective_date"
            Me.XrLabel_effective_date.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_effective_date.SizeF = New System.Drawing.SizeF(100.0!, 15.0!)
            Me.XrLabel_effective_date.StylePriority.UseTextAlignment = False
            Me.XrLabel_effective_date.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrLabel_effective_date.WordWrap = False
            '
            'XrLabel_pull_date
            '
            Me.XrLabel_pull_date.LocationFloat = New DevExpress.Utils.PointFloat(200.0!, 0.0!)
            Me.XrLabel_pull_date.Name = "XrLabel_pull_date"
            Me.XrLabel_pull_date.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_pull_date.SizeF = New System.Drawing.SizeF(100.0!, 15.0!)
            Me.XrLabel_pull_date.StylePriority.UseTextAlignment = False
            Me.XrLabel_pull_date.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrLabel_pull_date.WordWrap = False
            '
            'XrLabel_status
            '
            Me.XrLabel_status.LocationFloat = New DevExpress.Utils.PointFloat(83.0!, 0.0!)
            Me.XrLabel_status.Name = "XrLabel_status"
            Me.XrLabel_status.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_status.SizeF = New System.Drawing.SizeF(100.0!, 15.0!)
            Me.XrLabel_status.StylePriority.UseTextAlignment = False
            Me.XrLabel_status.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
            Me.XrLabel_status.WordWrap = False
            '
            'XrLabel_date_created
            '
            Me.XrLabel_date_created.LocationFloat = New DevExpress.Utils.PointFloat(600.0!, 0.0!)
            Me.XrLabel_date_created.Name = "XrLabel_date_created"
            Me.XrLabel_date_created.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_date_created.SizeF = New System.Drawing.SizeF(75.0!, 15.0!)
            Me.XrLabel_date_created.StylePriority.UseTextAlignment = False
            Me.XrLabel_date_created.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrLabel_date_created.WordWrap = False
            '
            'XrLabel_created_by
            '
            Me.XrLabel_created_by.LocationFloat = New DevExpress.Utils.PointFloat(683.0!, 0.0!)
            Me.XrLabel_created_by.Name = "XrLabel_created_by"
            Me.XrLabel_created_by.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_created_by.SizeF = New System.Drawing.SizeF(117.0!, 15.0!)
            Me.XrLabel_created_by.StylePriority.UseTextAlignment = False
            Me.XrLabel_created_by.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            Me.XrLabel_created_by.WordWrap = False
            '
            Me.XrLabel_ach_file.DataBindings.Add("Text", Nothing, "ach_file", "{0:f0}")
            Me.XrLabel_created_by.DataBindings.Add("Text", Nothing, "created_by")
            Me.XrLabel_date_created.DataBindings.Add("Text", Nothing, "date_created", "{0:d}")
            Me.XrLabel_date_posted.DataBindings.Add("Text", Nothing, "date_posted", "{0:d}")
            Me.XrLabel_effective_date.DataBindings.Add("Text", Nothing, "effective_date", "{0:d}")
            Me.XrLabel_pull_date.DataBindings.Add("Text", Nothing, "pull_date", "{0:d}")
            Me.XrLabel_settlement_date.DataBindings.Add("Text", Nothing, "settlement_date")
            Me.XrLabel_status.DataBindings.Add("Text", Nothing, "status")
            '
            'ACHBatchListReport
            '
            Me.Bands.Clear()
            Me.Bands.AddRange(New DevExpress.XtraReports.UI.Band() {Me.Detail, Me.PageHeader, Me.PageFooter, Me.TopMarginBand1, Me.BottomMarginBand1})
            Me.StyleSheet.Clear()
            Me.StyleSheet.AddRange(New DevExpress.XtraReports.UI.XRControlStyle() {Me.XrControlStyle_Header, Me.XrControlStyle_Totals, Me.XrControlStyle_HeaderPannel, Me.XrControlStyle_GroupHeader})
            Me.Version = "11.1"
            CType(Me, System.ComponentModel.ISupportInitialize).EndInit()
        End Sub

        Friend WithEvents XrLabel1 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel2 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel3 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel4 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel7 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel8 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel5 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_ach_file As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_date_posted As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_settlement_date As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_effective_date As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_pull_date As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_status As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_date_created As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_created_by As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrPanel1 As DevExpress.XtraReports.UI.XRPanel
    End Class
End Namespace
