﻿Namespace Operations.Fairshare.ChangeAnalysis
    <Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
    Partial Class OperationsFairshareChangeAnalysisParametersForm
        Inherits DebtPlus.Reports.Template.Forms.CreditorParametersForm

        'Form overrides dispose to clean up the component list.
        <System.Diagnostics.DebuggerNonUserCode()> _
        Protected Overrides Sub Dispose(ByVal disposing As Boolean)
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
            MyBase.Dispose(disposing)
        End Sub

        'Required by the Windows Form Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Windows Form Designer
        'It can be modified using the Windows Form Designer.  
        'Do not modify it using the code editor.
        <System.Diagnostics.DebuggerStepThrough()> _
        Private Sub InitializeComponent()
            Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl
            Me.PercentEdit_EFT = New DebtPlus.Data.Controls.PercentEdit
            Me.PercentEdit_CHECK = New DebtPlus.Data.Controls.PercentEdit
            Me.LabelControl2 = New DevExpress.XtraEditors.LabelControl
            CType(Me.XrCreditor_param_03_1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.DxErrorProvider1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.PercentEdit_EFT.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.PercentEdit_CHECK.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.SuspendLayout()
            '
            'Label1
            '
            Me.Label1.Location = New System.Drawing.Point(16, 21)
            '
            'XrCreditor_param_03_1
            '
            Me.XrCreditor_param_03_1.Location = New System.Drawing.Point(99, 18)
            Me.XrCreditor_param_03_1.Properties.Mask.AutoComplete = DevExpress.XtraEditors.Mask.AutoCompleteType.Strong
            Me.XrCreditor_param_03_1.Properties.Mask.BeepOnError = True
            Me.XrCreditor_param_03_1.Properties.Mask.EditMask = "[A-Z]{1,2}[0-9]{4,}"
            Me.XrCreditor_param_03_1.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx
            Me.XrCreditor_param_03_1.Properties.Mask.UseMaskAsDisplayFormat = True
            '
            'ButtonCancel
            '
            Me.ButtonCancel.Location = New System.Drawing.Point(244, 44)
            Me.ButtonCancel.TabIndex = 7
            '
            'ButtonOK
            '
            Me.ButtonOK.Location = New System.Drawing.Point(244, 12)
            Me.ButtonOK.TabIndex = 6
            '
            'LabelControl1
            '
            Me.LabelControl1.Location = New System.Drawing.Point(16, 47)
            Me.LabelControl1.Name = "LabelControl1"
            Me.LabelControl1.Size = New System.Drawing.Size(32, 13)
            Me.LabelControl1.TabIndex = 2
            Me.LabelControl1.Text = "EFT %"
            '
            'PercentEdit_EFT
            '
            Me.PercentEdit_EFT.Location = New System.Drawing.Point(99, 44)
            Me.PercentEdit_EFT.Name = "PercentEdit_EFT"
            Me.PercentEdit_EFT.Properties.Allow100Percent = False
            Me.PercentEdit_EFT.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.PercentEdit_EFT.Properties.DisplayFormat.FormatString = "{0:p3}"
            Me.PercentEdit_EFT.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.PercentEdit_EFT.Properties.EditFormat.FormatString = "{0:f3}"
            Me.PercentEdit_EFT.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.PercentEdit_EFT.Properties.Precision = 3
            Me.PercentEdit_EFT.Size = New System.Drawing.Size(86, 20)
            Me.PercentEdit_EFT.TabIndex = 3
            '
            'PercentEdit_CHECK
            '
            Me.PercentEdit_CHECK.Location = New System.Drawing.Point(99, 70)
            Me.PercentEdit_CHECK.Name = "PercentEdit_CHECK"
            Me.PercentEdit_CHECK.Properties.Allow100Percent = False
            Me.PercentEdit_CHECK.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.PercentEdit_CHECK.Properties.DisplayFormat.FormatString = "{0:p3}"
            Me.PercentEdit_CHECK.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.PercentEdit_CHECK.Properties.EditFormat.FormatString = "{0:f3}"
            Me.PercentEdit_CHECK.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.PercentEdit_CHECK.Properties.Precision = 3
            Me.PercentEdit_CHECK.Size = New System.Drawing.Size(86, 20)
            Me.PercentEdit_CHECK.TabIndex = 5
            '
            'LabelControl2
            '
            Me.LabelControl2.Location = New System.Drawing.Point(16, 73)
            Me.LabelControl2.Name = "LabelControl2"
            Me.LabelControl2.Size = New System.Drawing.Size(47, 13)
            Me.LabelControl2.TabIndex = 4
            Me.LabelControl2.Text = "CHECK %"
            '
            'ParametersForm
            '
            Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
            Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
            Me.ClientSize = New System.Drawing.Size(336, 135)
            Me.Controls.Add(Me.PercentEdit_CHECK)
            Me.Controls.Add(Me.LabelControl2)
            Me.Controls.Add(Me.PercentEdit_EFT)
            Me.Controls.Add(Me.LabelControl1)
            Me.Name = "ParametersForm"
            Me.Controls.SetChildIndex(Me.LabelControl1, 0)
            Me.Controls.SetChildIndex(Me.PercentEdit_EFT, 0)
            Me.Controls.SetChildIndex(Me.LabelControl2, 0)
            Me.Controls.SetChildIndex(Me.PercentEdit_CHECK, 0)
            Me.Controls.SetChildIndex(Me.ButtonOK, 0)
            Me.Controls.SetChildIndex(Me.ButtonCancel, 0)
            Me.Controls.SetChildIndex(Me.XrCreditor_param_03_1, 0)
            Me.Controls.SetChildIndex(Me.Label1, 0)
            Me.Controls.SetChildIndex(Me.LabelControl1, 0)
            CType(Me.XrCreditor_param_03_1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.DxErrorProvider1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.PercentEdit_EFT.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.PercentEdit_CHECK.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            Me.ResumeLayout(False)
            Me.PerformLayout()

        End Sub
        Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents PercentEdit_EFT As DebtPlus.Data.Controls.PercentEdit
        Friend WithEvents PercentEdit_CHECK As DebtPlus.Data.Controls.PercentEdit
        Friend WithEvents LabelControl2 As DevExpress.XtraEditors.LabelControl
    End Class
End Namespace