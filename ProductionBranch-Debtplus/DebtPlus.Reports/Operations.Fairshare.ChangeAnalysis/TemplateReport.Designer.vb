Namespace Operations.Fairshare.ChangeAnalysis
    <Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
    Partial Class TemplateReport

        'XtraReport overrides dispose to clean up the component list.
        <System.Diagnostics.DebuggerNonUserCode()> _
        Protected Overrides Sub Dispose(ByVal disposing As Boolean)
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
            MyBase.Dispose(disposing)
        End Sub

        'Required by the Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Designer
        'It can be modified using the Designer.  
        'Do not modify it using the code editor.
        <System.Diagnostics.DebuggerStepThrough()> _
        Private Sub InitializeComponent()
            Me.XrTable1 = New DevExpress.XtraReports.UI.XRTable
            Me.XrTableRow1 = New DevExpress.XtraReports.UI.XRTableRow
            Me.XrTableCell1 = New DevExpress.XtraReports.UI.XRTableCell
            Me.XrTableCell2 = New DevExpress.XtraReports.UI.XRTableCell
            Me.ParameterFairshareCheck = New DevExpress.XtraReports.Parameters.Parameter
            Me.XrTableCell4 = New DevExpress.XtraReports.UI.XRTableCell
            Me.XrTableRow3 = New DevExpress.XtraReports.UI.XRTableRow
            Me.XrTableCell9 = New DevExpress.XtraReports.UI.XRTableCell
            Me.XrTableCell10 = New DevExpress.XtraReports.UI.XRTableCell
            Me.ParameterFairshareEFT = New DevExpress.XtraReports.Parameters.Parameter
            Me.XrTableCell11 = New DevExpress.XtraReports.UI.XRTableCell
            Me.XrTableCell_creditor_type = New DevExpress.XtraReports.UI.XRTableCell
            Me.XrTableRow2 = New DevExpress.XtraReports.UI.XRTableRow
            Me.XrTableCell5 = New DevExpress.XtraReports.UI.XRTableCell
            Me.XrTableCell6 = New DevExpress.XtraReports.UI.XRTableCell
            Me.ParameterCreditor = New DevExpress.XtraReports.Parameters.Parameter
            Me.XrTableCell_creditor_name = New DevExpress.XtraReports.UI.XRTableCell
            Me.XrPanel1 = New DevExpress.XtraReports.UI.XRPanel
            Me.XrLabel7 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel6 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel5 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel4 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel3 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel2 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel1 = New DevExpress.XtraReports.UI.XRLabel
            Me.CalculatedFieldDisplayDate = New DevExpress.XtraReports.UI.CalculatedField
            CType(Me.XrTable1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
            '
            'Detail
            '
            Me.Detail.HeightF = 15.0!
            '
            'PageHeader
            '
            Me.PageHeader.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrPanel1, Me.XrTable1})
            Me.PageHeader.HeightF = 202.0!
            Me.PageHeader.Controls.SetChildIndex(Me.XrPanel_AgencyAddress, 0)
            Me.PageHeader.Controls.SetChildIndex(Me.XrLabel_Title, 0)
            Me.PageHeader.Controls.SetChildIndex(Me.XrLine_Title, 0)
            Me.PageHeader.Controls.SetChildIndex(Me.XrLabel_Subtitle, 0)
            Me.PageHeader.Controls.SetChildIndex(Me.XrPageInfo1, 0)
            Me.PageHeader.Controls.SetChildIndex(Me.XrTable1, 0)
            Me.PageHeader.Controls.SetChildIndex(Me.XrPanel1, 0)
            '
            'XrLabel_Title
            '
            Me.XrLabel_Title.StylePriority.UseFont = False
            '
            'XrLabel_Subtitle
            '
            Me.XrLabel_Subtitle.LocationFloat = New DevExpress.Utils.PointFloat(236.7916!, 67.00001!)
            Me.XrLabel_Subtitle.SizeF = New System.Drawing.SizeF(57.29172!, 17.0!)
            Me.XrLabel_Subtitle.Visible = False
            '
            'XrPageInfo_PageNumber
            '
            Me.XrPageInfo_PageNumber.StylePriority.UseTextAlignment = False
            '
            'XRLabel_Agency_Name
            '
            Me.XRLabel_Agency_Name.StylePriority.UseTextAlignment = False
            '
            'XrLabel_Agency_Address3
            '
            Me.XrLabel_Agency_Address3.StylePriority.UseFont = False
            Me.XrLabel_Agency_Address3.StylePriority.UseTextAlignment = False
            '
            'XrLabel_Agency_Address1
            '
            Me.XrLabel_Agency_Address1.StylePriority.UseFont = False
            Me.XrLabel_Agency_Address1.StylePriority.UseTextAlignment = False
            '
            'XrLabel_Agency_Phone
            '
            Me.XrLabel_Agency_Phone.StylePriority.UseFont = False
            Me.XrLabel_Agency_Phone.StylePriority.UseTextAlignment = False
            '
            'XrLabel_Agency_Address2
            '
            Me.XrLabel_Agency_Address2.StylePriority.UseFont = False
            Me.XrLabel_Agency_Address2.StylePriority.UseTextAlignment = False
            '
            'XrTable1
            '
            Me.XrTable1.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 110.0!)
            Me.XrTable1.Name = "XrTable1"
            Me.XrTable1.Padding = New DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 100.0!)
            Me.XrTable1.Rows.AddRange(New DevExpress.XtraReports.UI.XRTableRow() {Me.XrTableRow1, Me.XrTableRow3, Me.XrTableRow2})
            Me.XrTable1.SizeF = New System.Drawing.SizeF(638.6248!, 45.0!)
            Me.XrTable1.StyleName = "XrControlStyle_Header"
            Me.XrTable1.StylePriority.UsePadding = False
            '
            'XrTableRow1
            '
            Me.XrTableRow1.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.XrTableCell1, Me.XrTableCell2, Me.XrTableCell4})
            Me.XrTableRow1.Name = "XrTableRow1"
            Me.XrTableRow1.Weight = 0.86666664501744828
            '
            'XrTableCell1
            '
            Me.XrTableCell1.Name = "XrTableCell1"
            Me.XrTableCell1.Text = "New Fairshare for CHECKS:"
            Me.XrTableCell1.Weight = 0.9816909096005465
            '
            'XrTableCell2
            '
            Me.XrTableCell2.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding(Me.ParameterFairshareCheck, "Text", "{0:p}")})
            Me.XrTableCell2.Name = "XrTableCell2"
            Me.XrTableCell2.Text = "XrTableCell2"
            Me.XrTableCell2.Weight = 0.46617517952612442
            '
            'ParameterFairshareCheck
            '
            Me.ParameterFairshareCheck.Description = "Check %"
            Me.ParameterFairshareCheck.Name = "ParameterFairshareCheck"
            Me.ParameterFairshareCheck.Type = GetType(System.[Double])
            Me.ParameterFairshareCheck.Value = 0
            Me.ParameterFairshareCheck.Visible = False
            '
            'XrTableCell4
            '
            Me.XrTableCell4.Name = "XrTableCell4"
            Me.XrTableCell4.Weight = 1.5521339108733292
            '
            'XrTableRow3
            '
            Me.XrTableRow3.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.XrTableCell9, Me.XrTableCell10, Me.XrTableCell11, Me.XrTableCell_creditor_type})
            Me.XrTableRow3.Name = "XrTableRow3"
            Me.XrTableRow3.Weight = 0.86666659806732382
            '
            'XrTableCell9
            '
            Me.XrTableCell9.Name = "XrTableCell9"
            Me.XrTableCell9.Text = "New Fairshare for EFT:"
            Me.XrTableCell9.Weight = 0.98169103243397215
            '
            'XrTableCell10
            '
            Me.XrTableCell10.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding(Me.ParameterFairshareEFT, "Text", "{0:p}")})
            Me.XrTableCell10.Name = "XrTableCell10"
            Me.XrTableCell10.Text = "XrTableCell10"
            Me.XrTableCell10.Weight = 0.46617495839861889
            '
            'ParameterFairshareEFT
            '
            Me.ParameterFairshareEFT.Description = "EFT %"
            Me.ParameterFairshareEFT.Name = "ParameterFairshareEFT"
            Me.ParameterFairshareEFT.Type = GetType(System.[Double])
            Me.ParameterFairshareEFT.Value = 0
            Me.ParameterFairshareEFT.Visible = False
            '
            'XrTableCell11
            '
            Me.XrTableCell11.Name = "XrTableCell11"
            Me.XrTableCell11.Text = "Normal fairshare Method:"
            Me.XrTableCell11.Weight = 0.85395496579354946
            '
            'XrTableCell_creditor_type
            '
            Me.XrTableCell_creditor_type.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "creditor_type")})
            Me.XrTableCell_creditor_type.Name = "XrTableCell_creditor_type"
            Me.XrTableCell_creditor_type.Weight = 0.69817904337385972
            '
            'XrTableRow2
            '
            Me.XrTableRow2.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.XrTableCell5, Me.XrTableCell6, Me.XrTableCell_creditor_name})
            Me.XrTableRow2.Name = "XrTableRow2"
            Me.XrTableRow2.Weight = 0.86666664501745283
            '
            'XrTableCell5
            '
            Me.XrTableCell5.Name = "XrTableCell5"
            Me.XrTableCell5.Text = "Creditor Effected by "
            Me.XrTableCell5.Weight = 0.9816908460164383
            '
            'XrTableCell6
            '
            Me.XrTableCell6.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding(Me.ParameterCreditor, "Text", "")})
            Me.XrTableCell6.Name = "XrTableCell6"
            Me.XrTableCell6.Text = "XrTableCell6"
            Me.XrTableCell6.Weight = 0.46617525120573389
            '
            'ParameterCreditor
            '
            Me.ParameterCreditor.Description = "Creditor ID"
            Me.ParameterCreditor.Name = "ParameterCreditor"
            Me.ParameterCreditor.Value = ""
            '
            'XrTableCell_creditor_name
            '
            Me.XrTableCell_creditor_name.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "creditor_name")})
            Me.XrTableCell_creditor_name.Name = "XrTableCell_creditor_name"
            Me.XrTableCell_creditor_name.Weight = 1.5521339027778278
            '
            'XrPanel1
            '
            Me.XrPanel1.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel7, Me.XrLabel6, Me.XrLabel5, Me.XrLabel4, Me.XrLabel3, Me.XrLabel2, Me.XrLabel1})
            Me.XrPanel1.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 170.0!)
            Me.XrPanel1.Name = "XrPanel1"
            Me.XrPanel1.SizeF = New System.Drawing.SizeF(749.9999!, 17.70836!)
            Me.XrPanel1.StyleName = "XrControlStyle_HeaderPannel"
            '
            'XrLabel7
            '
            Me.XrLabel7.LocationFloat = New DevExpress.Utils.PointFloat(600.0!, 1.0!)
            Me.XrLabel7.Name = "XrLabel7"
            Me.XrLabel7.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel7.SizeF = New System.Drawing.SizeF(100.0!, 15.0!)
            Me.XrLabel7.Text = "VARIANCE"
            '
            'XrLabel6
            '
            Me.XrLabel6.LocationFloat = New DevExpress.Utils.PointFloat(500.0!, 1.0!)
            Me.XrLabel6.Name = "XrLabel6"
            Me.XrLabel6.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel6.SizeF = New System.Drawing.SizeF(100.0!, 15.0!)
            Me.XrLabel6.Text = "NEW AMT"
            '
            'XrLabel5
            '
            Me.XrLabel5.LocationFloat = New DevExpress.Utils.PointFloat(400.0!, 1.0!)
            Me.XrLabel5.Name = "XrLabel5"
            Me.XrLabel5.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel5.SizeF = New System.Drawing.SizeF(100.0!, 15.0!)
            Me.XrLabel5.Text = "NEW PCT"
            '
            'XrLabel4
            '
            Me.XrLabel4.LocationFloat = New DevExpress.Utils.PointFloat(300.0!, 1.0!)
            Me.XrLabel4.Name = "XrLabel4"
            Me.XrLabel4.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel4.SizeF = New System.Drawing.SizeF(100.0!, 15.0!)
            Me.XrLabel4.Text = "OLD AMT"
            '
            'XrLabel3
            '
            Me.XrLabel3.LocationFloat = New DevExpress.Utils.PointFloat(200.0!, 1.0!)
            Me.XrLabel3.Name = "XrLabel3"
            Me.XrLabel3.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel3.SizeF = New System.Drawing.SizeF(100.0!, 15.0!)
            Me.XrLabel3.Text = "OLD PCT"
            '
            'XrLabel2
            '
            Me.XrLabel2.LocationFloat = New DevExpress.Utils.PointFloat(100.0!, 1.0!)
            Me.XrLabel2.Name = "XrLabel2"
            Me.XrLabel2.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel2.SizeF = New System.Drawing.SizeF(100.0!, 15.0!)
            Me.XrLabel2.Text = "DISBURSED"
            '
            'XrLabel1
            '
            Me.XrLabel1.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 1.0!)
            Me.XrLabel1.Name = "XrLabel1"
            Me.XrLabel1.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel1.SizeF = New System.Drawing.SizeF(100.0!, 15.0!)
            Me.XrLabel1.Text = "DATE"
            '
            'CalculatedFieldDisplayDate
            '
            Me.CalculatedFieldDisplayDate.DisplayName = "DisplayDate"
            Me.CalculatedFieldDisplayDate.FieldType = DevExpress.XtraReports.UI.FieldType.DateTime
            Me.CalculatedFieldDisplayDate.Name = "CalculatedFieldDisplayDate"
            '
            'TemplateReport
            '
            Me.Bands.AddRange(New DevExpress.XtraReports.UI.Band() {Me.Detail, Me.PageHeader, Me.PageFooter, Me.TopMarginBand1, Me.BottomMarginBand1})
            Me.CalculatedFields.AddRange(New DevExpress.XtraReports.UI.CalculatedField() {Me.CalculatedFieldDisplayDate})
            Me.Parameters.AddRange(New DevExpress.XtraReports.Parameters.Parameter() {Me.ParameterCreditor, Me.ParameterFairshareCheck, Me.ParameterFairshareEFT})
            Me.RequestParameters = False
            Me.ScriptLanguage = DevExpress.XtraReports.ScriptLanguage.VisualBasic
            Me.StyleSheet.AddRange(New DevExpress.XtraReports.UI.XRControlStyle() {Me.XrControlStyle_Header, Me.XrControlStyle_Totals, Me.XrControlStyle_HeaderPannel, Me.XrControlStyle_GroupHeader})
            Me.Version = "10.1"
            CType(Me.XrTable1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me, System.ComponentModel.ISupportInitialize).EndInit()

        End Sub
        Protected Friend WithEvents XrTable1 As DevExpress.XtraReports.UI.XRTable
        Protected Friend WithEvents XrTableRow1 As DevExpress.XtraReports.UI.XRTableRow
        Protected Friend WithEvents XrTableCell1 As DevExpress.XtraReports.UI.XRTableCell
        Protected Friend WithEvents XrTableCell2 As DevExpress.XtraReports.UI.XRTableCell
        Protected Friend WithEvents XrTableCell4 As DevExpress.XtraReports.UI.XRTableCell
        Protected Friend WithEvents XrTableRow3 As DevExpress.XtraReports.UI.XRTableRow
        Protected Friend WithEvents XrTableCell9 As DevExpress.XtraReports.UI.XRTableCell
        Protected Friend WithEvents XrTableCell10 As DevExpress.XtraReports.UI.XRTableCell
        Protected Friend WithEvents XrTableCell11 As DevExpress.XtraReports.UI.XRTableCell
        Protected Friend WithEvents XrTableCell_creditor_type As DevExpress.XtraReports.UI.XRTableCell
        Protected Friend WithEvents XrTableRow2 As DevExpress.XtraReports.UI.XRTableRow
        Protected Friend WithEvents XrTableCell5 As DevExpress.XtraReports.UI.XRTableCell
        Protected Friend WithEvents XrTableCell6 As DevExpress.XtraReports.UI.XRTableCell
        Protected Friend WithEvents XrTableCell_creditor_name As DevExpress.XtraReports.UI.XRTableCell
        Protected Friend WithEvents XrPanel1 As DevExpress.XtraReports.UI.XRPanel
        Protected Friend WithEvents XrLabel7 As DevExpress.XtraReports.UI.XRLabel
        Protected Friend WithEvents XrLabel6 As DevExpress.XtraReports.UI.XRLabel
        Protected Friend WithEvents XrLabel5 As DevExpress.XtraReports.UI.XRLabel
        Protected Friend WithEvents XrLabel4 As DevExpress.XtraReports.UI.XRLabel
        Protected Friend WithEvents XrLabel3 As DevExpress.XtraReports.UI.XRLabel
        Protected Friend WithEvents XrLabel2 As DevExpress.XtraReports.UI.XRLabel
        Protected Friend WithEvents XrLabel1 As DevExpress.XtraReports.UI.XRLabel
        Protected Friend WithEvents ParameterCreditor As DevExpress.XtraReports.Parameters.Parameter
        Protected Friend WithEvents ParameterFairshareCheck As DevExpress.XtraReports.Parameters.Parameter
        Protected Friend WithEvents ParameterFairshareEFT As DevExpress.XtraReports.Parameters.Parameter
        Friend WithEvents CalculatedFieldDisplayDate As DevExpress.XtraReports.UI.CalculatedField
    End Class
End Namespace
