#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}

#End Region

Imports System.ComponentModel
Imports DebtPlus.Interfaces.Creditor

Imports DevExpress.XtraReports.UI
Imports System.Threading
Imports DebtPlus.Reports.Template

Namespace Operations.Fairshare.ChangeAnalysis
    Public Class FairshareChangeAnalysisReport
        Implements ICreditor

        Public Sub New()
            MyBase.new()
            InitializeComponent()
            AddHandler XrLabel_date_created.PreviewClick, AddressOf Field_PreviewClick
            AddHandler XrLabel_group_debit_amt.PreviewClick, AddressOf Field_PreviewClick
            AddHandler XrLabel_group_new_fairshare_amt.PreviewClick, AddressOf Field_PreviewClick
            AddHandler XrLabel_group_new_fairshare_pct.PreviewClick, AddressOf Field_PreviewClick
            AddHandler XrLabel_group_old_fairshare_amt.PreviewClick, AddressOf Field_PreviewClick
            AddHandler XrLabel_group_old_fairshare_pct.PreviewClick, AddressOf Field_PreviewClick
            AddHandler XrLabel_group_variance.PreviewClick, AddressOf Field_PreviewClick
        End Sub

        Public Property Parameter_Creditor() As String Implements ICreditor.Creditor
            Get
                Return ParameterCreditor.Value
            End Get
            Set(ByVal value As String)
                ParameterCreditor.Value = value
            End Set
        End Property

        Public Property Parameter_FairshareEFT() As Double
            Get
                Return ParameterFairshareEFT.Value
            End Get
            Set(ByVal value As Double)
                ParameterFairshareEFT.Value = value
            End Set
        End Property

        Public Property Parameter_FairshareCHECK() As Double
            Get
                Return ParameterFairshareCheck.Value
            End Get
            Set(ByVal value As Double)
                ParameterFairshareCheck.Value = value
            End Set
        End Property

        Public Overrides Sub SetReportParameter(ByVal Parameter As String, ByVal Value As Object)
            Select Case Parameter
                Case "Creditor"
                    Parameter_Creditor = Value
                Case "FairshareCheck"
                    Parameter_FairshareCHECK = Value
                Case "FairshareEFT"
                    Parameter_FairshareEFT = Value
                Case Else
                    MyBase.SetReportParameter(Parameter, Value)
            End Select
        End Sub

        Public Overrides Function NeedParameters() As Boolean
            Return MyBase.NeedParameters() OrElse ParameterCreditor.Value = String.Empty
        End Function

        Public Overrides Function RequestReportParameters() As DialogResult
            Dim answer As DialogResult = DialogResult.OK
            If NeedParameters() Then
                Using frm As New OperationsFairshareChangeAnalysisParametersForm()
                    answer = frm.ShowDialog()
                    Parameter_Creditor = frm.Parameter_Creditor
                    Parameter_FairshareCHECK = frm.Parameter_FairshareCHECK
                    Parameter_FairshareEFT = frm.Parameter_FairshareEFT
                End Using
            End If
            Return answer
        End Function

        Private Sub Field_PreviewClick(ByVal sender As Object, ByVal e As PreviewMouseEventArgs)
            Dim ItemDate As DateTime = CType(e.Brick.Value, DateTime)

            ' Generate a new view for just this data
            Dim StartingDate As DateTime = New Date(ItemDate.Year, ItemDate.Month, 1)
            Dim EndingDate As DateTime = StartingDate.AddMonths(1)

            Dim tbl As DataTable = ds.Tables(TableName)
            Dim vue As New DataView(tbl, String.Format("[date_created]>='{0:d}' AND [date_created]<'{1:d}'", StartingDate, EndingDate), "date_created", DataViewRowState.CurrentRows)
            If vue.Count > 0 Then
                Dim thrd As New Thread(New ParameterizedThreadStart(AddressOf Preview_Thread))
                thrd.SetApartmentState(ApartmentState.STA)
                thrd.Start(vue)
            End If
        End Sub

        Private Sub Preview_Thread(ByVal obj As Object)

            Dim vue As DataView = CType(obj, DataView)
            Using rpt As New DetailReport(vue)

                ' Pass along the report parameters
                rpt.Parameters("ParameterCreditor").Value = ParameterCreditor.Value
                rpt.Parameters("ParameterFairshareCheck").Value = ParameterFairshareCheck.Value
                rpt.Parameters("ParameterFairshareEFT").Value = ParameterFairshareEFT.Value

                ' Show the subreport
                CType(rpt, BaseXtraReportClass).DisplayPreviewDialog()
            End Using
        End Sub
    End Class
End Namespace