#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}

#End Region

Imports System.ComponentModel
Imports DebtPlus.UI.Creditor.Widgets.Controls
Imports DebtPlus.Data.Controls
Imports DebtPlus.Reports.Template

Imports DevExpress.XtraReports.UI
Imports System.Drawing.Printing
Imports DevExpress.XtraReports.Parameters
Imports System.Data.SqlClient

Namespace Operations.Fairshare.ChangeAnalysis
    Public Class TemplateReport
        Inherits TemplateXtraReportClass

        Protected Const TableName As String = "rpt_fairshare_change_analysis"

        Public Sub New()
            MyBase.New()
            InitializeComponent()
            AddHandler CalculatedFieldDisplayDate.GetValue, AddressOf CalculatedFieldDisplayDate_GetValue
            AddHandler Me.BeforePrint, AddressOf TemplateReport_BeforePrint
            AddHandler Me.ParametersRequestBeforeShow, AddressOf TemplateReport_ParametersRequestBeforeShow
        End Sub

        Protected Sub CalculatedFieldDisplayDate_GetValue(ByVal sender As Object, ByVal e As GetValueEventArgs)
            Dim NewDate As DateTime = CType(GetCurrentColumnValue("date_created"), DateTime)
            e.Value = New Date(NewDate.Year, NewDate.Month, 1)
        End Sub

        Public Overrides ReadOnly Property ReportTitle() As String
            Get
                Return "Fairshare Change Analysis"
            End Get
        End Property

        Protected Sub TemplateReport_BeforePrint(ByVal sender As Object, ByVal e As PrintEventArgs)
            RefreshData()
        End Sub

        Private Sub TemplateReport_ParametersRequestBeforeShow(ByVal sender As Object, ByVal e As ParametersRequestEventArgs)
            For Each pi As ParameterInfo In e.ParametersInformation
                If pi.Parameter.Type Is GetType(Double) Then
                    pi.Editor = New PercentEdit
                ElseIf pi.Parameter.Name = "ParameterCreditor" Then
                    pi.Editor = New CreditorID
                End If
            Next
        End Sub

        Protected Overridable Sub RefreshData()
            Dim tbl As DataTable = ds.Tables(TableName)
            If tbl IsNot Nothing Then
                tbl.Clear()
                tbl = Nothing
            End If

            Dim cn As SqlConnection = New System.Data.SqlClient.SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
            Try
                cn.Open()
                Using cmd As SqlCommand = New System.Data.SqlClient.SqlCommand()
                    With cmd
                        .Connection = cn
                        .CommandText = TableName
                        .CommandType = CommandType.StoredProcedure
                        .Parameters.Add("@creditor", SqlDbType.VarChar, 20).Value = ParameterCreditor.Value
                        .Parameters.Add("@new_fairshare_check", SqlDbType.Float).Value = ParameterFairshareCheck.Value
                        .Parameters.Add("@new_fairshare_eft", SqlDbType.Float).Value = ParameterFairshareEFT.Value
                    End With

                    Using da As New SqlDataAdapter(cmd)
                        da.Fill(ds, TableName)
                        tbl = ds.Tables(TableName)
                    End Using
                End Using

            Catch ex As SqlException
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error reading report information")

            Finally
                If cn IsNot Nothing Then cn.Dispose()
            End Try

            If tbl IsNot Nothing Then
                With tbl
                    If Not .Columns.Contains("variance") Then
                        .Columns.Add("variance", GetType(Decimal), "[new_fairshare_amt]-[old_fairshare_amt]")
                    End If
                End With
                DataSource = tbl.DefaultView

                For Each fld As CalculatedField In CalculatedFields
                    fld.Assign(DataSource, DataMember)
                Next
            End If
        End Sub
    End Class

    Friend Module Globals
        Friend ds As New DataSet("ds")
    End Module
End Namespace