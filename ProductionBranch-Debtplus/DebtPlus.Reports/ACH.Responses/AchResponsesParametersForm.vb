#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}
#End Region

Option Compare Binary
Option Explicit On 
Option Strict On

Imports DebtPlus.Reports.Template.Forms

Namespace ACH.Responses

    Public Class AchResponsesParametersForm
        Inherits ReportParametersForm
        Private _Parameter_DepositBatchID As System.Int32 = 0


        Public Sub New()
            MyBase.New()
            InitializeComponent()
            AddHandler Me.Load, AddressOf RequestParametersForm_Load
            AddHandler TextEdit_deposit_batch_id.GotFocus, AddressOf TextEdit_deposit_batch_id_GotFocus
        End Sub

        ''' <summary>
        ''' Deposit batch ID
        ''' </summary>
        Public Property Parameter_DepositBatchID() As System.Int32
            Get
                Return _Parameter_DepositBatchID
            End Get

            Set(ByVal Value As System.Int32)
                _Parameter_DepositBatchID = Value
            End Set
        End Property

#Region " Windows Form Designer generated code "

        'Form overrides dispose to clean up the component list.
        Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
            If disposing Then
                If components IsNot Nothing Then
                    components.Dispose()
                End If
            End If
            MyBase.Dispose(disposing)
        End Sub

        'Required by the Windows Form Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Windows Form Designer
        'It can be modified using the Windows Form Designer.  
        'Do not modify it using the code editor.
        Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
        Friend WithEvents Label1 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents RadioButton_Text As System.Windows.Forms.RadioButton
        Friend WithEvents RadioButton_List As System.Windows.Forms.RadioButton
        Friend WithEvents TextEdit_deposit_batch_id As DevExpress.XtraEditors.TextEdit
        Friend WithEvents LookUpEdit_Deposit As DevExpress.XtraEditors.LookUpEdit
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
            Me.GroupBox1 = New System.Windows.Forms.GroupBox
            Me.Label1 = New DevExpress.XtraEditors.LabelControl
            Me.RadioButton_Text = New System.Windows.Forms.RadioButton
            Me.RadioButton_List = New System.Windows.Forms.RadioButton
            Me.TextEdit_deposit_batch_id = New DevExpress.XtraEditors.TextEdit
            Me.LookUpEdit_Deposit = New DevExpress.XtraEditors.LookUpEdit
            CType(Me.DxErrorProvider1, System.ComponentModel.ISupportInitialize).BeginInit()

            Me.GroupBox1.SuspendLayout()
            CType(Me.TextEdit_deposit_batch_id.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LookUpEdit_Deposit.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.SuspendLayout()
            '
            'ButtonOK
            '
            Me.ButtonOK.Location = New System.Drawing.Point(248, 18)
            Me.ButtonOK.Size = New System.Drawing.Size(80, 28)
            '
            'ButtonCancel
            '
            Me.ButtonCancel.Location = New System.Drawing.Point(248, 56)
            Me.ButtonCancel.Size = New System.Drawing.Size(80, 28)

            '
            'GroupBox1
            '
            Me.GroupBox1.Controls.Add(Me.Label1)
            Me.GroupBox1.Controls.Add(Me.RadioButton_Text)
            Me.GroupBox1.Controls.Add(Me.RadioButton_List)
            Me.GroupBox1.Controls.Add(Me.TextEdit_deposit_batch_id)
            Me.GroupBox1.Controls.Add(Me.LookUpEdit_Deposit)
            Me.GroupBox1.Location = New System.Drawing.Point(8, 9)
            Me.GroupBox1.Name = "GroupBox1"
            Me.GroupBox1.Size = New System.Drawing.Size(232, 112)
            Me.ToolTipController1.SetSuperTip(Me.GroupBox1, Nothing)
            Me.GroupBox1.TabIndex = 1
            Me.GroupBox1.TabStop = False
            Me.GroupBox1.Text = " ACH Deposit Batch ID  "
            '
            'Label1
            '
            Me.Label1.Appearance.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.Label1.Appearance.Options.UseFont = True
            Me.Label1.Location = New System.Drawing.Point(32, 54)
            Me.Label1.Name = "Label1"
            Me.Label1.Size = New System.Drawing.Size(122, 13)
            Me.Label1.TabIndex = 2
            Me.Label1.Text = "Or enter a specific ID"
            '
            'RadioButton_Text
            '
            Me.RadioButton_Text.AllowDrop = True
            Me.RadioButton_Text.Location = New System.Drawing.Point(8, 78)
            Me.RadioButton_Text.Name = "RadioButton_Text"
            Me.RadioButton_Text.Size = New System.Drawing.Size(16, 17)
            Me.ToolTipController1.SetSuperTip(Me.RadioButton_Text, Nothing)
            Me.RadioButton_Text.TabIndex = 3
            Me.RadioButton_Text.Tag = "text"
            Me.RadioButton_Text.TextAlign = System.Drawing.ContentAlignment.MiddleRight
            '
            'RadioButton_List
            '
            Me.RadioButton_List.Checked = True
            Me.RadioButton_List.Location = New System.Drawing.Point(8, 28)
            Me.RadioButton_List.Name = "RadioButton_List"
            Me.RadioButton_List.Size = New System.Drawing.Size(16, 17)
            Me.ToolTipController1.SetSuperTip(Me.RadioButton_List, Nothing)
            Me.RadioButton_List.TabIndex = 0
            Me.RadioButton_List.TabStop = True
            Me.RadioButton_List.Tag = "list"
            '
            'TextEdit_deposit_batch_id
            '
            Me.TextEdit_deposit_batch_id.EditValue = ""
            Me.TextEdit_deposit_batch_id.Location = New System.Drawing.Point(32, 78)
            Me.TextEdit_deposit_batch_id.Name = "TextEdit_deposit_batch_id"
            Me.TextEdit_deposit_batch_id.Properties.Appearance.Options.UseTextOptions = True
            Me.TextEdit_deposit_batch_id.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.TextEdit_deposit_batch_id.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
            Me.TextEdit_deposit_batch_id.Properties.DisplayFormat.FormatString = "f0"
            Me.TextEdit_deposit_batch_id.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.TextEdit_deposit_batch_id.Properties.EditFormat.FormatString = "f0"
            Me.TextEdit_deposit_batch_id.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.TextEdit_deposit_batch_id.Properties.Mask.BeepOnError = True
            Me.TextEdit_deposit_batch_id.Properties.Mask.EditMask = "f0"
            Me.TextEdit_deposit_batch_id.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
            Me.TextEdit_deposit_batch_id.Properties.ValidateOnEnterKey = True
            Me.TextEdit_deposit_batch_id.Size = New System.Drawing.Size(64, 20)
            Me.TextEdit_deposit_batch_id.TabIndex = 4
            Me.TextEdit_deposit_batch_id.ToolTip = "Enter a specific disbursement if it is not included in the above list"
            '
            'LookUpEdit_Deposit
            '
            Me.LookUpEdit_Deposit.Location = New System.Drawing.Point(32, 26)
            Me.LookUpEdit_Deposit.Name = "LookUpEdit_Deposit"
            Me.LookUpEdit_Deposit.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.LookUpEdit_Deposit.Properties.NullText = ""
            Me.LookUpEdit_Deposit.Properties.PopupWidth = 500
            Me.LookUpEdit_Deposit.Size = New System.Drawing.Size(184, 20)
            Me.LookUpEdit_Deposit.TabIndex = 1
            Me.LookUpEdit_Deposit.ToolTip = "Choose a disbursement from the list if desired"
            '
            'RequestParametersForm
            '
            Me.AutoScaleBaseSize = New System.Drawing.Size(5, 14)
            Me.ClientSize = New System.Drawing.Size(338, 120)
            Me.Controls.Add(Me.GroupBox1)
            Me.Name = "RequestParametersForm"
            Me.ToolTipController1.SetSuperTip(Me, Nothing)
            Me.Text = "ParametersForm"
            Me.Controls.SetChildIndex(Me.GroupBox1, 0)
            Me.Controls.SetChildIndex(Me.ButtonOK, 0)
            Me.Controls.SetChildIndex(Me.ButtonCancel, 0)
            CType(Me.DxErrorProvider1, System.ComponentModel.ISupportInitialize).EndInit()

            Me.GroupBox1.ResumeLayout(False)
            Me.GroupBox1.PerformLayout()
            CType(Me.TextEdit_deposit_batch_id.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LookUpEdit_Deposit.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            Me.ResumeLayout(False)

        End Sub

#End Region

        Private Sub RequestParametersForm_Load(ByVal sender As System.Object, ByVal e As System.EventArgs)

            ' Load the deposit registers
            load_deposit_registers()

            ' Add the handling routines
            AddHandler LookUpEdit_Deposit.EditValueChanged, AddressOf LookUpEdit_Disbursement_EditValueChanged

            AddHandler TextEdit_deposit_batch_id.EditValueChanged, AddressOf TextEdit_disbursement_register_EditValueChanged
            AddHandler TextEdit_deposit_batch_id.KeyPress, AddressOf TextEdit_disbursement_register_KeyPress

            AddHandler RadioButton_List.CheckedChanged, AddressOf RadioButton_List_CheckedChanged
            AddHandler RadioButton_Text.CheckedChanged, AddressOf RadioButton_List_CheckedChanged

            ' Enable or disable the OK button
            EnableOK()
        End Sub

        Private Sub load_deposit_registers()

            ' Retrieve the database connection logic
#If 1 Then
            Dim SelectStatement As String = "SELECT TOP 50 deposit_batch_id as 'ID',date_created as 'Date',ach_pull_date as 'Pull Schedules',ach_effective_date as 'Effective', note as 'File' FROM deposit_batch_ids with (nolock) WHERE batch_type = 'AC' ORDER BY date_created DESC"
#Else
            Dim SelectStatement As String = "SELECT TOP 50 deposit_batch_id as 'ID',date_created as 'Date',ach_pull_schedules as 'Pull Schedules',ach_effective_date as 'Effective', note as 'File' FROM deposit_batch_ids with (nolock) WHERE batch_type = 'AC' ORDER BY date_created DESC"
#End If
            Dim cmd As SqlClient.SqlCommand = New System.Data.SqlClient.SqlCommand()
            With cmd
                .Connection = New System.Data.SqlClient.SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
                .CommandText = SelectStatement
                .CommandTimeout = 0
            End With

            ' Retrieve the disbursement table from the system
            Dim da As New SqlClient.SqlDataAdapter(cmd)
            Dim ds As New System.Data.DataSet

            Try
                da.Fill(ds, "deposit_batch_ids")
                Dim tbl As System.Data.DataTable = ds.Tables(0)

                ' Define the values for the lookup control
                With LookUpEdit_Deposit
                    With .Properties
                        .DataSource = New System.Data.DataView(tbl, "", "Date", DataViewRowState.CurrentRows)
                        .ShowFooter = False
                        .PopulateColumns()

                        ' The date and time are the default values
                        With .Columns("Date")
                            .FormatString = "MM/dd/yyyy hh:mm tt"
                            .Width = 90
                        End With

                        With .Columns("Effective")
                            .FormatString = "MM/dd/yyyy"
                            .Width = 60
                        End With

                        ' Populate the ID column with the disbursement register, showing it as a number
                        With .Columns("ID")
                            .FormatString = "f0"
                            .Alignment = DevExpress.Utils.HorzAlignment.Far
                            .Width = 40
                        End With

                        ' The display is the note, the value is the ID
                        .DisplayMember = "Effective"
                        .ValueMember = "ID"
                    End With

                    ' Set the value to the first item
                    If tbl.Rows.Count > 0 Then
                        Dim row As System.Data.DataRow = tbl.Rows(0)
                        .EditValue = row("ID")
                    End If

                    ' Populate the input value with the ID from the listbox
                    If .EditValue IsNot System.DBNull.Value Then
                        Parameter_DepositBatchID = Convert.ToInt32(.EditValue)
                        TextEdit_deposit_batch_id.Text = Parameter_DepositBatchID.ToString()
                    End If
                End With

            Catch ex As SqlClient.SqlException
                DebtPlus.Data.Forms.MessageBox.Show(ex.ToString(), "Error retrieving batch list", MessageBoxButtons.OK, MessageBoxIcon.Error)

            End Try
        End Sub

        Private Sub TextEdit_deposit_batch_id_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs)
            CType(sender, DevExpress.XtraEditors.TextEdit).SelectAll()
        End Sub

        Private Sub TextEdit_disbursement_register_EditValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
            Parameter_DepositBatchID = Convert.ToInt32(TextEdit_deposit_batch_id.EditValue)
            EnableOK()
        End Sub

        Private Sub TextEdit_disbursement_register_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs)
            If Not System.Char.IsControl(e.KeyChar) Then
                If Not System.Char.IsDigit(e.KeyChar) Then e.Handled = True
            End If
        End Sub

        Private Sub LookUpEdit_Disbursement_EditValueChanged(ByVal sender As Object, ByVal e As System.EventArgs)
            TextEdit_deposit_batch_id.EditValue = LookUpEdit_Deposit.EditValue
            Parameter_DepositBatchID = Convert.ToInt32(TextEdit_deposit_batch_id.EditValue)
            EnableOK()
        End Sub

        Private Sub EnableOK()
            Try
                Dim NewValue As System.Int32 = CType(TextEdit_deposit_batch_id.EditValue, Int32)
                If NewValue > 0 Then
                    ButtonOK.Enabled = True
                    Exit Sub
                End If
            Catch
            End Try

            ' The disbursement register is not valid. Disable the button.
            ButtonOK.Enabled = False
        End Sub

        Private Sub RadioButton_List_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)

            ' Enable or disable the controls based upon the radio buttons
            TextEdit_deposit_batch_id.Enabled = RadioButton_Text.Checked
            LookUpEdit_Deposit.Enabled = RadioButton_List.Checked

            ' Syncrhonize the edit value with the selected item
            TextEdit_deposit_batch_id.EditValue = LookUpEdit_Deposit.EditValue

            ' Go to the next control
            CType(sender, System.Windows.Forms.RadioButton).SelectNextControl(CType(sender, System.Windows.Forms.Control), True, True, False, True)
        End Sub
    End Class
End Namespace
