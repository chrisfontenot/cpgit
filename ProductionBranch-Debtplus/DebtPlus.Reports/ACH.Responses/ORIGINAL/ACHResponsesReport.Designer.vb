Namespace ACH.Responses
    <Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
    Partial Public Class ACHResponsesReport
        Inherits DebtPlus.Reports.Template.TemplateXtraReportClass

        'XtraReport overrides dispose to clean up the component list.
        <System.Diagnostics.DebuggerNonUserCode()> _
        Protected Overrides Sub Dispose(ByVal disposing As Boolean)
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
            MyBase.Dispose(disposing)
        End Sub

        'Required by the Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Designer
        'It can be modified using the Designer.  
        'Do not modify it using the code editor.
        <System.Diagnostics.DebuggerStepThrough()> _
        Private Sub InitializeComponent()
            Dim XrSummary1 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary()
            Dim XrSummary2 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary()
            Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(ACHResponsesReport))
            Me.ParameterDepositBatchID = New DevExpress.XtraReports.Parameters.Parameter()
            Me.GroupHeader1 = New DevExpress.XtraReports.UI.GroupHeaderBand()
            Me.XrPanel1 = New DevExpress.XtraReports.UI.XRPanel()
            Me.XrLabel5 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel4 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel3 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel2 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel1 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_GroupHeaderName = New DevExpress.XtraReports.UI.XRLabel()
            Me.GroupFooter1 = New DevExpress.XtraReports.UI.GroupFooterBand()
            Me.XrLine1 = New DevExpress.XtraReports.UI.XRLine()
            Me.XrLabel_group_client = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_group_amount = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_amount = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_ach_routing_number = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_ach_account_number = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_client = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_reference = New DevExpress.XtraReports.UI.XRLabel()
            CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
            '
            'XrLabel_Title
            '
            Me.XrLabel_Title.StylePriority.UseFont = False
            '
            'XrPageInfo_PageNumber
            '
            Me.XrPageInfo_PageNumber.StylePriority.UseTextAlignment = False
            '
            'XRLabel_Agency_Name
            '
            Me.XRLabel_Agency_Name.StylePriority.UseTextAlignment = False
            '
            'XrLabel_Agency_Address3
            '
            Me.XrLabel_Agency_Address3.StylePriority.UseFont = False
            Me.XrLabel_Agency_Address3.StylePriority.UseTextAlignment = False
            '
            'XrLabel_Agency_Address1
            '
            Me.XrLabel_Agency_Address1.StylePriority.UseFont = False
            Me.XrLabel_Agency_Address1.StylePriority.UseTextAlignment = False
            '
            'XrLabel_Agency_Phone
            '
            Me.XrLabel_Agency_Phone.StylePriority.UseFont = False
            Me.XrLabel_Agency_Phone.StylePriority.UseTextAlignment = False
            '
            'XrLabel_Agency_Address2
            '
            Me.XrLabel_Agency_Address2.StylePriority.UseFont = False
            Me.XrLabel_Agency_Address2.StylePriority.UseTextAlignment = False
            '
            'Detail
            '
            Me.Detail.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel_client, Me.XrLabel_reference, Me.XrLabel_ach_account_number, Me.XrLabel_amount, Me.XrLabel_ach_routing_number})
            Me.Detail.HeightF = 15.0!
            '
            'ParameterDepositBatchID
            '
            Me.ParameterDepositBatchID.Name = "ParameterDepositBatchID"
            Me.ParameterDepositBatchID.Type = GetType(Integer)
            Me.ParameterDepositBatchID.Value = -1
            Me.ParameterDepositBatchID.Visible = False
            '
            'GroupHeader1
            '
            Me.GroupHeader1.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrPanel1, Me.XrLabel_GroupHeaderName})
            Me.GroupHeader1.GroupFields.AddRange(New DevExpress.XtraReports.UI.GroupField() {New DevExpress.XtraReports.UI.GroupField("authentication_error", DevExpress.XtraReports.UI.XRColumnSortOrder.Ascending)})
            Me.GroupHeader1.HeightF = 87.5!
            Me.GroupHeader1.Name = "GroupHeader1"
            Me.GroupHeader1.RepeatEveryPage = True
            '
            'XrPanel1
            '
            Me.XrPanel1.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel5, Me.XrLabel4, Me.XrLabel3, Me.XrLabel2, Me.XrLabel1})
            Me.XrPanel1.LocationFloat = New DevExpress.Utils.PointFloat(7.999992!, 62.58335!)
            Me.XrPanel1.Name = "XrPanel1"
            Me.XrPanel1.SizeF = New System.Drawing.SizeF(742.0!, 17.00001!)
            Me.XrPanel1.StyleName = "XrControlStyle_HeaderPannel"
            '
            'XrLabel5
            '
            Me.XrLabel5.LocationFloat = New DevExpress.Utils.PointFloat(333.0!, 1.0!)
            Me.XrLabel5.Name = "XrLabel5"
            Me.XrLabel5.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel5.SizeF = New System.Drawing.SizeF(333.0001!, 14.99999!)
            Me.XrLabel5.StylePriority.UseTextAlignment = False
            Me.XrLabel5.Text = "CLIENT ID AND NAME"
            Me.XrLabel5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            '
            'XrLabel4
            '
            Me.XrLabel4.LocationFloat = New DevExpress.Utils.PointFloat(666.0!, 1.0!)
            Me.XrLabel4.Name = "XrLabel4"
            Me.XrLabel4.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel4.SizeF = New System.Drawing.SizeF(72.0!, 15.0!)
            Me.XrLabel4.Text = "AMOUNT"
            '
            'XrLabel3
            '
            Me.XrLabel3.LocationFloat = New DevExpress.Utils.PointFloat(208.0!, 1.0!)
            Me.XrLabel3.Name = "XrLabel3"
            Me.XrLabel3.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel3.SizeF = New System.Drawing.SizeF(108.0!, 15.0!)
            Me.XrLabel3.StylePriority.UseTextAlignment = False
            Me.XrLabel3.Text = "ACCOUNT"
            Me.XrLabel3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            '
            'XrLabel2
            '
            Me.XrLabel2.LocationFloat = New DevExpress.Utils.PointFloat(108.0!, 1.0!)
            Me.XrLabel2.Name = "XrLabel2"
            Me.XrLabel2.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel2.SizeF = New System.Drawing.SizeF(90.0!, 15.0!)
            Me.XrLabel2.StylePriority.UseTextAlignment = False
            Me.XrLabel2.Text = "ROUTING"
            Me.XrLabel2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            '
            'XrLabel1
            '
            Me.XrLabel1.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 1.0!)
            Me.XrLabel1.Name = "XrLabel1"
            Me.XrLabel1.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel1.SizeF = New System.Drawing.SizeF(108.0!, 14.99999!)
            Me.XrLabel1.StylePriority.UseTextAlignment = False
            Me.XrLabel1.Text = "TRACE"
            Me.XrLabel1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            '
            'XrLabel_GroupHeaderName
            '
            Me.XrLabel_GroupHeaderName.Font = New System.Drawing.Font("Times New Roman", 14.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.XrLabel_GroupHeaderName.LocationFloat = New DevExpress.Utils.PointFloat(7.999992!, 10.00001!)
            Me.XrLabel_GroupHeaderName.Name = "XrLabel_GroupHeaderName"
            Me.XrLabel_GroupHeaderName.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_GroupHeaderName.Scripts.OnBeforePrint = "XrLabel_GroupHeaderName_BeforePrint"
            Me.XrLabel_GroupHeaderName.SizeF = New System.Drawing.SizeF(733.0!, 33.41667!)
            Me.XrLabel_GroupHeaderName.StyleName = "XrControlStyle_GroupHeader"
            Me.XrLabel_GroupHeaderName.StylePriority.UseFont = False
            Me.XrLabel_GroupHeaderName.Text = "GroupHeaderName"
            '
            'GroupFooter1
            '
            Me.GroupFooter1.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLine1, Me.XrLabel_group_client, Me.XrLabel_group_amount})
            Me.GroupFooter1.HeightF = 25.00001!
            Me.GroupFooter1.Name = "GroupFooter1"
            Me.GroupFooter1.PageBreak = DevExpress.XtraReports.UI.PageBreak.AfterBand
            Me.GroupFooter1.PrintAtBottom = True
            Me.GroupFooter1.StyleName = "XrControlStyle_Totals"
            '
            'XrLine1
            '
            Me.XrLine1.LocationFloat = New DevExpress.Utils.PointFloat(10.00001!, 0.0!)
            Me.XrLine1.Name = "XrLine1"
            Me.XrLine1.SizeF = New System.Drawing.SizeF(735.9999!, 10.00001!)
            '
            'XrLabel_group_client
            '
            Me.XrLabel_group_client.LocationFloat = New DevExpress.Utils.PointFloat(7.999992!, 10.00001!)
            Me.XrLabel_group_client.Name = "XrLabel_group_client"
            Me.XrLabel_group_client.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_group_client.SizeF = New System.Drawing.SizeF(444.4583!, 14.99999!)
            Me.XrLabel_group_client.StylePriority.UseTextAlignment = False
            XrSummary1.FormatString = "TOTALS ({0:n0} items)"
            XrSummary1.Func = DevExpress.XtraReports.UI.SummaryFunc.Count
            XrSummary1.Running = DevExpress.XtraReports.UI.SummaryRunning.Group
            Me.XrLabel_group_client.Summary = XrSummary1
            Me.XrLabel_group_client.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
            '
            'XrLabel_group_amount
            '
            Me.XrLabel_group_amount.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "amount")})
            Me.XrLabel_group_amount.LocationFloat = New DevExpress.Utils.PointFloat(632.3333!, 10.00001!)
            Me.XrLabel_group_amount.Name = "XrLabel_group_amount"
            Me.XrLabel_group_amount.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_group_amount.SizeF = New System.Drawing.SizeF(113.6667!, 15.0!)
            Me.XrLabel_group_amount.StylePriority.UseTextAlignment = False
            XrSummary2.FormatString = "{0:c}"
            XrSummary2.Running = DevExpress.XtraReports.UI.SummaryRunning.Group
            Me.XrLabel_group_amount.Summary = XrSummary2
            Me.XrLabel_group_amount.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
            '
            'XrLabel_amount
            '
            Me.XrLabel_amount.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "amount", "{0:c}")})
            Me.XrLabel_amount.LocationFloat = New DevExpress.Utils.PointFloat(674.0!, 0.0!)
            Me.XrLabel_amount.Name = "XrLabel_amount"
            Me.XrLabel_amount.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_amount.SizeF = New System.Drawing.SizeF(72.0!, 15.0!)
            Me.XrLabel_amount.StylePriority.UseTextAlignment = False
            Me.XrLabel_amount.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
            '
            'XrLabel_ach_routing_number
            '
            Me.XrLabel_ach_routing_number.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "ach_routing_number")})
            Me.XrLabel_ach_routing_number.LocationFloat = New DevExpress.Utils.PointFloat(116.0!, 0.0!)
            Me.XrLabel_ach_routing_number.Name = "XrLabel_ach_routing_number"
            Me.XrLabel_ach_routing_number.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_ach_routing_number.SizeF = New System.Drawing.SizeF(90.0!, 15.0!)
            Me.XrLabel_ach_routing_number.StylePriority.UseTextAlignment = False
            Me.XrLabel_ach_routing_number.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
            '
            'XrLabel_ach_account_number
            '
            Me.XrLabel_ach_account_number.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "ach_account_number")})
            Me.XrLabel_ach_account_number.LocationFloat = New DevExpress.Utils.PointFloat(216.0!, 0.0!)
            Me.XrLabel_ach_account_number.Name = "XrLabel_ach_account_number"
            Me.XrLabel_ach_account_number.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_ach_account_number.SizeF = New System.Drawing.SizeF(108.0!, 15.0!)
            Me.XrLabel_ach_account_number.StylePriority.UseTextAlignment = False
            Me.XrLabel_ach_account_number.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
            '
            'XrLabel_client
            '
            Me.XrLabel_client.LocationFloat = New DevExpress.Utils.PointFloat(341.0!, 0.0!)
            Me.XrLabel_client.Name = "XrLabel_client"
            Me.XrLabel_client.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_client.Scripts.OnBeforePrint = "XrLabel_client_BeforePrint"
            Me.XrLabel_client.SizeF = New System.Drawing.SizeF(329.5!, 14.99999!)
            Me.XrLabel_client.StylePriority.UseTextAlignment = False
            Me.XrLabel_client.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
            '
            'XrLabel_reference
            '
            Me.XrLabel_reference.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "reference")})
            Me.XrLabel_reference.LocationFloat = New DevExpress.Utils.PointFloat(7.999992!, 0.0!)
            Me.XrLabel_reference.Name = "XrLabel_reference"
            Me.XrLabel_reference.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_reference.SizeF = New System.Drawing.SizeF(108.0!, 14.99999!)
            Me.XrLabel_reference.StylePriority.UseTextAlignment = False
            Me.XrLabel_reference.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
            '
            'ACHResponsesReport
            '
            Me.Bands.AddRange(New DevExpress.XtraReports.UI.Band() {Me.Detail, Me.PageHeader, Me.PageFooter, Me.TopMarginBand1, Me.BottomMarginBand1, Me.GroupHeader1, Me.GroupFooter1})
            Me.Parameters.AddRange(New DevExpress.XtraReports.Parameters.Parameter() {Me.ParameterDepositBatchID})
            Me.RequestParameters = False
            Me.ScriptLanguage = DevExpress.XtraReports.ScriptLanguage.VisualBasic
            Me.ScriptReferencesString = resources.GetString("$this.ScriptReferencesString")
            Me.Scripts.OnBeforePrint = "ACHResponsesReport_BeforePrint"
            Me.ScriptsSource = resources.GetString("$this.ScriptsSource")
            Me.StyleSheet.AddRange(New DevExpress.XtraReports.UI.XRControlStyle() {Me.XrControlStyle_Header, Me.XrControlStyle_Totals, Me.XrControlStyle_HeaderPannel, Me.XrControlStyle_GroupHeader})
            Me.Version = "11.2"
            Me.Controls.SetChildIndex(Me.GroupFooter1, 0)
            Me.Controls.SetChildIndex(Me.GroupHeader1, 0)
            Me.Controls.SetChildIndex(Me.BottomMarginBand1, 0)
            Me.Controls.SetChildIndex(Me.TopMarginBand1, 0)
            Me.Controls.SetChildIndex(Me.PageFooter, 0)
            Me.Controls.SetChildIndex(Me.PageHeader, 0)
            Me.Controls.SetChildIndex(Me.Detail, 0)
            CType(Me, System.ComponentModel.ISupportInitialize).EndInit()

        End Sub
        Friend WithEvents ParameterDepositBatchID As DevExpress.XtraReports.Parameters.Parameter
        Friend WithEvents GroupHeader1 As DevExpress.XtraReports.UI.GroupHeaderBand
        Friend WithEvents GroupFooter1 As DevExpress.XtraReports.UI.GroupFooterBand
        Friend WithEvents XrPanel1 As DevExpress.XtraReports.UI.XRPanel
        Friend WithEvents XrLabel5 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel4 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel3 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel2 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel1 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_GroupHeaderName As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_client As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_reference As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_ach_account_number As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_amount As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_ach_routing_number As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLine1 As DevExpress.XtraReports.UI.XRLine
        Friend WithEvents XrLabel_group_client As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_group_amount As DevExpress.XtraReports.UI.XRLabel
    End Class
End Namespace
