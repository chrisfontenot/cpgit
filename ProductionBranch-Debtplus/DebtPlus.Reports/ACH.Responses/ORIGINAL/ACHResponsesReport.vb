#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}
#End Region

Option Compare Binary
Option Explicit On
Option Strict On

Namespace ACH.Responses
    Public Class ACHResponsesReport

        ''' <summary>
        '''     Create an instance of our report
        ''' </summary>
        Public Sub New()
            MyBase.New()
            InitializeComponent()
            Parameter_DepositBatchID = -1
            'AddHandler BeforePrint, AddressOf Report_BeforePrint
            'AddHandler XrLabel_client.BeforePrint, AddressOf XrLabel_client_BeforePrint
            'AddHandler XrLabel_GroupHeaderName.BeforePrint, AddressOf XrLabel_GroupHeaderName_BeforePrint

            ReportFilter.IsEnabled = True
        End Sub


        ''' <summary>
        '''     Return the report title
        ''' </summary>
        Public Overrides ReadOnly Property ReportTitle() As String
            Get
                Return "ACH Error Listing"
            End Get
        End Property


        ''' <summary>
        '''     Parameter for the deposit batch number
        ''' </summary>
        Public Property Parameter_DepositBatchID() As System.Int32
            Get
                Return Convert.ToInt32(Parameters("ParameterDepositBatchID").Value)
            End Get
            Set(ByVal Value As System.Int32)
                Parameters("ParameterDepositBatchID").Value = Value
            End Set
        End Property


        ''' <summary>
        '''     Subroutine to set the parameter(s)
        ''' </summary>
        Public Overrides Sub SetReportParameter(ByVal Parameter As String, ByVal Value As Object)
            Select Case Parameter
                Case "DepositBatchID"
                    Parameter_DepositBatchID = Convert.ToInt32(Value)
                Case Else
                    MyBase.SetReportParameter(Parameter, Value)
            End Select
        End Sub


        ''' <summary>
        '''     Do we need to ask for parameters?
        ''' </summary>
        Public Overrides Function NeedParameters() As Boolean
            Return Parameter_DepositBatchID <= 0
        End Function


        ''' <summary>
        '''     Function to read the parameters from the user
        ''' </summary>
        Public Overrides Function RequestReportParameters() As System.Windows.Forms.DialogResult
            Dim Answer As System.Windows.Forms.DialogResult = DialogResult.OK
            If NeedParameters() Then
                Using frm As New AchResponsesParametersForm()
                    Answer = frm.ShowDialog
                    Parameter_DepositBatchID = frm.Parameter_DepositBatchID
                End Using
            End If
            Return Answer
        End Function


        ''' <summary>
        '''     Configure the report's datasource as needed
        ''' </summary>
        Private ds As New System.Data.DataSet("ds")
        Private Sub Report_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs)
            Const TableName As String = "rpt_ACH_Transaction_Errors"
            Dim rpt As DevExpress.XtraReports.UI.XtraReport = CType(sender, DevExpress.XtraReports.UI.XtraReport)
            Dim sqlInfo As DebtPlus.LINQ.SQLInfoClass = DebtPlus.LINQ.SQLInfoClass.getDefault()
            Dim cn As System.Data.SqlClient.SqlConnection = New System.Data.SqlClient.SqlConnection(sqlInfo.ConnectionString)
            Dim tbl As System.Data.DataTable = ds.Tables(TableName)
            If tbl Is Nothing Then
                Try
                    cn.Open()
                    Using cmd As New System.Data.SqlClient.SqlCommand()
                        With cmd
                            .Connection = cn
                            .CommandText = "rpt_ACH_Transaction_Errors"
                            .CommandType = System.Data.CommandType.StoredProcedure
                            System.Data.SqlClient.SqlCommandBuilder.DeriveParameters(cmd)
                            .CommandTimeout = 0
                            .Parameters(1).Value = rpt.Parameters("ParameterDepositBatchID").Value
                        End With
                        Using da As New System.Data.SqlClient.SqlDataAdapter(cmd)
                            da.Fill(ds, TableName)
                            tbl = ds.Tables(TableName)
                        End Using
                    End Using

                Catch ex As System.Data.SqlClient.SqlException
                    Dim gdr As New DebtPlus.Repository.GetDataResult() : gdr.HandleException(ex) : DebtPlus.UI.Common.ErrorHandling.HandleErrors(gdr, "Error reading ACH transactions")

                Finally
                    If cn IsNot Nothing Then
                        If cn.State <> System.Data.ConnectionState.Closed Then cn.Close()
                        cn.Dispose()
                    End If
                End Try
            End If

            If tbl IsNot Nothing Then
                rpt.DataSource = New System.Data.DataView(tbl, CType(rpt, DebtPlus.Interfaces.Reports.IReportFilter).ReportFilter.ViewFilter, "[authentication_error], [reference], [client]", System.Data.DataViewRowState.CurrentRows)
            End If
        End Sub

        Private Sub XrLabel_GroupHeaderName_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs)
            With CType(sender, DevExpress.XtraReports.UI.XRLabel)
                Dim rpt As DevExpress.XtraReports.UI.XtraReport = CType(.Report, DevExpress.XtraReports.UI.XtraReport)
                Dim authentication_error As String = DebtPlus.Utils.Nulls.DStr(rpt.GetCurrentColumnValue("authentication_error")).Trim
                Dim authentication_code As String = DebtPlus.Utils.Nulls.DStr(rpt.GetCurrentColumnValue("authentication_code")).Trim
                If authentication_error = String.Empty Then
                    .Text = "No Error(s) Detected"
                ElseIf authentication_code = String.Empty Then
                    .Text = String.Format("{0}: Unknown Error Response Code", authentication_code)
                Else
                    .Text = authentication_code
                End If
            End With
        End Sub

        Private Sub XrLabel_client_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs)
            With CType(sender, DevExpress.XtraReports.UI.XRLabel)
                Dim rpt As DevExpress.XtraReports.UI.XtraReport = CType(.Report, DevExpress.XtraReports.UI.XtraReport)
                Dim client As Int32 = DebtPlus.Utils.Nulls.DInt(rpt.GetCurrentColumnValue("client"))
                Dim name As String = DebtPlus.Utils.Nulls.DStr(rpt.GetCurrentColumnValue("client_name")).Trim
                If client > 0 AndAlso name <> String.Empty Then
                    .Text = String.Format("[{0}] {1}", DebtPlus.Utils.Format.Client.FormatClientID(client), name)
                Else
                    .Text = String.Format("{0}{1}", DebtPlus.Utils.Format.Client.FormatClientID(client), name)
                End If
            End With
        End Sub
    End Class
End Namespace
