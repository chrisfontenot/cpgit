#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}
#End Region

Option Compare Binary
Option Explicit On
Option Strict On


Namespace Client.ActiveStatus
    Public Class ClientActiveStatusReport
        Inherits DebtPlus.Reports.Template.TemplateXtraReportClass

        ''' <summary>
        ''' Create an instance of our report
        ''' </summary>
        Public Sub New()
            MyBase.New()
            InitializeComponent()
            AddHandler XrLabel_Status.PreviewClick, AddressOf XrLabel_Status_PreviewClick
            AddHandler XrLabel_Count.PreviewClick, AddressOf XrLabel_Status_PreviewClick
            AddHandler BeforePrint, AddressOf ClientActiveStatusReport_BeforePrint
        End Sub

        ''' <summary>
        ''' Return the report title
        ''' </summary>
        Public Overrides ReadOnly Property ReportTitle() As String
            Get
                Return "Count of Clients By Status"
            End Get
        End Property

#Region " Component Designer generated code "

        'Component overrides dispose to clean up the component list.
        Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
            If disposing Then
                If components IsNot Nothing Then
                    components.Dispose()
                End If
            End If
            MyBase.Dispose(disposing)
        End Sub

        'Required by the Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Designer
        'It can be modified using the Designer.
        'Do not modify it using the code editor.
        Friend WithEvents XrPanel1 As DevExpress.XtraReports.UI.XRPanel
        Friend WithEvents XrLabel1 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel2 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_Status As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_Count As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents ReportFooter As DevExpress.XtraReports.UI.ReportFooterBand
        Friend WithEvents XrLabel_Total_Count As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_Total_Status As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLine1 As DevExpress.XtraReports.UI.XRLine
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
            Dim XrSummary1 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary
            Me.XrPanel1 = New DevExpress.XtraReports.UI.XRPanel
            Me.XrLabel2 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel1 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_Status = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_Count = New DevExpress.XtraReports.UI.XRLabel
            Me.ReportFooter = New DevExpress.XtraReports.UI.ReportFooterBand
            Me.XrLine1 = New DevExpress.XtraReports.UI.XRLine
            Me.XrLabel_Total_Status = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_Total_Count = New DevExpress.XtraReports.UI.XRLabel
            CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
            '
            'XrLabel_Title
            '
            Me.XrLabel_Title.ParentStyleUsing.UseFont = False
            Me.XrLabel_Title.ParentStyleUsing.UseForeColor = False
            '
            'XrPageInfo_PageNumber
            '
            Me.XrPageInfo_PageNumber.ParentStyleUsing.UseFont = False
            Me.XrPageInfo_PageNumber.ParentStyleUsing.UseForeColor = False
            '
            'XrLabel_Agency_Address2
            '
            Me.XrLabel_Agency_Address2.ParentStyleUsing.UseFont = False
            Me.XrLabel_Agency_Address2.ParentStyleUsing.UseForeColor = False
            '
            'XrLabel_Agency_Phone
            '
            Me.XrLabel_Agency_Phone.ParentStyleUsing.UseFont = False
            Me.XrLabel_Agency_Phone.ParentStyleUsing.UseForeColor = False
            '
            'XrLabel_Agency_Address1
            '
            Me.XrLabel_Agency_Address1.ParentStyleUsing.UseFont = False
            Me.XrLabel_Agency_Address1.ParentStyleUsing.UseForeColor = False
            '
            'XrLabel_Agency_Address3
            '
            Me.XrLabel_Agency_Address3.ParentStyleUsing.UseFont = False
            Me.XrLabel_Agency_Address3.ParentStyleUsing.UseForeColor = False
            '
            'XrPageInfo1
            '
            Me.XrPageInfo1.ParentStyleUsing.UseFont = False
            Me.XrPageInfo1.ParentStyleUsing.UseForeColor = False
            '
            'XrLine_Title
            '
            Me.XrLine_Title.ParentStyleUsing.UseForeColor = False
            '
            'XRLabel_Agency_Name
            '
            Me.XRLabel_Agency_Name.ParentStyleUsing.UseFont = False
            Me.XRLabel_Agency_Name.ParentStyleUsing.UseForeColor = False
            '
            'Detail
            '
            Me.Detail.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel_Count, Me.XrLabel_Status})
            Me.Detail.Height = 17
            '
            'XrLabel_Subtitle
            '
            Me.XrLabel_Subtitle.ParentStyleUsing.UseFont = False
            Me.XrLabel_Subtitle.ParentStyleUsing.UseForeColor = False
            '
            'PageHeader
            '
            Me.PageHeader.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrPanel1})
            Me.PageHeader.Height = 150
            '
            'XrPanel1
            '
            Me.XrPanel1.BackColor = System.Drawing.Color.Teal
            Me.XrPanel1.BorderColor = System.Drawing.Color.Teal
            Me.XrPanel1.CanGrow = False
            Me.XrPanel1.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel2, Me.XrLabel1})
            Me.XrPanel1.Location = New System.Drawing.Point(8, 125)
            Me.XrPanel1.Name = "XrPanel1"
            Me.XrPanel1.ParentStyleUsing.UseBackColor = False
            Me.XrPanel1.ParentStyleUsing.UseBorderColor = False
            Me.XrPanel1.Size = New System.Drawing.Size(742, 17)
            '
            'XrLabel2
            '
            Me.XrLabel2.BorderColor = System.Drawing.Color.Transparent
            Me.XrLabel2.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel2.ForeColor = System.Drawing.Color.White
            Me.XrLabel2.Location = New System.Drawing.Point(417, 0)
            Me.XrLabel2.Name = "XrLabel2"
            Me.XrLabel2.ParentStyleUsing.UseBorderColor = False
            Me.XrLabel2.ParentStyleUsing.UseFont = False
            Me.XrLabel2.ParentStyleUsing.UseForeColor = False
            Me.XrLabel2.Size = New System.Drawing.Size(325, 17)
            Me.XrLabel2.Text = "COUNT"
            Me.XrLabel2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
            '
            'XrLabel1
            '
            Me.XrLabel1.BorderColor = System.Drawing.Color.Transparent
            Me.XrLabel1.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel1.ForeColor = System.Drawing.Color.White
            Me.XrLabel1.Location = New System.Drawing.Point(8, 0)
            Me.XrLabel1.Name = "XrLabel1"
            Me.XrLabel1.ParentStyleUsing.UseBorderColor = False
            Me.XrLabel1.ParentStyleUsing.UseFont = False
            Me.XrLabel1.ParentStyleUsing.UseForeColor = False
            Me.XrLabel1.Size = New System.Drawing.Size(325, 17)
            Me.XrLabel1.Text = "ACTIVE STATUS"
            '
            'XrLabel_Status
            '
            Me.XrLabel_Status.CanGrow = False
            Me.XrLabel_Status.Location = New System.Drawing.Point(17, 0)
            Me.XrLabel_Status.Name = "XrLabel_Status"
            Me.XrLabel_Status.Size = New System.Drawing.Size(550, 17)
            Me.XrLabel_Status.Text = "ACTIVE STATUS"
            '
            'XrLabel_Count
            '
            Me.XrLabel_Count.CanGrow = False
            Me.XrLabel_Count.Location = New System.Drawing.Point(567, 0)
            Me.XrLabel_Count.Name = "XrLabel_Count"
            Me.XrLabel_Count.Size = New System.Drawing.Size(182, 17)
            Me.XrLabel_Count.Text = "COUNT"
            Me.XrLabel_Count.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
            '
            'ReportFooter
            '
            Me.ReportFooter.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLine1, Me.XrLabel_Total_Status, Me.XrLabel_Total_Count})
            Me.ReportFooter.Height = 56
            Me.ReportFooter.Name = "ReportFooter"
            '
            'XrLine1
            '
            Me.XrLine1.ForeColor = System.Drawing.Color.Teal
            Me.XrLine1.LineWidth = 2
            Me.XrLine1.Location = New System.Drawing.Point(17, 8)
            Me.XrLine1.Name = "XrLine1"
            Me.XrLine1.ParentStyleUsing.UseBorderWidth = False
            Me.XrLine1.ParentStyleUsing.UseForeColor = False
            Me.XrLine1.Size = New System.Drawing.Size(733, 9)
            '
            'XrLabel_Total_Status
            '
            Me.XrLabel_Total_Status.CanGrow = False
            Me.XrLabel_Total_Status.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel_Total_Status.Location = New System.Drawing.Point(17, 25)
            Me.XrLabel_Total_Status.Name = "XrLabel_Total_Status"
            Me.XrLabel_Total_Status.ParentStyleUsing.UseFont = False
            Me.XrLabel_Total_Status.Size = New System.Drawing.Size(550, 17)
            Me.XrLabel_Total_Status.Text = "TOTALS"
            '
            'XrLabel_Total_Count
            '
            Me.XrLabel_Total_Count.CanGrow = False
            Me.XrLabel_Total_Count.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel_Total_Count.Location = New System.Drawing.Point(567, 25)
            Me.XrLabel_Total_Count.Name = "XrLabel_Total_Count"
            Me.XrLabel_Total_Count.ParentStyleUsing.UseFont = False
            Me.XrLabel_Total_Count.Size = New System.Drawing.Size(182, 17)
            XrSummary1.FormatString = "{0:#,#}"
            XrSummary1.Running = DevExpress.XtraReports.UI.SummaryRunning.Report
            Me.XrLabel_Total_Count.Summary = XrSummary1
            Me.XrLabel_Total_Count.Text = "COUNT"
            Me.XrLabel_Total_Count.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
            '
            Me.XrLabel_Status.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "description", ""), New DevExpress.XtraReports.UI.XRBinding("Tag", Nothing, "active_status", "")})
            Me.XrLabel_Count.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "count", "{0:#,#}"), New DevExpress.XtraReports.UI.XRBinding("Tag", Nothing, "active_status", "")})
            Me.XrLabel_Total_Count.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "count", "")})
            '
            'ClientActiveStatusReport
            '
            Me.Bands.AddRange(New DevExpress.XtraReports.UI.Band() {Me.ReportFooter})
            CType(Me, System.ComponentModel.ISupportInitialize).EndInit()
        End Sub

#End Region

        ''' <summary>
        ''' A detail line item was selected. Display the new report
        ''' </summary>
        Private Sub XrLabel_Status_PreviewClick(ByVal sender As Object, ByVal e As DevExpress.XtraReports.UI.PreviewMouseEventArgs)

            ' Display the new form with the list of clients that have this indicated active status
            Dim active_status As String = Convert.ToString(e.Brick.Value)
            Using rpt As New ListCilentsActiveStatusReport()
                rpt.SetReportParameter("ActiveStatus", active_status)
                rpt.ShowPreview()
            End Using
        End Sub

        ''' <summary>
        ''' Process the report setup
        ''' </summary>
        Private Sub ClientActiveStatusReport_BeforePrint(ByVal sender As Object, ByVal e As System.EventArgs)
            Dim ds As New System.Data.DataSet("view_active_status")
            Using da As New SqlClient.SqlDataAdapter(SelectCommand)
                da.Fill(ds, "view_active_status")
            End Using

            Dim tbl As System.Data.DataTable = ds.Tables(0)
            Dim vue As System.Data.DataView = tbl.DefaultView

            ' Bind the datasource to the report
            DataSource = vue
        End Sub


        ''' <summary>
        ''' Database select command for the main dataset
        ''' </summary>
        Private Function SelectCommand() As SqlClient.SqlCommand
            Dim cmd As SqlClient.SqlCommand = New System.Data.SqlClient.SqlCommand()
            With cmd
                .Connection = New System.Data.SqlClient.SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
                .CommandText = "SELECT [active_status],[count],[description] FROM view_client_active_status ORDER BY description"
                .CommandType = CommandType.Text
            End With
            Return cmd
        End Function
    End Class

End Namespace
