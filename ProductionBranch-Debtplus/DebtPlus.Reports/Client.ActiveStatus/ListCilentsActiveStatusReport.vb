#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}
#End Region

Option Compare Binary
Option Explicit On
Option Strict On


Namespace Client.ActiveStatus

    Public Class ListCilentsActiveStatusReport
        Inherits Template.TemplateXtraReportClass

        ''' <summary>
        ''' Create an instance of our report
        ''' </summary>
        Public Sub New()
            MyBase.New()
            InitializeComponent()
            AddHandler Me.BeforePrint, AddressOf ListCilentsActiveStatusReport_BeforePrint
            AddHandler XrLabel_Client.BeforePrint, AddressOf XrLabel_Client_BeforePrint
        End Sub

        ''' <summary>
        ''' Return the report title
        ''' </summary>
        Public Overrides ReadOnly Property ReportTitle() As String
            Get
                Return System.String.Format("List of Clients with {0} Status", Parameter_ActiveStatus)
            End Get
        End Property


        ''' <summary>
        ''' Parameter for the active status
        ''' </summary>
        Private _Parameter_ActiveStatus As String
        Public Property Parameter_ActiveStatus() As String
            Get
                Return _Parameter_ActiveStatus
            End Get
            Set(ByVal Value As String)
                _Parameter_ActiveStatus = Value
            End Set
        End Property


        ''' <summary>
        ''' Generic routine to set the parameter
        ''' </summary>
        Public Overrides Sub SetReportParameter(ByVal Parameter As String, ByVal Value As Object)
            Select Case Parameter
                Case "ActiveStatus"
                    Parameter_ActiveStatus = Convert.ToString(Value)
                Case Else
                    MyBase.SetReportParameter(Parameter, Value)
            End Select
        End Sub

#Region " Component Designer generated code "

        'Component overrides dispose to clean up the component list.
        Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
            If disposing Then
                If components IsNot Nothing Then
                    components.Dispose()
                End If
            End If
            MyBase.Dispose(disposing)
        End Sub

        'Required by the Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Designer
        'It can be modified using the Designer.
        'Do not modify it using the code editor.
        Friend WithEvents XrPanel1 As DevExpress.XtraReports.UI.XRPanel
        Friend WithEvents XrLabel1 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel2 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel3 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_Client As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_Name As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_StartDate As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents ReportFooter As DevExpress.XtraReports.UI.ReportFooterBand
        Friend WithEvents XrLine1 As DevExpress.XtraReports.UI.XRLine
        Friend WithEvents XrLabel_TotalClients As DevExpress.XtraReports.UI.XRLabel
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
            Me.XrPanel1 = New DevExpress.XtraReports.UI.XRPanel
            Me.XrLabel3 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel2 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel1 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_Client = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_Name = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_StartDate = New DevExpress.XtraReports.UI.XRLabel
            Me.ReportFooter = New DevExpress.XtraReports.UI.ReportFooterBand
            Me.XrLabel_TotalClients = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLine1 = New DevExpress.XtraReports.UI.XRLine
            CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
            '
            'XrLabel_Title
            '
            Me.XrLabel_Title.ParentStyleUsing.UseFont = False
            Me.XrLabel_Title.ParentStyleUsing.UseForeColor = False
            '
            'XrPageInfo_PageNumber
            '
            Me.XrPageInfo_PageNumber.ParentStyleUsing.UseFont = False
            Me.XrPageInfo_PageNumber.ParentStyleUsing.UseForeColor = False
            '
            'XrLabel_Agency_Address2
            '
            Me.XrLabel_Agency_Address2.ParentStyleUsing.UseFont = False
            Me.XrLabel_Agency_Address2.ParentStyleUsing.UseForeColor = False
            '
            'XrLabel_Agency_Phone
            '
            Me.XrLabel_Agency_Phone.ParentStyleUsing.UseFont = False
            Me.XrLabel_Agency_Phone.ParentStyleUsing.UseForeColor = False
            '
            'XrLabel_Agency_Address1
            '
            Me.XrLabel_Agency_Address1.ParentStyleUsing.UseFont = False
            Me.XrLabel_Agency_Address1.ParentStyleUsing.UseForeColor = False
            '
            'XrLabel_Agency_Address3
            '
            Me.XrLabel_Agency_Address3.ParentStyleUsing.UseFont = False
            Me.XrLabel_Agency_Address3.ParentStyleUsing.UseForeColor = False
            '
            'XrPageInfo1
            '
            Me.XrPageInfo1.ParentStyleUsing.UseFont = False
            Me.XrPageInfo1.ParentStyleUsing.UseForeColor = False
            '
            'XrLine_Title
            '
            Me.XrLine_Title.ParentStyleUsing.UseForeColor = False
            '
            'XRLabel_Agency_Name
            '
            Me.XRLabel_Agency_Name.ParentStyleUsing.UseFont = False
            Me.XRLabel_Agency_Name.ParentStyleUsing.UseForeColor = False
            '
            'Detail
            '
            Me.Detail.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel_StartDate, Me.XrLabel_Name, Me.XrLabel_Client})
            Me.Detail.Height = 17
            '
            'XrLabel_Subtitle
            '
            Me.XrLabel_Subtitle.ParentStyleUsing.UseFont = False
            Me.XrLabel_Subtitle.ParentStyleUsing.UseForeColor = False
            '
            'PageHeader
            '
            Me.PageHeader.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrPanel1})
            Me.PageHeader.Height = 141
            '
            'XrPanel1
            '
            Me.XrPanel1.BackColor = System.Drawing.Color.Teal
            Me.XrPanel1.BorderColor = System.Drawing.Color.Transparent
            Me.XrPanel1.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel3, Me.XrLabel2, Me.XrLabel1})
            Me.XrPanel1.Location = New System.Drawing.Point(0, 117)
            Me.XrPanel1.Name = "XrPanel1"
            Me.XrPanel1.ParentStyleUsing.UseBackColor = False
            Me.XrPanel1.ParentStyleUsing.UseBorderColor = False
            Me.XrPanel1.Size = New System.Drawing.Size(733, 16)
            '
            'XrLabel3
            '
            Me.XrLabel3.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel3.ForeColor = System.Drawing.Color.White
            Me.XrLabel3.Location = New System.Drawing.Point(609, 0)
            Me.XrLabel3.Name = "XrLabel3"
            Me.XrLabel3.ParentStyleUsing.UseFont = False
            Me.XrLabel3.ParentStyleUsing.UseForeColor = False
            Me.XrLabel3.Size = New System.Drawing.Size(117, 19)
            Me.XrLabel3.Text = "START DATE"
            Me.XrLabel3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
            '
            'XrLabel2
            '
            Me.XrLabel2.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel2.ForeColor = System.Drawing.Color.White
            Me.XrLabel2.Location = New System.Drawing.Point(92, 0)
            Me.XrLabel2.Name = "XrLabel2"
            Me.XrLabel2.ParentStyleUsing.UseFont = False
            Me.XrLabel2.ParentStyleUsing.UseForeColor = False
            Me.XrLabel2.Size = New System.Drawing.Size(67, 18)
            Me.XrLabel2.Text = "NAME"
            '
            'XrLabel1
            '
            Me.XrLabel1.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel1.ForeColor = System.Drawing.Color.White
            Me.XrLabel1.Location = New System.Drawing.Point(0, 0)
            Me.XrLabel1.Name = "XrLabel1"
            Me.XrLabel1.ParentStyleUsing.UseFont = False
            Me.XrLabel1.ParentStyleUsing.UseForeColor = False
            Me.XrLabel1.Size = New System.Drawing.Size(75, 17)
            Me.XrLabel1.Text = "Client"
            Me.XrLabel1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
            '
            'XrLabel_Client
            '
            Me.XrLabel_Client.Location = New System.Drawing.Point(8, 0)
            Me.XrLabel_Client.Name = "XrLabel_Client"
            Me.XrLabel_Client.Size = New System.Drawing.Size(75, 17)
            Me.XrLabel_Client.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
            '
            'XrLabel_Name
            '
            Me.XrLabel_Name.Location = New System.Drawing.Point(92, 0)
            Me.XrLabel_Name.Name = "XrLabel_Name"
            Me.XrLabel_Name.Size = New System.Drawing.Size(466, 17)
            '
            'XrLabel_StartDate
            '
            Me.XrLabel_StartDate.Location = New System.Drawing.Point(592, 0)
            Me.XrLabel_StartDate.Name = "XrLabel_StartDate"
            Me.XrLabel_StartDate.Size = New System.Drawing.Size(134, 17)
            Me.XrLabel_StartDate.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
            '
            'ReportFooter
            '
            Me.ReportFooter.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel_TotalClients, Me.XrLine1})
            Me.ReportFooter.Name = "ReportFooter"
            '
            'XrLabel_TotalClients
            '
            Me.XrLabel_TotalClients.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel_TotalClients.Location = New System.Drawing.Point(67, 25)
            Me.XrLabel_TotalClients.Name = "XrLabel_TotalClients"
            Me.XrLabel_TotalClients.ParentStyleUsing.UseFont = False
            Me.XrLabel_TotalClients.Size = New System.Drawing.Size(275, 17)
            '
            'XrLine1
            '
            Me.XrLine1.Location = New System.Drawing.Point(67, 8)
            Me.XrLine1.Name = "XrLine1"
            Me.XrLine1.Size = New System.Drawing.Size(675, 9)
            '
            'ListCilentsActiveStatusReport
            '
            Me.Bands.AddRange(New DevExpress.XtraReports.UI.Band() {Me.ReportFooter})
            CType(Me, System.ComponentModel.ISupportInitialize).EndInit()

        End Sub

#End Region


        ''' <summary>
        ''' Configure the report upon its load
        ''' </summary>
        Private Sub ListCilentsActiveStatusReport_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs)
            Dim ds As New System.Data.DataSet
            Dim da As New SqlClient.SqlDataAdapter(SelectCommand)
            da.Fill(ds)

            Dim tbl As System.Data.DataTable = ds.Tables(0)
            Dim vue As System.Data.DataView = tbl.DefaultView

            ' Bind the report to the data
            DataSource = vue

            ' Bind the columns to the rows
            XrLabel_Name.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", vue, "name", "")})
            XrLabel_StartDate.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", vue, "start_date", "{0:d}")})

            ' Bind the total number of clients to the final summary line
            XrLabel_TotalClients.Text = System.String.Format("Total clients: {0:d}", vue.Count)
        End Sub


        ''' <summary>
        ''' Dataset to select the main recordset
        ''' </summary>
        Private Function SelectCommand() As SqlClient.SqlCommand
            Dim cn As SqlClient.SqlConnection = New System.Data.SqlClient.SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
            Dim cmd As New SqlClient.SqlCommand()
            With cmd
                .Connection = cn
                .CommandText = "SELECT c.client as client, dbo.format_normal_name(p.prefix,p.first,p.middle,p.last,p.suffix) as name, c.start_date FROM clients c with (nolock) left outer join people p on c.client = p.client and 1 = p.person WHERE c.active_status = @active_status"
                .Parameters.Add(New SqlClient.SqlParameter("@active_status", SqlDbType.VarChar, 80)).Value = Parameter_ActiveStatus
                .CommandTimeout = 0
            End With
            Return cmd
        End Function

        ''' <summary>
        ''' Format the client ID using our own logic
        ''' </summary>
        Private Sub XrLabel_Client_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs)
            XrLabel_Client.Text = String.Format("{0:0000000}", Convert.ToInt32(GetCurrentColumnValue("client")))
        End Sub
    End Class
End Namespace
