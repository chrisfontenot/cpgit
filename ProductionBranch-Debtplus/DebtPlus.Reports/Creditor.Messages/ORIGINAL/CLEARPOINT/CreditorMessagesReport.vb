#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}
#End Region

Option Compare Binary
Option Explicit On 
Option Strict On

Imports DevExpress.XtraReports.UI

Namespace Creditor.Messages
    Public Class CreditorMessagesReport

        ''' <summary>
        '''     Create an instance of our report
        ''' </summary>
        Public Sub New()
            MyBase.New()
            InitializeComponent()

            'AddHandler Me.BeforePrint, AddressOf CreditorMessagesReport_BeforePrint
            'AddHandler XrLabel_phone_number.BeforePrint, AddressOf XrLabel_phone_number_BeforePrint
            'AddHandler XrLabel_client_id.BeforePrint, AddressOf XrLabel_client_id_BeforePrint
            'AddHandler XrBarCode_PostalCode.BeforePrint, AddressOf XrBarCode_PostalCode_BeforePrint
            'AddHandler XrLabel_Creditor_Address.BeforePrint, AddressOf XrLabel_Creditor_Address_BeforePrint
        End Sub

        ''' <summary>
        '''     Report title information
        ''' </summary>
        Public Overrides ReadOnly Property ReportTitle() As String
            Get
                Return "Creditor Messages"
            End Get
        End Property

        ''' <summary>
        '''     Configure the report with data
        ''' </summary>
        Private Sub XrBarCode_PostalCode_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs)
            Dim bar As DevExpress.XtraReports.UI.XRBarCode = CType(sender, DevExpress.XtraReports.UI.XRBarCode)
            Dim rpt As DevExpress.XtraReports.UI.XtraReport = CType(bar.Report, DevExpress.XtraReports.UI.XtraReport)
            Dim tbl As System.Data.DataTable = ds.Tables("rpt_CreditorAddress_L")
            Dim text_block As String = String.Empty
            Dim row As System.Data.DataRow = Nothing

            Dim Creditor As String = String.Empty
            If rpt.GetCurrentColumnValue("creditor") IsNot System.DBNull.Value Then Creditor = Convert.ToString(GetCurrentColumnValue("creditor"))
            If Not String.IsNullOrEmpty(Creditor) Then
                If tbl IsNot Nothing Then
                    row = tbl.Rows.Find(Creditor)
                End If

                If row Is Nothing Then
                    Try
                        Dim sqlInfo As DebtPlus.LINQ.SQLInfoClass = DebtPlus.LINQ.SQLInfoClass.getDefault()
                        Using cn As New System.Data.SqlClient.SqlConnection(sqlInfo.ConnectionString)
                            cn.Open()
                            Using cmd As New System.Data.SqlClient.SqlCommand()
                                cmd.Connection = cn
                                cmd.CommandText = "rpt_CreditorAddress_L"
                                cmd.CommandType = System.Data.CommandType.StoredProcedure
                                cmd.Parameters.Add("@creditor", System.Data.SqlDbType.VarChar, 10).Value = Creditor
                                cmd.CommandTimeout = 0

                                Using da As New System.Data.SqlClient.SqlDataAdapter(cmd)
                                    da.Fill(ds, "rpt_CreditorAddress_L")
                                    tbl = ds.Tables("rpt_CreditorAddress_L")
                                    If tbl.PrimaryKey.GetUpperBound(0) < 0 Then
                                        tbl.PrimaryKey = New System.Data.DataColumn() {tbl.Columns("creditor")}
                                    End If
                                End Using
                            End Using
                        End Using

                        If tbl IsNot Nothing Then
                            row = tbl.Rows.Find(Creditor)
                        End If

                    Catch ex As System.Data.SqlClient.SqlException
                        Using gdr As New DebtPlus.Repository.GetDataResult()
                            gdr.HandleException(ex)
                            DebtPlus.UI.Common.ErrorHandling.HandleErrors(gdr, "Error reading report data")
                            Return
                        End Using
                    End Try
                End If

                If row IsNot Nothing Then
                    If row("zipcode") IsNot Nothing AndAlso row("zipcode") IsNot System.DBNull.Value Then
                        text_block = Convert.ToString(row("zipcode"))
                    End If
                    text_block = System.Text.RegularExpressions.Regex.Replace(text_block, "[^0-9]", String.Empty)
                End If
            End If

            If Not String.IsNullOrEmpty(text_block) Then
                bar.Text = text_block
            Else
                e.Cancel = True
            End If
        End Sub

        Private Sub XrLabel_Creditor_Address_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs)
            Dim lbl As DevExpress.XtraReports.UI.XRLabel = CType(sender, DevExpress.XtraReports.UI.XRLabel)
            Dim rpt As DevExpress.XtraReports.UI.XtraReport = CType(lbl.Report, DevExpress.XtraReports.UI.XtraReport)
            Dim tbl As System.Data.DataTable = ds.Tables("creditor_addresses")
            Dim text_block As New System.Text.StringBuilder()

            '-- The creditor is part of the current row being printed
            Dim row As System.Data.DataRow = Nothing
            Dim Creditor As String = String.Empty
            If rpt.GetCurrentColumnValue("creditor") IsNot System.DBNull.Value Then Creditor = Convert.ToString(GetCurrentColumnValue("creditor"))
            If Not String.IsNullOrEmpty(Creditor) Then
                If tbl IsNot Nothing Then
                    row = tbl.Rows.Find(Creditor)
                End If

                If row Is Nothing Then
                    Try
                        Dim sqlInfo As DebtPlus.LINQ.SQLInfoClass = DebtPlus.LINQ.SQLInfoClass.getDefault()
                        Using cn As New System.Data.SqlClient.SqlConnection(sqlInfo.ConnectionString)
                            cn.Open()
                            Using cmd As New System.Data.SqlClient.SqlCommand()
                                cmd.Connection = cn
                                cmd.CommandText = "rpt_CreditorAddress_L"
                                cmd.CommandType = System.Data.CommandType.StoredProcedure
                                cmd.Parameters.Add("@creditor", System.Data.SqlDbType.VarChar, 10).Value = Creditor
                                cmd.CommandTimeout = 0

                                Using da As New System.Data.SqlClient.SqlDataAdapter(cmd)
                                    da.Fill(ds, "rpt_CreditorAddress_L")
                                    tbl = ds.Tables("rpt_CreditorAddress_L")
                                    If tbl.PrimaryKey.GetUpperBound(0) < 0 Then
                                        tbl.PrimaryKey = New System.Data.DataColumn() {tbl.Columns("creditor")}
                                    End If
                                End Using
                            End Using
                        End Using

                        If tbl IsNot Nothing Then
                            row = tbl.Rows.Find(Creditor)
                        End If

                    Catch ex As System.Data.SqlClient.SqlException
                        Using gdr As New DebtPlus.Repository.GetDataResult()
                            gdr.HandleException(ex)
                            DebtPlus.UI.Common.ErrorHandling.HandleErrors(gdr, "Error reading creditor address")
                            Return
                        End Using
                    End Try
                End If

                If row IsNot Nothing Then
                    For Each NameString As String In New String() {"addr1", "addr2", "addr3", "addr4", "addr5", "addr6", "addr7"}
                        Dim Value As String
                        If row(NameString) IsNot Nothing AndAlso row(NameString) IsNot System.DBNull.Value Then
                            Value = Convert.ToString(row(NameString)).Trim
                            If Value <> String.Empty Then
                                text_block.Append(Environment.NewLine)
                                text_block.Append(Value)
                            End If
                        End If
                    Next
                End If

                If text_block.Length > 0 Then
                    text_block.Remove(0, 2)
                End If
            End If

            lbl.Text = text_block.ToString()
        End Sub

        Private Sub XrLabel_client_id_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs)
            Dim lbl As DevExpress.XtraReports.UI.XRLabel = DirectCast(sender, DevExpress.XtraReports.UI.XRLabel)
            Dim rpt As DevExpress.XtraReports.UI.XtraReport = CType(lbl.Report, DevExpress.XtraReports.UI.XtraReport)

            Dim ClientID As String = DebtPlus.Utils.Format.Client.FormatClientID(rpt.GetCurrentColumnValue("client_id"))

            Dim ClientName As String = String.Empty
            If rpt.GetCurrentColumnValue("client_name") IsNot System.DBNull.Value Then ClientName = Convert.ToString(rpt.GetCurrentColumnValue("client_name"))

            If Not String.IsNullOrEmpty(ClientID) AndAlso Not String.IsNullOrEmpty(ClientName) Then
                lbl.Text = ClientID + " " + ClientName
            Else
                lbl.Text = ClientID + ClientName
            End If
        End Sub

        Private Sub XrLabel_phone_number_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs)
            Dim lbl As DevExpress.XtraReports.UI.XRLabel = CType(sender, DevExpress.XtraReports.UI.XRLabel)
            Dim FmtString As String = lbl.Text
            If FmtString = String.Empty Then
                FmtString = "The following messages were noted in our system concerning your institution regarding our recent disbursement process. We have terminated the clients listed below. The reason is provided in the message column. Please take a moment and review them. If you have any questions, please don't hesitate to contact us at [format_phone]." + Environment.NewLine + Environment.NewLine + "Thank You."
            End If

            If FmtString.Contains("{0}") Then
                Dim rpt As DevExpress.XtraReports.UI.XtraReport = CType(lbl.Report, DevExpress.XtraReports.UI.XtraReport)
                Dim PhoneNumber As String = String.Empty
                Try
                    Dim sqlInfo As DebtPlus.LINQ.SQLInfoClass = DebtPlus.LINQ.SQLInfoClass.getDefault()
                    Using cn As New System.Data.SqlClient.SqlConnection(sqlInfo.ConnectionString)
                        cn.Open()
                        Using cmd As New System.Data.SqlClient.SqlCommand()
                            cmd.Connection = cn
                            cmd.CommandText = "SELECT TOP 1 dbo.format_TelephoneNumber(TelephoneID) as telephone FROM config WITH (NOLOCK)"
                            cmd.CommandType = System.Data.CommandType.Text
                            cmd.CommandTimeout = 0
                            Dim objAnswer As Object = cmd.ExecuteScalar()
                            If objAnswer IsNot Nothing AndAlso objAnswer IsNot System.DBNull.Value Then PhoneNumber = Convert.ToString(objAnswer)
                        End Using
                    End Using

                    If PhoneNumber <> String.Empty Then
                        lbl.Text = String.Format(FmtString, PhoneNumber)
                    Else
                        lbl.Text = String.Format(FmtString, "the number listed above")
                    End If

                Catch ex As System.Data.SqlClient.SqlException
                    Using gdr As New DebtPlus.Repository.GetDataResult()
                        gdr.HandleException(ex)
                        DebtPlus.UI.Common.ErrorHandling.HandleErrors(gdr, "Error formatting telephone number")
                        Return
                    End Using
                End Try
            End If
        End Sub

        Dim ds As New System.Data.DataSet("ds")

        Private Sub CreditorMessagesReport_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs)
            Dim rpt As DevExpress.XtraReports.UI.XtraReport = CType(sender, DevExpress.XtraReports.UI.XtraReport)

            '-- Read the information from the config file
            Try
                Dim sqlInfo As DebtPlus.LINQ.SQLInfoClass = DebtPlus.LINQ.SQLInfoClass.getDefault()
                Using cn As New System.Data.SqlClient.SqlConnection(sqlInfo.ConnectionString)
                    cn.Open()

                    '-- Read the data from the database
                    Using cmd As New System.Data.SqlClient.SqlCommand()
                        cmd.Connection = cn
                        cmd.CommandText = "rpt_CreditorDisbursementNotes"
                        cmd.CommandType = System.Data.CommandType.StoredProcedure
                        cmd.CommandTimeout = 0

                        Using da As New System.Data.SqlClient.SqlDataAdapter(cmd)
                            da.Fill(ds, "rpt_CreditorDisbursementNotes")
                        End Using
                    End Using
                End Using

                '-- Set the datasource to the creditor view
                Dim tbl As System.Data.DataTable = ds.Tables("rpt_CreditorDisbursementNotes")
                rpt.DataSource = New System.Data.DataView(tbl, String.Empty, "creditor", System.Data.DataViewRowState.CurrentRows)

            Catch ex As System.Data.SqlClient.SqlException
                Using gdr As New DebtPlus.Repository.GetDataResult()
                    gdr.HandleException(ex)
                    DebtPlus.UI.Common.ErrorHandling.HandleErrors(gdr, "Error reading report data")
                    Return
                End Using
            End Try
        End Sub
    End Class
End Namespace
