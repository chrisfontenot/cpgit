#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}
#End Region

Option Compare Binary
Option Explicit On 
Option Strict On

Public Class CreditorMessagesReport

    ''' <summary>
    '''     Create an instance of our report
    ''' </summary>
    Public Sub New()
        MyBase.New()
        InitializeComponent()

        'AddHandler BeforePrint, AddressOf Report_BeforePrint
        'AddHandler XrLabel_phone_number.BeforePrint, AddressOf XrLabel_phone_number_BeforePrint
        'AddHandler XrLabel_client_id.BeforePrint, AddressOf XrLabel_client_id_BeforePrint
        'AddHandler XrBarCode_PostalCode.BeforePrint, AddressOf XrBarCode_PostalCode_BeforePrint
        'AddHandler XrLabel_Creditor_Address.BeforePrint, AddressOf XrLabel_Creditor_Address_BeforePrint
        'AddHandler XrPanel_AgencyAddress.BeforePrint, AddressOf XrPanel_AgencyAddress_BeforePrint
    End Sub


    ''' <summary>
    '''     Report title information
    ''' </summary>
    Public Overrides ReadOnly Property ReportTitle() As String
        Get
            Return "Creditor Messages"
        End Get
    End Property


    ''' <summary>
    '''     Configure the report with data
    ''' </summary>
    Dim ds As New System.Data.DataSet("ds")

    Private Sub Report_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs)
        With CType(sender, DevExpress.XtraReports.UI.XtraReport)
            Dim sqlInfo As DebtPlus.LINQ.SQLInfoClass = DebtPlus.LINQ.SQLInfoClass.getDefault()

            '-- Read the infromation from the config file
            Dim cn As System.Data.SqlClient.SqlConnection = New System.Data.SqlClient.SqlConnection(sqlInfo.ConnectionString)
            Try
                cn.Open()

                '-- Read the data from the database
                Using cmd As New System.Data.SqlClient.SqlCommand()
                    With cmd
                        .Connection = cn
                        .CommandText = "rpt_CreditorDisbursementNotes"
                        .CommandType = System.Data.CommandType.StoredProcedure
                        .CommandTimeout = 0
                    End With

                    Using da As New System.Data.SqlClient.SqlDataAdapter(cmd)
                        da.Fill(ds, "rpt_CreditorDisbursementNotes")
                    End Using
                End Using

                '-- Set the datasource to the creditor view
                Dim tbl As System.Data.DataTable = ds.Tables("rpt_CreditorDisbursementNotes")
                If Not tbl.Columns.Contains("fmt_amount") Then
                    tbl.Columns.Add("fmt_amount", GetType(Decimal), "iif([amount]=0,NULL,[amount])")
                End If

                .DataSource = New System.Data.DataView(tbl, String.Empty, "creditor", System.Data.DataViewRowState.CurrentRows)

            Catch ex As System.Data.SqlClient.SqlException
                Dim gdr As New DebtPlus.Repository.GetDataResult() : gdr.HandleException(ex) : DebtPlus.UI.Common.ErrorHandling.HandleErrors(gdr, "Error trying to read report information")

            Finally
                If cn IsNot Nothing Then
                    If cn.State <> System.Data.ConnectionState.Closed Then cn.Close()
                    cn.Dispose()
                End If
            End Try
        End With
    End Sub


    ''' <summary>
    '''     Format the postal barcode before it is printed
    ''' </summary>
    Private Sub XrBarCode_PostalCode_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs)
        With CType(sender, DevExpress.XtraReports.UI.XRBarCode)
            Dim rpt As DevExpress.XtraReports.UI.XtraReport = CType(.Report, DevExpress.XtraReports.UI.XtraReport)
            Dim sqlInfo As DebtPlus.LINQ.SQLInfoClass = DebtPlus.LINQ.SQLInfoClass.getDefault()
            Dim tbl As System.Data.DataTable = ds.Tables("rpt_CreditorAddress_L")
            Dim row As System.Data.DataRow = Nothing

            '-- The creditor is part of the current row being printed
            Dim Creditor As String = String.Empty
            If GetCurrentColumnValue("creditor") IsNot System.DBNull.Value Then Creditor = Convert.ToString(GetCurrentColumnValue("creditor"))

            '-- If there is a table then find the row
            If tbl IsNot Nothing Then
                row = tbl.Rows.Find(Creditor)
            End If

            If row Is Nothing Then
                Dim cn As System.Data.SqlClient.SqlConnection = New System.Data.SqlClient.SqlConnection(sqlInfo.ConnectionString)
                Try
                    cn.Open()
                    Using cmd As New System.Data.SqlClient.SqlCommand()
                        With cmd
                            .Connection = cn
                            .CommandText = "rpt_CreditorAddress_L"
                            .CommandType = System.Data.CommandType.StoredProcedure
                            .Parameters.Add("@creditor", System.Data.SqlDbType.VarChar, 10).Value = Creditor
                            .CommandTimeout = 0
                        End With

                        Using da As New System.Data.SqlClient.SqlDataAdapter(cmd)
                            da.Fill(ds, "rpt_CreditorAddress_L")
                            tbl = ds.Tables("rpt_CreditorAddress_L")
                            With tbl
                                If .PrimaryKey.GetUpperBound(0) < 0 Then
                                    .PrimaryKey = New System.Data.DataColumn() {.Columns("creditor")}
                                End If
                            End With
                        End Using
                    End Using

                    If tbl IsNot Nothing Then
                        row = tbl.Rows.Find(Creditor)
                    End If

                Catch ex As System.Data.SqlClient.SqlException
                    Dim gdr As New DebtPlus.Repository.GetDataResult() : gdr.HandleException(ex) : DebtPlus.UI.Common.ErrorHandling.HandleErrors(gdr, "Error reading creditor address")
                End Try
            End If

            Dim text_block As String = String.Empty
            If row IsNot Nothing Then
                If row("zipcode") IsNot Nothing AndAlso row("zipcode") IsNot System.DBNull.Value Then
                    text_block = Convert.ToString(row("zipcode"))
                End If
                text_block = System.Text.RegularExpressions.Regex.Replace(text_block, "[^0-9]", String.Empty)
            End If

            If text_block = String.Empty Then
                e.Cancel = True
            Else
                .Text = text_block
            End If
        End With
    End Sub


    ''' <summary>
    '''     Format the creditor address before it is printed
    ''' </summary>
    Private Sub XrLabel_Creditor_Address_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs)
        With CType(sender, DevExpress.XtraReports.UI.XRLabel)
            Dim rpt As DevExpress.XtraReports.UI.XtraReport = CType(.Report, DevExpress.XtraReports.UI.XtraReport)
            Dim sqlInfo As DebtPlus.LINQ.SQLInfoClass = DebtPlus.LINQ.SQLInfoClass.getDefault()
            Dim tbl As System.Data.DataTable = ds.Tables("creditor_addresses")
            Dim row As System.Data.DataRow = Nothing

            '-- The creditor is part of the current row being printed
            Dim Creditor As String = String.Empty
            If GetCurrentColumnValue("creditor") IsNot System.DBNull.Value Then Creditor = Convert.ToString(GetCurrentColumnValue("creditor"))

            '-- If there is a table then find the row
            If tbl IsNot Nothing Then
                row = tbl.Rows.Find(Creditor)
            End If

            If row Is Nothing Then
                Dim cn As System.Data.SqlClient.SqlConnection = New System.Data.SqlClient.SqlConnection(sqlInfo.ConnectionString)
                Try
                    cn.Open()
                    Using cmd As New System.Data.SqlClient.SqlCommand()
                        With cmd
                            .Connection = cn
                            .CommandText = "rpt_CreditorAddress_L"
                            .CommandType = System.Data.CommandType.StoredProcedure
                            .Parameters.Add("@creditor", System.Data.SqlDbType.VarChar, 10).Value = Creditor
                            .CommandTimeout = 0
                        End With

                        Using da As New System.Data.SqlClient.SqlDataAdapter(cmd)
                            da.Fill(ds, "rpt_CreditorAddress_L")
                            tbl = ds.Tables("rpt_CreditorAddress_L")
                            With tbl
                                If .PrimaryKey.GetUpperBound(0) < 0 Then
                                    .PrimaryKey = New System.Data.DataColumn() {.Columns("creditor")}
                                End If
                            End With
                        End Using
                    End Using

                    If tbl IsNot Nothing Then
                        row = tbl.Rows.Find(Creditor)
                    End If

                Catch ex As System.Data.SqlClient.SqlException
                    Dim gdr As New DebtPlus.Repository.GetDataResult() : gdr.HandleException(ex) : DebtPlus.UI.Common.ErrorHandling.HandleErrors(gdr, "Error reading creditor address")
                End Try
            End If

            Dim text_block As New System.Text.StringBuilder
            If row IsNot Nothing Then
                For Each NameString As String In New String() {"addr1", "addr2", "addr3", "addr4", "addr5", "addr6", "addr7"}
                    Dim Value As String
                    If row(NameString) IsNot Nothing AndAlso row(NameString) IsNot System.DBNull.Value Then
                        Value = Convert.ToString(row(NameString)).Trim
                        If Value <> String.Empty Then
                            text_block.Append(Environment.NewLine)
                            text_block.Append(Value)
                        End If
                    End If
                Next
            End If

            If text_block.Length > 0 Then
                text_block.Remove(0, 2)
            End If

            .Text = text_block.ToString
        End With
    End Sub


    ''' <summary>
    '''     Format the client ID and name
    ''' </summary>
    Private Sub XrLabel_client_id_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs)
        With DirectCast(sender, DevExpress.XtraReports.UI.XRLabel)
            Dim rpt As DevExpress.XtraReports.UI.XtraReport = CType(.Report, DevExpress.XtraReports.UI.XtraReport)
            Dim ClientID As String = String.Empty
            Dim ClientName As String = String.Empty
            ClientID = DebtPlus.Utils.Format.Client.FormatClientID(rpt.GetCurrentColumnValue("client_id"))
            If rpt.GetCurrentColumnValue("client_name") IsNot System.DBNull.Value Then ClientName = Convert.ToString(rpt.GetCurrentColumnValue("client_name"))

            If ClientID <> String.Empty AndAlso ClientName <> String.Empty Then
                .Text = String.Format("{0} {1}", ClientID, ClientName)
            Else
                .Text = ClientID + ClientName
            End If
        End With
    End Sub


    ''' <summary>
    '''     Format the comment line if there is one
    ''' </summary>
    Private Sub XrLabel_phone_number_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs)
        Const TableName As String = "xpr_OrganizationAddress"

        With CType(sender, DevExpress.XtraReports.UI.XRLabel)
            Dim rpt As DevExpress.XtraReports.UI.XtraReport = CType(.Report, DevExpress.XtraReports.UI.XtraReport)
            Dim tbl As System.Data.DataTable = ds.Tables(TableName)
            If tbl Is Nothing Then
                Dim sqlInfo As DebtPlus.LINQ.SQLInfoClass = DebtPlus.LINQ.SQLInfoClass.getDefault()
                Using cmd As SqlClient.SqlCommand = New System.Data.SqlClient.SqlCommand()
                    With cmd
                        .Connection = New System.Data.SqlClient.SqlConnection(sqlInfo.ConnectionString)
                        .CommandText = "xpr_OrganizationAddress"
                        .CommandType = CommandType.StoredProcedure
                    End With

                    Using da As New SqlClient.SqlDataAdapter(cmd)
                        da.Fill(ds, TableName)
                        tbl = ds.Tables(TableName)
                    End Using
                End Using
            End If

            Dim AgencyPhone As String = String.Empty
            If tbl.Rows.Count > 0 Then
                Dim row As System.Data.DataRow = tbl.Rows(0)
                For Each col As System.Data.DataColumn In tbl.Columns
                    If row(col) IsNot System.DBNull.Value Then
                        Select Case col.ColumnName.ToLower
                            Case "phone", "telephone"
                                AgencyPhone = Convert.ToString(row(col)).Trim
                            Case Else
                        End Select
                    End If
                Next
            End If

            .Text = String.Format("The following messages were noted in our system concerning your institution regarding our recent disbursement process. We have terminated the clients listed below. The reason is provided in the message column. Please take a moment and review them. If you have any questions, please don't hesitate to contact us at {0}." + Environment.NewLine + Environment.NewLine + "Thank You.", AgencyPhone)
        End With
    End Sub

    Private Sub XrPanel_AgencyAddress_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs)
        Const TableName As String = "xpr_OrganizationAddress"

        With CType(sender, DevExpress.XtraReports.UI.XRPanel)
            Dim rpt As DevExpress.XtraReports.UI.XtraReport = CType(.Report, DevExpress.XtraReports.UI.XtraReport)
            Dim tbl As System.Data.DataTable = ds.Tables(TableName)
            If tbl Is Nothing Then
                Dim sqlInfo As DebtPlus.LINQ.SQLInfoClass = DebtPlus.LINQ.SQLInfoClass.getDefault()
                Using cmd As SqlClient.SqlCommand = New System.Data.SqlClient.SqlCommand()
                    With cmd
                        .Connection = New System.Data.SqlClient.SqlConnection(sqlInfo.ConnectionString)
                        .CommandText = "xpr_OrganizationAddress"
                        .CommandType = CommandType.StoredProcedure
                    End With

                    Using da As New SqlClient.SqlDataAdapter(cmd)
                        da.Fill(ds, TableName)
                        tbl = ds.Tables(TableName)
                    End Using
                End Using
            End If

            Dim AgencyName As String = String.Empty
            Dim AgencyAddress1 As String = String.Empty
            Dim AgencyAddress2 As String = String.Empty
            Dim AgencyAddress3 As String = String.Empty
            Dim AgencyPhone As String = String.Empty
            Dim AgencyWWW As String = String.Empty

            If tbl.Rows.Count > 0 Then
                Dim row As System.Data.DataRow = tbl.Rows(0)
                For Each col As System.Data.DataColumn In tbl.Columns
                    If row(col) IsNot System.DBNull.Value Then
                        Select Case col.ColumnName.ToLower
                            Case "name"
                                AgencyName = Convert.ToString(row(col)).Trim
                            Case "addr1", "address1"
                                AgencyAddress1 = Convert.ToString(row(col)).Trim
                            Case "addr2", "address2"
                                AgencyAddress2 = Convert.ToString(row(col)).Trim
                            Case "addr3", "address3"
                                AgencyAddress3 = Convert.ToString(row(col)).Trim
                            Case "phone", "telephone"
                                AgencyPhone = Convert.ToString(row(col)).Trim
                            Case "www"
                                AgencyWWW = Convert.ToString(row(col)).Trim
                            Case Else
                        End Select
                    End If
                Next
            End If

            Dim ctl As DevExpress.XtraReports.UI.XRControl = .FindControl("XRLabel_Agency_Name", True)
            If ctl IsNot Nothing Then ctl.Text = AgencyName

            ctl = .FindControl("XrLabel_Agency_Address1", True)
            If ctl IsNot Nothing Then ctl.Text = AgencyAddress1

            ctl = .FindControl("XrLabel_Agency_Address2", True)
            If ctl IsNot Nothing Then ctl.Text = AgencyAddress2

            ctl = .FindControl("XrLabel_Agency_Address3", True)
            If ctl IsNot Nothing Then ctl.Text = AgencyAddress3

            ctl = .FindControl("XrLabel_Agency_Phone", True)
            If ctl IsNot Nothing Then ctl.Text = AgencyPhone

            ctl = .FindControl("XrLabel_Agency_WWW", True)
            If ctl IsNot Nothing Then ctl.Text = AgencyWWW
        End With
    End Sub
End Class
