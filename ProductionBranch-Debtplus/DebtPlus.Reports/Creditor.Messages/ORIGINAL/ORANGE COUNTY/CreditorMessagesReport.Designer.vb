﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class CreditorMessagesReport
    Inherits DebtPlus.Reports.Template.BaseXtraReportClass

    'XtraReport overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Designer
    'It can be modified using the Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(CreditorMessagesReport))
        Dim PostNetGenerator1 As DevExpress.XtraPrinting.BarCode.PostNetGenerator = New DevExpress.XtraPrinting.BarCode.PostNetGenerator
        Me.XrLabel_client_id = New DevExpress.XtraReports.UI.XRLabel
        Me.XrLabel_amount = New DevExpress.XtraReports.UI.XRLabel
        Me.XrLabel_account_number = New DevExpress.XtraReports.UI.XRLabel
        Me.XrLabel_note_date = New DevExpress.XtraReports.UI.XRLabel
        Me.XrLabel_message_text = New DevExpress.XtraReports.UI.XRLabel
        Me.PageHeader = New DevExpress.XtraReports.UI.PageHeaderBand
        Me.XrPanel_AgencyAddress = New DevExpress.XtraReports.UI.XRPanel
        Me.XrLabel_Agency_Address3 = New DevExpress.XtraReports.UI.XRLabel
        Me.XrLabel_Agency_Address1 = New DevExpress.XtraReports.UI.XRLabel
        Me.XRLabel_Agency_Name = New DevExpress.XtraReports.UI.XRLabel
        Me.XrLabel_Agency_Phone = New DevExpress.XtraReports.UI.XRLabel
        Me.XrLabel_Agency_Address2 = New DevExpress.XtraReports.UI.XRLabel
        Me.XrTable1 = New DevExpress.XtraReports.UI.XRTable
        Me.XrTableRow1 = New DevExpress.XtraReports.UI.XRTableRow
        Me.XrTableCell1 = New DevExpress.XtraReports.UI.XRTableCell
        Me.XrTableCell_creditor = New DevExpress.XtraReports.UI.XRTableCell
        Me.XrTableRow2 = New DevExpress.XtraReports.UI.XRTableRow
        Me.XrTableCell3 = New DevExpress.XtraReports.UI.XRTableCell
        Me.XrTableCell4 = New DevExpress.XtraReports.UI.XRTableCell
        Me.XrPageInfo1 = New DevExpress.XtraReports.UI.XRPageInfo
        Me.GroupHeader1 = New DevExpress.XtraReports.UI.GroupHeaderBand
        Me.XrPanel1 = New DevExpress.XtraReports.UI.XRPanel
        Me.XrLabel9 = New DevExpress.XtraReports.UI.XRLabel
        Me.XrLabel8 = New DevExpress.XtraReports.UI.XRLabel
        Me.XrLabel7 = New DevExpress.XtraReports.UI.XRLabel
        Me.XrLabel6 = New DevExpress.XtraReports.UI.XRLabel
        Me.XrLabel5 = New DevExpress.XtraReports.UI.XRLabel
        Me.GroupHeader2 = New DevExpress.XtraReports.UI.GroupHeaderBand
        Me.XrLabel_phone_number = New DevExpress.XtraReports.UI.XRLabel
        Me.XrPanel_CreditorAddress = New DevExpress.XtraReports.UI.XRPanel
        Me.XrLabel_Creditor_Address = New DevExpress.XtraReports.UI.XRLabel
        Me.XrBarCode_PostalCode = New DevExpress.XtraReports.UI.XRBarCode
        CType(Me.XrTable1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
        '
        'Detail
        '
        Me.Detail.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel_client_id, Me.XrLabel_amount, Me.XrLabel_account_number, Me.XrLabel_note_date, Me.XrLabel_message_text})
        Me.Detail.HeightF = 15.00003!
        Me.Detail.Name = "Detail"
        '
        'XrLabel_client_id
        '
        Me.XrLabel_client_id.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
        Me.XrLabel_client_id.ForeColor = System.Drawing.SystemColors.WindowText
        Me.XrLabel_client_id.KeepTogether = True
        Me.XrLabel_client_id.LocationFloat = New DevExpress.Utils.PointFloat(308.0!, 0.0!)
        Me.XrLabel_client_id.Multiline = True
        Me.XrLabel_client_id.Name = "XrLabel_client_id"
        Me.XrLabel_client_id.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel_client_id.Scripts.OnBeforePrint = "XrLabel_client_id_BeforePrint"
        Me.XrLabel_client_id.SizeF = New System.Drawing.SizeF(158.0!, 15.0!)
        Me.XrLabel_client_id.StylePriority.UseFont = False
        Me.XrLabel_client_id.StylePriority.UseForeColor = False
        Me.XrLabel_client_id.StylePriority.UseTextAlignment = False
        Me.XrLabel_client_id.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        Me.XrLabel_client_id.WordWrap = False
        '
        'XrLabel_amount
        '
        Me.XrLabel_amount.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "fmt_amount", "{0:c}")})
        Me.XrLabel_amount.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
        Me.XrLabel_amount.ForeColor = System.Drawing.SystemColors.WindowText
        Me.XrLabel_amount.KeepTogether = True
        Me.XrLabel_amount.LocationFloat = New DevExpress.Utils.PointFloat(75.0!, 0.0!)
        Me.XrLabel_amount.Name = "XrLabel_amount"
        Me.XrLabel_amount.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel_amount.SizeF = New System.Drawing.SizeF(67.0!, 15.0!)
        Me.XrLabel_amount.StylePriority.UseFont = False
        Me.XrLabel_amount.StylePriority.UseForeColor = False
        Me.XrLabel_amount.StylePriority.UseTextAlignment = False
        Me.XrLabel_amount.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
        Me.XrLabel_amount.WordWrap = False
        '
        'XrLabel_account_number
        '
        Me.XrLabel_account_number.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "account_number")})
        Me.XrLabel_account_number.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
        Me.XrLabel_account_number.ForeColor = System.Drawing.SystemColors.WindowText
        Me.XrLabel_account_number.KeepTogether = True
        Me.XrLabel_account_number.LocationFloat = New DevExpress.Utils.PointFloat(150.0!, 0.0!)
        Me.XrLabel_account_number.Name = "XrLabel_account_number"
        Me.XrLabel_account_number.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel_account_number.SizeF = New System.Drawing.SizeF(150.0!, 15.0!)
        Me.XrLabel_account_number.StylePriority.UseFont = False
        Me.XrLabel_account_number.StylePriority.UseForeColor = False
        Me.XrLabel_account_number.StylePriority.UseTextAlignment = False
        Me.XrLabel_account_number.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        Me.XrLabel_account_number.WordWrap = False
        '
        'XrLabel_note_date
        '
        Me.XrLabel_note_date.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "note_date", "{0:d}")})
        Me.XrLabel_note_date.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
        Me.XrLabel_note_date.ForeColor = System.Drawing.SystemColors.WindowText
        Me.XrLabel_note_date.KeepTogether = True
        Me.XrLabel_note_date.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
        Me.XrLabel_note_date.Name = "XrLabel_note_date"
        Me.XrLabel_note_date.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel_note_date.SizeF = New System.Drawing.SizeF(66.0!, 15.00003!)
        Me.XrLabel_note_date.StylePriority.UseFont = False
        Me.XrLabel_note_date.StylePriority.UseForeColor = False
        Me.XrLabel_note_date.StylePriority.UseTextAlignment = False
        Me.XrLabel_note_date.Text = "88/88/8888"
        Me.XrLabel_note_date.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
        Me.XrLabel_note_date.WordWrap = False
        '
        'XrLabel_message_text
        '
        Me.XrLabel_message_text.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "note_text")})
        Me.XrLabel_message_text.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
        Me.XrLabel_message_text.ForeColor = System.Drawing.SystemColors.WindowText
        Me.XrLabel_message_text.KeepTogether = True
        Me.XrLabel_message_text.LocationFloat = New DevExpress.Utils.PointFloat(475.0!, 0.0!)
        Me.XrLabel_message_text.Name = "XrLabel_message_text"
        Me.XrLabel_message_text.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel_message_text.SizeF = New System.Drawing.SizeF(275.0!, 15.0!)
        Me.XrLabel_message_text.StylePriority.UseFont = False
        Me.XrLabel_message_text.StylePriority.UseForeColor = False
        Me.XrLabel_message_text.StylePriority.UseTextAlignment = False
        Me.XrLabel_message_text.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        '
        'PageHeader
        '
        Me.PageHeader.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrPanel_AgencyAddress, Me.XrTable1})
        Me.PageHeader.HeightF = 149.2917!
        Me.PageHeader.Name = "PageHeader"
        '
        'XrPanel_AgencyAddress
        '
        Me.XrPanel_AgencyAddress.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel_Agency_Address3, Me.XrLabel_Agency_Address1, Me.XRLabel_Agency_Name, Me.XrLabel_Agency_Phone, Me.XrLabel_Agency_Address2})
        Me.XrPanel_AgencyAddress.LocationFloat = New DevExpress.Utils.PointFloat(466.0!, 41.0!)
        Me.XrPanel_AgencyAddress.Name = "XrPanel_AgencyAddress"
        Me.XrPanel_AgencyAddress.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
        Me.XrPanel_AgencyAddress.Scripts.OnBeforePrint = "XrPanel_AgencyAddress_BeforePrint"
        Me.XrPanel_AgencyAddress.SizeF = New System.Drawing.SizeF(308.0!, 85.00001!)
        '
        'XrLabel_Agency_Address3
        '
        Me.XrLabel_Agency_Address3.CanShrink = True
        Me.XrLabel_Agency_Address3.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.XrLabel_Agency_Address3.ForeColor = System.Drawing.Color.Teal
        Me.XrLabel_Agency_Address3.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 51.0!)
        Me.XrLabel_Agency_Address3.Name = "XrLabel_Agency_Address3"
        Me.XrLabel_Agency_Address3.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel_Agency_Address3.SizeF = New System.Drawing.SizeF(300.0!, 17.0!)
        Me.XrLabel_Agency_Address3.StylePriority.UseFont = False
        Me.XrLabel_Agency_Address3.StylePriority.UseTextAlignment = False
        Me.XrLabel_Agency_Address3.Text = "Address3"
        Me.XrLabel_Agency_Address3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
        '
        'XrLabel_Agency_Address1
        '
        Me.XrLabel_Agency_Address1.CanShrink = True
        Me.XrLabel_Agency_Address1.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.XrLabel_Agency_Address1.ForeColor = System.Drawing.Color.Teal
        Me.XrLabel_Agency_Address1.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 17.0!)
        Me.XrLabel_Agency_Address1.Name = "XrLabel_Agency_Address1"
        Me.XrLabel_Agency_Address1.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel_Agency_Address1.SizeF = New System.Drawing.SizeF(300.0!, 17.0!)
        Me.XrLabel_Agency_Address1.StylePriority.UseFont = False
        Me.XrLabel_Agency_Address1.StylePriority.UseTextAlignment = False
        Me.XrLabel_Agency_Address1.Text = "Address1"
        Me.XrLabel_Agency_Address1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
        '
        'XRLabel_Agency_Name
        '
        Me.XRLabel_Agency_Name.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
        Me.XRLabel_Agency_Name.ForeColor = System.Drawing.Color.Teal
        Me.XRLabel_Agency_Name.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
        Me.XRLabel_Agency_Name.Name = "XRLabel_Agency_Name"
        Me.XRLabel_Agency_Name.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XRLabel_Agency_Name.SizeF = New System.Drawing.SizeF(300.0!, 17.0!)
        Me.XRLabel_Agency_Name.StylePriority.UseTextAlignment = False
        Me.XRLabel_Agency_Name.Text = "AgencyName"
        Me.XRLabel_Agency_Name.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
        '
        'XrLabel_Agency_Phone
        '
        Me.XrLabel_Agency_Phone.CanShrink = True
        Me.XrLabel_Agency_Phone.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.XrLabel_Agency_Phone.ForeColor = System.Drawing.Color.Teal
        Me.XrLabel_Agency_Phone.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 68.0!)
        Me.XrLabel_Agency_Phone.Name = "XrLabel_Agency_Phone"
        Me.XrLabel_Agency_Phone.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel_Agency_Phone.SizeF = New System.Drawing.SizeF(300.0!, 17.0!)
        Me.XrLabel_Agency_Phone.StylePriority.UseFont = False
        Me.XrLabel_Agency_Phone.StylePriority.UseTextAlignment = False
        Me.XrLabel_Agency_Phone.Text = "Phone: (000) 555-1212"
        Me.XrLabel_Agency_Phone.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
        '
        'XrLabel_Agency_Address2
        '
        Me.XrLabel_Agency_Address2.CanShrink = True
        Me.XrLabel_Agency_Address2.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.XrLabel_Agency_Address2.ForeColor = System.Drawing.Color.Teal
        Me.XrLabel_Agency_Address2.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 34.0!)
        Me.XrLabel_Agency_Address2.Name = "XrLabel_Agency_Address2"
        Me.XrLabel_Agency_Address2.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel_Agency_Address2.SizeF = New System.Drawing.SizeF(300.0!, 17.0!)
        Me.XrLabel_Agency_Address2.StylePriority.UseFont = False
        Me.XrLabel_Agency_Address2.StylePriority.UseTextAlignment = False
        Me.XrLabel_Agency_Address2.Text = "Address2"
        Me.XrLabel_Agency_Address2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
        '
        'XrTable1
        '
        Me.XrTable1.LocationFloat = New DevExpress.Utils.PointFloat(60.0!, 41.0!)
        Me.XrTable1.Name = "XrTable1"
        Me.XrTable1.Rows.AddRange(New DevExpress.XtraReports.UI.XRTableRow() {Me.XrTableRow1, Me.XrTableRow2})
        Me.XrTable1.SizeF = New System.Drawing.SizeF(300.0!, 50.0!)
        '
        'XrTableRow1
        '
        Me.XrTableRow1.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.XrTableCell1, Me.XrTableCell_creditor})
        Me.XrTableRow1.Name = "XrTableRow1"
        Me.XrTableRow1.Weight = 0.5
        '
        'XrTableCell1
        '
        Me.XrTableCell1.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
        Me.XrTableCell1.ForeColor = System.Drawing.Color.Teal
        Me.XrTableCell1.Name = "XrTableCell1"
        Me.XrTableCell1.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrTableCell1.StylePriority.UseFont = False
        Me.XrTableCell1.StylePriority.UseForeColor = False
        Me.XrTableCell1.StylePriority.UseTextAlignment = False
        Me.XrTableCell1.Text = "Our Creditor ID:"
        Me.XrTableCell1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
        Me.XrTableCell1.Weight = 0.4
        '
        'XrTableCell_creditor
        '
        Me.XrTableCell_creditor.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "creditor")})
        Me.XrTableCell_creditor.Name = "XrTableCell_creditor"
        Me.XrTableCell_creditor.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrTableCell_creditor.StylePriority.UseTextAlignment = False
        Me.XrTableCell_creditor.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        Me.XrTableCell_creditor.Weight = 0.6
        '
        'XrTableRow2
        '
        Me.XrTableRow2.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.XrTableCell3, Me.XrTableCell4})
        Me.XrTableRow2.Name = "XrTableRow2"
        Me.XrTableRow2.Weight = 0.5
        '
        'XrTableCell3
        '
        Me.XrTableCell3.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
        Me.XrTableCell3.ForeColor = System.Drawing.Color.Teal
        Me.XrTableCell3.Name = "XrTableCell3"
        Me.XrTableCell3.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrTableCell3.StylePriority.UseFont = False
        Me.XrTableCell3.StylePriority.UseForeColor = False
        Me.XrTableCell3.StylePriority.UseTextAlignment = False
        Me.XrTableCell3.Text = "Date Printed:"
        Me.XrTableCell3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
        Me.XrTableCell3.Weight = 0.4
        '
        'XrTableCell4
        '
        Me.XrTableCell4.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrPageInfo1})
        Me.XrTableCell4.Name = "XrTableCell4"
        Me.XrTableCell4.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrTableCell4.StylePriority.UseTextAlignment = False
        Me.XrTableCell4.Text = "XrTableCell4"
        Me.XrTableCell4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        Me.XrTableCell4.Weight = 0.6
        '
        'XrPageInfo1
        '
        Me.XrPageInfo1.Format = "{0:MM/dd/yyyy}"
        Me.XrPageInfo1.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
        Me.XrPageInfo1.Name = "XrPageInfo1"
        Me.XrPageInfo1.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrPageInfo1.PageInfo = DevExpress.XtraPrinting.PageInfo.DateTime
        Me.XrPageInfo1.SizeF = New System.Drawing.SizeF(180.0!, 25.0!)
        Me.XrPageInfo1.StylePriority.UseTextAlignment = False
        Me.XrPageInfo1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        '
        'GroupHeader1
        '
        Me.GroupHeader1.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrPanel1})
        Me.GroupHeader1.HeightF = 22.91667!
        Me.GroupHeader1.KeepTogether = True
        Me.GroupHeader1.Name = "GroupHeader1"
        Me.GroupHeader1.RepeatEveryPage = True
        '
        'XrPanel1
        '
        Me.XrPanel1.BackColor = System.Drawing.Color.Teal
        Me.XrPanel1.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel9, Me.XrLabel8, Me.XrLabel7, Me.XrLabel6, Me.XrLabel5})
        Me.XrPanel1.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
        Me.XrPanel1.Name = "XrPanel1"
        Me.XrPanel1.SizeF = New System.Drawing.SizeF(750.0!, 17.0!)
        Me.XrPanel1.StylePriority.UseBackColor = False
        '
        'XrLabel9
        '
        Me.XrLabel9.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
        Me.XrLabel9.ForeColor = System.Drawing.Color.White
        Me.XrLabel9.LocationFloat = New DevExpress.Utils.PointFloat(75.0!, 1.0!)
        Me.XrLabel9.Name = "XrLabel9"
        Me.XrLabel9.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel9.SizeF = New System.Drawing.SizeF(67.0!, 15.0!)
        Me.XrLabel9.StylePriority.UseFont = False
        Me.XrLabel9.StylePriority.UseForeColor = False
        Me.XrLabel9.StylePriority.UseTextAlignment = False
        Me.XrLabel9.Text = "AMOUNT"
        Me.XrLabel9.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
        '
        'XrLabel8
        '
        Me.XrLabel8.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
        Me.XrLabel8.ForeColor = System.Drawing.Color.White
        Me.XrLabel8.LocationFloat = New DevExpress.Utils.PointFloat(150.0!, 1.0!)
        Me.XrLabel8.Name = "XrLabel8"
        Me.XrLabel8.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel8.SizeF = New System.Drawing.SizeF(150.0!, 15.0!)
        Me.XrLabel8.StylePriority.UseFont = False
        Me.XrLabel8.StylePriority.UseForeColor = False
        Me.XrLabel8.StylePriority.UseTextAlignment = False
        Me.XrLabel8.Text = "ACCOUNT NUMBER"
        Me.XrLabel8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        '
        'XrLabel7
        '
        Me.XrLabel7.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
        Me.XrLabel7.ForeColor = System.Drawing.Color.White
        Me.XrLabel7.LocationFloat = New DevExpress.Utils.PointFloat(308.0!, 1.0!)
        Me.XrLabel7.Name = "XrLabel7"
        Me.XrLabel7.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel7.SizeF = New System.Drawing.SizeF(158.0!, 15.0!)
        Me.XrLabel7.StylePriority.UseFont = False
        Me.XrLabel7.StylePriority.UseForeColor = False
        Me.XrLabel7.StylePriority.UseTextAlignment = False
        Me.XrLabel7.Text = "CLIENT ID AND NAME"
        Me.XrLabel7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        '
        'XrLabel6
        '
        Me.XrLabel6.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
        Me.XrLabel6.ForeColor = System.Drawing.Color.White
        Me.XrLabel6.LocationFloat = New DevExpress.Utils.PointFloat(475.0!, 1.0!)
        Me.XrLabel6.Name = "XrLabel6"
        Me.XrLabel6.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel6.SizeF = New System.Drawing.SizeF(275.0!, 15.0!)
        Me.XrLabel6.StylePriority.UseFont = False
        Me.XrLabel6.StylePriority.UseForeColor = False
        Me.XrLabel6.StylePriority.UseTextAlignment = False
        Me.XrLabel6.Text = "MESSAGE"
        Me.XrLabel6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        '
        'XrLabel5
        '
        Me.XrLabel5.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
        Me.XrLabel5.ForeColor = System.Drawing.Color.White
        Me.XrLabel5.LocationFloat = New DevExpress.Utils.PointFloat(8.0!, 1.0!)
        Me.XrLabel5.Name = "XrLabel5"
        Me.XrLabel5.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel5.SizeF = New System.Drawing.SizeF(58.0!, 15.0!)
        Me.XrLabel5.StylePriority.UseFont = False
        Me.XrLabel5.StylePriority.UseForeColor = False
        Me.XrLabel5.StylePriority.UseTextAlignment = False
        Me.XrLabel5.Text = "DATE"
        Me.XrLabel5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
        '
        'GroupHeader2
        '
        Me.GroupHeader2.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel_phone_number, Me.XrPanel_CreditorAddress})
        Me.GroupHeader2.GroupFields.AddRange(New DevExpress.XtraReports.UI.GroupField() {New DevExpress.XtraReports.UI.GroupField("creditor", DevExpress.XtraReports.UI.XRColumnSortOrder.Ascending)})
        Me.GroupHeader2.HeightF = 348.9583!
        Me.GroupHeader2.KeepTogether = True
        Me.GroupHeader2.Level = 1
        Me.GroupHeader2.Name = "GroupHeader2"
        Me.GroupHeader2.PageBreak = DevExpress.XtraReports.UI.PageBreak.BeforeBand
        '
        'XrLabel_phone_number
        '
        Me.XrLabel_phone_number.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
        Me.XrLabel_phone_number.LocationFloat = New DevExpress.Utils.PointFloat(10.00001!, 248.6666!)
        Me.XrLabel_phone_number.Multiline = True
        Me.XrLabel_phone_number.Name = "XrLabel_phone_number"
        Me.XrLabel_phone_number.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel_phone_number.Scripts.OnBeforePrint = "XrLabel_phone_number_BeforePrint"
        Me.XrLabel_phone_number.SizeF = New System.Drawing.SizeF(742.0!, 83.0!)
        Me.XrLabel_phone_number.StylePriority.UseFont = False
        Me.XrLabel_phone_number.Text = resources.GetString("XrLabel_phone_number.Text")
        '
        'XrPanel_CreditorAddress
        '
        Me.XrPanel_CreditorAddress.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel_Creditor_Address, Me.XrBarCode_PostalCode})
        Me.XrPanel_CreditorAddress.LocationFloat = New DevExpress.Utils.PointFloat(75.0!, 120.0!)
        Me.XrPanel_CreditorAddress.Name = "XrPanel_CreditorAddress"
        Me.XrPanel_CreditorAddress.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
        Me.XrPanel_CreditorAddress.SizeF = New System.Drawing.SizeF(300.0!, 117.0!)
        '
        'XrLabel_Creditor_Address
        '
        Me.XrLabel_Creditor_Address.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 16.0!)
        Me.XrLabel_Creditor_Address.Multiline = True
        Me.XrLabel_Creditor_Address.Name = "XrLabel_Creditor_Address"
        Me.XrLabel_Creditor_Address.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel_Creditor_Address.Scripts.OnBeforePrint = "XrLabel_Creditor_Address_BeforePrint"
        Me.XrLabel_Creditor_Address.SizeF = New System.Drawing.SizeF(300.0!, 100.0!)
        Me.XrLabel_Creditor_Address.Text = "XrLabel_Creditor_Address"
        Me.XrLabel_Creditor_Address.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'XrBarCode_PostalCode
        '
        Me.XrBarCode_PostalCode.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
        Me.XrBarCode_PostalCode.Name = "XrBarCode_PostalCode"
        Me.XrBarCode_PostalCode.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
        Me.XrBarCode_PostalCode.Scripts.OnBeforePrint = "XrBarCode_PostalCode_BeforePrint"
        Me.XrBarCode_PostalCode.ShowText = False
        Me.XrBarCode_PostalCode.SizeF = New System.Drawing.SizeF(300.0!, 16.0!)
        Me.XrBarCode_PostalCode.Symbology = PostNetGenerator1
        Me.XrBarCode_PostalCode.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'CreditorMessagesReport
        '
        Me.Bands.AddRange(New DevExpress.XtraReports.UI.Band() {Me.PageHeader, Me.GroupHeader1, Me.GroupHeader2})
        Me.ScriptLanguage = DevExpress.XtraReports.ScriptLanguage.VisualBasic
        Me.ScriptReferencesString = resources.GetString("$this.ScriptReferencesString")
        Me.Scripts.OnBeforePrint = "CreditorMessagesReport_BeforePrint"
        Me.ScriptsSource = resources.GetString("$this.ScriptsSource")
        Me.Version = "10.2"
        CType(Me.XrTable1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me, System.ComponentModel.ISupportInitialize).EndInit()

    End Sub

    Friend WithEvents PageHeader As DevExpress.XtraReports.UI.PageHeaderBand
    Friend WithEvents GroupHeader1 As DevExpress.XtraReports.UI.GroupHeaderBand
    Friend WithEvents XrTable1 As DevExpress.XtraReports.UI.XRTable
    Friend WithEvents XrTableRow1 As DevExpress.XtraReports.UI.XRTableRow
    Friend WithEvents XrTableCell1 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents XrTableCell_creditor As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents XrTableRow2 As DevExpress.XtraReports.UI.XRTableRow
    Friend WithEvents XrTableCell3 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents XrTableCell4 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents XrPageInfo1 As DevExpress.XtraReports.UI.XRPageInfo
    Friend WithEvents XrLabel_client_id As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel_amount As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel_account_number As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel_note_date As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel_message_text As DevExpress.XtraReports.UI.XRLabel
    Protected WithEvents XrPanel_AgencyAddress As DevExpress.XtraReports.UI.XRPanel
    Protected WithEvents XrLabel_Agency_Address3 As DevExpress.XtraReports.UI.XRLabel
    Protected WithEvents XrLabel_Agency_Address1 As DevExpress.XtraReports.UI.XRLabel
    Protected WithEvents XRLabel_Agency_Name As DevExpress.XtraReports.UI.XRLabel
    Protected WithEvents XrLabel_Agency_Phone As DevExpress.XtraReports.UI.XRLabel
    Protected WithEvents XrLabel_Agency_Address2 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents GroupHeader2 As DevExpress.XtraReports.UI.GroupHeaderBand
    Friend WithEvents XrPanel1 As DevExpress.XtraReports.UI.XRPanel
    Friend WithEvents XrLabel9 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel8 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel7 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel6 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel5 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel_phone_number As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrPanel_CreditorAddress As DevExpress.XtraReports.UI.XRPanel
    Friend WithEvents XrLabel_Creditor_Address As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrBarCode_PostalCode As DevExpress.XtraReports.UI.XRBarCode
End Class
