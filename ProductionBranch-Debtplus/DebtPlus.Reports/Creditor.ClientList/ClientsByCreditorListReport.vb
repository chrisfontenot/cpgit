#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}
#End Region

Imports DebtPlus.Interfaces.Creditor
Imports DebtPlus.Reports.Template
Imports System.IO
Imports DevExpress.XtraReports.UI
Imports DebtPlus.Reports.Template.Forms
Imports DebtPlus.Utils

Namespace Creditor.ClientList
    Public Class ClientsByCreditorListReport
        Inherits TemplateXtraReportClass
        Implements ICreditor

        Public Sub New()
            MyBase.New()

            Const ReportName As String = "DebtPlus.Reports.Creditor.ClientList.repx"

            ' See if there is a report reference that we can use
            Dim Fname As String = System.IO.Path.Combine(DebtPlus.Configuration.Config.ReportsDirectory, ReportName)
            Dim UseDefault As Boolean = True
            Try
                If (Not DebtPlus.Configuration.Config.LocalReportsOnly) AndAlso System.IO.File.Exists(Fname) Then
                    LoadLayout(Fname)
                    UseDefault = False
                End If
            Catch ex As Exception
            End Try

            ' Load the standard report definition if we need a new item
            If UseDefault Then
                Dim asm As Reflection.Assembly = Reflection.Assembly.GetExecutingAssembly()
                Using ios As Stream = asm.GetManifestResourceStream(String.Format("DebtPlus.Reports.{0}", ReportName))
                    If ios IsNot Nothing Then
                        LoadLayout(ios)
                    End If
                End Using
            End If

            ' Set the script references as needed
            ScriptReferences = DebtPlus.Utils.AssemblyTools.GetAllSiblingAssemblies()

            ' Copy the script references to the subreports as well
            For Each Band As Band In Bands
                For Each ctl As XRControl In Band.Controls
                    Dim rpt As XRSubreport = TryCast(ctl, XRSubreport)
                    If rpt IsNot Nothing Then
                        Dim SubRpt As XtraReport = TryCast(rpt.ReportSource, XtraReport)
                        If SubRpt IsNot Nothing Then
                            SubRpt.ScriptReferences = DebtPlus.Utils.AssemblyTools.GetAllSiblingAssemblies()
                        End If
                    End If
                Next
            Next

            ' Remove the parameter values
            Parameters("ParameterCreditor").Value = String.Empty

            ' Enable the select expert
            ReportFilter.IsEnabled = True
        End Sub

        ''' <summary>
        '''     Report title information
        ''' </summary>
        Public Overrides ReadOnly Property ReportTitle() As String
            Get
                Return "Clients By Creditor"
            End Get
        End Property

        ''' <summary>
        '''     Report subtitle information
        ''' </summary>
        Public Overrides ReadOnly Property ReportSubtitle() As String
            Get
                Dim CreditorName As String = String.Empty
                Dim vue As DataView = TryCast(DataSource, DataView)
                If vue IsNot Nothing Then
                    Dim ds As DataSet = CType(vue.Table.DataSet, DataSet)
                    Dim tbl As DataTable = ds.Tables("creditors")
                    If tbl IsNot Nothing AndAlso tbl.Rows.Count > 0 Then
                        Dim row As DataRow = tbl.Rows(0)
                        CreditorName = DebtPlus.Utils.Nulls.DStr(row("creditor_name")).Trim()
                    End If
                End If

                Dim Answer As String
                Dim id As String = DebtPlus.Utils.Nulls.DStr(Parameters("ParameterCreditor").Value)
                If id <> String.Empty AndAlso CreditorName <> String.Empty Then
                    Answer = String.Format("{0} {1}", id, CreditorName)
                Else
                    Answer = id + CreditorName
                End If

                Return Answer
            End Get
        End Property

        ''' <summary>
        '''     Parameter for the creditor ID
        ''' </summary>
        Public Property Parameter_Creditor() As String Implements ICreditor.Creditor
            Get
                Return CType(Parameters("ParameterCreditor").Value, String)
            End Get
            Set(ByVal Value As String)
                Parameters("ParameterCreditor").Value = Value
            End Set
        End Property

        ''' <summary>
        '''     Do we need to request parameters?
        ''' </summary>
        Public Overrides Function NeedParameters() As Boolean
            Return MyBase.NeedParameters() OrElse Parameter_Creditor = String.Empty
        End Function

        ''' <summary>
        '''     Change the parameter value for this report
        ''' </summary>
        Public Overrides Sub SetReportParameter(ByVal Parameter As String, ByVal Value As Object)
            Select Case Parameter
                Case "Creditor"
                    Parameter_Creditor = Convert.ToString(Value)
                Case Else
                    MyBase.SetReportParameter(Parameter, Value)
            End Select
        End Sub

        ''' <summary>
        '''     Request the parameters for the report from the user
        ''' </summary>
        Public Overrides Function RequestReportParameters() As DialogResult
            Dim answer As DialogResult = DialogResult.OK
            If NeedParameters() Then
                Using frm As New DebtPlus.Reports.CS.Template.Forms.CreditorParametersForm() ' CreditorParametersForm()
                    answer = frm.ShowDialog()
                    Parameter_Creditor = frm.Parameter_Creditor
                End Using
            End If
            Return answer
        End Function
    End Class
End Namespace
