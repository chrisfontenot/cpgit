Namespace Creditor.ClientList
    <Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
    Partial Class ClientsByCreditorListReport
        Inherits DebtPlus.Reports.Template.TemplateXtraReportClass

        'XtraReport overrides dispose to clean up the component list.
        <System.Diagnostics.DebuggerNonUserCode()> _
        Protected Overrides Sub Dispose(ByVal disposing As Boolean)
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
            MyBase.Dispose(disposing)
        End Sub

        'Required by the Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Designer
        'It can be modified using the Designer.  
        'Do not modify it using the code editor.
        <System.Diagnostics.DebuggerStepThrough()> _
        Private Sub InitializeComponent()
            Dim XrSummary1 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary()
            Dim XrSummary2 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary()
            Dim XrSummary3 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary()
            Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(ClientsByCreditorListReport))
            Me.Style_Detail_LText = New DevExpress.XtraReports.UI.XRControlStyle()
            Me.Style_Total_Amount = New DevExpress.XtraReports.UI.XRControlStyle()
            Me.Style_Heading_Amount = New DevExpress.XtraReports.UI.XRControlStyle()
            Me.Style_Heading_LText = New DevExpress.XtraReports.UI.XRControlStyle()
            Me.Style_Heading_Pannel = New DevExpress.XtraReports.UI.XRControlStyle()
            Me.Style_Detail_Amount = New DevExpress.XtraReports.UI.XRControlStyle()
            Me.Style_Heading_RText = New DevExpress.XtraReports.UI.XRControlStyle()
            Me.Style_Total_LText = New DevExpress.XtraReports.UI.XRControlStyle()
            Me.Style_Detail_RText = New DevExpress.XtraReports.UI.XRControlStyle()
            Me.Style_Total_RText = New DevExpress.XtraReports.UI.XRControlStyle()
            Me.XrLabel_Client = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_OriginalBalance = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_CurrentBalance = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_LastPaymentDate = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_LastPaymentAmount = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_TotalPayments = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_StartDate = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_Status = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_AccountNumber = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrPanel1 = New DevExpress.XtraReports.UI.XRPanel()
            Me.XrLabel9 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel8 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel7 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel6 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel5 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel3 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel2 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel1 = New DevExpress.XtraReports.UI.XRLabel()
            Me.ReportFooter = New DevExpress.XtraReports.UI.ReportFooterBand()
            Me.XrLine1 = New DevExpress.XtraReports.UI.XRLine()
            Me.XrLabel_Total_CurrentBalance = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_Total_TotalPayments = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_Total_OriginalBalance = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel4 = New DevExpress.XtraReports.UI.XRLabel()
            Me.ParameterCreditor = New DevExpress.XtraReports.Parameters.Parameter()
            CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
            '
            'PageHeader
            '
            Me.PageHeader.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrPanel1})
            Me.PageHeader.HeightF = 147.0!
            Me.PageHeader.Controls.SetChildIndex(Me.XrPanel1, 0)
            Me.PageHeader.Controls.SetChildIndex(Me.XrPanel_AgencyAddress, 0)
            Me.PageHeader.Controls.SetChildIndex(Me.XrLabel_Title, 0)
            Me.PageHeader.Controls.SetChildIndex(Me.XrLine_Title, 0)
            Me.PageHeader.Controls.SetChildIndex(Me.XrLabel_Subtitle, 0)
            Me.PageHeader.Controls.SetChildIndex(Me.XrPageInfo1, 0)
            '
            'PageFooter
            '
            Me.PageFooter.HeightF = 35.0!
            '
            'XrLabel_Title
            '
            Me.XrLabel_Title.StylePriority.UseFont = False
            '
            'XrPageInfo_PageNumber
            '
            Me.XrPageInfo_PageNumber.StylePriority.UseTextAlignment = False
            '
            'XRLabel_Agency_Name
            '
            Me.XRLabel_Agency_Name.StylePriority.UseTextAlignment = False
            '
            'XrLabel_Agency_Address3
            '
            Me.XrLabel_Agency_Address3.StylePriority.UseFont = False
            Me.XrLabel_Agency_Address3.StylePriority.UseTextAlignment = False
            '
            'XrLabel_Agency_Address1
            '
            Me.XrLabel_Agency_Address1.StylePriority.UseFont = False
            Me.XrLabel_Agency_Address1.StylePriority.UseTextAlignment = False
            '
            'XrLabel_Agency_Phone
            '
            Me.XrLabel_Agency_Phone.StylePriority.UseFont = False
            Me.XrLabel_Agency_Phone.StylePriority.UseTextAlignment = False
            '
            'XrLabel_Agency_Address2
            '
            Me.XrLabel_Agency_Address2.StylePriority.UseFont = False
            Me.XrLabel_Agency_Address2.StylePriority.UseTextAlignment = False
            '
            'Detail
            '
            Me.Detail.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel_AccountNumber, Me.XrLabel_Status, Me.XrLabel_StartDate, Me.XrLabel_TotalPayments, Me.XrLabel_LastPaymentAmount, Me.XrLabel_LastPaymentDate, Me.XrLabel_CurrentBalance, Me.XrLabel_OriginalBalance, Me.XrLabel_Client})
            Me.Detail.HeightF = 15.0!
            '
            'Style_Detail_LText
            '
            Me.Style_Detail_LText.Name = "Style_Detail_LText"
            Me.Style_Detail_LText.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            '
            'Style_Total_Amount
            '
            Me.Style_Total_Amount.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.Style_Total_Amount.Name = "Style_Total_Amount"
            '
            'Style_Heading_Amount
            '
            Me.Style_Heading_Amount.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.Style_Heading_Amount.ForeColor = System.Drawing.Color.White
            Me.Style_Heading_Amount.Name = "Style_Heading_Amount"
            Me.Style_Heading_Amount.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'Style_Heading_LText
            '
            Me.Style_Heading_LText.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.Style_Heading_LText.ForeColor = System.Drawing.Color.White
            Me.Style_Heading_LText.Name = "Style_Heading_LText"
            Me.Style_Heading_LText.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            '
            'Style_Heading_Pannel
            '
            Me.Style_Heading_Pannel.BackColor = System.Drawing.Color.Teal
            Me.Style_Heading_Pannel.BorderColor = System.Drawing.Color.Teal
            Me.Style_Heading_Pannel.Borders = CType((((DevExpress.XtraPrinting.BorderSide.Left Or DevExpress.XtraPrinting.BorderSide.Top) _
                Or DevExpress.XtraPrinting.BorderSide.Right) _
                Or DevExpress.XtraPrinting.BorderSide.Bottom), DevExpress.XtraPrinting.BorderSide)
            Me.Style_Heading_Pannel.ForeColor = System.Drawing.Color.White
            Me.Style_Heading_Pannel.Name = "Style_Heading_Pannel"
            Me.Style_Heading_Pannel.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            '
            'Style_Detail_Amount
            '
            Me.Style_Detail_Amount.Name = "Style_Detail_Amount"
            Me.Style_Detail_Amount.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'Style_Heading_RText
            '
            Me.Style_Heading_RText.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.Style_Heading_RText.ForeColor = System.Drawing.Color.White
            Me.Style_Heading_RText.Name = "Style_Heading_RText"
            Me.Style_Heading_RText.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'Style_Total_LText
            '
            Me.Style_Total_LText.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.Style_Total_LText.Name = "Style_Total_LText"
            '
            'Style_Detail_RText
            '
            Me.Style_Detail_RText.Name = "Style_Detail_RText"
            Me.Style_Detail_RText.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'Style_Total_RText
            '
            Me.Style_Total_RText.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.Style_Total_RText.Name = "Style_Total_RText"
            '
            'XrLabel_Client
            '
            Me.XrLabel_Client.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
            Me.XrLabel_Client.Name = "XrLabel_Client"
            Me.XrLabel_Client.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_Client.SizeF = New System.Drawing.SizeF(190.1666!, 15.0!)
            Me.XrLabel_Client.StyleName = "Style_Detail_LText"
            Me.XrLabel_Client.Text = "[client!0000000] [name]"
            Me.XrLabel_Client.WordWrap = False
            '
            'XrLabel_OriginalBalance
            '
            Me.XrLabel_OriginalBalance.CanGrow = False
            Me.XrLabel_OriginalBalance.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "orig_balance", "{0:c}")})
            Me.XrLabel_OriginalBalance.KeepTogether = True
            Me.XrLabel_OriginalBalance.LocationFloat = New DevExpress.Utils.PointFloat(361.0!, 0.0!)
            Me.XrLabel_OriginalBalance.Name = "XrLabel_OriginalBalance"
            Me.XrLabel_OriginalBalance.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_OriginalBalance.SizeF = New System.Drawing.SizeF(75.0!, 15.0!)
            Me.XrLabel_OriginalBalance.StyleName = "Style_Detail_Amount"
            Me.XrLabel_OriginalBalance.StylePriority.UseTextAlignment = False
            Me.XrLabel_OriginalBalance.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrLabel_OriginalBalance.WordWrap = False
            '
            'XrLabel_CurrentBalance
            '
            Me.XrLabel_CurrentBalance.CanGrow = False
            Me.XrLabel_CurrentBalance.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "current_balance", "{0:c}")})
            Me.XrLabel_CurrentBalance.KeepTogether = True
            Me.XrLabel_CurrentBalance.LocationFloat = New DevExpress.Utils.PointFloat(720.4583!, 0.0!)
            Me.XrLabel_CurrentBalance.Name = "XrLabel_CurrentBalance"
            Me.XrLabel_CurrentBalance.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_CurrentBalance.SizeF = New System.Drawing.SizeF(79.54169!, 15.0!)
            Me.XrLabel_CurrentBalance.StyleName = "Style_Detail_Amount"
            Me.XrLabel_CurrentBalance.StylePriority.UseTextAlignment = False
            Me.XrLabel_CurrentBalance.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrLabel_CurrentBalance.WordWrap = False
            '
            'XrLabel_LastPaymentDate
            '
            Me.XrLabel_LastPaymentDate.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "last_payment_date", "{0:d}")})
            Me.XrLabel_LastPaymentDate.KeepTogether = True
            Me.XrLabel_LastPaymentDate.LocationFloat = New DevExpress.Utils.PointFloat(651.0001!, 0.0!)
            Me.XrLabel_LastPaymentDate.Name = "XrLabel_LastPaymentDate"
            Me.XrLabel_LastPaymentDate.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_LastPaymentDate.SizeF = New System.Drawing.SizeF(69.45825!, 15.0!)
            Me.XrLabel_LastPaymentDate.StyleName = "Style_Detail_RText"
            Me.XrLabel_LastPaymentDate.StylePriority.UseTextAlignment = False
            Me.XrLabel_LastPaymentDate.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrLabel_LastPaymentDate.WordWrap = False
            '
            'XrLabel_LastPaymentAmount
            '
            Me.XrLabel_LastPaymentAmount.CanGrow = False
            Me.XrLabel_LastPaymentAmount.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "last_payment_amount", "{0:c}")})
            Me.XrLabel_LastPaymentAmount.KeepTogether = True
            Me.XrLabel_LastPaymentAmount.LocationFloat = New DevExpress.Utils.PointFloat(584.0!, 0.0!)
            Me.XrLabel_LastPaymentAmount.Name = "XrLabel_LastPaymentAmount"
            Me.XrLabel_LastPaymentAmount.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_LastPaymentAmount.SizeF = New System.Drawing.SizeF(66.0!, 15.0!)
            Me.XrLabel_LastPaymentAmount.StyleName = "Style_Detail_Amount"
            Me.XrLabel_LastPaymentAmount.StylePriority.UseTextAlignment = False
            Me.XrLabel_LastPaymentAmount.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrLabel_LastPaymentAmount.WordWrap = False
            '
            'XrLabel_TotalPayments
            '
            Me.XrLabel_TotalPayments.CanGrow = False
            Me.XrLabel_TotalPayments.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "principle_payments", "{0:c}")})
            Me.XrLabel_TotalPayments.KeepTogether = True
            Me.XrLabel_TotalPayments.LocationFloat = New DevExpress.Utils.PointFloat(509.0!, 0.0!)
            Me.XrLabel_TotalPayments.Name = "XrLabel_TotalPayments"
            Me.XrLabel_TotalPayments.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_TotalPayments.SizeF = New System.Drawing.SizeF(75.0!, 15.0!)
            Me.XrLabel_TotalPayments.StyleName = "Style_Detail_Amount"
            Me.XrLabel_TotalPayments.StylePriority.UseTextAlignment = False
            Me.XrLabel_TotalPayments.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrLabel_TotalPayments.WordWrap = False
            '
            'XrLabel_StartDate
            '
            Me.XrLabel_StartDate.CanGrow = False
            Me.XrLabel_StartDate.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "start_date", "{0:d}")})
            Me.XrLabel_StartDate.KeepTogether = True
            Me.XrLabel_StartDate.LocationFloat = New DevExpress.Utils.PointFloat(436.0!, 0.0!)
            Me.XrLabel_StartDate.Name = "XrLabel_StartDate"
            Me.XrLabel_StartDate.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_StartDate.SizeF = New System.Drawing.SizeF(73.00003!, 15.0!)
            Me.XrLabel_StartDate.StyleName = "Style_Detail_RText"
            Me.XrLabel_StartDate.StylePriority.UseTextAlignment = False
            Me.XrLabel_StartDate.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrLabel_StartDate.WordWrap = False
            '
            'XrLabel_Status
            '
            Me.XrLabel_Status.CanGrow = False
            Me.XrLabel_Status.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "status")})
            Me.XrLabel_Status.KeepTogether = True
            Me.XrLabel_Status.LocationFloat = New DevExpress.Utils.PointFloat(322.9999!, 0.0!)
            Me.XrLabel_Status.Name = "XrLabel_Status"
            Me.XrLabel_Status.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_Status.SizeF = New System.Drawing.SizeF(38.00003!, 15.0!)
            Me.XrLabel_Status.StyleName = "Style_Detail_RText"
            Me.XrLabel_Status.StylePriority.UseTextAlignment = False
            Me.XrLabel_Status.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
            Me.XrLabel_Status.WordWrap = False
            '
            'XrLabel_AccountNumber
            '
            Me.XrLabel_AccountNumber.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "account_number")})
            Me.XrLabel_AccountNumber.LocationFloat = New DevExpress.Utils.PointFloat(190.1666!, 0.0!)
            Me.XrLabel_AccountNumber.Name = "XrLabel_AccountNumber"
            Me.XrLabel_AccountNumber.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_AccountNumber.SizeF = New System.Drawing.SizeF(132.8333!, 15.0!)
            Me.XrLabel_AccountNumber.StyleName = "Style_Detail_LText"
            Me.XrLabel_AccountNumber.WordWrap = False
            '
            'XrPanel1
            '
            Me.XrPanel1.BackColor = System.Drawing.Color.Teal
            Me.XrPanel1.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel9, Me.XrLabel8, Me.XrLabel7, Me.XrLabel6, Me.XrLabel5, Me.XrLabel3, Me.XrLabel2, Me.XrLabel1})
            Me.XrPanel1.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 125.0!)
            Me.XrPanel1.Name = "XrPanel1"
            Me.XrPanel1.SizeF = New System.Drawing.SizeF(800.0!, 16.0!)
            Me.XrPanel1.StyleName = "Style_Heading_Pannel"
            '
            'XrLabel9
            '
            Me.XrLabel9.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel9.ForeColor = System.Drawing.Color.White
            Me.XrLabel9.LocationFloat = New DevExpress.Utils.PointFloat(190.1666!, 0.0!)
            Me.XrLabel9.Name = "XrLabel9"
            Me.XrLabel9.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel9.SizeF = New System.Drawing.SizeF(132.8333!, 15.0!)
            Me.XrLabel9.StyleName = "Style_Heading_LText"
            Me.XrLabel9.Text = "ACCOUNT"
            Me.XrLabel9.WordWrap = False
            '
            'XrLabel8
            '
            Me.XrLabel8.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel8.ForeColor = System.Drawing.Color.White
            Me.XrLabel8.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
            Me.XrLabel8.Name = "XrLabel8"
            Me.XrLabel8.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel8.SizeF = New System.Drawing.SizeF(190.1666!, 15.0!)
            Me.XrLabel8.StyleName = "Style_Heading_LText"
            Me.XrLabel8.Text = "CLIENT"
            Me.XrLabel8.WordWrap = False
            '
            'XrLabel7
            '
            Me.XrLabel7.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel7.ForeColor = System.Drawing.Color.White
            Me.XrLabel7.LocationFloat = New DevExpress.Utils.PointFloat(720.4583!, 0.0!)
            Me.XrLabel7.Name = "XrLabel7"
            Me.XrLabel7.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel7.SizeF = New System.Drawing.SizeF(79.54169!, 15.0!)
            Me.XrLabel7.StyleName = "Style_Heading_RText"
            Me.XrLabel7.Text = "CUR $"
            Me.XrLabel7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
            Me.XrLabel7.WordWrap = False
            '
            'XrLabel6
            '
            Me.XrLabel6.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel6.ForeColor = System.Drawing.Color.White
            Me.XrLabel6.LocationFloat = New DevExpress.Utils.PointFloat(436.0!, 0.0!)
            Me.XrLabel6.Name = "XrLabel6"
            Me.XrLabel6.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel6.SizeF = New System.Drawing.SizeF(73.00003!, 15.0!)
            Me.XrLabel6.StyleName = "Style_Heading_RText"
            Me.XrLabel6.Text = "START"
            Me.XrLabel6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
            Me.XrLabel6.WordWrap = False
            '
            'XrLabel5
            '
            Me.XrLabel5.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel5.ForeColor = System.Drawing.Color.White
            Me.XrLabel5.LocationFloat = New DevExpress.Utils.PointFloat(360.9999!, 0.0!)
            Me.XrLabel5.Name = "XrLabel5"
            Me.XrLabel5.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel5.SizeF = New System.Drawing.SizeF(75.0!, 15.0!)
            Me.XrLabel5.StyleName = "Style_Heading_RText"
            Me.XrLabel5.Text = "ORIG $"
            Me.XrLabel5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
            Me.XrLabel5.WordWrap = False
            '
            'XrLabel3
            '
            Me.XrLabel3.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel3.ForeColor = System.Drawing.Color.White
            Me.XrLabel3.LocationFloat = New DevExpress.Utils.PointFloat(322.9999!, 0.0!)
            Me.XrLabel3.Name = "XrLabel3"
            Me.XrLabel3.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel3.SizeF = New System.Drawing.SizeF(38.00003!, 15.0!)
            Me.XrLabel3.StyleName = "Style_Heading_RText"
            Me.XrLabel3.StylePriority.UseTextAlignment = False
            Me.XrLabel3.Text = "STS"
            Me.XrLabel3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
            Me.XrLabel3.WordWrap = False
            '
            'XrLabel2
            '
            Me.XrLabel2.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel2.ForeColor = System.Drawing.Color.White
            Me.XrLabel2.LocationFloat = New DevExpress.Utils.PointFloat(584.0!, 0.0!)
            Me.XrLabel2.Name = "XrLabel2"
            Me.XrLabel2.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel2.SizeF = New System.Drawing.SizeF(136.4583!, 15.0!)
            Me.XrLabel2.StyleName = "Style_Heading_RText"
            Me.XrLabel2.Text = "LAST PMT&DATE"
            Me.XrLabel2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
            Me.XrLabel2.WordWrap = False
            '
            'XrLabel1
            '
            Me.XrLabel1.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel1.ForeColor = System.Drawing.Color.White
            Me.XrLabel1.LocationFloat = New DevExpress.Utils.PointFloat(509.0!, 0.0!)
            Me.XrLabel1.Name = "XrLabel1"
            Me.XrLabel1.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel1.SizeF = New System.Drawing.SizeF(75.0!, 15.0!)
            Me.XrLabel1.StyleName = "Style_Heading_RText"
            Me.XrLabel1.Text = "PTD"
            Me.XrLabel1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
            Me.XrLabel1.WordWrap = False
            '
            'ReportFooter
            '
            Me.ReportFooter.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLine1, Me.XrLabel_Total_CurrentBalance, Me.XrLabel_Total_TotalPayments, Me.XrLabel_Total_OriginalBalance, Me.XrLabel4})
            Me.ReportFooter.HeightF = 44.0!
            Me.ReportFooter.Name = "ReportFooter"
            Me.ReportFooter.PrintAtBottom = True
            '
            'XrLine1
            '
            Me.XrLine1.ForeColor = System.Drawing.Color.Teal
            Me.XrLine1.LineWidth = 2
            Me.XrLine1.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 7.999992!)
            Me.XrLine1.Name = "XrLine1"
            Me.XrLine1.SizeF = New System.Drawing.SizeF(800.0!, 9.0!)
            '
            'XrLabel_Total_CurrentBalance
            '
            Me.XrLabel_Total_CurrentBalance.CanGrow = False
            Me.XrLabel_Total_CurrentBalance.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "current_balance")})
            Me.XrLabel_Total_CurrentBalance.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel_Total_CurrentBalance.LocationFloat = New DevExpress.Utils.PointFloat(592.0!, 17.0!)
            Me.XrLabel_Total_CurrentBalance.Name = "XrLabel_Total_CurrentBalance"
            Me.XrLabel_Total_CurrentBalance.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_Total_CurrentBalance.SizeF = New System.Drawing.SizeF(208.0!, 15.0!)
            Me.XrLabel_Total_CurrentBalance.StyleName = "Style_Total_Amount"
            Me.XrLabel_Total_CurrentBalance.StylePriority.UseTextAlignment = False
            XrSummary1.FormatString = "{0:c}"
            XrSummary1.Running = DevExpress.XtraReports.UI.SummaryRunning.Report
            Me.XrLabel_Total_CurrentBalance.Summary = XrSummary1
            Me.XrLabel_Total_CurrentBalance.Tag = "current_balance"
            Me.XrLabel_Total_CurrentBalance.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrLabel_Total_CurrentBalance.WordWrap = False
            '
            'XrLabel_Total_TotalPayments
            '
            Me.XrLabel_Total_TotalPayments.CanGrow = False
            Me.XrLabel_Total_TotalPayments.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "principle_payments")})
            Me.XrLabel_Total_TotalPayments.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel_Total_TotalPayments.LocationFloat = New DevExpress.Utils.PointFloat(468.0!, 17.0!)
            Me.XrLabel_Total_TotalPayments.Name = "XrLabel_Total_TotalPayments"
            Me.XrLabel_Total_TotalPayments.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_Total_TotalPayments.SizeF = New System.Drawing.SizeF(116.0!, 15.0!)
            Me.XrLabel_Total_TotalPayments.StyleName = "Style_Total_Amount"
            Me.XrLabel_Total_TotalPayments.StylePriority.UseTextAlignment = False
            XrSummary2.FormatString = "{0:c}"
            XrSummary2.Running = DevExpress.XtraReports.UI.SummaryRunning.Report
            Me.XrLabel_Total_TotalPayments.Summary = XrSummary2
            Me.XrLabel_Total_TotalPayments.Tag = "principle_payments"
            Me.XrLabel_Total_TotalPayments.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrLabel_Total_TotalPayments.WordWrap = False
            '
            'XrLabel_Total_OriginalBalance
            '
            Me.XrLabel_Total_OriginalBalance.CanGrow = False
            Me.XrLabel_Total_OriginalBalance.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "orig_balance")})
            Me.XrLabel_Total_OriginalBalance.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel_Total_OriginalBalance.LocationFloat = New DevExpress.Utils.PointFloat(233.0!, 17.0!)
            Me.XrLabel_Total_OriginalBalance.Name = "XrLabel_Total_OriginalBalance"
            Me.XrLabel_Total_OriginalBalance.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_Total_OriginalBalance.SizeF = New System.Drawing.SizeF(208.0!, 15.0!)
            Me.XrLabel_Total_OriginalBalance.StyleName = "Style_Total_Amount"
            Me.XrLabel_Total_OriginalBalance.StylePriority.UseTextAlignment = False
            XrSummary3.FormatString = "{0:c}"
            XrSummary3.Running = DevExpress.XtraReports.UI.SummaryRunning.Report
            Me.XrLabel_Total_OriginalBalance.Summary = XrSummary3
            Me.XrLabel_Total_OriginalBalance.Tag = "orig_balance"
            Me.XrLabel_Total_OriginalBalance.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrLabel_Total_OriginalBalance.WordWrap = False
            '
            'XrLabel4
            '
            Me.XrLabel4.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel4.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 17.0!)
            Me.XrLabel4.Name = "XrLabel4"
            Me.XrLabel4.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel4.SizeF = New System.Drawing.SizeF(208.0!, 15.0!)
            Me.XrLabel4.StyleName = "Style_Total_LText"
            Me.XrLabel4.StylePriority.UseTextAlignment = False
            Me.XrLabel4.Text = "Grand Total"
            Me.XrLabel4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            '
            'ParameterCreditor
            '
            Me.ParameterCreditor.Description = "Creditor ID"
            Me.ParameterCreditor.Name = "ParameterCreditor"
            Me.ParameterCreditor.Visible = False
            '
            'ClientsByCreditorListReport
            '
            Me.Bands.AddRange(New DevExpress.XtraReports.UI.Band() {Me.Detail, Me.PageHeader, Me.PageFooter, Me.TopMarginBand1, Me.BottomMarginBand1, Me.ReportFooter})
            Me.Font = New System.Drawing.Font("Times New Roman", 9.75!)
            Me.Parameters.AddRange(New DevExpress.XtraReports.Parameters.Parameter() {Me.ParameterCreditor})
            Me.RequestParameters = False
            Me.ScriptLanguage = DevExpress.XtraReports.ScriptLanguage.VisualBasic
            Me.ScriptReferencesString = resources.GetString("$this.ScriptReferencesString")
            Me.Scripts.OnBeforePrint = "ClientsByCreditorListReport_BeforePrint"
            Me.ScriptsSource = resources.GetString("$this.ScriptsSource")
            Me.StyleSheet.AddRange(New DevExpress.XtraReports.UI.XRControlStyle() {Me.XrControlStyle_Header, Me.XrControlStyle_Totals, Me.XrControlStyle_HeaderPannel, Me.XrControlStyle_GroupHeader, Me.Style_Detail_LText, Me.Style_Total_Amount, Me.Style_Heading_Amount, Me.Style_Heading_LText, Me.Style_Heading_Pannel, Me.Style_Detail_Amount, Me.Style_Heading_RText, Me.Style_Total_LText, Me.Style_Detail_RText, Me.Style_Total_RText})
            Me.Version = "11.2"
            Me.Controls.SetChildIndex(Me.ReportFooter, 0)
            Me.Controls.SetChildIndex(Me.BottomMarginBand1, 0)
            Me.Controls.SetChildIndex(Me.TopMarginBand1, 0)
            Me.Controls.SetChildIndex(Me.PageFooter, 0)
            Me.Controls.SetChildIndex(Me.PageHeader, 0)
            Me.Controls.SetChildIndex(Me.Detail, 0)
            CType(Me, System.ComponentModel.ISupportInitialize).EndInit()

        End Sub
        Friend WithEvents XrLabel_Client As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_OriginalBalance As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_CurrentBalance As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_LastPaymentDate As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_LastPaymentAmount As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_TotalPayments As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_StartDate As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_Status As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_AccountNumber As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrPanel1 As DevExpress.XtraReports.UI.XRPanel
        Friend WithEvents XrLabel1 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel2 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel3 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel5 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel6 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel7 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel8 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel9 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents ReportFooter As DevExpress.XtraReports.UI.ReportFooterBand
        Friend WithEvents XrLabel4 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_Total_OriginalBalance As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_Total_TotalPayments As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_Total_CurrentBalance As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLine1 As DevExpress.XtraReports.UI.XRLine
        Friend WithEvents Style_Detail_LText As DevExpress.XtraReports.UI.XRControlStyle
        Friend WithEvents Style_Total_Amount As DevExpress.XtraReports.UI.XRControlStyle
        Friend WithEvents Style_Heading_Amount As DevExpress.XtraReports.UI.XRControlStyle
        Friend WithEvents Style_Heading_LText As DevExpress.XtraReports.UI.XRControlStyle
        Friend WithEvents Style_Heading_Pannel As DevExpress.XtraReports.UI.XRControlStyle
        Friend WithEvents Style_Detail_Amount As DevExpress.XtraReports.UI.XRControlStyle
        Friend WithEvents Style_Heading_RText As DevExpress.XtraReports.UI.XRControlStyle
        Friend WithEvents Style_Total_LText As DevExpress.XtraReports.UI.XRControlStyle
        Friend WithEvents Style_Detail_RText As DevExpress.XtraReports.UI.XRControlStyle
        Friend WithEvents Style_Total_RText As DevExpress.XtraReports.UI.XRControlStyle
        Friend WithEvents ParameterCreditor As DevExpress.XtraReports.Parameters.Parameter
    End Class
End Namespace
