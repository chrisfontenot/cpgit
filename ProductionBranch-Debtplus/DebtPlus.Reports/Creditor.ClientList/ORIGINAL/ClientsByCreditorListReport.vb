#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}
#End Region

Option Compare Binary
Option Explicit On 
Option Strict On

Imports DevExpress.XtraReports.UI

Namespace Creditor.ClientList
    Public Class ClientsByCreditorListReport
        Implements DebtPlus.Interfaces.Creditor.ICreditor

        ''' <summary>
        ''' Create an instance of our report
        ''' </summary>
        Public Sub New()
            MyBase.New()
            InitializeComponent()

            '-- Add handler routines
            AddHandler BeforePrint, AddressOf ClientsByCreditorListReport_BeforePrint

            '-- Default the parameters
            ParameterCreditor.Value = String.Empty

            '-- Enable the select expert
            ReportFilter.IsEnabled = True
        End Sub

        ''' <summary>
        ''' Report title information
        ''' </summary>
        Public Overrides ReadOnly Property ReportTitle() As String
            Get
                Return "Clients By Creditor"
            End Get
        End Property

        ''' <summary>
        ''' Report subtitle information
        ''' </summary>
        Public Overrides ReadOnly Property ReportSubtitle() As String
            Get
                Dim CreditorName As String = String.Empty
                Dim vue As System.Data.DataView = TryCast(DataSource, System.Data.DataView)
                If vue IsNot Nothing Then
                    Dim ds As System.Data.DataSet = CType(vue.Table.DataSet, System.Data.DataSet)
                    Dim tbl As System.Data.DataTable = ds.Tables("creditors")
                    If tbl IsNot Nothing AndAlso tbl.Rows.Count > 0 Then
                        Dim row As System.Data.DataRow = tbl.Rows(0)
                        CreditorName = DebtPlus.Utils.Nulls.DStr(row("creditor_name")).Trim
                    End If
                End If

                Dim Answer As String
                Dim id As String = DebtPlus.Utils.Nulls.DStr(Parameters("ParameterCreditor").Value)
                If id <> String.Empty AndAlso CreditorName <> String.Empty Then
                    Answer = String.Format("{0} {1}", id, CreditorName)
                Else
                    Answer = id + CreditorName
                End If

                Return Answer
            End Get
        End Property

        Public Property Creditor As String Implements DebtPlus.Interfaces.Creditor.ICreditor.Creditor
            Get
                Return Parameter_Creditor
            End Get
            Set(value As String)
                Parameter_Creditor = value
            End Set
        End Property

        ''' <summary>
        ''' Parameter for the creditor ID
        ''' </summary>
        Public Property Parameter_Creditor() As String
            Get
                Dim parm As DevExpress.XtraReports.Parameters.Parameter = FindParameter("ParameterCreditor")
                If parm Is Nothing Then Return String.Empty
                Return Convert.ToString(parm.Value)
            End Get
            Set(ByVal Value As String)
                SetParameter("ParameterCreditor", GetType(String), Value, "Creditor Label", False)
            End Set
        End Property

        ''' <summary>
        ''' Do we need to request parameters?
        ''' </summary>
        Public Overrides Function NeedParameters() As Boolean
            Return MyBase.NeedParameters() OrElse Parameter_Creditor = String.Empty
        End Function

        ''' <summary>
        ''' Change the parameter value for this report
        ''' </summary>
        Public Overrides Sub SetReportParameter(ByVal Parameter As String, ByVal Value As Object)
            Select Case Parameter
                Case "Creditor"
                    Parameter_Creditor = Convert.ToString(Value)
                Case Else
                    MyBase.SetReportParameter(Parameter, Value)
            End Select
        End Sub

        ''' <summary>
        ''' Request the parameters for the report from the user
        ''' </summary>
        Public Overrides Function RequestReportParameters() As System.Windows.Forms.DialogResult
            Dim Answer As System.Windows.Forms.DialogResult = DialogResult.OK
            If NeedParameters() Then
                Using frm As New DebtPlus.Reports.Template.Forms.CreditorParametersForm()
                    Answer = frm.ShowDialog()
                    Parameter_Creditor = frm.Parameter_Creditor
                End Using
            End If
            Return Answer
        End Function

        ''' <summary>
        ''' Build the report dataset
        ''' </summary>
        Dim ds As New System.Data.DataSet("ds")

        Private Sub ClientsByCreditorListReport_BeforePrint(ByVal sender As Object, ByVal e As System.EventArgs)
            Const TableName As String = "view_clients_by_creditor"
            Dim rpt As DevExpress.XtraReports.UI.XtraReport = CType(sender, DevExpress.XtraReports.UI.XtraReport)
            Dim tbl As System.Data.DataTable = ds.Tables(TableName)

            If tbl Is Nothing Then
                Dim sqlInfo As DebtPlus.LINQ.SQLInfoClass = DebtPlus.LINQ.SQLInfoClass.getDefault()
                Try
                    Using cn As System.Data.SqlClient.SqlConnection = New System.Data.SqlClient.SqlConnection(sqlInfo.ConnectionString)
                        cn.Open()

                        Using cmd As New System.Data.SqlClient.SqlCommand()
                            cmd.Connection = cn
                            cmd.CommandText = "SELECT creditor, creditor_name FROM creditors WHERE creditor=@creditor"
                            cmd.CommandType = System.Data.CommandType.Text
                            cmd.Parameters.Add("@creditor", System.Data.SqlDbType.VarChar, 10).Value = rpt.Parameters("ParameterCreditor").Value

                            Using da As New System.Data.SqlClient.SqlDataAdapter(cmd)
                                da.Fill(ds, "creditors")
                            End Using
                        End Using

                        Using cmd As New System.Data.SqlClient.SqlCommand()
                            cmd.Connection = cn
                            cmd.CommandText = "SELECT always_disburse,[last_payment_date],[last_payment_amount],[client],[name],[status],[start_date],[account_number],[orig_balance],[principle_payments],[current_balance] FROM view_clients_by_creditor WITH (NOLOCK) WHERE (creditor=@creditor) AND ([status] in ('A','AR','EX') OR [always_disburse] <> 0) ORDER BY client"
                            cmd.CommandType = System.Data.CommandType.Text
                            cmd.Parameters.Add("@creditor", System.Data.SqlDbType.VarChar, 10).Value = rpt.Parameters("ParameterCreditor").Value
                            cmd.CommandTimeout = 0

                            Using da As New System.Data.SqlClient.SqlDataAdapter(cmd)
                                da.Fill(ds, TableName)
                                tbl = ds.Tables(TableName)
                            End Using
                        End Using
                    End Using

                Catch ex As System.Data.SqlClient.SqlException
                    Using gdr As New DebtPlus.Repository.GetDataResult()
                        gdr.HandleException(ex)
                        DebtPlus.UI.Common.ErrorHandling.HandleErrors(gdr, "Error reading information for report")
                    End Using
                End Try
            End If

            If tbl IsNot Nothing Then
                rpt.DataSource = New System.Data.DataView(tbl, CType(rpt, DebtPlus.Interfaces.Reports.IReportFilter).ReportFilter.ViewFilter, String.Empty, System.Data.DataViewRowState.CurrentRows)
            End If
        End Sub
    End Class
End Namespace
