#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}
#End Region

Option Compare Binary
Option Explicit On 
Option Strict On

Namespace Workshop.AttendanceRoster
    Public Class ParametersForm
        Inherits Template.Forms.ReportParametersForm

        Public Sub New()
            MyBase.New()
            InitializeComponent()
            AddHandler Me.Load, AddressOf RequestParametersForm_Load
            AddHandler TextEdit_Workshop.GotFocus, AddressOf TextEdit_Workshop_GotFocus
        End Sub

        Public Property Parameter_Workshop() As System.Int32
            Get
                If TextEdit_Workshop.Text = System.String.Empty Then
                    Return -1
                Else
                    Return Convert.ToInt32(TextEdit_Workshop.EditValue)
                End If
            End Get
            Set(ByVal Value As System.Int32)
                TextEdit_Workshop.EditValue = Value
            End Set
        End Property

        Public Property Parameter_IncludeEmail() As Boolean
            Get
                Return (CheckEdit_Email.CheckState <> CheckState.Unchecked)
            End Get
            Set(ByVal Value As Boolean)
                If Value Then
                    CheckEdit_Email.CheckState = CheckState.Checked
                Else
                    CheckEdit_Email.CheckState = CheckState.Unchecked
                End If
            End Set
        End Property

        Public Property Parameter_IncludeAddress() As Boolean
            Get
                Return (CheckEdit_Address.CheckState <> CheckState.Unchecked)
            End Get
            Set(ByVal Value As Boolean)
                If Value Then
                    CheckEdit_Address.CheckState = CheckState.Checked
                Else
                    CheckEdit_Address.CheckState = CheckState.Unchecked
                End If
            End Set
        End Property

#Region " Windows Form Designer generated code "

        'Form overrides dispose to clean up the component list.
        Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
            If disposing Then
                If components IsNot Nothing Then
                    components.Dispose()
                End If
            End If
            MyBase.Dispose(disposing)
        End Sub

        'Required by the Windows Form Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Windows Form Designer
        'It can be modified using the Windows Form Designer.  
        'Do not modify it using the code editor.
        Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
        Friend WithEvents Label1 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents RadioButton_Text As System.Windows.Forms.RadioButton
        Friend WithEvents RadioButton_List As System.Windows.Forms.RadioButton
        Friend WithEvents LookUpEdit_Workshop As DevExpress.XtraEditors.LookUpEdit
        Friend WithEvents CheckEdit_Address As DevExpress.XtraEditors.CheckEdit
        Friend WithEvents CheckEdit_Email As DevExpress.XtraEditors.CheckEdit
        Friend WithEvents TextEdit_Workshop As DevExpress.XtraEditors.TextEdit
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
            Me.GroupBox1 = New System.Windows.Forms.GroupBox
            Me.Label1 = New DevExpress.XtraEditors.LabelControl
            Me.RadioButton_Text = New System.Windows.Forms.RadioButton
            Me.RadioButton_List = New System.Windows.Forms.RadioButton
            Me.TextEdit_Workshop = New DevExpress.XtraEditors.TextEdit
            Me.LookUpEdit_Workshop = New DevExpress.XtraEditors.LookUpEdit
            Me.CheckEdit_Address = New DevExpress.XtraEditors.CheckEdit
            Me.CheckEdit_Email = New DevExpress.XtraEditors.CheckEdit

            Me.GroupBox1.SuspendLayout()
            CType(Me.TextEdit_Workshop.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LookUpEdit_Workshop.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.CheckEdit_Address.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.CheckEdit_Email.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.SuspendLayout()
            '
            'ButtonCancel
            '
            Me.ButtonCancel.Name = "ButtonCancel"
            '
            'ButtonOK
            '
            Me.ButtonOK.Name = "ButtonOK"
            '
            'GroupBox1
            '
            Me.GroupBox1.Controls.Add(Me.Label1)
            Me.GroupBox1.Controls.Add(Me.RadioButton_Text)
            Me.GroupBox1.Controls.Add(Me.RadioButton_List)
            Me.GroupBox1.Controls.Add(Me.TextEdit_Workshop)
            Me.GroupBox1.Controls.Add(Me.LookUpEdit_Workshop)
            Me.GroupBox1.Location = New System.Drawing.Point(8, 16)
            Me.GroupBox1.Name = "GroupBox1"
            Me.GroupBox1.Size = New System.Drawing.Size(232, 112)
            Me.GroupBox1.TabIndex = 0
            Me.GroupBox1.TabStop = False
            Me.GroupBox1.Text = "Workshop"
            '
            'Label1
            '
            Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.Label1.Location = New System.Drawing.Point(32, 54)
            Me.Label1.Name = "Label1"
            Me.Label1.Size = New System.Drawing.Size(152, 17)
            Me.Label1.TabIndex = 2
            Me.Label1.Text = "Or enter a specific ID"
            '
            'RadioButton_Text
            '
            Me.RadioButton_Text.AllowDrop = True
            Me.RadioButton_Text.Location = New System.Drawing.Point(8, 78)
            Me.RadioButton_Text.Name = "RadioButton_Text"
            Me.RadioButton_Text.Size = New System.Drawing.Size(16, 17)
            Me.RadioButton_Text.TabIndex = 3
            Me.RadioButton_Text.Tag = "text"
            Me.RadioButton_Text.TextAlign = System.Drawing.ContentAlignment.MiddleRight
            '
            'RadioButton_List
            '
            Me.RadioButton_List.Checked = True
            Me.RadioButton_List.Location = New System.Drawing.Point(8, 28)
            Me.RadioButton_List.Name = "RadioButton_List"
            Me.RadioButton_List.Size = New System.Drawing.Size(16, 17)
            Me.RadioButton_List.TabIndex = 0
            Me.RadioButton_List.TabStop = True
            Me.RadioButton_List.Tag = "list"
            '
            'TextEdit_Workshop
            '
            Me.TextEdit_Workshop.EditValue = ""
            Me.TextEdit_Workshop.Location = New System.Drawing.Point(32, 78)
            Me.TextEdit_Workshop.Name = "TextEdit_Workshop"
            '
            'TextEdit_Workshop.Properties
            '
            Me.TextEdit_Workshop.Properties.Appearance.Options.UseTextOptions = True
            Me.TextEdit_Workshop.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.TextEdit_Workshop.Properties.AppearanceDisabled.Options.UseTextOptions = True
            Me.TextEdit_Workshop.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.TextEdit_Workshop.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
            Me.TextEdit_Workshop.Properties.DisplayFormat.FormatString = "f0"
            Me.TextEdit_Workshop.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.TextEdit_Workshop.Properties.EditFormat.FormatString = "f0"
            Me.TextEdit_Workshop.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.TextEdit_Workshop.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
            Me.TextEdit_Workshop.Properties.ValidateOnEnterKey = True
            Me.TextEdit_Workshop.Size = New System.Drawing.Size(64, 20)
            Me.TextEdit_Workshop.TabIndex = 4
            Me.TextEdit_Workshop.ToolTip = "Enter a specific workshop if it is not included in the above list"
            Me.TextEdit_Workshop.ToolTipController = Me.ToolTipController1
            '
            'LookUpEdit_Workshop
            '
            Me.LookUpEdit_Workshop.Location = New System.Drawing.Point(32, 26)
            Me.LookUpEdit_Workshop.Name = "LookUpEdit_Workshop"
            '
            'LookUpEdit_Workshop.Properties
            '
            Me.LookUpEdit_Workshop.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.LookUpEdit_Workshop.Properties.NullText = ""
            Me.LookUpEdit_Workshop.Properties.PopupWidth = 500
            Me.LookUpEdit_Workshop.Size = New System.Drawing.Size(184, 20)
            Me.LookUpEdit_Workshop.TabIndex = 1
            Me.LookUpEdit_Workshop.ToolTip = "Choose a workshop from the list if desired"
            Me.LookUpEdit_Workshop.ToolTipController = Me.ToolTipController1
            '
            'CheckEdit_Address
            '
            Me.CheckEdit_Address.Location = New System.Drawing.Point(8, 136)
            Me.CheckEdit_Address.Name = "CheckEdit_Address"
            '
            'CheckEdit_Address.Properties
            '
            Me.CheckEdit_Address.Properties.Caption = "Include Client Address in the report"
            Me.CheckEdit_Address.Size = New System.Drawing.Size(320, 18)
            Me.CheckEdit_Address.TabIndex = 1
            Me.CheckEdit_Address.ToolTip = "Do you wish to include the client address in the report listing?"
            Me.CheckEdit_Address.ToolTipController = Me.ToolTipController1
            Me.CheckEdit_Address.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Question
            '
            'CheckEdit_Email
            '
            Me.CheckEdit_Email.Location = New System.Drawing.Point(8, 154)
            Me.CheckEdit_Email.Name = "CheckEdit_Email"
            '
            'CheckEdit_Email.Properties
            '
            Me.CheckEdit_Email.Properties.Caption = "Include Client Email in the report"
            Me.CheckEdit_Email.Size = New System.Drawing.Size(320, 18)
            Me.CheckEdit_Email.TabIndex = 2
            Me.CheckEdit_Email.ToolTip = "Do you wish to include the client Email address in the report?"
            Me.CheckEdit_Email.ToolTipController = Me.ToolTipController1
            Me.CheckEdit_Email.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Question
            '
            'ParametersForm
            '
            Me.AutoScaleBaseSize = New System.Drawing.Size(5, 14)
            Me.ClientSize = New System.Drawing.Size(336, 176)
            Me.Controls.Add(Me.CheckEdit_Email)
            Me.Controls.Add(Me.CheckEdit_Address)
            Me.Controls.Add(Me.GroupBox1)
            Me.Name = "ParametersForm"
            Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
            Me.Controls.SetChildIndex(Me.ButtonOK, 0)
            Me.Controls.SetChildIndex(Me.ButtonCancel, 0)
            Me.Controls.SetChildIndex(Me.GroupBox1, 0)
            Me.Controls.SetChildIndex(Me.CheckEdit_Address, 0)
            Me.Controls.SetChildIndex(Me.CheckEdit_Email, 0)

            Me.GroupBox1.ResumeLayout(False)
            CType(Me.TextEdit_Workshop.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LookUpEdit_Workshop.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.CheckEdit_Address.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.CheckEdit_Email.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            Me.ResumeLayout(False)

        End Sub

#End Region

        Private Sub RequestParametersForm_Load(ByVal sender As System.Object, ByVal e As System.EventArgs)

            ' If a batch is specified then don't bother to allow the user to change it.
            ' It comes from the application and it's value is correct.
            If Parameter_Workshop > 0 Then
                TextEdit_Workshop.EditValue = Parameter_Workshop

                RadioButton_List.Checked = False
                RadioButton_Text.Checked = True

                ' Disable the controls for selecting the batch number
                GroupBox1.Enabled = False

            Else

                ' Load the Workshop registers
                load_Workshops()

                ' Add the handling routines
                AddHandler LookUpEdit_Workshop.EditValueChanged, AddressOf LookUpEdit_Disbursement_EditValueChanged

                AddHandler TextEdit_Workshop.EditValueChanged, AddressOf TextEdit_workshop_EditValueChanged
                AddHandler TextEdit_Workshop.KeyPress, AddressOf TextEdit_workshop_KeyPress

                AddHandler RadioButton_List.CheckedChanged, AddressOf RadioButton_List_CheckedChanged
                AddHandler RadioButton_Text.CheckedChanged, AddressOf RadioButton_List_CheckedChanged
            End If

            ' Enable or disable the OK button
            ButtonOK.Enabled = Not HasErrors()
        End Sub

        Private Sub load_Workshops()

            ' Retrieve the database connection logic
            Dim SelectStatement As String = "SELECT w.Workshop AS 'ID', wt.description as 'Type', wl.name as 'Location', w.start_time AS 'Date' FROM Workshops w WITH (NOLOCK) LEFT OUTER JOIN workshop_types wt WITH (NOLOCK) ON w.workshop_type = wt.workshop_type LEFT OUTER JOIN workshop_locations wl WITH (NOLOCK) ON w.workshop_location = wl.workshop_location WHERE w.start_time > dateadd(d, -300, getdate()) ORDER BY w.start_time"
            Dim cmd As SqlClient.SqlCommand = New System.Data.SqlClient.SqlCommand()
            With cmd
                .Connection = New System.Data.SqlClient.SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
                .CommandText = SelectStatement
                .CommandTimeout = 0
            End With

            ' Retrieve the disbursement table from the system
            Dim da As New SqlClient.SqlDataAdapter(cmd)
            Dim ds As New System.Data.DataSet

            Try
                da.Fill(ds, "Workshop_ids")
                Dim tbl As System.Data.DataTable = ds.Tables(0)

                ' Define the values for the lookup control
                With LookUpEdit_Workshop
                    With .Properties
                        .DataSource = New System.Data.DataView(tbl, "", "Date", DataViewRowState.CurrentRows)
                        .ShowFooter = False
                        .PopulateColumns()

                        ' The date and time are the default values
                        With .Columns("Date")
                            .FormatString = "MM/dd/yyyy hh:mm tt"
                            .Width = 90
                        End With

                        ' Populate the ID column with the disbursement register, showing it as a number
                        With .Columns("ID")
                            .FormatString = "f0"
                            .Alignment = DevExpress.Utils.HorzAlignment.Far
                            .Width = 40
                        End With

                        ' The display is the note, the value is the ID
                        .DisplayMember = "Type"
                        .ValueMember = "ID"
                    End With

                    ' Set the value to the first item
                    If tbl.Rows.Count > 0 Then
                        Dim row As System.Data.DataRow = tbl.Rows(0)
                        .EditValue = row("ID")
                    End If

                    ' Populate the input value with the ID from the listbox
                    If .EditValue IsNot System.DBNull.Value Then
                        TextEdit_Workshop.Text = Convert.ToInt32(.EditValue).ToString()
                    End If
                End With

            Catch ex As SqlClient.SqlException
                DebtPlus.Data.Forms.MessageBox.Show(ex.ToString(), "Error retrieving batch list", MessageBoxButtons.OK, MessageBoxIcon.Error)

            End Try
        End Sub

        Private Sub TextEdit_Workshop_GotFocus(ByVal sender As System.Object, ByVal e As System.EventArgs)
            CType(sender, DevExpress.XtraEditors.TextEdit).SelectAll()
        End Sub

        Private Sub TextEdit_workshop_EditValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
            Parameter_Workshop = Convert.ToInt32(TextEdit_Workshop.EditValue)
            ButtonOK.Enabled = Not HasErrors()
        End Sub

        Private Sub TextEdit_workshop_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs)
            If Not System.Char.IsControl(e.KeyChar) Then
                If Not System.Char.IsDigit(e.KeyChar) Then e.Handled = True
            End If
        End Sub

        Private Sub LookUpEdit_Disbursement_EditValueChanged(ByVal sender As Object, ByVal e As System.EventArgs)
            TextEdit_Workshop.EditValue = LookUpEdit_Workshop.EditValue
            Parameter_Workshop = Convert.ToInt32(TextEdit_Workshop.EditValue)
            ButtonOK.Enabled = Not HasErrors()
        End Sub

        Protected Overrides Function HasErrors() As Boolean
            Try
                Dim NewValue As System.Int32 = CType(TextEdit_Workshop.EditValue, Int32)
                If NewValue > 0 Then Return False
            Catch
            End Try

            ' The workshop is not valid. Disable the button.
            Return True
        End Function

        Private Sub RadioButton_List_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)

            ' Enable or disable the controls based upon the radio buttons
            TextEdit_Workshop.Enabled = RadioButton_Text.Checked
            LookUpEdit_Workshop.Enabled = RadioButton_List.Checked

            ' Syncrhonize the edit value with the selected item
            TextEdit_Workshop.EditValue = LookUpEdit_Workshop.EditValue

            ' Go to the next control
            CType(sender, System.Windows.Forms.RadioButton).SelectNextControl(CType(sender, System.Windows.Forms.Control), True, True, False, True)
        End Sub
    End Class
End Namespace