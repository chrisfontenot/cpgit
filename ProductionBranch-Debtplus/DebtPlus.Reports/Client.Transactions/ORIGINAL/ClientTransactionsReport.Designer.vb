Namespace Client.Transactions
    <Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
    Partial Class ClientTransactionsReport

        'XtraReport overrides dispose to clean up the component list.
        <System.Diagnostics.DebuggerNonUserCode()> _
        Protected Overrides Sub Dispose(ByVal disposing As Boolean)
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
            MyBase.Dispose(disposing)
        End Sub

        'Required by the Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Designer
        'It can be modified using the Designer.  
        'Do not modify it using the code editor.
        <System.Diagnostics.DebuggerStepThrough()> _
        Private Sub InitializeComponent()
            Dim XrSummary1 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary()
            Dim XrSummary2 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary()
            Dim PostNetGenerator1 As DevExpress.XtraPrinting.BarCode.PostNetGenerator = New DevExpress.XtraPrinting.BarCode.PostNetGenerator()
            Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(ClientTransactionsReport))
            Me.XrPanel1 = New DevExpress.XtraReports.UI.XRPanel()
            Me.XrLabel6 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel5 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel4 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel3 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel2 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel1 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_tran_type = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_date_created = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_credit = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_debit = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_check_number = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_message = New DevExpress.XtraReports.UI.XRLabel()
            Me.ReportFooter = New DevExpress.XtraReports.UI.ReportFooterBand()
            Me.XrLabel8 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLine1 = New DevExpress.XtraReports.UI.XRLine()
            Me.XrLabel_Total_Credit = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_Total_Debit = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_ClientID = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrPanel_ClientAddress = New DevExpress.XtraReports.UI.XRPanel()
            Me.XrBarCode_Zipcode = New DevExpress.XtraReports.UI.XRBarCode()
            Me.XrLabel_Address = New DevExpress.XtraReports.UI.XRLabel()
            Me.ParameterClient = New DevExpress.XtraReports.Parameters.Parameter()
            Me.CalculatedField_credit_amt = New DevExpress.XtraReports.UI.CalculatedField()
            Me.CalculatedField_debit_amt = New DevExpress.XtraReports.UI.CalculatedField()
            CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
            '
            'PageHeader
            '
            Me.PageHeader.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrPanel1, Me.XrPanel_ClientAddress})
            Me.PageHeader.HeightF = 347.0!
            Me.PageHeader.Controls.SetChildIndex(Me.XrPanel_ClientAddress, 0)
            Me.PageHeader.Controls.SetChildIndex(Me.XrPanel1, 0)
            Me.PageHeader.Controls.SetChildIndex(Me.XrPanel_AgencyAddress, 0)
            Me.PageHeader.Controls.SetChildIndex(Me.XrLabel_Title, 0)
            Me.PageHeader.Controls.SetChildIndex(Me.XrLine_Title, 0)
            Me.PageHeader.Controls.SetChildIndex(Me.XrLabel_Subtitle, 0)
            Me.PageHeader.Controls.SetChildIndex(Me.XrPageInfo1, 0)
            '
            'PageFooter
            '
            Me.PageFooter.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel_ClientID})
            Me.PageFooter.Controls.SetChildIndex(Me.XrLabel_ClientID, 0)
            Me.PageFooter.Controls.SetChildIndex(Me.XrPageInfo_PageNumber, 0)
            '
            'XrLabel_Title
            '
            Me.XrLabel_Title.StylePriority.UseFont = False
            '
            'XrPageInfo_PageNumber
            '
            Me.XrPageInfo_PageNumber.StylePriority.UseTextAlignment = False
            '
            'XRLabel_Agency_Name
            '
            Me.XRLabel_Agency_Name.StylePriority.UseTextAlignment = False
            '
            'XrLabel_Agency_Address3
            '
            Me.XrLabel_Agency_Address3.StylePriority.UseFont = False
            Me.XrLabel_Agency_Address3.StylePriority.UseTextAlignment = False
            '
            'XrLabel_Agency_Address1
            '
            Me.XrLabel_Agency_Address1.StylePriority.UseFont = False
            Me.XrLabel_Agency_Address1.StylePriority.UseTextAlignment = False
            '
            'XrLabel_Agency_Phone
            '
            Me.XrLabel_Agency_Phone.StylePriority.UseFont = False
            Me.XrLabel_Agency_Phone.StylePriority.UseTextAlignment = False
            '
            'XrLabel_Agency_Address2
            '
            Me.XrLabel_Agency_Address2.StylePriority.UseFont = False
            Me.XrLabel_Agency_Address2.StylePriority.UseTextAlignment = False
            '
            'Detail
            '
            Me.Detail.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel_message, Me.XrLabel_check_number, Me.XrLabel_debit, Me.XrLabel_credit, Me.XrLabel_date_created, Me.XrLabel_tran_type})
            Me.Detail.HeightF = 15.0!
            '
            'XrPanel1
            '
            Me.XrPanel1.BackColor = System.Drawing.Color.Teal
            Me.XrPanel1.BorderColor = System.Drawing.Color.Teal
            Me.XrPanel1.CanGrow = False
            Me.XrPanel1.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel6, Me.XrLabel5, Me.XrLabel4, Me.XrLabel3, Me.XrLabel2, Me.XrLabel1})
            Me.XrPanel1.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 325.0!)
            Me.XrPanel1.Name = "XrPanel1"
            Me.XrPanel1.SizeF = New System.Drawing.SizeF(750.0!, 17.0!)
            '
            'XrLabel6
            '
            Me.XrLabel6.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel6.ForeColor = System.Drawing.Color.White
            Me.XrLabel6.LocationFloat = New DevExpress.Utils.PointFloat(541.0!, 1.0!)
            Me.XrLabel6.Name = "XrLabel6"
            Me.XrLabel6.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel6.SizeF = New System.Drawing.SizeF(198.9999!, 15.0!)
            Me.XrLabel6.StylePriority.UseTextAlignment = False
            Me.XrLabel6.Text = "MESSAGE"
            Me.XrLabel6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            '
            'XrLabel5
            '
            Me.XrLabel5.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel5.ForeColor = System.Drawing.Color.White
            Me.XrLabel5.LocationFloat = New DevExpress.Utils.PointFloat(442.0!, 1.0!)
            Me.XrLabel5.Name = "XrLabel5"
            Me.XrLabel5.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel5.SizeF = New System.Drawing.SizeF(91.0!, 15.0!)
            Me.XrLabel5.StylePriority.UseTextAlignment = False
            Me.XrLabel5.Text = "CHK #"
            Me.XrLabel5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            '
            'XrLabel4
            '
            Me.XrLabel4.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel4.ForeColor = System.Drawing.Color.White
            Me.XrLabel4.LocationFloat = New DevExpress.Utils.PointFloat(383.0!, 1.0!)
            Me.XrLabel4.Name = "XrLabel4"
            Me.XrLabel4.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel4.SizeF = New System.Drawing.SizeF(58.0!, 15.0!)
            Me.XrLabel4.Text = "DEBIT"
            Me.XrLabel4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel3
            '
            Me.XrLabel3.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel3.ForeColor = System.Drawing.Color.White
            Me.XrLabel3.LocationFloat = New DevExpress.Utils.PointFloat(252.0!, 1.0!)
            Me.XrLabel3.Name = "XrLabel3"
            Me.XrLabel3.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel3.SizeF = New System.Drawing.SizeF(72.0!, 15.0!)
            Me.XrLabel3.Text = "CREDIT"
            Me.XrLabel3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel2
            '
            Me.XrLabel2.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel2.ForeColor = System.Drawing.Color.White
            Me.XrLabel2.LocationFloat = New DevExpress.Utils.PointFloat(91.0!, 1.0!)
            Me.XrLabel2.Name = "XrLabel2"
            Me.XrLabel2.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel2.SizeF = New System.Drawing.SizeF(108.0!, 15.0!)
            Me.XrLabel2.Text = "TRANSACTION"
            Me.XrLabel2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            '
            'XrLabel1
            '
            Me.XrLabel1.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel1.ForeColor = System.Drawing.Color.White
            Me.XrLabel1.LocationFloat = New DevExpress.Utils.PointFloat(26.0!, 1.0!)
            Me.XrLabel1.Name = "XrLabel1"
            Me.XrLabel1.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel1.SizeF = New System.Drawing.SizeF(57.0!, 15.0!)
            Me.XrLabel1.StylePriority.UseTextAlignment = False
            Me.XrLabel1.Text = "DATE"
            Me.XrLabel1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel_tran_type
            '
            Me.XrLabel_tran_type.CanGrow = False
            Me.XrLabel_tran_type.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "tran_type"), New DevExpress.XtraReports.UI.XRBinding("Tag", Nothing, "item_key")})
            Me.XrLabel_tran_type.Font = New System.Drawing.Font("Times New Roman", 9.75!)
            Me.XrLabel_tran_type.ForeColor = System.Drawing.SystemColors.WindowText
            Me.XrLabel_tran_type.LocationFloat = New DevExpress.Utils.PointFloat(91.0!, 0.0!)
            Me.XrLabel_tran_type.Name = "XrLabel_tran_type"
            Me.XrLabel_tran_type.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_tran_type.Scripts.OnPreviewClick = "Field_Preview_Click"
            Me.XrLabel_tran_type.SizeF = New System.Drawing.SizeF(108.0!, 15.0!)
            Me.XrLabel_tran_type.StylePriority.UseFont = False
            Me.XrLabel_tran_type.StylePriority.UseForeColor = False
            Me.XrLabel_tran_type.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            '
            'XrLabel_date_created
            '
            Me.XrLabel_date_created.CanGrow = False
            Me.XrLabel_date_created.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "item_date", "{0:d}"), New DevExpress.XtraReports.UI.XRBinding("Tag", Nothing, "item_key")})
            Me.XrLabel_date_created.Font = New System.Drawing.Font("Times New Roman", 9.75!)
            Me.XrLabel_date_created.ForeColor = System.Drawing.SystemColors.WindowText
            Me.XrLabel_date_created.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
            Me.XrLabel_date_created.Name = "XrLabel_date_created"
            Me.XrLabel_date_created.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_date_created.Scripts.OnPreviewClick = "Field_Preview_Click"
            Me.XrLabel_date_created.SizeF = New System.Drawing.SizeF(84.0!, 15.0!)
            Me.XrLabel_date_created.StylePriority.UseFont = False
            Me.XrLabel_date_created.StylePriority.UseForeColor = False
            Me.XrLabel_date_created.StylePriority.UseTextAlignment = False
            Me.XrLabel_date_created.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel_credit
            '
            Me.XrLabel_credit.CanGrow = False
            Me.XrLabel_credit.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "credit_amt", "{0:c}"), New DevExpress.XtraReports.UI.XRBinding("Tag", Nothing, "item_key")})
            Me.XrLabel_credit.Font = New System.Drawing.Font("Times New Roman", 9.75!)
            Me.XrLabel_credit.ForeColor = System.Drawing.SystemColors.WindowText
            Me.XrLabel_credit.LocationFloat = New DevExpress.Utils.PointFloat(216.0!, 0.0!)
            Me.XrLabel_credit.Name = "XrLabel_credit"
            Me.XrLabel_credit.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_credit.Scripts.OnPreviewClick = "Field_Preview_Click"
            Me.XrLabel_credit.SizeF = New System.Drawing.SizeF(108.0!, 15.0!)
            Me.XrLabel_credit.StylePriority.UseFont = False
            Me.XrLabel_credit.StylePriority.UseForeColor = False
            Me.XrLabel_credit.Text = "$0.00"
            Me.XrLabel_credit.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel_debit
            '
            Me.XrLabel_debit.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "debit_amt", "{0:c}"), New DevExpress.XtraReports.UI.XRBinding("Tag", Nothing, "item_key")})
            Me.XrLabel_debit.Font = New System.Drawing.Font("Times New Roman", 9.75!)
            Me.XrLabel_debit.ForeColor = System.Drawing.SystemColors.WindowText
            Me.XrLabel_debit.LocationFloat = New DevExpress.Utils.PointFloat(333.0!, 0.0!)
            Me.XrLabel_debit.Name = "XrLabel_debit"
            Me.XrLabel_debit.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_debit.Scripts.OnPreviewClick = "Field_Preview_Click"
            Me.XrLabel_debit.SizeF = New System.Drawing.SizeF(108.0!, 15.0!)
            Me.XrLabel_debit.StylePriority.UseFont = False
            Me.XrLabel_debit.StylePriority.UseForeColor = False
            Me.XrLabel_debit.Text = "$0.00"
            Me.XrLabel_debit.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel_check_number
            '
            Me.XrLabel_check_number.CanGrow = False
            Me.XrLabel_check_number.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "check_number"), New DevExpress.XtraReports.UI.XRBinding("Tag", Nothing, "item_key")})
            Me.XrLabel_check_number.Font = New System.Drawing.Font("Times New Roman", 9.75!)
            Me.XrLabel_check_number.ForeColor = System.Drawing.SystemColors.WindowText
            Me.XrLabel_check_number.LocationFloat = New DevExpress.Utils.PointFloat(442.0!, 0.0!)
            Me.XrLabel_check_number.Name = "XrLabel_check_number"
            Me.XrLabel_check_number.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_check_number.SizeF = New System.Drawing.SizeF(91.0!, 14.99999!)
            Me.XrLabel_check_number.StylePriority.UseFont = False
            Me.XrLabel_check_number.StylePriority.UseForeColor = False
            Me.XrLabel_check_number.StylePriority.UseTextAlignment = False
            Me.XrLabel_check_number.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            '
            'XrLabel_message
            '
            Me.XrLabel_message.CanGrow = False
            Me.XrLabel_message.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "message"), New DevExpress.XtraReports.UI.XRBinding("Tag", Nothing, "item_key")})
            Me.XrLabel_message.Font = New System.Drawing.Font("Times New Roman", 9.75!)
            Me.XrLabel_message.ForeColor = System.Drawing.SystemColors.WindowText
            Me.XrLabel_message.LocationFloat = New DevExpress.Utils.PointFloat(541.0!, 0.0!)
            Me.XrLabel_message.Name = "XrLabel_message"
            Me.XrLabel_message.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_message.Scripts.OnPreviewClick = "Field_Preview_Click"
            Me.XrLabel_message.SizeF = New System.Drawing.SizeF(209.0!, 15.0!)
            Me.XrLabel_message.StylePriority.UseFont = False
            Me.XrLabel_message.StylePriority.UseForeColor = False
            Me.XrLabel_message.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            '
            'ReportFooter
            '
            Me.ReportFooter.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel8, Me.XrLine1, Me.XrLabel_Total_Credit, Me.XrLabel_Total_Debit})
            Me.ReportFooter.HeightF = 50.0!
            Me.ReportFooter.Name = "ReportFooter"
            '
            'XrLabel8
            '
            Me.XrLabel8.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel8.ForeColor = System.Drawing.SystemColors.WindowText
            Me.XrLabel8.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 25.0!)
            Me.XrLabel8.Name = "XrLabel8"
            Me.XrLabel8.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel8.SizeF = New System.Drawing.SizeF(108.0!, 15.0!)
            Me.XrLabel8.StylePriority.UseForeColor = False
            Me.XrLabel8.Text = "TOTALS"
            Me.XrLabel8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            '
            'XrLine1
            '
            Me.XrLine1.ForeColor = System.Drawing.Color.Teal
            Me.XrLine1.LineWidth = 2
            Me.XrLine1.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 8.0!)
            Me.XrLine1.Name = "XrLine1"
            Me.XrLine1.SizeF = New System.Drawing.SizeF(442.0!, 9.0!)
            Me.XrLine1.StylePriority.UseForeColor = False
            '
            'XrLabel_Total_Credit
            '
            Me.XrLabel_Total_Credit.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "CalculatedField_credit_amt")})
            Me.XrLabel_Total_Credit.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel_Total_Credit.ForeColor = System.Drawing.SystemColors.WindowText
            Me.XrLabel_Total_Credit.LocationFloat = New DevExpress.Utils.PointFloat(216.0!, 25.0!)
            Me.XrLabel_Total_Credit.Name = "XrLabel_Total_Credit"
            Me.XrLabel_Total_Credit.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_Total_Credit.SizeF = New System.Drawing.SizeF(108.0!, 15.0!)
            Me.XrLabel_Total_Credit.StylePriority.UseForeColor = False
            XrSummary1.FormatString = "{0:c}"
            XrSummary1.Running = DevExpress.XtraReports.UI.SummaryRunning.Report
            Me.XrLabel_Total_Credit.Summary = XrSummary1
            Me.XrLabel_Total_Credit.Text = "$0.00"
            Me.XrLabel_Total_Credit.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel_Total_Debit
            '
            Me.XrLabel_Total_Debit.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "CalculatedField_debit_amt")})
            Me.XrLabel_Total_Debit.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel_Total_Debit.ForeColor = System.Drawing.SystemColors.WindowText
            Me.XrLabel_Total_Debit.LocationFloat = New DevExpress.Utils.PointFloat(333.0!, 25.0!)
            Me.XrLabel_Total_Debit.Name = "XrLabel_Total_Debit"
            Me.XrLabel_Total_Debit.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_Total_Debit.SizeF = New System.Drawing.SizeF(108.0!, 15.0!)
            Me.XrLabel_Total_Debit.StylePriority.UseForeColor = False
            XrSummary2.FormatString = "{0:c}"
            XrSummary2.Running = DevExpress.XtraReports.UI.SummaryRunning.Report
            Me.XrLabel_Total_Debit.Summary = XrSummary2
            Me.XrLabel_Total_Debit.Text = "$0.00"
            Me.XrLabel_Total_Debit.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel_ClientID
            '
            Me.XrLabel_ClientID.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel_ClientID.ForeColor = System.Drawing.Color.Teal
            Me.XrLabel_ClientID.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 7.999992!)
            Me.XrLabel_ClientID.Name = "XrLabel_ClientID"
            Me.XrLabel_ClientID.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_ClientID.SizeF = New System.Drawing.SizeF(442.0!, 15.0!)
            Me.XrLabel_ClientID.StylePriority.UseFont = False
            Me.XrLabel_ClientID.StylePriority.UseForeColor = False
            Me.XrLabel_ClientID.Text = "[Parameters.ParameterClient!0000000]"
            Me.XrLabel_ClientID.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            '
            'XrPanel_ClientAddress
            '
            Me.XrPanel_ClientAddress.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrBarCode_Zipcode, Me.XrLabel_Address})
            Me.XrPanel_ClientAddress.LocationFloat = New DevExpress.Utils.PointFloat(50.0!, 208.0!)
            Me.XrPanel_ClientAddress.Name = "XrPanel_ClientAddress"
            Me.XrPanel_ClientAddress.SizeF = New System.Drawing.SizeF(300.0!, 100.0!)
            '
            'XrBarCode_Zipcode
            '
            Me.XrBarCode_Zipcode.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
            Me.XrBarCode_Zipcode.Name = "XrBarCode_Zipcode"
            Me.XrBarCode_Zipcode.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrBarCode_Zipcode.Scripts.OnBeforePrint = "XrBarCode_Zipcode_BeforePrint"
            Me.XrBarCode_Zipcode.ShowText = False
            Me.XrBarCode_Zipcode.SizeF = New System.Drawing.SizeF(300.0!, 17.0!)
            Me.XrBarCode_Zipcode.Symbology = PostNetGenerator1
            '
            'XrLabel_Address
            '
            Me.XrLabel_Address.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 17.0!)
            Me.XrLabel_Address.Multiline = True
            Me.XrLabel_Address.Name = "XrLabel_Address"
            Me.XrLabel_Address.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_Address.Scripts.OnBeforePrint = "XrLabel_Address_BeforePrint"
            Me.XrLabel_Address.SizeF = New System.Drawing.SizeF(300.0!, 83.0!)
            Me.XrLabel_Address.Text = "Client Address Goes Here"
            '
            'ParameterClient
            '
            Me.ParameterClient.Name = "ParameterClient"
            Me.ParameterClient.Type = GetType(Integer)
            Me.ParameterClient.Value = -1
            Me.ParameterClient.Visible = False
            '
            'CalculatedField_credit_amt
            '
            Me.CalculatedField_credit_amt.Expression = "Iif([tran_type]='BB' Or [tran_type]='LD', 0, [credit_amt])"
            Me.CalculatedField_credit_amt.FieldType = DevExpress.XtraReports.UI.FieldType.[Decimal]
            Me.CalculatedField_credit_amt.Name = "CalculatedField_credit_amt"
            '
            'CalculatedField_debit_amt
            '
            Me.CalculatedField_debit_amt.Expression = "Iif([tran_type]='BB' Or [tran_type]='LD', 0, [debit_amt])"
            Me.CalculatedField_debit_amt.FieldType = DevExpress.XtraReports.UI.FieldType.[Decimal]
            Me.CalculatedField_debit_amt.Name = "CalculatedField_debit_amt"
            '
            'ClientTransactionsReport
            '
            Me.Bands.AddRange(New DevExpress.XtraReports.UI.Band() {Me.Detail, Me.PageHeader, Me.PageFooter, Me.ReportFooter, Me.TopMarginBand1, Me.BottomMarginBand1})
            Me.CalculatedFields.AddRange(New DevExpress.XtraReports.UI.CalculatedField() {Me.CalculatedField_credit_amt, Me.CalculatedField_debit_amt})
            Me.Parameters.AddRange(New DevExpress.XtraReports.Parameters.Parameter() {Me.ParameterClient})
            Me.ScriptLanguage = DevExpress.XtraReports.ScriptLanguage.VisualBasic
            Me.ScriptReferencesString = resources.GetString("$this.ScriptReferencesString")
            Me.Scripts.OnBeforePrint = "ClientTransactionsReport_BeforePrint"
            Me.ScriptsSource = resources.GetString("$this.ScriptsSource")
            Me.StyleSheet.AddRange(New DevExpress.XtraReports.UI.XRControlStyle() {Me.XrControlStyle_Header, Me.XrControlStyle_Totals, Me.XrControlStyle_HeaderPannel, Me.XrControlStyle_GroupHeader})
            Me.Version = "11.2"
            Me.Controls.SetChildIndex(Me.BottomMarginBand1, 0)
            Me.Controls.SetChildIndex(Me.TopMarginBand1, 0)
            Me.Controls.SetChildIndex(Me.ReportFooter, 0)
            Me.Controls.SetChildIndex(Me.PageFooter, 0)
            Me.Controls.SetChildIndex(Me.PageHeader, 0)
            Me.Controls.SetChildIndex(Me.Detail, 0)
            CType(Me, System.ComponentModel.ISupportInitialize).EndInit()

        End Sub
        Friend WithEvents XrPanel1 As DevExpress.XtraReports.UI.XRPanel
        Friend WithEvents XrLabel5 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel4 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel3 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel2 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel1 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_check_number As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_debit As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_credit As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_date_created As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_tran_type As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_message As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents ReportFooter As DevExpress.XtraReports.UI.ReportFooterBand
        Friend WithEvents XrLine1 As DevExpress.XtraReports.UI.XRLine
        Friend WithEvents XrLabel_Total_Credit As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_Total_Debit As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel8 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_ClientID As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrPanel_ClientAddress As DevExpress.XtraReports.UI.XRPanel
        Friend WithEvents XrBarCode_Zipcode As DevExpress.XtraReports.UI.XRBarCode
        Friend WithEvents XrLabel_Address As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel6 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents ParameterClient As DevExpress.XtraReports.Parameters.Parameter
        Friend WithEvents CalculatedField_credit_amt As DevExpress.XtraReports.UI.CalculatedField
        Friend WithEvents CalculatedField_debit_amt As DevExpress.XtraReports.UI.CalculatedField
    End Class
End Namespace
