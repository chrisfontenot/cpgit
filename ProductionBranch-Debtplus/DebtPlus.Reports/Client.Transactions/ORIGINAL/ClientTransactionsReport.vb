#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}
#End Region

Namespace Client.Transactions
    Public Class ClientTransactionsReport
        Inherits DebtPlus.Reports.Template.DatedTemplateXtraReportClass
        Implements DebtPlus.Interfaces.Client.IClient

        Protected DateRangeValue As DebtPlus.Utils.DateRange = DebtPlus.Utils.DateRange.Today

        Public Sub New()
            MyClass.New(DebtPlus.Utils.DateRange.Today)
        End Sub

        Public Sub New(ByVal DateRangeValue As DebtPlus.Utils.DateRange)
            MyBase.New()
            MyClass.DateRangeValue = DateRangeValue
            InitializeComponent()

            AddHandler BeforePrint, AddressOf Report_BeforePrint
            AddHandler XrBarCode_Zipcode.BeforePrint, AddressOf XrBarCode_Zipcode_BeforePrint
            AddHandler XrLabel_Address.BeforePrint, AddressOf XrLabel_Address_BeforePrint
            AddHandler XrLabel_check_number.PreviewClick, AddressOf Field_PreviewClick
            AddHandler XrLabel_tran_type.PreviewClick, AddressOf Field_PreviewClick
            AddHandler XrLabel_message.PreviewClick, AddressOf Field_PreviewClick
            AddHandler XrLabel_debit.PreviewClick, AddressOf Field_PreviewClick
            AddHandler XrLabel_date_created.PreviewClick, AddressOf Field_PreviewClick
            AddHandler XrLabel_credit.PreviewClick, AddressOf Field_PreviewClick

            ScriptReferences = DebtPlus.Utils.AssemblyTools.GetAllSiblingAssemblies()

            '-- Copy the script references to the subreports as well
            For Each Band As DevExpress.XtraReports.UI.Band In Bands
                For Each ctl As DevExpress.XtraReports.UI.XRControl In Band.Controls
                    Dim subReport As DevExpress.XtraReports.UI.XRSubreport = TryCast(ctl, DevExpress.XtraReports.UI.XRSubreport)
                    If subReport IsNot Nothing Then
                        Dim rpt As DevExpress.XtraReports.UI.XtraReport = subReport.ReportSource
                        If rpt IsNot Nothing Then
                            rpt.ScriptReferences = ScriptReferences
                        End If
                    End If
                Next
            Next

            '-- Ensure that client is not specified by default
            ClientId = -1
        End Sub

        Public Property ClientId() As System.Int32 Implements DebtPlus.Interfaces.Client.IClient.ClientId
            Get
                Dim parm As DevExpress.XtraReports.Parameters.Parameter = FindParameter("ParameterClient")
                If parm Is Nothing Then Return -1
                Return Convert.ToInt32(parm.Value)
            End Get
            Set(ByVal value As System.Int32)
                SetParameter("ParameterClient", GetType(Int32), value, "Client ID", False)
            End Set
        End Property

        Public Overrides ReadOnly Property ReportTitle() As String
            Get
                Return "Client Transactions"
            End Get
        End Property

        Public Overrides Sub SetReportParameter(ByVal Parameter As String, ByVal Value As Object)
            If Parameter = "Client" Then
                ClientId = Convert.ToInt32(Value)
            Else
                MyBase.SetReportParameter(Parameter, Value)
            End If
        End Sub

        Public Overrides Function NeedParameters() As Boolean
            Return MyBase.NeedParameters OrElse ClientId < 0
        End Function

        Public Overrides Function RequestReportParameters() As System.Windows.Forms.DialogResult
            Dim Answer As System.Windows.Forms.DialogResult = DialogResult.OK

            If NeedParameters() Then
                Using frm As New DebtPlus.Reports.Template.Forms.DatedClientParametersForm(DateRangeValue)
                    With frm
                        .Parameter_Client = ClientId
                        Answer = .ShowDialog()
                        Parameter_FromDate = .Parameter_FromDate
                        Parameter_ToDate = .Parameter_ToDate
                        ClientId = .Parameter_Client
                    End With
                End Using
            End If

            Return Answer
        End Function

        '***************************** MOVED TO SCRIPTS ********************************

        Dim ds As New System.Data.DataSet("ds")
        Private Sub Report_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs)
            Dim rpt As DevExpress.XtraReports.UI.XtraReport = CType(sender, DevExpress.XtraReports.UI.XtraReport)
            Dim sqlInfo As DebtPlus.LINQ.SQLInfoClass = DebtPlus.LINQ.SQLInfoClass.getDefault()

            Dim tbl As System.Data.DataTable = ds.Tables("rpt_transactions_cl")

            '-- Read the transactions from the system
            If tbl Is Nothing Then
                Dim cn As New System.Data.SqlClient.SqlConnection(sqlInfo.ConnectionString)
                Try
                    Using cmd As System.Data.SqlClient.SqlCommand = New System.Data.SqlClient.SqlCommand()
                        cmd.Connection = cn
                        cmd.CommandText = "rpt_transactions_cl"
                        cmd.CommandType = System.Data.CommandType.StoredProcedure
                        cmd.CommandTimeout = 0
                        cmd.Parameters.Add("@client", System.Data.SqlDbType.Int).Value = rpt.Parameters("ParameterClient").Value
                        cmd.Parameters.Add("@fromdate", System.Data.SqlDbType.DateTime).Value = rpt.Parameters("ParameterFromDate").Value
                        cmd.Parameters.Add("@todate", System.Data.SqlDbType.DateTime).Value = rpt.Parameters("ParameterToDate").Value

                        Using da As New System.Data.SqlClient.SqlDataAdapter(cmd)
                            da.Fill(ds, "rpt_transactions_cl")
                        End Using
                    End Using

                    tbl = ds.Tables("rpt_transactions_cl")
                    tbl.PrimaryKey = New System.Data.DataColumn() {tbl.Columns("item_key")}

                Catch ex As System.Data.SqlClient.SqlException
                    Using gdr As New DebtPlus.Repository.GetDataResult()
                        gdr.HandleException(ex)
                        DebtPlus.UI.Common.ErrorHandling.HandleErrors(gdr, "Error reading client transactions")
                    End Using
                End Try
            End If

            '-- Set the datasource for the report
            rpt.DataSource = New System.Data.DataView(tbl, String.Empty, "item_date, item_key", System.Data.DataViewRowState.CurrentRows)
            For Each calc As DevExpress.XtraReports.UI.CalculatedField In rpt.CalculatedFields
                calc.Assign(rpt.DataSource, rpt.DataMember)
            Next
        End Sub

        Private Function GetClientRow(ByVal Client As Int32) As System.Data.DataRow
            Dim sqlInfo As DebtPlus.LINQ.SQLInfoClass = DebtPlus.LINQ.SQLInfoClass.getDefault()
            Const TableName As String = "view_client_address"
            Dim tbl As System.Data.DataTable = ds.Tables(TableName)

            '-- If the value has already been cached then return the cache entry
            If tbl IsNot Nothing Then
                Dim row As System.Data.DataRow = tbl.Rows.Find(Client)
                If row IsNot Nothing Then Return row
            End If

            '-- Read the data from the database for the client's name
            Using cn As New System.Data.SqlClient.SqlConnection(sqlInfo.ConnectionString)
                cn.Open()
                Using cmd As New System.Data.SqlClient.SqlCommand()
                    With cmd
                        .Connection = cn
                        .CommandText = "SELECT client, name, addr1, addr2, addr3, zipcode FROM view_client_address WHERE client=@client"
                        .CommandType = System.Data.CommandType.Text
                        .Parameters.Add("@client", System.Data.SqlDbType.Int).Value = Client
                    End With

                    Using da As New System.Data.SqlClient.SqlDataAdapter(cmd)
                        da.Fill(ds, TableName)

                        tbl = ds.Tables(TableName)
                        If tbl IsNot Nothing Then
                            If tbl.PrimaryKey.GetUpperBound(0) < 0 Then
                                tbl.PrimaryKey = New System.Data.DataColumn() {tbl.Columns("client")}
                            End If

                            Return tbl.Rows.Find(Client)
                        End If
                    End Using
                End Using
            End Using

            Return Nothing
        End Function

        Private Sub XrBarCode_Zipcode_BeforePrint(sender As Object, e As System.Drawing.Printing.PrintEventArgs)
            With CType(sender, DevExpress.XtraReports.UI.XRBarCode)
                Dim client As Int32 = Convert.ToInt32(CType(.Report, DevExpress.XtraReports.UI.XtraReport).Parameters("ParameterClient").Value())
                Dim row As System.Data.DataRow = GetClientRow(client)
                If row IsNot Nothing Then
                    .Text = DebtPlus.Utils.Format.Strings.DigitsOnly(DebtPlus.Utils.Nulls.DStr(row("zipcode")))
                Else
                    .Text = String.Empty
                End If
                .Visible = .Text <> String.Empty
            End With
        End Sub

        Private Sub XrLabel_Address_BeforePrint(sender As Object, e As System.Drawing.Printing.PrintEventArgs)
            Dim sb As New System.Text.StringBuilder()
            With CType(sender, DevExpress.XtraReports.UI.XRLabel)
                Dim client As Int32 = Convert.ToInt32(CType(.Report, DevExpress.XtraReports.UI.XtraReport).Parameters("ParameterClient").Value())
                Dim row As System.Data.DataRow = GetClientRow(client)
                If row IsNot Nothing Then
                    For Each Fld As String In New String() {"name", "addr1", "addr2", "addr3"}
                        Dim str As String = DebtPlus.Utils.Nulls.DStr(row(Fld)).Trim
                        If str <> String.Empty Then
                            sb.Append(System.Environment.NewLine)
                            sb.Append(str)
                        End If
                    Next
                End If

                If sb.Length <> 0 Then sb.Remove(0, 2)
                .Text = sb.ToString
            End With
        End Sub

        Private Sub Field_PreviewClick(ByVal sender As Object, ByVal e As DevExpress.XtraReports.UI.PreviewMouseEventArgs)
            Dim rpt As DevExpress.XtraReports.UI.XtraReport = CType(CType(sender, DevExpress.XtraReports.UI.XRControl).Report, DevExpress.XtraReports.UI.XtraReport)
            Dim ReportSubTitle As String = CType(rpt, DebtPlus.Interfaces.Reports.IReports).ReportSubTitle
            Dim client_register As System.Int32 = Convert.ToInt32(e.Brick.Value)

            With New cls_subreport(ReportSubTitle, client_register)
                Dim thrd As New System.Threading.Thread(AddressOf .ProcessReport)
                With thrd
                    .Name = "Subreport"
                    .SetApartmentState(Threading.ApartmentState.STA)
                    .IsBackground = True
                    .Start()
                End With
            End With
        End Sub

        Private Class cls_subreport
            Dim client_register As System.Int32
            Dim subtitle As String

            Public Sub New(ByVal Subtitle As String, ByVal client_register As System.Int32)
                MyClass.subtitle = Subtitle
                MyClass.client_register = client_register
            End Sub

            Public Sub ProcessReport()
                Using rpt As DebtPlus.Interfaces.Reports.IReports = CreateSubReport()
                    With CType(rpt, DevExpress.XtraReports.UI.XtraReport)
                        .Parameters("ParameterClientRegister").Value = client_register
                        .Parameters("ParameterSubTitle").Value = subtitle + " (Details)"
                    End With

                    rpt.DisplayPreviewDialog()
                End Using
            End Sub

            Private Function CreateSubReport() As DebtPlus.Interfaces.Reports.IReports
                Dim Asm As System.Reflection.Assembly = FindAssembly("DebtPlus.Reports")
                If Asm IsNot Nothing Then
                    Dim TypeReport As Type = Asm.GetType("DebtPlus.Reports.Client.Transactions.Details.ClientTransactionsDetailsReport")
                    If TypeReport IsNot Nothing Then
                        Return CType(Activator.CreateInstance(TypeReport, New Object() {}), DebtPlus.Interfaces.Reports.IReports)
                    End If
                End If

                Return Nothing
            End Function

            Private Function FindAssembly(ByVal name As String) As System.Reflection.Assembly
                For Each a As System.Reflection.Assembly In AppDomain.CurrentDomain.GetAssemblies()
                    Dim n As System.Reflection.AssemblyName = a.GetName()
                    If n.Name = name Then
                        Return a
                    End If
                Next
                Return Nothing
            End Function
        End Class
    End Class
End Namespace
