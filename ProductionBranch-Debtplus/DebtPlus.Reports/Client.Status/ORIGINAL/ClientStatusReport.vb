#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}
#End Region

Namespace Client.Status
    Public Class ClientStatusReport
        Implements DebtPlus.Interfaces.Client.IClient

        ''' <summary>
        ''' Create an instance of our report
        ''' </summary>
        Public Sub New()
            MyBase.New()
            InitializeComponent()
            AddHandler BeforePrint, AddressOf ClientStatusReport_BeforePrint
            AddHandler XrTable_Trust.BeforePrint, AddressOf XrTable_Trust_BeforePrint
            AddHandler XrPanel_ClientNameAddress.BeforePrint, AddressOf XrPanel_ClientNameAddress_BeforePrint
            AddHandler XrLabel_creditor.BeforePrint, AddressOf XrLabel_creditor_BeforePrint
        End Sub

        Public Property ClientId() As System.Int32 Implements DebtPlus.Interfaces.Client.IClient.ClientId
            Get
                Return Parameter_Client
            End Get
            Set(value As System.Int32)
                Parameter_Client = value
            End Set
        End Property

        Public Property Parameter_Client() As System.Int32
            Get
                Dim parm As DevExpress.XtraReports.Parameters.Parameter = FindParameter("ParameterClient")
                If parm Is Nothing Then Return -1
                Return Convert.ToInt32(parm.Value)
            End Get
            Set(ByVal value As System.Int32)
                SetParameter("ParameterClient", GetType(Int32), value, "Client ID", False)
            End Set
        End Property

        Public Overrides ReadOnly Property ReportTitle() As String
            Get
                Return "Client Status"
            End Get
        End Property

        Public Overrides ReadOnly Property ReportSubtitle() As String
            Get
                Return String.Format("Information for client # {0}", DebtPlus.Utils.Format.Client.FormatClientID(Parameter_Client))
            End Get
        End Property

        Public Overrides Function NeedParameters() As Boolean
            Return MyBase.NeedParameters() OrElse Parameter_Client < 0
        End Function

        Public Overrides Sub SetReportParameter(ByVal Parameter As String, ByVal Value As Object)
            If Parameter = "Client" Then
                Parameter_Client = Convert.ToInt32(Value)
            Else
                MyBase.SetReportParameter(Parameter, Value)
            End If
        End Sub

        Public Overrides Function RequestReportParameters() As System.Windows.Forms.DialogResult
            Dim Answer As System.Windows.Forms.DialogResult = DialogResult.OK
            If NeedParameters() Then
                Using frm As New DebtPlus.Reports.Template.Forms.ClientParametersForm
                    With frm
                        .Parameter_Client = Parameter_Client
                        Answer = .ShowDialog
                        Parameter_Client = .Parameter_Client
                    End With
                End Using
            End If
            Return Answer
        End Function

        Dim ds As New System.Data.DataSet("ds")

        Private Sub XrTable_Trust_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs)
            With CType(sender, DevExpress.XtraReports.UI.XRTable)
                Dim rpt As DevExpress.XtraReports.UI.XtraReport = CType(.Report, DevExpress.XtraReports.UI.XtraReport)
                Dim sqlInfo As DebtPlus.LINQ.SQLInfoClass = DebtPlus.LINQ.SQLInfoClass.getDefault()
                Dim cn As System.Data.SqlClient.SqlConnection = New System.Data.SqlClient.SqlConnection(sqlInfo.ConnectionString)
                Dim rd As System.Data.SqlClient.SqlDataReader = Nothing
                Try
                    cn.Open()
                    Using cmd As New System.Data.SqlClient.SqlCommand()
                        cmd.Connection = cn
                        cmd.CommandText = "rpt_client_status_summary"
                        cmd.CommandType = System.Data.CommandType.StoredProcedure
                        cmd.Parameters.Add("@client", System.Data.SqlDbType.Int).Value = rpt.Parameters("ParameterClient").Value
                        rd = cmd.ExecuteReader(System.Data.CommandBehavior.SingleRow Or System.Data.CommandBehavior.CloseConnection)
                    End Using

                    If rd IsNot Nothing AndAlso rd.Read Then
                        Dim ordinal As System.Int32
                        ordinal = rd.GetOrdinal("counselor")
                        If ordinal >= 0 Then
                            Dim fld As DevExpress.XtraReports.UI.XRTableCell = TryCast(.FindControl("XrTableCell_counselor", True), DevExpress.XtraReports.UI.XRTableCell)
                            If Not rd.IsDBNull(ordinal) AndAlso fld IsNot Nothing Then fld.Text = DebtPlus.Utils.Format.Counselor.FormatCounselor(rd.GetString(ordinal).Trim)
                        End If

                        ordinal = rd.GetOrdinal("last_payment")
                        If ordinal >= 0 Then
                            Dim fld As DevExpress.XtraReports.UI.XRTableCell = TryCast(.FindControl("XrTableCell_last_deposit_amount", True), DevExpress.XtraReports.UI.XRTableCell)
                            If Not rd.IsDBNull(ordinal) AndAlso fld IsNot Nothing Then fld.Text = String.Format("{0:c}", rd.GetValue(ordinal))
                        End If

                        ordinal = rd.GetOrdinal("payment_date")
                        If ordinal >= 0 Then
                            Dim fld As DevExpress.XtraReports.UI.XRTableCell = TryCast(.FindControl("XrTableCell_last_deposit_date", True), DevExpress.XtraReports.UI.XRTableCell)
                            If Not rd.IsDBNull(ordinal) AndAlso fld IsNot Nothing Then fld.Text = String.Format("{0:d}", rd.GetValue(ordinal))
                        End If

                        ordinal = rd.GetOrdinal("available_escrow")
                        If ordinal >= 0 Then
                            Dim fld As DevExpress.XtraReports.UI.XRTableCell = TryCast(.FindControl("XrTableCell_held_in_trust", True), DevExpress.XtraReports.UI.XRTableCell)
                            If Not rd.IsDBNull(ordinal) AndAlso fld IsNot Nothing Then fld.Text = String.Format("{0:c}", rd.GetValue(ordinal))
                        End If
                    End If

                Finally
                    If rd IsNot Nothing AndAlso Not rd.IsClosed Then rd.Close()
                    If cn IsNot Nothing Then
                        cn.Dispose()
                    End If
                End Try
            End With
        End Sub

        Private Sub XrPanel_ClientNameAddress_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs)
            With CType(sender, DevExpress.XtraReports.UI.XRPanel)
                Dim rpt As DevExpress.XtraReports.UI.XtraReport = CType(.Report, DevExpress.XtraReports.UI.XtraReport)
                Dim sqlInfo As DebtPlus.LINQ.SQLInfoClass = DebtPlus.LINQ.SQLInfoClass.getDefault()
                Dim cn As System.Data.SqlClient.SqlConnection = New System.Data.SqlClient.SqlConnection(sqlInfo.ConnectionString)
                Dim rd As System.Data.SqlClient.SqlDataReader = Nothing
                Dim Address As DevExpress.XtraReports.UI.XRLabel = TryCast(.FindControl("XrLabel_NameAndAddress", True), DevExpress.XtraReports.UI.XRLabel)
                Dim PostalCode As DevExpress.XtraReports.UI.XRBarCode = TryCast(.FindControl("XrBarCode1", True), DevExpress.XtraReports.UI.XRBarCode)
                Try
                    cn.Open()
                    Using cmd As New System.Data.SqlClient.SqlCommand()
                        cmd.Connection = cn
                        cmd.CommandText = "SELECT isnull(v.zipcode,'') as zipcode, isnull(v.name,'') as name, isnull(v.addr1,'') as addr1, isnull(v.addr2,'') as addr2, isnull(v.addr3,'') as addr3 FROM view_client_address v WITH (NOLOCK) WHERE v.client = @client"
                        cmd.CommandType = System.Data.CommandType.Text
                        cmd.Parameters.Add("@client", System.Data.SqlDbType.Int).Value = Parameters("ParameterClient").Value
                        rd = cmd.ExecuteReader(System.Data.CommandBehavior.SingleRow Or System.Data.CommandBehavior.CloseConnection)
                    End Using

                    If rd IsNot Nothing AndAlso rd.Read Then
                        If PostalCode IsNot Nothing Then PostalCode.Text = rd.GetString(0).Trim

                        Dim sb As New System.Text.StringBuilder
                        For fldNo As Integer = 0 To 4
                            Dim Item As String = rd.GetString(fldNo).Trim
                            If Item <> String.Empty Then
                                sb.Append(System.Environment.NewLine)
                                sb.Append(Item)
                            End If
                        Next

                        If sb.Length > 0 Then sb.Remove(0, 2)
                        If Address IsNot Nothing Then Address.Text = sb.ToString
                    End If

                Finally
                    If rd IsNot Nothing AndAlso Not rd.IsClosed Then rd.Close()
                    If cn IsNot Nothing Then
                        cn.Dispose()
                    End If
                End Try
            End With
        End Sub

        Private Sub ClientStatusReport_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs)
            Const TableName As String = "rpt_client_status_summary_creditors"
            Dim rpt As DevExpress.XtraReports.UI.XtraReport = CType(sender, DevExpress.XtraReports.UI.XtraReport)
            Dim sqlInfo As DebtPlus.LINQ.SQLInfoClass = DebtPlus.LINQ.SQLInfoClass.getDefault()
            Dim cn As System.Data.SqlClient.SqlConnection = New System.Data.SqlClient.SqlConnection(sqlInfo.ConnectionString)

            '-- Read the dataset for the report
            Try
                cn.Open()

                Using cmd As New System.Data.SqlClient.SqlCommand()
                    cmd.Connection = cn
                    cmd.CommandText = "rpt_client_status_summary_creditors"
                    cmd.CommandType = System.Data.CommandType.StoredProcedure
                    cmd.Parameters.Add("@client", System.Data.SqlDbType.Int).Value = rpt.Parameters("ParameterClient").Value
                    cmd.CommandTimeout = 0

                    Using da As New System.Data.SqlClient.SqlDataAdapter(cmd)
                        da.Fill(ds, TableName)
                    End Using
                End Using

                rpt.DataSource = ds.Tables(TableName).DefaultView

            Catch ex As Exception
                Using gdr As New DebtPlus.Repository.GetDataResult()
                    gdr.HandleException(ex)
                    DebtPlus.UI.Common.ErrorHandling.HandleErrors(gdr, "Error reading information from database")
                End Using

            Finally
                If cn IsNot Nothing Then
                    cn.Dispose()
                End If
            End Try
        End Sub

        Private Sub XrLabel_creditor_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs)
            With CType(sender, DevExpress.XtraReports.UI.XRLabel)
                Dim rpt As DevExpress.XtraReports.UI.XtraReport = CType(.Report, DevExpress.XtraReports.UI.XtraReport)
                Dim creditor_id As String = String.Empty
                Dim creditor_name As String = String.Empty
                If Not rpt.GetCurrentColumnValue("creditor") Is System.DBNull.Value Then creditor_id = Convert.ToString(rpt.GetCurrentColumnValue("creditor"))
                If Not rpt.GetCurrentColumnValue("creditor_name") Is System.DBNull.Value Then creditor_name = Convert.ToString(rpt.GetCurrentColumnValue("creditor_name"))

                '-- Include both with a space if both are available. Oterwise, just use the one.
                If creditor_id <> String.Empty AndAlso creditor_name <> String.Empty Then
                    .Text = String.Format("{0} {1}", creditor_id, creditor_name)
                Else
                    .Text = creditor_id + creditor_name
                End If
            End With
        End Sub

    End Class
End Namespace
