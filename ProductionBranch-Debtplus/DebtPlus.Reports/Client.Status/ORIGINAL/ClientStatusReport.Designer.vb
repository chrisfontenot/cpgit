Namespace Client.Status
    <Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
    Partial Class ClientStatusReport
        Inherits DebtPlus.Reports.Template.TemplateXtraReportClass

        'XtraReport overrides dispose to clean up the component list.
        <System.Diagnostics.DebuggerNonUserCode()> _
        Protected Overrides Sub Dispose(ByVal disposing As Boolean)
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
            MyBase.Dispose(disposing)
        End Sub

        'Required by the Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Designer
        'It can be modified using the Designer.  
        'Do not modify it using the code editor.
        <System.Diagnostics.DebuggerStepThrough()> _
        Private Sub InitializeComponent()
            Dim PostNetGenerator1 As DevExpress.XtraPrinting.BarCode.PostNetGenerator = New DevExpress.XtraPrinting.BarCode.PostNetGenerator()
            Dim XrSummary1 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary()
            Dim XrSummary2 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary()
            Dim XrSummary3 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary()
            Dim XrSummary4 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary()
            Dim XrSummary5 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary()
            Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(ClientStatusReport))
            Me.XrPanel_ClientNameAddress = New DevExpress.XtraReports.UI.XRPanel()
            Me.XrLabel_NameAndAddress = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrBarCode1 = New DevExpress.XtraReports.UI.XRBarCode()
            Me.XrPanel3 = New DevExpress.XtraReports.UI.XRPanel()
            Me.XrLabel11 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel10 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel9 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel8 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel7 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel6 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel5 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_current_balance = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_acct = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_starting_balance = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_dm_payment = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_last_payment = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_est_interest = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_creditor = New DevExpress.XtraReports.UI.XRLabel()
            Me.ReportFooter = New DevExpress.XtraReports.UI.ReportFooterBand()
            Me.XrLabel_total_est_interest = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel23 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_total_last_payment = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_total_dm_payment = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_total_starting_balance = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_total_current_balance = New DevExpress.XtraReports.UI.XRLabel()
            Me.ParameterClient = New DevExpress.XtraReports.Parameters.Parameter()
            Me.XrTable_Trust = New DevExpress.XtraReports.UI.XRTable()
            Me.XrTableRow1 = New DevExpress.XtraReports.UI.XRTableRow()
            Me.XrTableCell1 = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableCell_counselor = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableRow2 = New DevExpress.XtraReports.UI.XRTableRow()
            Me.XrTableCell2 = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableCell_last_deposit_amount = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableRow3 = New DevExpress.XtraReports.UI.XRTableRow()
            Me.XrTableCell5 = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableCell_last_deposit_date = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableRow4 = New DevExpress.XtraReports.UI.XRTableRow()
            Me.XrTableCell7 = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableCell_held_in_trust = New DevExpress.XtraReports.UI.XRTableCell()
            CType(Me.XrTable_Trust, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
            '
            'PageHeader
            '
            Me.PageHeader.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrTable_Trust, Me.XrPanel3, Me.XrPanel_ClientNameAddress})
            Me.PageHeader.HeightF = 401.5833!
            Me.PageHeader.Controls.SetChildIndex(Me.XrPanel_ClientNameAddress, 0)
            Me.PageHeader.Controls.SetChildIndex(Me.XrPanel3, 0)
            Me.PageHeader.Controls.SetChildIndex(Me.XrPanel_AgencyAddress, 0)
            Me.PageHeader.Controls.SetChildIndex(Me.XrLabel_Title, 0)
            Me.PageHeader.Controls.SetChildIndex(Me.XrLine_Title, 0)
            Me.PageHeader.Controls.SetChildIndex(Me.XrLabel_Subtitle, 0)
            Me.PageHeader.Controls.SetChildIndex(Me.XrPageInfo1, 0)
            Me.PageHeader.Controls.SetChildIndex(Me.XrTable_Trust, 0)
            '
            'XrLabel_Title
            '
            Me.XrLabel_Title.StylePriority.UseFont = False
            '
            'XrPageInfo_PageNumber
            '
            Me.XrPageInfo_PageNumber.LocationFloat = New DevExpress.Utils.PointFloat(658.0!, 8.0!)
            Me.XrPageInfo_PageNumber.StylePriority.UseTextAlignment = False
            '
            'XRLabel_Agency_Name
            '
            Me.XRLabel_Agency_Name.StylePriority.UseTextAlignment = False
            '
            'XrLabel_Agency_Address3
            '
            Me.XrLabel_Agency_Address3.StylePriority.UseFont = False
            Me.XrLabel_Agency_Address3.StylePriority.UseTextAlignment = False
            '
            'XrLabel_Agency_Address1
            '
            Me.XrLabel_Agency_Address1.StylePriority.UseFont = False
            Me.XrLabel_Agency_Address1.StylePriority.UseTextAlignment = False
            '
            'XrLabel_Agency_Phone
            '
            Me.XrLabel_Agency_Phone.StylePriority.UseFont = False
            Me.XrLabel_Agency_Phone.StylePriority.UseTextAlignment = False
            '
            'XrLabel_Agency_Address2
            '
            Me.XrLabel_Agency_Address2.StylePriority.UseFont = False
            Me.XrLabel_Agency_Address2.StylePriority.UseTextAlignment = False
            '
            'Detail
            '
            Me.Detail.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel_creditor, Me.XrLabel_est_interest, Me.XrLabel_last_payment, Me.XrLabel_dm_payment, Me.XrLabel_starting_balance, Me.XrLabel_acct, Me.XrLabel_current_balance})
            Me.Detail.HeightF = 17.0!
            '
            'XrPanel_ClientNameAddress
            '
            Me.XrPanel_ClientNameAddress.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel_NameAndAddress, Me.XrBarCode1})
            Me.XrPanel_ClientNameAddress.LocationFloat = New DevExpress.Utils.PointFloat(75.0!, 220.0!)
            Me.XrPanel_ClientNameAddress.Name = "XrPanel_ClientNameAddress"
            Me.XrPanel_ClientNameAddress.Scripts.OnBeforePrint = "XrPanel_ClientNameAddress_BeforePrint"
            Me.XrPanel_ClientNameAddress.SizeF = New System.Drawing.SizeF(300.0!, 100.0!)
            '
            'XrLabel_NameAndAddress
            '
            Me.XrLabel_NameAndAddress.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 18.0!)
            Me.XrLabel_NameAndAddress.Multiline = True
            Me.XrLabel_NameAndAddress.Name = "XrLabel_NameAndAddress"
            Me.XrLabel_NameAndAddress.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_NameAndAddress.SizeF = New System.Drawing.SizeF(300.0!, 82.0!)
            Me.XrLabel_NameAndAddress.Text = "Name" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Address Line 1" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Address Line 2" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Address Line 3"
            '
            'XrBarCode1
            '
            Me.XrBarCode1.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
            Me.XrBarCode1.Name = "XrBarCode1"
            Me.XrBarCode1.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
            Me.XrBarCode1.ShowText = False
            Me.XrBarCode1.SizeF = New System.Drawing.SizeF(300.0!, 17.0!)
            Me.XrBarCode1.Symbology = PostNetGenerator1
            Me.XrBarCode1.Text = "902108563"
            '
            'XrPanel3
            '
            Me.XrPanel3.BackColor = System.Drawing.Color.Teal
            Me.XrPanel3.BorderColor = System.Drawing.Color.Teal
            Me.XrPanel3.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel11, Me.XrLabel10, Me.XrLabel9, Me.XrLabel8, Me.XrLabel7, Me.XrLabel6, Me.XrLabel5})
            Me.XrPanel3.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 352.2917!)
            Me.XrPanel3.Name = "XrPanel3"
            Me.XrPanel3.SizeF = New System.Drawing.SizeF(800.0!, 42.0!)
            Me.XrPanel3.StylePriority.UseBackColor = False
            Me.XrPanel3.StylePriority.UseBorderColor = False
            '
            'XrLabel11
            '
            Me.XrLabel11.BackColor = System.Drawing.Color.Transparent
            Me.XrLabel11.BorderColor = System.Drawing.Color.Transparent
            Me.XrLabel11.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel11.ForeColor = System.Drawing.Color.White
            Me.XrLabel11.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 4.0!)
            Me.XrLabel11.Multiline = True
            Me.XrLabel11.Name = "XrLabel11"
            Me.XrLabel11.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel11.SizeF = New System.Drawing.SizeF(225.0!, 33.0!)
            Me.XrLabel11.StylePriority.UseBackColor = False
            Me.XrLabel11.StylePriority.UseBorderColor = False
            Me.XrLabel11.StylePriority.UseFont = False
            Me.XrLabel11.StylePriority.UseForeColor = False
            Me.XrLabel11.StylePriority.UseTextAlignment = False
            Me.XrLabel11.Text = "" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Creditor"
            Me.XrLabel11.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
            '
            'XrLabel10
            '
            Me.XrLabel10.BackColor = System.Drawing.Color.Transparent
            Me.XrLabel10.BorderColor = System.Drawing.Color.Transparent
            Me.XrLabel10.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel10.ForeColor = System.Drawing.Color.White
            Me.XrLabel10.LocationFloat = New DevExpress.Utils.PointFloat(225.0!, 4.0!)
            Me.XrLabel10.Multiline = True
            Me.XrLabel10.Name = "XrLabel10"
            Me.XrLabel10.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel10.SizeF = New System.Drawing.SizeF(158.0!, 33.0!)
            Me.XrLabel10.StylePriority.UseBackColor = False
            Me.XrLabel10.StylePriority.UseBorderColor = False
            Me.XrLabel10.StylePriority.UseFont = False
            Me.XrLabel10.StylePriority.UseForeColor = False
            Me.XrLabel10.StylePriority.UseTextAlignment = False
            Me.XrLabel10.Text = "" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Acct #"
            Me.XrLabel10.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
            '
            'XrLabel9
            '
            Me.XrLabel9.BackColor = System.Drawing.Color.Transparent
            Me.XrLabel9.BorderColor = System.Drawing.Color.Transparent
            Me.XrLabel9.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel9.ForeColor = System.Drawing.Color.White
            Me.XrLabel9.LocationFloat = New DevExpress.Utils.PointFloat(717.0!, 4.0!)
            Me.XrLabel9.Multiline = True
            Me.XrLabel9.Name = "XrLabel9"
            Me.XrLabel9.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel9.SizeF = New System.Drawing.SizeF(75.0!, 33.0!)
            Me.XrLabel9.StylePriority.UseBackColor = False
            Me.XrLabel9.StylePriority.UseBorderColor = False
            Me.XrLabel9.StylePriority.UseFont = False
            Me.XrLabel9.StylePriority.UseForeColor = False
            Me.XrLabel9.StylePriority.UseTextAlignment = False
            Me.XrLabel9.Text = "Current" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Balance"
            Me.XrLabel9.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
            '
            'XrLabel8
            '
            Me.XrLabel8.BackColor = System.Drawing.Color.Transparent
            Me.XrLabel8.BorderColor = System.Drawing.Color.Transparent
            Me.XrLabel8.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel8.ForeColor = System.Drawing.Color.White
            Me.XrLabel8.LocationFloat = New DevExpress.Utils.PointFloat(667.0!, 4.0!)
            Me.XrLabel8.Multiline = True
            Me.XrLabel8.Name = "XrLabel8"
            Me.XrLabel8.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel8.SizeF = New System.Drawing.SizeF(50.0!, 33.0!)
            Me.XrLabel8.StylePriority.UseBackColor = False
            Me.XrLabel8.StylePriority.UseBorderColor = False
            Me.XrLabel8.StylePriority.UseFont = False
            Me.XrLabel8.StylePriority.UseForeColor = False
            Me.XrLabel8.StylePriority.UseTextAlignment = False
            Me.XrLabel8.Text = "Est." & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Interest"
            Me.XrLabel8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
            '
            'XrLabel7
            '
            Me.XrLabel7.BackColor = System.Drawing.Color.Transparent
            Me.XrLabel7.BorderColor = System.Drawing.Color.Transparent
            Me.XrLabel7.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel7.ForeColor = System.Drawing.Color.White
            Me.XrLabel7.LocationFloat = New DevExpress.Utils.PointFloat(567.0!, 4.0!)
            Me.XrLabel7.Multiline = True
            Me.XrLabel7.Name = "XrLabel7"
            Me.XrLabel7.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel7.SizeF = New System.Drawing.SizeF(56.0!, 33.0!)
            Me.XrLabel7.StylePriority.UseBackColor = False
            Me.XrLabel7.StylePriority.UseBorderColor = False
            Me.XrLabel7.StylePriority.UseFont = False
            Me.XrLabel7.StylePriority.UseForeColor = False
            Me.XrLabel7.StylePriority.UseTextAlignment = False
            Me.XrLabel7.Text = "Last Mo" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Disb"
            Me.XrLabel7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
            '
            'XrLabel6
            '
            Me.XrLabel6.BackColor = System.Drawing.Color.Transparent
            Me.XrLabel6.BorderColor = System.Drawing.Color.Transparent
            Me.XrLabel6.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel6.ForeColor = System.Drawing.Color.White
            Me.XrLabel6.LocationFloat = New DevExpress.Utils.PointFloat(475.0!, 4.0!)
            Me.XrLabel6.Multiline = True
            Me.XrLabel6.Name = "XrLabel6"
            Me.XrLabel6.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel6.SizeF = New System.Drawing.SizeF(67.0!, 33.0!)
            Me.XrLabel6.StylePriority.UseBackColor = False
            Me.XrLabel6.StylePriority.UseBorderColor = False
            Me.XrLabel6.StylePriority.UseFont = False
            Me.XrLabel6.StylePriority.UseForeColor = False
            Me.XrLabel6.StylePriority.UseTextAlignment = False
            Me.XrLabel6.Text = "Proposed" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Payment"
            Me.XrLabel6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
            '
            'XrLabel5
            '
            Me.XrLabel5.BackColor = System.Drawing.Color.Transparent
            Me.XrLabel5.BorderColor = System.Drawing.Color.Transparent
            Me.XrLabel5.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel5.ForeColor = System.Drawing.Color.White
            Me.XrLabel5.LocationFloat = New DevExpress.Utils.PointFloat(392.0!, 4.0!)
            Me.XrLabel5.Multiline = True
            Me.XrLabel5.Name = "XrLabel5"
            Me.XrLabel5.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel5.SizeF = New System.Drawing.SizeF(83.0!, 33.0!)
            Me.XrLabel5.StylePriority.UseBackColor = False
            Me.XrLabel5.StylePriority.UseBorderColor = False
            Me.XrLabel5.StylePriority.UseFont = False
            Me.XrLabel5.StylePriority.UseForeColor = False
            Me.XrLabel5.StylePriority.UseTextAlignment = False
            Me.XrLabel5.Text = "Starting" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Balance"
            Me.XrLabel5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
            '
            'XrLabel_current_balance
            '
            Me.XrLabel_current_balance.BackColor = System.Drawing.Color.Transparent
            Me.XrLabel_current_balance.BorderColor = System.Drawing.Color.Transparent
            Me.XrLabel_current_balance.CanGrow = False
            Me.XrLabel_current_balance.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "current_balance", "{0:c}")})
            Me.XrLabel_current_balance.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel_current_balance.ForeColor = System.Drawing.SystemColors.WindowText
            Me.XrLabel_current_balance.LocationFloat = New DevExpress.Utils.PointFloat(717.0!, 0.0!)
            Me.XrLabel_current_balance.Name = "XrLabel_current_balance"
            Me.XrLabel_current_balance.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_current_balance.SizeF = New System.Drawing.SizeF(75.0!, 17.0!)
            Me.XrLabel_current_balance.StylePriority.UseBackColor = False
            Me.XrLabel_current_balance.StylePriority.UseBorderColor = False
            Me.XrLabel_current_balance.StylePriority.UseFont = False
            Me.XrLabel_current_balance.StylePriority.UseForeColor = False
            Me.XrLabel_current_balance.StylePriority.UseTextAlignment = False
            Me.XrLabel_current_balance.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrLabel_current_balance.WordWrap = False
            '
            'XrLabel_acct
            '
            Me.XrLabel_acct.BackColor = System.Drawing.Color.Transparent
            Me.XrLabel_acct.BorderColor = System.Drawing.Color.Transparent
            Me.XrLabel_acct.CanGrow = False
            Me.XrLabel_acct.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "acct")})
            Me.XrLabel_acct.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel_acct.ForeColor = System.Drawing.SystemColors.WindowText
            Me.XrLabel_acct.LocationFloat = New DevExpress.Utils.PointFloat(225.0!, 0.0!)
            Me.XrLabel_acct.Name = "XrLabel_acct"
            Me.XrLabel_acct.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_acct.SizeF = New System.Drawing.SizeF(158.0!, 17.0!)
            Me.XrLabel_acct.StylePriority.UseBackColor = False
            Me.XrLabel_acct.StylePriority.UseBorderColor = False
            Me.XrLabel_acct.StylePriority.UseFont = False
            Me.XrLabel_acct.StylePriority.UseForeColor = False
            Me.XrLabel_acct.StylePriority.UseTextAlignment = False
            Me.XrLabel_acct.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            Me.XrLabel_acct.WordWrap = False
            '
            'XrLabel_starting_balance
            '
            Me.XrLabel_starting_balance.BackColor = System.Drawing.Color.Transparent
            Me.XrLabel_starting_balance.BorderColor = System.Drawing.Color.Transparent
            Me.XrLabel_starting_balance.CanGrow = False
            Me.XrLabel_starting_balance.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "starting_balance", "{0:c}")})
            Me.XrLabel_starting_balance.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel_starting_balance.ForeColor = System.Drawing.SystemColors.WindowText
            Me.XrLabel_starting_balance.LocationFloat = New DevExpress.Utils.PointFloat(392.0!, 0.0!)
            Me.XrLabel_starting_balance.Name = "XrLabel_starting_balance"
            Me.XrLabel_starting_balance.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_starting_balance.SizeF = New System.Drawing.SizeF(83.0!, 17.0!)
            Me.XrLabel_starting_balance.StylePriority.UseBackColor = False
            Me.XrLabel_starting_balance.StylePriority.UseBorderColor = False
            Me.XrLabel_starting_balance.StylePriority.UseFont = False
            Me.XrLabel_starting_balance.StylePriority.UseForeColor = False
            Me.XrLabel_starting_balance.StylePriority.UseTextAlignment = False
            Me.XrLabel_starting_balance.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrLabel_starting_balance.WordWrap = False
            '
            'XrLabel_dm_payment
            '
            Me.XrLabel_dm_payment.BackColor = System.Drawing.Color.Transparent
            Me.XrLabel_dm_payment.BorderColor = System.Drawing.Color.Transparent
            Me.XrLabel_dm_payment.CanGrow = False
            Me.XrLabel_dm_payment.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "dm_payment", "{0:c}")})
            Me.XrLabel_dm_payment.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel_dm_payment.ForeColor = System.Drawing.SystemColors.WindowText
            Me.XrLabel_dm_payment.LocationFloat = New DevExpress.Utils.PointFloat(483.0!, 0.0!)
            Me.XrLabel_dm_payment.Name = "XrLabel_dm_payment"
            Me.XrLabel_dm_payment.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_dm_payment.SizeF = New System.Drawing.SizeF(58.0!, 17.0!)
            Me.XrLabel_dm_payment.StylePriority.UseBackColor = False
            Me.XrLabel_dm_payment.StylePriority.UseBorderColor = False
            Me.XrLabel_dm_payment.StylePriority.UseFont = False
            Me.XrLabel_dm_payment.StylePriority.UseForeColor = False
            Me.XrLabel_dm_payment.StylePriority.UseTextAlignment = False
            Me.XrLabel_dm_payment.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrLabel_dm_payment.WordWrap = False
            '
            'XrLabel_last_payment
            '
            Me.XrLabel_last_payment.BackColor = System.Drawing.Color.Transparent
            Me.XrLabel_last_payment.BorderColor = System.Drawing.Color.Transparent
            Me.XrLabel_last_payment.CanGrow = False
            Me.XrLabel_last_payment.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "last_payment", "{0:c}")})
            Me.XrLabel_last_payment.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel_last_payment.ForeColor = System.Drawing.SystemColors.WindowText
            Me.XrLabel_last_payment.LocationFloat = New DevExpress.Utils.PointFloat(550.0!, 0.0!)
            Me.XrLabel_last_payment.Name = "XrLabel_last_payment"
            Me.XrLabel_last_payment.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_last_payment.SizeF = New System.Drawing.SizeF(73.0!, 17.0!)
            Me.XrLabel_last_payment.StylePriority.UseBackColor = False
            Me.XrLabel_last_payment.StylePriority.UseBorderColor = False
            Me.XrLabel_last_payment.StylePriority.UseFont = False
            Me.XrLabel_last_payment.StylePriority.UseForeColor = False
            Me.XrLabel_last_payment.StylePriority.UseTextAlignment = False
            Me.XrLabel_last_payment.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrLabel_last_payment.WordWrap = False
            '
            'XrLabel_est_interest
            '
            Me.XrLabel_est_interest.BackColor = System.Drawing.Color.Transparent
            Me.XrLabel_est_interest.BorderColor = System.Drawing.Color.Transparent
            Me.XrLabel_est_interest.CanGrow = False
            Me.XrLabel_est_interest.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "est_interest", "{0:c}")})
            Me.XrLabel_est_interest.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel_est_interest.ForeColor = System.Drawing.SystemColors.WindowText
            Me.XrLabel_est_interest.LocationFloat = New DevExpress.Utils.PointFloat(625.0!, 0.0!)
            Me.XrLabel_est_interest.Name = "XrLabel_est_interest"
            Me.XrLabel_est_interest.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_est_interest.SizeF = New System.Drawing.SizeF(92.0!, 17.0!)
            Me.XrLabel_est_interest.StylePriority.UseBackColor = False
            Me.XrLabel_est_interest.StylePriority.UseBorderColor = False
            Me.XrLabel_est_interest.StylePriority.UseFont = False
            Me.XrLabel_est_interest.StylePriority.UseForeColor = False
            Me.XrLabel_est_interest.StylePriority.UseTextAlignment = False
            Me.XrLabel_est_interest.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrLabel_est_interest.WordWrap = False
            '
            'XrLabel_creditor
            '
            Me.XrLabel_creditor.BackColor = System.Drawing.Color.Transparent
            Me.XrLabel_creditor.BorderColor = System.Drawing.Color.Transparent
            Me.XrLabel_creditor.CanGrow = False
            Me.XrLabel_creditor.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel_creditor.ForeColor = System.Drawing.SystemColors.WindowText
            Me.XrLabel_creditor.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
            Me.XrLabel_creditor.Name = "XrLabel_creditor"
            Me.XrLabel_creditor.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_creditor.Scripts.OnBeforePrint = "XrLabel_creditor_BeforePrint"
            Me.XrLabel_creditor.SizeF = New System.Drawing.SizeF(225.0!, 17.0!)
            Me.XrLabel_creditor.StylePriority.UseBackColor = False
            Me.XrLabel_creditor.StylePriority.UseBorderColor = False
            Me.XrLabel_creditor.StylePriority.UseFont = False
            Me.XrLabel_creditor.StylePriority.UseForeColor = False
            Me.XrLabel_creditor.StylePriority.UseTextAlignment = False
            Me.XrLabel_creditor.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            Me.XrLabel_creditor.WordWrap = False
            '
            'ReportFooter
            '
            Me.ReportFooter.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel_total_est_interest, Me.XrLabel23, Me.XrLabel_total_last_payment, Me.XrLabel_total_dm_payment, Me.XrLabel_total_starting_balance, Me.XrLabel_total_current_balance})
            Me.ReportFooter.HeightF = 17.0!
            Me.ReportFooter.Name = "ReportFooter"
            '
            'XrLabel_total_est_interest
            '
            Me.XrLabel_total_est_interest.BackColor = System.Drawing.Color.Transparent
            Me.XrLabel_total_est_interest.BorderColor = System.Drawing.Color.Transparent
            Me.XrLabel_total_est_interest.CanGrow = False
            Me.XrLabel_total_est_interest.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "est_interest", "{0:c}")})
            Me.XrLabel_total_est_interest.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel_total_est_interest.ForeColor = System.Drawing.SystemColors.WindowText
            Me.XrLabel_total_est_interest.LocationFloat = New DevExpress.Utils.PointFloat(625.0!, 0.0!)
            Me.XrLabel_total_est_interest.Name = "XrLabel_total_est_interest"
            Me.XrLabel_total_est_interest.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_total_est_interest.SizeF = New System.Drawing.SizeF(92.0!, 17.0!)
            Me.XrLabel_total_est_interest.StylePriority.UseBackColor = False
            Me.XrLabel_total_est_interest.StylePriority.UseBorderColor = False
            Me.XrLabel_total_est_interest.StylePriority.UseFont = False
            Me.XrLabel_total_est_interest.StylePriority.UseForeColor = False
            Me.XrLabel_total_est_interest.StylePriority.UseTextAlignment = False
            XrSummary1.FormatString = "{0:c}"
            XrSummary1.Running = DevExpress.XtraReports.UI.SummaryRunning.Report
            Me.XrLabel_total_est_interest.Summary = XrSummary1
            Me.XrLabel_total_est_interest.Text = "$0.00"
            Me.XrLabel_total_est_interest.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrLabel_total_est_interest.WordWrap = False
            '
            'XrLabel23
            '
            Me.XrLabel23.BackColor = System.Drawing.Color.Transparent
            Me.XrLabel23.BorderColor = System.Drawing.Color.Transparent
            Me.XrLabel23.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel23.ForeColor = System.Drawing.SystemColors.WindowText
            Me.XrLabel23.LocationFloat = New DevExpress.Utils.PointFloat(8.0!, 0.0!)
            Me.XrLabel23.Name = "XrLabel23"
            Me.XrLabel23.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel23.SizeF = New System.Drawing.SizeF(158.0!, 17.0!)
            Me.XrLabel23.StylePriority.UseBackColor = False
            Me.XrLabel23.StylePriority.UseBorderColor = False
            Me.XrLabel23.StylePriority.UseFont = False
            Me.XrLabel23.StylePriority.UseForeColor = False
            Me.XrLabel23.StylePriority.UseTextAlignment = False
            Me.XrLabel23.Text = "Totals"
            Me.XrLabel23.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            '
            'XrLabel_total_last_payment
            '
            Me.XrLabel_total_last_payment.BackColor = System.Drawing.Color.Transparent
            Me.XrLabel_total_last_payment.BorderColor = System.Drawing.Color.Transparent
            Me.XrLabel_total_last_payment.CanGrow = False
            Me.XrLabel_total_last_payment.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "last_payment", "{0:c}")})
            Me.XrLabel_total_last_payment.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel_total_last_payment.ForeColor = System.Drawing.SystemColors.WindowText
            Me.XrLabel_total_last_payment.LocationFloat = New DevExpress.Utils.PointFloat(550.0!, 0.0!)
            Me.XrLabel_total_last_payment.Name = "XrLabel_total_last_payment"
            Me.XrLabel_total_last_payment.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_total_last_payment.SizeF = New System.Drawing.SizeF(73.0!, 17.0!)
            Me.XrLabel_total_last_payment.StylePriority.UseBackColor = False
            Me.XrLabel_total_last_payment.StylePriority.UseBorderColor = False
            Me.XrLabel_total_last_payment.StylePriority.UseFont = False
            Me.XrLabel_total_last_payment.StylePriority.UseForeColor = False
            Me.XrLabel_total_last_payment.StylePriority.UseTextAlignment = False
            XrSummary2.FormatString = "{0:c}"
            XrSummary2.Running = DevExpress.XtraReports.UI.SummaryRunning.Report
            Me.XrLabel_total_last_payment.Summary = XrSummary2
            Me.XrLabel_total_last_payment.Text = "$0.00"
            Me.XrLabel_total_last_payment.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrLabel_total_last_payment.WordWrap = False
            '
            'XrLabel_total_dm_payment
            '
            Me.XrLabel_total_dm_payment.BackColor = System.Drawing.Color.Transparent
            Me.XrLabel_total_dm_payment.BorderColor = System.Drawing.Color.Transparent
            Me.XrLabel_total_dm_payment.CanGrow = False
            Me.XrLabel_total_dm_payment.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "dm_payment", "{0:c}")})
            Me.XrLabel_total_dm_payment.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel_total_dm_payment.ForeColor = System.Drawing.SystemColors.WindowText
            Me.XrLabel_total_dm_payment.LocationFloat = New DevExpress.Utils.PointFloat(483.0!, 0.0!)
            Me.XrLabel_total_dm_payment.Name = "XrLabel_total_dm_payment"
            Me.XrLabel_total_dm_payment.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_total_dm_payment.SizeF = New System.Drawing.SizeF(58.0!, 17.0!)
            Me.XrLabel_total_dm_payment.StylePriority.UseBackColor = False
            Me.XrLabel_total_dm_payment.StylePriority.UseBorderColor = False
            Me.XrLabel_total_dm_payment.StylePriority.UseFont = False
            Me.XrLabel_total_dm_payment.StylePriority.UseForeColor = False
            Me.XrLabel_total_dm_payment.StylePriority.UseTextAlignment = False
            XrSummary3.FormatString = "{0:c}"
            XrSummary3.Running = DevExpress.XtraReports.UI.SummaryRunning.Report
            Me.XrLabel_total_dm_payment.Summary = XrSummary3
            Me.XrLabel_total_dm_payment.Text = "$0.00"
            Me.XrLabel_total_dm_payment.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrLabel_total_dm_payment.WordWrap = False
            '
            'XrLabel_total_starting_balance
            '
            Me.XrLabel_total_starting_balance.BackColor = System.Drawing.Color.Transparent
            Me.XrLabel_total_starting_balance.BorderColor = System.Drawing.Color.Transparent
            Me.XrLabel_total_starting_balance.CanGrow = False
            Me.XrLabel_total_starting_balance.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "starting_balance", "{0:c}")})
            Me.XrLabel_total_starting_balance.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel_total_starting_balance.ForeColor = System.Drawing.SystemColors.WindowText
            Me.XrLabel_total_starting_balance.LocationFloat = New DevExpress.Utils.PointFloat(225.0!, 0.0!)
            Me.XrLabel_total_starting_balance.Name = "XrLabel_total_starting_balance"
            Me.XrLabel_total_starting_balance.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_total_starting_balance.SizeF = New System.Drawing.SizeF(250.0!, 17.0!)
            Me.XrLabel_total_starting_balance.StylePriority.UseBackColor = False
            Me.XrLabel_total_starting_balance.StylePriority.UseBorderColor = False
            Me.XrLabel_total_starting_balance.StylePriority.UseFont = False
            Me.XrLabel_total_starting_balance.StylePriority.UseForeColor = False
            Me.XrLabel_total_starting_balance.StylePriority.UseTextAlignment = False
            XrSummary4.FormatString = "{0:c}"
            XrSummary4.Running = DevExpress.XtraReports.UI.SummaryRunning.Report
            Me.XrLabel_total_starting_balance.Summary = XrSummary4
            Me.XrLabel_total_starting_balance.Text = "$0.00"
            Me.XrLabel_total_starting_balance.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrLabel_total_starting_balance.WordWrap = False
            '
            'XrLabel_total_current_balance
            '
            Me.XrLabel_total_current_balance.BackColor = System.Drawing.Color.Transparent
            Me.XrLabel_total_current_balance.BorderColor = System.Drawing.Color.Transparent
            Me.XrLabel_total_current_balance.CanGrow = False
            Me.XrLabel_total_current_balance.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "current_balance", "{0:c}")})
            Me.XrLabel_total_current_balance.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel_total_current_balance.ForeColor = System.Drawing.SystemColors.WindowText
            Me.XrLabel_total_current_balance.LocationFloat = New DevExpress.Utils.PointFloat(717.0!, 0.0!)
            Me.XrLabel_total_current_balance.Name = "XrLabel_total_current_balance"
            Me.XrLabel_total_current_balance.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_total_current_balance.SizeF = New System.Drawing.SizeF(75.0!, 17.0!)
            Me.XrLabel_total_current_balance.StylePriority.UseBackColor = False
            Me.XrLabel_total_current_balance.StylePriority.UseBorderColor = False
            Me.XrLabel_total_current_balance.StylePriority.UseFont = False
            Me.XrLabel_total_current_balance.StylePriority.UseForeColor = False
            Me.XrLabel_total_current_balance.StylePriority.UseTextAlignment = False
            XrSummary5.FormatString = "{0:c}"
            XrSummary5.Running = DevExpress.XtraReports.UI.SummaryRunning.Report
            Me.XrLabel_total_current_balance.Summary = XrSummary5
            Me.XrLabel_total_current_balance.Text = "$0.00"
            Me.XrLabel_total_current_balance.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrLabel_total_current_balance.WordWrap = False
            '
            'ParameterClient
            '
            Me.ParameterClient.Name = "ParameterClient"
            Me.ParameterClient.Type = GetType(Integer)
            Me.ParameterClient.Value = -1
            Me.ParameterClient.Visible = False
            '
            'XrTable_Trust
            '
            Me.XrTable_Trust.LocationFloat = New DevExpress.Utils.PointFloat(500.0!, 220.0!)
            Me.XrTable_Trust.Name = "XrTable_Trust"
            Me.XrTable_Trust.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrTable_Trust.Rows.AddRange(New DevExpress.XtraReports.UI.XRTableRow() {Me.XrTableRow1, Me.XrTableRow2, Me.XrTableRow3, Me.XrTableRow4})
            Me.XrTable_Trust.Scripts.OnBeforePrint = "XrTable_Trust_BeforePrint"
            Me.XrTable_Trust.SizeF = New System.Drawing.SizeF(300.0!, 68.0!)
            Me.XrTable_Trust.StylePriority.UsePadding = False
            '
            'XrTableRow1
            '
            Me.XrTableRow1.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.XrTableCell1, Me.XrTableCell_counselor})
            Me.XrTableRow1.Name = "XrTableRow1"
            Me.XrTableRow1.Weight = 0.67999999999999994R
            '
            'XrTableCell1
            '
            Me.XrTableCell1.Name = "XrTableCell1"
            Me.XrTableCell1.StylePriority.UseTextAlignment = False
            Me.XrTableCell1.Text = "Counselor"
            Me.XrTableCell1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            Me.XrTableCell1.Weight = 1.7500006103515626R
            '
            'XrTableCell_counselor
            '
            Me.XrTableCell_counselor.Name = "XrTableCell_counselor"
            Me.XrTableCell_counselor.StylePriority.UseTextAlignment = False
            Me.XrTableCell_counselor.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrTableCell_counselor.Weight = 1.2499993896484374R
            '
            'XrTableRow2
            '
            Me.XrTableRow2.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.XrTableCell2, Me.XrTableCell_last_deposit_amount})
            Me.XrTableRow2.Name = "XrTableRow2"
            Me.XrTableRow2.Weight = 0.67999999999999994R
            '
            'XrTableCell2
            '
            Me.XrTableCell2.Name = "XrTableCell2"
            Me.XrTableCell2.StylePriority.UseTextAlignment = False
            Me.XrTableCell2.Text = "Last Deposit Amount"
            Me.XrTableCell2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            Me.XrTableCell2.Weight = 1.7500006103515626R
            '
            'XrTableCell_last_deposit_amount
            '
            Me.XrTableCell_last_deposit_amount.Name = "XrTableCell_last_deposit_amount"
            Me.XrTableCell_last_deposit_amount.StylePriority.UseTextAlignment = False
            Me.XrTableCell_last_deposit_amount.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrTableCell_last_deposit_amount.Weight = 1.2499993896484374R
            '
            'XrTableRow3
            '
            Me.XrTableRow3.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.XrTableCell5, Me.XrTableCell_last_deposit_date})
            Me.XrTableRow3.Name = "XrTableRow3"
            Me.XrTableRow3.Weight = 0.67999999999999994R
            '
            'XrTableCell5
            '
            Me.XrTableCell5.Name = "XrTableCell5"
            Me.XrTableCell5.StylePriority.UseTextAlignment = False
            Me.XrTableCell5.Text = "Last Deposit Date"
            Me.XrTableCell5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            Me.XrTableCell5.Weight = 1.7500006103515626R
            '
            'XrTableCell_last_deposit_date
            '
            Me.XrTableCell_last_deposit_date.Name = "XrTableCell_last_deposit_date"
            Me.XrTableCell_last_deposit_date.StylePriority.UseTextAlignment = False
            Me.XrTableCell_last_deposit_date.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrTableCell_last_deposit_date.Weight = 1.2499993896484374R
            '
            'XrTableRow4
            '
            Me.XrTableRow4.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.XrTableCell7, Me.XrTableCell_held_in_trust})
            Me.XrTableRow4.Name = "XrTableRow4"
            Me.XrTableRow4.Weight = 0.67999999999999994R
            '
            'XrTableCell7
            '
            Me.XrTableCell7.Name = "XrTableCell7"
            Me.XrTableCell7.StylePriority.UseTextAlignment = False
            Me.XrTableCell7.Text = "Available Escrow"
            Me.XrTableCell7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            Me.XrTableCell7.Weight = 1.7500006103515626R
            '
            'XrTableCell_held_in_trust
            '
            Me.XrTableCell_held_in_trust.Name = "XrTableCell_held_in_trust"
            Me.XrTableCell_held_in_trust.StylePriority.UseTextAlignment = False
            Me.XrTableCell_held_in_trust.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrTableCell_held_in_trust.Weight = 1.2499993896484374R
            '
            'ClientStatusReport
            '
            Me.Bands.AddRange(New DevExpress.XtraReports.UI.Band() {Me.Detail, Me.PageHeader, Me.PageFooter, Me.TopMarginBand1, Me.BottomMarginBand1, Me.ReportFooter})
            Me.Parameters.AddRange(New DevExpress.XtraReports.Parameters.Parameter() {Me.ParameterClient})
            Me.RequestParameters = False
            Me.ScriptLanguage = DevExpress.XtraReports.ScriptLanguage.VisualBasic
            Me.ScriptReferencesString = "C:\Users\Al Longyear\Documents\Visual Studio 2008\Projects\DebtPlus\Executables\D" & _
        "ebtPlus.Data.dll"
            Me.Scripts.OnBeforePrint = "ClientStatusReport_BeforePrint"
            Me.ScriptsSource = resources.GetString("$this.ScriptsSource")
            Me.StyleSheet.AddRange(New DevExpress.XtraReports.UI.XRControlStyle() {Me.XrControlStyle_Header, Me.XrControlStyle_Totals, Me.XrControlStyle_HeaderPannel, Me.XrControlStyle_GroupHeader})
            Me.Version = "11.2"
            Me.Controls.SetChildIndex(Me.ReportFooter, 0)
            Me.Controls.SetChildIndex(Me.BottomMarginBand1, 0)
            Me.Controls.SetChildIndex(Me.TopMarginBand1, 0)
            Me.Controls.SetChildIndex(Me.PageFooter, 0)
            Me.Controls.SetChildIndex(Me.PageHeader, 0)
            Me.Controls.SetChildIndex(Me.Detail, 0)
            CType(Me.XrTable_Trust, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me, System.ComponentModel.ISupportInitialize).EndInit()

        End Sub
        Friend WithEvents XrPanel_ClientNameAddress As DevExpress.XtraReports.UI.XRPanel
        Friend WithEvents XrBarCode1 As DevExpress.XtraReports.UI.XRBarCode
        Friend WithEvents XrLabel_NameAndAddress As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_creditor As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_est_interest As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_last_payment As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_dm_payment As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_starting_balance As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_acct As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_current_balance As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrPanel3 As DevExpress.XtraReports.UI.XRPanel
        Friend WithEvents XrLabel11 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel10 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel9 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel8 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel7 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel6 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel5 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents ReportFooter As DevExpress.XtraReports.UI.ReportFooterBand
        Friend WithEvents XrLabel23 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_total_last_payment As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_total_dm_payment As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_total_starting_balance As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_total_current_balance As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_total_est_interest As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents ParameterClient As DevExpress.XtraReports.Parameters.Parameter
        Friend WithEvents XrTable_Trust As DevExpress.XtraReports.UI.XRTable
        Friend WithEvents XrTableRow1 As DevExpress.XtraReports.UI.XRTableRow
        Friend WithEvents XrTableCell1 As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents XrTableCell_counselor As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents XrTableRow2 As DevExpress.XtraReports.UI.XRTableRow
        Friend WithEvents XrTableCell2 As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents XrTableCell_last_deposit_amount As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents XrTableRow3 As DevExpress.XtraReports.UI.XRTableRow
        Friend WithEvents XrTableCell5 As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents XrTableCell_last_deposit_date As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents XrTableRow4 As DevExpress.XtraReports.UI.XRTableRow
        Friend WithEvents XrTableCell7 As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents XrTableCell_held_in_trust As DevExpress.XtraReports.UI.XRTableCell
    End Class
End Namespace
