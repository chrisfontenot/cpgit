#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}
#End Region

Option Compare Binary
Option Explicit On 
Option Strict On

Namespace DailySummary.Transfers.OC
    Public Class DailySummaryTransfersOC

        ''' <summary>
        ''' Create an instance of our report
        ''' </summary>
        Public Sub New()
            MyBase.New()
            InitializeComponent()
            AddHandler BeforePrint, AddressOf DailySummaryTransfersOC_BeforePrint

            '-- Enable the select expert
            ReportFilter.IsEnabled = True
        End Sub

        ''' <summary>
        ''' Dispose of the items in the report
        ''' </summary>
        ''' <param name="disposing">TRUE if coming from dispose. False if from Finanalize.</param>
        ''' <remarks></remarks>
        Private Sub ReportDispose(ByVal disposing As Boolean)
            If disposing Then
                ds.Dispose()
            End If
        End Sub

        ''' <summary>
        ''' Title for the report
        ''' </summary>
        ''' <value></value>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Overrides ReadOnly Property ReportTitle() As String
            Get
                Return "Operating To Client Transfers"
            End Get
        End Property

        Private Shared ds As New System.Data.DataSet("ds")

        ''' <summary>
        ''' Handle the BEFORE_PRINT event for the report
        ''' </summary>
        ''' <param name="sender"></param>
        ''' <param name="e"></param>
        ''' <remarks></remarks>
        Private Sub DailySummaryTransfersOC_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs)
            Const TableName As String = "rpt_summary_oper_to_client"
            Dim rpt As DevExpress.XtraReports.UI.XtraReport = CType(sender, DevExpress.XtraReports.UI.XtraReport)
            Dim tbl As System.Data.DataTable = ds.Tables(TableName)

            If tbl Is Nothing Then
                Dim sqlInfo As DebtPlus.LINQ.SQLInfoClass = DebtPlus.LINQ.SQLInfoClass.getDefault()
                Dim cn As System.Data.SqlClient.SqlConnection = New System.Data.SqlClient.SqlConnection(sqlInfo.ConnectionString)

                '-- Read the transactions
                Dim current_cursor As System.Windows.Forms.Cursor = System.Windows.Forms.Cursor.Current
                Try
                    System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.WaitCursor
                    cn.Open()

                    Using cmd As New System.Data.SqlClient.SqlCommand()
                        With cmd
                            .Connection = cn
                            .CommandText = "rpt_summary_oper_to_client"
                            .CommandType = System.Data.CommandType.StoredProcedure

                            System.Data.SqlClient.SqlCommandBuilder.DeriveParameters(cmd)
                            .Parameters(1).Value = rpt.Parameters("ParameterFromDate").Value
                            .Parameters(2).Value = rpt.Parameters("ParameterToDate").Value
                            .CommandTimeout = 0
                        End With

                        Using da As New System.Data.SqlClient.SqlDataAdapter(cmd)
                            da.Fill(ds, TableName)
                            tbl = ds.Tables(TableName)
                        End Using
                    End Using

                Catch ex As System.Data.SqlClient.SqlException
                    Dim gdr As New DebtPlus.Repository.GetDataResult() : gdr.HandleException(ex) : DebtPlus.UI.Common.ErrorHandling.HandleErrors(gdr, "Error reading transactions")

                Finally
                    If cn IsNot Nothing Then
                        If cn.State <> System.Data.ConnectionState.Closed Then cn.Close()
                        cn.Dispose()
                    End If

                    System.Windows.Forms.Cursor.Current = current_cursor
                End Try
            End If

            '-- Bind the data to the report
            If tbl IsNot Nothing Then
                rpt.DataSource = New System.Data.DataView(tbl, CType(rpt, DebtPlus.Interfaces.Reports.IReportFilter).ReportFilter.ViewFilter, "item_date, dest_account, client", System.Data.DataViewRowState.CurrentRows)
                For Each calc As DevExpress.XtraReports.UI.CalculatedField In rpt.CalculatedFields
                    calc.Assign(rpt.DataSource, rpt.DataMember)
                Next
            End If
        End Sub
    End Class
End Namespace
