﻿Namespace Custom.FICO.Tracking
    <Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
    Partial Class FICOTrackingReport
        Inherits DebtPlus.Reports.Template.DatedTemplateXtraReportClass

        'XtraReport overrides dispose to clean up the component list.
        <System.Diagnostics.DebuggerNonUserCode()> _
        Protected Overrides Sub Dispose(ByVal disposing As Boolean)
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
            MyBase.Dispose(disposing)
        End Sub

        'Required by the Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Designer
        'It can be modified using the Designer.  
        'Do not modify it using the code editor.
        <System.Diagnostics.DebuggerStepThrough()> _
        Private Sub InitializeComponent()
            Dim XrSummary1 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary()
            Dim XrSummary2 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary()
            Dim XrSummary3 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary()
            Dim XrSummary4 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary()
            Dim XrSummary5 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary()
            Dim XrSummary6 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary()
            Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FICOTrackingReport))
            Me.pct_difference = New DevExpress.XtraReports.UI.CalculatedField()
            Me.XrPanel1 = New DevExpress.XtraReports.UI.XRPanel()
            Me.XrLabel9 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel8 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel7 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel6 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel5 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel4 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel3 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel1 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel2 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_pct_difference = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_name = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_ssn = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_fico_1_value = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_fico_2_value = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_fico_3_value = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_fico_4_value = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_fico_5_value = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_client = New DevExpress.XtraReports.UI.XRLabel()
            Me.ReportFooter = New DevExpress.XtraReports.UI.ReportFooterBand()
            Me.XrLabel16 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLine1 = New DevExpress.XtraReports.UI.XRLine()
            Me.XrLabel_total_fico_3_value = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_total_fico_2_value = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_total_pct_difference = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_total_fico_1_value = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_total_fico_5_value = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_total_fico_4_value = New DevExpress.XtraReports.UI.XRLabel()
            CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
            '
            'PageHeader
            '
            Me.PageHeader.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrPanel1})
            Me.PageHeader.HeightF = 140.0!
            Me.PageHeader.Controls.SetChildIndex(Me.XrPanel_AgencyAddress, 0)
            Me.PageHeader.Controls.SetChildIndex(Me.XrLabel_Title, 0)
            Me.PageHeader.Controls.SetChildIndex(Me.XrLine_Title, 0)
            Me.PageHeader.Controls.SetChildIndex(Me.XrLabel_Subtitle, 0)
            Me.PageHeader.Controls.SetChildIndex(Me.XrPageInfo1, 0)
            Me.PageHeader.Controls.SetChildIndex(Me.XrPanel1, 0)
            '
            'XrLabel_Title
            '
            Me.XrLabel_Title.StylePriority.UseFont = False
            '
            'XrPageInfo_PageNumber
            '
            Me.XrPageInfo_PageNumber.StylePriority.UseTextAlignment = False
            '
            'XRLabel_Agency_Name
            '
            Me.XRLabel_Agency_Name.StylePriority.UseTextAlignment = False
            '
            'XrLabel_Agency_Address3
            '
            Me.XrLabel_Agency_Address3.StylePriority.UseFont = False
            Me.XrLabel_Agency_Address3.StylePriority.UseTextAlignment = False
            '
            'XrLabel_Agency_Address1
            '
            Me.XrLabel_Agency_Address1.StylePriority.UseFont = False
            Me.XrLabel_Agency_Address1.StylePriority.UseTextAlignment = False
            '
            'XrLabel_Agency_Phone
            '
            Me.XrLabel_Agency_Phone.StylePriority.UseFont = False
            Me.XrLabel_Agency_Phone.StylePriority.UseTextAlignment = False
            '
            'XrLabel_Agency_Address2
            '
            Me.XrLabel_Agency_Address2.StylePriority.UseFont = False
            Me.XrLabel_Agency_Address2.StylePriority.UseTextAlignment = False
            '
            'Detail
            '
            Me.Detail.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel_fico_4_value, Me.XrLabel_fico_3_value, Me.XrLabel_client, Me.XrLabel_fico_5_value, Me.XrLabel_fico_2_value, Me.XrLabel_name, Me.XrLabel_pct_difference, Me.XrLabel_fico_1_value, Me.XrLabel_ssn})
            Me.Detail.HeightF = 15.0!
            '
            'pct_difference
            '
            Me.pct_difference.FieldType = DevExpress.XtraReports.UI.FieldType.[Double]
            Me.pct_difference.Name = "pct_difference"
            Me.pct_difference.Scripts.OnGetValue = "_1"
            '
            'XrPanel1
            '
            Me.XrPanel1.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel9, Me.XrLabel8, Me.XrLabel7, Me.XrLabel6, Me.XrLabel5, Me.XrLabel4, Me.XrLabel3, Me.XrLabel1, Me.XrLabel2})
            Me.XrPanel1.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 113.0!)
            Me.XrPanel1.Name = "XrPanel1"
            Me.XrPanel1.SizeF = New System.Drawing.SizeF(800.0!, 17.0!)
            Me.XrPanel1.StyleName = "XrControlStyle_HeaderPannel"
            '
            'XrLabel9
            '
            Me.XrLabel9.LocationFloat = New DevExpress.Utils.PointFloat(539.7083!, 0.0!)
            Me.XrLabel9.Name = "XrLabel9"
            Me.XrLabel9.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel9.SizeF = New System.Drawing.SizeF(28.0!, 15.0!)
            Me.XrLabel9.Text = "1"
            '
            'XrLabel8
            '
            Me.XrLabel8.LocationFloat = New DevExpress.Utils.PointFloat(580.2083!, 0.0!)
            Me.XrLabel8.Name = "XrLabel8"
            Me.XrLabel8.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel8.SizeF = New System.Drawing.SizeF(28.0!, 15.0!)
            Me.XrLabel8.Text = "2"
            '
            'XrLabel7
            '
            Me.XrLabel7.LocationFloat = New DevExpress.Utils.PointFloat(620.7083!, 0.0!)
            Me.XrLabel7.Name = "XrLabel7"
            Me.XrLabel7.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel7.SizeF = New System.Drawing.SizeF(28.0!, 15.0!)
            Me.XrLabel7.Text = "3"
            '
            'XrLabel6
            '
            Me.XrLabel6.LocationFloat = New DevExpress.Utils.PointFloat(661.2084!, 0.0!)
            Me.XrLabel6.Name = "XrLabel6"
            Me.XrLabel6.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel6.SizeF = New System.Drawing.SizeF(28.0!, 15.0!)
            Me.XrLabel6.Text = "4"
            '
            'XrLabel5
            '
            Me.XrLabel5.LocationFloat = New DevExpress.Utils.PointFloat(701.7084!, 0.0!)
            Me.XrLabel5.Name = "XrLabel5"
            Me.XrLabel5.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel5.SizeF = New System.Drawing.SizeF(28.0!, 15.0!)
            Me.XrLabel5.Text = "5"
            '
            'XrLabel4
            '
            Me.XrLabel4.LocationFloat = New DevExpress.Utils.PointFloat(742.2084!, 0.0!)
            Me.XrLabel4.Name = "XrLabel4"
            Me.XrLabel4.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel4.SizeF = New System.Drawing.SizeF(54.0!, 15.0!)
            Me.XrLabel4.Text = "% DIFF"
            '
            'XrLabel3
            '
            Me.XrLabel3.LocationFloat = New DevExpress.Utils.PointFloat(388.2083!, 0.0!)
            Me.XrLabel3.Name = "XrLabel3"
            Me.XrLabel3.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel3.SizeF = New System.Drawing.SizeF(139.0!, 15.0!)
            Me.XrLabel3.StylePriority.UseTextAlignment = False
            Me.XrLabel3.Text = "SSN"
            Me.XrLabel3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            '
            'XrLabel1
            '
            Me.XrLabel1.LocationFloat = New DevExpress.Utils.PointFloat(11.50001!, 0.0!)
            Me.XrLabel1.Name = "XrLabel1"
            Me.XrLabel1.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel1.SizeF = New System.Drawing.SizeF(66.0!, 15.0!)
            Me.XrLabel1.Text = "CLIENT"
            '
            'XrLabel2
            '
            Me.XrLabel2.LocationFloat = New DevExpress.Utils.PointFloat(86.49999!, 0.0!)
            Me.XrLabel2.Name = "XrLabel2"
            Me.XrLabel2.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel2.SizeF = New System.Drawing.SizeF(287.5!, 15.0!)
            Me.XrLabel2.StylePriority.UseTextAlignment = False
            Me.XrLabel2.Text = "NAME"
            Me.XrLabel2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            '
            'XrLabel_pct_difference
            '
            Me.XrLabel_pct_difference.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "pct_difference", "{0:p2}")})
            Me.XrLabel_pct_difference.LocationFloat = New DevExpress.Utils.PointFloat(729.7084!, 0.0!)
            Me.XrLabel_pct_difference.Name = "XrLabel_pct_difference"
            Me.XrLabel_pct_difference.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_pct_difference.SizeF = New System.Drawing.SizeF(66.5!, 14.99999!)
            Me.XrLabel_pct_difference.StylePriority.UseTextAlignment = False
            Me.XrLabel_pct_difference.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
            '
            'XrLabel_name
            '
            Me.XrLabel_name.LocationFloat = New DevExpress.Utils.PointFloat(86.49998!, 0.0!)
            Me.XrLabel_name.Multiline = True
            Me.XrLabel_name.Name = "XrLabel_name"
            Me.XrLabel_name.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_name.Scripts.OnBeforePrint = "XrLabel_name_BeforePrint"
            Me.XrLabel_name.SizeF = New System.Drawing.SizeF(301.7083!, 14.99999!)
            Me.XrLabel_name.StylePriority.UseTextAlignment = False
            Me.XrLabel_name.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            '
            'XrLabel_ssn
            '
            Me.XrLabel_ssn.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "ssn_1")})
            Me.XrLabel_ssn.LocationFloat = New DevExpress.Utils.PointFloat(388.2083!, 0.0!)
            Me.XrLabel_ssn.Name = "XrLabel_ssn"
            Me.XrLabel_ssn.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_ssn.SizeF = New System.Drawing.SizeF(139.0!, 15.0!)
            Me.XrLabel_ssn.StylePriority.UseTextAlignment = False
            Me.XrLabel_ssn.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            Me.XrLabel_ssn.WordWrap = False
            '
            'XrLabel_fico_1_value
            '
            Me.XrLabel_fico_1_value.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "fico_1_value", "{0:##0}")})
            Me.XrLabel_fico_1_value.LocationFloat = New DevExpress.Utils.PointFloat(527.2083!, 0.0!)
            Me.XrLabel_fico_1_value.Name = "XrLabel_fico_1_value"
            Me.XrLabel_fico_1_value.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_fico_1_value.SizeF = New System.Drawing.SizeF(40.5!, 14.99999!)
            Me.XrLabel_fico_1_value.StylePriority.UseTextAlignment = False
            Me.XrLabel_fico_1_value.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
            Me.XrLabel_fico_1_value.WordWrap = False
            '
            'XrLabel_fico_2_value
            '
            Me.XrLabel_fico_2_value.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "fico_2_value", "{0:##0}")})
            Me.XrLabel_fico_2_value.LocationFloat = New DevExpress.Utils.PointFloat(567.7083!, 0.0!)
            Me.XrLabel_fico_2_value.Name = "XrLabel_fico_2_value"
            Me.XrLabel_fico_2_value.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_fico_2_value.SizeF = New System.Drawing.SizeF(40.5!, 14.99999!)
            Me.XrLabel_fico_2_value.StylePriority.UseTextAlignment = False
            Me.XrLabel_fico_2_value.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
            Me.XrLabel_fico_2_value.WordWrap = False
            '
            'XrLabel_fico_3_value
            '
            Me.XrLabel_fico_3_value.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "fico_3_value", "{0:##0}")})
            Me.XrLabel_fico_3_value.LocationFloat = New DevExpress.Utils.PointFloat(608.2083!, 0.0!)
            Me.XrLabel_fico_3_value.Name = "XrLabel_fico_3_value"
            Me.XrLabel_fico_3_value.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_fico_3_value.SizeF = New System.Drawing.SizeF(40.50006!, 14.99999!)
            Me.XrLabel_fico_3_value.StylePriority.UseTextAlignment = False
            Me.XrLabel_fico_3_value.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
            Me.XrLabel_fico_3_value.WordWrap = False
            '
            'XrLabel_fico_4_value
            '
            Me.XrLabel_fico_4_value.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "fico_4_value", "{0:##0}")})
            Me.XrLabel_fico_4_value.LocationFloat = New DevExpress.Utils.PointFloat(648.7084!, 0.0!)
            Me.XrLabel_fico_4_value.Name = "XrLabel_fico_4_value"
            Me.XrLabel_fico_4_value.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_fico_4_value.SizeF = New System.Drawing.SizeF(40.5!, 14.99999!)
            Me.XrLabel_fico_4_value.StylePriority.UseTextAlignment = False
            Me.XrLabel_fico_4_value.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
            Me.XrLabel_fico_4_value.WordWrap = False
            '
            'XrLabel_fico_5_value
            '
            Me.XrLabel_fico_5_value.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "fico_5_value", "{0:##0}")})
            Me.XrLabel_fico_5_value.LocationFloat = New DevExpress.Utils.PointFloat(689.2084!, 0.0!)
            Me.XrLabel_fico_5_value.Name = "XrLabel_fico_5_value"
            Me.XrLabel_fico_5_value.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_fico_5_value.SizeF = New System.Drawing.SizeF(40.5!, 14.99999!)
            Me.XrLabel_fico_5_value.StylePriority.UseTextAlignment = False
            Me.XrLabel_fico_5_value.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
            Me.XrLabel_fico_5_value.WordWrap = False
            '
            'XrLabel_client
            '
            Me.XrLabel_client.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
            Me.XrLabel_client.Name = "XrLabel_client"
            Me.XrLabel_client.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_client.Scripts.OnBeforePrint = "XrLabel_client_BeforePrint"
            Me.XrLabel_client.SizeF = New System.Drawing.SizeF(78.5!, 15.0!)
            Me.XrLabel_client.StylePriority.UseTextAlignment = False
            Me.XrLabel_client.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
            Me.XrLabel_client.WordWrap = False
            '
            'ReportFooter
            '
            Me.ReportFooter.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel16, Me.XrLine1, Me.XrLabel_total_fico_3_value, Me.XrLabel_total_fico_2_value, Me.XrLabel_total_pct_difference, Me.XrLabel_total_fico_1_value, Me.XrLabel_total_fico_5_value, Me.XrLabel_total_fico_4_value})
            Me.ReportFooter.HeightF = 28.0!
            Me.ReportFooter.Name = "ReportFooter"
            Me.ReportFooter.StyleName = "XrControlStyle_Totals"
            '
            'XrLabel16
            '
            Me.XrLabel16.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 7.291667!)
            Me.XrLabel16.Name = "XrLabel16"
            Me.XrLabel16.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel16.SizeF = New System.Drawing.SizeF(100.0!, 15.0!)
            Me.XrLabel16.StylePriority.UseTextAlignment = False
            Me.XrLabel16.Text = "AVERAGE"
            Me.XrLabel16.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
            Me.XrLabel16.WordWrap = False
            '
            'XrLine1
            '
            Me.XrLine1.ForeColor = System.Drawing.Color.Teal
            Me.XrLine1.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 3.125!)
            Me.XrLine1.Name = "XrLine1"
            Me.XrLine1.SizeF = New System.Drawing.SizeF(800.0!, 4.166667!)
            Me.XrLine1.StylePriority.UseForeColor = False
            '
            'XrLabel_total_fico_3_value
            '
            Me.XrLabel_total_fico_3_value.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "fico_3_value")})
            Me.XrLabel_total_fico_3_value.LocationFloat = New DevExpress.Utils.PointFloat(608.2083!, 10.00001!)
            Me.XrLabel_total_fico_3_value.Name = "XrLabel_total_fico_3_value"
            Me.XrLabel_total_fico_3_value.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_total_fico_3_value.SizeF = New System.Drawing.SizeF(40.50006!, 14.99999!)
            Me.XrLabel_total_fico_3_value.StylePriority.UseTextAlignment = False
            XrSummary1.FormatString = "{0:##0}"
            XrSummary1.Func = DevExpress.XtraReports.UI.SummaryFunc.Avg
            XrSummary1.IgnoreNullValues = True
            XrSummary1.Running = DevExpress.XtraReports.UI.SummaryRunning.Report
            Me.XrLabel_total_fico_3_value.Summary = XrSummary1
            Me.XrLabel_total_fico_3_value.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
            Me.XrLabel_total_fico_3_value.WordWrap = False
            '
            'XrLabel_total_fico_2_value
            '
            Me.XrLabel_total_fico_2_value.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "fico_2_value")})
            Me.XrLabel_total_fico_2_value.LocationFloat = New DevExpress.Utils.PointFloat(567.7083!, 10.00001!)
            Me.XrLabel_total_fico_2_value.Name = "XrLabel_total_fico_2_value"
            Me.XrLabel_total_fico_2_value.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_total_fico_2_value.SizeF = New System.Drawing.SizeF(40.5!, 14.99999!)
            Me.XrLabel_total_fico_2_value.StylePriority.UseTextAlignment = False
            XrSummary2.FormatString = "{0:##0}"
            XrSummary2.Func = DevExpress.XtraReports.UI.SummaryFunc.Avg
            XrSummary2.IgnoreNullValues = True
            XrSummary2.Running = DevExpress.XtraReports.UI.SummaryRunning.Report
            Me.XrLabel_total_fico_2_value.Summary = XrSummary2
            Me.XrLabel_total_fico_2_value.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
            Me.XrLabel_total_fico_2_value.WordWrap = False
            '
            'XrLabel_total_pct_difference
            '
            Me.XrLabel_total_pct_difference.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "pct_difference")})
            Me.XrLabel_total_pct_difference.LocationFloat = New DevExpress.Utils.PointFloat(729.7084!, 10.00001!)
            Me.XrLabel_total_pct_difference.Name = "XrLabel_total_pct_difference"
            Me.XrLabel_total_pct_difference.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_total_pct_difference.SizeF = New System.Drawing.SizeF(66.5!, 14.99999!)
            Me.XrLabel_total_pct_difference.StylePriority.UseTextAlignment = False
            XrSummary3.FormatString = "{0:p2}"
            XrSummary3.Func = DevExpress.XtraReports.UI.SummaryFunc.Avg
            XrSummary3.IgnoreNullValues = True
            XrSummary3.Running = DevExpress.XtraReports.UI.SummaryRunning.Report
            Me.XrLabel_total_pct_difference.Summary = XrSummary3
            Me.XrLabel_total_pct_difference.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
            '
            'XrLabel_total_fico_1_value
            '
            Me.XrLabel_total_fico_1_value.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "fico_1_value")})
            Me.XrLabel_total_fico_1_value.LocationFloat = New DevExpress.Utils.PointFloat(527.2083!, 10.00001!)
            Me.XrLabel_total_fico_1_value.Name = "XrLabel_total_fico_1_value"
            Me.XrLabel_total_fico_1_value.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_total_fico_1_value.SizeF = New System.Drawing.SizeF(40.5!, 14.99999!)
            Me.XrLabel_total_fico_1_value.StylePriority.UseTextAlignment = False
            XrSummary4.FormatString = "{0:##0}"
            XrSummary4.Func = DevExpress.XtraReports.UI.SummaryFunc.Avg
            XrSummary4.IgnoreNullValues = True
            XrSummary4.Running = DevExpress.XtraReports.UI.SummaryRunning.Report
            Me.XrLabel_total_fico_1_value.Summary = XrSummary4
            Me.XrLabel_total_fico_1_value.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
            Me.XrLabel_total_fico_1_value.WordWrap = False
            '
            'XrLabel_total_fico_5_value
            '
            Me.XrLabel_total_fico_5_value.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "fico_5_value")})
            Me.XrLabel_total_fico_5_value.LocationFloat = New DevExpress.Utils.PointFloat(689.2084!, 10.00001!)
            Me.XrLabel_total_fico_5_value.Name = "XrLabel_total_fico_5_value"
            Me.XrLabel_total_fico_5_value.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_total_fico_5_value.SizeF = New System.Drawing.SizeF(40.5!, 14.99999!)
            Me.XrLabel_total_fico_5_value.StylePriority.UseTextAlignment = False
            XrSummary5.FormatString = "{0:##0}"
            XrSummary5.Func = DevExpress.XtraReports.UI.SummaryFunc.Avg
            XrSummary5.IgnoreNullValues = True
            XrSummary5.Running = DevExpress.XtraReports.UI.SummaryRunning.Report
            Me.XrLabel_total_fico_5_value.Summary = XrSummary5
            Me.XrLabel_total_fico_5_value.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
            Me.XrLabel_total_fico_5_value.WordWrap = False
            '
            'XrLabel_total_fico_4_value
            '
            Me.XrLabel_total_fico_4_value.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "fico_4_value")})
            Me.XrLabel_total_fico_4_value.LocationFloat = New DevExpress.Utils.PointFloat(648.7084!, 10.00001!)
            Me.XrLabel_total_fico_4_value.Name = "XrLabel_total_fico_4_value"
            Me.XrLabel_total_fico_4_value.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_total_fico_4_value.SizeF = New System.Drawing.SizeF(40.5!, 14.99999!)
            Me.XrLabel_total_fico_4_value.StylePriority.UseTextAlignment = False
            XrSummary6.FormatString = "{0:##0}"
            XrSummary6.Func = DevExpress.XtraReports.UI.SummaryFunc.Avg
            XrSummary6.IgnoreNullValues = True
            XrSummary6.Running = DevExpress.XtraReports.UI.SummaryRunning.Report
            Me.XrLabel_total_fico_4_value.Summary = XrSummary6
            Me.XrLabel_total_fico_4_value.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
            Me.XrLabel_total_fico_4_value.WordWrap = False
            '
            'FICOTrackingReport
            '
            Me.Bands.AddRange(New DevExpress.XtraReports.UI.Band() {Me.Detail, Me.PageHeader, Me.PageFooter, Me.TopMarginBand1, Me.BottomMarginBand1, Me.ReportFooter})
            Me.CalculatedFields.AddRange(New DevExpress.XtraReports.UI.CalculatedField() {Me.pct_difference})
            Me.ScriptLanguage = DevExpress.XtraReports.ScriptLanguage.VisualBasic
            Me.ScriptReferencesString = resources.GetString("$this.ScriptReferencesString")
            Me.Scripts.OnBeforePrint = "FICOTrackingReport_BeforePrint"
            Me.ScriptsSource = resources.GetString("$this.ScriptsSource")
            Me.StyleSheet.AddRange(New DevExpress.XtraReports.UI.XRControlStyle() {Me.XrControlStyle_Header, Me.XrControlStyle_Totals, Me.XrControlStyle_HeaderPannel, Me.XrControlStyle_GroupHeader})
            Me.Version = "11.2"
            Me.Controls.SetChildIndex(Me.ReportFooter, 0)
            Me.Controls.SetChildIndex(Me.BottomMarginBand1, 0)
            Me.Controls.SetChildIndex(Me.TopMarginBand1, 0)
            Me.Controls.SetChildIndex(Me.PageFooter, 0)
            Me.Controls.SetChildIndex(Me.PageHeader, 0)
            Me.Controls.SetChildIndex(Me.Detail, 0)
            CType(Me, System.ComponentModel.ISupportInitialize).EndInit()

        End Sub
        Protected Friend WithEvents pct_difference As DevExpress.XtraReports.UI.CalculatedField
        Friend WithEvents XrPanel1 As DevExpress.XtraReports.UI.XRPanel
        Friend WithEvents XrLabel9 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel8 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel7 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel6 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel5 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel4 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel3 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel1 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel2 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_fico_4_value As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_fico_3_value As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_client As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_fico_5_value As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_fico_2_value As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_name As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_pct_difference As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_fico_1_value As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_ssn As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents ReportFooter As DevExpress.XtraReports.UI.ReportFooterBand
        Friend WithEvents XrLabel_total_fico_3_value As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_total_fico_2_value As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_total_pct_difference As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_total_fico_1_value As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_total_fico_5_value As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_total_fico_4_value As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel16 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLine1 As DevExpress.XtraReports.UI.XRLine
    End Class
End Namespace
