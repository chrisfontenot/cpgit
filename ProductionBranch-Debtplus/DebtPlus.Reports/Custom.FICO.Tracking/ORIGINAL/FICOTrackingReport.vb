#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}
#End Region

Option Compare Binary
Option Explicit On 
Option Strict On

Imports DebtPlus.Reports
Imports DevExpress.XtraReports.UI

Namespace Custom.FICO.Tracking
    Public Class FICOTrackingReport

        ''' <summary>
        ''' Create an instance of our report
        ''' </summary>
        Public Sub New()
            MyBase.New()
            InitializeComponent()
            AddHandler Me.BeforePrint, AddressOf FICOTrackingReport_BeforePrint
            AddHandler pct_difference.GetValue, AddressOf pct_difference_GetValue
            AddHandler XrLabel_client.BeforePrint, AddressOf XrLabel_client_BeforePrint
            AddHandler XrLabel_name.BeforePrint, AddressOf XrLabel_name_BeforePrint
        End Sub

        Public Overrides ReadOnly Property ReportTitle() As String
            Get
                Return "FICO Tracking"
            End Get
        End Property

        Private ds As New System.Data.DataSet("ds")
        Private Sub FICOTrackingReport_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs)
            Const TableName As String = "rpt_custom_fico_tracking"

            Dim tbl As System.Data.DataTable
            Dim rpt As DevExpress.XtraReports.UI.XtraReport = CType(sender, DevExpress.XtraReports.UI.XtraReport)
            Dim sqlInfo As DebtPlus.LINQ.SQLInfoClass = DebtPlus.LINQ.SQLInfoClass.getDefault()
            Dim cn As System.Data.SqlClient.SqlConnection = New System.Data.SqlClient.SqlConnection(sqlInfo.ConnectionString)
            Try
                cn.Open()
                Using cmd As New System.Data.SqlClient.SqlCommand()
                    With cmd
                        .Connection = cn
                        .CommandText = "rpt_custom_fico_tracking"
                        .CommandType = System.Data.CommandType.StoredProcedure
                        System.Data.SqlClient.SqlCommandBuilder.DeriveParameters(cmd)

                        .Parameters(1).Value = rpt.Parameters("ParameterFromDate").Value
                        .Parameters(2).Value = rpt.Parameters("ParameterToDate").Value
                        .CommandTimeout = 0
                    End With

                    Using da As New System.Data.SqlClient.SqlDataAdapter(cmd)
                        da.Fill(ds, TableName)
                        tbl = ds.Tables(TableName)
                    End Using
                End Using

                If tbl IsNot Nothing Then
                    rpt.DataSource = tbl.DefaultView
                    For Each calc As DevExpress.XtraReports.UI.CalculatedField In rpt.CalculatedFields
                        calc.Assign(rpt.DataSource, rpt.DataMember)
                    Next
                End If

            Catch ex As System.Data.SqlClient.SqlException
                Dim gdr As New DebtPlus.Repository.GetDataResult() : gdr.HandleException(ex) : DebtPlus.UI.Common.ErrorHandling.HandleErrors(gdr, "Error reading report information")

            Finally
                If cn IsNot Nothing Then
                    If cn.State <> System.Data.ConnectionState.Closed Then cn.Close()
                    cn.Dispose()
                End If
            End Try
        End Sub

        Private Sub pct_difference_GetValue(ByVal sender As Object, ByVal e As DevExpress.XtraReports.UI.GetValueEventArgs)
            Dim row As System.Data.DataRow = CType(e.Row, System.Data.DataRowView).Row

            Dim value_1 As Double = 0.0!
            If row("fico_1_value") IsNot Nothing AndAlso row("fico_1_value") IsNot System.DBNull.Value Then value_1 = Convert.ToDouble(row("fico_1_value"), System.Globalization.CultureInfo.InvariantCulture)
            If value_1 < 0.0! Then
                value_1 = 0.0!
            End If

            Dim difference As Double = 0.0!
            If row("fico_difference") IsNot Nothing AndAlso row("fico_difference") IsNot System.DBNull.Value Then difference = Convert.ToDouble(row("fico_difference"), System.Globalization.CultureInfo.InvariantCulture)

            Dim Answer As Object = System.DBNull.Value
            If value_1 > 0.0! Then
                Try
                    Dim Pct As Double = difference / value_1
                    If Pct <> 0.0! Then
                        Answer = Pct
                    End If
                Catch ex As Exception
                End Try
            End If
            e.Value = Answer
        End Sub

        Private Sub XrLabel_client_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs)
            With CType(sender, DevExpress.XtraReports.UI.XRLabel)
                Dim rpt As DevExpress.XtraReports.UI.XtraReport = CType(.Report, DevExpress.XtraReports.UI.XtraReport)
                .Text = DebtPlus.Utils.Format.Client.FormatClientID(rpt.GetCurrentColumnValue("client"))
            End With
        End Sub

        Private Sub XrLabel_name_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs)
            With CType(sender, DevExpress.XtraReports.UI.XRLabel)
                Dim rpt As DevExpress.XtraReports.UI.XtraReport = CType(.Report, DevExpress.XtraReports.UI.XtraReport)
                Dim sb As New System.Text.StringBuilder
                For Each fld As String In New String() {"client_name", "addr1", "addr2", "addr3"}
                    Dim v As Object = CType(rpt.GetCurrentRow, System.Data.DataRowView)(fld)
                    If v IsNot Nothing AndAlso v IsNot System.DBNull.Value Then
                        sb.Append(System.Environment.NewLine)
                        sb.Append(Convert.ToString(v, System.Globalization.CultureInfo.InvariantCulture))
                    End If
                Next
                If sb.Length > 0 Then
                    sb.Remove(0, 2)
                End If
                .Text = sb.ToString
            End With
        End Sub
    End Class
End Namespace
