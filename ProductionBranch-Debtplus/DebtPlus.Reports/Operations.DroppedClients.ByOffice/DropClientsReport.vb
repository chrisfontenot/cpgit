#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}

#End Region

Option Compare Binary
Option Explicit On
Option Strict On

Imports System.ComponentModel
Imports DebtPlus.Reports.Template

Imports System.Drawing.Printing
Imports DevExpress.XtraReports.UI
Imports DevExpress.LookAndFeel
Imports DebtPlus.Reports.Template.Forms
Imports System.Data.SqlClient
Imports System.Threading

Namespace Operations.DroppedClients.ByOffice
    Public Class DroppedClientsReportClass
        Inherits DatedTemplateXtraReportClass

        Public Overrides Function RequestReportParameters() As DialogResult
            Dim answer As DialogResult = DialogResult.OK
            If NeedParameters() Then
                Dim frm As New DateReportParametersForm
                With frm
                    answer = .ShowDialog()
                    Parameter_FromDate = .Parameter_FromDate
                    Parameter_ToDate = .Parameter_ToDate
                    .Dispose()
                End With
            End If
            Return answer
        End Function

        Public Overrides ReadOnly Property ReportTitle() As String
            Get
                Return "Dropped Clients"
            End Get
        End Property


        ''' <summary>
        ''' Create an instance of our report
        ''' </summary>
        Public Sub New()
            MyBase.New()
            InitializeComponent()
            AddHandler Me.BeforePrint, AddressOf MyBase_BeforePrint
            AddHandler XrLabel_drop_reason.PreviewClick, AddressOf XrLabel_drop_reason_PreviewClick
            AddHandler XrLabel_office.PreviewClick, AddressOf XrLabel_drop_reason_PreviewClick
            AddHandler XrLabel_count.PreviewClick, AddressOf XrLabel_drop_reason_PreviewClick
        End Sub

#Region " Component Designer generated code "

        'Component overrides dispose to clean up the component list.
        Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
            If disposing Then
                If components IsNot Nothing Then
                    components.Dispose()
                End If
            End If
            MyBase.Dispose(disposing)
        End Sub

        Protected Friend WithEvents XrLabel_drop_reason As DevExpress.XtraReports.UI.XRLabel
        Protected Friend WithEvents GroupHeader1 As DevExpress.XtraReports.UI.GroupHeaderBand
        Protected Friend WithEvents XrLabel_office As DevExpress.XtraReports.UI.XRLabel
        Protected Friend WithEvents XrLabel_count As DevExpress.XtraReports.UI.XRLabel
        Protected Friend WithEvents GroupFooter1 As DevExpress.XtraReports.UI.GroupFooterBand
        Protected Friend WithEvents XrPanel1 As DevExpress.XtraReports.UI.XRPanel
        Protected Friend WithEvents XrLabel5 As DevExpress.XtraReports.UI.XRLabel
        Protected Friend WithEvents XrLabel1 As DevExpress.XtraReports.UI.XRLabel
        Protected Friend WithEvents XrLabel3 As DevExpress.XtraReports.UI.XRLabel
        Protected Friend WithEvents GroupFooter2 As DevExpress.XtraReports.UI.GroupFooterBand
        Protected Friend WithEvents XrLabel_total_count As DevExpress.XtraReports.UI.XRLabel
        Protected Friend WithEvents GroupHeader2 As DevExpress.XtraReports.UI.GroupHeaderBand
        Protected Friend WithEvents XrLabel4 As DevExpress.XtraReports.UI.XRLabel

        'Required by the Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Designer
        'It can be modified using the Designer.
        'Do not modify it using the code editor.
        <System.Diagnostics.DebuggerStepThrough()>
        Private Sub InitializeComponent()
            Me.GroupHeader1 = New DevExpress.XtraReports.UI.GroupHeaderBand
            Me.XrLabel_drop_reason = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_office = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_count = New DevExpress.XtraReports.UI.XRLabel
            Me.GroupFooter1 = New DevExpress.XtraReports.UI.GroupFooterBand
            Me.XrPanel1 = New DevExpress.XtraReports.UI.XRPanel
            Me.XrLabel5 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel1 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel3 = New DevExpress.XtraReports.UI.XRLabel
            Me.GroupFooter2 = New DevExpress.XtraReports.UI.GroupFooterBand
            Me.XrLabel_total_count = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel4 = New DevExpress.XtraReports.UI.XRLabel
            Me.GroupHeader2 = New DevExpress.XtraReports.UI.GroupHeaderBand
            CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
            '
            'Detail
            '
            Me.Detail.Height = 0
            '
            'PageHeader
            '
            Me.PageHeader.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrPanel1})
            Me.PageHeader.Height = 154
            Me.PageHeader.Controls.SetChildIndex(Me.XrPanel1, 0)
            Me.PageHeader.Controls.SetChildIndex(Me.XrPanel_AgencyAddress, 0)
            Me.PageHeader.Controls.SetChildIndex(Me.XrLabel_Title, 0)
            Me.PageHeader.Controls.SetChildIndex(Me.XrLine_Title, 0)
            Me.PageHeader.Controls.SetChildIndex(Me.XrLabel_Subtitle, 0)
            Me.PageHeader.Controls.SetChildIndex(Me.XrPageInfo1, 0)
            '
            'GroupHeader1
            '
            Me.GroupHeader1.Height = 0
            Me.GroupHeader1.Name = "GroupHeader1"
            '
            'XrLabel_drop_reason
            '
            Me.XrLabel_drop_reason.Location = New System.Drawing.Point(10, 0)
            Me.XrLabel_drop_reason.Name = "XrLabel_drop_reason"
            Me.XrLabel_drop_reason.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_drop_reason.ProcessDuplicates = DevExpress.XtraReports.UI.ValueSuppressType.Suppress
            Me.XrLabel_drop_reason.Size = New System.Drawing.Size(73, 15)
            '
            'XrLabel_office
            '
            Me.XrLabel_office.Location = New System.Drawing.Point(91, 0)
            Me.XrLabel_office.Name = "XrLabel_office"
            Me.XrLabel_office.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_office.Size = New System.Drawing.Size(400, 15)
            '
            'XrLabel_count
            '
            Me.XrLabel_count.Location = New System.Drawing.Point(641, 0)
            Me.XrLabel_count.Name = "XrLabel_count"
            Me.XrLabel_count.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_count.Size = New System.Drawing.Size(100, 15)
            Me.XrLabel_count.StylePriority.UseTextAlignment = False
            Me.XrLabel_count.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
            '
            'GroupFooter1
            '
            Me.GroupFooter1.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel_drop_reason, Me.XrLabel_office, Me.XrLabel_count})
            Me.GroupFooter1.Height = 15
            Me.GroupFooter1.Name = "GroupFooter1"
            '
            'XrPanel1
            '
            Me.XrPanel1.BackColor = System.Drawing.Color.Teal
            Me.XrPanel1.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel5, Me.XrLabel1, Me.XrLabel3})
            Me.XrPanel1.Location = New System.Drawing.Point(8, 133)
            Me.XrPanel1.Name = "XrPanel1"
            Me.XrPanel1.Size = New System.Drawing.Size(742, 17)
            '
            'XrLabel5
            '
            Me.XrLabel5.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.XrLabel5.ForeColor = System.Drawing.Color.White
            Me.XrLabel5.Location = New System.Drawing.Point(667, 1)
            Me.XrLabel5.Name = "XrLabel5"
            Me.XrLabel5.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel5.Size = New System.Drawing.Size(66, 15)
            Me.XrLabel5.StylePriority.UseTextAlignment = False
            Me.XrLabel5.Text = "Count"
            Me.XrLabel5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel1
            '
            Me.XrLabel1.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.XrLabel1.ForeColor = System.Drawing.Color.White
            Me.XrLabel1.Location = New System.Drawing.Point(83, 1)
            Me.XrLabel1.Name = "XrLabel1"
            Me.XrLabel1.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel1.Size = New System.Drawing.Size(166, 15)
            Me.XrLabel1.StylePriority.UseTextAlignment = False
            Me.XrLabel1.Text = "Office Location"
            Me.XrLabel1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            '
            'XrLabel3
            '
            Me.XrLabel3.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.XrLabel3.ForeColor = System.Drawing.Color.White
            Me.XrLabel3.Location = New System.Drawing.Point(1, 1)
            Me.XrLabel3.Name = "XrLabel3"
            Me.XrLabel3.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel3.Size = New System.Drawing.Size(73, 15)
            Me.XrLabel3.StylePriority.UseTextAlignment = False
            Me.XrLabel3.Text = "Reason"
            Me.XrLabel3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            '
            'GroupFooter2
            '
            Me.GroupFooter2.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel_total_count, Me.XrLabel4})
            Me.GroupFooter2.Height = 34
            Me.GroupFooter2.Level = 1
            Me.GroupFooter2.Name = "GroupFooter2"
            '
            'XrLabel_total_count
            '
            Me.XrLabel_total_count.Borders = DevExpress.XtraPrinting.BorderSide.Top
            Me.XrLabel_total_count.BorderWidth = 2
            Me.XrLabel_total_count.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel_total_count.Location = New System.Drawing.Point(641, 0)
            Me.XrLabel_total_count.Name = "XrLabel_total_count"
            Me.XrLabel_total_count.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_total_count.Size = New System.Drawing.Size(100, 17)
            Me.XrLabel_total_count.StylePriority.UseBorders = False
            Me.XrLabel_total_count.StylePriority.UseBorderWidth = False
            Me.XrLabel_total_count.StylePriority.UseFont = False
            Me.XrLabel_total_count.StylePriority.UseTextAlignment = False
            Me.XrLabel_total_count.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight
            '
            'XrLabel4
            '
            Me.XrLabel4.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel4.Location = New System.Drawing.Point(91, 2)
            Me.XrLabel4.Name = "XrLabel4"
            Me.XrLabel4.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel4.Size = New System.Drawing.Size(400, 15)
            Me.XrLabel4.StylePriority.UseFont = False
            Me.XrLabel4.StylePriority.UseTextAlignment = False
            Me.XrLabel4.Text = "DROP REASON TYPE TOTALS"
            Me.XrLabel4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft
            '
            'GroupHeader2
            '
            Me.GroupHeader2.Height = 0
            Me.GroupHeader2.Level = 1
            Me.GroupHeader2.Name = "GroupHeader2"
            '
            'DroppedClientsReportClass
            '
            Me.Bands.AddRange(New DevExpress.XtraReports.UI.Band() {Me.Detail, Me.PageHeader, Me.PageFooter, Me.GroupHeader1, Me.GroupFooter1, Me.GroupFooter2, Me.GroupHeader2})
            Me.Version = "9.2"
            Me.Controls.SetChildIndex(Me.GroupHeader2, 0)
            Me.Controls.SetChildIndex(Me.GroupFooter2, 0)
            Me.Controls.SetChildIndex(Me.GroupFooter1, 0)
            Me.Controls.SetChildIndex(Me.GroupHeader1, 0)
            Me.Controls.SetChildIndex(Me.PageFooter, 0)
            Me.Controls.SetChildIndex(Me.PageHeader, 0)
            Me.Controls.SetChildIndex(Me.Detail, 0)
            CType(Me, System.ComponentModel.ISupportInitialize).EndInit()
        End Sub

#End Region

        Dim ds As New DataSet("ds")

        Protected Sub MyBase_BeforePrint(ByVal sender As Object, ByVal e As PrintEventArgs)
            Try
                Using cmd As SqlCommand = New System.Data.SqlClient.SqlCommand()
                    With cmd
                        .Connection = New System.Data.SqlClient.SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
                        .CommandType = CommandType.StoredProcedure
                        .CommandText = "rpt_Dropped_Clients_By_Office"
                        .CommandTimeout = 0
                        .Parameters.Add("@fromdate", SqlDbType.DateTime).Value = Parameter_FromDate
                        .Parameters.Add("@todate", SqlDbType.DateTime).Value = Parameter_ToDate
                    End With

                    Using da As New SqlDataAdapter(cmd)
                        da.Fill(ds, "rpt_dropped_clients_by_office")
                    End Using
                End Using

                Dim ChildTbl As DataTable = ds.Tables("rpt_dropped_clients_by_office")
                With ChildTbl
                    .PrimaryKey = New DataColumn() {.Columns("client")}
                End With

                ' The view for the report
                Dim vue As New DataView(ChildTbl, String.Empty, "drop_reason, office", DataViewRowState.CurrentRows)
                DataSource = vue

                ' Add the grouping information to the report
                GroupHeader2.GroupFields.AddRange(New GroupField() {New GroupField("drop_reason", XRColumnSortOrder.Ascending)})
                GroupHeader1.GroupFields.AddRange(New GroupField() {New GroupField("drop_reason", XRColumnSortOrder.Ascending), New GroupField("office", XRColumnSortOrder.Ascending)})

                With XrLabel_drop_reason
                    .DataBindings.Clear()
                    .DataBindings.AddRange(New XRBinding() {New XRBinding("Text", vue, "drop_reason"), New XRBinding("Tag", vue, "client")})
                End With

                With XrLabel_office
                    .DataBindings.Clear()
                    .DataBindings.AddRange(New XRBinding() {New XRBinding("Text", vue, "office"), New XRBinding("Tag", vue, "client")})
                End With

                With XrLabel_count
                    .DataBindings.Clear()
                    .DataBindings.AddRange(New XRBinding() {New XRBinding("Text", vue, "count"), New XRBinding("Tag", vue, "client")})
                    Dim XrSummary1 As New XRSummary(SummaryRunning.Group, SummaryFunc.Count, "{0:n0}")
                    .Summary = XrSummary1
                End With

                With XrLabel_total_count
                    .DataBindings.Clear()
                    .DataBindings.AddRange(New XRBinding() {New XRBinding("Text", vue, "count"), New XRBinding("Tag", vue, "client")})
                    Dim XrSummary2 As New XRSummary(SummaryRunning.Group, SummaryFunc.Count, "{0:n0}")
                    .Summary = XrSummary2
                End With

            Catch ex As Exception
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error reading information for report")
            End Try
        End Sub

        Protected Sub XrLabel_drop_reason_PreviewClick(ByVal sender As Object, ByVal e As PreviewMouseEventArgs)
            Dim ClientObj As Object = e.Brick.Value
            Dim ChildTbl As DataTable = ds.Tables("rpt_dropped_clients_by_office")
            Dim row As DataRow = ChildTbl.Rows.Find(ClientObj)

            If row IsNot Nothing Then
                Dim DropReason As String = Convert.ToString(row("drop_reason"))
                Dim Office As String = Convert.ToString(row("office"))
                Dim NewView As New DataView(ChildTbl, String.Format("[drop_reason]='{0}' AND [office]='{1}'", DropReason, Office), "client", DataViewRowState.CurrentRows)
                Dim NewSubtitle As String = String.Format("{0} for drop reason '{1}' and office '{2}'", ReportSubtitle, DropReason, Office)

                With New SubReportThread(New DefaultLookAndFeel().LookAndFeel, NewView, NewSubtitle)
                    Dim thrd As New Thread(AddressOf .ShowReport)
                    With thrd
                        .IsBackground = True
                        .SetApartmentState(ApartmentState.STA)
                        .Name = "Subreport"
                        .Start()
                    End With
                End With
            End If
        End Sub

        Protected Class SubReportThread
            Protected privateVue As DataView = Nothing
            Protected privateSubtitle As String = String.Empty
            Protected LookAndFeel As UserLookAndFeel = Nothing

            Public Sub New(ByVal LookAndFeel As UserLookAndFeel, ByVal vue As DataView, ByVal subtitle As String)
                Me.privateSubtitle = subtitle
                Me.privateVue = vue
            End Sub

            Public Sub ShowReport()
                Using rpt As New DroppedClientsListSubreport(privateVue, privateSubtitle)
                    With rpt
                        .CreateDocument(True)
                        .DisplayPreviewDialog(LookAndFeel)
                    End With
                End Using
            End Sub
        End Class
    End Class
End Namespace
