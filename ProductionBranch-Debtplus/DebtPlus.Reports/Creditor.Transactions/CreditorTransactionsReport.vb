#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}

#End Region

Imports DebtPlus.Interfaces.Creditor
Imports DebtPlus.Reports.Template
Imports System.IO
Imports System.Reflection
Imports DebtPlus.Utils
Imports DevExpress.XtraReports.UI
Imports DebtPlus.Reports.Template.Forms

Namespace Creditor.Transactions
    Public Class CreditorTransactionsReport
        Inherits DatedTemplateXtraReportClass
        Implements ICreditor

        Public Sub New()
            MyClass.New(DateRange.Days_90)
        End Sub

        Public Sub New(ByVal DateRange As DateRange)
            MyBase.New(DateRange)
            InitializeComponent()

            ' Set the script references as needed
            ScriptReferences = DebtPlus.Utils.AssemblyTools.GetAllSiblingAssemblies()                                                        ' changed   

            ' Copy the script references to the subreports as well
            For Each Band As Band In Bands
                For Each ctl As XRControl In Band.Controls
                    Dim rpt As XRSubreport = TryCast(ctl, XRSubreport)
                    If rpt IsNot Nothing Then
                        Dim SubRpt As XtraReport = rpt.ReportSource
                        If SubRpt IsNot Nothing Then
                            SubRpt.ScriptReferences = ScriptReferences
                        End If
                    End If
                Next
            Next

            ' Remove the parameter values
            Parameters("ParameterCreditor").Value = String.Empty
        End Sub

        Private Sub InitializeComponent()
            Const ReportName As String = "DebtPlus.Reports.Creditor.Transactions.repx"

            ' See if there is a report reference that we can use
            Dim Fname As String = Path.Combine(DebtPlus.Configuration.Config.ReportsDirectory, ReportName)
            Dim UseDefault As Boolean = True
            Try
                If (Not DebtPlus.Configuration.Config.LocalReportsOnly) AndAlso System.IO.File.Exists(Fname) Then
                    LoadLayout(Fname)
                    UseDefault = False
                End If
            Catch ex As Exception
            End Try

            ' Load the standard report definition if we need a new item
            If UseDefault Then
                Dim asm As Assembly = Assembly.GetExecutingAssembly                                           ' changed
                Using ios As Stream = asm.GetManifestResourceStream(String.Format("DebtPlus.Reports.{0}", ReportName)) ' changed
                    If ios IsNot Nothing Then LoadLayout(ios) '                                                                       changed
                End Using
            End If
        End Sub

        Protected DateRange As DateRange

        ''' <summary>
        '''     Report title information
        ''' </summary>
        Public Overrides ReadOnly Property ReportTitle() As String
            Get
                Return "Creditor Transactions"
            End Get
        End Property

        ''' <summary>
        '''     Parameter for the creditor ID
        ''' </summary>
        Public Property Parameter_Creditor() As String Implements ICreditor.Creditor
            Get
                Return CType(Parameters("ParameterCreditor").Value, String)
            End Get
            Set(ByVal Value As String)
                Parameters("ParameterCreditor").Value = Value
            End Set
        End Property

        ''' <summary>
        '''     Do we need to request parameters?
        ''' </summary>
        Public Overrides Function NeedParameters() As Boolean
            Return MyBase.NeedParameters() OrElse Parameter_Creditor = String.Empty
        End Function

        ''' <summary>
        '''     Request the parameters for the report from the user
        ''' </summary>
        Public Overrides Function RequestReportParameters() As DialogResult
            Dim answer As DialogResult = DialogResult.OK
            If NeedParameters() Then
                Using frm As New DatedCreditorParametersForm(DateRange)
                    With frm
                        .Parameter_Creditor = Parameter_Creditor
                        answer = .ShowDialog()
                        If answer = DialogResult.OK Then
                            Parameter_FromDate = .Parameter_FromDate
                            Parameter_ToDate = .Parameter_ToDate
                            Parameter_Creditor = .Parameter_Creditor
                        End If
                    End With
                End Using
            End If
            Return answer
        End Function
    End Class
End Namespace
