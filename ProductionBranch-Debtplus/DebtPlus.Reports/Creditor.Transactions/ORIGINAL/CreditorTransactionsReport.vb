#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}
#End Region

Namespace Creditor.Transactions
    Public Class CreditorTransactionsReport
        Implements DebtPlus.Interfaces.Creditor.ICreditor

        ''' <summary>
        ''' Create an instance of our report
        ''' </summary>
        Public Sub New()
            MyClass.New(DebtPlus.Utils.DateRange.Today)
        End Sub

        Public Sub New(ByVal DateRange As DebtPlus.Utils.DateRange)
            MyBase.New(DateRange)
            InitializeComponent()

            Creditor = String.Empty

            AddHandler BeforePrint, AddressOf CreditorTransactionsReport_BeforePrint
            AddHandler XrBarCode_PostalCode.BeforePrint, AddressOf XrBarCode_PostalCode_BeforePrint
            AddHandler XrLabel_Creditor_Address.BeforePrint, AddressOf XrLabel_Creditor_Address_BeforePrint
        End Sub

        Public Property Parameter_Creditor As String
            Get
                Dim parm As DevExpress.XtraReports.Parameters.Parameter = FindParameter("ParameterCreditor")
                If parm Is Nothing Then Return String.Empty
                Return Convert.ToString(parm.Value)
            End Get
            Set(value As String)
                SetParameter("ParameterCreditor", GetType(String), value, "Creditor ID", False)
            End Set
        End Property

        Public Property Creditor As String Implements DebtPlus.Interfaces.Creditor.ICreditor.Creditor
            Get
                Return Parameter_Creditor
            End Get
            Set(value As String)
                Parameter_Creditor = value
            End Set
        End Property

        Public Overrides ReadOnly Property ReportTitle() As String
            Get
                Return "Creditor Transactions"
            End Get
        End Property

        Public Overrides Sub SetReportParameter(ByVal Parameter As String, ByVal Value As Object)
            If Parameter = "Creditor" Then
                Parameter_Creditor = Convert.ToString(Value)
            Else
                MyBase.SetReportParameter(Parameter, Value)
            End If
        End Sub

        Public Overrides Function NeedParameters() As Boolean
            Return MyBase.NeedParameters OrElse Parameter_Creditor = String.Empty
        End Function

        Public Overrides Function RequestReportParameters() As System.Windows.Forms.DialogResult
            Dim Answer As System.Windows.Forms.DialogResult = DialogResult.OK

            If NeedParameters() Then
                Using frm As New DebtPlus.Reports.Template.Forms.DatedCreditorParametersForm(ReportDateRange)
                    frm.Parameter_Creditor = Parameter_Creditor
                    Answer = frm.ShowDialog()
                    Parameter_FromDate = frm.Parameter_FromDate
                    Parameter_ToDate = frm.Parameter_ToDate
                    Parameter_Creditor = frm.Parameter_Creditor
                End Using
            End If

            Return Answer
        End Function

        Dim ds As New System.Data.DataSet("ds")

        Private Sub CreditorTransactionsReport_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs)
            Const TableName As String = "rpt_Transactions_CR"
            Dim rpt As DevExpress.XtraReports.UI.XtraReport = CType(sender, DevExpress.XtraReports.UI.XtraReport)

            Dim tbl As System.Data.DataTable = ds.Tables(TableName)
            If tbl Is Nothing Then
                Dim sqlInfo As DebtPlus.LINQ.SQLInfoClass = DebtPlus.LINQ.SQLInfoClass.getDefault()

                Try
                    Using cn As System.Data.SqlClient.SqlConnection = New System.Data.SqlClient.SqlConnection(sqlInfo.ConnectionString)
                        cn.Open()

                        '-- Read the transactions from the system
                        Using cmd As New System.Data.SqlClient.SqlCommand()
                            cmd.Connection = cn
                            cmd.CommandText = "rpt_Transactions_CR"
                            cmd.CommandType = System.Data.CommandType.StoredProcedure
                            cmd.Parameters.Add("@Creditor", System.Data.SqlDbType.VarChar, 10).Value = rpt.Parameters("ParameterCreditor").Value
                            cmd.Parameters.Add("@FromDate", System.Data.SqlDbType.DateTime).Value = rpt.Parameters("ParameterFromDate").Value
                            cmd.Parameters.Add("@ToDate", System.Data.SqlDbType.DateTime).Value = rpt.Parameters("ParameterToDate").Value
                            cmd.CommandTimeout = 0

                            Using da As New System.Data.SqlClient.SqlDataAdapter(cmd)
                                da.Fill(ds, TableName)
                                tbl = ds.Tables(TableName)
                                rpt.DataSource = New System.Data.DataView(tbl, String.Empty, "item_date, creditor_register", System.Data.DataViewRowState.CurrentRows)
                            End Using
                        End Using
                    End Using

                Catch ex As System.Data.SqlClient.SqlException
                    Using gdr As New DebtPlus.Repository.GetDataResult()
                        gdr.HandleException(ex)
                        DebtPlus.UI.Common.ErrorHandling.HandleErrors(gdr, "Error reading creditor transactions")
                    End Using
                End Try
            End If
        End Sub

        Private Function CreditorRow(ByVal rpt As DevExpress.XtraReports.UI.XtraReportBase) As System.Data.DataRow
            Const TableName As String = "rpt_CreditorAddress_P"
            Dim sqlInfo As DebtPlus.LINQ.SQLInfoClass = DebtPlus.LINQ.SQLInfoClass.getDefault()
            Dim ds As System.Data.DataSet = CType(CType(rpt, DevExpress.XtraReports.UI.XtraReport).DataSource, System.Data.DataView).Table.DataSet
            Dim tbl As System.Data.DataTable = ds.Tables(TableName)
            Dim drv As System.Data.DataRowView = TryCast(rpt.GetCurrentRow, System.Data.DataRowView)

            Dim Creditor As String = TryCast(CType(rpt, DevExpress.XtraReports.UI.XtraReport).Parameters("ParameterCreditor").Value, String)
            If tbl IsNot Nothing Then
                Dim row As System.Data.DataRow = tbl.Rows.Find(Creditor)
                If row IsNot Nothing Then Return row
            End If

            Try
                Using cn As System.Data.SqlClient.SqlConnection = New System.Data.SqlClient.SqlConnection(sqlInfo.ConnectionString)
                    cn.Open()
                    Using cmd As New System.Data.SqlClient.SqlCommand()
                        cmd.Connection = cn
                        cmd.CommandText = "rpt_CreditorAddress_P"
                        cmd.CommandType = System.Data.CommandType.StoredProcedure
                        cmd.Parameters.Add("@creditor", System.Data.SqlDbType.VarChar, 80).Value = Creditor

                        Using da As New System.Data.SqlClient.SqlDataAdapter(cmd)
                            da.Fill(ds, TableName)
                            tbl = ds.Tables(TableName)
                            If tbl.PrimaryKey.GetUpperBound(0) <= 0 Then
                                tbl.PrimaryKey = New System.Data.DataColumn() {tbl.Columns("creditor")}
                            End If

                            Return tbl.Rows.Find(Creditor)
                        End Using
                    End Using
                End Using

            Catch ex As Exception
                Using gdr As New DebtPlus.Repository.GetDataResult()
                    gdr.HandleException(ex)
                    DebtPlus.UI.Common.ErrorHandling.HandleErrors(gdr, "Error reading creditor address information")
                End Using
            End Try

            Return Nothing
        End Function

        Private Sub XrLabel_Creditor_Address_BeforePrint(sender As Object, e As System.Drawing.Printing.PrintEventArgs)
            Dim Text_Block As New System.Text.StringBuilder
            Dim lbl As DevExpress.XtraReports.UI.XRLabel = CType(sender, DevExpress.XtraReports.UI.XRLabel)
            Dim row As System.Data.DataRow = CreditorRow(lbl.Report)
            If row IsNot Nothing Then
                For Each NameString As String In New String() {"addr1", "addr2", "addr3", "addr4", "addr5", "addr6", "addr7"}
                    Dim col As System.Data.DataColumn = row.Table.Columns.Item(NameString)
                    Dim Value As String = DebtPlus.Utils.Nulls.DStr(row(col)).Trim
                    If Value <> System.String.Empty Then
                        Text_Block.Append(Environment.NewLine)
                        Text_Block.Append(Value)
                    End If
                Next NameString
            End If

            If Text_Block.Length > 0 Then Text_Block.Remove(0, 2)
            lbl.Text = Text_Block.ToString()
        End Sub

        Private Sub XrBarCode_PostalCode_BeforePrint(sender As Object, e As System.Drawing.Printing.PrintEventArgs)
            Dim Answer As String = String.Empty
            Dim barCode As DevExpress.XtraReports.UI.XRBarCode = CType(sender, DevExpress.XtraReports.UI.XRBarCode)
            Dim row As System.Data.DataRow = CreditorRow(barCode.Report)
            If row IsNot Nothing Then
                Answer = DebtPlus.Utils.Format.Strings.DigitsOnly(DebtPlus.Utils.Nulls.DStr(row("zipcode")))
            End If

            barCode.Text = Answer
        End Sub
    End Class
End Namespace
