Namespace Creditor.Transactions
    <Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
    Partial Class CreditorTransactionsReport
        Inherits DebtPlus.Reports.Template.DatedTemplateXtraReportClass

        'XtraReport overrides dispose to clean up the component list.
        <System.Diagnostics.DebuggerNonUserCode()> _
        Protected Overrides Sub Dispose(ByVal disposing As Boolean)
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
            MyBase.Dispose(disposing)
        End Sub

        'Required by the Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Designer
        'It can be modified using the Designer.  
        'Do not modify it using the code editor.
        <System.Diagnostics.DebuggerStepThrough()> _
        Private Sub InitializeComponent()
            Dim XrSummary1 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary()
            Dim XrSummary2 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary()
            Dim PostNetGenerator1 As DevExpress.XtraPrinting.BarCode.PostNetGenerator = New DevExpress.XtraPrinting.BarCode.PostNetGenerator()
            Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(CreditorTransactionsReport))
            Me.XrLabel_tran_type = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_item_date = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_credit_amt = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_debit_amt = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_check_number = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_message = New DevExpress.XtraReports.UI.XRLabel()
            Me.ReportFooter = New DevExpress.XtraReports.UI.ReportFooterBand()
            Me.XrLabel8 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLine1 = New DevExpress.XtraReports.UI.XRLine()
            Me.XrLabel_Total_Credit_amt = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_Total_Debit_amt = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_item_reconciled = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_invoice = New DevExpress.XtraReports.UI.XRLabel()
            Me.ParameterCreditor = New DevExpress.XtraReports.Parameters.Parameter()
            Me.GroupHeader1 = New DevExpress.XtraReports.UI.GroupHeaderBand()
            Me.XrPanel1 = New DevExpress.XtraReports.UI.XRPanel()
            Me.XrLabel10 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel7 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel6 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel5 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel4 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel3 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel2 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel1 = New DevExpress.XtraReports.UI.XRLabel()
            Me.GroupHeader2 = New DevExpress.XtraReports.UI.GroupHeaderBand()
            Me.XrPanel_CreditorAddress = New DevExpress.XtraReports.UI.XRPanel()
            Me.XrLabel_Creditor_Address = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrBarCode_PostalCode = New DevExpress.XtraReports.UI.XRBarCode()
            CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
            '
            'PageHeader
            '
            Me.PageHeader.HeightF = 103.3333!
            '
            'XrLabel_Title
            '
            Me.XrLabel_Title.StylePriority.UseFont = False
            '
            'XrPageInfo_PageNumber
            '
            Me.XrPageInfo_PageNumber.StylePriority.UseTextAlignment = False
            '
            'XRLabel_Agency_Name
            '
            Me.XRLabel_Agency_Name.StylePriority.UseTextAlignment = False
            '
            'XrLabel_Agency_Address3
            '
            Me.XrLabel_Agency_Address3.StylePriority.UseFont = False
            Me.XrLabel_Agency_Address3.StylePriority.UseTextAlignment = False
            '
            'XrLabel_Agency_Address1
            '
            Me.XrLabel_Agency_Address1.StylePriority.UseFont = False
            Me.XrLabel_Agency_Address1.StylePriority.UseTextAlignment = False
            '
            'XrLabel_Agency_Phone
            '
            Me.XrLabel_Agency_Phone.StylePriority.UseFont = False
            Me.XrLabel_Agency_Phone.StylePriority.UseTextAlignment = False
            '
            'XrLabel_Agency_Address2
            '
            Me.XrLabel_Agency_Address2.StylePriority.UseFont = False
            Me.XrLabel_Agency_Address2.StylePriority.UseTextAlignment = False
            '
            'Detail
            '
            Me.Detail.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel_invoice, Me.XrLabel_item_reconciled, Me.XrLabel_message, Me.XrLabel_check_number, Me.XrLabel_debit_amt, Me.XrLabel_credit_amt, Me.XrLabel_item_date, Me.XrLabel_tran_type})
            Me.Detail.HeightF = 15.0!
            '
            'XrLabel_tran_type
            '
            Me.XrLabel_tran_type.CanGrow = False
            Me.XrLabel_tran_type.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "tran_type"), New DevExpress.XtraReports.UI.XRBinding("Tag", Nothing, "creditor_register")})
            Me.XrLabel_tran_type.Font = New System.Drawing.Font("Times New Roman", 9.75!)
            Me.XrLabel_tran_type.ForeColor = System.Drawing.SystemColors.WindowText
            Me.XrLabel_tran_type.LocationFloat = New DevExpress.Utils.PointFloat(8.0!, 0.0!)
            Me.XrLabel_tran_type.Name = "XrLabel_tran_type"
            Me.XrLabel_tran_type.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_tran_type.SizeF = New System.Drawing.SizeF(26.0!, 15.0!)
            Me.XrLabel_tran_type.StylePriority.UseFont = False
            Me.XrLabel_tran_type.StylePriority.UseForeColor = False
            Me.XrLabel_tran_type.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            '
            'XrLabel_item_date
            '
            Me.XrLabel_item_date.CanGrow = False
            Me.XrLabel_item_date.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "item_date", "{0:d}"), New DevExpress.XtraReports.UI.XRBinding("Tag", Nothing, "creditor_register")})
            Me.XrLabel_item_date.Font = New System.Drawing.Font("Times New Roman", 9.75!)
            Me.XrLabel_item_date.ForeColor = System.Drawing.SystemColors.WindowText
            Me.XrLabel_item_date.LocationFloat = New DevExpress.Utils.PointFloat(42.0!, 0.0!)
            Me.XrLabel_item_date.Name = "XrLabel_item_date"
            Me.XrLabel_item_date.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_item_date.SizeF = New System.Drawing.SizeF(67.0!, 15.0!)
            Me.XrLabel_item_date.StylePriority.UseFont = False
            Me.XrLabel_item_date.StylePriority.UseForeColor = False
            Me.XrLabel_item_date.StylePriority.UseTextAlignment = False
            Me.XrLabel_item_date.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel_credit_amt
            '
            Me.XrLabel_credit_amt.CanGrow = False
            Me.XrLabel_credit_amt.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "credit_amt", "{0:c}"), New DevExpress.XtraReports.UI.XRBinding("Tag", Nothing, "creditor_register")})
            Me.XrLabel_credit_amt.Font = New System.Drawing.Font("Times New Roman", 9.75!)
            Me.XrLabel_credit_amt.ForeColor = System.Drawing.SystemColors.WindowText
            Me.XrLabel_credit_amt.LocationFloat = New DevExpress.Utils.PointFloat(292.0!, 0.0!)
            Me.XrLabel_credit_amt.Name = "XrLabel_credit_amt"
            Me.XrLabel_credit_amt.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_credit_amt.SizeF = New System.Drawing.SizeF(108.0!, 15.0!)
            Me.XrLabel_credit_amt.StylePriority.UseFont = False
            Me.XrLabel_credit_amt.StylePriority.UseForeColor = False
            Me.XrLabel_credit_amt.Text = "$0.00"
            Me.XrLabel_credit_amt.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel_debit_amt
            '
            Me.XrLabel_debit_amt.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "debit_amt", "{0:c}"), New DevExpress.XtraReports.UI.XRBinding("Tag", Nothing, "creditor_register")})
            Me.XrLabel_debit_amt.Font = New System.Drawing.Font("Times New Roman", 9.75!)
            Me.XrLabel_debit_amt.ForeColor = System.Drawing.SystemColors.WindowText
            Me.XrLabel_debit_amt.LocationFloat = New DevExpress.Utils.PointFloat(408.0!, 0.0!)
            Me.XrLabel_debit_amt.Name = "XrLabel_debit_amt"
            Me.XrLabel_debit_amt.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_debit_amt.SizeF = New System.Drawing.SizeF(108.0!, 15.0!)
            Me.XrLabel_debit_amt.StylePriority.UseFont = False
            Me.XrLabel_debit_amt.StylePriority.UseForeColor = False
            Me.XrLabel_debit_amt.Text = "$0.00"
            Me.XrLabel_debit_amt.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel_check_number
            '
            Me.XrLabel_check_number.CanGrow = False
            Me.XrLabel_check_number.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "check_number"), New DevExpress.XtraReports.UI.XRBinding("Tag", Nothing, "creditor_register")})
            Me.XrLabel_check_number.Font = New System.Drawing.Font("Times New Roman", 9.75!)
            Me.XrLabel_check_number.ForeColor = System.Drawing.SystemColors.WindowText
            Me.XrLabel_check_number.LocationFloat = New DevExpress.Utils.PointFloat(117.0!, 0.0!)
            Me.XrLabel_check_number.Name = "XrLabel_check_number"
            Me.XrLabel_check_number.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_check_number.SizeF = New System.Drawing.SizeF(92.0!, 15.0!)
            Me.XrLabel_check_number.StylePriority.UseFont = False
            Me.XrLabel_check_number.StylePriority.UseForeColor = False
            Me.XrLabel_check_number.StylePriority.UseTextAlignment = False
            Me.XrLabel_check_number.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel_message
            '
            Me.XrLabel_message.CanGrow = False
            Me.XrLabel_message.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "message"), New DevExpress.XtraReports.UI.XRBinding("Tag", Nothing, "creditor_register")})
            Me.XrLabel_message.Font = New System.Drawing.Font("Times New Roman", 9.75!)
            Me.XrLabel_message.ForeColor = System.Drawing.SystemColors.WindowText
            Me.XrLabel_message.LocationFloat = New DevExpress.Utils.PointFloat(617.0!, 0.0!)
            Me.XrLabel_message.Name = "XrLabel_message"
            Me.XrLabel_message.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_message.SizeF = New System.Drawing.SizeF(183.0!, 15.0!)
            Me.XrLabel_message.StylePriority.UseFont = False
            Me.XrLabel_message.StylePriority.UseForeColor = False
            Me.XrLabel_message.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            '
            'ReportFooter
            '
            Me.ReportFooter.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel8, Me.XrLine1, Me.XrLabel_Total_Credit_amt, Me.XrLabel_Total_Debit_amt})
            Me.ReportFooter.HeightF = 50.0!
            Me.ReportFooter.Name = "ReportFooter"
            '
            'XrLabel8
            '
            Me.XrLabel8.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel8.ForeColor = System.Drawing.SystemColors.WindowText
            Me.XrLabel8.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 25.0!)
            Me.XrLabel8.Name = "XrLabel8"
            Me.XrLabel8.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel8.SizeF = New System.Drawing.SizeF(108.0!, 15.0!)
            Me.XrLabel8.StylePriority.UseForeColor = False
            Me.XrLabel8.Text = "TOTALS"
            Me.XrLabel8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            '
            'XrLine1
            '
            Me.XrLine1.ForeColor = System.Drawing.Color.Teal
            Me.XrLine1.LineWidth = 2
            Me.XrLine1.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 8.0!)
            Me.XrLine1.Name = "XrLine1"
            Me.XrLine1.SizeF = New System.Drawing.SizeF(517.0!, 9.0!)
            Me.XrLine1.StylePriority.UseForeColor = False
            '
            'XrLabel_Total_Credit_amt
            '
            Me.XrLabel_Total_Credit_amt.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "credit_amt")})
            Me.XrLabel_Total_Credit_amt.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel_Total_Credit_amt.ForeColor = System.Drawing.SystemColors.WindowText
            Me.XrLabel_Total_Credit_amt.LocationFloat = New DevExpress.Utils.PointFloat(292.0!, 25.0!)
            Me.XrLabel_Total_Credit_amt.Name = "XrLabel_Total_Credit_amt"
            Me.XrLabel_Total_Credit_amt.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_Total_Credit_amt.SizeF = New System.Drawing.SizeF(108.0!, 15.0!)
            Me.XrLabel_Total_Credit_amt.StylePriority.UseForeColor = False
            XrSummary1.FormatString = "{0:c}"
            XrSummary1.Running = DevExpress.XtraReports.UI.SummaryRunning.Report
            Me.XrLabel_Total_Credit_amt.Summary = XrSummary1
            Me.XrLabel_Total_Credit_amt.Text = "$0.00"
            Me.XrLabel_Total_Credit_amt.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel_Total_Debit_amt
            '
            Me.XrLabel_Total_Debit_amt.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "debit_amt")})
            Me.XrLabel_Total_Debit_amt.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel_Total_Debit_amt.ForeColor = System.Drawing.SystemColors.WindowText
            Me.XrLabel_Total_Debit_amt.LocationFloat = New DevExpress.Utils.PointFloat(408.0!, 25.0!)
            Me.XrLabel_Total_Debit_amt.Name = "XrLabel_Total_Debit_amt"
            Me.XrLabel_Total_Debit_amt.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_Total_Debit_amt.SizeF = New System.Drawing.SizeF(108.0!, 15.0!)
            Me.XrLabel_Total_Debit_amt.StylePriority.UseForeColor = False
            XrSummary2.FormatString = "{0:c}"
            XrSummary2.Running = DevExpress.XtraReports.UI.SummaryRunning.Report
            Me.XrLabel_Total_Debit_amt.Summary = XrSummary2
            Me.XrLabel_Total_Debit_amt.Text = "$0.00"
            Me.XrLabel_Total_Debit_amt.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel_item_reconciled
            '
            Me.XrLabel_item_reconciled.CanGrow = False
            Me.XrLabel_item_reconciled.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "item_reconciled", "{0:d}"), New DevExpress.XtraReports.UI.XRBinding("Tag", Nothing, "creditor_register")})
            Me.XrLabel_item_reconciled.Font = New System.Drawing.Font("Times New Roman", 9.75!)
            Me.XrLabel_item_reconciled.ForeColor = System.Drawing.SystemColors.WindowText
            Me.XrLabel_item_reconciled.LocationFloat = New DevExpress.Utils.PointFloat(217.0!, 0.0!)
            Me.XrLabel_item_reconciled.Name = "XrLabel_item_reconciled"
            Me.XrLabel_item_reconciled.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_item_reconciled.SizeF = New System.Drawing.SizeF(67.0!, 15.0!)
            Me.XrLabel_item_reconciled.StylePriority.UseFont = False
            Me.XrLabel_item_reconciled.StylePriority.UseForeColor = False
            Me.XrLabel_item_reconciled.StylePriority.UseTextAlignment = False
            Me.XrLabel_item_reconciled.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel_invoice
            '
            Me.XrLabel_invoice.CanGrow = False
            Me.XrLabel_invoice.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "invoice"), New DevExpress.XtraReports.UI.XRBinding("Tag", Nothing, "creditor_register")})
            Me.XrLabel_invoice.Font = New System.Drawing.Font("Times New Roman", 9.75!)
            Me.XrLabel_invoice.ForeColor = System.Drawing.SystemColors.WindowText
            Me.XrLabel_invoice.LocationFloat = New DevExpress.Utils.PointFloat(525.0!, 0.0!)
            Me.XrLabel_invoice.Name = "XrLabel_invoice"
            Me.XrLabel_invoice.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_invoice.SizeF = New System.Drawing.SizeF(84.0!, 15.0!)
            Me.XrLabel_invoice.StylePriority.UseFont = False
            Me.XrLabel_invoice.StylePriority.UseForeColor = False
            Me.XrLabel_invoice.StylePriority.UseTextAlignment = False
            Me.XrLabel_invoice.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'ParameterCreditor
            '
            Me.ParameterCreditor.Description = "Creditor ID"
            Me.ParameterCreditor.Name = "ParameterCreditor"
            Me.ParameterCreditor.Visible = False
            '
            'GroupHeader1
            '
            Me.GroupHeader1.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrPanel1})
            Me.GroupHeader1.HeightF = 22.91667!
            Me.GroupHeader1.Name = "GroupHeader1"
            Me.GroupHeader1.RepeatEveryPage = True
            '
            'XrPanel1
            '
            Me.XrPanel1.BackColor = System.Drawing.Color.Teal
            Me.XrPanel1.BorderColor = System.Drawing.Color.Teal
            Me.XrPanel1.CanGrow = False
            Me.XrPanel1.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel10, Me.XrLabel7, Me.XrLabel6, Me.XrLabel5, Me.XrLabel4, Me.XrLabel3, Me.XrLabel2, Me.XrLabel1})
            Me.XrPanel1.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
            Me.XrPanel1.Name = "XrPanel1"
            Me.XrPanel1.SizeF = New System.Drawing.SizeF(800.0!, 16.0!)
            '
            'XrLabel10
            '
            Me.XrLabel10.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel10.ForeColor = System.Drawing.Color.White
            Me.XrLabel10.LocationFloat = New DevExpress.Utils.PointFloat(617.0!, 0.0!)
            Me.XrLabel10.Name = "XrLabel10"
            Me.XrLabel10.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel10.SizeF = New System.Drawing.SizeF(75.0!, 15.0!)
            Me.XrLabel10.Text = "MESSAGE"
            Me.XrLabel10.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            '
            'XrLabel7
            '
            Me.XrLabel7.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel7.ForeColor = System.Drawing.Color.White
            Me.XrLabel7.LocationFloat = New DevExpress.Utils.PointFloat(534.0!, 0.0!)
            Me.XrLabel7.Name = "XrLabel7"
            Me.XrLabel7.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel7.SizeF = New System.Drawing.SizeF(75.0!, 15.0!)
            Me.XrLabel7.StylePriority.UseTextAlignment = False
            Me.XrLabel7.Text = "INVOICE"
            Me.XrLabel7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel6
            '
            Me.XrLabel6.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel6.ForeColor = System.Drawing.Color.White
            Me.XrLabel6.LocationFloat = New DevExpress.Utils.PointFloat(217.0!, 0.0!)
            Me.XrLabel6.Name = "XrLabel6"
            Me.XrLabel6.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel6.SizeF = New System.Drawing.SizeF(67.0!, 15.0!)
            Me.XrLabel6.StylePriority.UseTextAlignment = False
            Me.XrLabel6.Text = "RECON"
            Me.XrLabel6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel5
            '
            Me.XrLabel5.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel5.ForeColor = System.Drawing.Color.White
            Me.XrLabel5.LocationFloat = New DevExpress.Utils.PointFloat(150.0!, 0.0!)
            Me.XrLabel5.Name = "XrLabel5"
            Me.XrLabel5.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel5.SizeF = New System.Drawing.SizeF(58.0!, 15.0!)
            Me.XrLabel5.StylePriority.UseTextAlignment = False
            Me.XrLabel5.Text = "CHECK"
            Me.XrLabel5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel4
            '
            Me.XrLabel4.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel4.ForeColor = System.Drawing.Color.White
            Me.XrLabel4.LocationFloat = New DevExpress.Utils.PointFloat(458.0!, 0.0!)
            Me.XrLabel4.Name = "XrLabel4"
            Me.XrLabel4.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel4.SizeF = New System.Drawing.SizeF(58.0!, 15.0!)
            Me.XrLabel4.Text = "DEBIT"
            Me.XrLabel4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel3
            '
            Me.XrLabel3.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel3.ForeColor = System.Drawing.Color.White
            Me.XrLabel3.LocationFloat = New DevExpress.Utils.PointFloat(328.0!, 0.0!)
            Me.XrLabel3.Name = "XrLabel3"
            Me.XrLabel3.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel3.SizeF = New System.Drawing.SizeF(72.0!, 15.0!)
            Me.XrLabel3.Text = "CREDIT"
            Me.XrLabel3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel2
            '
            Me.XrLabel2.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel2.ForeColor = System.Drawing.Color.White
            Me.XrLabel2.LocationFloat = New DevExpress.Utils.PointFloat(8.0!, 0.0!)
            Me.XrLabel2.Name = "XrLabel2"
            Me.XrLabel2.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel2.SizeF = New System.Drawing.SizeF(50.0!, 15.0!)
            Me.XrLabel2.Text = "TRAN"
            Me.XrLabel2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            '
            'XrLabel1
            '
            Me.XrLabel1.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel1.ForeColor = System.Drawing.Color.White
            Me.XrLabel1.LocationFloat = New DevExpress.Utils.PointFloat(58.0!, 0.0!)
            Me.XrLabel1.Name = "XrLabel1"
            Me.XrLabel1.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel1.SizeF = New System.Drawing.SizeF(50.0!, 15.0!)
            Me.XrLabel1.StylePriority.UseTextAlignment = False
            Me.XrLabel1.Text = "DATE"
            Me.XrLabel1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'GroupHeader2
            '
            Me.GroupHeader2.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrPanel_CreditorAddress})
            Me.GroupHeader2.HeightF = 204.1667!
            Me.GroupHeader2.Level = 1
            Me.GroupHeader2.Name = "GroupHeader2"
            '
            'XrPanel_CreditorAddress
            '
            Me.XrPanel_CreditorAddress.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel_Creditor_Address, Me.XrBarCode_PostalCode})
            Me.XrPanel_CreditorAddress.LocationFloat = New DevExpress.Utils.PointFloat(42.0!, 62.58333!)
            Me.XrPanel_CreditorAddress.Name = "XrPanel_CreditorAddress"
            Me.XrPanel_CreditorAddress.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
            Me.XrPanel_CreditorAddress.SizeF = New System.Drawing.SizeF(300.0!, 117.0!)
            '
            'XrLabel_Creditor_Address
            '
            Me.XrLabel_Creditor_Address.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 17.0!)
            Me.XrLabel_Creditor_Address.Multiline = True
            Me.XrLabel_Creditor_Address.Name = "XrLabel_Creditor_Address"
            Me.XrLabel_Creditor_Address.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_Creditor_Address.Scripts.OnBeforePrint = "XrLabel_Creditor_Address_BeforePrint"
            Me.XrLabel_Creditor_Address.SizeF = New System.Drawing.SizeF(300.0!, 100.0!)
            Me.XrLabel_Creditor_Address.Text = "XrLabel_Creditor_Address"
            Me.XrLabel_Creditor_Address.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
            '
            'XrBarCode_PostalCode
            '
            Me.XrBarCode_PostalCode.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
            Me.XrBarCode_PostalCode.Name = "XrBarCode_PostalCode"
            Me.XrBarCode_PostalCode.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
            Me.XrBarCode_PostalCode.Scripts.OnBeforePrint = "XrBarCode_PostalCode_BeforePrint"
            Me.XrBarCode_PostalCode.ShowText = False
            Me.XrBarCode_PostalCode.SizeF = New System.Drawing.SizeF(300.0!, 16.0!)
            Me.XrBarCode_PostalCode.Symbology = PostNetGenerator1
            Me.XrBarCode_PostalCode.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
            '
            'CreditorTransactionsReport
            '
            Me.Bands.AddRange(New DevExpress.XtraReports.UI.Band() {Me.Detail, Me.PageHeader, Me.PageFooter, Me.TopMarginBand1, Me.BottomMarginBand1, Me.ReportFooter, Me.GroupHeader1, Me.GroupHeader2})
            Me.Parameters.AddRange(New DevExpress.XtraReports.Parameters.Parameter() {Me.ParameterCreditor})
            Me.ScriptLanguage = DevExpress.XtraReports.ScriptLanguage.VisualBasic
            Me.ScriptReferencesString = resources.GetString("$this.ScriptReferencesString")
            Me.Scripts.OnBeforePrint = "CreditorTransactionsReport_BeforePrint"
            Me.ScriptsSource = resources.GetString("$this.ScriptsSource")
            Me.StyleSheet.AddRange(New DevExpress.XtraReports.UI.XRControlStyle() {Me.XrControlStyle_Header, Me.XrControlStyle_Totals, Me.XrControlStyle_HeaderPannel, Me.XrControlStyle_GroupHeader})
            Me.Version = "11.2"
            Me.Controls.SetChildIndex(Me.GroupHeader2, 0)
            Me.Controls.SetChildIndex(Me.GroupHeader1, 0)
            Me.Controls.SetChildIndex(Me.ReportFooter, 0)
            Me.Controls.SetChildIndex(Me.BottomMarginBand1, 0)
            Me.Controls.SetChildIndex(Me.TopMarginBand1, 0)
            Me.Controls.SetChildIndex(Me.PageFooter, 0)
            Me.Controls.SetChildIndex(Me.PageHeader, 0)
            Me.Controls.SetChildIndex(Me.Detail, 0)
            CType(Me, System.ComponentModel.ISupportInitialize).EndInit()

        End Sub
        Friend WithEvents XrLabel_check_number As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_debit_amt As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_credit_amt As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_item_date As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_tran_type As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_message As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents ReportFooter As DevExpress.XtraReports.UI.ReportFooterBand
        Friend WithEvents XrLine1 As DevExpress.XtraReports.UI.XRLine
        Friend WithEvents XrLabel_Total_Credit_amt As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_Total_Debit_amt As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel8 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_item_reconciled As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_invoice As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents ParameterCreditor As DevExpress.XtraReports.Parameters.Parameter
        Friend WithEvents GroupHeader1 As DevExpress.XtraReports.UI.GroupHeaderBand
        Friend WithEvents XrPanel1 As DevExpress.XtraReports.UI.XRPanel
        Friend WithEvents XrLabel10 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel7 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel6 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel5 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel4 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel3 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel2 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel1 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents GroupHeader2 As DevExpress.XtraReports.UI.GroupHeaderBand
        Friend WithEvents XrPanel_CreditorAddress As DevExpress.XtraReports.UI.XRPanel
        Friend WithEvents XrLabel_Creditor_Address As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrBarCode_PostalCode As DevExpress.XtraReports.UI.XRBarCode
    End Class
End Namespace
