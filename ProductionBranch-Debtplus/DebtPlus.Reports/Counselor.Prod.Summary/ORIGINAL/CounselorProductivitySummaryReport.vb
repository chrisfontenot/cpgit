#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}
#End Region

Option Compare Binary
Option Explicit On
Option Strict On

Namespace Counselor.Prod.Summary
    Public Class CounselorProductivitySummaryReport

        ''' <summary>
        ''' Create an instance of our report
        ''' </summary>
        Public Sub New()
            MyBase.New()
            InitializeComponent()

            AddHandler BeforePrint, AddressOf CounselorProductivitySummaryReport_BeforePrint
            AddHandler XrLabel_appts_per_day.BeforePrint, AddressOf XrLabel_appts_per_day_BeforePrint
            AddHandler XrLabel_pct.BeforePrint, AddressOf XrLabel_pct_BeforePrint
            AddHandler XrLabel_days.BeforePrint, AddressOf XrLabel_days_BeforePrint
            AddHandler XrLabel_days.PrintOnPage, AddressOf Field_PrintOnPage
            AddHandler XrLabel_pct.PrintOnPage, AddressOf Field_PrintOnPage
            AddHandler XrLabel_appts_per_day.PrintOnPage, AddressOf Field_PrintOnPage
        End Sub

        ''' <summary>
        ''' Report Title information
        ''' </summary>
        Public Overrides ReadOnly Property ReportTitle() As String
            Get
                Return "Counselor Productivity Summary"
            End Get
        End Property

        Private ds As New System.Data.DataSet("ds")
        ''' <summary>
        ''' Initialize the report
        ''' </summary>
        Private Sub CounselorProductivitySummaryReport_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs)
            Dim rpt As DevExpress.XtraReports.UI.XtraReport = CType(sender, DevExpress.XtraReports.UI.XtraReport)
            Dim sqlInfo As DebtPlus.LINQ.SQLInfoClass = DebtPlus.LINQ.SQLInfoClass.getDefault()

            Using cn As New System.Data.SqlClient.SqlConnection(sqlInfo.ConnectionString)
                cn.Open()
                Using cmd As New System.Data.SqlClient.SqlCommand()
                    cmd.Connection = cn
                    cmd.CommandText = "rpt_summary_counselor_productivity"
                    cmd.CommandType = System.Data.CommandType.StoredProcedure
                    cmd.Parameters.Add("@From_date", System.Data.SqlDbType.DateTime).Value = rpt.Parameters("ParameterFromDate").Value
                    cmd.Parameters.Add("@To_date", System.Data.SqlDbType.DateTime).Value = rpt.Parameters("ParameterToDate").Value
                    cmd.CommandTimeout = 0

                    Using da As New System.Data.SqlClient.SqlDataAdapter(cmd)
                        da.Fill(ds, "rpt_summary_counselor_productivity")
                    End Using
                End Using
            End Using

            Dim tbl As System.Data.DataTable = ds.Tables("rpt_summary_counselor_productivity")
            rpt.DataSource = New System.Data.DataView(tbl, String.Empty, "office,counselor,appt_type", System.Data.DataViewRowState.CurrentRows)
        End Sub

        ''' <summary>
        ''' Appointments per day average
        ''' </summary>
        Private Sub XrLabel_appts_per_day_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs)
            Dim lbl As DevExpress.XtraReports.UI.XRLabel = CType(sender, DevExpress.XtraReports.UI.XRLabel)
            Dim rpt As DevExpress.XtraReports.UI.XtraReportBase = CType(lbl.Report, DevExpress.XtraReports.UI.XtraReportBase)
            Dim appts As System.Int32 = DebtPlus.Utils.Nulls.DInt(rpt.GetCurrentColumnValue("total_appts"))
            Dim days As System.Int32 = DebtPlus.Utils.Nulls.DInt(rpt.GetCurrentColumnValue("days"))
            Dim Answer As String = String.Empty

            If days > 0 Then
                Try
                    Answer = String.Format("{0:f3}", appts / days)
                Catch ex As DivideByZeroException
                Catch ex As OverflowException
                End Try
            End If

            If Not ShouldPrintOnPage(lbl) Then
                Answer = String.Empty
            End If
            lbl.Text = Answer
        End Sub

        ''' <summary>
        ''' Percentage of the appointment types seen
        ''' </summary>
        Private Sub XrLabel_pct_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs)
            Dim lbl As DevExpress.XtraReports.UI.XRLabel = CType(sender, DevExpress.XtraReports.UI.XRLabel)
            Dim rpt As DevExpress.XtraReports.UI.XtraReportBase = CType(lbl.Report, DevExpress.XtraReports.UI.XtraReportBase)
            Dim appts As System.Int32 = DebtPlus.Utils.Nulls.DInt(rpt.GetCurrentColumnValue("appts"))
            Dim total_appts As System.Int32 = DebtPlus.Utils.Nulls.DInt(rpt.GetCurrentColumnValue("total_appts"))
            Dim Answer As String = String.Empty

            If total_appts > 0 Then
                Try
                    Answer = String.Format("{0:p2}", appts / total_appts)
                Catch ex As DivideByZeroException
                Catch ex As OverflowException
                End Try
            End If

            lbl.Text = Answer
        End Sub

        ''' <summary>
        ''' Key value for unique items
        ''' </summary>
        Private Function Key(ByVal rpt As DevExpress.XtraReports.UI.XtraReportBase) As String
            Dim counselor As String = DebtPlus.Utils.Nulls.DStr(rpt.GetCurrentColumnValue("counselor"))
            Dim office As String = DebtPlus.Utils.Nulls.DStr(rpt.GetCurrentColumnValue("office"))
            Return String.Format("{0}*{1}", office, counselor)
        End Function

        ''' <summary>
        ''' Format the days value correctly and suppress if duplicated
        ''' </summary>
        Private Sub XrLabel_days_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs)
            Dim lbl As DevExpress.XtraReports.UI.XRLabel = CType(sender, DevExpress.XtraReports.UI.XRLabel)
            Dim Days As System.Int32 = DebtPlus.Utils.Nulls.DInt(GetCurrentColumnValue("days"))

            Dim Answer As String = String.Empty
            If ShouldPrintOnPage(lbl) Then
                Answer = String.Format("{0:n0}", Days)
            End If

            lbl.Text = Answer
        End Sub

        ''' <summary>
        ''' Should we print the field on the report?
        ''' </summary>
        Private Sub Field_PrintOnPage(ByVal sender As Object, ByVal e As DevExpress.XtraReports.UI.PrintOnPageEventArgs)
            Dim lbl As DevExpress.XtraReports.UI.XRLabel = CType(sender, DevExpress.XtraReports.UI.XRLabel)
            If Not ShouldPrintOnPage(lbl) Then
                e.Cancel = True
            End If
        End Sub

        ''' <summary>
        ''' Should we print the field on the form?
        ''' </summary>
        Private Function ShouldPrintOnPage(ByVal FieldTag As DevExpress.XtraReports.UI.XRControl) As Boolean
            If FieldTag.Tag IsNot Nothing Then
                If Convert.ToString(FieldTag.Tag) = Key(FieldTag.Report) Then
                    Return False
                End If
            End If
            FieldTag.Tag = Key(FieldTag.Report)
            Return True
        End Function
    End Class
End Namespace