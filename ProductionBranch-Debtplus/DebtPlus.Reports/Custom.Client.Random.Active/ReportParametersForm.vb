#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}

#End Region

Imports DevExpress.XtraEditors.Controls


Namespace Custom.Client.Random.Active
    Public Class ReportParametersForm
        Inherits Template.Forms.ReportParametersForm

        Public Sub New()
            MyBase.New()
            InitializeComponent()
            AddHandler Load, AddressOf ReportParametersForm_Load
            AddHandler TextEdit_Percent.EditValueChanging, AddressOf TextEdit_Percent_EditValueChanging
        End Sub

        Public Property ParameterPercent() As Double
            Get
                Dim Value As Double
                If TextEdit_Percent.EditValue IsNot Nothing AndAlso TextEdit_Percent.EditValue IsNot DBNull.Value Then
                    Value = Convert.ToDouble(TextEdit_Percent.EditValue)
                    If Value < 0.0 Then
                        Value = 0.0
                    End If
                    If Value >= 1.0 Then
                        Value = Value/100.0
                    End If
                    Return Value
                End If
                Return 0.0
            End Get
            Set(ByVal value As Double)
                TextEdit_Percent.EditValue = value
            End Set
        End Property

        Private Sub ReportParametersForm_Load(ByVal sender As Object, ByVal e As EventArgs)
            ParameterPercent = 0.02
            ButtonOK.Enabled = True
        End Sub

        Private Sub TextEdit_Percent_EditValueChanging(ByVal sender As Object, ByVal e As ChangingEventArgs)
            Dim value As Double = 0.0
            If e.NewValue IsNot Nothing AndAlso e.NewValue IsNot DBNull.Value Then
                value = Convert.ToDouble(value)
                If value < 0.0 OrElse value > 100.0 Then
                    e.Cancel = True
                End If
            End If
        End Sub
    End Class
End Namespace