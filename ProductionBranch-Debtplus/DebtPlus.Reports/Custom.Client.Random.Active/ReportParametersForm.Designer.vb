Namespace Custom.Client.Random.Active
    <Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
    Partial Class ReportParametersForm

        'Form overrides dispose to clean up the component list.
        <System.Diagnostics.DebuggerNonUserCode()> _
        Protected Overrides Sub Dispose(ByVal disposing As Boolean)
            Try
                If disposing AndAlso components IsNot Nothing Then
                    components.Dispose()
                End If
            Finally
                MyBase.Dispose(disposing)
            End Try
        End Sub

        'Required by the Windows Form Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Windows Form Designer
        'It can be modified using the Windows Form Designer.  
        'Do not modify it using the code editor.
        <System.Diagnostics.DebuggerStepThrough()> _
        Private Sub InitializeComponent()
            Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl
            Me.TextEdit_Percent = New DevExpress.XtraEditors.TextEdit
            CType(Me.DxErrorProvider1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.TextEdit_Percent.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.SuspendLayout()
            '
            'ButtonOK
            '
            Me.ButtonOK.TabIndex = 2
            '
            'ButtonCancel
            '
            Me.ButtonCancel.TabIndex = 3
            '
            'LabelControl1
            '
            Me.LabelControl1.Location = New System.Drawing.Point(12, 30)
            Me.LabelControl1.Name = "LabelControl1"
            Me.LabelControl1.Size = New System.Drawing.Size(37, 13)
            Me.LabelControl1.TabIndex = 0
            Me.LabelControl1.Text = "Percent"
            '
            'TextEdit_Percent
            '
            Me.TextEdit_Percent.Location = New System.Drawing.Point(55, 27)
            Me.TextEdit_Percent.Name = "TextEdit_Percent"
            Me.TextEdit_Percent.Properties.Appearance.Options.UseTextOptions = True
            Me.TextEdit_Percent.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.TextEdit_Percent.Properties.DisplayFormat.FormatString = "p"
            Me.TextEdit_Percent.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.TextEdit_Percent.Properties.EditFormat.FormatString = "p"
            Me.TextEdit_Percent.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.TextEdit_Percent.Properties.Mask.BeepOnError = True
            Me.TextEdit_Percent.Properties.Mask.EditMask = "p"
            Me.TextEdit_Percent.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
            Me.TextEdit_Percent.Size = New System.Drawing.Size(100, 20)
            Me.TextEdit_Percent.TabIndex = 1
            '
            'ReportParametersForm
            '
            Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
            Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
            Me.ClientSize = New System.Drawing.Size(336, 135)
            Me.Controls.Add(Me.LabelControl1)
            Me.Controls.Add(Me.TextEdit_Percent)
            Me.LookAndFeel.UseDefaultLookAndFeel = True
            Me.Name = "ReportParametersForm"
            Me.Controls.SetChildIndex(Me.TextEdit_Percent, 0)
            Me.Controls.SetChildIndex(Me.LabelControl1, 0)
            Me.Controls.SetChildIndex(Me.ButtonOK, 0)
            Me.Controls.SetChildIndex(Me.ButtonCancel, 0)
            CType(Me.DxErrorProvider1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.TextEdit_Percent.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            Me.ResumeLayout(False)
            Me.PerformLayout()

        End Sub
        Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents TextEdit_Percent As DevExpress.XtraEditors.TextEdit
    End Class
End Namespace
