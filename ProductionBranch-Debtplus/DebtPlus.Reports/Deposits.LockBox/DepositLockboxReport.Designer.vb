﻿Namespace Deposits.LockBox
    <Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
    Partial Class DepositLockboxReport
        Inherits DebtPlus.Reports.Template.TemplateXtraReportClass

        'Component overrides dispose to clean up the component list.
        Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
            If disposing Then
                If components IsNot Nothing Then
                    components.Dispose()
                End If
            End If
            MyBase.Dispose(disposing)
        End Sub

        'Required by the Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Designer
        'It can be modified using the Designer.
        'Do not modify it using the code editor.
        Friend WithEvents XrLabel_SubType As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_OK As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_ScannedClient As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_Client As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_Date As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_Amount As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_Name As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_Reference As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents ReportFooter As DevExpress.XtraReports.UI.ReportFooterBand
        Friend WithEvents XrPanel_InvalidItems As DevExpress.XtraReports.UI.XRPanel
        Friend WithEvents XrLabel1 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_Subtotal As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_InvalidItems As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel2 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrPanel1 As DevExpress.XtraReports.UI.XRPanel
        Friend WithEvents XrLabel3 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_Total As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel5 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel6 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel7 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel8 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel9 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel10 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel11 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel12 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_Message As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel4 As DevExpress.XtraReports.UI.XRLabel
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
            Dim XrSummary1 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary()
            Dim XrSummary2 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary()
            Dim XrSummary3 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary()
            Me.XrLabel_SubType = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_OK = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_ScannedClient = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_Client = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_Date = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_Amount = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_Name = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_Reference = New DevExpress.XtraReports.UI.XRLabel()
            Me.ReportFooter = New DevExpress.XtraReports.UI.ReportFooterBand()
            Me.XrLabel_Total = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel3 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrPanel_InvalidItems = New DevExpress.XtraReports.UI.XRPanel()
            Me.XrLabel2 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_InvalidItems = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_Subtotal = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel1 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrPanel1 = New DevExpress.XtraReports.UI.XRPanel()
            Me.XrLabel4 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel12 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel10 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel9 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel8 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel7 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel6 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel5 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel11 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_Message = New DevExpress.XtraReports.UI.XRLabel()
            CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
            '
            'PageHeader
            '
            Me.PageHeader.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrPanel1})
            Me.PageHeader.HeightF = 145.0!
            Me.PageHeader.Controls.SetChildIndex(Me.XrPanel1, 0)
            Me.PageHeader.Controls.SetChildIndex(Me.XrPanel_AgencyAddress, 0)
            Me.PageHeader.Controls.SetChildIndex(Me.XrLabel_Title, 0)
            Me.PageHeader.Controls.SetChildIndex(Me.XrLine_Title, 0)
            Me.PageHeader.Controls.SetChildIndex(Me.XrLabel_Subtitle, 0)
            Me.PageHeader.Controls.SetChildIndex(Me.XrPageInfo1, 0)
            '
            'XrLabel_Title
            '
            Me.XrLabel_Title.StylePriority.UseFont = False
            '
            'XrPageInfo_PageNumber
            '
            Me.XrPageInfo_PageNumber.StylePriority.UseTextAlignment = False
            '
            'XRLabel_Agency_Name
            '
            Me.XRLabel_Agency_Name.StylePriority.UseTextAlignment = False
            '
            'XrLabel_Agency_Address3
            '
            Me.XrLabel_Agency_Address3.StylePriority.UseFont = False
            Me.XrLabel_Agency_Address3.StylePriority.UseTextAlignment = False
            '
            'XrLabel_Agency_Address1
            '
            Me.XrLabel_Agency_Address1.StylePriority.UseFont = False
            Me.XrLabel_Agency_Address1.StylePriority.UseTextAlignment = False
            '
            'XrLabel_Agency_Phone
            '
            Me.XrLabel_Agency_Phone.StylePriority.UseFont = False
            Me.XrLabel_Agency_Phone.StylePriority.UseTextAlignment = False
            '
            'XrLabel_Agency_Address2
            '
            Me.XrLabel_Agency_Address2.StylePriority.UseFont = False
            Me.XrLabel_Agency_Address2.StylePriority.UseTextAlignment = False
            '
            'Detail
            '
            Me.Detail.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel_Message, Me.XrLabel_Reference, Me.XrLabel_Name, Me.XrLabel_Amount, Me.XrLabel_Date, Me.XrLabel_Client, Me.XrLabel_ScannedClient, Me.XrLabel_OK, Me.XrLabel_SubType})
            Me.Detail.HeightF = 15.0!
            '
            'XrLabel_SubType
            '
            Me.XrLabel_SubType.CanGrow = False
            Me.XrLabel_SubType.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "subtype")})
            Me.XrLabel_SubType.LocationFloat = New DevExpress.Utils.PointFloat(9.999998!, 0.0!)
            Me.XrLabel_SubType.Name = "XrLabel_SubType"
            Me.XrLabel_SubType.SizeF = New System.Drawing.SizeF(25.0!, 15.0!)
            Me.XrLabel_SubType.WordWrap = False
            '
            'XrLabel_OK
            '
            Me.XrLabel_OK.CanGrow = False
            Me.XrLabel_OK.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "fmt_ok_to_post")})
            Me.XrLabel_OK.LocationFloat = New DevExpress.Utils.PointFloat(51.50001!, 0.0!)
            Me.XrLabel_OK.Name = "XrLabel_OK"
            Me.XrLabel_OK.SizeF = New System.Drawing.SizeF(13.0!, 15.0!)
            Me.XrLabel_OK.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter
            Me.XrLabel_OK.WordWrap = False
            '
            'XrLabel_ScannedClient
            '
            Me.XrLabel_ScannedClient.CanGrow = False
            Me.XrLabel_ScannedClient.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "scanned_client")})
            Me.XrLabel_ScannedClient.LocationFloat = New DevExpress.Utils.PointFloat(64.50002!, 0.0!)
            Me.XrLabel_ScannedClient.Name = "XrLabel_ScannedClient"
            Me.XrLabel_ScannedClient.SizeF = New System.Drawing.SizeF(92.49998!, 15.0!)
            Me.XrLabel_ScannedClient.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
            Me.XrLabel_ScannedClient.WordWrap = False
            '
            'XrLabel_Client
            '
            Me.XrLabel_Client.CanGrow = False
            Me.XrLabel_Client.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "deposit_client", "{0:0000000}")})
            Me.XrLabel_Client.LocationFloat = New DevExpress.Utils.PointFloat(157.0!, 0.0!)
            Me.XrLabel_Client.Name = "XrLabel_Client"
            Me.XrLabel_Client.SizeF = New System.Drawing.SizeF(85.00002!, 15.0!)
            Me.XrLabel_Client.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
            Me.XrLabel_Client.WordWrap = False
            '
            'XrLabel_Date
            '
            Me.XrLabel_Date.CanGrow = False
            Me.XrLabel_Date.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "item_date", "{0:M/d/yy}")})
            Me.XrLabel_Date.LocationFloat = New DevExpress.Utils.PointFloat(242.0!, 0.0!)
            Me.XrLabel_Date.Name = "XrLabel_Date"
            Me.XrLabel_Date.SizeF = New System.Drawing.SizeF(56.29164!, 15.0!)
            Me.XrLabel_Date.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
            Me.XrLabel_Date.WordWrap = False
            '
            'XrLabel_Amount
            '
            Me.XrLabel_Amount.CanGrow = False
            Me.XrLabel_Amount.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "amount", "{0:c}")})
            Me.XrLabel_Amount.LocationFloat = New DevExpress.Utils.PointFloat(298.2916!, 0.0!)
            Me.XrLabel_Amount.Name = "XrLabel_Amount"
            Me.XrLabel_Amount.SizeF = New System.Drawing.SizeF(81.0!, 15.0!)
            Me.XrLabel_Amount.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
            Me.XrLabel_Amount.WordWrap = False
            '
            'XrLabel_Name
            '
            Me.XrLabel_Name.CanGrow = False
            Me.XrLabel_Name.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "name")})
            Me.XrLabel_Name.LocationFloat = New DevExpress.Utils.PointFloat(379.2916!, 0.0!)
            Me.XrLabel_Name.Name = "XrLabel_Name"
            Me.XrLabel_Name.Padding = New DevExpress.XtraPrinting.PaddingInfo(4, 0, 0, 0, 100.0!)
            Me.XrLabel_Name.SizeF = New System.Drawing.SizeF(150.0!, 15.0!)
            Me.XrLabel_Name.StylePriority.UsePadding = False
            Me.XrLabel_Name.WordWrap = False
            '
            'XrLabel_Reference
            '
            Me.XrLabel_Reference.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "reference")})
            Me.XrLabel_Reference.LocationFloat = New DevExpress.Utils.PointFloat(529.2917!, 0.0!)
            Me.XrLabel_Reference.Name = "XrLabel_Reference"
            Me.XrLabel_Reference.SizeF = New System.Drawing.SizeF(185.9584!, 15.0!)
            Me.XrLabel_Reference.WordWrap = False
            '
            'ReportFooter
            '
            Me.ReportFooter.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel_Total, Me.XrLabel3, Me.XrPanel_InvalidItems})
            Me.ReportFooter.HeightF = 84.0!
            Me.ReportFooter.Name = "ReportFooter"
            '
            'XrLabel_Total
            '
            Me.XrLabel_Total.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "amt_net")})
            Me.XrLabel_Total.LocationFloat = New DevExpress.Utils.PointFloat(190.0!, 61.0!)
            Me.XrLabel_Total.Name = "XrLabel_Total"
            Me.XrLabel_Total.SizeF = New System.Drawing.SizeF(207.0!, 15.0!)
            XrSummary1.FormatString = "{0:c}"
            XrSummary1.Running = DevExpress.XtraReports.UI.SummaryRunning.Report
            Me.XrLabel_Total.Summary = XrSummary1
            Me.XrLabel_Total.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel3
            '
            Me.XrLabel3.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel3.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 61.0!)
            Me.XrLabel3.Name = "XrLabel3"
            Me.XrLabel3.SizeF = New System.Drawing.SizeF(117.0!, 17.0!)
            Me.XrLabel3.Text = "Deposit Total"
            '
            'XrPanel_InvalidItems
            '
            Me.XrPanel_InvalidItems.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel2, Me.XrLabel_InvalidItems, Me.XrLabel_Subtotal, Me.XrLabel1})
            Me.XrPanel_InvalidItems.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 28.0!)
            Me.XrPanel_InvalidItems.Name = "XrPanel_InvalidItems"
            Me.XrPanel_InvalidItems.SizeF = New System.Drawing.SizeF(484.0!, 33.0!)
            '
            'XrLabel2
            '
            Me.XrLabel2.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel2.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 15.0!)
            Me.XrLabel2.Name = "XrLabel2"
            Me.XrLabel2.SizeF = New System.Drawing.SizeF(117.0!, 15.0!)
            Me.XrLabel2.Text = "Less Invalid Items"
            Me.XrLabel2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            '
            'XrLabel_InvalidItems
            '
            Me.XrLabel_InvalidItems.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "amt_reserved")})
            Me.XrLabel_InvalidItems.ForeColor = System.Drawing.Color.Red
            Me.XrLabel_InvalidItems.LocationFloat = New DevExpress.Utils.PointFloat(190.0!, 15.0!)
            Me.XrLabel_InvalidItems.Name = "XrLabel_InvalidItems"
            Me.XrLabel_InvalidItems.SizeF = New System.Drawing.SizeF(207.0!, 15.0!)
            XrSummary2.FormatString = "{0:c}"
            XrSummary2.Running = DevExpress.XtraReports.UI.SummaryRunning.Report
            Me.XrLabel_InvalidItems.Summary = XrSummary2
            Me.XrLabel_InvalidItems.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel_Subtotal
            '
            Me.XrLabel_Subtotal.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "amount")})
            Me.XrLabel_Subtotal.LocationFloat = New DevExpress.Utils.PointFloat(190.0!, 0.0!)
            Me.XrLabel_Subtotal.Name = "XrLabel_Subtotal"
            Me.XrLabel_Subtotal.SizeF = New System.Drawing.SizeF(207.0!, 15.0!)
            XrSummary3.FormatString = "{0:c}"
            XrSummary3.Running = DevExpress.XtraReports.UI.SummaryRunning.Report
            Me.XrLabel_Subtotal.Summary = XrSummary3
            Me.XrLabel_Subtotal.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel1
            '
            Me.XrLabel1.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel1.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
            Me.XrLabel1.Name = "XrLabel1"
            Me.XrLabel1.SizeF = New System.Drawing.SizeF(92.0!, 15.0!)
            Me.XrLabel1.Text = "SubTotal"
            Me.XrLabel1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            '
            'XrPanel1
            '
            Me.XrPanel1.BackColor = System.Drawing.Color.Teal
            Me.XrPanel1.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel4, Me.XrLabel12, Me.XrLabel10, Me.XrLabel9, Me.XrLabel8, Me.XrLabel7, Me.XrLabel6, Me.XrLabel5, Me.XrLabel11})
            Me.XrPanel1.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 125.0!)
            Me.XrPanel1.Name = "XrPanel1"
            Me.XrPanel1.SizeF = New System.Drawing.SizeF(800.0!, 17.0!)
            '
            'XrLabel4
            '
            Me.XrLabel4.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel4.ForeColor = System.Drawing.Color.White
            Me.XrLabel4.LocationFloat = New DevExpress.Utils.PointFloat(715.2501!, 0.9999924!)
            Me.XrLabel4.Name = "XrLabel4"
            Me.XrLabel4.SizeF = New System.Drawing.SizeF(66.0!, 15.00001!)
            Me.XrLabel4.Text = "STATUS"
            '
            'XrLabel12
            '
            Me.XrLabel12.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel12.ForeColor = System.Drawing.Color.White
            Me.XrLabel12.LocationFloat = New DevExpress.Utils.PointFloat(379.2916!, 1.0!)
            Me.XrLabel12.Name = "XrLabel12"
            Me.XrLabel12.Padding = New DevExpress.XtraPrinting.PaddingInfo(4, 0, 0, 0, 100.0!)
            Me.XrLabel12.SizeF = New System.Drawing.SizeF(150.0!, 15.0!)
            Me.XrLabel12.StylePriority.UsePadding = False
            Me.XrLabel12.Text = "NAME"
            '
            'XrLabel10
            '
            Me.XrLabel10.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel10.ForeColor = System.Drawing.Color.White
            Me.XrLabel10.LocationFloat = New DevExpress.Utils.PointFloat(83.0!, 1.0!)
            Me.XrLabel10.Name = "XrLabel10"
            Me.XrLabel10.SizeF = New System.Drawing.SizeF(74.0!, 15.0!)
            Me.XrLabel10.Text = "SCANNED"
            Me.XrLabel10.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
            '
            'XrLabel9
            '
            Me.XrLabel9.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel9.ForeColor = System.Drawing.Color.White
            Me.XrLabel9.LocationFloat = New DevExpress.Utils.PointFloat(184.0!, 1.0!)
            Me.XrLabel9.Name = "XrLabel9"
            Me.XrLabel9.SizeF = New System.Drawing.SizeF(58.0!, 15.0!)
            Me.XrLabel9.Text = "CLIENT"
            Me.XrLabel9.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
            '
            'XrLabel8
            '
            Me.XrLabel8.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel8.ForeColor = System.Drawing.Color.White
            Me.XrLabel8.LocationFloat = New DevExpress.Utils.PointFloat(307.2916!, 1.0!)
            Me.XrLabel8.Name = "XrLabel8"
            Me.XrLabel8.SizeF = New System.Drawing.SizeF(72.0!, 15.0!)
            Me.XrLabel8.Text = "AMOUNT"
            Me.XrLabel8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
            '
            'XrLabel7
            '
            Me.XrLabel7.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel7.ForeColor = System.Drawing.Color.White
            Me.XrLabel7.LocationFloat = New DevExpress.Utils.PointFloat(248.2916!, 1.0!)
            Me.XrLabel7.Name = "XrLabel7"
            Me.XrLabel7.SizeF = New System.Drawing.SizeF(50.0!, 15.0!)
            Me.XrLabel7.Text = "DATE"
            Me.XrLabel7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
            '
            'XrLabel6
            '
            Me.XrLabel6.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel6.ForeColor = System.Drawing.Color.White
            Me.XrLabel6.LocationFloat = New DevExpress.Utils.PointFloat(529.2916!, 0.9999924!)
            Me.XrLabel6.Name = "XrLabel6"
            Me.XrLabel6.SizeF = New System.Drawing.SizeF(128.4583!, 14.99999!)
            Me.XrLabel6.Text = "REFERENCE"
            '
            'XrLabel5
            '
            Me.XrLabel5.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel5.ForeColor = System.Drawing.Color.White
            Me.XrLabel5.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.9999924!)
            Me.XrLabel5.Name = "XrLabel5"
            Me.XrLabel5.SizeF = New System.Drawing.SizeF(41.0!, 15.0!)
            Me.XrLabel5.Text = "TYPE"
            '
            'XrLabel11
            '
            Me.XrLabel11.AutoWidth = True
            Me.XrLabel11.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel11.ForeColor = System.Drawing.Color.White
            Me.XrLabel11.LocationFloat = New DevExpress.Utils.PointFloat(41.0!, 0.9999911!)
            Me.XrLabel11.Name = "XrLabel11"
            Me.XrLabel11.SizeF = New System.Drawing.SizeF(34.00001!, 14.99999!)
            Me.XrLabel11.Text = "OK?"
            Me.XrLabel11.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter
            Me.XrLabel11.WordWrap = False
            '
            'XrLabel_Message
            '
            Me.XrLabel_Message.CanGrow = False
            Me.XrLabel_Message.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "message")})
            Me.XrLabel_Message.ForeColor = System.Drawing.Color.Red
            Me.XrLabel_Message.LocationFloat = New DevExpress.Utils.PointFloat(715.2501!, 0.0!)
            Me.XrLabel_Message.Name = "XrLabel_Message"
            Me.XrLabel_Message.SizeF = New System.Drawing.SizeF(84.74994!, 15.0!)
            Me.XrLabel_Message.WordWrap = False
            '
            'DepositLockboxReport
            '
            Me.Bands.AddRange(New DevExpress.XtraReports.UI.Band() {Me.Detail, Me.PageHeader, Me.PageFooter, Me.TopMarginBand1, Me.BottomMarginBand1, Me.ReportFooter})
            Me.StyleSheet.AddRange(New DevExpress.XtraReports.UI.XRControlStyle() {Me.XrControlStyle_Header, Me.XrControlStyle_Totals, Me.XrControlStyle_HeaderPannel, Me.XrControlStyle_GroupHeader})
            Me.Version = "11.2"
            Me.Controls.SetChildIndex(Me.ReportFooter, 0)
            Me.Controls.SetChildIndex(Me.BottomMarginBand1, 0)
            Me.Controls.SetChildIndex(Me.TopMarginBand1, 0)
            Me.Controls.SetChildIndex(Me.PageFooter, 0)
            Me.Controls.SetChildIndex(Me.PageHeader, 0)
            Me.Controls.SetChildIndex(Me.Detail, 0)
            CType(Me, System.ComponentModel.ISupportInitialize).EndInit()

        End Sub
    End Class
End Namespace