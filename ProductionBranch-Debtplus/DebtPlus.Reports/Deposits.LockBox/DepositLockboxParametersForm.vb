#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}

#End Region

Option Compare Binary
Option Explicit On
Option Strict On

Imports DebtPlus.Data.Controls
Imports DebtPlus.Reports.Template.Forms

Imports System.Globalization
Imports System.Data.SqlClient
Imports DevExpress.Utils
Imports DebtPlus.Data.Forms

Namespace Deposits.LockBox
    Public Class DepositLockboxParametersForm
        Inherits ReportParametersForm

        Private _Parameter_DepositBatchID As Int32 = 0
        Private InInit As Boolean = True
        Private ds As New DataSet("ds")

        Public Sub New()
            MyBase.New()
            InitializeComponent()

            ' Add the handling routines
            AddHandler Load, AddressOf RequestParametersForm_Load
            AddHandler LookUpEdit_Deposit.EditValueChanged, AddressOf LookUpEdit_Disbursement_EditValueChanged
            AddHandler TextEdit_Parameter_DepositBatchID.EditValueChanged, AddressOf TextEdit_disbursement_register_EditValueChanged
            AddHandler TextEdit_Parameter_DepositBatchID.KeyPress, AddressOf TextEdit_disbursement_register_KeyPress
            AddHandler RadioButton_List.CheckedChanged, AddressOf RadioButton_List_CheckedChanged
            AddHandler RadioButton_Text.CheckedChanged, AddressOf RadioButton_List_CheckedChanged
        End Sub

        ''' <summary>
        ''' Deposit batch ID
        ''' </summary>
        Public Property Parameter_DepositBatchID() As Int32
            Get
                Dim value As Int32 = -1
                If Not Int32.TryParse(TextEdit_Parameter_DepositBatchID.Text, NumberStyles.Integer, CultureInfo.InvariantCulture, value) Then value = -1
                Return value
            End Get

            Set(ByVal Value As Int32)
                TextEdit_Parameter_DepositBatchID.Text = String.Format("{0:f0}", Value)
            End Set
        End Property

        Public ReadOnly Property sort_order() As String
            Get
                Dim item As ComboboxItem
                With ComboBoxEdit_Sorting
                    If .SelectedIndex < 0 Then Return String.Empty
                    item = CType(.SelectedItem, DebtPlus.Data.Controls.ComboboxItem)
                    Return Convert.ToString(item.value)
                End With
            End Get
        End Property

#Region " Windows Form Designer generated code "

        'Form overrides dispose to clean up the component list.
        Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
            If disposing Then
                If components IsNot Nothing Then
                    components.Dispose()
                End If
                ds.Dispose()
            End If

            ' Remove the handling routines
            RemoveHandler LookUpEdit_Deposit.EditValueChanged, AddressOf LookUpEdit_Disbursement_EditValueChanged
            RemoveHandler TextEdit_Parameter_DepositBatchID.EditValueChanged, AddressOf TextEdit_disbursement_register_EditValueChanged
            RemoveHandler TextEdit_Parameter_DepositBatchID.KeyPress, AddressOf TextEdit_disbursement_register_KeyPress
            RemoveHandler RadioButton_List.CheckedChanged, AddressOf RadioButton_List_CheckedChanged
            RemoveHandler RadioButton_Text.CheckedChanged, AddressOf RadioButton_List_CheckedChanged

            MyBase.Dispose(disposing)
        End Sub

        'Required by the Windows Form Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Windows Form Designer
        'It can be modified using the Windows Form Designer.  
        'Do not modify it using the code editor.
        Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
        Friend WithEvents RadioButton_List As System.Windows.Forms.RadioButton
        Friend WithEvents RadioButton_Text As System.Windows.Forms.RadioButton
        Friend WithEvents Label1 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents Label2 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents ComboBoxEdit_Sorting As DevExpress.XtraEditors.ComboBoxEdit
        Friend WithEvents TextEdit_Parameter_DepositBatchID As DevExpress.XtraEditors.TextEdit
        Friend WithEvents LookUpEdit_Deposit As DevExpress.XtraEditors.LookUpEdit

        <System.Diagnostics.DebuggerStepThrough()>
        Private Sub InitializeComponent()
            Me.GroupBox1 = New System.Windows.Forms.GroupBox
            Me.Label1 = New DevExpress.XtraEditors.LabelControl
            Me.RadioButton_Text = New System.Windows.Forms.RadioButton
            Me.RadioButton_List = New System.Windows.Forms.RadioButton
            Me.TextEdit_Parameter_DepositBatchID = New DevExpress.XtraEditors.TextEdit
            Me.LookUpEdit_Deposit = New DevExpress.XtraEditors.LookUpEdit
            Me.Label2 = New DevExpress.XtraEditors.LabelControl
            Me.ComboBoxEdit_Sorting = New DevExpress.XtraEditors.ComboBoxEdit
            CType(Me.DxErrorProvider1, System.ComponentModel.ISupportInitialize).BeginInit()

            Me.GroupBox1.SuspendLayout()
            CType(Me.TextEdit_Parameter_DepositBatchID.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LookUpEdit_Deposit.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.ComboBoxEdit_Sorting.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.SuspendLayout()
            '
            'ButtonOK
            '
            Me.ButtonOK.Location = New System.Drawing.Point(248, 18)
            Me.ButtonOK.Size = New System.Drawing.Size(80, 28)
            Me.ButtonOK.TabIndex = 3
            '
            'ButtonCancel
            '
            Me.ButtonCancel.Location = New System.Drawing.Point(248, 56)
            Me.ButtonCancel.Size = New System.Drawing.Size(80, 28)
            Me.ButtonCancel.TabIndex = 4

            '
            'GroupBox1
            '
            Me.GroupBox1.Controls.Add(Me.Label1)
            Me.GroupBox1.Controls.Add(Me.RadioButton_Text)
            Me.GroupBox1.Controls.Add(Me.RadioButton_List)
            Me.GroupBox1.Controls.Add(Me.TextEdit_Parameter_DepositBatchID)
            Me.GroupBox1.Controls.Add(Me.LookUpEdit_Deposit)
            Me.GroupBox1.Location = New System.Drawing.Point(8, 9)
            Me.GroupBox1.Name = "GroupBox1"
            Me.GroupBox1.Size = New System.Drawing.Size(232, 112)
            Me.ToolTipController1.SetSuperTip(Me.GroupBox1, Nothing)
            Me.GroupBox1.TabIndex = 0
            Me.GroupBox1.TabStop = False
            Me.GroupBox1.Text = "Lockbox Batch ID  "
            '
            'Label1
            '
            Me.Label1.Appearance.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.Label1.Appearance.Options.UseFont = True
            Me.Label1.Location = New System.Drawing.Point(32, 54)
            Me.Label1.Name = "Label1"
            Me.Label1.Size = New System.Drawing.Size(122, 13)
            Me.Label1.TabIndex = 2
            Me.Label1.Text = "Or enter a specific ID"
            '
            'RadioButton_Text
            '
            Me.RadioButton_Text.AllowDrop = True
            Me.RadioButton_Text.Location = New System.Drawing.Point(8, 78)
            Me.RadioButton_Text.Name = "RadioButton_Text"
            Me.RadioButton_Text.Size = New System.Drawing.Size(16, 17)
            Me.ToolTipController1.SetSuperTip(Me.RadioButton_Text, Nothing)
            Me.RadioButton_Text.TabIndex = 3
            Me.RadioButton_Text.Tag = "text"
            Me.RadioButton_Text.TextAlign = System.Drawing.ContentAlignment.MiddleRight
            '
            'RadioButton_List
            '
            Me.RadioButton_List.Checked = True
            Me.RadioButton_List.Location = New System.Drawing.Point(8, 28)
            Me.RadioButton_List.Name = "RadioButton_List"
            Me.RadioButton_List.Size = New System.Drawing.Size(16, 17)
            Me.ToolTipController1.SetSuperTip(Me.RadioButton_List, Nothing)
            Me.RadioButton_List.TabIndex = 0
            Me.RadioButton_List.TabStop = True
            Me.RadioButton_List.Tag = "list"
            '
            'TextEdit_Parameter_DepositBatchID
            '
            Me.TextEdit_Parameter_DepositBatchID.EditValue = ""
            Me.TextEdit_Parameter_DepositBatchID.Location = New System.Drawing.Point(32, 78)
            Me.TextEdit_Parameter_DepositBatchID.Name = "TextEdit_Parameter_DepositBatchID"
            Me.TextEdit_Parameter_DepositBatchID.Properties.Appearance.Options.UseTextOptions = True
            Me.TextEdit_Parameter_DepositBatchID.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.TextEdit_Parameter_DepositBatchID.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
            Me.TextEdit_Parameter_DepositBatchID.Properties.DisplayFormat.FormatString = "f0"
            Me.TextEdit_Parameter_DepositBatchID.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.TextEdit_Parameter_DepositBatchID.Properties.EditFormat.FormatString = "f0"
            Me.TextEdit_Parameter_DepositBatchID.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.TextEdit_Parameter_DepositBatchID.Properties.Mask.BeepOnError = True
            Me.TextEdit_Parameter_DepositBatchID.Properties.Mask.EditMask = "f0"
            Me.TextEdit_Parameter_DepositBatchID.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
            Me.TextEdit_Parameter_DepositBatchID.Properties.ValidateOnEnterKey = True
            Me.TextEdit_Parameter_DepositBatchID.Size = New System.Drawing.Size(64, 20)
            Me.TextEdit_Parameter_DepositBatchID.TabIndex = 4
            Me.TextEdit_Parameter_DepositBatchID.ToolTip = "Enter a specific disbursement if it is not included in the above list"
            Me.TextEdit_Parameter_DepositBatchID.ToolTipController = Me.ToolTipController1
            '
            'LookUpEdit_Deposit
            '
            Me.LookUpEdit_Deposit.Location = New System.Drawing.Point(32, 26)
            Me.LookUpEdit_Deposit.Name = "LookUpEdit_Deposit"
            Me.LookUpEdit_Deposit.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.LookUpEdit_Deposit.Properties.NullText = ""
            Me.LookUpEdit_Deposit.Properties.PopupWidth = 500
            Me.LookUpEdit_Deposit.Size = New System.Drawing.Size(184, 20)
            Me.LookUpEdit_Deposit.TabIndex = 1
            Me.LookUpEdit_Deposit.ToolTip = "Choose a disbursement from the list if desired"
            Me.LookUpEdit_Deposit.ToolTipController = Me.ToolTipController1
            '
            'Label2
            '
            Me.Label2.Enabled = False
            Me.Label2.Location = New System.Drawing.Point(8, 129)
            Me.Label2.Name = "Label2"
            Me.Label2.Size = New System.Drawing.Size(38, 13)
            Me.Label2.TabIndex = 1
            Me.Label2.Text = "Sorting:"
            '
            'ComboBoxEdit_Sorting
            '
            Me.ComboBoxEdit_Sorting.EditValue = ""
            Me.ComboBoxEdit_Sorting.Location = New System.Drawing.Point(64, 127)
            Me.ComboBoxEdit_Sorting.Name = "ComboBoxEdit_Sorting"
            Me.ComboBoxEdit_Sorting.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.ComboBoxEdit_Sorting.Size = New System.Drawing.Size(176, 20)
            Me.ComboBoxEdit_Sorting.TabIndex = 2
            Me.ComboBoxEdit_Sorting.ToolTip = "How should the report be sorted?"
            Me.ComboBoxEdit_Sorting.ToolTipController = Me.ToolTipController1
            '
            'RequestParametersForm
            '
            Me.AutoScaleBaseSize = New System.Drawing.Size(5, 14)
            Me.ClientSize = New System.Drawing.Size(336, 152)
            Me.Controls.Add(Me.ComboBoxEdit_Sorting)
            Me.Controls.Add(Me.Label2)
            Me.Controls.Add(Me.GroupBox1)
            Me.Name = "RequestParametersForm"
            Me.ToolTipController1.SetSuperTip(Me, Nothing)
            Me.Text = "Lockbox Batch Report Parameters"
            Me.Controls.SetChildIndex(Me.ButtonOK, 0)
            Me.Controls.SetChildIndex(Me.ButtonCancel, 0)
            Me.Controls.SetChildIndex(Me.GroupBox1, 0)
            Me.Controls.SetChildIndex(Me.Label2, 0)
            Me.Controls.SetChildIndex(Me.ComboBoxEdit_Sorting, 0)
            CType(Me.DxErrorProvider1, System.ComponentModel.ISupportInitialize).EndInit()

            Me.GroupBox1.ResumeLayout(False)
            Me.GroupBox1.PerformLayout()
            CType(Me.TextEdit_Parameter_DepositBatchID.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LookUpEdit_Deposit.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.ComboBoxEdit_Sorting.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            Me.ResumeLayout(False)
            Me.PerformLayout()
        End Sub

#End Region

        Private Sub RequestParametersForm_Load(ByVal sender As Object, ByVal e As EventArgs)
            InInit = True

            ' Populate the sorting order dropdown
            With ComboBoxEdit_Sorting
                With .Properties
                    With .Items
                        .Clear()
                        .Add(New DebtPlus.Data.Controls.ComboboxItem("Entry Order", "deposit_batch_detail"))
                        .Add(New DebtPlus.Data.Controls.ComboboxItem("Client ID", "deposit_client"))
                        .Add(New DebtPlus.Data.Controls.ComboboxItem("Amount", "amount"))
                        .Add(New DebtPlus.Data.Controls.ComboboxItem("Client Name", "name"))
                        .Add(New DebtPlus.Data.Controls.ComboboxItem("Deposit Type", "subtype"))
                        .Add(New DebtPlus.Data.Controls.ComboboxItem("Date/Time Entered", "date_created"))
                        .Add(New DebtPlus.Data.Controls.ComboboxItem("Instrument Date", "item_date"))
                        .Add(New DebtPlus.Data.Controls.ComboboxItem("Reference Field", "reference"))
                    End With
                End With
                .SelectedIndex = 0      ' Make the item default to the first position
            End With

            ' If a batch is specified then don't bother to allow the user to change it.
            ' It comes from the application and it's value is correct.
            If Parameter_DepositBatchID > 0 Then
                RadioButton_List.Checked = False
                RadioButton_Text.Checked = True

                ' Disable the controls for selecting the batch number
                GroupBox1.Enabled = False

            Else

                ' Load the deposit registers
                load_deposit_registers()
            End If

            ' Enable or disable the OK button
            EnableOK()
            InInit = False
        End Sub

        Private Sub load_deposit_registers()

            ' Retrieve the database connection logic
            Try
                Using cmd As SqlCommand = New System.Data.SqlClient.SqlCommand()
                    With cmd
                        .Connection = New System.Data.SqlClient.SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
                        .CommandText = "SELECT TOP 50 ids.deposit_batch_id AS 'ID', ids.date_created AS 'Date', ids.created_by AS 'Creator', coalesce(bnk.description,'Bank #' + convert(varchar,ids.bank),'') as 'Bank', isnull(ids.note,'') as 'Note' FROM deposit_batch_ids ids WITH (NOLOCK) LEFT OUTER JOIN banks bnk WITH (NOLOCK) ON ids.bank = bnk.bank WHERE ids.date_posted IS NULL AND ids.batch_type='CL' ORDER BY 2 desc"
                        .CommandType = CommandType.Text
                        .CommandTimeout = 0
                    End With

                    ' Retrieve the disbursement table from the system
                    Using da As New SqlDataAdapter(cmd)
                        da.Fill(ds, "deposit_batch_ids")
                    End Using
                End Using
                Dim tbl As DataTable = ds.Tables(0)

                ' Define the values for the lookup control
                With LookUpEdit_Deposit
                    With .Properties
                        .DataSource = New DataView(tbl, "", "Date", DataViewRowState.CurrentRows)
                        .ShowFooter = False
                        .PopulateColumns()

                        ' The date and time are the default values
                        With .Columns("Date")
                            .FormatString = "MM/dd/yyyy hh:mm tt"
                            .Width = 90
                        End With

                        ' Populate the ID column with the disbursement register, showing it as a number
                        With .Columns("ID")
                            .FormatString = "f0"
                            .Alignment = HorzAlignment.Far
                            .Width = 40
                        End With

                        ' The display is the note, the value is the ID
                        .DisplayMember = "Note"
                        .ValueMember = "ID"
                    End With

                    ' Set the value to the first item
                    If tbl.Rows.Count > 0 Then
                        Dim row As DataRow = tbl.Rows(0)
                        .EditValue = row("ID")
                    End If

                    ' Populate the input value with the ID from the listbox
                    If .EditValue IsNot Nothing AndAlso .EditValue IsNot DBNull.Value Then
                        Parameter_DepositBatchID = Convert.ToInt32(.EditValue)
                    End If
                End With

            Catch ex As SqlException
                MessageBox.Show(ex.ToString(), "Error retrieving batch list", MessageBoxButtons.OK, MessageBoxIcon.Error)
            End Try
        End Sub

        Private Sub TextEdit_disbursement_register_EditValueChanged(ByVal sender As Object, ByVal e As EventArgs)
            EnableOK()
        End Sub

        Private Sub TextEdit_disbursement_register_KeyPress(ByVal sender As Object, ByVal e As KeyPressEventArgs)
            If Not Char.IsControl(e.KeyChar) Then
                If Not Char.IsDigit(e.KeyChar) Then e.Handled = True
            End If
        End Sub

        Private Sub LookUpEdit_Disbursement_EditValueChanged(ByVal sender As Object, ByVal e As EventArgs)
            Parameter_DepositBatchID = Convert.ToInt32(LookUpEdit_Deposit.EditValue)
            EnableOK()
        End Sub

        Private Sub EnableOK()
            ButtonOK.Enabled = (Parameter_DepositBatchID > 0)
        End Sub

        Private Sub RadioButton_List_CheckedChanged(ByVal sender As Object, ByVal e As EventArgs)
            If Not InInit Then
                ' Enable or disable the controls based upon the radio buttons
                TextEdit_Parameter_DepositBatchID.Enabled = RadioButton_Text.Checked
                LookUpEdit_Deposit.Enabled = RadioButton_List.Checked

                ' Go to the next control
                CType(sender, RadioButton).SelectNextControl(CType(sender, Control), True, True, False, True)
            End If
        End Sub
    End Class
End Namespace
