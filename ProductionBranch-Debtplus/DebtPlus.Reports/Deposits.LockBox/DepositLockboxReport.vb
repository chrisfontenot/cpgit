#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}

#End Region

Option Compare Binary
Option Explicit On
Option Strict On


Imports System.Drawing.Printing
Imports DevExpress.XtraReports.UI
Imports System.Data.SqlClient
Imports DebtPlus.Interfaces.Reports

Namespace Deposits.LockBox
    Public Class DepositLockboxReport

        ''' <summary>
        ''' Create an instance of our report
        ''' </summary>
        Public Sub New()
            MyBase.New()
            InitializeComponent()
            AddHandler BeforePrint, AddressOf Report_BeforePrint
            AddHandler XrPanel_InvalidItems.BeforePrint, AddressOf XrPanel_InvalidItems_BeforePrint
            ReportFilter.IsEnabled = True
        End Sub

        Public Overrides ReadOnly Property ReportTitle() As String
            Get
                Return "Lockbox Detail"
            End Get
        End Property

        Public Overrides ReadOnly Property ReportSubtitle() As String
            Get
                Return String.Format("This report reflects the contents of batch number {0:d}", Parameter_DepositBatchID)
            End Get
        End Property

        Private _SortOrder As String = String.Empty

        Public Property Parameter_SortOrder() As String
            Get
                Return _SortOrder
            End Get
            Set(ByVal Value As String)
                _SortOrder = Value
            End Set
        End Property

        Private _DepositBatchID As Int32 = -1

        Public Property Parameter_DepositBatchID() As Int32
            Get
                Return _DepositBatchID
            End Get
            Set(ByVal Value As Int32)
                _DepositBatchID = Value
            End Set
        End Property

        Public Overrides Sub SetReportParameter(ByVal Parameter As String, ByVal Value As Object)
            Select Case Parameter
                Case "DepositBatchID"
                    Parameter_DepositBatchID = Convert.ToInt32(Value)
                Case "SortOrder"
                    Parameter_SortOrder = Convert.ToString(Value)
                Case Else
                    MyBase.SetReportParameter(Parameter, Value)
            End Select
        End Sub

        Public Overrides Function NeedParameters() As Boolean
            Return MyBase.NeedParameters() OrElse Parameter_DepositBatchID <= 0
        End Function

        Public Overrides Function RequestReportParameters() As DialogResult
            Dim answer As DialogResult = DialogResult.OK
            If NeedParameters() Then
                If Parameter_DepositBatchID <= 0 Then
                    Using frm As New DepositLockboxParametersForm()
                        answer = frm.ShowDialog()
                        Parameter_DepositBatchID = frm.Parameter_DepositBatchID
                        Parameter_SortOrder = frm.sort_order
                    End Using
                End If
            End If
            Return answer
        End Function

        Const TableName As String = "rpt_LockboxDetails"
        Dim ds As New DataSet("ds")

        Private Sub Report_BeforePrint(ByVal sender As Object, ByVal e As PrintEventArgs)
            Dim rpt As XtraReport = CType(sender, XtraReport)

            Dim tbl As DataTable = ds.Tables(TableName)
            If tbl Is Nothing Then
                Try
                    Using cn As SqlConnection = New System.Data.SqlClient.SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
                        cn.Open()
                        Using cmd As SqlCommand = New System.Data.SqlClient.SqlCommand()
                            cmd.Connection = cn
                            cmd.CommandText = "rpt_LockboxDetails"
                            cmd.CommandType = CommandType.StoredProcedure
                            SqlCommandBuilder.DeriveParameters(cmd)

                            cmd.Parameters(1).Value = Parameter_DepositBatchID
                            cmd.CommandTimeout = 0

                            Using da As New SqlDataAdapter(cmd)
                                da.Fill(ds, TableName)
                                tbl = ds.Tables(TableName)
                                tbl.Columns.Add("fmt_ok_to_post", GetType(String), "iif([ok_to_post] <> 0,'Y','N')")
                                tbl.Columns.Add("amt_reserved", GetType(Decimal), "iif([ok_to_post] = 0,0 - [amount],0)")
                                tbl.Columns.Add("amt_net", GetType(Decimal), "iif([ok_to_post] = 0,0,[amount])")
                            End Using
                        End Using
                    End Using

                Catch ex As SqlException
                    DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error reading report information")
                End Try
            End If

            ' Bind the report to the dataset
            If tbl IsNot Nothing Then
                rpt.DataSource = New DataView(tbl, CType(rpt, DebtPlus.Interfaces.Reports.IReportFilter).ReportFilter.ViewFilter, Parameter_SortOrder, DataViewRowState.CurrentRows)
                For Each calc As CalculatedField In rpt.CalculatedFields
                    calc.Assign(rpt.DataSource, rpt.DataMember)
                Next
            End If
        End Sub

        Private Sub XrPanel_InvalidItems_BeforePrint(ByVal sender As Object, ByVal e As PrintEventArgs)
            With CType(sender, XRPanel)
                Dim rpt As XtraReport = CType(.Report, XtraReport)
                Dim vue As DataView = CType(rpt.DataSource, DataView)
                Dim Total As Object = vue.Table.Compute("sum([amt_reserved])", vue.RowFilter)
                .Visible = DebtPlus.Utils.Nulls.DDec(Total) <> 0D
            End With
        End Sub
    End Class
End Namespace
