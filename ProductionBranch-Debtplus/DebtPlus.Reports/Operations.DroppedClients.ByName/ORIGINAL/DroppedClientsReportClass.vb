#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}
#End Region

Option Compare Binary
Option Explicit On
Option Strict On

Imports DebtPlus.Reports
Imports DevExpress.XtraReports.UI

Namespace Operations.DroppedClients.ByName
    Public Class DroppedClientsReportClass

        Public Sub New()
            MyBase.New()
            InitializeComponent()

            AddHandler XrLabel_client.BeforePrint, AddressOf XrLabel_client_BeforePrint
            AddHandler ParametersRequestBeforeShow, AddressOf DroppedClientsReportClass_ParametersRequestBeforeShow
            AddHandler BeforePrint, AddressOf DroppedClientsReportClass_BeforePrint
        End Sub

        Public Overrides ReadOnly Property ReportTitle() As String
            Get
                Return "Dropped Clients By Name"
            End Get
        End Property

        Public Overrides ReadOnly Property ReportSubTitle() As String
            Get
                Dim OfficeName As String = GetOfficeName(Parameter_Office)
                If OfficeName = String.Empty Then
                    OfficeName = " in all offices"
                Else
                    OfficeName = " in office '" + OfficeName + "'"
                End If
                Return MyBase.ReportSubtitle + OfficeName
            End Get
        End Property

        Private Function GetOfficeName(ByVal Office As Object) As String
            Dim Answer As String = String.Empty
            If Office IsNot Nothing AndAlso Office IsNot System.DBNull.Value Then
                Dim tbl As System.Data.DataTable = GetOfficesTable()
                If tbl IsNot Nothing Then
                    Dim value As System.Int32 = Convert.ToInt32(Office)
                    If value > 0 Then
                        Dim row As System.Data.DataRow = tbl.Rows.Find(value)
                        If row IsNot Nothing AndAlso row("description") IsNot System.DBNull.Value Then
                            Answer = Convert.ToString(row("description")).Trim
                        End If
                    End If
                End If
            End If
            Return Answer
        End Function

        Public Property Parameter_Office() As Int32
            Get
                Dim parm As DevExpress.XtraReports.Parameters.Parameter = FindParameter("ParameterOffice")
                If parm Is Nothing Then Return -1
                Return Convert.ToInt32(parm.Value)
            End Get
            Set(ByVal value As Int32)
                SetParameter("ParameterOffice", GetType(Int32), value, "Office ID", False)
            End Set
        End Property

        Public Overrides Sub SetReportParameter(ByVal Parameter As String, ByVal Value As Object)
            Select Case Parameter
                Case "Office"
                    Parameter_Office = Convert.ToInt32(Value)
                Case Else
                    MyBase.SetReportParameter(Parameter, Value)
            End Select
        End Sub

        Public Overrides Function RequestReportParameters() As System.Windows.Forms.DialogResult
            Dim Answer As System.Windows.Forms.DialogResult = DialogResult.OK
            If NeedParameters() Then
                Using frm As New DebtPlus.Reports.Template.Forms.DatedOfficeParametersForm()
                    Answer = frm.ShowDialog()
                    Parameter_Office = If(frm.Parameter_Office Is DBNull.Value, -1, Convert.ToInt32(frm.Parameter_Office))
                    Parameter_FromDate = frm.Parameter_FromDate
                    Parameter_ToDate = frm.Parameter_ToDate
                End Using
            End If
            Return Answer
        End Function

        Dim ds As New System.Data.DataSet("ds")

        Private Sub DroppedClientsReportClass_ParametersRequestBeforeShow(ByVal sender As Object, ByVal e As DevExpress.XtraReports.Parameters.ParametersRequestEventArgs)
            For Each pi As DevExpress.XtraReports.Parameters.ParameterInfo In e.ParametersInformation
                If pi.Parameter.Name = "ParameterOffice" Then
                    pi.Editor = GetOfficeEditor()
                End If
            Next
        End Sub

        Private Function GetOfficeEditor() As System.Windows.Forms.Control
            Dim ctl As New DevExpress.XtraEditors.LookUpEdit
            With ctl
                With .Properties
                    .DataSource = New System.Data.DataView(GetOfficesTable, String.Empty, "description", System.Data.DataViewRowState.CurrentRows)
                    .Columns.AddRange(New DevExpress.XtraEditors.Controls.LookUpColumnInfo() {New DevExpress.XtraEditors.Controls.LookUpColumnInfo("item_key", "ID", 5, DevExpress.Utils.FormatType.Numeric, "f0", True, DevExpress.Utils.HorzAlignment.[Default]), New DevExpress.XtraEditors.Controls.LookUpColumnInfo("description", "Name", 20, DevExpress.Utils.FormatType.None, "", True, DevExpress.Utils.HorzAlignment.[Default], DevExpress.Data.ColumnSortOrder.Descending), New DevExpress.XtraEditors.Controls.LookUpColumnInfo("TypeName", 15, "Type")})
                    .DisplayMember = "description"
                    .NullText = "[All Offices]"
                    .ShowFooter = False
                    .ValueMember = "item_key"
                End With
            End With

            Return ctl
        End Function

        Private Function GetOfficesTable() As System.Data.DataTable
            Dim sqlInfo As DebtPlus.LINQ.SQLInfoClass = DebtPlus.LINQ.SQLInfoClass.getDefault()
            Dim tbl As System.Data.DataTable = ds.Tables("lst_offices")
            If tbl Is Nothing Then
                Try
                    Using cn As System.Data.SqlClient.SqlConnection = New System.Data.SqlClient.SqlConnection(SqlInfo.ConnectionString)
                        cn.Open()
                        Using cmd As System.Data.SqlClient.SqlCommand = New System.Data.SqlClient.SqlCommand()
                            cmd.Connection = cn
                            cmd.CommandText = "lst_offices"
                            cmd.CommandType = System.Data.CommandType.StoredProcedure

                            Using da As New System.Data.SqlClient.SqlDataAdapter(cmd)
                                da.Fill(ds, "lst_offices")
                                tbl = ds.Tables("lst_offices")
                                If tbl.PrimaryKey.GetUpperBound(0) < 0 Then tbl.PrimaryKey = New System.Data.DataColumn() {tbl.Columns("item_key")}

                                Dim row As System.Data.DataRow = tbl.NewRow
                                row("item_key") = -1
                                row("description") = "[All Offices]"
                                row("Default") = False
                                row("ActiveFlag") = True
                                tbl.Rows.Add(row)
                            End Using
                        End Using
                    End Using

                Catch ex As System.Data.SqlClient.SqlException
                    Using gdr As New DebtPlus.Repository.GetDataResult()
                        gdr.HandleException(ex)
                        DebtPlus.UI.Common.ErrorHandling.HandleErrors(gdr, "Error reading office list")
                    End Using
                End Try
            End If

            Return tbl
        End Function

        Private Sub DroppedClientsReportClass_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs)
            Const TableName As String = "rpt_dropped_clients"
            Dim rpt As DevExpress.XtraReports.UI.XtraReport = CType(sender, DevExpress.XtraReports.UI.XtraReport)
            Dim sqlInfo As DebtPlus.LINQ.SQLInfoClass = DebtPlus.LINQ.SQLInfoClass.getDefault()
            Dim tbl As System.Data.DataTable = ds.Tables(TableName)

            If tbl IsNot Nothing Then
                tbl.Clear()
            End If

            Try
                Using cn As System.Data.SqlClient.SqlConnection = New System.Data.SqlClient.SqlConnection(sqlInfo.ConnectionString)
                    cn.Open()
                    Using cmd As New System.Data.SqlClient.SqlCommand()
                        cmd.Connection = cn
                        cmd.CommandText = TableName
                        cmd.CommandType = System.Data.CommandType.StoredProcedure
                        System.Data.SqlClient.SqlCommandBuilder.DeriveParameters(cmd)

                        cmd.Parameters(1).Value = rpt.Parameters("ParameterFromDate").Value
                        cmd.Parameters(2).Value = rpt.Parameters("ParameterToDate").Value

                        Dim v As Object = rpt.Parameters("ParameterOffice").Value
                        If v IsNot Nothing AndAlso v IsNot System.DBNull.Value AndAlso Convert.ToInt32(v) > 0 Then
                            cmd.Parameters(3).Value = v
                        End If

                        cmd.CommandTimeout = 0

                        Using da As New System.Data.SqlClient.SqlDataAdapter(cmd)
                            da.Fill(ds, TableName)
                            tbl = ds.Tables(TableName)
                            If Not tbl.Columns.Contains("pct_paid") Then
                                tbl.Columns.Add("pct_paid", GetType(Double), "iif(isnull([original_balance],0) = 0, 0, [paid_to_date]/[original_balance])")
                            End If

                            rpt.DataSource = tbl.DefaultView
                            For Each item As DevExpress.XtraReports.UI.CalculatedField In rpt.CalculatedFields
                                item.Assign(rpt.DataSource, rpt.DataMember)
                            Next
                        End Using
                    End Using
                End Using

            Catch ex As System.Data.SqlClient.SqlException
                Using gdr As New DebtPlus.Repository.GetDataResult()
                    gdr.HandleException(ex)
                    DebtPlus.UI.Common.ErrorHandling.HandleErrors(gdr, "Error reading report information")
                End Using
            End Try
        End Sub

        Private Sub XrLabel_client_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs)
            Dim lbl As DevExpress.XtraReports.UI.XRLabel = CType(sender, DevExpress.XtraReports.UI.XRLabel)
            lbl.Text = DebtPlus.Utils.Format.Client.FormatClientID(lbl.Report.GetCurrentColumnValue("client"))
        End Sub
    End Class
End Namespace
