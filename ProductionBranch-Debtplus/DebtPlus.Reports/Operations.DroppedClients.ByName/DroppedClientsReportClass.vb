#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}

#End Region

Option Compare Binary
Option Explicit On
Option Strict On

Imports System.ComponentModel
Imports DevExpress.Data
Imports DevExpress.XtraEditors.Controls
Imports DebtPlus.Reports.Template

Imports DevExpress.XtraReports.Parameters
Imports DebtPlus.Utils
Imports System.IO
Imports System.Reflection
Imports DebtPlus.Reports.Template.Forms
Imports DevExpress.XtraEditors
Imports DevExpress.Utils
Imports System.Data.SqlClient

Namespace Operations.DroppedClients.ByName
    Public Class DroppedClientsReportClass
        Inherits DatedTemplateXtraReportClass

        Public Sub New()
            MyBase.New()
            AddHandler Me.ParametersRequestBeforeShow, AddressOf DroppedClientsReportClass_ParametersRequestBeforeShow

            Const ReportName As String = "DebtPlus.Reports.Operations.DroppedClients.ByName.repx"

            ' See if there is a report reference that we can use
            Dim Fname As String = Path.Combine(DebtPlus.Configuration.Config.ReportsDirectory, ReportName)
            Dim UseDefault As Boolean = True
            Try
                If (Not DebtPlus.Configuration.Config.LocalReportsOnly) AndAlso System.IO.File.Exists(Fname) Then
                    LoadLayout(Fname)
                    UseDefault = False
                End If
            Catch ex As Exception
            End Try

            ' Load the standard report definition if we need a new item
            If UseDefault Then
                Dim asm As Assembly = Assembly.GetExecutingAssembly                                           ' changed
                Using ios As Stream = asm.GetManifestResourceStream(String.Format("DebtPlus.Reports.{0}", ReportName)) ' changed
                    If ios IsNot Nothing Then LoadLayout(ios) '                                                                       changed
                End Using
            End If

            ' Set the script references as needed
            ScriptReferences = DebtPlus.Utils.AssemblyTools.GetAllSiblingAssemblies()

            ' Set the default values for the parameters
            Parameters("ParameterFromDate").Value = Now.Date
            Parameters("ParameterToDate").Value = Now.Date
            Parameters("ParameterOffice").Value = -1
        End Sub

        Public Overrides ReadOnly Property ReportTitle() As String
            Get
                Return "Dropped Clients By Name"
            End Get
        End Property

        Public Overrides ReadOnly Property ReportSubTitle() As String
            Get
                Dim OfficeName As String = GetOfficeName(Parameter_Office)
                If OfficeName = String.Empty Then
                    OfficeName = " in all offices"
                Else
                    OfficeName = " in office '" + OfficeName + "'"
                End If
                Return MyBase.ReportSubtitle + OfficeName
            End Get
        End Property

        Private Function GetOfficeName(ByVal Office As Object) As String
            Dim Answer As String = String.Empty
            If Office IsNot Nothing AndAlso Office IsNot DBNull.Value Then
                Dim tbl As DataTable = GetOfficesTable()
                If tbl IsNot Nothing Then
                    Dim value As Int32 = Convert.ToInt32(Office)
                    If value > 0 Then
                        Dim row As DataRow = tbl.Rows.Find(value)
                        If row IsNot Nothing AndAlso row("description") IsNot DBNull.Value Then
                            Answer = Convert.ToString(row("description")).Trim()
                        End If
                    End If
                End If
            End If
            Return Answer
        End Function

        Public Property Parameter_Office() As Object
            Get
                Return Parameters("ParameterOffice").Value
            End Get
            Set(ByVal value As Object)
                Parameters("ParameterOffice").Value = value
            End Set
        End Property

        Public Overrides Sub SetReportParameter(ByVal Parameter As String, ByVal Value As Object)
            Select Case Parameter
                Case "Office"
                    Parameter_Office = Value
                Case Else
                    MyBase.SetReportParameter(Parameter, Value)
            End Select
        End Sub

        Public Overrides Function RequestReportParameters() As DialogResult
            Dim answer As DialogResult = DialogResult.OK
            If NeedParameters() Then
                Using frm As New DatedOfficeParametersForm()
                    With frm
                        answer = .ShowDialog()
                        Parameter_Office = .Parameter_Office
                        Parameter_FromDate = .Parameter_FromDate
                        Parameter_ToDate = .Parameter_ToDate
                    End With
                End Using
            End If
            Return answer
        End Function

        Private Sub DroppedClientsReportClass_ParametersRequestBeforeShow(ByVal sender As Object, ByVal e As ParametersRequestEventArgs)
            For Each pi As DevExpress.XtraReports.Parameters.ParameterInfo In e.ParametersInformation
                If pi.Parameter.Name = "ParameterOffice" Then
                    pi.Editor = GetOfficeEditor()
                End If
            Next
        End Sub

        Private Function GetOfficeEditor() As Control
            Dim ctl As New LookUpEdit
            With ctl
                With .Properties
                    .DataSource = New DataView(GetOfficesTable, String.Empty, "description", DataViewRowState.CurrentRows)
                    .Columns.AddRange(New LookUpColumnInfo() {New LookUpColumnInfo("item_key", "ID", 5, FormatType.Numeric, "f0", True, HorzAlignment.[Default]), New LookUpColumnInfo("description", "Name", 20, FormatType.None, "", True, HorzAlignment.[Default], ColumnSortOrder.Descending), New LookUpColumnInfo("TypeName", 15, "Type")})
                    .DisplayMember = "description"
                    .NullText = "[All Offices]"
                    .ShowFooter = False
                    .ValueMember = "item_key"
                End With
            End With

            Return ctl
        End Function

        Private Function GetOfficesTable() As DataTable
            Dim tbl As DataTable = ds.Tables("lst_offices")
            If tbl Is Nothing Then
                Try
                    Using cn As SqlConnection = New System.Data.SqlClient.SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
                        cn.Open()
                        Using cmd As SqlCommand = New System.Data.SqlClient.SqlCommand()
                            cmd.Connection = cn
                            cmd.CommandText = "lst_offices"
                            cmd.CommandType = CommandType.StoredProcedure

                            Using da As New SqlDataAdapter(cmd)
                                da.Fill(ds, "lst_offices")

                                tbl = ds.Tables("lst_offices")
                                If tbl.PrimaryKey.GetUpperBound(0) < 0 Then
                                    tbl.PrimaryKey = New DataColumn() {tbl.Columns("item_key")}
                                End If

                                Dim row As DataRow = tbl.NewRow()
                                row("item_key") = -1
                                row("description") = "[All Offices]"
                                row("Default") = False
                                row("ActiveFlag") = True
                                tbl.Rows.Add(row)
                            End Using
                        End Using
                    End Using

                Catch ex As SqlException
                    DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error reading office list")
                End Try
            End If

            Return tbl
        End Function

        Dim ds As New DataSet("ds")
    End Class
End Namespace

