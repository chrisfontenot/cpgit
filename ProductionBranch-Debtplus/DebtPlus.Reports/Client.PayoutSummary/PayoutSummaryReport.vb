#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}
#End Region

Imports DebtPlus.Interfaces.Debt
Imports DebtPlus.Reports.Client.PayoutDetail

Namespace Client.PayoutSummary
    Public Class PayoutSummaryReport
        Inherits ClientPayoutDetail

        Public Sub New()
            MyBase.New()
            InitializeComponent()
            AddHandler Me.BeforePrint, AddressOf Report_BeforePrint
        End Sub

        ''' <summary>
        ''' Create a report and override the previous report's initialization event
        ''' </summary>
        Private Sub Report_BeforePrint(ByVal sender As Object, ByVal e As System.EventArgs)

            ' Bind the data to the corresponding view if needed
            If vue Is Nothing Then
                If ReportTbl Is Nothing Then

                    ' Read the client information so that we may update the payout data
                    Using bc As New DebtPlus.LINQ.BusinessContext()
                        Dim clientRecord As DebtPlus.LINQ.client = bc.clients.Where(Function(s) s.Id = ClientId).FirstOrDefault()

                        ' The client should exist. If it does not then we can't do the update but still allocate the storage for it so that we can do the payout
                        If clientRecord Is Nothing Then
                            clientRecord = New DebtPlus.LINQ.client()
                        End If

                        ' Generate the payout details. This will update the clientrecord
                        ReportTbl = CreateTable(clientRecord)

                        ' Put the changes back into the database now.
                        bc.SubmitChanges()
                    End Using
                End If

                vue = New System.Data.DataView(ReportTbl, String.Empty, "date, ID, creditor, account_number", DataViewRowState.CurrentRows)
                DataSource = vue
            End If

            XrLabel_summary_date.DataBindings.Add("Text", vue, "date", "{0:d}")
        End Sub
    End Class

End Namespace
