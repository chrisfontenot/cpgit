Namespace Client.PayoutSummary
    <Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
    Partial Class PayoutSummaryReport

        'XtraReport overrides dispose to clean up the component list.
        <System.Diagnostics.DebuggerNonUserCode()> _
        Protected Overrides Sub Dispose(ByVal disposing As Boolean)
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
            MyBase.Dispose(disposing)
        End Sub

        'Required by the Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Designer
        'It can be modified using the Designer.  
        'Do not modify it using the code editor.
        <System.Diagnostics.DebuggerStepThrough()> _
        Private Sub InitializeComponent()
            Me.XrLabel_summary_date = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel12 = New DevExpress.XtraReports.UI.XRLabel
            CType(Me.ds, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.XrRichText1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.result, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
            '
            'XrLabel_balance
            '
            Me.XrLabel_balance.Location = New System.Drawing.Point(96, 0)
            Me.XrLabel_balance.Size = New System.Drawing.Size(8, 15)
            Me.XrLabel_balance.StylePriority.UseTextAlignment = False
            '
            'XrLabel_interest
            '
            Me.XrLabel_interest.Location = New System.Drawing.Point(80, 0)
            Me.XrLabel_interest.Size = New System.Drawing.Size(8, 15)
            Me.XrLabel_interest.StylePriority.UseTextAlignment = False
            '
            'XrLabel_payment
            '
            Me.XrLabel_payment.Location = New System.Drawing.Point(48, 0)
            Me.XrLabel_payment.Size = New System.Drawing.Size(8, 15)
            Me.XrLabel_payment.StylePriority.UseTextAlignment = False
            '
            'XrLabel_creditor_name
            '
            Me.XrLabel_creditor_name.Location = New System.Drawing.Point(16, 0)
            Me.XrLabel_creditor_name.Size = New System.Drawing.Size(8, 15)
            Me.XrLabel_creditor_name.StylePriority.UseTextAlignment = False
            '
            'XrLabel_date
            '
            Me.XrLabel_date.Size = New System.Drawing.Size(8, 15)
            Me.XrLabel_date.StylePriority.UseTextAlignment = False
            '
            'XrLabel_account_number
            '
            Me.XrLabel_account_number.Location = New System.Drawing.Point(32, 0)
            Me.XrLabel_account_number.Size = New System.Drawing.Size(8, 15)
            Me.XrLabel_account_number.StylePriority.UseTextAlignment = False
            '
            'GroupFooter1
            '
            Me.GroupFooter1.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel_summary_date})
            Me.GroupFooter1.Height = 15
            Me.GroupFooter1.Controls.SetChildIndex(Me.XrLabel_total_payment, 0)
            Me.GroupFooter1.Controls.SetChildIndex(Me.XrLabel_total_interest, 0)
            Me.GroupFooter1.Controls.SetChildIndex(Me.XrLabel_total_balance, 0)
            Me.GroupFooter1.Controls.SetChildIndex(Me.XrLabel5, 0)
            Me.GroupFooter1.Controls.SetChildIndex(Me.XrLabel_total_fees, 0)
            Me.GroupFooter1.Controls.SetChildIndex(Me.XrLabel_summary_date, 0)
            '
            'XrLabel5
            '
            Me.XrLabel5.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.XrLabel5.Location = New System.Drawing.Point(658, 0)
            Me.XrLabel5.Size = New System.Drawing.Size(8, 15)
            Me.XrLabel5.StylePriority.UseFont = False
            Me.XrLabel5.StylePriority.UseTextAlignment = False
            Me.XrLabel5.Visible = False
            '
            'XrLabel_total_balance
            '
            Me.XrLabel_total_balance.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.XrLabel_total_balance.Location = New System.Drawing.Point(383, 0)
            Me.XrLabel_total_balance.Size = New System.Drawing.Size(117, 15)
            Me.XrLabel_total_balance.StylePriority.UseFont = False
            Me.XrLabel_total_balance.StylePriority.UseTextAlignment = False
            '
            'XrLabel_total_interest
            '
            Me.XrLabel_total_interest.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.XrLabel_total_interest.Location = New System.Drawing.Point(283, 0)
            Me.XrLabel_total_interest.Size = New System.Drawing.Size(92, 15)
            Me.XrLabel_total_interest.StylePriority.UseFont = False
            Me.XrLabel_total_interest.StylePriority.UseTextAlignment = False
            '
            'XrLabel_total_payment
            '
            Me.XrLabel_total_payment.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.XrLabel_total_payment.Location = New System.Drawing.Point(83, 0)
            Me.XrLabel_total_payment.Size = New System.Drawing.Size(92, 15)
            Me.XrLabel_total_payment.StylePriority.UseFont = False
            Me.XrLabel_total_payment.StylePriority.UseTextAlignment = False
            '
            'XrPanel1
            '
            Me.XrPanel1.StylePriority.UseBackColor = False
            '
            'XrLabel8
            '
            Me.XrLabel8.StylePriority.UseBackColor = False
            Me.XrLabel8.StylePriority.UseBorderColor = False
            Me.XrLabel8.StylePriority.UseFont = False
            Me.XrLabel8.StylePriority.UseForeColor = False
            Me.XrLabel8.StylePriority.UseTextAlignment = False
            '
            'XrLabel7
            '
            Me.XrLabel7.Location = New System.Drawing.Point(300, 0)
            Me.XrLabel7.StylePriority.UseBackColor = False
            Me.XrLabel7.StylePriority.UseBorderColor = False
            Me.XrLabel7.StylePriority.UseFont = False
            Me.XrLabel7.StylePriority.UseForeColor = False
            Me.XrLabel7.StylePriority.UseTextAlignment = False
            '
            'XrLabel6
            '
            Me.XrLabel6.Location = New System.Drawing.Point(100, 0)
            Me.XrLabel6.StylePriority.UseBackColor = False
            Me.XrLabel6.StylePriority.UseBorderColor = False
            Me.XrLabel6.StylePriority.UseFont = False
            Me.XrLabel6.StylePriority.UseForeColor = False
            Me.XrLabel6.StylePriority.UseTextAlignment = False
            '
            'XrLabel4
            '
            Me.XrLabel4.Location = New System.Drawing.Point(700, 0)
            Me.XrLabel4.Size = New System.Drawing.Size(8, 15)
            Me.XrLabel4.StylePriority.UseBackColor = False
            Me.XrLabel4.StylePriority.UseBorderColor = False
            Me.XrLabel4.StylePriority.UseFont = False
            Me.XrLabel4.StylePriority.UseForeColor = False
            Me.XrLabel4.StylePriority.UseTextAlignment = False
            Me.XrLabel4.Visible = False
            '
            'XrLabel3
            '
            Me.XrLabel3.Location = New System.Drawing.Point(708, 0)
            Me.XrLabel3.Size = New System.Drawing.Size(8, 15)
            Me.XrLabel3.StylePriority.UseBackColor = False
            Me.XrLabel3.StylePriority.UseBorderColor = False
            Me.XrLabel3.StylePriority.UseFont = False
            Me.XrLabel3.StylePriority.UseForeColor = False
            Me.XrLabel3.StylePriority.UseTextAlignment = False
            Me.XrLabel3.Visible = False
            '
            'XrLabel1
            '
            Me.XrLabel1.Location = New System.Drawing.Point(400, 0)
            Me.XrLabel1.StylePriority.UseBackColor = False
            Me.XrLabel1.StylePriority.UseBorderColor = False
            Me.XrLabel1.StylePriority.UseFont = False
            Me.XrLabel1.StylePriority.UseForeColor = False
            Me.XrLabel1.StylePriority.UseTextAlignment = False
            '
            'ReportFooter
            '
            Me.ReportFooter.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel12})
            Me.ReportFooter.Height = 141
            Me.ReportFooter.KeepTogether = True
            Me.ReportFooter.Controls.SetChildIndex(Me.XrRichText1, 0)
            Me.ReportFooter.Controls.SetChildIndex(Me.XrLabel2, 0)
            Me.ReportFooter.Controls.SetChildIndex(Me.XrLabel9, 0)
            Me.ReportFooter.Controls.SetChildIndex(Me.XrLabel_number_of_months, 0)
            Me.ReportFooter.Controls.SetChildIndex(Me.XrLabel_grand_total_balance, 0)
            Me.ReportFooter.Controls.SetChildIndex(Me.XrLine1, 0)
            Me.ReportFooter.Controls.SetChildIndex(Me.XrLabel11, 0)
            Me.ReportFooter.Controls.SetChildIndex(Me.XrLabel_grand_total_interest, 0)
            Me.ReportFooter.Controls.SetChildIndex(Me.XrLabel13, 0)
            Me.ReportFooter.Controls.SetChildIndex(Me.XrLabel_grand_total_fees, 0)
            Me.ReportFooter.Controls.SetChildIndex(Me.XrLabel15, 0)
            Me.ReportFooter.Controls.SetChildIndex(Me.XrLabel_grand_total_payments, 0)
            Me.ReportFooter.Controls.SetChildIndex(Me.XrLabel12, 0)
            '
            'XrLabel2
            '
            Me.XrLabel2.Location = New System.Drawing.Point(8, 58)
            Me.XrLabel2.StylePriority.UseFont = False
            Me.XrLabel2.StylePriority.UseForeColor = False
            '
            'XrLabel_grand_total_balance
            '
            Me.XrLabel_grand_total_balance.Location = New System.Drawing.Point(225, 42)
            Me.XrLabel_grand_total_balance.StylePriority.UseForeColor = False
            Me.XrLabel_grand_total_balance.StylePriority.UseTextAlignment = False
            '
            'XrLabel_number_of_months
            '
            Me.XrLabel_number_of_months.Location = New System.Drawing.Point(225, 58)
            Me.XrLabel_number_of_months.StylePriority.UseForeColor = False
            Me.XrLabel_number_of_months.StylePriority.UseTextAlignment = False
            '
            'XrLabel9
            '
            Me.XrLabel9.Location = New System.Drawing.Point(8, 42)
            Me.XrLabel9.StylePriority.UseFont = False
            Me.XrLabel9.StylePriority.UseForeColor = False
            '
            'XrLine1
            '
            Me.XrLine1.StylePriority.UseForeColor = False
            '
            'XrLabel_fees
            '
            Me.XrLabel_fees.Location = New System.Drawing.Point(64, 0)
            Me.XrLabel_fees.Size = New System.Drawing.Size(8, 15)
            Me.XrLabel_fees.StylePriority.UseTextAlignment = False
            '
            'XrLabel10
            '
            Me.XrLabel10.Location = New System.Drawing.Point(233, 0)
            Me.XrLabel10.StylePriority.UseBackColor = False
            Me.XrLabel10.StylePriority.UseBorderColor = False
            Me.XrLabel10.StylePriority.UseFont = False
            Me.XrLabel10.StylePriority.UseForeColor = False
            Me.XrLabel10.StylePriority.UseTextAlignment = False
            '
            'XrLabel_total_fees
            '
            Me.XrLabel_total_fees.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.XrLabel_total_fees.Location = New System.Drawing.Point(183, 0)
            Me.XrLabel_total_fees.Size = New System.Drawing.Size(92, 15)
            Me.XrLabel_total_fees.StylePriority.UseFont = False
            Me.XrLabel_total_fees.StylePriority.UseTextAlignment = False
            '
            'XrLabel_grand_total_payments
            '
            Me.XrLabel_grand_total_payments.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.XrLabel_grand_total_payments.Location = New System.Drawing.Point(75, 17)
            Me.XrLabel_grand_total_payments.Size = New System.Drawing.Size(100, 17)
            Me.XrLabel_grand_total_payments.StylePriority.UseFont = False
            Me.XrLabel_grand_total_payments.StylePriority.UseForeColor = False
            Me.XrLabel_grand_total_payments.StylePriority.UseTextAlignment = False
            '
            'XrLabel15
            '
            Me.XrLabel15.Location = New System.Drawing.Point(433, 25)
            Me.XrLabel15.Size = New System.Drawing.Size(2, 16)
            Me.XrLabel15.StylePriority.UseFont = False
            Me.XrLabel15.StylePriority.UseForeColor = False
            Me.XrLabel15.Visible = False
            '
            'XrLabel_grand_total_fees
            '
            Me.XrLabel_grand_total_fees.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.XrLabel_grand_total_fees.Location = New System.Drawing.Point(183, 17)
            Me.XrLabel_grand_total_fees.Size = New System.Drawing.Size(92, 16)
            Me.XrLabel_grand_total_fees.StylePriority.UseFont = False
            Me.XrLabel_grand_total_fees.StylePriority.UseForeColor = False
            Me.XrLabel_grand_total_fees.StylePriority.UseTextAlignment = False
            '
            'XrLabel13
            '
            Me.XrLabel13.Location = New System.Drawing.Point(417, 25)
            Me.XrLabel13.Size = New System.Drawing.Size(2, 16)
            Me.XrLabel13.StylePriority.UseFont = False
            Me.XrLabel13.StylePriority.UseForeColor = False
            Me.XrLabel13.Visible = False
            '
            'XrLabel_grand_total_interest
            '
            Me.XrLabel_grand_total_interest.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.XrLabel_grand_total_interest.Location = New System.Drawing.Point(283, 17)
            Me.XrLabel_grand_total_interest.Size = New System.Drawing.Size(92, 16)
            Me.XrLabel_grand_total_interest.StylePriority.UseFont = False
            Me.XrLabel_grand_total_interest.StylePriority.UseForeColor = False
            Me.XrLabel_grand_total_interest.StylePriority.UseTextAlignment = False
            '
            'XrLabel11
            '
            Me.XrLabel11.Location = New System.Drawing.Point(425, 25)
            Me.XrLabel11.Size = New System.Drawing.Size(2, 16)
            Me.XrLabel11.StylePriority.UseFont = False
            Me.XrLabel11.StylePriority.UseForeColor = False
            Me.XrLabel11.Visible = False
            '
            'XrBarCode_PostalCode
            '
            Me.XrBarCode_PostalCode.StylePriority.UsePadding = False
            '
            'XrRichText1
            '
            Me.XrRichText1.Location = New System.Drawing.Point(8, 83)
            Me.XrRichText1.Size = New System.Drawing.Size(792, 58)
            '
            'Detail
            '
            Me.Detail.Visible = False
            '
            'XrLabel_summary_date
            '
            Me.XrLabel_summary_date.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.XrLabel_summary_date.Location = New System.Drawing.Point(0, 0)
            Me.XrLabel_summary_date.Name = "XrLabel_summary_date"
            Me.XrLabel_summary_date.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_summary_date.Size = New System.Drawing.Size(75, 15)
            Me.XrLabel_summary_date.StylePriority.UseFont = False
            Me.XrLabel_summary_date.StylePriority.UseTextAlignment = False
            Me.XrLabel_summary_date.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel12
            '
            Me.XrLabel12.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.XrLabel12.ForeColor = System.Drawing.Color.Teal
            Me.XrLabel12.Location = New System.Drawing.Point(8, 17)
            Me.XrLabel12.Name = "XrLabel12"
            Me.XrLabel12.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel12.Size = New System.Drawing.Size(67, 16)
            Me.XrLabel12.StylePriority.UseFont = False
            Me.XrLabel12.StylePriority.UseForeColor = False
            Me.XrLabel12.StylePriority.UseTextAlignment = False
            Me.XrLabel12.Text = "TOTALS"
            Me.XrLabel12.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            '
            'PayoutSummaryReport
            '
            Me.Bands.AddRange(New DevExpress.XtraReports.UI.Band() {Me.Detail, Me.PageHeader, Me.PageFooter, Me.GroupHeader1, Me.GroupFooter1, Me.ReportFooter, Me.GroupHeader2, Me.GroupHeader3})
            Me.Version = "8.1"
            CType(Me.ds, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.XrRichText1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.result, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me, System.ComponentModel.ISupportInitialize).EndInit()

        End Sub
        Friend WithEvents XrLabel_summary_date As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel12 As DevExpress.XtraReports.UI.XRLabel
    End Class
End Namespace
