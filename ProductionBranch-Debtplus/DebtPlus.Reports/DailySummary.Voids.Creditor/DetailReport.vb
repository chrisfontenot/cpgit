#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}

#End Region

Imports System.ComponentModel
Imports DebtPlus.Reports.Template

Imports System.Drawing.Printing
Imports DevExpress.XtraReports.UI
Imports DebtPlus.Utils.Format

Namespace DailySummary.Voids.Creditor
    Friend Class DetailReport
        Inherits TemplateXtraReportClass

        ''' <summary>
        ''' Create an instance of our report
        ''' </summary>
        Public Sub New()
            MyBase.New()
            InitializeComponent()
            AddHandler XrLabel_client.BeforePrint, AddressOf XrLabel_client_BeforePrint
        End Sub

        ''' <summary>
        ''' Create an instance of our report
        ''' </summary>
        Public Sub New(ByVal Container As IContainer)
            MyClass.New()
            Container.Add(Me)
        End Sub

        Public Overrides ReadOnly Property ReportTitle() As String
            Get
                Return "Creditor Checks Voided"
            End Get
        End Property

        Public Sub SetSubreportParameters(ByVal ds As DataSet)

            Dim tbl As DataTable = ds.Tables(0)
            With tbl
                .Columns.Add("deducted", GetType(Decimal), "iif(creditor_type='D',fairshare_amt,0)")
                .Columns.Add("billed", GetType(Decimal), "iif(creditor_type='N',0,iif(creditor_type='D',0,fairshare_amt))")
                .Columns.Add("net", GetType(Decimal), "[debit_amt]-[deducted]")
            End With

            Dim vue As DataView = New DataView(tbl, String.Empty, "client", DataViewRowState.CurrentRows)
            DataSource = vue

            With XrLabel_account_number
                .DataBindings.Add("Text", vue, "account_number")
            End With

            With XrLabel_billed
                .DataBindings.Add("Text", vue, "billed", "{0:c}")
            End With

            With XrLabel_total_billed
                .DataBindings.Add("Text", vue, "billed")
            End With

            With XrLabel_client_name
                .DataBindings.Add("Text", vue, "client_name")
            End With

            With XrLabel_debit_amt
                .DataBindings.Add("Text", vue, "debit_amt", "{0:c}")
            End With

            With XrLabel_total_debit_amt
                .DataBindings.Add("Text", vue, "debit_amt")
            End With

            With XrLabel_deducted
                .DataBindings.Add("Text", vue, "deducted", "{0:c}")
            End With

            With XrLabel_total_deducted
                .DataBindings.Add("Text", vue, "deducted")
            End With

            With XrLabel_net
                .DataBindings.Add("Text", vue, "net", "{0:c}")
            End With

            With XrLabel_total_net
                .DataBindings.Add("Text", vue, "net")
            End With

            With XrTableCell_date_created
                .Text = String.Format("{0:d}", Convert.ToDateTime(vue(0)("date_created")))
            End With
        End Sub

        Private Sub XrLabel_client_BeforePrint(ByVal sender As Object, ByVal e As PrintEventArgs)
            With CType(sender, XRLabel)
                .Text = String.Format("{0:0000000}", Convert.ToInt32(GetCurrentColumnValue("client")))
            End With
        End Sub
    End Class
End Namespace
