Namespace RPPS.Transactions
    <Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
    Partial Class RPPSTransactionsReport
        Inherits DebtPlus.Reports.Template.DatedTemplateXtraReportClass

        'XtraReport overrides dispose to clean up the component list.
        <System.Diagnostics.DebuggerNonUserCode()> _
        Protected Overrides Sub Dispose(ByVal disposing As Boolean)
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
            MyBase.Dispose(disposing)
        End Sub

        'Required by the Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Designer
        'It can be modified using the Designer.  
        'Do not modify it using the code editor.
        <System.Diagnostics.DebuggerStepThrough()> _
        Private Sub InitializeComponent()
            Dim XrSummary1 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary()
            Dim XrSummary2 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary()
            Dim XrSummary3 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary()
            Dim XrSummary4 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary()
            Dim XrSummary5 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary()
            Dim XrSummary6 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary()
            Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(RPPSTransactionsReport))
            Me.XrLabel_account_number = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_item_key = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_gross = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_net = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_client_id = New DevExpress.XtraReports.UI.XRLabel()
            Me.ReportFooter = New DevExpress.XtraReports.UI.ReportFooterBand()
            Me.XrLine2 = New DevExpress.XtraReports.UI.XRLine()
            Me.XrLabel_report_net = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_report_gross = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_report_total = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_group_total = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLine1 = New DevExpress.XtraReports.UI.XRLine()
            Me.XrLabel_group_gross = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_group_net = New DevExpress.XtraReports.UI.XRLabel()
            Me.GroupHeader2 = New DevExpress.XtraReports.UI.GroupHeaderBand()
            Me.XrLabel_rpps_batch = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_creditor_name = New DevExpress.XtraReports.UI.XRLabel()
            Me.GroupFooter1 = New DevExpress.XtraReports.UI.GroupFooterBand()
            Me.rpps_file = New DevExpress.XtraReports.Parameters.Parameter()
            Me.CalculatedField_BatchID = New DevExpress.XtraReports.UI.CalculatedField()
            Me.GroupHeader1 = New DevExpress.XtraReports.UI.GroupHeaderBand()
            Me.XrPanel1 = New DevExpress.XtraReports.UI.XRPanel()
            Me.XrLabel5 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel4 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel3 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel2 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel1 = New DevExpress.XtraReports.UI.XRLabel()
            CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
            '
            'PageHeader
            '
            Me.PageHeader.HeightF = 118.0!
            '
            'XrLabel_Title
            '
            Me.XrLabel_Title.LocationFloat = New DevExpress.Utils.PointFloat(8.0!, 7.999992!)
            Me.XrLabel_Title.SizeF = New System.Drawing.SizeF(484.0!, 42.0!)
            Me.XrLabel_Title.StylePriority.UseFont = False
            '
            'XrPageInfo_PageNumber
            '
            Me.XrPageInfo_PageNumber.LocationFloat = New DevExpress.Utils.PointFloat(617.0!, 8.0!)
            Me.XrPageInfo_PageNumber.StylePriority.UseTextAlignment = False
            '
            'XrPanel_AgencyAddress
            '
            Me.XrPanel_AgencyAddress.LocationFloat = New DevExpress.Utils.PointFloat(492.0!, 0.0!)
            '
            'XRLabel_Agency_Name
            '
            Me.XRLabel_Agency_Name.StylePriority.UseTextAlignment = False
            '
            'XrLine_Title
            '
            Me.XrLine_Title.SizeF = New System.Drawing.SizeF(484.0!, 8.0!)
            '
            'XrLabel_Agency_Address3
            '
            Me.XrLabel_Agency_Address3.StylePriority.UseFont = False
            Me.XrLabel_Agency_Address3.StylePriority.UseTextAlignment = False
            '
            'XrLabel_Agency_Address1
            '
            Me.XrLabel_Agency_Address1.StylePriority.UseFont = False
            Me.XrLabel_Agency_Address1.StylePriority.UseTextAlignment = False
            '
            'XrLabel_Agency_Phone
            '
            Me.XrLabel_Agency_Phone.StylePriority.UseFont = False
            Me.XrLabel_Agency_Phone.StylePriority.UseTextAlignment = False
            '
            'XrLabel_Agency_Address2
            '
            Me.XrLabel_Agency_Address2.StylePriority.UseFont = False
            Me.XrLabel_Agency_Address2.StylePriority.UseTextAlignment = False
            '
            'Detail
            '
            Me.Detail.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel_client_id, Me.XrLabel_net, Me.XrLabel_gross, Me.XrLabel_item_key, Me.XrLabel_account_number})
            Me.Detail.HeightF = 15.0!
            '
            'XrLabel_account_number
            '
            Me.XrLabel_account_number.CanGrow = False
            Me.XrLabel_account_number.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "account_number")})
            Me.XrLabel_account_number.Font = New System.Drawing.Font("Times New Roman", 9.75!)
            Me.XrLabel_account_number.ForeColor = System.Drawing.SystemColors.WindowText
            Me.XrLabel_account_number.LocationFloat = New DevExpress.Utils.PointFloat(144.0!, 0.0!)
            Me.XrLabel_account_number.Name = "XrLabel_account_number"
            Me.XrLabel_account_number.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_account_number.SizeF = New System.Drawing.SizeF(168.0!, 15.0!)
            Me.XrLabel_account_number.StylePriority.UseFont = False
            Me.XrLabel_account_number.StylePriority.UseForeColor = False
            Me.XrLabel_account_number.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            '
            'XrLabel_item_key
            '
            Me.XrLabel_item_key.CanGrow = False
            Me.XrLabel_item_key.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "item_key")})
            Me.XrLabel_item_key.Font = New System.Drawing.Font("Times New Roman", 9.75!)
            Me.XrLabel_item_key.ForeColor = System.Drawing.SystemColors.WindowText
            Me.XrLabel_item_key.LocationFloat = New DevExpress.Utils.PointFloat(10.0!, 0.0!)
            Me.XrLabel_item_key.Name = "XrLabel_item_key"
            Me.XrLabel_item_key.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_item_key.SizeF = New System.Drawing.SizeF(134.0!, 15.0!)
            Me.XrLabel_item_key.StylePriority.UseFont = False
            Me.XrLabel_item_key.StylePriority.UseForeColor = False
            Me.XrLabel_item_key.StylePriority.UseTextAlignment = False
            Me.XrLabel_item_key.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            '
            'XrLabel_gross
            '
            Me.XrLabel_gross.CanGrow = False
            Me.XrLabel_gross.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "gross", "{0:c}")})
            Me.XrLabel_gross.Font = New System.Drawing.Font("Times New Roman", 9.75!)
            Me.XrLabel_gross.ForeColor = System.Drawing.SystemColors.WindowText
            Me.XrLabel_gross.LocationFloat = New DevExpress.Utils.PointFloat(323.0!, 0.0!)
            Me.XrLabel_gross.Name = "XrLabel_gross"
            Me.XrLabel_gross.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_gross.SizeF = New System.Drawing.SizeF(85.0!, 15.0!)
            Me.XrLabel_gross.StylePriority.UseFont = False
            Me.XrLabel_gross.StylePriority.UseForeColor = False
            Me.XrLabel_gross.Text = "$0.00"
            Me.XrLabel_gross.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel_net
            '
            Me.XrLabel_net.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "net", "{0:c}")})
            Me.XrLabel_net.Font = New System.Drawing.Font("Times New Roman", 9.75!)
            Me.XrLabel_net.ForeColor = System.Drawing.SystemColors.WindowText
            Me.XrLabel_net.LocationFloat = New DevExpress.Utils.PointFloat(408.0!, 0.0!)
            Me.XrLabel_net.Name = "XrLabel_net"
            Me.XrLabel_net.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_net.SizeF = New System.Drawing.SizeF(85.0!, 15.0!)
            Me.XrLabel_net.StylePriority.UseFont = False
            Me.XrLabel_net.StylePriority.UseForeColor = False
            Me.XrLabel_net.Text = "$0.00"
            Me.XrLabel_net.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel_client_id
            '
            Me.XrLabel_client_id.Font = New System.Drawing.Font("Times New Roman", 9.75!)
            Me.XrLabel_client_id.ForeColor = System.Drawing.SystemColors.WindowText
            Me.XrLabel_client_id.LocationFloat = New DevExpress.Utils.PointFloat(504.4584!, 0.0!)
            Me.XrLabel_client_id.Name = "XrLabel_client_id"
            Me.XrLabel_client_id.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_client_id.SizeF = New System.Drawing.SizeF(245.5417!, 15.0!)
            Me.XrLabel_client_id.StylePriority.UseFont = False
            Me.XrLabel_client_id.StylePriority.UseForeColor = False
            Me.XrLabel_client_id.Text = "[client!0000000] [client_name]"
            Me.XrLabel_client_id.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            Me.XrLabel_client_id.WordWrap = False
            '
            'ReportFooter
            '
            Me.ReportFooter.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLine2, Me.XrLabel_report_net, Me.XrLabel_report_gross, Me.XrLabel_report_total})
            Me.ReportFooter.HeightF = 50.0!
            Me.ReportFooter.Name = "ReportFooter"
            '
            'XrLine2
            '
            Me.XrLine2.ForeColor = System.Drawing.Color.Teal
            Me.XrLine2.LineWidth = 2
            Me.XrLine2.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
            Me.XrLine2.Name = "XrLine2"
            Me.XrLine2.SizeF = New System.Drawing.SizeF(493.0!, 9.0!)
            Me.XrLine2.StylePriority.UseForeColor = False
            '
            'XrLabel_report_net
            '
            Me.XrLabel_report_net.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "net", "{0:c}")})
            Me.XrLabel_report_net.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel_report_net.ForeColor = System.Drawing.SystemColors.WindowText
            Me.XrLabel_report_net.LocationFloat = New DevExpress.Utils.PointFloat(408.0!, 17.0!)
            Me.XrLabel_report_net.Name = "XrLabel_report_net"
            Me.XrLabel_report_net.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_report_net.SizeF = New System.Drawing.SizeF(85.0!, 15.0!)
            Me.XrLabel_report_net.StylePriority.UseForeColor = False
            XrSummary1.FormatString = "{0:c}"
            XrSummary1.Running = DevExpress.XtraReports.UI.SummaryRunning.Report
            Me.XrLabel_report_net.Summary = XrSummary1
            Me.XrLabel_report_net.Text = "$0.00"
            Me.XrLabel_report_net.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel_report_gross
            '
            Me.XrLabel_report_gross.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "gross", "{0:c}")})
            Me.XrLabel_report_gross.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel_report_gross.ForeColor = System.Drawing.SystemColors.WindowText
            Me.XrLabel_report_gross.LocationFloat = New DevExpress.Utils.PointFloat(283.0!, 17.00001!)
            Me.XrLabel_report_gross.Name = "XrLabel_report_gross"
            Me.XrLabel_report_gross.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_report_gross.SizeF = New System.Drawing.SizeF(125.0!, 15.0!)
            Me.XrLabel_report_gross.StylePriority.UseForeColor = False
            XrSummary2.FormatString = "{0:c}"
            XrSummary2.Running = DevExpress.XtraReports.UI.SummaryRunning.Report
            Me.XrLabel_report_gross.Summary = XrSummary2
            Me.XrLabel_report_gross.Text = "$0.00"
            Me.XrLabel_report_gross.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel_report_total
            '
            Me.XrLabel_report_total.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "item_key")})
            Me.XrLabel_report_total.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel_report_total.ForeColor = System.Drawing.SystemColors.WindowText
            Me.XrLabel_report_total.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 17.00001!)
            Me.XrLabel_report_total.Name = "XrLabel_report_total"
            Me.XrLabel_report_total.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_report_total.SizeF = New System.Drawing.SizeF(267.0!, 15.0!)
            Me.XrLabel_report_total.StylePriority.UseForeColor = False
            XrSummary3.FormatString = "GRAND TOTAL ({0:n0} items)"
            XrSummary3.Func = DevExpress.XtraReports.UI.SummaryFunc.Count
            XrSummary3.Running = DevExpress.XtraReports.UI.SummaryRunning.Report
            Me.XrLabel_report_total.Summary = XrSummary3
            Me.XrLabel_report_total.Text = "GRAND TOTAL"
            Me.XrLabel_report_total.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            '
            'XrLabel_group_total
            '
            Me.XrLabel_group_total.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "item_key")})
            Me.XrLabel_group_total.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel_group_total.ForeColor = System.Drawing.SystemColors.WindowText
            Me.XrLabel_group_total.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 17.00001!)
            Me.XrLabel_group_total.Name = "XrLabel_group_total"
            Me.XrLabel_group_total.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_group_total.SizeF = New System.Drawing.SizeF(267.0!, 15.0!)
            Me.XrLabel_group_total.StylePriority.UseForeColor = False
            XrSummary4.FormatString = "BATCH TOTAL ({0:n0} items)"
            XrSummary4.Func = DevExpress.XtraReports.UI.SummaryFunc.Count
            XrSummary4.Running = DevExpress.XtraReports.UI.SummaryRunning.Group
            Me.XrLabel_group_total.Summary = XrSummary4
            Me.XrLabel_group_total.Text = "BATCH TOTAL"
            Me.XrLabel_group_total.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            '
            'XrLine1
            '
            Me.XrLine1.ForeColor = System.Drawing.Color.Teal
            Me.XrLine1.LineWidth = 2
            Me.XrLine1.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
            Me.XrLine1.Name = "XrLine1"
            Me.XrLine1.SizeF = New System.Drawing.SizeF(493.0!, 9.0!)
            Me.XrLine1.StylePriority.UseForeColor = False
            '
            'XrLabel_group_gross
            '
            Me.XrLabel_group_gross.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "gross", "{0:c}")})
            Me.XrLabel_group_gross.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel_group_gross.ForeColor = System.Drawing.SystemColors.WindowText
            Me.XrLabel_group_gross.LocationFloat = New DevExpress.Utils.PointFloat(283.0!, 17.00001!)
            Me.XrLabel_group_gross.Name = "XrLabel_group_gross"
            Me.XrLabel_group_gross.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_group_gross.SizeF = New System.Drawing.SizeF(125.0!, 15.0!)
            Me.XrLabel_group_gross.StylePriority.UseForeColor = False
            XrSummary5.FormatString = "{0:c}"
            XrSummary5.Running = DevExpress.XtraReports.UI.SummaryRunning.Group
            Me.XrLabel_group_gross.Summary = XrSummary5
            Me.XrLabel_group_gross.Text = "$0.00"
            Me.XrLabel_group_gross.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel_group_net
            '
            Me.XrLabel_group_net.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "net", "{0:c}")})
            Me.XrLabel_group_net.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel_group_net.ForeColor = System.Drawing.SystemColors.WindowText
            Me.XrLabel_group_net.LocationFloat = New DevExpress.Utils.PointFloat(408.0!, 17.0!)
            Me.XrLabel_group_net.Name = "XrLabel_group_net"
            Me.XrLabel_group_net.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_group_net.SizeF = New System.Drawing.SizeF(85.0!, 15.0!)
            Me.XrLabel_group_net.StylePriority.UseForeColor = False
            XrSummary6.FormatString = "{0:c}"
            XrSummary6.Running = DevExpress.XtraReports.UI.SummaryRunning.Group
            Me.XrLabel_group_net.Summary = XrSummary6
            Me.XrLabel_group_net.Text = "$0.00"
            Me.XrLabel_group_net.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'GroupHeader2
            '
            Me.GroupHeader2.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel_rpps_batch, Me.XrLabel_creditor_name})
            Me.GroupHeader2.GroupFields.AddRange(New DevExpress.XtraReports.UI.GroupField() {New DevExpress.XtraReports.UI.GroupField("rpps_batch", DevExpress.XtraReports.UI.XRColumnSortOrder.Ascending)})
            Me.GroupHeader2.GroupUnion = DevExpress.XtraReports.UI.GroupUnion.WithFirstDetail
            Me.GroupHeader2.HeightF = 44.37497!
            Me.GroupHeader2.Level = 1
            Me.GroupHeader2.Name = "GroupHeader2"
            '
            'XrLabel_rpps_batch
            '
            Me.XrLabel_rpps_batch.Font = New System.Drawing.Font("Times New Roman", 14.0!, System.Drawing.FontStyle.Bold)
            Me.XrLabel_rpps_batch.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 10.00001!)
            Me.XrLabel_rpps_batch.Name = "XrLabel_rpps_batch"
            Me.XrLabel_rpps_batch.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_rpps_batch.SizeF = New System.Drawing.SizeF(267.0!, 25.0!)
            Me.XrLabel_rpps_batch.StylePriority.UseFont = False
            Me.XrLabel_rpps_batch.Text = "Items for Batch ID: [CalculatedField_BatchID]"
            '
            'XrLabel_creditor_name
            '
            Me.XrLabel_creditor_name.CanGrow = False
            Me.XrLabel_creditor_name.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel_creditor_name.LocationFloat = New DevExpress.Utils.PointFloat(283.0!, 18.0!)
            Me.XrLabel_creditor_name.Name = "XrLabel_creditor_name"
            Me.XrLabel_creditor_name.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_creditor_name.SizeF = New System.Drawing.SizeF(467.0!, 17.0!)
            Me.XrLabel_creditor_name.StylePriority.UseFont = False
            Me.XrLabel_creditor_name.StylePriority.UseTextAlignment = False
            Me.XrLabel_creditor_name.Text = "[biller_id] [[creditor]] [creditor_name]"
            Me.XrLabel_creditor_name.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'GroupFooter1
            '
            Me.GroupFooter1.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel_group_total, Me.XrLabel_group_net, Me.XrLabel_group_gross, Me.XrLine1})
            Me.GroupFooter1.HeightF = 42.0!
            Me.GroupFooter1.Level = 1
            Me.GroupFooter1.Name = "GroupFooter1"
            '
            'rpps_file
            '
            Me.rpps_file.Description = "RPPS File ID number"
            Me.rpps_file.Name = "rpps_file"
            Me.rpps_file.Type = GetType(Integer)
            Me.rpps_file.Value = 0
            Me.rpps_file.Visible = False
            '
            'CalculatedField_BatchID
            '
            Me.CalculatedField_BatchID.DisplayName = "CalculatedField_BatchID"
            Me.CalculatedField_BatchID.Expression = "Reverse(Substring(Reverse(PadLeft([rpps_batch], 7, '0')), 0, 7))"
            Me.CalculatedField_BatchID.FieldType = DevExpress.XtraReports.UI.FieldType.[String]
            Me.CalculatedField_BatchID.Name = "CalculatedField_BatchID"
            '
            'GroupHeader1
            '
            Me.GroupHeader1.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrPanel1})
            Me.GroupHeader1.HeightF = 21.875!
            Me.GroupHeader1.Name = "GroupHeader1"
            Me.GroupHeader1.RepeatEveryPage = True
            '
            'XrPanel1
            '
            Me.XrPanel1.BackColor = System.Drawing.Color.Teal
            Me.XrPanel1.BorderColor = System.Drawing.Color.Teal
            Me.XrPanel1.CanGrow = False
            Me.XrPanel1.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel5, Me.XrLabel4, Me.XrLabel3, Me.XrLabel2, Me.XrLabel1})
            Me.XrPanel1.LocationFloat = New DevExpress.Utils.PointFloat(0.00006103516!, 0.0!)
            Me.XrPanel1.Name = "XrPanel1"
            Me.XrPanel1.SizeF = New System.Drawing.SizeF(750.0!, 17.0!)
            '
            'XrLabel5
            '
            Me.XrLabel5.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel5.ForeColor = System.Drawing.Color.White
            Me.XrLabel5.LocationFloat = New DevExpress.Utils.PointFloat(504.4583!, 1.0!)
            Me.XrLabel5.Name = "XrLabel5"
            Me.XrLabel5.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel5.SizeF = New System.Drawing.SizeF(234.0!, 15.0!)
            Me.XrLabel5.StylePriority.UseTextAlignment = False
            Me.XrLabel5.Text = "Client ID and Name"
            Me.XrLabel5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            '
            'XrLabel4
            '
            Me.XrLabel4.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel4.ForeColor = System.Drawing.Color.White
            Me.XrLabel4.LocationFloat = New DevExpress.Utils.PointFloat(407.9999!, 1.000023!)
            Me.XrLabel4.Name = "XrLabel4"
            Me.XrLabel4.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel4.SizeF = New System.Drawing.SizeF(74.0!, 15.0!)
            Me.XrLabel4.Text = "Net"
            Me.XrLabel4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel3
            '
            Me.XrLabel3.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel3.ForeColor = System.Drawing.Color.White
            Me.XrLabel3.LocationFloat = New DevExpress.Utils.PointFloat(322.9999!, 1.0!)
            Me.XrLabel3.Name = "XrLabel3"
            Me.XrLabel3.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel3.SizeF = New System.Drawing.SizeF(85.0!, 15.0!)
            Me.XrLabel3.Text = "Gross"
            Me.XrLabel3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel2
            '
            Me.XrLabel2.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel2.ForeColor = System.Drawing.Color.White
            Me.XrLabel2.LocationFloat = New DevExpress.Utils.PointFloat(143.9999!, 1.0!)
            Me.XrLabel2.Name = "XrLabel2"
            Me.XrLabel2.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel2.SizeF = New System.Drawing.SizeF(168.0!, 15.0!)
            Me.XrLabel2.Text = "Account Number"
            Me.XrLabel2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            '
            'XrLabel1
            '
            Me.XrLabel1.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel1.ForeColor = System.Drawing.Color.White
            Me.XrLabel1.LocationFloat = New DevExpress.Utils.PointFloat(9.999939!, 1.0!)
            Me.XrLabel1.Name = "XrLabel1"
            Me.XrLabel1.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel1.SizeF = New System.Drawing.SizeF(134.0!, 15.0!)
            Me.XrLabel1.StylePriority.UseTextAlignment = False
            Me.XrLabel1.Text = "Item Key"
            Me.XrLabel1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            '
            'RPPSTransactionsReport
            '
            Me.Bands.AddRange(New DevExpress.XtraReports.UI.Band() {Me.Detail, Me.PageHeader, Me.PageFooter, Me.TopMarginBand1, Me.BottomMarginBand1, Me.ReportFooter, Me.GroupHeader2, Me.GroupFooter1, Me.GroupHeader1})
            Me.CalculatedFields.AddRange(New DevExpress.XtraReports.UI.CalculatedField() {Me.CalculatedField_BatchID})
            Me.Parameters.AddRange(New DevExpress.XtraReports.Parameters.Parameter() {Me.rpps_file})
            Me.ScriptLanguage = DevExpress.XtraReports.ScriptLanguage.VisualBasic
            Me.ScriptReferencesString = resources.GetString("$this.ScriptReferencesString")
            Me.Scripts.OnBeforePrint = "RPPSTransactionsReport_BeforePrint"
            Me.ScriptsSource = resources.GetString("$this.ScriptsSource")
            Me.StyleSheet.AddRange(New DevExpress.XtraReports.UI.XRControlStyle() {Me.XrControlStyle_Header, Me.XrControlStyle_Totals, Me.XrControlStyle_HeaderPannel, Me.XrControlStyle_GroupHeader})
            Me.Version = "11.2"
            Me.Controls.SetChildIndex(Me.GroupHeader1, 0)
            Me.Controls.SetChildIndex(Me.GroupFooter1, 0)
            Me.Controls.SetChildIndex(Me.GroupHeader2, 0)
            Me.Controls.SetChildIndex(Me.ReportFooter, 0)
            Me.Controls.SetChildIndex(Me.BottomMarginBand1, 0)
            Me.Controls.SetChildIndex(Me.TopMarginBand1, 0)
            Me.Controls.SetChildIndex(Me.PageFooter, 0)
            Me.Controls.SetChildIndex(Me.PageHeader, 0)
            Me.Controls.SetChildIndex(Me.Detail, 0)
            CType(Me, System.ComponentModel.ISupportInitialize).EndInit()

        End Sub
        Friend WithEvents XrLabel_net As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_gross As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_item_key As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_account_number As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_client_id As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents ReportFooter As DevExpress.XtraReports.UI.ReportFooterBand
        Friend WithEvents XrLine1 As DevExpress.XtraReports.UI.XRLine
        Friend WithEvents XrLabel_group_gross As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_group_net As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_group_total As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents GroupHeader2 As DevExpress.XtraReports.UI.GroupHeaderBand
        Friend WithEvents GroupFooter1 As DevExpress.XtraReports.UI.GroupFooterBand
        Friend WithEvents XrLine2 As DevExpress.XtraReports.UI.XRLine
        Friend WithEvents XrLabel_report_net As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_report_gross As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_report_total As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents rpps_file As DevExpress.XtraReports.Parameters.Parameter
        Friend WithEvents CalculatedField_BatchID As DevExpress.XtraReports.UI.CalculatedField
        Friend WithEvents GroupHeader1 As DevExpress.XtraReports.UI.GroupHeaderBand
        Friend WithEvents XrLabel_rpps_batch As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_creditor_name As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrPanel1 As DevExpress.XtraReports.UI.XRPanel
        Friend WithEvents XrLabel5 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel4 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel3 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel2 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel1 As DevExpress.XtraReports.UI.XRLabel
    End Class
End Namespace
