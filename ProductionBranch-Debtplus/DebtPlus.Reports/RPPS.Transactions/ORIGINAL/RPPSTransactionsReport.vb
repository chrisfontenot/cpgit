#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}
#End Region

Namespace RPPS.Transactions
    Public Class RPPSTransactionsReport

        ''' <summary>
        ''' Create an instance of our report
        ''' </summary>
        Public Sub New()
            MyBase.New()
            InitializeComponent()

            AddHandler BeforePrint, AddressOf RPPSTransactionsReport_BeforePrint
            Parameter_Batch = -1

            '-- Enable the select expert
            ReportFilter.IsEnabled = True
        End Sub

        Public Property Parameter_Batch() As System.Int32
            Get
                Return DirectCast(Parameters("rpps_file").Value, Int32)
            End Get
            Set(ByVal value As System.Int32)
                Parameters("rpps_file").Value = value
            End Set
        End Property

        Public Overrides ReadOnly Property ReportTitle() As String
            Get
                Return "Disbursement RPPS Transactions"
            End Get
        End Property

        Public Overrides ReadOnly Property ReportSubTitle() As String
            Get
                Dim drv As System.Data.DataRowView = Nothing
                Dim vue As System.Data.DataView = TryCast(DataSource, System.Data.DataView)
                If vue IsNot Nothing Then
                    If vue.Count > 0 Then
                        drv = vue(0)
                    End If
                End If

                Dim Answer As DateTime
                If drv IsNot Nothing Then
                    If drv("file_date") IsNot Nothing AndAlso drv("file_date") IsNot System.DBNull.Value Then
                        Answer = Convert.ToDateTime(drv("file_date"))
                        Return String.Format("The RPPS file was created on {0:d}", Answer)
                    End If
                End If

                Return String.Empty
            End Get
        End Property

        Public Overrides Sub SetReportParameter(ByVal Parameter As String, ByVal Value As Object)
            If Parameter = "RPPS_File" Then
                Parameter_Batch = Convert.ToInt32(Value)
            Else
                MyBase.SetReportParameter(Parameter, Value)
            End If
        End Sub

        Public Overrides Function NeedParameters() As Boolean
            Return MyBase.NeedParameters OrElse Parameter_Batch <= 0
        End Function

        Public Overrides Function RequestReportParameters() As System.Windows.Forms.DialogResult
            Dim Answer As System.Windows.Forms.DialogResult = DialogResult.OK

            If NeedParameters() Then
                Using frm As New RequestParametersForm
                    Answer = frm.ShowDialog
                    Parameter_Batch = frm.Parameter_BatchID
                End Using
            End If

            Return Answer
        End Function

        Dim ds As New System.Data.DataSet("ds")
        Private Sub RPPSTransactionsReport_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs)
            Const TableName As String = "rpt_rps_transmission_report"
            Dim rpt As DevExpress.XtraReports.UI.XtraReport = CType(sender, DevExpress.XtraReports.UI.XtraReport)
            Dim tbl As System.Data.DataTable = ds.Tables(TableName)
            If tbl Is Nothing Then
                With rpt
                    Dim sqlInfo As DebtPlus.LINQ.SQLInfoClass = DebtPlus.LINQ.SQLInfoClass.getDefault()
                    Try
                        '-- Read the transactions from the system
                        Using cmd As New System.Data.SqlClient.SqlCommand()
                            With cmd
                                .Connection = New System.Data.SqlClient.SqlConnection(sqlInfo.ConnectionString)
                                .CommandText = "rpt_rps_transmission_report"
                                .CommandType = System.Data.CommandType.StoredProcedure
                                .Parameters.Add("@FileID", System.Data.SqlDbType.Int).Value = rpt.Parameters("rpps_file").Value
                                .CommandTimeout = 0
                            End With

                            Using da As New System.Data.SqlClient.SqlDataAdapter(cmd)
                                da.Fill(ds, TableName)
                                tbl = ds.Tables(TableName)
                            End Using
                        End Using

                    Catch ex As System.Data.SqlClient.SqlException
                        Using gdr As New DebtPlus.Repository.GetDataResult()
                            gdr.HandleException(ex)
                            DebtPlus.UI.Common.ErrorHandling.HandleErrors(gdr, "Error reading RPPS transactions")
                        End Using
                    End Try
                End With
            End If

            If tbl IsNot Nothing Then
                rpt.DataSource = New System.Data.DataView(tbl, CType(rpt, DebtPlus.Interfaces.Reports.IReportFilter).ReportFilter.ViewFilter, "rpps_batch, item_key", System.Data.DataViewRowState.CurrentRows)

                '-- Propagate the data source to the fields
                For Each fld As DevExpress.XtraReports.UI.CalculatedField In rpt.CalculatedFields
                    fld.Assign(rpt.DataSource, rpt.DataMember)
                Next
            End If
        End Sub
    End Class
End Namespace
