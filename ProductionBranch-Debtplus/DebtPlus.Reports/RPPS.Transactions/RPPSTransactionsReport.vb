#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}
#End Region

Option Compare Binary
Option Explicit On
Option Strict On

Imports System.IO
Imports System.Reflection
Imports DebtPlus.Utils
Imports DevExpress.XtraReports.UI

Namespace RPPS.Transactions
    Public Class RPPSTransactionsReport
        Inherits Template.TemplateXtraReportClass

        ''' <summary>
        '''     Create an instance of our report
        ''' </summary>
        Public Sub New()
            MyBase.new()

            Const ReportName As String = "DebtPlus.Reports.RPPS.Transactions.repx"

            ' See if there is a report reference that we can use
            Dim Fname As String = System.IO.Path.Combine(DebtPlus.Configuration.Config.ReportsDirectory, ReportName)
            Dim UseDefault As Boolean = True
            Try
                If (Not DebtPlus.Configuration.Config.LocalReportsOnly) AndAlso System.IO.File.Exists(Fname) Then
                    LoadLayout(Fname)
                    UseDefault = False
                End If
            Catch ex As Exception
            End Try

            ' Load the standard report definition if we need a new item
            If UseDefault Then
                Dim asm As Assembly = Assembly.GetExecutingAssembly                                                    ' changed
                Using ios As Stream = asm.GetManifestResourceStream(String.Format("DebtPlus.Reports.{0}", ReportName)) ' changed
                    If ios IsNot Nothing Then LoadLayout(ios) ' changed
                End Using
            End If

            ' Set the script references as needed
            ScriptReferences = DebtPlus.Utils.AssemblyTools.GetAllSiblingAssemblies()

            ' Copy the script references to the subreports as well
            For Each Band As Band In Bands
                For Each ctl As XRControl In Band.Controls
                    Dim rpt As XRSubreport = TryCast(ctl, XRSubreport)
                    If rpt IsNot Nothing Then
                        Dim SubRpt As XtraReport = TryCast(rpt.ReportSource, XtraReport)
                        If SubRpt IsNot Nothing Then
                            SubRpt.ScriptReferences = ScriptReferences
                        End If
                    End If
                Next
            Next

            ' Ensure that the items are initialized as "empty"
            Parameter_Batch = -1

            ' Enable the select expert
            ReportFilter.IsEnabled = True
        End Sub

        Public Property Parameter_Batch() As Int32
            Get
                Return DirectCast(Parameters("rpps_file").Value, Int32)
            End Get
            Set(ByVal value As Int32)
                Parameters("rpps_file").Value = value
            End Set
        End Property

        Public Overrides ReadOnly Property ReportTitle() As String
            Get
                Return "Disbursement RPPS Transactions"
            End Get
        End Property

        Public Overrides ReadOnly Property ReportSubTitle() As String
            Get
                Dim drv As DataRowView = Nothing
                Dim vue As DataView = TryCast(DataSource, DataView)
                If vue IsNot Nothing Then
                    If vue.Count > 0 Then
                        drv = vue(0)
                    End If
                End If

                Dim Answer As DateTime
                If drv IsNot Nothing Then
                    If drv("file_date") IsNot Nothing AndAlso drv("file_date") IsNot DBNull.Value Then
                        Answer = Convert.ToDateTime(drv("file_date"))
                        Return String.Format("The RPPS file was created on {0:d}", Answer)
                    End If
                End If

                Return String.Empty
            End Get
        End Property

        Public Overrides Sub SetReportParameter(ByVal Parameter As String, ByVal Value As Object)
            If Parameter = "RPPS_File" Then
                Parameter_Batch = Convert.ToInt32(Value)
            Else
                MyBase.SetReportParameter(Parameter, Value)
            End If
        End Sub

        Public Overrides Function NeedParameters() As Boolean
            Return MyBase.NeedParameters OrElse Parameter_Batch <= 0
        End Function

        Public Overrides Function RequestReportParameters() As DialogResult
            Dim answer As DialogResult = DialogResult.OK
            If NeedParameters() Then
                Using frm As New RequestParametersForm
                    answer = frm.ShowDialog()
                    Parameter_Batch = frm.Parameter_BatchID
                End Using
            End If
            Return answer
        End Function
    End Class
End Namespace
