#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}

#End Region

Option Compare Binary
Option Explicit On
Option Strict On

Imports System.ComponentModel
Imports DebtPlus.Reports.Template

Imports System.Drawing.Printing
Imports System.Data.SqlClient

Namespace MonthlySummary.NSF
    Public Class MonthlySummaryNSFReport
        Inherits DatedTemplateXtraReportClass

        Public Sub New()
            MyBase.New()
            InitializeComponent()
            AddHandler Me.BeforePrint, AddressOf DailySummaryRPPSRejects_BeforePrint
        End Sub

#Region " Component Designer generated code "

        'Component overrides dispose to clean up the component list.
        Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
            If disposing Then
                If components IsNot Nothing Then
                    components.Dispose()
                End If
            End If
            MyBase.Dispose(disposing)
        End Sub

        Friend WithEvents XrLabel_collected_pct As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents ReportFooter As DevExpress.XtraReports.UI.ReportFooterBand
        Friend WithEvents XrLabel_total_disbursed As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel1 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_disbursed As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_disbursed_pct As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_total_collected As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrPanel1 As DevExpress.XtraReports.UI.XRPanel
        Friend WithEvents XrLabel2 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel6 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel4 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel5 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel3 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel9 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_collected As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_expected As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_client As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_total_expected As DevExpress.XtraReports.UI.XRLabel

        'Required by the Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Designer
        'It can be modified using the Designer.
        'Do not modify it using the code editor.
        <System.Diagnostics.DebuggerStepThrough()>
        Private Sub InitializeComponent()
            Dim XrSummary1 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary
            Dim XrSummary2 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary
            Dim XrSummary3 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary
            Me.XrLabel_collected_pct = New DevExpress.XtraReports.UI.XRLabel
            Me.ReportFooter = New DevExpress.XtraReports.UI.ReportFooterBand
            Me.XrLabel_total_expected = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_total_collected = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_total_disbursed = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel1 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_disbursed = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_disbursed_pct = New DevExpress.XtraReports.UI.XRLabel
            Me.XrPanel1 = New DevExpress.XtraReports.UI.XRPanel
            Me.XrLabel2 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel6 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel4 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel5 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel3 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel9 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_client = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_expected = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_collected = New DevExpress.XtraReports.UI.XRLabel
            CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
            '
            'Detail
            '
            Me.Detail.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel_collected, Me.XrLabel_expected, Me.XrLabel_disbursed_pct, Me.XrLabel_disbursed, Me.XrLabel_collected_pct, Me.XrLabel_client})
            Me.Detail.HeightF = 15.0!
            '
            'PageHeader
            '
            Me.PageHeader.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrPanel1})
            Me.PageHeader.HeightF = 150.0!
            Me.PageHeader.Controls.SetChildIndex(Me.XrPanel_AgencyAddress, 0)
            Me.PageHeader.Controls.SetChildIndex(Me.XrLabel_Title, 0)
            Me.PageHeader.Controls.SetChildIndex(Me.XrLine_Title, 0)
            Me.PageHeader.Controls.SetChildIndex(Me.XrLabel_Subtitle, 0)
            Me.PageHeader.Controls.SetChildIndex(Me.XrPageInfo1, 0)
            Me.PageHeader.Controls.SetChildIndex(Me.XrPanel1, 0)
            '
            'XrLabel_Title
            '
            Me.XrLabel_Title.StylePriority.UseFont = False
            '
            'XrPageInfo_PageNumber
            '
            Me.XrPageInfo_PageNumber.StylePriority.UseTextAlignment = False
            '
            'XRLabel_Agency_Name
            '
            Me.XRLabel_Agency_Name.StylePriority.UseTextAlignment = False
            '
            'XrLabel_Agency_Address3
            '
            Me.XrLabel_Agency_Address3.StylePriority.UseFont = False
            Me.XrLabel_Agency_Address3.StylePriority.UseTextAlignment = False
            '
            'XrLabel_Agency_Address1
            '
            Me.XrLabel_Agency_Address1.StylePriority.UseFont = False
            Me.XrLabel_Agency_Address1.StylePriority.UseTextAlignment = False
            '
            'XrLabel_Agency_Phone
            '
            Me.XrLabel_Agency_Phone.StylePriority.UseFont = False
            Me.XrLabel_Agency_Phone.StylePriority.UseTextAlignment = False
            '
            'XrLabel_Agency_Address2
            '
            Me.XrLabel_Agency_Address2.StylePriority.UseFont = False
            Me.XrLabel_Agency_Address2.StylePriority.UseTextAlignment = False
            '
            'XrLabel_collected_pct
            '
            Me.XrLabel_collected_pct.CanGrow = False
            Me.XrLabel_collected_pct.LocationFloat = New DevExpress.Utils.PointFloat(512.4999!, 0.0!)
            Me.XrLabel_collected_pct.Name = "XrLabel_collected_pct"
            Me.XrLabel_collected_pct.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_collected_pct.SizeF = New System.Drawing.SizeF(89.0!, 15.0!)
            Me.XrLabel_collected_pct.StylePriority.UseTextAlignment = False
            Me.XrLabel_collected_pct.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'ReportFooter
            '
            Me.ReportFooter.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel_total_expected, Me.XrLabel_total_collected, Me.XrLabel_total_disbursed, Me.XrLabel1})
            Me.ReportFooter.HeightF = 32.0!
            Me.ReportFooter.Name = "ReportFooter"
            '
            'XrLabel_total_expected
            '
            Me.XrLabel_total_expected.CanGrow = False
            Me.XrLabel_total_expected.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel_total_expected.LocationFloat = New DevExpress.Utils.PointFloat(247.9167!, 10.00001!)
            Me.XrLabel_total_expected.Name = "XrLabel_total_expected"
            Me.XrLabel_total_expected.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_total_expected.SizeF = New System.Drawing.SizeF(150.3333!, 14.99999!)
            Me.XrLabel_total_expected.StylePriority.UseFont = False
            Me.XrLabel_total_expected.StylePriority.UseTextAlignment = False
            XrSummary1.FormatString = "{0:c}"
            XrSummary1.Running = DevExpress.XtraReports.UI.SummaryRunning.Report
            Me.XrLabel_total_expected.Summary = XrSummary1
            Me.XrLabel_total_expected.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel_total_collected
            '
            Me.XrLabel_total_collected.CanGrow = False
            Me.XrLabel_total_collected.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel_total_collected.LocationFloat = New DevExpress.Utils.PointFloat(413.2083!, 10.00001!)
            Me.XrLabel_total_collected.Name = "XrLabel_total_collected"
            Me.XrLabel_total_collected.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_total_collected.SizeF = New System.Drawing.SizeF(99.29163!, 14.99999!)
            Me.XrLabel_total_collected.StylePriority.UseFont = False
            Me.XrLabel_total_collected.StylePriority.UseTextAlignment = False
            XrSummary2.FormatString = "{0:c}"
            XrSummary2.Running = DevExpress.XtraReports.UI.SummaryRunning.Report
            Me.XrLabel_total_collected.Summary = XrSummary2
            Me.XrLabel_total_collected.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel_total_disbursed
            '
            Me.XrLabel_total_disbursed.CanGrow = False
            Me.XrLabel_total_disbursed.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel_total_disbursed.LocationFloat = New DevExpress.Utils.PointFloat(561.4583!, 10.00001!)
            Me.XrLabel_total_disbursed.Name = "XrLabel_total_disbursed"
            Me.XrLabel_total_disbursed.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_total_disbursed.SizeF = New System.Drawing.SizeF(142.0417!, 14.99999!)
            Me.XrLabel_total_disbursed.StylePriority.UseFont = False
            Me.XrLabel_total_disbursed.StylePriority.UseTextAlignment = False
            XrSummary3.FormatString = "{0:c}"
            XrSummary3.Running = DevExpress.XtraReports.UI.SummaryRunning.Report
            Me.XrLabel_total_disbursed.Summary = XrSummary3
            Me.XrLabel_total_disbursed.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel1
            '
            Me.XrLabel1.CanGrow = False
            Me.XrLabel1.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel1.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 7.999992!)
            Me.XrLabel1.Name = "XrLabel1"
            Me.XrLabel1.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel1.SizeF = New System.Drawing.SizeF(225.0!, 15.99998!)
            Me.XrLabel1.StylePriority.UseFont = False
            Me.XrLabel1.StylePriority.UseTextAlignment = False
            Me.XrLabel1.Text = "GRAND TOTAL"
            Me.XrLabel1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            '
            'XrLabel_disbursed
            '
            Me.XrLabel_disbursed.CanGrow = False
            Me.XrLabel_disbursed.LocationFloat = New DevExpress.Utils.PointFloat(612.5!, 0.0!)
            Me.XrLabel_disbursed.Name = "XrLabel_disbursed"
            Me.XrLabel_disbursed.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_disbursed.SizeF = New System.Drawing.SizeF(91.0!, 15.0!)
            Me.XrLabel_disbursed.StylePriority.UseTextAlignment = False
            Me.XrLabel_disbursed.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel_disbursed_pct
            '
            Me.XrLabel_disbursed_pct.CanGrow = False
            Me.XrLabel_disbursed_pct.LocationFloat = New DevExpress.Utils.PointFloat(708.0!, 0.0!)
            Me.XrLabel_disbursed_pct.Name = "XrLabel_disbursed_pct"
            Me.XrLabel_disbursed_pct.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_disbursed_pct.SizeF = New System.Drawing.SizeF(89.0!, 15.0!)
            Me.XrLabel_disbursed_pct.StylePriority.UseTextAlignment = False
            Me.XrLabel_disbursed_pct.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrPanel1
            '
            Me.XrPanel1.BackColor = System.Drawing.Color.Teal
            Me.XrPanel1.BorderColor = System.Drawing.Color.Teal
            Me.XrPanel1.CanGrow = False
            Me.XrPanel1.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel2, Me.XrLabel6, Me.XrLabel4, Me.XrLabel5, Me.XrLabel3, Me.XrLabel9})
            Me.XrPanel1.KeepTogether = False
            Me.XrPanel1.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 119.7917!)
            Me.XrPanel1.Name = "XrPanel1"
            Me.XrPanel1.SizeF = New System.Drawing.SizeF(800.0!, 18.0!)
            Me.XrPanel1.StyleName = "XrControlStyle_HeaderPannel"
            Me.XrPanel1.StylePriority.UseBackColor = False
            Me.XrPanel1.StylePriority.UseBorderColor = False
            '
            'XrLabel2
            '
            Me.XrLabel2.BackColor = System.Drawing.Color.Transparent
            Me.XrLabel2.BorderColor = System.Drawing.Color.Transparent
            Me.XrLabel2.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel2.ForeColor = System.Drawing.Color.White
            Me.XrLabel2.LocationFloat = New DevExpress.Utils.PointFloat(413.2083!, 1.0!)
            Me.XrLabel2.Name = "XrLabel2"
            Me.XrLabel2.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel2.SizeF = New System.Drawing.SizeF(99.29169!, 14.99999!)
            Me.XrLabel2.StylePriority.UseBackColor = False
            Me.XrLabel2.StylePriority.UseBorderColor = False
            Me.XrLabel2.StylePriority.UseFont = False
            Me.XrLabel2.StylePriority.UseForeColor = False
            Me.XrLabel2.StylePriority.UseTextAlignment = False
            Me.XrLabel2.Text = "COLLECTED"
            Me.XrLabel2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel6
            '
            Me.XrLabel6.BackColor = System.Drawing.Color.Transparent
            Me.XrLabel6.BorderColor = System.Drawing.Color.Transparent
            Me.XrLabel6.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel6.ForeColor = System.Drawing.Color.White
            Me.XrLabel6.LocationFloat = New DevExpress.Utils.PointFloat(512.5!, 1.0!)
            Me.XrLabel6.Name = "XrLabel6"
            Me.XrLabel6.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel6.SizeF = New System.Drawing.SizeF(89.0!, 14.99999!)
            Me.XrLabel6.StylePriority.UseBackColor = False
            Me.XrLabel6.StylePriority.UseBorderColor = False
            Me.XrLabel6.StylePriority.UseFont = False
            Me.XrLabel6.StylePriority.UseForeColor = False
            Me.XrLabel6.StylePriority.UseTextAlignment = False
            Me.XrLabel6.Text = "PCT"
            Me.XrLabel6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel4
            '
            Me.XrLabel4.BackColor = System.Drawing.Color.Transparent
            Me.XrLabel4.BorderColor = System.Drawing.Color.Transparent
            Me.XrLabel4.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel4.ForeColor = System.Drawing.Color.White
            Me.XrLabel4.LocationFloat = New DevExpress.Utils.PointFloat(708.0!, 1.0!)
            Me.XrLabel4.Name = "XrLabel4"
            Me.XrLabel4.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel4.SizeF = New System.Drawing.SizeF(89.0!, 14.99999!)
            Me.XrLabel4.StylePriority.UseBackColor = False
            Me.XrLabel4.StylePriority.UseBorderColor = False
            Me.XrLabel4.StylePriority.UseFont = False
            Me.XrLabel4.StylePriority.UseForeColor = False
            Me.XrLabel4.StylePriority.UseTextAlignment = False
            Me.XrLabel4.Text = "PCT"
            Me.XrLabel4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel5
            '
            Me.XrLabel5.BackColor = System.Drawing.Color.Transparent
            Me.XrLabel5.BorderColor = System.Drawing.Color.Transparent
            Me.XrLabel5.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel5.ForeColor = System.Drawing.Color.White
            Me.XrLabel5.LocationFloat = New DevExpress.Utils.PointFloat(612.5!, 1.0!)
            Me.XrLabel5.Name = "XrLabel5"
            Me.XrLabel5.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel5.SizeF = New System.Drawing.SizeF(91.0!, 14.99999!)
            Me.XrLabel5.StylePriority.UseBackColor = False
            Me.XrLabel5.StylePriority.UseBorderColor = False
            Me.XrLabel5.StylePriority.UseFont = False
            Me.XrLabel5.StylePriority.UseForeColor = False
            Me.XrLabel5.StylePriority.UseTextAlignment = False
            Me.XrLabel5.Text = "DISBURSED"
            Me.XrLabel5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel3
            '
            Me.XrLabel3.BackColor = System.Drawing.Color.Transparent
            Me.XrLabel3.BorderColor = System.Drawing.Color.Transparent
            Me.XrLabel3.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel3.ForeColor = System.Drawing.Color.White
            Me.XrLabel3.LocationFloat = New DevExpress.Utils.PointFloat(313.5417!, 1.0!)
            Me.XrLabel3.Name = "XrLabel3"
            Me.XrLabel3.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel3.SizeF = New System.Drawing.SizeF(84.70834!, 14.99999!)
            Me.XrLabel3.StylePriority.UseBackColor = False
            Me.XrLabel3.StylePriority.UseBorderColor = False
            Me.XrLabel3.StylePriority.UseFont = False
            Me.XrLabel3.StylePriority.UseForeColor = False
            Me.XrLabel3.StylePriority.UseTextAlignment = False
            Me.XrLabel3.Text = "EXPECTED"
            Me.XrLabel3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel9
            '
            Me.XrLabel9.BackColor = System.Drawing.Color.Transparent
            Me.XrLabel9.BorderColor = System.Drawing.Color.Transparent
            Me.XrLabel9.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel9.ForeColor = System.Drawing.Color.White
            Me.XrLabel9.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 1.0!)
            Me.XrLabel9.Name = "XrLabel9"
            Me.XrLabel9.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel9.SizeF = New System.Drawing.SizeF(225.0!, 14.99999!)
            Me.XrLabel9.StylePriority.UseBackColor = False
            Me.XrLabel9.StylePriority.UseBorderColor = False
            Me.XrLabel9.StylePriority.UseFont = False
            Me.XrLabel9.StylePriority.UseForeColor = False
            Me.XrLabel9.StylePriority.UseTextAlignment = False
            Me.XrLabel9.Text = "CLIENT ID AND NAME"
            Me.XrLabel9.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            '
            'XrLabel_client
            '
            Me.XrLabel_client.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
            Me.XrLabel_client.Name = "XrLabel_client"
            Me.XrLabel_client.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_client.SizeF = New System.Drawing.SizeF(298.25!, 14.99999!)
            '
            'XrLabel_expected
            '
            Me.XrLabel_expected.LocationFloat = New DevExpress.Utils.PointFloat(298.25!, 0.0!)
            Me.XrLabel_expected.Name = "XrLabel_expected"
            Me.XrLabel_expected.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_expected.SizeF = New System.Drawing.SizeF(100.0!, 14.99999!)
            Me.XrLabel_expected.StylePriority.UseTextAlignment = False
            Me.XrLabel_expected.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel_collected
            '
            Me.XrLabel_collected.LocationFloat = New DevExpress.Utils.PointFloat(412.4999!, 0.0!)
            Me.XrLabel_collected.Name = "XrLabel_collected"
            Me.XrLabel_collected.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_collected.SizeF = New System.Drawing.SizeF(100.0!, 14.99999!)
            Me.XrLabel_collected.StylePriority.UseTextAlignment = False
            Me.XrLabel_collected.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'MonthlySummaryNSFReport
            '
            Me.Bands.AddRange(New DevExpress.XtraReports.UI.Band() {Me.Detail, Me.PageHeader, Me.PageFooter, Me.ReportFooter})
            Me.Version = "9.3"
            Me.Controls.SetChildIndex(Me.ReportFooter, 0)
            Me.Controls.SetChildIndex(Me.PageFooter, 0)
            Me.Controls.SetChildIndex(Me.PageHeader, 0)
            Me.Controls.SetChildIndex(Me.Detail, 0)
            CType(Me, System.ComponentModel.ISupportInitialize).EndInit()
        End Sub

#End Region

        Public Overrides ReadOnly Property ReportTitle() As String
            Get
                Return "NSF Report"
            End Get
        End Property

        Dim ds As New DataSet("ds")

        Private Sub DailySummaryRPPSRejects_BeforePrint(ByVal sender As Object, ByVal e As PrintEventArgs)

            ' Read the transactions
            Dim tbl As DataTable = ds.Tables("rpt_NSF_report")
            If tbl Is Nothing Then
                Dim current_cursor As Cursor = Cursor.Current
                Dim cn As SqlConnection = New System.Data.SqlClient.SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
                Try
                    Cursor.Current = Cursors.WaitCursor
                    cn.Open()
                    Using cmd As SqlCommand = New System.Data.SqlClient.SqlCommand()
                        With cmd
                            .Connection = cn
                            .CommandText = "rpt_NSF_report"
                            .CommandType = CommandType.StoredProcedure

                            SqlCommandBuilder.DeriveParameters(cmd)
                            .Parameters(1).Value = Parameter_FromDate
                            .Parameters(2).Value = Parameter_ToDate
                            .CommandTimeout = 0
                        End With

                        Using da As New SqlDataAdapter(cmd)
                            da.Fill(ds, "rpt_NSF_report")
                        End Using
                    End Using

                    ' Bind the datasource to the items
                    tbl = ds.Tables("rpt_NSF_report")
                    Dim vue As New DataView(tbl, String.Empty, "client", DataViewRowState.CurrentRows)
                    DataSource = vue

                    ' Bind the fields to the datasource
                    With XrLabel_expected
                        .DataBindings.Clear()
                        .DataBindings.Add("Text", vue, "expected", "{0:c}")
                    End With

                    With XrLabel_total_expected
                        .DataBindings.Clear()
                        .DataBindings.Add("Text", vue, "expected")
                    End With

                    With XrLabel_disbursed
                        .DataBindings.Clear()
                        .DataBindings.Add("Text", vue, "disbursed", "{0:c}")
                    End With

                    With XrLabel_total_disbursed
                        .DataBindings.Clear()
                        .DataBindings.Add("Text", vue, "disbursed", "{0:c}")
                    End With

                    With XrLabel_collected
                        .DataBindings.Clear()
                        .DataBindings.Add("Text", vue, "collected", "{0:c}")
                    End With

                    With XrLabel_total_collected
                        .DataBindings.Clear()
                        .DataBindings.Add("Text", vue, "collected")
                    End With

                    With XrLabel_collected_pct
                        AddHandler .BeforePrint, AddressOf XrLabel_collected_pct_BeforePrint
                    End With

                    With XrLabel_disbursed_pct
                        AddHandler .BeforePrint, AddressOf XrLabel_disbursed_pct_BeforePrint
                    End With

                    With XrLabel_client
                        AddHandler .BeforePrint, AddressOf XrLabel_client_BeforePrint
                    End With

                Catch ex As Exception
                    DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error reading transactions")

                Finally
                    If cn IsNot Nothing Then cn.Dispose()

                    Cursor.Current = current_cursor
                End Try
            End If
        End Sub

        Private Sub XrLabel_collected_pct_BeforePrint(ByVal sender As Object, ByVal e As PrintEventArgs)
            Dim expected As Double = Convert.ToDouble(DebtPlus.Utils.Nulls.DDec(GetCurrentColumnValue("expected")))
            Dim collected As Double = Convert.ToDouble(DebtPlus.Utils.Nulls.DDec(GetCurrentColumnValue("collected")))

            Dim pct As Double
            Try
                pct = collected / expected
            Catch ex As Exception
                pct = Double.PositiveInfinity
            End Try

            If Double.IsInfinity(pct) Then
                XrLabel_collected_pct.Text = String.Empty
            Else
                XrLabel_collected_pct.Text = String.Format("{0:p}", pct)
            End If
        End Sub

        Private Sub XrLabel_disbursed_pct_BeforePrint(ByVal sender As Object, ByVal e As PrintEventArgs)
            Dim collected As Double = Convert.ToDouble(DebtPlus.Utils.Nulls.DDec(GetCurrentColumnValue("collected")))
            Dim disbursed As Double = Convert.ToDouble(DebtPlus.Utils.Nulls.DDec(GetCurrentColumnValue("disbursed")))

            Dim pct As Double
            Try
                pct = disbursed / collected
            Catch ex As Exception
                pct = Double.PositiveInfinity
            End Try

            If Double.IsInfinity(pct) Then
                XrLabel_disbursed_pct.Text = String.Empty
            Else
                XrLabel_disbursed_pct.Text = String.Format("{0:p}", pct)
            End If
        End Sub

        Private Sub XrLabel_client_BeforePrint(ByVal sender As Object, ByVal e As PrintEventArgs)
            XrLabel_client.Text = String.Format("{0:0000000}", DebtPlus.Utils.Nulls.DInt(GetCurrentColumnValue("client"))) + " " + DebtPlus.Utils.Nulls.DStr(GetCurrentColumnValue("name"))
        End Sub
    End Class
End Namespace
