Namespace Client.TrustActivity
    <Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
    Partial Class ClientTrustActivityReport

        'XtraReport overrides dispose to clean up the component list.
        <System.Diagnostics.DebuggerNonUserCode()> _
        Protected Overrides Sub Dispose(ByVal disposing As Boolean)
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
            MyBase.Dispose(disposing)
        End Sub

        'Required by the Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Designer
        'It can be modified using the Designer.  
        'Do not modify it using the code editor.
        <System.Diagnostics.DebuggerStepThrough()> _
        Private Sub InitializeComponent()
            Dim XrSummary1 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary()
            Dim XrSummary2 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary()
            Dim XrSummary3 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary()
            Dim XrSummary4 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary()
            Dim XrSummary5 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary()
            Dim XrSummary6 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary()
            Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(ClientTrustActivityReport))
            Me.XrPanel1 = New DevExpress.XtraReports.UI.XRPanel()
            Me.XrLabel7 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel6 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel5 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel4 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel3 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel2 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel1 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_date_created = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_tran_type = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_credit = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_debit = New DevExpress.XtraReports.UI.XRLabel()
            Me.ReportFooter = New DevExpress.XtraReports.UI.ReportFooterBand()
            Me.XrLabel_total_interest = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel8 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLine1 = New DevExpress.XtraReports.UI.XRLine()
            Me.XrLabel_Total_Credit = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_Total_Debit = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_interest = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_creditor = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_account_number = New DevExpress.XtraReports.UI.XRLabel()
            Me.GroupHeader1 = New DevExpress.XtraReports.UI.GroupHeaderBand()
            Me.GroupFooter1 = New DevExpress.XtraReports.UI.GroupFooterBand()
            Me.XrLabel_Subtotal_Interest = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_Subtotal = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_Subtotal_Debit = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_Subtotal_Credit = New DevExpress.XtraReports.UI.XRLabel()
            Me.formatted_date_created = New DevExpress.XtraReports.UI.CalculatedField()
            Me.ParameterClient = New DevExpress.XtraReports.Parameters.Parameter()
            CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
            '
            'PageHeader
            '
            Me.PageHeader.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrPanel1})
            Me.PageHeader.HeightF = 147.0!
            Me.PageHeader.Controls.SetChildIndex(Me.XrPanel1, 0)
            Me.PageHeader.Controls.SetChildIndex(Me.XrPanel_AgencyAddress, 0)
            Me.PageHeader.Controls.SetChildIndex(Me.XrLabel_Title, 0)
            Me.PageHeader.Controls.SetChildIndex(Me.XrLine_Title, 0)
            Me.PageHeader.Controls.SetChildIndex(Me.XrLabel_Subtitle, 0)
            Me.PageHeader.Controls.SetChildIndex(Me.XrPageInfo1, 0)
            '
            'XrLabel_Title
            '
            Me.XrLabel_Title.StylePriority.UseFont = False
            '
            'XrPageInfo_PageNumber
            '
            Me.XrPageInfo_PageNumber.StylePriority.UseTextAlignment = False
            '
            'XRLabel_Agency_Name
            '
            Me.XRLabel_Agency_Name.StylePriority.UseTextAlignment = False
            '
            'XrLabel_Agency_Address3
            '
            Me.XrLabel_Agency_Address3.StylePriority.UseFont = False
            Me.XrLabel_Agency_Address3.StylePriority.UseTextAlignment = False
            '
            'XrLabel_Agency_Address1
            '
            Me.XrLabel_Agency_Address1.StylePriority.UseFont = False
            Me.XrLabel_Agency_Address1.StylePriority.UseTextAlignment = False
            '
            'XrLabel_Agency_Phone
            '
            Me.XrLabel_Agency_Phone.StylePriority.UseFont = False
            Me.XrLabel_Agency_Phone.StylePriority.UseTextAlignment = False
            '
            'XrLabel_Agency_Address2
            '
            Me.XrLabel_Agency_Address2.StylePriority.UseFont = False
            Me.XrLabel_Agency_Address2.StylePriority.UseTextAlignment = False
            '
            'Detail
            '
            Me.Detail.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel_account_number, Me.XrLabel_creditor, Me.XrLabel_interest, Me.XrLabel_debit, Me.XrLabel_credit, Me.XrLabel_tran_type, Me.XrLabel_date_created})
            Me.Detail.HeightF = 15.0!
            '
            'XrPanel1
            '
            Me.XrPanel1.BackColor = System.Drawing.Color.Teal
            Me.XrPanel1.BorderColor = System.Drawing.Color.Teal
            Me.XrPanel1.CanGrow = False
            Me.XrPanel1.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel7, Me.XrLabel6, Me.XrLabel5, Me.XrLabel4, Me.XrLabel3, Me.XrLabel2, Me.XrLabel1})
            Me.XrPanel1.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 125.0!)
            Me.XrPanel1.Name = "XrPanel1"
            Me.XrPanel1.SizeF = New System.Drawing.SizeF(750.0!, 16.0!)
            '
            'XrLabel7
            '
            Me.XrLabel7.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel7.ForeColor = System.Drawing.Color.White
            Me.XrLabel7.LocationFloat = New DevExpress.Utils.PointFloat(658.0!, 0.0!)
            Me.XrLabel7.Name = "XrLabel7"
            Me.XrLabel7.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel7.SizeF = New System.Drawing.SizeF(83.0!, 15.0!)
            Me.XrLabel7.Text = "INTEREST"
            Me.XrLabel7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel6
            '
            Me.XrLabel6.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel6.ForeColor = System.Drawing.Color.White
            Me.XrLabel6.LocationFloat = New DevExpress.Utils.PointFloat(116.0!, 0.0!)
            Me.XrLabel6.Name = "XrLabel6"
            Me.XrLabel6.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel6.SizeF = New System.Drawing.SizeF(233.0!, 15.0!)
            Me.XrLabel6.StylePriority.UseTextAlignment = False
            Me.XrLabel6.Text = "CREDITOR"
            Me.XrLabel6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            '
            'XrLabel5
            '
            Me.XrLabel5.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel5.ForeColor = System.Drawing.Color.White
            Me.XrLabel5.LocationFloat = New DevExpress.Utils.PointFloat(350.0!, 0.0!)
            Me.XrLabel5.Name = "XrLabel5"
            Me.XrLabel5.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel5.SizeF = New System.Drawing.SizeF(141.0!, 15.0!)
            Me.XrLabel5.StylePriority.UseTextAlignment = False
            Me.XrLabel5.Text = "ACCOUNT"
            Me.XrLabel5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            '
            'XrLabel4
            '
            Me.XrLabel4.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel4.ForeColor = System.Drawing.Color.White
            Me.XrLabel4.LocationFloat = New DevExpress.Utils.PointFloat(575.0!, 0.0!)
            Me.XrLabel4.Name = "XrLabel4"
            Me.XrLabel4.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel4.SizeF = New System.Drawing.SizeF(83.0!, 15.0!)
            Me.XrLabel4.Text = "DEBIT"
            Me.XrLabel4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel3
            '
            Me.XrLabel3.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel3.ForeColor = System.Drawing.Color.White
            Me.XrLabel3.LocationFloat = New DevExpress.Utils.PointFloat(492.0!, 0.0!)
            Me.XrLabel3.Name = "XrLabel3"
            Me.XrLabel3.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel3.SizeF = New System.Drawing.SizeF(83.0!, 15.0!)
            Me.XrLabel3.Text = "CREDIT"
            Me.XrLabel3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel2
            '
            Me.XrLabel2.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel2.ForeColor = System.Drawing.Color.White
            Me.XrLabel2.LocationFloat = New DevExpress.Utils.PointFloat(8.0!, 0.0!)
            Me.XrLabel2.Name = "XrLabel2"
            Me.XrLabel2.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel2.SizeF = New System.Drawing.SizeF(41.0!, 15.0!)
            Me.XrLabel2.Text = "TYPE"
            Me.XrLabel2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            '
            'XrLabel1
            '
            Me.XrLabel1.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel1.ForeColor = System.Drawing.Color.White
            Me.XrLabel1.LocationFloat = New DevExpress.Utils.PointFloat(58.0!, 0.0!)
            Me.XrLabel1.Name = "XrLabel1"
            Me.XrLabel1.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel1.SizeF = New System.Drawing.SizeF(50.0!, 15.0!)
            Me.XrLabel1.StylePriority.UseTextAlignment = False
            Me.XrLabel1.Text = "DATE"
            Me.XrLabel1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel_date_created
            '
            Me.XrLabel_date_created.CanGrow = False
            Me.XrLabel_date_created.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "date_created", "{0:d}")})
            Me.XrLabel_date_created.Font = New System.Drawing.Font("Times New Roman", 9.75!)
            Me.XrLabel_date_created.ForeColor = System.Drawing.SystemColors.WindowText
            Me.XrLabel_date_created.LocationFloat = New DevExpress.Utils.PointFloat(33.0!, 0.0!)
            Me.XrLabel_date_created.Name = "XrLabel_date_created"
            Me.XrLabel_date_created.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_date_created.SizeF = New System.Drawing.SizeF(75.0!, 15.0!)
            Me.XrLabel_date_created.StylePriority.UseFont = False
            Me.XrLabel_date_created.StylePriority.UseForeColor = False
            Me.XrLabel_date_created.StylePriority.UseTextAlignment = False
            Me.XrLabel_date_created.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel_tran_type
            '
            Me.XrLabel_tran_type.CanGrow = False
            Me.XrLabel_tran_type.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "tran_type")})
            Me.XrLabel_tran_type.Font = New System.Drawing.Font("Times New Roman", 9.75!)
            Me.XrLabel_tran_type.ForeColor = System.Drawing.SystemColors.WindowText
            Me.XrLabel_tran_type.LocationFloat = New DevExpress.Utils.PointFloat(8.0!, 0.0!)
            Me.XrLabel_tran_type.Name = "XrLabel_tran_type"
            Me.XrLabel_tran_type.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_tran_type.SizeF = New System.Drawing.SizeF(25.0!, 15.0!)
            Me.XrLabel_tran_type.StylePriority.UseFont = False
            Me.XrLabel_tran_type.StylePriority.UseForeColor = False
            Me.XrLabel_tran_type.StylePriority.UseTextAlignment = False
            Me.XrLabel_tran_type.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel_credit
            '
            Me.XrLabel_credit.CanGrow = False
            Me.XrLabel_credit.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "credit_amt", "{0:c}")})
            Me.XrLabel_credit.Font = New System.Drawing.Font("Times New Roman", 9.75!)
            Me.XrLabel_credit.ForeColor = System.Drawing.SystemColors.WindowText
            Me.XrLabel_credit.LocationFloat = New DevExpress.Utils.PointFloat(492.0!, 0.0!)
            Me.XrLabel_credit.Name = "XrLabel_credit"
            Me.XrLabel_credit.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_credit.SizeF = New System.Drawing.SizeF(83.0!, 15.0!)
            Me.XrLabel_credit.StylePriority.UseFont = False
            Me.XrLabel_credit.StylePriority.UseForeColor = False
            Me.XrLabel_credit.Text = "$0.00"
            Me.XrLabel_credit.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel_debit
            '
            Me.XrLabel_debit.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "debit_amt", "{0:c}")})
            Me.XrLabel_debit.Font = New System.Drawing.Font("Times New Roman", 9.75!)
            Me.XrLabel_debit.ForeColor = System.Drawing.SystemColors.WindowText
            Me.XrLabel_debit.LocationFloat = New DevExpress.Utils.PointFloat(575.0!, 0.0!)
            Me.XrLabel_debit.Name = "XrLabel_debit"
            Me.XrLabel_debit.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_debit.SizeF = New System.Drawing.SizeF(83.0!, 15.0!)
            Me.XrLabel_debit.StylePriority.UseFont = False
            Me.XrLabel_debit.StylePriority.UseForeColor = False
            Me.XrLabel_debit.Text = "$0.00"
            Me.XrLabel_debit.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'ReportFooter
            '
            Me.ReportFooter.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel_total_interest, Me.XrLabel8, Me.XrLine1, Me.XrLabel_Total_Credit, Me.XrLabel_Total_Debit})
            Me.ReportFooter.HeightF = 50.0!
            Me.ReportFooter.Name = "ReportFooter"
            '
            'XrLabel_total_interest
            '
            Me.XrLabel_total_interest.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "interest_amt", "{0:c}")})
            Me.XrLabel_total_interest.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel_total_interest.ForeColor = System.Drawing.SystemColors.WindowText
            Me.XrLabel_total_interest.LocationFloat = New DevExpress.Utils.PointFloat(658.0!, 25.0!)
            Me.XrLabel_total_interest.Name = "XrLabel_total_interest"
            Me.XrLabel_total_interest.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_total_interest.SizeF = New System.Drawing.SizeF(83.0!, 15.0!)
            Me.XrLabel_total_interest.StylePriority.UseForeColor = False
            XrSummary1.FormatString = "{0:c}"
            XrSummary1.Running = DevExpress.XtraReports.UI.SummaryRunning.Report
            Me.XrLabel_total_interest.Summary = XrSummary1
            Me.XrLabel_total_interest.Text = "$0.00"
            Me.XrLabel_total_interest.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel8
            '
            Me.XrLabel8.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel8.ForeColor = System.Drawing.SystemColors.WindowText
            Me.XrLabel8.LocationFloat = New DevExpress.Utils.PointFloat(8.0!, 25.0!)
            Me.XrLabel8.Name = "XrLabel8"
            Me.XrLabel8.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel8.SizeF = New System.Drawing.SizeF(200.0!, 15.0!)
            Me.XrLabel8.StylePriority.UseForeColor = False
            Me.XrLabel8.Text = "GRAND TOTALS"
            Me.XrLabel8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            '
            'XrLine1
            '
            Me.XrLine1.ForeColor = System.Drawing.Color.Teal
            Me.XrLine1.LineWidth = 2
            Me.XrLine1.LocationFloat = New DevExpress.Utils.PointFloat(8.0!, 8.0!)
            Me.XrLine1.Name = "XrLine1"
            Me.XrLine1.SizeF = New System.Drawing.SizeF(733.0!, 9.0!)
            Me.XrLine1.StylePriority.UseForeColor = False
            '
            'XrLabel_Total_Credit
            '
            Me.XrLabel_Total_Credit.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "credit_amt", "{0:c}")})
            Me.XrLabel_Total_Credit.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel_Total_Credit.ForeColor = System.Drawing.SystemColors.WindowText
            Me.XrLabel_Total_Credit.LocationFloat = New DevExpress.Utils.PointFloat(492.0!, 25.0!)
            Me.XrLabel_Total_Credit.Name = "XrLabel_Total_Credit"
            Me.XrLabel_Total_Credit.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_Total_Credit.SizeF = New System.Drawing.SizeF(83.0!, 15.0!)
            Me.XrLabel_Total_Credit.StylePriority.UseForeColor = False
            XrSummary2.FormatString = "{0:c}"
            XrSummary2.Running = DevExpress.XtraReports.UI.SummaryRunning.Report
            Me.XrLabel_Total_Credit.Summary = XrSummary2
            Me.XrLabel_Total_Credit.Text = "$0.00"
            Me.XrLabel_Total_Credit.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel_Total_Debit
            '
            Me.XrLabel_Total_Debit.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "debit_amt", "{0:c}")})
            Me.XrLabel_Total_Debit.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel_Total_Debit.ForeColor = System.Drawing.SystemColors.WindowText
            Me.XrLabel_Total_Debit.LocationFloat = New DevExpress.Utils.PointFloat(575.0!, 25.0!)
            Me.XrLabel_Total_Debit.Name = "XrLabel_Total_Debit"
            Me.XrLabel_Total_Debit.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_Total_Debit.SizeF = New System.Drawing.SizeF(83.0!, 15.0!)
            Me.XrLabel_Total_Debit.StylePriority.UseForeColor = False
            XrSummary3.FormatString = "{0:c}"
            XrSummary3.Running = DevExpress.XtraReports.UI.SummaryRunning.Report
            Me.XrLabel_Total_Debit.Summary = XrSummary3
            Me.XrLabel_Total_Debit.Text = "$0.00"
            Me.XrLabel_Total_Debit.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel_interest
            '
            Me.XrLabel_interest.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "interest_amt", "{0:c}")})
            Me.XrLabel_interest.Font = New System.Drawing.Font("Times New Roman", 9.75!)
            Me.XrLabel_interest.ForeColor = System.Drawing.SystemColors.WindowText
            Me.XrLabel_interest.LocationFloat = New DevExpress.Utils.PointFloat(658.0!, 0.0!)
            Me.XrLabel_interest.Name = "XrLabel_interest"
            Me.XrLabel_interest.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_interest.SizeF = New System.Drawing.SizeF(83.0!, 15.0!)
            Me.XrLabel_interest.StylePriority.UseFont = False
            Me.XrLabel_interest.StylePriority.UseForeColor = False
            Me.XrLabel_interest.Text = "$0.00"
            Me.XrLabel_interest.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel_creditor
            '
            Me.XrLabel_creditor.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "creditor_name")})
            Me.XrLabel_creditor.Font = New System.Drawing.Font("Times New Roman", 9.75!)
            Me.XrLabel_creditor.ForeColor = System.Drawing.SystemColors.WindowText
            Me.XrLabel_creditor.LocationFloat = New DevExpress.Utils.PointFloat(116.0!, 0.0!)
            Me.XrLabel_creditor.Name = "XrLabel_creditor"
            Me.XrLabel_creditor.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_creditor.SizeF = New System.Drawing.SizeF(233.0!, 15.0!)
            Me.XrLabel_creditor.StylePriority.UseFont = False
            Me.XrLabel_creditor.StylePriority.UseForeColor = False
            Me.XrLabel_creditor.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            Me.XrLabel_creditor.WordWrap = False
            '
            'XrLabel_account_number
            '
            Me.XrLabel_account_number.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "account_number")})
            Me.XrLabel_account_number.Font = New System.Drawing.Font("Times New Roman", 9.75!)
            Me.XrLabel_account_number.ForeColor = System.Drawing.SystemColors.WindowText
            Me.XrLabel_account_number.LocationFloat = New DevExpress.Utils.PointFloat(350.0!, 0.0!)
            Me.XrLabel_account_number.Name = "XrLabel_account_number"
            Me.XrLabel_account_number.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_account_number.SizeF = New System.Drawing.SizeF(141.0!, 15.0!)
            Me.XrLabel_account_number.StylePriority.UseFont = False
            Me.XrLabel_account_number.StylePriority.UseForeColor = False
            Me.XrLabel_account_number.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            Me.XrLabel_account_number.WordWrap = False
            '
            'GroupHeader1
            '
            Me.GroupHeader1.GroupFields.AddRange(New DevExpress.XtraReports.UI.GroupField() {New DevExpress.XtraReports.UI.GroupField("formatted_date_created", DevExpress.XtraReports.UI.XRColumnSortOrder.Ascending)})
            Me.GroupHeader1.GroupUnion = DevExpress.XtraReports.UI.GroupUnion.WithFirstDetail
            Me.GroupHeader1.HeightF = 0.0!
            Me.GroupHeader1.Name = "GroupHeader1"
            '
            'GroupFooter1
            '
            Me.GroupFooter1.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel_Subtotal_Interest, Me.XrLabel_Subtotal, Me.XrLabel_Subtotal_Debit, Me.XrLabel_Subtotal_Credit})
            Me.GroupFooter1.HeightF = 33.0!
            Me.GroupFooter1.Name = "GroupFooter1"
            '
            'XrLabel_Subtotal_Interest
            '
            Me.XrLabel_Subtotal_Interest.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "interest_amt", "{0:c}")})
            Me.XrLabel_Subtotal_Interest.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel_Subtotal_Interest.ForeColor = System.Drawing.SystemColors.WindowText
            Me.XrLabel_Subtotal_Interest.LocationFloat = New DevExpress.Utils.PointFloat(658.0!, 0.0!)
            Me.XrLabel_Subtotal_Interest.Name = "XrLabel_Subtotal_Interest"
            Me.XrLabel_Subtotal_Interest.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_Subtotal_Interest.SizeF = New System.Drawing.SizeF(83.0!, 15.0!)
            Me.XrLabel_Subtotal_Interest.StylePriority.UseForeColor = False
            XrSummary4.FormatString = "{0:c}"
            XrSummary4.Running = DevExpress.XtraReports.UI.SummaryRunning.Group
            Me.XrLabel_Subtotal_Interest.Summary = XrSummary4
            Me.XrLabel_Subtotal_Interest.Text = "$0.00"
            Me.XrLabel_Subtotal_Interest.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel_Subtotal
            '
            Me.XrLabel_Subtotal.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "date_created", "DAILY TOTALS FOR {0:d}")})
            Me.XrLabel_Subtotal.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel_Subtotal.ForeColor = System.Drawing.SystemColors.WindowText
            Me.XrLabel_Subtotal.LocationFloat = New DevExpress.Utils.PointFloat(117.0!, 0.0!)
            Me.XrLabel_Subtotal.Name = "XrLabel_Subtotal"
            Me.XrLabel_Subtotal.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_Subtotal.SizeF = New System.Drawing.SizeF(308.0!, 15.0!)
            Me.XrLabel_Subtotal.StylePriority.UseForeColor = False
            Me.XrLabel_Subtotal.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            '
            'XrLabel_Subtotal_Debit
            '
            Me.XrLabel_Subtotal_Debit.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "debit_amt", "{0:c}")})
            Me.XrLabel_Subtotal_Debit.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel_Subtotal_Debit.ForeColor = System.Drawing.SystemColors.WindowText
            Me.XrLabel_Subtotal_Debit.LocationFloat = New DevExpress.Utils.PointFloat(575.0!, 0.0!)
            Me.XrLabel_Subtotal_Debit.Name = "XrLabel_Subtotal_Debit"
            Me.XrLabel_Subtotal_Debit.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_Subtotal_Debit.SizeF = New System.Drawing.SizeF(83.0!, 15.0!)
            Me.XrLabel_Subtotal_Debit.StylePriority.UseForeColor = False
            XrSummary5.FormatString = "{0:c}"
            XrSummary5.Running = DevExpress.XtraReports.UI.SummaryRunning.Group
            Me.XrLabel_Subtotal_Debit.Summary = XrSummary5
            Me.XrLabel_Subtotal_Debit.Text = "$0.00"
            Me.XrLabel_Subtotal_Debit.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel_Subtotal_Credit
            '
            Me.XrLabel_Subtotal_Credit.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "credit_amt", "{0:c}")})
            Me.XrLabel_Subtotal_Credit.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel_Subtotal_Credit.ForeColor = System.Drawing.SystemColors.WindowText
            Me.XrLabel_Subtotal_Credit.LocationFloat = New DevExpress.Utils.PointFloat(492.0!, 0.0!)
            Me.XrLabel_Subtotal_Credit.Name = "XrLabel_Subtotal_Credit"
            Me.XrLabel_Subtotal_Credit.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_Subtotal_Credit.SizeF = New System.Drawing.SizeF(83.0!, 15.0!)
            Me.XrLabel_Subtotal_Credit.StylePriority.UseForeColor = False
            XrSummary6.FormatString = "{0:c}"
            XrSummary6.Running = DevExpress.XtraReports.UI.SummaryRunning.Group
            Me.XrLabel_Subtotal_Credit.Summary = XrSummary6
            Me.XrLabel_Subtotal_Credit.Text = "$0.00"
            Me.XrLabel_Subtotal_Credit.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'formatted_date_created
            '
            Me.formatted_date_created.Expression = "GetDate([date_created])"
            Me.formatted_date_created.Name = "formatted_date_created"
            '
            'ParameterClient
            '
            Me.ParameterClient.Description = "Client ID"
            Me.ParameterClient.Name = "ParameterClient"
            Me.ParameterClient.Type = GetType(Integer)
            Me.ParameterClient.Value = -1
            Me.ParameterClient.Visible = False
            '
            'ClientTrustActivityReport
            '
            Me.Bands.AddRange(New DevExpress.XtraReports.UI.Band() {Me.Detail, Me.PageHeader, Me.PageFooter, Me.TopMarginBand1, Me.BottomMarginBand1, Me.ReportFooter, Me.GroupHeader1, Me.GroupFooter1})
            Me.CalculatedFields.AddRange(New DevExpress.XtraReports.UI.CalculatedField() {Me.formatted_date_created})
            Me.Parameters.AddRange(New DevExpress.XtraReports.Parameters.Parameter() {Me.ParameterClient})
            Me.ScriptLanguage = DevExpress.XtraReports.ScriptLanguage.VisualBasic
            Me.ScriptReferencesString = resources.GetString("$this.ScriptReferencesString")
            Me.Scripts.OnBeforePrint = "ClientTrustActivityReport_BeforePrint"
            Me.ScriptsSource = resources.GetString("$this.ScriptsSource")
            Me.StyleSheet.AddRange(New DevExpress.XtraReports.UI.XRControlStyle() {Me.XrControlStyle_Header, Me.XrControlStyle_Totals, Me.XrControlStyle_HeaderPannel, Me.XrControlStyle_GroupHeader})
            Me.Version = "11.2"
            Me.Controls.SetChildIndex(Me.GroupFooter1, 0)
            Me.Controls.SetChildIndex(Me.GroupHeader1, 0)
            Me.Controls.SetChildIndex(Me.ReportFooter, 0)
            Me.Controls.SetChildIndex(Me.BottomMarginBand1, 0)
            Me.Controls.SetChildIndex(Me.TopMarginBand1, 0)
            Me.Controls.SetChildIndex(Me.PageFooter, 0)
            Me.Controls.SetChildIndex(Me.PageHeader, 0)
            Me.Controls.SetChildIndex(Me.Detail, 0)
            CType(Me, System.ComponentModel.ISupportInitialize).EndInit()

        End Sub
        Friend WithEvents XrPanel1 As DevExpress.XtraReports.UI.XRPanel
        Friend WithEvents XrLabel5 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel4 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel3 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel2 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel1 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_debit As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_credit As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_tran_type As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_date_created As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents ReportFooter As DevExpress.XtraReports.UI.ReportFooterBand
        Friend WithEvents XrLine1 As DevExpress.XtraReports.UI.XRLine
        Friend WithEvents XrLabel_Total_Credit As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_Total_Debit As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel8 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel7 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel6 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_interest As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_account_number As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_creditor As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_total_interest As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents GroupHeader1 As DevExpress.XtraReports.UI.GroupHeaderBand
        Friend WithEvents GroupFooter1 As DevExpress.XtraReports.UI.GroupFooterBand
        Friend WithEvents XrLabel_Subtotal_Interest As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_Subtotal_Debit As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_Subtotal_Credit As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_Subtotal As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents formatted_date_created As DevExpress.XtraReports.UI.CalculatedField
        Friend WithEvents ParameterClient As DevExpress.XtraReports.Parameters.Parameter
    End Class
End Namespace
