#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}
#End Region

Imports DebtPlus.Interfaces.Client
Imports DebtPlus.Reports.Template
Imports System.IO
Imports DebtPlus.Reports.Template.Forms
Imports DebtPlus.Utils

Namespace Client.TrustActivity

    Public Class ClientTrustActivityReport
        Inherits DatedTemplateXtraReportClass
        Implements IClient

        Public Sub New()
            MyBase.New()
            Const ReportName As String = "DebtPlus.Reports.Client.TrustActivity.repx"

            ' See if there is a report reference that we can use
            Dim Fname As String = System.IO.Path.Combine(DebtPlus.Configuration.Config.ReportsDirectory, ReportName)
            Dim UseDefault As Boolean = True
            Try
                If (Not DebtPlus.Configuration.Config.LocalReportsOnly) AndAlso System.IO.File.Exists(Fname) Then
                    LoadLayout(Fname)
                    UseDefault = False
                End If
            Catch ex As Exception
            End Try

            ' Load the standard report definition if we need a new item
            If UseDefault Then
                Dim asm As Reflection.Assembly = Reflection.Assembly.GetExecutingAssembly                                           ' changed
                Using ios As Stream = asm.GetManifestResourceStream(String.Format("DebtPlus.Reports.{0}", ReportName))              ' changed
                    If ios IsNot Nothing Then LoadLayout(ios) '                                                                       changed
                End Using
            End If

            ScriptReferences = DebtPlus.Utils.AssemblyTools.GetAllSiblingAssemblies()

            ' Copy the script references to the subreports as well
            For Each Band As DevExpress.XtraReports.UI.Band In Bands
                For Each ctl As DevExpress.XtraReports.UI.XRControl In Band.Controls
                    Dim subReport As DevExpress.XtraReports.UI.XRSubreport = TryCast(ctl, DevExpress.XtraReports.UI.XRSubreport)
                    If subReport IsNot Nothing Then
                        Dim rpt As DevExpress.XtraReports.UI.XtraReport = subReport.ReportSource
                        If rpt IsNot Nothing Then
                            rpt.ScriptReferences = ScriptReferences
                        End If
                    End If
                Next
            Next

            ' Empty the parameters
            Parameter_Client = -1
        End Sub

        Public Property Parameter_Client() As System.Int32
            Get
                Return Convert.ToInt32(Parameters("ParameterClient").Value)
            End Get
            Set(ByVal value As System.Int32)
                Parameters("ParameterClient").Value = value
            End Set
        End Property

        Public Property ClientId As Int32 Implements IClient.ClientId
            Get
                Return Parameter_Client
            End Get
            Set(value As Int32)
                Parameter_Client = value
            End Set
        End Property

        Public Overrides ReadOnly Property ReportTitle() As String
            Get
                Return "Client Trust Activity"
            End Get
        End Property

        Public Overrides ReadOnly Property ReportSubTitle() As String
            Get
                Return String.Format("{0}{1}Client ID: {2}", MyBase.ReportSubtitle, Environment.NewLine, String.Format("{0:0000000}", Parameters("ParameterClient").Value))
            End Get
        End Property

        Public Overrides Sub SetReportParameter(ByVal Parameter As String, ByVal Value As Object)
            If Parameter = "client" Then
                Parameter_Client = Convert.ToInt32(Value)
            Else
                MyBase.SetReportParameter(Parameter, Value)
            End If
        End Sub

        Public Overrides Function NeedParameters() As Boolean
            Return MyBase.NeedParameters OrElse Parameter_Client < 0
        End Function

        Public Overrides Function RequestReportParameters() As System.Windows.Forms.DialogResult
            Dim answer As System.Windows.Forms.DialogResult = DialogResult.OK
            If NeedParameters() Then
                With New DatedClientParametersForm()
                    .Parameter_Client = Parameter_Client
                    answer = .ShowDialog()
                    Parameter_FromDate = .Parameter_FromDate
                    Parameter_ToDate = .Parameter_ToDate
                    Parameter_Client = .Parameter_Client
                End With
            End If
            Return answer
        End Function
    End Class
End Namespace