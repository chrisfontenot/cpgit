#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}

#End Region

Option Compare Binary
Option Explicit On
Option Strict On

Imports System.ComponentModel
Imports DebtPlus.Reports.Template

Imports System.Drawing.Printing
Imports System.Data.SqlClient
Imports DevExpress.XtraReports.UI

Namespace Housing.NoResults
    Public Class HousingNoResultsReport
        Inherits DatedTemplateXtraReportClass


        ''' <summary>
        ''' Create an instance of our report
        ''' </summary>
        Public Sub New()
            MyBase.New()
            InitializeComponent()
            AddHandler Me.BeforePrint, AddressOf Report_BeforePrint
        End Sub

#Region " Component Designer generated code "

        'Component overrides dispose to clean up the component list.
        Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
            If disposing Then
                If components IsNot Nothing Then
                    components.Dispose()
                End If
            End If
            MyBase.Dispose(disposing)
        End Sub

        Friend WithEvents GroupHeader1 As DevExpress.XtraReports.UI.GroupHeaderBand
        Friend WithEvents ReportFooter As DevExpress.XtraReports.UI.ReportFooterBand
        Friend WithEvents XrPanel1 As DevExpress.XtraReports.UI.XRPanel
        Friend WithEvents XrLabel6 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel3 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_counselor As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel2 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel1 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_hud_id As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_client_id_and_name As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_interview_created As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_interview_description As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_total_count As DevExpress.XtraReports.UI.XRLabel

        'Required by the Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Designer
        'It can be modified using the Designer.
        'Do not modify it using the code editor.
        <System.Diagnostics.DebuggerStepThrough()>
        Private Sub InitializeComponent()
            Dim XrSummary1 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary
            Me.GroupHeader1 = New DevExpress.XtraReports.UI.GroupHeaderBand
            Me.XrLabel_counselor = New DevExpress.XtraReports.UI.XRLabel
            Me.ReportFooter = New DevExpress.XtraReports.UI.ReportFooterBand
            Me.XrLabel_total_count = New DevExpress.XtraReports.UI.XRLabel
            Me.XrPanel1 = New DevExpress.XtraReports.UI.XRPanel
            Me.XrLabel2 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel1 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel6 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel3 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_interview_description = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_interview_created = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_client_id_and_name = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_hud_id = New DevExpress.XtraReports.UI.XRLabel
            CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
            '
            'Detail
            '
            Me.Detail.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel_hud_id, Me.XrLabel_client_id_and_name, Me.XrLabel_interview_created, Me.XrLabel_interview_description})
            Me.Detail.Height = 15
            '
            'PageHeader
            '
            Me.PageHeader.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrPanel1})
            Me.PageHeader.Height = 143
            '
            'GroupHeader1
            '
            Me.GroupHeader1.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel_counselor})
            Me.GroupHeader1.GroupFields.AddRange(New DevExpress.XtraReports.UI.GroupField() {New DevExpress.XtraReports.UI.GroupField("counselor", DevExpress.XtraReports.UI.XRColumnSortOrder.Ascending)})
            Me.GroupHeader1.Height = 42
            Me.GroupHeader1.Name = "GroupHeader1"
            '
            'XrLabel_counselor
            '
            Me.XrLabel_counselor.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel_counselor.ForeColor = System.Drawing.Color.Maroon
            Me.XrLabel_counselor.Location = New System.Drawing.Point(0, 17)
            Me.XrLabel_counselor.Name = "XrLabel_counselor"
            Me.XrLabel_counselor.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_counselor.Size = New System.Drawing.Size(800, 15)
            Me.XrLabel_counselor.StylePriority.UseFont = False
            Me.XrLabel_counselor.StylePriority.UseForeColor = False
            Me.XrLabel_counselor.Text = "XrLabel_counselor"
            '
            'ReportFooter
            '
            Me.ReportFooter.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel_total_count})
            Me.ReportFooter.Height = 42
            Me.ReportFooter.Name = "ReportFooter"
            Me.ReportFooter.PrintAtBottom = True
            '
            'XrLabel_total_count
            '
            Me.XrLabel_total_count.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel_total_count.ForeColor = System.Drawing.Color.Teal
            Me.XrLabel_total_count.Location = New System.Drawing.Point(0, 17)
            Me.XrLabel_total_count.Name = "XrLabel_total_count"
            Me.XrLabel_total_count.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_total_count.Size = New System.Drawing.Size(800, 17)
            Me.XrLabel_total_count.StylePriority.UseFont = False
            Me.XrLabel_total_count.StylePriority.UseForeColor = False
            XrSummary1.FormatString = "{0:n0} item(s) listed"
            XrSummary1.Func = DevExpress.XtraReports.UI.SummaryFunc.Count
            XrSummary1.Running = DevExpress.XtraReports.UI.SummaryRunning.Report
            Me.XrLabel_total_count.Summary = XrSummary1
            '
            'XrPanel1
            '
            Me.XrPanel1.BackColor = System.Drawing.Color.Teal
            Me.XrPanel1.BorderColor = System.Drawing.Color.Teal
            Me.XrPanel1.CanGrow = False
            Me.XrPanel1.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel2, Me.XrLabel1, Me.XrLabel6, Me.XrLabel3})
            Me.XrPanel1.KeepTogether = False
            Me.XrPanel1.Location = New System.Drawing.Point(0, 117)
            Me.XrPanel1.Name = "XrPanel1"
            Me.XrPanel1.Size = New System.Drawing.Size(800, 18)
            Me.XrPanel1.StylePriority.UseBackColor = False
            Me.XrPanel1.StylePriority.UseBorderColor = False
            '
            'XrLabel2
            '
            Me.XrLabel2.BackColor = System.Drawing.Color.Transparent
            Me.XrLabel2.BorderColor = System.Drawing.Color.Transparent
            Me.XrLabel2.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel2.ForeColor = System.Drawing.Color.White
            Me.XrLabel2.Location = New System.Drawing.Point(14, 0)
            Me.XrLabel2.Name = "XrLabel2"
            Me.XrLabel2.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel2.Size = New System.Drawing.Size(60, 15)
            Me.XrLabel2.StylePriority.UseBackColor = False
            Me.XrLabel2.StylePriority.UseBorderColor = False
            Me.XrLabel2.StylePriority.UseFont = False
            Me.XrLabel2.StylePriority.UseForeColor = False
            Me.XrLabel2.StylePriority.UseTextAlignment = False
            Me.XrLabel2.Text = "HUD ID"
            Me.XrLabel2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel1
            '
            Me.XrLabel1.BackColor = System.Drawing.Color.Transparent
            Me.XrLabel1.BorderColor = System.Drawing.Color.Transparent
            Me.XrLabel1.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel1.ForeColor = System.Drawing.Color.White
            Me.XrLabel1.Location = New System.Drawing.Point(100, 0)
            Me.XrLabel1.Name = "XrLabel1"
            Me.XrLabel1.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel1.Size = New System.Drawing.Size(58, 15)
            Me.XrLabel1.StylePriority.UseBackColor = False
            Me.XrLabel1.StylePriority.UseBorderColor = False
            Me.XrLabel1.StylePriority.UseFont = False
            Me.XrLabel1.StylePriority.UseForeColor = False
            Me.XrLabel1.StylePriority.UseTextAlignment = False
            Me.XrLabel1.Text = "DATE"
            Me.XrLabel1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel6
            '
            Me.XrLabel6.BackColor = System.Drawing.Color.Transparent
            Me.XrLabel6.BorderColor = System.Drawing.Color.Transparent
            Me.XrLabel6.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel6.ForeColor = System.Drawing.Color.White
            Me.XrLabel6.Location = New System.Drawing.Point(458, 0)
            Me.XrLabel6.Name = "XrLabel6"
            Me.XrLabel6.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel6.Size = New System.Drawing.Size(164, 15)
            Me.XrLabel6.StylePriority.UseBackColor = False
            Me.XrLabel6.StylePriority.UseBorderColor = False
            Me.XrLabel6.StylePriority.UseFont = False
            Me.XrLabel6.StylePriority.UseForeColor = False
            Me.XrLabel6.StylePriority.UseTextAlignment = False
            Me.XrLabel6.Text = "PURPOSE OF VISIT"
            Me.XrLabel6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            '
            'XrLabel3
            '
            Me.XrLabel3.BackColor = System.Drawing.Color.Transparent
            Me.XrLabel3.BorderColor = System.Drawing.Color.Transparent
            Me.XrLabel3.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel3.ForeColor = System.Drawing.Color.White
            Me.XrLabel3.Location = New System.Drawing.Point(166, 0)
            Me.XrLabel3.Name = "XrLabel3"
            Me.XrLabel3.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel3.Size = New System.Drawing.Size(191, 15)
            Me.XrLabel3.StylePriority.UseBackColor = False
            Me.XrLabel3.StylePriority.UseBorderColor = False
            Me.XrLabel3.StylePriority.UseFont = False
            Me.XrLabel3.StylePriority.UseForeColor = False
            Me.XrLabel3.StylePriority.UseTextAlignment = False
            Me.XrLabel3.Text = "CLIENT ID AND NAME"
            Me.XrLabel3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            '
            'XrLabel_interview_description
            '
            Me.XrLabel_interview_description.BackColor = System.Drawing.Color.Transparent
            Me.XrLabel_interview_description.BorderColor = System.Drawing.Color.Transparent
            Me.XrLabel_interview_description.Font = New System.Drawing.Font("Times New Roman", 9.75!)
            Me.XrLabel_interview_description.ForeColor = System.Drawing.SystemColors.ControlText
            Me.XrLabel_interview_description.Location = New System.Drawing.Point(458, 0)
            Me.XrLabel_interview_description.Name = "XrLabel_interview_description"
            Me.XrLabel_interview_description.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_interview_description.Size = New System.Drawing.Size(342, 15)
            Me.XrLabel_interview_description.StylePriority.UseBackColor = False
            Me.XrLabel_interview_description.StylePriority.UseBorderColor = False
            Me.XrLabel_interview_description.StylePriority.UseFont = False
            Me.XrLabel_interview_description.StylePriority.UseForeColor = False
            Me.XrLabel_interview_description.StylePriority.UseTextAlignment = False
            Me.XrLabel_interview_description.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            '
            'XrLabel_interview_created
            '
            Me.XrLabel_interview_created.BackColor = System.Drawing.Color.Transparent
            Me.XrLabel_interview_created.BorderColor = System.Drawing.Color.Transparent
            Me.XrLabel_interview_created.Font = New System.Drawing.Font("Times New Roman", 9.75!)
            Me.XrLabel_interview_created.ForeColor = System.Drawing.SystemColors.ControlText
            Me.XrLabel_interview_created.Location = New System.Drawing.Point(83, 0)
            Me.XrLabel_interview_created.Name = "XrLabel_interview_created"
            Me.XrLabel_interview_created.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_interview_created.Size = New System.Drawing.Size(75, 15)
            Me.XrLabel_interview_created.StylePriority.UseBackColor = False
            Me.XrLabel_interview_created.StylePriority.UseBorderColor = False
            Me.XrLabel_interview_created.StylePriority.UseFont = False
            Me.XrLabel_interview_created.StylePriority.UseForeColor = False
            Me.XrLabel_interview_created.StylePriority.UseTextAlignment = False
            Me.XrLabel_interview_created.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel_client_id_and_name
            '
            Me.XrLabel_client_id_and_name.BackColor = System.Drawing.Color.Transparent
            Me.XrLabel_client_id_and_name.BorderColor = System.Drawing.Color.Transparent
            Me.XrLabel_client_id_and_name.Font = New System.Drawing.Font("Times New Roman", 9.75!)
            Me.XrLabel_client_id_and_name.ForeColor = System.Drawing.SystemColors.ControlText
            Me.XrLabel_client_id_and_name.Location = New System.Drawing.Point(166, 0)
            Me.XrLabel_client_id_and_name.Name = "XrLabel_client_id_and_name"
            Me.XrLabel_client_id_and_name.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_client_id_and_name.Size = New System.Drawing.Size(284, 15)
            Me.XrLabel_client_id_and_name.StylePriority.UseBackColor = False
            Me.XrLabel_client_id_and_name.StylePriority.UseBorderColor = False
            Me.XrLabel_client_id_and_name.StylePriority.UseFont = False
            Me.XrLabel_client_id_and_name.StylePriority.UseForeColor = False
            Me.XrLabel_client_id_and_name.StylePriority.UseTextAlignment = False
            Me.XrLabel_client_id_and_name.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            '
            'XrLabel_hud_id
            '
            Me.XrLabel_hud_id.BackColor = System.Drawing.Color.Transparent
            Me.XrLabel_hud_id.BorderColor = System.Drawing.Color.Transparent
            Me.XrLabel_hud_id.Font = New System.Drawing.Font("Times New Roman", 9.75!)
            Me.XrLabel_hud_id.ForeColor = System.Drawing.SystemColors.ControlText
            Me.XrLabel_hud_id.Location = New System.Drawing.Point(0, 0)
            Me.XrLabel_hud_id.Name = "XrLabel_hud_id"
            Me.XrLabel_hud_id.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_hud_id.Size = New System.Drawing.Size(74, 15)
            Me.XrLabel_hud_id.StylePriority.UseBackColor = False
            Me.XrLabel_hud_id.StylePriority.UseBorderColor = False
            Me.XrLabel_hud_id.StylePriority.UseFont = False
            Me.XrLabel_hud_id.StylePriority.UseForeColor = False
            Me.XrLabel_hud_id.StylePriority.UseTextAlignment = False
            Me.XrLabel_hud_id.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'HousingNoResultsReport
            '
            Me.Bands.AddRange(New DevExpress.XtraReports.UI.Band() {Me.GroupHeader1, Me.ReportFooter})
            Me.Version = "8.1"
            CType(Me, System.ComponentModel.ISupportInitialize).EndInit()
        End Sub

#End Region

        Public Overrides ReadOnly Property ReportTitle() As String
            Get
                Return "Housing No Results"
            End Get
        End Property

        Dim ds As New DataSet("ds")
        Dim vue As DataView = Nothing

        Private Sub Report_BeforePrint(ByVal sender As Object, ByVal e As PrintEventArgs)

            ' Read the transactions
            Dim cn As SqlConnection = New System.Data.SqlClient.SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
            Dim current_cursor As Cursor = Cursor.Current
            Try
                Cursor.Current = Cursors.WaitCursor
                Dim cmd As SqlCommand = New System.Data.SqlClient.SqlCommand()
                cn.Open()
                With cmd
                    .Connection = cn
                    .CommandText = "rpt_housing_no_result"
                    .CommandType = CommandType.StoredProcedure
                    SqlCommandBuilder.DeriveParameters(cmd)

                    .Parameters(1).Value = Parameter_FromDate
                    .Parameters(2).Value = Parameter_ToDate
                    .CommandTimeout = 0
                End With

                Dim da As New SqlDataAdapter(cmd)
                da.Fill(ds, "records")
                vue = ds.Tables(0).DefaultView

                ' Bind the datasource to the items
                DataSource = vue

                With XrLabel_counselor
                    .DataBindings.Add("Text", vue, "counselor_name")
                End With

                With XrLabel_hud_id
                    .DataBindings.Add("Text", vue, "hud_id")
                End With

                With XrLabel_interview_created
                    .DataBindings.Add("Text", vue, "interview_created", "{0:d}")
                End With

                With XrLabel_interview_description
                    .DataBindings.Add("Text", vue, "interview_description")
                End With

                With XrLabel_total_count
                    .DataBindings.Add("Text", vue, "client")
                End With

                With XrLabel_client_id_and_name
                    AddHandler .BeforePrint, AddressOf XrLabel_client_id_and_name_BeforePrint
                End With

            Catch ex As Exception
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error reading transactions")

            Finally
                If cn IsNot Nothing AndAlso cn.State <> ConnectionState.Closed Then cn.Close()
                Cursor.Current = current_cursor
            End Try
        End Sub

        Private Sub XrLabel_client_id_and_name_BeforePrint(ByVal sender As Object, ByVal e As PrintEventArgs)
            With CType(sender, XRLabel)
                .Text = String.Format("{0:0000000}", DebtPlus.Utils.Nulls.DInt(GetCurrentColumnValue("client"))) + " " + DebtPlus.Utils.Nulls.DStr(GetCurrentColumnValue("client_name"))
            End With
        End Sub
    End Class
End Namespace
