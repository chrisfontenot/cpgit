#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}
#End Region

Option Compare Binary
Option Explicit On
Option Strict On

Imports DebtPlus.Reports.Template
Imports DebtPlus.Reports.Template.Forms

Namespace Client.CloseToPayoff.ByDebt

    Public Class CloseToPayoffByDebtReport
        Inherits TemplateXtraReportClass

        Public Sub New()
            MyBase.New()
            InitializeComponent()
            AddHandler Me.BeforePrint, AddressOf CloseToPayoffReport_RefreshDataReport
        End Sub

        Private privateParameter_Counselor As Object = Nothing
        Public Property Parameter_Counselor() As Object
            Get
                Return privateParameter_Counselor
            End Get
            Set(ByVal Value As Object)
                privateParameter_Counselor = Value
            End Set
        End Property

        Public Overrides Sub SetReportParameter(ByVal Parameter As String, ByVal Value As Object)
            Select Case Parameter
                Case "Counselor"
                    Parameter_Counselor = Value
                Case Else
                    MyBase.SetReportParameter(Parameter, Value)
            End Select
        End Sub

        Public Overrides Function RequestReportParameters() As System.Windows.Forms.DialogResult
            Dim answer As System.Windows.Forms.DialogResult
            With New CounselorParametersForm(True)
                answer = .ShowDialog()
                Parameter_Counselor = .Parameter_Counselor
                .Dispose()
            End With            
            Return answer
        End Function

        Public Overrides ReadOnly Property ReportTitle() As String
            Get
                Return "Close to Payoff"
            End Get
        End Property

        Private _ReportSubTitle As String = System.String.Empty
        Public Overrides ReadOnly Property ReportSubTitle() As String
            Get
                Return _ReportSubTitle
            End Get
        End Property

#Region " Component Designer generated code "

        'Component overrides dispose to clean up the component list.
        Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
            If disposing Then
                If components IsNot Nothing Then
                    components.Dispose()
                End If
            End If
            MyBase.Dispose(disposing)
        End Sub

        'Required by the Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Designer
        'It can be modified using the Designer.
        'Do not modify it using the code editor.
        Friend WithEvents GroupHeader1 As DevExpress.XtraReports.UI.GroupHeaderBand
        Friend WithEvents XrPanel1 As DevExpress.XtraReports.UI.XRPanel
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
            Me.GroupHeader1 = New DevExpress.XtraReports.UI.GroupHeaderBand
            Me.XrLabel_creditor_name = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_creditor = New DevExpress.XtraReports.UI.XRLabel
            Me.XrPanel1 = New DevExpress.XtraReports.UI.XRPanel
            Me.XrLabel7 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel6 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel1 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel4 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel3 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel2 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel5 = New DevExpress.XtraReports.UI.XRLabel
            Me.GroupFooter1 = New DevExpress.XtraReports.UI.GroupFooterBand
            Me.XrLabel9 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrControlStyle1 = New DevExpress.XtraReports.UI.XRControlStyle
            Me.XrControlStyle2 = New DevExpress.XtraReports.UI.XRControlStyle
            Me.XrLabel_client = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_last_payment_date = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_current_balance = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_disbursement_factor = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_orig_balance = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_account_number = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_held_in_trust = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_last_payment_amt = New DevExpress.XtraReports.UI.XRLabel
            CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
            '
            'Detail
            '
            Me.Detail.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel_last_payment_date, Me.XrLabel_last_payment_amt, Me.XrLabel_held_in_trust, Me.XrLabel_account_number, Me.XrLabel_orig_balance, Me.XrLabel_disbursement_factor, Me.XrLabel_current_balance, Me.XrLabel_client})
            Me.Detail.Height = 15
            '
            'PageHeader
            '
            Me.PageHeader.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrPanel1})
            Me.PageHeader.Height = 146
            '
            'GroupHeader1
            '
            Me.GroupHeader1.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel_creditor_name, Me.XrLabel_creditor})
            Me.GroupHeader1.GroupFields.AddRange(New DevExpress.XtraReports.UI.GroupField() {New DevExpress.XtraReports.UI.GroupField("creditor", DevExpress.XtraReports.UI.XRColumnSortOrder.Ascending)})
            Me.GroupHeader1.Height = 42
            Me.GroupHeader1.Name = "GroupHeader1"
            Me.GroupHeader1.RepeatEveryPage = True
            '
            'XrLabel_creditor_name
            '
            Me.XrLabel_creditor_name.Font = New System.Drawing.Font("Times New Roman", 12.0!, System.Drawing.FontStyle.Bold)
            Me.XrLabel_creditor_name.ForeColor = System.Drawing.Color.Maroon
            Me.XrLabel_creditor_name.Location = New System.Drawing.Point(108, 8)
            Me.XrLabel_creditor_name.Name = "XrLabel_creditor_name"
            Me.XrLabel_creditor_name.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_creditor_name.Size = New System.Drawing.Size(683, 25)
            Me.XrLabel_creditor_name.StylePriority.UseFont = False
            Me.XrLabel_creditor_name.StylePriority.UseForeColor = False
            '
            'XrLabel_creditor
            '
            Me.XrLabel_creditor.Font = New System.Drawing.Font("Times New Roman", 12.0!, System.Drawing.FontStyle.Bold)
            Me.XrLabel_creditor.ForeColor = System.Drawing.Color.Maroon
            Me.XrLabel_creditor.Location = New System.Drawing.Point(0, 8)
            Me.XrLabel_creditor.Name = "XrLabel_creditor"
            Me.XrLabel_creditor.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_creditor.Size = New System.Drawing.Size(100, 25)
            Me.XrLabel_creditor.StylePriority.UseFont = False
            Me.XrLabel_creditor.StylePriority.UseForeColor = False
            '
            'XrPanel1
            '
            Me.XrPanel1.BackColor = System.Drawing.Color.Teal
            Me.XrPanel1.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel7, Me.XrLabel6, Me.XrLabel1, Me.XrLabel4, Me.XrLabel3, Me.XrLabel2, Me.XrLabel5})
            Me.XrPanel1.Location = New System.Drawing.Point(0, 125)
            Me.XrPanel1.Name = "XrPanel1"
            Me.XrPanel1.Size = New System.Drawing.Size(800, 16)
            Me.XrPanel1.StyleName = "XrControlStyle1"
            '
            'XrLabel7
            '
            Me.XrLabel7.Location = New System.Drawing.Point(217, 0)
            Me.XrLabel7.Name = "XrLabel7"
            Me.XrLabel7.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel7.Size = New System.Drawing.Size(125, 15)
            Me.XrLabel7.Text = "ACCOUNT"
            '
            'XrLabel6
            '
            Me.XrLabel6.Location = New System.Drawing.Point(521, 1)
            Me.XrLabel6.Name = "XrLabel6"
            Me.XrLabel6.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel6.Size = New System.Drawing.Size(66, 15)
            Me.XrLabel6.StylePriority.UseTextAlignment = False
            Me.XrLabel6.Text = "CUR BAL"
            Me.XrLabel6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
            '
            'XrLabel1
            '
            Me.XrLabel1.Location = New System.Drawing.Point(0, 1)
            Me.XrLabel1.Name = "XrLabel1"
            Me.XrLabel1.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel1.Size = New System.Drawing.Size(118, 15)
            Me.XrLabel1.Text = "Client"
            '
            'XrLabel4
            '
            Me.XrLabel4.Location = New System.Drawing.Point(600, 1)
            Me.XrLabel4.Name = "XrLabel4"
            Me.XrLabel4.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel4.Size = New System.Drawing.Size(120, 15)
            Me.XrLabel4.StylePriority.UseTextAlignment = False
            Me.XrLabel4.Text = "LAST PAYMENT"
            Me.XrLabel4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel3
            '
            Me.XrLabel3.Location = New System.Drawing.Point(367, 1)
            Me.XrLabel3.Name = "XrLabel3"
            Me.XrLabel3.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel3.Size = New System.Drawing.Size(72, 15)
            Me.XrLabel3.StylePriority.UseTextAlignment = False
            Me.XrLabel3.Text = "ORIG BAL"
            Me.XrLabel3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel2
            '
            Me.XrLabel2.Location = New System.Drawing.Point(724, 0)
            Me.XrLabel2.Name = "XrLabel2"
            Me.XrLabel2.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
            Me.XrLabel2.Size = New System.Drawing.Size(68, 16)
            Me.XrLabel2.StylePriority.UsePadding = False
            Me.XrLabel2.StylePriority.UseTextAlignment = False
            Me.XrLabel2.Text = "TRUST"
            Me.XrLabel2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel5
            '
            Me.XrLabel5.Location = New System.Drawing.Point(439, 1)
            Me.XrLabel5.Name = "XrLabel5"
            Me.XrLabel5.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
            Me.XrLabel5.Size = New System.Drawing.Size(66, 15)
            Me.XrLabel5.StylePriority.UsePadding = False
            Me.XrLabel5.StylePriority.UseTextAlignment = False
            Me.XrLabel5.Text = "DISB FAC"
            Me.XrLabel5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'GroupFooter1
            '
            Me.GroupFooter1.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel9})
            Me.GroupFooter1.Height = 17
            Me.GroupFooter1.Name = "GroupFooter1"
            Me.GroupFooter1.StyleName = "XrControlStyle2"
            '
            'XrLabel9
            '
            Me.XrLabel9.Location = New System.Drawing.Point(0, 0)
            Me.XrLabel9.Name = "XrLabel9"
            Me.XrLabel9.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel9.Size = New System.Drawing.Size(100, 17)
            '
            'XrControlStyle1
            '
            Me.XrControlStyle1.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.XrControlStyle1.ForeColor = System.Drawing.Color.White
            Me.XrControlStyle1.Name = "XrControlStyle1"
            Me.XrControlStyle1.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
            Me.XrControlStyle1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            '
            'XrControlStyle2
            '
            Me.XrControlStyle2.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.XrControlStyle2.Name = "XrControlStyle2"
            Me.XrControlStyle2.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
            Me.XrControlStyle2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel_client
            '
            Me.XrLabel_client.CanGrow = False
            Me.XrLabel_client.Location = New System.Drawing.Point(0, 0)
            Me.XrLabel_client.Name = "XrLabel_client"
            Me.XrLabel_client.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_client.Size = New System.Drawing.Size(217, 15)
            Me.XrLabel_client.StylePriority.UseTextAlignment = False
            Me.XrLabel_client.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            Me.XrLabel_client.WordWrap = False
            '
            'XrLabel_last_payment_date
            '
            Me.XrLabel_last_payment_date.CanGrow = False
            Me.XrLabel_last_payment_date.Location = New System.Drawing.Point(587, 0)
            Me.XrLabel_last_payment_date.Name = "XrLabel_last_payment_date"
            Me.XrLabel_last_payment_date.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_last_payment_date.Size = New System.Drawing.Size(75, 15)
            Me.XrLabel_last_payment_date.StylePriority.UseTextAlignment = False
            Me.XrLabel_last_payment_date.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrLabel_last_payment_date.WordWrap = False
            '
            'XrLabel_current_balance
            '
            Me.XrLabel_current_balance.CanGrow = False
            Me.XrLabel_current_balance.Location = New System.Drawing.Point(505, 0)
            Me.XrLabel_current_balance.Name = "XrLabel_current_balance"
            Me.XrLabel_current_balance.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_current_balance.Size = New System.Drawing.Size(82, 15)
            Me.XrLabel_current_balance.StylePriority.UseTextAlignment = False
            Me.XrLabel_current_balance.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrLabel_current_balance.WordWrap = False
            '
            'XrLabel_disbursement_factor
            '
            Me.XrLabel_disbursement_factor.CanGrow = False
            Me.XrLabel_disbursement_factor.Location = New System.Drawing.Point(439, 0)
            Me.XrLabel_disbursement_factor.Name = "XrLabel_disbursement_factor"
            Me.XrLabel_disbursement_factor.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_disbursement_factor.Size = New System.Drawing.Size(66, 15)
            Me.XrLabel_disbursement_factor.StylePriority.UsePadding = False
            Me.XrLabel_disbursement_factor.StylePriority.UseTextAlignment = False
            Me.XrLabel_disbursement_factor.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrLabel_disbursement_factor.WordWrap = False
            '
            'XrLabel_orig_balance
            '
            Me.XrLabel_orig_balance.CanGrow = False
            Me.XrLabel_orig_balance.Location = New System.Drawing.Point(367, 0)
            Me.XrLabel_orig_balance.Name = "XrLabel_orig_balance"
            Me.XrLabel_orig_balance.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_orig_balance.Size = New System.Drawing.Size(72, 15)
            Me.XrLabel_orig_balance.StylePriority.UseTextAlignment = False
            Me.XrLabel_orig_balance.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrLabel_orig_balance.WordWrap = False
            '
            'XrLabel_account_number
            '
            Me.XrLabel_account_number.CanGrow = False
            Me.XrLabel_account_number.Location = New System.Drawing.Point(217, 0)
            Me.XrLabel_account_number.Name = "XrLabel_account_number"
            Me.XrLabel_account_number.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_account_number.Size = New System.Drawing.Size(150, 15)
            Me.XrLabel_account_number.StylePriority.UseTextAlignment = False
            Me.XrLabel_account_number.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            Me.XrLabel_account_number.WordWrap = False
            '
            'XrLabel_held_in_trust
            '
            Me.XrLabel_held_in_trust.CanGrow = False
            Me.XrLabel_held_in_trust.Location = New System.Drawing.Point(720, 0)
            Me.XrLabel_held_in_trust.Name = "XrLabel_held_in_trust"
            Me.XrLabel_held_in_trust.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_held_in_trust.Size = New System.Drawing.Size(72, 15)
            Me.XrLabel_held_in_trust.StylePriority.UsePadding = False
            Me.XrLabel_held_in_trust.StylePriority.UseTextAlignment = False
            Me.XrLabel_held_in_trust.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrLabel_held_in_trust.WordWrap = False
            '
            'XrLabel_last_payment_amt
            '
            Me.XrLabel_last_payment_amt.CanGrow = False
            Me.XrLabel_last_payment_amt.Location = New System.Drawing.Point(625, 0)
            Me.XrLabel_last_payment_amt.Name = "XrLabel_last_payment_amt"
            Me.XrLabel_last_payment_amt.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_last_payment_amt.Size = New System.Drawing.Size(96, 15)
            Me.XrLabel_last_payment_amt.StylePriority.UseTextAlignment = False
            Me.XrLabel_last_payment_amt.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrLabel_last_payment_amt.WordWrap = False
            '
            'CloseToPayoffByDebtReport
            '
            Me.Bands.AddRange(New DevExpress.XtraReports.UI.Band() {Me.GroupHeader1, Me.GroupFooter1})
            Me.StyleSheet.AddRange(New DevExpress.XtraReports.UI.XRControlStyle() {Me.XrControlStyle1, Me.XrControlStyle2})
            Me.Version = "8.3"
            CType(Me, System.ComponentModel.ISupportInitialize).EndInit()
        End Sub

        Friend WithEvents GroupFooter1 As DevExpress.XtraReports.UI.GroupFooterBand
        Friend WithEvents XrControlStyle1 As DevExpress.XtraReports.UI.XRControlStyle
        Friend WithEvents XrControlStyle2 As DevExpress.XtraReports.UI.XRControlStyle
        Friend WithEvents XrLabel7 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel6 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel1 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel4 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel3 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel2 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel5 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_last_payment_amt As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_held_in_trust As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_account_number As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_orig_balance As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_disbursement_factor As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_current_balance As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_last_payment_date As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_client As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_creditor_name As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_creditor As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel9 As DevExpress.XtraReports.UI.XRLabel
#End Region

        Dim ds As New System.Data.DataSet("ds")

        Private Sub CloseToPayoffReport_RefreshDataReport(ByVal sender As Object, ByVal e As System.EventArgs)
            Using cmd As SqlClient.SqlCommand = New System.Data.SqlClient.SqlCommand()
                With cmd
                    .Connection = New System.Data.SqlClient.SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
                    .CommandText = "rpt_close_to_payoff_debts"
                    .CommandType = CommandType.StoredProcedure
                    .CommandTimeout = 0

                    If DebtPlus.Utils.Nulls.DInt(Parameter_Counselor) > 0 Then
                        _ReportSubTitle = String.Format("This report shows counselor # {0:f0}", Parameter_Counselor)
                        .Parameters.Add("@counselor", SqlDbType.Int).Value = Parameter_Counselor
                    Else
                        _ReportSubTitle = "This report shows all counselors"
                        .Parameters.Add("@counselor", SqlDbType.Int).Value = System.DBNull.Value
                    End If
                    .CommandTimeout = 0
                End With

                Using da As New SqlClient.SqlDataAdapter(cmd)
                    da.Fill(ds, "rpt_close_to_payoff_debts")
                End Using
            End Using

            Dim vue As System.Data.DataView = ds.Tables("rpt_close_to_payoff_debts").DefaultView
            DataSource = vue

            With XrLabel_client
                AddHandler .BeforePrint, AddressOf XrLabel_client_BeforePrint
            End With

            With XrLabel_account_number
                .DataBindings.Add("Text", vue, "account_number")
            End With

            With XrLabel_creditor
                .DataBindings.Add("Text", vue, "creditor")
            End With

            With XrLabel_creditor_name
                .DataBindings.Add("Text", vue, "creditor_name")
            End With

            With XrLabel_current_balance
                .DataBindings.Add("Text", vue, "current_balance", "{0:c}")
            End With

            With XrLabel_disbursement_factor
                .DataBindings.Add("Text", vue, "disbursement_factor", "{0:c}")
            End With

            With XrLabel_held_in_trust
                .DataBindings.Add("Text", vue, "held_in_trust", "{0:c}")
            End With

            With XrLabel_last_payment_amt
                .DataBindings.Add("Text", vue, "last_payment_amt", "{0:c}")
            End With

            With XrLabel_last_payment_date
                .DataBindings.Add("Text", vue, "last_payment_date", "{0:d}")
            End With

            With XrLabel_orig_balance
                .DataBindings.Add("Text", vue, "orig_balance", "{0:c}")
            End With

        End Sub

        Private Sub XrLabel_client_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs)
            XrLabel_client.Text = "[" + String.Format("{0:0000000}", DebtPlus.Utils.Nulls.DInt(GetCurrentColumnValue("client"))) + "] " + DebtPlus.Utils.Nulls.DStr(GetCurrentColumnValue("client_name"))
        End Sub
    End Class

End Namespace
