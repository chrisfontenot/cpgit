Namespace Client.Budget
    <Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
    Partial Class ClientBudgetReport
        Inherits DebtPlus.Reports.Template.TemplateXtraReportClass

        'XtraReport overrides dispose to clean up the component list.
        <System.Diagnostics.DebuggerNonUserCode()> _
        Protected Overrides Sub Dispose(ByVal disposing As Boolean)
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
            MyBase.Dispose(disposing)
        End Sub

        'Required by the Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Designer
        'It can be modified using the Designer.  
        'Do not modify it using the code editor.
        <System.Diagnostics.DebuggerStepThrough()> _
        Private Sub InitializeComponent()
            Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(ClientBudgetReport))
            Me.ParameterClient = New DevExpress.XtraReports.Parameters.Parameter()
            Me.ParameterBudget = New DevExpress.XtraReports.Parameters.Parameter()
            Me.XrLabel_ClientID = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_ClientName = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrSubreport_Budget = New DevExpress.XtraReports.UI.XRSubreport()
            Me.BudgetItems1 = New DebtPlus.Reports.Client.Budget.Budget()
            CType(Me.BudgetItems1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
            '
            'PageFooter
            '
            Me.PageFooter.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel_ClientName, Me.XrLabel_ClientID})
            Me.PageFooter.HeightF = 39.74997!
            Me.PageFooter.Controls.SetChildIndex(Me.XrLabel_ClientID, 0)
            Me.PageFooter.Controls.SetChildIndex(Me.XrLabel_ClientName, 0)
            Me.PageFooter.Controls.SetChildIndex(Me.XrPageInfo_PageNumber, 0)
            '
            'XrLabel_Title
            '
            Me.XrLabel_Title.StylePriority.UseFont = False
            '
            'XrPageInfo_PageNumber
            '
            Me.XrPageInfo_PageNumber.LocationFloat = New DevExpress.Utils.PointFloat(609.0!, 0.0!)
            Me.XrPageInfo_PageNumber.StylePriority.UseTextAlignment = False
            '
            'XRLabel_Agency_Name
            '
            Me.XRLabel_Agency_Name.StylePriority.UseTextAlignment = False
            '
            'XrLabel_Agency_Address3
            '
            Me.XrLabel_Agency_Address3.StylePriority.UseFont = False
            Me.XrLabel_Agency_Address3.StylePriority.UseTextAlignment = False
            '
            'XrLabel_Agency_Address1
            '
            Me.XrLabel_Agency_Address1.StylePriority.UseFont = False
            Me.XrLabel_Agency_Address1.StylePriority.UseTextAlignment = False
            '
            'XrLabel_Agency_Phone
            '
            Me.XrLabel_Agency_Phone.StylePriority.UseFont = False
            Me.XrLabel_Agency_Phone.StylePriority.UseTextAlignment = False
            '
            'XrLabel_Agency_Address2
            '
            Me.XrLabel_Agency_Address2.StylePriority.UseFont = False
            Me.XrLabel_Agency_Address2.StylePriority.UseTextAlignment = False
            '
            'Detail
            '
            Me.Detail.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrSubreport_Budget})
            Me.Detail.HeightF = 23.0!
            '
            'ParameterClient
            '
            Me.ParameterClient.Description = "Client ID"
            Me.ParameterClient.Name = "ParameterClient"
            Me.ParameterClient.Type = GetType(Integer)
            Me.ParameterClient.Value = -1
            Me.ParameterClient.Visible = False
            '
            'ParameterBudget
            '
            Me.ParameterBudget.Description = "Budget ID"
            Me.ParameterBudget.Name = "ParameterBudget"
            Me.ParameterBudget.Type = GetType(Integer)
            Me.ParameterBudget.Value = -1
            Me.ParameterBudget.Visible = False
            '
            'XrLabel_ClientID
            '
            Me.XrLabel_ClientID.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel_ClientID.ForeColor = System.Drawing.Color.Teal
            Me.XrLabel_ClientID.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
            Me.XrLabel_ClientID.Name = "XrLabel_ClientID"
            Me.XrLabel_ClientID.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_ClientID.Scripts.OnBeforePrint = "XrLabel_ClientID_BeforePrint"
            Me.XrLabel_ClientID.SizeF = New System.Drawing.SizeF(400.0!, 17.0!)
            Me.XrLabel_ClientID.StylePriority.UseFont = False
            Me.XrLabel_ClientID.StylePriority.UseForeColor = False
            '
            'XrLabel_ClientName
            '
            Me.XrLabel_ClientName.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel_ClientName.ForeColor = System.Drawing.Color.Teal
            Me.XrLabel_ClientName.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 17.0!)
            Me.XrLabel_ClientName.Name = "XrLabel_ClientName"
            Me.XrLabel_ClientName.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_ClientName.Scripts.OnBeforePrint = "XrLabel_ClientName_BeforePrint"
            Me.XrLabel_ClientName.SizeF = New System.Drawing.SizeF(742.0!, 17.0!)
            Me.XrLabel_ClientName.StylePriority.UseFont = False
            Me.XrLabel_ClientName.StylePriority.UseForeColor = False
            '
            'XrSubreport_Budget
            '
            Me.XrSubreport_Budget.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
            Me.XrSubreport_Budget.Name = "XrSubreport_Budget"
            Me.XrSubreport_Budget.ReportSource = Me.BudgetItems1
            Me.XrSubreport_Budget.SizeF = New System.Drawing.SizeF(742.0!, 23.0!)
            '
            'ClientBudgetReport
            '
            Me.Bands.AddRange(New DevExpress.XtraReports.UI.Band() {Me.TopMarginBand1, Me.Detail, Me.BottomMarginBand1, Me.PageHeader, Me.PageFooter})
            Me.Parameters.AddRange(New DevExpress.XtraReports.Parameters.Parameter() {Me.ParameterClient, Me.ParameterBudget})
            Me.RequestParameters = False
            Me.ScriptLanguage = DevExpress.XtraReports.ScriptLanguage.VisualBasic
            Me.ScriptReferencesString = resources.GetString("$this.ScriptReferencesString")
            Me.ScriptsSource = resources.GetString("$this.ScriptsSource")
            Me.StyleSheet.AddRange(New DevExpress.XtraReports.UI.XRControlStyle() {Me.XrControlStyle_Header, Me.XrControlStyle_Totals, Me.XrControlStyle_HeaderPannel, Me.XrControlStyle_GroupHeader})
            Me.Version = "11.2"
            CType(Me.BudgetItems1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me, System.ComponentModel.ISupportInitialize).EndInit()

        End Sub
        Friend WithEvents XrSubreport_Budget As DevExpress.XtraReports.UI.XRSubreport
        Friend WithEvents ParameterClient As DevExpress.XtraReports.Parameters.Parameter
        Private WithEvents BudgetItems1 As DebtPlus.Reports.Client.Budget.Budget
        Friend WithEvents ParameterBudget As DevExpress.XtraReports.Parameters.Parameter
        Friend WithEvents XrLabel_ClientName As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_ClientID As DevExpress.XtraReports.UI.XRLabel
    End Class
End Namespace
