Namespace Client.Budget
    <Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
    Partial Class Budget
        Inherits DevExpress.XtraReports.UI.XtraReport

        'XtraReport overrides dispose to clean up the component list.
        <System.Diagnostics.DebuggerNonUserCode()> _
        Protected Overrides Sub Dispose(ByVal disposing As Boolean)
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
            MyBase.Dispose(disposing)
        End Sub

        'Required by the Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Designer
        'It can be modified using the Designer.  
        'Do not modify it using the code editor.
        <System.Diagnostics.DebuggerStepThrough()> _
        Private Sub InitializeComponent()
            Dim XrSummary1 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary()
            Dim XrSummary2 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary()
            Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Budget))
            Me.Detail = New DevExpress.XtraReports.UI.DetailBand()
            Me.XrLabel_suggested_amount = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_client_amount = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_detail = New DevExpress.XtraReports.UI.XRLabel()
            Me.BottomMargin = New DevExpress.XtraReports.UI.BottomMarginBand()
            Me.XrLabel_header = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel3 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrControlStyle_Totals = New DevExpress.XtraReports.UI.XRControlStyle()
            Me.XrControlStyle_Header = New DevExpress.XtraReports.UI.XRControlStyle()
            Me.ParameterBudget = New DevExpress.XtraReports.Parameters.Parameter()
            Me.XrLabel7 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLine1 = New DevExpress.XtraReports.UI.XRLine()
            Me.ReportFooter = New DevExpress.XtraReports.UI.ReportFooterBand()
            Me.XrLabel_total_client_amount = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_total_suggested_amount = New DevExpress.XtraReports.UI.XRLabel()
            Me.TopMargin = New DevExpress.XtraReports.UI.TopMarginBand()
            Me.XrControlStyle_GroupHeader = New DevExpress.XtraReports.UI.XRControlStyle()
            Me.PageHeader = New DevExpress.XtraReports.UI.PageHeaderBand()
            Me.XrLabel4 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel2 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrControlStyle_HeaderPannel = New DevExpress.XtraReports.UI.XRControlStyle()
            CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
            '
            'Detail
            '
            Me.Detail.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel_suggested_amount, Me.XrLabel_client_amount, Me.XrLabel_detail})
            Me.Detail.HeightF = 17.0!
            Me.Detail.Name = "Detail"
            Me.Detail.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
            Me.Detail.StylePriority.UseTextAlignment = False
            Me.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            '
            'XrLabel_suggested_amount
            '
            Me.XrLabel_suggested_amount.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "suggested_amount", "{0:c}")})
            Me.XrLabel_suggested_amount.LocationFloat = New DevExpress.Utils.PointFloat(375.0!, 0.0!)
            Me.XrLabel_suggested_amount.Name = "XrLabel_suggested_amount"
            Me.XrLabel_suggested_amount.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_suggested_amount.Scripts.OnBeforePrint = "XrLabel_suggested_amount_BeforePrint"
            Me.XrLabel_suggested_amount.SizeF = New System.Drawing.SizeF(125.0!, 17.0!)
            Me.XrLabel_suggested_amount.StylePriority.UseTextAlignment = False
            Me.XrLabel_suggested_amount.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel_client_amount
            '
            Me.XrLabel_client_amount.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "client_amount", "{0:c}")})
            Me.XrLabel_client_amount.LocationFloat = New DevExpress.Utils.PointFloat(283.0!, 0.0!)
            Me.XrLabel_client_amount.Name = "XrLabel_client_amount"
            Me.XrLabel_client_amount.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_client_amount.Scripts.OnBeforePrint = "XrLabel_client_amount_BeforePrint"
            Me.XrLabel_client_amount.SizeF = New System.Drawing.SizeF(91.0!, 17.0!)
            Me.XrLabel_client_amount.StylePriority.UseTextAlignment = False
            Me.XrLabel_client_amount.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel_detail
            '
            Me.XrLabel_detail.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "description")})
            Me.XrLabel_detail.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
            Me.XrLabel_detail.Name = "XrLabel_detail"
            Me.XrLabel_detail.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_detail.Scripts.OnBeforePrint = "XrLabel_detail_BeforePrint"
            Me.XrLabel_detail.SizeF = New System.Drawing.SizeF(268.0!, 17.0!)
            '
            'BottomMargin
            '
            Me.BottomMargin.HeightF = 0.0!
            Me.BottomMargin.Name = "BottomMargin"
            Me.BottomMargin.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
            Me.BottomMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
            '
            'XrLabel_header
            '
            Me.XrLabel_header.Font = New System.Drawing.Font("Times New Roman", 18.0!, System.Drawing.FontStyle.Bold)
            Me.XrLabel_header.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
            Me.XrLabel_header.Name = "XrLabel_header"
            Me.XrLabel_header.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_header.SizeF = New System.Drawing.SizeF(500.0!, 33.0!)
            Me.XrLabel_header.StylePriority.UseFont = False
            Me.XrLabel_header.StylePriority.UseTextAlignment = False
            Me.XrLabel_header.Text = "BUDGET INFORMATION"
            Me.XrLabel_header.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
            '
            'XrLabel3
            '
            Me.XrLabel3.Font = New System.Drawing.Font("Times New Roman", 9.75!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Underline), System.Drawing.FontStyle))
            Me.XrLabel3.LocationFloat = New DevExpress.Utils.PointFloat(283.0!, 50.0!)
            Me.XrLabel3.Name = "XrLabel3"
            Me.XrLabel3.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel3.SizeF = New System.Drawing.SizeF(91.0!, 17.0!)
            Me.XrLabel3.StylePriority.UseFont = False
            Me.XrLabel3.StylePriority.UseTextAlignment = False
            Me.XrLabel3.Text = "CLIENT"
            Me.XrLabel3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrControlStyle_Totals
            '
            Me.XrControlStyle_Totals.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.XrControlStyle_Totals.Name = "XrControlStyle_Totals"
            Me.XrControlStyle_Totals.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
            Me.XrControlStyle_Totals.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrControlStyle_Header
            '
            Me.XrControlStyle_Header.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.XrControlStyle_Header.ForeColor = System.Drawing.Color.Teal
            Me.XrControlStyle_Header.Name = "XrControlStyle_Header"
            Me.XrControlStyle_Header.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
            Me.XrControlStyle_Header.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            '
            'ParameterBudget
            '
            Me.ParameterBudget.Description = "Budget ID"
            Me.ParameterBudget.Name = "ParameterBudget"
            Me.ParameterBudget.Type = GetType(Integer)
            Me.ParameterBudget.Value = -1
            Me.ParameterBudget.Visible = False
            '
            'XrLabel7
            '
            Me.XrLabel7.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel7.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 17.0!)
            Me.XrLabel7.Name = "XrLabel7"
            Me.XrLabel7.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel7.SizeF = New System.Drawing.SizeF(268.0!, 17.0!)
            Me.XrLabel7.StylePriority.UseFont = False
            Me.XrLabel7.Text = "TOTALS"
            '
            'XrLine1
            '
            Me.XrLine1.ForeColor = System.Drawing.Color.Teal
            Me.XrLine1.LineWidth = 2
            Me.XrLine1.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
            Me.XrLine1.Name = "XrLine1"
            Me.XrLine1.SizeF = New System.Drawing.SizeF(500.0!, 9.0!)
            Me.XrLine1.StylePriority.UseForeColor = False
            '
            'ReportFooter
            '
            Me.ReportFooter.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLine1, Me.XrLabel7, Me.XrLabel_total_client_amount, Me.XrLabel_total_suggested_amount})
            Me.ReportFooter.HeightF = 34.0!
            Me.ReportFooter.Name = "ReportFooter"
            '
            'XrLabel_total_client_amount
            '
            Me.XrLabel_total_client_amount.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "client_amount")})
            Me.XrLabel_total_client_amount.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel_total_client_amount.LocationFloat = New DevExpress.Utils.PointFloat(283.0!, 17.0!)
            Me.XrLabel_total_client_amount.Name = "XrLabel_total_client_amount"
            Me.XrLabel_total_client_amount.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_total_client_amount.SizeF = New System.Drawing.SizeF(91.0!, 17.0!)
            Me.XrLabel_total_client_amount.StylePriority.UseFont = False
            Me.XrLabel_total_client_amount.StylePriority.UseTextAlignment = False
            XrSummary1.FormatString = "{0:c}"
            XrSummary1.IgnoreNullValues = True
            XrSummary1.Running = DevExpress.XtraReports.UI.SummaryRunning.Report
            Me.XrLabel_total_client_amount.Summary = XrSummary1
            Me.XrLabel_total_client_amount.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel_total_suggested_amount
            '
            Me.XrLabel_total_suggested_amount.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "suggested_amount")})
            Me.XrLabel_total_suggested_amount.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel_total_suggested_amount.LocationFloat = New DevExpress.Utils.PointFloat(375.0!, 17.0!)
            Me.XrLabel_total_suggested_amount.Name = "XrLabel_total_suggested_amount"
            Me.XrLabel_total_suggested_amount.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_total_suggested_amount.SizeF = New System.Drawing.SizeF(125.0!, 17.0!)
            Me.XrLabel_total_suggested_amount.StylePriority.UseFont = False
            Me.XrLabel_total_suggested_amount.StylePriority.UseTextAlignment = False
            XrSummary2.FormatString = "{0:c}"
            XrSummary2.IgnoreNullValues = True
            XrSummary2.Running = DevExpress.XtraReports.UI.SummaryRunning.Report
            Me.XrLabel_total_suggested_amount.Summary = XrSummary2
            Me.XrLabel_total_suggested_amount.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'TopMargin
            '
            Me.TopMargin.HeightF = 0.0!
            Me.TopMargin.Name = "TopMargin"
            Me.TopMargin.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
            Me.TopMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
            '
            'XrControlStyle_GroupHeader
            '
            Me.XrControlStyle_GroupHeader.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.XrControlStyle_GroupHeader.ForeColor = System.Drawing.Color.Maroon
            Me.XrControlStyle_GroupHeader.Name = "XrControlStyle_GroupHeader"
            Me.XrControlStyle_GroupHeader.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            '
            'PageHeader
            '
            Me.PageHeader.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel4, Me.XrLabel3, Me.XrLabel2, Me.XrLabel_header})
            Me.PageHeader.HeightF = 72.0!
            Me.PageHeader.Name = "PageHeader"
            Me.PageHeader.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
            Me.PageHeader.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
            '
            'XrLabel4
            '
            Me.XrLabel4.Font = New System.Drawing.Font("Times New Roman", 9.75!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Underline), System.Drawing.FontStyle))
            Me.XrLabel4.LocationFloat = New DevExpress.Utils.PointFloat(375.0!, 50.0!)
            Me.XrLabel4.Name = "XrLabel4"
            Me.XrLabel4.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel4.SizeF = New System.Drawing.SizeF(125.0!, 17.0!)
            Me.XrLabel4.StylePriority.UseFont = False
            Me.XrLabel4.StylePriority.UseTextAlignment = False
            Me.XrLabel4.Text = "SUGGESTED"
            Me.XrLabel4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel2
            '
            Me.XrLabel2.Font = New System.Drawing.Font("Times New Roman", 9.75!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Underline), System.Drawing.FontStyle))
            Me.XrLabel2.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 50.0!)
            Me.XrLabel2.Name = "XrLabel2"
            Me.XrLabel2.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel2.SizeF = New System.Drawing.SizeF(100.0!, 17.0!)
            Me.XrLabel2.StylePriority.UseFont = False
            Me.XrLabel2.Text = "CATEGORY"
            '
            'XrControlStyle_HeaderPannel
            '
            Me.XrControlStyle_HeaderPannel.BackColor = System.Drawing.Color.Teal
            Me.XrControlStyle_HeaderPannel.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.XrControlStyle_HeaderPannel.ForeColor = System.Drawing.Color.White
            Me.XrControlStyle_HeaderPannel.Name = "XrControlStyle_HeaderPannel"
            Me.XrControlStyle_HeaderPannel.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
            Me.XrControlStyle_HeaderPannel.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'Budget
            '
            Me.Bands.AddRange(New DevExpress.XtraReports.UI.Band() {Me.Detail, Me.TopMargin, Me.BottomMargin, Me.PageHeader, Me.ReportFooter})
            Me.Margins = New System.Drawing.Printing.Margins(25, 25, 0, 0)
            Me.Parameters.AddRange(New DevExpress.XtraReports.Parameters.Parameter() {Me.ParameterBudget})
            Me.RequestParameters = False
            Me.ScriptLanguage = DevExpress.XtraReports.ScriptLanguage.VisualBasic
            Me.ScriptReferencesString = resources.GetString("$this.ScriptReferencesString")
            Me.Scripts.OnBeforePrint = "Budget_BeforePrint"
            Me.ScriptsSource = resources.GetString("$this.ScriptsSource")
            Me.StyleSheet.AddRange(New DevExpress.XtraReports.UI.XRControlStyle() {Me.XrControlStyle_Header, Me.XrControlStyle_Totals, Me.XrControlStyle_HeaderPannel, Me.XrControlStyle_GroupHeader})
            Me.Version = "11.2"
            CType(Me, System.ComponentModel.ISupportInitialize).EndInit()

        End Sub
        Friend WithEvents Detail As DevExpress.XtraReports.UI.DetailBand
        Friend WithEvents XrLabel_suggested_amount As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_client_amount As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_detail As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents BottomMargin As DevExpress.XtraReports.UI.BottomMarginBand
        Friend WithEvents XrLabel_header As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel3 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrControlStyle_Totals As DevExpress.XtraReports.UI.XRControlStyle
        Friend WithEvents XrControlStyle_Header As DevExpress.XtraReports.UI.XRControlStyle
        Friend WithEvents ParameterBudget As DevExpress.XtraReports.Parameters.Parameter
        Friend WithEvents XrLabel7 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLine1 As DevExpress.XtraReports.UI.XRLine
        Friend WithEvents ReportFooter As DevExpress.XtraReports.UI.ReportFooterBand
        Friend WithEvents XrLabel_total_client_amount As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_total_suggested_amount As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents TopMargin As DevExpress.XtraReports.UI.TopMarginBand
        Friend WithEvents XrControlStyle_GroupHeader As DevExpress.XtraReports.UI.XRControlStyle
        Friend WithEvents PageHeader As DevExpress.XtraReports.UI.PageHeaderBand
        Friend WithEvents XrLabel4 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel2 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrControlStyle_HeaderPannel As DevExpress.XtraReports.UI.XRControlStyle
    End Class
End Namespace
