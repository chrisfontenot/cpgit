#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}
#End Region

Namespace Client.Budget
    Public Class ClientBudgetReport
        Implements DebtPlus.Interfaces.Client.IClient

        ''' <summary>
        ''' Create an instance of our report
        ''' </summary>
        Public Sub New()
            MyBase.New()
            InitializeComponent()

            '-- Ensure that the parameters are defaulted correctly
            Parameters("ParameterBudget").Value = -1
            Parameters("ParameterClient").Value = -1

            AddHandler XrLabel_ClientID.BeforePrint, AddressOf XrLabel_ClientID_BeforePrint
            AddHandler XrLabel_ClientName.BeforePrint, AddressOf XrLabel_ClientName_BeforePrint

            ' Set the script references as needed
            ScriptReferences = DebtPlus.Utils.AssemblyTools.GetAllSiblingAssemblies()

            ' Update the subreports as well
            For Each BandPtr As DevExpress.XtraReports.UI.Band In Bands
                For Each CtlPtr As DevExpress.XtraReports.UI.XRControl In BandPtr.Controls
                    Dim Rpt As DevExpress.XtraReports.UI.XRSubreport = TryCast(CtlPtr, DevExpress.XtraReports.UI.XRSubreport)
                    If Rpt IsNot Nothing Then
                        Dim RptSrc As DevExpress.XtraReports.UI.XtraReport = TryCast(Rpt.ReportSource, DevExpress.XtraReports.UI.XtraReport)
                        If RptSrc IsNot Nothing Then
                            RptSrc.ScriptReferences = ScriptReferences
                        End If
                    End If
                Next
            Next
        End Sub

        Public Property ClientID As Int32 Implements DebtPlus.Interfaces.Client.IClient.ClientId
            Get
                Return Parameter_Client
            End Get
            Set(value As Int32)
                Parameter_Client = value
            End Set
        End Property

        Public Property Parameter_Client() As System.Int32
            Get
                Dim parm As DevExpress.XtraReports.Parameters.Parameter = FindParameter("ParameterClient")
                If parm Is Nothing Then Return -1
                Return Convert.ToInt32(parm.Value)
            End Get
            Set(ByVal value As System.Int32)
                SetParameter("ParameterClient", GetType(Int32), value, "Client ID", False)
            End Set
        End Property

        Public Property Parameter_Budget() As System.Int32
            Get
                Dim parm As DevExpress.XtraReports.Parameters.Parameter = FindParameter("ParameterBudget")
                If parm Is Nothing Then Return -1
                Return Convert.ToInt32(parm.Value)
            End Get
            Set(ByVal value As System.Int32)
                SetParameter("ParameterBudget", GetType(Int32), value, "Client ID", False)
            End Set
        End Property

        Public Overrides Function NeedParameters() As Boolean
            Return MyBase.NeedParameters() OrElse (Parameter_Budget <= 0 AndAlso Parameter_Client <= 0)
        End Function

        Public Overrides ReadOnly Property ReportTitle() As String
            Get
                Return "Budget Detail"
            End Get
        End Property

        Public Overrides Sub SetReportParameter(ByVal Parameter As String, ByVal Value As Object)
            Select Case Parameter
                Case "Budget"
                    Parameter_Budget = Convert.ToInt32(Value)
                Case "Client"
                    Parameter_Client = Convert.ToInt32(Value)
                Case Else
                    MyBase.SetReportParameter(Parameter, Value)
            End Select
        End Sub

        Public Overrides Function RequestReportParameters() As System.Windows.Forms.DialogResult
            Dim Answer As System.Windows.Forms.DialogResult = DialogResult.OK
            If NeedParameters() Then
                Using frm As New DebtPlus.Reports.Template.Forms.ClientParametersForm()
                    Answer = frm.ShowDialog
                    Parameter_Client = frm.Parameter_Client
                End Using
            End If
            Return Answer
        End Function

        Private Sub XrLabel_ClientID_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs)
            Dim ClientID As Int32 = -1
            With CType(sender, DevExpress.XtraReports.UI.XRLabel)
                Dim rpt As DevExpress.XtraReports.UI.XtraReport = CType(.Report, DevExpress.XtraReports.UI.XtraReport)
                Dim sqlInfo As DebtPlus.LINQ.SQLInfoClass = DebtPlus.LINQ.SQLInfoClass.getDefault()
                Dim obj As Object = rpt.Parameters("ParameterClient").Value
                If obj Is Nothing OrElse obj Is System.DBNull.Value Then
                    obj = -1
                End If

                ClientID = Convert.ToInt32(obj, System.Globalization.CultureInfo.InvariantCulture)
                If ClientID < 0 Then
                    obj = rpt.Parameters("Budget").Value
                    If obj Is Nothing OrElse obj Is System.DBNull.Value Then
                        obj = -1
                    End If

                    Dim PlanID As Int32 = Convert.ToInt32(obj, System.Globalization.CultureInfo.InvariantCulture)
                    If PlanID > 0 Then
                        Using cn As System.Data.SqlClient.SqlConnection = New System.Data.SqlClient.SqlConnection(sqlInfo.ConnectionString)
                            cn.Open()
                            Using cmd As New System.Data.SqlClient.SqlCommand()
                                cmd.Connection = cn
                                cmd.CommandText = "SELECT client FROM budgets WHERE budget=@Budget"
                                cmd.CommandType = System.Data.CommandType.Text
                                cmd.Parameters.Add("@Budget", System.Data.SqlDbType.Int).Value = PlanID
                                obj = cmd.ExecuteScalar()
                                If obj IsNot Nothing AndAlso obj IsNot System.DBNull.Value Then
                                    ClientID = Convert.ToInt32(obj, System.Globalization.CultureInfo.InvariantCulture)
                                    rpt.Parameters("ParameterClient").Value = ClientID
                                End If
                            End Using
                        End Using
                    End If
                End If

                If ClientID >= 0 Then
                    .Text = DebtPlus.Utils.Format.Client.FormatClientID(ClientID)
                End If
            End With
        End Sub

        Private Sub XrLabel_ClientName_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs)
            Dim ClientID As Int32 = -1

            With CType(sender, DevExpress.XtraReports.UI.XRLabel)
                Dim rpt As DevExpress.XtraReports.UI.XtraReport = CType(.Report, DevExpress.XtraReports.UI.XtraReport)
                Dim sqlInfo As DebtPlus.LINQ.SQLInfoClass = DebtPlus.LINQ.SQLInfoClass.getDefault()
                Using cn As System.Data.SqlClient.SqlConnection = New System.Data.SqlClient.SqlConnection(sqlInfo.ConnectionString)
                    cn.Open()
                    Dim obj As Object = rpt.Parameters("ParameterClient").Value
                    If obj Is Nothing OrElse obj Is System.DBNull.Value Then
                        obj = -1
                    End If

                    ClientID = Convert.ToInt32(obj, System.Globalization.CultureInfo.InvariantCulture)
                    If ClientID < 0 Then
                        obj = rpt.Parameters("Budget").Value
                        If obj Is Nothing OrElse obj Is System.DBNull.Value Then
                            obj = -1
                        End If

                        Dim PlanID As Int32 = Convert.ToInt32(obj, System.Globalization.CultureInfo.InvariantCulture)
                        If PlanID > 0 Then
                            Using cmd As New System.Data.SqlClient.SqlCommand()
                                cmd.Connection = cn
                                cmd.CommandText = "SELECT client FROM budgets WHERE budget=@Budget"
                                cmd.CommandType = System.Data.CommandType.Text
                                cmd.Parameters.Add("@Budget", System.Data.SqlDbType.Int).Value = PlanID
                                obj = cmd.ExecuteScalar()
                                If obj IsNot Nothing AndAlso obj IsNot System.DBNull.Value Then
                                    ClientID = Convert.ToInt32(obj, System.Globalization.CultureInfo.InvariantCulture)
                                    rpt.Parameters("ParameterClient").Value = ClientID
                                End If
                            End Using
                        End If
                    End If

                    If ClientID >= 0 Then
                        Dim Name As String = String.Empty
                        Using cmd As New System.Data.SqlClient.SqlCommand()
                            cmd.Connection = cn
                            cmd.CommandText = "SELECT name FROM view_client_address WHERE client=@ClientID"
                            cmd.CommandType = System.Data.CommandType.Text
                            cmd.Parameters.Add("@ClientID", System.Data.SqlDbType.Int).Value = ClientID
                            obj = cmd.ExecuteScalar()
                            If obj IsNot Nothing AndAlso obj IsNot System.DBNull.Value Then
                                Name = Convert.ToString(obj, System.Globalization.CultureInfo.InvariantCulture)
                            End If
                        End Using

                        .Text = Name
                    End If
                End Using
            End With
        End Sub
    End Class
End Namespace
