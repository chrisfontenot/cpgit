﻿#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}
#End Region

Imports DebtPlus.Reports.Template
Imports System.IO
Imports DebtPlus.Utils

Namespace RPPS.Responses
    Public Class RPPSResponsesReport
        Inherits DatedTemplateXtraReportClass

        Public Sub New()
            MyBase.New()
            InitializeComponent()
            AddHandler Me.BeforePrint, AddressOf Report_BeforePrint
        End Sub

        Public Sub New(ByVal DateRange As DateRange)
            MyBase.New(DateRange)
            InitializeComponent()
            AddHandler Me.BeforePrint, AddressOf Report_BeforePrint
        End Sub

        Private Sub InitializeComponent()
            Const ReportName As String = "DebtPlus.Reports.RPPS.Responses.repx"

            ' See if there is a report reference that we can use
            Dim Fname As String = System.IO.Path.Combine(DebtPlus.Configuration.Config.ReportsDirectory, ReportName)
            Dim UseDefault As Boolean = True
            Try
                If (Not DebtPlus.Configuration.Config.LocalReportsOnly) AndAlso System.IO.File.Exists(Fname) Then
                    LoadLayout(Fname)
                    UseDefault = False
                End If
            Catch ex As Exception
            End Try

            ' Load the standard report definition if we need a new item
            If UseDefault Then
                Dim asm As Reflection.Assembly = Reflection.Assembly.GetExecutingAssembly                                           ' changed
                Using ios As Stream = asm.GetManifestResourceStream(String.Format("DebtPlus.Reports.{0}", ReportName))              ' changed
                    If ios IsNot Nothing Then LoadLayout(ios) ' changed
                End Using
            End If

            ' Ensure that the parameters are defaulted to their "missing" status
            ScriptReferences = DebtPlus.Utils.AssemblyTools.GetAllSiblingAssemblies()

            ' Copy the script references to the subreports as well
            For Each Band As DevExpress.XtraReports.UI.Band In Bands
                For Each ctl As DevExpress.XtraReports.UI.XRControl In Band.Controls
                    Dim subReport As DevExpress.XtraReports.UI.XRSubreport = TryCast(ctl, DevExpress.XtraReports.UI.XRSubreport)
                    If subReport IsNot Nothing Then
                        Dim rpt As DevExpress.XtraReports.UI.XtraReport = subReport.ReportSource
                        If rpt IsNot Nothing Then
                            rpt.ScriptReferences = DebtPlus.Utils.AssemblyTools.GetAllSiblingAssemblies()
                        End If
                    End If
                Next
            Next

            Parameter_Batch = -1
        End Sub

        Public Property Parameter_Batch() As System.Int32
            Get
                Return CType(Parameters("rpps_file").Value, Int32)
            End Get
            Set(ByVal value As System.Int32)
                Parameters("rpps_file").Value = value
            End Set
        End Property

        Public Overrides ReadOnly Property ReportTitle() As String
            Get
                Return "RPPS Responses"
            End Get
        End Property

        Private privateSubtitle As String = String.Empty
        Public Overrides ReadOnly Property ReportSubTitle() As String
            Get
                Return privateSubtitle
            End Get
        End Property

        Public Overrides Sub SetReportParameter(ByVal Parameter As String, ByVal Value As Object)
            If Parameter = "RPPS_ResponseFile" Then
                Parameter_Batch = Convert.ToInt32(Value)
            Else
                MyBase.SetReportParameter(Parameter, Value)
            End If
        End Sub

        Public Overrides Function NeedParameters() As Boolean
            Return MyBase.NeedParameters OrElse Parameter_Batch <= 0
        End Function

        Public Overrides Function RequestReportParameters() As System.Windows.Forms.DialogResult
            Dim answer As System.Windows.Forms.DialogResult = DialogResult.OK
            If NeedParameters() Then
                Using frm As New RequestParametersForm()
                    answer = frm.ShowDialog()
                    Parameter_Batch = frm.Parameter_BatchID
                End Using
            End If
            Return answer
        End Function

        Private Sub Report_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs)

            Try
                Using cn As SqlClient.SqlConnection = New System.Data.SqlClient.SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
                    cn.Open()
                    Using cmd As SqlClient.SqlCommand = New System.Data.SqlClient.SqlCommand()
                        cmd.Connection = cn
                        cmd.CommandText = "SELECT [label] FROM rpps_response_files WHERE rpps_response_file = @rpps_response_file"
                        cmd.CommandType = CommandType.Text
                        cmd.Parameters.Add("@rpps_response_file", SqlDbType.Int).Value = Parameter_Batch
                        cmd.CommandTimeout = 0

                        Dim obj As Object = cmd.ExecuteScalar()
                        Dim Label As String = DebtPlus.Utils.Nulls.DStr(obj).Trim()
                        If Label <> String.Empty Then
                            privateSubtitle = String.Format("This report is for response file #{0:f0} '{1}'", Parameter_Batch, Label)
                        Else
                            privateSubtitle = String.Format("This report is for response file #{0:f0}", Parameter_Batch)
                        End If
                    End Using
                End Using

            Catch ex As SqlClient.SqlException
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error reading RPPS Responses")
                e.Cancel = True
            End Try
        End Sub
    End Class
End Namespace