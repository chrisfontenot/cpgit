#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}
#End Region

Namespace RPPS.Responses
    Friend Class Subreport_COA
        Public Sub New()
            MyBase.New()
            InitializeComponent()
            AddHandler XrLabel_NewAccountNumber.BeforePrint, AddressOf XrLabel_NewAccountNumber_BeforePrint
            AddHandler XrLabel_NewBillerID.BeforePrint, AddressOf XrLabel_NewBillerID_BeforePrint
        End Sub

        Private Sub XrLabel_NewAccountNumber_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs)
            With CType(sender, DevExpress.XtraReports.UI.XRLabel)
                Dim rpt As DevExpress.XtraReports.UI.XtraReport = CType(.Report, DevExpress.XtraReports.UI.XtraReport)
                If DebtPlus.Utils.Nulls.DInt(rpt.GetCurrentColumnValue("ChangeAccountNumber")) = 0 Then
                    .Text = String.Empty
                Else
                    .Text = DebtPlus.Utils.Nulls.DStr(rpt.GetCurrentColumnValue("NewAccountNumber"))
                End If
            End With
        End Sub

        Private Sub XrLabel_NewBillerID_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs)
            With CType(sender, DevExpress.XtraReports.UI.XRLabel)
                Dim rpt As DevExpress.XtraReports.UI.XtraReport = CType(.Report, DevExpress.XtraReports.UI.XtraReport)
                If DebtPlus.Utils.Nulls.DInt(rpt.GetCurrentColumnValue("ChangeBillerID")) = 0 Then
                    .Text = String.Empty
                Else
                    .Text = DebtPlus.Utils.Nulls.DStr(rpt.GetCurrentColumnValue("NewBillerID"))
                End If
            End With
        End Sub
    End Class
End Namespace
