Namespace RPPS.Responses
    <Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
    Partial Class Subreport_CDR
        Inherits Subreport_CDP

        'XtraReport overrides dispose to clean up the component list.
        <System.Diagnostics.DebuggerNonUserCode()> _
        Protected Overrides Sub Dispose(ByVal disposing As Boolean)
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
            MyBase.Dispose(disposing)
        End Sub

        'Required by the Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Designer
        'It can be modified using the Designer.  
        'Do not modify it using the code editor.
        <System.Diagnostics.DebuggerStepThrough()> _
        Private Sub InitializeComponent()
            Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Subreport_CDR))
            Me.XrLabel_missing_item = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel10 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_counter_amount = New DevExpress.XtraReports.UI.XRLabel()
            CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
            '
            'Detail
            '
            Me.Detail.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel_counter_amount, Me.XrLabel_missing_item})
            Me.Detail.HeightF = 40.0!
            Me.Detail.Controls.SetChildIndex(Me.XrLabel_missing_item, 0)
            Me.Detail.Controls.SetChildIndex(Me.XrLabel_counter_amount, 0)
            Me.Detail.Controls.SetChildIndex(Me.XrLabel_counselor_name, 0)
            Me.Detail.Controls.SetChildIndex(Me.XrLabel_client, 0)
            Me.Detail.Controls.SetChildIndex(Me.XrLabel_account_number, 0)
            Me.Detail.Controls.SetChildIndex(Me.XrLabel_proposed_start_date, 0)
            Me.Detail.Controls.SetChildIndex(Me.XrLabel_proposed_amount, 0)
            Me.Detail.Controls.SetChildIndex(Me.XrLabel_response_reason, 0)
            '
            'XrLabel1
            '
            Me.XrLabel1.StylePriority.UseFont = False
            Me.XrLabel1.StylePriority.UseForeColor = False
            Me.XrLabel1.Text = "CDR CREDITOR REJECTED PROPOSALS"
            '
            'XrPanel1
            '
            Me.XrPanel1.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel10})
            Me.XrPanel1.StylePriority.UseBackColor = False
            Me.XrPanel1.Controls.SetChildIndex(Me.XrLabel10, 0)
            Me.XrPanel1.Controls.SetChildIndex(Me.XrLabel3, 0)
            Me.XrPanel1.Controls.SetChildIndex(Me.XrLabel4, 0)
            Me.XrPanel1.Controls.SetChildIndex(Me.XrLabel6, 0)
            Me.XrPanel1.Controls.SetChildIndex(Me.XrLabel7, 0)
            Me.XrPanel1.Controls.SetChildIndex(Me.XrLabel8, 0)
            Me.XrPanel1.Controls.SetChildIndex(Me.XrLabel9, 0)
            '
            'XrLabel9
            '
            Me.XrLabel9.StylePriority.UseFont = False
            Me.XrLabel9.StylePriority.UseForeColor = False
            Me.XrLabel9.StylePriority.UseTextAlignment = False
            '
            'XrLabel8
            '
            Me.XrLabel8.StylePriority.UseFont = False
            Me.XrLabel8.StylePriority.UseForeColor = False
            Me.XrLabel8.StylePriority.UseTextAlignment = False
            '
            'XrLabel7
            '
            Me.XrLabel7.StylePriority.UseFont = False
            Me.XrLabel7.StylePriority.UseForeColor = False
            Me.XrLabel7.StylePriority.UseTextAlignment = False
            '
            'XrLabel6
            '
            Me.XrLabel6.StylePriority.UseFont = False
            Me.XrLabel6.StylePriority.UseForeColor = False
            Me.XrLabel6.StylePriority.UseTextAlignment = False
            '
            'XrLabel4
            '
            Me.XrLabel4.LocationFloat = New DevExpress.Utils.PointFloat(533.0!, 0.0!)
            Me.XrLabel4.StylePriority.UseFont = False
            Me.XrLabel4.StylePriority.UseForeColor = False
            Me.XrLabel4.StylePriority.UseTextAlignment = False
            '
            'XrLabel3
            '
            Me.XrLabel3.StylePriority.UseFont = False
            Me.XrLabel3.StylePriority.UseForeColor = False
            Me.XrLabel3.StylePriority.UseTextAlignment = False
            '
            'XrLabel_group_header
            '
            Me.XrLabel_group_header.StylePriority.UseFont = False
            Me.XrLabel_group_header.StylePriority.UseForeColor = False
            '
            'XrLabel_response_reason
            '
            Me.XrLabel_response_reason.LocationFloat = New DevExpress.Utils.PointFloat(533.0!, 0.0!)
            Me.XrLabel_response_reason.SizeF = New System.Drawing.SizeF(150.0!, 15.0!)
            Me.XrLabel_response_reason.StylePriority.UseFont = False
            Me.XrLabel_response_reason.StylePriority.UseForeColor = False
            Me.XrLabel_response_reason.StylePriority.UseTextAlignment = False
            '
            'XrLabel_proposed_amount
            '
            Me.XrLabel_proposed_amount.StylePriority.UseFont = False
            Me.XrLabel_proposed_amount.StylePriority.UseForeColor = False
            Me.XrLabel_proposed_amount.StylePriority.UseTextAlignment = False
            '
            'XrLabel_proposed_start_date
            '
            Me.XrLabel_proposed_start_date.StylePriority.UseFont = False
            Me.XrLabel_proposed_start_date.StylePriority.UseForeColor = False
            Me.XrLabel_proposed_start_date.StylePriority.UseTextAlignment = False
            '
            'XrLabel_account_number
            '
            Me.XrLabel_account_number.StylePriority.UseFont = False
            Me.XrLabel_account_number.StylePriority.UseForeColor = False
            Me.XrLabel_account_number.StylePriority.UseTextAlignment = False
            '
            'XrLabel_client
            '
            Me.XrLabel_client.StylePriority.UseFont = False
            Me.XrLabel_client.StylePriority.UseForeColor = False
            Me.XrLabel_client.StylePriority.UseTextAlignment = False
            '
            'XrLabel_counselor_name
            '
            Me.XrLabel_counselor_name.StylePriority.UseFont = False
            Me.XrLabel_counselor_name.StylePriority.UseForeColor = False
            Me.XrLabel_counselor_name.StylePriority.UseTextAlignment = False
            '
            'XrLabel25
            '
            Me.XrLabel25.StylePriority.UseFont = False
            Me.XrLabel25.StylePriority.UseForeColor = False
            '
            'XrLabel_missing_item
            '
            Me.XrLabel_missing_item.CanShrink = True
            Me.XrLabel_missing_item.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel_missing_item.ForeColor = System.Drawing.Color.Red
            Me.XrLabel_missing_item.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 16.0!)
            Me.XrLabel_missing_item.Multiline = True
            Me.XrLabel_missing_item.Name = "XrLabel_missing_item"
            Me.XrLabel_missing_item.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_missing_item.Scripts.OnBeforePrint = "XrLabel_missing_item_BeforePrint"
            Me.XrLabel_missing_item.SizeF = New System.Drawing.SizeF(800.0!, 23.0!)
            Me.XrLabel_missing_item.StylePriority.UseFont = False
            Me.XrLabel_missing_item.StylePriority.UseForeColor = False
            Me.XrLabel_missing_item.StylePriority.UseTextAlignment = False
            Me.XrLabel_missing_item.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
            '
            'XrLabel10
            '
            Me.XrLabel10.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel10.ForeColor = System.Drawing.Color.White
            Me.XrLabel10.LocationFloat = New DevExpress.Utils.PointFloat(450.0!, 0.0!)
            Me.XrLabel10.Name = "XrLabel10"
            Me.XrLabel10.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
            Me.XrLabel10.SizeF = New System.Drawing.SizeF(75.0!, 15.0!)
            Me.XrLabel10.StylePriority.UseFont = False
            Me.XrLabel10.StylePriority.UseForeColor = False
            Me.XrLabel10.StylePriority.UsePadding = False
            Me.XrLabel10.StylePriority.UseTextAlignment = False
            Me.XrLabel10.Text = "COUNTER"
            Me.XrLabel10.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel_counter_amount
            '
            Me.XrLabel_counter_amount.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "counter_amount", "{0:c}")})
            Me.XrLabel_counter_amount.Font = New System.Drawing.Font("Times New Roman", 9.75!)
            Me.XrLabel_counter_amount.ForeColor = System.Drawing.SystemColors.WindowText
            Me.XrLabel_counter_amount.LocationFloat = New DevExpress.Utils.PointFloat(450.0!, 0.0!)
            Me.XrLabel_counter_amount.Name = "XrLabel_counter_amount"
            Me.XrLabel_counter_amount.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_counter_amount.SizeF = New System.Drawing.SizeF(75.0!, 15.0!)
            Me.XrLabel_counter_amount.StylePriority.UseFont = False
            Me.XrLabel_counter_amount.StylePriority.UseForeColor = False
            Me.XrLabel_counter_amount.StylePriority.UseTextAlignment = False
            Me.XrLabel_counter_amount.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrLabel_counter_amount.WordWrap = False
            '
            'Subreport_CDR
            '
            Me.Bands.AddRange(New DevExpress.XtraReports.UI.Band() {Me.Detail, Me.PageHeader, Me.ReportHeader, Me.GroupHeader1, Me.ReportFooter})
            Me.ScriptsSource = resources.GetString("$this.ScriptsSource")
            Me.Version = "11.2"
            CType(Me, System.ComponentModel.ISupportInitialize).EndInit()

        End Sub
        Protected Friend WithEvents XrLabel_missing_item As DevExpress.XtraReports.UI.XRLabel
        Protected Friend WithEvents XrLabel10 As DevExpress.XtraReports.UI.XRLabel
        Protected Friend WithEvents XrLabel_counter_amount As DevExpress.XtraReports.UI.XRLabel
    End Class
End Namespace
