Namespace RPPS.Responses
    <Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
    Partial Class RPPSResponsesReport
        Inherits DebtPlus.Reports.Template.DatedTemplateXtraReportClass

        'XtraReport overrides dispose to clean up the component list.
        <System.Diagnostics.DebuggerNonUserCode()> _
        Protected Overrides Sub Dispose(ByVal disposing As Boolean)
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
            MyBase.Dispose(disposing)
        End Sub

        'Required by the Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Designer
        'It can be modified using the Designer.  
        'Do not modify it using the code editor.
        <System.Diagnostics.DebuggerStepThrough()> _
        Private Sub InitializeComponent()
            Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(RPPSResponsesReport))
            Me.rpps_file = New DevExpress.XtraReports.Parameters.Parameter()
            Me.XrSubreport_COA = New DevExpress.XtraReports.UI.XRSubreport()
            Me.Subreport_COA1 = New DebtPlus.Reports.RPPS.Responses.Subreport_COA()
            Me.XrSubreport_CDP = New DevExpress.XtraReports.UI.XRSubreport()
            Me.Subreport_CDP1 = New DebtPlus.Reports.RPPS.Responses.Subreport_CDP()
            Me.XrSubreport_DFS = New DevExpress.XtraReports.UI.XRSubreport()
            Me.Subreport_DFS1 = New DebtPlus.Reports.RPPS.Responses.Subreport_DFS()
            Me.XrSubreport_CDC = New DevExpress.XtraReports.UI.XRSubreport()
            Me.Subreport_CDC1 = New DebtPlus.Reports.RPPS.Responses.Subreport_CDC()
            Me.XrSubreport_CDM = New DevExpress.XtraReports.UI.XRSubreport()
            Me.Subreport_CDM1 = New DebtPlus.Reports.RPPS.Responses.Subreport_CDM()
            Me.XrSubreport_CDV = New DevExpress.XtraReports.UI.XRSubreport()
            Me.Subreport_CDV1 = New DebtPlus.Reports.RPPS.Responses.Subreport_CDV()
            Me.XrSubreport_CDT = New DevExpress.XtraReports.UI.XRSubreport()
            Me.Subreport_CDT1 = New DebtPlus.Reports.RPPS.Responses.Subreport_CDT()
            Me.XrSubreport_CDR = New DevExpress.XtraReports.UI.XRSubreport()
            Me.Subreport_CDR1 = New DebtPlus.Reports.RPPS.Responses.Subreport_CDR()
            Me.XrSubreport_CDA = New DevExpress.XtraReports.UI.XRSubreport()
            Me.Subreport_CDA1 = New DebtPlus.Reports.RPPS.Responses.Subreport_CDA()
            Me.XrSubreport_CIE = New DevExpress.XtraReports.UI.XRSubreport()
            Me.Subreport_CIE1 = New DebtPlus.Reports.RPPS.Responses.Subreport_CIE()
            CType(Me.Subreport_COA1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.Subreport_CDP1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.Subreport_DFS1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.Subreport_CDC1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.Subreport_CDM1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.Subreport_CDV1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.Subreport_CDT1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.Subreport_CDR1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.Subreport_CDA1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.Subreport_CIE1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
            '
            'PageHeader
            '
            Me.PageHeader.HeightF = 130.0!
            '
            'XrLabel_Title
            '
            Me.XrLabel_Title.StylePriority.UseFont = False
            '
            'XrPageInfo_PageNumber
            '
            Me.XrPageInfo_PageNumber.StylePriority.UseTextAlignment = False
            '
            'XRLabel_Agency_Name
            '
            Me.XRLabel_Agency_Name.StylePriority.UseTextAlignment = False
            '
            'XrLabel_Agency_Address3
            '
            Me.XrLabel_Agency_Address3.StylePriority.UseFont = False
            Me.XrLabel_Agency_Address3.StylePriority.UseTextAlignment = False
            '
            'XrLabel_Agency_Address1
            '
            Me.XrLabel_Agency_Address1.StylePriority.UseFont = False
            Me.XrLabel_Agency_Address1.StylePriority.UseTextAlignment = False
            '
            'XrLabel_Agency_Phone
            '
            Me.XrLabel_Agency_Phone.StylePriority.UseFont = False
            Me.XrLabel_Agency_Phone.StylePriority.UseTextAlignment = False
            '
            'XrLabel_Agency_Address2
            '
            Me.XrLabel_Agency_Address2.StylePriority.UseFont = False
            Me.XrLabel_Agency_Address2.StylePriority.UseTextAlignment = False
            '
            'Detail
            '
            Me.Detail.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrSubreport_COA, Me.XrSubreport_CDP, Me.XrSubreport_DFS, Me.XrSubreport_CDC, Me.XrSubreport_CDM, Me.XrSubreport_CDV, Me.XrSubreport_CDT, Me.XrSubreport_CDR, Me.XrSubreport_CDA, Me.XrSubreport_CIE})
            Me.Detail.HeightF = 248.0!
            '
            'rpps_file
            '
            Me.rpps_file.Description = "RPPS File ID number"
            Me.rpps_file.Name = "rpps_file"
            Me.rpps_file.Type = GetType(Integer)
            Me.rpps_file.Value = 0
            '
            'XrSubreport_COA
            '
            Me.XrSubreport_COA.CanShrink = True
            Me.XrSubreport_COA.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 225.0!)
            Me.XrSubreport_COA.Name = "XrSubreport_COA"
            Me.XrSubreport_COA.ReportSource = Me.Subreport_COA1
            Me.XrSubreport_COA.Scripts.OnBeforePrint = "XrSubreport_COA_BeforePrint"
            Me.XrSubreport_COA.SizeF = New System.Drawing.SizeF(800.0!, 23.0!)
            '
            'XrSubreport_CDP
            '
            Me.XrSubreport_CDP.CanShrink = True
            Me.XrSubreport_CDP.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 75.0!)
            Me.XrSubreport_CDP.Name = "XrSubreport_CDP"
            Me.XrSubreport_CDP.ReportSource = Me.Subreport_CDP1
            Me.XrSubreport_CDP.Scripts.OnBeforePrint = "XrSubreport_CDP_BeforePrint"
            Me.XrSubreport_CDP.SizeF = New System.Drawing.SizeF(800.0!, 25.0!)
            '
            'XrSubreport_DFS
            '
            Me.XrSubreport_DFS.CanShrink = True
            Me.XrSubreport_DFS.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 200.0!)
            Me.XrSubreport_DFS.Name = "XrSubreport_DFS"
            Me.XrSubreport_DFS.ReportSource = Me.Subreport_DFS1
            Me.XrSubreport_DFS.Scripts.OnBeforePrint = "XrSubreport_DFS_BeforePrint"
            Me.XrSubreport_DFS.SizeF = New System.Drawing.SizeF(800.0!, 25.0!)
            '
            'XrSubreport_CDC
            '
            Me.XrSubreport_CDC.CanShrink = True
            Me.XrSubreport_CDC.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 175.0!)
            Me.XrSubreport_CDC.Name = "XrSubreport_CDC"
            Me.XrSubreport_CDC.ReportSource = Me.Subreport_CDC1
            Me.XrSubreport_CDC.Scripts.OnBeforePrint = "XrSubreport_CDC_BeforePrint"
            Me.XrSubreport_CDC.SizeF = New System.Drawing.SizeF(800.0!, 25.0!)
            '
            'XrSubreport_CDM
            '
            Me.XrSubreport_CDM.CanShrink = True
            Me.XrSubreport_CDM.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 150.0!)
            Me.XrSubreport_CDM.Name = "XrSubreport_CDM"
            Me.XrSubreport_CDM.ReportSource = Me.Subreport_CDM1
            Me.XrSubreport_CDM.Scripts.OnBeforePrint = "XrSubreport_CDM_BeforePrint"
            Me.XrSubreport_CDM.SizeF = New System.Drawing.SizeF(800.0!, 25.0!)
            '
            'XrSubreport_CDV
            '
            Me.XrSubreport_CDV.CanShrink = True
            Me.XrSubreport_CDV.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 125.0!)
            Me.XrSubreport_CDV.Name = "XrSubreport_CDV"
            Me.XrSubreport_CDV.ReportSource = Me.Subreport_CDV1
            Me.XrSubreport_CDV.Scripts.OnBeforePrint = "XrSubreport_CDV_BeforePrint"
            Me.XrSubreport_CDV.SizeF = New System.Drawing.SizeF(800.0!, 25.0!)
            '
            'XrSubreport_CDT
            '
            Me.XrSubreport_CDT.CanShrink = True
            Me.XrSubreport_CDT.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 100.0!)
            Me.XrSubreport_CDT.Name = "XrSubreport_CDT"
            Me.XrSubreport_CDT.ReportSource = Me.Subreport_CDT1
            Me.XrSubreport_CDT.Scripts.OnBeforePrint = "XrSubreport_CDT_BeforePrint"
            Me.XrSubreport_CDT.SizeF = New System.Drawing.SizeF(800.0!, 25.0!)
            '
            'XrSubreport_CDR
            '
            Me.XrSubreport_CDR.CanShrink = True
            Me.XrSubreport_CDR.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 50.0!)
            Me.XrSubreport_CDR.Name = "XrSubreport_CDR"
            Me.XrSubreport_CDR.ReportSource = Me.Subreport_CDR1
            Me.XrSubreport_CDR.Scripts.OnBeforePrint = "XrSubreport_CDR_BeforePrint"
            Me.XrSubreport_CDR.SizeF = New System.Drawing.SizeF(800.0!, 25.0!)
            '
            'XrSubreport_CDA
            '
            Me.XrSubreport_CDA.CanShrink = True
            Me.XrSubreport_CDA.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 25.0!)
            Me.XrSubreport_CDA.Name = "XrSubreport_CDA"
            Me.XrSubreport_CDA.ReportSource = Me.Subreport_CDA1
            Me.XrSubreport_CDA.Scripts.OnBeforePrint = "XrSubreport_CDA_BeforePrint"
            Me.XrSubreport_CDA.SizeF = New System.Drawing.SizeF(800.0!, 25.0!)
            '
            'XrSubreport_CIE
            '
            Me.XrSubreport_CIE.CanShrink = True
            Me.XrSubreport_CIE.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
            Me.XrSubreport_CIE.Name = "XrSubreport_CIE"
            Me.XrSubreport_CIE.ReportSource = Me.Subreport_CIE1
            Me.XrSubreport_CIE.Scripts.OnBeforePrint = "XrSubreport_CIE_BeforePrint"
            Me.XrSubreport_CIE.SizeF = New System.Drawing.SizeF(800.0!, 25.0!)
            '
            'RPPSResponsesReport
            '
            Me.Bands.AddRange(New DevExpress.XtraReports.UI.Band() {Me.TopMarginBand1, Me.Detail, Me.BottomMarginBand1, Me.PageHeader, Me.PageFooter})
            Me.Parameters.AddRange(New DevExpress.XtraReports.Parameters.Parameter() {Me.rpps_file})
            Me.ScriptLanguage = DevExpress.XtraReports.ScriptLanguage.VisualBasic
            Me.ScriptReferencesString = resources.GetString("$this.ScriptReferencesString")
            Me.Scripts.OnBeforePrint = "RPPSResponsesReport_BeforePrint"
            Me.ScriptsSource = resources.GetString("$this.ScriptsSource")
            Me.StyleSheet.AddRange(New DevExpress.XtraReports.UI.XRControlStyle() {Me.XrControlStyle_Header, Me.XrControlStyle_Totals, Me.XrControlStyle_HeaderPannel, Me.XrControlStyle_GroupHeader})
            Me.Version = "11.2"
            CType(Me.Subreport_COA1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.Subreport_CDP1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.Subreport_DFS1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.Subreport_CDC1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.Subreport_CDM1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.Subreport_CDV1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.Subreport_CDT1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.Subreport_CDR1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.Subreport_CDA1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.Subreport_CIE1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me, System.ComponentModel.ISupportInitialize).EndInit()

        End Sub
        Friend WithEvents rpps_file As DevExpress.XtraReports.Parameters.Parameter
        Friend WithEvents XrSubreport_CIE As DevExpress.XtraReports.UI.XRSubreport
        Private WithEvents Subreport_CIE1 As DebtPlus.Reports.RPPS.Responses.Subreport_CIE
        Friend WithEvents XrSubreport_CDA As DevExpress.XtraReports.UI.XRSubreport
        Private WithEvents Subreport_CDA1 As DebtPlus.Reports.RPPS.Responses.Subreport_CDA
        Friend WithEvents XrSubreport_CDR As DevExpress.XtraReports.UI.XRSubreport
        Private WithEvents Subreport_CDR1 As DebtPlus.Reports.RPPS.Responses.Subreport_CDR
        Friend WithEvents XrSubreport_CDT As DevExpress.XtraReports.UI.XRSubreport
        Private WithEvents Subreport_CDT1 As DebtPlus.Reports.RPPS.Responses.Subreport_CDT
        Friend WithEvents XrSubreport_CDV As DevExpress.XtraReports.UI.XRSubreport
        Private WithEvents Subreport_CDV1 As DebtPlus.Reports.RPPS.Responses.Subreport_CDV
        Friend WithEvents XrSubreport_CDM As DevExpress.XtraReports.UI.XRSubreport
        Private WithEvents Subreport_CDM1 As DebtPlus.Reports.RPPS.Responses.Subreport_CDM
        Friend WithEvents XrSubreport_CDC As DevExpress.XtraReports.UI.XRSubreport
        Private WithEvents Subreport_CDC1 As DebtPlus.Reports.RPPS.Responses.Subreport_CDC
        Friend WithEvents XrSubreport_DFS As DevExpress.XtraReports.UI.XRSubreport
        Private WithEvents Subreport_DFS1 As DebtPlus.Reports.RPPS.Responses.Subreport_DFS
        Friend WithEvents XrSubreport_CDP As DevExpress.XtraReports.UI.XRSubreport
        Private WithEvents Subreport_CDP1 As DebtPlus.Reports.RPPS.Responses.Subreport_CDP
        Friend WithEvents XrSubreport_COA As DevExpress.XtraReports.UI.XRSubreport
        Private WithEvents Subreport_COA1 As DebtPlus.Reports.RPPS.Responses.Subreport_COA
    End Class
End Namespace
