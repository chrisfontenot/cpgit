Namespace RPPS.Responses
    <Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
    Partial Class Subreport_CDV
        Inherits DevExpress.XtraReports.UI.XtraReport

        'XtraReport overrides dispose to clean up the component list.
        <System.Diagnostics.DebuggerNonUserCode()> _
        Protected Overrides Sub Dispose(ByVal disposing As Boolean)
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
            MyBase.Dispose(disposing)
        End Sub

        'Required by the Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Designer
        'It can be modified using the Designer.  
        'Do not modify it using the code editor.
        <System.Diagnostics.DebuggerStepThrough()> _
        Private Sub InitializeComponent()
            Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Subreport_CDV))
            Me.Detail = New DevExpress.XtraReports.UI.DetailBand()
            Me.XrLabel_old_balance = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_client_name = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_processing_error = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_account_balance = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_account_number = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_client = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_counselor_name = New DevExpress.XtraReports.UI.XRLabel()
            Me.PageHeader = New DevExpress.XtraReports.UI.PageHeaderBand()
            Me.XrPanel1 = New DevExpress.XtraReports.UI.XRPanel()
            Me.XrLabel6 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel2 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel9 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel8 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel7 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel4 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel3 = New DevExpress.XtraReports.UI.XRLabel()
            Me.ReportHeader = New DevExpress.XtraReports.UI.ReportHeaderBand()
            Me.XrLabel1 = New DevExpress.XtraReports.UI.XRLabel()
            Me.GroupHeader1 = New DevExpress.XtraReports.UI.GroupHeaderBand()
            Me.XrLabel_group_header = New DevExpress.XtraReports.UI.XRLabel()
            Me.ReportFooter = New DevExpress.XtraReports.UI.ReportFooterBand()
            Me.XrLabel5 = New DevExpress.XtraReports.UI.XRLabel()
            Me.TopMarginBand1 = New DevExpress.XtraReports.UI.TopMarginBand()
            Me.BottomMarginBand1 = New DevExpress.XtraReports.UI.BottomMarginBand()
            Me.XrControlStyle_Header = New DevExpress.XtraReports.UI.XRControlStyle()
            Me.XrControlStyle_Totals = New DevExpress.XtraReports.UI.XRControlStyle()
            Me.XrControlStyle_HeaderPannel = New DevExpress.XtraReports.UI.XRControlStyle()
            Me.XrControlStyle_GroupHeader = New DevExpress.XtraReports.UI.XRControlStyle()
            CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
            '
            'Detail
            '
            Me.Detail.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel_old_balance, Me.XrLabel_client_name, Me.XrLabel_processing_error, Me.XrLabel_account_balance, Me.XrLabel_account_number, Me.XrLabel_client, Me.XrLabel_counselor_name})
            Me.Detail.HeightF = 15.0!
            Me.Detail.Name = "Detail"
            Me.Detail.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
            Me.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
            '
            'XrLabel_old_balance
            '
            Me.XrLabel_old_balance.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "old_balance", "{0:c}")})
            Me.XrLabel_old_balance.Font = New System.Drawing.Font("Times New Roman", 9.75!)
            Me.XrLabel_old_balance.ForeColor = System.Drawing.SystemColors.WindowText
            Me.XrLabel_old_balance.LocationFloat = New DevExpress.Utils.PointFloat(378.0!, 0.0!)
            Me.XrLabel_old_balance.Name = "XrLabel_old_balance"
            Me.XrLabel_old_balance.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_old_balance.SizeF = New System.Drawing.SizeF(69.33331!, 15.0!)
            Me.XrLabel_old_balance.StylePriority.UseFont = False
            Me.XrLabel_old_balance.StylePriority.UseForeColor = False
            Me.XrLabel_old_balance.StylePriority.UseTextAlignment = False
            Me.XrLabel_old_balance.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrLabel_old_balance.WordWrap = False
            '
            'XrLabel_client_name
            '
            Me.XrLabel_client_name.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "client_name")})
            Me.XrLabel_client_name.Font = New System.Drawing.Font("Times New Roman", 9.75!)
            Me.XrLabel_client_name.ForeColor = System.Drawing.SystemColors.WindowText
            Me.XrLabel_client_name.LocationFloat = New DevExpress.Utils.PointFloat(75.0!, 0.0!)
            Me.XrLabel_client_name.Name = "XrLabel_client_name"
            Me.XrLabel_client_name.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_client_name.SizeF = New System.Drawing.SizeF(131.625!, 15.0!)
            Me.XrLabel_client_name.StylePriority.UseFont = False
            Me.XrLabel_client_name.StylePriority.UseForeColor = False
            Me.XrLabel_client_name.StylePriority.UseTextAlignment = False
            Me.XrLabel_client_name.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            Me.XrLabel_client_name.WordWrap = False
            '
            'XrLabel_processing_error
            '
            Me.XrLabel_processing_error.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "processing_error")})
            Me.XrLabel_processing_error.Font = New System.Drawing.Font("Times New Roman", 9.75!)
            Me.XrLabel_processing_error.ForeColor = System.Drawing.SystemColors.WindowText
            Me.XrLabel_processing_error.LocationFloat = New DevExpress.Utils.PointFloat(525.0!, 0.0!)
            Me.XrLabel_processing_error.Name = "XrLabel_processing_error"
            Me.XrLabel_processing_error.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_processing_error.SizeF = New System.Drawing.SizeF(166.0!, 15.0!)
            Me.XrLabel_processing_error.StylePriority.UseFont = False
            Me.XrLabel_processing_error.StylePriority.UseForeColor = False
            Me.XrLabel_processing_error.StylePriority.UseTextAlignment = False
            Me.XrLabel_processing_error.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            Me.XrLabel_processing_error.WordWrap = False
            '
            'XrLabel_account_balance
            '
            Me.XrLabel_account_balance.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "account_balance", "{0:c}")})
            Me.XrLabel_account_balance.Font = New System.Drawing.Font("Times New Roman", 9.75!)
            Me.XrLabel_account_balance.ForeColor = System.Drawing.SystemColors.WindowText
            Me.XrLabel_account_balance.LocationFloat = New DevExpress.Utils.PointFloat(447.6667!, 0.0!)
            Me.XrLabel_account_balance.Name = "XrLabel_account_balance"
            Me.XrLabel_account_balance.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_account_balance.SizeF = New System.Drawing.SizeF(69.33331!, 15.0!)
            Me.XrLabel_account_balance.StylePriority.UseFont = False
            Me.XrLabel_account_balance.StylePriority.UseForeColor = False
            Me.XrLabel_account_balance.StylePriority.UseTextAlignment = False
            Me.XrLabel_account_balance.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrLabel_account_balance.WordWrap = False
            '
            'XrLabel_account_number
            '
            Me.XrLabel_account_number.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "account_number")})
            Me.XrLabel_account_number.Font = New System.Drawing.Font("Times New Roman", 9.75!)
            Me.XrLabel_account_number.ForeColor = System.Drawing.SystemColors.WindowText
            Me.XrLabel_account_number.LocationFloat = New DevExpress.Utils.PointFloat(206.625!, 0.0!)
            Me.XrLabel_account_number.Name = "XrLabel_account_number"
            Me.XrLabel_account_number.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_account_number.SizeF = New System.Drawing.SizeF(163.375!, 15.0!)
            Me.XrLabel_account_number.StylePriority.UseFont = False
            Me.XrLabel_account_number.StylePriority.UseForeColor = False
            Me.XrLabel_account_number.StylePriority.UseTextAlignment = False
            Me.XrLabel_account_number.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            Me.XrLabel_account_number.WordWrap = False
            '
            'XrLabel_client
            '
            Me.XrLabel_client.Font = New System.Drawing.Font("Times New Roman", 9.75!)
            Me.XrLabel_client.ForeColor = System.Drawing.SystemColors.WindowText
            Me.XrLabel_client.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
            Me.XrLabel_client.Name = "XrLabel_client"
            Me.XrLabel_client.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_client.SizeF = New System.Drawing.SizeF(75.0!, 15.0!)
            Me.XrLabel_client.StylePriority.UseFont = False
            Me.XrLabel_client.StylePriority.UseForeColor = False
            Me.XrLabel_client.StylePriority.UseTextAlignment = False
            Me.XrLabel_client.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrLabel_client.WordWrap = False
            '
            'XrLabel_counselor_name
            '
            Me.XrLabel_counselor_name.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "counselor_name")})
            Me.XrLabel_counselor_name.Font = New System.Drawing.Font("Times New Roman", 9.75!)
            Me.XrLabel_counselor_name.ForeColor = System.Drawing.SystemColors.WindowText
            Me.XrLabel_counselor_name.LocationFloat = New DevExpress.Utils.PointFloat(691.0!, 0.0!)
            Me.XrLabel_counselor_name.Name = "XrLabel_counselor_name"
            Me.XrLabel_counselor_name.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_counselor_name.SizeF = New System.Drawing.SizeF(101.0!, 15.0!)
            Me.XrLabel_counselor_name.StylePriority.UseFont = False
            Me.XrLabel_counselor_name.StylePriority.UseForeColor = False
            Me.XrLabel_counselor_name.StylePriority.UseTextAlignment = False
            Me.XrLabel_counselor_name.Text = " "
            Me.XrLabel_counselor_name.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            Me.XrLabel_counselor_name.WordWrap = False
            '
            'PageHeader
            '
            Me.PageHeader.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrPanel1})
            Me.PageHeader.HeightF = 26.0!
            Me.PageHeader.Name = "PageHeader"
            Me.PageHeader.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
            Me.PageHeader.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
            '
            'XrPanel1
            '
            Me.XrPanel1.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel6, Me.XrLabel2, Me.XrLabel9, Me.XrLabel8, Me.XrLabel7, Me.XrLabel4, Me.XrLabel3})
            Me.XrPanel1.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
            Me.XrPanel1.Name = "XrPanel1"
            Me.XrPanel1.SizeF = New System.Drawing.SizeF(800.0!, 18.0!)
            Me.XrPanel1.StyleName = "XrControlStyle_HeaderPannel"
            '
            'XrLabel6
            '
            Me.XrLabel6.LocationFloat = New DevExpress.Utils.PointFloat(370.0!, 1.0!)
            Me.XrLabel6.Name = "XrLabel6"
            Me.XrLabel6.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel6.SizeF = New System.Drawing.SizeF(77.33331!, 15.0!)
            Me.XrLabel6.StylePriority.UsePadding = False
            Me.XrLabel6.StylePriority.UseTextAlignment = False
            Me.XrLabel6.Text = "OLD BAL"
            Me.XrLabel6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel2
            '
            Me.XrLabel2.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 1.0!)
            Me.XrLabel2.Name = "XrLabel2"
            Me.XrLabel2.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel2.SizeF = New System.Drawing.SizeF(75.0!, 15.0!)
            Me.XrLabel2.StylePriority.UsePadding = False
            Me.XrLabel2.StylePriority.UseTextAlignment = False
            Me.XrLabel2.Text = "CLIENT"
            Me.XrLabel2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel9
            '
            Me.XrLabel9.LocationFloat = New DevExpress.Utils.PointFloat(75.0!, 0.9999911!)
            Me.XrLabel9.Name = "XrLabel9"
            Me.XrLabel9.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel9.SizeF = New System.Drawing.SizeF(131.625!, 15.0!)
            Me.XrLabel9.StylePriority.UsePadding = False
            Me.XrLabel9.StylePriority.UseTextAlignment = False
            Me.XrLabel9.Text = "NAME"
            Me.XrLabel9.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            '
            'XrLabel8
            '
            Me.XrLabel8.LocationFloat = New DevExpress.Utils.PointFloat(206.625!, 0.9999911!)
            Me.XrLabel8.Name = "XrLabel8"
            Me.XrLabel8.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel8.SizeF = New System.Drawing.SizeF(163.375!, 15.0!)
            Me.XrLabel8.StylePriority.UsePadding = False
            Me.XrLabel8.StylePriority.UseTextAlignment = False
            Me.XrLabel8.Text = "ACCOUNT"
            Me.XrLabel8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            '
            'XrLabel7
            '
            Me.XrLabel7.LocationFloat = New DevExpress.Utils.PointFloat(447.6667!, 0.9999911!)
            Me.XrLabel7.Name = "XrLabel7"
            Me.XrLabel7.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel7.SizeF = New System.Drawing.SizeF(77.33331!, 15.0!)
            Me.XrLabel7.StylePriority.UsePadding = False
            Me.XrLabel7.StylePriority.UseTextAlignment = False
            Me.XrLabel7.Text = "NEW BAL"
            Me.XrLabel7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel4
            '
            Me.XrLabel4.LocationFloat = New DevExpress.Utils.PointFloat(525.0!, 1.0!)
            Me.XrLabel4.Name = "XrLabel4"
            Me.XrLabel4.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel4.SizeF = New System.Drawing.SizeF(166.0!, 15.0!)
            Me.XrLabel4.StylePriority.UsePadding = False
            Me.XrLabel4.StylePriority.UseTextAlignment = False
            Me.XrLabel4.Text = "ERROR"
            Me.XrLabel4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            '
            'XrLabel3
            '
            Me.XrLabel3.LocationFloat = New DevExpress.Utils.PointFloat(691.0!, 1.0!)
            Me.XrLabel3.Name = "XrLabel3"
            Me.XrLabel3.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel3.SizeF = New System.Drawing.SizeF(91.0!, 15.0!)
            Me.XrLabel3.StylePriority.UsePadding = False
            Me.XrLabel3.StylePriority.UseTextAlignment = False
            Me.XrLabel3.Text = "COUNSELOR"
            Me.XrLabel3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            '
            'ReportHeader
            '
            Me.ReportHeader.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel1})
            Me.ReportHeader.HeightF = 30.0!
            Me.ReportHeader.Name = "ReportHeader"
            '
            'XrLabel1
            '
            Me.XrLabel1.Font = New System.Drawing.Font("Times New Roman", 14.0!, System.Drawing.FontStyle.Bold)
            Me.XrLabel1.ForeColor = System.Drawing.Color.Teal
            Me.XrLabel1.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
            Me.XrLabel1.Name = "XrLabel1"
            Me.XrLabel1.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel1.SizeF = New System.Drawing.SizeF(800.0!, 25.0!)
            Me.XrLabel1.StylePriority.UseFont = False
            Me.XrLabel1.StylePriority.UseForeColor = False
            Me.XrLabel1.Text = "CDV BALANCE VERIFICATIONS"
            '
            'GroupHeader1
            '
            Me.GroupHeader1.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel_group_header})
            Me.GroupHeader1.GroupFields.AddRange(New DevExpress.XtraReports.UI.GroupField() {New DevExpress.XtraReports.UI.GroupField("creditor", DevExpress.XtraReports.UI.XRColumnSortOrder.Ascending)})
            Me.GroupHeader1.HeightF = 42.0!
            Me.GroupHeader1.Name = "GroupHeader1"
            '
            'XrLabel_group_header
            '
            Me.XrLabel_group_header.Font = New System.Drawing.Font("Times New Roman", 12.0!, System.Drawing.FontStyle.Bold)
            Me.XrLabel_group_header.ForeColor = System.Drawing.Color.Maroon
            Me.XrLabel_group_header.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 8.0!)
            Me.XrLabel_group_header.Name = "XrLabel_group_header"
            Me.XrLabel_group_header.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_group_header.SizeF = New System.Drawing.SizeF(800.0!, 25.0!)
            Me.XrLabel_group_header.StylePriority.UseFont = False
            Me.XrLabel_group_header.StylePriority.UseForeColor = False
            Me.XrLabel_group_header.Text = "[creditor] [creditor_name]"
            '
            'ReportFooter
            '
            Me.ReportFooter.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel5})
            Me.ReportFooter.HeightF = 25.0!
            Me.ReportFooter.Name = "ReportFooter"
            Me.ReportFooter.PageBreak = DevExpress.XtraReports.UI.PageBreak.AfterBand
            '
            'XrLabel5
            '
            Me.XrLabel5.ForeColor = System.Drawing.Color.Transparent
            Me.XrLabel5.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
            Me.XrLabel5.Name = "XrLabel5"
            Me.XrLabel5.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel5.SizeF = New System.Drawing.SizeF(100.0!, 25.0!)
            Me.XrLabel5.StylePriority.UseForeColor = False
            Me.XrLabel5.Text = " "
            '
            'TopMarginBand1
            '
            Me.TopMarginBand1.HeightF = 25.0!
            Me.TopMarginBand1.Name = "TopMarginBand1"
            '
            'BottomMarginBand1
            '
            Me.BottomMarginBand1.HeightF = 25.0!
            Me.BottomMarginBand1.Name = "BottomMarginBand1"
            '
            'XrControlStyle_Header
            '
            Me.XrControlStyle_Header.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.XrControlStyle_Header.ForeColor = System.Drawing.Color.Teal
            Me.XrControlStyle_Header.Name = "XrControlStyle_Header"
            Me.XrControlStyle_Header.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
            Me.XrControlStyle_Header.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            '
            'XrControlStyle_Totals
            '
            Me.XrControlStyle_Totals.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.XrControlStyle_Totals.Name = "XrControlStyle_Totals"
            Me.XrControlStyle_Totals.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
            Me.XrControlStyle_Totals.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrControlStyle_HeaderPannel
            '
            Me.XrControlStyle_HeaderPannel.BackColor = System.Drawing.Color.Teal
            Me.XrControlStyle_HeaderPannel.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.XrControlStyle_HeaderPannel.ForeColor = System.Drawing.Color.White
            Me.XrControlStyle_HeaderPannel.Name = "XrControlStyle_HeaderPannel"
            Me.XrControlStyle_HeaderPannel.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
            Me.XrControlStyle_HeaderPannel.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrControlStyle_GroupHeader
            '
            Me.XrControlStyle_GroupHeader.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.XrControlStyle_GroupHeader.ForeColor = System.Drawing.Color.Maroon
            Me.XrControlStyle_GroupHeader.Name = "XrControlStyle_GroupHeader"
            Me.XrControlStyle_GroupHeader.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            '
            Me.XrLabel_client.DataBindings.Add("Text", Nothing, "client", "{0:0000000}")
            '
            'Subreport_CDV
            '
            Me.Bands.AddRange(New DevExpress.XtraReports.UI.Band() {Me.Detail, Me.PageHeader, Me.ReportHeader, Me.GroupHeader1, Me.ReportFooter, Me.TopMarginBand1, Me.BottomMarginBand1})
            Me.Margins = New System.Drawing.Printing.Margins(25, 25, 25, 25)
            Me.ScriptLanguage = DevExpress.XtraReports.ScriptLanguage.VisualBasic
            Me.ScriptReferencesString = resources.GetString("$this.ScriptReferencesString")
            Me.StyleSheet.AddRange(New DevExpress.XtraReports.UI.XRControlStyle() {Me.XrControlStyle_Header, Me.XrControlStyle_Totals, Me.XrControlStyle_HeaderPannel, Me.XrControlStyle_GroupHeader})
            Me.Version = "11.2"
            CType(Me, System.ComponentModel.ISupportInitialize).EndInit()

        End Sub
        Friend WithEvents Detail As DevExpress.XtraReports.UI.DetailBand
        Friend WithEvents PageHeader As DevExpress.XtraReports.UI.PageHeaderBand
        Friend WithEvents ReportHeader As DevExpress.XtraReports.UI.ReportHeaderBand
        Friend WithEvents GroupHeader1 As DevExpress.XtraReports.UI.GroupHeaderBand
        Friend WithEvents XrLabel1 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrPanel1 As DevExpress.XtraReports.UI.XRPanel
        Friend WithEvents XrLabel9 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel8 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel7 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel4 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel3 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_group_header As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_processing_error As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_account_balance As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_account_number As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_client As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_counselor_name As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_client_name As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel2 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents ReportFooter As DevExpress.XtraReports.UI.ReportFooterBand
        Friend WithEvents XrLabel5 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents TopMarginBand1 As DevExpress.XtraReports.UI.TopMarginBand
        Friend WithEvents BottomMarginBand1 As DevExpress.XtraReports.UI.BottomMarginBand
        Friend WithEvents XrLabel_old_balance As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel6 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrControlStyle_Header As DevExpress.XtraReports.UI.XRControlStyle
        Friend WithEvents XrControlStyle_Totals As DevExpress.XtraReports.UI.XRControlStyle
        Friend WithEvents XrControlStyle_HeaderPannel As DevExpress.XtraReports.UI.XRControlStyle
        Friend WithEvents XrControlStyle_GroupHeader As DevExpress.XtraReports.UI.XRControlStyle
    End Class
End Namespace
