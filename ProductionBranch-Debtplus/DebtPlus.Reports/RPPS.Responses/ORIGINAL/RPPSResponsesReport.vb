#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}
#End Region
Imports System.ComponentModel

Namespace RPPS.Responses
    Public Class RPPSResponsesReport

        ''' <summary>
        ''' Create an instance of our report
        ''' </summary>
        Public Sub New()
            MyClass.New(DebtPlus.Utils.DateRange.Today)
        End Sub

        Public Sub New(ByVal DateRange As DebtPlus.Utils.DateRange)
            MyBase.New(DateRange)
            InitializeComponent()
            AddHandler BeforePrint, AddressOf RPPSResponsesReport_BeforePrint
            AddHandler XrSubreport_CDA.BeforePrint, AddressOf XrSubreport_CDA_BeforePrint
            AddHandler XrSubreport_CDC.BeforePrint, AddressOf XrSubreport_CDC_BeforePrint
            AddHandler XrSubreport_CDM.BeforePrint, AddressOf XrSubreport_CDM_BeforePrint
            AddHandler XrSubreport_CDP.BeforePrint, AddressOf XrSubreport_CDP_BeforePrint
            AddHandler XrSubreport_CDR.BeforePrint, AddressOf XrSubreport_CDR_BeforePrint
            AddHandler XrSubreport_CDT.BeforePrint, AddressOf XrSubreport_CDT_BeforePrint
            AddHandler XrSubreport_CDV.BeforePrint, AddressOf XrSubreport_CDV_BeforePrint
            AddHandler XrSubreport_CIE.BeforePrint, AddressOf XrSubreport_CIE_BeforePrint
            AddHandler XrSubreport_COA.BeforePrint, AddressOf XrSubreport_COA_BeforePrint
            AddHandler XrSubreport_DFS.BeforePrint, AddressOf XrSubreport_DFS_BeforePrint

            Parameter_Batch = -1
        End Sub

        Public Property Parameter_Batch() As System.Int32
            Get
                Dim parm As DevExpress.XtraReports.Parameters.Parameter = Me.Parameters("rpps_file")
                If parm IsNot Nothing Then
                    Return Convert.ToInt32(parm.Value)
                End If
                Return -1
            End Get
            Set(ByVal value As System.Int32)
                SetParameter("rpps_file", GetType(Int32), value, "Batch ID", False)
            End Set
        End Property

        Public Overrides ReadOnly Property ReportTitle() As String
            Get
                Return "RPPS Responses"
            End Get
        End Property

        Private privateSubtitle As String = String.Empty
        Public Overrides ReadOnly Property ReportSubTitle() As String
            Get
                Return privateSubtitle
            End Get
        End Property

        Public Overrides Sub SetReportParameter(ByVal Parameter As String, ByVal Value As Object)
            If Parameter = "RPPS_ResponseFile" Then
                Parameter_Batch = Convert.ToInt32(Value)
            Else
                MyBase.SetReportParameter(Parameter, Value)
            End If
        End Sub

        Public Overrides Function NeedParameters() As Boolean
            Return MyBase.NeedParameters OrElse Parameter_Batch <= 0
        End Function

        Public Overrides Function RequestReportParameters() As System.Windows.Forms.DialogResult
            Dim Answer As System.Windows.Forms.DialogResult = DialogResult.OK

            If NeedParameters() Then
                Using frm As New RequestParametersForm()
                    Answer = frm.ShowDialog
                    Parameter_Batch = frm.Parameter_BatchID
                End Using
            End If

            Return Answer
        End Function

        Private ds As New System.Data.DataSet("ds")

        Private Sub RPPSResponsesReport_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs)
            Dim rpt As DevExpress.XtraReports.UI.XtraReport = CType(sender, DevExpress.XtraReports.UI.XtraReport)
            Dim sqlInfo As DebtPlus.LINQ.SQLInfoClass = DebtPlus.LINQ.SQLInfoClass.getDefault()
            Try
                Using cn As System.Data.SqlClient.SqlConnection = New System.Data.SqlClient.SqlConnection(sqlInfo.ConnectionString)
                    cn.Open()
                    Using cmd As System.Data.SqlClient.SqlCommand = New System.Data.SqlClient.SqlCommand()
                        cmd.Connection = cn
                        cmd.CommandText = "SELECT [label] FROM rpps_response_files WHERE rpps_response_file = @rpps_response_file"
                        cmd.CommandType = System.Data.CommandType.Text
                        cmd.Parameters.Add("@rpps_response_file", System.Data.SqlDbType.Int).Value = rpt.Parameters("rpps_file").Value
                        cmd.CommandTimeout = 0

                        Dim obj As Object = cmd.ExecuteScalar()
                        Dim Label As String = DebtPlus.Utils.Nulls.DStr(obj).Trim
                        If Label <> String.Empty Then
                            privateSubtitle = String.Format("This report is for response file #{0:f0} '{1}'", Parameter_Batch, Label)
                        Else
                            privateSubtitle = String.Format("This report is for response file #{0:f0}", Parameter_Batch)
                        End If
                    End Using
                End Using

            Catch ex As System.Data.SqlClient.SqlException
                Using gdr As New DebtPlus.Repository.GetDataResult()
                    gdr.HandleException(ex)
                    DebtPlus.UI.Common.ErrorHandling.HandleErrors(gdr, "Error reading RPPS Responses")
                End Using
                e.Cancel = True
            End Try
        End Sub

        Private Sub XrSubreport_CIE_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs)
            With CType(sender, DevExpress.XtraReports.UI.XRSubreport)
                Dim MasterRpt As DevExpress.XtraReports.UI.XtraReport = CType(.Report, DevExpress.XtraReports.UI.XtraReport)
                Dim sqlInfo As DebtPlus.LINQ.SQLInfoClass = DebtPlus.LINQ.SQLInfoClass.getDefault()
                Dim SubRpt As DevExpress.XtraReports.UI.XtraReport = TryCast(.ReportSource, DevExpress.XtraReports.UI.XtraReport)

                If SubRpt IsNot Nothing Then
                    '-- Build a request to read the subreport data
                    Try
                        Using cn As System.Data.SqlClient.SqlConnection = New System.Data.SqlClient.SqlConnection(sqlInfo.ConnectionString)
                            cn.Open()
                            Using cmd As New System.Data.SqlClient.SqlCommand()
                                cmd.Connection = cn
                                cmd.CommandText = "rpt_rpps_response_cie"
                                cmd.CommandType = System.Data.CommandType.StoredProcedure
                                cmd.Parameters.Add("@rpps_response_file", System.Data.SqlDbType.Int).Value = MasterRpt.Parameters("rpps_file").Value
                                cmd.CommandTimeout = 0

                                '-- Read the data from the database
                                Using da As New System.Data.SqlClient.SqlDataAdapter(cmd)
                                    da.Fill(ds, "cie")
                                End Using
                            End Using
                        End Using

                        Dim tbl As System.Data.DataTable = ds.Tables("cie")

                        '-- If there are no rows then don't print the subreport at all
                        '-- Otherwise, pass the table to the subreport and let it bind the fields and print.
                        If tbl.Rows.Count = 0 Then
                            e.Cancel = True
                        Else
                            SubRpt.DataSource = New System.Data.DataView(tbl, String.Empty, "creditor, client, account_number", System.Data.DataViewRowState.CurrentRows)
                        End If

                    Catch ex As System.Data.SqlClient.SqlException
                        Using gdr As New DebtPlus.Repository.GetDataResult() : gdr.HandleException(ex) : DebtPlus.UI.Common.ErrorHandling.HandleErrors(gdr, "Error reading CDA transactions") : End Using
                        e.Cancel = True
                    End Try
                End If
            End With
        End Sub

        Private Sub XrSubreport_CDA_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs)
            With CType(sender, DevExpress.XtraReports.UI.XRSubreport)
                Dim MasterRpt As DevExpress.XtraReports.UI.XtraReport = CType(.Report, DevExpress.XtraReports.UI.XtraReport)
                Dim SubRpt As DevExpress.XtraReports.UI.XtraReport = TryCast(.ReportSource, DevExpress.XtraReports.UI.XtraReport)

                If SubRpt IsNot Nothing Then
                    '-- Build a request to read the subreport data
                    Try
                        Dim sqlInfo As DebtPlus.LINQ.SQLInfoClass = DebtPlus.LINQ.SQLInfoClass.getDefault()
                        Using cn As System.Data.SqlClient.SqlConnection = New System.Data.SqlClient.SqlConnection(sqlInfo.ConnectionString)
                            cn.Open()
                            Using cmd As New System.Data.SqlClient.SqlCommand()
                                cmd.Connection = cn
                                cmd.CommandText = "rpt_rpps_response_cda"
                                cmd.CommandType = System.Data.CommandType.StoredProcedure
                                cmd.Parameters.Add("@rpps_response_file", System.Data.SqlDbType.Int).Value = MasterRpt.Parameters("rpps_file").Value
                                cmd.CommandTimeout = 0

                                '-- Read the data from the database
                                Using da As New System.Data.SqlClient.SqlDataAdapter(cmd)
                                    da.Fill(ds, "cda")
                                End Using
                            End Using
                        End Using

                        Dim tbl As System.Data.DataTable = ds.Tables("cda")

                        '-- If there are no rows then don't print the subreport at all
                        '-- Otherwise, pass the table to the subreport and let it bind the fields and print.
                        If tbl.Rows.Count = 0 Then
                            e.Cancel = True
                        Else
                            SubRpt.DataSource = New System.Data.DataView(tbl, String.Empty, "creditor, client, account_number", System.Data.DataViewRowState.CurrentRows)
                        End If

                    Catch ex As System.Data.SqlClient.SqlException
                        Using gdr As New DebtPlus.Repository.GetDataResult()
                            gdr.HandleException(ex)
                            DebtPlus.UI.Common.ErrorHandling.HandleErrors(gdr, "Error reading RPPS Responses")
                        End Using

                        e.Cancel = True
                    End Try
                End If
            End With
        End Sub

        Private Sub XrSubreport_CDR_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs)
            With CType(sender, DevExpress.XtraReports.UI.XRSubreport)
                Dim MasterRpt As DevExpress.XtraReports.UI.XtraReport = CType(.Report, DevExpress.XtraReports.UI.XtraReport)
                Dim SubRpt As DevExpress.XtraReports.UI.XtraReport = TryCast(.ReportSource, DevExpress.XtraReports.UI.XtraReport)

                If SubRpt IsNot Nothing Then
                    '-- Build a request to read the subreport data
                    Try
                        Dim sqlInfo As DebtPlus.LINQ.SQLInfoClass = DebtPlus.LINQ.SQLInfoClass.getDefault()
                        Using cn As System.Data.SqlClient.SqlConnection = New System.Data.SqlClient.SqlConnection(sqlInfo.ConnectionString)
                            cn.Open()
                            Using cmd As New System.Data.SqlClient.SqlCommand()
                                cmd.Connection = cn
                                cmd.CommandText = "rpt_rpps_response_cdr"
                                cmd.CommandType = System.Data.CommandType.StoredProcedure
                                cmd.Parameters.Add("@rpps_response_file", System.Data.SqlDbType.Int).Value = MasterRpt.Parameters("rpps_file").Value
                                cmd.CommandTimeout = 0

                                '-- Read the data from the database
                                Using da As New System.Data.SqlClient.SqlDataAdapter(cmd)
                                    da.Fill(ds, "cdr")
                                End Using
                            End Using
                        End Using
                        Dim tbl As System.Data.DataTable = ds.Tables("cdr")

                        '-- If there are no rows then don't print the subreport at all
                        '-- Otherwise, pass the table to the subreport and let it bind the fields and print.
                        If tbl.Rows.Count = 0 Then
                            e.Cancel = True
                        Else
                            SubRpt.DataSource = New System.Data.DataView(tbl, String.Empty, "creditor, client, account_number", System.Data.DataViewRowState.CurrentRows)
                        End If

                    Catch ex As System.Data.SqlClient.SqlException
                        Using gdr As New DebtPlus.Repository.GetDataResult()
                            gdr.HandleException(ex)
                            DebtPlus.UI.Common.ErrorHandling.HandleErrors(gdr, "Error reading RPPS Responses")
                        End Using
                        e.Cancel = True
                    End Try
                End If
            End With
        End Sub

        Private Sub XrSubreport_CDP_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs)
            With CType(sender, DevExpress.XtraReports.UI.XRSubreport)
                Dim MasterRpt As DevExpress.XtraReports.UI.XtraReport = CType(.Report, DevExpress.XtraReports.UI.XtraReport)
                Dim SubRpt As DevExpress.XtraReports.UI.XtraReport = TryCast(.ReportSource, DevExpress.XtraReports.UI.XtraReport)

                If SubRpt IsNot Nothing Then
                    '-- Build a request to read the subreport data
                    Try
                        Dim sqlInfo As DebtPlus.LINQ.SQLInfoClass = DebtPlus.LINQ.SQLInfoClass.getDefault()
                        Using cn As System.Data.SqlClient.SqlConnection = New System.Data.SqlClient.SqlConnection(sqlInfo.ConnectionString)
                            cn.Open()
                            Using cmd As New System.Data.SqlClient.SqlCommand()
                                cmd.Connection = cn
                                cmd.CommandText = "rpt_rpps_response_cdp"
                                cmd.CommandType = System.Data.CommandType.StoredProcedure
                                cmd.Parameters.Add("@rpps_response_file", System.Data.SqlDbType.Int).Value = MasterRpt.Parameters("rpps_file").Value
                                cmd.CommandTimeout = 0

                                '-- Read the data from the database
                                Using da As New System.Data.SqlClient.SqlDataAdapter(cmd)
                                    da.Fill(ds, "cdp")
                                End Using
                            End Using
                        End Using
                        Dim tbl As System.Data.DataTable = ds.Tables("cdp")

                        '-- If there are no rows then don't print the subreport at all
                        '-- Otherwise, pass the table to the subreport and let it bind the fields and print.
                        If tbl.Rows.Count = 0 Then
                            e.Cancel = True
                        Else
                            SubRpt.DataSource = New System.Data.DataView(tbl, String.Empty, "creditor, client, account_number", System.Data.DataViewRowState.CurrentRows)
                        End If

                    Catch ex As System.Data.SqlClient.SqlException
                        Using gdr As New DebtPlus.Repository.GetDataResult()
                            gdr.HandleException(ex)
                            DebtPlus.UI.Common.ErrorHandling.HandleErrors(gdr, "Error reading RPPS Responses")
                        End Using
                        e.Cancel = True
                    End Try
                End If
            End With
        End Sub

        Private Sub XrSubreport_CDT_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs)
            With CType(sender, DevExpress.XtraReports.UI.XRSubreport)
                Dim MasterRpt As DevExpress.XtraReports.UI.XtraReport = CType(.Report, DevExpress.XtraReports.UI.XtraReport)
                Dim SubRpt As DevExpress.XtraReports.UI.XtraReport = TryCast(.ReportSource, DevExpress.XtraReports.UI.XtraReport)

                If SubRpt IsNot Nothing Then
                    '-- Build a request to read the subreport data
                    Try
                        Dim sqlInfo As DebtPlus.LINQ.SQLInfoClass = DebtPlus.LINQ.SQLInfoClass.getDefault()
                        Using cn As System.Data.SqlClient.SqlConnection = New System.Data.SqlClient.SqlConnection(sqlInfo.ConnectionString)
                            cn.Open()
                            Using cmd As New System.Data.SqlClient.SqlCommand()
                                cmd.Connection = cn
                                cmd.CommandText = "rpt_rpps_response_cdt"
                                cmd.CommandType = System.Data.CommandType.StoredProcedure
                                cmd.Parameters.Add("@rpps_response_file", System.Data.SqlDbType.Int).Value = MasterRpt.Parameters("rpps_file").Value
                                cmd.CommandTimeout = 0

                                '-- Read the data from the database
                                Using da As New System.Data.SqlClient.SqlDataAdapter(cmd)
                                    da.Fill(ds, "cdt")
                                End Using
                            End Using
                        End Using
                        Dim tbl As System.Data.DataTable = ds.Tables("cdt")

                        '-- If there are no rows then don't print the subreport at all
                        '-- Otherwise, pass the table to the subreport and let it bind the fields and print.
                        If tbl.Rows.Count = 0 Then
                            e.Cancel = True
                        Else
                            SubRpt.DataSource = New System.Data.DataView(tbl, String.Empty, "client, account_number", System.Data.DataViewRowState.CurrentRows)
                        End If

                    Catch ex As System.Data.SqlClient.SqlException
                        Using gdr As New DebtPlus.Repository.GetDataResult()
                            gdr.HandleException(ex)
                            DebtPlus.UI.Common.ErrorHandling.HandleErrors(gdr, "Error reading RPPS Responses")
                        End Using
                        e.Cancel = True
                    End Try
                End If
            End With
        End Sub

        Private Sub XrSubreport_CDV_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs)
            With CType(sender, DevExpress.XtraReports.UI.XRSubreport)
                Dim MasterRpt As DevExpress.XtraReports.UI.XtraReport = CType(.Report, DevExpress.XtraReports.UI.XtraReport)
                Dim SubRpt As DevExpress.XtraReports.UI.XtraReport = TryCast(.ReportSource, DevExpress.XtraReports.UI.XtraReport)

                If SubRpt IsNot Nothing Then
                    '-- Build a request to read the subreport data
                    Try
                        Dim sqlInfo As DebtPlus.LINQ.SQLInfoClass = DebtPlus.LINQ.SQLInfoClass.getDefault()
                        Using cn As System.Data.SqlClient.SqlConnection = New System.Data.SqlClient.SqlConnection(sqlInfo.ConnectionString)
                            cn.Open()
                            Using cmd As New System.Data.SqlClient.SqlCommand()
                                cmd.Connection = cn
                                cmd.CommandText = "rpt_rpps_response_cdv"
                                cmd.CommandType = System.Data.CommandType.StoredProcedure
                                cmd.Parameters.Add("@rpps_response_file", System.Data.SqlDbType.Int).Value = MasterRpt.Parameters("rpps_file").Value
                                cmd.CommandTimeout = 0

                                '-- Read the data from the database
                                Using da As New System.Data.SqlClient.SqlDataAdapter(cmd)
                                    da.Fill(ds, "cdv")
                                End Using
                            End Using
                        End Using
                        Dim tbl As System.Data.DataTable = ds.Tables("cdv")

                        '-- If there are no rows then don't print the subreport at all
                        '-- Otherwise, pass the table to the subreport and let it bind the fields and print.
                        If tbl.Rows.Count = 0 Then
                            e.Cancel = True
                        Else
                            SubRpt.DataSource = New System.Data.DataView(tbl, String.Empty, "creditor, client, account_number", System.Data.DataViewRowState.CurrentRows)
                        End If

                    Catch ex As System.Data.SqlClient.SqlException
                        Using gdr As New DebtPlus.Repository.GetDataResult()
                            gdr.HandleException(ex)
                            DebtPlus.UI.Common.ErrorHandling.HandleErrors(gdr, "Error reading RPPS Responses")
                        End Using
                        e.Cancel = True
                    End Try
                End If
            End With
        End Sub

        Private Sub XrSubreport_CDM_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs)
            With CType(sender, DevExpress.XtraReports.UI.XRSubreport)
                Dim MasterRpt As DevExpress.XtraReports.UI.XtraReport = CType(.Report, DevExpress.XtraReports.UI.XtraReport)
                Dim SubRpt As DevExpress.XtraReports.UI.XtraReport = TryCast(.ReportSource, DevExpress.XtraReports.UI.XtraReport)

                If SubRpt IsNot Nothing Then
                    '-- Build a request to read the subreport data
                    Try
                        Dim sqlInfo As DebtPlus.LINQ.SQLInfoClass = DebtPlus.LINQ.SQLInfoClass.getDefault()
                        Using cn As System.Data.SqlClient.SqlConnection = New System.Data.SqlClient.SqlConnection(sqlInfo.ConnectionString)
                            cn.Open()
                            Using cmd As New System.Data.SqlClient.SqlCommand()
                                cmd.Connection = cn
                                cmd.CommandText = "rpt_rpps_response_cdn"
                                cmd.CommandType = System.Data.CommandType.StoredProcedure
                                cmd.Parameters.Add("@rpps_response_file", System.Data.SqlDbType.Int).Value = MasterRpt.Parameters("rpps_file").Value
                                cmd.CommandTimeout = 0

                                '-- Read the data from the database
                                Using da As New System.Data.SqlClient.SqlDataAdapter(cmd)
                                    da.Fill(ds, "cdm")
                                End Using
                            End Using
                        End Using
                        Dim tbl As System.Data.DataTable = ds.Tables("cdm")

                        '-- If there are no rows then don't print the subreport at all
                        '-- Otherwise, pass the table to the subreport and let it bind the fields and print.
                        If tbl.Rows.Count = 0 Then
                            e.Cancel = True
                        Else
                            SubRpt.DataSource = New System.Data.DataView(tbl, String.Empty, "creditor, trace_number, client", System.Data.DataViewRowState.CurrentRows)
                        End If

                    Catch ex As System.Data.SqlClient.SqlException
                        Using gdr As New DebtPlus.Repository.GetDataResult()
                            gdr.HandleException(ex)
                            DebtPlus.UI.Common.ErrorHandling.HandleErrors(gdr, "Error reading RPPS Responses")
                        End Using
                        e.Cancel = True
                    End Try
                End If
            End With
        End Sub

        Private Sub XrSubreport_CDC_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs)
            With CType(sender, DevExpress.XtraReports.UI.XRSubreport)
                Dim MasterRpt As DevExpress.XtraReports.UI.XtraReport = CType(.Report, DevExpress.XtraReports.UI.XtraReport)
                Dim SubRpt As DevExpress.XtraReports.UI.XtraReport = TryCast(.ReportSource, DevExpress.XtraReports.UI.XtraReport)

                If SubRpt IsNot Nothing Then
                    '-- Build a request to read the subreport data
                    Try
                        Dim sqlInfo As DebtPlus.LINQ.SQLInfoClass = DebtPlus.LINQ.SQLInfoClass.getDefault()
                        Using cn As System.Data.SqlClient.SqlConnection = New System.Data.SqlClient.SqlConnection(sqlInfo.ConnectionString)
                            cn.Open()
                            Using cmd As New System.Data.SqlClient.SqlCommand()
                                cmd.Connection = cn
                                cmd.CommandText = "rpt_rpps_response_cdc"
                                cmd.CommandType = System.Data.CommandType.StoredProcedure
                                cmd.Parameters.Add("@rpps_response_file", System.Data.SqlDbType.Int).Value = MasterRpt.Parameters("rpps_file").Value
                                cmd.CommandTimeout = 0

                                '-- Read the data from the database
                                Using da As New System.Data.SqlClient.SqlDataAdapter(cmd)
                                    da.Fill(ds, "cdc")
                                End Using
                            End Using
                        End Using
                        Dim tbl As System.Data.DataTable = ds.Tables("cdc")

                        '-- If there are no rows then don't print the subreport at all
                        '-- Otherwise, pass the table to the subreport and let it bind the fields and print.
                        If tbl.Rows.Count = 0 Then
                            e.Cancel = True
                        Else
                            SubRpt.DataSource = New System.Data.DataView(tbl, String.Empty, "creditor, client", System.Data.DataViewRowState.CurrentRows)
                        End If

                    Catch ex As System.Data.SqlClient.SqlException
                        Using gdr As New DebtPlus.Repository.GetDataResult() : gdr.HandleException(ex) : DebtPlus.UI.Common.ErrorHandling.HandleErrors(gdr, "Error reading cdc transactions") : End Using
                        e.Cancel = True
                    End Try
                End If
            End With
        End Sub

        Private Sub XrSubreport_COA_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs)
            With CType(sender, DevExpress.XtraReports.UI.XRSubreport)
                Dim MasterRpt As DevExpress.XtraReports.UI.XtraReport = CType(.Report, DevExpress.XtraReports.UI.XtraReport)
                Dim SubRpt As DevExpress.XtraReports.UI.XtraReport = TryCast(.ReportSource, DevExpress.XtraReports.UI.XtraReport)

                If SubRpt IsNot Nothing Then
                    '-- Build a request to read the subreport data
                    Try
                        Dim sqlInfo As DebtPlus.LINQ.SQLInfoClass = DebtPlus.LINQ.SQLInfoClass.getDefault()
                        Using cn As System.Data.SqlClient.SqlConnection = New System.Data.SqlClient.SqlConnection(sqlInfo.ConnectionString)
                            cn.Open()
                            Using cmd As New System.Data.SqlClient.SqlCommand()
                                cmd.Connection = cn
                                cmd.CommandText = "rpt_rpps_response_coa"
                                cmd.CommandType = System.Data.CommandType.StoredProcedure
                                cmd.Parameters.Add("@rpps_response_file", System.Data.SqlDbType.Int).Value = MasterRpt.Parameters("rpps_file").Value
                                cmd.CommandTimeout = 0

                                '-- Read the data from the database
                                Using da As New System.Data.SqlClient.SqlDataAdapter(cmd)
                                    da.Fill(ds, "coa")
                                End Using
                            End Using
                        End Using
                        Dim tbl As System.Data.DataTable = ds.Tables("coa")

                        '-- If there are no rows then don't print the subreport at all
                        '-- Otherwise, pass the table to the subreport and let it bind the fields and print.
                        If tbl.Rows.Count = 0 Then
                            e.Cancel = True
                        Else
                            SubRpt.DataSource = New System.Data.DataView(tbl, String.Empty, "creditor, client, old_account_number", System.Data.DataViewRowState.CurrentRows)
                        End If

                    Catch ex As System.Data.SqlClient.SqlException
                        Using gdr As New DebtPlus.Repository.GetDataResult() : gdr.HandleException(ex) : DebtPlus.UI.Common.ErrorHandling.HandleErrors(gdr, "Error reading coa transactions") : End Using
                        e.Cancel = True
                    End Try
                End If
            End With
        End Sub

        Private Sub XrSubreport_DFS_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs)
            With CType(sender, DevExpress.XtraReports.UI.XRSubreport)
                Dim MasterRpt As DevExpress.XtraReports.UI.XtraReport = CType(.Report, DevExpress.XtraReports.UI.XtraReport)
                Dim SubRpt As DevExpress.XtraReports.UI.XtraReport = TryCast(.ReportSource, DevExpress.XtraReports.UI.XtraReport)

                If SubRpt IsNot Nothing Then
                    '-- Build a request to read the subreport data
                    Try
                        Dim sqlInfo As DebtPlus.LINQ.SQLInfoClass = DebtPlus.LINQ.SQLInfoClass.getDefault()
                        Using cn As System.Data.SqlClient.SqlConnection = New System.Data.SqlClient.SqlConnection(sqlInfo.ConnectionString)
                            cn.Open()
                            Using cmd As New System.Data.SqlClient.SqlCommand()
                                cmd.Connection = cn
                                cmd.CommandText = "rpt_rpps_response_dfs"
                                cmd.CommandType = System.Data.CommandType.StoredProcedure
                                cmd.Parameters.Add("@rpps_response_file", System.Data.SqlDbType.Int).Value = MasterRpt.Parameters("rpps_file").Value
                                cmd.CommandTimeout = 0

                                '-- Read the data from the database
                                Using da As New System.Data.SqlClient.SqlDataAdapter(cmd)
                                    da.Fill(ds, "dfs")
                                End Using
                            End Using
                        End Using
                        Dim tbl As System.Data.DataTable = ds.Tables("dfs")

                        '-- If there are no rows then don't print the subreport at all
                        '-- Otherwise, pass the table to the subreport and let it bind the fields and print.
                        If tbl.Rows.Count = 0 Then
                            e.Cancel = True
                        Else
                            SubRpt.DataSource = New System.Data.DataView(tbl, String.Empty, "rpps_biller_id, account_number", System.Data.DataViewRowState.CurrentRows)
                        End If

                    Catch ex As System.Data.SqlClient.SqlException
                        Using gdr As New DebtPlus.Repository.GetDataResult() : gdr.HandleException(ex) : DebtPlus.UI.Common.ErrorHandling.HandleErrors(gdr, "Error reading DFS transactions") : End Using
                        e.Cancel = True
                    End Try
                End If
            End With
        End Sub
    End Class
End Namespace
