Namespace RPPS.Responses
    <Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
    Partial Class Subreport_COA
        Inherits DevExpress.XtraReports.UI.XtraReport

        'XtraReport overrides dispose to clean up the component list.
        <System.Diagnostics.DebuggerNonUserCode()> _
        Protected Overrides Sub Dispose(ByVal disposing As Boolean)
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
            MyBase.Dispose(disposing)
        End Sub

        'Required by the Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Designer
        'It can be modified using the Designer.  
        'Do not modify it using the code editor.
        <System.Diagnostics.DebuggerStepThrough()> _
        Private Sub InitializeComponent()
            Dim XrSummary1 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary()
            Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Subreport_COA))
            Me.Detail = New DevExpress.XtraReports.UI.DetailBand()
            Me.XrLabel_NewBillerID = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_NewAccountNumber = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_old_account_number = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_client = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_counselor_name = New DevExpress.XtraReports.UI.XRLabel()
            Me.PageHeader = New DevExpress.XtraReports.UI.PageHeaderBand()
            Me.XrPanel1 = New DevExpress.XtraReports.UI.XRPanel()
            Me.XrLabel4 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel2 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel9 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel8 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel3 = New DevExpress.XtraReports.UI.XRLabel()
            Me.ReportHeader = New DevExpress.XtraReports.UI.ReportHeaderBand()
            Me.XrLabel1 = New DevExpress.XtraReports.UI.XRLabel()
            Me.GroupHeader1 = New DevExpress.XtraReports.UI.GroupHeaderBand()
            Me.XrLabel_group_header = New DevExpress.XtraReports.UI.XRLabel()
            Me.ReportFooter = New DevExpress.XtraReports.UI.ReportFooterBand()
            Me.XrLabel25 = New DevExpress.XtraReports.UI.XRLabel()
            Me.TopMarginBand1 = New DevExpress.XtraReports.UI.TopMarginBand()
            Me.BottomMarginBand1 = New DevExpress.XtraReports.UI.BottomMarginBand()
            CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
            '
            'Detail
            '
            Me.Detail.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel_NewBillerID, Me.XrLabel_NewAccountNumber, Me.XrLabel_old_account_number, Me.XrLabel_client, Me.XrLabel_counselor_name})
            Me.Detail.HeightF = 15.0!
            Me.Detail.Name = "Detail"
            Me.Detail.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
            Me.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
            '
            'XrLabel_NewBillerID
            '
            Me.XrLabel_NewBillerID.Font = New System.Drawing.Font("Times New Roman", 9.75!)
            Me.XrLabel_NewBillerID.ForeColor = System.Drawing.SystemColors.WindowText
            Me.XrLabel_NewBillerID.LocationFloat = New DevExpress.Utils.PointFloat(561.3646!, 0.0!)
            Me.XrLabel_NewBillerID.Name = "XrLabel_NewBillerID"
            Me.XrLabel_NewBillerID.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_NewBillerID.Scripts.OnBeforePrint = "XrLabel_NewBillerID_BeforePrint"
            Me.XrLabel_NewBillerID.SizeF = New System.Drawing.SizeF(119.125!, 14.99999!)
            Me.XrLabel_NewBillerID.StylePriority.UseFont = False
            Me.XrLabel_NewBillerID.StylePriority.UseForeColor = False
            Me.XrLabel_NewBillerID.StylePriority.UseTextAlignment = False
            Me.XrLabel_NewBillerID.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            Me.XrLabel_NewBillerID.WordWrap = False
            '
            'XrLabel_NewAccountNumber
            '
            Me.XrLabel_NewAccountNumber.Font = New System.Drawing.Font("Times New Roman", 9.75!)
            Me.XrLabel_NewAccountNumber.ForeColor = System.Drawing.SystemColors.WindowText
            Me.XrLabel_NewAccountNumber.LocationFloat = New DevExpress.Utils.PointFloat(400.8542!, 0.0!)
            Me.XrLabel_NewAccountNumber.Name = "XrLabel_NewAccountNumber"
            Me.XrLabel_NewAccountNumber.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_NewAccountNumber.Scripts.OnBeforePrint = "XrLabel_NewAccountNumber_BeforePrint"
            Me.XrLabel_NewAccountNumber.SizeF = New System.Drawing.SizeF(141.0!, 15.0!)
            Me.XrLabel_NewAccountNumber.StylePriority.UseFont = False
            Me.XrLabel_NewAccountNumber.StylePriority.UseForeColor = False
            Me.XrLabel_NewAccountNumber.StylePriority.UseTextAlignment = False
            Me.XrLabel_NewAccountNumber.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            Me.XrLabel_NewAccountNumber.WordWrap = False
            '
            'XrLabel_old_account_number
            '
            Me.XrLabel_old_account_number.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "old_account_number")})
            Me.XrLabel_old_account_number.Font = New System.Drawing.Font("Times New Roman", 9.75!)
            Me.XrLabel_old_account_number.ForeColor = System.Drawing.SystemColors.WindowText
            Me.XrLabel_old_account_number.LocationFloat = New DevExpress.Utils.PointFloat(240.3438!, 0.0!)
            Me.XrLabel_old_account_number.Name = "XrLabel_old_account_number"
            Me.XrLabel_old_account_number.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_old_account_number.SizeF = New System.Drawing.SizeF(141.0!, 15.0!)
            Me.XrLabel_old_account_number.StylePriority.UseFont = False
            Me.XrLabel_old_account_number.StylePriority.UseForeColor = False
            Me.XrLabel_old_account_number.StylePriority.UseTextAlignment = False
            Me.XrLabel_old_account_number.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            Me.XrLabel_old_account_number.WordWrap = False
            '
            'XrLabel_client
            '
            Me.XrLabel_client.Font = New System.Drawing.Font("Times New Roman", 9.75!)
            Me.XrLabel_client.ForeColor = System.Drawing.SystemColors.WindowText
            Me.XrLabel_client.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
            Me.XrLabel_client.Name = "XrLabel_client"
            Me.XrLabel_client.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_client.SizeF = New System.Drawing.SizeF(220.8333!, 14.99999!)
            Me.XrLabel_client.StylePriority.UseFont = False
            Me.XrLabel_client.StylePriority.UseForeColor = False
            Me.XrLabel_client.StylePriority.UseTextAlignment = False
            Me.XrLabel_client.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            Me.XrLabel_client.WordWrap = False
            '
            'XrLabel_counselor_name
            '
            Me.XrLabel_counselor_name.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "counselor_name")})
            Me.XrLabel_counselor_name.Font = New System.Drawing.Font("Times New Roman", 9.75!)
            Me.XrLabel_counselor_name.ForeColor = System.Drawing.SystemColors.WindowText
            Me.XrLabel_counselor_name.LocationFloat = New DevExpress.Utils.PointFloat(700.0!, 0.0!)
            Me.XrLabel_counselor_name.Name = "XrLabel_counselor_name"
            Me.XrLabel_counselor_name.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_counselor_name.SizeF = New System.Drawing.SizeF(91.0!, 15.0!)
            Me.XrLabel_counselor_name.StylePriority.UseFont = False
            Me.XrLabel_counselor_name.StylePriority.UseForeColor = False
            Me.XrLabel_counselor_name.StylePriority.UseTextAlignment = False
            Me.XrLabel_counselor_name.Text = " "
            Me.XrLabel_counselor_name.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            Me.XrLabel_counselor_name.WordWrap = False
            '
            'PageHeader
            '
            Me.PageHeader.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrPanel1})
            Me.PageHeader.HeightF = 26.0!
            Me.PageHeader.Name = "PageHeader"
            Me.PageHeader.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
            Me.PageHeader.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
            '
            'XrPanel1
            '
            Me.XrPanel1.BackColor = System.Drawing.Color.Teal
            Me.XrPanel1.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel4, Me.XrLabel2, Me.XrLabel9, Me.XrLabel8, Me.XrLabel3})
            Me.XrPanel1.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
            Me.XrPanel1.Name = "XrPanel1"
            Me.XrPanel1.SizeF = New System.Drawing.SizeF(800.0!, 18.0!)
            Me.XrPanel1.StylePriority.UseBackColor = False
            '
            'XrLabel4
            '
            Me.XrLabel4.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel4.ForeColor = System.Drawing.Color.White
            Me.XrLabel4.LocationFloat = New DevExpress.Utils.PointFloat(561.3646!, 0.0!)
            Me.XrLabel4.Name = "XrLabel4"
            Me.XrLabel4.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel4.SizeF = New System.Drawing.SizeF(119.125!, 15.0!)
            Me.XrLabel4.StylePriority.UseFont = False
            Me.XrLabel4.StylePriority.UseForeColor = False
            Me.XrLabel4.StylePriority.UseTextAlignment = False
            Me.XrLabel4.Text = "NEW BILLER ID"
            Me.XrLabel4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            '
            'XrLabel2
            '
            Me.XrLabel2.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel2.ForeColor = System.Drawing.Color.White
            Me.XrLabel2.LocationFloat = New DevExpress.Utils.PointFloat(400.8542!, 0.0!)
            Me.XrLabel2.Name = "XrLabel2"
            Me.XrLabel2.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel2.SizeF = New System.Drawing.SizeF(141.0!, 15.0!)
            Me.XrLabel2.StylePriority.UseFont = False
            Me.XrLabel2.StylePriority.UseForeColor = False
            Me.XrLabel2.StylePriority.UseTextAlignment = False
            Me.XrLabel2.Text = "NEW ACCOUNT NO."
            Me.XrLabel2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            '
            'XrLabel9
            '
            Me.XrLabel9.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel9.ForeColor = System.Drawing.Color.White
            Me.XrLabel9.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.9999911!)
            Me.XrLabel9.Name = "XrLabel9"
            Me.XrLabel9.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel9.SizeF = New System.Drawing.SizeF(220.8333!, 15.0!)
            Me.XrLabel9.StylePriority.UseFont = False
            Me.XrLabel9.StylePriority.UseForeColor = False
            Me.XrLabel9.StylePriority.UseTextAlignment = False
            Me.XrLabel9.Text = "CLIENT ID & NAME"
            Me.XrLabel9.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            '
            'XrLabel8
            '
            Me.XrLabel8.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel8.ForeColor = System.Drawing.Color.White
            Me.XrLabel8.LocationFloat = New DevExpress.Utils.PointFloat(240.3438!, 0.0!)
            Me.XrLabel8.Name = "XrLabel8"
            Me.XrLabel8.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel8.SizeF = New System.Drawing.SizeF(141.0!, 15.0!)
            Me.XrLabel8.StylePriority.UseFont = False
            Me.XrLabel8.StylePriority.UseForeColor = False
            Me.XrLabel8.StylePriority.UseTextAlignment = False
            Me.XrLabel8.Text = "OLD ACCOUNT NO."
            Me.XrLabel8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            '
            'XrLabel3
            '
            Me.XrLabel3.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel3.ForeColor = System.Drawing.Color.White
            Me.XrLabel3.LocationFloat = New DevExpress.Utils.PointFloat(700.0!, 1.0!)
            Me.XrLabel3.Name = "XrLabel3"
            Me.XrLabel3.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel3.SizeF = New System.Drawing.SizeF(91.0!, 15.0!)
            Me.XrLabel3.StylePriority.UseFont = False
            Me.XrLabel3.StylePriority.UseForeColor = False
            Me.XrLabel3.StylePriority.UseTextAlignment = False
            Me.XrLabel3.Text = "COUNSELOR"
            Me.XrLabel3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            '
            'ReportHeader
            '
            Me.ReportHeader.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel1})
            Me.ReportHeader.HeightF = 30.0!
            Me.ReportHeader.Name = "ReportHeader"
            '
            'XrLabel1
            '
            Me.XrLabel1.Font = New System.Drawing.Font("Times New Roman", 14.0!, System.Drawing.FontStyle.Bold)
            Me.XrLabel1.ForeColor = System.Drawing.Color.Teal
            Me.XrLabel1.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
            Me.XrLabel1.Name = "XrLabel1"
            Me.XrLabel1.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel1.SizeF = New System.Drawing.SizeF(800.0!, 25.0!)
            Me.XrLabel1.StylePriority.UseFont = False
            Me.XrLabel1.StylePriority.UseForeColor = False
            Me.XrLabel1.Text = "COA CHANGE NOTIFICATIONS"
            '
            'GroupHeader1
            '
            Me.GroupHeader1.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel_group_header})
            Me.GroupHeader1.GroupFields.AddRange(New DevExpress.XtraReports.UI.GroupField() {New DevExpress.XtraReports.UI.GroupField("creditor", DevExpress.XtraReports.UI.XRColumnSortOrder.Ascending)})
            Me.GroupHeader1.HeightF = 42.0!
            Me.GroupHeader1.Name = "GroupHeader1"
            '
            'XrLabel_group_header
            '
            Me.XrLabel_group_header.Font = New System.Drawing.Font("Times New Roman", 12.0!, System.Drawing.FontStyle.Bold)
            Me.XrLabel_group_header.ForeColor = System.Drawing.Color.Maroon
            Me.XrLabel_group_header.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 8.0!)
            Me.XrLabel_group_header.Name = "XrLabel_group_header"
            Me.XrLabel_group_header.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_group_header.SizeF = New System.Drawing.SizeF(800.0!, 25.0!)
            Me.XrLabel_group_header.StylePriority.UseFont = False
            Me.XrLabel_group_header.StylePriority.UseForeColor = False
            Me.XrLabel_group_header.Text = "[creditor] [creditor_name]"
            '
            'ReportFooter
            '
            Me.ReportFooter.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel25})
            Me.ReportFooter.HeightF = 45.0!
            Me.ReportFooter.Name = "ReportFooter"
            Me.ReportFooter.PageBreak = DevExpress.XtraReports.UI.PageBreak.AfterBand
            '
            'XrLabel25
            '
            Me.XrLabel25.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "client")})
            Me.XrLabel25.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.XrLabel25.ForeColor = System.Drawing.SystemColors.WindowText
            Me.XrLabel25.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 7.999992!)
            Me.XrLabel25.Name = "XrLabel25"
            Me.XrLabel25.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel25.SizeF = New System.Drawing.SizeF(789.9999!, 25.0!)
            Me.XrLabel25.StylePriority.UseFont = False
            Me.XrLabel25.StylePriority.UseForeColor = False
            XrSummary1.FormatString = "{0:n0} Account Change Notifications"
            XrSummary1.Func = DevExpress.XtraReports.UI.SummaryFunc.Count
            XrSummary1.Running = DevExpress.XtraReports.UI.SummaryRunning.Report
            Me.XrLabel25.Summary = XrSummary1
            '
            'TopMarginBand1
            '
            Me.TopMarginBand1.HeightF = 25.0!
            Me.TopMarginBand1.Name = "TopMarginBand1"
            '
            'BottomMarginBand1
            '
            Me.BottomMarginBand1.HeightF = 25.0!
            Me.BottomMarginBand1.Name = "BottomMarginBand1"
            '
            'Subreport_COA
            '
            Me.Bands.AddRange(New DevExpress.XtraReports.UI.Band() {Me.Detail, Me.PageHeader, Me.ReportHeader, Me.GroupHeader1, Me.ReportFooter, Me.TopMarginBand1, Me.BottomMarginBand1})
            Me.Margins = New System.Drawing.Printing.Margins(25, 25, 25, 25)
            Me.ScriptLanguage = DevExpress.XtraReports.ScriptLanguage.VisualBasic
            Me.ScriptReferencesString = resources.GetString("$this.ScriptReferencesString")
            Me.ScriptsSource = resources.GetString("$this.ScriptsSource")
            Me.Version = "11.2"
            CType(Me, System.ComponentModel.ISupportInitialize).EndInit()

        End Sub
        Friend WithEvents Detail As DevExpress.XtraReports.UI.DetailBand
        Friend WithEvents PageHeader As DevExpress.XtraReports.UI.PageHeaderBand
        Friend WithEvents ReportHeader As DevExpress.XtraReports.UI.ReportHeaderBand
        Friend WithEvents GroupHeader1 As DevExpress.XtraReports.UI.GroupHeaderBand
        Friend WithEvents ReportFooter As DevExpress.XtraReports.UI.ReportFooterBand
        Friend WithEvents XrLabel1 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrPanel1 As DevExpress.XtraReports.UI.XRPanel
        Friend WithEvents XrLabel9 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel3 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_group_header As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_old_account_number As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_client As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_counselor_name As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel25 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents TopMarginBand1 As DevExpress.XtraReports.UI.TopMarginBand
        Friend WithEvents BottomMarginBand1 As DevExpress.XtraReports.UI.BottomMarginBand
        Friend WithEvents XrLabel_NewBillerID As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_NewAccountNumber As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel4 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel2 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel8 As DevExpress.XtraReports.UI.XRLabel
    End Class
End Namespace
