#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}
#End Region
Imports System.Xml

Namespace RPPS.Responses
    Friend Class Subreport_CDR
        Public Sub New()
            MyBase.new()
            InitializeComponent()
            AddHandler XrLabel_missing_item.BeforePrint, AddressOf XrLabel_missing_item_BeforePrint
        End Sub

        Private Sub XrLabel_missing_item_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs)
            With CType(sender, DevExpress.XtraReports.UI.XRLabel)
                Dim rpt As DevExpress.XtraReports.UI.XtraReport = CType(.Report, DevExpress.XtraReports.UI.XtraReport)
                Dim reject_reason As String = DebtPlus.Utils.Nulls.DStr(rpt.GetCurrentColumnValue("reject_reason")).Trim
                Dim ItemText As String = String.Empty

                Select Case reject_reason.ToUpper
                    Case "AO"
                        ItemText = format_AO(rpt)
                    Case "FA"
                        ItemText = format_FA(rpt)
                    Case "IP"
                        ItemText = format_IP(rpt)
                    Case "IE"
                        ItemText = format_IE(rpt)
                    Case "MI"
                        ItemText = format_MI(rpt)
                    Case "PI"
                        ItemText = format_PI(rpt)
                    Case "PO"
                        ItemText = format_PO(rpt)
                    Case "PT"
                        ItemText = format_PT(rpt)
                    Case "RA"
                        ItemText = format_RA(rpt)
                    Case "TP"
                        ItemText = format_TP(rpt)
                    Case Else
                End Select

                '-- Finally, put the text into the field. Cancel the field if there is no text.
                If ItemText <> String.Empty Then
                    .Text = ItemText + Environment.NewLine
                Else
                    e.Cancel = True
                End If
            End With
        End Sub

        Private Function getResubmitDate(ByVal rpt As DevExpress.XtraReports.UI.XtraReport) As String
            Dim obj As Object = rpt.GetCurrentColumnValue("resubmit_date")
            Dim txt As String = String.Empty
            If obj IsNot Nothing AndAlso obj IsNot System.DBNull.Value Then
                txt = String.Format("Resubmit date:{0:d}", obj)
            End If
            Return txt
        End Function

        Private Function getCycleMonthsRemaining(ByVal rpt As DevExpress.XtraReports.UI.XtraReport) As String
            Dim obj As Object = rpt.GetCurrentColumnValue("cycle_months_remaining")
            Dim txt As String = String.Empty
            If obj IsNot Nothing AndAlso obj IsNot System.DBNull.Value Then
                txt = String.Format("Cycle Months Remaining:{0:f0}", obj)
            End If
            Return txt
        End Function

        Private Function getForgivenPct(ByVal rpt As DevExpress.XtraReports.UI.XtraReport) As String
            Dim obj As Object = rpt.GetCurrentColumnValue("forgiven_pct")
            Dim txt As String = String.Empty
            If obj IsNot Nothing AndAlso obj IsNot System.DBNull.Value Then
                txt = String.Format("Forgiven Percent:{0:p}", obj)
            End If
            Return txt
        End Function

        Private Function getInterestRate(ByVal rpt As DevExpress.XtraReports.UI.XtraReport) As String
            Dim obj As Object = rpt.GetCurrentColumnValue("interest_rate")
            Dim txt As String = String.Empty
            If obj IsNot Nothing AndAlso obj IsNot System.DBNull.Value Then
                txt = String.Format("Interest Rate:{0:p}", obj)
            End If
            Return txt
        End Function

        Private Function getMissingItem(ByVal rpt As DevExpress.XtraReports.UI.XtraReport) As String
            Dim obj As Object = rpt.GetCurrentColumnValue("missing_item")
            Dim txt As String = String.Empty
            If obj IsNot Nothing AndAlso obj IsNot System.DBNull.Value Then
                txt = String.Format("Missing Item:""{0}""", obj)
            End If
            Return txt
        End Function

        Private Function getFirstPaymentDate(ByVal rpt As DevExpress.XtraReports.UI.XtraReport) As String
            Dim obj As Object = rpt.GetCurrentColumnValue("first_payment_date")
            Dim txt As String = String.Empty
            If obj IsNot Nothing AndAlso obj IsNot System.DBNull.Value Then
                txt = String.Format("First Payment:{0:d}", obj)
            End If
            Return txt
        End Function

        Private Function getGoodThruDate(ByVal rpt As DevExpress.XtraReports.UI.XtraReport) As String
            Dim obj As Object = rpt.GetCurrentColumnValue("good_thru_date")
            Dim txt As String = String.Empty
            If obj IsNot Nothing AndAlso obj IsNot System.DBNull.Value Then
                txt = String.Format("Good Thru:{0:d}", obj)
            End If
            Return txt
        End Function

        Private Function getInEligibleReason(ByVal rpt As DevExpress.XtraReports.UI.XtraReport) As String
            Dim ErrorCode As String = DebtPlus.Utils.Nulls.DStr(rpt.GetCurrentColumnValue("ineligible_reason")).Trim
            Dim ErrorString As String = DebtPlus.Utils.Nulls.DStr(rpt.GetCurrentColumnValue("ineligible_reason_description")).Trim

            If ErrorCode <> String.Empty Then
                If ErrorString <> String.Empty Then
                    Return String.Format("Ineligible Reason:[{0}] ""{1}""", ErrorCode, ErrorString)
                End If
                Return String.Format("Ineligible Reason:[{0}]", ErrorCode)
            End If

            Return String.Empty
        End Function

        Private Function getThirdPartyDetail(ByVal rpt As DevExpress.XtraReports.UI.XtraReport) As String
            Dim ErrorCode As String = DebtPlus.Utils.Nulls.DStr(rpt.GetCurrentColumnValue("third_party_detail")).Trim
            Dim ErrorString As String = DebtPlus.Utils.Nulls.DStr(rpt.GetCurrentColumnValue("third_party_detail_description")).Trim

            If ErrorCode <> String.Empty Then
                If ErrorString <> String.Empty Then
                    Return String.Format("Third Party Detail:[{0}] ""{1}""", ErrorCode, ErrorString)
                End If
                Return String.Format("Third Party Detail:[{0}]", ErrorCode)
            End If

            Return String.Empty
        End Function

        Private Function getThirdPartyContact(ByVal rpt As DevExpress.XtraReports.UI.XtraReport) As String
            Dim txt As String = DebtPlus.Utils.Nulls.DStr(rpt.GetCurrentColumnValue("third_party_contact")).Trim

            If txt <> String.Empty Then
                Return String.Format("Contact info:""{0}""", txt)
            End If

            Return String.Empty
        End Function

        Private Function getInternalProgramEndsDate(ByVal rpt As DevExpress.XtraReports.UI.XtraReport) As String
            Dim obj As Object = rpt.GetCurrentColumnValue("internal_program_ends_date")
            Dim txt As String = String.Empty
            If obj IsNot Nothing AndAlso obj IsNot System.DBNull.Value Then
                txt = String.Format("Program Ends:{0:d}", obj)
            End If
            Return txt
        End Function

        Private Function format_AO(ByVal rpt As DevExpress.XtraReports.UI.XtraReport) As String
            Return getResubmitDate(rpt)
        End Function

        Private Function format_FA(ByVal rpt As DevExpress.XtraReports.UI.XtraReport) As String
            Dim sb As New System.Text.StringBuilder
            Dim txt As String = getCycleMonthsRemaining(rpt)
            If txt <> String.Empty Then
                sb.Append(", ")
                sb.Append(txt)
            End If

            txt = getForgivenPct(rpt)
            If txt <> String.Empty Then
                sb.Append(", ")
                sb.Append(txt)
            End If

            If sb.Length > 0 Then sb.Remove(0, 2)
            Return sb.ToString
        End Function

        Private Function format_IP(ByVal rpt As DevExpress.XtraReports.UI.XtraReport) As String
            Dim sb As New System.Text.StringBuilder
            Dim txt As String = getFirstPaymentDate(rpt)
            If txt <> String.Empty Then
                sb.Append(", ")
                sb.Append(txt)
            End If

            txt = getGoodThruDate(rpt)
            If txt <> String.Empty Then
                sb.Append(", ")
                sb.Append(txt)
            End If

            If sb.Length > 0 Then sb.Remove(0, 2)
            Return sb.ToString
        End Function

        Private Function format_IE(ByVal rpt As DevExpress.XtraReports.UI.XtraReport) As String
            Return getInEligibleReason(rpt)
        End Function

        Private Function format_MI(ByVal rpt As DevExpress.XtraReports.UI.XtraReport) As String
            Return getMissingItem(rpt)
        End Function

        Private Function format_PI(ByVal rpt As DevExpress.XtraReports.UI.XtraReport) As String
            Dim sb As New System.Text.StringBuilder
            Dim txt As String = getInternalProgramEndsDate(rpt)
            If txt <> String.Empty Then
                sb.Append(", ")
                sb.Append(txt)
            End If

            txt = getInterestRate(rpt)
            If txt <> String.Empty Then
                sb.Append(", ")
                sb.Append(txt)
            End If

            txt = getGoodThruDate(rpt)
            If txt <> String.Empty Then
                sb.Append(", ")
                sb.Append(txt)
            End If

            If sb.Length > 0 Then sb.Remove(0, 2)
            Return sb.ToString
        End Function

        Private Function format_PO(ByVal rpt As DevExpress.XtraReports.UI.XtraReport) As String
            Return getResubmitDate(rpt)
        End Function

        Private Function format_PT(ByVal rpt As DevExpress.XtraReports.UI.XtraReport) As String
            Dim sb As New System.Text.StringBuilder
            Dim txt As String = getFirstPaymentDate(rpt)
            If txt <> String.Empty Then
                sb.Append(", ")
                sb.Append(txt)
            End If

            txt = getGoodThruDate(rpt)
            If txt <> String.Empty Then
                sb.Append(", ")
                sb.Append(txt)
            End If

            If sb.Length > 0 Then sb.Remove(0, 2)
            Return sb.ToString
        End Function

        Private Function format_RA(ByVal rpt As DevExpress.XtraReports.UI.XtraReport) As String
            Return getResubmitDate(rpt)
        End Function

        Private Function format_TP(ByVal rpt As DevExpress.XtraReports.UI.XtraReport) As String
            Dim sb As New System.Text.StringBuilder
            Dim txt As String = getThirdPartyDetail(rpt)
            If txt <> String.Empty Then
                sb.Append(", ")
                sb.Append(txt)
            End If

            txt = getThirdPartyContact(rpt)
            If txt <> String.Empty Then
                sb.Append(", ")
                sb.Append(txt)
            End If

            If sb.Length > 0 Then sb.Remove(0, 2)
            Return sb.ToString
        End Function
    End Class
End Namespace
