#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}
#End Region

Option Compare Binary
Option Explicit On
Option Strict On

Namespace RPPS.Responses

    Friend Class RequestParametersForm
        Inherits DebtPlus.Reports.Template.Forms.ReportParametersForm

        Public Sub New()
            MyBase.New()
            InitializeComponent()
            AddHandler Me.Load, AddressOf RequestParametersForm_Load
        End Sub

        ''' <summary>
        ''' Deposit batch ID
        ''' </summary>
        Friend ReadOnly Property Parameter_BatchID() As System.Int32
            Get
                Dim BatchID As System.Int32 = -1
                If CheckEdit_lookup.Checked Then
                    If LookUpEdit_Batch.EditValue IsNot Nothing AndAlso LookUpEdit_Batch.EditValue IsNot System.DBNull.Value Then
                        BatchID = Convert.ToInt32(LookUpEdit_Batch.EditValue)
                    End If
                Else
                    If Int32.TryParse(Convert.ToString(TextEdit_rpps_file.EditValue), BatchID) AndAlso BatchID <= 0 Then BatchID = -1
                End If

                Return BatchID
            End Get
        End Property

#Region " Windows Form Designer generated code "

        'Form overrides dispose to clean up the component list.
        Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
            If disposing Then
                If components IsNot Nothing Then
                    components.Dispose()
                End If
            End If
            MyBase.Dispose(disposing)
        End Sub

        'Required by the Windows Form Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Windows Form Designer
        'It can be modified using the Windows Form Designer.  
        'Do not modify it using the code editor.
        Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
        Friend WithEvents Label1 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents TextEdit_rpps_file As DevExpress.XtraEditors.TextEdit
        Friend WithEvents CheckEdit_text As DevExpress.XtraEditors.CheckEdit
        Friend WithEvents CheckEdit_lookup As DevExpress.XtraEditors.CheckEdit
        Friend WithEvents LookUpEdit_Batch As DevExpress.XtraEditors.LookUpEdit
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
            Me.GroupBox1 = New System.Windows.Forms.GroupBox
            Me.CheckEdit_text = New DevExpress.XtraEditors.CheckEdit
            Me.CheckEdit_lookup = New DevExpress.XtraEditors.CheckEdit
            Me.Label1 = New DevExpress.XtraEditors.LabelControl
            Me.TextEdit_rpps_file = New DevExpress.XtraEditors.TextEdit
            Me.LookUpEdit_Batch = New DevExpress.XtraEditors.LookUpEdit
            CType(Me.DxErrorProvider1, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.GroupBox1.SuspendLayout()
            CType(Me.CheckEdit_text.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.CheckEdit_lookup.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.TextEdit_rpps_file.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LookUpEdit_Batch.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.SuspendLayout()
            '
            'ButtonOK
            '
            Me.ButtonOK.Location = New System.Drawing.Point(248, 18)
            Me.ButtonOK.Size = New System.Drawing.Size(80, 28)
            Me.ButtonOK.TabIndex = 3
            '
            'ButtonCancel
            '
            Me.ButtonCancel.Location = New System.Drawing.Point(248, 56)
            Me.ButtonCancel.Size = New System.Drawing.Size(80, 28)
            Me.ButtonCancel.TabIndex = 4
            '
            'GroupBox1
            '
            Me.GroupBox1.Controls.Add(Me.CheckEdit_text)
            Me.GroupBox1.Controls.Add(Me.CheckEdit_lookup)
            Me.GroupBox1.Controls.Add(Me.Label1)
            Me.GroupBox1.Controls.Add(Me.TextEdit_rpps_file)
            Me.GroupBox1.Controls.Add(Me.LookUpEdit_Batch)
            Me.GroupBox1.Location = New System.Drawing.Point(8, 9)
            Me.GroupBox1.Name = "GroupBox1"
            Me.GroupBox1.Size = New System.Drawing.Size(232, 112)
            Me.ToolTipController1.SetSuperTip(Me.GroupBox1, Nothing)
            Me.GroupBox1.TabIndex = 0
            Me.GroupBox1.TabStop = False
            Me.GroupBox1.Text = " RPPS Response Batch ID "
            '
            'CheckEdit_text
            '
            Me.CheckEdit_text.Location = New System.Drawing.Point(6, 78)
            Me.CheckEdit_text.Name = "CheckEdit_text"
            Me.CheckEdit_text.Properties.Caption = ""
            Me.CheckEdit_text.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
            Me.CheckEdit_text.Properties.RadioGroupIndex = 0
            Me.CheckEdit_text.Size = New System.Drawing.Size(20, 19)
            Me.CheckEdit_text.TabIndex = 7
            Me.CheckEdit_text.TabStop = False
            '
            'CheckEdit_lookup
            '
            Me.CheckEdit_lookup.EditValue = True
            Me.CheckEdit_lookup.Location = New System.Drawing.Point(6, 27)
            Me.CheckEdit_lookup.Name = "CheckEdit_lookup"
            Me.CheckEdit_lookup.Properties.Caption = ""
            Me.CheckEdit_lookup.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
            Me.CheckEdit_lookup.Properties.RadioGroupIndex = 0
            Me.CheckEdit_lookup.Size = New System.Drawing.Size(20, 19)
            Me.CheckEdit_lookup.TabIndex = 6
            '
            'Label1
            '
            Me.Label1.Appearance.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.Label1.Appearance.Options.UseFont = True
            Me.Label1.Location = New System.Drawing.Point(32, 54)
            Me.Label1.Name = "Label1"
            Me.Label1.Size = New System.Drawing.Size(122, 13)
            Me.Label1.TabIndex = 2
            Me.Label1.Text = "Or enter a specific ID"
            '
            'TextEdit_rpps_file
            '
            Me.TextEdit_rpps_file.EditValue = ""
            Me.TextEdit_rpps_file.Location = New System.Drawing.Point(32, 78)
            Me.TextEdit_rpps_file.Name = "TextEdit_rpps_file"
            Me.TextEdit_rpps_file.Properties.Appearance.Options.UseTextOptions = True
            Me.TextEdit_rpps_file.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.TextEdit_rpps_file.Properties.AppearanceDisabled.Options.UseTextOptions = True
            Me.TextEdit_rpps_file.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.TextEdit_rpps_file.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
            Me.TextEdit_rpps_file.Properties.DisplayFormat.FormatString = "f0"
            Me.TextEdit_rpps_file.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.TextEdit_rpps_file.Properties.EditFormat.FormatString = "f0"
            Me.TextEdit_rpps_file.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.TextEdit_rpps_file.Properties.ValidateOnEnterKey = True
            Me.TextEdit_rpps_file.Size = New System.Drawing.Size(64, 20)
            Me.TextEdit_rpps_file.TabIndex = 4
            Me.TextEdit_rpps_file.ToolTip = "Enter a specific disbursement if it is not included in the above list"
            Me.TextEdit_rpps_file.ToolTipController = Me.ToolTipController1
            '
            'LookUpEdit_Batch
            '
            Me.LookUpEdit_Batch.Location = New System.Drawing.Point(32, 26)
            Me.LookUpEdit_Batch.Name = "LookUpEdit_Batch"
            Me.LookUpEdit_Batch.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.LookUpEdit_Batch.Properties.Columns.AddRange(New DevExpress.XtraEditors.Controls.LookUpColumnInfo() {New DevExpress.XtraEditors.Controls.LookUpColumnInfo("rpps_response_file", "ID", 5, DevExpress.Utils.FormatType.Numeric, "f0", True, DevExpress.Utils.HorzAlignment.Far, DevExpress.Data.ColumnSortOrder.None), New DevExpress.XtraEditors.Controls.LookUpColumnInfo("date_created", "Created", 15, DevExpress.Utils.FormatType.DateTime, "g", True, DevExpress.Utils.HorzAlignment.Far, DevExpress.Data.ColumnSortOrder.None), New DevExpress.XtraEditors.Controls.LookUpColumnInfo("created_by", "By", 10), New DevExpress.XtraEditors.Controls.LookUpColumnInfo("label", "Label", 20, DevExpress.Utils.FormatType.None, "", True, DevExpress.Utils.HorzAlignment.[Default], DevExpress.Data.ColumnSortOrder.None), New DevExpress.XtraEditors.Controls.LookUpColumnInfo("date_posted", "Posted", 20, DevExpress.Utils.FormatType.DateTime, "d", True, DevExpress.Utils.HorzAlignment.[Default], DevExpress.Data.ColumnSortOrder.None)})
            Me.LookUpEdit_Batch.Properties.DisplayMember = "label"
            Me.LookUpEdit_Batch.Properties.NullText = ""
            Me.LookUpEdit_Batch.Properties.PopupWidth = 500
            Me.LookUpEdit_Batch.Properties.ShowFooter = False
            Me.LookUpEdit_Batch.Properties.ValueMember = "rpps_response_file"
            Me.LookUpEdit_Batch.Size = New System.Drawing.Size(184, 20)
            Me.LookUpEdit_Batch.TabIndex = 1
            Me.LookUpEdit_Batch.ToolTip = "Choose a disbursement from the list if desired"
            Me.LookUpEdit_Batch.ToolTipController = Me.ToolTipController1
            Me.LookUpEdit_Batch.Properties.SortColumnIndex = 1
            '
            'RequestParametersForm
            '
            Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
            Me.ClientSize = New System.Drawing.Size(336, 131)
            Me.Controls.Add(Me.GroupBox1)
            Me.Name = "RequestParametersForm"
            Me.ToolTipController1.SetSuperTip(Me, Nothing)
            Me.Text = "RPPS Batch Report Parameters"
            Me.Controls.SetChildIndex(Me.ButtonOK, 0)
            Me.Controls.SetChildIndex(Me.ButtonCancel, 0)
            Me.Controls.SetChildIndex(Me.GroupBox1, 0)
            CType(Me.DxErrorProvider1, System.ComponentModel.ISupportInitialize).EndInit()
            Me.GroupBox1.ResumeLayout(False)
            Me.GroupBox1.PerformLayout()
            CType(Me.CheckEdit_text.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.CheckEdit_lookup.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.TextEdit_rpps_file.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LookUpEdit_Batch.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            Me.ResumeLayout(False)

        End Sub

#End Region

        Private Sub RequestParametersForm_Load(ByVal sender As System.Object, ByVal e As System.EventArgs)

            With CheckEdit_text
                .Checked = False
                AddHandler .EditValueChanged, AddressOf CheckChanged
            End With

            With CheckEdit_lookup
                .Checked = True
                AddHandler .EditValueChanged, AddressOf CheckChanged
            End With

            With TextEdit_rpps_file
                .Enabled = False
                .EditValue = Nothing
                AddHandler .EditValueChanged, AddressOf FormChanged
            End With

            ' Load the deposit registers
            With LookUpEdit_Batch
                .Enabled = True
                Dim vue As New System.Data.DataView(rpps_files, String.Empty, "date_created desc", DataViewRowState.CurrentRows)
                .Properties.DataSource = vue
                .EditValue = Nothing
                If vue.Count > 0 Then
                    TextEdit_rpps_file.EditValue = vue(0)("rpps_response_file")
                    .EditValue = vue(0)("rpps_response_file")
                End If

                AddHandler .EditValueChanged, AddressOf FormChanged
                AddHandler .EditValueChanged, AddressOf CopyLabel
            End With

            ' Enable or disable the OK button
            ButtonOK.Enabled = Not HasErrors()
        End Sub

        Private Sub CopyLabel(ByVal sender As Object, ByVal e As System.EventArgs)
            If LookUpEdit_Batch.EditValue Is Nothing OrElse LookUpEdit_Batch.EditValue Is System.DBNull.Value Then
                TextEdit_rpps_file.Text = String.Empty
            Else
                TextEdit_rpps_file.Text = Convert.ToInt32(LookUpEdit_Batch.EditValue).ToString()
            End If
        End Sub

        Private Sub CheckChanged(ByVal sender As Object, ByVal e As System.EventArgs)
            If CheckEdit_text.Checked Then
                TextEdit_rpps_file.Enabled = True
                LookUpEdit_Batch.Enabled = False
                TextEdit_rpps_file.EditValue = LookUpEdit_Batch.EditValue
            Else
                TextEdit_rpps_file.Enabled = False
                LookUpEdit_Batch.Enabled = True
                Dim Batch As System.Int32
                If Int32.TryParse(TextEdit_rpps_file.Text.Trim(), Batch) Then LookUpEdit_Batch.EditValue = Batch
            End If

            ButtonOK.Enabled = Not HasErrors()
        End Sub

        Protected Overrides Function HasErrors() As Boolean
            Dim Answer As Boolean = True
            Dim BatchID As System.Int32
            Do
                If CheckEdit_lookup.Checked AndAlso (LookUpEdit_Batch.EditValue Is Nothing OrElse LookUpEdit_Batch.EditValue Is System.DBNull.Value) Then Exit Do
                If CheckEdit_text.Checked AndAlso (TextEdit_rpps_file.EditValue Is Nothing OrElse TextEdit_rpps_file.EditValue Is System.DBNull.Value OrElse Not Int32.TryParse(Convert.ToString(TextEdit_rpps_file.EditValue), BatchID) OrElse BatchID <= 0) Then Exit Do

                Answer = False
                Exit Do
            Loop
            Return Answer
        End Function

        Private Function rpps_files() As System.Data.DataTable
            Dim ds As New System.Data.DataSet("ds")

            ' Retrieve the database connection logic
            Dim cmd As SqlClient.SqlCommand = New System.Data.SqlClient.SqlCommand()
            With cmd
                .Connection = New System.Data.SqlClient.SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
                .CommandText = "SELECT TOP 50 rpps_response_file, date_posted, filename, label, file_date, date_created, created_by FROM rpps_response_files ORDER BY rpps_response_file DESC"
                .CommandType = CommandType.Text
                .CommandTimeout = 0
            End With

            ' Retrieve the disbursement table from the system
            Dim da As New SqlClient.SqlDataAdapter(cmd)
            da.Fill(ds, "rpps_files")
            Return ds.Tables(0)
        End Function
    End Class
End Namespace
