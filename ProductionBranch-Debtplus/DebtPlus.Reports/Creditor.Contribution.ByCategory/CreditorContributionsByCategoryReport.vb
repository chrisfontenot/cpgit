#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}

#End Region

Option Compare Binary
Option Explicit On
Option Strict On

Imports System.ComponentModel
Imports DebtPlus.Reports.Template

Imports System.Drawing.Printing
Imports System.Data.SqlClient
Imports DevExpress.XtraReports.UI

Namespace Creditor.Contribution.ByCategory
    Public Class CreditorContributionByCategoryReport
        Inherits TemplateXtraReportClass

        Public Sub New()
            MyBase.New()
            InitializeComponent()

            BindFields()

            AddHandler BeforePrint, AddressOf Report_BeforePrint
            AddHandler XrLabel_mtd_deduct_pct.BeforePrint, AddressOf Percent_BeforePrint
            AddHandler XrLabel_ytd_deduct_pct.BeforePrint, AddressOf Percent_BeforePrint
            AddHandler XrLabel_mtd_bill_pct.BeforePrint, AddressOf Percent_BeforePrint
            AddHandler XrLabel_ytd_bill_pct.BeforePrint, AddressOf Percent_BeforePrint
        End Sub

        Public Sub New(ByVal Container As IContainer)
            MyClass.New()
            Container.Add(Me)
        End Sub

        ''' <summary>
        ''' Bind the fields to the report
        ''' </summary>
        Protected Overridable Sub BindFields()
            XrLabel_creditor_type.DataBindings.Add("Text", Nothing, "creditor_type")
            XrLabel_description.DataBindings.Add("Text", Nothing, "description")
            XrLabel_mtd_deduct_dollars.DataBindings.Add("Text", Nothing, "mtd_deduct_dollars", "{0:c}")
            XrLabel_mtd_deduct_pct.DataBindings.Add("Text", Nothing, "mtd_deduct_pct")
            XrLabel_total_mtd_deduct.DataBindings.Add("Text", Nothing, "mtd_deduct_dollars")
            XrLabel_ytd_deduct_dollars.DataBindings.Add("Text", Nothing, "ytd_deduct_dollars", "{0:c}")
            XrLabel_ytd_deduct_pct.DataBindings.Add("Text", Nothing, "ytd_deduct_pct")
            XrLabel_total_ytd_deduct.DataBindings.Add("Text", Nothing, "ytd_deduct_dollars")
            XrLabel_mtd_bill_dollars.DataBindings.Add("Text", Nothing, "mtd_bill_dollars", "{0:c}")
            XrLabel_mtd_bill_pct.DataBindings.Add("Text", Nothing, "mtd_bill_pct")
            XrLabel_total_mtd_bill.DataBindings.Add("Text", Nothing, "mtd_bill_dollars")
            XrLabel_ytd_bill_dollars.DataBindings.Add("Text", Nothing, "ytd_bill_dollars", "{0:c}")
            XrLabel_ytd_bill_pct.DataBindings.Add("Text", Nothing, "ytd_bill_pct")
            XrLabel_total_ytd_bill.DataBindings.Add("Text", Nothing, "ytd_bill_dollars")
        End Sub

        ''' <summary>
        ''' Title printed on the top of the page
        ''' </summary>
        Public Overrides ReadOnly Property ReportTitle() As String
            Get
                Return "Creditor Contributions By Category"
            End Get
        End Property

        Protected ds As New DataSet("ds")

        ''' <summary>
        ''' Configure the report before it is printed
        ''' </summary>
        Protected Overridable Sub Report_BeforePrint(ByVal sender As Object, ByVal e As PrintEventArgs)
            Dim tbl As DataTable = ds.Tables("rpt_fairshare_by_category")

            If tbl Is Nothing Then
                Using cmd As SqlCommand = New System.Data.SqlClient.SqlCommand()
                    With cmd
                        .Connection = New System.Data.SqlClient.SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
                        .CommandText = "rpt_fairshare_by_category"
                        .CommandType = CommandType.StoredProcedure
                        .CommandTimeout = 0
                    End With

                    Using da As New SqlDataAdapter(cmd)
                        da.Fill(ds, "rpt_fairshare_by_category")
                    End Using
                End Using
                tbl = ds.Tables("rpt_fairshare_by_category")
            End If

            DataSource = New DataView(tbl, String.Empty, "creditor_type, description", DataViewRowState.CurrentRows)
        End Sub


        ''' <summary>
        ''' Reduce the percentage figures by 100 so that they may be formatted
        ''' </summary>
        Protected Sub Percent_BeforePrint(ByVal sender As Object, ByVal e As PrintEventArgs)
            With CType(sender, XRLabel)
                Dim FieldName As String = Convert.ToString(.DataBindings.Item("Text").DataMember)
                Dim Value As Double = DebtPlus.Utils.Nulls.DDbl(GetCurrentColumnValue(FieldName))
                .Text = String.Format("{0:p}", Value/100.0#)   ' format the number as a percent now
            End With
        End Sub
    End Class
End Namespace
