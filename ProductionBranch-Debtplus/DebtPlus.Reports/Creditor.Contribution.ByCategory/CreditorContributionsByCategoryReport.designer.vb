Namespace Creditor.Contribution.ByCategory
    Partial Class CreditorContributionByCategoryReport

        'Component overrides dispose to clean up the component list.
        Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
            If disposing Then
                If components IsNot Nothing Then
                    components.Dispose()
                End If
                ds.Dispose()
            End If
            MyBase.Dispose(disposing)
        End Sub

        'Required by the Designer
        Private components As System.ComponentModel.IContainer

        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
            components = New System.ComponentModel.Container()
            Dim XrSummary1 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary
            Dim XrSummary2 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary
            Dim XrSummary3 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary
            Dim XrSummary4 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary
            Me.XrLabel_ytd_bill_pct = New DevExpress.XtraReports.UI.XRLabel
            Me.XrPanel1 = New DevExpress.XtraReports.UI.XRPanel
            Me.XrLabel6 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel5 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel2 = New DevExpress.XtraReports.UI.XRLabel
            Me.ReportFooter = New DevExpress.XtraReports.UI.ReportFooterBand
            Me.XrLine1 = New DevExpress.XtraReports.UI.XRLine
            Me.XrLabel1 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_total_mtd_bill = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_total_ytd_bill = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_total_mtd_deduct = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_total_ytd_deduct = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_ytd_bill_dollars = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_creditor_type = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_description = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel16 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel8 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel17 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel4 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_mtd_bill_dollars = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_mtd_bill_pct = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_ytd_deduct_dollars = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_ytd_deduct_pct = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_mtd_deduct_dollars = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_mtd_deduct_pct = New DevExpress.XtraReports.UI.XRLabel
            CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
            '
            'Detail
            '
            Me.Detail.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel_mtd_deduct_pct, Me.XrLabel_mtd_deduct_dollars, Me.XrLabel_ytd_deduct_pct, Me.XrLabel_ytd_deduct_dollars, Me.XrLabel_mtd_bill_pct, Me.XrLabel_mtd_bill_dollars, Me.XrLabel_description, Me.XrLabel_creditor_type, Me.XrLabel_ytd_bill_dollars, Me.XrLabel_ytd_bill_pct})
            Me.Detail.Height = 15
            '
            'PageHeader
            '
            Me.PageHeader.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel4, Me.XrLabel16, Me.XrPanel1})
            Me.PageHeader.Height = 158
            '
            'XrPageInfo_PageNumber
            '
            Me.XrPageInfo_PageNumber.Location = New System.Drawing.Point(917, 8)
            '
            'XrPanel_AgencyAddress
            '
            Me.XrPanel_AgencyAddress.Location = New System.Drawing.Point(742, 0)
            '
            'XrLabel_ytd_bill_pct
            '
            Me.XrLabel_ytd_bill_pct.CanGrow = False
            Me.XrLabel_ytd_bill_pct.Font = New System.Drawing.Font("Times New Roman", 9.75!)
            Me.XrLabel_ytd_bill_pct.Location = New System.Drawing.Point(950, 0)
            Me.XrLabel_ytd_bill_pct.Name = "XrLabel_ytd_bill_pct"
            Me.XrLabel_ytd_bill_pct.Size = New System.Drawing.Size(50, 15)
            Me.XrLabel_ytd_bill_pct.StylePriority.UseFont = False
            Me.XrLabel_ytd_bill_pct.StylePriority.UseTextAlignment = False
            Me.XrLabel_ytd_bill_pct.Text = "0.00%"
            Me.XrLabel_ytd_bill_pct.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
            Me.XrLabel_ytd_bill_pct.WordWrap = False
            '
            'XrPanel1
            '
            Me.XrPanel1.BackColor = System.Drawing.Color.Teal
            Me.XrPanel1.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel17, Me.XrLabel8, Me.XrLabel2, Me.XrLabel5, Me.XrLabel6})
            Me.XrPanel1.Location = New System.Drawing.Point(0, 134)
            Me.XrPanel1.Name = "XrPanel1"
            Me.XrPanel1.Size = New System.Drawing.Size(1050, 17)
            Me.XrPanel1.StyleName = "XrControlStyle_HeaderPannel"
            '
            'XrLabel6
            '
            Me.XrLabel6.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel6.ForeColor = System.Drawing.Color.White
            Me.XrLabel6.Location = New System.Drawing.Point(883, 0)
            Me.XrLabel6.Name = "XrLabel6"
            Me.XrLabel6.Size = New System.Drawing.Size(116, 15)
            Me.XrLabel6.StylePriority.UseTextAlignment = False
            Me.XrLabel6.Text = "Y-T-D"
            Me.XrLabel6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter
            '
            'XrLabel5
            '
            Me.XrLabel5.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel5.ForeColor = System.Drawing.Color.White
            Me.XrLabel5.Location = New System.Drawing.Point(710, 1)
            Me.XrLabel5.Name = "XrLabel5"
            Me.XrLabel5.Size = New System.Drawing.Size(110, 15)
            Me.XrLabel5.StylePriority.UseTextAlignment = False
            Me.XrLabel5.Text = "M-T-D"
            Me.XrLabel5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter
            '
            'XrLabel2
            '
            Me.XrLabel2.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel2.ForeColor = System.Drawing.Color.White
            Me.XrLabel2.Location = New System.Drawing.Point(8, 0)
            Me.XrLabel2.Name = "XrLabel2"
            Me.XrLabel2.Size = New System.Drawing.Size(250, 15)
            Me.XrLabel2.StylePriority.UseTextAlignment = False
            Me.XrLabel2.Text = "CATEGORY"
            Me.XrLabel2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
            '
            'ReportFooter
            '
            Me.ReportFooter.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLine1, Me.XrLabel1, Me.XrLabel_total_mtd_bill, Me.XrLabel_total_ytd_bill, Me.XrLabel_total_mtd_deduct, Me.XrLabel_total_ytd_deduct})
            Me.ReportFooter.Height = 27
            Me.ReportFooter.Name = "ReportFooter"
            Me.ReportFooter.StyleName = "XrControlStyle_Totals"
            '
            'XrLine1
            '
            Me.XrLine1.BorderWidth = 1
            Me.XrLine1.ForeColor = System.Drawing.Color.Teal
            Me.XrLine1.LineWidth = 3
            Me.XrLine1.Location = New System.Drawing.Point(0, 0)
            Me.XrLine1.Name = "XrLine1"
            Me.XrLine1.Size = New System.Drawing.Size(1050, 8)
            Me.XrLine1.StylePriority.UseBorderWidth = False
            Me.XrLine1.StylePriority.UseForeColor = False
            '
            'XrLabel1
            '
            Me.XrLabel1.Location = New System.Drawing.Point(0, 10)
            Me.XrLabel1.Name = "XrLabel1"
            Me.XrLabel1.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel1.Size = New System.Drawing.Size(100, 17)
            Me.XrLabel1.StylePriority.UseTextAlignment = False
            Me.XrLabel1.Text = "TOTALS"
            Me.XrLabel1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
            '
            'XrLabel_total_mtd_bill
            '
            Me.XrLabel_total_mtd_bill.Location = New System.Drawing.Point(617, 10)
            Me.XrLabel_total_mtd_bill.Name = "XrLabel_total_mtd_bill"
            Me.XrLabel_total_mtd_bill.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_total_mtd_bill.Size = New System.Drawing.Size(139, 15)
            XrSummary1.FormatString = "{0:c}"
            XrSummary1.Running = DevExpress.XtraReports.UI.SummaryRunning.Report
            Me.XrLabel_total_mtd_bill.Summary = XrSummary1
            Me.XrLabel_total_mtd_bill.Text = "$0.00"
            Me.XrLabel_total_mtd_bill.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
            '
            'XrLabel_total_ytd_bill
            '
            Me.XrLabel_total_ytd_bill.Location = New System.Drawing.Point(758, 10)
            Me.XrLabel_total_ytd_bill.Name = "XrLabel_total_ytd_bill"
            Me.XrLabel_total_ytd_bill.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_total_ytd_bill.Size = New System.Drawing.Size(182, 15)
            XrSummary2.FormatString = "{0:c}"
            XrSummary2.Running = DevExpress.XtraReports.UI.SummaryRunning.Report
            Me.XrLabel_total_ytd_bill.Summary = XrSummary2
            Me.XrLabel_total_ytd_bill.Text = "$0.00"
            Me.XrLabel_total_ytd_bill.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
            '
            'XrLabel_total_mtd_deduct
            '
            Me.XrLabel_total_mtd_deduct.Location = New System.Drawing.Point(267, 10)
            Me.XrLabel_total_mtd_deduct.Name = "XrLabel_total_mtd_deduct"
            Me.XrLabel_total_mtd_deduct.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_total_mtd_deduct.Size = New System.Drawing.Size(164, 15)
            XrSummary3.FormatString = "{0:c}"
            XrSummary3.Running = DevExpress.XtraReports.UI.SummaryRunning.Report
            Me.XrLabel_total_mtd_deduct.Summary = XrSummary3
            Me.XrLabel_total_mtd_deduct.Text = "$0.00"
            Me.XrLabel_total_mtd_deduct.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
            '
            'XrLabel_total_ytd_deduct
            '
            Me.XrLabel_total_ytd_deduct.Location = New System.Drawing.Point(433, 10)
            Me.XrLabel_total_ytd_deduct.Name = "XrLabel_total_ytd_deduct"
            Me.XrLabel_total_ytd_deduct.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_total_ytd_deduct.Size = New System.Drawing.Size(180, 15)
            XrSummary4.FormatString = "{0:c}"
            XrSummary4.Running = DevExpress.XtraReports.UI.SummaryRunning.Report
            Me.XrLabel_total_ytd_deduct.Summary = XrSummary4
            Me.XrLabel_total_ytd_deduct.Text = "$0.00"
            Me.XrLabel_total_ytd_deduct.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
            '
            'XrLabel_ytd_bill_dollars
            '
            Me.XrLabel_ytd_bill_dollars.CanGrow = False
            Me.XrLabel_ytd_bill_dollars.Font = New System.Drawing.Font("Times New Roman", 9.75!)
            Me.XrLabel_ytd_bill_dollars.Location = New System.Drawing.Point(825, 0)
            Me.XrLabel_ytd_bill_dollars.Name = "XrLabel_ytd_bill_dollars"
            Me.XrLabel_ytd_bill_dollars.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96.0!)
            Me.XrLabel_ytd_bill_dollars.Size = New System.Drawing.Size(116, 15)
            Me.XrLabel_ytd_bill_dollars.StylePriority.UseFont = False
            Me.XrLabel_ytd_bill_dollars.StylePriority.UseTextAlignment = False
            Me.XrLabel_ytd_bill_dollars.Text = "$0.00"
            Me.XrLabel_ytd_bill_dollars.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
            Me.XrLabel_ytd_bill_dollars.WordWrap = False
            '
            'XrLabel_creditor_type
            '
            Me.XrLabel_creditor_type.CanGrow = False
            Me.XrLabel_creditor_type.Location = New System.Drawing.Point(0, 0)
            Me.XrLabel_creditor_type.Name = "XrLabel_creditor_type"
            Me.XrLabel_creditor_type.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96.0!)
            Me.XrLabel_creditor_type.Size = New System.Drawing.Size(18, 15)
            Me.XrLabel_creditor_type.StylePriority.UseTextAlignment = False
            Me.XrLabel_creditor_type.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
            Me.XrLabel_creditor_type.WordWrap = False
            '
            'XrLabel_description
            '
            Me.XrLabel_description.CanGrow = False
            Me.XrLabel_description.Location = New System.Drawing.Point(25, 0)
            Me.XrLabel_description.Name = "XrLabel_description"
            Me.XrLabel_description.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96.0!)
            Me.XrLabel_description.Size = New System.Drawing.Size(241, 15)
            Me.XrLabel_description.StylePriority.UseTextAlignment = False
            Me.XrLabel_description.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
            Me.XrLabel_description.WordWrap = False
            '
            'XrLabel16
            '
            Me.XrLabel16.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel16.Location = New System.Drawing.Point(383, 117)
            Me.XrLabel16.Name = "XrLabel16"
            Me.XrLabel16.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel16.Size = New System.Drawing.Size(292, 17)
            Me.XrLabel16.StyleName = "XrControlStyle_Header"
            Me.XrLabel16.StylePriority.UseFont = False
            Me.XrLabel16.StylePriority.UseTextAlignment = False
            Me.XrLabel16.Text = "DEDUCTED CONTRIBUTIONS"
            Me.XrLabel16.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter
            '
            'XrLabel8
            '
            Me.XrLabel8.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel8.ForeColor = System.Drawing.Color.White
            Me.XrLabel8.Location = New System.Drawing.Point(558, 0)
            Me.XrLabel8.Name = "XrLabel8"
            Me.XrLabel8.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96.0!)
            Me.XrLabel8.Size = New System.Drawing.Size(116, 15)
            Me.XrLabel8.StylePriority.UseTextAlignment = False
            Me.XrLabel8.Text = "Y-T-D"
            Me.XrLabel8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter
            '
            'XrLabel17
            '
            Me.XrLabel17.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel17.ForeColor = System.Drawing.Color.White
            Me.XrLabel17.Location = New System.Drawing.Point(383, 0)
            Me.XrLabel17.Name = "XrLabel17"
            Me.XrLabel17.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96.0!)
            Me.XrLabel17.Size = New System.Drawing.Size(110, 15)
            Me.XrLabel17.StylePriority.UseTextAlignment = False
            Me.XrLabel17.Text = "M-T-D"
            Me.XrLabel17.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter
            '
            'XrLabel4
            '
            Me.XrLabel4.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel4.Location = New System.Drawing.Point(708, 117)
            Me.XrLabel4.Name = "XrLabel4"
            Me.XrLabel4.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel4.Size = New System.Drawing.Size(292, 17)
            Me.XrLabel4.StyleName = "XrControlStyle_Header"
            Me.XrLabel4.StylePriority.UseFont = False
            Me.XrLabel4.StylePriority.UseTextAlignment = False
            Me.XrLabel4.Text = "BILLED CONTRIBUTIONS"
            Me.XrLabel4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter
            '
            'XrLabel_mtd_bill_dollars
            '
            Me.XrLabel_mtd_bill_dollars.CanGrow = False
            Me.XrLabel_mtd_bill_dollars.Font = New System.Drawing.Font("Times New Roman", 9.75!)
            Me.XrLabel_mtd_bill_dollars.Location = New System.Drawing.Point(683, 0)
            Me.XrLabel_mtd_bill_dollars.Name = "XrLabel_mtd_bill_dollars"
            Me.XrLabel_mtd_bill_dollars.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_mtd_bill_dollars.Size = New System.Drawing.Size(72, 15)
            Me.XrLabel_mtd_bill_dollars.StylePriority.UseFont = False
            Me.XrLabel_mtd_bill_dollars.StylePriority.UseTextAlignment = False
            Me.XrLabel_mtd_bill_dollars.Text = "$0.00"
            Me.XrLabel_mtd_bill_dollars.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
            Me.XrLabel_mtd_bill_dollars.WordWrap = False
            '
            'XrLabel_mtd_bill_pct
            '
            Me.XrLabel_mtd_bill_pct.CanGrow = False
            Me.XrLabel_mtd_bill_pct.Font = New System.Drawing.Font("Times New Roman", 9.75!)
            Me.XrLabel_mtd_bill_pct.Location = New System.Drawing.Point(767, 0)
            Me.XrLabel_mtd_bill_pct.Name = "XrLabel_mtd_bill_pct"
            Me.XrLabel_mtd_bill_pct.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96.0!)
            Me.XrLabel_mtd_bill_pct.Size = New System.Drawing.Size(50, 15)
            Me.XrLabel_mtd_bill_pct.StylePriority.UseFont = False
            Me.XrLabel_mtd_bill_pct.StylePriority.UseTextAlignment = False
            Me.XrLabel_mtd_bill_pct.Text = "0.00%"
            Me.XrLabel_mtd_bill_pct.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
            Me.XrLabel_mtd_bill_pct.WordWrap = False
            '
            'XrLabel_ytd_deduct_dollars
            '
            Me.XrLabel_ytd_deduct_dollars.CanGrow = False
            Me.XrLabel_ytd_deduct_dollars.Font = New System.Drawing.Font("Times New Roman", 9.75!)
            Me.XrLabel_ytd_deduct_dollars.Location = New System.Drawing.Point(500, 0)
            Me.XrLabel_ytd_deduct_dollars.Name = "XrLabel_ytd_deduct_dollars"
            Me.XrLabel_ytd_deduct_dollars.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_ytd_deduct_dollars.Size = New System.Drawing.Size(114, 15)
            Me.XrLabel_ytd_deduct_dollars.StylePriority.UseFont = False
            Me.XrLabel_ytd_deduct_dollars.StylePriority.UseTextAlignment = False
            Me.XrLabel_ytd_deduct_dollars.Text = "$0.00"
            Me.XrLabel_ytd_deduct_dollars.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
            Me.XrLabel_ytd_deduct_dollars.WordWrap = False
            '
            'XrLabel_ytd_deduct_pct
            '
            Me.XrLabel_ytd_deduct_pct.CanGrow = False
            Me.XrLabel_ytd_deduct_pct.Font = New System.Drawing.Font("Times New Roman", 9.75!)
            Me.XrLabel_ytd_deduct_pct.Location = New System.Drawing.Point(625, 0)
            Me.XrLabel_ytd_deduct_pct.Name = "XrLabel_ytd_deduct_pct"
            Me.XrLabel_ytd_deduct_pct.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96.0!)
            Me.XrLabel_ytd_deduct_pct.Size = New System.Drawing.Size(50, 15)
            Me.XrLabel_ytd_deduct_pct.StylePriority.UseFont = False
            Me.XrLabel_ytd_deduct_pct.StylePriority.UseTextAlignment = False
            Me.XrLabel_ytd_deduct_pct.Text = "0.00%"
            Me.XrLabel_ytd_deduct_pct.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
            Me.XrLabel_ytd_deduct_pct.WordWrap = False
            '
            'XrLabel_mtd_deduct_dollars
            '
            Me.XrLabel_mtd_deduct_dollars.CanGrow = False
            Me.XrLabel_mtd_deduct_dollars.Font = New System.Drawing.Font("Times New Roman", 9.75!)
            Me.XrLabel_mtd_deduct_dollars.Location = New System.Drawing.Point(358, 0)
            Me.XrLabel_mtd_deduct_dollars.Name = "XrLabel_mtd_deduct_dollars"
            Me.XrLabel_mtd_deduct_dollars.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_mtd_deduct_dollars.Size = New System.Drawing.Size(72, 15)
            Me.XrLabel_mtd_deduct_dollars.StylePriority.UseFont = False
            Me.XrLabel_mtd_deduct_dollars.StylePriority.UseTextAlignment = False
            Me.XrLabel_mtd_deduct_dollars.Text = "$0.00"
            Me.XrLabel_mtd_deduct_dollars.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
            Me.XrLabel_mtd_deduct_dollars.WordWrap = False
            '
            'XrLabel_mtd_deduct_pct
            '
            Me.XrLabel_mtd_deduct_pct.CanGrow = False
            Me.XrLabel_mtd_deduct_pct.Font = New System.Drawing.Font("Times New Roman", 9.75!)
            Me.XrLabel_mtd_deduct_pct.Location = New System.Drawing.Point(442, 0)
            Me.XrLabel_mtd_deduct_pct.Name = "XrLabel_mtd_deduct_pct"
            Me.XrLabel_mtd_deduct_pct.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96.0!)
            Me.XrLabel_mtd_deduct_pct.Size = New System.Drawing.Size(50, 15)
            Me.XrLabel_mtd_deduct_pct.StylePriority.UseFont = False
            Me.XrLabel_mtd_deduct_pct.StylePriority.UseTextAlignment = False
            Me.XrLabel_mtd_deduct_pct.Text = "0.00%"
            Me.XrLabel_mtd_deduct_pct.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
            Me.XrLabel_mtd_deduct_pct.WordWrap = False
            '
            'CreditorContributionByCategoryReport
            '
            Me.Bands.AddRange(New DevExpress.XtraReports.UI.Band() {Me.ReportFooter})
            Me.Landscape = True
            Me.PageHeight = 850
            Me.PageWidth = 1100
            Me.Version = "8.3"
            CType(Me, System.ComponentModel.ISupportInitialize).EndInit()
        End Sub

        Protected Friend WithEvents XrLabel_ytd_bill_pct As DevExpress.XtraReports.UI.XRLabel
        Protected Friend WithEvents XrPanel1 As DevExpress.XtraReports.UI.XRPanel
        Protected Friend WithEvents XrLabel2 As DevExpress.XtraReports.UI.XRLabel
        Protected Friend WithEvents XrLabel5 As DevExpress.XtraReports.UI.XRLabel
        Protected Friend WithEvents XrLabel6 As DevExpress.XtraReports.UI.XRLabel
        Protected Friend WithEvents ReportFooter As DevExpress.XtraReports.UI.ReportFooterBand
        Protected Friend WithEvents XrLabel_total_mtd_bill As DevExpress.XtraReports.UI.XRLabel
        Protected Friend WithEvents XrLabel_total_ytd_bill As DevExpress.XtraReports.UI.XRLabel
        Protected Friend WithEvents XrLabel_total_mtd_deduct As DevExpress.XtraReports.UI.XRLabel
        Protected Friend WithEvents XrLabel_total_ytd_deduct As DevExpress.XtraReports.UI.XRLabel
        Protected Friend WithEvents XrLine1 As DevExpress.XtraReports.UI.XRLine
        Protected Friend WithEvents XrLabel1 As DevExpress.XtraReports.UI.XRLabel
        Protected Friend WithEvents XrLabel_description As DevExpress.XtraReports.UI.XRLabel
        Protected Friend WithEvents XrLabel_creditor_type As DevExpress.XtraReports.UI.XRLabel
        Protected Friend WithEvents XrLabel_ytd_bill_dollars As DevExpress.XtraReports.UI.XRLabel
        Protected Friend WithEvents XrLabel16 As DevExpress.XtraReports.UI.XRLabel
        Protected Friend WithEvents XrLabel4 As DevExpress.XtraReports.UI.XRLabel
        Protected Friend WithEvents XrLabel17 As DevExpress.XtraReports.UI.XRLabel
        Protected Friend WithEvents XrLabel8 As DevExpress.XtraReports.UI.XRLabel
        Protected Friend WithEvents XrLabel_mtd_deduct_pct As DevExpress.XtraReports.UI.XRLabel
        Protected Friend WithEvents XrLabel_mtd_deduct_dollars As DevExpress.XtraReports.UI.XRLabel
        Protected Friend WithEvents XrLabel_ytd_deduct_pct As DevExpress.XtraReports.UI.XRLabel
        Protected Friend WithEvents XrLabel_ytd_deduct_dollars As DevExpress.XtraReports.UI.XRLabel
        Protected Friend WithEvents XrLabel_mtd_bill_pct As DevExpress.XtraReports.UI.XRLabel
        Protected Friend WithEvents XrLabel_mtd_bill_dollars As DevExpress.XtraReports.UI.XRLabel
    End Class
End Namespace
