#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}

#End Region

Imports System.ComponentModel
Imports DebtPlus.Reports.Template

Imports System.Data.SqlClient
Imports System.Drawing.Printing
Imports DevExpress.XtraReports.UI

Namespace NFCC.Bankruptcy
    Public Class NFCCBankruptcyReport
        Inherits DatedTemplateXtraReportClass

        Public Sub New()
            MyBase.New()
            InitializeComponent()
            AddHandler Me.BeforePrint, AddressOf NFCCBankruptcyReport_BeforePrint
            AddHandler XrLabel_Delinquent_Mortgage_0.BeforePrint, AddressOf XrLabel_Delinquent_Mortgage_0_BeforePrint
            AddHandler XrLabel_Delinquent_Mortgage_1.BeforePrint, AddressOf XrLabel_Delinquent_Mortgage_0_BeforePrint
            AddHandler XrLabel_FICO_0.PrintOnPage, AddressOf XrLabel_FICO_0_PrintOnPage
            AddHandler XrLabel_FICO_1.PrintOnPage, AddressOf XrLabel_FICO_0_PrintOnPage
        End Sub

        Public Overrides ReadOnly Property ReportTitle() As String
            Get
                Return "NFCC Bankruptcy"
            End Get
        End Property

#Region " Component Designer generated code "

        'Component overrides dispose to clean up the component list.
        Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
            If disposing Then
                If components IsNot Nothing Then
                    components.Dispose()
                End If
            End If
            MyBase.Dispose(disposing)
        End Sub

        'Required by the Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Designer
        'It can be modified using the Designer.
        'Do not modify it using the code editor.
        Friend WithEvents XrLabel1 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel2 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel3 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel4 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel5 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel6 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel7 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel8 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel9 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel10 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_AvgIncome_0 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_AvgDebt_0 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_AvgCreditors_0 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_Delinquent_Mortgage_0 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_FICO_1 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_Fin_Problem1_0 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_Fin_Problem2_0 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_Fin_Problem3_0 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_Female_0 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_Male_0 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_FICO_0 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_Fin_Problem1_1 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_AvgIncome_1 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_AvgDebt_1 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_Male_1 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_Fin_Problem3_1 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_Fin_Problem2_1 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_AvgCreditors_1 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_Female_1 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_Delinquent_Mortgage_1 As DevExpress.XtraReports.UI.XRLabel

        <System.Diagnostics.DebuggerStepThrough()>
        Private Sub InitializeComponent()
            Me.XrLabel1 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel2 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel3 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel4 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel5 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel6 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel7 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel8 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel9 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel10 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_AvgIncome_0 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_AvgDebt_0 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_AvgCreditors_0 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_Delinquent_Mortgage_0 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_FICO_0 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_Fin_Problem1_0 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_Fin_Problem2_0 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_Fin_Problem3_0 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_Female_0 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_Male_0 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_FICO_1 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_Delinquent_Mortgage_1 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_Fin_Problem1_1 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_AvgIncome_1 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_AvgDebt_1 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_Male_1 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_Fin_Problem3_1 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_Fin_Problem2_1 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_AvgCreditors_1 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_Female_1 = New DevExpress.XtraReports.UI.XRLabel
            CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
            '
            'XrLine_Title
            '
            Me.XrLine_Title.ParentStyleUsing.UseForeColor = False
            '
            'XrLabel_Agency_Address3
            '
            Me.XrLabel_Agency_Address3.ParentStyleUsing.UseFont = False
            Me.XrLabel_Agency_Address3.ParentStyleUsing.UseForeColor = False
            '
            'XRLabel_Agency_Name
            '
            Me.XRLabel_Agency_Name.ParentStyleUsing.UseFont = False
            Me.XRLabel_Agency_Name.ParentStyleUsing.UseForeColor = False
            '
            'XrLabel_Agency_Address1
            '
            Me.XrLabel_Agency_Address1.ParentStyleUsing.UseFont = False
            Me.XrLabel_Agency_Address1.ParentStyleUsing.UseForeColor = False
            '
            'XrLabel_Agency_Phone
            '
            Me.XrLabel_Agency_Phone.ParentStyleUsing.UseFont = False
            Me.XrLabel_Agency_Phone.ParentStyleUsing.UseForeColor = False
            '
            'XrLabel_Agency_Address2
            '
            Me.XrLabel_Agency_Address2.ParentStyleUsing.UseFont = False
            Me.XrLabel_Agency_Address2.ParentStyleUsing.UseForeColor = False
            '
            'XrPageInfo1
            '
            Me.XrPageInfo1.ParentStyleUsing.UseFont = False
            Me.XrPageInfo1.ParentStyleUsing.UseForeColor = False
            '
            'XrPageInfo_PageNumber
            '
            Me.XrPageInfo_PageNumber.ParentStyleUsing.UseFont = False
            Me.XrPageInfo_PageNumber.ParentStyleUsing.UseForeColor = False
            '
            'Detail
            '
            Me.Detail.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel_Female_1, Me.XrLabel_AvgCreditors_1, Me.XrLabel_Fin_Problem2_1, Me.XrLabel_Fin_Problem3_1, Me.XrLabel_Male_1, Me.XrLabel_AvgDebt_1, Me.XrLabel_AvgIncome_1, Me.XrLabel_Fin_Problem1_1, Me.XrLabel_Delinquent_Mortgage_1, Me.XrLabel_FICO_0, Me.XrLabel_Male_0, Me.XrLabel_Female_0, Me.XrLabel_Fin_Problem3_0, Me.XrLabel_Fin_Problem2_0, Me.XrLabel_Fin_Problem1_0, Me.XrLabel_FICO_1, Me.XrLabel_Delinquent_Mortgage_0, Me.XrLabel_AvgCreditors_0, Me.XrLabel_AvgDebt_0, Me.XrLabel_AvgIncome_0, Me.XrLabel10, Me.XrLabel9, Me.XrLabel8, Me.XrLabel7, Me.XrLabel6, Me.XrLabel5, Me.XrLabel4, Me.XrLabel3, Me.XrLabel2, Me.XrLabel1})
            Me.Detail.Height = 307
            '
            'XrLabel_Subtitle
            '
            Me.XrLabel_Subtitle.ParentStyleUsing.UseFont = False
            Me.XrLabel_Subtitle.ParentStyleUsing.UseForeColor = False
            '
            'XrLabel_Title
            '
            Me.XrLabel_Title.ParentStyleUsing.UseFont = False
            Me.XrLabel_Title.ParentStyleUsing.UseForeColor = False
            '
            'XrLabel1
            '
            Me.XrLabel1.Location = New System.Drawing.Point(308, 17)
            Me.XrLabel1.Name = "XrLabel1"
            Me.XrLabel1.Size = New System.Drawing.Size(217, 17)
            Me.XrLabel1.StyleName = "Style_Total_LText"
            Me.XrLabel1.Text = "FINANCIAL COUNSELING"
            '
            'XrLabel2
            '
            Me.XrLabel2.Location = New System.Drawing.Point(567, 17)
            Me.XrLabel2.Name = "XrLabel2"
            Me.XrLabel2.Size = New System.Drawing.Size(183, 17)
            Me.XrLabel2.StyleName = "Style_Total_LText"
            Me.XrLabel2.Text = "BANKRUPTCY"
            '
            'XrLabel3
            '
            Me.XrLabel3.Location = New System.Drawing.Point(8, 50)
            Me.XrLabel3.Multiline = True
            Me.XrLabel3.Name = "XrLabel3"
            Me.XrLabel3.Size = New System.Drawing.Size(234, 17)
            Me.XrLabel3.StyleName = "Style_Total_LText"
            Me.XrLabel3.Text = "AVERAGE INCOME"
            '
            'XrLabel4
            '
            Me.XrLabel4.Location = New System.Drawing.Point(8, 75)
            Me.XrLabel4.Multiline = True
            Me.XrLabel4.Name = "XrLabel4"
            Me.XrLabel4.Size = New System.Drawing.Size(234, 17)
            Me.XrLabel4.StyleName = "Style_Total_LText"
            Me.XrLabel4.Text = "AVERAGE UNSECURED DEBT"
            '
            'XrLabel5
            '
            Me.XrLabel5.Location = New System.Drawing.Point(8, 100)
            Me.XrLabel5.Multiline = True
            Me.XrLabel5.Name = "XrLabel5"
            Me.XrLabel5.Size = New System.Drawing.Size(259, 17)
            Me.XrLabel5.StyleName = "Style_Total_LText"
            Me.XrLabel5.Text = "AVERAGE NUMBER OF CREDITORS"
            '
            'XrLabel6
            '
            Me.XrLabel6.Location = New System.Drawing.Point(8, 125)
            Me.XrLabel6.Multiline = True
            Me.XrLabel6.Name = "XrLabel6"
            Me.XrLabel6.Size = New System.Drawing.Size(234, 17)
            Me.XrLabel6.StyleName = "Style_Total_LText"
            Me.XrLabel6.Text = "DELINQUENT MORTGAGES (Y/N)?"
            '
            'XrLabel7
            '
            Me.XrLabel7.Location = New System.Drawing.Point(8, 150)
            Me.XrLabel7.Multiline = True
            Me.XrLabel7.Name = "XrLabel7"
            Me.XrLabel7.Size = New System.Drawing.Size(234, 17)
            Me.XrLabel7.StyleName = "Style_Total_LText"
            Me.XrLabel7.Text = "AVERAGE FICO SCORE"
            '
            'XrLabel8
            '
            Me.XrLabel8.Location = New System.Drawing.Point(8, 175)
            Me.XrLabel8.Multiline = True
            Me.XrLabel8.Name = "XrLabel8"
            Me.XrLabel8.Size = New System.Drawing.Size(234, 17)
            Me.XrLabel8.StyleName = "Style_Total_LText"
            Me.XrLabel8.Text = "PERCENT OF MEN"
            '
            'XrLabel9
            '
            Me.XrLabel9.Location = New System.Drawing.Point(8, 200)
            Me.XrLabel9.Multiline = True
            Me.XrLabel9.Name = "XrLabel9"
            Me.XrLabel9.Size = New System.Drawing.Size(234, 17)
            Me.XrLabel9.StyleName = "Style_Total_LText"
            Me.XrLabel9.Text = "PERCENT OF WOMEN"
            '
            'XrLabel10
            '
            Me.XrLabel10.Location = New System.Drawing.Point(8, 225)
            Me.XrLabel10.Multiline = True
            Me.XrLabel10.Name = "XrLabel10"
            Me.XrLabel10.Size = New System.Drawing.Size(259, 17)
            Me.XrLabel10.StyleName = "Style_Total_LText"
            Me.XrLabel10.Text = "TOP THREE FINANCIAL PROBLEMS"
            '
            'XrLabel_AvgIncome_0
            '
            Me.XrLabel_AvgIncome_0.CanGrow = False
            Me.XrLabel_AvgIncome_0.Location = New System.Drawing.Point(308, 50)
            Me.XrLabel_AvgIncome_0.Name = "XrLabel_AvgIncome_0"
            Me.XrLabel_AvgIncome_0.Size = New System.Drawing.Size(217, 17)
            Me.XrLabel_AvgIncome_0.Text = "XrLabel_AvgIncome_0"
            Me.XrLabel_AvgIncome_0.WordWrap = False
            '
            'XrLabel_AvgDebt_0
            '
            Me.XrLabel_AvgDebt_0.CanGrow = False
            Me.XrLabel_AvgDebt_0.Location = New System.Drawing.Point(308, 75)
            Me.XrLabel_AvgDebt_0.Name = "XrLabel_AvgDebt_0"
            Me.XrLabel_AvgDebt_0.Size = New System.Drawing.Size(217, 17)
            Me.XrLabel_AvgDebt_0.Text = "XrLabel_AvgDebt_0"
            Me.XrLabel_AvgDebt_0.WordWrap = False
            '
            'XrLabel_AvgCreditors_0
            '
            Me.XrLabel_AvgCreditors_0.CanGrow = False
            Me.XrLabel_AvgCreditors_0.Location = New System.Drawing.Point(308, 100)
            Me.XrLabel_AvgCreditors_0.Name = "XrLabel_AvgCreditors_0"
            Me.XrLabel_AvgCreditors_0.Size = New System.Drawing.Size(217, 17)
            Me.XrLabel_AvgCreditors_0.Text = "XrLabel_AvgCreditors_0"
            Me.XrLabel_AvgCreditors_0.WordWrap = False
            '
            'XrLabel_Delinquent_Mortgage_0
            '
            Me.XrLabel_Delinquent_Mortgage_0.CanGrow = False
            Me.XrLabel_Delinquent_Mortgage_0.Location = New System.Drawing.Point(308, 125)
            Me.XrLabel_Delinquent_Mortgage_0.Name = "XrLabel_Delinquent_Mortgage_0"
            Me.XrLabel_Delinquent_Mortgage_0.Size = New System.Drawing.Size(217, 17)
            Me.XrLabel_Delinquent_Mortgage_0.Tag = "delinquent_mortgage_1"
            Me.XrLabel_Delinquent_Mortgage_0.Text = "XrLabel_Delinquent_Mortgage_0"
            Me.XrLabel_Delinquent_Mortgage_0.WordWrap = False
            '
            'XrLabel_FICO_0
            '
            Me.XrLabel_FICO_0.CanGrow = False
            Me.XrLabel_FICO_0.Location = New System.Drawing.Point(308, 150)
            Me.XrLabel_FICO_0.Name = "XrLabel_FICO_0"
            Me.XrLabel_FICO_0.Size = New System.Drawing.Size(217, 17)
            Me.XrLabel_FICO_0.Tag = "fico_score_0"
            Me.XrLabel_FICO_0.Text = "XrLabel_FICO_0"
            Me.XrLabel_FICO_0.WordWrap = False
            '
            'XrLabel_Fin_Problem1_0
            '
            Me.XrLabel_Fin_Problem1_0.CanGrow = False
            Me.XrLabel_Fin_Problem1_0.Location = New System.Drawing.Point(308, 225)
            Me.XrLabel_Fin_Problem1_0.Name = "XrLabel_Fin_Problem1_0"
            Me.XrLabel_Fin_Problem1_0.Size = New System.Drawing.Size(217, 17)
            Me.XrLabel_Fin_Problem1_0.Text = "XrLabel_Fin_Problem1_0"
            Me.XrLabel_Fin_Problem1_0.WordWrap = False
            '
            'XrLabel_Fin_Problem2_0
            '
            Me.XrLabel_Fin_Problem2_0.CanGrow = False
            Me.XrLabel_Fin_Problem2_0.Location = New System.Drawing.Point(308, 250)
            Me.XrLabel_Fin_Problem2_0.Name = "XrLabel_Fin_Problem2_0"
            Me.XrLabel_Fin_Problem2_0.Size = New System.Drawing.Size(217, 17)
            Me.XrLabel_Fin_Problem2_0.Text = "XrLabel_Fin_Problem2_0"
            Me.XrLabel_Fin_Problem2_0.WordWrap = False
            '
            'XrLabel_Fin_Problem3_0
            '
            Me.XrLabel_Fin_Problem3_0.CanGrow = False
            Me.XrLabel_Fin_Problem3_0.Location = New System.Drawing.Point(308, 275)
            Me.XrLabel_Fin_Problem3_0.Name = "XrLabel_Fin_Problem3_0"
            Me.XrLabel_Fin_Problem3_0.Size = New System.Drawing.Size(217, 17)
            Me.XrLabel_Fin_Problem3_0.Text = "XrLabel_Fin_Problem3_0"
            Me.XrLabel_Fin_Problem3_0.WordWrap = False
            '
            'XrLabel_Female_0
            '
            Me.XrLabel_Female_0.CanGrow = False
            Me.XrLabel_Female_0.Location = New System.Drawing.Point(308, 200)
            Me.XrLabel_Female_0.Name = "XrLabel_Female_0"
            Me.XrLabel_Female_0.Size = New System.Drawing.Size(217, 17)
            Me.XrLabel_Female_0.Text = "XrLabel_Female_0"
            Me.XrLabel_Female_0.WordWrap = False
            '
            'XrLabel_Male_0
            '
            Me.XrLabel_Male_0.CanGrow = False
            Me.XrLabel_Male_0.Location = New System.Drawing.Point(308, 175)
            Me.XrLabel_Male_0.Name = "XrLabel_Male_0"
            Me.XrLabel_Male_0.Size = New System.Drawing.Size(217, 17)
            Me.XrLabel_Male_0.Text = "XrLabel_Male_0"
            Me.XrLabel_Male_0.WordWrap = False
            '
            'XrLabel_FICO_1
            '
            Me.XrLabel_FICO_1.CanGrow = False
            Me.XrLabel_FICO_1.Location = New System.Drawing.Point(567, 150)
            Me.XrLabel_FICO_1.Name = "XrLabel_FICO_1"
            Me.XrLabel_FICO_1.Size = New System.Drawing.Size(217, 17)
            Me.XrLabel_FICO_1.Tag = "fico_score_1"
            Me.XrLabel_FICO_1.Text = "XrLabel_FICO_1"
            Me.XrLabel_FICO_1.WordWrap = False
            '
            'XrLabel_Delinquent_Mortgage_1
            '
            Me.XrLabel_Delinquent_Mortgage_1.CanGrow = False
            Me.XrLabel_Delinquent_Mortgage_1.Location = New System.Drawing.Point(567, 125)
            Me.XrLabel_Delinquent_Mortgage_1.Name = "XrLabel_Delinquent_Mortgage_1"
            Me.XrLabel_Delinquent_Mortgage_1.Size = New System.Drawing.Size(217, 17)
            Me.XrLabel_Delinquent_Mortgage_1.Tag = "delinquent_mortgage_1"
            Me.XrLabel_Delinquent_Mortgage_1.Text = "XrLabel_Delinquent_Mortgage_1"
            Me.XrLabel_Delinquent_Mortgage_1.WordWrap = False
            '
            'XrLabel_Fin_Problem1_1
            '
            Me.XrLabel_Fin_Problem1_1.CanGrow = False
            Me.XrLabel_Fin_Problem1_1.Location = New System.Drawing.Point(567, 225)
            Me.XrLabel_Fin_Problem1_1.Name = "XrLabel_Fin_Problem1_1"
            Me.XrLabel_Fin_Problem1_1.Size = New System.Drawing.Size(217, 17)
            Me.XrLabel_Fin_Problem1_1.Text = "XrLabel_Fin_Problem1_1"
            Me.XrLabel_Fin_Problem1_1.WordWrap = False
            '
            'XrLabel_AvgIncome_1
            '
            Me.XrLabel_AvgIncome_1.CanGrow = False
            Me.XrLabel_AvgIncome_1.Location = New System.Drawing.Point(567, 50)
            Me.XrLabel_AvgIncome_1.Name = "XrLabel_AvgIncome_1"
            Me.XrLabel_AvgIncome_1.Size = New System.Drawing.Size(217, 17)
            Me.XrLabel_AvgIncome_1.Text = "XrLabel_AvgIncome_1"
            Me.XrLabel_AvgIncome_1.WordWrap = False
            '
            'XrLabel_AvgDebt_1
            '
            Me.XrLabel_AvgDebt_1.CanGrow = False
            Me.XrLabel_AvgDebt_1.Location = New System.Drawing.Point(567, 75)
            Me.XrLabel_AvgDebt_1.Name = "XrLabel_AvgDebt_1"
            Me.XrLabel_AvgDebt_1.Size = New System.Drawing.Size(217, 17)
            Me.XrLabel_AvgDebt_1.Text = "XrLabel_AvgDebt_1"
            Me.XrLabel_AvgDebt_1.WordWrap = False
            '
            'XrLabel_Male_1
            '
            Me.XrLabel_Male_1.CanGrow = False
            Me.XrLabel_Male_1.Location = New System.Drawing.Point(567, 175)
            Me.XrLabel_Male_1.Name = "XrLabel_Male_1"
            Me.XrLabel_Male_1.Size = New System.Drawing.Size(217, 17)
            Me.XrLabel_Male_1.Text = "XrLabel_Male_1"
            Me.XrLabel_Male_1.WordWrap = False
            '
            'XrLabel_Fin_Problem3_1
            '
            Me.XrLabel_Fin_Problem3_1.CanGrow = False
            Me.XrLabel_Fin_Problem3_1.Location = New System.Drawing.Point(567, 275)
            Me.XrLabel_Fin_Problem3_1.Name = "XrLabel_Fin_Problem3_1"
            Me.XrLabel_Fin_Problem3_1.Size = New System.Drawing.Size(217, 17)
            Me.XrLabel_Fin_Problem3_1.Text = "XrLabel_Fin_Problem3_1"
            Me.XrLabel_Fin_Problem3_1.WordWrap = False
            '
            'XrLabel_Fin_Problem2_1
            '
            Me.XrLabel_Fin_Problem2_1.CanGrow = False
            Me.XrLabel_Fin_Problem2_1.Location = New System.Drawing.Point(567, 250)
            Me.XrLabel_Fin_Problem2_1.Name = "XrLabel_Fin_Problem2_1"
            Me.XrLabel_Fin_Problem2_1.Size = New System.Drawing.Size(217, 17)
            Me.XrLabel_Fin_Problem2_1.Text = "XrLabel_Fin_Problem2_1"
            Me.XrLabel_Fin_Problem2_1.WordWrap = False
            '
            'XrLabel_AvgCreditors_1
            '
            Me.XrLabel_AvgCreditors_1.CanGrow = False
            Me.XrLabel_AvgCreditors_1.Location = New System.Drawing.Point(567, 100)
            Me.XrLabel_AvgCreditors_1.Name = "XrLabel_AvgCreditors_1"
            Me.XrLabel_AvgCreditors_1.Size = New System.Drawing.Size(217, 17)
            Me.XrLabel_AvgCreditors_1.Text = "XrLabel_AvgCreditors_1"
            Me.XrLabel_AvgCreditors_1.WordWrap = False
            '
            'XrLabel_Female_1
            '
            Me.XrLabel_Female_1.CanGrow = False
            Me.XrLabel_Female_1.Location = New System.Drawing.Point(567, 200)
            Me.XrLabel_Female_1.Name = "XrLabel_Female_1"
            Me.XrLabel_Female_1.Size = New System.Drawing.Size(217, 17)
            Me.XrLabel_Female_1.Text = "XrLabel_Female_1"
            Me.XrLabel_Female_1.WordWrap = False
            '
            'NFCCBankruptcyReport
            '
            Me.StyleSheetPath = "C:\Documents and Settings\Al Longyear\My Documents\Visual Studio Projects\DebtPlu" &
                                "s\Reports\Report Styles.repss"
            CType(Me, System.ComponentModel.ISupportInitialize).EndInit()
        End Sub

#End Region

        Private Sub NFCCBankruptcyReport_BeforePrint(ByVal sender As Object, ByVal e As EventArgs)
            Dim ds As New DataSet("NFCCBankruptcy")
            Dim da As New SqlDataAdapter(SelectCommand)
            da.Fill(ds, "bankruptcy")

            ' Set the datasource
            Dim tbl As DataTable = ds.Tables(0)
            Dim vue As DataView = tbl.DefaultView
            Me.DataSource = vue

            ' Bind the columns to the result locations
            XrLabel_AvgCreditors_0.DataBindings.Add(New XRBinding("Text", vue, "debt_count_0", ""))
            XrLabel_AvgCreditors_1.DataBindings.Add(New XRBinding("Text", vue, "debt_count_1", ""))

            XrLabel_AvgDebt_0.DataBindings.Add(New XRBinding("Text", vue, "unsecured_debt_0", "{0:c}"))
            XrLabel_AvgDebt_1.DataBindings.Add(New XRBinding("Text", vue, "unsecured_debt_1", "{0:c}"))

            XrLabel_AvgIncome_0.DataBindings.Add(New XRBinding("Text", vue, "avg_income_0", "{0:c}"))
            XrLabel_AvgIncome_1.DataBindings.Add(New XRBinding("Text", vue, "avg_income_1", "{0:c}"))

            XrLabel_Female_0.DataBindings.Add(New XRBinding("Text", vue, "female_0", "{0:###.00}%"))
            XrLabel_Female_1.DataBindings.Add(New XRBinding("Text", vue, "female_1", "{0:###.00}%"))
            XrLabel_Male_0.DataBindings.Add(New XRBinding("Text", vue, "male_0", "{0:###.00}%"))
            XrLabel_Male_1.DataBindings.Add(New XRBinding("Text", vue, "male_1", "{0:###.00}%"))

            XrLabel_FICO_0.DataBindings.Add(New XRBinding("Text", vue, "fico_score_0", ""))
            XrLabel_FICO_1.DataBindings.Add(New XRBinding("Text", vue, "fico_score_1", ""))

            XrLabel_Fin_Problem1_0.DataBindings.Add(New XRBinding("Text", vue, "fin_problem1_0", ""))
            XrLabel_Fin_Problem2_0.DataBindings.Add(New XRBinding("Text", vue, "fin_problem2_0", ""))
            XrLabel_Fin_Problem3_0.DataBindings.Add(New XRBinding("Text", vue, "fin_problem3_0", ""))

            XrLabel_Fin_Problem1_1.DataBindings.Add(New XRBinding("Text", vue, "fin_problem1_1", ""))
            XrLabel_Fin_Problem2_1.DataBindings.Add(New XRBinding("Text", vue, "fin_problem2_1", ""))
            XrLabel_Fin_Problem3_1.DataBindings.Add(New XRBinding("Text", vue, "fin_problem3_1", ""))
        End Sub

        Private Function SelectCommand() As SqlCommand
            Dim cmd As SqlCommand = New System.Data.SqlClient.SqlCommand()
            With cmd
                .Connection = New System.Data.SqlClient.SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
                .CommandText = "rpt_custom_NFCC_bankruptcy"
                .CommandType = CommandType.StoredProcedure
                .Parameters.Add(New SqlParameter("@From_Date", SqlDbType.VarChar, 80)).Value = Parameter_FromDate.ToShortDateString & " 00:00:00"
                .Parameters.Add(New SqlParameter("@To_Date", SqlDbType.VarChar, 80)).Value = Parameter_ToDate.ToShortDateString & " 23:59:59"
                .CommandTimeout = 0
            End With

            Return cmd
        End Function

        Private Sub XrLabel_Delinquent_Mortgage_0_BeforePrint(ByVal sender As Object, ByVal e As PrintEventArgs)
            Dim ctl As XRLabel = CType(sender, XRLabel)
            With ctl
                Dim value As Int32 = 0
                If Convert.ToInt32(GetCurrentColumnValue(Convert.ToString(ctl.Tag))) <> 0 Then value = 1
                If value <> 0 Then
                    .Text = "Y"
                Else
                    .Text = "N"
                End If
            End With
        End Sub

        Private Sub XrLabel_FICO_0_PrintOnPage(ByVal sender As Object, ByVal e As PrintOnPageEventArgs)
            Dim ctl As XRLabel = CType(sender, XRLabel)
            With ctl
                Dim value As Int32 = 0
                If Convert.ToInt32(GetCurrentColumnValue(Convert.ToString(ctl.Tag))) <> 0 Then value = 1
                If value = 0 Then e.Cancel = True
            End With
        End Sub
    End Class
End Namespace
