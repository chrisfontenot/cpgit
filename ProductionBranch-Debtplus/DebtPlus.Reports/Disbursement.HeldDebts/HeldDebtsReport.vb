#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}

#End Region

Imports System.ComponentModel
Imports DebtPlus.Reports.Template

Imports System.Drawing.Printing
Imports System.Data.SqlClient
Imports DevExpress.XtraReports.UI

Namespace Disbursement.HeldDebts
    Public Class HeldDebtsReport
        Inherits TemplateXtraReportClass

        Public Sub New()
            MyBase.New()
            InitializeComponent()
            AddHandler Me.BeforePrint, AddressOf Report_BeforePrint
            AddHandler XrLabel_client.BeforePrint, AddressOf XrLabel_client_BeforePrint
        End Sub

        Public Overrides ReadOnly Property ReportTitle() As String
            Get
                Return "Held Debts"
            End Get
        End Property

        Dim ds As New DataSet("ds")

        Private Sub Report_BeforePrint(ByVal sender As Object, ByVal e As PrintEventArgs)
            Dim tbl As DataTable = ds.Tables("rpt_HeldDebts")

            If tbl Is Nothing Then
                Dim cn As SqlConnection = New System.Data.SqlClient.SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
                Try
                    cn.Open()
                    Using cmd As SqlCommand = New System.Data.SqlClient.SqlCommand()
                        With cmd
                            .Connection = cn
                            .CommandText = "rpt_HeldDebts"
                            .CommandType = CommandType.StoredProcedure
                            .CommandTimeout = 0
                        End With

                        Using da As New SqlDataAdapter(cmd)
                            da.Fill(ds, "rpt_HeldDebts")
                            tbl = ds.Tables("rpt_HeldDebts")
                        End Using
                    End Using

                Catch ex As SqlException
                    DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error reading report information")

                Finally
                    If cn IsNot Nothing Then cn.Dispose()
                End Try
            End If

            If tbl IsNot Nothing Then
                Dim vue As New DataView(tbl, String.Empty, "creditor, creditor_name, client, account_number", DataViewRowState.CurrentRows)
                DataSource = vue

                Dim xrGroup2 As New GroupField("disbursement_date", XRColumnSortOrder.Ascending)
                GroupHeader2.GroupFields.Add(xrGroup2)

                Dim xrGroup1 As New GroupField("creditor_name", XRColumnSortOrder.Ascending)
                GroupHeader1.GroupFields.Add(xrGroup1)

                With XrLabel_account_number
                    .DataBindings.Clear()
                    .DataBindings.Add("Text", vue, "account_number")
                End With

                With XrLabel_client_name
                    .DataBindings.Clear()
                    .DataBindings.Add("Text", vue, "name")
                End With

                With XrLabel_disbursement_factor
                    .DataBindings.Clear()
                    .DataBindings.Add("Text", vue, "disbursement_factor", "{0:c}")
                End With

                With XrLabel_group1_disbursement_factor
                    Dim xrSummary1 As New XRSummary(SummaryRunning.Group, SummaryFunc.Sum, "{0:c}")
                    .Summary = xrSummary1
                    .DataBindings.Clear()
                    .DataBindings.Add("Text", vue, "disbursement_factor")
                End With

                With XrLabel_group2_disbursement_factor
                    Dim xrSummary2 As New XRSummary(SummaryRunning.Group, SummaryFunc.Sum, "{0:c}")
                    .Summary = xrSummary2
                    .DataBindings.Clear()
                    .DataBindings.Add("Text", vue, "disbursement_factor")
                End With

                With XrLabel_total_disbursement_factor
                    Dim xrSummary3 As New XRSummary(SummaryRunning.Report, SummaryFunc.Sum, "{0:c}")
                    .Summary = xrSummary3
                    .DataBindings.Clear()
                    .DataBindings.Add("Text", vue, "disbursement_factor")
                End With
            End If
        End Sub

        Private Sub XrLabel_client_BeforePrint(ByVal sender As Object, ByVal e As PrintEventArgs)
            XrLabel_client.Text = String.Format("{0:0000000}", Convert.ToInt32(GetCurrentColumnValue("client")))
        End Sub
    End Class
End Namespace