#Region "Copyright 2000-2010 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2010 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}
#End Region
Namespace Bankruptcy
    <Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
    Partial Class BankruptcyByApptReport
        Inherits DebtPlus.Reports.Template.DatedTemplateXtraReportClass

        'Component overrides dispose to clean up the component list.
        <System.Diagnostics.DebuggerNonUserCode()> _
        Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
            If disposing Then
                If components IsNot Nothing Then
                    components.Dispose()
                End If
            End If
            MyBase.Dispose(disposing)
        End Sub

        'Required by the Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Designer
        'It can be modified using the Designer.
        'Do not modify it using the code editor.
        <System.Diagnostics.DebuggerStepThrough()> _
        Private Sub InitializeComponent()
            Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(BankruptcyByApptReport))
            Dim XrSummary1 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary()
            Dim XrSummary2 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary()
            Me.ReportFooter = New DevExpress.XtraReports.UI.ReportFooterBand()
            Me.XrLabel_notes = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_total_certificate_count = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_total_people_count = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel8 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel3 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLine1 = New DevExpress.XtraReports.UI.XRLine()
            Me.XrPanel1 = New DevExpress.XtraReports.UI.XRPanel()
            Me.XrLabel10 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel15 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel18 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel9 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel5 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel4 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel1 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel6 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel13 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_client = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_indicator_2 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_referred_by = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_bankruptcy_district_number = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_city_state_zip = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_appt_type = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_appt_time = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_people = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_method_first_contact = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_indicator_1 = New DevExpress.XtraReports.UI.XRLabel()
            CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
            '
            'PageHeader
            '
            Me.PageHeader.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrPanel1})
            Me.PageHeader.HeightF = 149.0!
            Me.PageHeader.Controls.SetChildIndex(Me.XrPanel1, 0)
            Me.PageHeader.Controls.SetChildIndex(Me.XrPanel_AgencyAddress, 0)
            Me.PageHeader.Controls.SetChildIndex(Me.XrLabel_Title, 0)
            Me.PageHeader.Controls.SetChildIndex(Me.XrLine_Title, 0)
            Me.PageHeader.Controls.SetChildIndex(Me.XrLabel_Subtitle, 0)
            Me.PageHeader.Controls.SetChildIndex(Me.XrPageInfo1, 0)
            '
            'XrLabel_Title
            '
            Me.XrLabel_Title.SizeF = New System.Drawing.SizeF(542.0!, 42.0!)
            Me.XrLabel_Title.StylePriority.UseFont = False
            '
            'XrLabel_Subtitle
            '
            Me.XrLabel_Subtitle.SizeF = New System.Drawing.SizeF(725.0!, 17.0!)
            '
            'XrPageInfo_PageNumber
            '
            Me.XrPageInfo_PageNumber.LocationFloat = New DevExpress.Utils.PointFloat(917.0!, 8.0!)
            Me.XrPageInfo_PageNumber.StylePriority.UseTextAlignment = False
            '
            'XrPanel_AgencyAddress
            '
            Me.XrPanel_AgencyAddress.LocationFloat = New DevExpress.Utils.PointFloat(742.0!, 0.0!)
            '
            'XRLabel_Agency_Name
            '
            Me.XRLabel_Agency_Name.StylePriority.UseTextAlignment = False
            '
            'XrLine_Title
            '
            Me.XrLine_Title.SizeF = New System.Drawing.SizeF(542.0!, 8.0!)
            '
            'XrLabel_Agency_Address3
            '
            Me.XrLabel_Agency_Address3.StylePriority.UseFont = False
            Me.XrLabel_Agency_Address3.StylePriority.UseTextAlignment = False
            '
            'XrLabel_Agency_Address1
            '
            Me.XrLabel_Agency_Address1.StylePriority.UseFont = False
            Me.XrLabel_Agency_Address1.StylePriority.UseTextAlignment = False
            '
            'XrLabel_Agency_Phone
            '
            Me.XrLabel_Agency_Phone.StylePriority.UseFont = False
            Me.XrLabel_Agency_Phone.StylePriority.UseTextAlignment = False
            '
            'XrLabel_Agency_Address2
            '
            Me.XrLabel_Agency_Address2.StylePriority.UseFont = False
            Me.XrLabel_Agency_Address2.StylePriority.UseTextAlignment = False
            '
            'Detail
            '
            Me.Detail.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel_appt_type, Me.XrLabel_indicator_1, Me.XrLabel_method_first_contact, Me.XrLabel_people, Me.XrLabel_appt_time, Me.XrLabel_city_state_zip, Me.XrLabel_bankruptcy_district_number, Me.XrLabel_referred_by, Me.XrLabel_indicator_2, Me.XrLabel_client})
            Me.Detail.HeightF = 15.0!
            '
            'ReportFooter
            '
            Me.ReportFooter.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel_notes, Me.XrLabel_total_certificate_count, Me.XrLabel_total_people_count, Me.XrLabel8, Me.XrLabel3, Me.XrLine1})
            Me.ReportFooter.HeightF = 165.0!
            Me.ReportFooter.KeepTogether = True
            Me.ReportFooter.Name = "ReportFooter"
            '
            'XrLabel_notes
            '
            Me.XrLabel_notes.ForeColor = System.Drawing.Color.Gray
            Me.XrLabel_notes.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 82.99999!)
            Me.XrLabel_notes.Multiline = True
            Me.XrLabel_notes.Name = "XrLabel_notes"
            Me.XrLabel_notes.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_notes.SizeF = New System.Drawing.SizeF(1050.0!, 82.00001!)
            Me.XrLabel_notes.StylePriority.UseForeColor = False
            Me.XrLabel_notes.Text = resources.GetString("XrLabel_notes.Text")
            '
            'XrLabel_total_certificate_count
            '
            Me.XrLabel_total_certificate_count.CanGrow = False
            Me.XrLabel_total_certificate_count.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "count_people_certified")})
            Me.XrLabel_total_certificate_count.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel_total_certificate_count.LocationFloat = New DevExpress.Utils.PointFloat(250.0!, 33.0!)
            Me.XrLabel_total_certificate_count.Name = "XrLabel_total_certificate_count"
            Me.XrLabel_total_certificate_count.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_total_certificate_count.SizeF = New System.Drawing.SizeF(100.0!, 17.0!)
            Me.XrLabel_total_certificate_count.StylePriority.UseFont = False
            Me.XrLabel_total_certificate_count.StylePriority.UseTextAlignment = False
            XrSummary1.FormatString = "{0:n0}"
            XrSummary1.Running = DevExpress.XtraReports.UI.SummaryRunning.Report
            Me.XrLabel_total_certificate_count.Summary = XrSummary1
            Me.XrLabel_total_certificate_count.Text = "0"
            Me.XrLabel_total_certificate_count.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
            Me.XrLabel_total_certificate_count.WordWrap = False
            '
            'XrLabel_total_people_count
            '
            Me.XrLabel_total_people_count.CanGrow = False
            Me.XrLabel_total_people_count.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "count_people")})
            Me.XrLabel_total_people_count.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel_total_people_count.LocationFloat = New DevExpress.Utils.PointFloat(250.0!, 50.0!)
            Me.XrLabel_total_people_count.Name = "XrLabel_total_people_count"
            Me.XrLabel_total_people_count.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_total_people_count.SizeF = New System.Drawing.SizeF(100.0!, 17.0!)
            Me.XrLabel_total_people_count.StylePriority.UseFont = False
            Me.XrLabel_total_people_count.StylePriority.UseTextAlignment = False
            XrSummary2.FormatString = "{0:n0}"
            XrSummary2.Running = DevExpress.XtraReports.UI.SummaryRunning.Report
            Me.XrLabel_total_people_count.Summary = XrSummary2
            Me.XrLabel_total_people_count.Text = "0"
            Me.XrLabel_total_people_count.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
            Me.XrLabel_total_people_count.WordWrap = False
            '
            'XrLabel8
            '
            Me.XrLabel8.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel8.LocationFloat = New DevExpress.Utils.PointFloat(33.0!, 50.0!)
            Me.XrLabel8.Name = "XrLabel8"
            Me.XrLabel8.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel8.SizeF = New System.Drawing.SizeF(208.0!, 17.0!)
            Me.XrLabel8.StylePriority.UseFont = False
            Me.XrLabel8.Text = "Number of counseling sessions"
            '
            'XrLabel3
            '
            Me.XrLabel3.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel3.LocationFloat = New DevExpress.Utils.PointFloat(33.0!, 33.0!)
            Me.XrLabel3.Name = "XrLabel3"
            Me.XrLabel3.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel3.SizeF = New System.Drawing.SizeF(208.0!, 17.0!)
            Me.XrLabel3.StylePriority.UseFont = False
            Me.XrLabel3.Text = "Number of certificates issued"
            '
            'XrLine1
            '
            Me.XrLine1.BorderWidth = 2
            Me.XrLine1.ForeColor = System.Drawing.Color.Teal
            Me.XrLine1.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
            Me.XrLine1.Name = "XrLine1"
            Me.XrLine1.SizeF = New System.Drawing.SizeF(1050.0!, 17.0!)
            Me.XrLine1.StylePriority.UseBorderWidth = False
            Me.XrLine1.StylePriority.UseForeColor = False
            '
            'XrPanel1
            '
            Me.XrPanel1.BackColor = System.Drawing.Color.Teal
            Me.XrPanel1.CanGrow = False
            Me.XrPanel1.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel10, Me.XrLabel15, Me.XrLabel18, Me.XrLabel9, Me.XrLabel5, Me.XrLabel4, Me.XrLabel1, Me.XrLabel6, Me.XrLabel13})
            Me.XrPanel1.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 125.0!)
            Me.XrPanel1.Name = "XrPanel1"
            Me.XrPanel1.SizeF = New System.Drawing.SizeF(1050.0!, 18.0!)
            Me.XrPanel1.StylePriority.UseBackColor = False
            '
            'XrLabel10
            '
            Me.XrLabel10.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel10.ForeColor = System.Drawing.Color.White
            Me.XrLabel10.LocationFloat = New DevExpress.Utils.PointFloat(916.5!, 0.0!)
            Me.XrLabel10.Name = "XrLabel10"
            Me.XrLabel10.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 2, 0, 0, 100.0!)
            Me.XrLabel10.SizeF = New System.Drawing.SizeF(17.0!, 15.0!)
            Me.XrLabel10.StylePriority.UseFont = False
            Me.XrLabel10.StylePriority.UseForeColor = False
            Me.XrLabel10.StylePriority.UsePadding = False
            Me.XrLabel10.StylePriority.UseTextAlignment = False
            Me.XrLabel10.Text = "C"
            Me.XrLabel10.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
            '
            'XrLabel15
            '
            Me.XrLabel15.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel15.ForeColor = System.Drawing.Color.White
            Me.XrLabel15.LocationFloat = New DevExpress.Utils.PointFloat(592.7083!, 3.0!)
            Me.XrLabel15.Name = "XrLabel15"
            Me.XrLabel15.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel15.SizeF = New System.Drawing.SizeF(44.125!, 15.0!)
            Me.XrLabel15.StylePriority.UseFont = False
            Me.XrLabel15.StylePriority.UseForeColor = False
            Me.XrLabel15.StylePriority.UseTextAlignment = False
            Me.XrLabel15.Text = "DIST"
            Me.XrLabel15.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel18
            '
            Me.XrLabel18.CanGrow = False
            Me.XrLabel18.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel18.ForeColor = System.Drawing.Color.White
            Me.XrLabel18.LocationFloat = New DevExpress.Utils.PointFloat(92.0!, 1.0!)
            Me.XrLabel18.Name = "XrLabel18"
            Me.XrLabel18.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel18.SizeF = New System.Drawing.SizeF(108.0!, 15.0!)
            Me.XrLabel18.StylePriority.UseFont = False
            Me.XrLabel18.StylePriority.UseForeColor = False
            Me.XrLabel18.StylePriority.UseTextAlignment = False
            Me.XrLabel18.Text = "TIME"
            Me.XrLabel18.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            '
            'XrLabel9
            '
            Me.XrLabel9.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel9.ForeColor = System.Drawing.Color.White
            Me.XrLabel9.LocationFloat = New DevExpress.Utils.PointFloat(67.0!, 1.0!)
            Me.XrLabel9.Name = "XrLabel9"
            Me.XrLabel9.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel9.SizeF = New System.Drawing.SizeF(16.0!, 15.0!)
            Me.XrLabel9.StylePriority.UseFont = False
            Me.XrLabel9.StylePriority.UseForeColor = False
            Me.XrLabel9.StylePriority.UseTextAlignment = False
            Me.XrLabel9.Text = "#"
            Me.XrLabel9.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel5
            '
            Me.XrLabel5.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel5.ForeColor = System.Drawing.Color.White
            Me.XrLabel5.LocationFloat = New DevExpress.Utils.PointFloat(950.0!, 0.0!)
            Me.XrLabel5.Name = "XrLabel5"
            Me.XrLabel5.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 0, 0, 0, 100.0!)
            Me.XrLabel5.SizeF = New System.Drawing.SizeF(93.66669!, 15.0!)
            Me.XrLabel5.StylePriority.UseFont = False
            Me.XrLabel5.StylePriority.UseForeColor = False
            Me.XrLabel5.StylePriority.UsePadding = False
            Me.XrLabel5.StylePriority.UseTextAlignment = False
            Me.XrLabel5.Text = "FEE RECVD"
            Me.XrLabel5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
            '
            'XrLabel4
            '
            Me.XrLabel4.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel4.ForeColor = System.Drawing.Color.White
            Me.XrLabel4.LocationFloat = New DevExpress.Utils.PointFloat(408.0!, 0.0!)
            Me.XrLabel4.Name = "XrLabel4"
            Me.XrLabel4.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 0, 0, 0, 100.0!)
            Me.XrLabel4.SizeF = New System.Drawing.SizeF(167.0!, 15.0!)
            Me.XrLabel4.StylePriority.UseFont = False
            Me.XrLabel4.StylePriority.UseForeColor = False
            Me.XrLabel4.StylePriority.UsePadding = False
            Me.XrLabel4.StylePriority.UseTextAlignment = False
            Me.XrLabel4.Text = "CITY, STATE, ZIP"
            Me.XrLabel4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            '
            'XrLabel1
            '
            Me.XrLabel1.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel1.ForeColor = System.Drawing.Color.White
            Me.XrLabel1.LocationFloat = New DevExpress.Utils.PointFloat(1.0!, 1.0!)
            Me.XrLabel1.Name = "XrLabel1"
            Me.XrLabel1.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel1.SizeF = New System.Drawing.SizeF(66.0!, 15.0!)
            Me.XrLabel1.StylePriority.UseFont = False
            Me.XrLabel1.StylePriority.UseForeColor = False
            Me.XrLabel1.StylePriority.UseTextAlignment = False
            Me.XrLabel1.Text = "CLIENT"
            Me.XrLabel1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel6
            '
            Me.XrLabel6.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel6.ForeColor = System.Drawing.Color.White
            Me.XrLabel6.LocationFloat = New DevExpress.Utils.PointFloat(208.0!, 0.0!)
            Me.XrLabel6.Name = "XrLabel6"
            Me.XrLabel6.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel6.SizeF = New System.Drawing.SizeF(190.0!, 15.0!)
            Me.XrLabel6.StylePriority.UseFont = False
            Me.XrLabel6.StylePriority.UseForeColor = False
            Me.XrLabel6.StylePriority.UseTextAlignment = False
            Me.XrLabel6.Text = "APPT TYPE"
            Me.XrLabel6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            '
            'XrLabel13
            '
            Me.XrLabel13.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel13.ForeColor = System.Drawing.Color.White
            Me.XrLabel13.LocationFloat = New DevExpress.Utils.PointFloat(708.0!, 0.0!)
            Me.XrLabel13.Name = "XrLabel13"
            Me.XrLabel13.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel13.SizeF = New System.Drawing.SizeF(192.0!, 15.0!)
            Me.XrLabel13.StylePriority.UseFont = False
            Me.XrLabel13.StylePriority.UseForeColor = False
            Me.XrLabel13.StylePriority.UseTextAlignment = False
            Me.XrLabel13.Text = "1ST CONTACT & REFFERAL"
            Me.XrLabel13.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            '
            'XrLabel_client
            '
            Me.XrLabel_client.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "client", "{0:0000000}")})
            Me.XrLabel_client.Font = New System.Drawing.Font("Times New Roman", 9.75!)
            Me.XrLabel_client.ForeColor = System.Drawing.SystemColors.WindowText
            Me.XrLabel_client.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
            Me.XrLabel_client.Name = "XrLabel_client"
            Me.XrLabel_client.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_client.SizeF = New System.Drawing.SizeF(66.0!, 15.0!)
            Me.XrLabel_client.StylePriority.UseFont = False
            Me.XrLabel_client.StylePriority.UseForeColor = False
            Me.XrLabel_client.StylePriority.UseTextAlignment = False
            Me.XrLabel_client.Text = "CLIENT"
            Me.XrLabel_client.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel_indicator_2
            '
            Me.XrLabel_indicator_2.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "indicator_2")})
            Me.XrLabel_indicator_2.Font = New System.Drawing.Font("Times New Roman", 9.75!)
            Me.XrLabel_indicator_2.ForeColor = System.Drawing.SystemColors.WindowText
            Me.XrLabel_indicator_2.LocationFloat = New DevExpress.Utils.PointFloat(950.0!, 0.0!)
            Me.XrLabel_indicator_2.Name = "XrLabel_indicator_2"
            Me.XrLabel_indicator_2.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_indicator_2.SizeF = New System.Drawing.SizeF(89.99988!, 14.99999!)
            Me.XrLabel_indicator_2.StylePriority.UseFont = False
            Me.XrLabel_indicator_2.StylePriority.UseForeColor = False
            Me.XrLabel_indicator_2.StylePriority.UseTextAlignment = False
            Me.XrLabel_indicator_2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
            '
            'XrLabel_referred_by
            '
            Me.XrLabel_referred_by.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "referred_by")})
            Me.XrLabel_referred_by.Font = New System.Drawing.Font("Times New Roman", 9.75!)
            Me.XrLabel_referred_by.ForeColor = System.Drawing.SystemColors.WindowText
            Me.XrLabel_referred_by.LocationFloat = New DevExpress.Utils.PointFloat(733.0!, 0.0!)
            Me.XrLabel_referred_by.Name = "XrLabel_referred_by"
            Me.XrLabel_referred_by.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_referred_by.SizeF = New System.Drawing.SizeF(167.0!, 14.99999!)
            Me.XrLabel_referred_by.StylePriority.UseFont = False
            Me.XrLabel_referred_by.StylePriority.UseForeColor = False
            Me.XrLabel_referred_by.StylePriority.UseTextAlignment = False
            Me.XrLabel_referred_by.Text = "1ST CONTACT & REFFERAL"
            Me.XrLabel_referred_by.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            Me.XrLabel_referred_by.WordWrap = False
            '
            'XrLabel_bankruptcy_district_number
            '
            Me.XrLabel_bankruptcy_district_number.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "bankruptcy_district_number", "{0:f0}")})
            Me.XrLabel_bankruptcy_district_number.Font = New System.Drawing.Font("Times New Roman", 9.75!)
            Me.XrLabel_bankruptcy_district_number.ForeColor = System.Drawing.SystemColors.WindowText
            Me.XrLabel_bankruptcy_district_number.LocationFloat = New DevExpress.Utils.PointFloat(592.7083!, 0.0!)
            Me.XrLabel_bankruptcy_district_number.Name = "XrLabel_bankruptcy_district_number"
            Me.XrLabel_bankruptcy_district_number.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_bankruptcy_district_number.SizeF = New System.Drawing.SizeF(44.125!, 15.0!)
            Me.XrLabel_bankruptcy_district_number.StylePriority.UseFont = False
            Me.XrLabel_bankruptcy_district_number.StylePriority.UseForeColor = False
            Me.XrLabel_bankruptcy_district_number.StylePriority.UseTextAlignment = False
            Me.XrLabel_bankruptcy_district_number.Text = "DIST"
            Me.XrLabel_bankruptcy_district_number.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel_city_state_zip
            '
            Me.XrLabel_city_state_zip.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "city_state_zip")})
            Me.XrLabel_city_state_zip.Font = New System.Drawing.Font("Times New Roman", 9.75!)
            Me.XrLabel_city_state_zip.ForeColor = System.Drawing.SystemColors.WindowText
            Me.XrLabel_city_state_zip.LocationFloat = New DevExpress.Utils.PointFloat(408.0!, 0.0!)
            Me.XrLabel_city_state_zip.Name = "XrLabel_city_state_zip"
            Me.XrLabel_city_state_zip.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 0, 0, 0, 100.0!)
            Me.XrLabel_city_state_zip.SizeF = New System.Drawing.SizeF(167.0!, 15.0!)
            Me.XrLabel_city_state_zip.StylePriority.UseFont = False
            Me.XrLabel_city_state_zip.StylePriority.UseForeColor = False
            Me.XrLabel_city_state_zip.StylePriority.UsePadding = False
            Me.XrLabel_city_state_zip.StylePriority.UseTextAlignment = False
            Me.XrLabel_city_state_zip.Text = "CITY, STATE, ZIP"
            Me.XrLabel_city_state_zip.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            Me.XrLabel_city_state_zip.WordWrap = False
            '
            'XrLabel_appt_type
            '
            Me.XrLabel_appt_type.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "appt_type")})
            Me.XrLabel_appt_type.Font = New System.Drawing.Font("Times New Roman", 9.75!)
            Me.XrLabel_appt_type.ForeColor = System.Drawing.SystemColors.WindowText
            Me.XrLabel_appt_type.LocationFloat = New DevExpress.Utils.PointFloat(208.0!, 0.0!)
            Me.XrLabel_appt_type.Name = "XrLabel_appt_type"
            Me.XrLabel_appt_type.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_appt_type.SizeF = New System.Drawing.SizeF(190.0!, 15.0!)
            Me.XrLabel_appt_type.StylePriority.UseFont = False
            Me.XrLabel_appt_type.StylePriority.UseForeColor = False
            Me.XrLabel_appt_type.StylePriority.UseTextAlignment = False
            Me.XrLabel_appt_type.Text = "APPT TYPE"
            Me.XrLabel_appt_type.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            '
            'XrLabel_appt_time
            '
            Me.XrLabel_appt_time.CanGrow = False
            Me.XrLabel_appt_time.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "appt_time", "{0:M/d/yy h:mm tt}")})
            Me.XrLabel_appt_time.Font = New System.Drawing.Font("Times New Roman", 9.75!)
            Me.XrLabel_appt_time.ForeColor = System.Drawing.SystemColors.WindowText
            Me.XrLabel_appt_time.LocationFloat = New DevExpress.Utils.PointFloat(92.00001!, 0.0!)
            Me.XrLabel_appt_time.Name = "XrLabel_appt_time"
            Me.XrLabel_appt_time.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_appt_time.SizeF = New System.Drawing.SizeF(108.0!, 14.99999!)
            Me.XrLabel_appt_time.StylePriority.UseFont = False
            Me.XrLabel_appt_time.StylePriority.UseForeColor = False
            Me.XrLabel_appt_time.StylePriority.UseTextAlignment = False
            Me.XrLabel_appt_time.Text = "TIME"
            Me.XrLabel_appt_time.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            '
            'XrLabel_people
            '
            Me.XrLabel_people.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "count_people", "{0:f0}")})
            Me.XrLabel_people.Font = New System.Drawing.Font("Times New Roman", 9.75!)
            Me.XrLabel_people.ForeColor = System.Drawing.SystemColors.WindowText
            Me.XrLabel_people.LocationFloat = New DevExpress.Utils.PointFloat(67.0!, 0.0!)
            Me.XrLabel_people.Name = "XrLabel_people"
            Me.XrLabel_people.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_people.SizeF = New System.Drawing.SizeF(16.0!, 15.0!)
            Me.XrLabel_people.StylePriority.UseFont = False
            Me.XrLabel_people.StylePriority.UseForeColor = False
            Me.XrLabel_people.StylePriority.UseTextAlignment = False
            Me.XrLabel_people.Text = "#"
            Me.XrLabel_people.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel_method_first_contact
            '
            Me.XrLabel_method_first_contact.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "method_first_contact")})
            Me.XrLabel_method_first_contact.Font = New System.Drawing.Font("Times New Roman", 9.75!)
            Me.XrLabel_method_first_contact.ForeColor = System.Drawing.SystemColors.WindowText
            Me.XrLabel_method_first_contact.LocationFloat = New DevExpress.Utils.PointFloat(705.0!, 0.0!)
            Me.XrLabel_method_first_contact.Name = "XrLabel_method_first_contact"
            Me.XrLabel_method_first_contact.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_method_first_contact.SizeF = New System.Drawing.SizeF(23.0!, 15.0!)
            Me.XrLabel_method_first_contact.StylePriority.UseFont = False
            Me.XrLabel_method_first_contact.StylePriority.UseForeColor = False
            Me.XrLabel_method_first_contact.StylePriority.UseTextAlignment = False
            Me.XrLabel_method_first_contact.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            '
            'XrLabel_indicator_1
            '
            Me.XrLabel_indicator_1.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "indicator_1")})
            Me.XrLabel_indicator_1.Font = New System.Drawing.Font("Times New Roman", 9.75!)
            Me.XrLabel_indicator_1.ForeColor = System.Drawing.SystemColors.WindowText
            Me.XrLabel_indicator_1.LocationFloat = New DevExpress.Utils.PointFloat(916.5!, 0.0!)
            Me.XrLabel_indicator_1.Name = "XrLabel_indicator_1"
            Me.XrLabel_indicator_1.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_indicator_1.SizeF = New System.Drawing.SizeF(16.0!, 15.0!)
            Me.XrLabel_indicator_1.StylePriority.UseFont = False
            Me.XrLabel_indicator_1.StylePriority.UseForeColor = False
            Me.XrLabel_indicator_1.StylePriority.UseTextAlignment = False
            Me.XrLabel_indicator_1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
            '
            'BankruptcyByApptReport
            '
            Me.Bands.AddRange(New DevExpress.XtraReports.UI.Band() {Me.Detail, Me.PageHeader, Me.PageFooter, Me.TopMarginBand1, Me.BottomMarginBand1, Me.ReportFooter})
            Me.DesignerOptions.ShowExportWarnings = False
            Me.DesignerOptions.ShowPrintingWarnings = False
            Me.Landscape = True
            Me.PageHeight = 850
            Me.PageWidth = 1100
            Me.ScriptLanguage = DevExpress.XtraReports.ScriptLanguage.VisualBasic
            Me.ScriptReferencesString = resources.GetString("$this.ScriptReferencesString")
            Me.Scripts.OnBeforePrint = "BankruptcyByApptReport_BeforePrint"
            Me.ScriptsSource = resources.GetString("$this.ScriptsSource")
            Me.StyleSheet.AddRange(New DevExpress.XtraReports.UI.XRControlStyle() {Me.XrControlStyle_Header, Me.XrControlStyle_Totals, Me.XrControlStyle_HeaderPannel, Me.XrControlStyle_GroupHeader})
            Me.Version = "11.2"
            Me.Controls.SetChildIndex(Me.ReportFooter, 0)
            Me.Controls.SetChildIndex(Me.BottomMarginBand1, 0)
            Me.Controls.SetChildIndex(Me.TopMarginBand1, 0)
            Me.Controls.SetChildIndex(Me.PageFooter, 0)
            Me.Controls.SetChildIndex(Me.PageHeader, 0)
            Me.Controls.SetChildIndex(Me.Detail, 0)
            CType(Me, System.ComponentModel.ISupportInitialize).EndInit()

        End Sub

        Friend WithEvents XrPanel1 As DevExpress.XtraReports.UI.XRPanel
        Friend WithEvents XrLabel1 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel6 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel5 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel4 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLine1 As DevExpress.XtraReports.UI.XRLine
        Friend WithEvents XrLabel18 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel15 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel13 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel9 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_people As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_appt_time As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_appt_type As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_city_state_zip As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_bankruptcy_district_number As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_referred_by As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_indicator_2 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_client As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_method_first_contact As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_indicator_1 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel10 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_total_certificate_count As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_total_people_count As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel8 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel3 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_notes As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents ReportFooter As DevExpress.XtraReports.UI.ReportFooterBand
    End Class
End Namespace
