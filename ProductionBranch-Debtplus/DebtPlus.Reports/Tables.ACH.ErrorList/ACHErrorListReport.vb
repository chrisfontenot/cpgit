#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}
#End Region

Option Compare Binary
Option Explicit On 
Option Strict On

Imports System.Data
Imports DebtPlus.Interfaces.Reports

Namespace Tables.ACH.ErrorList
    Public Class ACHErrorListReport
        Inherits DebtPlus.Reports.Template.TemplateXtraReportClass

        Public Overrides ReadOnly Property ReportTitle() As String
            Get
                Return "ACH Errors"
            End Get
        End Property


        ''' <summary>
        ''' Create an instance of our report
        ''' </summary>
        Public Sub New()
            MyBase.new()
            InitializeComponent()
            AddHandler BeforePrint, AddressOf Report_BeforePrint
            AddHandler XrLabel_Serious.BeforePrint, AddressOf XrLabel_Serious_BeforePrint

            ' indicate that we offer support for the report filter item
            CType(Me, DebtPlus.Interfaces.Reports.IReportFilter).ReportFilter.IsEnabled = True
        End Sub

#Region " Component Designer generated code "

        'Component overrides dispose to clean up the component list.
        Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
            If disposing Then
                If components IsNot Nothing Then
                    components.Dispose()
                End If
            End If
            MyBase.Dispose(disposing)
        End Sub

        'Required by the Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Designer
        'It can be modified using the Designer.
        'Do not modify it using the code editor.
        Friend WithEvents XrLabel_ErrorCode As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_Description As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_Serious As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrPanel1 As DevExpress.XtraReports.UI.XRPanel
        Friend WithEvents XrLabel3 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel2 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel1 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel4 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_LetterCode As DevExpress.XtraReports.UI.XRLabel
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
            Me.XrLabel_ErrorCode = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_Description = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_Serious = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_LetterCode = New DevExpress.XtraReports.UI.XRLabel
            Me.XrPanel1 = New DevExpress.XtraReports.UI.XRPanel
            Me.XrLabel3 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel2 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel1 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel4 = New DevExpress.XtraReports.UI.XRLabel
            CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
            '
            'Detail
            '
            Me.Detail.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel_LetterCode, Me.XrLabel_Serious, Me.XrLabel_Description, Me.XrLabel_ErrorCode})
            Me.Detail.HeightF = 17.0!
            '
            'PageHeader
            '
            Me.PageHeader.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrPanel1})
            Me.PageHeader.HeightF = 142.0!
            Me.PageHeader.Controls.SetChildIndex(Me.XrPanel1, 0)
            Me.PageHeader.Controls.SetChildIndex(Me.XrPanel_AgencyAddress, 0)
            Me.PageHeader.Controls.SetChildIndex(Me.XrLabel_Title, 0)
            Me.PageHeader.Controls.SetChildIndex(Me.XrLine_Title, 0)
            Me.PageHeader.Controls.SetChildIndex(Me.XrLabel_Subtitle, 0)
            Me.PageHeader.Controls.SetChildIndex(Me.XrPageInfo1, 0)
            '
            'XrLabel_Title
            '
            Me.XrLabel_Title.StylePriority.UseFont = False
            '
            'XrPageInfo_PageNumber
            '
            Me.XrPageInfo_PageNumber.StylePriority.UseTextAlignment = False
            '
            'XRLabel_Agency_Name
            '
            Me.XRLabel_Agency_Name.StylePriority.UseTextAlignment = False
            '
            'XrLabel_Agency_Address3
            '
            Me.XrLabel_Agency_Address3.StylePriority.UseFont = False
            Me.XrLabel_Agency_Address3.StylePriority.UseTextAlignment = False
            '
            'XrLabel_Agency_Address1
            '
            Me.XrLabel_Agency_Address1.StylePriority.UseFont = False
            Me.XrLabel_Agency_Address1.StylePriority.UseTextAlignment = False
            '
            'XrLabel_Agency_Phone
            '
            Me.XrLabel_Agency_Phone.StylePriority.UseFont = False
            Me.XrLabel_Agency_Phone.StylePriority.UseTextAlignment = False
            '
            'XrLabel_Agency_Address2
            '
            Me.XrLabel_Agency_Address2.StylePriority.UseFont = False
            Me.XrLabel_Agency_Address2.StylePriority.UseTextAlignment = False
            '
            'XrLabel_ErrorCode
            '
            Me.XrLabel_ErrorCode.LocationFloat = New DevExpress.Utils.PointFloat(16.0!, 0.0!)
            Me.XrLabel_ErrorCode.Name = "XrLabel_ErrorCode"
            Me.XrLabel_ErrorCode.SizeF = New System.Drawing.SizeF(100.0!, 15.0!)
            Me.XrLabel_ErrorCode.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
            Me.XrLabel_ErrorCode.DataBindings.Add("Text", Nothing, "ach_reject_code")
            '
            'XrLabel_Description
            '
            Me.XrLabel_Description.LocationFloat = New DevExpress.Utils.PointFloat(125.0!, 0.0!)
            Me.XrLabel_Description.Name = "XrLabel_Description"
            Me.XrLabel_Description.SizeF = New System.Drawing.SizeF(390.0!, 15.0!)
            Me.XrLabel_Description.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            Me.XrLabel_Description.DataBindings.Add("Text", Nothing, "description")
            '
            'XrLabel_Serious
            '
            Me.XrLabel_Serious.LocationFloat = New DevExpress.Utils.PointFloat(537.0!, 0.0!)
            Me.XrLabel_Serious.Name = "XrLabel_Serious"
            Me.XrLabel_Serious.SizeF = New System.Drawing.SizeF(74.0!, 15.0!)
            Me.XrLabel_Serious.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
            '
            'XrLabel_LetterCode
            '
            Me.XrLabel_LetterCode.LocationFloat = New DevExpress.Utils.PointFloat(625.0!, 0.0!)
            Me.XrLabel_LetterCode.Name = "XrLabel_LetterCode"
            Me.XrLabel_LetterCode.SizeF = New System.Drawing.SizeF(165.0!, 15.0!)
            Me.XrLabel_LetterCode.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            Me.XrLabel_LetterCode.DataBindings.Add("Text", Nothing, "letter_code")
            '
            'XrPanel1
            '
            Me.XrPanel1.BackColor = System.Drawing.Color.Teal
            Me.XrPanel1.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel3, Me.XrLabel2, Me.XrLabel1, Me.XrLabel4})
            Me.XrPanel1.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 117.0!)
            Me.XrPanel1.Name = "XrPanel1"
            Me.XrPanel1.SizeF = New System.Drawing.SizeF(800.0!, 17.0!)
            '
            'XrLabel3
            '
            Me.XrLabel3.BorderColor = System.Drawing.Color.Transparent
            Me.XrLabel3.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel3.ForeColor = System.Drawing.Color.White
            Me.XrLabel3.LocationFloat = New DevExpress.Utils.PointFloat(533.0!, 0.0!)
            Me.XrLabel3.Name = "XrLabel3"
            Me.XrLabel3.SizeF = New System.Drawing.SizeF(74.0!, 15.0!)
            Me.XrLabel3.Text = "SERIOUS"
            Me.XrLabel3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
            '
            'XrLabel2
            '
            Me.XrLabel2.BorderColor = System.Drawing.Color.Transparent
            Me.XrLabel2.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel2.ForeColor = System.Drawing.Color.White
            Me.XrLabel2.LocationFloat = New DevExpress.Utils.PointFloat(125.0!, 0.0!)
            Me.XrLabel2.Name = "XrLabel2"
            Me.XrLabel2.SizeF = New System.Drawing.SizeF(390.0!, 15.0!)
            Me.XrLabel2.Text = "DESCRIPTION"
            Me.XrLabel2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            '
            'XrLabel1
            '
            Me.XrLabel1.BorderColor = System.Drawing.Color.Transparent
            Me.XrLabel1.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel1.ForeColor = System.Drawing.Color.White
            Me.XrLabel1.LocationFloat = New DevExpress.Utils.PointFloat(17.0!, 0.0!)
            Me.XrLabel1.Name = "XrLabel1"
            Me.XrLabel1.SizeF = New System.Drawing.SizeF(100.0!, 15.0!)
            Me.XrLabel1.Text = "ERROR CODE"
            Me.XrLabel1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
            '
            'XrLabel4
            '
            Me.XrLabel4.BorderColor = System.Drawing.Color.Transparent
            Me.XrLabel4.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel4.ForeColor = System.Drawing.Color.White
            Me.XrLabel4.LocationFloat = New DevExpress.Utils.PointFloat(625.0!, 0.0!)
            Me.XrLabel4.Name = "XrLabel4"
            Me.XrLabel4.SizeF = New System.Drawing.SizeF(167.0!, 15.0!)
            Me.XrLabel4.Text = "LETTER CODE"
            Me.XrLabel4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            '
            Me.XrLabel_ErrorCode.DataBindings.Add("Text", Nothing, "ach_reject_code")
            Me.XrLabel_Description.DataBindings.Add("Text", Nothing, "description")
            Me.XrLabel_LetterCode.DataBindings.Add("Text", Nothing, "letter_code")
            '
            'ACHErrorListReport
            '
            Me.Bands.AddRange(New DevExpress.XtraReports.UI.Band() {Me.Detail, Me.PageHeader, Me.PageFooter, Me.TopMarginBand1, Me.BottomMarginBand1})
            Me.RequestParameters = False
            Me.StyleSheet.AddRange(New DevExpress.XtraReports.UI.XRControlStyle() {Me.XrControlStyle_Header, Me.XrControlStyle_Totals, Me.XrControlStyle_HeaderPannel, Me.XrControlStyle_GroupHeader})
            Me.Version = "10.2"
            CType(Me, System.ComponentModel.ISupportInitialize).EndInit()
        End Sub

#End Region

        ' Collection of records for the report.
        Dim ds As New System.Data.DataSet("ds")

        Private Sub Report_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs)
            Dim rpt As DevExpress.XtraReports.UI.XtraReport = CType(sender, DevExpress.XtraReports.UI.XtraReport)
            Dim TableName As String = "ach_reject_codes"
            Dim tbl As System.Data.DataTable = ds.Tables(TableName)

            If tbl Is Nothing Then
                Using cmd As SqlClient.SqlCommand = New System.Data.SqlClient.SqlCommand()
                    With cmd
                        .Connection = New System.Data.SqlClient.SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
                        .CommandText = "SELECT [ach_reject_code],[description],[serious_error],[letter_code] FROM ach_reject_codes WITH (NOLOCK)"
                        .CommandType = CommandType.Text
                    End With

                    Using da As New SqlClient.SqlDataAdapter(cmd)
                        da.Fill(ds, TableName)
                        tbl = ds.Tables(TableName)
                    End Using
                End Using
            End If

            ' Get the connection string and a session for the report
            rpt.DataSource = New System.Data.DataView(tbl, CType(rpt, DebtPlus.Interfaces.Reports.IReportFilter).ReportFilter.ViewFilter, "ach_reject_code", DataViewRowState.CurrentRows)
        End Sub

        ''' <summary>
        ''' Format the "serious error" column into Y or N rather than True / False
        ''' </summary>
        ''' <param name="sender"></param>
        ''' <param name="e"></param>
        ''' <remarks></remarks>
        Private Sub XrLabel_Serious_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs)
            With CType(sender, DevExpress.XtraReports.UI.XRLabel)
                Dim rpt As DevExpress.XtraReports.UI.XtraReport = CType(.Report, DevExpress.XtraReports.UI.XtraReport)
                Dim drv As System.Data.DataRowView = TryCast(rpt.GetCurrentRow, System.Data.DataRowView)
                If drv IsNot Nothing Then
                    .Text = If(DebtPlus.Utils.Nulls.DBool(drv("serious_error")), "Y", "N")
                Else
                    .Text = String.Empty
                End If
            End With
        End Sub
    End Class
End Namespace
