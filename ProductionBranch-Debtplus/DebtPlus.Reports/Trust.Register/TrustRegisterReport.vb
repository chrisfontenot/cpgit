#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}
#End Region

Option Compare Binary
Option Explicit On 
Option Strict On

Imports DebtPlus.Reports.Template
Imports DevExpress.XtraReports.UI

Namespace Trust.Register

    Public Class TrustRegisterReport
        Inherits DatedTemplateXtraReportClass

        ''' <summary>
        ''' Create an instance of our report
        ''' </summary>
        Public Sub New()
            MyBase.New()
            InitializeComponent()
            AddHandler Me.BeforePrint, AddressOf TrustRegisterReport_BeforePrint

            ReportFilter.IsEnabled = True
        End Sub

        Public Overrides ReadOnly Property ReportTitle() As String
            Get
                Return "Trust Register"
            End Get
        End Property

#Region " Component Designer generated code "

        'Component overrides dispose to clean up the component list.
        Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
            If disposing Then
                If components IsNot Nothing Then
                    components.Dispose()
                End If
            End If
            MyBase.Dispose(disposing)
        End Sub

        'Required by the Designer
        Private components As System.ComponentModel.IContainer
        Friend WithEvents XrPanel1 As DevExpress.XtraReports.UI.XRPanel
        Friend WithEvents XrLabel1 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel7 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel6 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel5 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel4 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel3 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel2 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_checknum As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_credit_amt As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_payee As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_reconciled_date As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_cleared As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_date_created As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_debit_amt As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLine1 As DevExpress.XtraReports.UI.XRLine
        Friend WithEvents XrTable1 As DevExpress.XtraReports.UI.XRTable
        Friend WithEvents XrTableRow1 As DevExpress.XtraReports.UI.XRTableRow
        Friend WithEvents XrTableCell1 As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents XrTableCell_total_credit As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents XrTableRow5 As DevExpress.XtraReports.UI.XRTableRow
        Friend WithEvents XrTableCell9 As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents XrTableCell_total_debit As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents XrTableRow4 As DevExpress.XtraReports.UI.XRTableRow
        Friend WithEvents XrTableCell7 As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents XrTableCell_total_void As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents XrTableRow3 As DevExpress.XtraReports.UI.XRTableRow
        Friend WithEvents XrTableCell5 As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents XrTableCell_total_destroyed As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents XrTableRow2 As DevExpress.XtraReports.UI.XRTableRow
        Friend WithEvents XrTableCell3 As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents XrTableCell_net_debit_amt As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents GroupHeader1 As DevExpress.XtraReports.UI.GroupHeaderBand
        Friend WithEvents XrLabel_bank_name As DevExpress.XtraReports.UI.XRLabel

        'NOTE: The following procedure is required by the Designer
        'It can be modified using the Designer.
        'Do not modify it using the code editor.
        Friend WithEvents ReportFooter As DevExpress.XtraReports.UI.ReportFooterBand
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
            Me.ReportFooter = New DevExpress.XtraReports.UI.ReportFooterBand
            Me.XrLine1 = New DevExpress.XtraReports.UI.XRLine
            Me.XrTable1 = New DevExpress.XtraReports.UI.XRTable
            Me.XrTableRow1 = New DevExpress.XtraReports.UI.XRTableRow
            Me.XrTableCell1 = New DevExpress.XtraReports.UI.XRTableCell
            Me.XrTableCell_total_credit = New DevExpress.XtraReports.UI.XRTableCell
            Me.XrTableRow5 = New DevExpress.XtraReports.UI.XRTableRow
            Me.XrTableCell9 = New DevExpress.XtraReports.UI.XRTableCell
            Me.XrTableCell_total_debit = New DevExpress.XtraReports.UI.XRTableCell
            Me.XrTableRow4 = New DevExpress.XtraReports.UI.XRTableRow
            Me.XrTableCell7 = New DevExpress.XtraReports.UI.XRTableCell
            Me.XrTableCell_total_void = New DevExpress.XtraReports.UI.XRTableCell
            Me.XrTableRow3 = New DevExpress.XtraReports.UI.XRTableRow
            Me.XrTableCell5 = New DevExpress.XtraReports.UI.XRTableCell
            Me.XrTableCell_total_destroyed = New DevExpress.XtraReports.UI.XRTableCell
            Me.XrTableRow2 = New DevExpress.XtraReports.UI.XRTableRow
            Me.XrTableCell3 = New DevExpress.XtraReports.UI.XRTableCell
            Me.XrTableCell_net_debit_amt = New DevExpress.XtraReports.UI.XRTableCell
            Me.XrPanel1 = New DevExpress.XtraReports.UI.XRPanel
            Me.XrLabel7 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel6 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel5 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel4 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel3 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel2 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel1 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_debit_amt = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_date_created = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_cleared = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_reconciled_date = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_payee = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_credit_amt = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_checknum = New DevExpress.XtraReports.UI.XRLabel
            Me.GroupHeader1 = New DevExpress.XtraReports.UI.GroupHeaderBand
            Me.XrLabel_bank_name = New DevExpress.XtraReports.UI.XRLabel
            CType(Me.XrTable1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
            '
            'Detail
            '
            Me.Detail.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel_checknum, Me.XrLabel_credit_amt, Me.XrLabel_payee, Me.XrLabel_reconciled_date, Me.XrLabel_cleared, Me.XrLabel_date_created, Me.XrLabel_debit_amt})
            Me.Detail.Height = 15
            '
            'PageHeader
            '
            Me.PageHeader.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrPanel1})
            Me.PageHeader.Height = 149
            Me.PageHeader.Controls.SetChildIndex(Me.XrPanel1, 0)
            Me.PageHeader.Controls.SetChildIndex(Me.XrPanel_AgencyAddress, 0)
            Me.PageHeader.Controls.SetChildIndex(Me.XrLabel_Title, 0)
            Me.PageHeader.Controls.SetChildIndex(Me.XrLine_Title, 0)
            Me.PageHeader.Controls.SetChildIndex(Me.XrLabel_Subtitle, 0)
            Me.PageHeader.Controls.SetChildIndex(Me.XrPageInfo1, 0)
            '
            'ReportFooter
            '
            Me.ReportFooter.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLine1, Me.XrTable1})
            Me.ReportFooter.Height = 167
            Me.ReportFooter.KeepTogether = True
            Me.ReportFooter.Name = "ReportFooter"
            '
            'XrLine1
            '
            Me.XrLine1.BorderWidth = 2
            Me.XrLine1.ForeColor = System.Drawing.Color.Teal
            Me.XrLine1.Location = New System.Drawing.Point(0, 0)
            Me.XrLine1.Name = "XrLine1"
            Me.XrLine1.Size = New System.Drawing.Size(775, 17)
            Me.XrLine1.StylePriority.UseBorderWidth = False
            Me.XrLine1.StylePriority.UseForeColor = False
            '
            'XrTable1
            '
            Me.XrTable1.KeepTogether = True
            Me.XrTable1.Location = New System.Drawing.Point(0, 33)
            Me.XrTable1.Name = "XrTable1"
            Me.XrTable1.Rows.AddRange(New DevExpress.XtraReports.UI.XRTableRow() {Me.XrTableRow1, Me.XrTableRow5, Me.XrTableRow4, Me.XrTableRow3, Me.XrTableRow2})
            Me.XrTable1.Size = New System.Drawing.Size(533, 125)
            '
            'XrTableRow1
            '
            Me.XrTableRow1.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.XrTableCell1, Me.XrTableCell_total_credit})
            Me.XrTableRow1.Name = "XrTableRow1"
            Me.XrTableRow1.Weight = 1
            '
            'XrTableCell1
            '
            Me.XrTableCell1.CanShrink = True
            Me.XrTableCell1.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrTableCell1.ForeColor = System.Drawing.Color.Teal
            Me.XrTableCell1.Name = "XrTableCell1"
            Me.XrTableCell1.ProcessNullValues = DevExpress.XtraReports.UI.ValueSuppressType.SuppressAndShrink
            Me.XrTableCell1.StylePriority.UseFont = False
            Me.XrTableCell1.StylePriority.UseForeColor = False
            Me.XrTableCell1.StylePriority.UseTextAlignment = False
            Me.XrTableCell1.Text = "Total of credit amounts to the trust"
            Me.XrTableCell1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            Me.XrTableCell1.Weight = 1
            '
            'XrTableCell_total_credit
            '
            Me.XrTableCell_total_credit.CanShrink = True
            Me.XrTableCell_total_credit.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrTableCell_total_credit.Name = "XrTableCell_total_credit"
            Me.XrTableCell_total_credit.ProcessNullValues = DevExpress.XtraReports.UI.ValueSuppressType.SuppressAndShrink
            Me.XrTableCell_total_credit.StylePriority.UseFont = False
            Me.XrTableCell_total_credit.StylePriority.UseTextAlignment = False
            Me.XrTableCell_total_credit.Text = "$0.00"
            Me.XrTableCell_total_credit.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrTableCell_total_credit.Weight = 1
            '
            'XrTableRow5
            '
            Me.XrTableRow5.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.XrTableCell9, Me.XrTableCell_total_debit})
            Me.XrTableRow5.Name = "XrTableRow5"
            Me.XrTableRow5.Weight = 1
            '
            'XrTableCell9
            '
            Me.XrTableCell9.CanShrink = True
            Me.XrTableCell9.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrTableCell9.ForeColor = System.Drawing.Color.Teal
            Me.XrTableCell9.Name = "XrTableCell9"
            Me.XrTableCell9.ProcessNullValues = DevExpress.XtraReports.UI.ValueSuppressType.SuppressAndShrink
            Me.XrTableCell9.StylePriority.UseFont = False
            Me.XrTableCell9.StylePriority.UseForeColor = False
            Me.XrTableCell9.StylePriority.UseTextAlignment = False
            Me.XrTableCell9.Text = "GROSS debit amounts to the trust"
            Me.XrTableCell9.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            Me.XrTableCell9.Weight = 1
            '
            'XrTableCell_total_debit
            '
            Me.XrTableCell_total_debit.CanShrink = True
            Me.XrTableCell_total_debit.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrTableCell_total_debit.Name = "XrTableCell_total_debit"
            Me.XrTableCell_total_debit.ProcessNullValues = DevExpress.XtraReports.UI.ValueSuppressType.SuppressAndShrink
            Me.XrTableCell_total_debit.StylePriority.UseFont = False
            Me.XrTableCell_total_debit.StylePriority.UseTextAlignment = False
            Me.XrTableCell_total_debit.Text = "$0.00"
            Me.XrTableCell_total_debit.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrTableCell_total_debit.Weight = 1
            '
            'XrTableRow4
            '
            Me.XrTableRow4.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.XrTableCell7, Me.XrTableCell_total_void})
            Me.XrTableRow4.Name = "XrTableRow4"
            Me.XrTableRow4.Weight = 1
            '
            'XrTableCell7
            '
            Me.XrTableCell7.CanShrink = True
            Me.XrTableCell7.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrTableCell7.ForeColor = System.Drawing.Color.Teal
            Me.XrTableCell7.Name = "XrTableCell7"
            Me.XrTableCell7.ProcessNullValues = DevExpress.XtraReports.UI.ValueSuppressType.SuppressAndShrink
            Me.XrTableCell7.StylePriority.UseFont = False
            Me.XrTableCell7.StylePriority.UseForeColor = False
            Me.XrTableCell7.StylePriority.UseTextAlignment = False
            Me.XrTableCell7.Text = "LESS checks VOIDED"
            Me.XrTableCell7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            Me.XrTableCell7.Weight = 1
            '
            'XrTableCell_total_void
            '
            Me.XrTableCell_total_void.CanShrink = True
            Me.XrTableCell_total_void.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrTableCell_total_void.ForeColor = System.Drawing.Color.Red
            Me.XrTableCell_total_void.Name = "XrTableCell_total_void"
            Me.XrTableCell_total_void.ProcessNullValues = DevExpress.XtraReports.UI.ValueSuppressType.SuppressAndShrink
            Me.XrTableCell_total_void.StylePriority.UseFont = False
            Me.XrTableCell_total_void.StylePriority.UseForeColor = False
            Me.XrTableCell_total_void.StylePriority.UseTextAlignment = False
            Me.XrTableCell_total_void.Text = "$0.00"
            Me.XrTableCell_total_void.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrTableCell_total_void.Weight = 1
            '
            'XrTableRow3
            '
            Me.XrTableRow3.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.XrTableCell5, Me.XrTableCell_total_destroyed})
            Me.XrTableRow3.Name = "XrTableRow3"
            Me.XrTableRow3.Weight = 1
            '
            'XrTableCell5
            '
            Me.XrTableCell5.CanShrink = True
            Me.XrTableCell5.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrTableCell5.ForeColor = System.Drawing.Color.Teal
            Me.XrTableCell5.Name = "XrTableCell5"
            Me.XrTableCell5.ProcessNullValues = DevExpress.XtraReports.UI.ValueSuppressType.SuppressAndShrink
            Me.XrTableCell5.StylePriority.UseFont = False
            Me.XrTableCell5.StylePriority.UseForeColor = False
            Me.XrTableCell5.StylePriority.UseTextAlignment = False
            Me.XrTableCell5.Text = "Less checks DESTROYED"
            Me.XrTableCell5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            Me.XrTableCell5.Weight = 1
            '
            'XrTableCell_total_destroyed
            '
            Me.XrTableCell_total_destroyed.CanShrink = True
            Me.XrTableCell_total_destroyed.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrTableCell_total_destroyed.ForeColor = System.Drawing.Color.Red
            Me.XrTableCell_total_destroyed.Name = "XrTableCell_total_destroyed"
            Me.XrTableCell_total_destroyed.ProcessNullValues = DevExpress.XtraReports.UI.ValueSuppressType.SuppressAndShrink
            Me.XrTableCell_total_destroyed.StylePriority.UseFont = False
            Me.XrTableCell_total_destroyed.StylePriority.UseForeColor = False
            Me.XrTableCell_total_destroyed.StylePriority.UseTextAlignment = False
            Me.XrTableCell_total_destroyed.Text = "$0.00"
            Me.XrTableCell_total_destroyed.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrTableCell_total_destroyed.Weight = 1
            '
            'XrTableRow2
            '
            Me.XrTableRow2.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.XrTableCell3, Me.XrTableCell_net_debit_amt})
            Me.XrTableRow2.Name = "XrTableRow2"
            Me.XrTableRow2.Weight = 1
            '
            'XrTableCell3
            '
            Me.XrTableCell3.CanShrink = True
            Me.XrTableCell3.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrTableCell3.ForeColor = System.Drawing.Color.Teal
            Me.XrTableCell3.Name = "XrTableCell3"
            Me.XrTableCell3.ProcessNullValues = DevExpress.XtraReports.UI.ValueSuppressType.SuppressAndShrink
            Me.XrTableCell3.StylePriority.UseFont = False
            Me.XrTableCell3.StylePriority.UseForeColor = False
            Me.XrTableCell3.StylePriority.UseTextAlignment = False
            Me.XrTableCell3.Text = "NET debit amounts to the trust"
            Me.XrTableCell3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            Me.XrTableCell3.Weight = 1
            '
            'XrTableCell_net_debit_amt
            '
            Me.XrTableCell_net_debit_amt.CanShrink = True
            Me.XrTableCell_net_debit_amt.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrTableCell_net_debit_amt.Name = "XrTableCell_net_debit_amt"
            Me.XrTableCell_net_debit_amt.ProcessNullValues = DevExpress.XtraReports.UI.ValueSuppressType.SuppressAndShrink
            Me.XrTableCell_net_debit_amt.StylePriority.UseFont = False
            Me.XrTableCell_net_debit_amt.StylePriority.UseTextAlignment = False
            Me.XrTableCell_net_debit_amt.Text = "$0.00"
            Me.XrTableCell_net_debit_amt.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrTableCell_net_debit_amt.Weight = 1
            '
            'XrPanel1
            '
            Me.XrPanel1.BackColor = System.Drawing.Color.Teal
            Me.XrPanel1.CanGrow = False
            Me.XrPanel1.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel7, Me.XrLabel6, Me.XrLabel5, Me.XrLabel4, Me.XrLabel3, Me.XrLabel2, Me.XrLabel1})
            Me.XrPanel1.Location = New System.Drawing.Point(0, 125)
            Me.XrPanel1.Name = "XrPanel1"
            Me.XrPanel1.Size = New System.Drawing.Size(800, 18)
            Me.XrPanel1.StylePriority.UseBackColor = False
            '
            'XrLabel7
            '
            Me.XrLabel7.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel7.ForeColor = System.Drawing.Color.White
            Me.XrLabel7.Location = New System.Drawing.Point(200, 0)
            Me.XrLabel7.Name = "XrLabel7"
            Me.XrLabel7.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel7.Size = New System.Drawing.Size(67, 15)
            Me.XrLabel7.StylePriority.UseFont = False
            Me.XrLabel7.StylePriority.UseForeColor = False
            Me.XrLabel7.StylePriority.UseTextAlignment = False
            Me.XrLabel7.Text = "RECON"
            Me.XrLabel7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel6
            '
            Me.XrLabel6.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel6.ForeColor = System.Drawing.Color.White
            Me.XrLabel6.Location = New System.Drawing.Point(658, 0)
            Me.XrLabel6.Name = "XrLabel6"
            Me.XrLabel6.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel6.Size = New System.Drawing.Size(116, 15)
            Me.XrLabel6.StylePriority.UseFont = False
            Me.XrLabel6.StylePriority.UseForeColor = False
            Me.XrLabel6.StylePriority.UseTextAlignment = False
            Me.XrLabel6.Text = "DEBIT"
            Me.XrLabel6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel5
            '
            Me.XrLabel5.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel5.ForeColor = System.Drawing.Color.White
            Me.XrLabel5.Location = New System.Drawing.Point(275, 0)
            Me.XrLabel5.Name = "XrLabel5"
            Me.XrLabel5.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel5.Size = New System.Drawing.Size(250, 15)
            Me.XrLabel5.StylePriority.UseFont = False
            Me.XrLabel5.StylePriority.UseForeColor = False
            Me.XrLabel5.StylePriority.UseTextAlignment = False
            Me.XrLabel5.Text = "PAYEE"
            Me.XrLabel5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            '
            'XrLabel4
            '
            Me.XrLabel4.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel4.ForeColor = System.Drawing.Color.White
            Me.XrLabel4.Location = New System.Drawing.Point(549, 0)
            Me.XrLabel4.Name = "XrLabel4"
            Me.XrLabel4.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel4.Size = New System.Drawing.Size(100, 15)
            Me.XrLabel4.StylePriority.UseFont = False
            Me.XrLabel4.StylePriority.UseForeColor = False
            Me.XrLabel4.StylePriority.UseTextAlignment = False
            Me.XrLabel4.Text = "CREDIT"
            Me.XrLabel4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel3
            '
            Me.XrLabel3.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel3.ForeColor = System.Drawing.Color.White
            Me.XrLabel3.Location = New System.Drawing.Point(167, 0)
            Me.XrLabel3.Name = "XrLabel3"
            Me.XrLabel3.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel3.Size = New System.Drawing.Size(33, 15)
            Me.XrLabel3.StylePriority.UseFont = False
            Me.XrLabel3.StylePriority.UseForeColor = False
            Me.XrLabel3.StylePriority.UseTextAlignment = False
            Me.XrLabel3.Text = "CLR"
            Me.XrLabel3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
            '
            'XrLabel2
            '
            Me.XrLabel2.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel2.ForeColor = System.Drawing.Color.White
            Me.XrLabel2.Location = New System.Drawing.Point(92, 0)
            Me.XrLabel2.Name = "XrLabel2"
            Me.XrLabel2.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel2.Size = New System.Drawing.Size(67, 15)
            Me.XrLabel2.StylePriority.UseFont = False
            Me.XrLabel2.StylePriority.UseForeColor = False
            Me.XrLabel2.StylePriority.UseTextAlignment = False
            Me.XrLabel2.Text = "DATE"
            Me.XrLabel2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel1
            '
            Me.XrLabel1.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel1.ForeColor = System.Drawing.Color.White
            Me.XrLabel1.Location = New System.Drawing.Point(0, 0)
            Me.XrLabel1.Name = "XrLabel1"
            Me.XrLabel1.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel1.Size = New System.Drawing.Size(83, 15)
            Me.XrLabel1.StylePriority.UseFont = False
            Me.XrLabel1.StylePriority.UseForeColor = False
            Me.XrLabel1.StylePriority.UseTextAlignment = False
            Me.XrLabel1.Text = "CHECK"
            Me.XrLabel1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel_debit_amt
            '
            Me.XrLabel_debit_amt.CanGrow = False
            Me.XrLabel_debit_amt.Font = New System.Drawing.Font("Times New Roman", 9.75!)
            Me.XrLabel_debit_amt.ForeColor = System.Drawing.SystemColors.WindowText
            Me.XrLabel_debit_amt.Location = New System.Drawing.Point(658, 0)
            Me.XrLabel_debit_amt.Name = "XrLabel_debit_amt"
            Me.XrLabel_debit_amt.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_debit_amt.Size = New System.Drawing.Size(116, 15)
            Me.XrLabel_debit_amt.StylePriority.UseFont = False
            Me.XrLabel_debit_amt.StylePriority.UseForeColor = False
            Me.XrLabel_debit_amt.StylePriority.UseTextAlignment = False
            Me.XrLabel_debit_amt.Text = " "
            Me.XrLabel_debit_amt.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrLabel_debit_amt.WordWrap = False
            '
            'XrLabel_date_created
            '
            Me.XrLabel_date_created.CanGrow = False
            Me.XrLabel_date_created.Font = New System.Drawing.Font("Times New Roman", 9.75!)
            Me.XrLabel_date_created.ForeColor = System.Drawing.SystemColors.WindowText
            Me.XrLabel_date_created.Location = New System.Drawing.Point(92, 0)
            Me.XrLabel_date_created.Name = "XrLabel_date_created"
            Me.XrLabel_date_created.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_date_created.Size = New System.Drawing.Size(67, 15)
            Me.XrLabel_date_created.StylePriority.UseFont = False
            Me.XrLabel_date_created.StylePriority.UseForeColor = False
            Me.XrLabel_date_created.StylePriority.UseTextAlignment = False
            Me.XrLabel_date_created.Text = " "
            Me.XrLabel_date_created.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrLabel_date_created.WordWrap = False
            '
            'XrLabel_cleared
            '
            Me.XrLabel_cleared.CanGrow = False
            Me.XrLabel_cleared.Font = New System.Drawing.Font("Times New Roman", 9.75!)
            Me.XrLabel_cleared.ForeColor = System.Drawing.SystemColors.WindowText
            Me.XrLabel_cleared.Location = New System.Drawing.Point(167, 0)
            Me.XrLabel_cleared.Name = "XrLabel_cleared"
            Me.XrLabel_cleared.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_cleared.Size = New System.Drawing.Size(33, 15)
            Me.XrLabel_cleared.StylePriority.UseFont = False
            Me.XrLabel_cleared.StylePriority.UseForeColor = False
            Me.XrLabel_cleared.StylePriority.UseTextAlignment = False
            Me.XrLabel_cleared.Text = " "
            Me.XrLabel_cleared.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
            Me.XrLabel_cleared.WordWrap = False
            '
            'XrLabel_reconciled_date
            '
            Me.XrLabel_reconciled_date.CanGrow = False
            Me.XrLabel_reconciled_date.Font = New System.Drawing.Font("Times New Roman", 9.75!)
            Me.XrLabel_reconciled_date.ForeColor = System.Drawing.SystemColors.WindowText
            Me.XrLabel_reconciled_date.Location = New System.Drawing.Point(200, 0)
            Me.XrLabel_reconciled_date.Name = "XrLabel_reconciled_date"
            Me.XrLabel_reconciled_date.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_reconciled_date.Size = New System.Drawing.Size(67, 15)
            Me.XrLabel_reconciled_date.StylePriority.UseFont = False
            Me.XrLabel_reconciled_date.StylePriority.UseForeColor = False
            Me.XrLabel_reconciled_date.StylePriority.UseTextAlignment = False
            Me.XrLabel_reconciled_date.Text = " "
            Me.XrLabel_reconciled_date.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrLabel_reconciled_date.WordWrap = False
            '
            'XrLabel_payee
            '
            Me.XrLabel_payee.Font = New System.Drawing.Font("Times New Roman", 9.75!)
            Me.XrLabel_payee.ForeColor = System.Drawing.SystemColors.WindowText
            Me.XrLabel_payee.Location = New System.Drawing.Point(275, 0)
            Me.XrLabel_payee.Name = "XrLabel_payee"
            Me.XrLabel_payee.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_payee.Size = New System.Drawing.Size(250, 15)
            Me.XrLabel_payee.StylePriority.UseFont = False
            Me.XrLabel_payee.StylePriority.UseForeColor = False
            Me.XrLabel_payee.StylePriority.UseTextAlignment = False
            Me.XrLabel_payee.Text = " "
            Me.XrLabel_payee.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            Me.XrLabel_payee.WordWrap = False
            '
            'XrLabel_credit_amt
            '
            Me.XrLabel_credit_amt.CanGrow = False
            Me.XrLabel_credit_amt.Font = New System.Drawing.Font("Times New Roman", 9.75!)
            Me.XrLabel_credit_amt.ForeColor = System.Drawing.SystemColors.WindowText
            Me.XrLabel_credit_amt.Location = New System.Drawing.Point(533, 0)
            Me.XrLabel_credit_amt.Name = "XrLabel_credit_amt"
            Me.XrLabel_credit_amt.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_credit_amt.Size = New System.Drawing.Size(116, 15)
            Me.XrLabel_credit_amt.StylePriority.UseFont = False
            Me.XrLabel_credit_amt.StylePriority.UseForeColor = False
            Me.XrLabel_credit_amt.StylePriority.UseTextAlignment = False
            Me.XrLabel_credit_amt.Text = " "
            Me.XrLabel_credit_amt.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrLabel_credit_amt.WordWrap = False
            '
            'XrLabel_checknum
            '
            Me.XrLabel_checknum.CanGrow = False
            Me.XrLabel_checknum.Font = New System.Drawing.Font("Times New Roman", 9.75!)
            Me.XrLabel_checknum.ForeColor = System.Drawing.SystemColors.WindowText
            Me.XrLabel_checknum.Location = New System.Drawing.Point(0, 0)
            Me.XrLabel_checknum.Name = "XrLabel_checknum"
            Me.XrLabel_checknum.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_checknum.Size = New System.Drawing.Size(83, 15)
            Me.XrLabel_checknum.StylePriority.UseFont = False
            Me.XrLabel_checknum.StylePriority.UseForeColor = False
            Me.XrLabel_checknum.StylePriority.UseTextAlignment = False
            Me.XrLabel_checknum.Text = " "
            Me.XrLabel_checknum.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrLabel_checknum.WordWrap = False
            '
            'GroupHeader1
            '
            Me.GroupHeader1.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel_bank_name})
            Me.GroupHeader1.GroupUnion = DevExpress.XtraReports.UI.GroupUnion.WithFirstDetail
            Me.GroupHeader1.Height = 47
            Me.GroupHeader1.Name = "GroupHeader1"
            Me.GroupHeader1.RepeatEveryPage = True
            '
            'XrLabel_bank_name
            '
            Me.XrLabel_bank_name.Font = New System.Drawing.Font("Times New Roman", 12.0!, System.Drawing.FontStyle.Bold)
            Me.XrLabel_bank_name.ForeColor = System.Drawing.Color.Maroon
            Me.XrLabel_bank_name.Location = New System.Drawing.Point(0, 8)
            Me.XrLabel_bank_name.Name = "XrLabel_bank_name"
            Me.XrLabel_bank_name.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_bank_name.Size = New System.Drawing.Size(800, 25)
            Me.XrLabel_bank_name.StylePriority.UseFont = False
            Me.XrLabel_bank_name.StylePriority.UseForeColor = False
            Me.XrLabel_bank_name.Text = "BANK: [bank_name]"
            '
            Me.XrLabel_checknum.DataBindings.Add("Text", Nothing, "checknum")
            Me.XrLabel_credit_amt.DataBindings.Add("Text", Nothing, "credit_amt", "{0:c}")
            Me.XrLabel_date_created.DataBindings.Add("Text", Nothing, "date_created", "{0:d}")
            Me.XrLabel_debit_amt.DataBindings.Add("Text", Nothing, "debit_amt", "{0:c}")
            Me.XrLabel_cleared.DataBindings.Add("Text", Nothing, "cleared")
            Me.XrLabel_payee.DataBindings.Add("Text", Nothing, "payee")
            Me.XrLabel_reconciled_date.DataBindings.Add("Text", Nothing, "reconciled_date", "{0:d}")
            '
            'TrustRegisterReport
            '
            Me.Bands.AddRange(New DevExpress.XtraReports.UI.Band() {Me.Detail, Me.PageHeader, Me.PageFooter, Me.ReportFooter, Me.GroupHeader1})
            Me.Version = "8.2"
            Me.Controls.SetChildIndex(Me.GroupHeader1, 0)
            Me.Controls.SetChildIndex(Me.ReportFooter, 0)
            Me.Controls.SetChildIndex(Me.PageFooter, 0)
            Me.Controls.SetChildIndex(Me.PageHeader, 0)
            Me.Controls.SetChildIndex(Me.Detail, 0)
            CType(Me.XrTable1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me, System.ComponentModel.ISupportInitialize).EndInit()

        End Sub

#End Region

        Dim ds As New System.Data.DataSet("ds")
        Private Sub TrustRegisterReport_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs)
            Dim cn As SqlClient.SqlConnection = New System.Data.SqlClient.SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
            Dim Current_Cursor As System.Windows.Forms.Cursor = System.Windows.Forms.Cursor.Current

            Const BanksName As String = "banks"
            Const TableName As String = "transactions"
            Dim BankTbl As System.Data.DataTable
            Dim TransactionTbl As System.Data.DataTable

            Try
                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.WaitCursor
                cn.Open()

                BankTbl = ds.Tables(BanksName)
                If BankTbl Is Nothing Then
                    Using cmd As SqlClient.SqlCommand = New System.Data.SqlClient.SqlCommand()
                        With cmd
                            .Connection = cn
                            .CommandText = "SELECT bank,description FROM banks"
                            .CommandType = CommandType.Text
                        End With

                        Using da As New SqlClient.SqlDataAdapter(cmd)
                            da.Fill(ds, BanksName)

                            BankTbl = ds.Tables(BanksName)
                            With BankTbl
                                .PrimaryKey = New System.Data.DataColumn() {.Columns("bank")}
                            End With
                        End Using
                    End Using
                End If

                TransactionTbl = ds.Tables(TableName)
                If TransactionTbl Is Nothing Then
                    Using cmd As SqlClient.SqlCommand = New System.Data.SqlClient.SqlCommand()
                        With cmd
                            .Connection = cn
                            .CommandText = "SELECT [trust_register],[tran_type],[created_by],[date_created],[payee],[checknum],[cleared],[reconciled_date],[debit_amt],[credit_amt],[bank] FROM view_trust_register WHERE (date_created >= @FromDate) AND (date_created < @ToDate) ORDER BY [date_created], [checknum]"
                            .CommandType = CommandType.Text
                            .Parameters.Add("@FromDate", SqlDbType.DateTime).Value = Parameter_FromDate.Date
                            .Parameters.Add("@ToDate", SqlDbType.DateTime).Value = Parameter_ToDate.Date.AddDays(1)
                            .CommandTimeout = 0
                        End With

                        Using da As New SqlClient.SqlDataAdapter(cmd)
                            da.Fill(ds, TableName)
                            TransactionTbl = ds.Tables(TableName)
                        End Using
                    End Using
                End If

                ' Add a relation to the system linking the banks
                If Not ds.Relations.Contains("view_trust_register_banks") Then
                    Dim rel As New System.Data.DataRelation("view_trust_register_banks", BankTbl.Columns("bank"), TransactionTbl.Columns("bank"), False)
                    ds.Relations.Add(rel)
                End If

                ' Add a column for the transactions to list the bank name
                If Not TransactionTbl.Columns.Contains("bank_name") Then
                    TransactionTbl.Columns.Add("bank_name", GetType(String), "Parent.description")
                    TransactionTbl.Columns.Add("sort_order", GetType(Int32), "iif([tran_type]='DP',1,iif([tran_type]='RR' OR [tran_type]='VD' OR [tran_type]='RF',2,3))")
                End If

                DataSource = New System.Data.DataView(TransactionTbl, ReportFilter.ViewFilter, "bank, sort_order, date_created, checknum", DataViewRowState.CurrentRows)

                ' Add a group field for the bank
                Dim XRGroupField1 As New DevExpress.XtraReports.UI.GroupField("bank", XRColumnSortOrder.Ascending)
                GroupHeader1.GroupFields.Clear()
                GroupHeader1.GroupFields.Add(XRGroupField1)

                ' Find the total credit amount
                Dim decimal_value As Decimal = DebtPlus.Utils.Nulls.DDec(TransactionTbl.Compute("sum(credit_amt)", String.Empty))
                With XrTableCell_total_credit
                    .Text = String.Format("{0:c}", decimal_value)
                End With

                ' Find the total debit amount
                Dim Gross_debit As Decimal = DebtPlus.Utils.Nulls.DDec(TransactionTbl.Compute("sum(debit_amt)", String.Empty))
                With XrTableCell_total_debit
                    .Text = String.Format("{0:c}", Gross_debit)
                End With

                ' Find the total debit amount of destroyed checks
                Dim total_destroyed As Decimal = DebtPlus.Utils.Nulls.DDec(TransactionTbl.Compute("sum(debit_amt)", "[cleared]='D'"))
                If total_destroyed <= 0D Then
                    XrTableRow3.Visible = False
                Else
                    With XrTableCell_total_destroyed
                        .Text = String.Format("{0:c}", 0D - total_destroyed)
                    End With
                End If

                ' Find the total debit amount of voided checks
                Dim total_voided As Decimal = DebtPlus.Utils.Nulls.DDec(TransactionTbl.Compute("sum(debit_amt)", "[cleared]='V'"))
                If total_voided <= 0D Then
                    XrTableRow4.Visible = False
                Else
                    With XrTableCell_total_void
                        .Text = String.Format("{0:c}", 0D - total_voided)
                    End With
                End If

                ' Calculate the total amount of the debits
                If total_destroyed > 0D OrElse total_voided > 0D Then
                    Dim Net_debit As Decimal = Gross_debit - total_destroyed - total_voided
                    With XrTableCell_net_debit_amt
                        .Text = String.Format("{0:c}", Net_debit)
                    End With
                Else
                    XrTableRow2.Visible = False
                End If

            Catch ex As SqlClient.SqlException
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Database Error")

            Finally
                If cn IsNot Nothing Then cn.Dispose()
                System.Windows.Forms.Cursor.Current = Current_Cursor
            End Try
        End Sub
    End Class
End Namespace
