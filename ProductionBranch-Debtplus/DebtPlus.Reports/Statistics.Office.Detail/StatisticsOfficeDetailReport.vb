#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}
#End Region

Option Compare Binary
Option Explicit On
Option Strict On

Imports DevExpress.XtraReports.UI

Namespace Statistics.Office.Detail
    Public Class StatisticsCounselorReport
        Inherits Template.TemplateXtraReportClass

        ''' <summary>
        ''' Create an instance of our report
        ''' </summary>
        Public Sub New()
            MyBase.new()
            InitializeComponent()
            AddHandler Me.BeforePrint, AddressOf TrustBalanceReportClass_BeforePrint
            AddHandler XrLabel_pct_disb.BeforePrint, AddressOf XrLabel_pct_disb_BeforePrint
        End Sub

#Region " Component Designer generated code "

        'Component overrides dispose to clean up the component list.
        Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
            If disposing Then
                If components IsNot Nothing Then
                    components.Dispose()
                End If
            End If
            MyBase.Dispose(disposing)
        End Sub
        Friend WithEvents GroupHeader1 As DevExpress.XtraReports.UI.GroupHeaderBand
        Friend WithEvents GroupFooter1 As DevExpress.XtraReports.UI.GroupFooterBand
        Friend WithEvents XrLabel6 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_clients_disbursed As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_actual_disbursement As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_expected_disbursement As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_active_clients As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_name As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_pct_disb As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_paf_fees As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_g1_actual_disbursement As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_g1_expected_disbursement As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_g1_clients_disbursed As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_g1_paf_fees As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_g1_active_clients As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_g1_name As DevExpress.XtraReports.UI.XRLabel

        'Required by the Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Designer
        'It can be modified using the Designer.
        'Do not modify it using the code editor.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
            Dim XrSummary1 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary
            Dim XrSummary2 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary
            Dim XrSummary3 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary
            Dim XrSummary4 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary
            Dim XrSummary5 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary
            Me.XrLabel1 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel2 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel3 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel4 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel5 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrPanel1 = New DevExpress.XtraReports.UI.XRPanel
            Me.XrLabel31 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel30 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel28 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel27 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel26 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel25 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel15 = New DevExpress.XtraReports.UI.XRLabel
            Me.GroupHeader1 = New DevExpress.XtraReports.UI.GroupHeaderBand
            Me.GroupFooter1 = New DevExpress.XtraReports.UI.GroupFooterBand
            Me.XrLabel_g1_actual_disbursement = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_g1_expected_disbursement = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_g1_clients_disbursed = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_g1_paf_fees = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_g1_active_clients = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_g1_name = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel6 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_name = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_paf_fees = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_pct_disb = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_actual_disbursement = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_clients_disbursed = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_expected_disbursement = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_active_clients = New DevExpress.XtraReports.UI.XRLabel
            CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
            '
            'Detail
            '
            Me.Detail.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel_clients_disbursed, Me.XrLabel_actual_disbursement, Me.XrLabel_expected_disbursement, Me.XrLabel_active_clients, Me.XrLabel_name, Me.XrLabel_pct_disb, Me.XrLabel_paf_fees})
            Me.Detail.HeightF = 15.0!
            '
            'XrLabel_Title
            '
            Me.XrLabel_Title.StylePriority.UseFont = False
            '
            'XrLabel_Subtitle
            '
            Me.XrLabel_Subtitle.LocationFloat = New DevExpress.Utils.PointFloat(7.999992!, 85.00001!)
            Me.XrLabel_Subtitle.SizeF = New System.Drawing.SizeF(706.0!, 17.00001!)
            '
            'XrPageInfo_PageNumber
            '
            Me.XrPageInfo_PageNumber.LocationFloat = New DevExpress.Utils.PointFloat(638.0417!, 10.00001!)
            Me.XrPageInfo_PageNumber.SizeF = New System.Drawing.SizeF(113.4167!, 17.00001!)
            Me.XrPageInfo_PageNumber.StylePriority.UseTextAlignment = False
            '
            'XrPanel_AgencyAddress
            '
            Me.XrPanel_AgencyAddress.LocationFloat = New DevExpress.Utils.PointFloat(443.4583!, 0.0!)
            '
            'XRLabel_Agency_Name
            '
            Me.XRLabel_Agency_Name.StylePriority.UseTextAlignment = False
            '
            'XrLabel_Agency_Address3
            '
            Me.XrLabel_Agency_Address3.StylePriority.UseFont = False
            Me.XrLabel_Agency_Address3.StylePriority.UseTextAlignment = False
            '
            'XrLabel_Agency_Address1
            '
            Me.XrLabel_Agency_Address1.StylePriority.UseFont = False
            Me.XrLabel_Agency_Address1.StylePriority.UseTextAlignment = False
            '
            'XrLabel_Agency_Phone
            '
            Me.XrLabel_Agency_Phone.StylePriority.UseFont = False
            Me.XrLabel_Agency_Phone.StylePriority.UseTextAlignment = False
            '
            'XrLabel_Agency_Address2
            '
            Me.XrLabel_Agency_Address2.StylePriority.UseFont = False
            Me.XrLabel_Agency_Address2.StylePriority.UseTextAlignment = False
            '
            'XrLabel1
            '
            Me.XrLabel1.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
            Me.XrLabel1.Name = "XrLabel1"
            Me.XrLabel1.SizeF = New System.Drawing.SizeF(100.0!, 23.0!)
            Me.XrLabel1.Text = "XrLabel1"
            '
            'XrLabel2
            '
            Me.XrLabel2.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
            Me.XrLabel2.Name = "XrLabel2"
            Me.XrLabel2.SizeF = New System.Drawing.SizeF(100.0!, 23.0!)
            Me.XrLabel2.Text = "XrLabel2"
            '
            'XrLabel3
            '
            Me.XrLabel3.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
            Me.XrLabel3.Name = "XrLabel3"
            Me.XrLabel3.SizeF = New System.Drawing.SizeF(100.0!, 23.0!)
            Me.XrLabel3.Text = "XrLabel3"
            '
            'XrLabel4
            '
            Me.XrLabel4.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
            Me.XrLabel4.Name = "XrLabel4"
            Me.XrLabel4.SizeF = New System.Drawing.SizeF(100.0!, 23.0!)
            Me.XrLabel4.Text = "XrLabel4"
            '
            'XrLabel5
            '
            Me.XrLabel5.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
            Me.XrLabel5.Name = "XrLabel5"
            Me.XrLabel5.SizeF = New System.Drawing.SizeF(100.0!, 23.0!)
            Me.XrLabel5.Text = "XrLabel5"
            '
            'XrPanel1
            '
            Me.XrPanel1.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel31, Me.XrLabel30, Me.XrLabel28, Me.XrLabel26, Me.XrLabel25, Me.XrLabel15, Me.XrLabel27})
            Me.XrPanel1.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 49.66666!)
            Me.XrPanel1.Name = "XrPanel1"
            Me.XrPanel1.SizeF = New System.Drawing.SizeF(751.4583!, 17.0!)
            Me.XrPanel1.StyleName = "XrControlStyle_HeaderPannel"
            '
            'XrLabel31
            '
            Me.XrLabel31.LocationFloat = New DevExpress.Utils.PointFloat(666.0!, 1.0!)
            Me.XrLabel31.Name = "XrLabel31"
            Me.XrLabel31.SizeF = New System.Drawing.SizeF(75.0!, 15.0!)
            Me.XrLabel31.Text = "$MON FEE"
            Me.XrLabel31.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel30
            '
            Me.XrLabel30.LocationFloat = New DevExpress.Utils.PointFloat(500.0!, 1.0!)
            Me.XrLabel30.Name = "XrLabel30"
            Me.XrLabel30.SizeF = New System.Drawing.SizeF(100.0!, 15.0!)
            Me.XrLabel30.Text = "$ACTUAL"
            Me.XrLabel30.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel28
            '
            Me.XrLabel28.LocationFloat = New DevExpress.Utils.PointFloat(600.0!, 1.0!)
            Me.XrLabel28.Name = "XrLabel28"
            Me.XrLabel28.SizeF = New System.Drawing.SizeF(58.0!, 15.0!)
            Me.XrLabel28.Text = "% DISB"
            Me.XrLabel28.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel27
            '
            Me.XrLabel27.LocationFloat = New DevExpress.Utils.PointFloat(7.999992!, 0.0!)
            Me.XrLabel27.Name = "XrLabel27"
            Me.XrLabel27.SizeF = New System.Drawing.SizeF(191.0!, 15.0!)
            Me.XrLabel27.Text = "OFFICE"
            Me.XrLabel27.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            '
            'XrLabel26
            '
            Me.XrLabel26.LocationFloat = New DevExpress.Utils.PointFloat(325.0!, 1.0!)
            Me.XrLabel26.Name = "XrLabel26"
            Me.XrLabel26.SizeF = New System.Drawing.SizeF(50.0!, 15.0!)
            Me.XrLabel26.Text = "# DISB"
            Me.XrLabel26.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel25
            '
            Me.XrLabel25.LocationFloat = New DevExpress.Utils.PointFloat(391.0!, 1.0!)
            Me.XrLabel25.Name = "XrLabel25"
            Me.XrLabel25.SizeF = New System.Drawing.SizeF(100.0!, 15.0!)
            Me.XrLabel25.Text = "$ SCHEDULED"
            Me.XrLabel25.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel15
            '
            Me.XrLabel15.LocationFloat = New DevExpress.Utils.PointFloat(236.0!, 1.0!)
            Me.XrLabel15.Name = "XrLabel15"
            Me.XrLabel15.SizeF = New System.Drawing.SizeF(72.0!, 15.0!)
            Me.XrLabel15.Text = "# ACTIVE"
            '
            'GroupHeader1
            '
            Me.GroupHeader1.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrPanel1, Me.XrLabel6})
            Me.GroupHeader1.GroupUnion = DevExpress.XtraReports.UI.GroupUnion.WithFirstDetail
            Me.GroupHeader1.HeightF = 75.0!
            Me.GroupHeader1.Name = "GroupHeader1"
            Me.GroupHeader1.RepeatEveryPage = True
            '
            'GroupFooter1
            '
            Me.GroupFooter1.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel_g1_actual_disbursement, Me.XrLabel_g1_expected_disbursement, Me.XrLabel_g1_clients_disbursed, Me.XrLabel_g1_paf_fees, Me.XrLabel_g1_active_clients, Me.XrLabel_g1_name})
            Me.GroupFooter1.HeightF = 22.0!
            Me.GroupFooter1.Name = "GroupFooter1"
            Me.GroupFooter1.StyleName = "XrControlStyle_Totals"
            '
            'XrLabel_g1_actual_disbursement
            '
            Me.XrLabel_g1_actual_disbursement.LocationFloat = New DevExpress.Utils.PointFloat(500.0!, 0.0!)
            Me.XrLabel_g1_actual_disbursement.Name = "XrLabel_g1_actual_disbursement"
            Me.XrLabel_g1_actual_disbursement.SizeF = New System.Drawing.SizeF(100.0!, 15.0!)
            Me.XrLabel_g1_actual_disbursement.StylePriority.UseTextAlignment = False
            XrSummary1.FormatString = "{0:c}"
            XrSummary1.Running = DevExpress.XtraReports.UI.SummaryRunning.Group
            Me.XrLabel_g1_actual_disbursement.Summary = XrSummary1
            Me.XrLabel_g1_actual_disbursement.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel_g1_expected_disbursement
            '
            Me.XrLabel_g1_expected_disbursement.LocationFloat = New DevExpress.Utils.PointFloat(391.0!, 0.0!)
            Me.XrLabel_g1_expected_disbursement.Name = "XrLabel_g1_expected_disbursement"
            Me.XrLabel_g1_expected_disbursement.SizeF = New System.Drawing.SizeF(100.0!, 15.0!)
            Me.XrLabel_g1_expected_disbursement.StylePriority.UseTextAlignment = False
            XrSummary2.FormatString = "{0:c}"
            XrSummary2.Running = DevExpress.XtraReports.UI.SummaryRunning.Group
            Me.XrLabel_g1_expected_disbursement.Summary = XrSummary2
            Me.XrLabel_g1_expected_disbursement.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel_g1_clients_disbursed
            '
            Me.XrLabel_g1_clients_disbursed.LocationFloat = New DevExpress.Utils.PointFloat(325.0!, 0.0!)
            Me.XrLabel_g1_clients_disbursed.Name = "XrLabel_g1_clients_disbursed"
            Me.XrLabel_g1_clients_disbursed.SizeF = New System.Drawing.SizeF(50.0!, 15.0!)
            Me.XrLabel_g1_clients_disbursed.StylePriority.UseTextAlignment = False
            XrSummary3.FormatString = "{0:f0}"
            XrSummary3.Running = DevExpress.XtraReports.UI.SummaryRunning.Group
            Me.XrLabel_g1_clients_disbursed.Summary = XrSummary3
            Me.XrLabel_g1_clients_disbursed.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel_g1_paf_fees
            '
            Me.XrLabel_g1_paf_fees.LocationFloat = New DevExpress.Utils.PointFloat(666.0!, 0.0!)
            Me.XrLabel_g1_paf_fees.Name = "XrLabel_g1_paf_fees"
            Me.XrLabel_g1_paf_fees.SizeF = New System.Drawing.SizeF(75.0!, 15.0!)
            Me.XrLabel_g1_paf_fees.StylePriority.UseTextAlignment = False
            XrSummary4.FormatString = "{0:c}"
            XrSummary4.Running = DevExpress.XtraReports.UI.SummaryRunning.Group
            Me.XrLabel_g1_paf_fees.Summary = XrSummary4
            Me.XrLabel_g1_paf_fees.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel_g1_active_clients
            '
            Me.XrLabel_g1_active_clients.LocationFloat = New DevExpress.Utils.PointFloat(236.0!, 0.0!)
            Me.XrLabel_g1_active_clients.Name = "XrLabel_g1_active_clients"
            Me.XrLabel_g1_active_clients.SizeF = New System.Drawing.SizeF(72.0!, 15.0!)
            Me.XrLabel_g1_active_clients.StylePriority.UseTextAlignment = False
            XrSummary5.FormatString = "{0:f0}"
            XrSummary5.Running = DevExpress.XtraReports.UI.SummaryRunning.Group
            Me.XrLabel_g1_active_clients.Summary = XrSummary5
            Me.XrLabel_g1_active_clients.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel_g1_name
            '
            Me.XrLabel_g1_name.LocationFloat = New DevExpress.Utils.PointFloat(8.0!, 0.0!)
            Me.XrLabel_g1_name.Name = "XrLabel_g1_name"
            Me.XrLabel_g1_name.SizeF = New System.Drawing.SizeF(217.0!, 14.99999!)
            Me.XrLabel_g1_name.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            '
            'XrLabel6
            '
            Me.XrLabel6.Font = New System.Drawing.Font("Times New Roman", 14.0!, System.Drawing.FontStyle.Bold)
            Me.XrLabel6.ForeColor = System.Drawing.Color.Red
            Me.XrLabel6.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 10.00001!)
            Me.XrLabel6.Name = "XrLabel6"
            Me.XrLabel6.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel6.SizeF = New System.Drawing.SizeF(751.4583!, 22.99998!)
            Me.XrLabel6.StylePriority.UseFont = False
            Me.XrLabel6.StylePriority.UseForeColor = False
            Me.XrLabel6.Text = "XrLabel6"
            '
            'XrLabel_name
            '
            Me.XrLabel_name.LocationFloat = New DevExpress.Utils.PointFloat(8.0!, 0.0!)
            Me.XrLabel_name.Name = "XrLabel_name"
            Me.XrLabel_name.SizeF = New System.Drawing.SizeF(191.0!, 15.0!)
            Me.XrLabel_name.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            '
            'XrLabel_paf_fees
            '
            Me.XrLabel_paf_fees.LocationFloat = New DevExpress.Utils.PointFloat(666.0!, 0.0!)
            Me.XrLabel_paf_fees.Name = "XrLabel_paf_fees"
            Me.XrLabel_paf_fees.SizeF = New System.Drawing.SizeF(75.0!, 15.0!)
            Me.XrLabel_paf_fees.StylePriority.UseTextAlignment = False
            Me.XrLabel_paf_fees.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel_pct_disb
            '
            Me.XrLabel_pct_disb.LocationFloat = New DevExpress.Utils.PointFloat(600.0!, 0.0!)
            Me.XrLabel_pct_disb.Name = "XrLabel_pct_disb"
            Me.XrLabel_pct_disb.SizeF = New System.Drawing.SizeF(58.0!, 15.0!)
            Me.XrLabel_pct_disb.StylePriority.UseTextAlignment = False
            Me.XrLabel_pct_disb.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel_actual_disbursement
            '
            Me.XrLabel_actual_disbursement.LocationFloat = New DevExpress.Utils.PointFloat(500.0!, 0.0!)
            Me.XrLabel_actual_disbursement.Name = "XrLabel_actual_disbursement"
            Me.XrLabel_actual_disbursement.SizeF = New System.Drawing.SizeF(100.0!, 15.0!)
            Me.XrLabel_actual_disbursement.StylePriority.UseTextAlignment = False
            Me.XrLabel_actual_disbursement.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel_clients_disbursed
            '
            Me.XrLabel_clients_disbursed.LocationFloat = New DevExpress.Utils.PointFloat(325.0!, 0.0!)
            Me.XrLabel_clients_disbursed.Name = "XrLabel_clients_disbursed"
            Me.XrLabel_clients_disbursed.SizeF = New System.Drawing.SizeF(50.0!, 15.0!)
            Me.XrLabel_clients_disbursed.StylePriority.UseTextAlignment = False
            Me.XrLabel_clients_disbursed.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel_expected_disbursement
            '
            Me.XrLabel_expected_disbursement.LocationFloat = New DevExpress.Utils.PointFloat(391.0!, 0.0!)
            Me.XrLabel_expected_disbursement.Name = "XrLabel_expected_disbursement"
            Me.XrLabel_expected_disbursement.SizeF = New System.Drawing.SizeF(100.0!, 15.0!)
            Me.XrLabel_expected_disbursement.StylePriority.UseTextAlignment = False
            Me.XrLabel_expected_disbursement.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel_active_clients
            '
            Me.XrLabel_active_clients.LocationFloat = New DevExpress.Utils.PointFloat(236.0!, 0.0!)
            Me.XrLabel_active_clients.Name = "XrLabel_active_clients"
            Me.XrLabel_active_clients.SizeF = New System.Drawing.SizeF(72.0!, 15.0!)
            Me.XrLabel_active_clients.StylePriority.UseTextAlignment = False
            Me.XrLabel_active_clients.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'StatisticsCounselorReport
            '
            Me.Bands.AddRange(New DevExpress.XtraReports.UI.Band() {Me.Detail, Me.PageHeader, Me.PageFooter, Me.GroupHeader1, Me.GroupFooter1})
            Me.Version = "10.1"
            Me.Controls.SetChildIndex(Me.GroupFooter1, 0)
            Me.Controls.SetChildIndex(Me.GroupHeader1, 0)
            Me.Controls.SetChildIndex(Me.PageFooter, 0)
            Me.Controls.SetChildIndex(Me.PageHeader, 0)
            Me.Controls.SetChildIndex(Me.Detail, 0)
            CType(Me, System.ComponentModel.ISupportInitialize).EndInit()

        End Sub

        Friend WithEvents XrLabel1 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel2 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel3 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel4 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel5 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrPanel1 As DevExpress.XtraReports.UI.XRPanel
        Friend WithEvents XrLabel15 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel25 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel26 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel27 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel28 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel30 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel31 As DevExpress.XtraReports.UI.XRLabel
#End Region

        ''' <summary>
        ''' Report Title string
        ''' </summary>
        Public Overrides ReadOnly Property ReportTitle() As String
            Get
                Return "Office Statistics Detail"
            End Get
        End Property

        Dim ds As New System.Data.DataSet("ds")

        ''' <summary>
        ''' Bind the dataset to the report
        ''' </summary>
        Private Sub TrustBalanceReportClass_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs)
            Dim tbl As System.Data.DataTable = ds.Tables("rpt_statistics_offices")
            If tbl Is Nothing Then
                Using cmd As SqlClient.SqlCommand = New System.Data.SqlClient.SqlCommand()
                    With cmd
                        .Connection = New System.Data.SqlClient.SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
                        .CommandTimeout = 0
                        .CommandType = CommandType.StoredProcedure
                        .CommandText = "rpt_statistics_offices"
                    End With

                    Using da As New SqlClient.SqlDataAdapter(cmd)
                        da.Fill(ds, "rpt_statistics_offices")
                        tbl = ds.Tables("rpt_statistics_offices")
                    End Using
                End Using
            End If

            If tbl IsNot Nothing Then
                Dim vue As New System.Data.DataView(tbl, String.Empty, "period_start, name", DataViewRowState.CurrentRows)
                DataSource = vue

                ' Group Fields
                GroupHeader1.GroupFields.Clear()
                GroupHeader1.GroupFields.Add(New GroupField("period_start"))

                ' Bind the fields to the data
                XrLabel6.DataBindings.Add("Text", vue, "period_start", "{0:MMMM, yyyy}")
                XrLabel_g1_name.DataBindings.Add("Text", vue, "period_start", "{0:MMM/yyyy TOTALS}")

                XrLabel_active_clients.DataBindings.Add("Text", vue, "active_clients", "{0:f0}")
                XrLabel_clients_disbursed.DataBindings.Add("Text", vue, "clients_disbursed", "{0:f0}")
                XrLabel_name.DataBindings.Add("Text", vue, "name")
                XrLabel_actual_disbursement.DataBindings.Add("Text", vue, "actual_disbursement", "{0:c}")
                XrLabel_expected_disbursement.DataBindings.Add("Text", vue, "expected_disbursement", "{0:c}")
                XrLabel_paf_fees.DataBindings.Add("Text", vue, "paf_fees", "{0:c}")

                XrLabel_g1_active_clients.DataBindings.Add("Text", vue, "active_clients")
                XrLabel_g1_clients_disbursed.DataBindings.Add("Text", vue, "clients_disbursed")
                XrLabel_g1_actual_disbursement.DataBindings.Add("Text", vue, "actual_disbursement")
                XrLabel_g1_expected_disbursement.DataBindings.Add("Text", vue, "expected_disbursement")
                XrLabel_g1_paf_fees.DataBindings.Add("Text", vue, "paf_fees")
            End If
        End Sub

        Private Sub XrLabel_pct_disb_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs)
            Dim Answer As Double = 0.0#
            With XrLabel_pct_disb
                .Text = String.Empty
                Dim obj As Object = GetCurrentColumnValue("expected_disbursement")
                If obj IsNot Nothing AndAlso obj IsNot System.DBNull.Value Then
                    Dim disbursement_expected As Decimal = Convert.ToDecimal(obj)

                    obj = GetCurrentColumnValue("actual_disbursement")
                    If obj IsNot Nothing AndAlso obj IsNot System.DBNull.Value Then
                        Dim disbursement_actual As Decimal = Convert.ToDecimal(obj)

                        Try
                            Answer = Convert.ToDouble(disbursement_actual) / Convert.ToDouble(disbursement_expected)
                            If Not Double.IsInfinity(Answer) AndAlso Not Double.IsNaN(Answer) AndAlso Not Double.IsNegativeInfinity(Answer) AndAlso Not Double.IsPositiveInfinity(Answer) Then
                                .Text = String.Format("{0:p}", Answer)
                            End If

                        Catch ex As Exception
                        End Try
                    End If
                End If
            End With
        End Sub
    End Class
End Namespace
