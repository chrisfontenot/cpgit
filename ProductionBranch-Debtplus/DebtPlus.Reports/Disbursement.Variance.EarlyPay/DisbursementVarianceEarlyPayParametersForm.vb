#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}

#End Region

Imports DebtPlus.Reports.Template.Forms

Imports System.Data.SqlClient

Namespace Disbursement.Variance.EarlyPay
    Public Class DisbursementVarianceEarlyPayParametersForm
        Inherits ReportParametersForm

        Dim ds As New DataSet("ds")

        ' TRUE if "[All Counselors]" is in the list
        Private EnableAllCounselors As Boolean = True

        Public Sub New()
            MyClass.New(True)
        End Sub

        Public Sub New(ByVal EnableAllCounselors As Boolean)
            MyBase.New()
            InitializeComponent()
            Me.EnableAllCounselors = EnableAllCounselors
            AddHandler Me.Load, AddressOf DateCounselorReportParametersForm_Load
        End Sub

        Public ReadOnly Property Parameter_FromDate() As DateTime
            Get
                Return DateEdit1.EditValue
            End Get
        End Property

        ''' <summary>
        ''' Return the counselor selected in the form
        ''' </summary>
        Public ReadOnly Property Parameter_Counselor() As Object
            Get
                With CounselorDropDown
                    If .EditValue IsNot Nothing AndAlso .EditValue IsNot DBNull.Value Then
                        If Convert.ToInt32(.EditValue) > 0 Then Return Convert.ToInt32(.EditValue)
                    End If

                    ' If all counselors is not permitted then return -1
                    If Not EnableAllCounselors Then Return -1
                    Return DBNull.Value
                End With
            End Get
        End Property


        ''' <summary>
        ''' Process the form load event
        ''' </summary>
        Private Sub DateCounselorReportParametersForm_Load(ByVal sender As Object, ByVal e As EventArgs)
            DateEdit1.EditValue = Now.AddDays(-30)

            CounselorDropDown_Load()
            ButtonOK.Enabled = Not HasErrors()
        End Sub


        ''' <summary>
        ''' Load the list of counselors
        ''' </summary>
        Private Sub CounselorDropDown_Load()
            Dim tbl As DataTable = CounselorsTable(EnableAllCounselors)

            With CounselorDropDown
                .Properties.AllowNullInput = EnableAllCounselors
                .Properties.DataSource = New DataView(tbl, String.Empty, "description, item_key", DataViewRowState.CurrentRows)
                If tbl.Columns.Contains("default") Then
                    Dim rows() As DataRow = tbl.Select("[default]<>0", "item_key")
                    If rows.GetUpperBound(0) >= 0 Then
                        .EditValue = rows(0)("item_key")
                    End If
                End If
                If EnableAllCounselors Then .EditValue = DBNull.Value

                AddHandler .EditValueChanging, AddressOf DebtPlus.Data.Validation.LookUpEdit_ActiveTest
                AddHandler .EditValueChanged, AddressOf FormChanged
            End With
        End Sub

        ''' <summary>
        ''' Look for errors in the counselor list
        ''' </summary>
        Protected Overrides Function HasErrors() As Boolean
            Return MyBase.HasErrors() OrElse (Not EnableAllCounselors AndAlso CounselorDropDown.EditValue Is DBNull.Value)
        End Function

        Friend Function CounselorsTable() As DataTable
            Return CounselorsTable(False)
        End Function

        Friend Function CounselorsTable(ByVal EnableAllCounselors As Boolean) As DataTable
            Dim tbl As DataTable = ds.Tables("counselors")
            If tbl Is Nothing Then
                Using sqlInfo As DebtPlus.LINQ.SQLInfoClass = DebtPlus.LINQ.SQLInfoClass.getDefault()
                    Using cmd As SqlCommand = New System.Data.SqlClient.SqlCommand()
                        With cmd
                            .Connection = New System.Data.SqlClient.SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
                            .CommandText = "lst_counselors"
                            .CommandType = CommandType.StoredProcedure
                        End With

                        Using da As New SqlDataAdapter(cmd)
                            da.Fill(ds, "counselors")
                            tbl = ds.Tables("counselors")
                            With tbl
                                If .PrimaryKey.GetUpperBound(0) < 0 Then .PrimaryKey = New DataColumn() {.Columns("item_key")}
                            End With
                        End Using
                    End Using
                End Using

                If EnableAllCounselors Then
                    With tbl
                        Dim row As DataRow = tbl.NewRow
                        With row
                            .Item("item_key") = -1
                            .Item("description") = "[All Counselors]"
                            .Item("default") = False
                            .Item("ActiveFlag") = True
                        End With
                        .Rows.InsertAt(row, 0)
                    End With
                End If
            End If

            Return tbl
        End Function
    End Class
End Namespace
