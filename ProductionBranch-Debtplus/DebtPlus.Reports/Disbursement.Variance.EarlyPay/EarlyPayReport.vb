#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}

#End Region

Option Compare Binary
Option Explicit On
Option Strict On

Imports DevExpress.XtraEditors.Controls
Imports DebtPlus.Reports.Template

Imports DevExpress.XtraReports.Parameters
Imports System.IO
Imports System.Reflection
Imports DevExpress.XtraEditors
Imports DevExpress.Utils
Imports DebtPlus.Utils

Namespace Disbursement.Variance.EarlyPay
    Public Class EarlyPayReport
        Inherits TemplateXtraReportClass

        ''' <summary>
        '''     Create an instance of our report
        ''' </summary>
        Public Sub New()
            MyBase.New()
            InitializeComponent()

            ' Set the script references as needed
            ScriptReferences = DebtPlus.Utils.AssemblyTools.GetAllSiblingAssemblies()

            Parameters("ParameterFromDate").Value = Now.AddDays(-30)
            Parameters("ParameterCounselor").Value = -1
            parameterFromDateDefined = False
        End Sub

        Private Sub InitializeComponent()
            Const ReportName As String = "DebtPlus.Reports.Disbursement.Variance.EarlyPay.repx"

            ' See if there is a report reference that we can use
            Dim Fname As String = Path.Combine(DebtPlus.Configuration.Config.ReportsDirectory, ReportName)
            Dim UseDefault As Boolean = True
            Try
                If (Not DebtPlus.Configuration.Config.LocalReportsOnly) AndAlso System.IO.File.Exists(Fname) Then
                    LoadLayout(Fname)
                    UseDefault = False
                End If
            Catch ex As Exception
            End Try

            ' Load the standard report definition if we need a new item
            If UseDefault Then
                Dim asm As Reflection.Assembly = Reflection.Assembly.GetExecutingAssembly                                           ' changed
                Using ios As Stream = asm.GetManifestResourceStream(String.Format("DebtPlus.Reports.{0}", ReportName))              ' changed
                    If ios IsNot Nothing Then LoadLayout(ios) '                                                                       changed
                End Using
            End If
        End Sub

        Public Overrides ReadOnly Property ReportTitle() As String
            Get
                Return "Early Pay"
            End Get
        End Property

        Public Overrides ReadOnly Property ReportSubTitle() As String
            Get
                Dim tbl As System.Data.DataTable = CounselorsTable()
                Dim row As System.Data.DataRow = tbl.Rows.Find(Parameter_Counselor)
                Dim CounselorString As String = String.Empty
                If row IsNot Nothing Then
                    If row("description") IsNot System.DBNull.Value Then
                        CounselorString = String.Format("counselor '{0}'", row("description"))
                    End If
                Else
                    CounselorString = "all counselors"
                End If

                Return String.Format("This report covers dates from {0:d} and {1}", Parameter_FromDate, CounselorString)
            End Get
        End Property

        Private parameterFromDateDefined As Boolean = False
        Public Property Parameter_FromDate() As DateTime
            Get
                Return CType(Parameters("ParameterFromDate").Value, DateTime)
            End Get
            Set(ByVal value As DateTime)
                Parameters("ParameterFromDate").Value = value
                parameterFromDateDefined = True
            End Set
        End Property

        Public Property Parameter_Counselor() As Object
            Get
                Dim obj As Object = Parameters("ParameterCounselor").Value
                If obj Is Nothing OrElse obj Is System.DBNull.Value Then Return System.DBNull.Value
                Dim value As System.Int32 = Convert.ToInt32(obj)
                If value <= 0 Then Return System.DBNull.Value
                Return value
            End Get
            Set(ByVal value As Object)
                If value Is System.DBNull.Value OrElse value Is Nothing Then Parameters("ParameterCounselor").Value = -1
                Parameters("ParameterCounselor").Value = value
            End Set
        End Property

        Public Overrides Function NeedParameters() As Boolean
            Return Not parameterFromDateDefined
        End Function

        Public Overrides Function RequestReportParameters() As System.Windows.Forms.DialogResult
            Dim Answer As System.Windows.Forms.DialogResult = DialogResult.OK
            If NeedParameters() Then
                Using frm As New DisbursementVarianceEarlyPayParametersForm(True)
                    With frm
                        Answer = .ShowDialog()
                        Parameter_FromDate = .Parameter_FromDate
                        Parameter_Counselor = .Parameter_Counselor
                    End With
                End Using
            End If
            Return Answer
        End Function

        Dim ds As New System.Data.DataSet("ds")

        Friend Function CounselorsTable() As DataTable
            Return CounselorsTable(False)
        End Function

        Friend Function CounselorsTable(ByVal EnableAllCounselors As Boolean) As DataTable
            Dim tbl As DataTable = ds.Tables("counselors")
            If tbl Is Nothing Then
                Using sqlInfo As DebtPlus.LINQ.SQLInfoClass = DebtPlus.LINQ.SQLInfoClass.getDefault()
                    Using cmd As System.Data.SqlClient.SqlCommand = New System.Data.SqlClient.SqlCommand()
                        With cmd
                            .Connection = New System.Data.SqlClient.SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
                            .CommandText = "lst_counselors"
                            .CommandType = CommandType.StoredProcedure
                        End With

                        Using da As New System.Data.SqlClient.SqlDataAdapter(cmd)
                            da.Fill(ds, "counselors")
                            tbl = ds.Tables("counselors")
                            With tbl
                                If .PrimaryKey.GetUpperBound(0) < 0 Then .PrimaryKey = New DataColumn() {.Columns("item_key")}
                            End With
                        End Using
                    End Using
                End Using

                If EnableAllCounselors Then
                    With tbl
                        Dim row As DataRow = tbl.NewRow
                        With row
                            .Item("item_key") = -1
                            .Item("description") = "[All Counselors]"
                            .Item("default") = False
                            .Item("ActiveFlag") = True
                        End With
                        .Rows.InsertAt(row, 0)
                    End With
                End If
            End If

            Return tbl
        End Function
    End Class
End Namespace
