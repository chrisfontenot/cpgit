#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}
#End Region

Option Compare Binary
Option Explicit On
Option Strict On

Imports DevExpress.XtraReports.UI

Namespace Disbursement.Variance.EarlyPay
    Public Class EarlyPayReport

        ''' <summary>
        ''' Create an instance of our report
        ''' </summary>
        Public Sub New()
            MyBase.New()
            InitializeComponent()

            Parameters("ParameterFromDate").Value = Now.AddDays(-30)
            Parameters("ParameterCounselor").Value = -1
            Parameters("ParameterCounselor").Visible = False
            parameterFromDateDefined = False

            AddHandler XrLabel_client.BeforePrint, AddressOf XrLabel_client_BeforePrint
            AddHandler BeforePrint, AddressOf EarlyPayReport_BeforePrint
            AddHandler ParametersRequestBeforeShow, AddressOf Report_ParametersRequestBeforeShow
        End Sub

        Public Overrides ReadOnly Property ReportTitle() As String
            Get
                Return "Early Pay"
            End Get
        End Property

        Public Overrides ReadOnly Property ReportSubTitle() As String
            Get
                Dim tbl As System.Data.DataTable = CounselorsTable()
                Dim row As System.Data.DataRow = tbl.Rows.Find(Parameter_Counselor)
                Dim CounselorString As String = String.Empty
                If row IsNot Nothing Then
                    If row("description") IsNot System.DBNull.Value Then
                        CounselorString = String.Format("counselor '{0}'", row("description"))
                    End If
                Else
                    CounselorString = "all counselors"
                End If

                Return String.Format("This report covers dates from {0:d} and {1}", Parameter_FromDate, CounselorString)
            End Get
        End Property

        Private parameterFromDateDefined As Boolean = False
        Public Property Parameter_FromDate() As DateTime
            Get
                Return CType(Parameters("ParameterFromDate").Value, DateTime)
            End Get
            Set(ByVal value As DateTime)
                Parameters("ParameterFromDate").Value = value
                parameterFromDateDefined = True
            End Set
        End Property

        Public Property Parameter_Counselor() As Object
            Get
                Dim obj As Object = Parameters("ParameterCounselor").Value
                If obj Is Nothing OrElse obj Is System.DBNull.Value Then Return System.DBNull.Value
                Dim value As System.Int32 = Convert.ToInt32(obj)
                If value <= 0 Then Return System.DBNull.Value
                Return value
            End Get
            Set(ByVal value As Object)
                If value Is System.DBNull.Value OrElse value Is Nothing Then Parameters("ParameterCounselor").Value = -1
                Parameters("ParameterCounselor").Value = value
            End Set
        End Property

        Public Overrides Function NeedParameters() As Boolean
            Return Not parameterFromDateDefined
        End Function

        Public Overrides Function RequestReportParameters() As System.Windows.Forms.DialogResult
            Dim Answer As System.Windows.Forms.DialogResult = DialogResult.OK
            If NeedParameters() Then
                Using frm As New DisbursementVarianceEarlyPayParametersForm(True)
                    With frm
                        Answer = .ShowDialog
                        Parameter_FromDate = .Parameter_FromDate
                        Parameter_Counselor = .Parameter_Counselor
                    End With
                End Using
            End If
            Return Answer
        End Function

        Dim ds As New System.Data.DataSet("ds")

        Friend Function CounselorsTable() As DataTable
            Return CounselorsTable(False)
        End Function

        Friend Function CounselorsTable(ByVal EnableAllCounselors As Boolean) As DataTable
            Dim tbl As DataTable = ds.Tables("counselors")
            If tbl Is Nothing Then
                Using sqlInfo As DebtPlus.LINQ.SQLInfoClass = DebtPlus.LINQ.SQLInfoClass.getDefault()
                    Using cmd As System.Data.SqlClient.SqlCommand = New System.Data.SqlClient.SqlCommand()
                        With cmd
                            .Connection = New System.Data.SqlClient.SqlConnection(sqlInfo.ConnectionString)
                            .CommandText = "lst_counselors"
                            .CommandType = CommandType.StoredProcedure
                        End With

                        Using da As New System.Data.SqlClient.SqlDataAdapter(cmd)
                            da.Fill(ds, "counselors")
                            tbl = ds.Tables("counselors")
                            With tbl
                                If .PrimaryKey.GetUpperBound(0) < 0 Then .PrimaryKey = New DataColumn() {.Columns("item_key")}
                            End With
                        End Using
                    End Using
                End Using

                If EnableAllCounselors Then
                    With tbl
                        Dim row As DataRow = tbl.NewRow
                        With row
                            .Item("item_key") = -1
                            .Item("description") = "[All Counselors]"
                            .Item("default") = False
                            .Item("ActiveFlag") = True
                        End With
                        .Rows.InsertAt(row, 0)
                    End With
                End If
            End If

            Return tbl
        End Function

        Private Sub Report_ParametersRequestBeforeShow(ByVal sender As Object, ByVal e As DevExpress.XtraReports.Parameters.ParametersRequestEventArgs)
            For Each pi As DevExpress.XtraReports.Parameters.ParameterInfo In e.ParametersInformation
                If pi.Parameter.Name = "ParameterCounselor" Then
                    pi.Editor = GetCounselorEditor()
                End If
            Next
        End Sub

        Private Function GetCounselorEditor() As System.Windows.Forms.Control
            Dim ctl As New DevExpress.XtraEditors.LookUpEdit
            With ctl
                With .Properties
                    .DataSource = CounselorsTable(True)
                    .AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
                    .Columns.AddRange(New DevExpress.XtraEditors.Controls.LookUpColumnInfo() {New DevExpress.XtraEditors.Controls.LookUpColumnInfo("item_key", "ID", 5, DevExpress.Utils.FormatType.Numeric, "f0", False, DevExpress.Utils.HorzAlignment.[Default]), New DevExpress.XtraEditors.Controls.LookUpColumnInfo("description", 50, "Name")})
                    .DisplayMember = "description"
                    .NullText = "[All Counselors]"
                    .PopupWidth = 300
                    .ShowFooter = False
                    .ShowHeader = False
                    .ValueMember = "item_key"
                End With
            End With
            Return ctl
        End Function

        Private Sub EarlyPayReport_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs)
            Const TableName As String = "rpt_Early_Pay"
            Dim rpt As DevExpress.XtraReports.UI.XtraReport = CType(sender, DevExpress.XtraReports.UI.XtraReport)
            Dim sqlInfo As DebtPlus.LINQ.SQLInfoClass = DebtPlus.LINQ.SQLInfoClass.getDefault()

            Dim tbl As System.Data.DataTable = ds.Tables(TableName)
            If tbl IsNot Nothing Then
                tbl.Clear()
                tbl = Nothing
            End If

            Dim cn As System.Data.SqlClient.SqlConnection = New System.Data.SqlClient.SqlConnection(sqlInfo.ConnectionString)
            Try
                cn.Open()
                Using cmd As New System.Data.SqlClient.SqlCommand()
                    With cmd
                        .Connection = cn
                        .CommandText = TableName
                        .CommandType = System.Data.CommandType.StoredProcedure
                        System.Data.SqlClient.SqlCommandBuilder.DeriveParameters(cmd)

                        .Parameters(1).Value = rpt.Parameters("ParameterFromDate").Value
                        .Parameters(2).Value = rpt.Parameters("ParameterCounselor").Value
                        .CommandTimeout = 0
                    End With

                    Using da As New System.Data.SqlClient.SqlDataAdapter(cmd)
                        da.Fill(ds, TableName)
                        tbl = ds.Tables(TableName)
                    End Using
                End Using

                rpt.DataSource = New System.Data.DataView(tbl, String.Empty, "[counselor], [client]", System.Data.DataViewRowState.CurrentRows)

            Catch ex As Exception
                Dim gdr As New DebtPlus.Repository.GetDataResult() : gdr.HandleException(ex) : DebtPlus.UI.Common.ErrorHandling.HandleErrors(gdr, "Error reading report information")

            Finally
                If cn IsNot Nothing Then
                    If cn.State <> System.Data.ConnectionState.Closed Then
                        cn.Close()
                    End If
                    cn.Dispose()
                End If
            End Try
        End Sub

        Private Sub XrLabel_client_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs)
            With CType(sender, DevExpress.XtraReports.UI.XRLabel)
                .Text = DebtPlus.Utils.Format.Client.FormatClientID(GetCurrentColumnValue("client"))
            End With
        End Sub
    End Class
End Namespace
