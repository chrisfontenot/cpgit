Namespace Disbursement.Variance.EarlyPay
    <Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
    Partial Class DisbursementVarianceEarlyPayParametersForm

        'Form overrides dispose to clean up the component list.
        <System.Diagnostics.DebuggerNonUserCode()> _
        Protected Overrides Sub Dispose(ByVal disposing As Boolean)
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
            MyBase.Dispose(disposing)
        End Sub

        'Required by the Windows Form Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Windows Form Designer
        'It can be modified using the Windows Form Designer.  
        'Do not modify it using the code editor.
        <System.Diagnostics.DebuggerStepThrough()> _
        Private Sub InitializeComponent()
            Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl
            Me.DateEdit1 = New DevExpress.XtraEditors.DateEdit
            Me.LabelControl2 = New DevExpress.XtraEditors.LabelControl
            Me.CounselorDropDown = New DevExpress.XtraEditors.LookUpEdit
            CType(Me.DxErrorProvider1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.DateEdit1.Properties.VistaTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.DateEdit1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.CounselorDropDown.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.SuspendLayout()
            '
            'ButtonOK
            '
            Me.ButtonOK.TabIndex = 4
            '
            'ButtonCancel
            '
            Me.ButtonCancel.TabIndex = 5
            '
            'LabelControl1
            '
            Me.LabelControl1.Location = New System.Drawing.Point(13, 28)
            Me.LabelControl1.Name = "LabelControl1"
            Me.LabelControl1.Size = New System.Drawing.Size(64, 13)
            Me.LabelControl1.TabIndex = 0
            Me.LabelControl1.Text = "Starting Date"
            '
            'DateEdit1
            '
            Me.DateEdit1.EditValue = Nothing
            Me.DateEdit1.Location = New System.Drawing.Point(93, 25)
            Me.DateEdit1.Name = "DateEdit1"
            Me.DateEdit1.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.DateEdit1.Properties.VistaTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton})
            Me.DateEdit1.Size = New System.Drawing.Size(100, 20)
            Me.DateEdit1.TabIndex = 1
            '
            'LabelControl2
            '
            Me.LabelControl2.Location = New System.Drawing.Point(12, 55)
            Me.LabelControl2.Name = "LabelControl2"
            Me.LabelControl2.Size = New System.Drawing.Size(48, 13)
            Me.LabelControl2.TabIndex = 2
            Me.LabelControl2.Text = "Counselor"
            '
            'CounselorDropDown
            '
            Me.CounselorDropDown.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
                                                 Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.CounselorDropDown.Location = New System.Drawing.Point(93, 52)
            Me.CounselorDropDown.Name = "CounselorDropDown"
            Me.CounselorDropDown.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
            Me.CounselorDropDown.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.CounselorDropDown.Properties.Columns.AddRange(New DevExpress.XtraEditors.Controls.LookUpColumnInfo() {New DevExpress.XtraEditors.Controls.LookUpColumnInfo("item_key", "ID", 5, DevExpress.Utils.FormatType.Numeric, "f0", False, DevExpress.Utils.HorzAlignment.[Default]), New DevExpress.XtraEditors.Controls.LookUpColumnInfo("description", 50, "Name")})
            Me.CounselorDropDown.Properties.DisplayMember = "description"
            Me.CounselorDropDown.Properties.NullText = "[All Counselors]"
            Me.CounselorDropDown.Properties.PopupWidth = 300
            Me.CounselorDropDown.Properties.ShowFooter = False
            Me.CounselorDropDown.Properties.ShowHeader = False
            Me.CounselorDropDown.Properties.ValueMember = "item_key"
            Me.CounselorDropDown.Size = New System.Drawing.Size(149, 20)
            Me.CounselorDropDown.TabIndex = 3
            Me.CounselorDropDown.ToolTip = "Choose the counselor to which you wish to limit the report"
            Me.CounselorDropDown.ToolTipController = Me.ToolTipController1
            Me.CounselorDropDown.Properties.SortColumnIndex = 1
            '
            'ParametersForm
            '
            Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
            Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
            Me.ClientSize = New System.Drawing.Size(336, 135)
            Me.Controls.Add(Me.LabelControl1)
            Me.Controls.Add(Me.DateEdit1)
            Me.Controls.Add(Me.CounselorDropDown)
            Me.Controls.Add(Me.LabelControl2)
            Me.Name = "ParametersForm"
            Me.Controls.SetChildIndex(Me.LabelControl2, 0)
            Me.Controls.SetChildIndex(Me.CounselorDropDown, 0)
            Me.Controls.SetChildIndex(Me.DateEdit1, 0)
            Me.Controls.SetChildIndex(Me.LabelControl1, 0)
            Me.Controls.SetChildIndex(Me.ButtonOK, 0)
            Me.Controls.SetChildIndex(Me.ButtonCancel, 0)
            CType(Me.DxErrorProvider1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.DateEdit1.Properties.VistaTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.DateEdit1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.CounselorDropDown.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            Me.ResumeLayout(False)
            Me.PerformLayout()
        End Sub

        Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents DateEdit1 As DevExpress.XtraEditors.DateEdit
        Friend WithEvents LabelControl2 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents CounselorDropDown As DevExpress.XtraEditors.LookUpEdit
    End Class
End Namespace
