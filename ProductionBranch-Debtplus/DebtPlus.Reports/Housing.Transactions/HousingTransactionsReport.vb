#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}

#End Region

Option Compare Binary
Option Explicit On
Option Strict On

Imports DebtPlus.Reports.Template

Imports System.Drawing.Printing
Imports System.Data.SqlClient
Imports DevExpress.XtraReports.UI

Namespace Housing.Transactions
    Public Class DailySummaryRPPSRejects
        Inherits DatedTemplateXtraReportClass

        ''' <summary>
        ''' Create an instance of our report
        ''' </summary>
        Public Sub New()
            MyBase.New()
            InitializeComponent()
            AddHandler Me.BeforePrint, AddressOf Report_BeforePrint
            AddHandler XrLabel_client.BeforePrint, AddressOf XrLabel_client_BeforePrint
        End Sub

#Region " Component Designer generated code "

        'Component overrides dispose to clean up the component list.
        Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
            If disposing Then
                If components IsNot Nothing Then
                    components.Dispose()
                End If
            End If
            MyBase.Dispose(disposing)
        End Sub

        Friend WithEvents GroupHeader1 As DevExpress.XtraReports.UI.GroupHeaderBand
        Friend WithEvents GroupFooter1 As DevExpress.XtraReports.UI.GroupFooterBand
        Friend WithEvents XrLabel_date_created As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents ReportFooter As DevExpress.XtraReports.UI.ReportFooterBand
        Friend WithEvents XrPanel1 As DevExpress.XtraReports.UI.XRPanel
        Friend WithEvents GroupHeader2 As DevExpress.XtraReports.UI.GroupHeaderBand
        Friend WithEvents XrLabel_hud_type As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents GroupFooter2 As DevExpress.XtraReports.UI.GroupFooterBand
        Friend WithEvents XrLabel3 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel4 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel5 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel6 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel8 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel9 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel10 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel11 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel12 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel14 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_counselor As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_source As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_client As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_ethnic As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_type As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_results As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_minutes As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_housing_status As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_status As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_hud_id As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel27 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_group1_minutes As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel29 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel37 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_group2_minutes As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel39 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel22 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_total_minutes As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel24 As DevExpress.XtraReports.UI.XRLabel

        'Required by the Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Designer
        'It can be modified using the Designer.
        'Do not modify it using the code editor.
        <System.Diagnostics.DebuggerStepThrough()>
        Private Sub InitializeComponent()
            Me.GroupHeader1 = New DevExpress.XtraReports.UI.GroupHeaderBand
            Me.XrPanel1 = New DevExpress.XtraReports.UI.XRPanel
            Me.XrLabel3 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel4 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel5 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel6 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel8 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel9 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel10 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel11 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel12 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel14 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_date_created = New DevExpress.XtraReports.UI.XRLabel
            Me.GroupFooter1 = New DevExpress.XtraReports.UI.GroupFooterBand
            Me.XrLabel27 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_group1_minutes = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel29 = New DevExpress.XtraReports.UI.XRLabel
            Me.ReportFooter = New DevExpress.XtraReports.UI.ReportFooterBand
            Me.XrLabel22 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_total_minutes = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel24 = New DevExpress.XtraReports.UI.XRLabel
            Me.GroupHeader2 = New DevExpress.XtraReports.UI.GroupHeaderBand
            Me.XrLabel_hud_type = New DevExpress.XtraReports.UI.XRLabel
            Me.GroupFooter2 = New DevExpress.XtraReports.UI.GroupFooterBand
            Me.XrLabel37 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_group2_minutes = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel39 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_counselor = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_source = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_client = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_ethnic = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_type = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_results = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_minutes = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_housing_status = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_status = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_hud_id = New DevExpress.XtraReports.UI.XRLabel
            CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
            '
            'Detail
            '
            Me.Detail.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel_counselor, Me.XrLabel_source, Me.XrLabel_client, Me.XrLabel_ethnic, Me.XrLabel_type, Me.XrLabel_results, Me.XrLabel_minutes, Me.XrLabel_housing_status, Me.XrLabel_status, Me.XrLabel_hud_id})
            Me.Detail.Height = 15
            '
            'PageHeader
            '
            Me.PageHeader.Height = 119
            '
            'XrPageInfo_PageNumber
            '
            Me.XrPageInfo_PageNumber.Location = New System.Drawing.Point(917, 8)
            '
            'XrPanel_AgencyAddress
            '
            Me.XrPanel_AgencyAddress.Location = New System.Drawing.Point(742, 0)
            '
            'XrPanel1
            '
            Me.XrPanel1.BackColor = System.Drawing.Color.Teal
            Me.XrPanel1.BorderColor = System.Drawing.Color.Teal
            Me.XrPanel1.CanGrow = False
            Me.XrPanel1.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel3, Me.XrLabel4, Me.XrLabel5, Me.XrLabel6, Me.XrLabel8, Me.XrLabel9, Me.XrLabel10, Me.XrLabel11, Me.XrLabel12, Me.XrLabel14})
            Me.XrPanel1.KeepTogether = False
            Me.XrPanel1.Location = New System.Drawing.Point(0, 67)
            Me.XrPanel1.Name = "XrPanel1"
            Me.XrPanel1.Size = New System.Drawing.Size(1050, 18)
            Me.XrPanel1.StyleName = "XrControlStyle_HeaderPannel"
            Me.XrPanel1.StylePriority.UseBackColor = False
            Me.XrPanel1.StylePriority.UseBorderColor = False
            '
            'XrLabel3
            '
            Me.XrLabel3.Location = New System.Drawing.Point(0, 1)
            Me.XrLabel3.Name = "XrLabel3"
            Me.XrLabel3.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel3.Size = New System.Drawing.Size(46, 15)
            Me.XrLabel3.Text = "ID"
            '
            'XrLabel4
            '
            Me.XrLabel4.Location = New System.Drawing.Point(292, 1)
            Me.XrLabel4.Name = "XrLabel4"
            Me.XrLabel4.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel4.Size = New System.Drawing.Size(75, 15)
            Me.XrLabel4.StylePriority.UseTextAlignment = False
            Me.XrLabel4.Text = "ETHNIC"
            Me.XrLabel4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            '
            'XrLabel5
            '
            Me.XrLabel5.Location = New System.Drawing.Point(714, 1)
            Me.XrLabel5.Name = "XrLabel5"
            Me.XrLabel5.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel5.Size = New System.Drawing.Size(68, 15)
            Me.XrLabel5.Text = "MINUTES"
            '
            'XrLabel6
            '
            Me.XrLabel6.Location = New System.Drawing.Point(97, 1)
            Me.XrLabel6.Name = "XrLabel6"
            Me.XrLabel6.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel6.Size = New System.Drawing.Size(183, 15)
            Me.XrLabel6.StylePriority.UseTextAlignment = False
            Me.XrLabel6.Text = "CLIENT ID AND NAME"
            Me.XrLabel6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            '
            'XrLabel8
            '
            Me.XrLabel8.Location = New System.Drawing.Point(375, 1)
            Me.XrLabel8.Name = "XrLabel8"
            Me.XrLabel8.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel8.Size = New System.Drawing.Size(150, 15)
            Me.XrLabel8.StylePriority.UseTextAlignment = False
            Me.XrLabel8.Text = "PURPOSE OF VISIT"
            Me.XrLabel8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            '
            'XrLabel9
            '
            Me.XrLabel9.Location = New System.Drawing.Point(791, 1)
            Me.XrLabel9.Name = "XrLabel9"
            Me.XrLabel9.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel9.Size = New System.Drawing.Size(108, 15)
            Me.XrLabel9.StylePriority.UseTextAlignment = False
            Me.XrLabel9.Text = "HOUSING STS"
            Me.XrLabel9.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            '
            'XrLabel10
            '
            Me.XrLabel10.Location = New System.Drawing.Point(900, 1)
            Me.XrLabel10.Name = "XrLabel10"
            Me.XrLabel10.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel10.Size = New System.Drawing.Size(37, 15)
            Me.XrLabel10.StylePriority.UseTextAlignment = False
            Me.XrLabel10.Text = "STS"
            Me.XrLabel10.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            '
            'XrLabel11
            '
            Me.XrLabel11.Location = New System.Drawing.Point(950, 1)
            Me.XrLabel11.Name = "XrLabel11"
            Me.XrLabel11.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel11.Size = New System.Drawing.Size(100, 15)
            Me.XrLabel11.StylePriority.UseTextAlignment = False
            Me.XrLabel11.Text = "COUNSELOR"
            Me.XrLabel11.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            '
            'XrLabel12
            '
            Me.XrLabel12.Location = New System.Drawing.Point(58, 1)
            Me.XrLabel12.Name = "XrLabel12"
            Me.XrLabel12.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel12.Size = New System.Drawing.Size(37, 15)
            Me.XrLabel12.StylePriority.UseTextAlignment = False
            Me.XrLabel12.Text = "SRC"
            Me.XrLabel12.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            '
            'XrLabel14
            '
            Me.XrLabel14.Location = New System.Drawing.Point(533, 1)
            Me.XrLabel14.Name = "XrLabel14"
            Me.XrLabel14.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel14.Size = New System.Drawing.Size(175, 15)
            Me.XrLabel14.StylePriority.UseTextAlignment = False
            Me.XrLabel14.Text = "RESULT"
            Me.XrLabel14.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            '
            'XrLabel_date_created
            '
            Me.XrLabel_date_created.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel_date_created.ForeColor = System.Drawing.SystemColors.ControlText
            Me.XrLabel_date_created.Location = New System.Drawing.Point(0, 33)
            Me.XrLabel_date_created.Name = "XrLabel_date_created"
            Me.XrLabel_date_created.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_date_created.Size = New System.Drawing.Size(1050, 25)
            Me.XrLabel_date_created.StylePriority.UseFont = False
            Me.XrLabel_date_created.StylePriority.UseForeColor = False
            Me.XrLabel_date_created.StylePriority.UseTextAlignment = False
            Me.XrLabel_date_created.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
            '
            'XrLabel27
            '
            Me.XrLabel27.Location = New System.Drawing.Point(533, 8)
            Me.XrLabel27.Name = "XrLabel27"
            Me.XrLabel27.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel27.Size = New System.Drawing.Size(158, 15)
            Me.XrLabel27.StylePriority.UseTextAlignment = False
            Me.XrLabel27.Text = "DAILY TOTALS"
            Me.XrLabel27.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            '
            'XrLabel_group1_minutes
            '
            Me.XrLabel_group1_minutes.Location = New System.Drawing.Point(706, 8)
            Me.XrLabel_group1_minutes.Name = "XrLabel_group1_minutes"
            Me.XrLabel_group1_minutes.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_group1_minutes.Size = New System.Drawing.Size(76, 15)
            Me.XrLabel_group1_minutes.StylePriority.UseTextAlignment = False
            Me.XrLabel_group1_minutes.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel29
            '
            Me.XrLabel29.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.XrLabel29.Location = New System.Drawing.Point(792, 8)
            Me.XrLabel29.Name = "XrLabel29"
            Me.XrLabel29.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel29.Size = New System.Drawing.Size(108, 15)
            Me.XrLabel29.StylePriority.UseFont = False
            Me.XrLabel29.StylePriority.UseTextAlignment = False
            Me.XrLabel29.Text = "*"
            Me.XrLabel29.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            '
            'ReportFooter
            '
            Me.ReportFooter.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel22, Me.XrLabel_total_minutes, Me.XrLabel24})
            Me.ReportFooter.Height = 32
            Me.ReportFooter.Name = "ReportFooter"
            Me.ReportFooter.StyleName = "XrControlStyle_Totals"
            '
            'XrLabel22
            '
            Me.XrLabel22.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.XrLabel22.Location = New System.Drawing.Point(792, 8)
            Me.XrLabel22.Name = "XrLabel22"
            Me.XrLabel22.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel22.Size = New System.Drawing.Size(108, 15)
            Me.XrLabel22.StylePriority.UseFont = False
            Me.XrLabel22.StylePriority.UseTextAlignment = False
            Me.XrLabel22.Text = "***"
            Me.XrLabel22.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            '
            'XrLabel_total_minutes
            '
            Me.XrLabel_total_minutes.Location = New System.Drawing.Point(706, 8)
            Me.XrLabel_total_minutes.Name = "XrLabel_total_minutes"
            Me.XrLabel_total_minutes.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_total_minutes.Size = New System.Drawing.Size(76, 15)
            Me.XrLabel_total_minutes.StylePriority.UseTextAlignment = False
            Me.XrLabel_total_minutes.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel24
            '
            Me.XrLabel24.Location = New System.Drawing.Point(533, 8)
            Me.XrLabel24.Name = "XrLabel24"
            Me.XrLabel24.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel24.Size = New System.Drawing.Size(158, 15)
            Me.XrLabel24.StylePriority.UseTextAlignment = False
            Me.XrLabel24.Text = "GRAND TOTALS"
            Me.XrLabel24.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            '
            'GroupHeader1
            '
            Me.GroupHeader1.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrPanel1, Me.XrLabel_date_created})
            Me.GroupHeader1.GroupUnion = DevExpress.XtraReports.UI.GroupUnion.WithFirstDetail
            Me.GroupHeader1.Height = 94
            Me.GroupHeader2.Level = 0
            Me.GroupHeader1.Name = "GroupHeader1"
            Me.GroupHeader1.RepeatEveryPage = True
            '
            'GroupFooter1
            '
            Me.GroupFooter1.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel27, Me.XrLabel_group1_minutes, Me.XrLabel29})
            Me.GroupFooter1.GroupUnion = DevExpress.XtraReports.UI.GroupFooterUnion.WithLastDetail
            Me.GroupFooter1.Height = 32
            Me.GroupFooter1.Level = 0
            Me.GroupFooter1.Name = "GroupFooter1"
            Me.GroupFooter1.StyleName = "XrControlStyle_Totals"
            '
            'GroupHeader2
            '
            Me.GroupHeader2.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel_hud_type})
            Me.GroupHeader2.GroupUnion = DevExpress.XtraReports.UI.GroupUnion.WithFirstDetail
            Me.GroupHeader2.Height = 40
            Me.GroupHeader2.Level = 1
            Me.GroupHeader2.Name = "GroupHeader2"
            Me.GroupHeader2.RepeatEveryPage = True
            '
            'GroupFooter2
            '
            Me.GroupFooter2.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel37, Me.XrLabel_group2_minutes, Me.XrLabel39})
            Me.GroupFooter2.GroupUnion = DevExpress.XtraReports.UI.GroupFooterUnion.WithLastDetail
            Me.GroupFooter2.Height = 32
            Me.GroupFooter2.Level = 1
            Me.GroupFooter2.Name = "GroupFooter2"
            Me.GroupFooter2.StyleName = "XrControlStyle_Totals"
            '
            'XrLabel_hud_type
            '
            Me.XrLabel_hud_type.Font = New System.Drawing.Font("Times New Roman", 14.0!, System.Drawing.FontStyle.Bold)
            Me.XrLabel_hud_type.ForeColor = System.Drawing.Color.Maroon
            Me.XrLabel_hud_type.Location = New System.Drawing.Point(0, 0)
            Me.XrLabel_hud_type.Name = "XrLabel_hud_type"
            Me.XrLabel_hud_type.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_hud_type.Size = New System.Drawing.Size(1050, 25)
            Me.XrLabel_hud_type.StylePriority.UseFont = False
            Me.XrLabel_hud_type.StylePriority.UseForeColor = False
            Me.XrLabel_hud_type.Text = "GRANT ID: [hud_type]"
            '
            'XrLabel37
            '
            Me.XrLabel37.Location = New System.Drawing.Point(533, 8)
            Me.XrLabel37.Name = "XrLabel37"
            Me.XrLabel37.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel37.Size = New System.Drawing.Size(158, 15)
            Me.XrLabel37.StylePriority.UseTextAlignment = False
            Me.XrLabel37.Text = "HUD GRANT TOTALS"
            Me.XrLabel37.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            '
            'XrLabel_group2_minutes
            '
            Me.XrLabel_group2_minutes.Location = New System.Drawing.Point(706, 8)
            Me.XrLabel_group2_minutes.Name = "XrLabel_group2_minutes"
            Me.XrLabel_group2_minutes.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_group2_minutes.Size = New System.Drawing.Size(76, 15)
            Me.XrLabel_group2_minutes.StylePriority.UseTextAlignment = False
            Me.XrLabel_group2_minutes.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel39
            '
            Me.XrLabel39.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.XrLabel39.Location = New System.Drawing.Point(792, 8)
            Me.XrLabel39.Name = "XrLabel39"
            Me.XrLabel39.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel39.Size = New System.Drawing.Size(108, 15)
            Me.XrLabel39.StylePriority.UseFont = False
            Me.XrLabel39.StylePriority.UseTextAlignment = False
            Me.XrLabel39.Text = "**"
            Me.XrLabel39.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            '
            'XrLabel_counselor
            '
            Me.XrLabel_counselor.Location = New System.Drawing.Point(950, 0)
            Me.XrLabel_counselor.Name = "XrLabel_counselor"
            Me.XrLabel_counselor.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_counselor.Size = New System.Drawing.Size(100, 15)
            Me.XrLabel_counselor.StylePriority.UseTextAlignment = False
            Me.XrLabel_counselor.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            Me.XrLabel_counselor.WordWrap = False
            '
            'XrLabel_source
            '
            Me.XrLabel_source.Location = New System.Drawing.Point(58, 0)
            Me.XrLabel_source.Name = "XrLabel_source"
            Me.XrLabel_source.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_source.Size = New System.Drawing.Size(37, 15)
            Me.XrLabel_source.StylePriority.UseTextAlignment = False
            Me.XrLabel_source.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            Me.XrLabel_source.WordWrap = False
            '
            'XrLabel_client
            '
            Me.XrLabel_client.Location = New System.Drawing.Point(100, 0)
            Me.XrLabel_client.Name = "XrLabel_client"
            Me.XrLabel_client.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_client.Size = New System.Drawing.Size(192, 15)
            Me.XrLabel_client.StylePriority.UseTextAlignment = False
            Me.XrLabel_client.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            Me.XrLabel_client.WordWrap = False
            '
            'XrLabel_ethnic
            '
            Me.XrLabel_ethnic.Location = New System.Drawing.Point(292, 0)
            Me.XrLabel_ethnic.Name = "XrLabel_ethnic"
            Me.XrLabel_ethnic.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_ethnic.Size = New System.Drawing.Size(83, 15)
            Me.XrLabel_ethnic.StylePriority.UseTextAlignment = False
            Me.XrLabel_ethnic.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            Me.XrLabel_ethnic.WordWrap = False
            '
            'XrLabel_type
            '
            Me.XrLabel_type.Location = New System.Drawing.Point(375, 0)
            Me.XrLabel_type.Name = "XrLabel_type"
            Me.XrLabel_type.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_type.Size = New System.Drawing.Size(150, 15)
            Me.XrLabel_type.StylePriority.UseTextAlignment = False
            Me.XrLabel_type.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            Me.XrLabel_type.WordWrap = False
            '
            'XrLabel_results
            '
            Me.XrLabel_results.Location = New System.Drawing.Point(533, 0)
            Me.XrLabel_results.Name = "XrLabel_results"
            Me.XrLabel_results.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_results.Size = New System.Drawing.Size(208, 15)
            Me.XrLabel_results.StylePriority.UseTextAlignment = False
            Me.XrLabel_results.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            Me.XrLabel_results.WordWrap = False
            '
            'XrLabel_minutes
            '
            Me.XrLabel_minutes.Location = New System.Drawing.Point(748, 0)
            Me.XrLabel_minutes.Name = "XrLabel_minutes"
            Me.XrLabel_minutes.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_minutes.Size = New System.Drawing.Size(34, 15)
            Me.XrLabel_minutes.StylePriority.UseTextAlignment = False
            Me.XrLabel_minutes.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrLabel_minutes.WordWrap = False
            '
            'XrLabel_housing_status
            '
            Me.XrLabel_housing_status.Location = New System.Drawing.Point(792, 0)
            Me.XrLabel_housing_status.Name = "XrLabel_housing_status"
            Me.XrLabel_housing_status.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_housing_status.Size = New System.Drawing.Size(108, 15)
            Me.XrLabel_housing_status.StylePriority.UseTextAlignment = False
            Me.XrLabel_housing_status.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            Me.XrLabel_housing_status.WordWrap = False
            '
            'XrLabel_status
            '
            Me.XrLabel_status.Location = New System.Drawing.Point(900, 0)
            Me.XrLabel_status.Name = "XrLabel_status"
            Me.XrLabel_status.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_status.Size = New System.Drawing.Size(46, 15)
            Me.XrLabel_status.StylePriority.UseTextAlignment = False
            Me.XrLabel_status.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            Me.XrLabel_status.WordWrap = False
            '
            'XrLabel_hud_id
            '
            Me.XrLabel_hud_id.Location = New System.Drawing.Point(0, 0)
            Me.XrLabel_hud_id.Name = "XrLabel_hud_id"
            Me.XrLabel_hud_id.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_hud_id.Size = New System.Drawing.Size(46, 15)
            Me.XrLabel_hud_id.StylePriority.UseTextAlignment = False
            Me.XrLabel_hud_id.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrLabel_hud_id.WordWrap = False
            '
            'DailySummaryRPPSRejects
            '
            Me.Bands.AddRange(New DevExpress.XtraReports.UI.Band() {Me.Detail, Me.PageHeader, Me.PageFooter, Me.GroupHeader1, Me.GroupFooter1, Me.ReportFooter, Me.GroupHeader2, Me.GroupFooter2})
            Me.Landscape = True
            Me.PageHeight = 850
            Me.PageWidth = 1100
            Me.Version = "8.1"
            Me.Controls.SetChildIndex(Me.GroupFooter2, 0)
            Me.Controls.SetChildIndex(Me.GroupHeader2, 0)
            Me.Controls.SetChildIndex(Me.ReportFooter, 0)
            Me.Controls.SetChildIndex(Me.GroupFooter1, 0)
            Me.Controls.SetChildIndex(Me.GroupHeader1, 0)
            Me.Controls.SetChildIndex(Me.PageFooter, 0)
            Me.Controls.SetChildIndex(Me.PageHeader, 0)
            Me.Controls.SetChildIndex(Me.Detail, 0)
            CType(Me, System.ComponentModel.ISupportInitialize).EndInit()
        End Sub

#End Region

        Public Overrides ReadOnly Property ReportTitle() As String
            Get
                Return "Housing Transactions"
            End Get
        End Property

        Dim ds As New DataSet("ds")

        Private Sub Report_BeforePrint(ByVal sender As Object, ByVal e As PrintEventArgs)

            ' Read the transactions
            Dim tbl As DataTable = ds.Tables("rpt_Housing_Transactions")
            If tbl Is Nothing Then
                Dim current_cursor As Cursor = Cursor.Current
                Dim cn As SqlConnection = New System.Data.SqlClient.SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
                Try
                    Cursor.Current = Cursors.WaitCursor
                    cn.Open()
                    Using cmd As SqlCommand = New System.Data.SqlClient.SqlCommand()
                        With cmd
                            .Connection = cn
                            .CommandText = "rpt_Housing_Transactions"
                            .CommandType = CommandType.StoredProcedure

                            SqlCommandBuilder.DeriveParameters(cmd)
                            .Parameters(1).Value = Parameter_FromDate
                            .Parameters(2).Value = Parameter_ToDate
                            .CommandTimeout = 0
                        End With

                        Using da As New SqlDataAdapter(cmd)
                            da.Fill(ds, "rpt_Housing_Transactions")
                        End Using
                    End Using

                    ' Bind the datasource to the items
                    tbl = ds.Tables("rpt_Housing_Transactions")
                    Dim vue As New DataView(tbl, String.Empty, "hud_type, date_created", DataViewRowState.CurrentRows)
                    DataSource = vue

                    ' Create a calculated field, and add it to the report's collection
                    Dim calcField As New CalculatedField(DataSource, DataMember)
                    CalculatedFields.Add(calcField)

                    ' Define its name, field type and expression.
                    With calcField
                        .Name = "modified_date_created"
                        .FieldType = FieldType.DateTime
                        .Expression = "GetDate([date_created])"
                    End With

                    ' Define the calculated field as a grouping criteria for the group header band.
                    With GroupHeader2
                        .GroupFields.Clear()
                        .GroupFields.Add(New GroupField("hud_type"))
                    End With

                    With GroupHeader1
                        .GroupFields.Clear()
                        .GroupFields.Add(New GroupField("modified_date_created"))
                    End With

                    ' Bind the fields to the datasource
                    With XrLabel_date_created
                        .DataBindings.Clear()
                        .DataBindings.Add("Text", vue, "date_created", "Transactions for {0:d}")
                    End With

                    With XrLabel_counselor
                        .DataBindings.Clear()
                        .DataBindings.Add("Text", vue, "counselor")
                    End With

                    With XrLabel_ethnic
                        .DataBindings.Clear()
                        .DataBindings.Add("Text", vue, "ethnic")
                    End With

                    With XrLabel_group1_minutes
                        .DataBindings.Clear()
                        .DataBindings.Add("Text", vue, "minutes")
                        .Summary = New XRSummary(SummaryRunning.Group, SummaryFunc.Sum, "{0:n0}")
                    End With

                    With XrLabel_group2_minutes
                        .DataBindings.Clear()
                        .DataBindings.Add("Text", vue, "minutes")
                        .Summary = New XRSummary(SummaryRunning.Group, SummaryFunc.Sum, "{0:n0}")
                    End With

                    With XrLabel_housing_status
                        .DataBindings.Clear()
                        .DataBindings.Add("Text", vue, "housing_status")
                    End With

                    With XrLabel_hud_id
                        .DataBindings.Clear()
                        .DataBindings.Add("Text", vue, "hud_id", "{0:f0}")
                    End With

                    With XrLabel_minutes
                        .DataBindings.Clear()
                        .DataBindings.Add("Text", vue, "minutes", "{0:f0}")
                    End With

                    With XrLabel_results
                        .DataBindings.Clear()
                        .DataBindings.Add("Text", vue, "results")
                    End With

                    With XrLabel_source
                        .DataBindings.Clear()
                        .DataBindings.Add("Text", vue, "source")
                    End With

                    With XrLabel_status
                        .DataBindings.Clear()
                        .DataBindings.Add("Text", vue, "status")
                    End With

                    With XrLabel_total_minutes
                        .DataBindings.Clear()
                        .DataBindings.Add("Text", vue, "minutes")
                        .Summary = New XRSummary(SummaryRunning.Report, SummaryFunc.Sum, "{0:n0}")
                    End With

                    With XrLabel_type
                        .DataBindings.Clear()
                        .DataBindings.Add("Text", vue, "type")
                    End With

                Catch ex As Exception
                    DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error reading transactions")

                Finally
                    If cn IsNot Nothing Then cn.Dispose()

                    Cursor.Current = current_cursor
                End Try
            End If
        End Sub

        Private Sub XrLabel_client_BeforePrint(ByVal sender As Object, ByVal e As PrintEventArgs)
            Dim ClientID As String = String.Format("{0:0000000}", Me.GetCurrentColumnValue("client"))
            Dim ClientName As String = DebtPlus.Utils.Nulls.DStr(Me.GetCurrentColumnValue("client_name"))
            XrLabel_client.Text = String.Format("{0} {1}", ClientID, ClientName)
        End Sub
    End Class
End Namespace
