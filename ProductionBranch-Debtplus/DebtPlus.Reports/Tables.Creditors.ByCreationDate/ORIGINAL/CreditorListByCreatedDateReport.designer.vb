Namespace Tables.Creditors.ByCreationDate
    Partial Class CreditorListByCreatedDateReport
        Inherits DebtPlus.Reports.Template.DatedTemplateXtraReportClass

        'Component overrides dispose to clean up the component list.
        Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
            If disposing Then
                If components IsNot Nothing Then
                    components.Dispose()
                End If
            End If
            MyBase.Dispose(disposing)
        End Sub

        'Required by the Designer
        Private components As System.ComponentModel.IContainer

        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
            Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(CreditorListByCreatedDateReport))
            Me.XrPanel1 = New DevExpress.XtraReports.UI.XRPanel()
            Me.XrLabel1 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel4 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel8 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel7 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel5 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel2 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_creditor_name = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_contact_phone = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_zipcode = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_outstanding_invoice_amt = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_contact_name = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_creditor = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_contribution_pct = New DevExpress.XtraReports.UI.XRLabel()
            CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
            '
            'PageHeader
            '
            Me.PageHeader.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrPanel1})
            Me.PageHeader.HeightF = 160.0!
            Me.PageHeader.Controls.SetChildIndex(Me.XrPanel1, 0)
            Me.PageHeader.Controls.SetChildIndex(Me.XrPanel_AgencyAddress, 0)
            Me.PageHeader.Controls.SetChildIndex(Me.XrLabel_Title, 0)
            Me.PageHeader.Controls.SetChildIndex(Me.XrLine_Title, 0)
            Me.PageHeader.Controls.SetChildIndex(Me.XrLabel_Subtitle, 0)
            Me.PageHeader.Controls.SetChildIndex(Me.XrPageInfo1, 0)
            '
            'XrLabel_Title
            '
            Me.XrLabel_Title.StylePriority.UseFont = False
            '
            'XrPageInfo_PageNumber
            '
            Me.XrPageInfo_PageNumber.StylePriority.UseTextAlignment = False
            '
            'XRLabel_Agency_Name
            '
            Me.XRLabel_Agency_Name.StylePriority.UseTextAlignment = False
            '
            'XrLabel_Agency_Address3
            '
            Me.XrLabel_Agency_Address3.StylePriority.UseFont = False
            Me.XrLabel_Agency_Address3.StylePriority.UseTextAlignment = False
            '
            'XrLabel_Agency_Address1
            '
            Me.XrLabel_Agency_Address1.StylePriority.UseFont = False
            Me.XrLabel_Agency_Address1.StylePriority.UseTextAlignment = False
            '
            'XrLabel_Agency_Phone
            '
            Me.XrLabel_Agency_Phone.StylePriority.UseFont = False
            Me.XrLabel_Agency_Phone.StylePriority.UseTextAlignment = False
            '
            'XrLabel_Agency_Address2
            '
            Me.XrLabel_Agency_Address2.StylePriority.UseFont = False
            Me.XrLabel_Agency_Address2.StylePriority.UseTextAlignment = False
            '
            'Detail
            '
            Me.Detail.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel_creditor, Me.XrLabel_outstanding_invoice_amt, Me.XrLabel_contact_name, Me.XrLabel_zipcode, Me.XrLabel_creditor_name, Me.XrLabel_contact_phone, Me.XrLabel_contribution_pct})
            Me.Detail.HeightF = 15.0!
            '
            'XrPanel1
            '
            Me.XrPanel1.BackColor = System.Drawing.Color.Teal
            Me.XrPanel1.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel1, Me.XrLabel4, Me.XrLabel8, Me.XrLabel7, Me.XrLabel5, Me.XrLabel2})
            Me.XrPanel1.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 133.0!)
            Me.XrPanel1.Name = "XrPanel1"
            Me.XrPanel1.SizeF = New System.Drawing.SizeF(800.0!, 17.0!)
            Me.XrPanel1.StyleName = "XrControlStyle_HeaderPannel"
            '
            'XrLabel1
            '
            Me.XrLabel1.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel1.ForeColor = System.Drawing.Color.White
            Me.XrLabel1.LocationFloat = New DevExpress.Utils.PointFloat(259.9417!, 1.0!)
            Me.XrLabel1.Name = "XrLabel1"
            Me.XrLabel1.SizeF = New System.Drawing.SizeF(183.0!, 14.99999!)
            Me.XrLabel1.StylePriority.UseTextAlignment = False
            Me.XrLabel1.Text = "CONTACT"
            Me.XrLabel1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            '
            'XrLabel4
            '
            Me.XrLabel4.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel4.ForeColor = System.Drawing.Color.White
            Me.XrLabel4.LocationFloat = New DevExpress.Utils.PointFloat(690.0!, 1.0!)
            Me.XrLabel4.Name = "XrLabel4"
            Me.XrLabel4.SizeF = New System.Drawing.SizeF(100.0!, 15.0!)
            Me.XrLabel4.StylePriority.UseTextAlignment = False
            Me.XrLabel4.Text = "INVOICED"
            Me.XrLabel4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel8
            '
            Me.XrLabel8.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel8.ForeColor = System.Drawing.Color.White
            Me.XrLabel8.LocationFloat = New DevExpress.Utils.PointFloat(563.1583!, 1.0!)
            Me.XrLabel8.Name = "XrLabel8"
            Me.XrLabel8.SizeF = New System.Drawing.SizeF(72.58331!, 14.99999!)
            Me.XrLabel8.StylePriority.UseTextAlignment = False
            Me.XrLabel8.Text = "ZIPCODE"
            Me.XrLabel8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            '
            'XrLabel7
            '
            Me.XrLabel7.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel7.ForeColor = System.Drawing.Color.White
            Me.XrLabel7.LocationFloat = New DevExpress.Utils.PointFloat(455.3!, 1.0!)
            Me.XrLabel7.Name = "XrLabel7"
            Me.XrLabel7.SizeF = New System.Drawing.SizeF(95.5!, 15.0!)
            Me.XrLabel7.StylePriority.UseTextAlignment = False
            Me.XrLabel7.Text = "PHONE"
            Me.XrLabel7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            '
            'XrLabel5
            '
            Me.XrLabel5.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel5.ForeColor = System.Drawing.Color.White
            Me.XrLabel5.LocationFloat = New DevExpress.Utils.PointFloat(654.2791!, 1.0!)
            Me.XrLabel5.Name = "XrLabel5"
            Me.XrLabel5.SizeF = New System.Drawing.SizeF(29.54169!, 15.0!)
            Me.XrLabel5.StylePriority.UseTextAlignment = False
            Me.XrLabel5.Text = "PCT"
            Me.XrLabel5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel2
            '
            Me.XrLabel2.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel2.ForeColor = System.Drawing.Color.White
            Me.XrLabel2.LocationFloat = New DevExpress.Utils.PointFloat(7.999992!, 1.0!)
            Me.XrLabel2.Name = "XrLabel2"
            Me.XrLabel2.SizeF = New System.Drawing.SizeF(92.0!, 15.0!)
            Me.XrLabel2.StylePriority.UseTextAlignment = False
            Me.XrLabel2.Text = "CREDITOR"
            Me.XrLabel2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            '
            'XrLabel_creditor_name
            '
            Me.XrLabel_creditor_name.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "creditor_name")})
            Me.XrLabel_creditor_name.LocationFloat = New DevExpress.Utils.PointFloat(76.12498!, 0.0!)
            Me.XrLabel_creditor_name.Multiline = True
            Me.XrLabel_creditor_name.Name = "XrLabel_creditor_name"
            Me.XrLabel_creditor_name.SizeF = New System.Drawing.SizeF(171.4583!, 14.99999!)
            Me.XrLabel_creditor_name.StylePriority.UseTextAlignment = False
            Me.XrLabel_creditor_name.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            Me.XrLabel_creditor_name.WordWrap = False
            '
            'XrLabel_contact_phone
            '
            Me.XrLabel_contact_phone.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "contact_phone")})
            Me.XrLabel_contact_phone.LocationFloat = New DevExpress.Utils.PointFloat(455.3!, 0.0!)
            Me.XrLabel_contact_phone.Name = "XrLabel_contact_phone"
            Me.XrLabel_contact_phone.SizeF = New System.Drawing.SizeF(95.5!, 15.0!)
            Me.XrLabel_contact_phone.StylePriority.UseTextAlignment = False
            Me.XrLabel_contact_phone.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            Me.XrLabel_contact_phone.WordWrap = False
            '
            'XrLabel_zipcode
            '
            Me.XrLabel_zipcode.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "zipcode")})
            Me.XrLabel_zipcode.LocationFloat = New DevExpress.Utils.PointFloat(563.1583!, 0.0!)
            Me.XrLabel_zipcode.Name = "XrLabel_zipcode"
            Me.XrLabel_zipcode.SizeF = New System.Drawing.SizeF(62.16663!, 14.99999!)
            Me.XrLabel_zipcode.StylePriority.UseTextAlignment = False
            Me.XrLabel_zipcode.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            Me.XrLabel_zipcode.WordWrap = False
            '
            'XrLabel_outstanding_invoice_amt
            '
            Me.XrLabel_outstanding_invoice_amt.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "outstanding_invoice_amt", "{0:c}")})
            Me.XrLabel_outstanding_invoice_amt.LocationFloat = New DevExpress.Utils.PointFloat(690.0!, 0.0!)
            Me.XrLabel_outstanding_invoice_amt.Name = "XrLabel_outstanding_invoice_amt"
            Me.XrLabel_outstanding_invoice_amt.SizeF = New System.Drawing.SizeF(100.0!, 15.0!)
            Me.XrLabel_outstanding_invoice_amt.StylePriority.UseTextAlignment = False
            Me.XrLabel_outstanding_invoice_amt.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrLabel_outstanding_invoice_amt.WordWrap = False
            '
            'XrLabel_contact_name
            '
            Me.XrLabel_contact_name.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "contact_name")})
            Me.XrLabel_contact_name.LocationFloat = New DevExpress.Utils.PointFloat(259.9417!, 0.0!)
            Me.XrLabel_contact_name.Name = "XrLabel_contact_name"
            Me.XrLabel_contact_name.SizeF = New System.Drawing.SizeF(183.0!, 14.99999!)
            Me.XrLabel_contact_name.StylePriority.UseTextAlignment = False
            Me.XrLabel_contact_name.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            Me.XrLabel_contact_name.WordWrap = False
            '
            'XrLabel_creditor
            '
            Me.XrLabel_creditor.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "creditor")})
            Me.XrLabel_creditor.LocationFloat = New DevExpress.Utils.PointFloat(7.999992!, 0.0!)
            Me.XrLabel_creditor.Name = "XrLabel_creditor"
            Me.XrLabel_creditor.SizeF = New System.Drawing.SizeF(68.12499!, 15.0!)
            Me.XrLabel_creditor.StylePriority.UseTextAlignment = False
            Me.XrLabel_creditor.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            Me.XrLabel_creditor.WordWrap = False
            '
            'XrLabel_contribution_pct
            '
            Me.XrLabel_contribution_pct.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "contribution_pct", "{0:p}")})
            Me.XrLabel_contribution_pct.LocationFloat = New DevExpress.Utils.PointFloat(625.325!, 0.0!)
            Me.XrLabel_contribution_pct.Name = "XrLabel_contribution_pct"
            Me.XrLabel_contribution_pct.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_contribution_pct.SizeF = New System.Drawing.SizeF(58.49585!, 14.99999!)
            Me.XrLabel_contribution_pct.StylePriority.UseTextAlignment = False
            Me.XrLabel_contribution_pct.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
            '
            'CreditorListByCreatedDateReport
            '
            Me.Bands.AddRange(New DevExpress.XtraReports.UI.Band() {Me.TopMarginBand1, Me.Detail, Me.BottomMarginBand1, Me.PageHeader, Me.PageFooter})
            Me.ScriptLanguage = DevExpress.XtraReports.ScriptLanguage.VisualBasic
            Me.ScriptReferencesString = "C:\Documents and Settings\Al Longyear\My Documents\Visual Studio 2008\Projects\De" & _
        "btPlus\Executables\DebtPlus.Data.dll"
            Me.Scripts.OnBeforePrint = "CreditorListByCreatedDateReport_BeforePrint"
            Me.ScriptsSource = resources.GetString("$this.ScriptsSource")
            Me.StyleSheet.AddRange(New DevExpress.XtraReports.UI.XRControlStyle() {Me.XrControlStyle_Header, Me.XrControlStyle_Totals, Me.XrControlStyle_HeaderPannel, Me.XrControlStyle_GroupHeader})
            Me.Version = "11.2"
            CType(Me, System.ComponentModel.ISupportInitialize).EndInit()

        End Sub
        Friend WithEvents XrPanel1 As DevExpress.XtraReports.UI.XRPanel
        Friend WithEvents XrLabel2 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel5 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel7 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel8 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel4 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_outstanding_invoice_amt As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_contact_name As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_zipcode As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_creditor_name As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_contact_phone As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_creditor As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel1 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_contribution_pct As DevExpress.XtraReports.UI.XRLabel
    End Class
End Namespace
