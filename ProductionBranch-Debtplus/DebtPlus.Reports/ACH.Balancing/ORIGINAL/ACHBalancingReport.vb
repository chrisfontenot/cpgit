#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}
#End Region

Option Compare Binary
Option Explicit On 
Option Strict On

Namespace ACH.Balancing
    Public Class AchBalancingReport
        Inherits DebtPlus.Reports.Template.TemplateXtraReportClass

        ''' <summary>
        '''     Create an instance of our report
        ''' </summary>
        Public Sub New()
            MyBase.New()
            InitializeComponent()

            'AddHandler BeforePrint, AddressOf AchBalancingReport_BeforePrint

            Parameter_PullDate = Date.MinValue

            '-- Indicate that we support the report filter
            ReportFilter.IsEnabled = True
        End Sub

        Public Overrides ReadOnly Property ReportTitle() As String
            Get
                Return "ACH Balancing"
            End Get
        End Property

        Public Overrides ReadOnly Property ReportSubTitle() As String
            Get
                Return System.String.Format("This report covers the ACH pull date {0:d}", Parameter_PullDate)
            End Get
        End Property

        Public Property Parameter_PullDate() As DateTime
            Get
                Return Convert.ToDateTime(ParameterPullDate.Value)
            End Get
            Set(ByVal Value As DateTime)
                ParameterPullDate.Value = Value
            End Set
        End Property

        Public Overrides Sub SetReportParameter(ByVal Parameter As String, ByVal Value As Object)
            Select Case Parameter
                Case "PullDate"
                    ParameterPullDate.Value = Value
                Case Else
                    MyBase.SetReportParameter(Parameter, Value)
            End Select
        End Sub

        Public Overrides Function NeedParameters() As Boolean
            Return MyBase.NeedParameters() OrElse Parameter_PullDate = Date.MinValue
        End Function

        Public Overrides Function RequestReportParameters() As System.Windows.Forms.DialogResult
            Dim Answer As System.Windows.Forms.DialogResult = DialogResult.OK

            If NeedParameters() Then
                Using frm As New ParametersForm()
                    With frm
                        Answer = .ShowDialog
                        Parameter_PullDate = .Parameter_PullDate
                    End With
                End Using
            End If

            Return Answer
        End Function

#Region " Component Designer generated code "

        'Component overrides dispose to clean up the component list.
        Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
            If disposing Then
                If components IsNot Nothing Then
                    components.Dispose()
                End If
            End If
            MyBase.Dispose(disposing)
        End Sub

        'Required by the Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Designer
        'It can be modified using the Designer.
        'Do not modify it using the code editor.
        Friend WithEvents XrPanel1 As DevExpress.XtraReports.UI.XRPanel
        Friend WithEvents XrLabel1 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel2 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel3 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel5 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel6 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_Client As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_Name As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_Normal As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_Pull As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_Variance As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_Reason As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_Total_Normal As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_Total_Pull As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_Total_Variance As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_Total_Name As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents ReportFooter As DevExpress.XtraReports.UI.ReportFooterBand
        Friend WithEvents XrLabel4 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLine1 As DevExpress.XtraReports.UI.XRLine
        Friend WithEvents ParameterPullDate As DevExpress.XtraReports.Parameters.Parameter
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
            Dim XrSummary1 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary()
            Dim XrSummary2 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary()
            Dim XrSummary3 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary()
            Dim XrSummary4 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary()
            Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(AchBalancingReport))
            Me.XrPanel1 = New DevExpress.XtraReports.UI.XRPanel()
            Me.XrLabel4 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel6 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel5 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel3 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel2 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel1 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_Client = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_Name = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_Normal = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_Pull = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_Variance = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_Reason = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_Total_Normal = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_Total_Pull = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_Total_Variance = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_Total_Name = New DevExpress.XtraReports.UI.XRLabel()
            Me.ReportFooter = New DevExpress.XtraReports.UI.ReportFooterBand()
            Me.XrLine1 = New DevExpress.XtraReports.UI.XRLine()
            Me.ParameterPullDate = New DevExpress.XtraReports.Parameters.Parameter()
            CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
            '
            'PageHeader
            '
            Me.PageHeader.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrPanel1})
            Me.PageHeader.HeightF = 142.0!
            Me.PageHeader.Controls.SetChildIndex(Me.XrPanel1, 0)
            Me.PageHeader.Controls.SetChildIndex(Me.XrPanel_AgencyAddress, 0)
            Me.PageHeader.Controls.SetChildIndex(Me.XrLabel_Title, 0)
            Me.PageHeader.Controls.SetChildIndex(Me.XrLine_Title, 0)
            Me.PageHeader.Controls.SetChildIndex(Me.XrLabel_Subtitle, 0)
            Me.PageHeader.Controls.SetChildIndex(Me.XrPageInfo1, 0)
            '
            'XrLabel_Title
            '
            Me.XrLabel_Title.StylePriority.UseFont = False
            '
            'XrPageInfo_PageNumber
            '
            Me.XrPageInfo_PageNumber.StylePriority.UseTextAlignment = False
            '
            'XRLabel_Agency_Name
            '
            Me.XRLabel_Agency_Name.StylePriority.UseTextAlignment = False
            '
            'XrLabel_Agency_Address3
            '
            Me.XrLabel_Agency_Address3.StylePriority.UseFont = False
            Me.XrLabel_Agency_Address3.StylePriority.UseTextAlignment = False
            '
            'XrLabel_Agency_Address1
            '
            Me.XrLabel_Agency_Address1.StylePriority.UseFont = False
            Me.XrLabel_Agency_Address1.StylePriority.UseTextAlignment = False
            '
            'XrLabel_Agency_Phone
            '
            Me.XrLabel_Agency_Phone.StylePriority.UseFont = False
            Me.XrLabel_Agency_Phone.StylePriority.UseTextAlignment = False
            '
            'XrLabel_Agency_Address2
            '
            Me.XrLabel_Agency_Address2.StylePriority.UseFont = False
            Me.XrLabel_Agency_Address2.StylePriority.UseTextAlignment = False
            '
            'Detail
            '
            Me.Detail.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel_Reason, Me.XrLabel_Variance, Me.XrLabel_Pull, Me.XrLabel_Normal, Me.XrLabel_Name, Me.XrLabel_Client})
            Me.Detail.HeightF = 17.0!
            '
            'XrPanel1
            '
            Me.XrPanel1.BackColor = System.Drawing.Color.Teal
            Me.XrPanel1.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel4, Me.XrLabel6, Me.XrLabel5, Me.XrLabel3, Me.XrLabel2, Me.XrLabel1})
            Me.XrPanel1.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 117.0!)
            Me.XrPanel1.Name = "XrPanel1"
            Me.XrPanel1.SizeF = New System.Drawing.SizeF(800.0!, 17.0!)
            Me.XrPanel1.StyleName = "XrControlStyle_HeaderPannel"
            '
            'XrLabel4
            '
            Me.XrLabel4.BorderColor = System.Drawing.Color.Transparent
            Me.XrLabel4.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel4.ForeColor = System.Drawing.Color.White
            Me.XrLabel4.LocationFloat = New DevExpress.Utils.PointFloat(534.0!, 1.0!)
            Me.XrLabel4.Name = "XrLabel4"
            Me.XrLabel4.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel4.SizeF = New System.Drawing.SizeF(266.0!, 15.0!)
            Me.XrLabel4.Text = "REASON"
            Me.XrLabel4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            '
            'XrLabel6
            '
            Me.XrLabel6.BorderColor = System.Drawing.Color.Transparent
            Me.XrLabel6.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel6.ForeColor = System.Drawing.Color.White
            Me.XrLabel6.LocationFloat = New DevExpress.Utils.PointFloat(449.0417!, 1.0!)
            Me.XrLabel6.Name = "XrLabel6"
            Me.XrLabel6.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel6.SizeF = New System.Drawing.SizeF(80.0!, 15.0!)
            Me.XrLabel6.Text = "VARIANCE"
            Me.XrLabel6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel5
            '
            Me.XrLabel5.BorderColor = System.Drawing.Color.Transparent
            Me.XrLabel5.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel5.ForeColor = System.Drawing.Color.White
            Me.XrLabel5.LocationFloat = New DevExpress.Utils.PointFloat(289.125!, 1.0!)
            Me.XrLabel5.Name = "XrLabel5"
            Me.XrLabel5.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel5.SizeF = New System.Drawing.SizeF(75.0!, 15.0!)
            Me.XrLabel5.Text = "NORMAL"
            Me.XrLabel5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel3
            '
            Me.XrLabel3.BorderColor = System.Drawing.Color.Transparent
            Me.XrLabel3.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel3.ForeColor = System.Drawing.Color.White
            Me.XrLabel3.LocationFloat = New DevExpress.Utils.PointFloat(369.0833!, 1.0!)
            Me.XrLabel3.Name = "XrLabel3"
            Me.XrLabel3.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel3.SizeF = New System.Drawing.SizeF(75.0!, 15.0!)
            Me.XrLabel3.Text = "PULL"
            Me.XrLabel3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel2
            '
            Me.XrLabel2.BorderColor = System.Drawing.Color.Transparent
            Me.XrLabel2.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel2.ForeColor = System.Drawing.Color.White
            Me.XrLabel2.LocationFloat = New DevExpress.Utils.PointFloat(93.16667!, 1.0!)
            Me.XrLabel2.Name = "XrLabel2"
            Me.XrLabel2.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel2.SizeF = New System.Drawing.SizeF(191.0!, 15.0!)
            Me.XrLabel2.Text = "NAME"
            Me.XrLabel2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            '
            'XrLabel1
            '
            Me.XrLabel1.BorderColor = System.Drawing.Color.Transparent
            Me.XrLabel1.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel1.ForeColor = System.Drawing.Color.White
            Me.XrLabel1.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 1.999982!)
            Me.XrLabel1.Name = "XrLabel1"
            Me.XrLabel1.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel1.SizeF = New System.Drawing.SizeF(88.20834!, 15.0!)
            Me.XrLabel1.Text = "CLIENT"
            Me.XrLabel1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel_Client
            '
            Me.XrLabel_Client.CanGrow = False
            Me.XrLabel_Client.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "client", "{0:0000000}")})
            Me.XrLabel_Client.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
            Me.XrLabel_Client.Name = "XrLabel_Client"
            Me.XrLabel_Client.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_Client.Scripts.OnBeforePrint = "XrLabel_Client_BeforePrint"
            Me.XrLabel_Client.SizeF = New System.Drawing.SizeF(88.20834!, 15.0!)
            Me.XrLabel_Client.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel_Name
            '
            Me.XrLabel_Name.CanGrow = False
            Me.XrLabel_Name.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "name")})
            Me.XrLabel_Name.LocationFloat = New DevExpress.Utils.PointFloat(93.16667!, 0.0!)
            Me.XrLabel_Name.Name = "XrLabel_Name"
            Me.XrLabel_Name.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_Name.SizeF = New System.Drawing.SizeF(191.0!, 15.0!)
            Me.XrLabel_Name.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            '
            'XrLabel_Normal
            '
            Me.XrLabel_Normal.CanGrow = False
            Me.XrLabel_Normal.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "deposit_amount", "{0:c2}")})
            Me.XrLabel_Normal.LocationFloat = New DevExpress.Utils.PointFloat(289.125!, 0.0!)
            Me.XrLabel_Normal.Name = "XrLabel_Normal"
            Me.XrLabel_Normal.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_Normal.SizeF = New System.Drawing.SizeF(75.0!, 17.0!)
            Me.XrLabel_Normal.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel_Pull
            '
            Me.XrLabel_Pull.CanGrow = False
            Me.XrLabel_Pull.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "normal_amount", "{0:c2}")})
            Me.XrLabel_Pull.LocationFloat = New DevExpress.Utils.PointFloat(369.0833!, 0.0!)
            Me.XrLabel_Pull.Name = "XrLabel_Pull"
            Me.XrLabel_Pull.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_Pull.SizeF = New System.Drawing.SizeF(75.0!, 15.0!)
            Me.XrLabel_Pull.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel_Variance
            '
            Me.XrLabel_Variance.CanGrow = False
            Me.XrLabel_Variance.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "difference", "{0:c2}")})
            Me.XrLabel_Variance.LocationFloat = New DevExpress.Utils.PointFloat(449.0417!, 0.0!)
            Me.XrLabel_Variance.Name = "XrLabel_Variance"
            Me.XrLabel_Variance.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_Variance.SizeF = New System.Drawing.SizeF(80.0!, 15.0!)
            Me.XrLabel_Variance.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel_Reason
            '
            Me.XrLabel_Reason.CanGrow = False
            Me.XrLabel_Reason.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "reason")})
            Me.XrLabel_Reason.LocationFloat = New DevExpress.Utils.PointFloat(534.0!, 0.0!)
            Me.XrLabel_Reason.Name = "XrLabel_Reason"
            Me.XrLabel_Reason.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_Reason.SizeF = New System.Drawing.SizeF(266.0!, 15.0!)
            Me.XrLabel_Reason.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            '
            'XrLabel_Total_Normal
            '
            Me.XrLabel_Total_Normal.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "deposit_amount", "{0:c2}")})
            Me.XrLabel_Total_Normal.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel_Total_Normal.LocationFloat = New DevExpress.Utils.PointFloat(289.125!, 17.0!)
            Me.XrLabel_Total_Normal.Name = "XrLabel_Total_Normal"
            Me.XrLabel_Total_Normal.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_Total_Normal.SizeF = New System.Drawing.SizeF(75.0!, 17.0!)
            XrSummary1.FormatString = "{0:c2}"
            XrSummary1.Running = DevExpress.XtraReports.UI.SummaryRunning.Report
            Me.XrLabel_Total_Normal.Summary = XrSummary1
            Me.XrLabel_Total_Normal.Text = "$0.00"
            Me.XrLabel_Total_Normal.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel_Total_Pull
            '
            Me.XrLabel_Total_Pull.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "normal_amount", "{0:c2}")})
            Me.XrLabel_Total_Pull.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel_Total_Pull.LocationFloat = New DevExpress.Utils.PointFloat(369.0833!, 17.0!)
            Me.XrLabel_Total_Pull.Name = "XrLabel_Total_Pull"
            Me.XrLabel_Total_Pull.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_Total_Pull.SizeF = New System.Drawing.SizeF(75.0!, 17.0!)
            XrSummary2.FormatString = "{0:c2}"
            XrSummary2.Running = DevExpress.XtraReports.UI.SummaryRunning.Report
            Me.XrLabel_Total_Pull.Summary = XrSummary2
            Me.XrLabel_Total_Pull.Text = "$0.00"
            Me.XrLabel_Total_Pull.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel_Total_Variance
            '
            Me.XrLabel_Total_Variance.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "difference", "{0:c2}")})
            Me.XrLabel_Total_Variance.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel_Total_Variance.LocationFloat = New DevExpress.Utils.PointFloat(449.0417!, 17.0!)
            Me.XrLabel_Total_Variance.Name = "XrLabel_Total_Variance"
            Me.XrLabel_Total_Variance.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_Total_Variance.SizeF = New System.Drawing.SizeF(80.0!, 17.0!)
            XrSummary3.FormatString = "{0:c2}"
            XrSummary3.Running = DevExpress.XtraReports.UI.SummaryRunning.Report
            Me.XrLabel_Total_Variance.Summary = XrSummary3
            Me.XrLabel_Total_Variance.Text = "$0.00"
            Me.XrLabel_Total_Variance.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel_Total_Name
            '
            Me.XrLabel_Total_Name.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "client")})
            Me.XrLabel_Total_Name.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel_Total_Name.LocationFloat = New DevExpress.Utils.PointFloat(93.16667!, 17.0!)
            Me.XrLabel_Total_Name.Name = "XrLabel_Total_Name"
            Me.XrLabel_Total_Name.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_Total_Name.SizeF = New System.Drawing.SizeF(191.0!, 17.0!)
            XrSummary4.FormatString = "Total ({0:d} clients)"
            XrSummary4.Func = DevExpress.XtraReports.UI.SummaryFunc.Count
            XrSummary4.Running = DevExpress.XtraReports.UI.SummaryRunning.Report
            Me.XrLabel_Total_Name.Summary = XrSummary4
            Me.XrLabel_Total_Name.Text = "Total (0 clients)"
            Me.XrLabel_Total_Name.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            '
            'ReportFooter
            '
            Me.ReportFooter.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel_Total_Pull, Me.XrLabel_Total_Normal, Me.XrLabel_Total_Variance, Me.XrLabel_Total_Name, Me.XrLine1})
            Me.ReportFooter.HeightF = 43.0!
            Me.ReportFooter.Name = "ReportFooter"
            Me.ReportFooter.StyleName = "XrControlStyle_Totals"
            '
            'XrLine1
            '
            Me.XrLine1.ForeColor = System.Drawing.Color.Teal
            Me.XrLine1.LineWidth = 2
            Me.XrLine1.LocationFloat = New DevExpress.Utils.PointFloat(93.16667!, 7.999992!)
            Me.XrLine1.Name = "XrLine1"
            Me.XrLine1.SizeF = New System.Drawing.SizeF(435.875!, 9.0!)
            '
            'ParameterPullDate
            '
            Me.ParameterPullDate.Name = "ParameterPullDate"
            Me.ParameterPullDate.Type = GetType(Date)
            Me.ParameterPullDate.Visible = False
            '
            'AchBalancingReport
            '
            Me.Bands.AddRange(New DevExpress.XtraReports.UI.Band() {Me.Detail, Me.PageHeader, Me.PageFooter, Me.ReportFooter, Me.TopMarginBand1, Me.BottomMarginBand1})
            Me.Parameters.AddRange(New DevExpress.XtraReports.Parameters.Parameter() {Me.ParameterPullDate})
            Me.RequestParameters = False
            Me.ScriptLanguage = DevExpress.XtraReports.ScriptLanguage.VisualBasic
            Me.ScriptReferencesString = resources.GetString("$this.ScriptReferencesString")
            Me.Scripts.OnBeforePrint = "AchBalancingReport_BeforePrint"
            Me.ScriptsSource = resources.GetString("$this.ScriptsSource")
            Me.StyleSheet.AddRange(New DevExpress.XtraReports.UI.XRControlStyle() {Me.XrControlStyle_Header, Me.XrControlStyle_Totals, Me.XrControlStyle_HeaderPannel, Me.XrControlStyle_GroupHeader})
            Me.Version = "11.2"
            Me.Controls.SetChildIndex(Me.BottomMarginBand1, 0)
            Me.Controls.SetChildIndex(Me.TopMarginBand1, 0)
            Me.Controls.SetChildIndex(Me.ReportFooter, 0)
            Me.Controls.SetChildIndex(Me.PageFooter, 0)
            Me.Controls.SetChildIndex(Me.PageHeader, 0)
            Me.Controls.SetChildIndex(Me.Detail, 0)
            CType(Me, System.ComponentModel.ISupportInitialize).EndInit()

        End Sub

#End Region

        Private ReadOnly ds As New System.Data.DataSet("ds")
        Private Sub AchBalancingReport_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs)
            Const TableName As String = "rpt_ach_balance"
            Dim rpt As DevExpress.XtraReports.UI.XtraReport = CType(sender, DevExpress.XtraReports.UI.XtraReport)
            Dim Tbl As System.Data.DataTable = ds.Tables(TableName)

            If Tbl Is Nothing Then
                Using sqlInfo As DebtPlus.LINQ.SQLInfoClass = New DebtPlus.LINQ.SQLInfoClass()
                    Using cn As New System.Data.SqlClient.SqlConnection(sqlInfo.ConnectionString)
                        cn.Open()
                        Using cmd As New System.Data.SqlClient.SqlCommand()
                            cmd.Connection = cn
                            cmd.CommandText = "rpt_ach_balance"
                            cmd.CommandType = System.Data.CommandType.StoredProcedure
                            System.Data.SqlClient.SqlCommandBuilder.DeriveParameters(cmd)

                            cmd.Parameters(1).Value = rpt.Parameters("ParameterPullDate").Value
                            cmd.CommandTimeout = 0

                            Using da As New System.Data.SqlClient.SqlDataAdapter(cmd)
                                da.Fill(ds, TableName)
                                Tbl = ds.Tables(TableName)
                            End Using
                        End Using
                    End Using
                End Using
            End If

            rpt.DataSource = New System.Data.DataView(Tbl, CType(rpt, DebtPlus.Interfaces.Reports.IReportFilter).ReportFilter.ViewFilter, String.Empty, System.Data.DataViewRowState.CurrentRows)
        End Sub
    End Class
End Namespace
