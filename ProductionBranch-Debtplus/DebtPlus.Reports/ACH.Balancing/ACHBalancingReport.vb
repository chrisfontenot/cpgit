#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}
#End Region

Option Compare Binary
Option Explicit On
Option Strict On

Imports DebtPlus.Reports.Template
Imports System.IO
Imports DebtPlus.Utils

Namespace ACH.Balancing
    Public Class AchBalancingReport
        Inherits TemplateXtraReportClass

        Public Sub New()
            MyBase.New()
            InitializeComponent()

            ' Set the script references as needed
            ScriptReferences = DebtPlus.Utils.AssemblyTools.GetAllSiblingAssemblies()                                                        ' changed   

            ' Update the subreports as well
            For Each BandPtr As DevExpress.XtraReports.UI.Band In Bands
                For Each CtlPtr As DevExpress.XtraReports.UI.XRControl In BandPtr.Controls
                    Dim Rpt As DevExpress.XtraReports.UI.XRSubreport = TryCast(CtlPtr, DevExpress.XtraReports.UI.XRSubreport)
                    If Rpt IsNot Nothing Then
                        Dim RptSrc As DevExpress.XtraReports.UI.XtraReport = Rpt.ReportSource
                        If RptSrc IsNot Nothing Then
                            RptSrc.ScriptReferences = ScriptReferences                                 ' changed
                        End If
                    End If
                Next
            Next

            Parameter_PullDate = Date.MinValue

            ' Indicate that we support the report filter
            ReportFilter.IsEnabled = True
        End Sub

        Private Sub InitializeComponent()
            ' Name of the report file
            Const ReportName As String = "DebtPlus.Reports.ACH.Balancing.repx"

            ' See if there is a report reference that we can use
            Dim Fname As String = System.IO.Path.Combine(DebtPlus.Configuration.Config.ReportsDirectory, ReportName)
            Dim UseDefault As Boolean = True
            Try
                If (Not DebtPlus.Configuration.Config.LocalReportsOnly) AndAlso System.IO.File.Exists(Fname) Then
                    LoadLayout(Fname)
                    UseDefault = False
                End If

            Catch ex As System.Exception
                DebtPlus.Svc.Common.ErrorHandling.HandleErrors(ex)
            End Try

            ' Load the standard report definition if we need a new item
            If UseDefault Then
                Dim asm As Reflection.Assembly = Reflection.Assembly.GetExecutingAssembly                                           ' changed
                Using ios As Stream = asm.GetManifestResourceStream(String.Format("DebtPlus.Reports.{0}", ReportName))              ' changed
                    If ios IsNot Nothing Then LoadLayout(ios) '                                                                       changed
                End Using
            End If
        End Sub

        Public Overrides ReadOnly Property ReportTitle() As String
            Get
                Return "ACH Balancing"
            End Get
        End Property

        Public Overrides ReadOnly Property ReportSubTitle() As String
            Get
                Return System.String.Format("This report covers the ACH pull date {0:d}", Parameter_PullDate)
            End Get
        End Property

        Public Property Parameter_PullDate() As DateTime
            Get
                Return Convert.ToDateTime(Parameters("ParameterPullDate").Value)
            End Get
            Set(ByVal Value As DateTime)
                Parameters("ParameterPullDate").Value = Value
            End Set
        End Property

        Public Overrides Function NeedParameters() As Boolean
            Return MyBase.NeedParameters() OrElse Parameter_PullDate = Date.MinValue
        End Function

        Public Overrides Function RequestReportParameters() As DialogResult
            Dim answer As DialogResult = DialogResult.OK

            If NeedParameters() AndAlso AllowParameterChangesByUser Then
                Using frm As New ParametersForm()
                    With frm
                        answer = .ShowDialog()
                        Parameter_PullDate = .Parameter_PullDate
                    End With
                End Using
            End If

            Return answer
        End Function
    End Class
End Namespace
