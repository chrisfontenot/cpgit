#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}
#End Region

Namespace Letters.Client.PreBalanceVerify
    Public Class PreZeroLetterReport

        ''' <summary>
        '''     Create an instance of our report
        ''' </summary>
        Public Sub New()
            MyBase.New()
            InitializeComponent()

            'AddHandler XrBarCode_PostalCode.BeforePrint, AddressOf XrBarCode_PostalCode_BeforePrint
            'AddHandler XrLabel_Address.BeforePrint, AddressOf XrLabel_Address_BeforePrint
            'AddHandler BeforePrint, AddressOf PreZeroLetterReport_BeforePrint
        End Sub

        Private ds As New System.Data.DataSet("ds")

        Private Function ClientAddressRow(ByVal Client As System.Int32) As System.Data.DataRow
            Dim tbl As System.Data.DataTable = ds.Tables("view_client_address")
            If tbl IsNot Nothing Then
                Dim row As System.Data.DataRow = tbl.Rows.Find(Client)
                If row IsNot Nothing Then
                    Return row
                End If
            End If

            '-- Read the client information
            Dim sqlInfo As DebtPlus.LINQ.SQLInfoClass = DebtPlus.LINQ.SQLInfoClass.getDefault()
            Using cn As New System.Data.SqlClient.SqlConnection(sqlInfo.ConnectionString)
                cn.Open()
                Using cmd As New System.Data.SqlClient.SqlCommand()
                    cmd.Connection = cn
                    cmd.CommandType = System.Data.CommandType.Text
                    cmd.CommandText = "SELECT [client],[name],[addr1],[addr2],[addr3],[zipcode] FROM view_client_address WHERE client=@client"
                    cmd.Parameters.Add("@client", System.Data.SqlDbType.Int).Value = Client

                    Using da As New System.Data.SqlClient.SqlDataAdapter(cmd)
                        da.Fill(ds, "view_client_address")
                    End Using
                End Using
            End Using

            tbl = ds.Tables("view_client_address")
            With tbl
                If .PrimaryKey.GetUpperBound(0) < 0 Then .PrimaryKey = New System.Data.DataColumn() {.Columns("client")}
            End With

            Return tbl.Rows.Find(Client)
        End Function

        Private Sub XrBarCode_PostalCode_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs) ' Handles XrBarCode_PostalCode.BeforePrint
            With CType(sender, DevExpress.XtraReports.UI.XRBarCode)
                Dim rpt As DevExpress.XtraReports.UI.XtraReport = CType(.Report, DevExpress.XtraReports.UI.XtraReport)

                '-- Find the current client ID
                Dim Client As System.Int32 = -1
                If rpt.GetCurrentColumnValue("client") IsNot Nothing AndAlso rpt.GetCurrentColumnValue("client") IsNot System.DBNull.Value Then
                    Client = Convert.ToInt32(rpt.GetCurrentColumnValue("client"), System.Globalization.CultureInfo.InvariantCulture)
                End If

                '-- If there is no client then don't bother with the barcode
                If Client < 0 Then
                    e.Cancel = True
                Else

                    '-- Find the client's zipcode from the address block
                    Dim Postalcode As String = String.Empty
                    Dim row As System.Data.DataRow = ClientAddressRow(Client)
                    If row IsNot Nothing Then Postalcode = DebtPlus.Utils.Nulls.DStr(row("zipcode"))
                    .Text = Postalcode.Replace("-", "").Trim
                End If
            End With
        End Sub

        Private Sub XrLabel_Address_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs) ' Handles XrLabel_Address.BeforePrint
            With CType(sender, DevExpress.XtraReports.UI.XRLabel)
                Dim rpt As DevExpress.XtraReports.UI.XtraReport = CType(.Report, DevExpress.XtraReports.UI.XtraReport)

                '-- Find the current client ID
                Dim Client As System.Int32 = -1
                If rpt.GetCurrentColumnValue("client") IsNot Nothing AndAlso rpt.GetCurrentColumnValue("client") IsNot System.DBNull.Value Then
                    Client = Convert.ToInt32(rpt.GetCurrentColumnValue("client"), System.Globalization.CultureInfo.InvariantCulture)
                End If

                '-- If there is no client then don't bother with the barcode
                If Client < 0 Then
                    e.Cancel = True
                Else

                    Dim sb As New System.Text.StringBuilder()
                    Dim row As System.Data.DataRow = ClientAddressRow(Client)
                    If row IsNot Nothing Then
                        For Each Key As String In New String() {"name", "addr1", "addr2", "addr3"}
                            Dim str As String = DebtPlus.Utils.Nulls.DStr(row(Key))
                            If str <> String.Empty Then
                                sb.Append(System.Environment.NewLine)
                                sb.Append(str)
                            End If
                        Next
                        If sb.Length > 0 Then sb.Remove(0, 2)
                    End If

                    .Text = sb.ToString()
                End If
            End With
        End Sub

        Private Sub PreZeroLetterReport_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs) ' Handles Me.BeforePrint
            Const TableName As String = "rpt_pre_zero_balance_letter"
            Dim rpt As DevExpress.XtraReports.UI.XtraReport = CType(sender, DevExpress.XtraReports.UI.XtraReport)
            Dim tbl As System.Data.DataTable = ds.Tables(TableName)
            If tbl Is Nothing Then

                '-- Find the client address information
                Try
                    Dim sqlInfo As DebtPlus.LINQ.SQLInfoClass = DebtPlus.LINQ.SQLInfoClass.getDefault()
                    Using cn As System.Data.SqlClient.SqlConnection = New System.Data.SqlClient.SqlConnection(sqlInfo.ConnectionString)
                        cn.Open()
                        Using cmd As New System.Data.SqlClient.SqlCommand()
                            cmd.Connection = cn
                            cmd.CommandText = "rpt_pre_zero_balance_letter"
                            cmd.CommandType = System.Data.CommandType.StoredProcedure
                            cmd.CommandTimeout = 0

                            Using da As New System.Data.SqlClient.SqlDataAdapter(cmd)
                                da.Fill(ds, TableName)
                            End Using
                        End Using
                    End Using

                    '-- Find the datasource
                    tbl = ds.Tables(TableName)

                Catch ex As System.Data.SqlClient.SqlException
                    Using gdr As New DebtPlus.Repository.GetDataResult()
                        gdr.HandleException(ex)
                        DebtPlus.UI.Common.ErrorHandling.HandleErrors(gdr, String.Format("Error in reading {0}", TableName))
                    End Using
                End Try
            End If

            If tbl IsNot Nothing Then
                rpt.DataSource = tbl.DefaultView
            End If
        End Sub

    End Class
End Namespace
