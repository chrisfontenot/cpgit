Namespace Letters.Client.PreBalanceVerify
    <Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
    Partial Class PreZeroLetterReport
        Inherits DebtPlus.Reports.Template.BaseXtraReportClass

        'XtraReport overrides dispose to clean up the component list.
        <System.Diagnostics.DebuggerNonUserCode()> _
        Protected Overrides Sub Dispose(ByVal disposing As Boolean)
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
            MyBase.Dispose(disposing)
        End Sub

        'Required by the Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Designer
        'It can be modified using the Designer.  
        'Do not modify it using the code editor.
        <System.Diagnostics.DebuggerStepThrough()> _
        Private Sub InitializeComponent()
            Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(PreZeroLetterReport))
            Dim PostNetGenerator1 As DevExpress.XtraPrinting.BarCode.PostNetGenerator = New DevExpress.XtraPrinting.BarCode.PostNetGenerator()
            Me.PageHeader = New DevExpress.XtraReports.UI.PageHeaderBand()
            Me.XrRichText1 = New DevExpress.XtraReports.UI.XRRichText()
            Me.XrPictureBox1 = New DevExpress.XtraReports.UI.XRPictureBox()
            Me.XrRichText2 = New DevExpress.XtraReports.UI.XRRichText()
            Me.XrPanel_ClientAddress = New DevExpress.XtraReports.UI.XRPanel()
            Me.XrBarCode_PostalCode = New DevExpress.XtraReports.UI.XRBarCode()
            Me.XrLabel_Address = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrTable1 = New DevExpress.XtraReports.UI.XRTable()
            Me.XrTableRow1 = New DevExpress.XtraReports.UI.XRTableRow()
            Me.XrTableCell1 = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableCell_creditor_name = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableRow2 = New DevExpress.XtraReports.UI.XRTableRow()
            Me.XrTableCell3 = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableCell_account_number = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableRow3 = New DevExpress.XtraReports.UI.XRTableRow()
            Me.XrTableCell5 = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableCell_debit_amt = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableRow4 = New DevExpress.XtraReports.UI.XRTableRow()
            Me.XrTableCell7 = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableCell_date_created = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableRow5 = New DevExpress.XtraReports.UI.XRTableRow()
            Me.XrTableCell2 = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableCell_balance = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrLabel2 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel3 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrPageInfo1 = New DevExpress.XtraReports.UI.XRPageInfo()
            CType(Me.XrRichText1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.XrRichText2, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.XrTable1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
            '
            'Detail
            '
            Me.Detail.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrRichText2, Me.XrPanel_ClientAddress, Me.XrTable1, Me.XrLabel2, Me.XrLabel3, Me.XrPageInfo1})
            Me.Detail.HeightF = 850.8749!
            Me.Detail.PageBreak = DevExpress.XtraReports.UI.PageBreak.AfterBand
            Me.Detail.StylePriority.UseFont = False
            '
            'PageHeader
            '
            Me.PageHeader.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrRichText1, Me.XrPictureBox1})
            Me.PageHeader.HeightF = 125.0!
            Me.PageHeader.Name = "PageHeader"
            '
            'XrRichText1
            '
            Me.XrRichText1.LocationFloat = New DevExpress.Utils.PointFloat(413.9167!, 10.00001!)
            Me.XrRichText1.Name = "XrRichText1"
            Me.XrRichText1.Padding = New DevExpress.XtraPrinting.PaddingInfo(5, 5, 5, 5, 100.0!)
            Me.XrRichText1.SerializableRtfString = resources.GetString("XrRichText1.SerializableRtfString")
            Me.XrRichText1.SizeF = New System.Drawing.SizeF(386.0833!, 95.91663!)
            Me.XrRichText1.StylePriority.UsePadding = False
            '
            'XrPictureBox1
            '
            Me.XrPictureBox1.Image = CType(resources.GetObject("XrPictureBox1.Image"), System.Drawing.Image)
            Me.XrPictureBox1.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 28.45832!)
            Me.XrPictureBox1.Name = "XrPictureBox1"
            Me.XrPictureBox1.SizeF = New System.Drawing.SizeF(300.0!, 59.0!)
            Me.XrPictureBox1.Sizing = DevExpress.XtraPrinting.ImageSizeMode.ZoomImage
            '
            'XrRichText2
            '
            Me.XrRichText2.LocationFloat = New DevExpress.Utils.PointFloat(50.0!, 204.125!)
            Me.XrRichText2.Name = "XrRichText2"
            Me.XrRichText2.Padding = New DevExpress.XtraPrinting.PaddingInfo(5, 5, 5, 5, 100.0!)
            Me.XrRichText2.SerializableRtfString = resources.GetString("XrRichText2.SerializableRtfString")
            Me.XrRichText2.SizeF = New System.Drawing.SizeF(722.9167!, 466.0!)
            Me.XrRichText2.StylePriority.UsePadding = False
            '
            'XrPanel_ClientAddress
            '
            Me.XrPanel_ClientAddress.AnchorVertical = DevExpress.XtraReports.UI.VerticalAnchorStyles.Top
            Me.XrPanel_ClientAddress.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrBarCode_PostalCode, Me.XrLabel_Address})
            Me.XrPanel_ClientAddress.LocationFloat = New DevExpress.Utils.PointFloat(100.0!, 64.99999!)
            Me.XrPanel_ClientAddress.Name = "XrPanel_ClientAddress"
            Me.XrPanel_ClientAddress.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrPanel_ClientAddress.SizeF = New System.Drawing.SizeF(300.0!, 85.41666!)
            '
            'XrBarCode_PostalCode
            '
            Me.XrBarCode_PostalCode.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
            Me.XrBarCode_PostalCode.Name = "XrBarCode_PostalCode"
            Me.XrBarCode_PostalCode.Scripts.OnBeforePrint = "XrBarCode_PostalCode_BeforePrint"
            Me.XrBarCode_PostalCode.ShowText = False
            Me.XrBarCode_PostalCode.SizeF = New System.Drawing.SizeF(300.0!, 17.0!)
            Me.XrBarCode_PostalCode.StylePriority.UsePadding = False
            Me.XrBarCode_PostalCode.Symbology = PostNetGenerator1
            Me.XrBarCode_PostalCode.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
            '
            'XrLabel_Address
            '
            Me.XrLabel_Address.Font = New System.Drawing.Font("Courier New", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.XrLabel_Address.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 17.0!)
            Me.XrLabel_Address.Multiline = True
            Me.XrLabel_Address.Name = "XrLabel_Address"
            Me.XrLabel_Address.Scripts.OnBeforePrint = "XrLabel_Address_BeforePrint"
            Me.XrLabel_Address.SizeF = New System.Drawing.SizeF(300.0!, 67.375!)
            Me.XrLabel_Address.StylePriority.UseFont = False
            Me.XrLabel_Address.Text = "Address Line 1" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Address Line 2" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "City State Zip"
            Me.XrLabel_Address.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
            Me.XrLabel_Address.WordWrap = False
            '
            'XrTable1
            '
            Me.XrTable1.LocationFloat = New DevExpress.Utils.PointFloat(50.0!, 682.3333!)
            Me.XrTable1.Name = "XrTable1"
            Me.XrTable1.Padding = New DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 100.0!)
            Me.XrTable1.Rows.AddRange(New DevExpress.XtraReports.UI.XRTableRow() {Me.XrTableRow1, Me.XrTableRow2, Me.XrTableRow3, Me.XrTableRow4, Me.XrTableRow5})
            Me.XrTable1.SizeF = New System.Drawing.SizeF(700.0!, 90.0!)
            Me.XrTable1.StylePriority.UsePadding = False
            '
            'XrTableRow1
            '
            Me.XrTableRow1.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.XrTableCell1, Me.XrTableCell_creditor_name})
            Me.XrTableRow1.Name = "XrTableRow1"
            Me.XrTableRow1.Weight = 0.19780219780219777R
            '
            'XrTableCell1
            '
            Me.XrTableCell1.Font = New System.Drawing.Font("Calibri", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.XrTableCell1.Name = "XrTableCell1"
            Me.XrTableCell1.StylePriority.UseFont = False
            Me.XrTableCell1.Text = "Creditor Name:"
            Me.XrTableCell1.Weight = 0.67844522968197885R
            '
            'XrTableCell_creditor_name
            '
            Me.XrTableCell_creditor_name.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "creditor_name")})
            Me.XrTableCell_creditor_name.Name = "XrTableCell_creditor_name"
            Me.XrTableCell_creditor_name.Weight = 1.9399293286219079R
            '
            'XrTableRow2
            '
            Me.XrTableRow2.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.XrTableCell3, Me.XrTableCell_account_number})
            Me.XrTableRow2.Name = "XrTableRow2"
            Me.XrTableRow2.Weight = 0.19780219780219777R
            '
            'XrTableCell3
            '
            Me.XrTableCell3.Font = New System.Drawing.Font("Calibri", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.XrTableCell3.Name = "XrTableCell3"
            Me.XrTableCell3.StylePriority.UseFont = False
            Me.XrTableCell3.Text = "Creditor Account Number:"
            Me.XrTableCell3.Weight = 0.67844522968197885R
            '
            'XrTableCell_account_number
            '
            Me.XrTableCell_account_number.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "account_number")})
            Me.XrTableCell_account_number.Name = "XrTableCell_account_number"
            Me.XrTableCell_account_number.Weight = 1.9399293286219079R
            '
            'XrTableRow3
            '
            Me.XrTableRow3.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.XrTableCell5, Me.XrTableCell_debit_amt})
            Me.XrTableRow3.Name = "XrTableRow3"
            Me.XrTableRow3.Weight = 0.19780219780219777R
            '
            'XrTableCell5
            '
            Me.XrTableCell5.Font = New System.Drawing.Font("Calibri", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.XrTableCell5.Name = "XrTableCell5"
            Me.XrTableCell5.StylePriority.UseFont = False
            Me.XrTableCell5.Text = "Last Payment Amount:"
            Me.XrTableCell5.Weight = 0.67844522968197885R
            '
            'XrTableCell_debit_amt
            '
            Me.XrTableCell_debit_amt.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "debit_amt", "{0:c}")})
            Me.XrTableCell_debit_amt.Name = "XrTableCell_debit_amt"
            Me.XrTableCell_debit_amt.Weight = 1.9399293286219079R
            '
            'XrTableRow4
            '
            Me.XrTableRow4.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.XrTableCell7, Me.XrTableCell_date_created})
            Me.XrTableRow4.Name = "XrTableRow4"
            Me.XrTableRow4.Weight = 0.19780219780219777R
            '
            'XrTableCell7
            '
            Me.XrTableCell7.Font = New System.Drawing.Font("Calibri", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.XrTableCell7.Name = "XrTableCell7"
            Me.XrTableCell7.StylePriority.UseFont = False
            Me.XrTableCell7.Text = "Last Payment Date:"
            Me.XrTableCell7.Weight = 0.67844522968197885R
            '
            'XrTableCell_date_created
            '
            Me.XrTableCell_date_created.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "date_created", "{0:MMMM d, yyyy}")})
            Me.XrTableCell_date_created.Name = "XrTableCell_date_created"
            Me.XrTableCell_date_created.Weight = 1.9399293286219079R
            '
            'XrTableRow5
            '
            Me.XrTableRow5.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.XrTableCell2, Me.XrTableCell_balance})
            Me.XrTableRow5.Name = "XrTableRow5"
            Me.XrTableRow5.Weight = 0.19780219780219777R
            '
            'XrTableCell2
            '
            Me.XrTableCell2.Font = New System.Drawing.Font("Calibri", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.XrTableCell2.Name = "XrTableCell2"
            Me.XrTableCell2.StylePriority.UseFont = False
            Me.XrTableCell2.Text = "Current Balance"
            Me.XrTableCell2.Weight = 0.67844522968197885R
            '
            'XrTableCell_balance
            '
            Me.XrTableCell_balance.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "balance", "{0:c}")})
            Me.XrTableCell_balance.Name = "XrTableCell_balance"
            Me.XrTableCell_balance.Weight = 1.9399293286219079R
            '
            'XrLabel2
            '
            Me.XrLabel2.LocationFloat = New DevExpress.Utils.PointFloat(50.0!, 784.1249!)
            Me.XrLabel2.Multiline = True
            Me.XrLabel2.Name = "XrLabel2"
            Me.XrLabel2.Padding = New DevExpress.XtraPrinting.PaddingInfo(5, 5, 5, 5, 100.0!)
            Me.XrLabel2.SizeF = New System.Drawing.SizeF(292.0!, 66.75!)
            Me.XrLabel2.StylePriority.UsePadding = False
            Me.XrLabel2.Text = "Sincerely," & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Clearpoint"
            Me.XrLabel2.WordWrap = False
            '
            'XrLabel3
            '
            Me.XrLabel3.LocationFloat = New DevExpress.Utils.PointFloat(50.0!, 174.6666!)
            Me.XrLabel3.Name = "XrLabel3"
            Me.XrLabel3.Padding = New DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 100.0!)
            Me.XrLabel3.SizeF = New System.Drawing.SizeF(300.0!, 19.08334!)
            Me.XrLabel3.StylePriority.UsePadding = False
            Me.XrLabel3.Text = "Dear Client(s):"
            '
            'XrPageInfo1
            '
            Me.XrPageInfo1.Format = "{0:MMMM d, yyyy}"
            Me.XrPageInfo1.LocationFloat = New DevExpress.Utils.PointFloat(50.0!, 10.00001!)
            Me.XrPageInfo1.Name = "XrPageInfo1"
            Me.XrPageInfo1.Padding = New DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 100.0!)
            Me.XrPageInfo1.PageInfo = DevExpress.XtraPrinting.PageInfo.DateTime
            Me.XrPageInfo1.SizeF = New System.Drawing.SizeF(283.0!, 19.08333!)
            Me.XrPageInfo1.StylePriority.UseTextAlignment = False
            Me.XrPageInfo1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            '
            'PreZeroLetterReport
            '
            Me.Bands.AddRange(New DevExpress.XtraReports.UI.Band() {Me.TopMarginBand1, Me.Detail, Me.BottomMarginBand1, Me.PageHeader})
            Me.Font = New System.Drawing.Font("Calibri", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.ReportPrintOptions.DetailCountOnEmptyDataSource = 0
            Me.ReportPrintOptions.PrintOnEmptyDataSource = False
            Me.ScriptLanguage = DevExpress.XtraReports.ScriptLanguage.VisualBasic
            Me.ScriptReferencesString = resources.GetString("$this.ScriptReferencesString")
            Me.Scripts.OnBeforePrint = "PreZeroLetterReport_BeforePrint"
            Me.ScriptsSource = resources.GetString("$this.ScriptsSource")
            Me.Version = "11.2"
            Me.Controls.SetChildIndex(Me.PageHeader, 0)
            Me.Controls.SetChildIndex(Me.BottomMarginBand1, 0)
            Me.Controls.SetChildIndex(Me.Detail, 0)
            Me.Controls.SetChildIndex(Me.TopMarginBand1, 0)
            CType(Me.XrRichText1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.XrRichText2, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.XrTable1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me, System.ComponentModel.ISupportInitialize).EndInit()

        End Sub
        Friend WithEvents PageHeader As DevExpress.XtraReports.UI.PageHeaderBand
        Friend WithEvents XrPictureBox1 As DevExpress.XtraReports.UI.XRPictureBox

        Friend WithEvents XrPanel_ClientAddress As DevExpress.XtraReports.UI.XRPanel
        Friend WithEvents XrBarCode_PostalCode As DevExpress.XtraReports.UI.XRBarCode
        Friend WithEvents XrLabel_Address As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrTable1 As DevExpress.XtraReports.UI.XRTable
        Friend WithEvents XrTableRow1 As DevExpress.XtraReports.UI.XRTableRow
        Friend WithEvents XrTableCell1 As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents XrTableCell_creditor_name As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents XrTableRow2 As DevExpress.XtraReports.UI.XRTableRow
        Friend WithEvents XrTableCell3 As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents XrTableCell_account_number As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents XrTableRow3 As DevExpress.XtraReports.UI.XRTableRow
        Friend WithEvents XrTableCell5 As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents XrTableCell_debit_amt As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents XrTableRow4 As DevExpress.XtraReports.UI.XRTableRow
        Friend WithEvents XrTableCell7 As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents XrTableCell_date_created As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents XrLabel2 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel3 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrPageInfo1 As DevExpress.XtraReports.UI.XRPageInfo
        Friend WithEvents XrRichText1 As DevExpress.XtraReports.UI.XRRichText
        Friend WithEvents XrTableRow5 As DevExpress.XtraReports.UI.XRTableRow
        Friend WithEvents XrTableCell2 As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents XrTableCell_balance As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents XrRichText2 As DevExpress.XtraReports.UI.XRRichText
    End Class
End Namespace
