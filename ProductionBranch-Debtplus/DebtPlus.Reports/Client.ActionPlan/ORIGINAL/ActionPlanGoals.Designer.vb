Namespace Client.ActionPlan
    <Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
    Partial Public Class ActionPlanGoals
        Inherits DevExpress.XtraReports.UI.XtraReport

        'XtraReport overrides dispose to clean up the component list.
        <System.Diagnostics.DebuggerNonUserCode()> _
        Protected Overrides Sub Dispose(ByVal disposing As Boolean)
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
            MyBase.Dispose(disposing)
        End Sub

        'Required by the Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Designer
        'It can be modified using the Designer.  
        'Do not modify it using the code editor.
        <System.Diagnostics.DebuggerStepThrough()> _
        Private Sub InitializeComponent()
            Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(ActionPlanGoals))
            Me.XrControlStyle_HeaderPannel = New DevExpress.XtraReports.UI.XRControlStyle()
            Me.XrRichText1 = New DevExpress.XtraReports.UI.XRRichText()
            Me.XrControlStyle_Totals = New DevExpress.XtraReports.UI.XRControlStyle()
            Me.XrControlStyle_GroupHeader = New DevExpress.XtraReports.UI.XRControlStyle()
            Me.Detail = New DevExpress.XtraReports.UI.DetailBand()
            Me.XrLabel1 = New DevExpress.XtraReports.UI.XRLabel()
            Me.ReportHeader = New DevExpress.XtraReports.UI.ReportHeaderBand()
            Me.BottomMargin = New DevExpress.XtraReports.UI.BottomMarginBand()
            Me.XrControlStyle_Header = New DevExpress.XtraReports.UI.XRControlStyle()
            Me.TopMargin = New DevExpress.XtraReports.UI.TopMarginBand()
            Me.ReportFooter = New DevExpress.XtraReports.UI.ReportFooterBand()
            Me.XrLabel2 = New DevExpress.XtraReports.UI.XRLabel()
            CType(Me.XrRichText1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
            '
            'XrControlStyle_HeaderPannel
            '
            Me.XrControlStyle_HeaderPannel.BackColor = System.Drawing.Color.Teal
            Me.XrControlStyle_HeaderPannel.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.XrControlStyle_HeaderPannel.ForeColor = System.Drawing.Color.White
            Me.XrControlStyle_HeaderPannel.Name = "XrControlStyle_HeaderPannel"
            Me.XrControlStyle_HeaderPannel.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
            Me.XrControlStyle_HeaderPannel.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrRichText1
            '
            Me.XrRichText1.Borders = DevExpress.XtraPrinting.BorderSide.None
            Me.XrRichText1.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
            Me.XrRichText1.Name = "XrRichText1"
            Me.XrRichText1.SerializableRtfString = resources.GetString("XrRichText1.SerializableRtfString")
            Me.XrRichText1.SizeF = New System.Drawing.SizeF(389.0!, 26.625!)
            Me.XrRichText1.StylePriority.UseBorders = False
            '
            'XrControlStyle_Totals
            '
            Me.XrControlStyle_Totals.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.XrControlStyle_Totals.Name = "XrControlStyle_Totals"
            Me.XrControlStyle_Totals.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
            Me.XrControlStyle_Totals.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrControlStyle_GroupHeader
            '
            Me.XrControlStyle_GroupHeader.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.XrControlStyle_GroupHeader.ForeColor = System.Drawing.Color.Maroon
            Me.XrControlStyle_GroupHeader.Name = "XrControlStyle_GroupHeader"
            Me.XrControlStyle_GroupHeader.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            '
            'Detail
            '
            Me.Detail.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrRichText1})
            Me.Detail.HeightF = 32.875!
            Me.Detail.Name = "Detail"
            Me.Detail.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
            Me.Detail.StylePriority.UseTextAlignment = False
            Me.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
            '
            'XrLabel1
            '
            Me.XrLabel1.BorderColor = System.Drawing.Color.Teal
            Me.XrLabel1.Borders = DevExpress.XtraPrinting.BorderSide.Bottom
            Me.XrLabel1.BorderWidth = 2
            Me.XrLabel1.Font = New System.Drawing.Font("Times New Roman", 12.0!, System.Drawing.FontStyle.Bold)
            Me.XrLabel1.ForeColor = System.Drawing.Color.Teal
            Me.XrLabel1.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
            Me.XrLabel1.Name = "XrLabel1"
            Me.XrLabel1.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel1.SizeF = New System.Drawing.SizeF(389.0!, 25.0!)
            Me.XrLabel1.StylePriority.UseBorderColor = False
            Me.XrLabel1.StylePriority.UseBorders = False
            Me.XrLabel1.StylePriority.UseBorderWidth = False
            Me.XrLabel1.StylePriority.UseFont = False
            Me.XrLabel1.StylePriority.UseForeColor = False
            Me.XrLabel1.StylePriority.UseTextAlignment = False
            Me.XrLabel1.Text = "GOALS AND OBJECTIVES"
            Me.XrLabel1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
            '
            'ReportHeader
            '
            Me.ReportHeader.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel1})
            Me.ReportHeader.HeightF = 25.0!
            Me.ReportHeader.Name = "ReportHeader"
            '
            'BottomMargin
            '
            Me.BottomMargin.HeightF = 0.0!
            Me.BottomMargin.Name = "BottomMargin"
            Me.BottomMargin.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
            Me.BottomMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
            '
            'XrControlStyle_Header
            '
            Me.XrControlStyle_Header.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.XrControlStyle_Header.ForeColor = System.Drawing.Color.Teal
            Me.XrControlStyle_Header.Name = "XrControlStyle_Header"
            Me.XrControlStyle_Header.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
            Me.XrControlStyle_Header.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            '
            'TopMargin
            '
            Me.TopMargin.HeightF = 0.0!
            Me.TopMargin.Name = "TopMargin"
            Me.TopMargin.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
            Me.TopMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
            '
            'ReportFooter
            '
            Me.ReportFooter.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel2})
            Me.ReportFooter.HeightF = 78.125!
            Me.ReportFooter.Name = "ReportFooter"
            Me.ReportFooter.PrintAtBottom = True
            Me.ReportFooter.Visible = False
            '
            'XrLabel2
            '
            Me.XrLabel2.Borders = DevExpress.XtraPrinting.BorderSide.Top
            Me.XrLabel2.BorderWidth = 2
            Me.XrLabel2.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 50.0!)
            Me.XrLabel2.Name = "XrLabel2"
            Me.XrLabel2.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel2.SizeF = New System.Drawing.SizeF(389.0!, 22.99998!)
            Me.XrLabel2.StylePriority.UseBorders = False
            Me.XrLabel2.StylePriority.UseBorderWidth = False
            Me.XrLabel2.Text = "Applicant's Signature"
            '
            'ActionPlanGoals
            '
            Me.Bands.AddRange(New DevExpress.XtraReports.UI.Band() {Me.Detail, Me.TopMargin, Me.BottomMargin, Me.ReportHeader, Me.ReportFooter})
            Me.Margins = New System.Drawing.Printing.Margins(0, 461, 0, 0)
            Me.RequestParameters = False
            Me.ScriptLanguage = DevExpress.XtraReports.ScriptLanguage.VisualBasic
            Me.ScriptReferencesString = resources.GetString("$this.ScriptReferencesString")
            Me.Scripts.OnBeforePrint = "ActionPlanGoals_BeforePrint"
            Me.ScriptsSource = resources.GetString("$this.ScriptsSource")
            Me.StyleSheet.AddRange(New DevExpress.XtraReports.UI.XRControlStyle() {Me.XrControlStyle_Header, Me.XrControlStyle_Totals, Me.XrControlStyle_HeaderPannel, Me.XrControlStyle_GroupHeader})
            Me.Version = "11.2"
            CType(Me.XrRichText1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me, System.ComponentModel.ISupportInitialize).EndInit()

        End Sub
        Friend WithEvents XrControlStyle_HeaderPannel As DevExpress.XtraReports.UI.XRControlStyle
        Friend WithEvents XrRichText1 As DevExpress.XtraReports.UI.XRRichText
        Friend WithEvents XrControlStyle_Totals As DevExpress.XtraReports.UI.XRControlStyle
        Friend WithEvents XrControlStyle_GroupHeader As DevExpress.XtraReports.UI.XRControlStyle
        Friend WithEvents Detail As DevExpress.XtraReports.UI.DetailBand
        Friend WithEvents XrLabel1 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents ReportHeader As DevExpress.XtraReports.UI.ReportHeaderBand
        Friend WithEvents BottomMargin As DevExpress.XtraReports.UI.BottomMarginBand
        Friend WithEvents XrControlStyle_Header As DevExpress.XtraReports.UI.XRControlStyle
        Friend WithEvents TopMargin As DevExpress.XtraReports.UI.TopMarginBand
        Friend WithEvents ReportFooter As DevExpress.XtraReports.UI.ReportFooterBand
        Friend WithEvents XrLabel2 As DevExpress.XtraReports.UI.XRLabel
    End Class
End Namespace
