#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}
#End Region

Namespace Client.ActionPlan
    Public Class ActionPlanGoals

        Public Sub New()
            MyBase.New()
            InitializeComponent()
            AddHandler BeforePrint, AddressOf ActionPlanGoals_BeforePrint
        End Sub

        Dim ds As New System.Data.DataSet("ds")

        Private Sub ActionPlanGoals_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs)
            Const TableName As String = "action_items_goals"
            Dim Rpt As DevExpress.XtraReports.UI.XtraReport = CType(sender, DevExpress.XtraReports.UI.XtraReport)
            Dim MasterRpt As DevExpress.XtraReports.UI.XtraReport = CType(Rpt.MasterReport, DevExpress.XtraReports.UI.XtraReport)
            Dim sqlInfo As DebtPlus.LINQ.SQLInfoClass = DebtPlus.LINQ.SQLInfoClass.getDefault()
            Dim tbl As System.Data.DataTable = ds.Tables(TableName)
            Try
                Using cn As System.Data.SqlClient.SqlConnection = New System.Data.SqlClient.SqlConnection(sqlInfo.ConnectionString)
                    cn.Open()
                    Dim ActionPlan As Integer = CType(MasterRpt.Parameters("ParameterActionPlan").Value, Integer)
                    If ActionPlan <= 0 Then
                        Dim Client As Integer = CType(MasterRpt.Parameters("ParameterClient").Value, Integer)

                        Using cmd As New System.Data.SqlClient.SqlCommand()
                                cmd.Connection = cn
                                cmd.CommandText = "SELECT dbo.map_client_to_action_plan(@client)"
                                cmd.CommandType = System.Data.CommandType.Text
                                cmd.Parameters.Add("@client", System.Data.SqlDbType.Int).Value = Client
                                Dim obj As Object = cmd.ExecuteScalar()
                                ActionPlan = DebtPlus.Utils.Nulls.DInt(obj)
                        End Using
                    End If

                    If ActionPlan > 0 Then
                        Using cmd As New System.Data.SqlClient.SqlCommand()
                            cmd.Connection = cn
                            cmd.CommandText = "SELECT [text] FROM [action_items_goals] WITH (NOLOCK) WHERE [action_plan] = @ActionPlan"
                            cmd.CommandType = System.Data.CommandType.Text
                            cmd.CommandTimeout = 0
                            cmd.Parameters.Add("@ActionPlan", System.Data.SqlDbType.Int).Value = ActionPlan

                            Using da As New System.Data.SqlClient.SqlDataAdapter(cmd)
                                da.Fill(ds, TableName)
                            End Using
                        End Using

                        tbl = ds.Tables(TableName)
                    End If
                End Using

            Catch ex As System.Data.SqlClient.SqlException
                Using gdr As New DebtPlus.Repository.GetDataResult()
                    gdr.HandleException(ex)
                    DebtPlus.UI.Common.ErrorHandling.HandleErrors(gdr, "Error reading report information")
                End Using
            End Try

            '-- Find the text string for the goals information
            Dim RTFText As String
            If tbl IsNot Nothing AndAlso tbl.Rows.Count > 0 Then
                RTFText = DebtPlus.Utils.Nulls.DStr(tbl.Rows(0)(0)).Trim
            Else
                RTFText = String.Empty
            End If

            '-- Ensure that the text is somewhat valid for RTF
            If RTFText = String.Empty Then RTFText = "{\rtf1 }"

            '-- Set the RTF text into the display control.
            Dim Item As DevExpress.XtraReports.UI.XRRichText = TryCast(Rpt.FindControl("XrRichText1", True), DevExpress.XtraReports.UI.XRRichText)
            If Item IsNot Nothing Then
                Try
                    If RTFText.StartsWith("{\") Then
                        Item.Rtf = RTFText
                    Else
                        Item.Text = RTFText
                    End If

                Catch ex As System.Exception
                    Item.Text = RTFText
                End Try
            End If
        End Sub
    End Class
End Namespace
