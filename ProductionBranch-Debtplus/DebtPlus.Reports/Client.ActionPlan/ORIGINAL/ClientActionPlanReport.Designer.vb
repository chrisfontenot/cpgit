Namespace Client.ActionPlan
    <Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
    Partial Public Class ClientActionPlanReport
        Inherits DebtPlus.Reports.Template.TemplateXtraReportClass

        'XtraReport overrides dispose to clean up the component list.
        <System.Diagnostics.DebuggerNonUserCode()> _
        Protected Overrides Sub Dispose(ByVal disposing As Boolean)
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
            MyBase.Dispose(disposing)
        End Sub

        'Required by the Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Designer
        'It can be modified using the Designer.  
        'Do not modify it using the code editor.
        <System.Diagnostics.DebuggerStepThrough()> _
        Private Sub InitializeComponent()
            Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(ClientActionPlanReport))
            Me.ParameterClient = New DevExpress.XtraReports.Parameters.Parameter()
            Me.ParameterActionPlan = New DevExpress.XtraReports.Parameters.Parameter()
            Me.XrLabel_ClientID = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_ClientName = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrSubreport_Goals = New DevExpress.XtraReports.UI.XRSubreport()
            Me.ActionPlanGoals1 = New DebtPlus.Reports.Client.ActionPlan.ActionPlanGoals()
            Me.XrSubreport_Items = New DevExpress.XtraReports.UI.XRSubreport()
            Me.ActionPlanItems1 = New DebtPlus.Reports.Client.ActionPlan.ActionPlanItems()
            CType(Me.ActionPlanGoals1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.ActionPlanItems1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
            '
            'PageFooter
            '
            Me.PageFooter.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel_ClientName, Me.XrLabel_ClientID})
            Me.PageFooter.HeightF = 39.74997!
            Me.PageFooter.Controls.SetChildIndex(Me.XrLabel_ClientID, 0)
            Me.PageFooter.Controls.SetChildIndex(Me.XrLabel_ClientName, 0)
            Me.PageFooter.Controls.SetChildIndex(Me.XrPageInfo_PageNumber, 0)
            '
            'XrLabel_Title
            '
            Me.XrLabel_Title.StylePriority.UseFont = False
            '
            'XrPageInfo_PageNumber
            '
            Me.XrPageInfo_PageNumber.LocationFloat = New DevExpress.Utils.PointFloat(609.0!, 0.0!)
            Me.XrPageInfo_PageNumber.StylePriority.UseTextAlignment = False
            '
            'XRLabel_Agency_Name
            '
            Me.XRLabel_Agency_Name.StylePriority.UseTextAlignment = False
            '
            'XrLabel_Agency_Address3
            '
            Me.XrLabel_Agency_Address3.StylePriority.UseFont = False
            Me.XrLabel_Agency_Address3.StylePriority.UseTextAlignment = False
            '
            'XrLabel_Agency_Address1
            '
            Me.XrLabel_Agency_Address1.StylePriority.UseFont = False
            Me.XrLabel_Agency_Address1.StylePriority.UseTextAlignment = False
            '
            'XrLabel_Agency_Phone
            '
            Me.XrLabel_Agency_Phone.StylePriority.UseFont = False
            Me.XrLabel_Agency_Phone.StylePriority.UseTextAlignment = False
            '
            'XrLabel_Agency_Address2
            '
            Me.XrLabel_Agency_Address2.StylePriority.UseFont = False
            Me.XrLabel_Agency_Address2.StylePriority.UseTextAlignment = False
            '
            'Detail
            '
            Me.Detail.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrSubreport_Goals, Me.XrSubreport_Items})
            Me.Detail.HeightF = 23.0!
            '
            'ParameterClient
            '
            Me.ParameterClient.Description = "Client ID"
            Me.ParameterClient.Name = "ParameterClient"
            Me.ParameterClient.Type = GetType(Integer)
            Me.ParameterClient.Value = -1
            Me.ParameterClient.Visible = False
            '
            'ParameterActionPlan
            '
            Me.ParameterActionPlan.Description = "Action Plan ID"
            Me.ParameterActionPlan.Name = "ParameterActionPlan"
            Me.ParameterActionPlan.Type = GetType(Integer)
            Me.ParameterActionPlan.Value = -1
            Me.ParameterActionPlan.Visible = False
            '
            'XrLabel_ClientID
            '
            Me.XrLabel_ClientID.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel_ClientID.ForeColor = System.Drawing.Color.Teal
            Me.XrLabel_ClientID.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
            Me.XrLabel_ClientID.Name = "XrLabel_ClientID"
            Me.XrLabel_ClientID.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_ClientID.Scripts.OnBeforePrint = "XrLabel_ClientID_BeforePrint"
            Me.XrLabel_ClientID.SizeF = New System.Drawing.SizeF(400.0!, 17.0!)
            Me.XrLabel_ClientID.StylePriority.UseFont = False
            Me.XrLabel_ClientID.StylePriority.UseForeColor = False
            '
            'XrLabel_ClientName
            '
            Me.XrLabel_ClientName.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel_ClientName.ForeColor = System.Drawing.Color.Teal
            Me.XrLabel_ClientName.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 17.0!)
            Me.XrLabel_ClientName.Name = "XrLabel_ClientName"
            Me.XrLabel_ClientName.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_ClientName.Scripts.OnBeforePrint = "XrLabel_ClientName_BeforePrint"
            Me.XrLabel_ClientName.SizeF = New System.Drawing.SizeF(742.0!, 17.0!)
            Me.XrLabel_ClientName.StylePriority.UseFont = False
            Me.XrLabel_ClientName.StylePriority.UseForeColor = False
            '
            'XrSubreport_Goals
            '
            Me.XrSubreport_Goals.LocationFloat = New DevExpress.Utils.PointFloat(400.0!, 0.0!)
            Me.XrSubreport_Goals.Name = "XrSubreport_Goals"
            Me.XrSubreport_Goals.ReportSource = Me.ActionPlanGoals1
            Me.XrSubreport_Goals.SizeF = New System.Drawing.SizeF(386.125!, 23.0!)
            '
            'XrSubreport_Items
            '
            Me.XrSubreport_Items.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
            Me.XrSubreport_Items.Name = "XrSubreport_Items"
            Me.XrSubreport_Items.ReportSource = Me.ActionPlanItems1
            Me.XrSubreport_Items.SizeF = New System.Drawing.SizeF(400.0!, 23.0!)
            '
            'ClientActionPlanReport
            '
            Me.Bands.AddRange(New DevExpress.XtraReports.UI.Band() {Me.TopMarginBand1, Me.Detail, Me.BottomMarginBand1, Me.PageHeader, Me.PageFooter})
            Me.Parameters.AddRange(New DevExpress.XtraReports.Parameters.Parameter() {Me.ParameterClient, Me.ParameterActionPlan})
            Me.RequestParameters = False
            Me.ScriptLanguage = DevExpress.XtraReports.ScriptLanguage.VisualBasic
            Me.ScriptReferencesString = resources.GetString("$this.ScriptReferencesString")
            Me.ScriptsSource = resources.GetString("$this.ScriptsSource")
            Me.StyleSheet.AddRange(New DevExpress.XtraReports.UI.XRControlStyle() {Me.XrControlStyle_Header, Me.XrControlStyle_Totals, Me.XrControlStyle_HeaderPannel, Me.XrControlStyle_GroupHeader})
            Me.Version = "11.2"
            CType(Me.ActionPlanGoals1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.ActionPlanItems1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me, System.ComponentModel.ISupportInitialize).EndInit()

        End Sub
        Friend WithEvents XrSubreport_Items As DevExpress.XtraReports.UI.XRSubreport
        Friend WithEvents XrSubreport_Goals As DevExpress.XtraReports.UI.XRSubreport
        Friend WithEvents ParameterClient As DevExpress.XtraReports.Parameters.Parameter
        Private WithEvents ActionPlanGoals1 As DebtPlus.Reports.Client.ActionPlan.ActionPlanGoals
        Private WithEvents ActionPlanItems1 As DebtPlus.Reports.Client.ActionPlan.ActionPlanItems
        Friend WithEvents ParameterActionPlan As DevExpress.XtraReports.Parameters.Parameter
        Friend WithEvents XrLabel_ClientName As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_ClientID As DevExpress.XtraReports.UI.XRLabel
    End Class
End Namespace
