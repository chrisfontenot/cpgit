#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}

#End Region

Option Compare Binary
Option Explicit On
Option Strict On

Imports System.ComponentModel
Imports DebtPlus.Reports.Template

Imports System.Drawing.Printing
Imports System.Data.SqlClient
Imports DevExpress.XtraReports.UI

Namespace Housing.Demographics
    Public Class HousingDemoInformationReport
        Inherits DatedTemplateXtraReportClass

        ''' <summary>
        ''' Create an instance of our report
        ''' </summary>
        Public Sub New()
            MyBase.New()
            InitializeComponent()
            AddHandler Me.BeforePrint, AddressOf Report_BeforePrint
        End Sub

#Region " Component Designer generated code "

        'Component overrides dispose to clean up the component list.
        Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
            If disposing Then
                If components IsNot Nothing Then
                    components.Dispose()
                End If
                ds.Dispose()
            End If
            MyBase.Dispose(disposing)
        End Sub

        Friend WithEvents XrControlStyle1 As DevExpress.XtraReports.UI.XRControlStyle
        Friend WithEvents XrControlStyle2 As DevExpress.XtraReports.UI.XRControlStyle
        Friend WithEvents XrLabel_description As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_count_cl As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_count_total As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_units As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_count_ap As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents GroupHeader1 As DevExpress.XtraReports.UI.GroupHeaderBand
        Friend WithEvents XrLabel1 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrPanel1 As DevExpress.XtraReports.UI.XRPanel
        Friend WithEvents XrLabel9 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel6 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel4 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel3 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel2 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents GroupFooter1 As DevExpress.XtraReports.UI.GroupFooterBand
        Friend WithEvents XrLabel_group_count_cl As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_group_units As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_group_count_total As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_group_count_ap As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents ReportFooter As DevExpress.XtraReports.UI.ReportFooterBand
        Friend WithEvents XrLabel_total_count_cl As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_total_units As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_total_count_total As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_total_count_ap As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel5 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel7 As DevExpress.XtraReports.UI.XRLabel

        'Required by the Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Designer
        'It can be modified using the Designer.
        'Do not modify it using the code editor.
        <System.Diagnostics.DebuggerStepThrough()>
        Private Sub InitializeComponent()
            Dim XrSummary1 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary
            Dim XrSummary2 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary
            Dim XrSummary3 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary
            Dim XrSummary4 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary
            Dim XrSummary5 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary
            Dim XrSummary6 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary
            Dim XrSummary7 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary
            Dim XrSummary8 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary
            Me.XrControlStyle1 = New DevExpress.XtraReports.UI.XRControlStyle
            Me.XrControlStyle2 = New DevExpress.XtraReports.UI.XRControlStyle
            Me.XrLabel_count_ap = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_units = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_count_total = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_count_cl = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_description = New DevExpress.XtraReports.UI.XRLabel
            Me.GroupHeader1 = New DevExpress.XtraReports.UI.GroupHeaderBand
            Me.XrLabel1 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrPanel1 = New DevExpress.XtraReports.UI.XRPanel
            Me.XrLabel9 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel6 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel4 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel3 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel2 = New DevExpress.XtraReports.UI.XRLabel
            Me.GroupFooter1 = New DevExpress.XtraReports.UI.GroupFooterBand
            Me.XrLabel5 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_group_count_cl = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_group_units = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_group_count_total = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_group_count_ap = New DevExpress.XtraReports.UI.XRLabel
            Me.ReportFooter = New DevExpress.XtraReports.UI.ReportFooterBand
            Me.XrLabel7 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_total_count_cl = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_total_units = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_total_count_total = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_total_count_ap = New DevExpress.XtraReports.UI.XRLabel
            CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
            '
            'Detail
            '
            Me.Detail.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel_description, Me.XrLabel_count_cl, Me.XrLabel_count_total, Me.XrLabel_units, Me.XrLabel_count_ap})
            Me.Detail.HeightF = 15.0!
            Me.Detail.StyleName = "XrControlStyle2"
            '
            'PageHeader
            '
            Me.PageHeader.HeightF = 113.0!
            '
            'XrLabel_Title
            '
            Me.XrLabel_Title.StylePriority.UseFont = False
            '
            'XrPageInfo_PageNumber
            '
            Me.XrPageInfo_PageNumber.LocationFloat = New DevExpress.Utils.PointFloat(917.0!, 8.0!)
            Me.XrPageInfo_PageNumber.StylePriority.UseTextAlignment = False
            '
            'XrPanel_AgencyAddress
            '
            Me.XrPanel_AgencyAddress.LocationFloat = New DevExpress.Utils.PointFloat(742.0!, 0.0!)
            '
            'XRLabel_Agency_Name
            '
            Me.XRLabel_Agency_Name.StylePriority.UseTextAlignment = False
            '
            'XrLabel_Agency_Address3
            '
            Me.XrLabel_Agency_Address3.StylePriority.UseFont = False
            Me.XrLabel_Agency_Address3.StylePriority.UseTextAlignment = False
            '
            'XrLabel_Agency_Address1
            '
            Me.XrLabel_Agency_Address1.StylePriority.UseFont = False
            Me.XrLabel_Agency_Address1.StylePriority.UseTextAlignment = False
            '
            'XrLabel_Agency_Phone
            '
            Me.XrLabel_Agency_Phone.StylePriority.UseFont = False
            Me.XrLabel_Agency_Phone.StylePriority.UseTextAlignment = False
            '
            'XrLabel_Agency_Address2
            '
            Me.XrLabel_Agency_Address2.StylePriority.UseFont = False
            Me.XrLabel_Agency_Address2.StylePriority.UseTextAlignment = False
            '
            'XrControlStyle1
            '
            Me.XrControlStyle1.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.XrControlStyle1.ForeColor = System.Drawing.Color.White
            Me.XrControlStyle1.Name = "XrControlStyle1"
            Me.XrControlStyle1.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
            Me.XrControlStyle1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            '
            'XrControlStyle2
            '
            Me.XrControlStyle2.Name = "XrControlStyle2"
            Me.XrControlStyle2.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
            Me.XrControlStyle2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            '
            'XrLabel_count_ap
            '
            Me.XrLabel_count_ap.LocationFloat = New DevExpress.Utils.PointFloat(416.0!, 0.0!)
            Me.XrLabel_count_ap.Name = "XrLabel_count_ap"
            Me.XrLabel_count_ap.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_count_ap.SizeF = New System.Drawing.SizeF(83.0!, 15.0!)
            Me.XrLabel_count_ap.StylePriority.UseTextAlignment = False
            Me.XrLabel_count_ap.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrLabel_count_ap.WordWrap = False
            '
            'XrLabel_units
            '
            Me.XrLabel_units.LocationFloat = New DevExpress.Utils.PointFloat(347.0!, 0.0!)
            Me.XrLabel_units.Name = "XrLabel_units"
            Me.XrLabel_units.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_units.SizeF = New System.Drawing.SizeF(60.0!, 15.0!)
            Me.XrLabel_units.StylePriority.UseTextAlignment = False
            Me.XrLabel_units.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrLabel_units.WordWrap = False
            '
            'XrLabel_count_total
            '
            Me.XrLabel_count_total.LocationFloat = New DevExpress.Utils.PointFloat(608.0!, 0.0!)
            Me.XrLabel_count_total.Name = "XrLabel_count_total"
            Me.XrLabel_count_total.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_count_total.SizeF = New System.Drawing.SizeF(91.0!, 15.0!)
            Me.XrLabel_count_total.StylePriority.UseTextAlignment = False
            Me.XrLabel_count_total.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrLabel_count_total.WordWrap = False
            '
            'XrLabel_count_cl
            '
            Me.XrLabel_count_cl.LocationFloat = New DevExpress.Utils.PointFloat(516.0!, 0.0!)
            Me.XrLabel_count_cl.Name = "XrLabel_count_cl"
            Me.XrLabel_count_cl.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_count_cl.SizeF = New System.Drawing.SizeF(83.0!, 15.0!)
            Me.XrLabel_count_cl.StylePriority.UseTextAlignment = False
            Me.XrLabel_count_cl.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrLabel_count_cl.WordWrap = False
            '
            'XrLabel_description
            '
            Me.XrLabel_description.CanGrow = False
            Me.XrLabel_description.LocationFloat = New DevExpress.Utils.PointFloat(8.0!, 0.0!)
            Me.XrLabel_description.Name = "XrLabel_description"
            Me.XrLabel_description.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_description.SizeF = New System.Drawing.SizeF(316.0!, 15.0!)
            Me.XrLabel_description.WordWrap = False
            '
            'GroupHeader1
            '
            Me.GroupHeader1.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel1, Me.XrPanel1})
            Me.GroupHeader1.GroupUnion = DevExpress.XtraReports.UI.GroupUnion.WithFirstDetail
            Me.GroupHeader1.HeightF = 89.0!
            Me.GroupHeader1.Name = "GroupHeader1"
            Me.GroupHeader1.RepeatEveryPage = True
            '
            'XrLabel1
            '
            Me.XrLabel1.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel1.ForeColor = System.Drawing.Color.Maroon
            Me.XrLabel1.LocationFloat = New DevExpress.Utils.PointFloat(7.999992!, 10.00001!)
            Me.XrLabel1.Name = "XrLabel1"
            Me.XrLabel1.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel1.SizeF = New System.Drawing.SizeF(1032.0!, 22.99998!)
            Me.XrLabel1.StylePriority.UseFont = False
            Me.XrLabel1.StylePriority.UseForeColor = False
            Me.XrLabel1.Text = "SUMMARY TOTALS FOR [type]"
            '
            'XrPanel1
            '
            Me.XrPanel1.BackColor = System.Drawing.Color.Teal
            Me.XrPanel1.BorderColor = System.Drawing.Color.Teal
            Me.XrPanel1.CanGrow = False
            Me.XrPanel1.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel9, Me.XrLabel6, Me.XrLabel4, Me.XrLabel3, Me.XrLabel2})
            Me.XrPanel1.KeepTogether = False
            Me.XrPanel1.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 45.0!)
            Me.XrPanel1.Name = "XrPanel1"
            Me.XrPanel1.SizeF = New System.Drawing.SizeF(1050.0!, 34.0!)
            Me.XrPanel1.StyleName = "XrControlStyle_HeaderPannel"
            Me.XrPanel1.StylePriority.UseBackColor = False
            Me.XrPanel1.StylePriority.UseBorderColor = False
            '
            'XrLabel9
            '
            Me.XrLabel9.LocationFloat = New DevExpress.Utils.PointFloat(416.0!, 1.0!)
            Me.XrLabel9.Multiline = True
            Me.XrLabel9.Name = "XrLabel9"
            Me.XrLabel9.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel9.SizeF = New System.Drawing.SizeF(83.0!, 33.0!)
            Me.XrLabel9.StylePriority.UseTextAlignment = False
            Me.XrLabel9.Text = "# OF APPTS" & ChrW(13) & ChrW(10) & "HOUSING"
            Me.XrLabel9.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight
            '
            'XrLabel6
            '
            Me.XrLabel6.LocationFloat = New DevExpress.Utils.PointFloat(7.999992!, 0.9999924!)
            Me.XrLabel6.Name = "XrLabel6"
            Me.XrLabel6.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel6.SizeF = New System.Drawing.SizeF(316.0!, 32.99998!)
            Me.XrLabel6.StylePriority.UseTextAlignment = False
            Me.XrLabel6.Text = "DESCRIPTION"
            Me.XrLabel6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft
            '
            'XrLabel4
            '
            Me.XrLabel4.LocationFloat = New DevExpress.Utils.PointFloat(516.0!, 1.0!)
            Me.XrLabel4.Name = "XrLabel4"
            Me.XrLabel4.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel4.SizeF = New System.Drawing.SizeF(83.0!, 33.0!)
            Me.XrLabel4.StylePriority.UseTextAlignment = False
            Me.XrLabel4.Text = "# OF HUD RESULTS"
            Me.XrLabel4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight
            '
            'XrLabel3
            '
            Me.XrLabel3.LocationFloat = New DevExpress.Utils.PointFloat(608.0!, 1.0!)
            Me.XrLabel3.Multiline = True
            Me.XrLabel3.Name = "XrLabel3"
            Me.XrLabel3.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel3.SizeF = New System.Drawing.SizeF(91.0!, 33.0!)
            Me.XrLabel3.StylePriority.UseTextAlignment = False
            Me.XrLabel3.Text = "" & ChrW(13) & ChrW(10) & "TOTAL #"
            Me.XrLabel3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight
            '
            'XrLabel2
            '
            Me.XrLabel2.LocationFloat = New DevExpress.Utils.PointFloat(347.0!, 1.0!)
            Me.XrLabel2.Multiline = True
            Me.XrLabel2.Name = "XrLabel2"
            Me.XrLabel2.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel2.SizeF = New System.Drawing.SizeF(60.0!, 33.0!)
            Me.XrLabel2.StylePriority.UseTextAlignment = False
            Me.XrLabel2.Text = "# OF" & ChrW(13) & ChrW(10) & "UNITS"
            Me.XrLabel2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight
            '
            'GroupFooter1
            '
            Me.GroupFooter1.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel5, Me.XrLabel_group_count_cl, Me.XrLabel_group_units, Me.XrLabel_group_count_total, Me.XrLabel_group_count_ap})
            Me.GroupFooter1.GroupUnion = DevExpress.XtraReports.UI.GroupFooterUnion.WithLastDetail
            Me.GroupFooter1.HeightF = 35.0!
            Me.GroupFooter1.Name = "GroupFooter1"
            Me.GroupFooter1.StyleName = "XrControlStyle_Totals"
            '
            'XrLabel5
            '
            Me.XrLabel5.LocationFloat = New DevExpress.Utils.PointFloat(7.999992!, 10.00001!)
            Me.XrLabel5.Name = "XrLabel5"
            Me.XrLabel5.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel5.SizeF = New System.Drawing.SizeF(100.0!, 15.0!)
            Me.XrLabel5.StylePriority.UseTextAlignment = False
            Me.XrLabel5.Text = "SUB-TOTALS"
            Me.XrLabel5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            '
            'XrLabel_group_count_cl
            '
            Me.XrLabel_group_count_cl.LocationFloat = New DevExpress.Utils.PointFloat(516.0!, 10.00001!)
            Me.XrLabel_group_count_cl.Name = "XrLabel_group_count_cl"
            Me.XrLabel_group_count_cl.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_group_count_cl.SizeF = New System.Drawing.SizeF(83.0!, 15.0!)
            Me.XrLabel_group_count_cl.StylePriority.UseTextAlignment = False
            XrSummary1.FormatString = "{0:n0}"
            XrSummary1.Running = DevExpress.XtraReports.UI.SummaryRunning.Group
            Me.XrLabel_group_count_cl.Summary = XrSummary1
            Me.XrLabel_group_count_cl.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrLabel_group_count_cl.WordWrap = False
            '
            'XrLabel_group_units
            '
            Me.XrLabel_group_units.LocationFloat = New DevExpress.Utils.PointFloat(347.0!, 10.00001!)
            Me.XrLabel_group_units.Name = "XrLabel_group_units"
            Me.XrLabel_group_units.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_group_units.SizeF = New System.Drawing.SizeF(60.0!, 15.0!)
            Me.XrLabel_group_units.StylePriority.UseTextAlignment = False
            XrSummary2.FormatString = "{0:n0}"
            XrSummary2.Running = DevExpress.XtraReports.UI.SummaryRunning.Group
            Me.XrLabel_group_units.Summary = XrSummary2
            Me.XrLabel_group_units.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrLabel_group_units.WordWrap = False
            '
            'XrLabel_group_count_total
            '
            Me.XrLabel_group_count_total.LocationFloat = New DevExpress.Utils.PointFloat(608.0!, 10.00001!)
            Me.XrLabel_group_count_total.Name = "XrLabel_group_count_total"
            Me.XrLabel_group_count_total.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_group_count_total.SizeF = New System.Drawing.SizeF(91.0!, 15.0!)
            Me.XrLabel_group_count_total.StylePriority.UseTextAlignment = False
            XrSummary3.FormatString = "{0:n0}"
            XrSummary3.Running = DevExpress.XtraReports.UI.SummaryRunning.Group
            Me.XrLabel_group_count_total.Summary = XrSummary3
            Me.XrLabel_group_count_total.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrLabel_group_count_total.WordWrap = False
            '
            'XrLabel_group_count_ap
            '
            Me.XrLabel_group_count_ap.LocationFloat = New DevExpress.Utils.PointFloat(416.0!, 10.00001!)
            Me.XrLabel_group_count_ap.Name = "XrLabel_group_count_ap"
            Me.XrLabel_group_count_ap.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_group_count_ap.SizeF = New System.Drawing.SizeF(83.0!, 15.0!)
            Me.XrLabel_group_count_ap.StylePriority.UseTextAlignment = False
            XrSummary4.FormatString = "{0:n0}"
            XrSummary4.Running = DevExpress.XtraReports.UI.SummaryRunning.Group
            Me.XrLabel_group_count_ap.Summary = XrSummary4
            Me.XrLabel_group_count_ap.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrLabel_group_count_ap.WordWrap = False
            '
            'ReportFooter
            '
            Me.ReportFooter.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel7, Me.XrLabel_total_count_cl, Me.XrLabel_total_units, Me.XrLabel_total_count_total, Me.XrLabel_total_count_ap})
            Me.ReportFooter.HeightF = 36.0!
            Me.ReportFooter.Name = "ReportFooter"
            Me.ReportFooter.StyleName = "XrControlStyle_Totals"
            '
            'XrLabel7
            '
            Me.XrLabel7.LocationFloat = New DevExpress.Utils.PointFloat(10.00001!, 9.999974!)
            Me.XrLabel7.Name = "XrLabel7"
            Me.XrLabel7.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel7.SizeF = New System.Drawing.SizeF(100.0!, 15.0!)
            Me.XrLabel7.StylePriority.UseTextAlignment = False
            Me.XrLabel7.Text = "TOTALS"
            Me.XrLabel7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            '
            'XrLabel_total_count_cl
            '
            Me.XrLabel_total_count_cl.LocationFloat = New DevExpress.Utils.PointFloat(516.0!, 9.999974!)
            Me.XrLabel_total_count_cl.Name = "XrLabel_total_count_cl"
            Me.XrLabel_total_count_cl.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_total_count_cl.SizeF = New System.Drawing.SizeF(83.0!, 15.0!)
            Me.XrLabel_total_count_cl.StylePriority.UseTextAlignment = False
            XrSummary5.FormatString = "{0:n0}"
            XrSummary5.Running = DevExpress.XtraReports.UI.SummaryRunning.Report
            Me.XrLabel_total_count_cl.Summary = XrSummary5
            Me.XrLabel_total_count_cl.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrLabel_total_count_cl.WordWrap = False
            '
            'XrLabel_total_units
            '
            Me.XrLabel_total_units.LocationFloat = New DevExpress.Utils.PointFloat(347.0!, 9.999974!)
            Me.XrLabel_total_units.Name = "XrLabel_total_units"
            Me.XrLabel_total_units.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_total_units.SizeF = New System.Drawing.SizeF(60.0!, 15.0!)
            Me.XrLabel_total_units.StylePriority.UseTextAlignment = False
            XrSummary6.FormatString = "{0:n0}"
            XrSummary6.Running = DevExpress.XtraReports.UI.SummaryRunning.Report
            Me.XrLabel_total_units.Summary = XrSummary6
            Me.XrLabel_total_units.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrLabel_total_units.WordWrap = False
            '
            'XrLabel_total_count_total
            '
            Me.XrLabel_total_count_total.LocationFloat = New DevExpress.Utils.PointFloat(608.0001!, 9.999974!)
            Me.XrLabel_total_count_total.Name = "XrLabel_total_count_total"
            Me.XrLabel_total_count_total.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_total_count_total.SizeF = New System.Drawing.SizeF(91.0!, 15.0!)
            Me.XrLabel_total_count_total.StylePriority.UseTextAlignment = False
            XrSummary7.FormatString = "{0:n0}"
            XrSummary7.Running = DevExpress.XtraReports.UI.SummaryRunning.Report
            Me.XrLabel_total_count_total.Summary = XrSummary7
            Me.XrLabel_total_count_total.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrLabel_total_count_total.WordWrap = False
            '
            'XrLabel_total_count_ap
            '
            Me.XrLabel_total_count_ap.LocationFloat = New DevExpress.Utils.PointFloat(416.0!, 9.999974!)
            Me.XrLabel_total_count_ap.Name = "XrLabel_total_count_ap"
            Me.XrLabel_total_count_ap.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_total_count_ap.SizeF = New System.Drawing.SizeF(83.0!, 15.0!)
            Me.XrLabel_total_count_ap.StylePriority.UseTextAlignment = False
            XrSummary8.FormatString = "{0:n0}"
            XrSummary8.Running = DevExpress.XtraReports.UI.SummaryRunning.Report
            Me.XrLabel_total_count_ap.Summary = XrSummary8
            Me.XrLabel_total_count_ap.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrLabel_total_count_ap.WordWrap = False
            '
            'HousingDemoInformationReport
            '
            Me.Bands.AddRange(New DevExpress.XtraReports.UI.Band() {Me.Detail, Me.PageHeader, Me.PageFooter, Me.GroupHeader1, Me.GroupFooter1, Me.ReportFooter})
            Me.Landscape = True
            Me.PageHeight = 850
            Me.PageWidth = 1100
            Me.StyleSheet.AddRange(New DevExpress.XtraReports.UI.XRControlStyle() {Me.XrControlStyle1, Me.XrControlStyle2})
            Me.Version = "10.1"
            Me.Controls.SetChildIndex(Me.ReportFooter, 0)
            Me.Controls.SetChildIndex(Me.GroupFooter1, 0)
            Me.Controls.SetChildIndex(Me.GroupHeader1, 0)
            Me.Controls.SetChildIndex(Me.PageFooter, 0)
            Me.Controls.SetChildIndex(Me.PageHeader, 0)
            Me.Controls.SetChildIndex(Me.Detail, 0)
            CType(Me, System.ComponentModel.ISupportInitialize).EndInit()
        End Sub

#End Region

        Public Overrides ReadOnly Property ReportTitle() As String
            Get
                Return "Housing Demographic Statistics"
            End Get
        End Property

        Dim ds As New DataSet("ds")
        Dim vue As DataView = Nothing

        Private Sub Report_BeforePrint(ByVal sender As Object, ByVal e As PrintEventArgs)

            ' Read the transactions
            Dim cn As SqlConnection = New System.Data.SqlClient.SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
            Dim current_cursor As Cursor = Cursor.Current
            Try
                Cursor.Current = Cursors.WaitCursor
                cn.Open()
                Using cmd As SqlCommand = New System.Data.SqlClient.SqlCommand()
                    With cmd
                        .Connection = cn
                        .CommandText = "rpt_housing_demographics"
                        .CommandType = CommandType.StoredProcedure
                        .CommandTimeout = 0
                        .Parameters.Add("@FromDate", SqlDbType.DateTime).Value = Parameter_FromDate
                        .Parameters.Add("@ToDate", SqlDbType.DateTime).Value = Parameter_ToDate
                    End With

                    Using da As New SqlDataAdapter(cmd)
                        da.Fill(ds, "rpt_housing_demographics")
                        With ds.Tables("rpt_housing_demographics")
                            If Not .Columns.Contains("total") Then
                                .Columns.Add("total", GetType(Int32), "[count_cl]+[count_ap]")
                            End If
                        End With
                    End Using
                End Using

                ' Bind the datasource to the items
                vue = New DataView(ds.Tables("rpt_housing_demographics"), String.Empty, "type", DataViewRowState.CurrentRows)
                DataSource = vue
                GroupHeader1.GroupFields.Add(New GroupField("type"))

                With XrLabel_count_cl
                    .DataBindings.Add("Text", vue, "count_cl", "{0:n0}")
                End With

                XrLabel_group_count_cl.DataBindings.Add("Text", vue, "count_cl")
                XrLabel_total_count_cl.DataBindings.Add("Text", vue, "count_cl")

                With XrLabel_description
                    .DataBindings.Add("Text", vue, "description")
                End With

                With XrLabel_count_ap
                    .DataBindings.Add("Text", vue, "count_ap", "{0:n0}")
                End With

                XrLabel_group_count_ap.DataBindings.Add("Text", vue, "count_ap")
                XrLabel_total_count_ap.DataBindings.Add("Text", vue, "count_ap")

                With XrLabel_units
                    .DataBindings.Add("Text", vue, "units", "{0:n0}")
                End With

                XrLabel_group_units.DataBindings.Add("Text", vue, "units")
                XrLabel_total_units.DataBindings.Add("Text", vue, "units")

                With XrLabel_count_total
                    .DataBindings.Add("Text", vue, "total", "{0:n0}")
                End With

                XrLabel_group_count_total.DataBindings.Add("Text", vue, "total")
                XrLabel_total_count_total.DataBindings.Add("Text", vue, "total")

            Catch ex As Exception
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error reading transactions")

            Finally
                If cn IsNot Nothing Then cn.Dispose()

                Cursor.Current = current_cursor
            End Try
        End Sub
    End Class
End Namespace
