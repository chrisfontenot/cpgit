#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}
#End Region
Imports DebtPlus.Reports.Template.Forms

Namespace Client.CloseToPayoff.Detail
    Public Class CloseToPayoffDetailReport
        Inherits DetailSubreport

        Public Sub New()
            MyBase.New()
            AddHandler Me.BeforePrint, AddressOf Report_BeforePrint
            AddHandler Me.GetSubTitleString, AddressOf CloseToPayoffDetailReport_GetSubTitleString

            ReportFilter.IsEnabled = True
        End Sub

        Private privateParameter_Counselor As Object = Nothing
        Public Property Parameter_Counselor() As Object
            Get
                Return privateParameter_Counselor
            End Get
            Set(ByVal Value As Object)
                privateParameter_Counselor = Value
            End Set
        End Property

        Public Overrides Sub SetReportParameter(ByVal Parameter As String, ByVal Value As Object)
            Select Case Parameter
                Case "Counselor"
                    Parameter_Counselor = Value
                Case Else
                    MyBase.SetReportParameter(Parameter, Value)
            End Select
        End Sub

        Public Overrides Function RequestReportParameters() As System.Windows.Forms.DialogResult
            Dim answer As System.Windows.Forms.DialogResult
            With New CounselorParametersForm(True)
                answer = .ShowDialog()
                Parameter_Counselor = .Parameter_Counselor
                .Dispose()
            End With
            Return answer
        End Function

        Private ds As New System.Data.DataSet("ds")
        Private Sub Report_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs)
            Dim tbl As System.Data.DataTable = ds.Tables("rpt_close_to_payoff")
            If tbl Is Nothing Then
                Dim cn As SqlClient.SqlConnection = New System.Data.SqlClient.SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
                Dim Current_Cursor As System.Windows.Forms.Cursor = System.Windows.Forms.Cursor.Current
                Try
                    System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.WaitCursor
                    cn.Open()

                    Using cmd As SqlClient.SqlCommand = New System.Data.SqlClient.SqlCommand()
                        With cmd
                            .Connection = cn
                            .CommandText = "rpt_close_to_payoff"
                            .CommandType = CommandType.StoredProcedure
                            .CommandTimeout = 0
                            If Parameter_Counselor Is Nothing OrElse Parameter_Counselor Is System.DBNull.Value Then
                                .Parameters.Add("@counselor", SqlDbType.Int).Value = System.DBNull.Value
                            Else
                                .Parameters.Add("@counselor", SqlDbType.Int).Value = Parameter_Counselor
                            End If
                            .CommandTimeout = 0
                        End With

                        Using da As New SqlClient.SqlDataAdapter(cmd)
                            da.Fill(ds, "rpt_close_to_payoff")
                            tbl = ds.Tables("rpt_close_to_payoff")
                        End Using
                    End Using

                Catch ex As SqlClient.SqlException
                    DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Database Error")

                Finally
                    If cn IsNot Nothing Then cn.Dispose()
                    System.Windows.Forms.Cursor.Current = Current_Cursor
                End Try
            End If

            If tbl IsNot Nothing Then
                DataSource = New System.Data.DataView(tbl, String.Empty, "months_to_payoff, client", DataViewRowState.CurrentRows)
            End If
        End Sub

        Private Sub CloseToPayoffDetailReport_GetSubTitleString(ByVal Sender As Object, ByVal e As Template.TitleStringEventArgs)
            If Parameter_Counselor Is Nothing OrElse Parameter_Counselor Is System.DBNull.Value Then
                e.Title = "This report covers all counselors"
            Else
                e.Title = String.Format("This report covers counselor #{0:f0}", Parameter_Counselor)
            End If
        End Sub
    End Class
End Namespace
