﻿Namespace Appointments.Schedule
    Partial Class AppointmentScheduleReport
        Inherits DebtPlus.Reports.Template.DatedTemplateXtraReportClass

        'Component overrides dispose to clean up the component list.
        Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
            If disposing Then
                If components IsNot Nothing Then
                    components.Dispose()
                End If
            End If
            MyBase.Dispose(disposing)
        End Sub

        'Required by the Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Designer
        'It can be modified using the Designer.
        'Do not modify it using the code editor.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
            Dim XrSummary1 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary()
            Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(AppointmentScheduleReport))
            Me.GroupHeader_appt_time = New DevExpress.XtraReports.UI.GroupHeaderBand()
            Me.XrPanel1 = New DevExpress.XtraReports.UI.XRPanel()
            Me.XrLabel5 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel4 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel3 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel2 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel1 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_office_name = New DevExpress.XtraReports.UI.XRLabel()
            Me.GroupFooter_appt_time = New DevExpress.XtraReports.UI.GroupFooterBand()
            Me.XrLabel6 = New DevExpress.XtraReports.UI.XRLabel()
            Me.ParameterOffice = New DevExpress.XtraReports.Parameters.Parameter()
            Me.XrLabel_inactive_reason = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_start_time_time = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_appt_name = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_counselor_name = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_start_time_date = New DevExpress.XtraReports.UI.XRLabel()
            Me.GroupHeader_DateValue = New DevExpress.XtraReports.UI.GroupHeaderBand()
            Me.GroupFooter_DateValue = New DevExpress.XtraReports.UI.GroupFooterBand()
            Me.XrLabel7 = New DevExpress.XtraReports.UI.XRLabel()
            Me.GroupHeader_office_name = New DevExpress.XtraReports.UI.GroupHeaderBand()
            Me.DateValue = New DevExpress.XtraReports.UI.CalculatedField()
            Me.ActiveItems = New DevExpress.XtraReports.UI.CalculatedField()
            CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
            '
            'PageHeader
            '
            Me.PageHeader.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrPanel1, Me.XrLabel_office_name})
            Me.PageHeader.HeightF = 152.0!
            Me.PageHeader.Controls.SetChildIndex(Me.XrLabel_office_name, 0)
            Me.PageHeader.Controls.SetChildIndex(Me.XrPanel1, 0)
            Me.PageHeader.Controls.SetChildIndex(Me.XrPanel_AgencyAddress, 0)
            Me.PageHeader.Controls.SetChildIndex(Me.XrLabel_Title, 0)
            Me.PageHeader.Controls.SetChildIndex(Me.XrLine_Title, 0)
            Me.PageHeader.Controls.SetChildIndex(Me.XrLabel_Subtitle, 0)
            Me.PageHeader.Controls.SetChildIndex(Me.XrPageInfo1, 0)
            '
            'XrLabel_Title
            '
            Me.XrLabel_Title.StylePriority.UseFont = False
            '
            'XrPageInfo_PageNumber
            '
            Me.XrPageInfo_PageNumber.StylePriority.UseTextAlignment = False
            '
            'XRLabel_Agency_Name
            '
            Me.XRLabel_Agency_Name.StylePriority.UseTextAlignment = False
            '
            'XrLabel_Agency_Address3
            '
            Me.XrLabel_Agency_Address3.StylePriority.UseFont = False
            Me.XrLabel_Agency_Address3.StylePriority.UseTextAlignment = False
            '
            'XrLabel_Agency_Address1
            '
            Me.XrLabel_Agency_Address1.StylePriority.UseFont = False
            Me.XrLabel_Agency_Address1.StylePriority.UseTextAlignment = False
            '
            'XrLabel_Agency_Phone
            '
            Me.XrLabel_Agency_Phone.StylePriority.UseFont = False
            Me.XrLabel_Agency_Phone.StylePriority.UseTextAlignment = False
            '
            'XrLabel_Agency_Address2
            '
            Me.XrLabel_Agency_Address2.StylePriority.UseFont = False
            Me.XrLabel_Agency_Address2.StylePriority.UseTextAlignment = False
            '
            'Detail
            '
            Me.Detail.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel_counselor_name, Me.XrLabel_start_time_date, Me.XrLabel_appt_name, Me.XrLabel_inactive_reason, Me.XrLabel_start_time_time})
            Me.Detail.HeightF = 15.0!
            '
            'GroupHeader_appt_time
            '
            Me.GroupHeader_appt_time.Expanded = False
            Me.GroupHeader_appt_time.GroupFields.AddRange(New DevExpress.XtraReports.UI.GroupField() {New DevExpress.XtraReports.UI.GroupField("appt_time", DevExpress.XtraReports.UI.XRColumnSortOrder.Ascending), New DevExpress.XtraReports.UI.GroupField("appt_time", DevExpress.XtraReports.UI.XRColumnSortOrder.Ascending)})
            Me.GroupHeader_appt_time.GroupUnion = DevExpress.XtraReports.UI.GroupUnion.WithFirstDetail
            Me.GroupHeader_appt_time.HeightF = 0.0!
            Me.GroupHeader_appt_time.Name = "GroupHeader_appt_time"
            Me.GroupHeader_appt_time.StyleName = "XrControlStyle_GroupHeader"
            '
            'XrPanel1
            '
            Me.XrPanel1.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel5, Me.XrLabel4, Me.XrLabel3, Me.XrLabel2, Me.XrLabel1})
            Me.XrPanel1.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 125.0!)
            Me.XrPanel1.Name = "XrPanel1"
            Me.XrPanel1.SizeF = New System.Drawing.SizeF(750.0!, 17.0!)
            Me.XrPanel1.StyleName = "XrControlStyle_HeaderPannel"
            '
            'XrLabel5
            '
            Me.XrLabel5.LocationFloat = New DevExpress.Utils.PointFloat(516.0!, 0.0!)
            Me.XrLabel5.Name = "XrLabel5"
            Me.XrLabel5.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel5.SizeF = New System.Drawing.SizeF(226.0!, 14.99999!)
            Me.XrLabel5.StylePriority.UseTextAlignment = False
            Me.XrLabel5.Text = "REASON FOR UN-AVAILABILITY"
            Me.XrLabel5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
            '
            'XrLabel4
            '
            Me.XrLabel4.LocationFloat = New DevExpress.Utils.PointFloat(213.9167!, 2.000014!)
            Me.XrLabel4.Name = "XrLabel4"
            Me.XrLabel4.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel4.SizeF = New System.Drawing.SizeF(125.0!, 15.0!)
            Me.XrLabel4.StylePriority.UseTextAlignment = False
            Me.XrLabel4.Text = "TYPE"
            Me.XrLabel4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
            '
            'XrLabel3
            '
            Me.XrLabel3.LocationFloat = New DevExpress.Utils.PointFloat(135.9583!, 0.9999911!)
            Me.XrLabel3.Name = "XrLabel3"
            Me.XrLabel3.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel3.SizeF = New System.Drawing.SizeF(66.0!, 15.0!)
            Me.XrLabel3.StylePriority.UseTextAlignment = False
            Me.XrLabel3.Text = "TIME"
            Me.XrLabel3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
            '
            'XrLabel2
            '
            Me.XrLabel2.LocationFloat = New DevExpress.Utils.PointFloat(350.875!, 0.0!)
            Me.XrLabel2.Name = "XrLabel2"
            Me.XrLabel2.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel2.SizeF = New System.Drawing.SizeF(93.70834!, 14.99999!)
            Me.XrLabel2.StylePriority.UseTextAlignment = False
            Me.XrLabel2.Text = "COUNSELOR"
            Me.XrLabel2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
            '
            'XrLabel1
            '
            Me.XrLabel1.LocationFloat = New DevExpress.Utils.PointFloat(7.999992!, 0.9999911!)
            Me.XrLabel1.Name = "XrLabel1"
            Me.XrLabel1.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel1.SizeF = New System.Drawing.SizeF(53.5!, 15.00001!)
            Me.XrLabel1.StylePriority.UseTextAlignment = False
            Me.XrLabel1.Text = "DATE"
            Me.XrLabel1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
            '
            'XrLabel_office_name
            '
            Me.XrLabel_office_name.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "office_name")})
            Me.XrLabel_office_name.LocationFloat = New DevExpress.Utils.PointFloat(7.999992!, 102.0!)
            Me.XrLabel_office_name.Name = "XrLabel_office_name"
            Me.XrLabel_office_name.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_office_name.SizeF = New System.Drawing.SizeF(725.0!, 17.0!)
            Me.XrLabel_office_name.StyleName = "XrControlStyle_Header"
            '
            'GroupFooter_appt_time
            '
            Me.GroupFooter_appt_time.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel6})
            Me.GroupFooter_appt_time.GroupUnion = DevExpress.XtraReports.UI.GroupFooterUnion.WithLastDetail
            Me.GroupFooter_appt_time.HeightF = 15.0!
            Me.GroupFooter_appt_time.Name = "GroupFooter_appt_time"
            '
            'XrLabel6
            '
            Me.XrLabel6.LocationFloat = New DevExpress.Utils.PointFloat(7.999992!, 0.0!)
            Me.XrLabel6.Name = "XrLabel6"
            Me.XrLabel6.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel6.SizeF = New System.Drawing.SizeF(344.7917!, 15.0!)
            '
            'ParameterOffice
            '
            Me.ParameterOffice.Description = "Office ID"
            Me.ParameterOffice.Name = "ParameterOffice"
            Me.ParameterOffice.Type = GetType(Integer)
            Me.ParameterOffice.Value = 0
            Me.ParameterOffice.Visible = False
            '
            'XrLabel_inactive_reason
            '
            Me.XrLabel_inactive_reason.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "inactive_reason")})
            Me.XrLabel_inactive_reason.LocationFloat = New DevExpress.Utils.PointFloat(516.0!, 0.0!)
            Me.XrLabel_inactive_reason.Name = "XrLabel_inactive_reason"
            Me.XrLabel_inactive_reason.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_inactive_reason.SizeF = New System.Drawing.SizeF(226.0!, 14.99999!)
            Me.XrLabel_inactive_reason.StylePriority.UseTextAlignment = False
            Me.XrLabel_inactive_reason.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
            '
            'XrLabel_start_time_time
            '
            Me.XrLabel_start_time_time.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "start_time", "{0:t}")})
            Me.XrLabel_start_time_time.LocationFloat = New DevExpress.Utils.PointFloat(135.75!, 0.0!)
            Me.XrLabel_start_time_time.Name = "XrLabel_start_time_time"
            Me.XrLabel_start_time_time.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_start_time_time.ProcessDuplicates = DevExpress.XtraReports.UI.ValueSuppressType.Suppress
            Me.XrLabel_start_time_time.SizeF = New System.Drawing.SizeF(66.0!, 15.0!)
            Me.XrLabel_start_time_time.StylePriority.UseTextAlignment = False
            Me.XrLabel_start_time_time.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
            '
            'XrLabel_appt_name
            '
            Me.XrLabel_appt_name.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "appt_name")})
            Me.XrLabel_appt_name.LocationFloat = New DevExpress.Utils.PointFloat(213.5!, 0.0!)
            Me.XrLabel_appt_name.Name = "XrLabel_appt_name"
            Me.XrLabel_appt_name.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_appt_name.ProcessDuplicates = DevExpress.XtraReports.UI.ValueSuppressType.Suppress
            Me.XrLabel_appt_name.SizeF = New System.Drawing.SizeF(125.0!, 15.0!)
            Me.XrLabel_appt_name.StylePriority.UseTextAlignment = False
            Me.XrLabel_appt_name.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
            '
            'XrLabel_counselor_name
            '
            Me.XrLabel_counselor_name.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "counselor_name")})
            Me.XrLabel_counselor_name.LocationFloat = New DevExpress.Utils.PointFloat(350.25!, 0.0!)
            Me.XrLabel_counselor_name.Name = "XrLabel_counselor_name"
            Me.XrLabel_counselor_name.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_counselor_name.Scripts.OnBeforePrint = "XrLabel_counselor_name_BeforePrint"
            Me.XrLabel_counselor_name.SizeF = New System.Drawing.SizeF(154.0!, 14.99999!)
            Me.XrLabel_counselor_name.StylePriority.UseTextAlignment = False
            Me.XrLabel_counselor_name.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
            '
            'XrLabel_start_time_date
            '
            Me.XrLabel_start_time_date.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "start_time", "{0:ddd MMM dd, yyyy}")})
            Me.XrLabel_start_time_date.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel_start_time_date.ForeColor = System.Drawing.Color.Maroon
            Me.XrLabel_start_time_date.LocationFloat = New DevExpress.Utils.PointFloat(7.999992!, 0.0!)
            Me.XrLabel_start_time_date.Name = "XrLabel_start_time_date"
            Me.XrLabel_start_time_date.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_start_time_date.ProcessDuplicates = DevExpress.XtraReports.UI.ValueSuppressType.Suppress
            Me.XrLabel_start_time_date.ProcessNullValues = DevExpress.XtraReports.UI.ValueSuppressType.Suppress
            Me.XrLabel_start_time_date.SizeF = New System.Drawing.SizeF(116.0!, 15.00001!)
            Me.XrLabel_start_time_date.StylePriority.UseFont = False
            Me.XrLabel_start_time_date.StylePriority.UseTextAlignment = False
            Me.XrLabel_start_time_date.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
            '
            'GroupHeader_DateValue
            '
            Me.GroupHeader_DateValue.Expanded = False
            Me.GroupHeader_DateValue.GroupFields.AddRange(New DevExpress.XtraReports.UI.GroupField() {New DevExpress.XtraReports.UI.GroupField("DateValue", DevExpress.XtraReports.UI.XRColumnSortOrder.Ascending)})
            Me.GroupHeader_DateValue.HeightF = 0.0!
            Me.GroupHeader_DateValue.Level = 1
            Me.GroupHeader_DateValue.Name = "GroupHeader_DateValue"
            '
            'GroupFooter_DateValue
            '
            Me.GroupFooter_DateValue.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel7})
            Me.GroupFooter_DateValue.HeightF = 30.0!
            Me.GroupFooter_DateValue.Level = 1
            Me.GroupFooter_DateValue.Name = "GroupFooter_DateValue"
            '
            'XrLabel7
            '
            Me.XrLabel7.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "ActiveItems")})
            Me.XrLabel7.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel7.ForeColor = System.Drawing.Color.Teal
            Me.XrLabel7.LocationFloat = New DevExpress.Utils.PointFloat(213.5!, 0.0!)
            Me.XrLabel7.Name = "XrLabel7"
            Me.XrLabel7.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel7.SizeF = New System.Drawing.SizeF(344.7917!, 15.0!)
            Me.XrLabel7.StylePriority.UseFont = False
            Me.XrLabel7.StylePriority.UseForeColor = False
            XrSummary1.FormatString = "{0:n0} Available Appointments for this day."
            XrSummary1.Running = DevExpress.XtraReports.UI.SummaryRunning.Group
            Me.XrLabel7.Summary = XrSummary1
            '
            'GroupHeader_office_name
            '
            Me.GroupHeader_office_name.Expanded = False
            Me.GroupHeader_office_name.GroupFields.AddRange(New DevExpress.XtraReports.UI.GroupField() {New DevExpress.XtraReports.UI.GroupField("office_name", DevExpress.XtraReports.UI.XRColumnSortOrder.Ascending)})
            Me.GroupHeader_office_name.GroupUnion = DevExpress.XtraReports.UI.GroupUnion.WithFirstDetail
            Me.GroupHeader_office_name.HeightF = 0.0!
            Me.GroupHeader_office_name.Level = 2
            Me.GroupHeader_office_name.Name = "GroupHeader_office_name"
            Me.GroupHeader_office_name.PageBreak = DevExpress.XtraReports.UI.PageBreak.BeforeBand
            '
            'DateValue
            '
            Me.DateValue.Expression = "GetDate([start_time])"
            Me.DateValue.FieldType = DevExpress.XtraReports.UI.FieldType.DateTime
            Me.DateValue.Name = "DateValue"
            '
            'ActiveItems
            '
            Me.ActiveItems.FieldType = DevExpress.XtraReports.UI.FieldType.Int32
            Me.ActiveItems.Name = "ActiveItems"
            Me.ActiveItems.Scripts.OnGetValue = "ActiveItems_GetValue"
            '
            'AppointmentScheduleReport
            '
            Me.Bands.AddRange(New DevExpress.XtraReports.UI.Band() {Me.Detail, Me.PageHeader, Me.PageFooter, Me.TopMarginBand1, Me.BottomMarginBand1, Me.GroupHeader_appt_time, Me.GroupFooter_appt_time, Me.GroupHeader_DateValue, Me.GroupFooter_DateValue, Me.GroupHeader_office_name})
            Me.CalculatedFields.AddRange(New DevExpress.XtraReports.UI.CalculatedField() {Me.DateValue, Me.ActiveItems})
            Me.Parameters.AddRange(New DevExpress.XtraReports.Parameters.Parameter() {Me.ParameterOffice})
            Me.ScriptLanguage = DevExpress.XtraReports.ScriptLanguage.VisualBasic
            Me.ScriptReferencesString = resources.GetString("$this.ScriptReferencesString")
            Me.Scripts.OnBeforePrint = "AppointmentScheduleReport_BeforePrint"
            Me.ScriptsSource = resources.GetString("$this.ScriptsSource")
            Me.StyleSheet.AddRange(New DevExpress.XtraReports.UI.XRControlStyle() {Me.XrControlStyle_Header, Me.XrControlStyle_Totals, Me.XrControlStyle_HeaderPannel, Me.XrControlStyle_GroupHeader})
            Me.Version = "11.2"
            Me.Controls.SetChildIndex(Me.GroupHeader_office_name, 0)
            Me.Controls.SetChildIndex(Me.GroupFooter_DateValue, 0)
            Me.Controls.SetChildIndex(Me.GroupHeader_DateValue, 0)
            Me.Controls.SetChildIndex(Me.GroupFooter_appt_time, 0)
            Me.Controls.SetChildIndex(Me.GroupHeader_appt_time, 0)
            Me.Controls.SetChildIndex(Me.BottomMarginBand1, 0)
            Me.Controls.SetChildIndex(Me.TopMarginBand1, 0)
            Me.Controls.SetChildIndex(Me.PageFooter, 0)
            Me.Controls.SetChildIndex(Me.PageHeader, 0)
            Me.Controls.SetChildIndex(Me.Detail, 0)
            CType(Me, System.ComponentModel.ISupportInitialize).EndInit()

        End Sub

        Friend WithEvents XrPanel1 As DevExpress.XtraReports.UI.XRPanel
        Friend WithEvents XrLabel4 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel3 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel2 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel1 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents GroupHeader_appt_time As DevExpress.XtraReports.UI.GroupHeaderBand
        Friend WithEvents XrLabel_office_name As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents GroupFooter_appt_time As DevExpress.XtraReports.UI.GroupFooterBand
        Friend WithEvents ParameterOffice As DevExpress.XtraReports.Parameters.Parameter
        Friend WithEvents XrLabel_counselor_name As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_start_time_date As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_appt_name As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_inactive_reason As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_start_time_time As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel5 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents GroupHeader_DateValue As DevExpress.XtraReports.UI.GroupHeaderBand
        Friend WithEvents GroupFooter_DateValue As DevExpress.XtraReports.UI.GroupFooterBand
        Friend WithEvents XrLabel6 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents GroupHeader_office_name As DevExpress.XtraReports.UI.GroupHeaderBand
        Friend WithEvents XrLabel7 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents DateValue As DevExpress.XtraReports.UI.CalculatedField
        Friend WithEvents ActiveItems As DevExpress.XtraReports.UI.CalculatedField
    End Class
End Namespace
