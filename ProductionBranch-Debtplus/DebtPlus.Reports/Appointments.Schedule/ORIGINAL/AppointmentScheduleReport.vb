#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}
#End Region

Option Compare Binary
Option Explicit On 
Option Strict On

Namespace Appointments.Schedule
    Public Class AppointmentScheduleReport

        ''' <summary>
        ''' Pass the outer parameters to the sub-report
        ''' </summary>
        Public Sub New()
            MyBase.New()
            InitializeComponent()
            AddHandler BeforePrint, AddressOf AppointmentScheduleReport_BeforePrint
            AddHandler XrLabel_counselor_name.BeforePrint, AddressOf XrLabel_counselor_name_BeforePrint
            AddHandler ActiveItems.GetValue, AddressOf ActiveItems_GetValue

            ReportFilter.IsEnabled = True
        End Sub

        Public Property Parameter_Office() As Object
            Get
                Return Parameters("ParameterOffice").Value
            End Get
            Set(ByVal value As Object)
                Parameters("ParameterOffice").Value = value
            End Set
        End Property

        ''' <summary>
        ''' Provide the linkage to set the parameters
        ''' </summary>
        Public Overrides Sub SetReportParameter(ByVal Parameter As String, ByVal Value As Object)
            Select Case Parameter
                Case "Office"
                    Parameter_Office = Value
                Case Else
                    MyBase.SetReportParameter(Parameter, Value)
            End Select
        End Sub

        ''' <summary>
        ''' Title associated with the report
        ''' </summary>
        Public Overrides ReadOnly Property ReportTitle() As String
            Get
                Return "Review Appointment Schedule"
            End Get
        End Property

        ''' <summary>
        ''' Request the parameters for the report
        ''' </summary>
        Public Overrides Function RequestReportParameters() As System.Windows.Forms.DialogResult
            Dim Answer As System.Windows.Forms.DialogResult = DialogResult.OK

            If NeedParameters() Then
                Using DialogForm As New DebtPlus.Reports.Template.Forms.DatedOfficeParametersForm()
                    With DialogForm
                        Answer = .ShowDialog
                        Parameter_Office = .Parameter_Office
                        Parameter_FromDate = .Parameter_FromDate
                        Parameter_ToDate = .Parameter_ToDate
                    End With
                End Using
            End If

            '-- The answer should be OK if we want to show the report.
            Return Answer
        End Function

        '-- Create a dataset for the report fields
        Private ReadOnly ds As New System.Data.DataSet("ds")

        ''' <summary>
        ''' Determine the value, as in count, for the current row
        ''' </summary>
        Private Sub ActiveItems_GetValue(ByVal sender As Object, ByVal e As DevExpress.XtraReports.UI.GetValueEventArgs)
            Dim Value As Int32 = 1
            Dim row As System.Data.DataRowView = TryCast(e.Row, System.Data.DataRowView)
            If row IsNot Nothing Then
                Dim InactiveStatus As Int32 = DebtPlus.Utils.Nulls.DInt(row("inactive"))
                If InactiveStatus <> 0 Then
                    Value = 0
                End If
            End If
            e.Value = Value
        End Sub

        ''' <summary>
        ''' Bind the dataset to the report
        ''' </summary>
        Private Sub AppointmentScheduleReport_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs)
            Dim rpt As DevExpress.XtraReports.UI.XtraReport = CType(sender, DevExpress.XtraReports.UI.XtraReport)
            Const TableName As String = "rpt_appt_weekly_schedule"

            Dim tbl As System.Data.DataTable = ds.Tables(TableName)
            If tbl Is Nothing Then
                Dim sqlInfo As DebtPlus.LINQ.SQLInfoClass = DebtPlus.LINQ.SQLInfoClass.getDefault()
                Try
                    Using cn As System.Data.SqlClient.SqlConnection = New System.Data.SqlClient.SqlConnection(sqlInfo.ConnectionString)
                        cn.Open()
                        Using cmd As New System.Data.SqlClient.SqlCommand()
                            cmd.Connection = cn
                            cmd.CommandText = "rpt_appt_weekly_schedule"
                            cmd.CommandType = System.Data.CommandType.StoredProcedure
                            System.Data.SqlClient.SqlCommandBuilder.DeriveParameters(cmd)

                            cmd.Parameters(1).Value = rpt.Parameters("ParameterFromDate").Value
                            cmd.Parameters(2).Value = rpt.Parameters("ParameterToDate").Value
                            Dim IntValue As Int32 = DebtPlus.Utils.Nulls.DInt(rpt.Parameters("ParameterOffice").Value)
                            If IntValue > 0 Then
                                cmd.Parameters(3).Value = IntValue
                            End If

                            cmd.CommandTimeout = 0

                            Using da As New System.Data.SqlClient.SqlDataAdapter(cmd)
                                da.Fill(ds, TableName)
                                tbl = ds.Tables(TableName)
                            End Using
                        End Using
                    End Using

                Catch ex As System.Data.SqlClient.SqlException
                    Using gdr As New DebtPlus.Repository.GetDataResult()
                        gdr.HandleException(ex)
                        DebtPlus.UI.Common.ErrorHandling.HandleErrors(gdr, "Error reading report information")
                    End Using
                End Try
            End If

            If tbl IsNot Nothing Then
                rpt.DataSource = New System.Data.DataView(tbl, CType(rpt, DebtPlus.Interfaces.Reports.IReportFilter).ReportFilter.ViewFilter, "office, start_time, counselor_name", System.Data.DataViewRowState.CurrentRows)
                For Each calc As DevExpress.XtraReports.UI.CalculatedField In rpt.CalculatedFields
                    calc.Assign(rpt.DataSource, rpt.DataMember)
                Next
            End If
        End Sub

        Private Sub XrLabel_counselor_name_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs)
            Dim lbl As DevExpress.XtraReports.UI.XRLabel = CType(sender, DevExpress.XtraReports.UI.XRLabel)
            With lbl
                Dim rpt As DevExpress.XtraReports.UI.XtraReport = CType(.Report, DevExpress.XtraReports.UI.XtraReport)
                Dim InactiveStatus As Int32 = DebtPlus.Utils.Nulls.DInt(rpt.GetCurrentColumnValue("inactive"))
                If InactiveStatus <> 0 Then
                    .Font = New System.Drawing.Font(.Font, FontStyle.Strikeout)
                Else
                    .Font = New System.Drawing.Font(.Font, FontStyle.Regular)
                End If
            End With
        End Sub
    End Class
End Namespace
