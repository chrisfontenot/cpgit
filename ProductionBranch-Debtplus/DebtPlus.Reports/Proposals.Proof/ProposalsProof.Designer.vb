Namespace Proposals.Proof
    <Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
    Partial Class ProposalsProofReport

        'XtraReport overrides dispose to clean up the component list.
        <System.Diagnostics.DebuggerNonUserCode()> _
        Protected Overrides Sub Dispose(ByVal disposing As Boolean)
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
            MyBase.Dispose(disposing)
        End Sub

        'Required by the Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Designer
        'It can be modified using the Designer.  
        'Do not modify it using the code editor.
        <System.Diagnostics.DebuggerStepThrough()> _
        Private Sub InitializeComponent()
            Me.XrControlStyle1 = New DevExpress.XtraReports.UI.XRControlStyle
            Me.XrLabel_ClientNameAddress = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_Creditor_L = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_ID = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel1 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_DebtID = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel3 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_proposal_message = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_date_created = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_creditor_name = New DevExpress.XtraReports.UI.XRLabel
            Me.XrSubreport_FBD = New DevExpress.XtraReports.UI.XRSubreport
            Me.XtraReport_rpt_proposal_proof_fbd1 = New DebtPlus.Reports.Proposals.Proof.SubReports.XtraReport_rpt_proposal_proof_fbd
            Me.XrSubreport_FBC = New DevExpress.XtraReports.UI.XRSubreport
            Me.XtraReport_rpt_proposal_proof_fbc1 = New DebtPlus.Reports.Proposals.Proof.SubReports.XtraReport_rpt_proposal_proof_fbc
            Me.XrSubreport_CDP = New DevExpress.XtraReports.UI.XRSubreport
            Me.XtraReport_rpt_proposal_proof_cdp1 = New DebtPlus.Reports.Proposals.Proof.SubReports.XtraReport_rpt_proposal_proof_cdp
            Me.XrSubreport_rpt_Proposal_AccountNumber = New DevExpress.XtraReports.UI.XRSubreport
            Me.XtraReport_rpt_Proposal_AccountNumber1 = New DebtPlus.Reports.Proposals.Proof.SubReports.XtraReport_rpt_Proposal_AccountNumber
            CType(Me.XtraReport_rpt_proposal_proof_fbd1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.XtraReport_rpt_proposal_proof_fbc1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.XtraReport_rpt_proposal_proof_cdp1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.XtraReport_rpt_Proposal_AccountNumber1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
            '
            'Detail
            '
            Me.Detail.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel_ID, Me.XrSubreport_FBD, Me.XrSubreport_FBC, Me.XrSubreport_CDP, Me.XrSubreport_rpt_Proposal_AccountNumber, Me.XrLabel_ClientNameAddress, Me.XrLabel_Creditor_L, Me.XrLabel3, Me.XrLabel_proposal_message})
            Me.Detail.HeightF = 273.0!
            Me.Detail.PageBreak = DevExpress.XtraReports.UI.PageBreak.AfterBand
            Me.Detail.StylePriority.UseTextAlignment = False
            Me.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            '
            'PageHeader
            '
            Me.PageHeader.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel_DebtID, Me.XrLabel1})
            Me.PageHeader.HeightF = 123.0!
            Me.PageHeader.Controls.SetChildIndex(Me.XrLabel1, 0)
            Me.PageHeader.Controls.SetChildIndex(Me.XrLabel_DebtID, 0)
            Me.PageHeader.Controls.SetChildIndex(Me.XrPanel_AgencyAddress, 0)
            Me.PageHeader.Controls.SetChildIndex(Me.XrLabel_Title, 0)
            Me.PageHeader.Controls.SetChildIndex(Me.XrLine_Title, 0)
            Me.PageHeader.Controls.SetChildIndex(Me.XrLabel_Subtitle, 0)
            Me.PageHeader.Controls.SetChildIndex(Me.XrPageInfo1, 0)
            '
            'PageFooter
            '
            Me.PageFooter.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel_creditor_name, Me.XrLabel_date_created})
            Me.PageFooter.HeightF = 42.0!
            Me.PageFooter.Controls.SetChildIndex(Me.XrLabel_date_created, 0)
            Me.PageFooter.Controls.SetChildIndex(Me.XrLabel_creditor_name, 0)
            Me.PageFooter.Controls.SetChildIndex(Me.XrPageInfo_PageNumber, 0)
            '
            'XrLabel_Title
            '
            Me.XrLabel_Title.StylePriority.UseFont = False
            '
            'XrPageInfo_PageNumber
            '
            Me.XrPageInfo_PageNumber.LocationFloat = New DevExpress.Utils.PointFloat(675.0!, 8.0!)
            Me.XrPageInfo_PageNumber.SizeF = New System.Drawing.SizeF(67.0!, 17.0!)
            Me.XrPageInfo_PageNumber.StylePriority.UseTextAlignment = False
            '
            'XRLabel_Agency_Name
            '
            Me.XRLabel_Agency_Name.StylePriority.UseTextAlignment = False
            '
            'XrLabel_Agency_Address3
            '
            Me.XrLabel_Agency_Address3.StylePriority.UseFont = False
            Me.XrLabel_Agency_Address3.StylePriority.UseTextAlignment = False
            '
            'XrLabel_Agency_Address1
            '
            Me.XrLabel_Agency_Address1.StylePriority.UseFont = False
            Me.XrLabel_Agency_Address1.StylePriority.UseTextAlignment = False
            '
            'XrLabel_Agency_Phone
            '
            Me.XrLabel_Agency_Phone.StylePriority.UseFont = False
            Me.XrLabel_Agency_Phone.StylePriority.UseTextAlignment = False
            '
            'XrLabel_Agency_Address2
            '
            Me.XrLabel_Agency_Address2.StylePriority.UseFont = False
            Me.XrLabel_Agency_Address2.StylePriority.UseTextAlignment = False
            '
            'XrControlStyle1
            '
            Me.XrControlStyle1.BackColor = System.Drawing.Color.Teal
            Me.XrControlStyle1.BorderColor = System.Drawing.Color.Teal
            Me.XrControlStyle1.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.XrControlStyle1.ForeColor = System.Drawing.Color.White
            Me.XrControlStyle1.Name = "XrControlStyle1"
            Me.XrControlStyle1.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
            Me.XrControlStyle1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            '
            'XrLabel_ClientNameAddress
            '
            Me.XrLabel_ClientNameAddress.LocationFloat = New DevExpress.Utils.PointFloat(442.0!, 26.74999!)
            Me.XrLabel_ClientNameAddress.Multiline = True
            Me.XrLabel_ClientNameAddress.Name = "XrLabel_ClientNameAddress"
            Me.XrLabel_ClientNameAddress.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_ClientNameAddress.SizeF = New System.Drawing.SizeF(308.0!, 100.0!)
            '
            'XrLabel_Creditor_L
            '
            Me.XrLabel_Creditor_L.LocationFloat = New DevExpress.Utils.PointFloat(9.999998!, 26.74999!)
            Me.XrLabel_Creditor_L.Multiline = True
            Me.XrLabel_Creditor_L.Name = "XrLabel_Creditor_L"
            Me.XrLabel_Creditor_L.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_Creditor_L.SizeF = New System.Drawing.SizeF(308.0!, 100.0!)
            '
            'XrLabel_ID
            '
            Me.XrLabel_ID.BorderColor = System.Drawing.Color.Transparent
            Me.XrLabel_ID.CanGrow = False
            Me.XrLabel_ID.ForeColor = System.Drawing.Color.Transparent
            Me.XrLabel_ID.LocationFloat = New DevExpress.Utils.PointFloat(650.0!, 139.2917!)
            Me.XrLabel_ID.Name = "XrLabel_ID"
            Me.XrLabel_ID.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_ID.SizeF = New System.Drawing.SizeF(100.0!, 25.0!)
            Me.XrLabel_ID.StylePriority.UseBorderColor = False
            Me.XrLabel_ID.StylePriority.UseForeColor = False
            '
            'XrLabel1
            '
            Me.XrLabel1.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel1.ForeColor = System.Drawing.Color.Maroon
            Me.XrLabel1.LocationFloat = New DevExpress.Utils.PointFloat(17.0!, 108.0!)
            Me.XrLabel1.Name = "XrLabel1"
            Me.XrLabel1.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel1.SizeF = New System.Drawing.SizeF(75.0!, 15.0!)
            Me.XrLabel1.StylePriority.UseFont = False
            Me.XrLabel1.StylePriority.UseForeColor = False
            Me.XrLabel1.Text = "Debt ID:"
            '
            'XrLabel_DebtID
            '
            Me.XrLabel_DebtID.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel_DebtID.ForeColor = System.Drawing.Color.Maroon
            Me.XrLabel_DebtID.LocationFloat = New DevExpress.Utils.PointFloat(100.0!, 108.0!)
            Me.XrLabel_DebtID.Name = "XrLabel_DebtID"
            Me.XrLabel_DebtID.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_DebtID.SizeF = New System.Drawing.SizeF(308.0!, 15.0!)
            Me.XrLabel_DebtID.StylePriority.UseFont = False
            Me.XrLabel_DebtID.StylePriority.UseForeColor = False
            '
            'XrLabel3
            '
            Me.XrLabel3.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel3.ForeColor = System.Drawing.Color.Maroon
            Me.XrLabel3.LocationFloat = New DevExpress.Utils.PointFloat(17.0!, 0.0!)
            Me.XrLabel3.Name = "XrLabel3"
            Me.XrLabel3.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel3.SizeF = New System.Drawing.SizeF(75.0!, 15.0!)
            Me.XrLabel3.StylePriority.UseFont = False
            Me.XrLabel3.StylePriority.UseForeColor = False
            Me.XrLabel3.Text = "Message:"
            '
            'XrLabel_proposal_message
            '
            Me.XrLabel_proposal_message.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel_proposal_message.ForeColor = System.Drawing.Color.Maroon
            Me.XrLabel_proposal_message.LocationFloat = New DevExpress.Utils.PointFloat(100.0!, 0.0!)
            Me.XrLabel_proposal_message.Name = "XrLabel_proposal_message"
            Me.XrLabel_proposal_message.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_proposal_message.SizeF = New System.Drawing.SizeF(650.0!, 15.0!)
            Me.XrLabel_proposal_message.StylePriority.UseFont = False
            Me.XrLabel_proposal_message.StylePriority.UseForeColor = False
            '
            'XrLabel_date_created
            '
            Me.XrLabel_date_created.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel_date_created.ForeColor = System.Drawing.Color.Teal
            Me.XrLabel_date_created.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 8.0!)
            Me.XrLabel_date_created.Name = "XrLabel_date_created"
            Me.XrLabel_date_created.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_date_created.SizeF = New System.Drawing.SizeF(100.0!, 17.0!)
            Me.XrLabel_date_created.StylePriority.UseFont = False
            Me.XrLabel_date_created.StylePriority.UseForeColor = False
            '
            'XrLabel_creditor_name
            '
            Me.XrLabel_creditor_name.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel_creditor_name.ForeColor = System.Drawing.Color.Teal
            Me.XrLabel_creditor_name.LocationFloat = New DevExpress.Utils.PointFloat(100.0!, 8.0!)
            Me.XrLabel_creditor_name.Name = "XrLabel_creditor_name"
            Me.XrLabel_creditor_name.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_creditor_name.SizeF = New System.Drawing.SizeF(575.0!, 17.0!)
            Me.XrLabel_creditor_name.StylePriority.UseFont = False
            Me.XrLabel_creditor_name.StylePriority.UseForeColor = False
            Me.XrLabel_creditor_name.StylePriority.UseTextAlignment = False
            Me.XrLabel_creditor_name.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter
            '
            'XrSubreport_FBD
            '
            Me.XrSubreport_FBD.CanShrink = True
            Me.XrSubreport_FBD.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 241.0!)
            Me.XrSubreport_FBD.Name = "XrSubreport_FBD"
            Me.XrSubreport_FBD.ReportSource = Me.XtraReport_rpt_proposal_proof_fbd1
            Me.XrSubreport_FBD.SizeF = New System.Drawing.SizeF(750.0!, 25.0!)
            '
            'XrSubreport_FBC
            '
            Me.XrSubreport_FBC.CanShrink = True
            Me.XrSubreport_FBC.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 208.0!)
            Me.XrSubreport_FBC.Name = "XrSubreport_FBC"
            Me.XrSubreport_FBC.ReportSource = Me.XtraReport_rpt_proposal_proof_fbc1
            Me.XrSubreport_FBC.SizeF = New System.Drawing.SizeF(750.0!, 25.0!)
            '
            'XrSubreport_CDP
            '
            Me.XrSubreport_CDP.CanShrink = True
            Me.XrSubreport_CDP.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 175.0!)
            Me.XrSubreport_CDP.Name = "XrSubreport_CDP"
            Me.XrSubreport_CDP.ReportSource = Me.XtraReport_rpt_proposal_proof_cdp1
            Me.XrSubreport_CDP.SizeF = New System.Drawing.SizeF(750.0!, 25.0!)
            '
            'XrSubreport_rpt_Proposal_AccountNumber
            '
            Me.XrSubreport_rpt_Proposal_AccountNumber.CanShrink = True
            Me.XrSubreport_rpt_Proposal_AccountNumber.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 139.2917!)
            Me.XrSubreport_rpt_Proposal_AccountNumber.Name = "XrSubreport_rpt_Proposal_AccountNumber"
            Me.XrSubreport_rpt_Proposal_AccountNumber.ReportSource = Me.XtraReport_rpt_Proposal_AccountNumber1
            Me.XrSubreport_rpt_Proposal_AccountNumber.SizeF = New System.Drawing.SizeF(425.0!, 25.0!)
            '
            Me.XrLabel_ID.DataBindings.Add("Text", Nothing, "client")
            Me.XrLabel_creditor_name.DataBindings.Add("Text", Nothing, "creditor_name")
            Me.XrLabel_date_created.DataBindings.Add("Text", Nothing, "date_created", "{0:d}")
            Me.XrLabel_proposal_message.DataBindings.Add("Text", Nothing, "proposal_message")
            '
            'ProposalsProofReport
            '
            Me.Bands.AddRange(New DevExpress.XtraReports.UI.Band() {Me.Detail, Me.PageHeader, Me.PageFooter})
            Me.StyleSheet.AddRange(New DevExpress.XtraReports.UI.XRControlStyle() {Me.XrControlStyle1})
            Me.Version = "9.3"
            CType(Me.XtraReport_rpt_proposal_proof_fbd1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.XtraReport_rpt_proposal_proof_fbc1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.XtraReport_rpt_proposal_proof_cdp1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.XtraReport_rpt_Proposal_AccountNumber1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me, System.ComponentModel.ISupportInitialize).EndInit()
        End Sub

        Friend WithEvents XrControlStyle1 As DevExpress.XtraReports.UI.XRControlStyle
        Friend WithEvents XrLabel_Creditor_L As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_ClientNameAddress As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrSubreport_FBD As DevExpress.XtraReports.UI.XRSubreport
        Friend WithEvents XrSubreport_FBC As DevExpress.XtraReports.UI.XRSubreport
        Friend WithEvents XrSubreport_CDP As DevExpress.XtraReports.UI.XRSubreport
        Friend WithEvents XrSubreport_rpt_Proposal_AccountNumber As DevExpress.XtraReports.UI.XRSubreport
        Friend WithEvents XrLabel_ID As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_proposal_message As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel3 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_DebtID As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel1 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_date_created As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_creditor_name As DevExpress.XtraReports.UI.XRLabel
        Private WithEvents XtraReport_rpt_Proposal_AccountNumber1 As DebtPlus.Reports.Proposals.Proof.SubReports.XtraReport_rpt_Proposal_AccountNumber
        Private WithEvents XtraReport_rpt_proposal_proof_cdp1 As DebtPlus.Reports.Proposals.Proof.SubReports.XtraReport_rpt_proposal_proof_cdp
        Private WithEvents XtraReport_rpt_proposal_proof_fbc1 As DebtPlus.Reports.Proposals.Proof.SubReports.XtraReport_rpt_proposal_proof_fbc
        Private WithEvents XtraReport_rpt_proposal_proof_fbd1 As DebtPlus.Reports.Proposals.Proof.SubReports.XtraReport_rpt_proposal_proof_fbd
    End Class
End Namespace