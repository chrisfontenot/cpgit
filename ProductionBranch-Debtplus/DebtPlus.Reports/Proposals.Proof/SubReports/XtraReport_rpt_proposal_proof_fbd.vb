#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}
#End Region

Namespace Proposals.Proof.SubReports
    Public Class XtraReport_rpt_proposal_proof_fbd
        Inherits DevExpress.XtraReports.UI.XtraReport

        Public Sub New()
            MyBase.New()
            InitializeComponent()
            AddHandler XrLabel_category.BeforePrint, AddressOf FormatItemName
            AddHandler XrLabel_item.BeforePrint, AddressOf FormatItemValue
            AddHandler XrLabel_GroupName.BeforePrint, AddressOf FormatCategoryName
        End Sub

        Public Sub SetSubreportParameters(ByVal vue As System.Data.DataView)
            DataSource = vue
            With XrLabel_Blank
                .DataBindings.Add("Text", vue, "category")
            End With
        End Sub

        Private Sub FormatItemName(ByVal Sender As Object, ByVal e As System.EventArgs)
            Dim category As Object = Me.GetCurrentColumnValue("category")
            Dim item As Object = Me.GetCurrentColumnValue("item")

            ' Make sure that the values are convertible
            If category Is Nothing OrElse category Is System.DBNull.Value Then category = -1
            If item Is Nothing OrElse item Is System.DBNull.Value Then item = -1

            ' This will be changed below if the code is recognized
            Dim Answer As String = "Unknown " + Convert.ToString(category) + "/" + Convert.ToString(item)

            Select Case Convert.ToInt32(category)
                Case 1
                    Select Case Convert.ToInt32(item)
                        Case 1
                            Answer = "Monthly rent and renters insurance payment"
                        Case 2
                            Answer = "Monthly mortgage and HO insurance payment"
                        Case 3
                            Answer = "Monthly automobile payment"
                        Case 4
                            Answer = "Monthly utilities"
                        Case 5
                            Answer = "Monthly auto and insurance expense"
                        Case 6
                            Answer = "Monthly grocery expense"
                        Case 7
                            Answer = "Monthly medical and life insurance"
                        Case 8
                            Answer = "Monthly medical expense"
                        Case 9
                            Answer = "Monthly alimony & child support"
                        Case 10
                            Answer = "Monthly child daycare"
                        Case 11
                            Answer = "Monthly miscellaneous expense"
                        Case 12
                            Answer = "Student Loans"
                    End Select

                Case 2
                    Select Case Convert.ToInt32(item)
                        Case 1
                            Answer = "Revolving credit cards: current balances"
                        Case 2
                            Answer = "Revolving credit cards: minimum payment"
                        Case 3
                            Answer = "Any other secured or installment loans"
                    End Select

                Case 3
                    Select Case Convert.ToInt32(item)
                        Case 1
                            Answer = "Applicant's bring home pay"
                        Case 2
                            Answer = "Spouse's bring home pay"
                        Case 3
                            Answer = "Military retirement/retirement or pension"
                        Case 4
                            Answer = "Alimony & Child support payments (Income)"
                        Case 5
                            Answer = "Social security payments (Income)"
                        Case 6
                            Answer = "Food stamps or other"
                        Case 7
                            Answer = "Total assets"
                        Case 8
                            Answer = "Total liabilities"
                        Case 9
                            Answer = "Monthly income"
                    End Select

                Case 4
                    Select Case Convert.ToInt32(item)
                        Case 1
                            Answer = "Household expense to income variance"
                        Case 2
                            Answer = "Total expense to income variance"
                        Case 3
                            Answer = "Total debt to income percentage"
                        Case 4
                            Answer = "Total number of people for this budget"
                    End Select

                Case 5
                    Select Case Convert.ToInt32(item)
                        Case 1
                            Answer = "Poor money management"
                        Case 2
                            Answer = "reduced income / unemployment"
                        Case 3
                            Answer = "Personal domestic conflict, divorce or separation"
                        Case 4
                            Answer = "Medical expense, accident or disability"
                        Case 5
                            Answer = "Death of family member"
                        Case 6
                            Answer = "Confidential matter (e.g., gambling, substance abuse, psychiatric, etc.)"
                        Case 7
                            Answer = "Not classified / other"
                        Case 8
                            Answer = "Unexpected/Catastrophic Expenses"
                        Case 9
                            Answer = "Housing Issues"
                        Case 10
                            Answer = "Student Loans"
                        Case 11
                            Answer = "Military Service"
                    End Select
            End Select

            With CType(Sender, DevExpress.XtraReports.UI.XRLabel)
                .Text = Answer
            End With
        End Sub

        Private Sub FormatItemValue(ByVal Sender As Object, ByVal e As System.EventArgs)
            Dim category As Object = Me.GetCurrentColumnValue("category")
            If category Is Nothing OrElse category Is System.DBNull.Value Then category = -1

            Dim item As Object = Me.GetCurrentColumnValue("item")
            If item Is Nothing OrElse item Is System.DBNull.Value Then item = -1

            Dim value As Object = Me.GetCurrentColumnValue("value")
            If value Is Nothing OrElse value Is System.DBNull.Value Then value = 0

            ' Assume that the value is a monetary figure
            Dim Answer As String = String.Format("{0:c}", Convert.ToDecimal(value))

            ' Cause for financial problem has no value
            If Convert.ToInt32(category) = 5 Then
                Answer = String.Empty

                ' total debt/expenses to total income percentage
            ElseIf Convert.ToInt32(category) = 4 AndAlso (Convert.ToInt32(item) = 3 OrElse Convert.ToInt32(item) = 2) Then
                Answer = String.Format("{0:f2}", Convert.ToDouble(value))

                ' Number of dependents
            ElseIf Convert.ToInt32(category) = 4 AndAlso Convert.ToInt32(item) = 4 Then
                Answer = Convert.ToInt32(value).ToString()
            End If

            With CType(Sender, DevExpress.XtraReports.UI.XRLabel)
                .Text = Answer
            End With
        End Sub

        Private Sub FormatCategoryName(ByVal Sender As Object, ByVal e As System.EventArgs)
            Dim category As Object = Me.GetCurrentColumnValue("category")
            If category Is Nothing OrElse category Is System.DBNull.Value Then category = -1

            Dim formula As String
            Select Case Convert.ToInt32(category)
                Case 1
                    formula = "Household Expenses"
                Case 2
                    formula = "Total revolving debt or loans"
                Case 3
                    formula = "Income Source"
                Case 4
                    formula = "Miscellaneous"
                Case 5
                    formula = "Reason for Hardship"
                Case Else
                    formula = "Unknown " + category.ToString()
            End Select

            With CType(Sender, DevExpress.XtraReports.UI.XRLabel)
                .Text = formula
            End With
        End Sub
    End Class
End Namespace