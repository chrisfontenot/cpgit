Namespace Proposals.Proof.SubReports
    <Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
    Partial Class XtraReport_rpt_proposal_proof_cdp

        'XtraReport overrides dispose to clean up the component list.
        <System.Diagnostics.DebuggerNonUserCode()> _
        Protected Overrides Sub Dispose(ByVal disposing As Boolean)
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
            MyBase.Dispose(disposing)
        End Sub

        'Required by the Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Designer
        'It can be modified using the Designer.  
        'Do not modify it using the code editor.
        <System.Diagnostics.DebuggerStepThrough()> _
        Private Sub InitializeComponent()
            Me.Detail = New DevExpress.XtraReports.UI.DetailBand
            Me.XrLabel_work_ph = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_creditor_count = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_proposed_payment = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel22 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_home_ph = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_total_debt = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel19 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_proposed_start = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel17 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel16 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_total_debt_payments = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel14 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_account_balance = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel12 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_account_number = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_total_income = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel9 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_living_expenses = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel7 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel6 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_first_counsel = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel4 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel3 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel2 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel1 = New DevExpress.XtraReports.UI.XRLabel
            Me.ReportFooter = New DevExpress.XtraReports.UI.ReportFooterBand
            Me.XrLabel_blank_space = New DevExpress.XtraReports.UI.XRLabel
            Me.TopMarginBand1 = New DevExpress.XtraReports.UI.TopMarginBand
            Me.BottomMarginBand1 = New DevExpress.XtraReports.UI.BottomMarginBand
            CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
            '
            'Detail
            '
            Me.Detail.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel_work_ph, Me.XrLabel_creditor_count, Me.XrLabel_proposed_payment, Me.XrLabel22, Me.XrLabel_home_ph, Me.XrLabel_total_debt, Me.XrLabel19, Me.XrLabel_proposed_start, Me.XrLabel17, Me.XrLabel16, Me.XrLabel_total_debt_payments, Me.XrLabel14, Me.XrLabel_account_balance, Me.XrLabel12, Me.XrLabel_account_number, Me.XrLabel_total_income, Me.XrLabel9, Me.XrLabel_living_expenses, Me.XrLabel7, Me.XrLabel6, Me.XrLabel_first_counsel, Me.XrLabel4, Me.XrLabel3, Me.XrLabel2, Me.XrLabel1})
            Me.Detail.HeightF = 123.0!
            Me.Detail.Name = "Detail"
            Me.Detail.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
            Me.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
            '
            'XrLabel_work_ph
            '
            Me.XrLabel_work_ph.LocationFloat = New DevExpress.Utils.PointFloat(408.0!, 108.0!)
            Me.XrLabel_work_ph.Name = "XrLabel_work_ph"
            Me.XrLabel_work_ph.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_work_ph.SizeF = New System.Drawing.SizeF(135.0!, 15.0!)
            Me.XrLabel_work_ph.StylePriority.UseTextAlignment = False
            Me.XrLabel_work_ph.Text = " "
            Me.XrLabel_work_ph.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
            '
            'XrLabel_creditor_count
            '
            Me.XrLabel_creditor_count.LocationFloat = New DevExpress.Utils.PointFloat(133.0!, 61.0!)
            Me.XrLabel_creditor_count.Name = "XrLabel_creditor_count"
            Me.XrLabel_creditor_count.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_creditor_count.SizeF = New System.Drawing.SizeF(130.0!, 15.0!)
            Me.XrLabel_creditor_count.StylePriority.UseTextAlignment = False
            Me.XrLabel_creditor_count.Text = " "
            Me.XrLabel_creditor_count.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
            '
            'XrLabel_proposed_payment
            '
            Me.XrLabel_proposed_payment.LocationFloat = New DevExpress.Utils.PointFloat(133.0!, 31.0!)
            Me.XrLabel_proposed_payment.Name = "XrLabel_proposed_payment"
            Me.XrLabel_proposed_payment.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_proposed_payment.SizeF = New System.Drawing.SizeF(130.0!, 15.0!)
            Me.XrLabel_proposed_payment.StylePriority.UseTextAlignment = False
            Me.XrLabel_proposed_payment.Text = " "
            Me.XrLabel_proposed_payment.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
            '
            'XrLabel22
            '
            Me.XrLabel22.LocationFloat = New DevExpress.Utils.PointFloat(275.0!, 48.0!)
            Me.XrLabel22.Name = "XrLabel22"
            Me.XrLabel22.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel22.SizeF = New System.Drawing.SizeF(120.0!, 15.0!)
            Me.XrLabel22.Text = "Total Income"
            '
            'XrLabel_home_ph
            '
            Me.XrLabel_home_ph.LocationFloat = New DevExpress.Utils.PointFloat(408.0!, 93.0!)
            Me.XrLabel_home_ph.Name = "XrLabel_home_ph"
            Me.XrLabel_home_ph.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_home_ph.SizeF = New System.Drawing.SizeF(135.0!, 15.0!)
            Me.XrLabel_home_ph.StylePriority.UseTextAlignment = False
            Me.XrLabel_home_ph.Text = " "
            Me.XrLabel_home_ph.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
            '
            'XrLabel_total_debt
            '
            Me.XrLabel_total_debt.LocationFloat = New DevExpress.Utils.PointFloat(408.0!, 33.0!)
            Me.XrLabel_total_debt.Name = "XrLabel_total_debt"
            Me.XrLabel_total_debt.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_total_debt.SizeF = New System.Drawing.SizeF(135.0!, 15.0!)
            Me.XrLabel_total_debt.StylePriority.UseTextAlignment = False
            Me.XrLabel_total_debt.Text = " "
            Me.XrLabel_total_debt.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
            '
            'XrLabel19
            '
            Me.XrLabel19.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 31.0!)
            Me.XrLabel19.Name = "XrLabel19"
            Me.XrLabel19.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel19.SizeF = New System.Drawing.SizeF(120.0!, 15.0!)
            Me.XrLabel19.Text = "Proposed Payment"
            '
            'XrLabel_proposed_start
            '
            Me.XrLabel_proposed_start.LocationFloat = New DevExpress.Utils.PointFloat(133.0!, 76.0!)
            Me.XrLabel_proposed_start.Name = "XrLabel_proposed_start"
            Me.XrLabel_proposed_start.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_proposed_start.SizeF = New System.Drawing.SizeF(130.0!, 15.0!)
            Me.XrLabel_proposed_start.StylePriority.UseTextAlignment = False
            Me.XrLabel_proposed_start.Text = " "
            Me.XrLabel_proposed_start.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
            '
            'XrLabel17
            '
            Me.XrLabel17.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 91.0!)
            Me.XrLabel17.Name = "XrLabel17"
            Me.XrLabel17.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel17.SizeF = New System.Drawing.SizeF(120.0!, 15.0!)
            Me.XrLabel17.Text = "Date of first Counsel"
            '
            'XrLabel16
            '
            Me.XrLabel16.LocationFloat = New DevExpress.Utils.PointFloat(275.0!, 33.0!)
            Me.XrLabel16.Name = "XrLabel16"
            Me.XrLabel16.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel16.SizeF = New System.Drawing.SizeF(120.0!, 15.0!)
            Me.XrLabel16.Text = "Total Debt"
            '
            'XrLabel_total_debt_payments
            '
            Me.XrLabel_total_debt_payments.LocationFloat = New DevExpress.Utils.PointFloat(408.0!, 63.0!)
            Me.XrLabel_total_debt_payments.Name = "XrLabel_total_debt_payments"
            Me.XrLabel_total_debt_payments.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_total_debt_payments.SizeF = New System.Drawing.SizeF(135.0!, 15.0!)
            Me.XrLabel_total_debt_payments.StylePriority.UseTextAlignment = False
            Me.XrLabel_total_debt_payments.Text = " "
            Me.XrLabel_total_debt_payments.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
            '
            'XrLabel14
            '
            Me.XrLabel14.LocationFloat = New DevExpress.Utils.PointFloat(275.0!, 93.0!)
            Me.XrLabel14.Name = "XrLabel14"
            Me.XrLabel14.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel14.SizeF = New System.Drawing.SizeF(120.0!, 15.0!)
            Me.XrLabel14.Text = "Home Phone"
            '
            'XrLabel_account_balance
            '
            Me.XrLabel_account_balance.LocationFloat = New DevExpress.Utils.PointFloat(133.0!, 46.0!)
            Me.XrLabel_account_balance.Name = "XrLabel_account_balance"
            Me.XrLabel_account_balance.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_account_balance.SizeF = New System.Drawing.SizeF(130.0!, 15.0!)
            Me.XrLabel_account_balance.StylePriority.UseTextAlignment = False
            Me.XrLabel_account_balance.Text = " "
            Me.XrLabel_account_balance.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
            '
            'XrLabel12
            '
            Me.XrLabel12.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 76.0!)
            Me.XrLabel12.Name = "XrLabel12"
            Me.XrLabel12.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel12.SizeF = New System.Drawing.SizeF(120.0!, 15.0!)
            Me.XrLabel12.Text = "Proposed Start Date"
            '
            'XrLabel_account_number
            '
            Me.XrLabel_account_number.LocationFloat = New DevExpress.Utils.PointFloat(133.0!, 106.0!)
            Me.XrLabel_account_number.Name = "XrLabel_account_number"
            Me.XrLabel_account_number.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_account_number.SizeF = New System.Drawing.SizeF(130.0!, 15.0!)
            Me.XrLabel_account_number.StylePriority.UseTextAlignment = False
            Me.XrLabel_account_number.Text = " "
            Me.XrLabel_account_number.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
            '
            'XrLabel_total_income
            '
            Me.XrLabel_total_income.LocationFloat = New DevExpress.Utils.PointFloat(408.0!, 48.0!)
            Me.XrLabel_total_income.Name = "XrLabel_total_income"
            Me.XrLabel_total_income.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_total_income.SizeF = New System.Drawing.SizeF(135.0!, 15.0!)
            Me.XrLabel_total_income.StylePriority.UseTextAlignment = False
            Me.XrLabel_total_income.Text = " "
            Me.XrLabel_total_income.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
            '
            'XrLabel9
            '
            Me.XrLabel9.LocationFloat = New DevExpress.Utils.PointFloat(275.0!, 78.0!)
            Me.XrLabel9.Name = "XrLabel9"
            Me.XrLabel9.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel9.SizeF = New System.Drawing.SizeF(120.0!, 15.0!)
            Me.XrLabel9.Text = "Living Expenses"
            '
            'XrLabel_living_expenses
            '
            Me.XrLabel_living_expenses.LocationFloat = New DevExpress.Utils.PointFloat(408.0!, 78.0!)
            Me.XrLabel_living_expenses.Name = "XrLabel_living_expenses"
            Me.XrLabel_living_expenses.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_living_expenses.SizeF = New System.Drawing.SizeF(135.0!, 15.0!)
            Me.XrLabel_living_expenses.StylePriority.UseTextAlignment = False
            Me.XrLabel_living_expenses.Text = " "
            Me.XrLabel_living_expenses.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
            '
            'XrLabel7
            '
            Me.XrLabel7.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 106.0!)
            Me.XrLabel7.Name = "XrLabel7"
            Me.XrLabel7.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel7.SizeF = New System.Drawing.SizeF(120.0!, 15.0!)
            Me.XrLabel7.Text = "Account Number"
            '
            'XrLabel6
            '
            Me.XrLabel6.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 46.0!)
            Me.XrLabel6.Name = "XrLabel6"
            Me.XrLabel6.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel6.SizeF = New System.Drawing.SizeF(120.0!, 15.0!)
            Me.XrLabel6.Text = "Account Balance"
            '
            'XrLabel_first_counsel
            '
            Me.XrLabel_first_counsel.LocationFloat = New DevExpress.Utils.PointFloat(133.0!, 91.0!)
            Me.XrLabel_first_counsel.Name = "XrLabel_first_counsel"
            Me.XrLabel_first_counsel.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_first_counsel.SizeF = New System.Drawing.SizeF(130.0!, 15.0!)
            Me.XrLabel_first_counsel.StylePriority.UseTextAlignment = False
            Me.XrLabel_first_counsel.Text = " "
            Me.XrLabel_first_counsel.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
            '
            'XrLabel4
            '
            Me.XrLabel4.LocationFloat = New DevExpress.Utils.PointFloat(275.0!, 108.0!)
            Me.XrLabel4.Name = "XrLabel4"
            Me.XrLabel4.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel4.SizeF = New System.Drawing.SizeF(120.0!, 15.0!)
            Me.XrLabel4.Text = "Work Phone"
            '
            'XrLabel3
            '
            Me.XrLabel3.LocationFloat = New DevExpress.Utils.PointFloat(275.0!, 63.0!)
            Me.XrLabel3.Name = "XrLabel3"
            Me.XrLabel3.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel3.SizeF = New System.Drawing.SizeF(120.0!, 15.0!)
            Me.XrLabel3.Text = "Total Debt Payments"
            '
            'XrLabel2
            '
            Me.XrLabel2.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 61.0!)
            Me.XrLabel2.Name = "XrLabel2"
            Me.XrLabel2.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel2.SizeF = New System.Drawing.SizeF(120.0!, 15.0!)
            Me.XrLabel2.Text = "Number of creditors"
            '
            'XrLabel1
            '
            Me.XrLabel1.BackColor = System.Drawing.Color.Purple
            Me.XrLabel1.BorderColor = System.Drawing.Color.Purple
            Me.XrLabel1.Font = New System.Drawing.Font("Times New Roman", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.XrLabel1.ForeColor = System.Drawing.Color.White
            Me.XrLabel1.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
            Me.XrLabel1.Name = "XrLabel1"
            Me.XrLabel1.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel1.SizeF = New System.Drawing.SizeF(542.0!, 25.0!)
            Me.XrLabel1.StylePriority.UseBackColor = False
            Me.XrLabel1.StylePriority.UseBorderColor = False
            Me.XrLabel1.StylePriority.UseFont = False
            Me.XrLabel1.StylePriority.UseForeColor = False
            Me.XrLabel1.StylePriority.UseTextAlignment = False
            Me.XrLabel1.Text = "STANDARD PROPOSAL INFORMATION"
            Me.XrLabel1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
            Me.XrLabel1.WordWrap = False
            '
            'ReportFooter
            '
            Me.ReportFooter.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel_blank_space})
            Me.ReportFooter.HeightF = 29.99999!
            Me.ReportFooter.Name = "ReportFooter"
            '
            'XrLabel_blank_space
            '
            Me.XrLabel_blank_space.LocationFloat = New DevExpress.Utils.PointFloat(67.00001!, 4.999987!)
            Me.XrLabel_blank_space.Name = "XrLabel_blank_space"
            Me.XrLabel_blank_space.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_blank_space.SizeF = New System.Drawing.SizeF(100.0!, 25.0!)
            '
            'TopMarginBand1
            '
            Me.TopMarginBand1.HeightF = 0.0!
            Me.TopMarginBand1.Name = "TopMarginBand1"
            '
            'BottomMarginBand1
            '
            Me.BottomMarginBand1.HeightF = 0.0!
            Me.BottomMarginBand1.Name = "BottomMarginBand1"
            '
            'XtraReport_rpt_proposal_proof_cdp
            '
            Me.Bands.AddRange(New DevExpress.XtraReports.UI.Band() {Me.Detail, Me.ReportFooter, Me.TopMarginBand1, Me.BottomMarginBand1})
            Me.Margins = New System.Drawing.Printing.Margins(100, 100, 0, 0)
            Me.Version = "9.3"
            CType(Me, System.ComponentModel.ISupportInitialize).EndInit()

        End Sub
        Friend WithEvents Detail As DevExpress.XtraReports.UI.DetailBand
        Friend WithEvents XrLabel4 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel3 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel2 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel1 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_creditor_count As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_proposed_payment As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel22 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_home_ph As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_total_debt As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel19 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_proposed_start As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel17 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel16 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_total_debt_payments As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel14 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_account_balance As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel12 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_account_number As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_total_income As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel9 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_living_expenses As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel7 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel6 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_first_counsel As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_work_ph As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents ReportFooter As DevExpress.XtraReports.UI.ReportFooterBand
        Friend WithEvents XrLabel_blank_space As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents TopMarginBand1 As DevExpress.XtraReports.UI.TopMarginBand
        Friend WithEvents BottomMarginBand1 As DevExpress.XtraReports.UI.BottomMarginBand
    End Class
End Namespace