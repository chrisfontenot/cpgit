#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}
#End Region

Imports DebtPlus.Utils.Format.PhoneNumber

Namespace Proposals.Proof.SubReports
    Public Class XtraReport_rpt_proposal_proof_cdp
        Inherits DevExpress.XtraReports.UI.XtraReport

        Public Sub SetSubreportParameters(ByVal row As System.Data.DataRow)
            Const STR_C As String = "{0:c}"
            Const STR_F0 As String = "{0:f0}"
            Const STR_D As String = "{0:d}"

            For Each col As System.Data.DataColumn In row.Table.Columns
                If row(col) IsNot System.DBNull.Value Then
                    Select Case col.ColumnName.ToLower()
                        Case "proposed_payment"
                            XrLabel_proposed_payment.Text = String.Format(STR_C, Convert.ToDecimal(row(col)))
                        Case "account_balance"
                            XrLabel_account_balance.Text = String.Format(STR_C, Convert.ToDecimal(row(col)))
                        Case "creditor_count"
                            XrLabel_creditor_count.Text = String.Format(STR_F0, Convert.ToInt32(row(col)))
                        Case "proposed_start"
                            XrLabel_proposed_start.Text = String.Format(STR_D, Convert.ToDateTime(row(col)))
                        Case "first_counsel"
                            XrLabel_first_counsel.Text = String.Format(STR_D, Convert.ToDateTime(row(col)))
                        Case "account_number"
                            XrLabel_account_number.Text = Convert.ToString(row(col))
                        Case "total_debt"
                            XrLabel_total_debt.Text = String.Format(STR_C, Convert.ToDecimal(row(col)))
                        Case "total_income"
                            XrLabel_total_income.Text = String.Format(STR_C, Convert.ToDecimal(row(col)))
                        Case "total_debt_payments"
                            XrLabel_total_debt_payments.Text = String.Format(STR_C, Convert.ToDecimal(row(col)))
                        Case "living_expenses"
                            XrLabel_living_expenses.Text = String.Format(STR_C, Convert.ToDecimal(row(col)))
                        Case "home_phone"
                            XrLabel_home_ph.Text = DebtPlus.Utils.Format.PhoneNumber.FormatPhoneNumber(Convert.ToString(row(col)))
                        Case "work_phone"
                            XrLabel_work_ph.Text = DebtPlus.Utils.Format.PhoneNumber.FormatPhoneNumber(Convert.ToString(row(col)))
                    End Select
                End If
            Next
        End Sub
    End Class
End Namespace