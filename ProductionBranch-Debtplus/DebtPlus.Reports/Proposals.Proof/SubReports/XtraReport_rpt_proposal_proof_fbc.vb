#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}
#End Region

Namespace Proposals.Proof.SubReports
    Public Class XtraReport_rpt_proposal_proof_fbc
        Inherits DevExpress.XtraReports.UI.XtraReport

        Public Sub SetSubreportParameters(ByVal vue As System.Data.DataView)
            Const STR_C As String = "{0:c}"

            DataSource = vue

            ' Bind the fields to the information
            With XrLabel_creditor_name
                .DataBindings.Add("Text", vue, "creditor_name")
            End With

            With XrLabel_balance
                .DataBindings.Add("Text", vue, "balance", STR_C)
            End With

            With XrLabel_disbursement_factor
                .DataBindings.Add("Text", vue, "disbursement_factor", STR_C)
            End With

            With XrLabel_total_balance
                .DataBindings.Add("Text", vue, "balance")
            End With

            With XrLabel_total_disbursement_factor
                .DataBindings.Add("Text", vue, "disbursement_factor")
            End With
        End Sub
    End Class
End Namespace