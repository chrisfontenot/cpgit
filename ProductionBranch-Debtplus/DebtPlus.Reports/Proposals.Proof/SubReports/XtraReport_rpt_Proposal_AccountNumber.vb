#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}
#End Region

Namespace Proposals.Proof.SubReports
    Friend Class XtraReport_rpt_Proposal_AccountNumber
        Inherits DevExpress.XtraReports.UI.XtraReport

        Public Sub SetSubreportParameters(ByVal row As System.Data.DataRow)

            ' Store the fields for the display
            If row("account_number") IsNot System.DBNull.Value Then
                XrLabel_account_number.Text = Convert.ToString(row("account_number"))
            End If

            If row("ssn") IsNot System.DBNull.Value Then
                Dim ssn As String = DebtPlus.Utils.Format.SSN.FormatSSN(Convert.ToString(row("ssn")))
                XrLabel_ssn.Text = ssn
            End If

            If row("referred_by") IsNot System.DBNull.Value Then
                XrLabel_referred_by.Text = Convert.ToString(row("referred_by"))
            End If
        End Sub
    End Class
End Namespace