#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}
#End Region

Imports DebtPlus.Utils.Format.Client
Imports DebtPlus.Reports.Template.Forms

Namespace Proposals.Proof
    Public Class ProposalsProofReport
        Inherits Template.TemplateXtraReportClass

        ''' <summary>
        ''' Create an instance of our report
        ''' </summary>
        Public Sub New()
            MyBase.New()
            InitializeComponent()

            ' Reset the parameter values
            Parameter_Batch = -1
            Parameter_Proposal = -1

            ' Register the handlers for the events
            AddHandler BeforePrint, AddressOf ProposalsProofReport_BeforePrint
            AddHandler XtraReport_rpt_proposal_proof_cdp1.BeforePrint, AddressOf XrSubreport_CDP_BeforePrint
            AddHandler XtraReport_rpt_Proposal_AccountNumber1.BeforePrint, AddressOf XtraReport_rpt_Proposal_AccountNumber1_BeforePrint
            AddHandler XrLabel_ClientNameAddress.BeforePrint, AddressOf XrLabel_ClientNameAddress_BeforePrint
            AddHandler XrLabel_Creditor_L.BeforePrint, AddressOf XrLabel_Creditor_L_BeforePrint
            AddHandler XrLabel_DebtID.BeforePrint, AddressOf Format_DebtID
            AddHandler XrSubreport_FBC.BeforePrint, AddressOf FBC_BeforePrint
            AddHandler XrSubreport_FBD.BeforePrint, AddressOf FBD_BeforePrint
        End Sub

        Public Property Parameter_Batch() As System.Int32
            Get
                Dim parm As DevExpress.XtraReports.Parameters.Parameter = FindParameter("Parameter_Batch")
                If parm Is Nothing Then Return -1
                Return Convert.ToInt32(parm.Value)
            End Get
            Set(ByVal value As System.Int32)
                SetParameter("Parameter_Batch", GetType(Int32), value, "Batch ID", False)
            End Set
        End Property

        Public Property Parameter_Proposal() As System.Int32
            Get
                Dim parm As DevExpress.XtraReports.Parameters.Parameter = FindParameter("Parameter_Proposal")
                If parm Is Nothing Then Return -1
                Return Convert.ToInt32(parm.Value)
            End Get
            Set(ByVal value As System.Int32)
                SetParameter("Parameter_Proposal", GetType(Int32), value, "Batch ID", False)
            End Set
        End Property

        Public Overrides ReadOnly Property ReportTitle() As String
            Get
                Return "Proposal Proof"
            End Get
        End Property

        Public Overrides ReadOnly Property ReportSubTitle() As String
            Get
                Return String.Format("This proposal is part of the proposal batch number: {0:f0}", Parameter_Batch)
            End Get
        End Property

        Public Overrides Function NeedParameters() As Boolean
            Return MyBase.NeedParameters() OrElse (Parameter_Batch <= 0 AndAlso Parameter_Proposal <= 0)
        End Function

        Public Overrides Function RequestReportParameters() As System.Windows.Forms.DialogResult
            Dim answer As System.Windows.Forms.DialogResult = DialogResult.OK
            If NeedParameters() Then
                Using frm As New ProposalParametersForm()
                    frm.ShowClosedBatches = False
                    answer = frm.ShowDialog()
                    Parameter_Batch = frm.Parameter_BatchID
                End Using
            End If
            Return answer
        End Function

        Dim ds As New System.Data.DataSet("ds")

        Private Sub ProposalsProofReport_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs)
            Dim rpt As DevExpress.XtraReports.UI.XtraReport = CType(sender, DevExpress.XtraReports.UI.XtraReport)
            Dim proposal As Int32 = Convert.ToInt32(rpt.Parameters("Parameter_Proposal").Value)
            Dim batch As Int32 = Convert.ToInt32(rpt.Parameters("Parameter_Batch").Value)

            Using cn As New System.Data.SqlClient.SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
                cn.Open()
                Using cmd As New System.Data.SqlClient.SqlCommand()
                    cmd.Connection = cn
                    cmd.Connection = New System.Data.SqlClient.SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
                    cmd.CommandType = CommandType.Text
                    cmd.CommandTimeout = 0
                    If proposal >= 0 Then
                        cmd.CommandText = "SELECT TOP 1 [proposal_batch_id],[client],[creditor],[client_creditor],[client_creditor_proposal],[proposal_message],[date_created],[creditor_name],[account_number] FROM [view_proposal_proof] WHERE [client_creditor_proposal]=@client_creditor_proposal"
                        cmd.Parameters.Add("@client_creditor_proposal", SqlDbType.Int).Value = proposal
                    Else
                        cmd.CommandText = "SELECT [proposal_batch_id],[client],[creditor],[client_creditor],[client_creditor_proposal],[proposal_message],[date_created],[creditor_name],[account_number] FROM [view_proposal_proof] WHERE [proposal_batch_id]=@proposal_batch_id"
                        cmd.Parameters.Add("@proposal_batch_id", SqlDbType.Int).Value = batch
                    End If

                    Using da As New SqlClient.SqlDataAdapter(cmd)
                        da.Fill(ds, "view_proposal_proof")
                        Dim tbl As System.Data.DataTable = ds.Tables("view_proposal_proof")

                        ' Correct any references for fields
                        DataSource = New System.Data.DataView(tbl, String.Empty, "creditor, client, client_creditor", DataViewRowState.CurrentRows)
                        For Each fld As DevExpress.XtraReports.UI.CalculatedField In CalculatedFields
                            fld.Assign(DataSource, DataMember)
                        Next
                    End Using
                End Using
            End Using
        End Sub

        Private Sub XrLabel_ClientNameAddress_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs)
            Dim lbl As DevExpress.XtraReports.UI.XRLabel = CType(sender, DevExpress.XtraReports.UI.XRLabel)
            Dim rpt As DevExpress.XtraReports.UI.XtraReport = lbl.Report
            XrLabel_ClientNameAddress.Text = GetClientNameAndAddressByID(rpt, rpt.GetCurrentColumnValue("client"))
        End Sub

        Private Function GetClientNameAndAddressByID(ByVal rpt As DevExpress.XtraReports.UI.XtraReport, ByVal Client As Object) As String
            Dim sb As New System.Text.StringBuilder

            If Client IsNot Nothing AndAlso Client IsNot System.DBNull.Value AndAlso Convert.ToInt32(Client) >= 0 Then
                Dim tbl As System.Data.DataTable = ds.Tables("client_name")
                Dim row As System.Data.DataRow = Nothing
                If tbl IsNot Nothing Then
                    row = tbl.Rows.Find(Client)
                End If

                If row Is Nothing Then
                    Using cn As New System.Data.SqlClient.SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
                        cn.Open()
                        Using cmd As New System.Data.SqlClient.SqlCommand()
                            cmd.Connection = cn
                            cmd.Connection = New System.Data.SqlClient.SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
                            cmd.CommandType = CommandType.Text
                            cmd.CommandTimeout = DebtPlus.LINQ.SQLInfoClass.getDefault().CommandTimeout
                            cmd.CommandText = "SELECT client, name as name, addr1 as address1, addr2 as address2, addr3 as address3 FROM view_client_address WHERE client = @client"
                            cmd.Parameters.Add("@client", SqlDbType.Int).Value = Client

                            Using da As New SqlClient.SqlDataAdapter(cmd)
                                da.Fill(ds, "client_name")

                                tbl = ds.Tables("client_name")
                                If tbl.PrimaryKey.GetUpperBound(0) < 0 Then
                                    tbl.PrimaryKey = New System.Data.DataColumn() {tbl.Columns("client")}
                                End If
                                row = tbl.Rows.Find(Client)
                            End Using
                        End Using
                    End Using
                End If

                If row IsNot Nothing Then
                    sb.Append(Environment.NewLine)
                    sb.AppendFormat("Re: Client # {0}", String.Format("{0:0000000}", Convert.ToInt32(Client)))

                    If Not row.IsNull("name") Then
                        sb.Append(Environment.NewLine)
                        sb.Append(Convert.ToString(row("name")))
                    End If

                    If Not row.IsNull("address1") Then
                        sb.Append(Environment.NewLine)
                        sb.Append(Convert.ToString(row("address1")))
                    End If

                    If Not row.IsNull("address2") Then
                        sb.Append(Environment.NewLine)
                        sb.Append(Convert.ToString(row("address2")))
                    End If

                    If Not row.IsNull("address3") Then
                        sb.Append(Environment.NewLine)
                        sb.Append(Convert.ToString(row("address3")))
                    End If

                    If sb.Length > 0 Then
                        sb.Remove(0, 2)
                    End If
                End If
            End If

            Return sb.ToString()
        End Function

        Private Sub XrLabel_Creditor_L_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs)
            Dim lbl As DevExpress.XtraReports.UI.XRLabel = CType(sender, DevExpress.XtraReports.UI.XRLabel)
            Dim rpt As DevExpress.XtraReports.UI.XtraReport = lbl.Report
            Dim Creditor As Object = rpt.GetCurrentColumnValue("creditor")
            Dim sb As New System.Text.StringBuilder

            If Creditor IsNot Nothing AndAlso Creditor IsNot System.DBNull.Value AndAlso Convert.ToString(Creditor) <> String.Empty Then
                Dim row As System.Data.DataRow = Nothing
                Dim tbl As System.Data.DataTable = ds.Tables("creditor_name")
                If tbl IsNot Nothing Then
                    row = tbl.Rows.Find(New Object() {Creditor, "L"})
                    If row Is Nothing Then row = tbl.Rows.Find(New Object() {Creditor, "P"})
                End If

                If row Is Nothing Then
                    Using cn As New System.Data.SqlClient.SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
                        cn.Open()
                        Using cmd As New System.Data.SqlClient.SqlCommand()
                            cmd.Connection = cn
                            cmd.CommandType = CommandType.Text
                            cmd.CommandText = "SELECT attn, addr1 as address1, addr2 as address2, addr3 as address3, addr4 as address4, addr5 as address5, addr6 as address6, creditor, type FROM view_creditor_addresses WHERE type in ('P', 'L') AND creditor = @creditor"
                            cmd.Parameters.Add("@creditor", SqlDbType.VarChar, 10).Value = Creditor

                            Using da As New System.Data.SqlClient.SqlDataAdapter(cmd)
                                da.Fill(ds, "creditor_name")
                            End Using
                        End Using
                    End Using

                    tbl = ds.Tables("creditor_name")
                    If tbl.PrimaryKey.GetUpperBound(0) < 0 Then
                        tbl.PrimaryKey = New System.Data.DataColumn() {tbl.Columns("creditor"), tbl.Columns("type")}
                    End If

                    row = tbl.Rows.Find(New Object() {Creditor, "L"})
                    If row Is Nothing Then
                        row = tbl.Rows.Find(New Object() {Creditor, "P"})
                    End If
                End If

                If row IsNot Nothing Then
                    For Field As System.Int32 = 0 To 6
                        If Not row.IsNull(Field) Then
                            sb.Append(Environment.NewLine)
                            sb.Append(Convert.ToString(row(Field)))
                        End If
                    Next
                End If

                If sb.Length > 0 Then
                    sb.Remove(0, 2)
                End If
            End If

            lbl.Text = sb.ToString()
        End Sub

        Private Sub XrSubreport_CDP_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs)
            Dim subRpt As DevExpress.XtraReports.UI.XtraReport = CType(sender, DevExpress.XtraReports.UI.XtraReport)
            Dim rpt As DevExpress.XtraReports.UI.XtraReport = subRpt.MasterReport
            Dim tbl As System.Data.DataTable = ds.Tables("rpt_proposal_proof_cdp")
            Dim client_creditor_proposal As System.Int32 = Convert.ToInt32(rpt.GetCurrentColumnValue("client_creditor_proposal"))

            Try
                Using cn As New System.Data.SqlClient.SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
                    cn.Open()
                    Using cmd As New System.Data.SqlClient.SqlCommand()
                        cmd.Connection = cn
                        cmd.CommandText = "rpt_proposal_proof_cdp"
                        cmd.CommandType = System.Data.CommandType.StoredProcedure
                        cmd.Parameters.Add("@client_creditor_proposal", System.Data.SqlDbType.Int).Value = client_creditor_proposal
                        cmd.CommandTimeout = 0

                        Using da As New System.Data.SqlClient.SqlDataAdapter(cmd)
                            da.Fill(ds, "rpt_proposal_proof_cdp")
                            tbl = ds.Tables("rpt_proposal_proof_cdp")
                            If tbl.PrimaryKey.GetUpperBound(0) < 0 Then tbl.PrimaryKey = New System.Data.DataColumn() {tbl.Columns("client_creditor_proposal")}
                        End Using
                    End Using
                End Using

            Catch ex As System.Data.SqlClient.SqlException
                Using gdr As New Repository.GetDataResult()
                    gdr.HandleException(ex)
                    DebtPlus.UI.Common.ErrorHandling.HandleErrors(gdr, "Error reading rpt_proposal_proof_cdp")
                End Using
            End Try

            If tbl IsNot Nothing Then
                Dim row As System.Data.DataRow = tbl.Rows.Find(client_creditor_proposal)
                XtraReport_rpt_proposal_proof_cdp1.SetSubreportParameters(row)
            End If
        End Sub

        Private Sub FBC_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs)
            Dim subRpt As DevExpress.XtraReports.UI.XRSubreport = CType(sender, DevExpress.XtraReports.UI.XRSubreport)
            Dim rpt As DevExpress.XtraReports.UI.XtraReport = subRpt.Report
            Dim client_creditor_proposal As System.Int32 = Convert.ToInt32(rpt.GetCurrentColumnValue("client_creditor_proposal"))
            Dim tbl As System.Data.DataTable = ds.Tables("rpt_Proposal_CreditorList")

            Using cn As New System.Data.SqlClient.SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
                cn.Open()
                Using cmd As New System.Data.SqlClient.SqlCommand()
                    cmd.Connection = cn
                    cmd.CommandType = System.Data.CommandType.StoredProcedure
                    cmd.CommandText = "rpt_Proposal_CreditorList"
                    cmd.CommandTimeout = 0
                    cmd.Parameters.Add("@client_creditor_proposal", System.Data.SqlDbType.Int).Value = client_creditor_proposal

                    Using da As New System.Data.SqlClient.SqlDataAdapter(cmd)
                        da.Fill(ds, "rpt_Proposal_CreditorList")
                        tbl = ds.Tables("rpt_Proposal_CreditorList")
                    End Using
                End Using
            End Using

            Dim vue As New System.Data.DataView(tbl, String.Format("[client_creditor_proposal]={0:f0}", client_creditor_proposal), String.Empty, DataViewRowState.CurrentRows)
            If vue.Count > 0 Then
                XtraReport_rpt_proposal_proof_fbc1.SetSubreportParameters(vue)
            Else
                vue.Dispose()
                e.Cancel = True
            End If
        End Sub

        Private Sub FBD_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs)
            Dim subRpt As DevExpress.XtraReports.UI.XRSubreport = CType(sender, DevExpress.XtraReports.UI.XRSubreport)
            Dim rpt As DevExpress.XtraReports.UI.XtraReport = subRpt.Report
            Dim client_creditor_proposal As System.Int32 = Convert.ToInt32(rpt.GetCurrentColumnValue("client_creditor_proposal"))
            Dim tbl As System.Data.DataTable = ds.Tables("rpt_proposal_fbd_info")

            Using cn As New System.Data.SqlClient.SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
                cn.Open()
                Using cmd As New System.Data.SqlClient.SqlCommand()
                    cmd.Connection = cn
                    cmd.CommandType = CommandType.StoredProcedure
                    cmd.CommandText = "rpt_proposal_fbd_info"
                    cmd.CommandTimeout = 0
                    cmd.Parameters.Add("@proposal_record", SqlDbType.Int).Value = client_creditor_proposal

                    Using da As New System.Data.SqlClient.SqlDataAdapter(cmd)
                        da.Fill(ds, "rpt_proposal_fbd_info")
                        tbl = ds.Tables("rpt_proposal_fbd_info")
                    End Using
                End Using
            End Using

            Dim vue As New System.Data.DataView(tbl, String.Format("[client_creditor_proposal]={0:f0}", client_creditor_proposal), String.Empty, DataViewRowState.CurrentRows)
            If vue.Count > 0 Then
                XtraReport_rpt_proposal_proof_fbd1.SetSubreportParameters(vue)
            Else
                vue.Dispose()
                e.Cancel = True
            End If
        End Sub

        Private Sub XtraReport_rpt_Proposal_AccountNumber1_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs)
            Dim subRpt As DevExpress.XtraReports.UI.XtraReport = CType(sender, DevExpress.XtraReports.UI.XtraReport)
            Dim rpt As DevExpress.XtraReports.UI.XtraReport = subRpt.MasterReport
            Dim client_creditor As System.Int32 = Convert.ToInt32(rpt.GetCurrentColumnValue("client_creditor"))
            Dim tbl As System.Data.DataTable = ds.Tables("rpt_Proposal_AccountNumber")

            Try
                Using cn As New System.Data.SqlClient.SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
                    cn.Open()
                    Using cmd As New System.Data.SqlClient.SqlCommand()
                        cmd.Connection = cn
                        cmd.CommandText = "rpt_Proposal_AccountNumber"
                        cmd.CommandType = CommandType.StoredProcedure
                        cmd.Parameters.Add("@ClientCreditor", SqlDbType.Int).Value = client_creditor
                        cmd.CommandTimeout = 0

                        Using da As New SqlClient.SqlDataAdapter(cmd)
                            da.Fill(ds, "rpt_Proposal_AccountNumber")
                            tbl = ds.Tables("rpt_Proposal_AccountNumber")

                            If tbl.PrimaryKey.GetUpperBound(0) < 0 Then
                                tbl.PrimaryKey = New System.Data.DataColumn() {tbl.Columns("client_creditor")}
                            End If
                        End Using
                    End Using
                End Using

            Catch ex As System.Data.SqlClient.SqlException
                Using gdr As New Repository.GetDataResult()
                    gdr.HandleException(ex)
                    DebtPlus.UI.Common.ErrorHandling.HandleErrors(gdr, "Error reading rpt_Proposal_AccountNumber")
                End Using
            End Try

            If tbl IsNot Nothing Then
                Dim row As System.Data.DataRow = tbl.Rows.Find(client_creditor)
                XtraReport_rpt_Proposal_AccountNumber1.SetSubreportParameters(row)
            End If
        End Sub

        Private Sub Format_DebtID(ByVal sender As Object, ByVal e As System.EventArgs)
            Dim lbl As DevExpress.XtraReports.UI.XRLabel = CType(sender, DevExpress.XtraReports.UI.XRLabel)
            Dim rpt As DevExpress.XtraReports.UI.XtraReport = lbl.Report
            Dim Client As Object = rpt.GetCurrentColumnValue("client")
            Dim Creditor As Object = rpt.GetCurrentColumnValue("creditor")

            lbl.Text = If(Client IsNot System.DBNull.Value AndAlso Creditor IsNot System.DBNull.Value, String.Format("{0:0000000}*{1}", Client, Creditor), String.Empty)
        End Sub
    End Class
End Namespace
