﻿Namespace Custom.FeeIncome.ByOffice
    <Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
    Partial Class FeeIncomeByOfficeReport
        Inherits DebtPlus.Reports.Template.DatedTemplateXtraReportClass

        'XtraReport overrides dispose to clean up the component list.
        <System.Diagnostics.DebuggerNonUserCode()> _
        Protected Overrides Sub Dispose(ByVal disposing As Boolean)
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
            MyBase.Dispose(disposing)
        End Sub

        'Required by the Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Designer
        'It can be modified using the Designer.  
        'Do not modify it using the code editor.
        <System.Diagnostics.DebuggerStepThrough()> _
        Private Sub InitializeComponent()
            Dim XrSummary1 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary()
            Dim XrSummary2 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary()
            Dim XrSummary3 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary()
            Dim XrSummary4 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary()
            Dim XrSummary5 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary()
            Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FeeIncomeByOfficeReport))
            Me.XrTable1 = New DevExpress.XtraReports.UI.XRTable()
            Me.XrTableRow1 = New DevExpress.XtraReports.UI.XRTableRow()
            Me.XrTableCell1 = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableCell3 = New DevExpress.XtraReports.UI.XRTableCell()
            Me.ReportFooter = New DevExpress.XtraReports.UI.ReportFooterBand()
            Me.XrTable2 = New DevExpress.XtraReports.UI.XRTable()
            Me.XrTableRow2 = New DevExpress.XtraReports.UI.XRTableRow()
            Me.XrTableCell2 = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableCell_total_col_1 = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableCell_total_col_2 = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableCell_total_col_3 = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableCell_total_col_4 = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTablecell_total_col_5 = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTable3 = New DevExpress.XtraReports.UI.XRTable()
            Me.XrTableRow3 = New DevExpress.XtraReports.UI.XRTableRow()
            Me.XrTableCell6 = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableCell23 = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableCell15 = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableCell7 = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableCell19 = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableCell8 = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableRow5 = New DevExpress.XtraReports.UI.XRTableRow()
            Me.XrTableCell12 = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableCell24 = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableCell16 = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableCell13 = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableCell20 = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableCell14 = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTable4 = New DevExpress.XtraReports.UI.XRTable()
            Me.XrTableRow4 = New DevExpress.XtraReports.UI.XRTableRow()
            Me.XrTableCell_name = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableCell_col_1 = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTablecell_col_2 = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTablecell_col_3 = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTablecell_col_4 = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTablecell_col_5 = New DevExpress.XtraReports.UI.XRTableCell()
            CType(Me.XrTable1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.XrTable2, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.XrTable3, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.XrTable4, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
            '
            'PageHeader
            '
            Me.PageHeader.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrTable1, Me.XrTable3})
            Me.PageHeader.HeightF = 205.0!
            Me.PageHeader.Controls.SetChildIndex(Me.XrTable3, 0)
            Me.PageHeader.Controls.SetChildIndex(Me.XrTable1, 0)
            Me.PageHeader.Controls.SetChildIndex(Me.XrPanel_AgencyAddress, 0)
            Me.PageHeader.Controls.SetChildIndex(Me.XrLabel_Title, 0)
            Me.PageHeader.Controls.SetChildIndex(Me.XrLine_Title, 0)
            Me.PageHeader.Controls.SetChildIndex(Me.XrLabel_Subtitle, 0)
            Me.PageHeader.Controls.SetChildIndex(Me.XrPageInfo1, 0)
            '
            'XrLabel_Title
            '
            Me.XrLabel_Title.StylePriority.UseFont = False
            '
            'XrLabel_Subtitle
            '
            Me.XrLabel_Subtitle.LocationFloat = New DevExpress.Utils.PointFloat(8.000024!, 85.00001!)
            '
            'XrPageInfo_PageNumber
            '
            Me.XrPageInfo_PageNumber.StylePriority.UseTextAlignment = False
            '
            'XRLabel_Agency_Name
            '
            Me.XRLabel_Agency_Name.StylePriority.UseTextAlignment = False
            '
            'XrLabel_Agency_Address3
            '
            Me.XrLabel_Agency_Address3.StylePriority.UseFont = False
            Me.XrLabel_Agency_Address3.StylePriority.UseTextAlignment = False
            '
            'XrLabel_Agency_Address1
            '
            Me.XrLabel_Agency_Address1.StylePriority.UseFont = False
            Me.XrLabel_Agency_Address1.StylePriority.UseTextAlignment = False
            '
            'XrLabel_Agency_Phone
            '
            Me.XrLabel_Agency_Phone.StylePriority.UseFont = False
            Me.XrLabel_Agency_Phone.StylePriority.UseTextAlignment = False
            '
            'XrLabel_Agency_Address2
            '
            Me.XrLabel_Agency_Address2.StylePriority.UseFont = False
            Me.XrLabel_Agency_Address2.StylePriority.UseTextAlignment = False
            '
            'Detail
            '
            Me.Detail.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrTable4})
            Me.Detail.HeightF = 15.0!
            '
            'XrTable1
            '
            Me.XrTable1.LocationFloat = New DevExpress.Utils.PointFloat(7.999992!, 113.5417!)
            Me.XrTable1.Name = "XrTable1"
            Me.XrTable1.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 2, 2, 100.0!)
            Me.XrTable1.Rows.AddRange(New DevExpress.XtraReports.UI.XRTableRow() {Me.XrTableRow1})
            Me.XrTable1.SizeF = New System.Drawing.SizeF(759.5349!, 36.45832!)
            Me.XrTable1.StyleName = "XrControlStyle_Header"
            Me.XrTable1.StylePriority.UsePadding = False
            '
            'XrTableRow1
            '
            Me.XrTableRow1.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.XrTableCell1, Me.XrTableCell3})
            Me.XrTableRow1.Name = "XrTableRow1"
            Me.XrTableRow1.Weight = 1.0R
            '
            'XrTableCell1
            '
            Me.XrTableCell1.Name = "XrTableCell1"
            Me.XrTableCell1.StylePriority.UseTextAlignment = False
            Me.XrTableCell1.Text = "NOTE:"
            Me.XrTableCell1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
            Me.XrTableCell1.Weight = 0.31802808831853724R
            '
            'XrTableCell3
            '
            Me.XrTableCell3.Name = "XrTableCell3"
            Me.XrTableCell3.StylePriority.UseTextAlignment = False
            Me.XrTableCell3.Text = "Report shows various fees (Z0002) Credit When Credit Is Due, (Z0003) Credit When " & _
        "Credit Is Due, (Z0004) Setup Fee, (Z0009) Return Item Chg, (Z0010) Active Restar" & _
        "t Fee"
            Me.XrTableCell3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
            Me.XrTableCell3.Weight = 4.0858237213197217R
            '
            'ReportFooter
            '
            Me.ReportFooter.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrTable2})
            Me.ReportFooter.HeightF = 27.0!
            Me.ReportFooter.Name = "ReportFooter"
            Me.ReportFooter.StyleName = "XrControlStyle_Totals"
            '
            'XrTable2
            '
            Me.XrTable2.LocationFloat = New DevExpress.Utils.PointFloat(3.125!, 8.0!)
            Me.XrTable2.Name = "XrTable2"
            Me.XrTable2.Rows.AddRange(New DevExpress.XtraReports.UI.XRTableRow() {Me.XrTableRow2})
            Me.XrTable2.SizeF = New System.Drawing.SizeF(764.41!, 17.0!)
            Me.XrTable2.StyleName = "XrControlStyle_Totals"
            '
            'XrTableRow2
            '
            Me.XrTableRow2.Borders = DevExpress.XtraPrinting.BorderSide.Top
            Me.XrTableRow2.BorderWidth = 2
            Me.XrTableRow2.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.XrTableCell2, Me.XrTableCell_total_col_1, Me.XrTableCell_total_col_2, Me.XrTableCell_total_col_3, Me.XrTableCell_total_col_4, Me.XrTablecell_total_col_5})
            Me.XrTableRow2.Name = "XrTableRow2"
            Me.XrTableRow2.StylePriority.UseBorders = False
            Me.XrTableRow2.StylePriority.UseBorderWidth = False
            Me.XrTableRow2.Weight = 1.0R
            '
            'XrTableCell2
            '
            Me.XrTableCell2.Name = "XrTableCell2"
            Me.XrTableCell2.StylePriority.UseTextAlignment = False
            Me.XrTableCell2.Text = "TOTALS"
            Me.XrTableCell2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            Me.XrTableCell2.Weight = 1.0465465567964807R
            '
            'XrTableCell_total_col_1
            '
            Me.XrTableCell_total_col_1.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "col_1")})
            Me.XrTableCell_total_col_1.Name = "XrTableCell_total_col_1"
            Me.XrTableCell_total_col_1.StylePriority.UseTextAlignment = False
            XrSummary1.FormatString = "{0:c}"
            XrSummary1.Running = DevExpress.XtraReports.UI.SummaryRunning.Report
            Me.XrTableCell_total_col_1.Summary = XrSummary1
            Me.XrTableCell_total_col_1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrTableCell_total_col_1.Weight = 0.39944520415857954R
            '
            'XrTableCell_total_col_2
            '
            Me.XrTableCell_total_col_2.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "col_2")})
            Me.XrTableCell_total_col_2.Name = "XrTableCell_total_col_2"
            Me.XrTableCell_total_col_2.StylePriority.UseTextAlignment = False
            XrSummary2.FormatString = "{0:c}"
            XrSummary2.Running = DevExpress.XtraReports.UI.SummaryRunning.Report
            Me.XrTableCell_total_col_2.Summary = XrSummary2
            Me.XrTableCell_total_col_2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrTableCell_total_col_2.Weight = 0.39944520415857954R
            '
            'XrTableCell_total_col_3
            '
            Me.XrTableCell_total_col_3.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "col_3")})
            Me.XrTableCell_total_col_3.Name = "XrTableCell_total_col_3"
            Me.XrTableCell_total_col_3.StylePriority.UseTextAlignment = False
            XrSummary3.FormatString = "{0:c}"
            XrSummary3.Running = DevExpress.XtraReports.UI.SummaryRunning.Report
            Me.XrTableCell_total_col_3.Summary = XrSummary3
            Me.XrTableCell_total_col_3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrTableCell_total_col_3.Weight = 0.39944471655457059R
            '
            'XrTableCell_total_col_4
            '
            Me.XrTableCell_total_col_4.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "col_4")})
            Me.XrTableCell_total_col_4.Name = "XrTableCell_total_col_4"
            Me.XrTableCell_total_col_4.StylePriority.UseTextAlignment = False
            XrSummary4.FormatString = "{0:c}"
            XrSummary4.Running = DevExpress.XtraReports.UI.SummaryRunning.Report
            Me.XrTableCell_total_col_4.Summary = XrSummary4
            Me.XrTableCell_total_col_4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrTableCell_total_col_4.Weight = 0.39944520415857954R
            '
            'XrTablecell_total_col_5
            '
            Me.XrTablecell_total_col_5.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "col_5")})
            Me.XrTablecell_total_col_5.Name = "XrTablecell_total_col_5"
            Me.XrTablecell_total_col_5.StylePriority.UseTextAlignment = False
            XrSummary5.FormatString = "{0:c}"
            XrSummary5.Running = DevExpress.XtraReports.UI.SummaryRunning.Report
            Me.XrTablecell_total_col_5.Summary = XrSummary5
            Me.XrTablecell_total_col_5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrTablecell_total_col_5.Weight = 0.40907160440491708R
            '
            'XrTable3
            '
            Me.XrTable3.LocationFloat = New DevExpress.Utils.PointFloat(3.125!, 159.9583!)
            Me.XrTable3.Name = "XrTable3"
            Me.XrTable3.Rows.AddRange(New DevExpress.XtraReports.UI.XRTableRow() {Me.XrTableRow3, Me.XrTableRow5})
            Me.XrTable3.SizeF = New System.Drawing.SizeF(764.4099!, 36.0!)
            '
            'XrTableRow3
            '
            Me.XrTableRow3.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.XrTableCell6, Me.XrTableCell23, Me.XrTableCell15, Me.XrTableCell7, Me.XrTableCell19, Me.XrTableCell8})
            Me.XrTableRow3.ForeColor = System.Drawing.Color.Gray
            Me.XrTableRow3.Name = "XrTableRow3"
            Me.XrTableRow3.StylePriority.UseForeColor = False
            Me.XrTableRow3.Weight = 0.78678011328854558R
            '
            'XrTableCell6
            '
            Me.XrTableCell6.CanGrow = False
            Me.XrTableCell6.Name = "XrTableCell6"
            Me.XrTableCell6.Weight = 1.0465464348954783R
            '
            'XrTableCell23
            '
            Me.XrTableCell23.CanGrow = False
            Me.XrTableCell23.Name = "XrTableCell23"
            Me.XrTableCell23.StylePriority.UseTextAlignment = False
            Me.XrTableCell23.Text = "Z11973"
            Me.XrTableCell23.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrTableCell23.Weight = 0.39944532605958172R
            '
            'XrTableCell15
            '
            Me.XrTableCell15.CanGrow = False
            Me.XrTableCell15.Name = "XrTableCell15"
            Me.XrTableCell15.StylePriority.UseTextAlignment = False
            Me.XrTableCell15.Text = "Z0003"
            Me.XrTableCell15.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrTableCell15.Weight = 0.39944520415857954R
            '
            'XrTableCell7
            '
            Me.XrTableCell7.CanGrow = False
            Me.XrTableCell7.Name = "XrTableCell7"
            Me.XrTableCell7.StylePriority.UseTextAlignment = False
            Me.XrTableCell7.Text = "Z0004"
            Me.XrTableCell7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrTableCell7.Weight = 0.39944471655457059R
            '
            'XrTableCell19
            '
            Me.XrTableCell19.CanGrow = False
            Me.XrTableCell19.Name = "XrTableCell19"
            Me.XrTableCell19.StylePriority.UseTextAlignment = False
            Me.XrTableCell19.Text = "Z0009"
            Me.XrTableCell19.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrTableCell19.Weight = 0.39944520415857948R
            '
            'XrTableCell8
            '
            Me.XrTableCell8.CanGrow = False
            Me.XrTableCell8.Name = "XrTableCell8"
            Me.XrTableCell8.StylePriority.UseTextAlignment = False
            Me.XrTableCell8.Text = "Z0010"
            Me.XrTableCell8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrTableCell8.Weight = 0.40907160440491713R
            '
            'XrTableRow5
            '
            Me.XrTableRow5.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.XrTableCell12, Me.XrTableCell24, Me.XrTableCell16, Me.XrTableCell13, Me.XrTableCell20, Me.XrTableCell14})
            Me.XrTableRow5.Name = "XrTableRow5"
            Me.XrTableRow5.StyleName = "XrControlStyle_HeaderPannel"
            Me.XrTableRow5.Weight = 0.87934246844825725R
            '
            'XrTableCell12
            '
            Me.XrTableCell12.CanGrow = False
            Me.XrTableCell12.Name = "XrTableCell12"
            Me.XrTableCell12.StylePriority.UseTextAlignment = False
            Me.XrTableCell12.Text = "NAME"
            Me.XrTableCell12.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            Me.XrTableCell12.Weight = 1.0465464348954783R
            '
            'XrTableCell24
            '
            Me.XrTableCell24.CanGrow = False
            Me.XrTableCell24.Name = "XrTableCell24"
            Me.XrTableCell24.StylePriority.UseTextAlignment = False
            Me.XrTableCell24.Text = "CWCID"
            Me.XrTableCell24.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrTableCell24.Weight = 0.39944520415857954R
            '
            'XrTableCell16
            '
            Me.XrTableCell16.CanGrow = False
            Me.XrTableCell16.Name = "XrTableCell16"
            Me.XrTableCell16.StylePriority.UseTextAlignment = False
            Me.XrTableCell16.Text = "CWCID"
            Me.XrTableCell16.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrTableCell16.Weight = 0.39944520415857959R
            '
            'XrTableCell13
            '
            Me.XrTableCell13.CanGrow = False
            Me.XrTableCell13.Name = "XrTableCell13"
            Me.XrTableCell13.StylePriority.UseTextAlignment = False
            Me.XrTableCell13.Text = "SETUP"
            Me.XrTableCell13.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrTableCell13.Weight = 0.39944520415857965R
            '
            'XrTableCell20
            '
            Me.XrTableCell20.CanGrow = False
            Me.XrTableCell20.Name = "XrTableCell20"
            Me.XrTableCell20.StylePriority.UseTextAlignment = False
            Me.XrTableCell20.Text = "RTN ITEM"
            Me.XrTableCell20.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrTableCell20.Weight = 0.39944520415857954R
            '
            'XrTableCell14
            '
            Me.XrTableCell14.CanGrow = False
            Me.XrTableCell14.Name = "XrTableCell14"
            Me.XrTableCell14.StylePriority.UseTextAlignment = False
            Me.XrTableCell14.Text = "RESTART"
            Me.XrTableCell14.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrTableCell14.Weight = 0.4090712387019102R
            '
            'XrTable4
            '
            Me.XrTable4.AnchorVertical = CType((DevExpress.XtraReports.UI.VerticalAnchorStyles.Top Or DevExpress.XtraReports.UI.VerticalAnchorStyles.Bottom), DevExpress.XtraReports.UI.VerticalAnchorStyles)
            Me.XrTable4.LocationFloat = New DevExpress.Utils.PointFloat(3.125!, 0.0!)
            Me.XrTable4.Name = "XrTable4"
            Me.XrTable4.Rows.AddRange(New DevExpress.XtraReports.UI.XRTableRow() {Me.XrTableRow4})
            Me.XrTable4.SizeF = New System.Drawing.SizeF(764.41!, 15.0!)
            '
            'XrTableRow4
            '
            Me.XrTableRow4.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.XrTableCell_name, Me.XrTableCell_col_1, Me.XrTablecell_col_2, Me.XrTablecell_col_3, Me.XrTablecell_col_4, Me.XrTablecell_col_5})
            Me.XrTableRow4.Name = "XrTableRow4"
            Me.XrTableRow4.Weight = 1.0R
            '
            'XrTableCell_name
            '
            Me.XrTableCell_name.CanGrow = False
            Me.XrTableCell_name.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "name")})
            Me.XrTableCell_name.Name = "XrTableCell_name"
            Me.XrTableCell_name.StylePriority.UseTextAlignment = False
            Me.XrTableCell_name.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
            Me.XrTableCell_name.Weight = 1.0465464348954785R
            '
            'XrTableCell_col_1
            '
            Me.XrTableCell_col_1.CanGrow = False
            Me.XrTableCell_col_1.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "col_1", "{0:c}")})
            Me.XrTableCell_col_1.Name = "XrTableCell_col_1"
            Me.XrTableCell_col_1.StylePriority.UseTextAlignment = False
            Me.XrTableCell_col_1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
            Me.XrTableCell_col_1.Weight = 0.39944520415857954R
            '
            'XrTablecell_col_2
            '
            Me.XrTablecell_col_2.CanGrow = False
            Me.XrTablecell_col_2.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "col_2", "{0:c}")})
            Me.XrTablecell_col_2.Name = "XrTablecell_col_2"
            Me.XrTablecell_col_2.StylePriority.UseTextAlignment = False
            Me.XrTablecell_col_2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
            Me.XrTablecell_col_2.Weight = 0.39944532605958177R
            '
            'XrTablecell_col_3
            '
            Me.XrTablecell_col_3.CanGrow = False
            Me.XrTablecell_col_3.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "col_3", "{0:c}")})
            Me.XrTablecell_col_3.Name = "XrTablecell_col_3"
            Me.XrTablecell_col_3.StylePriority.UseTextAlignment = False
            Me.XrTablecell_col_3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
            Me.XrTablecell_col_3.Weight = 0.39944471655457059R
            '
            'XrTablecell_col_4
            '
            Me.XrTablecell_col_4.CanGrow = False
            Me.XrTablecell_col_4.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "col_4", "{0:c}")})
            Me.XrTablecell_col_4.Name = "XrTablecell_col_4"
            Me.XrTablecell_col_4.StylePriority.UseTextAlignment = False
            Me.XrTablecell_col_4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
            Me.XrTablecell_col_4.Weight = 0.39944520415857954R
            '
            'XrTablecell_col_5
            '
            Me.XrTablecell_col_5.CanGrow = False
            Me.XrTablecell_col_5.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "col_5", "{0:c}")})
            Me.XrTablecell_col_5.Name = "XrTablecell_col_5"
            Me.XrTablecell_col_5.StylePriority.UseTextAlignment = False
            Me.XrTablecell_col_5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
            Me.XrTablecell_col_5.Weight = 0.40907160440491708R
            '
            'FeeIncomeByOfficeReport
            '
            Me.Bands.AddRange(New DevExpress.XtraReports.UI.Band() {Me.Detail, Me.PageHeader, Me.PageFooter, Me.TopMarginBand1, Me.BottomMarginBand1, Me.ReportFooter})
            Me.ScriptLanguage = DevExpress.XtraReports.ScriptLanguage.VisualBasic
            Me.ScriptReferencesString = resources.GetString("$this.ScriptReferencesString")
            Me.Scripts.OnBeforePrint = "FeeIncomeByOfficeReport_BeforePrint"
            Me.ScriptsSource = resources.GetString("$this.ScriptsSource")
            Me.StyleSheet.AddRange(New DevExpress.XtraReports.UI.XRControlStyle() {Me.XrControlStyle_Header, Me.XrControlStyle_Totals, Me.XrControlStyle_HeaderPannel, Me.XrControlStyle_GroupHeader})
            Me.Version = "11.2"
            Me.Controls.SetChildIndex(Me.ReportFooter, 0)
            Me.Controls.SetChildIndex(Me.BottomMarginBand1, 0)
            Me.Controls.SetChildIndex(Me.TopMarginBand1, 0)
            Me.Controls.SetChildIndex(Me.PageFooter, 0)
            Me.Controls.SetChildIndex(Me.PageHeader, 0)
            Me.Controls.SetChildIndex(Me.Detail, 0)
            CType(Me.XrTable1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.XrTable2, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.XrTable3, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.XrTable4, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me, System.ComponentModel.ISupportInitialize).EndInit()

        End Sub
        Friend WithEvents XrTable1 As DevExpress.XtraReports.UI.XRTable
        Friend WithEvents XrTableRow1 As DevExpress.XtraReports.UI.XRTableRow
        Friend WithEvents XrTableCell1 As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents XrTableCell3 As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents XrTable4 As DevExpress.XtraReports.UI.XRTable
        Friend WithEvents XrTableRow4 As DevExpress.XtraReports.UI.XRTableRow
        Friend WithEvents XrTableCell_name As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents XrTableCell_col_1 As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents XrTablecell_col_2 As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents XrTablecell_col_3 As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents XrTablecell_col_4 As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents XrTablecell_col_5 As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents XrTable3 As DevExpress.XtraReports.UI.XRTable
        Friend WithEvents XrTableRow3 As DevExpress.XtraReports.UI.XRTableRow
        Friend WithEvents XrTableCell6 As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents XrTableCell23 As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents XrTableCell15 As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents XrTableCell7 As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents XrTableCell19 As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents XrTableCell8 As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents XrTableRow5 As DevExpress.XtraReports.UI.XRTableRow
        Friend WithEvents XrTableCell12 As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents XrTableCell24 As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents XrTableCell16 As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents XrTableCell13 As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents XrTableCell20 As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents XrTableCell14 As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents ReportFooter As DevExpress.XtraReports.UI.ReportFooterBand
        Friend WithEvents XrTable2 As DevExpress.XtraReports.UI.XRTable
        Friend WithEvents XrTableRow2 As DevExpress.XtraReports.UI.XRTableRow
        Friend WithEvents XrTableCell2 As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents XrTableCell_total_col_2 As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents XrTableCell_total_col_3 As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents XrTableCell_total_col_4 As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents XrTablecell_total_col_5 As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents XrTableCell_total_col_1 As DevExpress.XtraReports.UI.XRTableCell
    End Class
End Namespace
