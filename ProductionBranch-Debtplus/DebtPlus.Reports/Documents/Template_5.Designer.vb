﻿Namespace Documents
    <Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
    Partial Class Template_5

        'XtraReport overrides dispose to clean up the component list.
        <System.Diagnostics.DebuggerNonUserCode()> _
        Protected Overrides Sub Dispose(ByVal disposing As Boolean)
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
            MyBase.Dispose(disposing)
        End Sub

        'Required by the Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Designer
        'It can be modified using the Designer.  
        'Do not modify it using the code editor.
        <System.Diagnostics.DebuggerStepThrough()> _
        Private Sub InitializeComponent()
            CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
            '
            'XrLabel_reportHeaderBox3
            '
            Me.XrLabel_reportHeaderBox3.LocationFloat = New DevExpress.Utils.PointFloat(504.7083!, 99.54169!)
            Me.XrLabel_reportHeaderBox3.SizeF = New System.Drawing.SizeF(174.0!, 10.0!)
            Me.XrLabel_reportHeaderBox3.StylePriority.UseBackColor = False
            Me.XrLabel_reportHeaderBox3.StylePriority.UseBorderColor = False
            Me.XrLabel_reportHeaderBox3.StylePriority.UseForeColor = False
            '
            'XrLabel_reportHeaderBox2
            '
            Me.XrLabel_reportHeaderBox2.LocationFloat = New DevExpress.Utils.PointFloat(313.0!, 99.54169!)
            Me.XrLabel_reportHeaderBox2.SizeF = New System.Drawing.SizeF(174.0!, 10.0!)
            Me.XrLabel_reportHeaderBox2.StylePriority.UseBackColor = False
            Me.XrLabel_reportHeaderBox2.StylePriority.UseBorderColor = False
            Me.XrLabel_reportHeaderBox2.StylePriority.UseForeColor = False
            '
            'XrLabel_reportHeaderBox1
            '
            Me.XrLabel_reportHeaderBox1.LocationFloat = New DevExpress.Utils.PointFloat(121.2917!, 98.41669!)
            Me.XrLabel_reportHeaderBox1.SizeF = New System.Drawing.SizeF(174.0!, 10.0!)
            Me.XrLabel_reportHeaderBox1.StylePriority.UseBackColor = False
            Me.XrLabel_reportHeaderBox1.StylePriority.UseBorderColor = False
            Me.XrLabel_reportHeaderBox1.StylePriority.UseForeColor = False
            '
            'XrLabel_pageHeaderBox3
            '
            Me.XrLabel_pageHeaderBox3.LocationFloat = New DevExpress.Utils.PointFloat(504.7083!, 98.42!)
            Me.XrLabel_pageHeaderBox3.SizeF = New System.Drawing.SizeF(174.0!, 10.0!)
            Me.XrLabel_pageHeaderBox3.StylePriority.UseBackColor = False
            Me.XrLabel_pageHeaderBox3.StylePriority.UseBorderColor = False
            Me.XrLabel_pageHeaderBox3.StylePriority.UseForeColor = False
            '
            'XrLabel_pageHeaderBox2
            '
            Me.XrLabel_pageHeaderBox2.LocationFloat = New DevExpress.Utils.PointFloat(313.0!, 98.42!)
            Me.XrLabel_pageHeaderBox2.SizeF = New System.Drawing.SizeF(174.0!, 10.0!)
            Me.XrLabel_pageHeaderBox2.StylePriority.UseBackColor = False
            Me.XrLabel_pageHeaderBox2.StylePriority.UseBorderColor = False
            Me.XrLabel_pageHeaderBox2.StylePriority.UseForeColor = False
            '
            'XrLabel_pageHeaderBox1
            '
            Me.XrLabel_pageHeaderBox1.LocationFloat = New DevExpress.Utils.PointFloat(121.2917!, 98.42!)
            Me.XrLabel_pageHeaderBox1.SizeF = New System.Drawing.SizeF(174.0!, 10.0!)
            Me.XrLabel_pageHeaderBox1.StylePriority.UseBackColor = False
            Me.XrLabel_pageHeaderBox1.StylePriority.UseBorderColor = False
            Me.XrLabel_pageHeaderBox1.StylePriority.UseForeColor = False
            '
            'XrLabel_ReportTitle
            '
            Me.XrLabel_ReportTitle.LocationFloat = New DevExpress.Utils.PointFloat(10.00001!, 0.0!)
            Me.XrLabel_ReportTitle.SizeF = New System.Drawing.SizeF(780.0!, 98.41669!)
            Me.XrLabel_ReportTitle.StylePriority.UseFont = False
            Me.XrLabel_ReportTitle.StylePriority.UseForeColor = False
            Me.XrLabel_ReportTitle.StylePriority.UseTextAlignment = False
            Me.XrLabel_ReportTitle.Text = "TEMPLATE_5"
            Me.XrLabel_ReportTitle.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
            '
            'XrLine_reportHeaderLine
            '
            Me.XrLine_reportHeaderLine.LineWidth = 1
            Me.XrLine_reportHeaderLine.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
            Me.XrLine_reportHeaderLine.SizeF = New System.Drawing.SizeF(10.0!, 2.0!)
            Me.XrLine_reportHeaderLine.StylePriority.UseBorderColor = False
            Me.XrLine_reportHeaderLine.StylePriority.UseForeColor = False
            Me.XrLine_reportHeaderLine.Visible = False
            '
            'XrLine_pageHeaderLine
            '
            Me.XrLine_pageHeaderLine.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
            Me.XrLine_pageHeaderLine.SizeF = New System.Drawing.SizeF(10.0!, 2.09!)
            Me.XrLine_pageHeaderLine.StylePriority.UseBorderColor = False
            Me.XrLine_pageHeaderLine.StylePriority.UseForeColor = False
            Me.XrLine_pageHeaderLine.Visible = False
            '
            'XrLabel_pageHeader_Title
            '
            Me.XrLabel_pageHeader_Title.LocationFloat = New DevExpress.Utils.PointFloat(10.0!, 0.0!)
            Me.XrLabel_pageHeader_Title.SizeF = New System.Drawing.SizeF(780.0!, 98.42!)
            Me.XrLabel_pageHeader_Title.StylePriority.UseFont = False
            Me.XrLabel_pageHeader_Title.StylePriority.UseForeColor = False
            Me.XrLabel_pageHeader_Title.StylePriority.UseTextAlignment = False
            Me.XrLabel_pageHeader_Title.Text = "TEMPLATE_5 (Continued)"
            Me.XrLabel_pageHeader_Title.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
            '
            'ReportHeader
            '
            Me.ReportHeader.HeightF = 114.4!
            '
            'PageHeader
            '
            Me.PageHeader.HeightF = 114.4!
            Me.PageHeader.Visible = True
            '
            'Template_5
            '
            Me.Bands.AddRange(New DevExpress.XtraReports.UI.Band() {Me.TopMarginBand1, Me.Detail, Me.BottomMarginBand1, Me.ReportHeader, Me.PageHeader})
            Me.StyleSheet.AddRange(New DevExpress.XtraReports.UI.XRControlStyle() {Me.XrControlStyle_HeaderText, Me.XrControlStyle_HeaderLine, Me.XrControlStyle_HeaderBox1, Me.XrControlStyle_HeaderBox2, Me.XrControlStyle_HeaderBox3, Me.XrControlStyle_Report_Column, Me.XrControlStyle_Report_Group, Me.XrControlStyle_Report_Detail, Me.XrControlStyle_Report_Total, Me.XrControlStyle_Footer, Me.XrControlStyle_HeaderSubTitle})
            Me.Version = "11.2"
            CType(Me, System.ComponentModel.ISupportInitialize).EndInit()

        End Sub
    End Class
End Namespace