﻿Namespace Documents
    <Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
    Partial Class Template_3

        'XtraReport overrides dispose to clean up the component list.
        <System.Diagnostics.DebuggerNonUserCode()> _
        Protected Overrides Sub Dispose(ByVal disposing As Boolean)
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
            MyBase.Dispose(disposing)
        End Sub

        'Required by the Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Designer
        'It can be modified using the Designer.  
        'Do not modify it using the code editor.
        <System.Diagnostics.DebuggerStepThrough()> _
        Private Sub InitializeComponent()
            Me.XrLabel_reportHeaderBox3 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_reportHeaderBox2 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_reportHeaderBox1 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_pageHeaderBox1 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_pageHeaderBox2 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_pageHeaderBox3 = New DevExpress.XtraReports.UI.XRLabel()
            CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
            '
            'ReportHeader
            '
            Me.ReportHeader.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel_reportHeaderBox1, Me.XrLabel_reportHeaderBox2, Me.XrLabel_reportHeaderBox3})
            Me.PageHeader.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel_pageHeaderBox1, Me.XrLabel_pageHeaderBox2, Me.XrLabel_pageHeaderBox3})
            '
            'XrLabel_reportHeaderBox3
            '
            Me.XrLabel_reportHeaderBox3.LocationFloat = New DevExpress.Utils.PointFloat(734.7917!, 27.045!)
            Me.XrLabel_reportHeaderBox3.Name = "XrLabel_reportHeaderBox3"
            Me.XrLabel_reportHeaderBox3.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_reportHeaderBox3.SizeF = New System.Drawing.SizeF(56.0!, 56.0!)
            Me.XrLabel_reportHeaderBox3.StyleName = "XrControlStyle_HeaderBox3"
            '
            'XrLabel_reportHeaderBox2
            '
            Me.XrLabel_reportHeaderBox2.LocationFloat = New DevExpress.Utils.PointFloat(667.9167!, 27.045!)
            Me.XrLabel_reportHeaderBox2.Name = "XrLabel_reportHeaderBox2"
            Me.XrLabel_reportHeaderBox2.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_reportHeaderBox2.SizeF = New System.Drawing.SizeF(56.0!, 56.0!)
            Me.XrLabel_reportHeaderBox2.StyleName = "XrControlStyle_HeaderBox2"
            '
            'XrLabel_reportHeaderBox1
            '
            Me.XrLabel_reportHeaderBox1.LocationFloat = New DevExpress.Utils.PointFloat(601.0417!, 27.045!)
            Me.XrLabel_reportHeaderBox1.Name = "XrLabel_reportHeaderBox1"
            Me.XrLabel_reportHeaderBox1.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_reportHeaderBox1.SizeF = New System.Drawing.SizeF(56.0!, 56.0!)
            Me.XrLabel_reportHeaderBox1.StyleName = "XrControlStyle_HeaderBox1"
            '
            'XrLabel_pageHeaderBox1
            '
            Me.XrLabel_pageHeaderBox1.LocationFloat = New DevExpress.Utils.PointFloat(601.04!, 27.045!)
            Me.XrLabel_pageHeaderBox1.Name = "XrLabel_pageHeaderBox1"
            Me.XrLabel_pageHeaderBox1.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_pageHeaderBox1.SizeF = New System.Drawing.SizeF(56.0!, 56.0!)
            Me.XrLabel_pageHeaderBox1.StyleName = "XrControlStyle_HeaderBox1"
            '
            'XrLabel_pageHeaderBox2
            '
            Me.XrLabel_pageHeaderBox2.LocationFloat = New DevExpress.Utils.PointFloat(667.92!, 27.045!)
            Me.XrLabel_pageHeaderBox2.Name = "XrLabel_pageHeaderBox2"
            Me.XrLabel_pageHeaderBox2.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_pageHeaderBox2.SizeF = New System.Drawing.SizeF(56.0!, 56.0!)
            Me.XrLabel_pageHeaderBox2.StyleName = "XrControlStyle_HeaderBox2"
            '
            'XrLabel_pageHeaderBox3
            '
            Me.XrLabel_pageHeaderBox3.LocationFloat = New DevExpress.Utils.PointFloat(734.79!, 27.045!)
            Me.XrLabel_pageHeaderBox3.Name = "XrLabel_pageHeaderBox3"
            Me.XrLabel_pageHeaderBox3.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_pageHeaderBox3.SizeF = New System.Drawing.SizeF(56.0!, 56.0!)
            Me.XrLabel_pageHeaderBox3.StyleName = "XrControlStyle_HeaderBox3"
            '
            'Template_3
            '
            Me.Bands.AddRange(New DevExpress.XtraReports.UI.Band() {Me.TopMarginBand1, Me.Detail, Me.BottomMarginBand1, Me.ReportHeader, Me.PageHeader})
            Me.Version = "11.2"
            CType(Me, System.ComponentModel.ISupportInitialize).EndInit()
        End Sub

        Protected XrLabel_reportHeaderBox3 As DevExpress.XtraReports.UI.XRLabel
        Protected XrLabel_reportHeaderBox2 As DevExpress.XtraReports.UI.XRLabel
        Protected XrLabel_reportHeaderBox1 As DevExpress.XtraReports.UI.XRLabel
        Protected XrLabel_pageHeaderBox3 As DevExpress.XtraReports.UI.XRLabel
        Protected XrLabel_pageHeaderBox2 As DevExpress.XtraReports.UI.XRLabel
        Protected XrLabel_pageHeaderBox1 As DevExpress.XtraReports.UI.XRLabel
    End Class
End Namespace