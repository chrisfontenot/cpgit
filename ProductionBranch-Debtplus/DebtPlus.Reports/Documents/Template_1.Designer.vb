﻿Namespace Documents
    <Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
    Partial Class Template_1

        'XtraReport overrides dispose to clean up the component list.
        <System.Diagnostics.DebuggerNonUserCode()> _
        Protected Overrides Sub Dispose(ByVal disposing As Boolean)
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
            MyBase.Dispose(disposing)
        End Sub

        'Required by the Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Designer
        'It can be modified using the Designer.  
        'Do not modify it using the code editor.
        <System.Diagnostics.DebuggerStepThrough()> _
        Private Sub InitializeComponent()
            Me.ReportHeader = New DevExpress.XtraReports.UI.ReportHeaderBand()
            Me.PageHeader = New DevExpress.XtraReports.UI.PageHeaderBand()
            Me.XrControlStyle_HeaderText = New DevExpress.XtraReports.UI.XRControlStyle()
            Me.XrControlStyle_HeaderLine = New DevExpress.XtraReports.UI.XRControlStyle()
            Me.XrControlStyle_HeaderBox1 = New DevExpress.XtraReports.UI.XRControlStyle()
            Me.XrControlStyle_HeaderBox2 = New DevExpress.XtraReports.UI.XRControlStyle()
            Me.XrControlStyle_HeaderBox3 = New DevExpress.XtraReports.UI.XRControlStyle()
            Me.XrControlStyle_Report_Column = New DevExpress.XtraReports.UI.XRControlStyle()
            Me.XrControlStyle_Report_Group = New DevExpress.XtraReports.UI.XRControlStyle()
            Me.XrControlStyle_Report_Detail = New DevExpress.XtraReports.UI.XRControlStyle()
            Me.XrControlStyle_Report_Total = New DevExpress.XtraReports.UI.XRControlStyle()
            Me.XrControlStyle_Footer = New DevExpress.XtraReports.UI.XRControlStyle()
            Me.XrControlStyle_HeaderSubTitle = New DevExpress.XtraReports.UI.XRControlStyle()
            CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
            '
            'Detail
            '
            Me.Detail.HeightF = 0.0!
            '
            'ReportHeader
            '
            Me.ReportHeader.HeightF = 0.0!
            Me.ReportHeader.Name = "ReportHeader"
            '
            'PageHeader
            '
            Me.PageHeader.HeightF = 0.0!
            Me.PageHeader.Name = "PageHeader"
            Me.PageHeader.PrintOn = DevExpress.XtraReports.UI.PrintOnPages.NotWithReportHeader
            Me.PageHeader.Visible = False
            '
            'XrControlStyle_HeaderText
            '
            Me.XrControlStyle_HeaderText.Font = New System.Drawing.Font("Calibri", 27.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.XrControlStyle_HeaderText.ForeColor = System.Drawing.Color.Teal
            Me.XrControlStyle_HeaderText.Name = "XrControlStyle_HeaderText"
            Me.XrControlStyle_HeaderText.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
            Me.XrControlStyle_HeaderText.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            '
            'XrControlStyle_HeaderLine
            '
            Me.XrControlStyle_HeaderLine.ForeColor = System.Drawing.Color.Teal
            Me.XrControlStyle_HeaderLine.Name = "XrControlStyle_HeaderLine"
            Me.XrControlStyle_HeaderLine.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
            Me.XrControlStyle_HeaderLine.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            '
            'XrControlStyle_HeaderBox1
            '
            Me.XrControlStyle_HeaderBox1.BackColor = System.Drawing.Color.Teal
            Me.XrControlStyle_HeaderBox1.BorderColor = System.Drawing.Color.Teal
            Me.XrControlStyle_HeaderBox1.ForeColor = System.Drawing.Color.Teal
            Me.XrControlStyle_HeaderBox1.Name = "XrControlStyle_HeaderBox1"
            '
            'XrControlStyle_HeaderBox2
            '
            Me.XrControlStyle_HeaderBox2.BackColor = System.Drawing.Color.FromArgb(CType(CType(102, Byte), Integer), CType(CType(102, Byte), Integer), CType(CType(153, Byte), Integer))
            Me.XrControlStyle_HeaderBox2.BorderColor = System.Drawing.Color.FromArgb(CType(CType(102, Byte), Integer), CType(CType(102, Byte), Integer), CType(CType(153, Byte), Integer))
            Me.XrControlStyle_HeaderBox2.ForeColor = System.Drawing.Color.FromArgb(CType(CType(102, Byte), Integer), CType(CType(102, Byte), Integer), CType(CType(153, Byte), Integer))
            Me.XrControlStyle_HeaderBox2.Name = "XrControlStyle_HeaderBox2"
            '
            'XrControlStyle_HeaderBox3
            '
            Me.XrControlStyle_HeaderBox3.BackColor = System.Drawing.Color.FromArgb(CType(CType(137, Byte), Integer), CType(CType(201, Byte), Integer), CType(CType(191, Byte), Integer))
            Me.XrControlStyle_HeaderBox3.BorderColor = System.Drawing.Color.FromArgb(CType(CType(137, Byte), Integer), CType(CType(201, Byte), Integer), CType(CType(191, Byte), Integer))
            Me.XrControlStyle_HeaderBox3.ForeColor = System.Drawing.Color.FromArgb(CType(CType(137, Byte), Integer), CType(CType(201, Byte), Integer), CType(CType(191, Byte), Integer))
            Me.XrControlStyle_HeaderBox3.Name = "XrControlStyle_HeaderBox3"
            '
            'XrControlStyle_Report_Column
            '
            Me.XrControlStyle_Report_Column.BackColor = System.Drawing.Color.Teal
            Me.XrControlStyle_Report_Column.BorderColor = System.Drawing.Color.White
            Me.XrControlStyle_Report_Column.BorderDashStyle = DevExpress.XtraPrinting.BorderDashStyle.Solid
            Me.XrControlStyle_Report_Column.Borders = DevExpress.XtraPrinting.BorderSide.Bottom
            Me.XrControlStyle_Report_Column.BorderWidth = 2
            Me.XrControlStyle_Report_Column.Font = New System.Drawing.Font("Calibri", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.XrControlStyle_Report_Column.ForeColor = System.Drawing.Color.White
            Me.XrControlStyle_Report_Column.Name = "XrControlStyle_Report_Column"
            Me.XrControlStyle_Report_Column.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrControlStyle_Report_Column.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            '
            'XrControlStyle_Report_Group
            '
            Me.XrControlStyle_Report_Group.BackColor = System.Drawing.Color.FromArgb(CType(CType(102, Byte), Integer), CType(CType(102, Byte), Integer), CType(CType(153, Byte), Integer))
            Me.XrControlStyle_Report_Group.BorderColor = System.Drawing.Color.White
            Me.XrControlStyle_Report_Group.BorderDashStyle = DevExpress.XtraPrinting.BorderDashStyle.Solid
            Me.XrControlStyle_Report_Group.Borders = DevExpress.XtraPrinting.BorderSide.Bottom
            Me.XrControlStyle_Report_Group.BorderWidth = 2
            Me.XrControlStyle_Report_Group.Font = New System.Drawing.Font("Calibri", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.XrControlStyle_Report_Group.ForeColor = System.Drawing.Color.White
            Me.XrControlStyle_Report_Group.Name = "XrControlStyle_Report_Group"
            Me.XrControlStyle_Report_Group.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            '
            'XrControlStyle_Report_Detail
            '
            Me.XrControlStyle_Report_Detail.BackColor = System.Drawing.Color.FromArgb(CType(CType(222, Byte), Integer), CType(CType(222, Byte), Integer), CType(CType(233, Byte), Integer))
            Me.XrControlStyle_Report_Detail.BorderColor = System.Drawing.Color.White
            Me.XrControlStyle_Report_Detail.BorderDashStyle = DevExpress.XtraPrinting.BorderDashStyle.Solid
            Me.XrControlStyle_Report_Detail.Borders = DevExpress.XtraPrinting.BorderSide.Bottom
            Me.XrControlStyle_Report_Detail.BorderWidth = 2
            Me.XrControlStyle_Report_Detail.Font = New System.Drawing.Font("Calibri", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.XrControlStyle_Report_Detail.Name = "XrControlStyle_Report_Detail"
            Me.XrControlStyle_Report_Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            '
            'XrControlStyle_Report_Total
            '
            Me.XrControlStyle_Report_Total.BackColor = System.Drawing.Color.FromArgb(CType(CType(222, Byte), Integer), CType(CType(222, Byte), Integer), CType(CType(233, Byte), Integer))
            Me.XrControlStyle_Report_Total.BorderColor = System.Drawing.Color.White
            Me.XrControlStyle_Report_Total.BorderDashStyle = DevExpress.XtraPrinting.BorderDashStyle.Solid
            Me.XrControlStyle_Report_Total.Borders = DevExpress.XtraPrinting.BorderSide.Bottom
            Me.XrControlStyle_Report_Total.BorderWidth = 2
            Me.XrControlStyle_Report_Total.Font = New System.Drawing.Font("Calibri", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.XrControlStyle_Report_Total.Name = "XrControlStyle_Report_Total"
            Me.XrControlStyle_Report_Total.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            '
            'XrControlStyle_Footer
            '
            Me.XrControlStyle_Footer.BorderColor = System.Drawing.Color.Teal
            Me.XrControlStyle_Footer.BorderDashStyle = DevExpress.XtraPrinting.BorderDashStyle.Solid
            Me.XrControlStyle_Footer.BorderWidth = 2
            Me.XrControlStyle_Footer.Font = New System.Drawing.Font("Calibri", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.XrControlStyle_Footer.ForeColor = System.Drawing.Color.Teal
            Me.XrControlStyle_Footer.Name = "XrControlStyle_Footer"
            Me.XrControlStyle_Footer.Padding = New DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 100.0!)
            Me.XrControlStyle_Footer.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            '
            'XrControlStyle_HeaderSubTitle
            '
            Me.XrControlStyle_HeaderSubTitle.Font = New System.Drawing.Font("Calibri", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.XrControlStyle_HeaderSubTitle.Name = "XrControlStyle_HeaderSubTitle"
            Me.XrControlStyle_HeaderSubTitle.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            '
            'Template_1
            '
            Me.Bands.AddRange(New DevExpress.XtraReports.UI.Band() {Me.TopMarginBand1, Me.Detail, Me.BottomMarginBand1, Me.ReportHeader, Me.PageHeader})
            Me.Font = New System.Drawing.Font("Calibri", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.StyleSheet.AddRange(New DevExpress.XtraReports.UI.XRControlStyle() {Me.XrControlStyle_HeaderText, Me.XrControlStyle_HeaderLine, Me.XrControlStyle_HeaderBox1, Me.XrControlStyle_HeaderBox2, Me.XrControlStyle_HeaderBox3, Me.XrControlStyle_Report_Column, Me.XrControlStyle_Report_Group, Me.XrControlStyle_Report_Detail, Me.XrControlStyle_Report_Total, Me.XrControlStyle_Footer, Me.XrControlStyle_HeaderSubTitle})
            Me.Version = "11.2"
            Me.Controls.SetChildIndex(Me.PageHeader, 0)
            Me.Controls.SetChildIndex(Me.ReportHeader, 0)
            Me.Controls.SetChildIndex(Me.BottomMarginBand1, 0)
            Me.Controls.SetChildIndex(Me.Detail, 0)
            Me.Controls.SetChildIndex(Me.TopMarginBand1, 0)
            CType(Me, System.ComponentModel.ISupportInitialize).EndInit()

        End Sub
        Protected ReportHeader As DevExpress.XtraReports.UI.ReportHeaderBand
        Protected PageHeader As DevExpress.XtraReports.UI.PageHeaderBand
        Protected XrControlStyle_HeaderText As DevExpress.XtraReports.UI.XRControlStyle
        Protected XrControlStyle_HeaderLine As DevExpress.XtraReports.UI.XRControlStyle
        Protected XrControlStyle_HeaderBox1 As DevExpress.XtraReports.UI.XRControlStyle
        Protected XrControlStyle_HeaderBox2 As DevExpress.XtraReports.UI.XRControlStyle
        Protected XrControlStyle_HeaderBox3 As DevExpress.XtraReports.UI.XRControlStyle
        Protected XrControlStyle_Report_Column As DevExpress.XtraReports.UI.XRControlStyle
        Protected XrControlStyle_Report_Group As DevExpress.XtraReports.UI.XRControlStyle
        Protected XrControlStyle_Report_Detail As DevExpress.XtraReports.UI.XRControlStyle
        Protected XrControlStyle_Report_Total As DevExpress.XtraReports.UI.XRControlStyle
        Protected XrControlStyle_Footer As DevExpress.XtraReports.UI.XRControlStyle
        Protected XrControlStyle_HeaderSubTitle As DevExpress.XtraReports.UI.XRControlStyle
    End Class
End Namespace