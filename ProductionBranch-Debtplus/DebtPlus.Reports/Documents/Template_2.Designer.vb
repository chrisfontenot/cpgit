﻿Namespace Documents
    <Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
    Partial Class Template_2

        'XtraReport overrides dispose to clean up the component list.
        <System.Diagnostics.DebuggerNonUserCode()> _
        Protected Overrides Sub Dispose(ByVal disposing As Boolean)
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
            MyBase.Dispose(disposing)
        End Sub

        'Required by the Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Designer
        'It can be modified using the Designer.  
        'Do not modify it using the code editor.
        <System.Diagnostics.DebuggerStepThrough()> _
        Private Sub InitializeComponent()
            Me.XrLabel_ReportTitle = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLine_reportHeaderLine = New DevExpress.XtraReports.UI.XRLine()
            Me.XrLabel_pageHeader_Title = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLine_pageHeaderLine = New DevExpress.XtraReports.UI.XRLine()
            CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
            '
            'ReportHeader
            '
            Me.ReportHeader.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel_ReportTitle, Me.XrLine_reportHeaderLine})
            Me.ReportHeader.HeightF = 135.0!
            '
            'PageHeader
            '
            Me.PageHeader.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLine_pageHeaderLine, Me.XrLabel_pageHeader_Title})
            Me.PageHeader.HeightF = 135.0!
            '
            'XrLabel_ReportTitle
            '
            Me.XrLabel_ReportTitle.LocationFloat = New DevExpress.Utils.PointFloat(10.00001!, 10.00001!)
            Me.XrLabel_ReportTitle.Multiline = True
            Me.XrLabel_ReportTitle.Name = "XrLabel_ReportTitle"
            Me.XrLabel_ReportTitle.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_ReportTitle.SizeF = New System.Drawing.SizeF(564.5833!, 98.0!)
            Me.XrLabel_ReportTitle.StyleName = "XrControlStyle_HeaderText"
            '
            'XrLine_reportHeaderLine
            '
            Me.XrLine_reportHeaderLine.LineWidth = 2
            Me.XrLine_reportHeaderLine.LocationFloat = New DevExpress.Utils.PointFloat(12.5!, 108.0!)
            Me.XrLine_reportHeaderLine.Name = "XrLine_reportHeaderLine"
            Me.XrLine_reportHeaderLine.SizeF = New System.Drawing.SizeF(777.5!, 2.09!)
            Me.XrLine_reportHeaderLine.StyleName = "XrControlStyle_HeaderLine"
            '
            'XrLabel_pageHeader_Title
            '
            Me.XrLabel_pageHeader_Title.LocationFloat = New DevExpress.Utils.PointFloat(10.0!, 10.0!)
            Me.XrLabel_pageHeader_Title.Multiline = True
            Me.XrLabel_pageHeader_Title.Name = "XrLabel_pageHeader_Title"
            Me.XrLabel_pageHeader_Title.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_pageHeader_Title.SizeF = New System.Drawing.SizeF(564.5833!, 98.0!)
            Me.XrLabel_pageHeader_Title.StyleName = "XrControlStyle_HeaderText"
            '
            'XrLine_pageHeaderLine
            '
            Me.XrLine_pageHeaderLine.LineWidth = 2
            Me.XrLine_pageHeaderLine.LocationFloat = New DevExpress.Utils.PointFloat(12.5!, 108.0!)
            Me.XrLine_pageHeaderLine.Name = "XrLine_pageHeaderLine"
            Me.XrLine_pageHeaderLine.SizeF = New System.Drawing.SizeF(777.5!, 2.09!)
            Me.XrLine_pageHeaderLine.StyleName = "XrControlStyle_HeaderLine"
            '
            'Template_2
            '
            Me.Bands.AddRange(New DevExpress.XtraReports.UI.Band() {Me.TopMarginBand1, Me.Detail, Me.BottomMarginBand1, Me.ReportHeader, Me.PageHeader})
            Me.Version = "11.2"
            CType(Me, System.ComponentModel.ISupportInitialize).EndInit()

        End Sub

        Protected XrLabel_ReportTitle As DevExpress.XtraReports.UI.XRLabel
        Protected XrLine_reportHeaderLine As DevExpress.XtraReports.UI.XRLine
        Protected XrLine_pageHeaderLine As DevExpress.XtraReports.UI.XRLine
        Protected XrLabel_pageHeader_Title As DevExpress.XtraReports.UI.XRLabel
    End Class
End Namespace