﻿Namespace Documents
    Public Class ActionItems_1
        Public Sub New()
            MyBase.New()
            InitializeComponent()
            ' AddHandler BeforePrint, AddressOf ActionItems_1_BeforePrint
        End Sub

        Private Class StringItem
            Public Property Description As String
            Public Property SectionID As Int32
        End Class

        Private Sub ActionItems_1_BeforePrint(sender As Object, e As System.Drawing.Printing.PrintEventArgs)

            Dim rpt As DevExpress.XtraReports.UI.XtraReport = TryCast(sender, DevExpress.XtraReports.UI.XtraReport)
            If rpt Is Nothing Then
                Return
            End If

            ' Find the client from our own parameter value. The base report sets it when the client is supplied.
            Dim ClientID As Int32 = -1
            Dim parm As DevExpress.XtraReports.Parameters.Parameter = rpt.Parameters("ParameterClient")
            If parm IsNot Nothing Then
                ClientID = Convert.ToInt32(parm.Value)
            End If

            If ClientID < 0 Then
                Return
            End If

            Dim PlanID As System.Int32 = 0
            Try
                Using cn As New System.Data.SqlClient.SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
                    cn.Open()

                    Using cmd As New System.Data.SqlClient.SqlCommand()
                        cmd.Connection = cn
                        cmd.CommandType = System.Data.CommandType.Text
                        cmd.CommandText = "SELECT TOP 1 [action_plan] FROM [action_plans] WHERE [client]=@client ORDER BY [date_created] DESC"
                        cmd.Parameters.Add("@client", System.Data.SqlDbType.Int).Value = ClientID
                        Dim obj = cmd.ExecuteScalar()
                        If obj IsNot Nothing AndAlso Not System.Object.Equals(System.DBNull.Value, obj) Then
                            PlanID = Convert.ToInt32(obj)
                        End If
                    End Using

                    If PlanID < 1 Then
                        Return
                    End If

                    ' List of the items for the action plan
                    Dim items As New System.Collections.Generic.List(Of StringItem)
                    rpt.DataSource = items

                    ' Retrieve the list of items in the action plan.
                    Using cmd As New System.Data.SqlClient.SqlCommand()
                        cmd.Connection = cn
                        cmd.CommandText = "select i.description as description, i.item_group from action_items i inner join client_action_items s on i.action_item = s.action_item where s.action_plan = @ActionPlan AND s.checked = 1 union all select i.description, i.item_group as description from action_items_other i inner join client_action_items s on i.action_item_other = s.action_item where s.action_plan = @ActionPlan AND s.checked = 1 order by 2, 1"
                        cmd.Parameters.Add("@ActionPlan", System.Data.SqlDbType.Int).Value = PlanID
                        Using rd As System.Data.SqlClient.SqlDataReader = cmd.ExecuteReader(System.Data.CommandBehavior.CloseConnection Or System.Data.CommandBehavior.SingleResult)
                            While rd.Read()
                                If rd.IsDBNull(0) Then
                                    Continue While
                                End If

                                Dim strItem As String = rd.GetString(0).Trim()
                                If String.IsNullOrWhiteSpace(strItem) Then
                                    Continue While
                                End If

                                items.Add(New StringItem() With {.Description = strItem, .SectionID = rd.GetInt32(1)})
                            End While
                        End Using
                    End Using
                End Using

            Catch ex As System.Data.SqlClient.SqlException
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex)
            End Try
        End Sub

        Private Sub XrLabel_GroupHeader_BeforePrint(sender As Object, e As System.Drawing.Printing.PrintEventArgs)
            Dim record As StringItem = TryCast(GetCurrentRow(), StringItem)
            If record Is Nothing Then
                Return
            End If

            Select Case record.SectionID
                Case 1
                    XrLabel_GroupHeader.Text = "ACTION ITEMS"

                Case 2
                    XrLabel_GroupHeader.Text = "ITEMS TO INCREASE INCOME"

                Case 3
                    XrLabel_GroupHeader.Text = "ITEMS TO REDUCE EXPENSES"

                Case 4
                    XrLabel_GroupHeader.Text = "ITEMS REQUIRED FOR DMP"

                Case Else
            End Select
        End Sub
    End Class
End Namespace
