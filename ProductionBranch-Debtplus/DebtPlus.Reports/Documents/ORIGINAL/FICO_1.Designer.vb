﻿Namespace Documents
    <Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
    Partial Class FICO_1
        Inherits Template_1

        'XtraReport overrides dispose to clean up the component list.
        <System.Diagnostics.DebuggerNonUserCode()> _
        Protected Overrides Sub Dispose(ByVal disposing As Boolean)
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
            MyBase.Dispose(disposing)
        End Sub

        'Required by the Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Designer
        'It can be modified using the Designer.  
        'Do not modify it using the code editor.
        <System.Diagnostics.DebuggerStepThrough()> _
        Private Sub InitializeComponent()
            Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FICO_1))
            Me.XrPanel1 = New DevExpress.XtraReports.UI.XRPanel()
            Me.XrLabel2 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel1 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_long_description = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_description = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel3 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel4 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_client_name = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel6 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrTable1 = New DevExpress.XtraReports.UI.XRTable()
            Me.XrTableRow1 = New DevExpress.XtraReports.UI.XRTableRow()
            Me.XrTableCell2 = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableCell_pull_date = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableRow2 = New DevExpress.XtraReports.UI.XRTableRow()
            Me.XrTableCell1 = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableCell4 = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableRow3 = New DevExpress.XtraReports.UI.XRTableRow()
            Me.XrTableCell5 = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableCell_agency = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrPageInfo1 = New DevExpress.XtraReports.UI.XRPageInfo()
            Me.XrPanel2 = New DevExpress.XtraReports.UI.XRPanel()
            Me.XrLabel_fico_score = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel9 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel8 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel7 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel11 = New DevExpress.XtraReports.UI.XRLabel()
            Me.GroupHeader1 = New DevExpress.XtraReports.UI.GroupHeaderBand()
            Me.GroupFooter1 = New DevExpress.XtraReports.UI.GroupFooterBand()
            Me.XrRichText1 = New DevExpress.XtraReports.UI.XRRichText()
            CType(Me.XrTable1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.XrRichText1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
            '
            'PageHeader
            '
            Me.PageHeader.Visible = True
            '
            'Detail
            '
            Me.Detail.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrPanel1})
            Me.Detail.HeightF = 182.2917!
            '
            'BottomMarginBand1
            '
            Me.BottomMarginBand1.HeightF = 0.0!
            '
            'XrPanel1
            '
            Me.XrPanel1.Borders = CType((((DevExpress.XtraPrinting.BorderSide.Left Or DevExpress.XtraPrinting.BorderSide.Top) _
                Or DevExpress.XtraPrinting.BorderSide.Right) _
                Or DevExpress.XtraPrinting.BorderSide.Bottom), DevExpress.XtraPrinting.BorderSide)
            Me.XrPanel1.BorderWidth = 2
            Me.XrPanel1.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel2, Me.XrLabel1, Me.XrLabel_long_description, Me.XrLabel_description})
            Me.XrPanel1.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
            Me.XrPanel1.Name = "XrPanel1"
            Me.XrPanel1.SizeF = New System.Drawing.SizeF(799.9999!, 177.0833!)
            Me.XrPanel1.StylePriority.UseBorders = False
            Me.XrPanel1.StylePriority.UseBorderWidth = False
            '
            'XrLabel2
            '
            Me.XrLabel2.BackColor = System.Drawing.Color.LightCyan
            Me.XrLabel2.Borders = DevExpress.XtraPrinting.BorderSide.None
            Me.XrLabel2.Font = New System.Drawing.Font("Calibri", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.XrLabel2.LocationFloat = New DevExpress.Utils.PointFloat(3.0!, 88.0!)
            Me.XrLabel2.Name = "XrLabel2"
            Me.XrLabel2.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel2.SizeF = New System.Drawing.SizeF(794.0!, 23.0!)
            Me.XrLabel2.StylePriority.UseBackColor = False
            Me.XrLabel2.StylePriority.UseBorders = False
            Me.XrLabel2.StylePriority.UseFont = False
            Me.XrLabel2.Text = "Keep in mind"
            '
            'XrLabel1
            '
            Me.XrLabel1.AutoWidth = True
            Me.XrLabel1.BackColor = System.Drawing.Color.LightCyan
            Me.XrLabel1.Borders = DevExpress.XtraPrinting.BorderSide.None
            Me.XrLabel1.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "tip")})
            Me.XrLabel1.LocationFloat = New DevExpress.Utils.PointFloat(3.0!, 111.0!)
            Me.XrLabel1.Multiline = True
            Me.XrLabel1.Name = "XrLabel1"
            Me.XrLabel1.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel1.SizeF = New System.Drawing.SizeF(794.0!, 62.0!)
            Me.XrLabel1.StylePriority.UseBackColor = False
            Me.XrLabel1.StylePriority.UseBorders = False
            '
            'XrLabel_long_description
            '
            Me.XrLabel_long_description.AutoWidth = True
            Me.XrLabel_long_description.BackColor = System.Drawing.Color.PowderBlue
            Me.XrLabel_long_description.Borders = DevExpress.XtraPrinting.BorderSide.None
            Me.XrLabel_long_description.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "long_description")})
            Me.XrLabel_long_description.LocationFloat = New DevExpress.Utils.PointFloat(3.0!, 26.0!)
            Me.XrLabel_long_description.Multiline = True
            Me.XrLabel_long_description.Name = "XrLabel_long_description"
            Me.XrLabel_long_description.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_long_description.SizeF = New System.Drawing.SizeF(794.0!, 62.0!)
            Me.XrLabel_long_description.StylePriority.UseBackColor = False
            Me.XrLabel_long_description.StylePriority.UseBorders = False
            '
            'XrLabel_description
            '
            Me.XrLabel_description.BackColor = System.Drawing.Color.PowderBlue
            Me.XrLabel_description.Borders = DevExpress.XtraPrinting.BorderSide.None
            Me.XrLabel_description.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "description")})
            Me.XrLabel_description.Font = New System.Drawing.Font("Calibri", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.XrLabel_description.LocationFloat = New DevExpress.Utils.PointFloat(3.0!, 3.0!)
            Me.XrLabel_description.Name = "XrLabel_description"
            Me.XrLabel_description.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_description.SizeF = New System.Drawing.SizeF(794.0!, 23.0!)
            Me.XrLabel_description.StylePriority.UseBackColor = False
            Me.XrLabel_description.StylePriority.UseBorders = False
            Me.XrLabel_description.StylePriority.UseFont = False
            '
            'XrLabel3
            '
            Me.XrLabel3.Font = New System.Drawing.Font("Calibri", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.XrLabel3.ForeColor = System.Drawing.Color.Blue
            Me.XrLabel3.LocationFloat = New DevExpress.Utils.PointFloat(275.0!, 0.0!)
            Me.XrLabel3.Name = "XrLabel3"
            Me.XrLabel3.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel3.SizeF = New System.Drawing.SizeF(252.0833!, 23.0!)
            Me.XrLabel3.StylePriority.UseFont = False
            Me.XrLabel3.StylePriority.UseForeColor = False
            Me.XrLabel3.StylePriority.UseTextAlignment = False
            Me.XrLabel3.Text = "FICO© Score Summary Report"
            Me.XrLabel3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
            '
            'XrLabel4
            '
            Me.XrLabel4.Font = New System.Drawing.Font("Calibri", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.XrLabel4.LocationFloat = New DevExpress.Utils.PointFloat(3.000005!, 33.41668!)
            Me.XrLabel4.Name = "XrLabel4"
            Me.XrLabel4.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 8, 0, 0, 100.0!)
            Me.XrLabel4.SizeF = New System.Drawing.SizeF(471.6667!, 23.0!)
            Me.XrLabel4.StylePriority.UseFont = False
            Me.XrLabel4.StylePriority.UsePadding = False
            Me.XrLabel4.StylePriority.UseTextAlignment = False
            Me.XrLabel4.Text = "This FICO© Score Summary is provided for the exclusive use of:"
            Me.XrLabel4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel_client_name
            '
            Me.XrLabel_client_name.BackColor = System.Drawing.Color.LightCyan
            Me.XrLabel_client_name.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "client_name")})
            Me.XrLabel_client_name.Font = New System.Drawing.Font("Calibri", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.XrLabel_client_name.LocationFloat = New DevExpress.Utils.PointFloat(474.6667!, 33.41667!)
            Me.XrLabel_client_name.Name = "XrLabel_client_name"
            Me.XrLabel_client_name.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_client_name.SizeF = New System.Drawing.SizeF(308.3333!, 23.0!)
            Me.XrLabel_client_name.StylePriority.UseBackColor = False
            Me.XrLabel_client_name.StylePriority.UseFont = False
            Me.XrLabel_client_name.StylePriority.UseTextAlignment = False
            Me.XrLabel_client_name.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
            '
            'XrLabel6
            '
            Me.XrLabel6.Font = New System.Drawing.Font("Calibri", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.XrLabel6.LocationFloat = New DevExpress.Utils.PointFloat(406.2498!, 56.41664!)
            Me.XrLabel6.Name = "XrLabel6"
            Me.XrLabel6.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 8, 0, 0, 100.0!)
            Me.XrLabel6.SizeF = New System.Drawing.SizeF(68.4169!, 23.0!)
            Me.XrLabel6.StylePriority.UseFont = False
            Me.XrLabel6.StylePriority.UsePadding = False
            Me.XrLabel6.StylePriority.UseTextAlignment = False
            Me.XrLabel6.Text = "on:"
            Me.XrLabel6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrTable1
            '
            Me.XrTable1.LocationFloat = New DevExpress.Utils.PointFloat(340.2917!, 92.20839!)
            Me.XrTable1.Name = "XrTable1"
            Me.XrTable1.Rows.AddRange(New DevExpress.XtraReports.UI.XRTableRow() {Me.XrTableRow1, Me.XrTableRow2, Me.XrTableRow3})
            Me.XrTable1.SizeF = New System.Drawing.SizeF(442.7083!, 75.0!)
            '
            'XrTableRow1
            '
            Me.XrTableRow1.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.XrTableCell2, Me.XrTableCell_pull_date})
            Me.XrTableRow1.Name = "XrTableRow1"
            Me.XrTableRow1.Weight = 1.0R
            '
            'XrTableCell2
            '
            Me.XrTableCell2.Name = "XrTableCell2"
            Me.XrTableCell2.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 8, 0, 0, 100.0!)
            Me.XrTableCell2.StylePriority.UsePadding = False
            Me.XrTableCell2.StylePriority.UseTextAlignment = False
            Me.XrTableCell2.Text = "Your FICO© Score was pulled on:"
            Me.XrTableCell2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
            Me.XrTableCell2.Weight = 1.752941302984435R
            '
            'XrTableCell_pull_date
            '
            Me.XrTableCell_pull_date.BackColor = System.Drawing.Color.LightCyan
            Me.XrTableCell_pull_date.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "FICO_pull_date", "{0:d}")})
            Me.XrTableCell_pull_date.Font = New System.Drawing.Font("Calibri", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.XrTableCell_pull_date.Name = "XrTableCell_pull_date"
            Me.XrTableCell_pull_date.StylePriority.UseBackColor = False
            Me.XrTableCell_pull_date.StylePriority.UseFont = False
            Me.XrTableCell_pull_date.StylePriority.UseTextAlignment = False
            Me.XrTableCell_pull_date.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
            Me.XrTableCell_pull_date.Weight = 1.247058697015565R
            '
            'XrTableRow2
            '
            Me.XrTableRow2.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.XrTableCell1, Me.XrTableCell4})
            Me.XrTableRow2.Name = "XrTableRow2"
            Me.XrTableRow2.Weight = 1.0R
            '
            'XrTableCell1
            '
            Me.XrTableCell1.Name = "XrTableCell1"
            Me.XrTableCell1.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 8, 0, 0, 100.0!)
            Me.XrTableCell1.StylePriority.UsePadding = False
            Me.XrTableCell1.StylePriority.UseTextAlignment = False
            Me.XrTableCell1.Text = "FICO© Score Version:"
            Me.XrTableCell1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
            Me.XrTableCell1.Weight = 1.752941302984435R
            '
            'XrTableCell4
            '
            Me.XrTableCell4.BackColor = System.Drawing.Color.LightCyan
            Me.XrTableCell4.Font = New System.Drawing.Font("Calibri", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.XrTableCell4.Name = "XrTableCell4"
            Me.XrTableCell4.StylePriority.UseBackColor = False
            Me.XrTableCell4.StylePriority.UseFont = False
            Me.XrTableCell4.StylePriority.UseTextAlignment = False
            Me.XrTableCell4.Text = "FICO© Score 8"
            Me.XrTableCell4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
            Me.XrTableCell4.Weight = 1.247058697015565R
            '
            'XrTableRow3
            '
            Me.XrTableRow3.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.XrTableCell5, Me.XrTableCell_agency})
            Me.XrTableRow3.Name = "XrTableRow3"
            Me.XrTableRow3.Weight = 1.0R
            '
            'XrTableCell5
            '
            Me.XrTableCell5.Name = "XrTableCell5"
            Me.XrTableCell5.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 8, 0, 0, 100.0!)
            Me.XrTableCell5.StylePriority.UsePadding = False
            Me.XrTableCell5.StylePriority.UseTextAlignment = False
            Me.XrTableCell5.Text = "FICO© Score is based on data from:"
            Me.XrTableCell5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
            Me.XrTableCell5.Weight = 1.752941302984435R
            '
            'XrTableCell_agency
            '
            Me.XrTableCell_agency.BackColor = System.Drawing.Color.LightCyan
            Me.XrTableCell_agency.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "CreditAgency")})
            Me.XrTableCell_agency.Font = New System.Drawing.Font("Calibri", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.XrTableCell_agency.Name = "XrTableCell_agency"
            Me.XrTableCell_agency.StylePriority.UseBackColor = False
            Me.XrTableCell_agency.StylePriority.UseFont = False
            Me.XrTableCell_agency.StylePriority.UseTextAlignment = False
            Me.XrTableCell_agency.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
            Me.XrTableCell_agency.Weight = 1.247058697015565R
            '
            'XrPageInfo1
            '
            Me.XrPageInfo1.BackColor = System.Drawing.Color.LightCyan
            Me.XrPageInfo1.Font = New System.Drawing.Font("Calibri", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.XrPageInfo1.Format = "{0:d}"
            Me.XrPageInfo1.LocationFloat = New DevExpress.Utils.PointFloat(474.6667!, 56.41664!)
            Me.XrPageInfo1.Name = "XrPageInfo1"
            Me.XrPageInfo1.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrPageInfo1.PageInfo = DevExpress.XtraPrinting.PageInfo.DateTime
            Me.XrPageInfo1.SizeF = New System.Drawing.SizeF(308.3333!, 23.0!)
            Me.XrPageInfo1.StylePriority.UseBackColor = False
            Me.XrPageInfo1.StylePriority.UseFont = False
            Me.XrPageInfo1.StylePriority.UseTextAlignment = False
            Me.XrPageInfo1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
            '
            'XrPanel2
            '
            Me.XrPanel2.BackColor = System.Drawing.Color.SteelBlue
            Me.XrPanel2.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel_fico_score, Me.XrLabel9, Me.XrLabel8, Me.XrLabel7})
            Me.XrPanel2.LocationFloat = New DevExpress.Utils.PointFloat(9.999998!, 67.20839!)
            Me.XrPanel2.Name = "XrPanel2"
            Me.XrPanel2.SizeF = New System.Drawing.SizeF(166.0!, 100.0!)
            Me.XrPanel2.StylePriority.UseBackColor = False
            '
            'XrLabel_fico_score
            '
            Me.XrLabel_fico_score.BackColor = System.Drawing.Color.White
            Me.XrLabel_fico_score.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "FICO_Score", "{0:000}")})
            Me.XrLabel_fico_score.Font = New System.Drawing.Font("Calibri", 24.0!, System.Drawing.FontStyle.Bold)
            Me.XrLabel_fico_score.ForeColor = System.Drawing.Color.SteelBlue
            Me.XrLabel_fico_score.LocationFloat = New DevExpress.Utils.PointFloat(40.0!, 45.29161!)
            Me.XrLabel_fico_score.Name = "XrLabel_fico_score"
            Me.XrLabel_fico_score.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_fico_score.SizeF = New System.Drawing.SizeF(90.0!, 46.0!)
            Me.XrLabel_fico_score.StylePriority.UseBackColor = False
            Me.XrLabel_fico_score.StylePriority.UseFont = False
            Me.XrLabel_fico_score.StylePriority.UseForeColor = False
            Me.XrLabel_fico_score.StylePriority.UseTextAlignment = False
            Me.XrLabel_fico_score.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
            '
            'XrLabel9
            '
            Me.XrLabel9.Font = New System.Drawing.Font("Calibri", 6.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.XrLabel9.ForeColor = System.Drawing.Color.White
            Me.XrLabel9.LocationFloat = New DevExpress.Utils.PointFloat(8.0!, 23.0!)
            Me.XrLabel9.Name = "XrLabel9"
            Me.XrLabel9.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel9.SizeF = New System.Drawing.SizeF(150.0!, 13.625!)
            Me.XrLabel9.StylePriority.UseFont = False
            Me.XrLabel9.StylePriority.UseForeColor = False
            Me.XrLabel9.StylePriority.UseTextAlignment = False
            Me.XrLabel9.Text = "THE SCORE LENDERS USE"
            Me.XrLabel9.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter
            '
            'XrLabel8
            '
            Me.XrLabel8.Font = New System.Drawing.Font("Calibri", 12.0!)
            Me.XrLabel8.ForeColor = System.Drawing.Color.White
            Me.XrLabel8.LocationFloat = New DevExpress.Utils.PointFloat(83.00001!, 0.0!)
            Me.XrLabel8.Name = "XrLabel8"
            Me.XrLabel8.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel8.SizeF = New System.Drawing.SizeF(75.0!, 23.0!)
            Me.XrLabel8.StylePriority.UseFont = False
            Me.XrLabel8.StylePriority.UseForeColor = False
            Me.XrLabel8.Text = "SCORE"
            '
            'XrLabel7
            '
            Me.XrLabel7.Font = New System.Drawing.Font("Calibri", 12.0!, System.Drawing.FontStyle.Bold)
            Me.XrLabel7.ForeColor = System.Drawing.Color.White
            Me.XrLabel7.LocationFloat = New DevExpress.Utils.PointFloat(8.0!, 0.0!)
            Me.XrLabel7.Name = "XrLabel7"
            Me.XrLabel7.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel7.SizeF = New System.Drawing.SizeF(75.0!, 23.0!)
            Me.XrLabel7.StylePriority.UseFont = False
            Me.XrLabel7.StylePriority.UseForeColor = False
            Me.XrLabel7.StylePriority.UseTextAlignment = False
            Me.XrLabel7.Text = "FICO©"
            Me.XrLabel7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
            '
            'XrLabel11
            '
            Me.XrLabel11.Font = New System.Drawing.Font("Calibri", 12.0!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Underline), System.Drawing.FontStyle))
            Me.XrLabel11.ForeColor = System.Drawing.Color.RoyalBlue
            Me.XrLabel11.LocationFloat = New DevExpress.Utils.PointFloat(9.999998!, 180.0417!)
            Me.XrLabel11.Name = "XrLabel11"
            Me.XrLabel11.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel11.SizeF = New System.Drawing.SizeF(358.3333!, 23.00002!)
            Me.XrLabel11.StylePriority.UseFont = False
            Me.XrLabel11.StylePriority.UseForeColor = False
            Me.XrLabel11.Text = "Key factors impacting your personal FICO© Score"
            '
            'GroupHeader1
            '
            Me.GroupHeader1.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel11, Me.XrPanel2, Me.XrPageInfo1, Me.XrTable1, Me.XrLabel6, Me.XrLabel_client_name, Me.XrLabel4, Me.XrLabel3})
            Me.GroupHeader1.GroupFields.AddRange(New DevExpress.XtraReports.UI.GroupField() {New DevExpress.XtraReports.UI.GroupField("person", DevExpress.XtraReports.UI.XRColumnSortOrder.Ascending)})
            Me.GroupHeader1.GroupUnion = DevExpress.XtraReports.UI.GroupUnion.WithFirstDetail
            Me.GroupHeader1.HeightF = 213.0417!
            Me.GroupHeader1.Name = "GroupHeader1"
            Me.GroupHeader1.PageBreak = DevExpress.XtraReports.UI.PageBreak.BeforeBand
            '
            'GroupFooter1
            '
            Me.GroupFooter1.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrRichText1})
            Me.GroupFooter1.HeightF = 757.7084!
            Me.GroupFooter1.Name = "GroupFooter1"
            '
            'XrRichText1
            '
            Me.XrRichText1.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
            Me.XrRichText1.Name = "XrRichText1"
            Me.XrRichText1.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 2, 2, 100.0!)
            Me.XrRichText1.SerializableRtfString = resources.GetString("XrRichText1.SerializableRtfString")
            Me.XrRichText1.SizeF = New System.Drawing.SizeF(797.0!, 757.7084!)
            Me.XrRichText1.StylePriority.UsePadding = False
            '
            'FICO_1
            '
            Me.Bands.AddRange(New DevExpress.XtraReports.UI.Band() {Me.TopMarginBand1, Me.Detail, Me.BottomMarginBand1, Me.ReportHeader, Me.PageHeader, Me.GroupHeader1, Me.GroupFooter1})
            Me.Margins = New System.Drawing.Printing.Margins(25, 25, 25, 0)
            Me.ReportPrintOptions.DetailCount = 1
            Me.ReportPrintOptions.DetailCountAtDesignTime = 1
            Me.RequestParameters = False
            Me.ScriptLanguage = DevExpress.XtraReports.ScriptLanguage.VisualBasic
            Me.Scripts.OnBeforePrint = "FICO_1_BeforePrint"
            Me.ScriptsSource = resources.GetString("$this.ScriptsSource")
            Me.StyleSheet.AddRange(New DevExpress.XtraReports.UI.XRControlStyle() {Me.XrControlStyle_HeaderText, Me.XrControlStyle_HeaderLine, Me.XrControlStyle_HeaderBox1, Me.XrControlStyle_HeaderBox2, Me.XrControlStyle_HeaderBox3, Me.XrControlStyle_Report_Column, Me.XrControlStyle_Report_Group, Me.XrControlStyle_Report_Detail, Me.XrControlStyle_Report_Total, Me.XrControlStyle_Footer, Me.XrControlStyle_HeaderSubTitle})
            Me.Version = "11.2"
            Me.Controls.SetChildIndex(Me.GroupFooter1, 0)
            Me.Controls.SetChildIndex(Me.GroupHeader1, 0)
            Me.Controls.SetChildIndex(Me.PageHeader, 0)
            Me.Controls.SetChildIndex(Me.ReportHeader, 0)
            Me.Controls.SetChildIndex(Me.BottomMarginBand1, 0)
            Me.Controls.SetChildIndex(Me.Detail, 0)
            Me.Controls.SetChildIndex(Me.TopMarginBand1, 0)
            CType(Me.XrTable1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.XrRichText1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me, System.ComponentModel.ISupportInitialize).EndInit()

        End Sub

        Protected WithEvents XrPanel1 As DevExpress.XtraReports.UI.XRPanel
        Protected WithEvents XrLabel2 As DevExpress.XtraReports.UI.XRLabel
        Protected WithEvents XrLabel1 As DevExpress.XtraReports.UI.XRLabel
        Protected WithEvents XrLabel_long_description As DevExpress.XtraReports.UI.XRLabel
        Protected WithEvents XrLabel_description As DevExpress.XtraReports.UI.XRLabel
        Protected WithEvents XrPageInfo1 As DevExpress.XtraReports.UI.XRPageInfo
        Protected WithEvents XrTable1 As DevExpress.XtraReports.UI.XRTable
        Protected WithEvents XrTableRow1 As DevExpress.XtraReports.UI.XRTableRow
        Protected WithEvents XrTableCell2 As DevExpress.XtraReports.UI.XRTableCell
        Protected WithEvents XrTableCell_pull_date As DevExpress.XtraReports.UI.XRTableCell
        Protected WithEvents XrTableRow2 As DevExpress.XtraReports.UI.XRTableRow
        Protected WithEvents XrTableCell1 As DevExpress.XtraReports.UI.XRTableCell
        Protected WithEvents XrTableCell4 As DevExpress.XtraReports.UI.XRTableCell
        Protected WithEvents XrTableRow3 As DevExpress.XtraReports.UI.XRTableRow
        Protected WithEvents XrTableCell5 As DevExpress.XtraReports.UI.XRTableCell
        Protected WithEvents XrTableCell_agency As DevExpress.XtraReports.UI.XRTableCell
        Protected WithEvents XrLabel6 As DevExpress.XtraReports.UI.XRLabel
        Protected WithEvents XrLabel_client_name As DevExpress.XtraReports.UI.XRLabel
        Protected WithEvents XrLabel4 As DevExpress.XtraReports.UI.XRLabel
        Protected WithEvents XrLabel3 As DevExpress.XtraReports.UI.XRLabel
        Protected WithEvents XrLabel11 As DevExpress.XtraReports.UI.XRLabel
        Protected WithEvents XrPanel2 As DevExpress.XtraReports.UI.XRPanel
        Protected WithEvents XrLabel_fico_score As DevExpress.XtraReports.UI.XRLabel
        Protected WithEvents XrLabel9 As DevExpress.XtraReports.UI.XRLabel
        Protected WithEvents XrLabel8 As DevExpress.XtraReports.UI.XRLabel
        Protected WithEvents XrLabel7 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents GroupHeader1 As DevExpress.XtraReports.UI.GroupHeaderBand
        Friend WithEvents GroupFooter1 As DevExpress.XtraReports.UI.GroupFooterBand
        Friend WithEvents XrRichText1 As DevExpress.XtraReports.UI.XRRichText
    End Class
End Namespace
