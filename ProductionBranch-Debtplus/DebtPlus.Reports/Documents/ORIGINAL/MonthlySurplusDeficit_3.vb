﻿Namespace Documents
    Public Class MonthlySurplusDeficit_3
        Public Sub New()
            MyBase.New()
            InitializeComponent()
            ' AddHandler BeforePrint, AddressOf MonthlySurplusDeficit_BeforePrint
        End Sub

        Private Class ReportInformation
            Public Class itemData

                ' Data from the stored procedure inputs. These are not reported.
                Public Property payroll_1 As Decimal = 0D
                Public Property payroll_2 As Decimal = 0D
                Public Property assets As Decimal = 0D
                Public Property v2_plan_debt_payment As Decimal = 0D
                Public Property v2_self_debt_payment As Decimal = 0D
                Public Property v2_self_expense As Decimal = 0D
                Public Property v2_plan_expense As Decimal = 0D

                Public ReadOnly Property person_1_net As Decimal
                    Get
                        Return payroll_1
                    End Get
                End Property

                Public ReadOnly Property person_2_net As Decimal
                    Get
                        Return payroll_2
                    End Get
                End Property

                Public ReadOnly Property asset As Decimal
                    Get
                        Return assets
                    End Get
                End Property

                Public Property debt_payment As Decimal = 0D
                Public Property expense As Decimal = 0D
                Public Property other_debt As Decimal = 0D

                Public ReadOnly Property total_assets As Decimal
                    Get
                        Return person_1_net + person_2_net + asset
                    End Get
                End Property

                Public ReadOnly Property expense_total As Decimal
                    Get
                        Return total_assets - expense - other_debt
                    End Get
                End Property

                Public ReadOnly Property total As Decimal
                    Get
                        Return expense_total - debt_payment
                    End Get
                End Property
            End Class

            ' Self and plan figures
            Private m_self As New itemData()
            Public ReadOnly Property self As itemData
                Get
                    Return m_self
                End Get
            End Property

            Private m_plan As New itemData()
            Public ReadOnly Property plan As itemData
                Get
                    Return m_plan
                End Get
            End Property

            Public Sub SetBottomLine(cn As System.Data.SqlClient.SqlConnection, clientID As Int32)
                Using cmd As New System.Data.SqlClient.SqlCommand()
                    cmd.Connection = cn
                    cmd.CommandText = "rpt_PrintClient_BottomLine"
                    cmd.CommandType = System.Data.CommandType.StoredProcedure
                    cmd.Parameters.Add("@client", System.Data.SqlDbType.Int).Value = clientID

                    Using rd As System.Data.SqlClient.SqlDataReader = cmd.ExecuteReader()
                        If rd.Read() Then
                            For ordinal As Int32 = 0 To rd.FieldCount - 1
                                If rd.IsDBNull(ordinal) Then
                                    Continue For
                                End If
                                Dim columnName As String = rd.GetName(ordinal)
                                Dim value As Decimal = rd.GetDecimal(ordinal)

                                Dim t As System.Type = GetType(itemData)
                                Dim pi As System.Reflection.PropertyInfo = t.GetProperty(columnName)
                                If pi IsNot Nothing Then
                                    pi.SetValue(self, value, Nothing)
                                    pi.SetValue(plan, value, Nothing)
                                End If
                            Next
                        End If
                        rd.Close()
                    End Using
                End Using

                ' Adjust the debt payments from the input fields
                self.debt_payment = self.v2_self_debt_payment
                plan.debt_payment = plan.v2_plan_debt_payment

                ' Adjust the expenses from the input fields
                plan.expense = plan.v2_plan_expense
                self.expense = self.v2_self_expense
            End Sub
        End Class

        Private Sub MonthlySurplusDeficit_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs)
            Dim rpt As DevExpress.XtraReports.UI.XtraReport = TryCast(sender, DevExpress.XtraReports.UI.XtraReport)
            If rpt Is Nothing Then
                Return
            End If

            Dim MasterRpt As DebtPlus.Interfaces.Client.IClient = TryCast(rpt.MasterReport, DebtPlus.Interfaces.Client.IClient)
            Dim ClientID As Int32 = -1
            If MasterRpt IsNot Nothing Then
                ClientID = MasterRpt.ClientId
            End If

            If ClientID < 0 Then
                Return
            End If

            Dim sqlInfo As DebtPlus.LINQ.SQLInfoClass = DebtPlus.LINQ.SQLInfoClass.getDefault()

            Try
                Dim record As New ReportInformation()
                Using cn As New System.Data.SqlClient.SqlConnection(sqlInfo.ConnectionString)
                    cn.Open()
                    record.SetBottomLine(cn, Client)
                    cn.Close()
                End Using

                ' Build the dataset for the report.
                Dim dataset As New System.Collections.Generic.List(Of ReportInformation)()
                dataset.Add(record)
                rpt.DataSource = dataset

            Catch ex As System.Data.SqlClient.SqlException
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex)
            End Try
        End Sub

    End Class
End Namespace
