﻿Namespace Documents
    Public Class CoverSheet
        Public Sub New()
            MyBase.New()
            InitializeComponent()
            ' AddHandler BeforePrint, AddressOf CoverSheet_BeforePrint
        End Sub

        Private Sub CoverSheet_BeforePrint(sender As Object, e As System.Drawing.Printing.PrintEventArgs)

            Dim rpt As DevExpress.XtraReports.UI.XtraReport = TryCast(sender, DevExpress.XtraReports.UI.XtraReport)
            If rpt Is Nothing Then
                Return
            End If

            Dim MasterRpt As DebtPlus.Interfaces.Client.IClient = TryCast(rpt.MasterReport, DebtPlus.Interfaces.Client.IClient)
            Dim ClientID As Int32 = -1
            If MasterRpt IsNot Nothing Then
                ClientID = MasterRpt.ClientId
            End If

            If ClientID < 0 Then
                Return
            End If

            Using cn As New System.Data.SqlClient.SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
                cn.Open()
                Using cmd As New System.Data.SqlClient.SqlCommand()
                    cmd.Connection = cn
                    cmd.CommandText = "SELECT [name],[addr1],[addr2],[addr3] FROM [view_client_address] WHERE [client]=@client"
                    cmd.CommandType = System.Data.CommandType.Text
                    cmd.Parameters.Add("@client", System.Data.SqlDbType.Int).Value = ClientID

                    Dim sb As New System.Text.StringBuilder()
                    Using rd As System.Data.SqlClient.SqlDataReader = cmd.ExecuteReader()
                        If rd.Read() Then

                            ' The client name goes into the bold section
                            If Not rd.IsDBNull(0) Then
                                XrLabel_name.Text = rd.GetString(0).Trim()
                            End If

                            ' The address information goes into the normal section
                            For idx As Int32 = 1 To 3
                                If rd.IsDBNull(idx) Then
                                    Continue For
                                End If

                                Dim strText As String = rd.GetString(idx).Trim()
                                If String.IsNullOrWhiteSpace(strText) Then
                                    Continue For
                                End If

                                sb.Append(System.Environment.NewLine)
                                sb.Append(strText)
                            Next
                        End If
                        rd.Close()
                    End Using

                    If sb.Length > 0 Then
                        sb.Remove(0, 2)
                    End If

                    XrLabel_client_address.Text = sb.ToString()
                End Using
            End Using
        End Sub
    End Class
End Namespace
