﻿Namespace Documents
    Public Class FICO_1

        Public Sub New()
            MyBase.New()
            InitializeComponent()
            'AddHandler BeforePrint, AddressOf FICO_1_BeforePrint
        End Sub

        ' Dataset to hold the results.
        Private ds As New System.Data.DataSet("ds")

        Private Sub FICO_1_BeforePrint(sender As Object, e As System.Drawing.Printing.PrintEventArgs)
            Dim rpt As DevExpress.XtraReports.UI.XtraReport = TryCast(sender, DevExpress.XtraReports.UI.XtraReport)
            If rpt Is Nothing Then
                Return
            End If

            Dim MasterRpt As DevExpress.XtraReports.UI.XtraReport = TryCast(rpt.MasterReport, DevExpress.XtraReports.UI.XtraReport)
            If MasterRpt Is Nothing Then
                Return
            End If

            Dim IMasterRpt As DebtPlus.Interfaces.Client.IClient = TryCast(MasterRpt, DebtPlus.Interfaces.Client.IClient)
            Dim ClientID As Int32 = -1
            If IMasterRpt IsNot Nothing Then
                ClientID = IMasterRpt.ClientId
            End If

            If ClientID < 0 Then
                Return
            End If

            ' Build a list of the reason for the fico score. This is our dataset.
            Using cn As New System.Data.SqlClient.SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
                cn.Open()
                Using cmd As New System.Data.SqlClient.SqlCommand()
                    cmd.Connection = cn
                    cmd.CommandText = _
"SELECT p.client, p.person, p.FICO_Score, p.FICO_pull_date, p.CreditAgency, dbo.format_normal_name(default,pn.first,pn.middle,pn.last,pn.suffix) as client_name, sr.description, sr.long_description, sr.tip, dbo.format_normal_name(default,con.first,default,con.last,con.suffix) as counselor_name " +
"from people p with (nolock) " +
"left outer join names pn with (nolock) on p.NameID = pn.Name " +
"left outer join PeopleFICOScoreReasons pr with (nolock) on p.person = pr.PersonID " +
"left outer join FICOScoreReasonCodes sr with (nolock) on pr.ScoreReasonID = sr.oID " +
"left outer join counselors co with (nolock) on co.person = suser_sname() " +
"left outer join names con with (nolock) on co.nameid = con.name " +
"where p.client = @client And isnull(p.fico_score, 0) > 0 " +
"order by p.relation, p.person"

                    cmd.CommandType = CommandType.Text
                    cmd.Parameters.Add("@client", SqlDbType.Int).Value = ClientID

                    Using da As New System.Data.SqlClient.SqlDataAdapter(cmd)
                        da.Fill(ds, "FicoScore")
                        rpt.DataSource = New System.Data.DataView(ds.Tables(0), String.Empty, "[relation],[person]", DataViewRowState.CurrentRows)
                    End Using
                End Using
            End Using
        End Sub
    End Class
End Namespace
