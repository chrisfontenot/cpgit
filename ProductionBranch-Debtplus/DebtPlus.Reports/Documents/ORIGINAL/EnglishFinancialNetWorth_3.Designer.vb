﻿Namespace Documents
    <Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
    Partial Class EnglishFinancialNetWorth_3
        Inherits Template_3

        'XtraReport overrides dispose to clean up the component list.
        <System.Diagnostics.DebuggerNonUserCode()> _
        Protected Overrides Sub Dispose(ByVal disposing As Boolean)
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
            MyBase.Dispose(disposing)
        End Sub

        'Required by the Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Designer
        'It can be modified using the Designer.  
        'Do not modify it using the code editor.
        <System.Diagnostics.DebuggerStepThrough()> _
        Private Sub InitializeComponent()
            Dim XrSummary1 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary()
            Dim XrSummary2 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary()
            Dim XrSummary3 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary()
            Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(EnglishFinancialNetWorth_3))
            Me.FormattingRule_l_ShowHeading = New DevExpress.XtraReports.UI.FormattingRule()
            Me.FormattingRule_l_HideDetails = New DevExpress.XtraReports.UI.FormattingRule()
            Me.FormattingRule_r_ShowHeading = New DevExpress.XtraReports.UI.FormattingRule()
            Me.FormattingRule_r_HideDetails = New DevExpress.XtraReports.UI.FormattingRule()
            Me.GroupHeader1 = New DevExpress.XtraReports.UI.GroupHeaderBand()
            Me.XrTable2 = New DevExpress.XtraReports.UI.XRTable()
            Me.XrTableRow2 = New DevExpress.XtraReports.UI.XRTableRow()
            Me.XrTableCell4 = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableCell5 = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableCell13 = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableCell6 = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableCell12 = New DevExpress.XtraReports.UI.XRTableCell()
            Me.GroupFooter1 = New DevExpress.XtraReports.UI.GroupFooterBand()
            Me.XrTable1 = New DevExpress.XtraReports.UI.XRTable()
            Me.XrTableRow1 = New DevExpress.XtraReports.UI.XRTableRow()
            Me.XrTableCell1 = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableCell_total_l_value = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableRow4 = New DevExpress.XtraReports.UI.XRTableRow()
            Me.XrTableCell7 = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableCell_total_r_value = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableRow5 = New DevExpress.XtraReports.UI.XRTableRow()
            Me.XrTableCell2 = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableCell_total_difference = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrLabel1 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrTable3 = New DevExpress.XtraReports.UI.XRTable()
            Me.XrTableRow3 = New DevExpress.XtraReports.UI.XRTableRow()
            Me.XrTableCell_l_description = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableCell_l_value = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableCell14 = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableCell_r_description = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableCell_r_value = New DevExpress.XtraReports.UI.XRTableCell()
            CType(Me.XrTable2, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.XrTable1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.XrTable3, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
            '
            'XrLabel_ReportTitle
            '
            Me.XrLabel_ReportTitle.Text = "FINANCIAL" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "NET WORTH"
            '
            'XrLabel_pageHeader_Title
            '
            Me.XrLabel_pageHeader_Title.Text = "FINANCIAL" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "NET WORTH (Continued)"
            '
            'PageHeader
            '
            Me.PageHeader.Visible = True
            '
            'Detail
            '
            Me.Detail.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrTable3})
            Me.Detail.HeightF = 23.0!
            '
            'FormattingRule_l_ShowHeading
            '
            Me.FormattingRule_l_ShowHeading.Condition = "[l_heading] == True"
            '
            '
            '
            Me.FormattingRule_l_ShowHeading.Formatting.BackColor = System.Drawing.Color.FromArgb(CType(CType(102, Byte), Integer), CType(CType(102, Byte), Integer), CType(CType(153, Byte), Integer))
            Me.FormattingRule_l_ShowHeading.Formatting.BorderColor = System.Drawing.Color.White
            Me.FormattingRule_l_ShowHeading.Formatting.Borders = DevExpress.XtraPrinting.BorderSide.Bottom
            Me.FormattingRule_l_ShowHeading.Formatting.BorderWidth = 1
            Me.FormattingRule_l_ShowHeading.Formatting.Font = New System.Drawing.Font("Calibri", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.FormattingRule_l_ShowHeading.Formatting.ForeColor = System.Drawing.Color.White
            Me.FormattingRule_l_ShowHeading.Name = "FormattingRule_l_ShowHeading"
            '
            'FormattingRule_l_HideDetails
            '
            Me.FormattingRule_l_HideDetails.Condition = "[l_heading] == True"
            '
            '
            '
            Me.FormattingRule_l_HideDetails.Formatting.BackColor = System.Drawing.Color.FromArgb(CType(CType(102, Byte), Integer), CType(CType(102, Byte), Integer), CType(CType(153, Byte), Integer))
            Me.FormattingRule_l_HideDetails.Formatting.BorderColor = System.Drawing.Color.White
            Me.FormattingRule_l_HideDetails.Formatting.Borders = DevExpress.XtraPrinting.BorderSide.Bottom
            Me.FormattingRule_l_HideDetails.Formatting.BorderWidth = 1
            Me.FormattingRule_l_HideDetails.Formatting.Font = New System.Drawing.Font("Calibri", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.FormattingRule_l_HideDetails.Formatting.ForeColor = System.Drawing.Color.FromArgb(CType(CType(102, Byte), Integer), CType(CType(102, Byte), Integer), CType(CType(153, Byte), Integer))
            Me.FormattingRule_l_HideDetails.Name = "FormattingRule_l_HideDetails"
            '
            'FormattingRule_r_ShowHeading
            '
            Me.FormattingRule_r_ShowHeading.Condition = "[r_heading] == True"
            '
            '
            '
            Me.FormattingRule_r_ShowHeading.Formatting.BackColor = System.Drawing.Color.FromArgb(CType(CType(102, Byte), Integer), CType(CType(102, Byte), Integer), CType(CType(153, Byte), Integer))
            Me.FormattingRule_r_ShowHeading.Formatting.BorderColor = System.Drawing.Color.White
            Me.FormattingRule_r_ShowHeading.Formatting.Borders = DevExpress.XtraPrinting.BorderSide.Bottom
            Me.FormattingRule_r_ShowHeading.Formatting.BorderWidth = 1
            Me.FormattingRule_r_ShowHeading.Formatting.Font = New System.Drawing.Font("Calibri", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.FormattingRule_r_ShowHeading.Formatting.ForeColor = System.Drawing.Color.White
            Me.FormattingRule_r_ShowHeading.Name = "FormattingRule_r_ShowHeading"
            '
            'FormattingRule_r_HideDetails
            '
            Me.FormattingRule_r_HideDetails.Condition = "[r_heading] == True"
            '
            '
            '
            Me.FormattingRule_r_HideDetails.Formatting.BackColor = System.Drawing.Color.FromArgb(CType(CType(102, Byte), Integer), CType(CType(102, Byte), Integer), CType(CType(153, Byte), Integer))
            Me.FormattingRule_r_HideDetails.Formatting.BorderColor = System.Drawing.Color.White
            Me.FormattingRule_r_HideDetails.Formatting.Borders = DevExpress.XtraPrinting.BorderSide.Bottom
            Me.FormattingRule_r_HideDetails.Formatting.BorderWidth = 1
            Me.FormattingRule_r_HideDetails.Formatting.Font = New System.Drawing.Font("Calibri", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.FormattingRule_r_HideDetails.Formatting.ForeColor = System.Drawing.Color.FromArgb(CType(CType(102, Byte), Integer), CType(CType(102, Byte), Integer), CType(CType(153, Byte), Integer))
            Me.FormattingRule_r_HideDetails.Name = "FormattingRule_r_HideDetails"
            '
            'GroupHeader1
            '
            Me.GroupHeader1.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrTable2})
            Me.GroupHeader1.HeightF = 21.88!
            Me.GroupHeader1.Name = "GroupHeader1"
            '
            'XrTable2
            '
            Me.XrTable2.AnchorVertical = DevExpress.XtraReports.UI.VerticalAnchorStyles.Bottom
            Me.XrTable2.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
            Me.XrTable2.Name = "XrTable2"
            Me.XrTable2.Padding = New DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 100.0!)
            Me.XrTable2.Rows.AddRange(New DevExpress.XtraReports.UI.XRTableRow() {Me.XrTableRow2})
            Me.XrTable2.SizeF = New System.Drawing.SizeF(790.0!, 21.88!)
            Me.XrTable2.StylePriority.UsePadding = False
            '
            'XrTableRow2
            '
            Me.XrTableRow2.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.XrTableCell4, Me.XrTableCell5, Me.XrTableCell13, Me.XrTableCell6, Me.XrTableCell12})
            Me.XrTableRow2.Name = "XrTableRow2"
            Me.XrTableRow2.StyleName = "XrControlStyle_Report_Column"
            Me.XrTableRow2.StylePriority.UseTextAlignment = False
            Me.XrTableRow2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            Me.XrTableRow2.Weight = 0.4558333158493042R
            '
            'XrTableCell4
            '
            Me.XrTableCell4.CanGrow = False
            Me.XrTableCell4.Name = "XrTableCell4"
            Me.XrTableCell4.Padding = New DevExpress.XtraPrinting.PaddingInfo(5, 0, 0, 0, 100.0!)
            Me.XrTableCell4.StylePriority.UsePadding = False
            Me.XrTableCell4.Text = "ASSETS"
            Me.XrTableCell4.Weight = 1.3510437197903722R
            '
            'XrTableCell5
            '
            Me.XrTableCell5.CanGrow = False
            Me.XrTableCell5.Name = "XrTableCell5"
            Me.XrTableCell5.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 5, 0, 0, 100.0!)
            Me.XrTableCell5.StylePriority.UsePadding = False
            Me.XrTableCell5.StylePriority.UseTextAlignment = False
            Me.XrTableCell5.Text = "RESALE VALUE"
            Me.XrTableCell5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrTableCell5.Weight = 0.64895178649916252R
            '
            'XrTableCell13
            '
            Me.XrTableCell13.BackColor = System.Drawing.Color.Transparent
            Me.XrTableCell13.CanGrow = False
            Me.XrTableCell13.Name = "XrTableCell13"
            Me.XrTableCell13.StylePriority.UseBackColor = False
            Me.XrTableCell13.Weight = 0.1010682009504453R
            '
            'XrTableCell6
            '
            Me.XrTableCell6.CanGrow = False
            Me.XrTableCell6.Name = "XrTableCell6"
            Me.XrTableCell6.Padding = New DevExpress.XtraPrinting.PaddingInfo(5, 0, 0, 0, 100.0!)
            Me.XrTableCell6.StylePriority.UsePadding = False
            Me.XrTableCell6.Text = "LIABILITIES"
            Me.XrTableCell6.Weight = 1.2499832712482073R
            '
            'XrTableCell12
            '
            Me.XrTableCell12.CanGrow = False
            Me.XrTableCell12.Name = "XrTableCell12"
            Me.XrTableCell12.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 5, 0, 0, 100.0!)
            Me.XrTableCell12.StylePriority.UsePadding = False
            Me.XrTableCell12.StylePriority.UseTextAlignment = False
            Me.XrTableCell12.Text = "VALUE"
            Me.XrTableCell12.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrTableCell12.Weight = 0.68298653630569572R
            '
            'GroupFooter1
            '
            Me.GroupFooter1.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrTable1, Me.XrLabel1})
            Me.GroupFooter1.HeightF = 107.7084!
            Me.GroupFooter1.Name = "GroupFooter1"
            '
            'XrTable1
            '
            Me.XrTable1.LocationFloat = New DevExpress.Utils.PointFloat(359.3759!, 41.88!)
            Me.XrTable1.Name = "XrTable1"
            Me.XrTable1.Rows.AddRange(New DevExpress.XtraReports.UI.XRTableRow() {Me.XrTableRow1, Me.XrTableRow4, Me.XrTableRow5})
            Me.XrTable1.SizeF = New System.Drawing.SizeF(430.6223!, 65.64!)
            Me.XrTable1.StyleName = "XrControlStyle_Report_Total"
            Me.XrTable1.StylePriority.UseTextAlignment = False
            Me.XrTable1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            '
            'XrTableRow1
            '
            Me.XrTableRow1.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.XrTableCell1, Me.XrTableCell_total_l_value})
            Me.XrTableRow1.Name = "XrTableRow1"
            Me.XrTableRow1.Weight = 0.875199966430664R
            '
            'XrTableCell1
            '
            Me.XrTableCell1.Name = "XrTableCell1"
            Me.XrTableCell1.Padding = New DevExpress.XtraPrinting.PaddingInfo(5, 0, 0, 0, 100.0!)
            Me.XrTableCell1.StylePriority.UsePadding = False
            Me.XrTableCell1.Text = "TOTAL ASSETS"
            Me.XrTableCell1.Weight = 1.6434728087689405R
            '
            'XrTableCell_total_l_value
            '
            Me.XrTableCell_total_l_value.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "l_value")})
            Me.XrTableCell_total_l_value.Name = "XrTableCell_total_l_value"
            Me.XrTableCell_total_l_value.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 5, 0, 0, 100.0!)
            Me.XrTableCell_total_l_value.StylePriority.UsePadding = False
            Me.XrTableCell_total_l_value.StylePriority.UseTextAlignment = False
            XrSummary1.FormatString = "{0:C}"
            XrSummary1.Running = DevExpress.XtraReports.UI.SummaryRunning.Report
            Me.XrTableCell_total_l_value.Summary = XrSummary1
            Me.XrTableCell_total_l_value.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrTableCell_total_l_value.Weight = 1.3565271912310595R
            '
            'XrTableRow4
            '
            Me.XrTableRow4.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.XrTableCell7, Me.XrTableCell_total_r_value})
            Me.XrTableRow4.Name = "XrTableRow4"
            Me.XrTableRow4.Weight = 0.87519996643066411R
            '
            'XrTableCell7
            '
            Me.XrTableCell7.Name = "XrTableCell7"
            Me.XrTableCell7.Padding = New DevExpress.XtraPrinting.PaddingInfo(5, 0, 0, 0, 100.0!)
            Me.XrTableCell7.StylePriority.UsePadding = False
            Me.XrTableCell7.Text = "MINUS TOTAL LIABILITES"
            Me.XrTableCell7.Weight = 1.6434732339801978R
            '
            'XrTableCell_total_r_value
            '
            Me.XrTableCell_total_r_value.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "r_value")})
            Me.XrTableCell_total_r_value.Name = "XrTableCell_total_r_value"
            Me.XrTableCell_total_r_value.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 5, 0, 0, 100.0!)
            Me.XrTableCell_total_r_value.StylePriority.UsePadding = False
            Me.XrTableCell_total_r_value.StylePriority.UseTextAlignment = False
            XrSummary2.FormatString = "{0:C}"
            XrSummary2.Running = DevExpress.XtraReports.UI.SummaryRunning.Report
            Me.XrTableCell_total_r_value.Summary = XrSummary2
            Me.XrTableCell_total_r_value.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrTableCell_total_r_value.Weight = 1.3565267660198022R
            '
            'XrTableRow5
            '
            Me.XrTableRow5.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.XrTableCell2, Me.XrTableCell_total_difference})
            Me.XrTableRow5.Name = "XrTableRow5"
            Me.XrTableRow5.Weight = 0.87519996643066422R
            '
            'XrTableCell2
            '
            Me.XrTableCell2.Name = "XrTableCell2"
            Me.XrTableCell2.Padding = New DevExpress.XtraPrinting.PaddingInfo(5, 0, 0, 0, 100.0!)
            Me.XrTableCell2.StylePriority.UsePadding = False
            Me.XrTableCell2.Text = "YOUR FINANCIAL NET WORTH"
            Me.XrTableCell2.Weight = 1.6434736591914552R
            '
            'XrTableCell_total_difference
            '
            Me.XrTableCell_total_difference.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "difference")})
            Me.XrTableCell_total_difference.Name = "XrTableCell_total_difference"
            Me.XrTableCell_total_difference.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 5, 0, 0, 100.0!)
            Me.XrTableCell_total_difference.StylePriority.UsePadding = False
            Me.XrTableCell_total_difference.StylePriority.UseTextAlignment = False
            XrSummary3.FormatString = "{0:C}"
            XrSummary3.Running = DevExpress.XtraReports.UI.SummaryRunning.Report
            Me.XrTableCell_total_difference.Summary = XrSummary3
            Me.XrTableCell_total_difference.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrTableCell_total_difference.Weight = 1.3565263408085448R
            '
            'XrLabel1
            '
            Me.XrLabel1.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 10.0!)
            Me.XrLabel1.Name = "XrLabel1"
            Me.XrLabel1.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel1.SizeF = New System.Drawing.SizeF(789.9982!, 21.88!)
            Me.XrLabel1.StyleName = "XrControlStyle_Report_Column"
            Me.XrLabel1.StylePriority.UseTextAlignment = False
            Me.XrLabel1.Text = "CALCULATE YOUR NET WORTH"
            Me.XrLabel1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
            '
            'XrTable3
            '
            Me.XrTable3.AnchorVertical = CType((DevExpress.XtraReports.UI.VerticalAnchorStyles.Top Or DevExpress.XtraReports.UI.VerticalAnchorStyles.Bottom), DevExpress.XtraReports.UI.VerticalAnchorStyles)
            Me.XrTable3.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
            Me.XrTable3.Name = "XrTable3"
            Me.XrTable3.Padding = New DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 100.0!)
            Me.XrTable3.Rows.AddRange(New DevExpress.XtraReports.UI.XRTableRow() {Me.XrTableRow3})
            Me.XrTable3.SizeF = New System.Drawing.SizeF(789.9982!, 23.0!)
            '
            'XrTableRow3
            '
            Me.XrTableRow3.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.XrTableCell_l_description, Me.XrTableCell_l_value, Me.XrTableCell14, Me.XrTableCell_r_description, Me.XrTableCell_r_value})
            Me.XrTableRow3.Name = "XrTableRow3"
            Me.XrTableRow3.StyleName = "XrControlStyle_Report_Detail"
            Me.XrTableRow3.Weight = 1.065443906080036R
            '
            'XrTableCell_l_description
            '
            Me.XrTableCell_l_description.BorderColor = System.Drawing.Color.White
            Me.XrTableCell_l_description.CanGrow = False
            Me.XrTableCell_l_description.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "l_description")})
            Me.XrTableCell_l_description.FormattingRules.Add(Me.FormattingRule_l_ShowHeading)
            Me.XrTableCell_l_description.Name = "XrTableCell_l_description"
            Me.XrTableCell_l_description.Padding = New DevExpress.XtraPrinting.PaddingInfo(5, 0, 0, 0, 100.0!)
            Me.XrTableCell_l_description.Scripts.OnBeforePrint = "XrTableCell_l_description_BeforePrint"
            Me.XrTableCell_l_description.StylePriority.UseBorderColor = False
            Me.XrTableCell_l_description.StylePriority.UsePadding = False
            Me.XrTableCell_l_description.Weight = 1.3510467659158909R
            '
            'XrTableCell_l_value
            '
            Me.XrTableCell_l_value.CanGrow = False
            Me.XrTableCell_l_value.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "l_value", "{0:c}")})
            Me.XrTableCell_l_value.FormattingRules.Add(Me.FormattingRule_l_HideDetails)
            Me.XrTableCell_l_value.Name = "XrTableCell_l_value"
            Me.XrTableCell_l_value.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 5, 0, 0, 100.0!)
            Me.XrTableCell_l_value.StylePriority.UsePadding = False
            Me.XrTableCell_l_value.StylePriority.UseTextAlignment = False
            Me.XrTableCell_l_value.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrTableCell_l_value.Weight = 0.648953376932347R
            '
            'XrTableCell14
            '
            Me.XrTableCell14.BackColor = System.Drawing.Color.Transparent
            Me.XrTableCell14.CanGrow = False
            Me.XrTableCell14.Name = "XrTableCell14"
            Me.XrTableCell14.StylePriority.UseBackColor = False
            Me.XrTableCell14.Weight = 0.10106831002742683R
            '
            'XrTableCell_r_description
            '
            Me.XrTableCell_r_description.CanGrow = False
            Me.XrTableCell_r_description.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "r_description")})
            Me.XrTableCell_r_description.FormattingRules.Add(Me.FormattingRule_r_ShowHeading)
            Me.XrTableCell_r_description.Name = "XrTableCell_r_description"
            Me.XrTableCell_r_description.Padding = New DevExpress.XtraPrinting.PaddingInfo(5, 0, 0, 0, 100.0!)
            Me.XrTableCell_r_description.Scripts.OnBeforePrint = "XrTableCell_r_description_BeforePrint"
            Me.XrTableCell_r_description.StylePriority.UsePadding = False
            Me.XrTableCell_r_description.Weight = 1.2499860528174866R
            '
            'XrTableCell_r_value
            '
            Me.XrTableCell_r_value.CanGrow = False
            Me.XrTableCell_r_value.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "r_value", "{0:c}")})
            Me.XrTableCell_r_value.FormattingRules.Add(Me.FormattingRule_r_HideDetails)
            Me.XrTableCell_r_value.Name = "XrTableCell_r_value"
            Me.XrTableCell_r_value.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 5, 0, 0, 100.0!)
            Me.XrTableCell_r_value.StylePriority.UsePadding = False
            Me.XrTableCell_r_value.StylePriority.UseTextAlignment = False
            Me.XrTableCell_r_value.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrTableCell_r_value.Weight = 0.68297869743184836R
            '
            'EnglishFinancialNetWorth_3
            '
            Me.Bands.AddRange(New DevExpress.XtraReports.UI.Band() {Me.Detail, Me.TopMarginBand1, Me.BottomMarginBand1, Me.ReportHeader, Me.PageHeader, Me.GroupHeader1, Me.GroupFooter1})
            Me.FormattingRuleSheet.AddRange(New DevExpress.XtraReports.UI.FormattingRule() {Me.FormattingRule_l_ShowHeading, Me.FormattingRule_l_HideDetails, Me.FormattingRule_r_ShowHeading, Me.FormattingRule_r_HideDetails})
            Me.Margins = New System.Drawing.Printing.Margins(25, 24, 25, 25)
            Me.ReportPrintOptions.PrintOnEmptyDataSource = False
            Me.ScriptLanguage = DevExpress.XtraReports.ScriptLanguage.VisualBasic
            Me.Scripts.OnBeforePrint = "EnglishFinancialNetWorth_BeforePrint"
            Me.ScriptsSource = resources.GetString("$this.ScriptsSource")
            Me.StyleSheet.AddRange(New DevExpress.XtraReports.UI.XRControlStyle() {Me.XrControlStyle_HeaderText, Me.XrControlStyle_HeaderLine, Me.XrControlStyle_HeaderBox1, Me.XrControlStyle_HeaderBox2, Me.XrControlStyle_HeaderBox3, Me.XrControlStyle_Report_Column, Me.XrControlStyle_Report_Group, Me.XrControlStyle_Report_Detail, Me.XrControlStyle_Report_Total, Me.XrControlStyle_Footer, Me.XrControlStyle_HeaderSubTitle})
            Me.Version = "11.2"
            Me.Controls.SetChildIndex(Me.GroupFooter1, 0)
            Me.Controls.SetChildIndex(Me.GroupHeader1, 0)
            Me.Controls.SetChildIndex(Me.PageHeader, 0)
            Me.Controls.SetChildIndex(Me.ReportHeader, 0)
            Me.Controls.SetChildIndex(Me.BottomMarginBand1, 0)
            Me.Controls.SetChildIndex(Me.TopMarginBand1, 0)
            Me.Controls.SetChildIndex(Me.Detail, 0)
            CType(Me.XrTable2, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.XrTable1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.XrTable3, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me, System.ComponentModel.ISupportInitialize).EndInit()

        End Sub

        Private GroupHeader1 As DevExpress.XtraReports.UI.GroupHeaderBand
        Private GroupFooter1 As DevExpress.XtraReports.UI.GroupFooterBand
        Private XrTable3 As DevExpress.XtraReports.UI.XRTable
        Private XrTableRow3 As DevExpress.XtraReports.UI.XRTableRow
        Private XrTableCell_l_description As DevExpress.XtraReports.UI.XRTableCell
        Private XrTableCell_l_value As DevExpress.XtraReports.UI.XRTableCell
        Private XrTableCell14 As DevExpress.XtraReports.UI.XRTableCell
        Private XrTableCell_r_description As DevExpress.XtraReports.UI.XRTableCell
        Private XrTableCell_r_value As DevExpress.XtraReports.UI.XRTableCell
        Private XrTable2 As DevExpress.XtraReports.UI.XRTable
        Private XrTableRow2 As DevExpress.XtraReports.UI.XRTableRow
        Private XrTableCell4 As DevExpress.XtraReports.UI.XRTableCell
        Private XrTableCell5 As DevExpress.XtraReports.UI.XRTableCell
        Private XrTableCell13 As DevExpress.XtraReports.UI.XRTableCell
        Private XrTableCell6 As DevExpress.XtraReports.UI.XRTableCell
        Private XrTableCell12 As DevExpress.XtraReports.UI.XRTableCell
        Private XrTable1 As DevExpress.XtraReports.UI.XRTable
        Private XrTableRow1 As DevExpress.XtraReports.UI.XRTableRow
        Private XrTableCell1 As DevExpress.XtraReports.UI.XRTableCell
        Private XrTableCell_total_l_value As DevExpress.XtraReports.UI.XRTableCell
        Private XrTableRow4 As DevExpress.XtraReports.UI.XRTableRow
        Private XrTableCell7 As DevExpress.XtraReports.UI.XRTableCell
        Private XrTableCell_total_r_value As DevExpress.XtraReports.UI.XRTableCell
        Private XrTableRow5 As DevExpress.XtraReports.UI.XRTableRow
        Private XrTableCell2 As DevExpress.XtraReports.UI.XRTableCell
        Private XrTableCell_total_difference As DevExpress.XtraReports.UI.XRTableCell
        Private XrLabel1 As DevExpress.XtraReports.UI.XRLabel
        Protected FormattingRule_l_HideDetails As DevExpress.XtraReports.UI.FormattingRule
        Protected FormattingRule_l_ShowHeading As DevExpress.XtraReports.UI.FormattingRule
        Protected FormattingRule_r_HideDetails As DevExpress.XtraReports.UI.FormattingRule
        Protected FormattingRule_r_ShowHeading As DevExpress.XtraReports.UI.FormattingRule
    End Class
End Namespace
