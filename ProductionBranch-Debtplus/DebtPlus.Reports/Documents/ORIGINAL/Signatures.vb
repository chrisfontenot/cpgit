﻿Namespace Documents

    Public Class Signatures
        Public Sub New()
            MyBase.New()
            InitializeComponent()
            ' AddHandler BeforePrint, AddressOf Signatures_BeforePrint
        End Sub

        Private Sub Signatures_BeforePrint(sender As Object, e As System.Drawing.Printing.PrintEventArgs)
            Dim rpt As DevExpress.XtraReports.UI.XtraReport = TryCast(sender, DevExpress.XtraReports.UI.XtraReport)
            If rpt Is Nothing Then
                Return
            End If

            Dim parm As DevExpress.XtraReports.Parameters.Parameter = rpt.Parameters("ParameterQuery")
            If parm Is Nothing Then
                Return
            End If

            ' Find the marker string from the parameter. It comes from the database when we were created.
            Dim queryString As String = Convert.ToString(parm.Value)
            If String.IsNullOrWhiteSpace(queryString) Then
                Return
            End If

            ' Update the text with the marker and enable the line to be printed
            For Each FieldString As String In queryString.Split(";")
                Dim keys() As String = FieldString.Split("=")
                If keys.GetUpperBound(0) < 1 Then
                    Continue For
                End If

                ' If the item is the marker then set the string and enable the printing of it
                If String.Compare(keys(0), "marker", True) = 0 Then
                    XrLabel_Marker.Text = keys(1).Replace("\r\n", Environment.NewLine)
                    GroupHeader_Marker.Visible = True
                    Return
                End If
            Next
        End Sub
    End Class
End Namespace
