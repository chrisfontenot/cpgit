﻿Namespace Documents
    Public Class CreditorList_5
        Public Sub New()
            MyBase.New()
            InitializeComponent()
            ' AddHandler BeforePrint, AddressOf CreditorList_BeforePrint
        End Sub

        Private Sub CreditorList_BeforePrint(sender As Object, e As System.Drawing.Printing.PrintEventArgs)
            Dim rpt As DevExpress.XtraReports.UI.XtraReport = TryCast(sender, DevExpress.XtraReports.UI.XtraReport)
            If rpt Is Nothing Then
                Return
            End If

            Dim MasterRpt As DebtPlus.Interfaces.Client.IClient = TryCast(rpt.MasterReport, DebtPlus.Interfaces.Client.IClient)
            Dim ClientID As Int32 = -1
            If MasterRpt IsNot Nothing Then
                ClientID = MasterRpt.ClientId
            End If

            If ClientID < 0 Then
                Return
            End If

            Dim tableName As String = "rpt_PrintClient_CreditorDebt"
            Dim ds As New System.Data.DataSet("ds")

            Using cn As New System.Data.SqlClient.SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
                cn.Open()
                Using cmd As New System.Data.SqlClient.SqlCommand()
                    cmd.Connection = cn
                    cmd.CommandText = "rpt_PrintClient_CreditorDebt"
                    cmd.CommandType = System.Data.CommandType.StoredProcedure
                    cmd.CommandTimeout = 0
                    cmd.Parameters.Add("@client", System.Data.SqlDbType.Int).Value = ClientID

                    Using da As New System.Data.SqlClient.SqlDataAdapter(cmd)
                        da.Fill(ds, tableName)
                        rpt.DataSource = ds.Tables(tableName).DefaultView()
                    End Using
                End Using
            End Using
        End Sub
    End Class
End Namespace
