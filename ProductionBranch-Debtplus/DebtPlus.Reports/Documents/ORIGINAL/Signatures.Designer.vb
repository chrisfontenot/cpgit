﻿Namespace Documents
    <Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
    Partial Public Class Signatures
        Inherits Template.BaseXtraReportClass

        'XtraReport overrides dispose to clean up the component list.
        <System.Diagnostics.DebuggerNonUserCode()> _
        Protected Overrides Sub Dispose(ByVal disposing As Boolean)
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
            MyBase.Dispose(disposing)
        End Sub

        'Required by the Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Designer
        'It can be modified using the Designer.  
        'Do not modify it using the code editor.
        <System.Diagnostics.DebuggerStepThrough()> _
        Private Sub InitializeComponent()
            Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Signatures))
            Me.XrPanel1 = New DevExpress.XtraReports.UI.XRPanel()
            Me.XrLabel1 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_Signature = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel2 = New DevExpress.XtraReports.UI.XRLabel()
            Me.ParameterClient = New DevExpress.XtraReports.Parameters.Parameter()
            Me.ParameterLanguage = New DevExpress.XtraReports.Parameters.Parameter()
            Me.ParameterQuery = New DevExpress.XtraReports.Parameters.Parameter()
            Me.GroupHeader1 = New DevExpress.XtraReports.UI.GroupHeaderBand()
            Me.GroupHeader_Marker = New DevExpress.XtraReports.UI.GroupHeaderBand()
            Me.XrLabel_Marker = New DevExpress.XtraReports.UI.XRLabel()
            CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
            '
            'Detail
            '
            Me.Detail.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrPanel1})
            Me.Detail.HeightF = 65.0!
            Me.Detail.MultiColumn.ColumnCount = 2
            Me.Detail.MultiColumn.ColumnSpacing = 8.0!
            Me.Detail.MultiColumn.Layout = DevExpress.XtraPrinting.ColumnLayout.AcrossThenDown
            Me.Detail.MultiColumn.Mode = DevExpress.XtraReports.UI.MultiColumnMode.UseColumnCount
            Me.Detail.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
            Me.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
            '
            'XrPanel1
            '
            Me.XrPanel1.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel1, Me.XrLabel_Signature})
            Me.XrPanel1.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
            Me.XrPanel1.Name = "XrPanel1"
            Me.XrPanel1.SizeF = New System.Drawing.SizeF(369.7917!, 65.0!)
            '
            'XrLabel1
            '
            Me.XrLabel1.Borders = DevExpress.XtraPrinting.BorderSide.Top
            Me.XrLabel1.LocationFloat = New DevExpress.Utils.PointFloat(9.99999!, 42.00001!)
            Me.XrLabel1.Name = "XrLabel1"
            Me.XrLabel1.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel1.SizeF = New System.Drawing.SizeF(352.9166!, 23.0!)
            Me.XrLabel1.StylePriority.UseBorders = False
            Me.XrLabel1.Text = "Date"
            '
            'XrLabel_Signature
            '
            Me.XrLabel_Signature.Borders = DevExpress.XtraPrinting.BorderSide.Top
            Me.XrLabel_Signature.LocationFloat = New DevExpress.Utils.PointFloat(10.00001!, 0.0!)
            Me.XrLabel_Signature.Name = "XrLabel_Signature"
            Me.XrLabel_Signature.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_Signature.SizeF = New System.Drawing.SizeF(352.9166!, 23.0!)
            Me.XrLabel_Signature.StylePriority.UseBorders = False
            Me.XrLabel_Signature.Text = "Client Signature"
            '
            'XrLabel2
            '
            Me.XrLabel2.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.00001589457!)
            Me.XrLabel2.Name = "XrLabel2"
            Me.XrLabel2.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel2.SizeF = New System.Drawing.SizeF(100.0!, 23.0!)
            '
            'ParameterClient
            '
            Me.ParameterClient.Description = "Client ID"
            Me.ParameterClient.Name = "ParameterClient"
            Me.ParameterClient.Type = GetType(Integer)
            Me.ParameterClient.Value = -1
            Me.ParameterClient.Visible = False
            '
            'ParameterLanguage
            '
            Me.ParameterLanguage.Description = "Language ID"
            Me.ParameterLanguage.Name = "ParameterLanguage"
            Me.ParameterLanguage.Type = GetType(Integer)
            Me.ParameterLanguage.Value = 1
            Me.ParameterLanguage.Visible = False
            '
            'ParameterQuery
            '
            Me.ParameterQuery.Description = "Query Strng"
            Me.ParameterQuery.Name = "ParameterQuery"
            Me.ParameterQuery.Value = "Marker=Please sign below to indicate your acceptance"
            Me.ParameterQuery.Visible = False
            '
            'GroupHeader1
            '
            Me.GroupHeader1.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel2})
            Me.GroupHeader1.GroupUnion = DevExpress.XtraReports.UI.GroupUnion.WithFirstDetail
            Me.GroupHeader1.HeightF = 23.00002!
            Me.GroupHeader1.Name = "GroupHeader1"
            '
            'GroupHeader_Marker
            '
            Me.GroupHeader_Marker.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel_Marker})
            Me.GroupHeader_Marker.GroupUnion = DevExpress.XtraReports.UI.GroupUnion.WithFirstDetail
            Me.GroupHeader_Marker.HeightF = 23.0!
            Me.GroupHeader_Marker.KeepTogether = True
            Me.GroupHeader_Marker.Level = 1
            Me.GroupHeader_Marker.Name = "GroupHeader_Marker"
            Me.GroupHeader_Marker.Visible = False
            '
            'XrLabel_Marker
            '
            Me.XrLabel_Marker.Font = New System.Drawing.Font("Calibri", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.XrLabel_Marker.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
            Me.XrLabel_Marker.Multiline = True
            Me.XrLabel_Marker.Name = "XrLabel_Marker"
            Me.XrLabel_Marker.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_Marker.SizeF = New System.Drawing.SizeF(800.0!, 23.0!)
            Me.XrLabel_Marker.StylePriority.UseFont = False
            '
            'Signatures
            '
            Me.Bands.AddRange(New DevExpress.XtraReports.UI.Band() {Me.TopMarginBand1, Me.Detail, Me.BottomMarginBand1, Me.GroupHeader1, Me.GroupHeader_Marker})
            Me.Font = New System.Drawing.Font("Calibri", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.Parameters.AddRange(New DevExpress.XtraReports.Parameters.Parameter() {Me.ParameterClient, Me.ParameterLanguage, Me.ParameterQuery})
            Me.ReportPrintOptions.DetailCount = 2
            Me.ReportPrintOptions.DetailCountAtDesignTime = 2
            Me.ReportPrintOptions.DetailCountOnEmptyDataSource = 2
            Me.ScriptLanguage = DevExpress.XtraReports.ScriptLanguage.VisualBasic
            Me.Scripts.OnBeforePrint = "Signatures_BeforePrint"
            Me.ScriptsSource = resources.GetString("$this.ScriptsSource")
            Me.Version = "11.2"
            Me.Controls.SetChildIndex(Me.GroupHeader_Marker, 0)
            Me.Controls.SetChildIndex(Me.GroupHeader1, 0)
            Me.Controls.SetChildIndex(Me.BottomMarginBand1, 0)
            Me.Controls.SetChildIndex(Me.Detail, 0)
            Me.Controls.SetChildIndex(Me.TopMarginBand1, 0)
            CType(Me, System.ComponentModel.ISupportInitialize).EndInit()

        End Sub
        Protected XrPanel1 As DevExpress.XtraReports.UI.XRPanel
        Protected XrLabel1 As DevExpress.XtraReports.UI.XRLabel
        Protected XrLabel_Signature As DevExpress.XtraReports.UI.XRLabel
        Protected XrLabel2 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents ParameterClient As DevExpress.XtraReports.Parameters.Parameter
        Friend WithEvents ParameterLanguage As DevExpress.XtraReports.Parameters.Parameter
        Friend WithEvents ParameterQuery As DevExpress.XtraReports.Parameters.Parameter
        Friend WithEvents GroupHeader1 As DevExpress.XtraReports.UI.GroupHeaderBand
        Friend WithEvents GroupHeader_Marker As DevExpress.XtraReports.UI.GroupHeaderBand
        Protected WithEvents XrLabel_Marker As DevExpress.XtraReports.UI.XRLabel
    End Class
End Namespace
