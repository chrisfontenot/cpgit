﻿Namespace Documents
    <Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
    Partial Class CLientGrievenceProcedures_3
        Inherits Template_3

        'XtraReport overrides dispose to clean up the component list.
        <System.Diagnostics.DebuggerNonUserCode()> _
        Protected Overrides Sub Dispose(ByVal disposing As Boolean)
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
            MyBase.Dispose(disposing)
        End Sub

        'Required by the Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Designer
        'It can be modified using the Designer.  
        'Do not modify it using the code editor.
        <System.Diagnostics.DebuggerStepThrough()> _
        Private Sub InitializeComponent()
            CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
            '
            'XrLabel_ReportTitle
            '
            Me.XrLabel_ReportTitle.StylePriority.UseFont = False
            Me.XrLabel_ReportTitle.StylePriority.UseForeColor = False
            Me.XrLabel_ReportTitle.Text = "CLIENT GRIEVENCE" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "PROCEDURES"
            '
            'XrLine_reportHeaderLine
            '
            Me.XrLine_reportHeaderLine.StylePriority.UseBorderColor = False
            Me.XrLine_reportHeaderLine.StylePriority.UseForeColor = False
            '
            'XrLabel_pageHeader_Title
            '
            Me.XrLabel_pageHeader_Title.Text = "CLIENT GRIEVENCE" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "PROCEDURES (Continued)"
            '
            'PageHeader
            '
            Me.PageHeader.Visible = True
            '
            'CLientGrievenceProcedures_2
            '
            Me.Bands.AddRange(New DevExpress.XtraReports.UI.Band() {Me.TopMarginBand1, Me.Detail, Me.BottomMarginBand1, Me.ReportHeader, Me.PageHeader})
            Me.Version = "11.2"
            CType(Me, System.ComponentModel.ISupportInitialize).EndInit()

        End Sub
    End Class
End Namespace
