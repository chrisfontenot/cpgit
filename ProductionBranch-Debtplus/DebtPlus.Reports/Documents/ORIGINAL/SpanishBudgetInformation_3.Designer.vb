﻿Namespace Documents
    <Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
    Partial Public Class SpanishBudgetInformation_3
        Inherits DevExpress.XtraReports.UI.XtraReport

        'XtraReport overrides dispose to clean up the component list.
        <System.Diagnostics.DebuggerNonUserCode()> _
        Protected Overrides Sub Dispose(ByVal disposing As Boolean)
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
            MyBase.Dispose(disposing)
        End Sub

        'Required by the Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Designer
        'It can be modified using the Designer.  
        'Do not modify it using the code editor.
        <System.Diagnostics.DebuggerStepThrough()> _
        Private Sub InitializeComponent()
            Dim XrSummary1 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary()
            Dim XrSummary2 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary()
            Dim XrSummary3 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary()
            Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(SpanishBudgetInformation_3))
            Me.XrLabel_pageHeader_Title = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrControlStyle_Footer = New DevExpress.XtraReports.UI.XRControlStyle()
            Me.XrLabel_reportHeaderBox1 = New DevExpress.XtraReports.UI.XRLabel()
            Me.PageHeader = New DevExpress.XtraReports.UI.PageHeaderBand()
            Me.XrLine_pageHeaderLine = New DevExpress.XtraReports.UI.XRLine()
            Me.XrLabel_pageHeaderBox1 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_pageHeaderBox2 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_pageHeaderBox3 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrControlStyle_Report_Total = New DevExpress.XtraReports.UI.XRControlStyle()
            Me.FormattingRule_HideDetails = New DevExpress.XtraReports.UI.FormattingRule()
            Me.XrLabel_reportHeaderBox2 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrTableRow_detail = New DevExpress.XtraReports.UI.XRTableRow()
            Me.XrTableCell_budget_category = New DevExpress.XtraReports.UI.XRTableCell()
            Me.FormattingRule_ShowHeading = New DevExpress.XtraReports.UI.FormattingRule()
            Me.XrTableCell_client_amount = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableCell_suggested_amount = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableCell_difference = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrLabel_reportHeaderBox3 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrControlStyle_HeaderText = New DevExpress.XtraReports.UI.XRControlStyle()
            Me.XrControlStyle_HeaderBox3 = New DevExpress.XtraReports.UI.XRControlStyle()
            Me.Detail = New DevExpress.XtraReports.UI.DetailBand()
            Me.XrTable2 = New DevExpress.XtraReports.UI.XRTable()
            Me.XrControlStyle_HeaderLine = New DevExpress.XtraReports.UI.XRControlStyle()
            Me.XrControlStyle_Report_Column = New DevExpress.XtraReports.UI.XRControlStyle()
            Me.TopMarginBand1 = New DevExpress.XtraReports.UI.TopMarginBand()
            Me.XrTableRow2 = New DevExpress.XtraReports.UI.XRTableRow()
            Me.XrTableCell4 = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableCell5 = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableCell6 = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableCell8 = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableCell2 = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableCell7 = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableCell3 = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTable3 = New DevExpress.XtraReports.UI.XRTable()
            Me.BottomMarginBand1 = New DevExpress.XtraReports.UI.BottomMarginBand()
            Me.ReportHeader = New DevExpress.XtraReports.UI.ReportHeaderBand()
            Me.XrLabel_ReportTitle = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLine_reportHeaderLine = New DevExpress.XtraReports.UI.XRLine()
            Me.XrControlStyle_HeaderBox1 = New DevExpress.XtraReports.UI.XRControlStyle()
            Me.XrControlStyle_HeaderBox2 = New DevExpress.XtraReports.UI.XRControlStyle()
            Me.XrControlStyle_Report_Group = New DevExpress.XtraReports.UI.XRControlStyle()
            Me.XrControlStyle_Report_Detail = New DevExpress.XtraReports.UI.XRControlStyle()
            Me.XrTableCell1 = New DevExpress.XtraReports.UI.XRTableCell()
            Me.GroupFooter1 = New DevExpress.XtraReports.UI.GroupFooterBand()
            Me.XrTableRow1 = New DevExpress.XtraReports.UI.XRTableRow()
            Me.GroupHeader1 = New DevExpress.XtraReports.UI.GroupHeaderBand()
            Me.XrTable1 = New DevExpress.XtraReports.UI.XRTable()
            Me.XrControlStyle_HeaderSubTitle = New DevExpress.XtraReports.UI.XRControlStyle()
            CType(Me.XrTable2, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.XrTable3, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.XrTable1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
            '
            'XrLabel_pageHeader_Title
            '
            Me.XrLabel_pageHeader_Title.LocationFloat = New DevExpress.Utils.PointFloat(10.0!, 10.0!)
            Me.XrLabel_pageHeader_Title.Multiline = True
            Me.XrLabel_pageHeader_Title.Name = "XrLabel_pageHeader_Title"
            Me.XrLabel_pageHeader_Title.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_pageHeader_Title.SizeF = New System.Drawing.SizeF(564.5833!, 98.0!)
            Me.XrLabel_pageHeader_Title.StyleName = "XrControlStyle_HeaderText"
            Me.XrLabel_pageHeader_Title.Text = "PRESUPUESTO (Continuación)"
            '
            'XrControlStyle_Footer
            '
            Me.XrControlStyle_Footer.BorderColor = System.Drawing.Color.Teal
            Me.XrControlStyle_Footer.BorderDashStyle = DevExpress.XtraPrinting.BorderDashStyle.Solid
            Me.XrControlStyle_Footer.BorderWidth = 2
            Me.XrControlStyle_Footer.Font = New System.Drawing.Font("Calibri", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.XrControlStyle_Footer.ForeColor = System.Drawing.Color.Teal
            Me.XrControlStyle_Footer.Name = "XrControlStyle_Footer"
            Me.XrControlStyle_Footer.Padding = New DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 100.0!)
            Me.XrControlStyle_Footer.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            '
            'XrLabel_reportHeaderBox1
            '
            Me.XrLabel_reportHeaderBox1.LocationFloat = New DevExpress.Utils.PointFloat(601.0417!, 27.045!)
            Me.XrLabel_reportHeaderBox1.Name = "XrLabel_reportHeaderBox1"
            Me.XrLabel_reportHeaderBox1.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_reportHeaderBox1.SizeF = New System.Drawing.SizeF(56.0!, 56.0!)
            Me.XrLabel_reportHeaderBox1.StyleName = "XrControlStyle_HeaderBox1"
            '
            'PageHeader
            '
            Me.PageHeader.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLine_pageHeaderLine, Me.XrLabel_pageHeader_Title, Me.XrLabel_pageHeaderBox1, Me.XrLabel_pageHeaderBox2, Me.XrLabel_pageHeaderBox3})
            Me.PageHeader.Expanded = False
            Me.PageHeader.HeightF = 135.0!
            Me.PageHeader.Name = "PageHeader"
            Me.PageHeader.PrintOn = DevExpress.XtraReports.UI.PrintOnPages.NotWithReportHeader
            '
            'XrLine_pageHeaderLine
            '
            Me.XrLine_pageHeaderLine.LineWidth = 2
            Me.XrLine_pageHeaderLine.LocationFloat = New DevExpress.Utils.PointFloat(12.5!, 108.0!)
            Me.XrLine_pageHeaderLine.Name = "XrLine_pageHeaderLine"
            Me.XrLine_pageHeaderLine.SizeF = New System.Drawing.SizeF(777.5!, 2.09!)
            Me.XrLine_pageHeaderLine.StyleName = "XrControlStyle_HeaderLine"
            '
            'XrLabel_pageHeaderBox1
            '
            Me.XrLabel_pageHeaderBox1.LocationFloat = New DevExpress.Utils.PointFloat(601.04!, 27.045!)
            Me.XrLabel_pageHeaderBox1.Name = "XrLabel_pageHeaderBox1"
            Me.XrLabel_pageHeaderBox1.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_pageHeaderBox1.SizeF = New System.Drawing.SizeF(56.0!, 56.0!)
            Me.XrLabel_pageHeaderBox1.StyleName = "XrControlStyle_HeaderBox1"
            '
            'XrLabel_pageHeaderBox2
            '
            Me.XrLabel_pageHeaderBox2.LocationFloat = New DevExpress.Utils.PointFloat(667.92!, 27.045!)
            Me.XrLabel_pageHeaderBox2.Name = "XrLabel_pageHeaderBox2"
            Me.XrLabel_pageHeaderBox2.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_pageHeaderBox2.SizeF = New System.Drawing.SizeF(56.0!, 56.0!)
            Me.XrLabel_pageHeaderBox2.StyleName = "XrControlStyle_HeaderBox2"
            '
            'XrLabel_pageHeaderBox3
            '
            Me.XrLabel_pageHeaderBox3.LocationFloat = New DevExpress.Utils.PointFloat(734.79!, 27.045!)
            Me.XrLabel_pageHeaderBox3.Name = "XrLabel_pageHeaderBox3"
            Me.XrLabel_pageHeaderBox3.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_pageHeaderBox3.SizeF = New System.Drawing.SizeF(56.0!, 56.0!)
            Me.XrLabel_pageHeaderBox3.StyleName = "XrControlStyle_HeaderBox3"
            '
            'XrControlStyle_Report_Total
            '
            Me.XrControlStyle_Report_Total.BackColor = System.Drawing.Color.FromArgb(CType(CType(222, Byte), Integer), CType(CType(222, Byte), Integer), CType(CType(233, Byte), Integer))
            Me.XrControlStyle_Report_Total.BorderColor = System.Drawing.Color.White
            Me.XrControlStyle_Report_Total.BorderDashStyle = DevExpress.XtraPrinting.BorderDashStyle.Solid
            Me.XrControlStyle_Report_Total.Borders = DevExpress.XtraPrinting.BorderSide.Bottom
            Me.XrControlStyle_Report_Total.BorderWidth = 2
            Me.XrControlStyle_Report_Total.Font = New System.Drawing.Font("Calibri", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.XrControlStyle_Report_Total.Name = "XrControlStyle_Report_Total"
            Me.XrControlStyle_Report_Total.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            '
            'FormattingRule_HideDetails
            '
            Me.FormattingRule_HideDetails.Condition = "[heading] == True"
            '
            '
            '
            Me.FormattingRule_HideDetails.Formatting.BackColor = System.Drawing.Color.FromArgb(CType(CType(102, Byte), Integer), CType(CType(102, Byte), Integer), CType(CType(153, Byte), Integer))
            Me.FormattingRule_HideDetails.Formatting.BorderColor = System.Drawing.Color.White
            Me.FormattingRule_HideDetails.Formatting.Borders = DevExpress.XtraPrinting.BorderSide.Bottom
            Me.FormattingRule_HideDetails.Formatting.BorderWidth = 1
            Me.FormattingRule_HideDetails.Formatting.Font = New System.Drawing.Font("Calibri", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.FormattingRule_HideDetails.Formatting.ForeColor = System.Drawing.Color.FromArgb(CType(CType(102, Byte), Integer), CType(CType(102, Byte), Integer), CType(CType(153, Byte), Integer))
            Me.FormattingRule_HideDetails.Name = "FormattingRule_HideDetails"
            '
            'XrLabel_reportHeaderBox2
            '
            Me.XrLabel_reportHeaderBox2.LocationFloat = New DevExpress.Utils.PointFloat(667.9167!, 27.045!)
            Me.XrLabel_reportHeaderBox2.Name = "XrLabel_reportHeaderBox2"
            Me.XrLabel_reportHeaderBox2.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_reportHeaderBox2.SizeF = New System.Drawing.SizeF(56.0!, 56.0!)
            Me.XrLabel_reportHeaderBox2.StyleName = "XrControlStyle_HeaderBox2"
            '
            'XrTableRow_detail
            '
            Me.XrTableRow_detail.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.XrTableCell_budget_category, Me.XrTableCell_client_amount, Me.XrTableCell_suggested_amount, Me.XrTableCell_difference})
            Me.XrTableRow_detail.FormattingRules.Add(Me.FormattingRule_ShowHeading)
            Me.XrTableRow_detail.Name = "XrTableRow_detail"
            Me.XrTableRow_detail.Padding = New DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 100.0!)
            Me.XrTableRow_detail.StyleName = "XrControlStyle_Report_Detail"
            Me.XrTableRow_detail.Weight = 1.0R
            '
            'XrTableCell_budget_category
            '
            Me.XrTableCell_budget_category.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "budget_category")})
            Me.XrTableCell_budget_category.FormattingRules.Add(Me.FormattingRule_ShowHeading)
            Me.XrTableCell_budget_category.Name = "XrTableCell_budget_category"
            Me.XrTableCell_budget_category.Padding = New DevExpress.XtraPrinting.PaddingInfo(5, 0, 0, 0, 100.0!)
            Me.XrTableCell_budget_category.Scripts.OnBeforePrint = "XrTableCell_budget_category_BeforePrint"
            Me.XrTableCell_budget_category.StylePriority.UseBackColor = False
            Me.XrTableCell_budget_category.StylePriority.UseBorderColor = False
            Me.XrTableCell_budget_category.StylePriority.UseForeColor = False
            Me.XrTableCell_budget_category.StylePriority.UsePadding = False
            Me.XrTableCell_budget_category.Text = "XrTableCell_budget_category"
            Me.XrTableCell_budget_category.Weight = 3.6514153625824006R
            '
            'FormattingRule_ShowHeading
            '
            Me.FormattingRule_ShowHeading.Condition = "[heading] == True"
            '
            '
            '
            Me.FormattingRule_ShowHeading.Formatting.BackColor = System.Drawing.Color.FromArgb(CType(CType(102, Byte), Integer), CType(CType(102, Byte), Integer), CType(CType(153, Byte), Integer))
            Me.FormattingRule_ShowHeading.Formatting.BorderColor = System.Drawing.Color.White
            Me.FormattingRule_ShowHeading.Formatting.Borders = DevExpress.XtraPrinting.BorderSide.Bottom
            Me.FormattingRule_ShowHeading.Formatting.BorderWidth = 1
            Me.FormattingRule_ShowHeading.Formatting.Font = New System.Drawing.Font("Calibri", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.FormattingRule_ShowHeading.Formatting.ForeColor = System.Drawing.Color.White
            Me.FormattingRule_ShowHeading.Name = "FormattingRule_ShowHeading"
            '
            'XrTableCell_client_amount
            '
            Me.XrTableCell_client_amount.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "client_amount", "{0:c}")})
            Me.XrTableCell_client_amount.FormattingRules.Add(Me.FormattingRule_HideDetails)
            Me.XrTableCell_client_amount.Name = "XrTableCell_client_amount"
            Me.XrTableCell_client_amount.StyleName = "XrControlStyle_Report_Detail"
            Me.XrTableCell_client_amount.StylePriority.UseTextAlignment = False
            Me.XrTableCell_client_amount.Text = "XrTableCell_client_amount"
            Me.XrTableCell_client_amount.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrTableCell_client_amount.Weight = 0.815864928308148R
            '
            'XrTableCell_suggested_amount
            '
            Me.XrTableCell_suggested_amount.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "suggested_amount", "{0:c}")})
            Me.XrTableCell_suggested_amount.FormattingRules.Add(Me.FormattingRule_HideDetails)
            Me.XrTableCell_suggested_amount.Name = "XrTableCell_suggested_amount"
            Me.XrTableCell_suggested_amount.StyleName = "XrControlStyle_Report_Detail"
            Me.XrTableCell_suggested_amount.StylePriority.UseTextAlignment = False
            Me.XrTableCell_suggested_amount.Text = "XrTableCell_suggested_amount"
            Me.XrTableCell_suggested_amount.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrTableCell_suggested_amount.Weight = 0.98201105678879053R
            '
            'XrTableCell_difference
            '
            Me.XrTableCell_difference.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "difference", "{0:c}")})
            Me.XrTableCell_difference.FormattingRules.Add(Me.FormattingRule_HideDetails)
            Me.XrTableCell_difference.Name = "XrTableCell_difference"
            Me.XrTableCell_difference.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 5, 0, 0, 100.0!)
            Me.XrTableCell_difference.StyleName = "XrControlStyle_Report_Total"
            Me.XrTableCell_difference.StylePriority.UsePadding = False
            Me.XrTableCell_difference.StylePriority.UseTextAlignment = False
            Me.XrTableCell_difference.Text = "XrTableCell_difference"
            Me.XrTableCell_difference.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrTableCell_difference.Weight = 0.98201105678879053R
            '
            'XrLabel_reportHeaderBox3
            '
            Me.XrLabel_reportHeaderBox3.LocationFloat = New DevExpress.Utils.PointFloat(734.7917!, 27.045!)
            Me.XrLabel_reportHeaderBox3.Name = "XrLabel_reportHeaderBox3"
            Me.XrLabel_reportHeaderBox3.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_reportHeaderBox3.SizeF = New System.Drawing.SizeF(56.0!, 56.0!)
            Me.XrLabel_reportHeaderBox3.StyleName = "XrControlStyle_HeaderBox3"
            '
            'XrControlStyle_HeaderText
            '
            Me.XrControlStyle_HeaderText.Font = New System.Drawing.Font("Calibri", 27.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.XrControlStyle_HeaderText.ForeColor = System.Drawing.Color.Teal
            Me.XrControlStyle_HeaderText.Name = "XrControlStyle_HeaderText"
            Me.XrControlStyle_HeaderText.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
            Me.XrControlStyle_HeaderText.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            '
            'XrControlStyle_HeaderBox3
            '
            Me.XrControlStyle_HeaderBox3.BackColor = System.Drawing.Color.FromArgb(CType(CType(137, Byte), Integer), CType(CType(201, Byte), Integer), CType(CType(191, Byte), Integer))
            Me.XrControlStyle_HeaderBox3.BorderColor = System.Drawing.Color.FromArgb(CType(CType(137, Byte), Integer), CType(CType(201, Byte), Integer), CType(CType(191, Byte), Integer))
            Me.XrControlStyle_HeaderBox3.ForeColor = System.Drawing.Color.FromArgb(CType(CType(137, Byte), Integer), CType(CType(201, Byte), Integer), CType(CType(191, Byte), Integer))
            Me.XrControlStyle_HeaderBox3.Name = "XrControlStyle_HeaderBox3"
            '
            'Detail
            '
            Me.Detail.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrTable2})
            Me.Detail.HeightF = 21.88!
            Me.Detail.Name = "Detail"
            Me.Detail.StylePriority.UseFont = False
            '
            'XrTable2
            '
            Me.XrTable2.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
            Me.XrTable2.Name = "XrTable2"
            Me.XrTable2.Padding = New DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 100.0!)
            Me.XrTable2.Rows.AddRange(New DevExpress.XtraReports.UI.XRTableRow() {Me.XrTableRow_detail})
            Me.XrTable2.SizeF = New System.Drawing.SizeF(788.2813!, 21.875!)
            Me.XrTable2.StylePriority.UsePadding = False
            '
            'XrControlStyle_HeaderLine
            '
            Me.XrControlStyle_HeaderLine.ForeColor = System.Drawing.Color.Teal
            Me.XrControlStyle_HeaderLine.Name = "XrControlStyle_HeaderLine"
            Me.XrControlStyle_HeaderLine.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
            Me.XrControlStyle_HeaderLine.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            '
            'XrControlStyle_Report_Column
            '
            Me.XrControlStyle_Report_Column.BackColor = System.Drawing.Color.Teal
            Me.XrControlStyle_Report_Column.BorderColor = System.Drawing.Color.White
            Me.XrControlStyle_Report_Column.BorderDashStyle = DevExpress.XtraPrinting.BorderDashStyle.Solid
            Me.XrControlStyle_Report_Column.Borders = DevExpress.XtraPrinting.BorderSide.Bottom
            Me.XrControlStyle_Report_Column.BorderWidth = 2
            Me.XrControlStyle_Report_Column.Font = New System.Drawing.Font("Calibri", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.XrControlStyle_Report_Column.ForeColor = System.Drawing.Color.White
            Me.XrControlStyle_Report_Column.Name = "XrControlStyle_Report_Column"
            Me.XrControlStyle_Report_Column.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrControlStyle_Report_Column.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            '
            'TopMarginBand1
            '
            Me.TopMarginBand1.HeightF = 25.0!
            Me.TopMarginBand1.Name = "TopMarginBand1"
            '
            'XrTableRow2
            '
            Me.XrTableRow2.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.XrTableCell4, Me.XrTableCell5, Me.XrTableCell6, Me.XrTableCell8})
            Me.XrTableRow2.FormattingRules.Add(Me.FormattingRule_ShowHeading)
            Me.XrTableRow2.Name = "XrTableRow2"
            Me.XrTableRow2.Padding = New DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 100.0!)
            Me.XrTableRow2.StyleName = "XrControlStyle_Report_Total"
            Me.XrTableRow2.Weight = 1.0R
            '
            'XrTableCell4
            '
            Me.XrTableCell4.FormattingRules.Add(Me.FormattingRule_ShowHeading)
            Me.XrTableCell4.Name = "XrTableCell4"
            Me.XrTableCell4.Padding = New DevExpress.XtraPrinting.PaddingInfo(5, 0, 0, 0, 100.0!)
            Me.XrTableCell4.StylePriority.UseBackColor = False
            Me.XrTableCell4.StylePriority.UseBorderColor = False
            Me.XrTableCell4.StylePriority.UseForeColor = False
            Me.XrTableCell4.StylePriority.UsePadding = False
            Me.XrTableCell4.Text = "TOTALS"
            Me.XrTableCell4.Weight = 3.6514153625824006R
            '
            'XrTableCell5
            '
            Me.XrTableCell5.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "client_amount", "{0:c}")})
            Me.XrTableCell5.FormattingRules.Add(Me.FormattingRule_HideDetails)
            Me.XrTableCell5.Name = "XrTableCell5"
            Me.XrTableCell5.StylePriority.UseTextAlignment = False
            XrSummary1.FormatString = "{0:c}"
            XrSummary1.Running = DevExpress.XtraReports.UI.SummaryRunning.Report
            Me.XrTableCell5.Summary = XrSummary1
            Me.XrTableCell5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrTableCell5.Weight = 0.815864928308148R
            '
            'XrTableCell6
            '
            Me.XrTableCell6.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "suggested_amount", "{0:c}")})
            Me.XrTableCell6.FormattingRules.Add(Me.FormattingRule_HideDetails)
            Me.XrTableCell6.Name = "XrTableCell6"
            Me.XrTableCell6.StylePriority.UseTextAlignment = False
            XrSummary2.FormatString = "{0:c}"
            XrSummary2.Running = DevExpress.XtraReports.UI.SummaryRunning.Report
            Me.XrTableCell6.Summary = XrSummary2
            Me.XrTableCell6.Text = "XrTableCell_suggested_amount"
            Me.XrTableCell6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrTableCell6.Weight = 0.98201105678879053R
            '
            'XrTableCell8
            '
            Me.XrTableCell8.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "difference", "{0:c}")})
            Me.XrTableCell8.FormattingRules.Add(Me.FormattingRule_HideDetails)
            Me.XrTableCell8.Name = "XrTableCell8"
            Me.XrTableCell8.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 5, 0, 0, 100.0!)
            Me.XrTableCell8.StylePriority.UseTextAlignment = False
            XrSummary3.FormatString = "{0:c}"
            XrSummary3.Running = DevExpress.XtraReports.UI.SummaryRunning.Report
            Me.XrTableCell8.Summary = XrSummary3
            Me.XrTableCell8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrTableCell8.Weight = 0.98201105678879053R
            '
            'XrTableCell2
            '
            Me.XrTableCell2.CanGrow = False
            Me.XrTableCell2.Name = "XrTableCell2"
            Me.XrTableCell2.StylePriority.UseTextAlignment = False
            Me.XrTableCell2.Text = "CLIENT"
            Me.XrTableCell2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrTableCell2.Weight = 0.81586517729006092R
            '
            'XrTableCell7
            '
            Me.XrTableCell7.CanGrow = False
            Me.XrTableCell7.Name = "XrTableCell7"
            Me.XrTableCell7.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 5, 0, 0, 100.0!)
            Me.XrTableCell7.StylePriority.UsePadding = False
            Me.XrTableCell7.StylePriority.UseTextAlignment = False
            Me.XrTableCell7.Text = "DIFFERENCE"
            Me.XrTableCell7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrTableCell7.Weight = 0.98201105678879053R
            '
            'XrTableCell3
            '
            Me.XrTableCell3.CanGrow = False
            Me.XrTableCell3.Name = "XrTableCell3"
            Me.XrTableCell3.StylePriority.UseTextAlignment = False
            Me.XrTableCell3.Text = "SUGGESTED"
            Me.XrTableCell3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrTableCell3.Weight = 0.98201105678879053R
            '
            'XrTable3
            '
            Me.XrTable3.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
            Me.XrTable3.Name = "XrTable3"
            Me.XrTable3.Padding = New DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 100.0!)
            Me.XrTable3.Rows.AddRange(New DevExpress.XtraReports.UI.XRTableRow() {Me.XrTableRow2})
            Me.XrTable3.SizeF = New System.Drawing.SizeF(788.2813!, 21.875!)
            Me.XrTable3.StylePriority.UsePadding = False
            '
            'BottomMarginBand1
            '
            Me.BottomMarginBand1.HeightF = 25.0!
            Me.BottomMarginBand1.Name = "BottomMarginBand1"
            '
            'ReportHeader
            '
            Me.ReportHeader.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel_ReportTitle, Me.XrLine_reportHeaderLine, Me.XrLabel_reportHeaderBox1, Me.XrLabel_reportHeaderBox2, Me.XrLabel_reportHeaderBox3})
            Me.ReportHeader.HeightF = 135.0!
            Me.ReportHeader.Name = "ReportHeader"
            '
            'XrLabel_ReportTitle
            '
            Me.XrLabel_ReportTitle.LocationFloat = New DevExpress.Utils.PointFloat(10.00001!, 10.00001!)
            Me.XrLabel_ReportTitle.Multiline = True
            Me.XrLabel_ReportTitle.Name = "XrLabel_ReportTitle"
            Me.XrLabel_ReportTitle.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_ReportTitle.SizeF = New System.Drawing.SizeF(564.5833!, 98.0!)
            Me.XrLabel_ReportTitle.StyleName = "XrControlStyle_HeaderText"
            Me.XrLabel_ReportTitle.Text = "PRESUPUESTO"
            '
            'XrLine_reportHeaderLine
            '
            Me.XrLine_reportHeaderLine.LineWidth = 2
            Me.XrLine_reportHeaderLine.LocationFloat = New DevExpress.Utils.PointFloat(12.5!, 108.0!)
            Me.XrLine_reportHeaderLine.Name = "XrLine_reportHeaderLine"
            Me.XrLine_reportHeaderLine.SizeF = New System.Drawing.SizeF(777.5!, 2.09!)
            Me.XrLine_reportHeaderLine.StyleName = "XrControlStyle_HeaderLine"
            '
            'XrControlStyle_HeaderBox1
            '
            Me.XrControlStyle_HeaderBox1.BackColor = System.Drawing.Color.Teal
            Me.XrControlStyle_HeaderBox1.BorderColor = System.Drawing.Color.Teal
            Me.XrControlStyle_HeaderBox1.ForeColor = System.Drawing.Color.Teal
            Me.XrControlStyle_HeaderBox1.Name = "XrControlStyle_HeaderBox1"
            '
            'XrControlStyle_HeaderBox2
            '
            Me.XrControlStyle_HeaderBox2.BackColor = System.Drawing.Color.FromArgb(CType(CType(102, Byte), Integer), CType(CType(102, Byte), Integer), CType(CType(153, Byte), Integer))
            Me.XrControlStyle_HeaderBox2.BorderColor = System.Drawing.Color.FromArgb(CType(CType(102, Byte), Integer), CType(CType(102, Byte), Integer), CType(CType(153, Byte), Integer))
            Me.XrControlStyle_HeaderBox2.ForeColor = System.Drawing.Color.FromArgb(CType(CType(102, Byte), Integer), CType(CType(102, Byte), Integer), CType(CType(153, Byte), Integer))
            Me.XrControlStyle_HeaderBox2.Name = "XrControlStyle_HeaderBox2"
            '
            'XrControlStyle_Report_Group
            '
            Me.XrControlStyle_Report_Group.BackColor = System.Drawing.Color.FromArgb(CType(CType(102, Byte), Integer), CType(CType(102, Byte), Integer), CType(CType(153, Byte), Integer))
            Me.XrControlStyle_Report_Group.BorderColor = System.Drawing.Color.White
            Me.XrControlStyle_Report_Group.BorderDashStyle = DevExpress.XtraPrinting.BorderDashStyle.Solid
            Me.XrControlStyle_Report_Group.Borders = DevExpress.XtraPrinting.BorderSide.Bottom
            Me.XrControlStyle_Report_Group.BorderWidth = 2
            Me.XrControlStyle_Report_Group.Font = New System.Drawing.Font("Calibri", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.XrControlStyle_Report_Group.ForeColor = System.Drawing.Color.White
            Me.XrControlStyle_Report_Group.Name = "XrControlStyle_Report_Group"
            Me.XrControlStyle_Report_Group.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            '
            'XrControlStyle_Report_Detail
            '
            Me.XrControlStyle_Report_Detail.BackColor = System.Drawing.Color.FromArgb(CType(CType(222, Byte), Integer), CType(CType(222, Byte), Integer), CType(CType(233, Byte), Integer))
            Me.XrControlStyle_Report_Detail.BorderColor = System.Drawing.Color.White
            Me.XrControlStyle_Report_Detail.BorderDashStyle = DevExpress.XtraPrinting.BorderDashStyle.Solid
            Me.XrControlStyle_Report_Detail.Borders = DevExpress.XtraPrinting.BorderSide.Bottom
            Me.XrControlStyle_Report_Detail.BorderWidth = 2
            Me.XrControlStyle_Report_Detail.Font = New System.Drawing.Font("Calibri", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.XrControlStyle_Report_Detail.Name = "XrControlStyle_Report_Detail"
            Me.XrControlStyle_Report_Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            '
            'XrTableCell1
            '
            Me.XrTableCell1.CanGrow = False
            Me.XrTableCell1.Name = "XrTableCell1"
            Me.XrTableCell1.Padding = New DevExpress.XtraPrinting.PaddingInfo(5, 0, 0, 0, 100.0!)
            Me.XrTableCell1.StylePriority.UsePadding = False
            Me.XrTableCell1.Text = "CATEGORY"
            Me.XrTableCell1.Weight = 3.6514151136004878R
            '
            'GroupFooter1
            '
            Me.GroupFooter1.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrTable3})
            Me.GroupFooter1.HeightF = 21.875!
            Me.GroupFooter1.Name = "GroupFooter1"
            '
            'XrTableRow1
            '
            Me.XrTableRow1.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.XrTableCell1, Me.XrTableCell2, Me.XrTableCell3, Me.XrTableCell7})
            Me.XrTableRow1.Name = "XrTableRow1"
            Me.XrTableRow1.Padding = New DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 100.0!)
            Me.XrTableRow1.StyleName = "XrControlStyle_Report_Column"
            Me.XrTableRow1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            Me.XrTableRow1.Weight = 1.0R
            '
            'GroupHeader1
            '
            Me.GroupHeader1.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrTable1})
            Me.GroupHeader1.HeightF = 21.88!
            Me.GroupHeader1.Name = "GroupHeader1"
            Me.GroupHeader1.RepeatEveryPage = True
            '
            'XrTable1
            '
            Me.XrTable1.AnchorVertical = DevExpress.XtraReports.UI.VerticalAnchorStyles.Bottom
            Me.XrTable1.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
            Me.XrTable1.Name = "XrTable1"
            Me.XrTable1.Padding = New DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 100.0!)
            Me.XrTable1.Rows.AddRange(New DevExpress.XtraReports.UI.XRTableRow() {Me.XrTableRow1})
            Me.XrTable1.SizeF = New System.Drawing.SizeF(788.28!, 21.88!)
            Me.XrTable1.StylePriority.UsePadding = False
            '
            'XrControlStyle_HeaderSubTitle
            '
            Me.XrControlStyle_HeaderSubTitle.Font = New System.Drawing.Font("Calibri", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.XrControlStyle_HeaderSubTitle.Name = "XrControlStyle_HeaderSubTitle"
            Me.XrControlStyle_HeaderSubTitle.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            '
            'SpanishBudgetInformation_3
            '
            Me.Bands.AddRange(New DevExpress.XtraReports.UI.Band() {Me.Detail, Me.TopMarginBand1, Me.BottomMarginBand1, Me.ReportHeader, Me.PageHeader, Me.GroupHeader1, Me.GroupFooter1})
            Me.Font = New System.Drawing.Font("Calibri", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.FormattingRuleSheet.AddRange(New DevExpress.XtraReports.UI.FormattingRule() {Me.FormattingRule_ShowHeading, Me.FormattingRule_HideDetails})
            Me.Margins = New System.Drawing.Printing.Margins(25, 25, 25, 25)
            Me.ReportPrintOptions.PrintOnEmptyDataSource = False
            Me.ScriptLanguage = DevExpress.XtraReports.ScriptLanguage.VisualBasic
            Me.Scripts.OnBeforePrint = "SpanishBudgetInformation_BeforePrint"
            Me.ScriptsSource = resources.GetString("$this.ScriptsSource")
            Me.StyleSheet.AddRange(New DevExpress.XtraReports.UI.XRControlStyle() {Me.XrControlStyle_HeaderText, Me.XrControlStyle_HeaderLine, Me.XrControlStyle_HeaderBox1, Me.XrControlStyle_HeaderBox2, Me.XrControlStyle_HeaderBox3, Me.XrControlStyle_Report_Column, Me.XrControlStyle_Report_Group, Me.XrControlStyle_Report_Detail, Me.XrControlStyle_Report_Total, Me.XrControlStyle_Footer, Me.XrControlStyle_HeaderSubTitle})
            Me.Version = "11.2"
            CType(Me.XrTable2, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.XrTable3, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.XrTable1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me, System.ComponentModel.ISupportInitialize).EndInit()

        End Sub
        Friend WithEvents XrLabel_pageHeader_Title As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrControlStyle_Footer As DevExpress.XtraReports.UI.XRControlStyle
        Friend WithEvents XrLabel_reportHeaderBox1 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents PageHeader As DevExpress.XtraReports.UI.PageHeaderBand
        Friend WithEvents XrLine_pageHeaderLine As DevExpress.XtraReports.UI.XRLine
        Friend WithEvents XrLabel_pageHeaderBox1 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_pageHeaderBox2 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_pageHeaderBox3 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrControlStyle_Report_Total As DevExpress.XtraReports.UI.XRControlStyle
        Friend WithEvents FormattingRule_HideDetails As DevExpress.XtraReports.UI.FormattingRule
        Friend WithEvents XrLabel_reportHeaderBox2 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrTableRow_detail As DevExpress.XtraReports.UI.XRTableRow
        Friend WithEvents XrTableCell_budget_category As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents FormattingRule_ShowHeading As DevExpress.XtraReports.UI.FormattingRule
        Friend WithEvents XrTableCell_client_amount As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents XrTableCell_suggested_amount As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents XrTableCell_difference As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents XrLabel_reportHeaderBox3 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrControlStyle_HeaderText As DevExpress.XtraReports.UI.XRControlStyle
        Friend WithEvents XrControlStyle_HeaderBox3 As DevExpress.XtraReports.UI.XRControlStyle
        Friend WithEvents Detail As DevExpress.XtraReports.UI.DetailBand
        Friend WithEvents XrTable2 As DevExpress.XtraReports.UI.XRTable
        Friend WithEvents XrControlStyle_HeaderLine As DevExpress.XtraReports.UI.XRControlStyle
        Friend WithEvents XrControlStyle_Report_Column As DevExpress.XtraReports.UI.XRControlStyle
        Friend WithEvents TopMarginBand1 As DevExpress.XtraReports.UI.TopMarginBand
        Friend WithEvents XrTableRow2 As DevExpress.XtraReports.UI.XRTableRow
        Friend WithEvents XrTableCell4 As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents XrTableCell5 As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents XrTableCell6 As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents XrTableCell8 As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents XrTableCell2 As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents XrTableCell7 As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents XrTableCell3 As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents XrTable3 As DevExpress.XtraReports.UI.XRTable
        Friend WithEvents BottomMarginBand1 As DevExpress.XtraReports.UI.BottomMarginBand
        Friend WithEvents ReportHeader As DevExpress.XtraReports.UI.ReportHeaderBand
        Friend WithEvents XrLabel_ReportTitle As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLine_reportHeaderLine As DevExpress.XtraReports.UI.XRLine
        Friend WithEvents XrControlStyle_HeaderBox1 As DevExpress.XtraReports.UI.XRControlStyle
        Friend WithEvents XrControlStyle_HeaderBox2 As DevExpress.XtraReports.UI.XRControlStyle
        Friend WithEvents XrControlStyle_Report_Group As DevExpress.XtraReports.UI.XRControlStyle
        Friend WithEvents XrControlStyle_Report_Detail As DevExpress.XtraReports.UI.XRControlStyle
        Friend WithEvents XrTableCell1 As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents GroupFooter1 As DevExpress.XtraReports.UI.GroupFooterBand
        Friend WithEvents XrTableRow1 As DevExpress.XtraReports.UI.XRTableRow
        Friend WithEvents GroupHeader1 As DevExpress.XtraReports.UI.GroupHeaderBand
        Friend WithEvents XrTable1 As DevExpress.XtraReports.UI.XRTable
        Friend WithEvents XrControlStyle_HeaderSubTitle As DevExpress.XtraReports.UI.XRControlStyle
    End Class
End Namespace
