﻿Namespace Documents
    Public Class EnglishNonDMPDebts_3

        Public Sub New()
            MyBase.New()
            InitializeComponent()
            ' AddHandler XrLabel_detail_account_number.BeforePrint, AddressOf XrLabel_detail_account_number_BeforePrint
            ' AddHandler BeforePrint, AddressOf NonDMPDebts_BeforePrint
        End Sub

        Private Sub NonDMPDebts_BeforePrint(sender As Object, e As System.Drawing.Printing.PrintEventArgs)
            Dim rpt As DevExpress.XtraReports.UI.XtraReport = TryCast(sender, DevExpress.XtraReports.UI.XtraReport)
            If rpt Is Nothing Then
                Return
            End If

            Dim MasterRpt As DebtPlus.Interfaces.Client.IClient = TryCast(rpt.MasterReport, DebtPlus.Interfaces.Client.IClient)
            Dim ClientID As Int32 = -1
            If MasterRpt IsNot Nothing Then
                ClientID = MasterRpt.ClientId
            End If

            If ClientID < 0 Then
                Return
            End If

            Dim tableName As String = "rpt_PrintClient_other_debt"
            Dim ds As New System.Data.DataSet("ds")

            Using cn As New System.Data.SqlClient.SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
                cn.Open()
                Using cmd As New System.Data.SqlClient.SqlCommand()
                    cmd.Connection = cn
                    cmd.CommandText = "rpt_PrintClient_other_debt"
                    cmd.CommandType = System.Data.CommandType.StoredProcedure
                    cmd.CommandTimeout = 0
                    cmd.Parameters.Add("@client", System.Data.SqlDbType.Int).Value = ClientID

                    Using da As New System.Data.SqlClient.SqlDataAdapter(cmd)
                        da.Fill(ds, tableName)
                        rpt.DataSource = ds.Tables(tableName).DefaultView()
                    End Using
                End Using
            End Using
        End Sub

        ''' <summary>
        ''' Format the account number as the last 4 digits only.
        ''' </summary>
        Private Sub XrLabel_detail_account_number_BeforePrint(sender As Object, e As System.Drawing.Printing.PrintEventArgs)

            ' Find the report
            Dim lbl As DevExpress.XtraReports.UI.XRLabel = TryCast(sender, DevExpress.XtraReports.UI.XRLabel)
            If lbl Is Nothing Then
                Return
            End If

            Dim rpt As DevExpress.XtraReports.UI.XtraReport = TryCast(lbl.Report, DevExpress.XtraReports.UI.XtraReport)
            If rpt Is Nothing Then
                Return
            End If

            Dim drv As System.Data.DataRowView = TryCast(rpt.GetCurrentRow(), System.Data.DataRowView)
            If drv Is Nothing Then
                Return
            End If

            Dim accountNumber As String = If(DebtPlus.Utils.Nulls.v_String(drv("account_number")), String.Empty)
            If accountNumber.Length > 4 Then
                accountNumber = accountNumber.Substring(accountNumber.Length - 4)
            End If

            lbl.Text = accountNumber
        End Sub
    End Class
End Namespace
