﻿Namespace Documents
    Public Class CoverLetter

        Public Sub New()
            MyBase.New()
            InitializeComponent()
            ' AddHandler BeforePrint, AddressOf CoverLetter_BeforePrint
        End Sub

        Private Sub CoverLetter_BeforePrint(sender As Object, e As System.Drawing.Printing.PrintEventArgs)
            Dim rpt As DevExpress.XtraReports.UI.XtraReport = TryCast(sender, DevExpress.XtraReports.UI.XtraReport)
            If rpt Is Nothing Then
                Return
            End If

            Dim MasterRpt As DevExpress.XtraReports.UI.XtraReport = TryCast(rpt.MasterReport, DevExpress.XtraReports.UI.XtraReport)
            If MasterRpt Is Nothing Then
                Return
            End If

            Dim IMasterRpt As DebtPlus.Interfaces.Client.IClient = TryCast(MasterRpt, DebtPlus.Interfaces.Client.IClient)
            If IMasterRpt Is Nothing Then
                Return
            End If

            ' Find the current language value
            Dim language As Int32 = 1
            Dim parm As DevExpress.XtraReports.Parameters.Parameter = MasterRpt.Parameters("ParameterLanguage")
            If parm IsNot Nothing Then
                language = Convert.ToInt32(parm.Value)
                If language < 1 Then
                    language = 1
                End If
            End If

            Dim currentDate = System.DateTime.Now.Date
            Dim culture As System.Globalization.CultureInfo = DebtPlus.LINQ.AttributeType.SpecificCulture(language)
            If culture Is Nothing Then
                culture = System.Globalization.CultureInfo.CurrentCulture
            End If
            XrLabel_date.Text = System.String.Format(culture, "{0:D}", currentDate)

            Dim ClientID As Int32 = -1
            If MasterRpt IsNot Nothing Then
                ClientID = IMasterRpt.ClientId
            End If

            If ClientID < 0 Then
                Return
            End If

            Dim ds As New System.Data.DataSet("ds")
            Using cn As New System.Data.SqlClient.SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
                Using cmd As New System.Data.SqlClient.SqlCommand()
                    cmd.Connection = cn
                    cmd.CommandText = "SELECT [salutation],[counselor_name] FROM [view_client_address] WHERE [client]=@client"
                    cmd.CommandType = System.Data.CommandType.Text
                    cmd.Parameters.Add("@client", System.Data.SqlDbType.Int).Value = ClientID

                    Using da As New System.Data.SqlClient.SqlDataAdapter(cmd)
                        da.Fill(ds, "view_client_address")
                        rpt.DataSource = ds.Tables("view_client_address").DefaultView
                    End Using
                End Using
            End Using
        End Sub
    End Class
End Namespace
