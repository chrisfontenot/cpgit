﻿Namespace Documents
    <Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
    Partial Class CreditorList_5
        Inherits Template_5

        'XtraReport overrides dispose to clean up the component list.
        <System.Diagnostics.DebuggerNonUserCode()> _
        Protected Overrides Sub Dispose(ByVal disposing As Boolean)
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
            MyBase.Dispose(disposing)
        End Sub

        'Required by the Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Designer
        'It can be modified using the Designer.  
        'Do not modify it using the code editor.
        <System.Diagnostics.DebuggerStepThrough()> _
        Private Sub InitializeComponent()
            Dim XrSummary1 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary()
            Dim XrSummary2 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary()
            Dim XrSummary3 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary()
            Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(CreditorList_5))
            Me.GroupHeader1 = New DevExpress.XtraReports.UI.GroupHeaderBand()
            Me.XrTable1 = New DevExpress.XtraReports.UI.XRTable()
            Me.XrTableRow1 = New DevExpress.XtraReports.UI.XRTableRow()
            Me.XrTableCell25 = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableCell33 = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableCell29 = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableCell21 = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableRow4 = New DevExpress.XtraReports.UI.XRTableRow()
            Me.XrTableCell10 = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableCell14 = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableCell26 = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableCell11 = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableCell18 = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableCell34 = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableCell12 = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableCell30 = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableCell22 = New DevExpress.XtraReports.UI.XRTableCell()
            Me.GroupFooter1 = New DevExpress.XtraReports.UI.GroupFooterBand()
            Me.XrTable3 = New DevExpress.XtraReports.UI.XRTable()
            Me.XrTableRow3 = New DevExpress.XtraReports.UI.XRTableRow()
            Me.XrTableCell7 = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableCell_total_balance = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableCell_total_dmp_payment = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableCell_total_non_dmp_payment = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableCell24 = New DevExpress.XtraReports.UI.XRTableCell()
            Me.ReportFooter = New DevExpress.XtraReports.UI.ReportFooterBand()
            Me.XrTable2 = New DevExpress.XtraReports.UI.XRTable()
            Me.XrTableRow2 = New DevExpress.XtraReports.UI.XRTableRow()
            Me.XrTableCell_creditor_name = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableCell_account_number = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableCell_balance = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableCell_apr = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableCell_dmp_payment = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableCell_non_dmp_rate = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableCell_non_dmp_payment = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableCell_payout = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableCell_creditor = New DevExpress.XtraReports.UI.XRTableCell()
            CType(Me.XrTable1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.XrTable3, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.XrTable2, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
            '
            'XrLabel_reportHeaderBox3
            '
            Me.XrLabel_reportHeaderBox3.LocationFloat = New DevExpress.Utils.PointFloat(504.71!, 64.19!)
            Me.XrLabel_reportHeaderBox3.StylePriority.UseBackColor = False
            Me.XrLabel_reportHeaderBox3.StylePriority.UseBorderColor = False
            Me.XrLabel_reportHeaderBox3.StylePriority.UseForeColor = False
            '
            'XrLabel_reportHeaderBox2
            '
            Me.XrLabel_reportHeaderBox2.LocationFloat = New DevExpress.Utils.PointFloat(313.0!, 64.19!)
            Me.XrLabel_reportHeaderBox2.StylePriority.UseBackColor = False
            Me.XrLabel_reportHeaderBox2.StylePriority.UseBorderColor = False
            Me.XrLabel_reportHeaderBox2.StylePriority.UseForeColor = False
            '
            'XrLabel_reportHeaderBox1
            '
            Me.XrLabel_reportHeaderBox1.LocationFloat = New DevExpress.Utils.PointFloat(121.2917!, 64.19!)
            Me.XrLabel_reportHeaderBox1.StylePriority.UseBackColor = False
            Me.XrLabel_reportHeaderBox1.StylePriority.UseBorderColor = False
            Me.XrLabel_reportHeaderBox1.StylePriority.UseForeColor = False
            '
            'XrLabel_pageHeaderBox3
            '
            Me.XrLabel_pageHeaderBox3.LocationFloat = New DevExpress.Utils.PointFloat(504.7083!, 64.19167!)
            Me.XrLabel_pageHeaderBox3.StylePriority.UseBackColor = False
            Me.XrLabel_pageHeaderBox3.StylePriority.UseBorderColor = False
            Me.XrLabel_pageHeaderBox3.StylePriority.UseForeColor = False
            '
            'XrLabel_pageHeaderBox2
            '
            Me.XrLabel_pageHeaderBox2.LocationFloat = New DevExpress.Utils.PointFloat(313.0!, 64.19167!)
            Me.XrLabel_pageHeaderBox2.StylePriority.UseBackColor = False
            Me.XrLabel_pageHeaderBox2.StylePriority.UseBorderColor = False
            Me.XrLabel_pageHeaderBox2.StylePriority.UseForeColor = False
            '
            'XrLabel_pageHeaderBox1
            '
            Me.XrLabel_pageHeaderBox1.LocationFloat = New DevExpress.Utils.PointFloat(121.2917!, 64.19167!)
            Me.XrLabel_pageHeaderBox1.StylePriority.UseBackColor = False
            Me.XrLabel_pageHeaderBox1.StylePriority.UseBorderColor = False
            Me.XrLabel_pageHeaderBox1.StylePriority.UseForeColor = False
            '
            'XrLabel_ReportTitle
            '
            Me.XrLabel_ReportTitle.SizeF = New System.Drawing.SizeF(780.0!, 50.0!)
            Me.XrLabel_ReportTitle.StylePriority.UseFont = False
            Me.XrLabel_ReportTitle.StylePriority.UseForeColor = False
            Me.XrLabel_ReportTitle.StylePriority.UseTextAlignment = False
            Me.XrLabel_ReportTitle.Text = "CREDITOR LIST"
            '
            'XrLine_reportHeaderLine
            '
            Me.XrLine_reportHeaderLine.StylePriority.UseBorderColor = False
            Me.XrLine_reportHeaderLine.StylePriority.UseForeColor = False
            '
            'XrLine_pageHeaderLine
            '
            Me.XrLine_pageHeaderLine.StylePriority.UseBorderColor = False
            Me.XrLine_pageHeaderLine.StylePriority.UseForeColor = False
            '
            'XrLabel_pageHeader_Title
            '
            Me.XrLabel_pageHeader_Title.LocationFloat = New DevExpress.Utils.PointFloat(10.00001!, 0.0!)
            Me.XrLabel_pageHeader_Title.SizeF = New System.Drawing.SizeF(779.9999!, 50.0!)
            Me.XrLabel_pageHeader_Title.StylePriority.UseFont = False
            Me.XrLabel_pageHeader_Title.StylePriority.UseForeColor = False
            Me.XrLabel_pageHeader_Title.StylePriority.UseTextAlignment = False
            Me.XrLabel_pageHeader_Title.Text = "CREDITOR LIST (Continued)"
            '
            'ReportHeader
            '
            Me.ReportHeader.HeightF = 85.0!
            '
            'PageHeader
            '
            Me.PageHeader.HeightF = 88.12501!
            Me.PageHeader.Visible = True
            '
            'Detail
            '
            Me.Detail.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrTable2})
            Me.Detail.HeightF = 21.88!
            '
            'GroupHeader1
            '
            Me.GroupHeader1.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrTable1})
            Me.GroupHeader1.HeightF = 43.76!
            Me.GroupHeader1.Name = "GroupHeader1"
            Me.GroupHeader1.RepeatEveryPage = True
            '
            'XrTable1
            '
            Me.XrTable1.AnchorVertical = DevExpress.XtraReports.UI.VerticalAnchorStyles.Bottom
            Me.XrTable1.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
            Me.XrTable1.Name = "XrTable1"
            Me.XrTable1.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrTable1.Rows.AddRange(New DevExpress.XtraReports.UI.XRTableRow() {Me.XrTableRow1, Me.XrTableRow4})
            Me.XrTable1.SizeF = New System.Drawing.SizeF(789.9998!, 43.76!)
            Me.XrTable1.StylePriority.UsePadding = False
            '
            'XrTableRow1
            '
            Me.XrTableRow1.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.XrTableCell25, Me.XrTableCell33, Me.XrTableCell29, Me.XrTableCell21})
            Me.XrTableRow1.Name = "XrTableRow1"
            Me.XrTableRow1.Weight = 0.91325204194420406R
            '
            'XrTableCell25
            '
            Me.XrTableCell25.CanGrow = False
            Me.XrTableCell25.Name = "XrTableCell25"
            Me.XrTableCell25.Weight = 1.1056141579035486R
            '
            'XrTableCell33
            '
            Me.XrTableCell33.Borders = CType((DevExpress.XtraPrinting.BorderSide.Right Or DevExpress.XtraPrinting.BorderSide.Bottom), DevExpress.XtraPrinting.BorderSide)
            Me.XrTableCell33.CanGrow = False
            Me.XrTableCell33.Name = "XrTableCell33"
            Me.XrTableCell33.StyleName = "XrControlStyle_Report_Column"
            Me.XrTableCell33.StylePriority.UseBorders = False
            Me.XrTableCell33.StylePriority.UseTextAlignment = False
            Me.XrTableCell33.Text = "PLAN"
            Me.XrTableCell33.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
            Me.XrTableCell33.Weight = 0.57753163456744638R
            '
            'XrTableCell29
            '
            Me.XrTableCell29.Borders = CType((DevExpress.XtraPrinting.BorderSide.Left Or DevExpress.XtraPrinting.BorderSide.Bottom), DevExpress.XtraPrinting.BorderSide)
            Me.XrTableCell29.CanGrow = False
            Me.XrTableCell29.Name = "XrTableCell29"
            Me.XrTableCell29.StyleName = "XrControlStyle_Report_Column"
            Me.XrTableCell29.StylePriority.UseBorders = False
            Me.XrTableCell29.StylePriority.UseTextAlignment = False
            Me.XrTableCell29.Text = "SELF"
            Me.XrTableCell29.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
            Me.XrTableCell29.Weight = 0.58742485624092733R
            '
            'XrTableCell21
            '
            Me.XrTableCell21.CanGrow = False
            Me.XrTableCell21.Name = "XrTableCell21"
            Me.XrTableCell21.Weight = 0.72942935128807773R
            '
            'XrTableRow4
            '
            Me.XrTableRow4.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.XrTableCell10, Me.XrTableCell14, Me.XrTableCell26, Me.XrTableCell11, Me.XrTableCell18, Me.XrTableCell34, Me.XrTableCell12, Me.XrTableCell30, Me.XrTableCell22})
            Me.XrTableRow4.Name = "XrTableRow4"
            Me.XrTableRow4.StyleName = "XrControlStyle_Report_Group"
            Me.XrTableRow4.Weight = 0.91325204194420417R
            '
            'XrTableCell10
            '
            Me.XrTableCell10.CanGrow = False
            Me.XrTableCell10.Name = "XrTableCell10"
            Me.XrTableCell10.Padding = New DevExpress.XtraPrinting.PaddingInfo(5, 2, 0, 0, 100.0!)
            Me.XrTableCell10.StyleName = "XrControlStyle_Report_Group"
            Me.XrTableCell10.StylePriority.UsePadding = False
            Me.XrTableCell10.Text = "Creditor"
            Me.XrTableCell10.Weight = 0.54786734422916716R
            '
            'XrTableCell14
            '
            Me.XrTableCell14.CanGrow = False
            Me.XrTableCell14.Name = "XrTableCell14"
            Me.XrTableCell14.StylePriority.UseTextAlignment = False
            Me.XrTableCell14.Text = "Acct #"
            Me.XrTableCell14.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
            Me.XrTableCell14.Weight = 0.24920887989105145R
            '
            'XrTableCell26
            '
            Me.XrTableCell26.CanGrow = False
            Me.XrTableCell26.Name = "XrTableCell26"
            Me.XrTableCell26.StylePriority.UseTextAlignment = False
            Me.XrTableCell26.Text = "Balance"
            Me.XrTableCell26.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrTableCell26.Weight = 0.30853793378332994R
            '
            'XrTableCell11
            '
            Me.XrTableCell11.CanGrow = False
            Me.XrTableCell11.Name = "XrTableCell11"
            Me.XrTableCell11.StylePriority.UseTextAlignment = False
            Me.XrTableCell11.Text = "Rate"
            Me.XrTableCell11.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrTableCell11.Weight = 0.25910126136519296R
            '
            'XrTableCell18
            '
            Me.XrTableCell18.CanGrow = False
            Me.XrTableCell18.Name = "XrTableCell18"
            Me.XrTableCell18.StylePriority.UseTextAlignment = False
            Me.XrTableCell18.Text = "Payment"
            Me.XrTableCell18.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrTableCell18.Weight = 0.31843070155601827R
            '
            'XrTableCell34
            '
            Me.XrTableCell34.CanGrow = False
            Me.XrTableCell34.Name = "XrTableCell34"
            Me.XrTableCell34.StylePriority.UseTextAlignment = False
            Me.XrTableCell34.Text = "Rate"
            Me.XrTableCell34.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrTableCell34.Weight = 0.25119018582273511R
            '
            'XrTableCell12
            '
            Me.XrTableCell12.CanGrow = False
            Me.XrTableCell12.Name = "XrTableCell12"
            Me.XrTableCell12.StylePriority.UseTextAlignment = False
            Me.XrTableCell12.Text = "Payment"
            Me.XrTableCell12.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrTableCell12.Weight = 0.33623388816363486R
            '
            'XrTableCell30
            '
            Me.XrTableCell30.CanGrow = False
            Me.XrTableCell30.Name = "XrTableCell30"
            Me.XrTableCell30.StylePriority.UseTextAlignment = False
            Me.XrTableCell30.Text = "Payout"
            Me.XrTableCell30.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrTableCell30.Weight = 0.30221470944516171R
            '
            'XrTableCell22
            '
            Me.XrTableCell22.CanGrow = False
            Me.XrTableCell22.Name = "XrTableCell22"
            Me.XrTableCell22.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 5, 0, 0, 100.0!)
            Me.XrTableCell22.StylePriority.UsePadding = False
            Me.XrTableCell22.StylePriority.UseTextAlignment = False
            Me.XrTableCell22.Text = "Creditor ID"
            Me.XrTableCell22.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrTableCell22.Weight = 0.42721509574370853R
            '
            'GroupFooter1
            '
            Me.GroupFooter1.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrTable3})
            Me.GroupFooter1.GroupUnion = DevExpress.XtraReports.UI.GroupFooterUnion.WithLastDetail
            Me.GroupFooter1.HeightF = 21.88!
            Me.GroupFooter1.Name = "GroupFooter1"
            '
            'XrTable3
            '
            Me.XrTable3.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.XrTable3.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
            Me.XrTable3.Name = "XrTable3"
            Me.XrTable3.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrTable3.Rows.AddRange(New DevExpress.XtraReports.UI.XRTableRow() {Me.XrTableRow3})
            Me.XrTable3.SizeF = New System.Drawing.SizeF(790.0!, 21.88!)
            Me.XrTable3.StylePriority.UseFont = False
            Me.XrTable3.StylePriority.UsePadding = False
            '
            'XrTableRow3
            '
            Me.XrTableRow3.Borders = DevExpress.XtraPrinting.BorderSide.None
            Me.XrTableRow3.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.XrTableCell7, Me.XrTableCell_total_balance, Me.XrTableCell_total_dmp_payment, Me.XrTableCell_total_non_dmp_payment, Me.XrTableCell24})
            Me.XrTableRow3.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.XrTableRow3.Name = "XrTableRow3"
            Me.XrTableRow3.StyleName = "XrControlStyle_Report_Total"
            Me.XrTableRow3.StylePriority.UseBorders = False
            Me.XrTableRow3.StylePriority.UseFont = False
            Me.XrTableRow3.StylePriority.UseTextAlignment = False
            Me.XrTableRow3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            Me.XrTableRow3.Weight = 0.95130431133767834R
            '
            'XrTableCell7
            '
            Me.XrTableCell7.Name = "XrTableCell7"
            Me.XrTableCell7.Padding = New DevExpress.XtraPrinting.PaddingInfo(5, 2, 0, 0, 100.0!)
            Me.XrTableCell7.StylePriority.UsePadding = False
            Me.XrTableCell7.Text = "TOTALS"
            Me.XrTableCell7.Weight = 0.54786737689977771R
            '
            'XrTableCell_total_balance
            '
            Me.XrTableCell_total_balance.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "balance")})
            Me.XrTableCell_total_balance.Name = "XrTableCell_total_balance"
            Me.XrTableCell_total_balance.StylePriority.UseTextAlignment = False
            XrSummary1.FormatString = "{0:c}"
            XrSummary1.Running = DevExpress.XtraReports.UI.SummaryRunning.Report
            Me.XrTableCell_total_balance.Summary = XrSummary1
            Me.XrTableCell_total_balance.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrTableCell_total_balance.Weight = 0.557746847108277R
            '
            'XrTableCell_total_dmp_payment
            '
            Me.XrTableCell_total_dmp_payment.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "dmp_payment")})
            Me.XrTableCell_total_dmp_payment.Name = "XrTableCell_total_dmp_payment"
            Me.XrTableCell_total_dmp_payment.StylePriority.UseTextAlignment = False
            XrSummary2.FormatString = "{0:c}"
            XrSummary2.Running = DevExpress.XtraReports.UI.SummaryRunning.Report
            Me.XrTableCell_total_dmp_payment.Summary = XrSummary2
            Me.XrTableCell_total_dmp_payment.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrTableCell_total_dmp_payment.Weight = 0.57753186267920009R
            '
            'XrTableCell_total_non_dmp_payment
            '
            Me.XrTableCell_total_non_dmp_payment.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "non_dmp_payment")})
            Me.XrTableCell_total_non_dmp_payment.Name = "XrTableCell_total_non_dmp_payment"
            Me.XrTableCell_total_non_dmp_payment.StylePriority.UseTextAlignment = False
            XrSummary3.FormatString = "{0:c}"
            XrSummary3.Running = DevExpress.XtraReports.UI.SummaryRunning.Report
            Me.XrTableCell_total_non_dmp_payment.Summary = XrSummary3
            Me.XrTableCell_total_non_dmp_payment.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrTableCell_total_non_dmp_payment.Weight = 0.587424080740729R
            '
            'XrTableCell24
            '
            Me.XrTableCell24.Name = "XrTableCell24"
            Me.XrTableCell24.Weight = 0.7294298325720161R
            '
            'ReportFooter
            '
            Me.ReportFooter.HeightF = 1.041667!
            Me.ReportFooter.Name = "ReportFooter"
            '
            'XrTable2
            '
            Me.XrTable2.AnchorVertical = CType((DevExpress.XtraReports.UI.VerticalAnchorStyles.Top Or DevExpress.XtraReports.UI.VerticalAnchorStyles.Bottom), DevExpress.XtraReports.UI.VerticalAnchorStyles)
            Me.XrTable2.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.XrTable2.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
            Me.XrTable2.Name = "XrTable2"
            Me.XrTable2.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrTable2.Rows.AddRange(New DevExpress.XtraReports.UI.XRTableRow() {Me.XrTableRow2})
            Me.XrTable2.SizeF = New System.Drawing.SizeF(790.0!, 20.76!)
            Me.XrTable2.StylePriority.UseFont = False
            Me.XrTable2.StylePriority.UsePadding = False
            '
            'XrTableRow2
            '
            Me.XrTableRow2.BorderColor = System.Drawing.Color.White
            Me.XrTableRow2.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.XrTableCell_creditor_name, Me.XrTableCell_account_number, Me.XrTableCell_balance, Me.XrTableCell_apr, Me.XrTableCell_dmp_payment, Me.XrTableCell_non_dmp_rate, Me.XrTableCell_non_dmp_payment, Me.XrTableCell_payout, Me.XrTableCell_creditor})
            Me.XrTableRow2.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.XrTableRow2.Name = "XrTableRow2"
            Me.XrTableRow2.StyleName = "XrControlStyle_Report_Detail"
            Me.XrTableRow2.StylePriority.UseBorderColor = False
            Me.XrTableRow2.StylePriority.UseFont = False
            Me.XrTableRow2.StylePriority.UseTextAlignment = False
            Me.XrTableRow2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            Me.XrTableRow2.Weight = 0.45583313471749043R
            '
            'XrTableCell_creditor_name
            '
            Me.XrTableCell_creditor_name.CanGrow = False
            Me.XrTableCell_creditor_name.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "creditor_name")})
            Me.XrTableCell_creditor_name.Name = "XrTableCell_creditor_name"
            Me.XrTableCell_creditor_name.Padding = New DevExpress.XtraPrinting.PaddingInfo(5, 2, 0, 0, 100.0!)
            Me.XrTableCell_creditor_name.StylePriority.UsePadding = False
            Me.XrTableCell_creditor_name.Weight = 0.54786737689977771R
            '
            'XrTableCell_account_number
            '
            Me.XrTableCell_account_number.CanGrow = False
            Me.XrTableCell_account_number.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "account_number")})
            Me.XrTableCell_account_number.Name = "XrTableCell_account_number"
            Me.XrTableCell_account_number.StylePriority.UseTextAlignment = False
            Me.XrTableCell_account_number.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
            Me.XrTableCell_account_number.Weight = 0.24920893777471623R
            '
            'XrTableCell_balance
            '
            Me.XrTableCell_balance.CanGrow = False
            Me.XrTableCell_balance.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "balance", "{0:c}")})
            Me.XrTableCell_balance.Name = "XrTableCell_balance"
            Me.XrTableCell_balance.StylePriority.UseTextAlignment = False
            Me.XrTableCell_balance.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrTableCell_balance.Weight = 0.30853790933356079R
            '
            'XrTableCell_apr
            '
            Me.XrTableCell_apr.CanGrow = False
            Me.XrTableCell_apr.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "dmp_interest", "{0:p}")})
            Me.XrTableCell_apr.Name = "XrTableCell_apr"
            Me.XrTableCell_apr.StylePriority.UseTextAlignment = False
            Me.XrTableCell_apr.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrTableCell_apr.Weight = 0.25910115583624527R
            '
            'XrTableCell_dmp_payment
            '
            Me.XrTableCell_dmp_payment.CanGrow = False
            Me.XrTableCell_dmp_payment.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "dmp_payment", "{0:c}")})
            Me.XrTableCell_dmp_payment.Name = "XrTableCell_dmp_payment"
            Me.XrTableCell_dmp_payment.StylePriority.UseTextAlignment = False
            Me.XrTableCell_dmp_payment.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrTableCell_dmp_payment.Weight = 0.31843070684295482R
            '
            'XrTableCell_non_dmp_rate
            '
            Me.XrTableCell_non_dmp_rate.CanGrow = False
            Me.XrTableCell_non_dmp_rate.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "non_dmp_interest", "{0:p}")})
            Me.XrTableCell_non_dmp_rate.Name = "XrTableCell_non_dmp_rate"
            Me.XrTableCell_non_dmp_rate.StylePriority.UseTextAlignment = False
            Me.XrTableCell_non_dmp_rate.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrTableCell_non_dmp_rate.Weight = 0.25119018591468856R
            '
            'XrTableCell_non_dmp_payment
            '
            Me.XrTableCell_non_dmp_payment.CanGrow = False
            Me.XrTableCell_non_dmp_payment.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "non_dmp_payment", "{0:c}")})
            Me.XrTableCell_non_dmp_payment.Name = "XrTableCell_non_dmp_payment"
            Me.XrTableCell_non_dmp_payment.StylePriority.UseTextAlignment = False
            Me.XrTableCell_non_dmp_payment.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrTableCell_non_dmp_payment.Weight = 0.33623389482604049R
            '
            'XrTableCell_payout
            '
            Me.XrTableCell_payout.CanGrow = False
            Me.XrTableCell_payout.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "dmp_payout", "{0:MMM/yy}")})
            Me.XrTableCell_payout.Name = "XrTableCell_payout"
            Me.XrTableCell_payout.StylePriority.UseTextAlignment = False
            Me.XrTableCell_payout.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrTableCell_payout.Weight = 0.30221474245164859R
            '
            'XrTableCell_creditor
            '
            Me.XrTableCell_creditor.CanGrow = False
            Me.XrTableCell_creditor.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "creditor")})
            Me.XrTableCell_creditor.Name = "XrTableCell_creditor"
            Me.XrTableCell_creditor.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 5, 0, 0, 100.0!)
            Me.XrTableCell_creditor.StylePriority.UsePadding = False
            Me.XrTableCell_creditor.StylePriority.UseTextAlignment = False
            Me.XrTableCell_creditor.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrTableCell_creditor.Weight = 0.42721509012036751R
            '
            'CreditorList_5
            '
            Me.Bands.AddRange(New DevExpress.XtraReports.UI.Band() {Me.Detail, Me.TopMarginBand1, Me.BottomMarginBand1, Me.ReportHeader, Me.PageHeader, Me.GroupHeader1, Me.GroupFooter1, Me.ReportFooter})
            Me.ReportPrintOptions.PrintOnEmptyDataSource = False
            Me.ScriptLanguage = DevExpress.XtraReports.ScriptLanguage.VisualBasic
            Me.Scripts.OnBeforePrint = "CreditorList_BeforePrint"
            Me.ScriptsSource = resources.GetString("$this.ScriptsSource")
            Me.StyleSheet.AddRange(New DevExpress.XtraReports.UI.XRControlStyle() {Me.XrControlStyle_HeaderText, Me.XrControlStyle_HeaderLine, Me.XrControlStyle_HeaderBox1, Me.XrControlStyle_HeaderBox2, Me.XrControlStyle_HeaderBox3, Me.XrControlStyle_Report_Column, Me.XrControlStyle_Report_Group, Me.XrControlStyle_Report_Detail, Me.XrControlStyle_Report_Total, Me.XrControlStyle_Footer, Me.XrControlStyle_HeaderSubTitle})
            Me.Version = "11.2"
            Me.Controls.SetChildIndex(Me.ReportFooter, 0)
            Me.Controls.SetChildIndex(Me.GroupFooter1, 0)
            Me.Controls.SetChildIndex(Me.GroupHeader1, 0)
            Me.Controls.SetChildIndex(Me.PageHeader, 0)
            Me.Controls.SetChildIndex(Me.ReportHeader, 0)
            Me.Controls.SetChildIndex(Me.BottomMarginBand1, 0)
            Me.Controls.SetChildIndex(Me.TopMarginBand1, 0)
            Me.Controls.SetChildIndex(Me.Detail, 0)
            CType(Me.XrTable1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.XrTable3, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.XrTable2, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me, System.ComponentModel.ISupportInitialize).EndInit()

        End Sub
        Protected GroupHeader1 As DevExpress.XtraReports.UI.GroupHeaderBand
        Protected GroupFooter1 As DevExpress.XtraReports.UI.GroupFooterBand
        Protected ReportFooter As DevExpress.XtraReports.UI.ReportFooterBand
        Protected XrTable2 As DevExpress.XtraReports.UI.XRTable
        Protected XrTableRow2 As DevExpress.XtraReports.UI.XRTableRow
        Protected XrTableCell_creditor_name As DevExpress.XtraReports.UI.XRTableCell
        Protected XrTableCell_account_number As DevExpress.XtraReports.UI.XRTableCell
        Protected XrTableCell_balance As DevExpress.XtraReports.UI.XRTableCell
        Protected XrTableCell_apr As DevExpress.XtraReports.UI.XRTableCell
        Protected XrTableCell_dmp_payment As DevExpress.XtraReports.UI.XRTableCell
        Protected XrTableCell_non_dmp_rate As DevExpress.XtraReports.UI.XRTableCell
        Protected XrTableCell_non_dmp_payment As DevExpress.XtraReports.UI.XRTableCell
        Protected XrTableCell_payout As DevExpress.XtraReports.UI.XRTableCell
        Protected XrTableCell_creditor As DevExpress.XtraReports.UI.XRTableCell
        Protected XrTable1 As DevExpress.XtraReports.UI.XRTable
        Protected XrTableRow1 As DevExpress.XtraReports.UI.XRTableRow
        Protected XrTableCell25 As DevExpress.XtraReports.UI.XRTableCell
        Protected XrTableCell33 As DevExpress.XtraReports.UI.XRTableCell
        Protected XrTableCell29 As DevExpress.XtraReports.UI.XRTableCell
        Protected XrTableCell21 As DevExpress.XtraReports.UI.XRTableCell
        Protected XrTableRow4 As DevExpress.XtraReports.UI.XRTableRow
        Protected XrTableCell10 As DevExpress.XtraReports.UI.XRTableCell
        Protected XrTableCell14 As DevExpress.XtraReports.UI.XRTableCell
        Protected XrTableCell26 As DevExpress.XtraReports.UI.XRTableCell
        Protected XrTableCell11 As DevExpress.XtraReports.UI.XRTableCell
        Protected XrTableCell18 As DevExpress.XtraReports.UI.XRTableCell
        Protected XrTableCell34 As DevExpress.XtraReports.UI.XRTableCell
        Protected XrTableCell12 As DevExpress.XtraReports.UI.XRTableCell
        Protected XrTableCell30 As DevExpress.XtraReports.UI.XRTableCell
        Protected XrTableCell22 As DevExpress.XtraReports.UI.XRTableCell
        Protected XrTable3 As DevExpress.XtraReports.UI.XRTable
        Protected XrTableRow3 As DevExpress.XtraReports.UI.XRTableRow
        Protected XrTableCell7 As DevExpress.XtraReports.UI.XRTableCell
        Protected XrTableCell_total_balance As DevExpress.XtraReports.UI.XRTableCell
        Protected XrTableCell_total_dmp_payment As DevExpress.XtraReports.UI.XRTableCell
        Protected XrTableCell_total_non_dmp_payment As DevExpress.XtraReports.UI.XRTableCell
        Protected XrTableCell24 As DevExpress.XtraReports.UI.XRTableCell
    End Class
End Namespace
