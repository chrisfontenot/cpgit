﻿Namespace Documents
    <Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
    Partial Class DMPEnrollment_4
        Inherits Template_4

        'XtraReport overrides dispose to clean up the component list.
        <System.Diagnostics.DebuggerNonUserCode()> _
        Protected Overrides Sub Dispose(ByVal disposing As Boolean)
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
            MyBase.Dispose(disposing)
        End Sub

        'Required by the Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Designer
        'It can be modified using the Designer.  
        'Do not modify it using the code editor.
        <System.Diagnostics.DebuggerStepThrough()> _
        Private Sub InitializeComponent()
            CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
            '
            'XrLabel_subTitle
            '
            Me.XrLabel_subTitle.LocationFloat = New DevExpress.Utils.PointFloat(10.00001!, 112.8333!)
            Me.XrLabel_subTitle.SizeF = New System.Drawing.SizeF(564.5834!, 22.99998!)
            Me.XrLabel_subTitle.StylePriority.UseFont = False
            Me.XrLabel_subTitle.StylePriority.UseForeColor = False
            Me.XrLabel_subTitle.Text = "Please return the following items to begin your DMP:"
            '
            'XrLabel_reportHeaderBox3
            '
            Me.XrLabel_reportHeaderBox3.StylePriority.UseBackColor = False
            Me.XrLabel_reportHeaderBox3.StylePriority.UseBorderColor = False
            Me.XrLabel_reportHeaderBox3.StylePriority.UseForeColor = False
            '
            'XrLabel_reportHeaderBox2
            '
            Me.XrLabel_reportHeaderBox2.StylePriority.UseBackColor = False
            Me.XrLabel_reportHeaderBox2.StylePriority.UseBorderColor = False
            Me.XrLabel_reportHeaderBox2.StylePriority.UseForeColor = False
            '
            'XrLabel_reportHeaderBox1
            '
            Me.XrLabel_reportHeaderBox1.StylePriority.UseBackColor = False
            Me.XrLabel_reportHeaderBox1.StylePriority.UseBorderColor = False
            Me.XrLabel_reportHeaderBox1.StylePriority.UseForeColor = False
            '
            'XrLabel_ReportTitle
            '
            Me.XrLabel_ReportTitle.StylePriority.UseFont = False
            Me.XrLabel_ReportTitle.StylePriority.UseForeColor = False
            Me.XrLabel_ReportTitle.Text = "DMP ENROLLMENT" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "CHECKLIST"
            '
            'XrLine_reportHeaderLine
            '
            Me.XrLine_reportHeaderLine.StylePriority.UseBorderColor = False
            Me.XrLine_reportHeaderLine.StylePriority.UseForeColor = False
            '
            'XrLabel_pageHeader_Title
            '
            Me.XrLabel_pageHeader_Title.Text = "DMP ENROLLMENT" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "CHECKLIST (Continued)"
            '
            'DMPEnrollment_4
            '
            Me.Bands.AddRange(New DevExpress.XtraReports.UI.Band() {Me.TopMarginBand1, Me.Detail, Me.BottomMarginBand1, Me.ReportHeader, Me.PageHeader})
            Me.StyleSheet.AddRange(New DevExpress.XtraReports.UI.XRControlStyle() {Me.XrControlStyle_HeaderText, Me.XrControlStyle_HeaderLine, Me.XrControlStyle_HeaderBox1, Me.XrControlStyle_HeaderBox2, Me.XrControlStyle_HeaderBox3, Me.XrControlStyle_Report_Column, Me.XrControlStyle_Report_Group, Me.XrControlStyle_Report_Detail, Me.XrControlStyle_Report_Total, Me.XrControlStyle_Footer, Me.XrControlStyle_HeaderSubTitle})
            Me.Version = "11.2"
            CType(Me, System.ComponentModel.ISupportInitialize).EndInit()

        End Sub
    End Class
End Namespace
