﻿Namespace Documents
    Public Class EnglishFinancialNetWorth_3
        Public Sub New()
            MyBase.New()
            InitializeComponent()
            ' AddHandler BeforePrint, AddressOf EnglishFinancialNetWorth_BeforePrint
            ' AddHandler XrTableCell_l_description.BeforePrint, AddressOf XrTableCell_l_description_BeforePrint
            ' AddHandler XrTableCell_r_description.BeforePrint, AddressOf XrTableCell_r_description_BeforePrint
        End Sub

        Private Sub EnglishFinancialNetWorth_BeforePrint(sender As Object, e As System.Drawing.Printing.PrintEventArgs)
            Dim rpt As DevExpress.XtraReports.UI.XtraReport = TryCast(sender, DevExpress.XtraReports.UI.XtraReport)
            If rpt Is Nothing Then
                Return
            End If

            Dim MasterRpt As DebtPlus.Interfaces.Client.IClient = TryCast(rpt.MasterReport, DebtPlus.Interfaces.Client.IClient)
            Dim ClientID As Int32 = -1
            If MasterRpt IsNot Nothing Then
                ClientID = MasterRpt.ClientId
            End If

            If ClientID < 0 Then
                Return
            End If

            Dim tableName As String = "rpt_Financial_Net_Worth"
            Dim ds As New System.Data.DataSet("ds")

            Using cn As New System.Data.SqlClient.SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
                cn.Open()
                Using cmd As New System.Data.SqlClient.SqlCommand()
                    cmd.Connection = cn
                    cmd.CommandText = "rpt_Financial_Net_Worth"
                    cmd.CommandType = System.Data.CommandType.StoredProcedure
                    cmd.CommandTimeout = 0
                    cmd.Parameters.Add("@client", System.Data.SqlDbType.Int).Value = ClientID

                    Using da As New System.Data.SqlClient.SqlDataAdapter(cmd)
                        da.Fill(ds, tableName)
                        rpt.DataSource = ds.Tables(tableName).DefaultView()
                    End Using
                End Using
            End Using
        End Sub

        Private Sub XrTableCell_l_description_BeforePrint(sender As Object, e As System.Drawing.Printing.PrintEventArgs)

            ' Find the label
            Dim cell As DevExpress.XtraReports.UI.XRTableCell = TryCast(sender, DevExpress.XtraReports.UI.XRTableCell)
            If cell Is Nothing Then
                Return
            End If

            ' Find the report from the label
            Dim rpt As DevExpress.XtraReports.UI.XtraReport = TryCast(cell.Report, DevExpress.XtraReports.UI.XtraReport)
            Dim record As System.Data.DataRowView = TryCast(rpt.GetCurrentRow(), System.Data.DataRowView)
            If record Is Nothing Then
                Return
            End If

            If DebtPlus.Utils.Nulls.v_Boolean(record("l_heading")).GetValueOrDefault(False) Then
                cell.Padding = New DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 100.0!)
            Else
                cell.Padding = New DevExpress.XtraPrinting.PaddingInfo(25, 5, 0, 0, 100.0!)
            End If
        End Sub

        Private Sub XrTableCell_r_description_BeforePrint(sender As Object, e As System.Drawing.Printing.PrintEventArgs)
            ' Find the label
            Dim cell As DevExpress.XtraReports.UI.XRTableCell = TryCast(sender, DevExpress.XtraReports.UI.XRTableCell)
            If cell Is Nothing Then
                Return
            End If

            ' Find the report from the label
            Dim rpt As DevExpress.XtraReports.UI.XtraReport = TryCast(cell.Report, DevExpress.XtraReports.UI.XtraReport)
            Dim record As System.Data.DataRowView = TryCast(rpt.GetCurrentRow(), System.Data.DataRowView)
            If record Is Nothing Then
                Return
            End If

            If DebtPlus.Utils.Nulls.v_Boolean(record("r_heading")).GetValueOrDefault(False) Then
                cell.Padding = New DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 100.0!)
            Else
                cell.Padding = New DevExpress.XtraPrinting.PaddingInfo(25, 5, 0, 0, 100.0!)
            End If
        End Sub
    End Class
End Namespace
