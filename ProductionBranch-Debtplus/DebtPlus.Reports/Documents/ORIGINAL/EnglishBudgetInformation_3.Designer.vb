﻿Namespace Documents
    <Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
    Partial Class EnglishBudgetInformation_3
        Inherits Template_3

        'XtraReport overrides dispose to clean up the component list.
        <System.Diagnostics.DebuggerNonUserCode()> _
        Protected Overrides Sub Dispose(ByVal disposing As Boolean)
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
            MyBase.Dispose(disposing)
        End Sub

        'Required by the Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Designer
        'It can be modified using the Designer.  
        'Do not modify it using the code editor.
        <System.Diagnostics.DebuggerStepThrough()> _
        Private Sub InitializeComponent()
            Dim XrSummary1 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary()
            Dim XrSummary2 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary()
            Dim XrSummary3 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary()
            Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(EnglishBudgetInformation_3))
            Me.GroupHeader1 = New DevExpress.XtraReports.UI.GroupHeaderBand()
            Me.XrTable1 = New DevExpress.XtraReports.UI.XRTable()
            Me.XrTableRow1 = New DevExpress.XtraReports.UI.XRTableRow()
            Me.XrTableCell1 = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableCell2 = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableCell3 = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableCell7 = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTable2 = New DevExpress.XtraReports.UI.XRTable()
            Me.XrTableRow_detail = New DevExpress.XtraReports.UI.XRTableRow()
            Me.XrTableCell_budget_category = New DevExpress.XtraReports.UI.XRTableCell()
            Me.FormattingRule_ShowHeading = New DevExpress.XtraReports.UI.FormattingRule()
            Me.XrTableCell_client_amount = New DevExpress.XtraReports.UI.XRTableCell()
            Me.FormattingRule_HideDetails = New DevExpress.XtraReports.UI.FormattingRule()
            Me.XrTableCell_suggested_amount = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableCell_difference = New DevExpress.XtraReports.UI.XRTableCell()
            Me.GroupFooter1 = New DevExpress.XtraReports.UI.GroupFooterBand()
            Me.XrTable3 = New DevExpress.XtraReports.UI.XRTable()
            Me.XrTableRow2 = New DevExpress.XtraReports.UI.XRTableRow()
            Me.XrTableCell4 = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableCell5 = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableCell6 = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableCell8 = New DevExpress.XtraReports.UI.XRTableCell()
            CType(Me.XrTable1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.XrTable2, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.XrTable3, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
            '
            'XrLabel_ReportTitle
            '
            Me.XrLabel_ReportTitle.Text = "BUDGET" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "INFORMATION"
            '
            'XrLabel_pageHeader_Title
            '
            Me.XrLabel_pageHeader_Title.Text = "BUDGET" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "INFORMATION (Continued)"
            '
            'PageHeader
            '
            Me.PageHeader.Visible = True
            '
            'Detail
            '
            Me.Detail.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrTable2})
            Me.Detail.HeightF = 21.88!
            Me.Detail.StylePriority.UseFont = False
            '
            'GroupHeader1
            '
            Me.GroupHeader1.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrTable1})
            Me.GroupHeader1.HeightF = 21.88!
            Me.GroupHeader1.Name = "GroupHeader1"
            Me.GroupHeader1.RepeatEveryPage = True
            '
            'XrTable1
            '
            Me.XrTable1.AnchorVertical = DevExpress.XtraReports.UI.VerticalAnchorStyles.Bottom
            Me.XrTable1.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
            Me.XrTable1.Name = "XrTable1"
            Me.XrTable1.Padding = New DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 100.0!)
            Me.XrTable1.Rows.AddRange(New DevExpress.XtraReports.UI.XRTableRow() {Me.XrTableRow1})
            Me.XrTable1.SizeF = New System.Drawing.SizeF(788.28!, 21.88!)
            Me.XrTable1.StylePriority.UsePadding = False
            '
            'XrTableRow1
            '
            Me.XrTableRow1.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.XrTableCell1, Me.XrTableCell2, Me.XrTableCell3, Me.XrTableCell7})
            Me.XrTableRow1.Name = "XrTableRow1"
            Me.XrTableRow1.Padding = New DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 100.0!)
            Me.XrTableRow1.StyleName = "XrControlStyle_Report_Column"
            Me.XrTableRow1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            Me.XrTableRow1.Weight = 1.0R
            '
            'XrTableCell1
            '
            Me.XrTableCell1.CanGrow = False
            Me.XrTableCell1.Name = "XrTableCell1"
            Me.XrTableCell1.Padding = New DevExpress.XtraPrinting.PaddingInfo(5, 0, 0, 0, 100.0!)
            Me.XrTableCell1.StylePriority.UsePadding = False
            Me.XrTableCell1.Text = "CATEGORY"
            Me.XrTableCell1.Weight = 3.6514151136004878R
            '
            'XrTableCell2
            '
            Me.XrTableCell2.CanGrow = False
            Me.XrTableCell2.Name = "XrTableCell2"
            Me.XrTableCell2.StylePriority.UseTextAlignment = False
            Me.XrTableCell2.Text = "CLIENT"
            Me.XrTableCell2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrTableCell2.Weight = 0.81586517729006092R
            '
            'XrTableCell3
            '
            Me.XrTableCell3.CanGrow = False
            Me.XrTableCell3.Name = "XrTableCell3"
            Me.XrTableCell3.StylePriority.UseTextAlignment = False
            Me.XrTableCell3.Text = "SUGGESTED"
            Me.XrTableCell3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrTableCell3.Weight = 0.98201105678879053R
            '
            'XrTableCell7
            '
            Me.XrTableCell7.CanGrow = False
            Me.XrTableCell7.Name = "XrTableCell7"
            Me.XrTableCell7.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 5, 0, 0, 100.0!)
            Me.XrTableCell7.StylePriority.UsePadding = False
            Me.XrTableCell7.StylePriority.UseTextAlignment = False
            Me.XrTableCell7.Text = "DIFFERENCE"
            Me.XrTableCell7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrTableCell7.Weight = 0.98201105678879053R
            '
            'XrTable2
            '
            Me.XrTable2.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
            Me.XrTable2.Name = "XrTable2"
            Me.XrTable2.Padding = New DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 100.0!)
            Me.XrTable2.Rows.AddRange(New DevExpress.XtraReports.UI.XRTableRow() {Me.XrTableRow_detail})
            Me.XrTable2.SizeF = New System.Drawing.SizeF(788.2813!, 21.875!)
            Me.XrTable2.StylePriority.UsePadding = False
            '
            'XrTableRow_detail
            '
            Me.XrTableRow_detail.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.XrTableCell_budget_category, Me.XrTableCell_client_amount, Me.XrTableCell_suggested_amount, Me.XrTableCell_difference})
            Me.XrTableRow_detail.FormattingRules.Add(Me.FormattingRule_ShowHeading)
            Me.XrTableRow_detail.Name = "XrTableRow_detail"
            Me.XrTableRow_detail.Padding = New DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 100.0!)
            Me.XrTableRow_detail.StyleName = "XrControlStyle_Report_Detail"
            Me.XrTableRow_detail.Weight = 1.0R
            '
            'XrTableCell_budget_category
            '
            Me.XrTableCell_budget_category.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "budget_category")})
            Me.XrTableCell_budget_category.FormattingRules.Add(Me.FormattingRule_ShowHeading)
            Me.XrTableCell_budget_category.Name = "XrTableCell_budget_category"
            Me.XrTableCell_budget_category.Padding = New DevExpress.XtraPrinting.PaddingInfo(5, 0, 0, 0, 100.0!)
            Me.XrTableCell_budget_category.Scripts.OnBeforePrint = "XrTableCell_budget_category_BeforePrint"
            Me.XrTableCell_budget_category.StylePriority.UseBackColor = False
            Me.XrTableCell_budget_category.StylePriority.UseBorderColor = False
            Me.XrTableCell_budget_category.StylePriority.UseForeColor = False
            Me.XrTableCell_budget_category.StylePriority.UsePadding = False
            Me.XrTableCell_budget_category.Text = "XrTableCell_budget_category"
            Me.XrTableCell_budget_category.Weight = 3.6514153625824006R
            '
            'FormattingRule_ShowHeading
            '
            Me.FormattingRule_ShowHeading.Condition = "[heading] == True"
            '
            '
            '
            Me.FormattingRule_ShowHeading.Formatting.BackColor = System.Drawing.Color.FromArgb(CType(CType(102, Byte), Integer), CType(CType(102, Byte), Integer), CType(CType(153, Byte), Integer))
            Me.FormattingRule_ShowHeading.Formatting.BorderColor = System.Drawing.Color.White
            Me.FormattingRule_ShowHeading.Formatting.Borders = DevExpress.XtraPrinting.BorderSide.Bottom
            Me.FormattingRule_ShowHeading.Formatting.BorderWidth = 1
            Me.FormattingRule_ShowHeading.Formatting.Font = New System.Drawing.Font("Calibri", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.FormattingRule_ShowHeading.Formatting.ForeColor = System.Drawing.Color.White
            Me.FormattingRule_ShowHeading.Name = "FormattingRule_ShowHeading"
            '
            'XrTableCell_client_amount
            '
            Me.XrTableCell_client_amount.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "client_amount", "{0:c}")})
            Me.XrTableCell_client_amount.FormattingRules.Add(Me.FormattingRule_HideDetails)
            Me.XrTableCell_client_amount.Name = "XrTableCell_client_amount"
            Me.XrTableCell_client_amount.StyleName = "XrControlStyle_Report_Detail"
            Me.XrTableCell_client_amount.StylePriority.UseTextAlignment = False
            Me.XrTableCell_client_amount.Text = "XrTableCell_client_amount"
            Me.XrTableCell_client_amount.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrTableCell_client_amount.Weight = 0.815864928308148R
            '
            'FormattingRule_HideDetails
            '
            Me.FormattingRule_HideDetails.Condition = "[heading] == True"
            '
            '
            '
            Me.FormattingRule_HideDetails.Formatting.BackColor = System.Drawing.Color.FromArgb(CType(CType(102, Byte), Integer), CType(CType(102, Byte), Integer), CType(CType(153, Byte), Integer))
            Me.FormattingRule_HideDetails.Formatting.BorderColor = System.Drawing.Color.White
            Me.FormattingRule_HideDetails.Formatting.Borders = DevExpress.XtraPrinting.BorderSide.Bottom
            Me.FormattingRule_HideDetails.Formatting.BorderWidth = 1
            Me.FormattingRule_HideDetails.Formatting.Font = New System.Drawing.Font("Calibri", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.FormattingRule_HideDetails.Formatting.ForeColor = System.Drawing.Color.FromArgb(CType(CType(102, Byte), Integer), CType(CType(102, Byte), Integer), CType(CType(153, Byte), Integer))
            Me.FormattingRule_HideDetails.Name = "FormattingRule_HideDetails"
            '
            'XrTableCell_suggested_amount
            '
            Me.XrTableCell_suggested_amount.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "suggested_amount", "{0:c}")})
            Me.XrTableCell_suggested_amount.FormattingRules.Add(Me.FormattingRule_HideDetails)
            Me.XrTableCell_suggested_amount.Name = "XrTableCell_suggested_amount"
            Me.XrTableCell_suggested_amount.StyleName = "XrControlStyle_Report_Detail"
            Me.XrTableCell_suggested_amount.StylePriority.UseTextAlignment = False
            Me.XrTableCell_suggested_amount.Text = "XrTableCell_suggested_amount"
            Me.XrTableCell_suggested_amount.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrTableCell_suggested_amount.Weight = 0.98201105678879053R
            '
            'XrTableCell_difference
            '
            Me.XrTableCell_difference.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "difference", "{0:c}")})
            Me.XrTableCell_difference.FormattingRules.Add(Me.FormattingRule_HideDetails)
            Me.XrTableCell_difference.Name = "XrTableCell_difference"
            Me.XrTableCell_difference.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 5, 0, 0, 100.0!)
            Me.XrTableCell_difference.StyleName = "XrControlStyle_Report_Total"
            Me.XrTableCell_difference.StylePriority.UsePadding = False
            Me.XrTableCell_difference.StylePriority.UseTextAlignment = False
            Me.XrTableCell_difference.Text = "XrTableCell_difference"
            Me.XrTableCell_difference.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrTableCell_difference.Weight = 0.98201105678879053R
            '
            'GroupFooter1
            '
            Me.GroupFooter1.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrTable3})
            Me.GroupFooter1.HeightF = 21.875!
            Me.GroupFooter1.Name = "GroupFooter1"
            '
            'XrTable3
            '
            Me.XrTable3.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
            Me.XrTable3.Name = "XrTable3"
            Me.XrTable3.Padding = New DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 100.0!)
            Me.XrTable3.Rows.AddRange(New DevExpress.XtraReports.UI.XRTableRow() {Me.XrTableRow2})
            Me.XrTable3.SizeF = New System.Drawing.SizeF(788.2813!, 21.875!)
            Me.XrTable3.StylePriority.UsePadding = False
            '
            'XrTableRow2
            '
            Me.XrTableRow2.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.XrTableCell4, Me.XrTableCell5, Me.XrTableCell6, Me.XrTableCell8})
            Me.XrTableRow2.FormattingRules.Add(Me.FormattingRule_ShowHeading)
            Me.XrTableRow2.Name = "XrTableRow2"
            Me.XrTableRow2.Padding = New DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 100.0!)
            Me.XrTableRow2.StyleName = "XrControlStyle_Report_Total"
            Me.XrTableRow2.Weight = 1.0R
            '
            'XrTableCell4
            '
            Me.XrTableCell4.FormattingRules.Add(Me.FormattingRule_ShowHeading)
            Me.XrTableCell4.Name = "XrTableCell4"
            Me.XrTableCell4.Padding = New DevExpress.XtraPrinting.PaddingInfo(5, 0, 0, 0, 100.0!)
            Me.XrTableCell4.Scripts.OnBeforePrint = "XrTableCell_budget_category_BeforePrint"
            Me.XrTableCell4.StylePriority.UseBackColor = False
            Me.XrTableCell4.StylePriority.UseBorderColor = False
            Me.XrTableCell4.StylePriority.UseForeColor = False
            Me.XrTableCell4.StylePriority.UsePadding = False
            Me.XrTableCell4.Text = "TOTALS"
            Me.XrTableCell4.Weight = 3.6514153625824006R
            '
            'XrTableCell5
            '
            Me.XrTableCell5.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "client_amount", "{0:c}")})
            Me.XrTableCell5.FormattingRules.Add(Me.FormattingRule_HideDetails)
            Me.XrTableCell5.Name = "XrTableCell5"
            Me.XrTableCell5.StylePriority.UseTextAlignment = False
            XrSummary1.FormatString = "{0:c}"
            XrSummary1.Running = DevExpress.XtraReports.UI.SummaryRunning.Report
            Me.XrTableCell5.Summary = XrSummary1
            Me.XrTableCell5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrTableCell5.Weight = 0.815864928308148R
            '
            'XrTableCell6
            '
            Me.XrTableCell6.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "suggested_amount", "{0:c}")})
            Me.XrTableCell6.FormattingRules.Add(Me.FormattingRule_HideDetails)
            Me.XrTableCell6.Name = "XrTableCell6"
            Me.XrTableCell6.StylePriority.UseTextAlignment = False
            XrSummary2.FormatString = "{0:c}"
            XrSummary2.Running = DevExpress.XtraReports.UI.SummaryRunning.Report
            Me.XrTableCell6.Summary = XrSummary2
            Me.XrTableCell6.Text = "XrTableCell_suggested_amount"
            Me.XrTableCell6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrTableCell6.Weight = 0.98201105678879053R
            '
            'XrTableCell8
            '
            Me.XrTableCell8.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "difference", "{0:c}")})
            Me.XrTableCell8.FormattingRules.Add(Me.FormattingRule_HideDetails)
            Me.XrTableCell8.Name = "XrTableCell8"
            Me.XrTableCell8.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 5, 0, 0, 100.0!)
            Me.XrTableCell8.StylePriority.UseTextAlignment = False
            XrSummary3.FormatString = "{0:c}"
            XrSummary3.Running = DevExpress.XtraReports.UI.SummaryRunning.Report
            Me.XrTableCell8.Summary = XrSummary3
            Me.XrTableCell8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrTableCell8.Weight = 0.98201105678879053R
            '
            'EnglishBudgetInformation_3
            '
            Me.Bands.AddRange(New DevExpress.XtraReports.UI.Band() {Me.Detail, Me.TopMarginBand1, Me.BottomMarginBand1, Me.ReportHeader, Me.PageHeader, Me.GroupHeader1, Me.GroupFooter1})
            Me.FormattingRuleSheet.AddRange(New DevExpress.XtraReports.UI.FormattingRule() {Me.FormattingRule_ShowHeading, Me.FormattingRule_HideDetails})
            Me.ReportPrintOptions.PrintOnEmptyDataSource = False
            Me.ScriptLanguage = DevExpress.XtraReports.ScriptLanguage.VisualBasic
            Me.Scripts.OnBeforePrint = "BudgetInformation_BeforePrint"
            Me.ScriptsSource = resources.GetString("$this.ScriptsSource")
            Me.StyleSheet.AddRange(New DevExpress.XtraReports.UI.XRControlStyle() {Me.XrControlStyle_HeaderText, Me.XrControlStyle_HeaderLine, Me.XrControlStyle_HeaderBox1, Me.XrControlStyle_HeaderBox2, Me.XrControlStyle_HeaderBox3, Me.XrControlStyle_Report_Column, Me.XrControlStyle_Report_Group, Me.XrControlStyle_Report_Detail, Me.XrControlStyle_Report_Total, Me.XrControlStyle_Footer, Me.XrControlStyle_HeaderSubTitle})
            Me.Version = "11.2"
            Me.Controls.SetChildIndex(Me.GroupFooter1, 0)
            Me.Controls.SetChildIndex(Me.GroupHeader1, 0)
            Me.Controls.SetChildIndex(Me.PageHeader, 0)
            Me.Controls.SetChildIndex(Me.ReportHeader, 0)
            Me.Controls.SetChildIndex(Me.BottomMarginBand1, 0)
            Me.Controls.SetChildIndex(Me.TopMarginBand1, 0)
            Me.Controls.SetChildIndex(Me.Detail, 0)
            CType(Me.XrTable1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.XrTable2, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.XrTable3, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me, System.ComponentModel.ISupportInitialize).EndInit()
        End Sub

        Protected GroupHeader1 As DevExpress.XtraReports.UI.GroupHeaderBand
        Protected XrTable2 As DevExpress.XtraReports.UI.XRTable
        Protected XrTableRow_detail As DevExpress.XtraReports.UI.XRTableRow
        Protected XrTableCell_budget_category As DevExpress.XtraReports.UI.XRTableCell
        Protected XrTableCell_client_amount As DevExpress.XtraReports.UI.XRTableCell
        Protected XrTableCell_suggested_amount As DevExpress.XtraReports.UI.XRTableCell
        Protected XrTableCell_difference As DevExpress.XtraReports.UI.XRTableCell
        Protected XrTable1 As DevExpress.XtraReports.UI.XRTable
        Protected XrTableRow1 As DevExpress.XtraReports.UI.XRTableRow
        Protected XrTableCell1 As DevExpress.XtraReports.UI.XRTableCell
        Protected XrTableCell2 As DevExpress.XtraReports.UI.XRTableCell
        Protected XrTableCell3 As DevExpress.XtraReports.UI.XRTableCell
        Protected XrTableCell7 As DevExpress.XtraReports.UI.XRTableCell
        Protected FormattingRule_HideDetails As DevExpress.XtraReports.UI.FormattingRule
        Protected FormattingRule_ShowHeading As DevExpress.XtraReports.UI.FormattingRule
        Friend WithEvents GroupFooter1 As DevExpress.XtraReports.UI.GroupFooterBand
        Protected WithEvents XrTable3 As DevExpress.XtraReports.UI.XRTable
        Protected WithEvents XrTableRow2 As DevExpress.XtraReports.UI.XRTableRow
        Protected WithEvents XrTableCell4 As DevExpress.XtraReports.UI.XRTableCell
        Protected WithEvents XrTableCell5 As DevExpress.XtraReports.UI.XRTableCell
        Protected WithEvents XrTableCell6 As DevExpress.XtraReports.UI.XRTableCell
        Protected WithEvents XrTableCell8 As DevExpress.XtraReports.UI.XRTableCell
    End Class
End Namespace
