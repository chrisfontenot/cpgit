﻿Namespace Documents
    <Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
    Partial Public Class EnglishFHLBCertificate_1
        Inherits Template_1

        'XtraReport overrides dispose to clean up the component list.
        <System.Diagnostics.DebuggerNonUserCode()> _
        Protected Overrides Sub Dispose(ByVal disposing As Boolean)
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
            MyBase.Dispose(disposing)
        End Sub

        'Required by the Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Designer
        'It can be modified using the Designer.  
        'Do not modify it using the code editor.
        <System.Diagnostics.DebuggerStepThrough()> _
        Private Sub InitializeComponent()
            Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(EnglishFHLBCertificate_1))
            Me.XrPictureBox3 = New DevExpress.XtraReports.UI.XRPictureBox()
            Me.XrLabel_Id = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_Id_String = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_CounselorName = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_Counselor = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_Date = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_ProgramstartDate = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrPictureBox2 = New DevExpress.XtraReports.UI.XRPictureBox()
            Me.XrPictureBox5 = New DevExpress.XtraReports.UI.XRPictureBox()
            Me.XrPictureBox4 = New DevExpress.XtraReports.UI.XRPictureBox()
            Me.XrLabel7 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel6 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel5 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel4 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_ClientName = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel3 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel1 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel8 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrPictureBox1 = New DevExpress.XtraReports.UI.XRPictureBox()
            Me.XrCrossBandBox1 = New DevExpress.XtraReports.UI.XRCrossBandBox()
            Me.ParameterClient = New DevExpress.XtraReports.Parameters.Parameter()
            Me.ParameterLanguage = New DevExpress.XtraReports.Parameters.Parameter()
            Me.XrLabel9 = New DevExpress.XtraReports.UI.XRLabel()
            CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
            '
            'ReportHeader
            '
            Me.ReportHeader.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel9})
            Me.ReportHeader.HeightF = 1050.0!
            Me.ReportHeader.PageBreak = DevExpress.XtraReports.UI.PageBreak.AfterBand
            '
            'Detail
            '
            Me.Detail.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel8, Me.XrPictureBox1, Me.XrLabel3, Me.XrLabel_ClientName, Me.XrLabel1, Me.XrLabel6, Me.XrLabel4, Me.XrLabel5, Me.XrPictureBox2, Me.XrLabel_ProgramstartDate, Me.XrPictureBox5, Me.XrLabel7, Me.XrPictureBox4, Me.XrLabel_Date, Me.XrLabel_Id, Me.XrPictureBox3, Me.XrLabel_Id_String, Me.XrLabel_Counselor, Me.XrLabel_CounselorName})
            Me.Detail.ForeColor = System.Drawing.Color.Black
            Me.Detail.HeightF = 938.3749!
            Me.Detail.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.Detail.StylePriority.UseForeColor = False
            Me.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            '
            'XrPictureBox3
            '
            Me.XrPictureBox3.Image = CType(resources.GetObject("XrPictureBox3.Image"), System.Drawing.Image)
            Me.XrPictureBox3.LocationFloat = New DevExpress.Utils.PointFloat(578.1283!, 831.0!)
            Me.XrPictureBox3.Name = "XrPictureBox3"
            Me.XrPictureBox3.SizeF = New System.Drawing.SizeF(96.875!, 89.66663!)
            '
            'XrLabel_Id
            '
            Me.XrLabel_Id.Font = New System.Drawing.Font("Calibri", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.XrLabel_Id.LocationFloat = New DevExpress.Utils.PointFloat(76.04338!, 758.3702!)
            Me.XrLabel_Id.Name = "XrLabel_Id"
            Me.XrLabel_Id.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_Id.SizeF = New System.Drawing.SizeF(162.5!, 22.99994!)
            Me.XrLabel_Id.StylePriority.UseFont = False
            Me.XrLabel_Id.StylePriority.UseTextAlignment = False
            Me.XrLabel_Id.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
            '
            'XrLabel_Id_String
            '
            Me.XrLabel_Id_String.Font = New System.Drawing.Font("Calibri", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.XrLabel_Id_String.LocationFloat = New DevExpress.Utils.PointFloat(76.04338!, 781.3703!)
            Me.XrLabel_Id_String.Name = "XrLabel_Id_String"
            Me.XrLabel_Id_String.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_Id_String.SizeF = New System.Drawing.SizeF(100.0!, 23.0!)
            Me.XrLabel_Id_String.StylePriority.UseFont = False
            Me.XrLabel_Id_String.Text = "ID#"
            '
            'XrLabel_CounselorName
            '
            Me.XrLabel_CounselorName.Font = New System.Drawing.Font("Calibri", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.XrLabel_CounselorName.LocationFloat = New DevExpress.Utils.PointFloat(312.5034!, 758.3702!)
            Me.XrLabel_CounselorName.Name = "XrLabel_CounselorName"
            Me.XrLabel_CounselorName.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_CounselorName.SizeF = New System.Drawing.SizeF(227.0784!, 22.99994!)
            Me.XrLabel_CounselorName.StylePriority.UseFont = False
            Me.XrLabel_CounselorName.StylePriority.UseTextAlignment = False
            Me.XrLabel_CounselorName.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
            '
            'XrLabel_Counselor
            '
            Me.XrLabel_Counselor.Font = New System.Drawing.Font("Calibri", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.XrLabel_Counselor.LocationFloat = New DevExpress.Utils.PointFloat(312.5034!, 781.3704!)
            Me.XrLabel_Counselor.Name = "XrLabel_Counselor"
            Me.XrLabel_Counselor.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_Counselor.SizeF = New System.Drawing.SizeF(100.0!, 23.0!)
            Me.XrLabel_Counselor.StylePriority.UseFont = False
            Me.XrLabel_Counselor.Text = "Certified by"
            '
            'XrLabel_Date
            '
            Me.XrLabel_Date.Font = New System.Drawing.Font("Calibri", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.XrLabel_Date.LocationFloat = New DevExpress.Utils.PointFloat(552.0816!, 758.3702!)
            Me.XrLabel_Date.Name = "XrLabel_Date"
            Me.XrLabel_Date.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_Date.SizeF = New System.Drawing.SizeF(167.7083!, 22.99994!)
            Me.XrLabel_Date.StylePriority.UseFont = False
            Me.XrLabel_Date.StylePriority.UseTextAlignment = False
            Me.XrLabel_Date.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
            '
            'XrLabel_ProgramstartDate
            '
            Me.XrLabel_ProgramstartDate.Font = New System.Drawing.Font("Calibri", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.XrLabel_ProgramstartDate.LocationFloat = New DevExpress.Utils.PointFloat(552.0815!, 781.3704!)
            Me.XrLabel_ProgramstartDate.Name = "XrLabel_ProgramstartDate"
            Me.XrLabel_ProgramstartDate.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_ProgramstartDate.SizeF = New System.Drawing.SizeF(145.8334!, 23.0!)
            Me.XrLabel_ProgramstartDate.StylePriority.UseFont = False
            Me.XrLabel_ProgramstartDate.Text = "Date Of Completion"
            '
            'XrPictureBox2
            '
            Me.XrPictureBox2.Image = CType(resources.GetObject("XrPictureBox2.Image"), System.Drawing.Image)
            Me.XrPictureBox2.LocationFloat = New DevExpress.Utils.PointFloat(124.9967!, 831.0!)
            Me.XrPictureBox2.Name = "XrPictureBox2"
            Me.XrPictureBox2.SizeF = New System.Drawing.SizeF(76.04167!, 69.87494!)
            '
            'XrPictureBox5
            '
            Me.XrPictureBox5.Image = CType(resources.GetObject("XrPictureBox5.Image"), System.Drawing.Image)
            Me.XrPictureBox5.LocationFloat = New DevExpress.Utils.PointFloat(244.0961!, 831.0!)
            Me.XrPictureBox5.Name = "XrPictureBox5"
            Me.XrPictureBox5.SizeF = New System.Drawing.SizeF(140.625!, 69.87494!)
            '
            'XrPictureBox4
            '
            Me.XrPictureBox4.Image = CType(resources.GetObject("XrPictureBox4.Image"), System.Drawing.Image)
            Me.XrPictureBox4.LocationFloat = New DevExpress.Utils.PointFloat(427.7789!, 831.0!)
            Me.XrPictureBox4.Name = "XrPictureBox4"
            Me.XrPictureBox4.SizeF = New System.Drawing.SizeF(107.2917!, 89.66663!)
            '
            'XrLabel7
            '
            Me.XrLabel7.Font = New System.Drawing.Font("Calibri", 14.0!)
            Me.XrLabel7.LocationFloat = New DevExpress.Utils.PointFloat(101.0449!, 656.25!)
            Me.XrLabel7.Multiline = True
            Me.XrLabel7.Name = "XrLabel7"
            Me.XrLabel7.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel7.SizeF = New System.Drawing.SizeF(573.9584!, 50.91663!)
            Me.XrLabel7.StylePriority.UseFont = False
            Me.XrLabel7.StylePriority.UseTextAlignment = False
            Me.XrLabel7.Text = "This certificate is to be used for Homebuyer Education only, not FHA" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "mortgage in" & _
        "surance reduction"
            Me.XrLabel7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
            '
            'XrLabel6
            '
            Me.XrLabel6.Font = New System.Drawing.Font("Calibri", 14.0!)
            Me.XrLabel6.LocationFloat = New DevExpress.Utils.PointFloat(2.083294!, 524.9167!)
            Me.XrLabel6.Name = "XrLabel6"
            Me.XrLabel6.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel6.SizeF = New System.Drawing.SizeF(781.2551!, 22.99997!)
            Me.XrLabel6.StylePriority.UseFont = False
            Me.XrLabel6.StylePriority.UseTextAlignment = False
            Me.XrLabel6.Text = "Conducted by"
            Me.XrLabel6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
            '
            'XrLabel5
            '
            Me.XrLabel5.Font = New System.Drawing.Font("Calibri", 24.0!)
            Me.XrLabel5.LocationFloat = New DevExpress.Utils.PointFloat(2.083333!, 433.1667!)
            Me.XrLabel5.Name = "XrLabel5"
            Me.XrLabel5.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel5.SizeF = New System.Drawing.SizeF(781.2551!, 55.29166!)
            Me.XrLabel5.StylePriority.UseFont = False
            Me.XrLabel5.StylePriority.UseTextAlignment = False
            Me.XrLabel5.Text = "Pre-Purchase Housing counseling"
            Me.XrLabel5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
            '
            'XrLabel4
            '
            Me.XrLabel4.Font = New System.Drawing.Font("Calibri", 14.0!, System.Drawing.FontStyle.Italic)
            Me.XrLabel4.LocationFloat = New DevExpress.Utils.PointFloat(2.083294!, 393.5!)
            Me.XrLabel4.Name = "XrLabel4"
            Me.XrLabel4.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel4.SizeF = New System.Drawing.SizeF(781.2551!, 39.66666!)
            Me.XrLabel4.StylePriority.UseFont = False
            Me.XrLabel4.StylePriority.UseTextAlignment = False
            Me.XrLabel4.Text = "Has successfully completed"
            Me.XrLabel4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
            '
            'XrLabel_ClientName
            '
            Me.XrLabel_ClientName.Font = New System.Drawing.Font("Calibri", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.XrLabel_ClientName.LocationFloat = New DevExpress.Utils.PointFloat(2.083294!, 340.2916!)
            Me.XrLabel_ClientName.Multiline = True
            Me.XrLabel_ClientName.Name = "XrLabel_ClientName"
            Me.XrLabel_ClientName.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_ClientName.Scripts.OnBeforePrint = "XrLabelClientName_BeforePrint"
            Me.XrLabel_ClientName.SizeF = New System.Drawing.SizeF(781.2551!, 37.58337!)
            Me.XrLabel_ClientName.StylePriority.UseFont = False
            Me.XrLabel_ClientName.StylePriority.UseTextAlignment = False
            Me.XrLabel_ClientName.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
            '
            'XrLabel3
            '
            Me.XrLabel3.Font = New System.Drawing.Font("Calibri", 16.0!, System.Drawing.FontStyle.Italic)
            Me.XrLabel3.LocationFloat = New DevExpress.Utils.PointFloat(2.083294!, 298.5416!)
            Me.XrLabel3.Name = "XrLabel3"
            Me.XrLabel3.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel3.SizeF = New System.Drawing.SizeF(781.2551!, 29.25!)
            Me.XrLabel3.StylePriority.UseFont = False
            Me.XrLabel3.StylePriority.UseTextAlignment = False
            Me.XrLabel3.Text = "This certifies that"
            Me.XrLabel3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
            '
            'XrLabel1
            '
            Me.XrLabel1.CanGrow = False
            Me.XrLabel1.Font = New System.Drawing.Font("Calibri", 48.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.XrLabel1.ForeColor = System.Drawing.Color.Teal
            Me.XrLabel1.LocationFloat = New DevExpress.Utils.PointFloat(3.0!, 182.375!)
            Me.XrLabel1.Name = "XrLabel1"
            Me.XrLabel1.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 2, 2, 100.0!)
            Me.XrLabel1.SizeF = New System.Drawing.SizeF(780.0!, 70.91666!)
            Me.XrLabel1.StylePriority.UseFont = False
            Me.XrLabel1.StylePriority.UseForeColor = False
            Me.XrLabel1.StylePriority.UsePadding = False
            Me.XrLabel1.StylePriority.UseTextAlignment = False
            Me.XrLabel1.Text = "ACHIEVEMENT"
            Me.XrLabel1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter
            '
            'XrLabel8
            '
            Me.XrLabel8.CanGrow = False
            Me.XrLabel8.Font = New System.Drawing.Font("Calibri", 48.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.XrLabel8.ForeColor = System.Drawing.Color.Teal
            Me.XrLabel8.LocationFloat = New DevExpress.Utils.PointFloat(3.0!, 126.6666!)
            Me.XrLabel8.Name = "XrLabel8"
            Me.XrLabel8.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 2, 2, 100.0!)
            Me.XrLabel8.SizeF = New System.Drawing.SizeF(780.0!, 70.91666!)
            Me.XrLabel8.StylePriority.UseFont = False
            Me.XrLabel8.StylePriority.UseForeColor = False
            Me.XrLabel8.StylePriority.UsePadding = False
            Me.XrLabel8.StylePriority.UseTextAlignment = False
            Me.XrLabel8.Text = "CERTIFICATE OF"
            Me.XrLabel8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter
            '
            'XrPictureBox1
            '
            Me.XrPictureBox1.Image = CType(resources.GetObject("XrPictureBox1.Image"), System.Drawing.Image)
            Me.XrPictureBox1.LocationFloat = New DevExpress.Utils.PointFloat(192.7083!, 547.9166!)
            Me.XrPictureBox1.Name = "XrPictureBox1"
            Me.XrPictureBox1.SizeF = New System.Drawing.SizeF(389.5833!, 76.04166!)
            Me.XrPictureBox1.Sizing = DevExpress.XtraPrinting.ImageSizeMode.AutoSize
            '
            'XrCrossBandBox1
            '
            Me.XrCrossBandBox1.EndBand = Me.Detail
            Me.XrCrossBandBox1.EndPointFloat = New DevExpress.Utils.PointFloat(0.0!, 937.4166!)
            Me.XrCrossBandBox1.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 103.0!)
            Me.XrCrossBandBox1.Name = "XrCrossBandBox1"
            Me.XrCrossBandBox1.StartBand = Me.Detail
            Me.XrCrossBandBox1.StartPointFloat = New DevExpress.Utils.PointFloat(0.0!, 103.0!)
            Me.XrCrossBandBox1.WidthF = 785.4218!
            '
            'ParameterClient
            '
            Me.ParameterClient.Description = "Client ID"
            Me.ParameterClient.Name = "ParameterClient"
            Me.ParameterClient.Type = GetType(Integer)
            Me.ParameterClient.Value = -1
            Me.ParameterClient.Visible = False
            '
            'ParameterLanguage
            '
            Me.ParameterLanguage.Description = "Language ID"
            Me.ParameterLanguage.Name = "ParameterLanguage"
            Me.ParameterLanguage.Type = GetType(Integer)
            Me.ParameterLanguage.Value = -1
            Me.ParameterLanguage.Visible = False
            '
            'XrLabel9
            '
            Me.XrLabel9.BackColor = System.Drawing.Color.Transparent
            Me.XrLabel9.BorderColor = System.Drawing.Color.Transparent
            Me.XrLabel9.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 1027.0!)
            Me.XrLabel9.Name = "XrLabel9"
            Me.XrLabel9.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel9.SizeF = New System.Drawing.SizeF(800.0!, 23.0!)
            Me.XrLabel9.StyleName = "XrControlStyle_HeaderBox3"
            Me.XrLabel9.StylePriority.UseBackColor = False
            Me.XrLabel9.StylePriority.UseBorderColor = False
            Me.XrLabel9.StylePriority.UseTextAlignment = False
            Me.XrLabel9.Text = "This page was intentionally left blank"
            Me.XrLabel9.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter
            '
            'EnglishFHLBCertificate_1
            '
            Me.Bands.AddRange(New DevExpress.XtraReports.UI.Band() {Me.TopMarginBand1, Me.Detail, Me.BottomMarginBand1, Me.ReportHeader, Me.PageHeader})
            Me.CrossBandControls.AddRange(New DevExpress.XtraReports.UI.XRCrossBandControl() {Me.XrCrossBandBox1})
            Me.Font = New System.Drawing.Font("Calibri", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.ForeColor = System.Drawing.Color.Teal
            Me.Parameters.AddRange(New DevExpress.XtraReports.Parameters.Parameter() {Me.ParameterClient, Me.ParameterLanguage})
            Me.ScriptLanguage = DevExpress.XtraReports.ScriptLanguage.VisualBasic
            Me.Scripts.OnBeforePrint = "EnglishFHLBCertificate_1_BeforePrint"
            Me.ScriptsSource = resources.GetString("$this.ScriptsSource")
            Me.StyleSheet.AddRange(New DevExpress.XtraReports.UI.XRControlStyle() {Me.XrControlStyle_HeaderText, Me.XrControlStyle_HeaderLine, Me.XrControlStyle_HeaderBox1, Me.XrControlStyle_HeaderBox2, Me.XrControlStyle_HeaderBox3, Me.XrControlStyle_Report_Column, Me.XrControlStyle_Report_Group, Me.XrControlStyle_Report_Detail, Me.XrControlStyle_Report_Total, Me.XrControlStyle_Footer, Me.XrControlStyle_HeaderSubTitle})
            Me.Version = "11.2"
            CType(Me, System.ComponentModel.ISupportInitialize).EndInit()

        End Sub

        Protected XrLabel8 As DevExpress.XtraReports.UI.XRLabel
        Protected XrPictureBox1 As DevExpress.XtraReports.UI.XRPictureBox
        Protected XrLabel3 As DevExpress.XtraReports.UI.XRLabel
        Protected XrLabel_ClientName As DevExpress.XtraReports.UI.XRLabel
        Protected XrLabel1 As DevExpress.XtraReports.UI.XRLabel
        Protected XrLabel6 As DevExpress.XtraReports.UI.XRLabel
        Protected XrLabel4 As DevExpress.XtraReports.UI.XRLabel
        Protected XrLabel5 As DevExpress.XtraReports.UI.XRLabel
        Protected XrPictureBox2 As DevExpress.XtraReports.UI.XRPictureBox
        Protected XrLabel_ProgramstartDate As DevExpress.XtraReports.UI.XRLabel
        Protected XrPictureBox5 As DevExpress.XtraReports.UI.XRPictureBox
        Protected XrLabel7 As DevExpress.XtraReports.UI.XRLabel
        Protected XrPictureBox4 As DevExpress.XtraReports.UI.XRPictureBox
        Protected XrLabel_Date As DevExpress.XtraReports.UI.XRLabel
        Protected XrLabel_Id As DevExpress.XtraReports.UI.XRLabel
        Protected XrPictureBox3 As DevExpress.XtraReports.UI.XRPictureBox
        Protected XrLabel_Id_String As DevExpress.XtraReports.UI.XRLabel
        Protected XrLabel_Counselor As DevExpress.XtraReports.UI.XRLabel
        Protected XrLabel_CounselorName As DevExpress.XtraReports.UI.XRLabel
        Protected XrCrossBandBox1 As DevExpress.XtraReports.UI.XRCrossBandBox
        Protected WithEvents ParameterClient As DevExpress.XtraReports.Parameters.Parameter
        Protected WithEvents ParameterLanguage As DevExpress.XtraReports.Parameters.Parameter
        Protected WithEvents XrLabel9 As DevExpress.XtraReports.UI.XRLabel
    End Class
End Namespace
