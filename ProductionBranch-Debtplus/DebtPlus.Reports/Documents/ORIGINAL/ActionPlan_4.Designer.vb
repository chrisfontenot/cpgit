﻿Namespace Documents
    <Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
    Partial Class ActionPlan_4
        Inherits Template_4

        'XtraReport overrides dispose to clean up the component list.
        <System.Diagnostics.DebuggerNonUserCode()> _
        Protected Overrides Sub Dispose(ByVal disposing As Boolean)
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
            MyBase.Dispose(disposing)
        End Sub

        'Required by the Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Designer
        'It can be modified using the Designer.  
        'Do not modify it using the code editor.
        <System.Diagnostics.DebuggerStepThrough()> _
        Private Sub InitializeComponent()
            Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(ActionPlan_4))
            Me.XrLabel1 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_financial_problem1 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel2 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_date = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_client_name = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel7 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_clientID = New DevExpress.XtraReports.UI.XRLabel()
            Me.ParameterLanguage = New DevExpress.XtraReports.Parameters.Parameter()
            Me.GroupHeader_plan = New DevExpress.XtraReports.UI.GroupHeaderBand()
            Me.XrLabel4 = New DevExpress.XtraReports.UI.XRLabel()
            Me.ReportFooter = New DevExpress.XtraReports.UI.ReportFooterBand()
            Me.XrRichText_housing_considerations = New DevExpress.XtraReports.UI.XRRichText()
            Me.GroupHeader_goals = New DevExpress.XtraReports.UI.GroupHeaderBand()
            Me.XrLabel3 = New DevExpress.XtraReports.UI.XRLabel()
            CType(Me.XrRichText_housing_considerations, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
            '
            'XrLabel_subTitle
            '
            Me.XrLabel_subTitle.LocationFloat = New DevExpress.Utils.PointFloat(10.00001!, 62.21167!)
            Me.XrLabel_subTitle.Multiline = True
            Me.XrLabel_subTitle.SizeF = New System.Drawing.SizeF(564.5833!, 46.70504!)
            Me.XrLabel_subTitle.StylePriority.UseFont = False
            Me.XrLabel_subTitle.StylePriority.UseForeColor = False
            Me.XrLabel_subTitle.StylePriority.UseTextAlignment = False
            Me.XrLabel_subTitle.Text = "Please find below the goals and recommendations" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "identified during your counselin" & _
        "g session."
            Me.XrLabel_subTitle.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
            '
            'XrLabel_ReportTitle
            '
            Me.XrLabel_ReportTitle.SizeF = New System.Drawing.SizeF(564.5833!, 52.21166!)
            Me.XrLabel_ReportTitle.Text = "ACTION PLAN"
            '
            'XrLine_reportHeaderLine
            '
            Me.XrLine_reportHeaderLine.LocationFloat = New DevExpress.Utils.PointFloat(12.49989!, 108.9167!)
            '
            'XrLabel_pageHeader_Title
            '
            Me.XrLabel_pageHeader_Title.Text = "ACTION PLAN (Continued)"
            '
            'ReportHeader
            '
            Me.ReportHeader.HeightF = 118.9167!
            '
            'PageHeader
            '
            Me.PageHeader.HeightF = 125.625!
            Me.PageHeader.Visible = True
            '
            'XrLabel1
            '
            Me.XrLabel1.BackColor = System.Drawing.Color.Transparent
            Me.XrLabel1.Font = New System.Drawing.Font("Calibri", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.XrLabel1.ForeColor = System.Drawing.Color.FromArgb(CType(CType(102, Byte), Integer), CType(CType(102, Byte), Integer), CType(CType(153, Byte), Integer))
            Me.XrLabel1.LocationFloat = New DevExpress.Utils.PointFloat(0.7917404!, 45.99998!)
            Me.XrLabel1.Name = "XrLabel1"
            Me.XrLabel1.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel1.SizeF = New System.Drawing.SizeF(304.3747!, 23.0!)
            Me.XrLabel1.StylePriority.UseBackColor = False
            Me.XrLabel1.StylePriority.UseFont = False
            Me.XrLabel1.StylePriority.UseForeColor = False
            Me.XrLabel1.Text = "PRIMARY CAUSE OF FINANCIAL PROBLEM:"
            '
            'XrLabel_financial_problem1
            '
            Me.XrLabel_financial_problem1.LocationFloat = New DevExpress.Utils.PointFloat(305.1665!, 45.99998!)
            Me.XrLabel_financial_problem1.Name = "XrLabel_financial_problem1"
            Me.XrLabel_financial_problem1.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_financial_problem1.SizeF = New System.Drawing.SizeF(485.6251!, 23.0!)
            Me.XrLabel_financial_problem1.Text = "NOT SPECIFIED"
            '
            'XrLabel2
            '
            Me.XrLabel2.BackColor = System.Drawing.Color.Transparent
            Me.XrLabel2.Font = New System.Drawing.Font("Calibri", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.XrLabel2.ForeColor = System.Drawing.Color.FromArgb(CType(CType(102, Byte), Integer), CType(CType(102, Byte), Integer), CType(CType(153, Byte), Integer))
            Me.XrLabel2.LocationFloat = New DevExpress.Utils.PointFloat(0.7917404!, 0.0!)
            Me.XrLabel2.Name = "XrLabel2"
            Me.XrLabel2.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel2.SizeF = New System.Drawing.SizeF(104.5832!, 23.0!)
            Me.XrLabel2.StylePriority.UseBackColor = False
            Me.XrLabel2.StylePriority.UseFont = False
            Me.XrLabel2.StylePriority.UseForeColor = False
            Me.XrLabel2.Text = "CREATED ON:"
            '
            'XrLabel_date
            '
            Me.XrLabel_date.LocationFloat = New DevExpress.Utils.PointFloat(105.375!, 0.0!)
            Me.XrLabel_date.Name = "XrLabel_date"
            Me.XrLabel_date.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_date.SizeF = New System.Drawing.SizeF(132.7082!, 23.0!)
            '
            'XrLabel_client_name
            '
            Me.XrLabel_client_name.LocationFloat = New DevExpress.Utils.PointFloat(407.6665!, 0.0!)
            Me.XrLabel_client_name.Name = "XrLabel_client_name"
            Me.XrLabel_client_name.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_client_name.SizeF = New System.Drawing.SizeF(383.1252!, 23.00001!)
            Me.XrLabel_client_name.StylePriority.UseTextAlignment = False
            Me.XrLabel_client_name.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            '
            'XrLabel7
            '
            Me.XrLabel7.BackColor = System.Drawing.Color.Transparent
            Me.XrLabel7.Font = New System.Drawing.Font("Calibri", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.XrLabel7.ForeColor = System.Drawing.Color.FromArgb(CType(CType(102, Byte), Integer), CType(CType(102, Byte), Integer), CType(CType(153, Byte), Integer))
            Me.XrLabel7.LocationFloat = New DevExpress.Utils.PointFloat(238.0832!, 0.0!)
            Me.XrLabel7.Name = "XrLabel7"
            Me.XrLabel7.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel7.SizeF = New System.Drawing.SizeF(67.08325!, 23.0!)
            Me.XrLabel7.StylePriority.UseBackColor = False
            Me.XrLabel7.StylePriority.UseFont = False
            Me.XrLabel7.StylePriority.UseForeColor = False
            Me.XrLabel7.Text = "CLIENT:"
            '
            'XrLabel_clientID
            '
            Me.XrLabel_clientID.LocationFloat = New DevExpress.Utils.PointFloat(305.1664!, 0.0!)
            Me.XrLabel_clientID.Name = "XrLabel_clientID"
            Me.XrLabel_clientID.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_clientID.SizeF = New System.Drawing.SizeF(102.5001!, 23.0!)
            Me.XrLabel_clientID.StylePriority.UseTextAlignment = False
            Me.XrLabel_clientID.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            '
            'ParameterLanguage
            '
            Me.ParameterLanguage.Description = "Language ID for the date formatting"
            Me.ParameterLanguage.Name = "ParameterLanguage"
            Me.ParameterLanguage.Type = GetType(Integer)
            Me.ParameterLanguage.Value = 1
            Me.ParameterLanguage.Visible = False
            '
            'GroupHeader_plan
            '
            Me.GroupHeader_plan.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel_client_name, Me.XrLabel7, Me.XrLabel_clientID, Me.XrLabel_date, Me.XrLabel1, Me.XrLabel_financial_problem1, Me.XrLabel2, Me.XrLabel4})
            Me.GroupHeader_plan.HeightF = 91.99999!
            Me.GroupHeader_plan.Level = 1
            Me.GroupHeader_plan.Name = "GroupHeader_plan"
            '
            'XrLabel4
            '
            Me.XrLabel4.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 68.99999!)
            Me.XrLabel4.Name = "XrLabel4"
            Me.XrLabel4.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel4.SizeF = New System.Drawing.SizeF(102.5001!, 23.0!)
            Me.XrLabel4.StylePriority.UseTextAlignment = False
            Me.XrLabel4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            '
            'ReportFooter
            '
            Me.ReportFooter.HeightF = 23.0!
            Me.ReportFooter.Name = "ReportFooter"
            '
            'XrRichText_housing_considerations
            '
            Me.XrRichText_housing_considerations.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
            Me.XrRichText_housing_considerations.Name = "XrRichText_housing_considerations"
            Me.XrRichText_housing_considerations.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrRichText_housing_considerations.SerializableRtfString = resources.GetString("XrRichText_housing_considerations.SerializableRtfString")
            Me.XrRichText_housing_considerations.SizeF = New System.Drawing.SizeF(789.9999!, 23.0!)
            Me.XrRichText_housing_considerations.StylePriority.UsePadding = False
            '
            'GroupHeader_goals
            '
            Me.GroupHeader_goals.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel3, Me.XrRichText_housing_considerations})
            Me.GroupHeader_goals.HeightF = 46.0!
            Me.GroupHeader_goals.Name = "GroupHeader_goals"
            '
            'XrLabel3
            '
            Me.XrLabel3.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 23.0!)
            Me.XrLabel3.Name = "XrLabel3"
            Me.XrLabel3.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel3.SizeF = New System.Drawing.SizeF(102.5001!, 23.0!)
            Me.XrLabel3.StylePriority.UseTextAlignment = False
            Me.XrLabel3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            '
            'ActionPlan_4
            '
            Me.Bands.AddRange(New DevExpress.XtraReports.UI.Band() {Me.TopMarginBand1, Me.Detail, Me.BottomMarginBand1, Me.ReportHeader, Me.PageHeader, Me.GroupHeader_goals, Me.GroupHeader_plan, Me.ReportFooter})
            Me.Parameters.AddRange(New DevExpress.XtraReports.Parameters.Parameter() {Me.ParameterLanguage})
            Me.ReportPrintOptions.DetailCount = 1
            Me.ReportPrintOptions.DetailCountAtDesignTime = 1
            Me.RequestParameters = False
            Me.ScriptLanguage = DevExpress.XtraReports.ScriptLanguage.VisualBasic
            Me.Scripts.OnBeforePrint = "ActionPlan_BeforePrint"
            Me.ScriptsSource = resources.GetString("$this.ScriptsSource")
            Me.StyleSheet.AddRange(New DevExpress.XtraReports.UI.XRControlStyle() {Me.XrControlStyle_HeaderText, Me.XrControlStyle_HeaderLine, Me.XrControlStyle_HeaderBox1, Me.XrControlStyle_HeaderBox2, Me.XrControlStyle_HeaderBox3, Me.XrControlStyle_Report_Column, Me.XrControlStyle_Report_Group, Me.XrControlStyle_Report_Detail, Me.XrControlStyle_Report_Total, Me.XrControlStyle_Footer, Me.XrControlStyle_HeaderSubTitle})
            Me.Version = "11.2"
            Me.Controls.SetChildIndex(Me.ReportFooter, 0)
            Me.Controls.SetChildIndex(Me.GroupHeader_plan, 0)
            Me.Controls.SetChildIndex(Me.GroupHeader_goals, 0)
            Me.Controls.SetChildIndex(Me.PageHeader, 0)
            Me.Controls.SetChildIndex(Me.ReportHeader, 0)
            Me.Controls.SetChildIndex(Me.BottomMarginBand1, 0)
            Me.Controls.SetChildIndex(Me.Detail, 0)
            Me.Controls.SetChildIndex(Me.TopMarginBand1, 0)
            CType(Me.XrRichText_housing_considerations, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me, System.ComponentModel.ISupportInitialize).EndInit()

        End Sub

        Protected XrLabel_financial_problem1 As DevExpress.XtraReports.UI.XRLabel
        Protected XrLabel1 As DevExpress.XtraReports.UI.XRLabel
        Protected XrLabel_clientID As DevExpress.XtraReports.UI.XRLabel
        Protected XrLabel7 As DevExpress.XtraReports.UI.XRLabel
        Protected XrLabel_client_name As DevExpress.XtraReports.UI.XRLabel
        Protected XrLabel_date As DevExpress.XtraReports.UI.XRLabel
        Protected XrLabel2 As DevExpress.XtraReports.UI.XRLabel
        Protected ParameterLanguage As DevExpress.XtraReports.Parameters.Parameter
        Protected GroupHeader_plan As DevExpress.XtraReports.UI.GroupHeaderBand
        Protected ReportFooter As DevExpress.XtraReports.UI.ReportFooterBand
        Protected XrLabel4 As DevExpress.XtraReports.UI.XRLabel
        Protected WithEvents XrRichText_housing_considerations As DevExpress.XtraReports.UI.XRRichText
        Protected WithEvents GroupHeader_goals As DevExpress.XtraReports.UI.GroupHeaderBand
        Protected WithEvents XrLabel3 As DevExpress.XtraReports.UI.XRLabel
    End Class
End Namespace
