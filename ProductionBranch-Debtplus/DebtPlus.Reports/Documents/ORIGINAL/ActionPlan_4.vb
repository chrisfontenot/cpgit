﻿Namespace Documents
    Public Class ActionPlan_4
        Public Sub New()
            MyBase.New()
            InitializeComponent()
            ' AddHandler BeforePrint, AddressOf ActionPlan_BeforePrint
        End Sub

        Private Sub ActionPlan_BeforePrint(sender As Object, e As System.Drawing.Printing.PrintEventArgs)
            Dim rpt As DevExpress.XtraReports.UI.XtraReport = TryCast(sender, DevExpress.XtraReports.UI.XtraReport)
            If rpt Is Nothing Then
                Return
            End If

            Dim MasterRpt As DevExpress.XtraReports.UI.XtraReport = TryCast(rpt.MasterReport, DevExpress.XtraReports.UI.XtraReport)
            If MasterRpt Is Nothing Then
                Return
            End If

            Dim IMasterRpt As DebtPlus.Interfaces.Client.IClient = TryCast(MasterRpt, DebtPlus.Interfaces.Client.IClient)
            Dim ClientID As Int32 = -1
            If IMasterRpt IsNot Nothing Then
                ClientID = IMasterRpt.ClientId
            End If

            If ClientID < 0 Then
                Return
            End If

            ' Find the language for the report
            Dim languageID As Int32 = 1
            Dim parmLanguage As DevExpress.XtraReports.Parameters.Parameter = MasterRpt.Parameters("ParameterLanguage")
            If parmLanguage IsNot Nothing Then
                languageID = Convert.ToInt32(parmLanguage.Value)
                If languageID < 0 Then
                    languageID = 1
                End If
            End If

            ' Set the known values into the standard text blocks
            XrLabel_clientID.Text = DebtPlus.Utils.Format.Client.FormatClientID(ClientID)

            ' Retrieve the values from the database
            Try
                Dim planText As String = String.Empty
                Using cn As New System.Data.SqlClient.SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
                    cn.Open()

                    ' Retrieve the client financial problem
                    Using cmd As New System.Data.SqlClient.SqlCommand()
                        cmd.Connection = cn
                        cmd.CommandText = "SELECT [cause_fin_problem1] FROM [clients] WHERE [client]=@client"
                        cmd.CommandType = System.Data.CommandType.Text
                        cmd.Parameters.Add("@client", System.Data.SqlDbType.Int).Value = ClientID

                        Dim obj As Object = cmd.ExecuteScalar()
                        If obj IsNot Nothing AndAlso Not System.Object.Equals(obj, System.DBNull.Value) Then
                            Dim financialProblem As System.Int32 = Convert.ToInt32(obj)

                            ' Map the problem to the description string if there is one to be mapped.
                            If financialProblem > 0 Then
                                For Each problem As DebtPlus.LINQ.financial_problem In DebtPlus.LINQ.Cache.financial_problemType.getList()
                                    If problem.Id = financialProblem Then
                                        XrLabel_financial_problem1.Text = problem.description
                                        Exit For
                                    End If
                                Next
                            End If
                        End If
                    End Using

                    ' Find the client's name
                    Using cmd As New System.Data.SqlClient.SqlCommand()
                        cmd.Connection = cn
                        cmd.CommandType = System.Data.CommandType.Text
                        cmd.CommandText = "SELECT [salutation] as [name] FROM [view_client_address] WHERE [client]=@client"
                        cmd.Parameters.Add("@client", System.Data.SqlDbType.Int).Value = ClientID

                        Dim obj As Object = cmd.ExecuteScalar()
                        If obj IsNot Nothing AndAlso Not System.Object.Equals(obj, System.DBNull.Value) Then
                            XrLabel_client_name.Text = Convert.ToString(obj).Trim()
                        End If
                    End Using

                    ' Find the action plan for the client
                    Dim actionPlan As Int32 = 0
                    Using cmd As New System.Data.SqlClient.SqlCommand()
                        cmd.Connection = cn
                        cmd.CommandText = "SELECT TOP 1 [action_plan] as oID, [date_created] as date_created FROM [action_plans] WHERE [client]=@client ORDER BY [date_created] DESC"
                        cmd.CommandType = System.Data.CommandType.Text
                        cmd.Parameters.Add("@client", System.Data.SqlDbType.Int).Value = ClientID

                        Using rd As System.Data.SqlClient.SqlDataReader = cmd.ExecuteReader(System.Data.CommandBehavior.SingleRow)
                            If rd.Read() Then

                                ' Find the action plan id
                                If Not rd.IsDBNull(0) Then
                                    actionPlan = rd.GetInt32(0)
                                End If

                                ' Get the date that the plan was created
                                If Not rd.IsDBNull(1) Then
                                    Dim culture As System.Globalization.CultureInfo = DebtPlus.LINQ.AttributeType.SpecificCulture(languageID)
                                    If culture Is Nothing Then
                                        culture = System.Globalization.CultureInfo.CurrentCulture
                                    End If
                                    Dim strFormat As String = "{0:" + culture.DateTimeFormat.ShortDatePattern + "}"
                                    XrLabel_date.Text = String.Format(culture, strFormat, rd.GetDateTime(1))
                                End If
                            End If
                            rd.Close()
                        End Using
                    End Using

                    ' If there is a plan then find the text buffer
                    If actionPlan > 0 Then
                        Using ds As New System.Data.DataSet("ds")
                            Using cmd As New System.Data.SqlClient.SqlCommand()
                                cmd.Connection = cn
                                cmd.CommandText = "SELECT [text] FROM action_items_goals WHERE [action_plan]=@ActionPlan"
                                cmd.CommandType = System.Data.CommandType.Text
                                cmd.Parameters.Add("@ActionPlan", System.Data.SqlDbType.Int).Value = actionPlan

                                Using da As New System.Data.SqlClient.SqlDataAdapter(cmd)
                                    da.Fill(ds, "actionPlan")
                                    Dim tbl As System.Data.DataTable = ds.Tables(0)
                                    If tbl.Rows.Count > 0 Then
                                        Dim row As System.Data.DataRow = tbl.Rows(0)
                                        planText = Convert.ToString(row(0))
                                    End If
                                End Using
                            End Using
                        End Using

                        ' Try the RTF format first. This is the most common.
                        If DebtPlus.Utils.Format.Strings.IsRTF(planText) Then
                            Try
                                XrRichText_housing_considerations.Rtf = planText
                                Return
                            Catch ex As Exception
                            End Try
                        End If

                        ' Try the HTML format next.
                        If DebtPlus.Utils.Format.Strings.IsHTML(planText) Then
                            Try
                                XrRichText_housing_considerations.Html = planText
                                Return
                            Catch ex As Exception
                            End Try
                        End If

                        ' Finally, just use the normal text format
                        XrRichText_housing_considerations.Text = planText
                        Return
                    End If

                    cn.Close()
                End Using

            Catch ex As System.Data.SqlClient.SqlException
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex)

            Finally

                ' If the goals are empty then don't print the section
                If (String.IsNullOrWhiteSpace(XrRichText_housing_considerations.Text)) Then
                    GroupHeader_goals.Visible = False
                    GroupHeader_goals.HeightF = 0.0!
                End If

            End Try
        End Sub
    End Class
End Namespace
