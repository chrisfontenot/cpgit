﻿Namespace Documents
    <Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
    Partial Class MakingAppropriateChoices_3
        Inherits Template_3

        'XtraReport overrides dispose to clean up the component list.
        <System.Diagnostics.DebuggerNonUserCode()> _
        Protected Overrides Sub Dispose(ByVal disposing As Boolean)
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
            MyBase.Dispose(disposing)
        End Sub

        'Required by the Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Designer
        'It can be modified using the Designer.  
        'Do not modify it using the code editor.
        <System.Diagnostics.DebuggerStepThrough()> _
        Private Sub InitializeComponent()
            CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
            '
            'XrLabel_reportHeaderBox3
            '
            Me.XrLabel_reportHeaderBox3.StylePriority.UseBackColor = False
            Me.XrLabel_reportHeaderBox3.StylePriority.UseBorderColor = False
            Me.XrLabel_reportHeaderBox3.StylePriority.UseForeColor = False
            '
            'XrLabel_reportHeaderBox2
            '
            Me.XrLabel_reportHeaderBox2.StylePriority.UseBackColor = False
            Me.XrLabel_reportHeaderBox2.StylePriority.UseBorderColor = False
            Me.XrLabel_reportHeaderBox2.StylePriority.UseForeColor = False
            '
            'XrLabel_reportHeaderBox1
            '
            Me.XrLabel_reportHeaderBox1.StylePriority.UseBackColor = False
            Me.XrLabel_reportHeaderBox1.StylePriority.UseBorderColor = False
            Me.XrLabel_reportHeaderBox1.StylePriority.UseForeColor = False
            '
            'XrLabel_pageHeaderBox3
            '
            Me.XrLabel_pageHeaderBox3.StylePriority.UseBackColor = False
            Me.XrLabel_pageHeaderBox3.StylePriority.UseBorderColor = False
            Me.XrLabel_pageHeaderBox3.StylePriority.UseForeColor = False
            '
            'XrLabel_pageHeaderBox2
            '
            Me.XrLabel_pageHeaderBox2.StylePriority.UseBackColor = False
            Me.XrLabel_pageHeaderBox2.StylePriority.UseBorderColor = False
            Me.XrLabel_pageHeaderBox2.StylePriority.UseForeColor = False
            '
            'XrLabel_pageHeaderBox1
            '
            Me.XrLabel_pageHeaderBox1.StylePriority.UseBackColor = False
            Me.XrLabel_pageHeaderBox1.StylePriority.UseBorderColor = False
            Me.XrLabel_pageHeaderBox1.StylePriority.UseForeColor = False
            '
            'XrLabel_ReportTitle
            '
            Me.XrLabel_ReportTitle.StylePriority.UseFont = False
            Me.XrLabel_ReportTitle.StylePriority.UseForeColor = False
            Me.XrLabel_ReportTitle.Text = "MAKING APPROPRIATE" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "CHOICES"
            '
            'XrLine_reportHeaderLine
            '
            Me.XrLine_reportHeaderLine.StylePriority.UseBorderColor = False
            Me.XrLine_reportHeaderLine.StylePriority.UseForeColor = False
            '
            'XrLine_pageHeaderLine
            '
            Me.XrLine_pageHeaderLine.StylePriority.UseBorderColor = False
            Me.XrLine_pageHeaderLine.StylePriority.UseForeColor = False
            '
            'XrLabel_pageHeader_Title
            '
            Me.XrLabel_pageHeader_Title.StylePriority.UseFont = False
            Me.XrLabel_pageHeader_Title.StylePriority.UseForeColor = False
            Me.XrLabel_pageHeader_Title.Text = "MAKING APPROPRIATE" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "CHOICES (Continued)"
            '
            'PageHeader
            '
            Me.PageHeader.Visible = True
            '
            'MakingAppropriateChoices
            '
            Me.Bands.AddRange(New DevExpress.XtraReports.UI.Band() {Me.Detail, Me.TopMarginBand1, Me.BottomMarginBand1, Me.ReportHeader, Me.PageHeader})
            Me.Version = "11.2"
            CType(Me, System.ComponentModel.ISupportInitialize).EndInit()

        End Sub
    End Class
End Namespace
