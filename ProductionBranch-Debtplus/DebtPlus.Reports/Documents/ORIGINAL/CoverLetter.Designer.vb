﻿Namespace Documents
    <Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
    Partial Public Class CoverLetter
        Inherits Template_1

        'XtraReport overrides dispose to clean up the component list.
        <System.Diagnostics.DebuggerNonUserCode()> _
        Protected Overrides Sub Dispose(ByVal disposing As Boolean)
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
            MyBase.Dispose(disposing)
        End Sub

        'Required by the Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Designer
        'It can be modified using the Designer.  
        'Do not modify it using the code editor.
        <System.Diagnostics.DebuggerStepThrough()> _
        Private Sub InitializeComponent()
            Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(CoverLetter))
            Me.XrPictureBox_Image2 = New DevExpress.XtraReports.UI.XRPictureBox()
            Me.XrPictureBox1_Image1 = New DevExpress.XtraReports.UI.XRPictureBox()
            Me.XrLabel_reportHeaderBox2 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_reportHeaderBox1 = New DevExpress.XtraReports.UI.XRLabel()
            Me.ReportFooter = New DevExpress.XtraReports.UI.ReportFooterBand()
            Me.XrLabel1 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_date = New DevExpress.XtraReports.UI.XRLabel()
            CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
            '
            'ReportHeader
            '
            Me.ReportHeader.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrPictureBox_Image2, Me.XrPictureBox1_Image1, Me.XrLabel_reportHeaderBox2, Me.XrLabel_reportHeaderBox1})
            Me.ReportHeader.HeightF = 107.2917!
            '
            'Detail
            '
            Me.Detail.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel_date})
            Me.Detail.HeightF = 71.91664!
            Me.Detail.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
            Me.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
            '
            'XrPictureBox_Image2
            '
            Me.XrPictureBox_Image2.Image = CType(resources.GetObject("XrPictureBox_Image2.Image"), System.Drawing.Image)
            Me.XrPictureBox_Image2.LocationFloat = New DevExpress.Utils.PointFloat(471.8751!, 10.00001!)
            Me.XrPictureBox_Image2.Name = "XrPictureBox_Image2"
            Me.XrPictureBox_Image2.SizeF = New System.Drawing.SizeF(318.125!, 79.0!)
            Me.XrPictureBox_Image2.Sizing = DevExpress.XtraPrinting.ImageSizeMode.ZoomImage
            '
            'XrPictureBox1_Image1
            '
            Me.XrPictureBox1_Image1.Image = CType(resources.GetObject("XrPictureBox1_Image1.Image"), System.Drawing.Image)
            Me.XrPictureBox1_Image1.LocationFloat = New DevExpress.Utils.PointFloat(197.4998!, 10.00001!)
            Me.XrPictureBox1_Image1.Name = "XrPictureBox1_Image1"
            Me.XrPictureBox1_Image1.SizeF = New System.Drawing.SizeF(79.0!, 79.0!)
            '
            'XrLabel_reportHeaderBox2
            '
            Me.XrLabel_reportHeaderBox2.LocationFloat = New DevExpress.Utils.PointFloat(103.7499!, 10.00001!)
            Me.XrLabel_reportHeaderBox2.Name = "XrLabel_reportHeaderBox2"
            Me.XrLabel_reportHeaderBox2.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_reportHeaderBox2.SizeF = New System.Drawing.SizeF(79.0!, 79.0!)
            Me.XrLabel_reportHeaderBox2.StyleName = "XrControlStyle_HeaderBox2"
            '
            'XrLabel_reportHeaderBox1
            '
            Me.XrLabel_reportHeaderBox1.LocationFloat = New DevExpress.Utils.PointFloat(9.999998!, 10.00001!)
            Me.XrLabel_reportHeaderBox1.Name = "XrLabel_reportHeaderBox1"
            Me.XrLabel_reportHeaderBox1.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_reportHeaderBox1.SizeF = New System.Drawing.SizeF(79.0!, 79.0!)
            Me.XrLabel_reportHeaderBox1.StyleName = "XrControlStyle_HeaderBox1"
            '
            'ReportFooter
            '
            Me.ReportFooter.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel1})
            Me.ReportFooter.HeightF = 23.0!
            Me.ReportFooter.Name = "ReportFooter"
            Me.ReportFooter.PageBreak = DevExpress.XtraReports.UI.PageBreak.BeforeBand
            Me.ReportFooter.PrintAtBottom = True
            '
            'XrLabel1
            '
            Me.XrLabel1.BackColor = System.Drawing.Color.Transparent
            Me.XrLabel1.BorderColor = System.Drawing.Color.Transparent
            Me.XrLabel1.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
            Me.XrLabel1.Name = "XrLabel1"
            Me.XrLabel1.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel1.SizeF = New System.Drawing.SizeF(800.0!, 23.0!)
            Me.XrLabel1.StyleName = "XrControlStyle_HeaderBox3"
            Me.XrLabel1.StylePriority.UseBackColor = False
            Me.XrLabel1.StylePriority.UseBorderColor = False
            Me.XrLabel1.StylePriority.UseTextAlignment = False
            Me.XrLabel1.Text = "This page was intentionally left blank"
            Me.XrLabel1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter
            '
            'XrLabel_date
            '
            Me.XrLabel_date.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 10.00001!)
            Me.XrLabel_date.LockedInUserDesigner = True
            Me.XrLabel_date.Name = "XrLabel_date"
            Me.XrLabel_date.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_date.SizeF = New System.Drawing.SizeF(423.9549!, 23.0!)
            '
            'CoverLetter
            '
            Me.Bands.AddRange(New DevExpress.XtraReports.UI.Band() {Me.Detail, Me.TopMarginBand1, Me.BottomMarginBand1, Me.ReportHeader, Me.PageHeader, Me.ReportFooter})
            Me.Font = New System.Drawing.Font("Calibri", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.ScriptLanguage = DevExpress.XtraReports.ScriptLanguage.VisualBasic
            Me.Scripts.OnBeforePrint = "CoverLetter_BeforePrint"
            Me.ScriptsSource = resources.GetString("$this.ScriptsSource")
            Me.StyleSheet.AddRange(New DevExpress.XtraReports.UI.XRControlStyle() {Me.XrControlStyle_HeaderText, Me.XrControlStyle_HeaderLine, Me.XrControlStyle_HeaderBox1, Me.XrControlStyle_HeaderBox2, Me.XrControlStyle_HeaderBox3, Me.XrControlStyle_Report_Column, Me.XrControlStyle_Report_Group, Me.XrControlStyle_Report_Detail, Me.XrControlStyle_Report_Total, Me.XrControlStyle_Footer, Me.XrControlStyle_HeaderSubTitle})
            Me.Version = "11.2"
            Me.Controls.SetChildIndex(Me.ReportFooter, 0)
            Me.Controls.SetChildIndex(Me.PageHeader, 0)
            Me.Controls.SetChildIndex(Me.ReportHeader, 0)
            Me.Controls.SetChildIndex(Me.BottomMarginBand1, 0)
            Me.Controls.SetChildIndex(Me.TopMarginBand1, 0)
            Me.Controls.SetChildIndex(Me.Detail, 0)
            CType(Me, System.ComponentModel.ISupportInitialize).EndInit()

        End Sub
        Protected XrPictureBox_Image2 As DevExpress.XtraReports.UI.XRPictureBox
        Protected XrPictureBox1_Image1 As DevExpress.XtraReports.UI.XRPictureBox
        Protected XrLabel_reportHeaderBox2 As DevExpress.XtraReports.UI.XRLabel
        Protected XrLabel_reportHeaderBox1 As DevExpress.XtraReports.UI.XRLabel
        Protected ReportFooter As DevExpress.XtraReports.UI.ReportFooterBand
        Protected XrLabel1 As DevExpress.XtraReports.UI.XRLabel
        Protected XrLabel_date As DevExpress.XtraReports.UI.XRLabel
    End Class
End Namespace
