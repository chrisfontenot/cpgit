﻿Namespace Documents
    <Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
    Partial Class EnglishNonDMPDebts_3
        Inherits Template_3

        'XtraReport overrides dispose to clean up the component list.
        <System.Diagnostics.DebuggerNonUserCode()> _
        Protected Overrides Sub Dispose(ByVal disposing As Boolean)
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
            MyBase.Dispose(disposing)
        End Sub

        'Required by the Designer
        Private components As System.ComponentModel.IContainer = Nothing

        'NOTE: The following procedure is required by the Designer
        'It can be modified using the Designer.  
        'Do not modify it using the code editor.
        <System.Diagnostics.DebuggerStepThrough()> _
        Private Sub InitializeComponent()
            Dim XrSummary1 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary()
            Dim XrSummary2 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary()
            Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(EnglishNonDMPDebts_3))
            Me.ReportFooter = New DevExpress.XtraReports.UI.ReportFooterBand()
            Me.XrTable3 = New DevExpress.XtraReports.UI.XRTable()
            Me.XrTableRow3 = New DevExpress.XtraReports.UI.XRTableRow()
            Me.XrTableCell7 = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableCell12 = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableCell_total_balance = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableCell_total_payment = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableCell15 = New DevExpress.XtraReports.UI.XRTableCell()
            Me.GroupHeader1 = New DevExpress.XtraReports.UI.GroupHeaderBand()
            Me.XrTable1 = New DevExpress.XtraReports.UI.XRTable()
            Me.XrTableRow1 = New DevExpress.XtraReports.UI.XRTableRow()
            Me.XrTableCell1 = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableCell10 = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableCell2 = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableCell13 = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableCell3 = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrLabel_detail_creditor_name = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_detail_account_number = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_detail_balance = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_detail_payment = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_detail_interest_rate = New DevExpress.XtraReports.UI.XRLabel()
            CType(Me.XrTable3, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.XrTable1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
            '
            'XrLabel_ReportTitle
            '
            Me.XrLabel_ReportTitle.Text = "NON-DMP DEBTS"
            '
            'XrLabel_pageHeader_Title
            '
            Me.XrLabel_pageHeader_Title.Text = "NON-DMP DEBTS (Continued)"
            '
            'PageHeader
            '
            Me.PageHeader.Visible = True
            '
            'Detail
            '
            Me.Detail.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel_detail_creditor_name, Me.XrLabel_detail_account_number, Me.XrLabel_detail_balance, Me.XrLabel_detail_payment, Me.XrLabel_detail_interest_rate})
            Me.Detail.HeightF = 23.0!
            '
            'ReportFooter
            '
            Me.ReportFooter.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrTable3})
            Me.ReportFooter.HeightF = 21.88!
            Me.ReportFooter.Name = "ReportFooter"
            '
            'XrTable3
            '
            Me.XrTable3.LocationFloat = New DevExpress.Utils.PointFloat(0.00001589457!, 0.0!)
            Me.XrTable3.Name = "XrTable3"
            Me.XrTable3.Padding = New DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 100.0!)
            Me.XrTable3.Rows.AddRange(New DevExpress.XtraReports.UI.XRTableRow() {Me.XrTableRow3})
            Me.XrTable3.SizeF = New System.Drawing.SizeF(767.9166!, 21.88!)
            Me.XrTable3.StylePriority.UsePadding = False
            '
            'XrTableRow3
            '
            Me.XrTableRow3.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.XrTableCell7, Me.XrTableCell12, Me.XrTableCell_total_balance, Me.XrTableCell_total_payment, Me.XrTableCell15})
            Me.XrTableRow3.Name = "XrTableRow3"
            Me.XrTableRow3.StyleName = "XrControlStyle_Report_Total"
            Me.XrTableRow3.Weight = 0.56162528028826852R
            '
            'XrTableCell7
            '
            Me.XrTableCell7.Name = "XrTableCell7"
            Me.XrTableCell7.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrTableCell7.StylePriority.UsePadding = False
            Me.XrTableCell7.Text = "TOTALS"
            Me.XrTableCell7.Weight = 1.2249051507660507R
            '
            'XrTableCell12
            '
            Me.XrTableCell12.Name = "XrTableCell12"
            Me.XrTableCell12.Weight = 0.25271252435908909R
            '
            'XrTableCell_total_balance
            '
            Me.XrTableCell_total_balance.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "balance")})
            Me.XrTableCell_total_balance.Name = "XrTableCell_total_balance"
            Me.XrTableCell_total_balance.StylePriority.UseTextAlignment = False
            XrSummary1.FormatString = "{0:c}"
            XrSummary1.Running = DevExpress.XtraReports.UI.SummaryRunning.Report
            Me.XrTableCell_total_balance.Summary = XrSummary1
            Me.XrTableCell_total_balance.Text = "XrTableCell_total_balance"
            Me.XrTableCell_total_balance.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrTableCell_total_balance.Weight = 0.58193216080122423R
            '
            'XrTableCell_total_payment
            '
            Me.XrTableCell_total_payment.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "payment")})
            Me.XrTableCell_total_payment.Name = "XrTableCell_total_payment"
            Me.XrTableCell_total_payment.StylePriority.UseTextAlignment = False
            XrSummary2.FormatString = "{0:c}"
            XrSummary2.Running = DevExpress.XtraReports.UI.SummaryRunning.Report
            Me.XrTableCell_total_payment.Summary = XrSummary2
            Me.XrTableCell_total_payment.Text = "XrTableCell_total_payment"
            Me.XrTableCell_total_payment.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrTableCell_total_payment.Weight = 0.47022511184237614R
            '
            'XrTableCell15
            '
            Me.XrTableCell15.Name = "XrTableCell15"
            Me.XrTableCell15.Weight = 0.47022505223126R
            '
            'GroupHeader1
            '
            Me.GroupHeader1.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrTable1})
            Me.GroupHeader1.HeightF = 21.88!
            Me.GroupHeader1.Name = "GroupHeader1"
            '
            'XrTable1
            '
            Me.XrTable1.AnchorVertical = DevExpress.XtraReports.UI.VerticalAnchorStyles.Bottom
            Me.XrTable1.LocationFloat = New DevExpress.Utils.PointFloat(0.00001525879!, 0.0!)
            Me.XrTable1.Name = "XrTable1"
            Me.XrTable1.Padding = New DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 100.0!)
            Me.XrTable1.Rows.AddRange(New DevExpress.XtraReports.UI.XRTableRow() {Me.XrTableRow1})
            Me.XrTable1.SizeF = New System.Drawing.SizeF(767.9166!, 21.88!)
            Me.XrTable1.StylePriority.UsePadding = False
            '
            'XrTableRow1
            '
            Me.XrTableRow1.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.XrTableCell1, Me.XrTableCell10, Me.XrTableCell2, Me.XrTableCell13, Me.XrTableCell3})
            Me.XrTableRow1.Name = "XrTableRow1"
            Me.XrTableRow1.StyleName = "XrControlStyle_Report_Column"
            Me.XrTableRow1.Weight = 0.55275778475184634R
            '
            'XrTableCell1
            '
            Me.XrTableCell1.CanGrow = False
            Me.XrTableCell1.Name = "XrTableCell1"
            Me.XrTableCell1.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrTableCell1.StylePriority.UsePadding = False
            Me.XrTableCell1.Text = "CREDITOR"
            Me.XrTableCell1.Weight = 2.4672131147540983R
            '
            'XrTableCell10
            '
            Me.XrTableCell10.CanGrow = False
            Me.XrTableCell10.Name = "XrTableCell10"
            Me.XrTableCell10.Padding = New DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 100.0!)
            Me.XrTableCell10.StylePriority.UsePadding = False
            Me.XrTableCell10.StylePriority.UseTextAlignment = False
            Me.XrTableCell10.Text = "ACCOUNT"
            Me.XrTableCell10.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
            Me.XrTableCell10.Weight = 0.78688550605148544R
            '
            'XrTableCell2
            '
            Me.XrTableCell2.CanGrow = False
            Me.XrTableCell2.Name = "XrTableCell2"
            Me.XrTableCell2.Padding = New DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 100.0!)
            Me.XrTableCell2.StylePriority.UsePadding = False
            Me.XrTableCell2.StylePriority.UseTextAlignment = False
            Me.XrTableCell2.Text = "BALANCE"
            Me.XrTableCell2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrTableCell2.Weight = 0.89426209496670073R
            '
            'XrTableCell13
            '
            Me.XrTableCell13.CanGrow = False
            Me.XrTableCell13.Name = "XrTableCell13"
            Me.XrTableCell13.Padding = New DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 100.0!)
            Me.XrTableCell13.StylePriority.UsePadding = False
            Me.XrTableCell13.StylePriority.UseTextAlignment = False
            Me.XrTableCell13.Text = "PAYMENT"
            Me.XrTableCell13.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrTableCell13.Weight = 0.94713148773693645R
            '
            'XrTableCell3
            '
            Me.XrTableCell3.CanGrow = False
            Me.XrTableCell3.Name = "XrTableCell3"
            Me.XrTableCell3.Padding = New DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 100.0!)
            Me.XrTableCell3.StylePriority.UsePadding = False
            Me.XrTableCell3.StylePriority.UseTextAlignment = False
            Me.XrTableCell3.Text = "APR"
            Me.XrTableCell3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrTableCell3.Weight = 0.94713042712602458R
            '
            'XrLabel_detail_creditor_name
            '
            Me.XrLabel_detail_creditor_name.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "creditor_name")})
            Me.XrLabel_detail_creditor_name.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
            Me.XrLabel_detail_creditor_name.Name = "XrLabel_detail_creditor_name"
            Me.XrLabel_detail_creditor_name.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_detail_creditor_name.SizeF = New System.Drawing.SizeF(313.5417!, 23.0!)
            Me.XrLabel_detail_creditor_name.StylePriority.UseTextAlignment = False
            Me.XrLabel_detail_creditor_name.Text = "XrLabel_detail_creditor_name"
            Me.XrLabel_detail_creditor_name.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            Me.XrLabel_detail_creditor_name.WordWrap = False
            '
            'XrLabel_detail_account_number
            '
            Me.XrLabel_detail_account_number.LocationFloat = New DevExpress.Utils.PointFloat(313.5417!, 0.0!)
            Me.XrLabel_detail_account_number.Name = "XrLabel_detail_account_number"
            Me.XrLabel_detail_account_number.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_detail_account_number.Scripts.OnBeforePrint = "XrLabel_detail_account_number_BeforePrint"
            Me.XrLabel_detail_account_number.SizeF = New System.Drawing.SizeF(100.0!, 23.0!)
            Me.XrLabel_detail_account_number.StylePriority.UseTextAlignment = False
            Me.XrLabel_detail_account_number.Text = "XrLabel_detail_account_number"
            Me.XrLabel_detail_account_number.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
            Me.XrLabel_detail_account_number.WordWrap = False
            '
            'XrLabel_detail_balance
            '
            Me.XrLabel_detail_balance.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "balance", "{0:c}")})
            Me.XrLabel_detail_balance.LocationFloat = New DevExpress.Utils.PointFloat(413.5417!, 0.0!)
            Me.XrLabel_detail_balance.Name = "XrLabel_detail_balance"
            Me.XrLabel_detail_balance.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_detail_balance.SizeF = New System.Drawing.SizeF(113.6458!, 23.0!)
            Me.XrLabel_detail_balance.StylePriority.UseTextAlignment = False
            Me.XrLabel_detail_balance.Text = "XrLabel_detail_balance"
            Me.XrLabel_detail_balance.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel_detail_payment
            '
            Me.XrLabel_detail_payment.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "payment", "{0:c}")})
            Me.XrLabel_detail_payment.LocationFloat = New DevExpress.Utils.PointFloat(527.1875!, 0.0!)
            Me.XrLabel_detail_payment.Name = "XrLabel_detail_payment"
            Me.XrLabel_detail_payment.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_detail_payment.SizeF = New System.Drawing.SizeF(120.3646!, 23.0!)
            Me.XrLabel_detail_payment.StylePriority.UseTextAlignment = False
            Me.XrLabel_detail_payment.Text = "XrLabel_detail_payment"
            Me.XrLabel_detail_payment.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel_detail_interest_rate
            '
            Me.XrLabel_detail_interest_rate.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "interest_rate", "{0:p}")})
            Me.XrLabel_detail_interest_rate.LocationFloat = New DevExpress.Utils.PointFloat(647.5521!, 0.0!)
            Me.XrLabel_detail_interest_rate.Name = "XrLabel_detail_interest_rate"
            Me.XrLabel_detail_interest_rate.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_detail_interest_rate.SizeF = New System.Drawing.SizeF(120.3646!, 23.0!)
            Me.XrLabel_detail_interest_rate.StylePriority.UseTextAlignment = False
            Me.XrLabel_detail_interest_rate.Text = "XrLabel_detail_interest_rate"
            Me.XrLabel_detail_interest_rate.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'NonDMPDebts_3
            '
            Me.Bands.AddRange(New DevExpress.XtraReports.UI.Band() {Me.Detail, Me.TopMarginBand1, Me.BottomMarginBand1, Me.ReportHeader, Me.PageHeader, Me.ReportFooter, Me.GroupHeader1})
            Me.ReportPrintOptions.PrintOnEmptyDataSource = False
            Me.ScriptLanguage = DevExpress.XtraReports.ScriptLanguage.VisualBasic
            Me.Scripts.OnBeforePrint = "NonDMPDebts_BeforePrint"
            Me.ScriptsSource = resources.GetString("$this.ScriptsSource")
            Me.StyleSheet.AddRange(New DevExpress.XtraReports.UI.XRControlStyle() {Me.XrControlStyle_HeaderText, Me.XrControlStyle_HeaderLine, Me.XrControlStyle_HeaderBox1, Me.XrControlStyle_HeaderBox2, Me.XrControlStyle_HeaderBox3, Me.XrControlStyle_Report_Column, Me.XrControlStyle_Report_Group, Me.XrControlStyle_Report_Detail, Me.XrControlStyle_Report_Total, Me.XrControlStyle_Footer, Me.XrControlStyle_HeaderSubTitle})
            Me.Version = "11.2"
            Me.Controls.SetChildIndex(Me.GroupHeader1, 0)
            Me.Controls.SetChildIndex(Me.ReportFooter, 0)
            Me.Controls.SetChildIndex(Me.PageHeader, 0)
            Me.Controls.SetChildIndex(Me.ReportHeader, 0)
            Me.Controls.SetChildIndex(Me.BottomMarginBand1, 0)
            Me.Controls.SetChildIndex(Me.TopMarginBand1, 0)
            Me.Controls.SetChildIndex(Me.Detail, 0)
            CType(Me.XrTable3, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.XrTable1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me, System.ComponentModel.ISupportInitialize).EndInit()

        End Sub

        Private ReportFooter As DevExpress.XtraReports.UI.ReportFooterBand
        Private GroupHeader1 As DevExpress.XtraReports.UI.GroupHeaderBand
        Private XrTable3 As DevExpress.XtraReports.UI.XRTable
        Private XrTableRow3 As DevExpress.XtraReports.UI.XRTableRow
        Private XrTableCell7 As DevExpress.XtraReports.UI.XRTableCell
        Private XrTableCell12 As DevExpress.XtraReports.UI.XRTableCell
        Private XrTableCell_total_balance As DevExpress.XtraReports.UI.XRTableCell
        Private XrTableCell_total_payment As DevExpress.XtraReports.UI.XRTableCell
        Private XrTableCell15 As DevExpress.XtraReports.UI.XRTableCell
        Private XrTable1 As DevExpress.XtraReports.UI.XRTable
        Private XrTableRow1 As DevExpress.XtraReports.UI.XRTableRow
        Private XrTableCell1 As DevExpress.XtraReports.UI.XRTableCell
        Private XrTableCell10 As DevExpress.XtraReports.UI.XRTableCell
        Private XrTableCell2 As DevExpress.XtraReports.UI.XRTableCell
        Private XrTableCell13 As DevExpress.XtraReports.UI.XRTableCell
        Private XrTableCell3 As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents XrLabel_detail_creditor_name As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_detail_account_number As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_detail_balance As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_detail_payment As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_detail_interest_rate As DevExpress.XtraReports.UI.XRLabel
    End Class
End Namespace
