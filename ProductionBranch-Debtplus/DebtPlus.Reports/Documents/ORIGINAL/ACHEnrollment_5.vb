﻿Namespace Documents
    Public Class ACHEnrollment_5
        Public Sub New()
            MyBase.New()
            InitializeComponent()
            ' AddHandler BeforePrint, AddressOf ACHEnrollment_5_BeforePrint
        End Sub

        ''' <summary>
        ''' Class for the output record
        ''' </summary>
        ''' <remarks></remarks>
        Public Class record
            Private ReadOnly private_client As Int32
            Public ReadOnly Property client As Int32
                Get
                    Return private_client
                End Get
            End Property
            Public Sub New()
            End Sub
            Public Sub New(ByVal client As Int32)
                MyClass.New()
                MyClass.private_client = client
            End Sub
        End Class

        Private Sub ACHEnrollment_5_BeforePrint(sender As Object, e As System.Drawing.Printing.PrintEventArgs)

            ' Find the current report
            Dim rpt As DevExpress.XtraReports.UI.XtraReport = TryCast(sender, DevExpress.XtraReports.UI.XtraReport)
            If rpt Is Nothing Then
                Return
            End If

            ' Find the current Client number
            Dim MasterRpt As DebtPlus.Interfaces.Client.IClient = TryCast(rpt.MasterReport, DebtPlus.Interfaces.Client.IClient)
            Dim ClientID As Int32 = -1
            If MasterRpt IsNot Nothing Then
                ClientID = MasterRpt.ClientId
            End If

            If ClientID < 0 Then
                Return
            End If

            ' Generate a record list with the single record
            Dim rec As New record(ClientID)
            Dim lst As New System.Collections.Generic.List(Of record)()
            lst.Add(rec)
            rpt.DataSource = lst
        End Sub
    End Class
End Namespace
