﻿Namespace Documents
    Public Class EnglishFHLBInvoice_1
        Public Sub New()
            MyBase.New()
            InitializeComponent()
            ' AddHandler BeforePrint, AddressOf FHLB_Invoice_1_BeforePrint
        End Sub

        Private Class dataItems
            ''' <summary>
            ''' Initialize the new class
            ''' </summary>
            Public Sub New()
                Quantity = 1
            End Sub

            Public Property Description As String = Nothing
            Public Property ItemNo As String = Nothing
            Public Property Quantity As System.Nullable(Of Int32) = Nothing
            Public Property Discount As System.Nullable(Of Decimal) = Nothing
            Public Property UnitPrice As System.Nullable(Of Decimal) = Nothing
            Public Property Payments As System.Nullable(Of Decimal) = Nothing
            Public Property LineTotal As System.Nullable(Of Decimal) = Nothing
        End Class

        Private Sub ReadAppointmentData(cn As System.Data.SqlClient.SqlConnection, clientID As Int32)

            Using cmd As New System.Data.SqlClient.SqlCommand()
                cmd.Connection = cn
                cmd.CommandText = "SELECT TOP 1 ca.start_time, ca.partner FROM client_appointments ca WITH (NOLOCK) WHERE ca.client = @client AND ltrim(rtrim(isnull(ca.partner,''))) <> '' AND ca.status in ('K','W') AND ca.office IS NOT NULL ORDER BY ca.start_time DESC"
                cmd.Parameters.Add("@client", System.Data.SqlDbType.Int).Value = clientID
                cmd.CommandTimeout = 0
                cmd.CommandType = System.Data.CommandType.Text

                Using rd As System.Data.SqlClient.SqlDataReader = cmd.ExecuteReader(System.Data.CommandBehavior.SingleRow)
                    If rd.Read() Then
                        XrTableCell_Date.Text = rd.GetDateTime(0).ToString("M/d/yy")
                        XrTableCell_appointment_date.Text = rd.GetDateTime(0).ToString("MMMM d, yyyy")
                    End If
                    rd.Close()
                End Using
            End Using

        End Sub

        Private Function getCreditorData(cn As System.Data.SqlClient.SqlConnection, clientID As Int32, creditorID As String) As System.Collections.Generic.List(Of dataItems)
            Dim colAnswer = New System.Collections.Generic.List(Of dataItems)()

            Using cmd As New System.Data.SqlClient.SqlCommand()
                cmd.Connection = cn
                cmd.CommandText = "SELECT TOP 1 bal.orig_balance as orig_balance, bal.total_payments as payments, cc.creditor as creditor, cr.creditor_name as creditor_name FROM client_creditor cc WITH (NOLOCK) INNER JOIN client_creditor_balances bal WITH (NOLOCK) ON cc.client_creditor_balance = bal.client_creditor_balance INNER JOIN creditors cr on cr.creditor = cc.creditor WHERE cc.client = @clientID AND cc.creditor = @creditor AND cc.reassigned_debt = 0"
                cmd.CommandType = System.Data.CommandType.Text
                cmd.CommandTimeout = 0
                cmd.Parameters.Add("@clientID", System.Data.SqlDbType.Int).Value = clientID
                cmd.Parameters.Add("@creditor", System.Data.SqlDbType.VarChar, 10).Value = creditorID

                Dim rd As System.Data.SqlClient.SqlDataReader = cmd.ExecuteReader(System.Data.CommandBehavior.SingleRow)
                Do While rd.Read()
                    Dim item As New dataItems()
                    colAnswer.Add(item)
                    item.Quantity = 1

                    If Not rd.IsDBNull(0) Then
                        item.UnitPrice = rd.GetDecimal(0)
                    End If

                    If Not rd.IsDBNull(1) Then
                        item.Payments = rd.GetDecimal(1)
                    End If

                    If Not rd.IsDBNull(2) Then
                        item.ItemNo = rd.GetString(2).Trim()
                    End If

                    If Not rd.IsDBNull(3) Then
                        item.Description = rd.GetString(3).Trim()
                    End If

                    item.LineTotal = item.UnitPrice.GetValueOrDefault() - item.Payments.GetValueOrDefault()
                Loop

                rd.Close()
            End Using

            Return colAnswer
        End Function

        Private Sub ReadClientData(cn As System.Data.SqlClient.SqlConnection, clientID As Int32)

            ' Insert the client id
            XrTableCell_Client.Text = DebtPlus.Utils.Format.Client.FormatClientID(clientID)
            XrTableCell_Footer_Client.Text = DebtPlus.Utils.Format.Client.FormatClientID(clientID)

            ' Read the client information from the view if needed
            Using cmd As New System.Data.SqlClient.SqlCommand()
                cmd.Connection = cn
                cmd.CommandText = "SELECT [name],[addr1],[addr2],[addr3] FROM [view_client_address] WITH (NOLOCK) WHERE [client] = @client"
                cmd.CommandType = System.Data.CommandType.Text
                cmd.CommandTimeout = 0
                cmd.Parameters.Add("@client", System.Data.SqlDbType.Int).Value = clientID

                Dim rd As System.Data.SqlClient.SqlDataReader = cmd.ExecuteReader(System.Data.CommandBehavior.SingleRow)
                If rd.Read() Then
                    If Not rd.IsDBNull(0) Then
                        Dim nameString As String = rd.GetString(0).Trim()
                        XrTableCell_client_name.Text = nameString
                        XrTableCell_Footer_Name.Text = nameString
                    End If

                    Dim sbAddress As New System.Text.StringBuilder()
                    For colNo As Int32 = 1 To 3
                        If rd.IsDBNull(colNo) Then
                            Continue For
                        End If
                        Dim strItem As String = rd.GetString(colNo).Trim()
                        If Not String.IsNullOrEmpty(strItem) Then
                            sbAddress.Append(Environment.NewLine)
                            sbAddress.Append(strItem)
                        End If
                    Next

                    If sbAddress.Length > 0 Then
                        sbAddress.Remove(0, 2)
                    End If

                    XrTableCell_client_address.Text = sbAddress.ToString()
                    XrTableCell_Footer_Address.Text = sbAddress.ToString()
                End If
            End Using
        End Sub

        Private Sub FHLB_Invoice_1_BeforePrint(sender As Object, e As System.Drawing.Printing.PrintEventArgs)

            Dim rpt As DevExpress.XtraReports.UI.XtraReport = TryCast(sender, DevExpress.XtraReports.UI.XtraReport)
            If rpt Is Nothing Then
                Return
            End If

            ' Find the client from our own parameter value. The base report sets it when the client is supplied.
            Dim ClientID As Int32 = -1
            Dim parm As DevExpress.XtraReports.Parameters.Parameter = rpt.Parameters("ParameterClient")
            If parm IsNot Nothing Then
                ClientID = Convert.ToInt32(parm.Value)
            End If

            If ClientID < 0 Then
                Return
            End If

            ' Find the creditor from the report parameters
            Dim creditor As String = String.Empty
            parm = rpt.Parameters("ParameterCreditor")
            If parm IsNot Nothing Then
                creditor = Convert.ToString(parm.Value).Trim()
            End If

            Try
                Using cn As New System.Data.SqlClient.SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
                    cn.Open()
                    rpt.DataSource = getCreditorData(cn, ClientID, creditor)
                    ReadAppointmentData(cn, ClientID)
                    ReadClientData(cn, ClientID)
                    cn.Close()
                End Using

            Catch ex As System.Data.SqlClient.SqlException
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex)
            End Try
        End Sub
    End Class
End Namespace
