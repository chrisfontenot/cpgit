﻿Namespace Documents
    Public Class EnglishBudgetInformation_3
        Public Sub New()
            MyBase.New()
            InitializeComponent()
            ' AddHandler XrTableCell_budget_category.BeforePrint, AddressOf XrTableCell_budget_category_BeforePrint
            ' AddHandler BeforePrint, AddressOf EnglishBudgetInformation_BeforePrint
        End Sub

        Private Class RecordInformation
            Public Sub New()
            End Sub

            Public Property budget_category As String = String.Empty
            Public Property client_amount As Decimal = 0D
            Public Property suggested_amount As Decimal = 0D
            Public Property heading As Boolean = False
            Private Property evenLine As Boolean = False

            Public ReadOnly Property difference As Decimal
                Get
                    Return client_amount - suggested_amount
                End Get
            End Property

            Public Sub New(rdr As System.Data.SqlClient.SqlDataReader, evenLine As Boolean)
                MyClass.New()

                Dim ordinal As Int32 = rdr.GetOrdinal("description")
                If ordinal >= 0 AndAlso Not rdr.IsDBNull(ordinal) Then
                    budget_category = rdr.GetString(ordinal).Trim()
                End If

                ordinal = rdr.GetOrdinal("client_amount")
                If ordinal >= 0 AndAlso Not rdr.IsDBNull(ordinal) Then
                    client_amount = rdr.GetDecimal(ordinal)
                End If

                ordinal = rdr.GetOrdinal("suggested_amount")
                If ordinal >= 0 AndAlso Not rdr.IsDBNull(ordinal) Then
                    suggested_amount = rdr.GetDecimal(ordinal)
                End If

                ordinal = rdr.GetOrdinal("heading")
                If ordinal >= 0 AndAlso Not rdr.IsDBNull(ordinal) Then
                    heading = Convert.ToInt32(rdr.GetValue(ordinal)) <> 0
                End If

            End Sub
        End Class

        Private Sub EnglishBudgetInformation_BeforePrint(sender As Object, e As System.Drawing.Printing.PrintEventArgs)
            Dim rpt As DevExpress.XtraReports.UI.XtraReport = TryCast(sender, DevExpress.XtraReports.UI.XtraReport)
            If rpt Is Nothing Then
                Return
            End If

            Dim MasterRpt As DebtPlus.Interfaces.Client.IClient = TryCast(rpt.MasterReport, DebtPlus.Interfaces.Client.IClient)
            Dim ClientID As Int32 = -1
            If MasterRpt IsNot Nothing Then
                ClientID = MasterRpt.ClientId
            End If

            If ClientID < 0 Then
                Return
            End If

            Try
                Dim recordSet As New System.Collections.Generic.List(Of RecordInformation)
                Using cn As New System.Data.SqlClient.SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
                    cn.Open()
                    Dim Budget As Integer = 0

                    ' Try to find the last budget for this client
                    Using cmdBudget As System.Data.SqlClient.SqlCommand = New System.Data.SqlClient.SqlCommand()
                        cmdBudget.Connection = cn
                        cmdBudget.CommandText = "SELECT dbo.map_client_to_budget(@client)"
                        cmdBudget.CommandType = System.Data.CommandType.Text
                        cmdBudget.Parameters.Add("@client", System.Data.SqlDbType.Int).Value = ClientID
                        Dim obj As Object = cmdBudget.ExecuteScalar()
                        If Not System.Object.Equals(System.DBNull.Value, obj) AndAlso obj IsNot Nothing Then
                            Budget = Convert.ToInt32(obj)
                        End If
                    End Using

                    If Budget > 0 Then
                        ' Once we have a budget then we can access the budget data
                        Using cmd As System.Data.SqlClient.SqlCommand = New System.Data.SqlClient.SqlCommand()
                            cmd.Connection = cn
                            cmd.CommandText = "rpt_PrintClient_BudgetDetail_ByBudget"
                            cmd.CommandType = System.Data.CommandType.StoredProcedure
                            cmd.CommandTimeout = 0
                            cmd.Parameters.Add("@budget", System.Data.SqlDbType.Int).Value = Budget

                            ' Process all of the records in the RecordSet for the data
                            Using rdr As System.Data.SqlClient.SqlDataReader = cmd.ExecuteReader()
                                Dim evenLine As Boolean = False
                                While rdr.Read()
                                    evenLine = Not evenLine
                                    Dim record As New RecordInformation(rdr, evenLine)
                                    recordSet.Add(record)
                                    If record.heading Then
                                        evenLine = False
                                    End If
                                End While
                            End Using
                        End Using
                    End If
                End Using

                ' Set the RecordSet for the report
                rpt.DataSource = recordSet

            Catch ex As Exception
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex)
            End Try
        End Sub

        ''' <summary>
        ''' Adjust the padding so that the detail information is indented from the heading
        ''' </summary>
        Private Sub XrTableCell_budget_category_BeforePrint(sender As Object, e As System.Drawing.Printing.PrintEventArgs)

            ' Find the label
            Dim cell As DevExpress.XtraReports.UI.XRTableCell = TryCast(sender, DevExpress.XtraReports.UI.XRTableCell)
            If cell Is Nothing Then
                Return
            End If

            ' Find the report from the label
            Dim rpt As DevExpress.XtraReports.UI.XtraReport = TryCast(cell.Report, DevExpress.XtraReports.UI.XtraReport)
            Dim record As RecordInformation = TryCast(rpt.GetCurrentRow(), RecordInformation)
            If record Is Nothing Then
                Return
            End If

            If record.heading Then
                cell.Padding = New DevExpress.XtraPrinting.PaddingInfo(15, 5, 0, 0, 100.0!)
            Else
                cell.Padding = New DevExpress.XtraPrinting.PaddingInfo(35, 5, 0, 0, 100.0!)
            End If
        End Sub
    End Class
End Namespace
