﻿Namespace Documents
    <Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
    Partial Public Class SpanishFinancialNetWorth_3
        Inherits DevExpress.XtraReports.UI.XtraReport

        'XtraReport overrides dispose to clean up the component list.
        <System.Diagnostics.DebuggerNonUserCode()> _
        Protected Overrides Sub Dispose(ByVal disposing As Boolean)
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
            MyBase.Dispose(disposing)
        End Sub

        'Required by the Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Designer
        'It can be modified using the Designer.  
        'Do not modify it using the code editor.
        <System.Diagnostics.DebuggerStepThrough()> _
        Private Sub InitializeComponent()
            Dim XrSummary2 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary()
            Dim XrSummary3 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary()
            Dim XrSummary1 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary()
            Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(SpanishFinancialNetWorth_3))
            Me.XrTableCell_l_value = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTable3 = New DevExpress.XtraReports.UI.XRTable()
            Me.XrControlStyle_Report_Detail = New DevExpress.XtraReports.UI.XRControlStyle()
            Me.PageHeader = New DevExpress.XtraReports.UI.PageHeaderBand()
            Me.XrControlStyle_Report_Column = New DevExpress.XtraReports.UI.XRControlStyle()
            Me.XrLabel1 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrTableCell4 = New DevExpress.XtraReports.UI.XRTableCell()
            Me.FormattingRule_l_HideDetails = New DevExpress.XtraReports.UI.FormattingRule()
            Me.TopMarginBand1 = New DevExpress.XtraReports.UI.TopMarginBand()
            Me.XrTableCell6 = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableRow5 = New DevExpress.XtraReports.UI.XRTableRow()
            Me.XrTableCell_total_r_value = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTable2 = New DevExpress.XtraReports.UI.XRTable()
            Me.XrTableCell_r_description = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrControlStyle_HeaderText = New DevExpress.XtraReports.UI.XRControlStyle()
            Me.XrControlStyle_HeaderBox1 = New DevExpress.XtraReports.UI.XRControlStyle()
            Me.XrLabel_ReportTitle = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrTableCell_total_l_value = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableCell13 = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrControlStyle_HeaderSubTitle = New DevExpress.XtraReports.UI.XRControlStyle()
            Me.XrTable1 = New DevExpress.XtraReports.UI.XRTable()
            Me.XrTableCell1 = New DevExpress.XtraReports.UI.XRTableCell()
            Me.FormattingRule_l_ShowHeading = New DevExpress.XtraReports.UI.FormattingRule()
            Me.XrLabel_pageHeaderBox3 = New DevExpress.XtraReports.UI.XRLabel()
            Me.BottomMarginBand1 = New DevExpress.XtraReports.UI.BottomMarginBand()
            Me.Detail = New DevExpress.XtraReports.UI.DetailBand()
            Me.XrControlStyle_HeaderBox3 = New DevExpress.XtraReports.UI.XRControlStyle()
            Me.XrTableCell5 = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrLabel_reportHeaderBox1 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_reportHeaderBox3 = New DevExpress.XtraReports.UI.XRLabel()
            Me.GroupFooter1 = New DevExpress.XtraReports.UI.GroupFooterBand()
            Me.XrLabel_reportHeaderBox2 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_pageHeader_Title = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrControlStyle_HeaderBox2 = New DevExpress.XtraReports.UI.XRControlStyle()
            Me.FormattingRule_r_ShowHeading = New DevExpress.XtraReports.UI.FormattingRule()
            Me.XrTableCell_l_description = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableCell_total_difference = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrLine_reportHeaderLine = New DevExpress.XtraReports.UI.XRLine()
            Me.XrTableRow1 = New DevExpress.XtraReports.UI.XRTableRow()
            Me.XrLabel_pageHeaderBox2 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrTableRow3 = New DevExpress.XtraReports.UI.XRTableRow()
            Me.XrLine_pageHeaderLine = New DevExpress.XtraReports.UI.XRLine()
            Me.XrControlStyle_Footer = New DevExpress.XtraReports.UI.XRControlStyle()
            Me.XrControlStyle_Report_Group = New DevExpress.XtraReports.UI.XRControlStyle()
            Me.GroupHeader1 = New DevExpress.XtraReports.UI.GroupHeaderBand()
            Me.XrTableCell12 = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrControlStyle_Report_Total = New DevExpress.XtraReports.UI.XRControlStyle()
            Me.XrTableCell7 = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableCell_r_value = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrControlStyle_HeaderLine = New DevExpress.XtraReports.UI.XRControlStyle()
            Me.XrTableCell2 = New DevExpress.XtraReports.UI.XRTableCell()
            Me.FormattingRule_r_HideDetails = New DevExpress.XtraReports.UI.FormattingRule()
            Me.XrTableRow2 = New DevExpress.XtraReports.UI.XRTableRow()
            Me.ReportHeader = New DevExpress.XtraReports.UI.ReportHeaderBand()
            Me.XrLabel_pageHeaderBox1 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrTableCell14 = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableRow4 = New DevExpress.XtraReports.UI.XRTableRow()
            CType(Me.XrTable3, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.XrTable2, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.XrTable1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
            '
            'XrTableCell_l_value
            '
            Me.XrTableCell_l_value.CanGrow = False
            Me.XrTableCell_l_value.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "l_value", "{0:c}")})
            Me.XrTableCell_l_value.FormattingRules.Add(Me.FormattingRule_l_HideDetails)
            Me.XrTableCell_l_value.Name = "XrTableCell_l_value"
            Me.XrTableCell_l_value.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 5, 0, 0, 100.0!)
            Me.XrTableCell_l_value.StylePriority.UsePadding = False
            Me.XrTableCell_l_value.StylePriority.UseTextAlignment = False
            Me.XrTableCell_l_value.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrTableCell_l_value.Weight = 0.648953376932347R
            '
            'XrTable3
            '
            Me.XrTable3.AnchorVertical = CType((DevExpress.XtraReports.UI.VerticalAnchorStyles.Top Or DevExpress.XtraReports.UI.VerticalAnchorStyles.Bottom), DevExpress.XtraReports.UI.VerticalAnchorStyles)
            Me.XrTable3.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
            Me.XrTable3.Name = "XrTable3"
            Me.XrTable3.Padding = New DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 100.0!)
            Me.XrTable3.Rows.AddRange(New DevExpress.XtraReports.UI.XRTableRow() {Me.XrTableRow3})
            Me.XrTable3.SizeF = New System.Drawing.SizeF(789.9982!, 23.0!)
            '
            'XrControlStyle_Report_Detail
            '
            Me.XrControlStyle_Report_Detail.BackColor = System.Drawing.Color.FromArgb(CType(CType(222, Byte), Integer), CType(CType(222, Byte), Integer), CType(CType(233, Byte), Integer))
            Me.XrControlStyle_Report_Detail.BorderColor = System.Drawing.Color.White
            Me.XrControlStyle_Report_Detail.BorderDashStyle = DevExpress.XtraPrinting.BorderDashStyle.Solid
            Me.XrControlStyle_Report_Detail.Borders = DevExpress.XtraPrinting.BorderSide.Bottom
            Me.XrControlStyle_Report_Detail.BorderWidth = 2
            Me.XrControlStyle_Report_Detail.Font = New System.Drawing.Font("Calibri", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.XrControlStyle_Report_Detail.Name = "XrControlStyle_Report_Detail"
            Me.XrControlStyle_Report_Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            '
            'PageHeader
            '
            Me.PageHeader.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLine_pageHeaderLine, Me.XrLabel_pageHeader_Title, Me.XrLabel_pageHeaderBox1, Me.XrLabel_pageHeaderBox2, Me.XrLabel_pageHeaderBox3})
            Me.PageHeader.HeightF = 135.0!
            Me.PageHeader.Name = "PageHeader"
            Me.PageHeader.PrintOn = DevExpress.XtraReports.UI.PrintOnPages.NotWithReportHeader
            '
            'XrControlStyle_Report_Column
            '
            Me.XrControlStyle_Report_Column.BackColor = System.Drawing.Color.Teal
            Me.XrControlStyle_Report_Column.BorderColor = System.Drawing.Color.White
            Me.XrControlStyle_Report_Column.BorderDashStyle = DevExpress.XtraPrinting.BorderDashStyle.Solid
            Me.XrControlStyle_Report_Column.Borders = DevExpress.XtraPrinting.BorderSide.Bottom
            Me.XrControlStyle_Report_Column.BorderWidth = 2
            Me.XrControlStyle_Report_Column.Font = New System.Drawing.Font("Calibri", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.XrControlStyle_Report_Column.ForeColor = System.Drawing.Color.White
            Me.XrControlStyle_Report_Column.Name = "XrControlStyle_Report_Column"
            Me.XrControlStyle_Report_Column.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrControlStyle_Report_Column.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            '
            'XrLabel1
            '
            Me.XrLabel1.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 10.0!)
            Me.XrLabel1.Name = "XrLabel1"
            Me.XrLabel1.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel1.SizeF = New System.Drawing.SizeF(789.9982!, 21.88!)
            Me.XrLabel1.StyleName = "XrControlStyle_Report_Column"
            Me.XrLabel1.StylePriority.UseTextAlignment = False
            Me.XrLabel1.Text = "CALCULATE YOUR NET WORTH"
            Me.XrLabel1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
            '
            'XrTableCell4
            '
            Me.XrTableCell4.CanGrow = False
            Me.XrTableCell4.Name = "XrTableCell4"
            Me.XrTableCell4.Padding = New DevExpress.XtraPrinting.PaddingInfo(5, 0, 0, 0, 100.0!)
            Me.XrTableCell4.StylePriority.UsePadding = False
            Me.XrTableCell4.Text = "ASSETS"
            Me.XrTableCell4.Weight = 1.3510437197903722R
            '
            'FormattingRule_l_HideDetails
            '
            Me.FormattingRule_l_HideDetails.Condition = "[l_heading] == True"
            '
            '
            '
            Me.FormattingRule_l_HideDetails.Formatting.BackColor = System.Drawing.Color.FromArgb(CType(CType(102, Byte), Integer), CType(CType(102, Byte), Integer), CType(CType(153, Byte), Integer))
            Me.FormattingRule_l_HideDetails.Formatting.BorderColor = System.Drawing.Color.White
            Me.FormattingRule_l_HideDetails.Formatting.Borders = DevExpress.XtraPrinting.BorderSide.Bottom
            Me.FormattingRule_l_HideDetails.Formatting.BorderWidth = 1
            Me.FormattingRule_l_HideDetails.Formatting.Font = New System.Drawing.Font("Calibri", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.FormattingRule_l_HideDetails.Formatting.ForeColor = System.Drawing.Color.FromArgb(CType(CType(102, Byte), Integer), CType(CType(102, Byte), Integer), CType(CType(153, Byte), Integer))
            Me.FormattingRule_l_HideDetails.Name = "FormattingRule_l_HideDetails"
            '
            'TopMarginBand1
            '
            Me.TopMarginBand1.HeightF = 25.0!
            Me.TopMarginBand1.Name = "TopMarginBand1"
            '
            'XrTableCell6
            '
            Me.XrTableCell6.CanGrow = False
            Me.XrTableCell6.Name = "XrTableCell6"
            Me.XrTableCell6.Padding = New DevExpress.XtraPrinting.PaddingInfo(5, 0, 0, 0, 100.0!)
            Me.XrTableCell6.StylePriority.UsePadding = False
            Me.XrTableCell6.Text = "LIABILITIES"
            Me.XrTableCell6.Weight = 1.2499832712482073R
            '
            'XrTableRow5
            '
            Me.XrTableRow5.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.XrTableCell2, Me.XrTableCell_total_difference})
            Me.XrTableRow5.Name = "XrTableRow5"
            Me.XrTableRow5.Weight = 0.87519996643066422R
            '
            'XrTableCell_total_r_value
            '
            Me.XrTableCell_total_r_value.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "r_value")})
            Me.XrTableCell_total_r_value.Name = "XrTableCell_total_r_value"
            Me.XrTableCell_total_r_value.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 5, 0, 0, 100.0!)
            Me.XrTableCell_total_r_value.StylePriority.UsePadding = False
            Me.XrTableCell_total_r_value.StylePriority.UseTextAlignment = False
            XrSummary2.FormatString = "{0:C}"
            XrSummary2.Running = DevExpress.XtraReports.UI.SummaryRunning.Report
            Me.XrTableCell_total_r_value.Summary = XrSummary2
            Me.XrTableCell_total_r_value.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrTableCell_total_r_value.Weight = 1.3565267660198022R
            '
            'XrTable2
            '
            Me.XrTable2.AnchorVertical = DevExpress.XtraReports.UI.VerticalAnchorStyles.Bottom
            Me.XrTable2.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
            Me.XrTable2.Name = "XrTable2"
            Me.XrTable2.Padding = New DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 100.0!)
            Me.XrTable2.Rows.AddRange(New DevExpress.XtraReports.UI.XRTableRow() {Me.XrTableRow2})
            Me.XrTable2.SizeF = New System.Drawing.SizeF(790.0!, 21.88!)
            Me.XrTable2.StylePriority.UsePadding = False
            '
            'XrTableCell_r_description
            '
            Me.XrTableCell_r_description.CanGrow = False
            Me.XrTableCell_r_description.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "r_description")})
            Me.XrTableCell_r_description.FormattingRules.Add(Me.FormattingRule_r_ShowHeading)
            Me.XrTableCell_r_description.Name = "XrTableCell_r_description"
            Me.XrTableCell_r_description.Padding = New DevExpress.XtraPrinting.PaddingInfo(5, 0, 0, 0, 100.0!)
            Me.XrTableCell_r_description.Scripts.OnBeforePrint = "XrTableCell_r_description_BeforePrint"
            Me.XrTableCell_r_description.StylePriority.UsePadding = False
            Me.XrTableCell_r_description.Weight = 1.2499860528174866R
            '
            'XrControlStyle_HeaderText
            '
            Me.XrControlStyle_HeaderText.Font = New System.Drawing.Font("Calibri", 27.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.XrControlStyle_HeaderText.ForeColor = System.Drawing.Color.Teal
            Me.XrControlStyle_HeaderText.Name = "XrControlStyle_HeaderText"
            Me.XrControlStyle_HeaderText.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
            Me.XrControlStyle_HeaderText.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            '
            'XrControlStyle_HeaderBox1
            '
            Me.XrControlStyle_HeaderBox1.BackColor = System.Drawing.Color.Teal
            Me.XrControlStyle_HeaderBox1.BorderColor = System.Drawing.Color.Teal
            Me.XrControlStyle_HeaderBox1.ForeColor = System.Drawing.Color.Teal
            Me.XrControlStyle_HeaderBox1.Name = "XrControlStyle_HeaderBox1"
            '
            'XrLabel_ReportTitle
            '
            Me.XrLabel_ReportTitle.LocationFloat = New DevExpress.Utils.PointFloat(10.00001!, 10.00001!)
            Me.XrLabel_ReportTitle.Multiline = True
            Me.XrLabel_ReportTitle.Name = "XrLabel_ReportTitle"
            Me.XrLabel_ReportTitle.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_ReportTitle.SizeF = New System.Drawing.SizeF(564.5833!, 98.0!)
            Me.XrLabel_ReportTitle.StyleName = "XrControlStyle_HeaderText"
            Me.XrLabel_ReportTitle.Text = "PATRIMONIO" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "FAMILIAR"
            '
            'XrTableCell_total_l_value
            '
            Me.XrTableCell_total_l_value.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "l_value")})
            Me.XrTableCell_total_l_value.Name = "XrTableCell_total_l_value"
            Me.XrTableCell_total_l_value.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 5, 0, 0, 100.0!)
            Me.XrTableCell_total_l_value.StylePriority.UsePadding = False
            Me.XrTableCell_total_l_value.StylePriority.UseTextAlignment = False
            XrSummary3.FormatString = "{0:C}"
            XrSummary3.Running = DevExpress.XtraReports.UI.SummaryRunning.Report
            Me.XrTableCell_total_l_value.Summary = XrSummary3
            Me.XrTableCell_total_l_value.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrTableCell_total_l_value.Weight = 1.3565271912310595R
            '
            'XrTableCell13
            '
            Me.XrTableCell13.BackColor = System.Drawing.Color.Transparent
            Me.XrTableCell13.CanGrow = False
            Me.XrTableCell13.Name = "XrTableCell13"
            Me.XrTableCell13.StylePriority.UseBackColor = False
            Me.XrTableCell13.Weight = 0.1010682009504453R
            '
            'XrControlStyle_HeaderSubTitle
            '
            Me.XrControlStyle_HeaderSubTitle.Font = New System.Drawing.Font("Calibri", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.XrControlStyle_HeaderSubTitle.Name = "XrControlStyle_HeaderSubTitle"
            Me.XrControlStyle_HeaderSubTitle.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            '
            'XrTable1
            '
            Me.XrTable1.LocationFloat = New DevExpress.Utils.PointFloat(359.3759!, 41.88!)
            Me.XrTable1.Name = "XrTable1"
            Me.XrTable1.Rows.AddRange(New DevExpress.XtraReports.UI.XRTableRow() {Me.XrTableRow1, Me.XrTableRow4, Me.XrTableRow5})
            Me.XrTable1.SizeF = New System.Drawing.SizeF(430.6223!, 65.64!)
            Me.XrTable1.StyleName = "XrControlStyle_Report_Total"
            Me.XrTable1.StylePriority.UseTextAlignment = False
            Me.XrTable1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            '
            'XrTableCell1
            '
            Me.XrTableCell1.Name = "XrTableCell1"
            Me.XrTableCell1.Padding = New DevExpress.XtraPrinting.PaddingInfo(5, 0, 0, 0, 100.0!)
            Me.XrTableCell1.StylePriority.UsePadding = False
            Me.XrTableCell1.Text = "TOTAL ASSETS"
            Me.XrTableCell1.Weight = 1.6434728087689405R
            '
            'FormattingRule_l_ShowHeading
            '
            Me.FormattingRule_l_ShowHeading.Condition = "[l_heading] == True"
            '
            '
            '
            Me.FormattingRule_l_ShowHeading.Formatting.BackColor = System.Drawing.Color.FromArgb(CType(CType(102, Byte), Integer), CType(CType(102, Byte), Integer), CType(CType(153, Byte), Integer))
            Me.FormattingRule_l_ShowHeading.Formatting.BorderColor = System.Drawing.Color.White
            Me.FormattingRule_l_ShowHeading.Formatting.Borders = DevExpress.XtraPrinting.BorderSide.Bottom
            Me.FormattingRule_l_ShowHeading.Formatting.BorderWidth = 1
            Me.FormattingRule_l_ShowHeading.Formatting.Font = New System.Drawing.Font("Calibri", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.FormattingRule_l_ShowHeading.Formatting.ForeColor = System.Drawing.Color.White
            Me.FormattingRule_l_ShowHeading.Name = "FormattingRule_l_ShowHeading"
            '
            'XrLabel_pageHeaderBox3
            '
            Me.XrLabel_pageHeaderBox3.LocationFloat = New DevExpress.Utils.PointFloat(734.79!, 27.045!)
            Me.XrLabel_pageHeaderBox3.Name = "XrLabel_pageHeaderBox3"
            Me.XrLabel_pageHeaderBox3.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_pageHeaderBox3.SizeF = New System.Drawing.SizeF(56.0!, 56.0!)
            Me.XrLabel_pageHeaderBox3.StyleName = "XrControlStyle_HeaderBox3"
            '
            'BottomMarginBand1
            '
            Me.BottomMarginBand1.HeightF = 25.0!
            Me.BottomMarginBand1.Name = "BottomMarginBand1"
            '
            'Detail
            '
            Me.Detail.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrTable3})
            Me.Detail.HeightF = 23.0!
            Me.Detail.Name = "Detail"
            '
            'XrControlStyle_HeaderBox3
            '
            Me.XrControlStyle_HeaderBox3.BackColor = System.Drawing.Color.FromArgb(CType(CType(137, Byte), Integer), CType(CType(201, Byte), Integer), CType(CType(191, Byte), Integer))
            Me.XrControlStyle_HeaderBox3.BorderColor = System.Drawing.Color.FromArgb(CType(CType(137, Byte), Integer), CType(CType(201, Byte), Integer), CType(CType(191, Byte), Integer))
            Me.XrControlStyle_HeaderBox3.ForeColor = System.Drawing.Color.FromArgb(CType(CType(137, Byte), Integer), CType(CType(201, Byte), Integer), CType(CType(191, Byte), Integer))
            Me.XrControlStyle_HeaderBox3.Name = "XrControlStyle_HeaderBox3"
            '
            'XrTableCell5
            '
            Me.XrTableCell5.CanGrow = False
            Me.XrTableCell5.Name = "XrTableCell5"
            Me.XrTableCell5.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 5, 0, 0, 100.0!)
            Me.XrTableCell5.StylePriority.UsePadding = False
            Me.XrTableCell5.StylePriority.UseTextAlignment = False
            Me.XrTableCell5.Text = "RESALE VALUE"
            Me.XrTableCell5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrTableCell5.Weight = 0.64895178649916252R
            '
            'XrLabel_reportHeaderBox1
            '
            Me.XrLabel_reportHeaderBox1.LocationFloat = New DevExpress.Utils.PointFloat(601.0417!, 27.045!)
            Me.XrLabel_reportHeaderBox1.Name = "XrLabel_reportHeaderBox1"
            Me.XrLabel_reportHeaderBox1.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_reportHeaderBox1.SizeF = New System.Drawing.SizeF(56.0!, 56.0!)
            Me.XrLabel_reportHeaderBox1.StyleName = "XrControlStyle_HeaderBox1"
            '
            'XrLabel_reportHeaderBox3
            '
            Me.XrLabel_reportHeaderBox3.LocationFloat = New DevExpress.Utils.PointFloat(734.7917!, 27.045!)
            Me.XrLabel_reportHeaderBox3.Name = "XrLabel_reportHeaderBox3"
            Me.XrLabel_reportHeaderBox3.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_reportHeaderBox3.SizeF = New System.Drawing.SizeF(56.0!, 56.0!)
            Me.XrLabel_reportHeaderBox3.StyleName = "XrControlStyle_HeaderBox3"
            '
            'GroupFooter1
            '
            Me.GroupFooter1.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrTable1, Me.XrLabel1})
            Me.GroupFooter1.HeightF = 107.7084!
            Me.GroupFooter1.Name = "GroupFooter1"
            '
            'XrLabel_reportHeaderBox2
            '
            Me.XrLabel_reportHeaderBox2.LocationFloat = New DevExpress.Utils.PointFloat(667.9167!, 27.045!)
            Me.XrLabel_reportHeaderBox2.Name = "XrLabel_reportHeaderBox2"
            Me.XrLabel_reportHeaderBox2.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_reportHeaderBox2.SizeF = New System.Drawing.SizeF(56.0!, 56.0!)
            Me.XrLabel_reportHeaderBox2.StyleName = "XrControlStyle_HeaderBox2"
            '
            'XrLabel_pageHeader_Title
            '
            Me.XrLabel_pageHeader_Title.LocationFloat = New DevExpress.Utils.PointFloat(10.0!, 10.0!)
            Me.XrLabel_pageHeader_Title.Multiline = True
            Me.XrLabel_pageHeader_Title.Name = "XrLabel_pageHeader_Title"
            Me.XrLabel_pageHeader_Title.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_pageHeader_Title.SizeF = New System.Drawing.SizeF(564.5833!, 98.0!)
            Me.XrLabel_pageHeader_Title.StyleName = "XrControlStyle_HeaderText"
            Me.XrLabel_pageHeader_Title.Text = "PATRIMONIO" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "FAMILIAR (Continuación)"
            '
            'XrControlStyle_HeaderBox2
            '
            Me.XrControlStyle_HeaderBox2.BackColor = System.Drawing.Color.FromArgb(CType(CType(102, Byte), Integer), CType(CType(102, Byte), Integer), CType(CType(153, Byte), Integer))
            Me.XrControlStyle_HeaderBox2.BorderColor = System.Drawing.Color.FromArgb(CType(CType(102, Byte), Integer), CType(CType(102, Byte), Integer), CType(CType(153, Byte), Integer))
            Me.XrControlStyle_HeaderBox2.ForeColor = System.Drawing.Color.FromArgb(CType(CType(102, Byte), Integer), CType(CType(102, Byte), Integer), CType(CType(153, Byte), Integer))
            Me.XrControlStyle_HeaderBox2.Name = "XrControlStyle_HeaderBox2"
            '
            'FormattingRule_r_ShowHeading
            '
            Me.FormattingRule_r_ShowHeading.Condition = "[r_heading] == True"
            '
            '
            '
            Me.FormattingRule_r_ShowHeading.Formatting.BackColor = System.Drawing.Color.FromArgb(CType(CType(102, Byte), Integer), CType(CType(102, Byte), Integer), CType(CType(153, Byte), Integer))
            Me.FormattingRule_r_ShowHeading.Formatting.BorderColor = System.Drawing.Color.White
            Me.FormattingRule_r_ShowHeading.Formatting.Borders = DevExpress.XtraPrinting.BorderSide.Bottom
            Me.FormattingRule_r_ShowHeading.Formatting.BorderWidth = 1
            Me.FormattingRule_r_ShowHeading.Formatting.Font = New System.Drawing.Font("Calibri", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.FormattingRule_r_ShowHeading.Formatting.ForeColor = System.Drawing.Color.White
            Me.FormattingRule_r_ShowHeading.Name = "FormattingRule_r_ShowHeading"
            '
            'XrTableCell_l_description
            '
            Me.XrTableCell_l_description.BorderColor = System.Drawing.Color.White
            Me.XrTableCell_l_description.CanGrow = False
            Me.XrTableCell_l_description.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "l_description")})
            Me.XrTableCell_l_description.FormattingRules.Add(Me.FormattingRule_l_ShowHeading)
            Me.XrTableCell_l_description.Name = "XrTableCell_l_description"
            Me.XrTableCell_l_description.Padding = New DevExpress.XtraPrinting.PaddingInfo(5, 0, 0, 0, 100.0!)
            Me.XrTableCell_l_description.Scripts.OnBeforePrint = "XrTableCell_l_description_BeforePrint"
            Me.XrTableCell_l_description.StylePriority.UseBorderColor = False
            Me.XrTableCell_l_description.StylePriority.UsePadding = False
            Me.XrTableCell_l_description.Weight = 1.3510467659158909R
            '
            'XrTableCell_total_difference
            '
            Me.XrTableCell_total_difference.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "difference")})
            Me.XrTableCell_total_difference.Name = "XrTableCell_total_difference"
            Me.XrTableCell_total_difference.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 5, 0, 0, 100.0!)
            Me.XrTableCell_total_difference.StylePriority.UsePadding = False
            Me.XrTableCell_total_difference.StylePriority.UseTextAlignment = False
            XrSummary1.FormatString = "{0:C}"
            XrSummary1.Running = DevExpress.XtraReports.UI.SummaryRunning.Report
            Me.XrTableCell_total_difference.Summary = XrSummary1
            Me.XrTableCell_total_difference.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrTableCell_total_difference.Weight = 1.3565263408085448R
            '
            'XrLine_reportHeaderLine
            '
            Me.XrLine_reportHeaderLine.LineWidth = 2
            Me.XrLine_reportHeaderLine.LocationFloat = New DevExpress.Utils.PointFloat(12.5!, 108.0!)
            Me.XrLine_reportHeaderLine.Name = "XrLine_reportHeaderLine"
            Me.XrLine_reportHeaderLine.SizeF = New System.Drawing.SizeF(777.5!, 2.09!)
            Me.XrLine_reportHeaderLine.StyleName = "XrControlStyle_HeaderLine"
            '
            'XrTableRow1
            '
            Me.XrTableRow1.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.XrTableCell1, Me.XrTableCell_total_l_value})
            Me.XrTableRow1.Name = "XrTableRow1"
            Me.XrTableRow1.Weight = 0.875199966430664R
            '
            'XrLabel_pageHeaderBox2
            '
            Me.XrLabel_pageHeaderBox2.LocationFloat = New DevExpress.Utils.PointFloat(667.92!, 27.045!)
            Me.XrLabel_pageHeaderBox2.Name = "XrLabel_pageHeaderBox2"
            Me.XrLabel_pageHeaderBox2.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_pageHeaderBox2.SizeF = New System.Drawing.SizeF(56.0!, 56.0!)
            Me.XrLabel_pageHeaderBox2.StyleName = "XrControlStyle_HeaderBox2"
            '
            'XrTableRow3
            '
            Me.XrTableRow3.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.XrTableCell_l_description, Me.XrTableCell_l_value, Me.XrTableCell14, Me.XrTableCell_r_description, Me.XrTableCell_r_value})
            Me.XrTableRow3.Name = "XrTableRow3"
            Me.XrTableRow3.StyleName = "XrControlStyle_Report_Detail"
            Me.XrTableRow3.Weight = 1.065443906080036R
            '
            'XrLine_pageHeaderLine
            '
            Me.XrLine_pageHeaderLine.LineWidth = 2
            Me.XrLine_pageHeaderLine.LocationFloat = New DevExpress.Utils.PointFloat(12.5!, 108.0!)
            Me.XrLine_pageHeaderLine.Name = "XrLine_pageHeaderLine"
            Me.XrLine_pageHeaderLine.SizeF = New System.Drawing.SizeF(777.5!, 2.09!)
            Me.XrLine_pageHeaderLine.StyleName = "XrControlStyle_HeaderLine"
            '
            'XrControlStyle_Footer
            '
            Me.XrControlStyle_Footer.BorderColor = System.Drawing.Color.Teal
            Me.XrControlStyle_Footer.BorderDashStyle = DevExpress.XtraPrinting.BorderDashStyle.Solid
            Me.XrControlStyle_Footer.BorderWidth = 2
            Me.XrControlStyle_Footer.Font = New System.Drawing.Font("Calibri", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.XrControlStyle_Footer.ForeColor = System.Drawing.Color.Teal
            Me.XrControlStyle_Footer.Name = "XrControlStyle_Footer"
            Me.XrControlStyle_Footer.Padding = New DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 100.0!)
            Me.XrControlStyle_Footer.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            '
            'XrControlStyle_Report_Group
            '
            Me.XrControlStyle_Report_Group.BackColor = System.Drawing.Color.FromArgb(CType(CType(102, Byte), Integer), CType(CType(102, Byte), Integer), CType(CType(153, Byte), Integer))
            Me.XrControlStyle_Report_Group.BorderColor = System.Drawing.Color.White
            Me.XrControlStyle_Report_Group.BorderDashStyle = DevExpress.XtraPrinting.BorderDashStyle.Solid
            Me.XrControlStyle_Report_Group.Borders = DevExpress.XtraPrinting.BorderSide.Bottom
            Me.XrControlStyle_Report_Group.BorderWidth = 2
            Me.XrControlStyle_Report_Group.Font = New System.Drawing.Font("Calibri", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.XrControlStyle_Report_Group.ForeColor = System.Drawing.Color.White
            Me.XrControlStyle_Report_Group.Name = "XrControlStyle_Report_Group"
            Me.XrControlStyle_Report_Group.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            '
            'GroupHeader1
            '
            Me.GroupHeader1.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrTable2})
            Me.GroupHeader1.HeightF = 21.88!
            Me.GroupHeader1.Name = "GroupHeader1"
            '
            'XrTableCell12
            '
            Me.XrTableCell12.CanGrow = False
            Me.XrTableCell12.Name = "XrTableCell12"
            Me.XrTableCell12.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 5, 0, 0, 100.0!)
            Me.XrTableCell12.StylePriority.UsePadding = False
            Me.XrTableCell12.StylePriority.UseTextAlignment = False
            Me.XrTableCell12.Text = "VALUE"
            Me.XrTableCell12.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrTableCell12.Weight = 0.68298653630569572R
            '
            'XrControlStyle_Report_Total
            '
            Me.XrControlStyle_Report_Total.BackColor = System.Drawing.Color.FromArgb(CType(CType(222, Byte), Integer), CType(CType(222, Byte), Integer), CType(CType(233, Byte), Integer))
            Me.XrControlStyle_Report_Total.BorderColor = System.Drawing.Color.White
            Me.XrControlStyle_Report_Total.BorderDashStyle = DevExpress.XtraPrinting.BorderDashStyle.Solid
            Me.XrControlStyle_Report_Total.Borders = DevExpress.XtraPrinting.BorderSide.Bottom
            Me.XrControlStyle_Report_Total.BorderWidth = 2
            Me.XrControlStyle_Report_Total.Font = New System.Drawing.Font("Calibri", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.XrControlStyle_Report_Total.Name = "XrControlStyle_Report_Total"
            Me.XrControlStyle_Report_Total.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            '
            'XrTableCell7
            '
            Me.XrTableCell7.Name = "XrTableCell7"
            Me.XrTableCell7.Padding = New DevExpress.XtraPrinting.PaddingInfo(5, 0, 0, 0, 100.0!)
            Me.XrTableCell7.StylePriority.UsePadding = False
            Me.XrTableCell7.Text = "MINUS TOTAL LIABILITES"
            Me.XrTableCell7.Weight = 1.6434732339801978R
            '
            'XrTableCell_r_value
            '
            Me.XrTableCell_r_value.CanGrow = False
            Me.XrTableCell_r_value.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "r_value", "{0:c}")})
            Me.XrTableCell_r_value.FormattingRules.Add(Me.FormattingRule_r_HideDetails)
            Me.XrTableCell_r_value.Name = "XrTableCell_r_value"
            Me.XrTableCell_r_value.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 5, 0, 0, 100.0!)
            Me.XrTableCell_r_value.StylePriority.UsePadding = False
            Me.XrTableCell_r_value.StylePriority.UseTextAlignment = False
            Me.XrTableCell_r_value.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrTableCell_r_value.Weight = 0.68297869743184836R
            '
            'XrControlStyle_HeaderLine
            '
            Me.XrControlStyle_HeaderLine.ForeColor = System.Drawing.Color.Teal
            Me.XrControlStyle_HeaderLine.Name = "XrControlStyle_HeaderLine"
            Me.XrControlStyle_HeaderLine.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
            Me.XrControlStyle_HeaderLine.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            '
            'XrTableCell2
            '
            Me.XrTableCell2.Name = "XrTableCell2"
            Me.XrTableCell2.Padding = New DevExpress.XtraPrinting.PaddingInfo(5, 0, 0, 0, 100.0!)
            Me.XrTableCell2.StylePriority.UsePadding = False
            Me.XrTableCell2.Text = "YOUR FINANCIAL NET WORTH"
            Me.XrTableCell2.Weight = 1.6434736591914552R
            '
            'FormattingRule_r_HideDetails
            '
            Me.FormattingRule_r_HideDetails.Condition = "[r_heading] == True"
            '
            '
            '
            Me.FormattingRule_r_HideDetails.Formatting.BackColor = System.Drawing.Color.FromArgb(CType(CType(102, Byte), Integer), CType(CType(102, Byte), Integer), CType(CType(153, Byte), Integer))
            Me.FormattingRule_r_HideDetails.Formatting.BorderColor = System.Drawing.Color.White
            Me.FormattingRule_r_HideDetails.Formatting.Borders = DevExpress.XtraPrinting.BorderSide.Bottom
            Me.FormattingRule_r_HideDetails.Formatting.BorderWidth = 1
            Me.FormattingRule_r_HideDetails.Formatting.Font = New System.Drawing.Font("Calibri", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.FormattingRule_r_HideDetails.Formatting.ForeColor = System.Drawing.Color.FromArgb(CType(CType(102, Byte), Integer), CType(CType(102, Byte), Integer), CType(CType(153, Byte), Integer))
            Me.FormattingRule_r_HideDetails.Name = "FormattingRule_r_HideDetails"
            '
            'XrTableRow2
            '
            Me.XrTableRow2.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.XrTableCell4, Me.XrTableCell5, Me.XrTableCell13, Me.XrTableCell6, Me.XrTableCell12})
            Me.XrTableRow2.Name = "XrTableRow2"
            Me.XrTableRow2.StyleName = "XrControlStyle_Report_Column"
            Me.XrTableRow2.StylePriority.UseTextAlignment = False
            Me.XrTableRow2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            Me.XrTableRow2.Weight = 0.4558333158493042R
            '
            'ReportHeader
            '
            Me.ReportHeader.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel_ReportTitle, Me.XrLine_reportHeaderLine, Me.XrLabel_reportHeaderBox1, Me.XrLabel_reportHeaderBox2, Me.XrLabel_reportHeaderBox3})
            Me.ReportHeader.HeightF = 135.0!
            Me.ReportHeader.Name = "ReportHeader"
            '
            'XrLabel_pageHeaderBox1
            '
            Me.XrLabel_pageHeaderBox1.LocationFloat = New DevExpress.Utils.PointFloat(601.04!, 27.045!)
            Me.XrLabel_pageHeaderBox1.Name = "XrLabel_pageHeaderBox1"
            Me.XrLabel_pageHeaderBox1.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_pageHeaderBox1.SizeF = New System.Drawing.SizeF(56.0!, 56.0!)
            Me.XrLabel_pageHeaderBox1.StyleName = "XrControlStyle_HeaderBox1"
            '
            'XrTableCell14
            '
            Me.XrTableCell14.BackColor = System.Drawing.Color.Transparent
            Me.XrTableCell14.CanGrow = False
            Me.XrTableCell14.Name = "XrTableCell14"
            Me.XrTableCell14.StylePriority.UseBackColor = False
            Me.XrTableCell14.Weight = 0.10106831002742683R
            '
            'XrTableRow4
            '
            Me.XrTableRow4.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.XrTableCell7, Me.XrTableCell_total_r_value})
            Me.XrTableRow4.Name = "XrTableRow4"
            Me.XrTableRow4.Weight = 0.87519996643066411R
            '
            'SpanishFinancialNetWorth_3
            '
            Me.Bands.AddRange(New DevExpress.XtraReports.UI.Band() {Me.Detail, Me.TopMarginBand1, Me.BottomMarginBand1, Me.ReportHeader, Me.PageHeader, Me.GroupHeader1, Me.GroupFooter1})
            Me.Font = New System.Drawing.Font("Calibri", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.FormattingRuleSheet.AddRange(New DevExpress.XtraReports.UI.FormattingRule() {Me.FormattingRule_l_ShowHeading, Me.FormattingRule_l_HideDetails, Me.FormattingRule_r_ShowHeading, Me.FormattingRule_r_HideDetails})
            Me.Margins = New System.Drawing.Printing.Margins(25, 24, 25, 25)
            Me.ReportPrintOptions.PrintOnEmptyDataSource = False
            Me.ScriptLanguage = DevExpress.XtraReports.ScriptLanguage.VisualBasic
            Me.Scripts.OnBeforePrint = "FinancialNetWorth_BeforePrint"
            Me.ScriptsSource = resources.GetString("$this.ScriptsSource")
            Me.StyleSheet.AddRange(New DevExpress.XtraReports.UI.XRControlStyle() {Me.XrControlStyle_HeaderText, Me.XrControlStyle_HeaderLine, Me.XrControlStyle_HeaderBox1, Me.XrControlStyle_HeaderBox2, Me.XrControlStyle_HeaderBox3, Me.XrControlStyle_Report_Column, Me.XrControlStyle_Report_Group, Me.XrControlStyle_Report_Detail, Me.XrControlStyle_Report_Total, Me.XrControlStyle_Footer, Me.XrControlStyle_HeaderSubTitle})
            Me.Version = "11.2"
            CType(Me.XrTable3, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.XrTable2, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.XrTable1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me, System.ComponentModel.ISupportInitialize).EndInit()

        End Sub
        Friend WithEvents XrTableCell_l_value As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents FormattingRule_l_HideDetails As DevExpress.XtraReports.UI.FormattingRule
        Friend WithEvents XrTable3 As DevExpress.XtraReports.UI.XRTable
        Friend WithEvents XrTableRow3 As DevExpress.XtraReports.UI.XRTableRow
        Friend WithEvents XrTableCell_l_description As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents FormattingRule_l_ShowHeading As DevExpress.XtraReports.UI.FormattingRule
        Friend WithEvents XrTableCell14 As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents XrTableCell_r_description As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents FormattingRule_r_ShowHeading As DevExpress.XtraReports.UI.FormattingRule
        Friend WithEvents XrTableCell_r_value As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents FormattingRule_r_HideDetails As DevExpress.XtraReports.UI.FormattingRule
        Friend WithEvents XrControlStyle_Report_Detail As DevExpress.XtraReports.UI.XRControlStyle
        Friend WithEvents PageHeader As DevExpress.XtraReports.UI.PageHeaderBand
        Friend WithEvents XrLine_pageHeaderLine As DevExpress.XtraReports.UI.XRLine
        Friend WithEvents XrLabel_pageHeader_Title As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_pageHeaderBox1 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_pageHeaderBox2 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_pageHeaderBox3 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrControlStyle_Report_Column As DevExpress.XtraReports.UI.XRControlStyle
        Friend WithEvents XrLabel1 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrTableCell4 As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents TopMarginBand1 As DevExpress.XtraReports.UI.TopMarginBand
        Friend WithEvents XrTableCell6 As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents XrTableRow5 As DevExpress.XtraReports.UI.XRTableRow
        Friend WithEvents XrTableCell2 As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents XrTableCell_total_difference As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents XrTableCell_total_r_value As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents XrTable2 As DevExpress.XtraReports.UI.XRTable
        Friend WithEvents XrTableRow2 As DevExpress.XtraReports.UI.XRTableRow
        Friend WithEvents XrTableCell5 As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents XrTableCell13 As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents XrTableCell12 As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents XrControlStyle_HeaderText As DevExpress.XtraReports.UI.XRControlStyle
        Friend WithEvents XrControlStyle_HeaderBox1 As DevExpress.XtraReports.UI.XRControlStyle
        Friend WithEvents XrLabel_ReportTitle As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrTableCell_total_l_value As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents XrControlStyle_HeaderSubTitle As DevExpress.XtraReports.UI.XRControlStyle
        Friend WithEvents XrTable1 As DevExpress.XtraReports.UI.XRTable
        Friend WithEvents XrTableRow1 As DevExpress.XtraReports.UI.XRTableRow
        Friend WithEvents XrTableCell1 As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents XrTableRow4 As DevExpress.XtraReports.UI.XRTableRow
        Friend WithEvents XrTableCell7 As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents BottomMarginBand1 As DevExpress.XtraReports.UI.BottomMarginBand
        Friend WithEvents Detail As DevExpress.XtraReports.UI.DetailBand
        Friend WithEvents XrControlStyle_HeaderBox3 As DevExpress.XtraReports.UI.XRControlStyle
        Friend WithEvents XrLabel_reportHeaderBox1 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_reportHeaderBox3 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents GroupFooter1 As DevExpress.XtraReports.UI.GroupFooterBand
        Friend WithEvents XrLabel_reportHeaderBox2 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrControlStyle_HeaderBox2 As DevExpress.XtraReports.UI.XRControlStyle
        Friend WithEvents XrLine_reportHeaderLine As DevExpress.XtraReports.UI.XRLine
        Friend WithEvents XrControlStyle_Footer As DevExpress.XtraReports.UI.XRControlStyle
        Friend WithEvents XrControlStyle_Report_Group As DevExpress.XtraReports.UI.XRControlStyle
        Friend WithEvents GroupHeader1 As DevExpress.XtraReports.UI.GroupHeaderBand
        Friend WithEvents XrControlStyle_Report_Total As DevExpress.XtraReports.UI.XRControlStyle
        Friend WithEvents XrControlStyle_HeaderLine As DevExpress.XtraReports.UI.XRControlStyle
        Friend WithEvents ReportHeader As DevExpress.XtraReports.UI.ReportHeaderBand
    End Class
End Namespace
