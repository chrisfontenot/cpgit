﻿Namespace Documents
    <Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
    Partial Class MonthlySurplusDeficit_2
        Inherits Template_2

        'XtraReport overrides dispose to clean up the component list.
        <System.Diagnostics.DebuggerNonUserCode()> _
        Protected Overrides Sub Dispose(ByVal disposing As Boolean)
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
            MyBase.Dispose(disposing)
        End Sub

        'Required by the Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Designer
        'It can be modified using the Designer.  
        'Do not modify it using the code editor.
        <System.Diagnostics.DebuggerStepThrough()> _
        Private Sub InitializeComponent()
            Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(MonthlySurplusDeficit_2))
            Me.XrTable1 = New DevExpress.XtraReports.UI.XRTable()
            Me.XrTableRow6 = New DevExpress.XtraReports.UI.XRTableRow()
            Me.XrTableCell8 = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableCell16 = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableCell15 = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableRow1 = New DevExpress.XtraReports.UI.XRTableRow()
            Me.XrTableCell1 = New DevExpress.XtraReports.UI.XRTableCell()
            Me.lbl_self_person_1_net = New DevExpress.XtraReports.UI.XRTableCell()
            Me.lbl_plan_person_1_net = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableRow2 = New DevExpress.XtraReports.UI.XRTableRow()
            Me.XrTableCell4 = New DevExpress.XtraReports.UI.XRTableCell()
            Me.lbl_self_person_2_net = New DevExpress.XtraReports.UI.XRTableCell()
            Me.lbl_plan_person_2_net = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableRow5 = New DevExpress.XtraReports.UI.XRTableRow()
            Me.XrTableCell18 = New DevExpress.XtraReports.UI.XRTableCell()
            Me.lbl_self_asset = New DevExpress.XtraReports.UI.XRTableCell()
            Me.lbl_plan_asset = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableRow4 = New DevExpress.XtraReports.UI.XRTableRow()
            Me.XrTableCell14 = New DevExpress.XtraReports.UI.XRTableCell()
            Me.lbl_self_total_assets = New DevExpress.XtraReports.UI.XRTableCell()
            Me.lbl_plan_total_assets = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableRow7 = New DevExpress.XtraReports.UI.XRTableRow()
            Me.XrTableCell26 = New DevExpress.XtraReports.UI.XRTableCell()
            Me.lbl_self_expense = New DevExpress.XtraReports.UI.XRTableCell()
            Me.lbl_plan_expense = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableRow8 = New DevExpress.XtraReports.UI.XRTableRow()
            Me.XrTableCell30 = New DevExpress.XtraReports.UI.XRTableCell()
            Me.lbl_self_other_debt = New DevExpress.XtraReports.UI.XRTableCell()
            Me.lbl_plan_other_debt = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableRow9 = New DevExpress.XtraReports.UI.XRTableRow()
            Me.XrTableCell34 = New DevExpress.XtraReports.UI.XRTableCell()
            Me.lbl_self_expense_total = New DevExpress.XtraReports.UI.XRTableCell()
            Me.lbl_plan_expense_total = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableRow11 = New DevExpress.XtraReports.UI.XRTableRow()
            Me.XrTableCell42 = New DevExpress.XtraReports.UI.XRTableCell()
            Me.lbl_self_debt_payment = New DevExpress.XtraReports.UI.XRTableCell()
            Me.lbl_plan_debt_payment = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableRow10 = New DevExpress.XtraReports.UI.XRTableRow()
            Me.XrTableCell38 = New DevExpress.XtraReports.UI.XRTableCell()
            Me.lbl_self_total = New DevExpress.XtraReports.UI.XRTableCell()
            Me.lbl_plan_total = New DevExpress.XtraReports.UI.XRTableCell()
            CType(Me.XrTable1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
            '
            'XrLabel_ReportTitle
            '
            Me.XrLabel_ReportTitle.Text = "MONTHLY SURPLUS/DEFICIT"
            '
            'XrLabel_pageHeader_Title
            '
            Me.XrLabel_pageHeader_Title.Text = "MONTHLY SURPLUS/DEFICIT (Continued)"
            '
            'Detail
            '
            Me.Detail.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrTable1})
            Me.Detail.HeightF = 218.8!
            '
            'XrTable1
            '
            Me.XrTable1.LocationFloat = New DevExpress.Utils.PointFloat(12.5!, 0.0!)
            Me.XrTable1.Name = "XrTable1"
            Me.XrTable1.Padding = New DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 100.0!)
            Me.XrTable1.Rows.AddRange(New DevExpress.XtraReports.UI.XRTableRow() {Me.XrTableRow6, Me.XrTableRow1, Me.XrTableRow2, Me.XrTableRow5, Me.XrTableRow4, Me.XrTableRow7, Me.XrTableRow8, Me.XrTableRow9, Me.XrTableRow11, Me.XrTableRow10})
            Me.XrTable1.SizeF = New System.Drawing.SizeF(690.2917!, 218.8!)
            Me.XrTable1.StyleName = "XrControlStyle_Report_Detail"
            Me.XrTable1.StylePriority.UsePadding = False
            '
            'XrTableRow6
            '
            Me.XrTableRow6.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.XrTableCell8, Me.XrTableCell16, Me.XrTableCell15})
            Me.XrTableRow6.Font = New System.Drawing.Font("Calibri", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.XrTableRow6.Name = "XrTableRow6"
            Me.XrTableRow6.StyleName = "XrControlStyle_Report_Column"
            Me.XrTableRow6.Weight = 0.035545408201116048R
            '
            'XrTableCell8
            '
            Me.XrTableCell8.Name = "XrTableCell8"
            Me.XrTableCell8.Text = "CATEGORY"
            Me.XrTableCell8.Weight = 0.41606218586526644R
            '
            'XrTableCell16
            '
            Me.XrTableCell16.Name = "XrTableCell16"
            Me.XrTableCell16.StylePriority.UseTextAlignment = False
            Me.XrTableCell16.Text = "SELF"
            Me.XrTableCell16.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrTableCell16.Weight = 0.21183687997209924R
            '
            'XrTableCell15
            '
            Me.XrTableCell15.Name = "XrTableCell15"
            Me.XrTableCell15.StylePriority.UseTextAlignment = False
            Me.XrTableCell15.Text = "SUGGESTED"
            Me.XrTableCell15.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrTableCell15.Weight = 0.19446339939008142R
            '
            'XrTableRow1
            '
            Me.XrTableRow1.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.XrTableCell1, Me.lbl_self_person_1_net, Me.lbl_plan_person_1_net})
            Me.XrTableRow1.Name = "XrTableRow1"
            Me.XrTableRow1.Weight = 0.035545407788400409R
            '
            'XrTableCell1
            '
            Me.XrTableCell1.CanGrow = False
            Me.XrTableCell1.Name = "XrTableCell1"
            Me.XrTableCell1.Padding = New DevExpress.XtraPrinting.PaddingInfo(15, 5, 0, 0, 100.0!)
            Me.XrTableCell1.StylePriority.UsePadding = False
            Me.XrTableCell1.Text = "Applicant Employment Monthly Income"
            Me.XrTableCell1.Weight = 0.41606218586526644R
            Me.XrTableCell1.WordWrap = False
            '
            'lbl_self_person_1_net
            '
            Me.lbl_self_person_1_net.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "self.person_1_net", "{0:c}")})
            Me.lbl_self_person_1_net.Name = "lbl_self_person_1_net"
            Me.lbl_self_person_1_net.StylePriority.UseTextAlignment = False
            Me.lbl_self_person_1_net.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
            Me.lbl_self_person_1_net.Weight = 0.21183687997209924R
            '
            'lbl_plan_person_1_net
            '
            Me.lbl_plan_person_1_net.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "plan.person_1_net", "{0:c}")})
            Me.lbl_plan_person_1_net.Name = "lbl_plan_person_1_net"
            Me.lbl_plan_person_1_net.StylePriority.UseTextAlignment = False
            Me.lbl_plan_person_1_net.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
            Me.lbl_plan_person_1_net.Weight = 0.19446339939008142R
            '
            'XrTableRow2
            '
            Me.XrTableRow2.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.XrTableCell4, Me.lbl_self_person_2_net, Me.lbl_plan_person_2_net})
            Me.XrTableRow2.Name = "XrTableRow2"
            Me.XrTableRow2.Weight = 0.035545407728011749R
            '
            'XrTableCell4
            '
            Me.XrTableCell4.CanGrow = False
            Me.XrTableCell4.Name = "XrTableCell4"
            Me.XrTableCell4.Padding = New DevExpress.XtraPrinting.PaddingInfo(15, 5, 0, 0, 100.0!)
            Me.XrTableCell4.StylePriority.UsePadding = False
            Me.XrTableCell4.Text = "Co-Applicant Employment Monthly Income"
            Me.XrTableCell4.Weight = 0.41606218586526644R
            Me.XrTableCell4.WordWrap = False
            '
            'lbl_self_person_2_net
            '
            Me.lbl_self_person_2_net.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "self.person_2_net", "{0:c}")})
            Me.lbl_self_person_2_net.Name = "lbl_self_person_2_net"
            Me.lbl_self_person_2_net.StylePriority.UseTextAlignment = False
            Me.lbl_self_person_2_net.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
            Me.lbl_self_person_2_net.Weight = 0.21183687997209924R
            '
            'lbl_plan_person_2_net
            '
            Me.lbl_plan_person_2_net.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "plan.person_2_net", "{0:c}")})
            Me.lbl_plan_person_2_net.Name = "lbl_plan_person_2_net"
            Me.lbl_plan_person_2_net.StylePriority.UseTextAlignment = False
            Me.lbl_plan_person_2_net.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.lbl_plan_person_2_net.Weight = 0.19446339939008142R
            '
            'XrTableRow5
            '
            Me.XrTableRow5.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.XrTableCell18, Me.lbl_self_asset, Me.lbl_plan_asset})
            Me.XrTableRow5.Name = "XrTableRow5"
            Me.XrTableRow5.Weight = 0.035545407728011763R
            '
            'XrTableCell18
            '
            Me.XrTableCell18.CanGrow = False
            Me.XrTableCell18.Name = "XrTableCell18"
            Me.XrTableCell18.Padding = New DevExpress.XtraPrinting.PaddingInfo(15, 5, 0, 0, 100.0!)
            Me.XrTableCell18.StylePriority.UsePadding = False
            Me.XrTableCell18.Text = "Other Monthly Income"
            Me.XrTableCell18.Weight = 0.41606218586526644R
            Me.XrTableCell18.WordWrap = False
            '
            'lbl_self_asset
            '
            Me.lbl_self_asset.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "self.asset", "{0:c}")})
            Me.lbl_self_asset.Name = "lbl_self_asset"
            Me.lbl_self_asset.StylePriority.UseTextAlignment = False
            Me.lbl_self_asset.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
            Me.lbl_self_asset.Weight = 0.21183687997209924R
            '
            'lbl_plan_asset
            '
            Me.lbl_plan_asset.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "plan.asset", "{0:c}")})
            Me.lbl_plan_asset.Name = "lbl_plan_asset"
            Me.lbl_plan_asset.StylePriority.UseTextAlignment = False
            Me.lbl_plan_asset.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
            Me.lbl_plan_asset.Weight = 0.19446339939008142R
            '
            'XrTableRow4
            '
            Me.XrTableRow4.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.XrTableCell14, Me.lbl_self_total_assets, Me.lbl_plan_total_assets})
            Me.XrTableRow4.Name = "XrTableRow4"
            Me.XrTableRow4.Weight = 0.035545407728011749R
            '
            'XrTableCell14
            '
            Me.XrTableCell14.CanGrow = False
            Me.XrTableCell14.Name = "XrTableCell14"
            Me.XrTableCell14.Padding = New DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 100.0!)
            Me.XrTableCell14.StylePriority.UsePadding = False
            Me.XrTableCell14.Text = "Total Monthly Income"
            Me.XrTableCell14.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            Me.XrTableCell14.Weight = 0.41606218586526644R
            Me.XrTableCell14.WordWrap = False
            '
            'lbl_self_total_assets
            '
            Me.lbl_self_total_assets.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "self.total_assets", "{0:c}")})
            Me.lbl_self_total_assets.Name = "lbl_self_total_assets"
            Me.lbl_self_total_assets.StylePriority.UseTextAlignment = False
            Me.lbl_self_total_assets.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
            Me.lbl_self_total_assets.Weight = 0.21183687997209924R
            '
            'lbl_plan_total_assets
            '
            Me.lbl_plan_total_assets.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "plan.total_assets", "{0:c}")})
            Me.lbl_plan_total_assets.Name = "lbl_plan_total_assets"
            Me.lbl_plan_total_assets.StylePriority.UseTextAlignment = False
            Me.lbl_plan_total_assets.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
            Me.lbl_plan_total_assets.Weight = 0.19446339939008142R
            '
            'XrTableRow7
            '
            Me.XrTableRow7.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.XrTableCell26, Me.lbl_self_expense, Me.lbl_plan_expense})
            Me.XrTableRow7.Name = "XrTableRow7"
            Me.XrTableRow7.Weight = 0.035545407728011728R
            '
            'XrTableCell26
            '
            Me.XrTableCell26.CanGrow = False
            Me.XrTableCell26.Name = "XrTableCell26"
            Me.XrTableCell26.Padding = New DevExpress.XtraPrinting.PaddingInfo(15, 5, 0, 0, 100.0!)
            Me.XrTableCell26.StylePriority.UsePadding = False
            Me.XrTableCell26.Text = "Total Budgeted Expenses"
            Me.XrTableCell26.Weight = 0.41606218586526644R
            Me.XrTableCell26.WordWrap = False
            '
            'lbl_self_expense
            '
            Me.lbl_self_expense.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "self.expense", "{0:c}")})
            Me.lbl_self_expense.Name = "lbl_self_expense"
            Me.lbl_self_expense.StylePriority.UseTextAlignment = False
            Me.lbl_self_expense.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
            Me.lbl_self_expense.Weight = 0.21183687997209924R
            '
            'lbl_plan_expense
            '
            Me.lbl_plan_expense.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "plan.expense", "{0:c}")})
            Me.lbl_plan_expense.Name = "lbl_plan_expense"
            Me.lbl_plan_expense.StylePriority.UseTextAlignment = False
            Me.lbl_plan_expense.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
            Me.lbl_plan_expense.Weight = 0.19446339939008142R
            '
            'XrTableRow8
            '
            Me.XrTableRow8.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.XrTableCell30, Me.lbl_self_other_debt, Me.lbl_plan_other_debt})
            Me.XrTableRow8.Name = "XrTableRow8"
            Me.XrTableRow8.Weight = 0.03554540511353179R
            '
            'XrTableCell30
            '
            Me.XrTableCell30.CanGrow = False
            Me.XrTableCell30.Name = "XrTableCell30"
            Me.XrTableCell30.Padding = New DevExpress.XtraPrinting.PaddingInfo(15, 5, 0, 0, 100.0!)
            Me.XrTableCell30.StylePriority.UsePadding = False
            Me.XrTableCell30.Text = "Other Debt"
            Me.XrTableCell30.Weight = 0.41606218586526644R
            Me.XrTableCell30.WordWrap = False
            '
            'lbl_self_other_debt
            '
            Me.lbl_self_other_debt.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "self.other_debt", "{0:c}")})
            Me.lbl_self_other_debt.Name = "lbl_self_other_debt"
            Me.lbl_self_other_debt.StylePriority.UseTextAlignment = False
            Me.lbl_self_other_debt.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
            Me.lbl_self_other_debt.Weight = 0.21183687997209924R
            '
            'lbl_plan_other_debt
            '
            Me.lbl_plan_other_debt.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "plan.other_debt", "{0:c}")})
            Me.lbl_plan_other_debt.Name = "lbl_plan_other_debt"
            Me.lbl_plan_other_debt.StylePriority.UseTextAlignment = False
            Me.lbl_plan_other_debt.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
            Me.lbl_plan_other_debt.Weight = 0.19446339939008142R
            '
            'XrTableRow9
            '
            Me.XrTableRow9.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.XrTableCell34, Me.lbl_self_expense_total, Me.lbl_plan_expense_total})
            Me.XrTableRow9.Name = "XrTableRow9"
            Me.XrTableRow9.Weight = 0.035545401660893594R
            '
            'XrTableCell34
            '
            Me.XrTableCell34.CanGrow = False
            Me.XrTableCell34.Name = "XrTableCell34"
            Me.XrTableCell34.Text = "Subtotal Surplus/Deficit"
            Me.XrTableCell34.Weight = 0.41606218586526644R
            Me.XrTableCell34.WordWrap = False
            '
            'lbl_self_expense_total
            '
            Me.lbl_self_expense_total.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "self.expense_total", "{0:c}")})
            Me.lbl_self_expense_total.Name = "lbl_self_expense_total"
            Me.lbl_self_expense_total.StylePriority.UseTextAlignment = False
            Me.lbl_self_expense_total.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
            Me.lbl_self_expense_total.Weight = 0.21183687997209924R
            '
            'lbl_plan_expense_total
            '
            Me.lbl_plan_expense_total.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "plan.expense_total", "{0:c}")})
            Me.lbl_plan_expense_total.Name = "lbl_plan_expense_total"
            Me.lbl_plan_expense_total.StylePriority.UseTextAlignment = False
            Me.lbl_plan_expense_total.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
            Me.lbl_plan_expense_total.Weight = 0.19446339939008142R
            '
            'XrTableRow11
            '
            Me.XrTableRow11.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.XrTableCell42, Me.lbl_self_debt_payment, Me.lbl_plan_debt_payment})
            Me.XrTableRow11.Name = "XrTableRow11"
            Me.XrTableRow11.Weight = 0.035545402014926529R
            '
            'XrTableCell42
            '
            Me.XrTableCell42.CanGrow = False
            Me.XrTableCell42.Name = "XrTableCell42"
            Me.XrTableCell42.Padding = New DevExpress.XtraPrinting.PaddingInfo(15, 5, 0, 0, 100.0!)
            Me.XrTableCell42.StylePriority.UsePadding = False
            Me.XrTableCell42.Text = "Debt Payments"
            Me.XrTableCell42.Weight = 0.41606218586526644R
            Me.XrTableCell42.WordWrap = False
            '
            'lbl_self_debt_payment
            '
            Me.lbl_self_debt_payment.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "self.debt_payment", "{0:c}")})
            Me.lbl_self_debt_payment.Name = "lbl_self_debt_payment"
            Me.lbl_self_debt_payment.StylePriority.UseTextAlignment = False
            Me.lbl_self_debt_payment.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
            Me.lbl_self_debt_payment.Weight = 0.21183687997209924R
            '
            'lbl_plan_debt_payment
            '
            Me.lbl_plan_debt_payment.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "plan.debt_payment", "{0:c}")})
            Me.lbl_plan_debt_payment.Name = "lbl_plan_debt_payment"
            Me.lbl_plan_debt_payment.StylePriority.UseTextAlignment = False
            Me.lbl_plan_debt_payment.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
            Me.lbl_plan_debt_payment.Weight = 0.19446339939008142R
            '
            'XrTableRow10
            '
            Me.XrTableRow10.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.XrTableCell38, Me.lbl_self_total, Me.lbl_plan_total})
            Me.XrTableRow10.Name = "XrTableRow10"
            Me.XrTableRow10.StyleName = "XrControlStyle_Report_Total"
            Me.XrTableRow10.Weight = 0.035545431734342714R
            '
            'XrTableCell38
            '
            Me.XrTableCell38.CanGrow = False
            Me.XrTableCell38.Name = "XrTableCell38"
            Me.XrTableCell38.Text = "TOTAL SURPLUS/DEFICIT"
            Me.XrTableCell38.Weight = 0.41606218586526644R
            Me.XrTableCell38.WordWrap = False
            '
            'lbl_self_total
            '
            Me.lbl_self_total.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "self.total", "{0:c}")})
            Me.lbl_self_total.Name = "lbl_self_total"
            Me.lbl_self_total.StylePriority.UseTextAlignment = False
            Me.lbl_self_total.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
            Me.lbl_self_total.Weight = 0.21183687997209924R
            '
            'lbl_plan_total
            '
            Me.lbl_plan_total.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "plan.total", "{0:c}")})
            Me.lbl_plan_total.Name = "lbl_plan_total"
            Me.lbl_plan_total.StylePriority.UseTextAlignment = False
            Me.lbl_plan_total.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
            Me.lbl_plan_total.Weight = 0.19446339939008142R
            '
            'MonthlySurplusDeficit_2
            '
            Me.Bands.AddRange(New DevExpress.XtraReports.UI.Band() {Me.TopMarginBand1, Me.Detail, Me.BottomMarginBand1, Me.ReportHeader, Me.PageHeader})
            Me.ScriptLanguage = DevExpress.XtraReports.ScriptLanguage.VisualBasic
            Me.Scripts.OnBeforePrint = "MonthlySurplusDeficit_BeforePrint"
            Me.ScriptsSource = resources.GetString("$this.ScriptsSource")
            Me.StyleSheet.AddRange(New DevExpress.XtraReports.UI.XRControlStyle() {Me.XrControlStyle_HeaderText, Me.XrControlStyle_HeaderLine, Me.XrControlStyle_HeaderBox1, Me.XrControlStyle_HeaderBox2, Me.XrControlStyle_HeaderBox3, Me.XrControlStyle_Report_Column, Me.XrControlStyle_Report_Group, Me.XrControlStyle_Report_Detail, Me.XrControlStyle_Report_Total, Me.XrControlStyle_Footer, Me.XrControlStyle_HeaderSubTitle})
            Me.Version = "11.2"
            CType(Me.XrTable1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me, System.ComponentModel.ISupportInitialize).EndInit()

        End Sub

        Private XrTable1 As DevExpress.XtraReports.UI.XRTable
        Private XrTableRow6 As DevExpress.XtraReports.UI.XRTableRow
        Private XrTableCell8 As DevExpress.XtraReports.UI.XRTableCell
        Private XrTableCell16 As DevExpress.XtraReports.UI.XRTableCell
        Private XrTableCell15 As DevExpress.XtraReports.UI.XRTableCell
        Private XrTableRow1 As DevExpress.XtraReports.UI.XRTableRow
        Private XrTableCell1 As DevExpress.XtraReports.UI.XRTableCell
        Private lbl_self_person_1_net As DevExpress.XtraReports.UI.XRTableCell
        Private lbl_plan_person_1_net As DevExpress.XtraReports.UI.XRTableCell
        Private XrTableRow2 As DevExpress.XtraReports.UI.XRTableRow
        Private XrTableCell4 As DevExpress.XtraReports.UI.XRTableCell
        Private lbl_self_person_2_net As DevExpress.XtraReports.UI.XRTableCell
        Private lbl_plan_person_2_net As DevExpress.XtraReports.UI.XRTableCell
        Private XrTableRow5 As DevExpress.XtraReports.UI.XRTableRow
        Private XrTableCell18 As DevExpress.XtraReports.UI.XRTableCell
        Private lbl_self_asset As DevExpress.XtraReports.UI.XRTableCell
        Private lbl_plan_asset As DevExpress.XtraReports.UI.XRTableCell
        Private XrTableRow4 As DevExpress.XtraReports.UI.XRTableRow
        Private XrTableCell14 As DevExpress.XtraReports.UI.XRTableCell
        Private lbl_self_total_assets As DevExpress.XtraReports.UI.XRTableCell
        Private lbl_plan_total_assets As DevExpress.XtraReports.UI.XRTableCell
        Private XrTableRow7 As DevExpress.XtraReports.UI.XRTableRow
        Private XrTableCell26 As DevExpress.XtraReports.UI.XRTableCell
        Private lbl_self_expense As DevExpress.XtraReports.UI.XRTableCell
        Private lbl_plan_expense As DevExpress.XtraReports.UI.XRTableCell
        Private XrTableRow8 As DevExpress.XtraReports.UI.XRTableRow
        Private XrTableCell30 As DevExpress.XtraReports.UI.XRTableCell
        Private lbl_self_other_debt As DevExpress.XtraReports.UI.XRTableCell
        Private lbl_plan_other_debt As DevExpress.XtraReports.UI.XRTableCell
        Private XrTableCell34 As DevExpress.XtraReports.UI.XRTableCell
        Private lbl_self_expense_total As DevExpress.XtraReports.UI.XRTableCell
        Private lbl_plan_expense_total As DevExpress.XtraReports.UI.XRTableCell
        Private XrTableRow11 As DevExpress.XtraReports.UI.XRTableRow
        Private XrTableCell42 As DevExpress.XtraReports.UI.XRTableCell
        Private lbl_self_debt_payment As DevExpress.XtraReports.UI.XRTableCell
        Private lbl_plan_debt_payment As DevExpress.XtraReports.UI.XRTableCell
        Private XrTableRow10 As DevExpress.XtraReports.UI.XRTableRow
        Private XrTableCell38 As DevExpress.XtraReports.UI.XRTableCell
        Private lbl_self_total As DevExpress.XtraReports.UI.XRTableCell
        Private lbl_plan_total As DevExpress.XtraReports.UI.XRTableCell
        Private XrTableRow9 As DevExpress.XtraReports.UI.XRTableRow
    End Class
End Namespace
