﻿Namespace Documents
    <Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
    Partial Class EnglishFHLBInvoice_1
        Inherits Template_1

        'XtraReport overrides dispose to clean up the component list.
        <System.Diagnostics.DebuggerNonUserCode()> _
        Protected Overrides Sub Dispose(ByVal disposing As Boolean)
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
            MyBase.Dispose(disposing)
        End Sub

        'Required by the Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Designer
        'It can be modified using the Designer.  
        'Do not modify it using the code editor.
        <System.Diagnostics.DebuggerStepThrough()> _
        Private Sub InitializeComponent()
            Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(EnglishFHLBInvoice_1))
            Dim XrSummary1 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary()
            Dim XrSummary2 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary()
            Me.ParameterClient = New DevExpress.XtraReports.Parameters.Parameter()
            Me.ParameterCreditor = New DevExpress.XtraReports.Parameters.Parameter()
            Me.XrLabel_Header_Invoice = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrTable1 = New DevExpress.XtraReports.UI.XRTable()
            Me.XrTableRow1 = New DevExpress.XtraReports.UI.XRTableRow()
            Me.XrTableCell2 = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableCell_Date = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableRow2 = New DevExpress.XtraReports.UI.XRTableRow()
            Me.XrTableCell1 = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableCell_Client = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTable2 = New DevExpress.XtraReports.UI.XRTable()
            Me.XrTableRow3 = New DevExpress.XtraReports.UI.XRTableRow()
            Me.XrTableCell3 = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableCell_client_name = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableRow4 = New DevExpress.XtraReports.UI.XRTableRow()
            Me.XrTableCell7 = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableCell_client_address = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableRow5 = New DevExpress.XtraReports.UI.XRTableRow()
            Me.XrTableCell8 = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableCell_appointment_date = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrPictureBox1 = New DevExpress.XtraReports.UI.XRPictureBox()
            Me.GroupHeader2 = New DevExpress.XtraReports.UI.GroupHeaderBand()
            Me.XrTable3 = New DevExpress.XtraReports.UI.XRTable()
            Me.XrTableRow6 = New DevExpress.XtraReports.UI.XRTableRow()
            Me.XrTableCell4 = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableCell5 = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableCell6 = New DevExpress.XtraReports.UI.XRTableCell()
            Me.GroupFooter1 = New DevExpress.XtraReports.UI.GroupFooterBand()
            Me.XrTable5 = New DevExpress.XtraReports.UI.XRTable()
            Me.XrTableRow10 = New DevExpress.XtraReports.UI.XRTableRow()
            Me.XrTableCell18 = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableCell19 = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableCell20 = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableRow8 = New DevExpress.XtraReports.UI.XRTableRow()
            Me.XrTableCell12 = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableCell13 = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrLabel_Subtotal = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableRow9 = New DevExpress.XtraReports.UI.XRTableRow()
            Me.XrTableCell15 = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableCell16 = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrLabel_AmountPaid = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableRow11 = New DevExpress.XtraReports.UI.XRTableRow()
            Me.XrTableCell21 = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableCell22 = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrLabel_AmountDue = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTable8 = New DevExpress.XtraReports.UI.XRTable()
            Me.XrTableRow19 = New DevExpress.XtraReports.UI.XRTableRow()
            Me.XrTableCell29 = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableCell_Payment_1 = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableRow20 = New DevExpress.XtraReports.UI.XRTableRow()
            Me.XrTableCell30 = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableCell_Payment_2 = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableRow21 = New DevExpress.XtraReports.UI.XRTableRow()
            Me.XrTableCell31 = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableCell_Payment_3 = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableRow22 = New DevExpress.XtraReports.UI.XRTableRow()
            Me.XrTableCell32 = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableCell_Payment_4 = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableRow23 = New DevExpress.XtraReports.UI.XRTableRow()
            Me.XrTableCell33 = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableCell_Payment_5 = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableRow24 = New DevExpress.XtraReports.UI.XRTableRow()
            Me.XrTableCell35 = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableCell_Payment_6 = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrLabel18 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrTable6 = New DevExpress.XtraReports.UI.XRTable()
            Me.XrTableRow12 = New DevExpress.XtraReports.UI.XRTableRow()
            Me.XrTableCell24 = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableCell_Footer_Client = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableRow13 = New DevExpress.XtraReports.UI.XRTableRow()
            Me.XrTableCell25 = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableCell_Footer_Name = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableRow14 = New DevExpress.XtraReports.UI.XRTableRow()
            Me.XrTableCell26 = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableCell_Footer_Address = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrLabel8 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrTable4 = New DevExpress.XtraReports.UI.XRTable()
            Me.XrTableRow7 = New DevExpress.XtraReports.UI.XRTableRow()
            Me.XrLabel_Description = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrLabel_UnitPrice = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrLabel_LineTotal = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTable7 = New DevExpress.XtraReports.UI.XRTable()
            Me.XrTableRow15 = New DevExpress.XtraReports.UI.XRTableRow()
            Me.XrTableCell_AgencyAddress_1 = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableRow16 = New DevExpress.XtraReports.UI.XRTableRow()
            Me.XrTableCell_AgencyAddress_2 = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableRow17 = New DevExpress.XtraReports.UI.XRTableRow()
            Me.XrTableCell_AgencyAddress_3 = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableRow18 = New DevExpress.XtraReports.UI.XRTableRow()
            Me.XrTableCell_AgencyAddress_4 = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrLabel9 = New DevExpress.XtraReports.UI.XRLabel()
            Me.GroupHeader1 = New DevExpress.XtraReports.UI.GroupHeaderBand()
            Me.GroupFooter2 = New DevExpress.XtraReports.UI.GroupFooterBand()
            Me.XrControlStyle_LightGray = New DevExpress.XtraReports.UI.XRControlStyle()
            CType(Me.XrTable1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.XrTable2, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.XrTable3, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.XrTable5, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.XrTable8, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.XrTable6, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.XrTable4, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.XrTable7, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
            '
            'ReportHeader
            '
            Me.ReportHeader.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel9})
            Me.ReportHeader.HeightF = 1050.0!
            Me.ReportHeader.PageBreak = DevExpress.XtraReports.UI.PageBreak.AfterBand
            '
            'Detail
            '
            Me.Detail.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrTable4})
            Me.Detail.HeightF = 25.0!
            Me.Detail.StylePriority.UseFont = False
            '
            'ParameterClient
            '
            Me.ParameterClient.Description = "Client ID"
            Me.ParameterClient.Name = "ParameterClient"
            Me.ParameterClient.Type = GetType(Integer)
            Me.ParameterClient.Value = -1
            Me.ParameterClient.Visible = False
            '
            'ParameterCreditor
            '
            Me.ParameterCreditor.Description = "Creditor ID"
            Me.ParameterCreditor.Name = "ParameterCreditor"
            Me.ParameterCreditor.Value = "V71585"
            Me.ParameterCreditor.Visible = False
            '
            'XrLabel_Header_Invoice
            '
            Me.XrLabel_Header_Invoice.Font = New System.Drawing.Font("Calibri", 38.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.XrLabel_Header_Invoice.ForeColor = System.Drawing.Color.Teal
            Me.XrLabel_Header_Invoice.LocationFloat = New DevExpress.Utils.PointFloat(579.17!, 10.0!)
            Me.XrLabel_Header_Invoice.Name = "XrLabel_Header_Invoice"
            Me.XrLabel_Header_Invoice.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_Header_Invoice.SizeF = New System.Drawing.SizeF(190.6251!, 48.95832!)
            Me.XrLabel_Header_Invoice.StylePriority.UseFont = False
            Me.XrLabel_Header_Invoice.StylePriority.UseForeColor = False
            Me.XrLabel_Header_Invoice.StylePriority.UseTextAlignment = False
            Me.XrLabel_Header_Invoice.Text = "Invoice"
            Me.XrLabel_Header_Invoice.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrTable1
            '
            Me.XrTable1.BorderWidth = 0
            Me.XrTable1.LocationFloat = New DevExpress.Utils.PointFloat(579.17!, 77.08664!)
            Me.XrTable1.Name = "XrTable1"
            Me.XrTable1.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrTable1.Rows.AddRange(New DevExpress.XtraReports.UI.XRTableRow() {Me.XrTableRow1, Me.XrTableRow2})
            Me.XrTable1.SizeF = New System.Drawing.SizeF(190.6216!, 49.99999!)
            Me.XrTable1.StylePriority.UseBorderWidth = False
            Me.XrTable1.StylePriority.UsePadding = False
            Me.XrTable1.StylePriority.UseTextAlignment = False
            Me.XrTable1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            '
            'XrTableRow1
            '
            Me.XrTableRow1.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.XrTableCell2, Me.XrTableCell_Date})
            Me.XrTableRow1.Name = "XrTableRow1"
            Me.XrTableRow1.Weight = 1.0R
            '
            'XrTableCell2
            '
            Me.XrTableCell2.Name = "XrTableCell2"
            Me.XrTableCell2.Text = "Date"
            Me.XrTableCell2.Weight = 0.70528524045235963R
            '
            'XrTableCell_Date
            '
            Me.XrTableCell_Date.Name = "XrTableCell_Date"
            Me.XrTableCell_Date.Padding = New DevExpress.XtraPrinting.PaddingInfo(20, 2, 0, 0, 100.0!)
            Me.XrTableCell_Date.StylePriority.UsePadding = False
            Me.XrTableCell_Date.StylePriority.UseTextAlignment = False
            Me.XrTableCell_Date.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
            Me.XrTableCell_Date.Weight = 2.2947147595476403R
            '
            'XrTableRow2
            '
            Me.XrTableRow2.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.XrTableCell1, Me.XrTableCell_Client})
            Me.XrTableRow2.Name = "XrTableRow2"
            Me.XrTableRow2.Weight = 1.0R
            '
            'XrTableCell1
            '
            Me.XrTableCell1.Name = "XrTableCell1"
            Me.XrTableCell1.Text = "Inv #"
            Me.XrTableCell1.Weight = 0.705286201022554R
            '
            'XrTableCell_Client
            '
            Me.XrTableCell_Client.Name = "XrTableCell_Client"
            Me.XrTableCell_Client.Padding = New DevExpress.XtraPrinting.PaddingInfo(20, 2, 0, 0, 100.0!)
            Me.XrTableCell_Client.StylePriority.UsePadding = False
            Me.XrTableCell_Client.StylePriority.UseTextAlignment = False
            Me.XrTableCell_Client.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
            Me.XrTableCell_Client.Weight = 2.2947137989774458R
            '
            'XrTable2
            '
            Me.XrTable2.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 154.17!)
            Me.XrTable2.Name = "XrTable2"
            Me.XrTable2.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrTable2.Rows.AddRange(New DevExpress.XtraReports.UI.XRTableRow() {Me.XrTableRow3, Me.XrTableRow4, Me.XrTableRow5})
            Me.XrTable2.SizeF = New System.Drawing.SizeF(769.7918!, 75.0!)
            Me.XrTable2.StylePriority.UsePadding = False
            '
            'XrTableRow3
            '
            Me.XrTableRow3.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.XrTableCell3, Me.XrTableCell_client_name})
            Me.XrTableRow3.Name = "XrTableRow3"
            Me.XrTableRow3.Weight = 1.0R
            '
            'XrTableCell3
            '
            Me.XrTableCell3.Name = "XrTableCell3"
            Me.XrTableCell3.Padding = New DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 100.0!)
            Me.XrTableCell3.StylePriority.UsePadding = False
            Me.XrTableCell3.StylePriority.UseTextAlignment = False
            Me.XrTableCell3.Text = "RE:"
            Me.XrTableCell3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft
            Me.XrTableCell3.Weight = 0.13898078028655228R
            '
            'XrTableCell_client_name
            '
            Me.XrTableCell_client_name.Name = "XrTableCell_client_name"
            Me.XrTableCell_client_name.StylePriority.UseTextAlignment = False
            Me.XrTableCell_client_name.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft
            Me.XrTableCell_client_name.Weight = 2.8610192197134476R
            '
            'XrTableRow4
            '
            Me.XrTableRow4.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.XrTableCell7, Me.XrTableCell_client_address})
            Me.XrTableRow4.Name = "XrTableRow4"
            Me.XrTableRow4.Weight = 1.0R
            '
            'XrTableCell7
            '
            Me.XrTableCell7.Name = "XrTableCell7"
            Me.XrTableCell7.Weight = 0.13898081001950505R
            '
            'XrTableCell_client_address
            '
            Me.XrTableCell_client_address.Multiline = True
            Me.XrTableCell_client_address.Name = "XrTableCell_client_address"
            Me.XrTableCell_client_address.Weight = 2.8610191899804946R
            '
            'XrTableRow5
            '
            Me.XrTableRow5.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.XrTableCell8, Me.XrTableCell_appointment_date})
            Me.XrTableRow5.Name = "XrTableRow5"
            Me.XrTableRow5.Weight = 1.0R
            '
            'XrTableCell8
            '
            Me.XrTableCell8.Name = "XrTableCell8"
            Me.XrTableCell8.Padding = New DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 100.0!)
            Me.XrTableCell8.StylePriority.UsePadding = False
            Me.XrTableCell8.StylePriority.UseTextAlignment = False
            Me.XrTableCell8.Text = "Date of Service:"
            Me.XrTableCell8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            Me.XrTableCell8.Weight = 0.46811589274697529R
            '
            'XrTableCell_appointment_date
            '
            Me.XrTableCell_appointment_date.Name = "XrTableCell_appointment_date"
            Me.XrTableCell_appointment_date.StylePriority.UseTextAlignment = False
            Me.XrTableCell_appointment_date.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            Me.XrTableCell_appointment_date.Weight = 2.5318841072530249R
            '
            'XrPictureBox1
            '
            Me.XrPictureBox1.Image = CType(resources.GetObject("XrPictureBox1.Image"), System.Drawing.Image)
            Me.XrPictureBox1.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 10.41667!)
            Me.XrPictureBox1.Name = "XrPictureBox1"
            Me.XrPictureBox1.SizeF = New System.Drawing.SizeF(200.0!, 38.54167!)
            '
            'GroupHeader2
            '
            Me.GroupHeader2.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrTable3})
            Me.GroupHeader2.HeightF = 33.33333!
            Me.GroupHeader2.Name = "GroupHeader2"
            Me.GroupHeader2.RepeatEveryPage = True
            '
            'XrTable3
            '
            Me.XrTable3.AnchorVertical = DevExpress.XtraReports.UI.VerticalAnchorStyles.Bottom
            Me.XrTable3.Borders = DevExpress.XtraPrinting.BorderSide.Bottom
            Me.XrTable3.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 8.333333!)
            Me.XrTable3.Name = "XrTable3"
            Me.XrTable3.Padding = New DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 100.0!)
            Me.XrTable3.Rows.AddRange(New DevExpress.XtraReports.UI.XRTableRow() {Me.XrTableRow6})
            Me.XrTable3.SizeF = New System.Drawing.SizeF(769.7917!, 25.0!)
            Me.XrTable3.StylePriority.UseBorders = False
            Me.XrTable3.StylePriority.UsePadding = False
            Me.XrTable3.StylePriority.UseTextAlignment = False
            Me.XrTable3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            '
            'XrTableRow6
            '
            Me.XrTableRow6.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.XrTableCell4, Me.XrTableCell5, Me.XrTableCell6})
            Me.XrTableRow6.Font = New System.Drawing.Font("Calibri", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.XrTableRow6.Name = "XrTableRow6"
            Me.XrTableRow6.StyleName = "XrControlStyle_Report_Column"
            Me.XrTableRow6.StylePriority.UseFont = False
            Me.XrTableRow6.Weight = 1.0R
            '
            'XrTableCell4
            '
            Me.XrTableCell4.Borders = CType(((DevExpress.XtraPrinting.BorderSide.Left Or DevExpress.XtraPrinting.BorderSide.Top) _
                Or DevExpress.XtraPrinting.BorderSide.Bottom), DevExpress.XtraPrinting.BorderSide)
            Me.XrTableCell4.CanGrow = False
            Me.XrTableCell4.Name = "XrTableCell4"
            Me.XrTableCell4.StylePriority.UseBorders = False
            Me.XrTableCell4.Text = "Description"
            Me.XrTableCell4.Weight = 3.5066317559275397R
            '
            'XrTableCell5
            '
            Me.XrTableCell5.Borders = CType(((DevExpress.XtraPrinting.BorderSide.Left Or DevExpress.XtraPrinting.BorderSide.Top) _
                Or DevExpress.XtraPrinting.BorderSide.Bottom), DevExpress.XtraPrinting.BorderSide)
            Me.XrTableCell5.CanGrow = False
            Me.XrTableCell5.Name = "XrTableCell5"
            Me.XrTableCell5.StylePriority.UseBorders = False
            Me.XrTableCell5.StylePriority.UseTextAlignment = False
            Me.XrTableCell5.Text = "Unit Price"
            Me.XrTableCell5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrTableCell5.Weight = 0.64520414259086767R
            '
            'XrTableCell6
            '
            Me.XrTableCell6.Borders = CType((((DevExpress.XtraPrinting.BorderSide.Left Or DevExpress.XtraPrinting.BorderSide.Top) _
                Or DevExpress.XtraPrinting.BorderSide.Right) _
                Or DevExpress.XtraPrinting.BorderSide.Bottom), DevExpress.XtraPrinting.BorderSide)
            Me.XrTableCell6.CanGrow = False
            Me.XrTableCell6.Name = "XrTableCell6"
            Me.XrTableCell6.StylePriority.UseBorders = False
            Me.XrTableCell6.StylePriority.UseTextAlignment = False
            Me.XrTableCell6.Text = "Line Total"
            Me.XrTableCell6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrTableCell6.Weight = 0.63650126207523217R
            '
            'GroupFooter1
            '
            Me.GroupFooter1.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrTable5})
            Me.GroupFooter1.GroupUnion = DevExpress.XtraReports.UI.GroupFooterUnion.WithLastDetail
            Me.GroupFooter1.HeightF = 72.49!
            Me.GroupFooter1.Name = "GroupFooter1"
            '
            'XrTable5
            '
            Me.XrTable5.LocationFloat = New DevExpress.Utils.PointFloat(0.00001525879!, 0.0!)
            Me.XrTable5.Name = "XrTable5"
            Me.XrTable5.Padding = New DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 100.0!)
            Me.XrTable5.Rows.AddRange(New DevExpress.XtraReports.UI.XRTableRow() {Me.XrTableRow10, Me.XrTableRow8, Me.XrTableRow9, Me.XrTableRow11})
            Me.XrTable5.SizeF = New System.Drawing.SizeF(769.7917!, 72.49!)
            Me.XrTable5.StylePriority.UsePadding = False
            Me.XrTable5.StylePriority.UseTextAlignment = False
            Me.XrTable5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            '
            'XrTableRow10
            '
            Me.XrTableRow10.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.XrTableCell18, Me.XrTableCell19, Me.XrTableCell20})
            Me.XrTableRow10.Name = "XrTableRow10"
            Me.XrTableRow10.Weight = 0.47999998535156296R
            '
            'XrTableCell18
            '
            Me.XrTableCell18.Name = "XrTableCell18"
            Me.XrTableCell18.Weight = 8.1644455145814341R
            '
            'XrTableCell19
            '
            Me.XrTableCell19.Name = "XrTableCell19"
            Me.XrTableCell19.Weight = 0.37911087743378336R
            '
            'XrTableCell20
            '
            Me.XrTableCell20.Name = "XrTableCell20"
            Me.XrTableCell20.Weight = 1.309778154968638R
            '
            'XrTableRow8
            '
            Me.XrTableRow8.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.XrTableCell12, Me.XrTableCell13, Me.XrLabel_Subtotal})
            Me.XrTableRow8.Name = "XrTableRow8"
            Me.XrTableRow8.Weight = 0.99983996582519641R
            '
            'XrTableCell12
            '
            Me.XrTableCell12.Font = New System.Drawing.Font("Calibri", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.XrTableCell12.Name = "XrTableCell12"
            Me.XrTableCell12.StylePriority.UseFont = False
            Me.XrTableCell12.StylePriority.UseTextAlignment = False
            Me.XrTableCell12.Text = "Subtotal"
            Me.XrTableCell12.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrTableCell12.Weight = 8.1644455145814341R
            '
            'XrTableCell13
            '
            Me.XrTableCell13.Borders = CType((DevExpress.XtraPrinting.BorderSide.Left Or DevExpress.XtraPrinting.BorderSide.Top), DevExpress.XtraPrinting.BorderSide)
            Me.XrTableCell13.Name = "XrTableCell13"
            Me.XrTableCell13.StyleName = "XrControlStyle_LightGray"
            Me.XrTableCell13.StylePriority.UseBorders = False
            Me.XrTableCell13.StylePriority.UseTextAlignment = False
            Me.XrTableCell13.Text = "$"
            Me.XrTableCell13.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
            Me.XrTableCell13.Weight = 0.37911087743378336R
            '
            'XrLabel_Subtotal
            '
            Me.XrLabel_Subtotal.Borders = CType(((DevExpress.XtraPrinting.BorderSide.Left Or DevExpress.XtraPrinting.BorderSide.Top) _
                Or DevExpress.XtraPrinting.BorderSide.Right), DevExpress.XtraPrinting.BorderSide)
            Me.XrLabel_Subtotal.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "LineTotal")})
            Me.XrLabel_Subtotal.Font = New System.Drawing.Font("Calibri", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.XrLabel_Subtotal.Name = "XrLabel_Subtotal"
            Me.XrLabel_Subtotal.StyleName = "XrControlStyle_LightGray"
            Me.XrLabel_Subtotal.StylePriority.UseBorders = False
            Me.XrLabel_Subtotal.StylePriority.UseFont = False
            Me.XrLabel_Subtotal.StylePriority.UseTextAlignment = False
            XrSummary1.FormatString = "{0:n2}"
            XrSummary1.Running = DevExpress.XtraReports.UI.SummaryRunning.Report
            Me.XrLabel_Subtotal.Summary = XrSummary1
            Me.XrLabel_Subtotal.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrLabel_Subtotal.Weight = 1.309778154968638R
            '
            'XrTableRow9
            '
            Me.XrTableRow9.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.XrTableCell15, Me.XrTableCell16, Me.XrLabel_AmountPaid})
            Me.XrTableRow9.Name = "XrTableRow9"
            Me.XrTableRow9.Weight = 0.99983996582519641R
            '
            'XrTableCell15
            '
            Me.XrTableCell15.Font = New System.Drawing.Font("Calibri", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.XrTableCell15.Name = "XrTableCell15"
            Me.XrTableCell15.StylePriority.UseFont = False
            Me.XrTableCell15.StylePriority.UseTextAlignment = False
            Me.XrTableCell15.Text = "Amount Paid"
            Me.XrTableCell15.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrTableCell15.Weight = 8.1644455145814341R
            '
            'XrTableCell16
            '
            Me.XrTableCell16.Borders = CType((DevExpress.XtraPrinting.BorderSide.Left Or DevExpress.XtraPrinting.BorderSide.Top), DevExpress.XtraPrinting.BorderSide)
            Me.XrTableCell16.Name = "XrTableCell16"
            Me.XrTableCell16.StyleName = "XrControlStyle_LightGray"
            Me.XrTableCell16.StylePriority.UseBorders = False
            Me.XrTableCell16.Weight = 0.37911087743378324R
            '
            'XrLabel_AmountPaid
            '
            Me.XrLabel_AmountPaid.Borders = CType(((DevExpress.XtraPrinting.BorderSide.Left Or DevExpress.XtraPrinting.BorderSide.Top) _
                Or DevExpress.XtraPrinting.BorderSide.Right), DevExpress.XtraPrinting.BorderSide)
            Me.XrLabel_AmountPaid.Name = "XrLabel_AmountPaid"
            Me.XrLabel_AmountPaid.StyleName = "XrControlStyle_LightGray"
            Me.XrLabel_AmountPaid.StylePriority.UseBorders = False
            Me.XrLabel_AmountPaid.StylePriority.UseTextAlignment = False
            Me.XrLabel_AmountPaid.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrLabel_AmountPaid.Weight = 1.309778154968638R
            '
            'XrTableRow11
            '
            Me.XrTableRow11.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.XrTableCell21, Me.XrTableCell22, Me.XrLabel_AmountDue})
            Me.XrTableRow11.Name = "XrTableRow11"
            Me.XrTableRow11.Weight = 0.9998399658251963R
            '
            'XrTableCell21
            '
            Me.XrTableCell21.Font = New System.Drawing.Font("Calibri", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.XrTableCell21.Name = "XrTableCell21"
            Me.XrTableCell21.StylePriority.UseFont = False
            Me.XrTableCell21.StylePriority.UseTextAlignment = False
            Me.XrTableCell21.Text = "Amount Due"
            Me.XrTableCell21.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrTableCell21.Weight = 8.1644455145814341R
            '
            'XrTableCell22
            '
            Me.XrTableCell22.Borders = CType(((DevExpress.XtraPrinting.BorderSide.Left Or DevExpress.XtraPrinting.BorderSide.Top) _
                Or DevExpress.XtraPrinting.BorderSide.Bottom), DevExpress.XtraPrinting.BorderSide)
            Me.XrTableCell22.Name = "XrTableCell22"
            Me.XrTableCell22.StyleName = "XrControlStyle_LightGray"
            Me.XrTableCell22.StylePriority.UseBorders = False
            Me.XrTableCell22.StylePriority.UseTextAlignment = False
            Me.XrTableCell22.Text = "$"
            Me.XrTableCell22.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
            Me.XrTableCell22.Weight = 0.37911087743378324R
            '
            'XrLabel_AmountDue
            '
            Me.XrLabel_AmountDue.Borders = CType((((DevExpress.XtraPrinting.BorderSide.Left Or DevExpress.XtraPrinting.BorderSide.Top) _
                Or DevExpress.XtraPrinting.BorderSide.Right) _
                Or DevExpress.XtraPrinting.BorderSide.Bottom), DevExpress.XtraPrinting.BorderSide)
            Me.XrLabel_AmountDue.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "LineTotal")})
            Me.XrLabel_AmountDue.Font = New System.Drawing.Font("Calibri", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.XrLabel_AmountDue.Name = "XrLabel_AmountDue"
            Me.XrLabel_AmountDue.StyleName = "XrControlStyle_LightGray"
            Me.XrLabel_AmountDue.StylePriority.UseBorders = False
            Me.XrLabel_AmountDue.StylePriority.UseFont = False
            Me.XrLabel_AmountDue.StylePriority.UseTextAlignment = False
            XrSummary2.FormatString = "{0:n2}"
            XrSummary2.Running = DevExpress.XtraReports.UI.SummaryRunning.Report
            Me.XrLabel_AmountDue.Summary = XrSummary2
            Me.XrLabel_AmountDue.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrLabel_AmountDue.Weight = 1.309778154968638R
            '
            'XrTable8
            '
            Me.XrTable8.LocationFloat = New DevExpress.Utils.PointFloat(0.00001589457!, 29.91666!)
            Me.XrTable8.Name = "XrTable8"
            Me.XrTable8.Padding = New DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 100.0!)
            Me.XrTable8.Rows.AddRange(New DevExpress.XtraReports.UI.XRTableRow() {Me.XrTableRow19, Me.XrTableRow20, Me.XrTableRow21, Me.XrTableRow22, Me.XrTableRow23, Me.XrTableRow24})
            Me.XrTable8.SizeF = New System.Drawing.SizeF(480.8748!, 120.0!)
            Me.XrTable8.StylePriority.UsePadding = False
            Me.XrTable8.StylePriority.UseTextAlignment = False
            Me.XrTable8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
            '
            'XrTableRow19
            '
            Me.XrTableRow19.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.XrTableCell29, Me.XrTableCell_Payment_1})
            Me.XrTableRow19.Name = "XrTableRow19"
            Me.XrTableRow19.Weight = 0.8R
            '
            'XrTableCell29
            '
            Me.XrTableCell29.Font = New System.Drawing.Font("Calibri", 12.0!, System.Drawing.FontStyle.Bold)
            Me.XrTableCell29.Name = "XrTableCell29"
            Me.XrTableCell29.Padding = New DevExpress.XtraPrinting.PaddingInfo(5, 10, 0, 0, 100.0!)
            Me.XrTableCell29.StylePriority.UseFont = False
            Me.XrTableCell29.StylePriority.UsePadding = False
            Me.XrTableCell29.StylePriority.UseTextAlignment = False
            Me.XrTableCell29.Text = "Mail Payment To:"
            Me.XrTableCell29.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
            Me.XrTableCell29.Weight = 0.9357942554262928R
            '
            'XrTableCell_Payment_1
            '
            Me.XrTableCell_Payment_1.Name = "XrTableCell_Payment_1"
            Me.XrTableCell_Payment_1.Text = "ClearPoint Credit Counseling Solutions"
            Me.XrTableCell_Payment_1.Weight = 2.064205744573707R
            '
            'XrTableRow20
            '
            Me.XrTableRow20.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.XrTableCell30, Me.XrTableCell_Payment_2})
            Me.XrTableRow20.Name = "XrTableRow20"
            Me.XrTableRow20.Weight = 0.80000000000000016R
            '
            'XrTableCell30
            '
            Me.XrTableCell30.Name = "XrTableCell30"
            Me.XrTableCell30.Weight = 0.9357942554262928R
            '
            'XrTableCell_Payment_2
            '
            Me.XrTableCell_Payment_2.Name = "XrTableCell_Payment_2"
            Me.XrTableCell_Payment_2.Text = "Attn: Payment Processing-FHLB"
            Me.XrTableCell_Payment_2.Weight = 2.064205744573707R
            '
            'XrTableRow21
            '
            Me.XrTableRow21.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.XrTableCell31, Me.XrTableCell_Payment_3})
            Me.XrTableRow21.Name = "XrTableRow21"
            Me.XrTableRow21.Weight = 0.79999999999999993R
            '
            'XrTableCell31
            '
            Me.XrTableCell31.Name = "XrTableCell31"
            Me.XrTableCell31.Weight = 0.9357942554262928R
            '
            'XrTableCell_Payment_3
            '
            Me.XrTableCell_Payment_3.Name = "XrTableCell_Payment_3"
            Me.XrTableCell_Payment_3.Text = "270 Peachtree Street NW, Suite 1800"
            Me.XrTableCell_Payment_3.Weight = 2.064205744573707R
            '
            'XrTableRow22
            '
            Me.XrTableRow22.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.XrTableCell32, Me.XrTableCell_Payment_4})
            Me.XrTableRow22.Name = "XrTableRow22"
            Me.XrTableRow22.Weight = 0.8R
            '
            'XrTableCell32
            '
            Me.XrTableCell32.Name = "XrTableCell32"
            Me.XrTableCell32.Weight = 0.9331950799962303R
            '
            'XrTableCell_Payment_4
            '
            Me.XrTableCell_Payment_4.Name = "XrTableCell_Payment_4"
            Me.XrTableCell_Payment_4.Text = "Atlanta, GA 30303"
            Me.XrTableCell_Payment_4.Weight = 2.0668049200037695R
            '
            'XrTableRow23
            '
            Me.XrTableRow23.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.XrTableCell33, Me.XrTableCell_Payment_5})
            Me.XrTableRow23.Name = "XrTableRow23"
            Me.XrTableRow23.Weight = 0.8R
            '
            'XrTableCell33
            '
            Me.XrTableCell33.Name = "XrTableCell33"
            Me.XrTableCell33.Weight = 0.9331951751901737R
            '
            'XrTableCell_Payment_5
            '
            Me.XrTableCell_Payment_5.Name = "XrTableCell_Payment_5"
            Me.XrTableCell_Payment_5.Weight = 2.0668048248098261R
            '
            'XrTableRow24
            '
            Me.XrTableRow24.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.XrTableCell35, Me.XrTableCell_Payment_6})
            Me.XrTableRow24.Name = "XrTableRow24"
            Me.XrTableRow24.Weight = 0.8R
            '
            'XrTableCell35
            '
            Me.XrTableCell35.Name = "XrTableCell35"
            Me.XrTableCell35.Weight = 0.9331950799962303R
            '
            'XrTableCell_Payment_6
            '
            Me.XrTableCell_Payment_6.Name = "XrTableCell_Payment_6"
            Me.XrTableCell_Payment_6.Weight = 2.0668049200037695R
            '
            'XrLabel18
            '
            Me.XrLabel18.Font = New System.Drawing.Font("Calibri", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.XrLabel18.ForeColor = System.Drawing.Color.Teal
            Me.XrLabel18.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 149.9167!)
            Me.XrLabel18.Name = "XrLabel18"
            Me.XrLabel18.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel18.SizeF = New System.Drawing.SizeF(799.0!, 23.0!)
            Me.XrLabel18.StylePriority.UseFont = False
            Me.XrLabel18.StylePriority.UseTextAlignment = False
            Me.XrLabel18.Text = "Thank you for your business!"
            Me.XrLabel18.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
            '
            'XrTable6
            '
            Me.XrTable6.BorderWidth = 0
            Me.XrTable6.LocationFloat = New DevExpress.Utils.PointFloat(500.3472!, 37.43749!)
            Me.XrTable6.Name = "XrTable6"
            Me.XrTable6.Rows.AddRange(New DevExpress.XtraReports.UI.XRTableRow() {Me.XrTableRow12, Me.XrTableRow13, Me.XrTableRow14})
            Me.XrTable6.SizeF = New System.Drawing.SizeF(292.0833!, 69.0!)
            Me.XrTable6.StylePriority.UseBorderWidth = False
            '
            'XrTableRow12
            '
            Me.XrTableRow12.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.XrTableCell24, Me.XrTableCell_Footer_Client})
            Me.XrTableRow12.Name = "XrTableRow12"
            Me.XrTableRow12.Weight = 0.53670425627445928R
            '
            'XrTableCell24
            '
            Me.XrTableCell24.Name = "XrTableCell24"
            Me.XrTableCell24.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 10, 0, 0, 100.0!)
            Me.XrTableCell24.StylePriority.UsePadding = False
            Me.XrTableCell24.StylePriority.UseTextAlignment = False
            Me.XrTableCell24.Text = "Inv #"
            Me.XrTableCell24.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight
            Me.XrTableCell24.Weight = 0.67022957515180115R
            '
            'XrTableCell_Footer_Client
            '
            Me.XrTableCell_Footer_Client.Font = New System.Drawing.Font("Calibri", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.XrTableCell_Footer_Client.Name = "XrTableCell_Footer_Client"
            Me.XrTableCell_Footer_Client.Padding = New DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 100.0!)
            Me.XrTableCell_Footer_Client.StylePriority.UseFont = False
            Me.XrTableCell_Footer_Client.StylePriority.UsePadding = False
            Me.XrTableCell_Footer_Client.StylePriority.UseTextAlignment = False
            Me.XrTableCell_Footer_Client.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft
            Me.XrTableCell_Footer_Client.Weight = 2.4178762189397807R
            '
            'XrTableRow13
            '
            Me.XrTableRow13.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.XrTableCell25, Me.XrTableCell_Footer_Name})
            Me.XrTableRow13.Name = "XrTableRow13"
            Me.XrTableRow13.Weight = 0.53670425627445928R
            '
            'XrTableCell25
            '
            Me.XrTableCell25.Name = "XrTableCell25"
            Me.XrTableCell25.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 10, 0, 0, 100.0!)
            Me.XrTableCell25.StylePriority.UsePadding = False
            Me.XrTableCell25.StylePriority.UseTextAlignment = False
            Me.XrTableCell25.Text = "Name"
            Me.XrTableCell25.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight
            Me.XrTableCell25.Weight = 0.67022957515180115R
            '
            'XrTableCell_Footer_Name
            '
            Me.XrTableCell_Footer_Name.Name = "XrTableCell_Footer_Name"
            Me.XrTableCell_Footer_Name.Padding = New DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 100.0!)
            Me.XrTableCell_Footer_Name.StylePriority.UsePadding = False
            Me.XrTableCell_Footer_Name.StylePriority.UseTextAlignment = False
            Me.XrTableCell_Footer_Name.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft
            Me.XrTableCell_Footer_Name.Weight = 2.4178762189397807R
            '
            'XrTableRow14
            '
            Me.XrTableRow14.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.XrTableCell26, Me.XrTableCell_Footer_Address})
            Me.XrTableRow14.Name = "XrTableRow14"
            Me.XrTableRow14.Weight = 0.53670425627445928R
            '
            'XrTableCell26
            '
            Me.XrTableCell26.Name = "XrTableCell26"
            Me.XrTableCell26.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 10, 0, 0, 100.0!)
            Me.XrTableCell26.StylePriority.UsePadding = False
            Me.XrTableCell26.Weight = 0.67022957515180115R
            '
            'XrTableCell_Footer_Address
            '
            Me.XrTableCell_Footer_Address.Multiline = True
            Me.XrTableCell_Footer_Address.Name = "XrTableCell_Footer_Address"
            Me.XrTableCell_Footer_Address.Padding = New DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 100.0!)
            Me.XrTableCell_Footer_Address.StylePriority.UsePadding = False
            Me.XrTableCell_Footer_Address.StylePriority.UseTextAlignment = False
            Me.XrTableCell_Footer_Address.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
            Me.XrTableCell_Footer_Address.Weight = 2.4178762189397807R
            '
            'XrLabel8
            '
            Me.XrLabel8.BorderColor = System.Drawing.Color.Teal
            Me.XrLabel8.BorderDashStyle = DevExpress.XtraPrinting.BorderDashStyle.Dash
            Me.XrLabel8.Borders = DevExpress.XtraPrinting.BorderSide.Top
            Me.XrLabel8.BorderWidth = 3
            Me.XrLabel8.Font = New System.Drawing.Font("Calibri", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.XrLabel8.ForeColor = System.Drawing.Color.Teal
            Me.XrLabel8.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
            Me.XrLabel8.Name = "XrLabel8"
            Me.XrLabel8.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel8.SizeF = New System.Drawing.SizeF(798.9983!, 23.00001!)
            Me.XrLabel8.StylePriority.UseBorderColor = False
            Me.XrLabel8.StylePriority.UseBorderDashStyle = False
            Me.XrLabel8.StylePriority.UseBorders = False
            Me.XrLabel8.StylePriority.UseBorderWidth = False
            Me.XrLabel8.StylePriority.UseFont = False
            Me.XrLabel8.StylePriority.UseForeColor = False
            Me.XrLabel8.StylePriority.UseTextAlignment = False
            Me.XrLabel8.Text = "Please return this coupon with the payment if you are going to send the payment b" & _
        "y mail"
            Me.XrLabel8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
            '
            'XrTable4
            '
            Me.XrTable4.AnchorVertical = CType((DevExpress.XtraReports.UI.VerticalAnchorStyles.Top Or DevExpress.XtraReports.UI.VerticalAnchorStyles.Bottom), DevExpress.XtraReports.UI.VerticalAnchorStyles)
            Me.XrTable4.LocationFloat = New DevExpress.Utils.PointFloat(0.00001525879!, 0.0!)
            Me.XrTable4.Name = "XrTable4"
            Me.XrTable4.Padding = New DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 100.0!)
            Me.XrTable4.Rows.AddRange(New DevExpress.XtraReports.UI.XRTableRow() {Me.XrTableRow7})
            Me.XrTable4.SizeF = New System.Drawing.SizeF(769.7917!, 25.0!)
            Me.XrTable4.StylePriority.UsePadding = False
            Me.XrTable4.StylePriority.UseTextAlignment = False
            Me.XrTable4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            '
            'XrTableRow7
            '
            Me.XrTableRow7.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.XrLabel_Description, Me.XrLabel_UnitPrice, Me.XrLabel_LineTotal})
            Me.XrTableRow7.Name = "XrTableRow7"
            Me.XrTableRow7.StyleName = "XrControlStyle_LightGray"
            Me.XrTableRow7.Weight = 1.0R
            '
            'XrLabel_Description
            '
            Me.XrLabel_Description.Borders = CType((DevExpress.XtraPrinting.BorderSide.Left Or DevExpress.XtraPrinting.BorderSide.Bottom), DevExpress.XtraPrinting.BorderSide)
            Me.XrLabel_Description.CanGrow = False
            Me.XrLabel_Description.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "Description")})
            Me.XrLabel_Description.Font = New System.Drawing.Font("Calibri", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.XrLabel_Description.Name = "XrLabel_Description"
            Me.XrLabel_Description.StylePriority.UseBorders = False
            Me.XrLabel_Description.StylePriority.UseFont = False
            Me.XrLabel_Description.Weight = 7.215868860313984R
            '
            'XrLabel_UnitPrice
            '
            Me.XrLabel_UnitPrice.Borders = CType((DevExpress.XtraPrinting.BorderSide.Left Or DevExpress.XtraPrinting.BorderSide.Bottom), DevExpress.XtraPrinting.BorderSide)
            Me.XrLabel_UnitPrice.CanGrow = False
            Me.XrLabel_UnitPrice.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "UnitPrice", "{0:n2}")})
            Me.XrLabel_UnitPrice.Font = New System.Drawing.Font("Calibri", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.XrLabel_UnitPrice.Name = "XrLabel_UnitPrice"
            Me.XrLabel_UnitPrice.StylePriority.UseBorders = False
            Me.XrLabel_UnitPrice.StylePriority.UseFont = False
            Me.XrLabel_UnitPrice.StylePriority.UseTextAlignment = False
            Me.XrLabel_UnitPrice.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrLabel_UnitPrice.Weight = 1.3276875317012329R
            '
            'XrLabel_LineTotal
            '
            Me.XrLabel_LineTotal.Borders = CType(((DevExpress.XtraPrinting.BorderSide.Left Or DevExpress.XtraPrinting.BorderSide.Right) _
                Or DevExpress.XtraPrinting.BorderSide.Bottom), DevExpress.XtraPrinting.BorderSide)
            Me.XrLabel_LineTotal.CanGrow = False
            Me.XrLabel_LineTotal.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "LineTotal", "{0:n2}")})
            Me.XrLabel_LineTotal.Font = New System.Drawing.Font("Calibri", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.XrLabel_LineTotal.Name = "XrLabel_LineTotal"
            Me.XrLabel_LineTotal.StylePriority.UseBorders = False
            Me.XrLabel_LineTotal.StylePriority.UseFont = False
            Me.XrLabel_LineTotal.StylePriority.UseTextAlignment = False
            Me.XrLabel_LineTotal.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrLabel_LineTotal.Weight = 1.309778154968638R
            '
            'XrTable7
            '
            Me.XrTable7.LocationFloat = New DevExpress.Utils.PointFloat(0.00001589457!, 61.45833!)
            Me.XrTable7.Name = "XrTable7"
            Me.XrTable7.Padding = New DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 100.0!)
            Me.XrTable7.Rows.AddRange(New DevExpress.XtraReports.UI.XRTableRow() {Me.XrTableRow15, Me.XrTableRow16, Me.XrTableRow17, Me.XrTableRow18})
            Me.XrTable7.SizeF = New System.Drawing.SizeF(388.5417!, 80.0!)
            Me.XrTable7.StylePriority.UsePadding = False
            Me.XrTable7.StylePriority.UseTextAlignment = False
            Me.XrTable7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
            '
            'XrTableRow15
            '
            Me.XrTableRow15.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.XrTableCell_AgencyAddress_1})
            Me.XrTableRow15.Name = "XrTableRow15"
            Me.XrTableRow15.Weight = 0.8R
            '
            'XrTableCell_AgencyAddress_1
            '
            Me.XrTableCell_AgencyAddress_1.Name = "XrTableCell_AgencyAddress_1"
            Me.XrTableCell_AgencyAddress_1.Text = "ClearPoint Credit Counseling Solutions"
            Me.XrTableCell_AgencyAddress_1.Weight = 3.0R
            '
            'XrTableRow16
            '
            Me.XrTableRow16.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.XrTableCell_AgencyAddress_2})
            Me.XrTableRow16.Name = "XrTableRow16"
            Me.XrTableRow16.Weight = 0.80000000000000016R
            '
            'XrTableCell_AgencyAddress_2
            '
            Me.XrTableCell_AgencyAddress_2.Name = "XrTableCell_AgencyAddress_2"
            Me.XrTableCell_AgencyAddress_2.Text = "270 Peachtree Street NW, Suite 1800"
            Me.XrTableCell_AgencyAddress_2.Weight = 3.0R
            '
            'XrTableRow17
            '
            Me.XrTableRow17.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.XrTableCell_AgencyAddress_3})
            Me.XrTableRow17.Name = "XrTableRow17"
            Me.XrTableRow17.Weight = 0.79999999999999993R
            '
            'XrTableCell_AgencyAddress_3
            '
            Me.XrTableCell_AgencyAddress_3.Name = "XrTableCell_AgencyAddress_3"
            Me.XrTableCell_AgencyAddress_3.Text = "Atlanta, GA 30303"
            Me.XrTableCell_AgencyAddress_3.Weight = 3.0R
            '
            'XrTableRow18
            '
            Me.XrTableRow18.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.XrTableCell_AgencyAddress_4})
            Me.XrTableRow18.Name = "XrTableRow18"
            Me.XrTableRow18.Weight = 0.8R
            '
            'XrTableCell_AgencyAddress_4
            '
            Me.XrTableCell_AgencyAddress_4.Name = "XrTableCell_AgencyAddress_4"
            Me.XrTableCell_AgencyAddress_4.Weight = 3.0R
            '
            'XrLabel9
            '
            Me.XrLabel9.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 1027.0!)
            Me.XrLabel9.Name = "XrLabel9"
            Me.XrLabel9.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel9.SizeF = New System.Drawing.SizeF(800.0!, 23.0!)
            Me.XrLabel9.StyleName = "XrControlStyle_HeaderBox3"
            Me.XrLabel9.StylePriority.UseTextAlignment = False
            Me.XrLabel9.Text = "This page was intentionally left blank"
            Me.XrLabel9.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
            '
            'GroupHeader1
            '
            Me.GroupHeader1.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrTable7, Me.XrPictureBox1, Me.XrTable2, Me.XrTable1, Me.XrLabel_Header_Invoice})
            Me.GroupHeader1.HeightF = 237.0833!
            Me.GroupHeader1.Level = 1
            Me.GroupHeader1.Name = "GroupHeader1"
            Me.GroupHeader1.RepeatEveryPage = True
            '
            'GroupFooter2
            '
            Me.GroupFooter2.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrTable8, Me.XrLabel18, Me.XrTable6, Me.XrLabel8})
            Me.GroupFooter2.HeightF = 172.9167!
            Me.GroupFooter2.KeepTogether = True
            Me.GroupFooter2.Level = 1
            Me.GroupFooter2.Name = "GroupFooter2"
            Me.GroupFooter2.PrintAtBottom = True
            Me.GroupFooter2.StylePriority.UseTextAlignment = False
            Me.GroupFooter2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            '
            'XrControlStyle_LightGray
            '
            Me.XrControlStyle_LightGray.BackColor = System.Drawing.Color.LightGray
            Me.XrControlStyle_LightGray.Font = New System.Drawing.Font("Calibri", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.XrControlStyle_LightGray.Name = "XrControlStyle_LightGray"
            Me.XrControlStyle_LightGray.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            '
            'EnglishFHLBInvoice_1
            '
            Me.Bands.AddRange(New DevExpress.XtraReports.UI.Band() {Me.TopMarginBand1, Me.Detail, Me.BottomMarginBand1, Me.ReportHeader, Me.PageHeader, Me.GroupHeader2, Me.GroupFooter1, Me.GroupHeader1, Me.GroupFooter2})
            Me.Parameters.AddRange(New DevExpress.XtraReports.Parameters.Parameter() {Me.ParameterClient, Me.ParameterCreditor})
            Me.ReportPrintOptions.DetailCountOnEmptyDataSource = 0
            Me.ReportPrintOptions.PrintOnEmptyDataSource = False
            Me.RequestParameters = False
            Me.ScriptLanguage = DevExpress.XtraReports.ScriptLanguage.VisualBasic
            Me.Scripts.OnBeforePrint = "FHLB_Invoice_1_BeforePrint"
            Me.ScriptsSource = resources.GetString("$this.ScriptsSource")
            Me.StyleSheet.AddRange(New DevExpress.XtraReports.UI.XRControlStyle() {Me.XrControlStyle_HeaderText, Me.XrControlStyle_HeaderLine, Me.XrControlStyle_HeaderBox1, Me.XrControlStyle_HeaderBox2, Me.XrControlStyle_HeaderBox3, Me.XrControlStyle_Report_Column, Me.XrControlStyle_Report_Group, Me.XrControlStyle_Report_Detail, Me.XrControlStyle_Report_Total, Me.XrControlStyle_Footer, Me.XrControlStyle_HeaderSubTitle, Me.XrControlStyle_LightGray})
            Me.Version = "11.2"
            Me.Controls.SetChildIndex(Me.GroupFooter2, 0)
            Me.Controls.SetChildIndex(Me.GroupHeader1, 0)
            Me.Controls.SetChildIndex(Me.GroupFooter1, 0)
            Me.Controls.SetChildIndex(Me.GroupHeader2, 0)
            Me.Controls.SetChildIndex(Me.PageHeader, 0)
            Me.Controls.SetChildIndex(Me.ReportHeader, 0)
            Me.Controls.SetChildIndex(Me.BottomMarginBand1, 0)
            Me.Controls.SetChildIndex(Me.Detail, 0)
            Me.Controls.SetChildIndex(Me.TopMarginBand1, 0)
            CType(Me.XrTable1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.XrTable2, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.XrTable3, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.XrTable5, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.XrTable8, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.XrTable6, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.XrTable4, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.XrTable7, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me, System.ComponentModel.ISupportInitialize).EndInit()

        End Sub

        Protected GroupFooter1 As DevExpress.XtraReports.UI.GroupFooterBand
        Protected GroupHeader2 As DevExpress.XtraReports.UI.GroupHeaderBand
        Protected ParameterClient As DevExpress.XtraReports.Parameters.Parameter
        Protected ParameterCreditor As DevExpress.XtraReports.Parameters.Parameter
        Protected XrLabel_AmountDue As DevExpress.XtraReports.UI.XRTableCell
        Protected XrLabel_AmountPaid As DevExpress.XtraReports.UI.XRTableCell
        Protected XrLabel_Description As DevExpress.XtraReports.UI.XRTableCell
        Protected XrLabel_Header_Invoice As DevExpress.XtraReports.UI.XRLabel
        Protected XrLabel_LineTotal As DevExpress.XtraReports.UI.XRTableCell
        Protected XrLabel_Subtotal As DevExpress.XtraReports.UI.XRTableCell
        Protected XrLabel_UnitPrice As DevExpress.XtraReports.UI.XRTableCell
        Protected XrLabel18 As DevExpress.XtraReports.UI.XRLabel
        Protected XrLabel8 As DevExpress.XtraReports.UI.XRLabel
        Protected XrPictureBox1 As DevExpress.XtraReports.UI.XRPictureBox
        Protected XrTable1 As DevExpress.XtraReports.UI.XRTable
        Protected XrTable2 As DevExpress.XtraReports.UI.XRTable
        Protected XrTable3 As DevExpress.XtraReports.UI.XRTable
        Protected XrTable4 As DevExpress.XtraReports.UI.XRTable
        Protected XrTable5 As DevExpress.XtraReports.UI.XRTable
        Protected XrTable6 As DevExpress.XtraReports.UI.XRTable
        Protected XrTableCell_appointment_date As DevExpress.XtraReports.UI.XRTableCell
        Protected XrTableCell_Client As DevExpress.XtraReports.UI.XRTableCell
        Protected XrTableCell_client_address As DevExpress.XtraReports.UI.XRTableCell
        Protected XrTableCell_client_name As DevExpress.XtraReports.UI.XRTableCell
        Protected XrTableCell_Date As DevExpress.XtraReports.UI.XRTableCell
        Protected XrTableCell_Footer_Address As DevExpress.XtraReports.UI.XRTableCell
        Protected XrTableCell_Footer_Client As DevExpress.XtraReports.UI.XRTableCell
        Protected XrTableCell_Footer_Name As DevExpress.XtraReports.UI.XRTableCell
        Protected XrTableCell1 As DevExpress.XtraReports.UI.XRTableCell
        Protected XrTableCell12 As DevExpress.XtraReports.UI.XRTableCell
        Protected XrTableCell13 As DevExpress.XtraReports.UI.XRTableCell
        Protected XrTableCell15 As DevExpress.XtraReports.UI.XRTableCell
        Protected XrTableCell16 As DevExpress.XtraReports.UI.XRTableCell
        Protected XrTableCell18 As DevExpress.XtraReports.UI.XRTableCell
        Protected XrTableCell19 As DevExpress.XtraReports.UI.XRTableCell
        Protected XrTableCell2 As DevExpress.XtraReports.UI.XRTableCell
        Protected XrTableCell20 As DevExpress.XtraReports.UI.XRTableCell
        Protected XrTableCell21 As DevExpress.XtraReports.UI.XRTableCell
        Protected XrTableCell22 As DevExpress.XtraReports.UI.XRTableCell
        Protected XrTableCell24 As DevExpress.XtraReports.UI.XRTableCell
        Protected XrTableCell25 As DevExpress.XtraReports.UI.XRTableCell
        Protected XrTableCell26 As DevExpress.XtraReports.UI.XRTableCell
        Protected XrTableCell3 As DevExpress.XtraReports.UI.XRTableCell
        Protected XrTableCell4 As DevExpress.XtraReports.UI.XRTableCell
        Protected XrTableCell5 As DevExpress.XtraReports.UI.XRTableCell
        Protected XrTableCell6 As DevExpress.XtraReports.UI.XRTableCell
        Protected XrTableCell7 As DevExpress.XtraReports.UI.XRTableCell
        Protected XrTableCell8 As DevExpress.XtraReports.UI.XRTableCell
        Protected XrTableRow1 As DevExpress.XtraReports.UI.XRTableRow
        Protected XrTableRow10 As DevExpress.XtraReports.UI.XRTableRow
        Protected XrTableRow11 As DevExpress.XtraReports.UI.XRTableRow
        Protected XrTableRow12 As DevExpress.XtraReports.UI.XRTableRow
        Protected XrTableRow13 As DevExpress.XtraReports.UI.XRTableRow
        Protected XrTableRow14 As DevExpress.XtraReports.UI.XRTableRow
        Protected XrTableRow2 As DevExpress.XtraReports.UI.XRTableRow
        Protected XrTableRow3 As DevExpress.XtraReports.UI.XRTableRow
        Protected XrTableRow4 As DevExpress.XtraReports.UI.XRTableRow
        Protected XrTableRow5 As DevExpress.XtraReports.UI.XRTableRow
        Protected XrTableRow6 As DevExpress.XtraReports.UI.XRTableRow
        Protected XrTableRow7 As DevExpress.XtraReports.UI.XRTableRow
        Protected XrTableRow8 As DevExpress.XtraReports.UI.XRTableRow
        Protected XrTableRow9 As DevExpress.XtraReports.UI.XRTableRow
        Friend WithEvents XrTable7 As DevExpress.XtraReports.UI.XRTable
        Friend WithEvents XrTableRow15 As DevExpress.XtraReports.UI.XRTableRow
        Friend WithEvents XrTableCell_AgencyAddress_1 As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents XrTableRow16 As DevExpress.XtraReports.UI.XRTableRow
        Friend WithEvents XrTableCell_AgencyAddress_2 As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents XrTableRow17 As DevExpress.XtraReports.UI.XRTableRow
        Friend WithEvents XrTableCell_AgencyAddress_3 As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents XrTableRow18 As DevExpress.XtraReports.UI.XRTableRow
        Friend WithEvents XrTableCell_AgencyAddress_4 As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents XrTable8 As DevExpress.XtraReports.UI.XRTable
        Friend WithEvents XrTableRow19 As DevExpress.XtraReports.UI.XRTableRow
        Friend WithEvents XrTableCell29 As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents XrTableCell_Payment_1 As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents XrTableRow20 As DevExpress.XtraReports.UI.XRTableRow
        Friend WithEvents XrTableCell30 As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents XrTableCell_Payment_2 As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents XrTableRow21 As DevExpress.XtraReports.UI.XRTableRow
        Friend WithEvents XrTableCell31 As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents XrTableCell_Payment_3 As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents XrTableRow22 As DevExpress.XtraReports.UI.XRTableRow
        Friend WithEvents XrTableCell32 As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents XrTableCell_Payment_4 As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents XrTableRow23 As DevExpress.XtraReports.UI.XRTableRow
        Friend WithEvents XrTableCell33 As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents XrTableCell_Payment_5 As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents XrTableRow24 As DevExpress.XtraReports.UI.XRTableRow
        Friend WithEvents XrTableCell35 As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents XrTableCell_Payment_6 As DevExpress.XtraReports.UI.XRTableCell
        Protected WithEvents XrLabel9 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents GroupHeader1 As DevExpress.XtraReports.UI.GroupHeaderBand
        Friend WithEvents GroupFooter2 As DevExpress.XtraReports.UI.GroupFooterBand
        Friend WithEvents XrControlStyle_LightGray As DevExpress.XtraReports.UI.XRControlStyle
    End Class
End Namespace
