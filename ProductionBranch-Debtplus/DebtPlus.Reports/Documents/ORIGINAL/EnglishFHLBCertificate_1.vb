﻿Namespace Documents
    Public Class EnglishFHLBCertificate_1
        Public Sub New()
            MyBase.New()
            InitializeComponent()
            AddHandler BeforePrint, AddressOf EnglishFHLBCertificate_1_BeforePrint
        End Sub

        Private Sub EnglishFHLBCertificate_1_BeforePrint(sender As Object, e As System.Drawing.Printing.PrintEventArgs)

            Dim rpt As DevExpress.XtraReports.UI.XtraReport = TryCast(sender, DevExpress.XtraReports.UI.XtraReport)
            If rpt Is Nothing Then
                Return
            End If

            Dim MasterRpt As DebtPlus.Interfaces.Client.IClient = TryCast(rpt.MasterReport, DebtPlus.Interfaces.Client.IClient)
            Dim ClientID As Int32 = -1
            If MasterRpt IsNot Nothing Then
                ClientID = MasterRpt.ClientId
            End If

            If ClientID < 0 Then
                Return
            End If

            ' Find the language for the globalization routine
            Dim languageKey As String = Nothing
            Dim parm As DevExpress.XtraReports.Parameters.Parameter = rpt.Parameters("ParameterLanguage")
            If parm IsNot Nothing Then
                Dim languageID As Int32 = Convert.ToInt32(parm.Value)
                For Each lan As DebtPlus.LINQ.AttributeType In DebtPlus.LINQ.Cache.AttributeType.getLanguageList()
                    If lan.Id = languageID Then
                        languageKey = lan.LanguageID
                        Exit For
                    End If
                Next
            End If

            ' Ensure that there is an ID for the language
            If String.IsNullOrEmpty(languageKey) Then
                languageKey = "en-US"
            End If

            ' Map the language string to a culture reference
            Dim culture As System.Globalization.CultureInfo = System.Globalization.CultureInfo.CreateSpecificCulture(languageKey)
            If culture Is Nothing Then
                culture = System.Globalization.CultureInfo.CurrentCulture
            End If

            Try
                Using ds As New System.Data.DataSet()
                    Using cn As New System.Data.SqlClient.SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
                        cn.Open()

                        ' Read the information that goes into the header area
                        ReadClientData(cn, ClientID)
                        ReadAppointmentData(cn, ClientID, culture)
                    End Using
                End Using

            Catch ex As System.Data.SqlClient.SqlException
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error reading invoice report data")
            End Try
        End Sub

        ''' <summary>
        ''' Read the client information for the current report
        ''' </summary>
        Private Sub ReadClientData(cn As System.Data.SqlClient.SqlConnection, clientID As Int32)

            ' Read the client information from the view if needed
            Using cmd As New System.Data.SqlClient.SqlCommand()
                cmd.Connection = cn
                cmd.CommandText = "SELECT [salutation] FROM [view_client_address] WITH (NOLOCK) WHERE [client] = @client"
                cmd.CommandType = System.Data.CommandType.Text
                cmd.CommandTimeout = 0
                cmd.Parameters.Add("@client", System.Data.SqlDbType.Int).Value = clientID
                Dim obj = cmd.ExecuteScalar()
                Dim strName As String = DebtPlus.Utils.Nulls.v_String(obj)

                If Not String.IsNullOrEmpty(strName) Then
                    XrLabel_ClientName.Text = strName
                End If
            End Using
        End Sub

        ''' <summary>
        ''' Read the header information for the current appointment data
        ''' </summary>
        Private Sub ReadAppointmentData(cn As System.Data.SqlClient.SqlConnection, clientID As Int32, culture As System.Globalization.CultureInfo)

            Dim counsleorID As System.Nullable(Of System.Int32) = Nothing
            Dim startDate As System.Nullable(Of System.DateTime) = Nothing
            Dim partnerID As System.String = Nothing

            Using cmd As New System.Data.SqlClient.SqlCommand()
                cmd.Connection = cn
                cmd.CommandText = "SELECT TOP 1 ca.counselor, ca.start_time, ca.partner FROM client_appointments ca WITH (NOLOCK) WHERE ca.client = @client AND ltrim(rtrim(isnull(ca.partner,''))) <> '' AND ca.status in ('K','W') AND ca.office IS NOT NULL ORDER BY ca.start_time DESC"
                cmd.Parameters.Add("@client", System.Data.SqlDbType.Int).Value = clientID
                cmd.CommandTimeout = 0
                cmd.CommandType = System.Data.CommandType.Text

                Using rd As System.Data.SqlClient.SqlDataReader = cmd.ExecuteReader(System.Data.CommandBehavior.SingleRow)
                    If rd.Read Then
                        If Not rd.IsDBNull(0) Then counsleorID = rd.GetInt32(0)
                        If Not rd.IsDBNull(1) Then startDate = rd.GetDateTime(1)
                        If Not rd.IsDBNull(2) Then partnerID = rd.GetString(2)
                    End If
                End Using
            End Using

            ' If there is a counselor then format it
            If counsleorID.GetValueOrDefault(0) > 0 Then
                For Each co As DebtPlus.LINQ.counselor In DebtPlus.LINQ.Cache.counselor.getList()
                    If co.Id = counsleorID.Value Then
                        If co.Name IsNot Nothing Then
                            XrLabel_CounselorName.Text = co.Name.ToString()
                        End If
                        Exit For
                    End If
                Next
            End If

            ' If there is a start date then format it
            If startDate.HasValue Then
                XrLabel_Date.Text = startDate.Value.ToString(culture.DateTimeFormat.ShortDatePattern, culture)
            End If

            ' If there is a partner then use it
            If Not String.IsNullOrWhiteSpace(partnerID) Then
                XrLabel_Id.Text = partnerID
            End If
        End Sub
    End Class
End Namespace
