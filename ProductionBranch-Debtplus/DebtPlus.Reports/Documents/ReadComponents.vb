﻿Namespace Documents
    Public Module Utils

        ''' <summary>
        ''' List of document components that represent the report
        ''' </summary>
        Public Function ReadComponents(ByVal documentId As Int32, ByVal clientId As Int32) As System.Collections.Generic.List(Of Document_Components)
            Dim collection As New System.Collections.Generic.List(Of Document_Components)
            Dim sqlInfo As DebtPlus.LINQ.SQLInfoClass = DebtPlus.LINQ.SQLInfoClass.getDefault()
            Dim lastSequence As Int32 = 100000

            Using cn As New SqlClient.SqlConnection(sqlInfo.ConnectionString)
                cn.Open()
                Using cmd As New SqlClient.SqlCommand()
                    cmd.Connection = cn
                    cmd.CommandText = "rpt_Documents"
                    cmd.CommandType = CommandType.StoredProcedure

                    SqlClient.SqlCommandBuilder.DeriveParameters(cmd)
                    cmd.CommandTimeout = 0
                    cmd.Parameters(1).Value = documentId

                    ' If there is a client then include it
                    If clientId >= 0 Then
                        cmd.Parameters(2).Value = clientId
                    End If

                    Using rd As System.Data.SqlClient.SqlDataReader = cmd.ExecuteReader(System.Data.CommandBehavior.CloseConnection Or System.Data.CommandBehavior.SingleResult)
                        Do While rd.Read
                            lastSequence = lastSequence + 1
                            Dim Item As New Document_Components() With
                                {
                                    .ClientId = clientId,
                                    .SequenceID = lastSequence,
                                    .ConnectionString = sqlInfo.ConnectionString
                                }
                            collection.Add(Item)

                            For colID As Int32 = 0 To rd.FieldCount - 1
                                Dim value As Object = rd.GetValue(colID)
                                If value IsNot Nothing AndAlso value IsNot System.DBNull.Value Then
                                    Dim colName As String = rd.GetName(colID).ToLower()
                                    Select Case colName
                                        Case "client"
                                            Item.ClientId = Convert.ToInt32(value)
                                        Case "sequenceid"
                                            Item.SequenceID = Convert.ToInt32(value)
                                        Case "componentlocationuri"
                                            Item.ComponentLocationURI = Convert.ToString(value)
                                        Case "componenttype"
                                            Item.ComponentType = Convert.ToString(value)
                                        Case "documentid"
                                            Item.DocumentID = Convert.ToInt32(value)
                                        Case "effectiveexpiredate"
                                            Item.EffectiveExpireDate = Convert.ToDateTime(value)
                                        Case "effectivestartdate"
                                            Item.EffectiveStartDate = Convert.ToDateTime(value)
                                        Case "pageejectbeforeflg"
                                            Item.PageEjectBeforeFLG = Convert.ToBoolean(value)
                                        Case "sectionid"
                                            Item.SectionID = Convert.ToString(value)
                                    End Select
                                End If
                            Next
                        Loop
                    End Using
                End Using
            End Using

            ' Return the collection
            Return collection
        End Function
    End Module
End Namespace