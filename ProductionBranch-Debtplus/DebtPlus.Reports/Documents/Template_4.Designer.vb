﻿Namespace Documents
    <Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
    Partial Class Template_4

        'XtraReport overrides dispose to clean up the component list.
        <System.Diagnostics.DebuggerNonUserCode()> _
        Protected Overrides Sub Dispose(ByVal disposing As Boolean)
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
            MyBase.Dispose(disposing)
        End Sub

        'Required by the Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Designer
        'It can be modified using the Designer.  
        'Do not modify it using the code editor.
        <System.Diagnostics.DebuggerStepThrough()> _
        Private Sub InitializeComponent()
            Me.XrLabel_subTitle = New DevExpress.XtraReports.UI.XRLabel()
            CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
            '
            'XrLine_reportHeaderLine
            '
            Me.XrLine_reportHeaderLine.LocationFloat = New DevExpress.Utils.PointFloat(10.00001!, 143.91!)
            '
            'ReportHeader
            '
            Me.ReportHeader.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel_subTitle})
            Me.ReportHeader.HeightF = 160.5833!
            Me.ReportHeader.Controls.SetChildIndex(Me.XrLabel_reportHeaderBox1, 0)
            Me.ReportHeader.Controls.SetChildIndex(Me.XrLabel_reportHeaderBox2, 0)
            Me.ReportHeader.Controls.SetChildIndex(Me.XrLabel_reportHeaderBox3, 0)
            Me.ReportHeader.Controls.SetChildIndex(Me.XrLine_reportHeaderLine, 0)
            Me.ReportHeader.Controls.SetChildIndex(Me.XrLabel_ReportTitle, 0)
            Me.ReportHeader.Controls.SetChildIndex(Me.XrLabel_subTitle, 0)
            '
            'XrLabel_subTitle
            '
            Me.XrLabel_subTitle.Font = New System.Drawing.Font("Calibri", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.XrLabel_subTitle.ForeColor = System.Drawing.Color.SlateGray
            Me.XrLabel_subTitle.LocationFloat = New DevExpress.Utils.PointFloat(10.00001!, 112.8333!)
            Me.XrLabel_subTitle.Name = "XrLabel_subTitle"
            Me.XrLabel_subTitle.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_subTitle.SizeF = New System.Drawing.SizeF(779.9999!, 23.0!)
            Me.XrLabel_subTitle.StyleName = "XrControlStyle_HeaderSubTitle"
            Me.XrLabel_subTitle.StylePriority.UseFont = False
            Me.XrLabel_subTitle.StylePriority.UseForeColor = False
            '
            'Template_4
            '
            Me.Bands.AddRange(New DevExpress.XtraReports.UI.Band() {Me.TopMarginBand1, Me.Detail, Me.BottomMarginBand1, Me.ReportHeader, Me.PageHeader})
            Me.Version = "11.2"
            CType(Me, System.ComponentModel.ISupportInitialize).EndInit()

        End Sub
        Protected XrLabel_subTitle As DevExpress.XtraReports.UI.XRLabel
    End Class
End Namespace