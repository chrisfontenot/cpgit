Imports DebtPlus.Interfaces.Client
Imports DebtPlus.Utils

Namespace Documents
    Public Class Template_1
        Inherits Template.BaseXtraReportClass

        Public Sub New()
            MyBase.New()
            InitializeComponent()
            AddHandler Me.BeforePrint, AddressOf CommonReport_BeforePrint
        End Sub

        Const Language As String = "en-US"

        Public Shared Function FindDocument(ByVal DocumentName As String) As String
            Dim PathName As String = DebtPlus.Configuration.Config.ReportsDirectory()
            If PathName <> String.Empty Then

                ' Try the packets folder in the files first
                Dim TestPath As String = System.IO.Path.Combine(PathName, "ClientPackets", Language, "DMP")
                PathName = System.IO.Path.Combine(TestPath, DocumentName)
                If System.IO.File.Exists(PathName) Then
                    Return PathName
                End If

                ' Try it without the language
                TestPath = System.IO.Path.Combine(PathName, "ClientPackets", "DMP")
                PathName = System.IO.Path.Combine(TestPath, DocumentName)
                If System.IO.File.Exists(PathName) Then
                    Return PathName
                End If

                ' Try it without the DMP modifier but a language
                TestPath = System.IO.Path.Combine(PathName, "ClientPackets", Language)
                PathName = System.IO.Path.Combine(TestPath, DocumentName)
                If System.IO.File.Exists(PathName) Then
                    Return PathName
                End If

                ' Try it without any modifiers
                TestPath = System.IO.Path.Combine(PathName, "ClientPackets")
                PathName = System.IO.Path.Combine(TestPath, DocumentName)
                If System.IO.File.Exists(PathName) Then
                    Return PathName
                End If
            End If

            ' Give up and return an empty string
            Return String.Empty
        End Function

        Public ReadOnly Property Client As Int32
            Get
                If Not DesignMode Then
                    Dim rpt As IClient = TryCast(MasterReport, IClient)
                    If rpt IsNot Nothing Then
                        Return rpt.ClientId
                    End If
                End If

                Return 0
            End Get
        End Property

        Public Property TitleString As String = String.Empty
        Public Property TitleColor As System.Drawing.Color = Color.FromArgb(51, 214, 153)

        Public Shared Function ToNormalCase(ByVal inputString As String) As String
            Dim sb As New System.Text.StringBuilder(inputString)

            ' Replace the characters with the proper case
            Dim shouldBeUpper As Boolean = True
            For indxChr As Int32 = 0 To sb.Length - 1
                Dim chrValue As Char = sb.Chars(indxChr)
                If System.Char.IsDigit(chrValue) Then
                    shouldBeUpper = False
                ElseIf System.Char.IsLetter(chrValue) Then
                    If shouldBeUpper Then
                        If System.Char.IsLower(chrValue) Then sb.Chars(indxChr) = System.Char.ToUpperInvariant(chrValue)
                    Else
                        If Not System.Char.IsLower(chrValue) Then sb.Chars(indxChr) = System.Char.ToLowerInvariant(chrValue)
                    End If
                    shouldBeUpper = False
                Else
                    shouldBeUpper = True
                End If
            Next

            Return sb.ToString()
        End Function

        Private Sub CommonReport_BeforePrint(sender As Object, e As System.Drawing.Printing.PrintEventArgs)
        End Sub
    End Class
End Namespace
