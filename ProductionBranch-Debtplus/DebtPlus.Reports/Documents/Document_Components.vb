
Imports DebtPlus.Interfaces.Client

Namespace Documents
    Partial Public Class Document_Components

        Public Sub New()
        End Sub

        Public Property oID As Int32
        Public Property DocumentID As Int32
        Public Property SequenceID As Int32
        Public Property SectionID As String
        Public Property EffectiveStartDate As DateTime
        Public Property EffectiveExpireDate As DateTime
        Public Property PageEjectBeforeFLG As Boolean = True
        Public Property ComponentType As String
        Public Property ComponentLocationURI As String

        Public ReadOnly Property HasFiename As Boolean
            Get
                If New String() {"RPT", "RTF"}.Contains(ComponentType.ToUpper()) Then
                    Return True
                End If
                Return False
            End Get
        End Property

    End Class

    Partial Public Class Document_Components
        Implements DebtPlus.Interfaces.Client.IClient

        Public Property ConnectionString As String
            Get
                Return DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString
            End Get
            Set(value As String)
                ' just ignore - can't make this prop a readonly either (yet)
            End Set
        End Property

        Private privateClientId As Int32 = -1
        Public Property ClientId As Int32 Implements IClient.ClientId
            Get
                Return privateClientId
            End Get
            Set(value As Int32)
                privateClientId = value
            End Set
        End Property

        Protected Friend ReadOnly Property SectionPriority As Int32
            Get
                Select Case SectionID
                    Case "RPTHEAD"
                        Return 1
                    Case "DETAIL"
                        Return 2
                    Case "RPTFOOT"
                        Return 3
                    Case Else
                        Return 0
                End Select
            End Get
        End Property

        Public Sub New(ByVal ComponentType As String, ByVal ComponentLocationURI As String)
            MyClass.New("DETAIL", ComponentType, ComponentLocationURI)
        End Sub

        Public Sub New(ByVal SectionID As String, ByVal ComponentType As String, ByVal ComponentLocationURI As String)
            Me.SectionID = SectionID
            Me.ComponentType = ComponentType
            Me.ComponentLocationURI = ComponentLocationURI
        End Sub
    End Class
End Namespace
