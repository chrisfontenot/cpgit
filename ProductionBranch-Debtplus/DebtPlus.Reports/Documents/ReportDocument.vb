Imports DebtPlus.Interfaces.Client
Imports DebtPlus.Utils

Namespace Documents
    Public Class ReportDocument
        Inherits Template.BaseXtraReportClass
        Implements DebtPlus.Interfaces.Client.IClient

        Public Property ClientId As Int32 Implements DebtPlus.Interfaces.Client.IClient.ClientId
            Get
                Return Client
            End Get
            Set(value As Int32)
                Client = value
            End Set
        End Property

        Public Property Client As Int32
            Get
                Dim parm As DevExpress.XtraReports.Parameters.Parameter = FindParameter("ParameterClient")
                If parm Is Nothing Then Return -1
                Return Convert.ToInt32(parm.Value)
            End Get
            Set(value As Int32)
                SetParameter("ParameterClient", GetType(Int32), value, "Client ID", False)
                SetReportClient(Me, value, Language)
            End Set
        End Property

        Public Property Language As Int32
            Get
                Dim parm As DevExpress.XtraReports.Parameters.Parameter = FindParameter("ParameterLanguage")
                If parm Is Nothing Then Return 0
                Return Convert.ToInt32(parm.Value)
            End Get
            Set(value As Int32)
                SetParameter("ParameterLanguage", GetType(Int32), value, "Language ID", False)
                SetReportClient(Me, Client, value)
            End Set
        End Property

        Public Sub New()
            MyBase.New()
            InitializeComponent()
            AddHandler XrTableCell_PageNumber.PrintOnPage, AddressOf XrLabel_PageNumber_PrintOnPage
        End Sub

        Public Sub New(reportDefinition As String)
            MyClass.New()

            ' Set the script references to the proper locations
            ScriptReferences = DebtPlus.Utils.AssemblyTools.GetAllSiblingAssemblies()

            ' If there is a query string then recurse to load that report
            Dim documentKey As String = String.Empty
            Dim fields() As String = reportDefinition.Split("&")
            Dim id As Int32 = 0

            For Each fieldString As String In fields
                Dim keys() As String = fieldString.Split("=")

                ' Look for the string that says "language=####"
                If String.Compare(keys(0), "language", True) = 0 Then
                    documentKey = keys(1)

                    ' Translate the report ID string to a numerical value
                    If Not String.IsNullOrWhiteSpace(documentKey) AndAlso Int32.TryParse(documentKey, id) Then
                        Language = id
                    End If
                    Continue For
                End If

                ' Look for the string that says "id=####"
                If String.Compare(keys(0), "id", True) = 0 Then
                    documentKey = keys(1)

                    ' Translate the report ID string to a numerical value
                    If Not String.IsNullOrWhiteSpace(documentKey) AndAlso Int32.TryParse(documentKey, id) Then
                        LoadChildControls(Me, ReadComponents(id, -1), 0.0, 800.0)
                        Exit For
                    End If
                End If
            Next
        End Sub

#Region "Initialize Components"
        Private Sub InitializeComponent()
            Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(ReportDocument))
            Me.PageHeader = New DevExpress.XtraReports.UI.PageHeaderBand()
            Me.PageFooter = New DevExpress.XtraReports.UI.PageFooterBand()
            Me.XrTable1 = New DevExpress.XtraReports.UI.XRTable()
            Me.XrTableRow1 = New DevExpress.XtraReports.UI.XRTableRow()
            Me.XrTableCell_PageNumber = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableCell2 = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableCell3 = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableCell4 = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrPictureBox1 = New DevExpress.XtraReports.UI.XRPictureBox()
            Me.ReportHeader = New DevExpress.XtraReports.UI.ReportHeaderBand()
            Me.ReportFooter = New DevExpress.XtraReports.UI.ReportFooterBand()
            Me.ParameterClient = New DevExpress.XtraReports.Parameters.Parameter()
            Me.ParameterLanguage = New DevExpress.XtraReports.Parameters.Parameter()
            CType(Me.XrTable1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
            '
            'Detail
            '
            Me.Detail.HeightF = 0.0!
            '
            'BottomMarginBand1
            '
            Me.BottomMarginBand1.HeightF = 25.41666!
            '
            'PageHeader
            '
            Me.PageHeader.HeightF = 0.0!
            Me.PageHeader.Name = "PageHeader"
            Me.PageHeader.PrintOn = CType((DevExpress.XtraReports.UI.PrintOnPages.NotWithReportHeader Or DevExpress.XtraReports.UI.PrintOnPages.NotWithReportFooter), DevExpress.XtraReports.UI.PrintOnPages)
            '
            'PageFooter
            '
            Me.PageFooter.BorderColor = System.Drawing.Color.Teal
            Me.PageFooter.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrTable1, Me.XrPictureBox1})
            Me.PageFooter.Font = New System.Drawing.Font("Calibri", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.PageFooter.ForeColor = System.Drawing.Color.Teal
            Me.PageFooter.HeightF = 54.0!
            Me.PageFooter.Name = "PageFooter"
            Me.PageFooter.PrintOn = CType((DevExpress.XtraReports.UI.PrintOnPages.NotWithReportHeader Or DevExpress.XtraReports.UI.PrintOnPages.NotWithReportFooter), DevExpress.XtraReports.UI.PrintOnPages)
            Me.PageFooter.StylePriority.UseBorderColor = False
            Me.PageFooter.StylePriority.UseFont = False
            Me.PageFooter.StylePriority.UseForeColor = False
            '
            'XrTable1
            '
            Me.XrTable1.Font = New System.Drawing.Font("Calibri", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.XrTable1.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 23.49997!)
            Me.XrTable1.Name = "XrTable1"
            Me.XrTable1.Rows.AddRange(New DevExpress.XtraReports.UI.XRTableRow() {Me.XrTableRow1})
            Me.XrTable1.SizeF = New System.Drawing.SizeF(435.4167!, 25.0!)
            Me.XrTable1.StylePriority.UseFont = False
            Me.XrTable1.StylePriority.UseTextAlignment = False
            Me.XrTable1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            '
            'XrTableRow1
            '
            Me.XrTableRow1.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.XrTableCell_PageNumber, Me.XrTableCell2, Me.XrTableCell3, Me.XrTableCell4})
            Me.XrTableRow1.Name = "XrTableRow1"
            Me.XrTableRow1.Weight = 1.0R
            '
            'XrTableCell_PageNumber
            '
            Me.XrTableCell_PageNumber.Borders = DevExpress.XtraPrinting.BorderSide.Right
            Me.XrTableCell_PageNumber.BorderWidth = 2
            Me.XrTableCell_PageNumber.Name = "XrTableCell_PageNumber"
            Me.XrTableCell_PageNumber.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 10, 0, 0, 100.0!)
            Me.XrTableCell_PageNumber.StylePriority.UseBorders = False
            Me.XrTableCell_PageNumber.StylePriority.UseBorderWidth = False
            Me.XrTableCell_PageNumber.StylePriority.UsePadding = False
            Me.XrTableCell_PageNumber.Text = "1"
            Me.XrTableCell_PageNumber.Weight = 0.37499999999999994R
            '
            'XrTableCell2
            '
            Me.XrTableCell2.Borders = DevExpress.XtraPrinting.BorderSide.Right
            Me.XrTableCell2.BorderWidth = 2
            Me.XrTableCell2.Name = "XrTableCell2"
            Me.XrTableCell2.Padding = New DevExpress.XtraPrinting.PaddingInfo(10, 10, 0, 0, 100.0!)
            Me.XrTableCell2.StylePriority.UseBorders = False
            Me.XrTableCell2.StylePriority.UseBorderWidth = False
            Me.XrTableCell2.StylePriority.UsePadding = False
            Me.XrTableCell2.StylePriority.UseTextAlignment = False
            Me.XrTableCell2.Text = "Clearpoint.org"
            Me.XrTableCell2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
            Me.XrTableCell2.Weight = 0.96875R
            '
            'XrTableCell3
            '
            Me.XrTableCell3.Borders = DevExpress.XtraPrinting.BorderSide.Right
            Me.XrTableCell3.BorderWidth = 2
            Me.XrTableCell3.Name = "XrTableCell3"
            Me.XrTableCell3.Padding = New DevExpress.XtraPrinting.PaddingInfo(10, 10, 0, 0, 100.0!)
            Me.XrTableCell3.StylePriority.UseBorders = False
            Me.XrTableCell3.StylePriority.UseBorderWidth = False
            Me.XrTableCell3.StylePriority.UsePadding = False
            Me.XrTableCell3.StylePriority.UseTextAlignment = False
            Me.XrTableCell3.Text = "1-877-877-1995"
            Me.XrTableCell3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
            Me.XrTableCell3.Weight = 1.0937500000000002R
            '
            'XrTableCell4
            '
            Me.XrTableCell4.Name = "XrTableCell4"
            Me.XrTableCell4.Padding = New DevExpress.XtraPrinting.PaddingInfo(10, 2, 0, 0, 100.0!)
            Me.XrTableCell4.StylePriority.UsePadding = False
            Me.XrTableCell4.Text = "Client #[Parameters.ParameterClient!0000000]"
            Me.XrTableCell4.Weight = 1.9166665649414061R
            '
            'XrPictureBox1
            '
            Me.XrPictureBox1.Image = CType(resources.GetObject("XrPictureBox1.Image"), System.Drawing.Image)
            Me.XrPictureBox1.LocationFloat = New DevExpress.Utils.PointFloat(616.0!, 18.99997!)
            Me.XrPictureBox1.Name = "XrPictureBox1"
            Me.XrPictureBox1.SizeF = New System.Drawing.SizeF(174.0!, 34.0!)
            Me.XrPictureBox1.Sizing = DevExpress.XtraPrinting.ImageSizeMode.ZoomImage
            '
            'ReportHeader
            '
            Me.ReportHeader.HeightF = 0.0!
            Me.ReportHeader.Name = "ReportHeader"
            Me.ReportHeader.PageBreak = DevExpress.XtraReports.UI.PageBreak.AfterBand
            '
            'ReportFooter
            '
            Me.ReportFooter.HeightF = 0.0!
            Me.ReportFooter.Name = "ReportFooter"
            Me.ReportFooter.PageBreak = DevExpress.XtraReports.UI.PageBreak.BeforeBand
            '
            'ParameterClient
            '
            Me.ParameterClient.Name = "ParameterClient"
            Me.ParameterClient.Type = GetType(Integer)
            Me.ParameterClient.Value = -1
            Me.ParameterClient.Visible = False
            '
            'ParameterLanguage
            '
            Me.ParameterLanguage.Description = "Language ID"
            Me.ParameterLanguage.Name = "ParameterLanguage"
            Me.ParameterLanguage.Type = GetType(Integer)
            Me.ParameterLanguage.Visible = False
            '
            'ReportDocument
            '
            Me.Bands.AddRange(New DevExpress.XtraReports.UI.Band() {Me.TopMarginBand1, Me.Detail, Me.BottomMarginBand1, Me.PageHeader, Me.PageFooter, Me.ReportHeader, Me.ReportFooter})
            Me.Font = New System.Drawing.Font("Calibri", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.Parameters.AddRange(New DevExpress.XtraReports.Parameters.Parameter() {Me.ParameterClient, Me.ParameterLanguage})
            Me.RequestParameters = False
            Me.ScriptLanguage = DevExpress.XtraReports.ScriptLanguage.VisualBasic
            Me.Version = "11.2"
            Me.Controls.SetChildIndex(Me.ReportFooter, 0)
            Me.Controls.SetChildIndex(Me.ReportHeader, 0)
            Me.Controls.SetChildIndex(Me.PageFooter, 0)
            Me.Controls.SetChildIndex(Me.PageHeader, 0)
            Me.Controls.SetChildIndex(Me.BottomMarginBand1, 0)
            Me.Controls.SetChildIndex(Me.Detail, 0)
            Me.Controls.SetChildIndex(Me.TopMarginBand1, 0)
            CType(Me.XrTable1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me, System.ComponentModel.ISupportInitialize).EndInit()

        End Sub

        Protected PageFooter As DevExpress.XtraReports.UI.PageFooterBand
        Protected ReportHeader As DevExpress.XtraReports.UI.ReportHeaderBand
        Protected ReportFooter As DevExpress.XtraReports.UI.ReportFooterBand
        Protected ParameterClient As DevExpress.XtraReports.Parameters.Parameter
        Protected XrPictureBox1 As DevExpress.XtraReports.UI.XRPictureBox
        Protected ParameterLanguage As DevExpress.XtraReports.Parameters.Parameter
        Friend WithEvents XrTable1 As DevExpress.XtraReports.UI.XRTable
        Friend WithEvents XrTableRow1 As DevExpress.XtraReports.UI.XRTableRow
        Friend WithEvents XrTableCell_PageNumber As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents XrTableCell2 As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents XrTableCell3 As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents XrTableCell4 As DevExpress.XtraReports.UI.XRTableCell
        Protected PageHeader As DevExpress.XtraReports.UI.PageHeaderBand
#End Region

        ''' <summary>
        ''' Get the control from the system. This may be any valid report control.
        ''' </summary>
        Public Function GetReportControl(nameBase As String, docComponent As Document_Components, ByVal width As Single, ByVal Height As Single) As DevExpress.XtraReports.UI.XRControl

            ' Generate the URI from the input string
            Dim FileName As String = String.Empty
            Dim queryString As String = String.Empty

            ' Split the filename into the name and query extension.
            If Not String.IsNullOrWhiteSpace(docComponent.ComponentLocationURI) Then
                Dim iPtr As Int32 = docComponent.ComponentLocationURI.IndexOf(";"c)
                If iPtr > 0 Then
                    FileName = docComponent.ComponentLocationURI.Substring(0, iPtr)
                    queryString = docComponent.ComponentLocationURI.Substring(iPtr + 1)
                Else
                    If docComponent.HasFiename Then
                        FileName = docComponent.ComponentLocationURI
                    Else
                        queryString = docComponent.ComponentLocationURI
                    End If
                End If
            End If

            ' Try to map the filename to a suitable location
            If docComponent.HasFiename Then
                Try
                    Dim u As New Uri(FileName)
                    If u.IsFile Then
                        FileName = u.LocalPath
                    End If
                    FileName = MapFileName(FileName)
                Catch ex As Exception
                End Try
            End If

            ' Determine which component to load
            Select Case docComponent.ComponentType
                Case "SPC"
                    For Each fld As String In queryString.Split(";"c)
                        Dim key() As String = fld.Split("="c)
                        If key.GetUpperBound(0) >= 0 Then
                            Select Case key(0).ToLower.Trim()
                                Case "height"
                                    Dim v As Int32
                                    If Int32.TryParse(key(1).Trim(), v) AndAlso v > 0 Then
                                        Height = v
                                    End If

                                Case "width"
                                    Dim v As Int32
                                    If Int32.TryParse(key(1).Trim(), v) AndAlso v > 0 Then
                                        width = v
                                    End If

                                Case Else
                            End Select
                        End If
                    Next

                    ' The spacer is just a blank label. Define it.
                    Return New DevExpress.XtraReports.UI.XRLabel() With _
                        {
                            .Name = nameBase,
                            .Text = String.Empty,
                            .Tag = Me,
                            .Borders = DevExpress.XtraPrinting.BorderSide.None,
                            .SizeF = New System.Drawing.Size(width, Height),
                            .HeightF = Height
                        }

                Case "PNL"
                    ' Handle a panel to group the controls together so that they move to a new page as a whole entry.
                    Dim pnl As New DevExpress.XtraReports.UI.XRPanel() With _
                        {
                            .Name = nameBase,
                            .Borders = DevExpress.XtraPrinting.BorderSide.None,
                            .CanGrow = True,
                            .CanShrink = False,
                            .KeepTogether = True,
                            .Tag = Me,
                            .Visible = True,
                            .Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 100.0),
                            .WidthF = width,
                            .HeightF = 0.0
                         }

                    ' If there is a query string then recurse to load that report
                    If queryString <> String.Empty Then
                        For Each FieldString As String In queryString.Split(";")
                            Dim Keys() As String = FieldString.Split("=")

                            ' Look for the special tag of "ID=####" to define the report contents
                            If String.Compare(Keys(0).Trim(), "ID", True) = 0 AndAlso Keys.GetUpperBound(0) = 1 Then
                                Dim IntID As Int32
                                If Int32.TryParse(Keys(1).Trim(), IntID) AndAlso IntID > 0 Then
                                    LoadChildControls(pnl, ReadComponents(IntID, ClientId), Left, width)
                                End If
                                Exit For
                            End If
                        Next
                    End If

                    ' Correct the size and return the panel to the caller
                    pnl.SizeF = New System.Drawing.SizeF(pnl.WidthF, pnl.HeightF)
                    Return pnl

                Case "RTF"
                    Dim ctl As New DevExpress.XtraReports.UI.XRRichText() With _
                        {
                            .Name = nameBase,
                            .Tag = Me,
                            .Borders = DevExpress.XtraPrinting.BorderSide.None,
                            .SizeF = New System.Drawing.Size(width, Height),
                            .HeightF = Height
                        }

                    ' Load the document text for the appropriate type
                    If FileName <> String.Empty Then
                        Dim strText As String = ReadFileContents(FileName)
                        If Not String.IsNullOrEmpty(strText) Then

                            If DebtPlus.Utils.Format.Strings.IsRTF(strText) Then

                                ' Look for RTF text
                                Try
                                    ctl.Rtf = strText
                                    Return ctl
                                Catch ex As Exception
                                End Try

                            ElseIf DebtPlus.Utils.Format.Strings.IsHTML(strText) Then

                                ' Look for HTML text
                                Try
                                    ctl.Html = strText
                                    Return ctl
                                Catch ex As Exception
                                End Try
                            End If

                            ' Finally, accept it as strictly text
                            ctl.Text = strText
                        End If
                    End If
                    Return ctl

                Case "RPT"
                    Dim rpt As DevExpress.XtraReports.UI.XtraReport = LoadReport(FileName)
                    If rpt Is Nothing Then
                        Return Nothing
                    End If

                    ' Pass the query string to the sub-report if needed
                    Dim parm As DevExpress.XtraReports.Parameters.Parameter = rpt.Parameters("ParameterQuery")
                    If parm IsNot Nothing Then
                        parm.Value = queryString
                    End If

                    ' Define the script DLL reference strings.
                    rpt.ScriptReferences = DebtPlus.Utils.AssemblyTools.GetAllSiblingAssemblies()

                    ' Define the common parameters for the report
                    Dim ctl As New DevExpress.XtraReports.UI.XRSubreport() With _
                        {
                            .Name = nameBase,
                            .ReportSource = rpt,
                            .Tag = Me,
                            .Borders = DevExpress.XtraPrinting.BorderSide.None,
                            .SizeF = New System.Drawing.Size(width, Height),
                            .Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 100.0),
                            .HeightF = Height
                        }

                    ' Process the report parameters
                    SetReportClient(rpt, Client, Language)

                    ' If there is a query string then recurse to load that report
                    If queryString <> String.Empty Then

                        For Each FieldString As String In queryString.Split(";")
                            Dim keys() As String = FieldString.Split("=")

                            ' Look for the special tag of "ID=####" to define the report contents
                            If String.Compare(keys(0).Trim(), "ID", True) = 0 AndAlso keys.GetUpperBound(0) = 1 Then
                                Dim IntID As Int32
                                If Int32.TryParse(keys(1).Trim(), IntID) AndAlso IntID > 0 Then
                                    LoadChildControls(rpt, ReadComponents(IntID, ClientId), Left, width)
                                End If
                                Exit For
                            End If
                        Next
                    End If

                    ' Return the report control to the caller
                    Return ctl

                Case Else
                    Throw New ArgumentException("Invalid ComponentType")
            End Select

            Return Nothing
        End Function

        ''' <summary>
        ''' Define the control when it is based upon an entry in the documents table
        ''' </summary>
        Public Sub LoadChildControls(ByRef ctlChild As DevExpress.XtraReports.UI.XRControl, ByVal DocumentList As System.Collections.Generic.List(Of Document_Components), left As Double, width As Double)
            If ctlChild Is Nothing Then
                Return
            End If

            ' Hook into the ISupportInitialize to tell the control that we are initializing it.
            Dim initialize As System.ComponentModel.ISupportInitialize = TryCast(ctlChild, System.ComponentModel.ISupportInitialize)
            If initialize IsNot Nothing Then
                initialize.BeginInit()
            End If

            Try
                If TypeOf ctlChild Is DevExpress.XtraReports.UI.XtraReport Then
                    LoadReportControls(DirectCast(ctlChild, DevExpress.XtraReports.UI.XtraReport), DocumentList, left, width)
                    Return
                End If

                If TypeOf ctlChild Is DevExpress.XtraReports.UI.XRPanel Then
                    LoadPanelControls(DirectCast(ctlChild, DevExpress.XtraReports.UI.XRPanel), DocumentList, left, width)
                    Return
                End If

            Finally
                If initialize IsNot Nothing Then
                    initialize.EndInit()
                End If
            End Try
        End Sub

        ''' <summary>
        ''' Load the components for a sub-report definition
        ''' </summary>
        Public Sub LoadReportControls(ByRef rpt As DevExpress.XtraReports.UI.XtraReport, ByVal DocumentList As System.Collections.Generic.List(Of Document_Components), left As Double, width As Double)

            ' Used to generate names.
            Dim itemCounter As Int32 = 0

            ' Construct the report from the pieces
            Dim ie As IEnumerator = DocumentList.OrderBy(Function(s) s.SectionID).ThenBy(Function(s) s.SectionPriority).ThenBy(Function(s) s.SequenceID).GetEnumerator()
            Do While ie.MoveNext
                Dim item As Document_Components = DirectCast(ie.Current, Document_Components)

                ' Find the appropriate band to use for the report
                Dim band As DevExpress.XtraReports.UI.Band = Nothing
                Select Case item.SectionID
                    Case "RPTHEAD"
                        band = rpt.Bands(DevExpress.XtraReports.UI.BandKind.ReportHeader)
                    Case "RPTFOOT"
                        band = rpt.Bands(DevExpress.XtraReports.UI.BandKind.ReportFooter)
                    Case Else
                        band = rpt.Bands(DevExpress.XtraReports.UI.BandKind.Detail)
                End Select

                ' We need the band to do anything now.
                If band Is Nothing Then
                    Continue Do
                End If

                ' Add a break control if needed
                itemCounter += 1
                Dim ctl As DevExpress.XtraReports.UI.XRControl = GetReportControl(String.Format("{0}_{1:f0}", rpt.Name, itemCounter), item, width, 23.0!)
                If ctl IsNot Nothing Then
                    If band.Controls.Count > 0 AndAlso item.PageEjectBeforeFLG Then
                        Dim ejectCtl As New DevExpress.XtraReports.UI.XRPageBreak
                        With ejectCtl
                            ejectCtl.LocationFloat = New DevExpress.Utils.PointFloat(left, band.HeightF)
                            band.HeightF += 2.0!
                            itemCounter += 1
                            ejectCtl.Name = String.Format("{0}_{1:f0}", rpt.Name, itemCounter)
                        End With
                        band.Controls.Add(ejectCtl)
                    End If

                    ' Add the proper control
                    ctl.LocationF = New DevExpress.Utils.PointFloat(left, band.HeightF)
                    ctl.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 100.0!)
                    band.HeightF += ctl.HeightF
                    band.Controls.Add(ctl)
                End If
            Loop
        End Sub

        ''' <summary>
        ''' Load the components for a panel definition
        ''' </summary>
        Public Sub LoadPanelControls(ByRef pnl As DevExpress.XtraReports.UI.XRPanel, ByVal DocumentList As System.Collections.Generic.List(Of Document_Components), left As Double, width As Double)

            ' A panel is required
            If pnl Is Nothing Then
                Return
            End If

            ' Used to generate names.
            Dim itemCounter As Int32 = 0

            ' Indent the panel controls by one pixel all around
            left = left + 1.0
            width = width - 2.0
            pnl.HeightF = pnl.HeightF + 2.0

            ' Construct the report from the pieces
            Dim ie As IEnumerator = DocumentList.OrderBy(Function(s) s.SectionID).ThenBy(Function(s) s.SectionPriority).ThenBy(Function(s) s.SequenceID).GetEnumerator()
            Do While ie.MoveNext()
                Dim item As Document_Components = DirectCast(ie.Current, Document_Components)

                itemCounter += 1
                Dim ctl As DevExpress.XtraReports.UI.XRControl = GetReportControl(String.Format("{0}_{1:f0}", pnl.Name, itemCounter), item, width, 23.0!)
                ctl.LocationF = New DevExpress.Utils.PointFloat(left, pnl.HeightF - 3.0)
                ctl.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 100.0!)
                pnl.HeightF += ctl.HeightF
                pnl.Controls.Add(ctl)
            Loop
        End Sub

        ''' <summary>
        ''' Read the contents of a disk file
        ''' </summary>
        Private Function ReadFileContents(ByVal Fname As String) As String
            Try
                Using fs As New System.IO.StreamReader(Fname, System.Text.Encoding.UTF7)
                    Return fs.ReadToEnd()
                End Using

            Catch ex As System.IO.IOException
            End Try

            Return String.Empty
        End Function

        ''' <summary>
        ''' Translate a filename for the references of documents, reports, letters, etc.
        ''' </summary>
        Private Function MapFileName(ByVal InputFileName As String) As String
            If InputFileName <> String.Empty Then
                If InputFileName.ToLower().StartsWith("/[reports]/") Then
                    Return System.IO.Path.Combine(DebtPlus.Configuration.Config.ReportsDirectory, InputFileName.Substring(11))
                End If

                If InputFileName.ToLower().StartsWith("/[letters]/") Then
                    Return System.IO.Path.Combine(DebtPlus.Configuration.Config.LettersDirectory, InputFileName.Substring(11))
                End If

                If InputFileName.ToLower().StartsWith("/[documents]/") Then
                    Return System.IO.Path.Combine(System.Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments), InputFileName.Substring(13))
                End If
            End If

            Return InputFileName
        End Function

        ''' <summary>
        ''' Define the settings for a client and language items where possible.
        ''' </summary>
        Private Sub SetReportClient(rpt As DevExpress.XtraReports.UI.XtraReport, ByVal Client As Int32, ByVal Language As Int32)

            ' Ignore empty report references. They are possible for sub-reports that are undefined.
            If rpt Is Nothing Then
                Return
            End If

            ' Process all of the parameters for this report
            For Each parm As DevExpress.XtraReports.Parameters.Parameter In rpt.Parameters
                If String.Compare(parm.Name, "ParameterClient", True) = 0 Then
                    parm.Value = Client
                ElseIf String.Compare(parm.Name, "ParameterLanguage", True) = 0 Then
                    parm.Value = Language
                End If
            Next

            ' Process each of the bands for the controls in them
            For Each Band As DevExpress.XtraReports.UI.Band In rpt.Bands
                For Each ctl As DevExpress.XtraReports.UI.XRControl In Band.Controls

                    ' Process sub-report controls here looking for the corresponding report-source
                    Dim subRpt As DevExpress.XtraReports.UI.XRSubreport = TryCast(ctl, DevExpress.XtraReports.UI.XRSubreport)

                    ' If the control is a sub-report then get the report source and process that report source as well.
                    If subRpt IsNot Nothing Then
                        SetReportClient(subRpt.ReportSource, Client, Language)
                    End If
                Next
            Next
        End Sub

        ''' <summary>
        ''' Load the contents of the report repx file
        ''' </summary>
        Private Function LoadReport(ByVal FileName As String) As DevExpress.XtraReports.UI.XtraReport

            Try
                ' If the entry is not pre-loaded then read the report definition.
                If (Not DebtPlus.Configuration.Config.LocalReportsOnly) AndAlso System.IO.File.Exists(FileName) Then
                    Dim rpt As New DebtPlus.Reports.Template.BaseXtraReportClass()
                    rpt.LoadLayout(FileName)
                    Return rpt
                End If

            Catch ex As System.IO.IOException
            End Try

            ' Find the class name.
            Dim className As String = System.IO.Path.GetFileNameWithoutExtension(FileName)
            Dim rootNamespace As String = "DebtPlus.Reports.Documents"
            className = rootNamespace + "." + className

            ' If there is a valid type then create the instance of the report and return it.
            Dim t As System.Type = DebtPlus.Utils.Modules.FindTypeFromName(className)
            If t IsNot Nothing Then
                Dim constructorInfoObj As System.Reflection.ConstructorInfo = t.GetConstructor(New Type() {})
                If constructorInfoObj IsNot Nothing Then
                    Return DirectCast(constructorInfoObj.Invoke(Nothing), DevExpress.XtraReports.UI.XtraReport)
                End If
            End If

            ' The report can not be loaded
            Return Nothing
        End Function

        Dim startingPageNumber As Int32 = Int32.MaxValue

        ''' <summary>
        ''' Handle the formatting of the page number. Do not count the pages in the header section.
        ''' </summary>
        Private Sub XrLabel_PageNumber_PrintOnPage(sender As Object, e As DevExpress.XtraReports.UI.PrintOnPageEventArgs)
            If e.PageCount < 1 Then
                XrTableCell_PageNumber.Text = String.Empty
                Return
            End If

            ' Save the first page number that we are asked to format. It is the base page.
            If startingPageNumber = Int32.MaxValue Then
                startingPageNumber = e.PageIndex
            End If

            ' The formatted page number ignores the pages before the base page number
            XrTableCell_PageNumber.Text = (e.PageIndex - startingPageNumber + 1).ToString()
        End Sub
    End Class
End Namespace
