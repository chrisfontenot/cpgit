#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}

#End Region

Imports DevExpress.XtraReports.UI

Namespace Operations.Production
    Friend Class rpt_ProductionReport_NonAR
        Public Sub PassParameters(ByVal tbl As DataTable)

            ' Bind the datasource
            DataSource = tbl

            ' Bind the parameters to the report
            XrLabel_credit.DataBindings.Add(New XRBinding("Text", tbl, "credit_amt", currency_format))
            XrLabel_debit.DataBindings.Add(New XRBinding("Text", tbl, "debit_amt", currency_format))
            XrLabel_total_credit.DataBindings.Add(New XRBinding("Text", tbl, "credit_amt", currency_format))
            XrLabel_total_debit.DataBindings.Add(New XRBinding("Text", tbl, "debit_amt", currency_format))
            XrLabel_description.DataBindings.Add(New XRBinding("Text", tbl, "description", ""))
            XrLabel_ledger_code.DataBindings.Add(New XRBinding("Text", tbl, "ledger_code", ""))
        End Sub
    End Class
End Namespace