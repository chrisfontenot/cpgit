#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}

#End Region

Option Compare Binary
Option Explicit On
Option Strict On

Imports System.ComponentModel
Imports DebtPlus.Reports.Template

Imports System.Drawing.Printing
Imports System.Data.SqlClient

Namespace Operations.Production
    Public Class ProductionReportClass
        Inherits DatedTemplateXtraReportClass

        Private ds As New DataSet("ds")
        Private tbl As DataTable
        Private WithEvents ProductionReport_ClientCount As New rpt_ProductionReport_ClientCount
        Private WithEvents ProductionReport_LeftReport As New rpt_ProductionReport_LeftReport
        Private WithEvents ProductionReport_NonAR As New rpt_ProductionReport_NonAR
        Private WithEvents ProductionReport_apt_byStatus As New rpt_ProductionReport_apt_byStatus

        ''' <summary>
        ''' Create an instance of our report
        ''' </summary>
        Public Sub New()
            MyBase.New()
            InitializeComponent()
            AddHandler ProductionReport_ClientCount.BeforePrint, AddressOf ProductionReport_ClientCount_BeforePrint
            AddHandler ProductionReport_NonAR.BeforePrint, AddressOf ProductionReport_NonAR_BeforePrint
            AddHandler ProductionReport_LeftReport.BeforePrint, AddressOf ProductionReport_LeftReport_BeforePrint
            AddHandler Me.BeforePrint, AddressOf ProductionReportClass_BeforePrint
            AddHandler ProductionReport_apt_byStatus.BeforePrint, AddressOf ProductionReport_apt_byStatus_BeforePrint
        End Sub

#Region " Component Designer generated code "

        'Component overrides dispose to clean up the component list.
        Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
            If disposing Then
                If components IsNot Nothing Then
                    components.Dispose()
                End If
            End If
            MyBase.Dispose(disposing)
        End Sub

        Friend WithEvents XrSubreport4 As DevExpress.XtraReports.UI.XRSubreport
        Friend WithEvents XrSubreport1 As DevExpress.XtraReports.UI.XRSubreport
        Friend WithEvents XrSubreport3 As DevExpress.XtraReports.UI.XRSubreport
        Friend WithEvents XrSubreport2 As DevExpress.XtraReports.UI.XRSubreport
        Friend WithEvents XrLabel_deduction_earned As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel10 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_total_disbursed_amount As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel8 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_disbursed_client_percentage As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel6 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_total_active_clients As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel4 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_total_disbursed_clients As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel2 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel1 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_total_setup_fees As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_total_monthly_fee_received As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_total_other_fee_amounts As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_invoiced_fairshare_received As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_deductions_received As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_total_receipts As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_receipts_to_disbursements As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel23 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel15 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel14 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel25 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel29 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel26 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel18 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel19 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_invoice_adjustments As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel16 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_total_invoiced As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel12 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_invoices_generated As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel9 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_billed_amount As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel5 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel3 As DevExpress.XtraReports.UI.XRLabel

        'Required by the Component Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Component Designer
        'It can be modified using the Component Designer.
        'Do not modify it using the code editor.
        <System.Diagnostics.DebuggerStepThrough()>
        Private Sub InitializeComponent()
            Me.XrSubreport2 = New DevExpress.XtraReports.UI.XRSubreport
            Me.XrSubreport3 = New DevExpress.XtraReports.UI.XRSubreport
            Me.XrLabel19 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel18 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel26 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel29 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel25 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel14 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel15 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel23 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel1 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel2 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel4 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel6 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel8 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel10 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel3 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel5 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel9 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel12 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel16 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_total_active_clients = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_disbursed_client_percentage = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_total_disbursed_amount = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_deduction_earned = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_billed_amount = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_invoices_generated = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_total_invoiced = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_invoice_adjustments = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_receipts_to_disbursements = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_total_receipts = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_deductions_received = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_invoiced_fairshare_received = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_total_other_fee_amounts = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_total_monthly_fee_received = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_total_setup_fees = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_total_disbursed_clients = New DevExpress.XtraReports.UI.XRLabel
            Me.XrSubreport1 = New DevExpress.XtraReports.UI.XRSubreport
            Me.XrSubreport4 = New DevExpress.XtraReports.UI.XRSubreport
            CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
            '
            'Detail
            '
            Me.Detail.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrSubreport4, Me.XrSubreport1, Me.XrLabel_invoice_adjustments, Me.XrLabel16, Me.XrLabel_total_invoiced, Me.XrLabel12, Me.XrLabel_invoices_generated, Me.XrLabel9, Me.XrLabel_billed_amount, Me.XrLabel5, Me.XrLabel3, Me.XrLabel_deduction_earned, Me.XrLabel10, Me.XrLabel_total_disbursed_amount, Me.XrLabel8, Me.XrLabel_disbursed_client_percentage, Me.XrLabel6, Me.XrLabel_total_active_clients, Me.XrLabel4, Me.XrLabel_total_disbursed_clients, Me.XrLabel2, Me.XrLabel1, Me.XrLabel_total_setup_fees, Me.XrLabel_total_monthly_fee_received, Me.XrLabel_total_other_fee_amounts, Me.XrLabel_invoiced_fairshare_received, Me.XrLabel_deductions_received, Me.XrLabel_total_receipts, Me.XrLabel_receipts_to_disbursements, Me.XrLabel23, Me.XrLabel15, Me.XrLabel14, Me.XrLabel25, Me.XrLabel29, Me.XrLabel26, Me.XrLabel18, Me.XrLabel19, Me.XrSubreport2, Me.XrSubreport3})
            Me.Detail.Height = 336
            '
            'PageHeader
            '
            Me.PageHeader.Height = 134
            '
            'XrSubreport2
            '
            Me.XrSubreport2.Location = New System.Drawing.Point(0, 0)
            Me.XrSubreport2.Name = "XrSubreport2"
            Me.XrSubreport2.Size = New System.Drawing.Size(392, 25)
            '
            'XrSubreport3
            '
            Me.XrSubreport3.Location = New System.Drawing.Point(400, 0)
            Me.XrSubreport3.Name = "XrSubreport3"
            Me.XrSubreport3.Size = New System.Drawing.Size(400, 25)
            '
            'XrLabel19
            '
            Me.XrLabel19.Font = New System.Drawing.Font("Times New Roman", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.XrLabel19.ForeColor = System.Drawing.Color.Maroon
            Me.XrLabel19.Location = New System.Drawing.Point(0, 33)
            Me.XrLabel19.Name = "XrLabel19"
            Me.XrLabel19.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel19.Size = New System.Drawing.Size(300, 25)
            Me.XrLabel19.Text = "DMP Income Information"
            '
            'XrLabel18
            '
            Me.XrLabel18.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.XrLabel18.Location = New System.Drawing.Point(0, 67)
            Me.XrLabel18.Name = "XrLabel18"
            Me.XrLabel18.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel18.Size = New System.Drawing.Size(242, 16)
            Me.XrLabel18.Text = "Total Setup Fees"
            '
            'XrLabel26
            '
            Me.XrLabel26.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.XrLabel26.Location = New System.Drawing.Point(0, 83)
            Me.XrLabel26.Name = "XrLabel26"
            Me.XrLabel26.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel26.Size = New System.Drawing.Size(242, 16)
            Me.XrLabel26.Text = "Total Monthly Fee Received"
            '
            'XrLabel29
            '
            Me.XrLabel29.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.XrLabel29.Location = New System.Drawing.Point(0, 100)
            Me.XrLabel29.Name = "XrLabel29"
            Me.XrLabel29.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel29.Size = New System.Drawing.Size(242, 16)
            Me.XrLabel29.Text = "Other Fee Amounts"
            '
            'XrLabel25
            '
            Me.XrLabel25.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.XrLabel25.Location = New System.Drawing.Point(0, 117)
            Me.XrLabel25.Name = "XrLabel25"
            Me.XrLabel25.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel25.Size = New System.Drawing.Size(242, 16)
            Me.XrLabel25.Text = "Deductions Received"
            '
            'XrLabel14
            '
            Me.XrLabel14.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.XrLabel14.Location = New System.Drawing.Point(0, 133)
            Me.XrLabel14.Name = "XrLabel14"
            Me.XrLabel14.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel14.Size = New System.Drawing.Size(242, 16)
            Me.XrLabel14.Text = "Invoiced Fairshare Received"
            '
            'XrLabel15
            '
            Me.XrLabel15.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.XrLabel15.Location = New System.Drawing.Point(0, 167)
            Me.XrLabel15.Name = "XrLabel15"
            Me.XrLabel15.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel15.Size = New System.Drawing.Size(242, 16)
            Me.XrLabel15.Text = "TOTAL RECEIPTS"
            '
            'XrLabel23
            '
            Me.XrLabel23.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.XrLabel23.Location = New System.Drawing.Point(0, 200)
            Me.XrLabel23.Name = "XrLabel23"
            Me.XrLabel23.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel23.Size = New System.Drawing.Size(242, 16)
            Me.XrLabel23.Text = "Total Receipts to Disbursements"
            '
            'XrLabel1
            '
            Me.XrLabel1.Font = New System.Drawing.Font("Times New Roman", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.XrLabel1.ForeColor = System.Drawing.Color.Maroon
            Me.XrLabel1.Location = New System.Drawing.Point(400, 33)
            Me.XrLabel1.Name = "XrLabel1"
            Me.XrLabel1.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel1.Size = New System.Drawing.Size(300, 25)
            Me.XrLabel1.Text = "Disbursement Information"
            '
            'XrLabel2
            '
            Me.XrLabel2.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.XrLabel2.Location = New System.Drawing.Point(400, 67)
            Me.XrLabel2.Name = "XrLabel2"
            Me.XrLabel2.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel2.Size = New System.Drawing.Size(242, 16)
            Me.XrLabel2.Text = "Disbursed Clients:"
            '
            'XrLabel4
            '
            Me.XrLabel4.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.XrLabel4.Location = New System.Drawing.Point(400, 83)
            Me.XrLabel4.Name = "XrLabel4"
            Me.XrLabel4.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel4.Size = New System.Drawing.Size(242, 16)
            Me.XrLabel4.Text = "Current Active/Restart Clients:"
            '
            'XrLabel6
            '
            Me.XrLabel6.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.XrLabel6.Location = New System.Drawing.Point(400, 100)
            Me.XrLabel6.Name = "XrLabel6"
            Me.XrLabel6.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel6.Size = New System.Drawing.Size(242, 16)
            Me.XrLabel6.Text = "Disbursed client percentage"
            '
            'XrLabel8
            '
            Me.XrLabel8.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.XrLabel8.Location = New System.Drawing.Point(400, 117)
            Me.XrLabel8.Name = "XrLabel8"
            Me.XrLabel8.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel8.Size = New System.Drawing.Size(242, 16)
            Me.XrLabel8.Text = "Total Creditor Disbursements:"
            '
            'XrLabel10
            '
            Me.XrLabel10.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.XrLabel10.Location = New System.Drawing.Point(400, 133)
            Me.XrLabel10.Name = "XrLabel10"
            Me.XrLabel10.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel10.Size = New System.Drawing.Size(242, 16)
            Me.XrLabel10.Text = "Total Deduction Earned"
            '
            'XrLabel3
            '
            Me.XrLabel3.Font = New System.Drawing.Font("Times New Roman", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.XrLabel3.ForeColor = System.Drawing.Color.Maroon
            Me.XrLabel3.Location = New System.Drawing.Point(400, 167)
            Me.XrLabel3.Name = "XrLabel3"
            Me.XrLabel3.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel3.Size = New System.Drawing.Size(300, 25)
            Me.XrLabel3.Text = "Invoice Information"
            '
            'XrLabel5
            '
            Me.XrLabel5.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.XrLabel5.Location = New System.Drawing.Point(400, 200)
            Me.XrLabel5.Name = "XrLabel5"
            Me.XrLabel5.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel5.Size = New System.Drawing.Size(242, 16)
            Me.XrLabel5.Text = "Disbursements w/Billed Fairshare:"
            '
            'XrLabel9
            '
            Me.XrLabel9.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.XrLabel9.Location = New System.Drawing.Point(400, 217)
            Me.XrLabel9.Name = "XrLabel9"
            Me.XrLabel9.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel9.Size = New System.Drawing.Size(242, 16)
            Me.XrLabel9.Text = "Invoices Generated:"
            '
            'XrLabel12
            '
            Me.XrLabel12.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.XrLabel12.Location = New System.Drawing.Point(400, 233)
            Me.XrLabel12.Name = "XrLabel12"
            Me.XrLabel12.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel12.Size = New System.Drawing.Size(242, 16)
            Me.XrLabel12.Text = "Outstanding Invoices:"
            '
            'XrLabel16
            '
            Me.XrLabel16.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.XrLabel16.Location = New System.Drawing.Point(400, 250)
            Me.XrLabel16.Name = "XrLabel16"
            Me.XrLabel16.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel16.Size = New System.Drawing.Size(242, 16)
            Me.XrLabel16.Text = "Adjustments to Invoices:"
            '
            'XrLabel_total_active_clients
            '
            Me.XrLabel_total_active_clients.Location = New System.Drawing.Point(650, 83)
            Me.XrLabel_total_active_clients.Name = "XrLabel_total_active_clients"
            Me.XrLabel_total_active_clients.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_total_active_clients.Size = New System.Drawing.Size(141, 15)
            Me.XrLabel_total_active_clients.Text = "0"
            Me.XrLabel_total_active_clients.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrLabel_total_active_clients.WordWrap = False
            '
            'XrLabel_disbursed_client_percentage
            '
            Me.XrLabel_disbursed_client_percentage.Location = New System.Drawing.Point(650, 100)
            Me.XrLabel_disbursed_client_percentage.Name = "XrLabel_disbursed_client_percentage"
            Me.XrLabel_disbursed_client_percentage.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_disbursed_client_percentage.Size = New System.Drawing.Size(141, 15)
            Me.XrLabel_disbursed_client_percentage.Text = "0"
            Me.XrLabel_disbursed_client_percentage.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrLabel_disbursed_client_percentage.WordWrap = False
            '
            'XrLabel_total_disbursed_amount
            '
            Me.XrLabel_total_disbursed_amount.Location = New System.Drawing.Point(650, 117)
            Me.XrLabel_total_disbursed_amount.Name = "XrLabel_total_disbursed_amount"
            Me.XrLabel_total_disbursed_amount.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_total_disbursed_amount.Size = New System.Drawing.Size(141, 15)
            Me.XrLabel_total_disbursed_amount.Text = "0"
            Me.XrLabel_total_disbursed_amount.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrLabel_total_disbursed_amount.WordWrap = False
            '
            'XrLabel_deduction_earned
            '
            Me.XrLabel_deduction_earned.Location = New System.Drawing.Point(650, 133)
            Me.XrLabel_deduction_earned.Name = "XrLabel_deduction_earned"
            Me.XrLabel_deduction_earned.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_deduction_earned.Size = New System.Drawing.Size(141, 15)
            Me.XrLabel_deduction_earned.Text = "0"
            Me.XrLabel_deduction_earned.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrLabel_deduction_earned.WordWrap = False
            '
            'XrLabel_billed_amount
            '
            Me.XrLabel_billed_amount.Location = New System.Drawing.Point(650, 200)
            Me.XrLabel_billed_amount.Name = "XrLabel_billed_amount"
            Me.XrLabel_billed_amount.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_billed_amount.Size = New System.Drawing.Size(141, 15)
            Me.XrLabel_billed_amount.Text = "0"
            Me.XrLabel_billed_amount.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrLabel_billed_amount.WordWrap = False
            '
            'XrLabel_invoices_generated
            '
            Me.XrLabel_invoices_generated.Location = New System.Drawing.Point(650, 217)
            Me.XrLabel_invoices_generated.Name = "XrLabel_invoices_generated"
            Me.XrLabel_invoices_generated.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_invoices_generated.Size = New System.Drawing.Size(141, 15)
            Me.XrLabel_invoices_generated.Text = "0"
            Me.XrLabel_invoices_generated.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrLabel_invoices_generated.WordWrap = False
            '
            'XrLabel_total_invoiced
            '
            Me.XrLabel_total_invoiced.Location = New System.Drawing.Point(650, 233)
            Me.XrLabel_total_invoiced.Name = "XrLabel_total_invoiced"
            Me.XrLabel_total_invoiced.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_total_invoiced.Size = New System.Drawing.Size(141, 15)
            Me.XrLabel_total_invoiced.Text = "0"
            Me.XrLabel_total_invoiced.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrLabel_total_invoiced.WordWrap = False
            '
            'XrLabel_invoice_adjustments
            '
            Me.XrLabel_invoice_adjustments.Location = New System.Drawing.Point(650, 250)
            Me.XrLabel_invoice_adjustments.Name = "XrLabel_invoice_adjustments"
            Me.XrLabel_invoice_adjustments.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_invoice_adjustments.Size = New System.Drawing.Size(141, 15)
            Me.XrLabel_invoice_adjustments.Text = "0"
            Me.XrLabel_invoice_adjustments.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrLabel_invoice_adjustments.WordWrap = False
            '
            'XrLabel_receipts_to_disbursements
            '
            Me.XrLabel_receipts_to_disbursements.Location = New System.Drawing.Point(250, 200)
            Me.XrLabel_receipts_to_disbursements.Name = "XrLabel_receipts_to_disbursements"
            Me.XrLabel_receipts_to_disbursements.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_receipts_to_disbursements.Size = New System.Drawing.Size(141, 15)
            Me.XrLabel_receipts_to_disbursements.Text = "0"
            Me.XrLabel_receipts_to_disbursements.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrLabel_receipts_to_disbursements.WordWrap = False
            '
            'XrLabel_total_receipts
            '
            Me.XrLabel_total_receipts.Location = New System.Drawing.Point(250, 167)
            Me.XrLabel_total_receipts.Name = "XrLabel_total_receipts"
            Me.XrLabel_total_receipts.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_total_receipts.Size = New System.Drawing.Size(141, 15)
            Me.XrLabel_total_receipts.Text = "0"
            Me.XrLabel_total_receipts.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrLabel_total_receipts.WordWrap = False
            '
            'XrLabel_deductions_received
            '
            Me.XrLabel_deductions_received.Location = New System.Drawing.Point(250, 117)
            Me.XrLabel_deductions_received.Name = "XrLabel_deductions_received"
            Me.XrLabel_deductions_received.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_deductions_received.Size = New System.Drawing.Size(141, 15)
            Me.XrLabel_deductions_received.Text = "0"
            Me.XrLabel_deductions_received.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrLabel_deductions_received.WordWrap = False
            '
            'XrLabel_invoiced_fairshare_received
            '
            Me.XrLabel_invoiced_fairshare_received.Location = New System.Drawing.Point(250, 133)
            Me.XrLabel_invoiced_fairshare_received.Name = "XrLabel_invoiced_fairshare_received"
            Me.XrLabel_invoiced_fairshare_received.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_invoiced_fairshare_received.Size = New System.Drawing.Size(141, 15)
            Me.XrLabel_invoiced_fairshare_received.Text = "0"
            Me.XrLabel_invoiced_fairshare_received.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrLabel_invoiced_fairshare_received.WordWrap = False
            '
            'XrLabel_total_other_fee_amounts
            '
            Me.XrLabel_total_other_fee_amounts.Location = New System.Drawing.Point(250, 100)
            Me.XrLabel_total_other_fee_amounts.Name = "XrLabel_total_other_fee_amounts"
            Me.XrLabel_total_other_fee_amounts.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_total_other_fee_amounts.Size = New System.Drawing.Size(141, 15)
            Me.XrLabel_total_other_fee_amounts.Text = "0"
            Me.XrLabel_total_other_fee_amounts.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrLabel_total_other_fee_amounts.WordWrap = False
            '
            'XrLabel_total_monthly_fee_received
            '
            Me.XrLabel_total_monthly_fee_received.Location = New System.Drawing.Point(250, 83)
            Me.XrLabel_total_monthly_fee_received.Name = "XrLabel_total_monthly_fee_received"
            Me.XrLabel_total_monthly_fee_received.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_total_monthly_fee_received.Size = New System.Drawing.Size(141, 15)
            Me.XrLabel_total_monthly_fee_received.Text = "0"
            Me.XrLabel_total_monthly_fee_received.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrLabel_total_monthly_fee_received.WordWrap = False
            '
            'XrLabel_total_setup_fees
            '
            Me.XrLabel_total_setup_fees.Location = New System.Drawing.Point(250, 67)
            Me.XrLabel_total_setup_fees.Name = "XrLabel_total_setup_fees"
            Me.XrLabel_total_setup_fees.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_total_setup_fees.Size = New System.Drawing.Size(141, 15)
            Me.XrLabel_total_setup_fees.Text = "0"
            Me.XrLabel_total_setup_fees.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrLabel_total_setup_fees.WordWrap = False
            '
            'XrLabel_total_disbursed_clients
            '
            Me.XrLabel_total_disbursed_clients.Location = New System.Drawing.Point(650, 67)
            Me.XrLabel_total_disbursed_clients.Name = "XrLabel_total_disbursed_clients"
            Me.XrLabel_total_disbursed_clients.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_total_disbursed_clients.Size = New System.Drawing.Size(141, 15)
            Me.XrLabel_total_disbursed_clients.Text = "0"
            Me.XrLabel_total_disbursed_clients.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrLabel_total_disbursed_clients.WordWrap = False
            '
            'XrSubreport1
            '
            Me.XrSubreport1.CanShrink = True
            Me.XrSubreport1.Location = New System.Drawing.Point(75, 275)
            Me.XrSubreport1.Name = "XrSubreport1"
            Me.XrSubreport1.Size = New System.Drawing.Size(650, 25)
            '
            'XrSubreport4
            '
            Me.XrSubreport4.CanShrink = True
            Me.XrSubreport4.Location = New System.Drawing.Point(25, 308)
            Me.XrSubreport4.Name = "XrSubreport4"
            Me.XrSubreport4.Size = New System.Drawing.Size(750, 25)
            '
            'ProductionReportClass
            '
            Me.Version = "8.1"
            CType(Me, System.ComponentModel.ISupportInitialize).EndInit()
        End Sub

#End Region

        Public Overrides ReadOnly Property ReportTitle() As String
            Get
                Return "Production Report"
            End Get
        End Property

        Private Sub ProductionReport_ClientCount_BeforePrint(ByVal sender As Object, ByVal e As PrintEventArgs)
            ProductionReport_ClientCount.PassParameters(Parameter_FromDate, Parameter_ToDate)
        End Sub

        Private Sub ProductionReport_NonAR_BeforePrint(ByVal sender As Object, ByVal e As PrintEventArgs)
            Static ds As New DataSet("ds")

            Dim tbl As DataTable = ds.Tables("non_ar")
            If tbl Is Nothing Then
                Dim cmd As SqlCommand = New System.Data.SqlClient.SqlCommand()
                With cmd
                    .Connection = New System.Data.SqlClient.SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
                    .CommandText = "rpt_ProductionReport_By_NonAR"
                    .CommandType = CommandType.StoredProcedure
                    .Parameters.Add("@fromdate", SqlDbType.DateTime).Value = Parameter_FromDate
                    .Parameters.Add("@todate", SqlDbType.DateTime).Value = Parameter_ToDate
                    .CommandTimeout = 0
                End With

                Dim da As New SqlDataAdapter(cmd)
                da.Fill(ds, "non_ar")
                tbl = ds.Tables("non_ar")
            End If

            ' IF there is nothing to print then do not print even the headings
            If tbl.Rows.Count = 0 Then
                e.Cancel = True
            Else
                ProductionReport_NonAR.PassParameters(tbl)
            End If
        End Sub

        Private Sub ProductionReport_LeftReport_BeforePrint(ByVal sender As Object, ByVal e As PrintEventArgs)
            ProductionReport_LeftReport.PassParameters(ds)
        End Sub

        Private Sub ProductionReportClass_BeforePrint(ByVal sender As Object, ByVal e As PrintEventArgs)
            Dim SelectString As String = "rpt_Production_Report"
            Dim cn As SqlConnection = New System.Data.SqlClient.SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
            Dim cmd As SqlCommand = New System.Data.SqlClient.SqlCommand()
            With cmd
                .Connection = cn
                .CommandText = SelectString
                .CommandType = CommandType.StoredProcedure
                .CommandTimeout = 0
                .Parameters.Add(New SqlParameter("@fromdate", SqlDbType.VarChar, 80)).Value = Parameter_FromDate.ToShortDateString & " 00:00:00"
                .Parameters.Add(New SqlParameter("@todate", SqlDbType.VarChar, 80)).Value = Parameter_ToDate.ToShortDateString & " 23:59:59"
            End With

            Dim da As New SqlDataAdapter(cmd)
            da.Fill(ds, "rpt_Production_Report")

            ' Set the report definitions
            XrSubreport1.ReportSource = ProductionReport_NonAR
            XrSubreport2.ReportSource = ProductionReport_LeftReport
            XrSubreport3.ReportSource = ProductionReport_ClientCount
            XrSubreport4.ReportSource = ProductionReport_apt_byStatus

            ' Bind the current fields to the database
            bind_fields()
        End Sub

        Private Sub bind_fields()

            Dim row As DataRow = ds.Tables(0).Rows(0)

            XrLabel_total_setup_fees.Text = String.Format(currency_format, Convert.ToDecimal(row("total_setup_fees")))
            XrLabel_total_monthly_fee_received.Text = String.Format(currency_format, Convert.ToDecimal(row("total_paf_amount")))
            XrLabel_total_other_fee_amounts.Text = String.Format(currency_format, Convert.ToDecimal(row("other_fees_amount")))
            XrLabel_deductions_received.Text = String.Format(currency_format, Convert.ToDecimal(row("deduction_amount")))
            XrLabel_invoiced_fairshare_received.Text = String.Format(currency_format, Convert.ToDecimal(row("invoice_payments")))
            XrLabel_receipts_to_disbursements.Text = String.Format(percent_format, receipts_to_disbursements)
            XrLabel_total_receipts.Text = String.Format(currency_format, total_fairshare)
            XrLabel_deductions_received.Text = String.Format(currency_format, Convert.ToDecimal(row("deduction_amount")))
            XrLabel_invoiced_fairshare_received.Text = String.Format(currency_format, Convert.ToDecimal(row("invoice_payments")))
            XrLabel_total_other_fee_amounts.Text = String.Format(currency_format, Convert.ToDecimal(row("other_fees_amount")))
            XrLabel_total_monthly_fee_received.Text = String.Format(currency_format, Convert.ToDecimal(row("total_paf_amount")))
            XrLabel_total_setup_fees.Text = String.Format(currency_format, Convert.ToDecimal(row("total_setup_fees")))
            XrLabel_total_disbursed_clients.Text = String.Format(number_format, Convert.ToInt32(row("total_disbursed_clients")))
            XrLabel_total_active_clients.Text = String.Format(number_format, Convert.ToInt32(row("total_active_clients")))
            XrLabel_total_disbursed_amount.Text = String.Format(currency_format, Convert.ToDecimal(row("total_disbursed_amount")))
            XrLabel_deduction_earned.Text = String.Format(currency_format, Convert.ToDecimal(row("deduction_earned")))
            XrLabel_billed_amount.Text = String.Format(currency_format, Convert.ToDecimal(row("billed_amount")))
            XrLabel_invoices_generated.Text = String.Format(currency_format, Convert.ToDecimal(row("invoices_generated")))
            XrLabel_total_invoiced.Text = String.Format(currency_format, Convert.ToDecimal(row("total_invoiced")))
            XrLabel_invoice_adjustments.Text = String.Format(currency_format, Convert.ToDecimal(row("invoice_adjustments")))
            XrLabel_disbursed_client_percentage.Text = String.Format(percent_format, disbursed_client_percentage)
        End Sub

        Private ReadOnly Property total_fairshare() As Decimal
            Get
                Dim row As DataRow = ds.Tables(0).Rows(0)
                Return Convert.ToDecimal(row("total_setup_fees")) + Convert.ToDecimal(row("total_paf_amount")) + Convert.ToDecimal(row("other_fees_amount")) + Convert.ToDecimal(row("deduction_amount")) + Convert.ToDecimal(row("invoice_payments"))
            End Get
        End Property

        Private ReadOnly Property receipts_to_disbursements() As Double
            Get
                Dim row As DataRow = ds.Tables(0).Rows(0)
                Dim fairshare As Decimal = total_fairshare
                Dim disbursements As Decimal = Convert.ToDecimal(row("total_disbursed_amount"))

                If disbursements = 0 Then Return 0.0
                If disbursements < fairshare Then Return 1.0
                Return Convert.ToDouble(fairshare) / Convert.ToDouble(disbursements)
            End Get
        End Property

        Private ReadOnly Property disbursed_client_percentage() As Double
            Get
                Dim row As DataRow = ds.Tables(0).Rows(0)
                Dim disbursed As Int32 = Convert.ToInt32(row("total_disbursed_clients"))
                Dim active As Int32 = Convert.ToInt32(row("total_active_clients"))

                If active = 0 Then Return 0.0
                If active < disbursed Then Return 1.0
                Return disbursed / active
            End Get
        End Property

        Private Sub ProductionReport_apt_byStatus_BeforePrint(ByVal sender As Object, ByVal e As PrintEventArgs)
            Static ds As New DataSet("ds")

            Dim tbl As DataTable = ds.Tables("apt_byStatus")
            If tbl Is Nothing Then
                Dim cmd As SqlCommand = New System.Data.SqlClient.SqlCommand()
                With cmd
                    .Connection = New System.Data.SqlClient.SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
                    .CommandText = "rpt_ProductionReport_apt_byStatus"
                    .CommandType = CommandType.StoredProcedure
                    .Parameters.Add("@fromdate", SqlDbType.DateTime).Value = Parameter_FromDate
                    .Parameters.Add("@todate", SqlDbType.DateTime).Value = Parameter_ToDate
                    .CommandTimeout = 0
                End With

                Dim da As New SqlDataAdapter(cmd)
                da.Fill(ds, "apt_byStatus")
                tbl = ds.Tables("apt_byStatus")
            End If

            ' IF there is nothing to print then do not print even the headings
            If tbl.Rows.Count = 0 Then
                e.Cancel = True
            Else
                ProductionReport_apt_byStatus.PassParameters(tbl)
            End If
        End Sub
    End Class

    Friend Module formatting
        Friend Const number_format As String = "{0:n0}"
        Friend Const currency_format As String = "{0:c2}"
        Friend Const percent_format As String = "{0:P3}"
    End Module
End Namespace
