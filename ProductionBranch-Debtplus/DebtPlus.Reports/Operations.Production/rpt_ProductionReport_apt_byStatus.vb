#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}

#End Region

Imports DevExpress.XtraReports.UI

Namespace Operations.Production
    Friend Class rpt_ProductionReport_apt_byStatus
        Public Sub PassParameters(ByVal tbl As DataTable)

            ' Calculate the total column with the table
            With tbl
                Dim col As DataColumn = tbl.Columns("total")
                If col Is Nothing Then
                    col = New DataColumn("total", GetType(Int32), "[pending]+[missed]+[cancelled]+[rescheduled]+[kept]+[walkin]")
                    tbl.Columns.Add(col)
                    tbl.AcceptChanges()
                End If
            End With

            ' Bind the datasource
            DataSource = tbl

            ' Bind the detail rows
            XrLabel_description.DataBindings.Add(New XRBinding("Text", tbl, "description", ""))
            XrLabel_PE.DataBindings.Add(New XRBinding("Text", tbl, "pending", number_format))
            XrLabel_NS.DataBindings.Add(New XRBinding("Text", tbl, "missed", number_format))
            XrLabel_XC.DataBindings.Add(New XRBinding("Text", tbl, "cancelled", number_format))
            XrLabel_RS.DataBindings.Add(New XRBinding("Text", tbl, "rescheduled", number_format))
            XrLabel_CO.DataBindings.Add(New XRBinding("Text", tbl, "kept", number_format))
            XrLabel_W.DataBindings.Add(New XRBinding("Text", tbl, "walkin", number_format))
            XrLabel_Total.DataBindings.Add(New XRBinding("Text", tbl, "total", number_format))

            ' Bind the summary rows
            XrLabel_total_PE.DataBindings.Add(New XRBinding("Text", tbl, "pending", number_format))
            XrLabel_total_NS.DataBindings.Add(New XRBinding("Text", tbl, "missed", number_format))
            XrLabel_total_XC.DataBindings.Add(New XRBinding("Text", tbl, "cancelled", number_format))
            XrLabel_total_RS.DataBindings.Add(New XRBinding("Text", tbl, "rescheduled", number_format))
            XrLabel_total_CO.DataBindings.Add(New XRBinding("Text", tbl, "kept", number_format))
            XrLabel_total_W.DataBindings.Add(New XRBinding("Text", tbl, "walkin", number_format))
            XrLabel_total_total.DataBindings.Add(New XRBinding("Text", tbl, "total", number_format))
        End Sub
    End Class
End Namespace