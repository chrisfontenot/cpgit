Namespace Operations.Production
    <Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
    Partial Class rpt_ProductionReport_LeftReport
        Inherits DevExpress.XtraReports.UI.XtraReport

        'XtraReport overrides dispose to clean up the component list.
        <System.Diagnostics.DebuggerNonUserCode()> _
        Protected Overrides Sub Dispose(ByVal disposing As Boolean)
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
            MyBase.Dispose(disposing)
        End Sub

        'Required by the Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Designer
        'It can be modified using the Designer.  
        'Do not modify it using the code editor.
        <System.Diagnostics.DebuggerStepThrough()> _
        Private Sub InitializeComponent()
            Me.Detail = New DevExpress.XtraReports.UI.DetailBand
            Me.XrLabel24 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel9 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel4 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel2 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_total_first_appointments = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_total_clients_counseled = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_total_dmps_written = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_total_sa_counseling = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_total_fco_counseling = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_total_attny_referrals = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_total_same_month_returns = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_total_recounseled_clients = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_total_housing_counseling = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_total_first_deposits = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_total_successful_completions = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_total_sa_clients = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_total_dropped_clients = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_total_dmp_starts = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel20 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel16 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel21 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel17 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel12 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel11 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel10 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel8 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel7 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel5 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel6 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel3 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_total_appointments_scheduled = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel1 = New DevExpress.XtraReports.UI.XRLabel
            Me.TopMarginBand1 = New DevExpress.XtraReports.UI.TopMarginBand
            Me.BottomMarginBand1 = New DevExpress.XtraReports.UI.BottomMarginBand
            Me.XrLabel13 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel14 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_total_bk13 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_total_bk7 = New DevExpress.XtraReports.UI.XRLabel
            CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
            '
            'Detail
            '
            Me.Detail.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel_total_bk13, Me.XrLabel_total_bk7, Me.XrLabel13, Me.XrLabel14, Me.XrLabel24, Me.XrLabel9, Me.XrLabel4, Me.XrLabel2, Me.XrLabel_total_first_appointments, Me.XrLabel_total_clients_counseled, Me.XrLabel_total_dmps_written, Me.XrLabel_total_sa_counseling, Me.XrLabel_total_fco_counseling, Me.XrLabel_total_attny_referrals, Me.XrLabel_total_same_month_returns, Me.XrLabel_total_recounseled_clients, Me.XrLabel_total_housing_counseling, Me.XrLabel_total_first_deposits, Me.XrLabel_total_successful_completions, Me.XrLabel_total_sa_clients, Me.XrLabel_total_dropped_clients, Me.XrLabel_total_dmp_starts, Me.XrLabel20, Me.XrLabel16, Me.XrLabel21, Me.XrLabel17, Me.XrLabel12, Me.XrLabel11, Me.XrLabel10, Me.XrLabel8, Me.XrLabel7, Me.XrLabel5, Me.XrLabel6, Me.XrLabel3, Me.XrLabel_total_appointments_scheduled, Me.XrLabel1})
            Me.Detail.HeightF = 362.5417!
            Me.Detail.Name = "Detail"
            Me.Detail.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
            Me.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
            '
            'XrLabel24
            '
            Me.XrLabel24.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.XrLabel24.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 330.5417!)
            Me.XrLabel24.Name = "XrLabel24"
            Me.XrLabel24.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel24.SizeF = New System.Drawing.SizeF(242.0!, 16.0!)
            Me.XrLabel24.Text = "Total Dropped Clients"
            '
            'XrLabel9
            '
            Me.XrLabel9.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.XrLabel9.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 177.0!)
            Me.XrLabel9.Name = "XrLabel9"
            Me.XrLabel9.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel9.SizeF = New System.Drawing.SizeF(242.0!, 16.0!)
            Me.XrLabel9.Text = "Total Same Month Returns"
            '
            'XrLabel4
            '
            Me.XrLabel4.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.XrLabel4.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 65.0!)
            Me.XrLabel4.Name = "XrLabel4"
            Me.XrLabel4.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel4.SizeF = New System.Drawing.SizeF(242.0!, 16.0!)
            Me.XrLabel4.Text = "Total Clients Counseled"
            '
            'XrLabel2
            '
            Me.XrLabel2.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.XrLabel2.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 33.0!)
            Me.XrLabel2.Name = "XrLabel2"
            Me.XrLabel2.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel2.SizeF = New System.Drawing.SizeF(242.0!, 16.0!)
            Me.XrLabel2.Text = "Total Appointments Scheduled"
            '
            'XrLabel_total_first_appointments
            '
            Me.XrLabel_total_first_appointments.LocationFloat = New DevExpress.Utils.PointFloat(250.0!, 49.0!)
            Me.XrLabel_total_first_appointments.Name = "XrLabel_total_first_appointments"
            Me.XrLabel_total_first_appointments.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_total_first_appointments.SizeF = New System.Drawing.SizeF(141.0!, 16.0!)
            Me.XrLabel_total_first_appointments.Text = "0"
            Me.XrLabel_total_first_appointments.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrLabel_total_first_appointments.WordWrap = False
            '
            'XrLabel_total_clients_counseled
            '
            Me.XrLabel_total_clients_counseled.LocationFloat = New DevExpress.Utils.PointFloat(250.0!, 65.0!)
            Me.XrLabel_total_clients_counseled.Name = "XrLabel_total_clients_counseled"
            Me.XrLabel_total_clients_counseled.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_total_clients_counseled.SizeF = New System.Drawing.SizeF(141.0!, 16.0!)
            Me.XrLabel_total_clients_counseled.Text = "0"
            Me.XrLabel_total_clients_counseled.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrLabel_total_clients_counseled.WordWrap = False
            '
            'XrLabel_total_dmps_written
            '
            Me.XrLabel_total_dmps_written.LocationFloat = New DevExpress.Utils.PointFloat(250.0!, 81.0!)
            Me.XrLabel_total_dmps_written.Name = "XrLabel_total_dmps_written"
            Me.XrLabel_total_dmps_written.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_total_dmps_written.SizeF = New System.Drawing.SizeF(141.0!, 16.0!)
            Me.XrLabel_total_dmps_written.Text = "0"
            Me.XrLabel_total_dmps_written.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrLabel_total_dmps_written.WordWrap = False
            '
            'XrLabel_total_sa_counseling
            '
            Me.XrLabel_total_sa_counseling.LocationFloat = New DevExpress.Utils.PointFloat(250.0!, 97.0!)
            Me.XrLabel_total_sa_counseling.Name = "XrLabel_total_sa_counseling"
            Me.XrLabel_total_sa_counseling.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_total_sa_counseling.SizeF = New System.Drawing.SizeF(141.0!, 16.0!)
            Me.XrLabel_total_sa_counseling.Text = "0"
            Me.XrLabel_total_sa_counseling.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrLabel_total_sa_counseling.WordWrap = False
            '
            'XrLabel_total_fco_counseling
            '
            Me.XrLabel_total_fco_counseling.LocationFloat = New DevExpress.Utils.PointFloat(250.0!, 113.0!)
            Me.XrLabel_total_fco_counseling.Name = "XrLabel_total_fco_counseling"
            Me.XrLabel_total_fco_counseling.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_total_fco_counseling.SizeF = New System.Drawing.SizeF(141.0!, 16.0!)
            Me.XrLabel_total_fco_counseling.Text = "0"
            Me.XrLabel_total_fco_counseling.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrLabel_total_fco_counseling.WordWrap = False
            '
            'XrLabel_total_attny_referrals
            '
            Me.XrLabel_total_attny_referrals.LocationFloat = New DevExpress.Utils.PointFloat(250.0!, 161.0!)
            Me.XrLabel_total_attny_referrals.Name = "XrLabel_total_attny_referrals"
            Me.XrLabel_total_attny_referrals.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_total_attny_referrals.SizeF = New System.Drawing.SizeF(141.0!, 16.0!)
            Me.XrLabel_total_attny_referrals.Text = "0"
            Me.XrLabel_total_attny_referrals.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrLabel_total_attny_referrals.WordWrap = False
            '
            'XrLabel_total_same_month_returns
            '
            Me.XrLabel_total_same_month_returns.LocationFloat = New DevExpress.Utils.PointFloat(250.0!, 177.0!)
            Me.XrLabel_total_same_month_returns.Name = "XrLabel_total_same_month_returns"
            Me.XrLabel_total_same_month_returns.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_total_same_month_returns.SizeF = New System.Drawing.SizeF(141.0!, 16.0!)
            Me.XrLabel_total_same_month_returns.Text = "0"
            Me.XrLabel_total_same_month_returns.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrLabel_total_same_month_returns.WordWrap = False
            '
            'XrLabel_total_recounseled_clients
            '
            Me.XrLabel_total_recounseled_clients.LocationFloat = New DevExpress.Utils.PointFloat(250.0!, 193.0!)
            Me.XrLabel_total_recounseled_clients.Name = "XrLabel_total_recounseled_clients"
            Me.XrLabel_total_recounseled_clients.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_total_recounseled_clients.SizeF = New System.Drawing.SizeF(141.0!, 16.0!)
            Me.XrLabel_total_recounseled_clients.Text = "0"
            Me.XrLabel_total_recounseled_clients.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrLabel_total_recounseled_clients.WordWrap = False
            '
            'XrLabel_total_housing_counseling
            '
            Me.XrLabel_total_housing_counseling.LocationFloat = New DevExpress.Utils.PointFloat(250.0!, 209.0!)
            Me.XrLabel_total_housing_counseling.Name = "XrLabel_total_housing_counseling"
            Me.XrLabel_total_housing_counseling.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_total_housing_counseling.SizeF = New System.Drawing.SizeF(141.0!, 16.0!)
            Me.XrLabel_total_housing_counseling.Text = "0"
            Me.XrLabel_total_housing_counseling.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrLabel_total_housing_counseling.WordWrap = False
            '
            'XrLabel_total_first_deposits
            '
            Me.XrLabel_total_first_deposits.LocationFloat = New DevExpress.Utils.PointFloat(250.0!, 225.0!)
            Me.XrLabel_total_first_deposits.Name = "XrLabel_total_first_deposits"
            Me.XrLabel_total_first_deposits.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_total_first_deposits.SizeF = New System.Drawing.SizeF(141.0!, 16.0!)
            Me.XrLabel_total_first_deposits.Text = "0"
            Me.XrLabel_total_first_deposits.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrLabel_total_first_deposits.WordWrap = False
            '
            'XrLabel_total_successful_completions
            '
            Me.XrLabel_total_successful_completions.LocationFloat = New DevExpress.Utils.PointFloat(250.0!, 296.5417!)
            Me.XrLabel_total_successful_completions.Name = "XrLabel_total_successful_completions"
            Me.XrLabel_total_successful_completions.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_total_successful_completions.SizeF = New System.Drawing.SizeF(141.0!, 15.0!)
            Me.XrLabel_total_successful_completions.Text = "0"
            Me.XrLabel_total_successful_completions.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrLabel_total_successful_completions.WordWrap = False
            '
            'XrLabel_total_sa_clients
            '
            Me.XrLabel_total_sa_clients.LocationFloat = New DevExpress.Utils.PointFloat(250.0!, 313.5417!)
            Me.XrLabel_total_sa_clients.Name = "XrLabel_total_sa_clients"
            Me.XrLabel_total_sa_clients.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_total_sa_clients.SizeF = New System.Drawing.SizeF(141.0!, 15.0!)
            Me.XrLabel_total_sa_clients.Text = "0"
            Me.XrLabel_total_sa_clients.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrLabel_total_sa_clients.WordWrap = False
            '
            'XrLabel_total_dropped_clients
            '
            Me.XrLabel_total_dropped_clients.LocationFloat = New DevExpress.Utils.PointFloat(250.0!, 330.5417!)
            Me.XrLabel_total_dropped_clients.Name = "XrLabel_total_dropped_clients"
            Me.XrLabel_total_dropped_clients.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_total_dropped_clients.SizeF = New System.Drawing.SizeF(141.0!, 15.0!)
            Me.XrLabel_total_dropped_clients.Text = "0"
            Me.XrLabel_total_dropped_clients.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrLabel_total_dropped_clients.WordWrap = False
            '
            'XrLabel_total_dmp_starts
            '
            Me.XrLabel_total_dmp_starts.LocationFloat = New DevExpress.Utils.PointFloat(250.0!, 346.5417!)
            Me.XrLabel_total_dmp_starts.Name = "XrLabel_total_dmp_starts"
            Me.XrLabel_total_dmp_starts.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_total_dmp_starts.SizeF = New System.Drawing.SizeF(141.0!, 15.0!)
            Me.XrLabel_total_dmp_starts.Text = "0"
            Me.XrLabel_total_dmp_starts.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrLabel_total_dmp_starts.WordWrap = False
            '
            'XrLabel20
            '
            Me.XrLabel20.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.XrLabel20.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 346.5417!)
            Me.XrLabel20.Name = "XrLabel20"
            Me.XrLabel20.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel20.SizeF = New System.Drawing.SizeF(242.0!, 16.0!)
            Me.XrLabel20.Text = "Total DMP Starts"
            '
            'XrLabel16
            '
            Me.XrLabel16.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.XrLabel16.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 296.5417!)
            Me.XrLabel16.Name = "XrLabel16"
            Me.XrLabel16.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel16.SizeF = New System.Drawing.SizeF(242.0!, 16.0!)
            Me.XrLabel16.Text = "Total Successful Completions"
            '
            'XrLabel21
            '
            Me.XrLabel21.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.XrLabel21.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 313.5417!)
            Me.XrLabel21.Name = "XrLabel21"
            Me.XrLabel21.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel21.SizeF = New System.Drawing.SizeF(242.0!, 16.0!)
            Me.XrLabel21.Text = "Total Self Adminstered Clients"
            '
            'XrLabel17
            '
            Me.XrLabel17.Font = New System.Drawing.Font("Times New Roman", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.XrLabel17.ForeColor = System.Drawing.Color.Maroon
            Me.XrLabel17.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 263.5417!)
            Me.XrLabel17.Name = "XrLabel17"
            Me.XrLabel17.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel17.SizeF = New System.Drawing.SizeF(300.0!, 25.0!)
            Me.XrLabel17.Text = "Changes to Client Counts"
            '
            'XrLabel12
            '
            Me.XrLabel12.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.XrLabel12.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 225.0!)
            Me.XrLabel12.Name = "XrLabel12"
            Me.XrLabel12.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel12.SizeF = New System.Drawing.SizeF(242.0!, 16.0!)
            Me.XrLabel12.Text = "Total First Deposits"
            '
            'XrLabel11
            '
            Me.XrLabel11.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.XrLabel11.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 209.0!)
            Me.XrLabel11.Name = "XrLabel11"
            Me.XrLabel11.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel11.SizeF = New System.Drawing.SizeF(242.0!, 16.0!)
            Me.XrLabel11.Text = "Total Housing Counselings"
            '
            'XrLabel10
            '
            Me.XrLabel10.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.XrLabel10.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 193.0!)
            Me.XrLabel10.Name = "XrLabel10"
            Me.XrLabel10.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel10.SizeF = New System.Drawing.SizeF(242.0!, 16.0!)
            Me.XrLabel10.Text = "Total Re-Counseled Clients"
            '
            'XrLabel8
            '
            Me.XrLabel8.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.XrLabel8.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 161.0!)
            Me.XrLabel8.Name = "XrLabel8"
            Me.XrLabel8.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel8.SizeF = New System.Drawing.SizeF(242.0!, 16.0!)
            Me.XrLabel8.Text = "Total Attorney Referrals"
            '
            'XrLabel7
            '
            Me.XrLabel7.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.XrLabel7.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 113.0!)
            Me.XrLabel7.Name = "XrLabel7"
            Me.XrLabel7.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel7.SizeF = New System.Drawing.SizeF(242.0!, 16.0!)
            Me.XrLabel7.Text = "Total FCO Counseling"
            '
            'XrLabel5
            '
            Me.XrLabel5.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.XrLabel5.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 81.0!)
            Me.XrLabel5.Name = "XrLabel5"
            Me.XrLabel5.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel5.SizeF = New System.Drawing.SizeF(242.0!, 16.0!)
            Me.XrLabel5.Text = "Total DMPs Written"
            '
            'XrLabel6
            '
            Me.XrLabel6.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.XrLabel6.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 97.0!)
            Me.XrLabel6.Name = "XrLabel6"
            Me.XrLabel6.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel6.SizeF = New System.Drawing.SizeF(242.0!, 16.0!)
            Me.XrLabel6.Text = "Total S/A Counseling"
            '
            'XrLabel3
            '
            Me.XrLabel3.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.XrLabel3.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 49.0!)
            Me.XrLabel3.Name = "XrLabel3"
            Me.XrLabel3.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel3.SizeF = New System.Drawing.SizeF(242.0!, 16.0!)
            Me.XrLabel3.Text = "Total First Appointments"
            '
            'XrLabel_total_appointments_scheduled
            '
            Me.XrLabel_total_appointments_scheduled.LocationFloat = New DevExpress.Utils.PointFloat(250.0!, 33.0!)
            Me.XrLabel_total_appointments_scheduled.Name = "XrLabel_total_appointments_scheduled"
            Me.XrLabel_total_appointments_scheduled.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_total_appointments_scheduled.SizeF = New System.Drawing.SizeF(141.0!, 16.0!)
            Me.XrLabel_total_appointments_scheduled.Text = "0"
            Me.XrLabel_total_appointments_scheduled.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrLabel_total_appointments_scheduled.WordWrap = False
            '
            'XrLabel1
            '
            Me.XrLabel1.Font = New System.Drawing.Font("Times New Roman", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.XrLabel1.ForeColor = System.Drawing.Color.Maroon
            Me.XrLabel1.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
            Me.XrLabel1.Name = "XrLabel1"
            Me.XrLabel1.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel1.SizeF = New System.Drawing.SizeF(300.0!, 25.0!)
            Me.XrLabel1.Text = "Appointment Information"
            '
            'TopMarginBand1
            '
            Me.TopMarginBand1.HeightF = 0.0!
            Me.TopMarginBand1.Name = "TopMarginBand1"
            '
            'BottomMarginBand1
            '
            Me.BottomMarginBand1.HeightF = 0.0!
            Me.BottomMarginBand1.Name = "BottomMarginBand1"
            '
            'XrLabel13
            '
            Me.XrLabel13.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.XrLabel13.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 129.0!)
            Me.XrLabel13.Name = "XrLabel13"
            Me.XrLabel13.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel13.SizeF = New System.Drawing.SizeF(242.0!, 16.0!)
            Me.XrLabel13.Text = "Total Bankruptcy Chapter 7"
            '
            'XrLabel14
            '
            Me.XrLabel14.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.XrLabel14.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 145.0!)
            Me.XrLabel14.Name = "XrLabel14"
            Me.XrLabel14.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel14.SizeF = New System.Drawing.SizeF(242.0!, 16.0!)
            Me.XrLabel14.Text = "Total Bankruptcy Chapter 13"
            '
            'XrLabel_total_bk13
            '
            Me.XrLabel_total_bk13.LocationFloat = New DevExpress.Utils.PointFloat(250.0!, 145.0!)
            Me.XrLabel_total_bk13.Name = "XrLabel_total_bk13"
            Me.XrLabel_total_bk13.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_total_bk13.SizeF = New System.Drawing.SizeF(141.0!, 16.0!)
            Me.XrLabel_total_bk13.Text = "0"
            Me.XrLabel_total_bk13.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrLabel_total_bk13.WordWrap = False
            '
            'XrLabel_total_bk7
            '
            Me.XrLabel_total_bk7.LocationFloat = New DevExpress.Utils.PointFloat(250.0!, 129.0!)
            Me.XrLabel_total_bk7.Name = "XrLabel_total_bk7"
            Me.XrLabel_total_bk7.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_total_bk7.SizeF = New System.Drawing.SizeF(141.0!, 16.0!)
            Me.XrLabel_total_bk7.Text = "0"
            Me.XrLabel_total_bk7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrLabel_total_bk7.WordWrap = False
            '
            'rpt_ProductionReport_LeftReport
            '
            Me.Bands.AddRange(New DevExpress.XtraReports.UI.Band() {Me.Detail, Me.TopMarginBand1, Me.BottomMarginBand1})
            Me.Margins = New System.Drawing.Printing.Margins(100, 100, 0, 0)
            Me.Version = "10.2"
            CType(Me, System.ComponentModel.ISupportInitialize).EndInit()

        End Sub
        Friend WithEvents Detail As DevExpress.XtraReports.UI.DetailBand
        Friend WithEvents XrLabel1 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel2 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_total_first_appointments As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_total_clients_counseled As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_total_dmps_written As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_total_sa_counseling As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_total_fco_counseling As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_total_attny_referrals As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_total_same_month_returns As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_total_recounseled_clients As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_total_housing_counseling As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_total_first_deposits As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_total_successful_completions As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_total_sa_clients As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_total_dropped_clients As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_total_dmp_starts As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel20 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel16 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel21 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel17 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel12 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel11 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel10 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel8 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel7 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel5 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel6 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel3 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_total_appointments_scheduled As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel4 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel9 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel24 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents TopMarginBand1 As DevExpress.XtraReports.UI.TopMarginBand
        Friend WithEvents BottomMarginBand1 As DevExpress.XtraReports.UI.BottomMarginBand
        Friend WithEvents XrLabel_total_bk13 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_total_bk7 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel13 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel14 As DevExpress.XtraReports.UI.XRLabel
    End Class
End Namespace