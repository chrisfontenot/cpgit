#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}

#End Region

Imports System.ComponentModel

Namespace Operations.Production
    Friend Class rpt_ProductionReport_LeftReport

        ''' <summary>
        ''' Create an instance of our report
        ''' </summary>
        Public Sub New(ByVal Container As IContainer)
            MyClass.New()
            Container.Add(Me)
        End Sub

        Public Sub New()
            MyBase.New()
            InitializeComponent()
        End Sub

        Public Sub PassParameters(ByVal ds As DataSet)
            bind_fields(ds.Tables(0))
        End Sub

        Private Sub bind_fields(ByRef tbl As DataTable)

            ' There is always a single row.
            Dim row As DataRow = tbl.Rows(0)

            XrLabel_total_appointments_scheduled.Text = String.Format("{0:n0}", row("total_appts_scheduled"))
            XrLabel_total_first_appointments.Text = String.Format("{0:n0}", row("total_first_appts"))
            XrLabel_total_clients_counseled.Text = String.Format("{0:n0}", row("total_counseled_counseled"))
            XrLabel_total_dmps_written.Text = String.Format("{0:n0}", row("total_dmp_written"))
            XrLabel_total_sa_counseling.Text = String.Format("{0:n0}", row("total_sa_written"))
            XrLabel_total_fco_counseling.Text = String.Format("{0:n0}", row("total_fco_written"))
            XrLabel_total_attny_referrals.Text = String.Format("{0:n0}", row("total_rla_written"))
            XrLabel_total_same_month_returns.Text = String.Format("{0:n0}", row("total_same_month_returns"))
            XrLabel_total_recounseled_clients.Text = String.Format("{0:n0}", row("total_recounsel"))
            XrLabel_total_housing_counseling.Text = String.Format("{0:n0}", row("total_hud"))
            XrLabel_total_first_deposits.Text = String.Format("{0:n0}", row("total_first_deposits"))
            XrLabel_total_successful_completions.Text = String.Format("{0:n0}", row("total_sc_clients"))
            XrLabel_total_sa_clients.Text = String.Format("{0:n0}", row("total_sa_clients"))
            XrLabel_total_dropped_clients.Text = String.Format("{0:n0}", row("total_dropped_clients"))
            XrLabel_total_dmp_starts.Text = String.Format("{0:n0}", row("total_dmp_starts"))
            XrLabel_total_bk7.Text = String.Format("{0:n0}", row("total_bk7_clients"))
            XrLabel_total_bk13.Text = String.Format("{0:n0}", row("total_bk13_clients"))
        End Sub
    End Class
End Namespace
