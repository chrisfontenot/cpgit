Namespace Operations.Production
    <Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
    Partial Class rpt_ProductionReport_apt_byStatus
        Inherits DevExpress.XtraReports.UI.XtraReport

        'XtraReport overrides dispose to clean up the component list.
        <System.Diagnostics.DebuggerNonUserCode()> _
        Protected Overrides Sub Dispose(ByVal disposing As Boolean)
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
            MyBase.Dispose(disposing)
        End Sub

        'Required by the Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Designer
        'It can be modified using the Designer.  
        'Do not modify it using the code editor.
        <System.Diagnostics.DebuggerStepThrough()> _
        Private Sub InitializeComponent()
            Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(rpt_ProductionReport_apt_byStatus))
            Dim XrSummary1 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary
            Dim XrSummary2 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary
            Dim XrSummary3 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary
            Dim XrSummary4 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary
            Dim XrSummary5 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary
            Dim XrSummary6 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary
            Dim XrSummary7 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary
            Me.Detail = New DevExpress.XtraReports.UI.DetailBand
            Me.XrLabel_Total = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_W = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_CO = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_XC = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_RS = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_NS = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_PE = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_description = New DevExpress.XtraReports.UI.XRLabel
            Me.PageHeader = New DevExpress.XtraReports.UI.PageHeaderBand
            Me.XrPanel1 = New DevExpress.XtraReports.UI.XRPanel
            Me.XrLabel8 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel9 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel7 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel6 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel1 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel4 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel3 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel2 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel17 = New DevExpress.XtraReports.UI.XRLabel
            Me.ReportFooter = New DevExpress.XtraReports.UI.ReportFooterBand
            Me.XrRichText1 = New DevExpress.XtraReports.UI.XRRichText
            Me.XrLabel_total_PE = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_total_W = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_total_CO = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_total_XC = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_total_RS = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_total_NS = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_total_total = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLine1 = New DevExpress.XtraReports.UI.XRLine
            Me.XrLabel5 = New DevExpress.XtraReports.UI.XRLabel
            Me.ReportHeader = New DevExpress.XtraReports.UI.ReportHeaderBand
            CType(Me.XrRichText1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
            '
            'Detail
            '
            Me.Detail.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel_Total, Me.XrLabel_W, Me.XrLabel_CO, Me.XrLabel_XC, Me.XrLabel_RS, Me.XrLabel_NS, Me.XrLabel_PE, Me.XrLabel_description})
            Me.Detail.Height = 16
            Me.Detail.KeepTogether = True
            Me.Detail.Name = "Detail"
            Me.Detail.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
            Me.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
            '
            'XrLabel_Total
            '
            Me.XrLabel_Total.BorderColor = System.Drawing.Color.Transparent
            Me.XrLabel_Total.CanGrow = False
            Me.XrLabel_Total.Font = New System.Drawing.Font("Times New Roman", 9.75!)
            Me.XrLabel_Total.ForeColor = System.Drawing.SystemColors.ControlText
            Me.XrLabel_Total.Location = New System.Drawing.Point(667, 0)
            Me.XrLabel_Total.Name = "XrLabel_Total"
            Me.XrLabel_Total.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_Total.Size = New System.Drawing.Size(75, 16)
            Me.XrLabel_Total.StylePriority.UseBorderColor = False
            Me.XrLabel_Total.StylePriority.UseFont = False
            Me.XrLabel_Total.StylePriority.UseForeColor = False
            Me.XrLabel_Total.StylePriority.UseTextAlignment = False
            Me.XrLabel_Total.Text = "0"
            Me.XrLabel_Total.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrLabel_Total.WordWrap = False
            '
            'XrLabel_W
            '
            Me.XrLabel_W.BorderColor = System.Drawing.Color.Transparent
            Me.XrLabel_W.CanGrow = False
            Me.XrLabel_W.Font = New System.Drawing.Font("Times New Roman", 9.75!)
            Me.XrLabel_W.ForeColor = System.Drawing.SystemColors.ControlText
            Me.XrLabel_W.Location = New System.Drawing.Point(608, 0)
            Me.XrLabel_W.Name = "XrLabel_W"
            Me.XrLabel_W.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_W.Size = New System.Drawing.Size(58, 16)
            Me.XrLabel_W.StylePriority.UseBorderColor = False
            Me.XrLabel_W.StylePriority.UseFont = False
            Me.XrLabel_W.StylePriority.UseForeColor = False
            Me.XrLabel_W.StylePriority.UseTextAlignment = False
            Me.XrLabel_W.Text = "0"
            Me.XrLabel_W.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrLabel_W.WordWrap = False
            '
            'XrLabel_CO
            '
            Me.XrLabel_CO.BorderColor = System.Drawing.Color.Transparent
            Me.XrLabel_CO.CanGrow = False
            Me.XrLabel_CO.Font = New System.Drawing.Font("Times New Roman", 9.75!)
            Me.XrLabel_CO.ForeColor = System.Drawing.SystemColors.ControlText
            Me.XrLabel_CO.Location = New System.Drawing.Point(550, 0)
            Me.XrLabel_CO.Name = "XrLabel_CO"
            Me.XrLabel_CO.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_CO.Size = New System.Drawing.Size(58, 16)
            Me.XrLabel_CO.StylePriority.UseBorderColor = False
            Me.XrLabel_CO.StylePriority.UseFont = False
            Me.XrLabel_CO.StylePriority.UseForeColor = False
            Me.XrLabel_CO.StylePriority.UseTextAlignment = False
            Me.XrLabel_CO.Text = "0"
            Me.XrLabel_CO.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrLabel_CO.WordWrap = False
            '
            'XrLabel_XC
            '
            Me.XrLabel_XC.BorderColor = System.Drawing.Color.Transparent
            Me.XrLabel_XC.CanGrow = False
            Me.XrLabel_XC.Font = New System.Drawing.Font("Times New Roman", 9.75!)
            Me.XrLabel_XC.ForeColor = System.Drawing.SystemColors.ControlText
            Me.XrLabel_XC.Location = New System.Drawing.Point(492, 0)
            Me.XrLabel_XC.Name = "XrLabel_XC"
            Me.XrLabel_XC.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_XC.Size = New System.Drawing.Size(58, 16)
            Me.XrLabel_XC.StylePriority.UseBorderColor = False
            Me.XrLabel_XC.StylePriority.UseFont = False
            Me.XrLabel_XC.StylePriority.UseForeColor = False
            Me.XrLabel_XC.StylePriority.UseTextAlignment = False
            Me.XrLabel_XC.Text = "0"
            Me.XrLabel_XC.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrLabel_XC.WordWrap = False
            '
            'XrLabel_RS
            '
            Me.XrLabel_RS.BorderColor = System.Drawing.Color.Transparent
            Me.XrLabel_RS.CanGrow = False
            Me.XrLabel_RS.Font = New System.Drawing.Font("Times New Roman", 9.75!)
            Me.XrLabel_RS.ForeColor = System.Drawing.SystemColors.ControlText
            Me.XrLabel_RS.Location = New System.Drawing.Point(433, 0)
            Me.XrLabel_RS.Name = "XrLabel_RS"
            Me.XrLabel_RS.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_RS.Size = New System.Drawing.Size(58, 16)
            Me.XrLabel_RS.StylePriority.UseBorderColor = False
            Me.XrLabel_RS.StylePriority.UseFont = False
            Me.XrLabel_RS.StylePriority.UseForeColor = False
            Me.XrLabel_RS.StylePriority.UseTextAlignment = False
            Me.XrLabel_RS.Text = "0"
            Me.XrLabel_RS.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrLabel_RS.WordWrap = False
            '
            'XrLabel_NS
            '
            Me.XrLabel_NS.BorderColor = System.Drawing.Color.Transparent
            Me.XrLabel_NS.CanGrow = False
            Me.XrLabel_NS.Font = New System.Drawing.Font("Times New Roman", 9.75!)
            Me.XrLabel_NS.ForeColor = System.Drawing.SystemColors.ControlText
            Me.XrLabel_NS.Location = New System.Drawing.Point(375, 0)
            Me.XrLabel_NS.Name = "XrLabel_NS"
            Me.XrLabel_NS.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_NS.Size = New System.Drawing.Size(58, 16)
            Me.XrLabel_NS.StylePriority.UseBorderColor = False
            Me.XrLabel_NS.StylePriority.UseFont = False
            Me.XrLabel_NS.StylePriority.UseForeColor = False
            Me.XrLabel_NS.StylePriority.UseTextAlignment = False
            Me.XrLabel_NS.Text = "0"
            Me.XrLabel_NS.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrLabel_NS.WordWrap = False
            '
            'XrLabel_PE
            '
            Me.XrLabel_PE.BorderColor = System.Drawing.Color.Transparent
            Me.XrLabel_PE.CanGrow = False
            Me.XrLabel_PE.Font = New System.Drawing.Font("Times New Roman", 9.75!)
            Me.XrLabel_PE.ForeColor = System.Drawing.SystemColors.ControlText
            Me.XrLabel_PE.Location = New System.Drawing.Point(317, 0)
            Me.XrLabel_PE.Name = "XrLabel_PE"
            Me.XrLabel_PE.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_PE.Size = New System.Drawing.Size(58, 16)
            Me.XrLabel_PE.StylePriority.UseBorderColor = False
            Me.XrLabel_PE.StylePriority.UseFont = False
            Me.XrLabel_PE.StylePriority.UseForeColor = False
            Me.XrLabel_PE.StylePriority.UseTextAlignment = False
            Me.XrLabel_PE.Text = "0"
            Me.XrLabel_PE.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrLabel_PE.WordWrap = False
            '
            'XrLabel_description
            '
            Me.XrLabel_description.BorderColor = System.Drawing.Color.Transparent
            Me.XrLabel_description.Font = New System.Drawing.Font("Times New Roman", 9.75!)
            Me.XrLabel_description.ForeColor = System.Drawing.SystemColors.ControlText
            Me.XrLabel_description.Location = New System.Drawing.Point(1, 0)
            Me.XrLabel_description.Name = "XrLabel_description"
            Me.XrLabel_description.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_description.Size = New System.Drawing.Size(291, 16)
            Me.XrLabel_description.StylePriority.UseBorderColor = False
            Me.XrLabel_description.StylePriority.UseFont = False
            Me.XrLabel_description.StylePriority.UseForeColor = False
            Me.XrLabel_description.StylePriority.UseTextAlignment = False
            Me.XrLabel_description.Text = "DESCRIPTION"
            Me.XrLabel_description.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            '
            'PageHeader
            '
            Me.PageHeader.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrPanel1})
            Me.PageHeader.Height = 32
            Me.PageHeader.KeepTogether = True
            Me.PageHeader.Name = "PageHeader"
            Me.PageHeader.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
            Me.PageHeader.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
            '
            'XrPanel1
            '
            Me.XrPanel1.BackColor = System.Drawing.Color.Teal
            Me.XrPanel1.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel8, Me.XrLabel9, Me.XrLabel7, Me.XrLabel6, Me.XrLabel1, Me.XrLabel4, Me.XrLabel3, Me.XrLabel2})
            Me.XrPanel1.Location = New System.Drawing.Point(0, 8)
            Me.XrPanel1.Name = "XrPanel1"
            Me.XrPanel1.Size = New System.Drawing.Size(750, 18)
            Me.XrPanel1.StylePriority.UseBackColor = False
            '
            'XrLabel8
            '
            Me.XrLabel8.BorderColor = System.Drawing.Color.Transparent
            Me.XrLabel8.CanGrow = False
            Me.XrLabel8.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel8.ForeColor = System.Drawing.Color.White
            Me.XrLabel8.Location = New System.Drawing.Point(383, 0)
            Me.XrLabel8.Name = "XrLabel8"
            Me.XrLabel8.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel8.Size = New System.Drawing.Size(52, 16)
            Me.XrLabel8.StylePriority.UseBorderColor = False
            Me.XrLabel8.StylePriority.UseFont = False
            Me.XrLabel8.StylePriority.UseForeColor = False
            Me.XrLabel8.StylePriority.UseTextAlignment = False
            Me.XrLabel8.Text = "NS"
            Me.XrLabel8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrLabel8.WordWrap = False
            '
            'XrLabel9
            '
            Me.XrLabel9.BorderColor = System.Drawing.Color.Transparent
            Me.XrLabel9.CanGrow = False
            Me.XrLabel9.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel9.ForeColor = System.Drawing.Color.White
            Me.XrLabel9.Location = New System.Drawing.Point(442, 0)
            Me.XrLabel9.Name = "XrLabel9"
            Me.XrLabel9.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel9.Size = New System.Drawing.Size(52, 16)
            Me.XrLabel9.StylePriority.UseBorderColor = False
            Me.XrLabel9.StylePriority.UseFont = False
            Me.XrLabel9.StylePriority.UseForeColor = False
            Me.XrLabel9.StylePriority.UseTextAlignment = False
            Me.XrLabel9.Text = "RS"
            Me.XrLabel9.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrLabel9.WordWrap = False
            '
            'XrLabel7
            '
            Me.XrLabel7.BorderColor = System.Drawing.Color.Transparent
            Me.XrLabel7.CanGrow = False
            Me.XrLabel7.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel7.ForeColor = System.Drawing.Color.White
            Me.XrLabel7.Location = New System.Drawing.Point(617, 0)
            Me.XrLabel7.Name = "XrLabel7"
            Me.XrLabel7.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel7.Size = New System.Drawing.Size(50, 16)
            Me.XrLabel7.StylePriority.UseBorderColor = False
            Me.XrLabel7.StylePriority.UseFont = False
            Me.XrLabel7.StylePriority.UseForeColor = False
            Me.XrLabel7.StylePriority.UseTextAlignment = False
            Me.XrLabel7.Text = "W"
            Me.XrLabel7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrLabel7.WordWrap = False
            '
            'XrLabel6
            '
            Me.XrLabel6.BorderColor = System.Drawing.Color.Transparent
            Me.XrLabel6.CanGrow = False
            Me.XrLabel6.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel6.ForeColor = System.Drawing.Color.White
            Me.XrLabel6.Location = New System.Drawing.Point(558, 0)
            Me.XrLabel6.Name = "XrLabel6"
            Me.XrLabel6.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel6.Size = New System.Drawing.Size(52, 16)
            Me.XrLabel6.StylePriority.UseBorderColor = False
            Me.XrLabel6.StylePriority.UseFont = False
            Me.XrLabel6.StylePriority.UseForeColor = False
            Me.XrLabel6.StylePriority.UseTextAlignment = False
            Me.XrLabel6.Text = "CO"
            Me.XrLabel6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrLabel6.WordWrap = False
            '
            'XrLabel1
            '
            Me.XrLabel1.BorderColor = System.Drawing.Color.Transparent
            Me.XrLabel1.CanGrow = False
            Me.XrLabel1.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel1.ForeColor = System.Drawing.Color.White
            Me.XrLabel1.Location = New System.Drawing.Point(500, 0)
            Me.XrLabel1.Name = "XrLabel1"
            Me.XrLabel1.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel1.Size = New System.Drawing.Size(52, 16)
            Me.XrLabel1.StylePriority.UseBorderColor = False
            Me.XrLabel1.StylePriority.UseFont = False
            Me.XrLabel1.StylePriority.UseForeColor = False
            Me.XrLabel1.StylePriority.UseTextAlignment = False
            Me.XrLabel1.Text = "XC"
            Me.XrLabel1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrLabel1.WordWrap = False
            '
            'XrLabel4
            '
            Me.XrLabel4.BorderColor = System.Drawing.Color.Transparent
            Me.XrLabel4.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel4.ForeColor = System.Drawing.Color.White
            Me.XrLabel4.Location = New System.Drawing.Point(683, 1)
            Me.XrLabel4.Name = "XrLabel4"
            Me.XrLabel4.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel4.Size = New System.Drawing.Size(58, 16)
            Me.XrLabel4.StylePriority.UseBorderColor = False
            Me.XrLabel4.StylePriority.UseFont = False
            Me.XrLabel4.StylePriority.UseForeColor = False
            Me.XrLabel4.StylePriority.UseTextAlignment = False
            Me.XrLabel4.Text = "TOTAL"
            Me.XrLabel4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel3
            '
            Me.XrLabel3.BorderColor = System.Drawing.Color.Transparent
            Me.XrLabel3.CanGrow = False
            Me.XrLabel3.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel3.ForeColor = System.Drawing.Color.White
            Me.XrLabel3.Location = New System.Drawing.Point(317, 0)
            Me.XrLabel3.Name = "XrLabel3"
            Me.XrLabel3.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel3.Size = New System.Drawing.Size(58, 16)
            Me.XrLabel3.StylePriority.UseBorderColor = False
            Me.XrLabel3.StylePriority.UseFont = False
            Me.XrLabel3.StylePriority.UseForeColor = False
            Me.XrLabel3.StylePriority.UseTextAlignment = False
            Me.XrLabel3.Text = "PE"
            Me.XrLabel3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrLabel3.WordWrap = False
            '
            'XrLabel2
            '
            Me.XrLabel2.BorderColor = System.Drawing.Color.Transparent
            Me.XrLabel2.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel2.ForeColor = System.Drawing.Color.White
            Me.XrLabel2.Location = New System.Drawing.Point(1, 1)
            Me.XrLabel2.Name = "XrLabel2"
            Me.XrLabel2.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel2.Size = New System.Drawing.Size(291, 16)
            Me.XrLabel2.StylePriority.UseBorderColor = False
            Me.XrLabel2.StylePriority.UseFont = False
            Me.XrLabel2.StylePriority.UseForeColor = False
            Me.XrLabel2.StylePriority.UseTextAlignment = False
            Me.XrLabel2.Text = "DESCRIPTION"
            Me.XrLabel2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            '
            'XrLabel17
            '
            Me.XrLabel17.Font = New System.Drawing.Font("Times New Roman", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.XrLabel17.ForeColor = System.Drawing.Color.Maroon
            Me.XrLabel17.Location = New System.Drawing.Point(0, 0)
            Me.XrLabel17.Name = "XrLabel17"
            Me.XrLabel17.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel17.Size = New System.Drawing.Size(750, 25)
            Me.XrLabel17.StylePriority.UseTextAlignment = False
            Me.XrLabel17.Text = "Appointment status by type of appointment"
            Me.XrLabel17.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
            '
            'ReportFooter
            '
            Me.ReportFooter.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrRichText1, Me.XrLabel_total_PE, Me.XrLabel_total_W, Me.XrLabel_total_CO, Me.XrLabel_total_XC, Me.XrLabel_total_RS, Me.XrLabel_total_NS, Me.XrLabel_total_total, Me.XrLine1, Me.XrLabel5})
            Me.ReportFooter.Height = 192
            Me.ReportFooter.KeepTogether = True
            Me.ReportFooter.Name = "ReportFooter"
            '
            'XrRichText1
            '
            Me.XrRichText1.Location = New System.Drawing.Point(8, 67)
            Me.XrRichText1.Name = "XrRichText1"
            Me.XrRichText1.SerializableRtfString = resources.GetString("XrRichText1.SerializableRtfString")
            Me.XrRichText1.Size = New System.Drawing.Size(458, 125)
            '
            'XrLabel_total_PE
            '
            Me.XrLabel_total_PE.BorderColor = System.Drawing.Color.Transparent
            Me.XrLabel_total_PE.CanGrow = False
            Me.XrLabel_total_PE.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel_total_PE.ForeColor = System.Drawing.SystemColors.ControlText
            Me.XrLabel_total_PE.Location = New System.Drawing.Point(317, 25)
            Me.XrLabel_total_PE.Name = "XrLabel_total_PE"
            Me.XrLabel_total_PE.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_total_PE.Size = New System.Drawing.Size(58, 16)
            Me.XrLabel_total_PE.StylePriority.UseBorderColor = False
            Me.XrLabel_total_PE.StylePriority.UseFont = False
            Me.XrLabel_total_PE.StylePriority.UseForeColor = False
            Me.XrLabel_total_PE.StylePriority.UseTextAlignment = False
            XrSummary1.FormatString = "{0:n0}"
            XrSummary1.IgnoreNullValues = True
            XrSummary1.Running = DevExpress.XtraReports.UI.SummaryRunning.Report
            Me.XrLabel_total_PE.Summary = XrSummary1
            Me.XrLabel_total_PE.Text = "0"
            Me.XrLabel_total_PE.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrLabel_total_PE.WordWrap = False
            '
            'XrLabel_total_W
            '
            Me.XrLabel_total_W.BorderColor = System.Drawing.Color.Transparent
            Me.XrLabel_total_W.CanGrow = False
            Me.XrLabel_total_W.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel_total_W.ForeColor = System.Drawing.SystemColors.ControlText
            Me.XrLabel_total_W.Location = New System.Drawing.Point(608, 25)
            Me.XrLabel_total_W.Name = "XrLabel_total_W"
            Me.XrLabel_total_W.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_total_W.Size = New System.Drawing.Size(58, 16)
            Me.XrLabel_total_W.StylePriority.UseBorderColor = False
            Me.XrLabel_total_W.StylePriority.UseFont = False
            Me.XrLabel_total_W.StylePriority.UseForeColor = False
            Me.XrLabel_total_W.StylePriority.UseTextAlignment = False
            XrSummary2.FormatString = "{0:n0}"
            XrSummary2.IgnoreNullValues = True
            XrSummary2.Running = DevExpress.XtraReports.UI.SummaryRunning.Report
            Me.XrLabel_total_W.Summary = XrSummary2
            Me.XrLabel_total_W.Text = "0"
            Me.XrLabel_total_W.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrLabel_total_W.WordWrap = False
            '
            'XrLabel_total_CO
            '
            Me.XrLabel_total_CO.BorderColor = System.Drawing.Color.Transparent
            Me.XrLabel_total_CO.CanGrow = False
            Me.XrLabel_total_CO.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel_total_CO.ForeColor = System.Drawing.SystemColors.ControlText
            Me.XrLabel_total_CO.Location = New System.Drawing.Point(550, 25)
            Me.XrLabel_total_CO.Name = "XrLabel_total_CO"
            Me.XrLabel_total_CO.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_total_CO.Size = New System.Drawing.Size(58, 16)
            Me.XrLabel_total_CO.StylePriority.UseBorderColor = False
            Me.XrLabel_total_CO.StylePriority.UseFont = False
            Me.XrLabel_total_CO.StylePriority.UseForeColor = False
            Me.XrLabel_total_CO.StylePriority.UseTextAlignment = False
            XrSummary3.FormatString = "{0:n0}"
            XrSummary3.IgnoreNullValues = True
            XrSummary3.Running = DevExpress.XtraReports.UI.SummaryRunning.Report
            Me.XrLabel_total_CO.Summary = XrSummary3
            Me.XrLabel_total_CO.Text = "0"
            Me.XrLabel_total_CO.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrLabel_total_CO.WordWrap = False
            '
            'XrLabel_total_XC
            '
            Me.XrLabel_total_XC.BorderColor = System.Drawing.Color.Transparent
            Me.XrLabel_total_XC.CanGrow = False
            Me.XrLabel_total_XC.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel_total_XC.ForeColor = System.Drawing.SystemColors.ControlText
            Me.XrLabel_total_XC.Location = New System.Drawing.Point(492, 25)
            Me.XrLabel_total_XC.Name = "XrLabel_total_XC"
            Me.XrLabel_total_XC.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_total_XC.Size = New System.Drawing.Size(58, 16)
            Me.XrLabel_total_XC.StylePriority.UseBorderColor = False
            Me.XrLabel_total_XC.StylePriority.UseFont = False
            Me.XrLabel_total_XC.StylePriority.UseForeColor = False
            Me.XrLabel_total_XC.StylePriority.UseTextAlignment = False
            XrSummary4.FormatString = "{0:n0}"
            XrSummary4.IgnoreNullValues = True
            XrSummary4.Running = DevExpress.XtraReports.UI.SummaryRunning.Report
            Me.XrLabel_total_XC.Summary = XrSummary4
            Me.XrLabel_total_XC.Text = "0"
            Me.XrLabel_total_XC.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrLabel_total_XC.WordWrap = False
            '
            'XrLabel_total_RS
            '
            Me.XrLabel_total_RS.BorderColor = System.Drawing.Color.Transparent
            Me.XrLabel_total_RS.CanGrow = False
            Me.XrLabel_total_RS.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel_total_RS.ForeColor = System.Drawing.SystemColors.ControlText
            Me.XrLabel_total_RS.Location = New System.Drawing.Point(433, 25)
            Me.XrLabel_total_RS.Name = "XrLabel_total_RS"
            Me.XrLabel_total_RS.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_total_RS.Size = New System.Drawing.Size(58, 16)
            Me.XrLabel_total_RS.StylePriority.UseBorderColor = False
            Me.XrLabel_total_RS.StylePriority.UseFont = False
            Me.XrLabel_total_RS.StylePriority.UseForeColor = False
            Me.XrLabel_total_RS.StylePriority.UseTextAlignment = False
            XrSummary5.FormatString = "{0:n0}"
            XrSummary5.IgnoreNullValues = True
            XrSummary5.Running = DevExpress.XtraReports.UI.SummaryRunning.Report
            Me.XrLabel_total_RS.Summary = XrSummary5
            Me.XrLabel_total_RS.Text = "0"
            Me.XrLabel_total_RS.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrLabel_total_RS.WordWrap = False
            '
            'XrLabel_total_NS
            '
            Me.XrLabel_total_NS.BorderColor = System.Drawing.Color.Transparent
            Me.XrLabel_total_NS.CanGrow = False
            Me.XrLabel_total_NS.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel_total_NS.ForeColor = System.Drawing.SystemColors.ControlText
            Me.XrLabel_total_NS.Location = New System.Drawing.Point(375, 25)
            Me.XrLabel_total_NS.Name = "XrLabel_total_NS"
            Me.XrLabel_total_NS.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_total_NS.Size = New System.Drawing.Size(58, 16)
            Me.XrLabel_total_NS.StylePriority.UseBorderColor = False
            Me.XrLabel_total_NS.StylePriority.UseFont = False
            Me.XrLabel_total_NS.StylePriority.UseForeColor = False
            Me.XrLabel_total_NS.StylePriority.UseTextAlignment = False
            XrSummary6.FormatString = "{0:n0}"
            XrSummary6.IgnoreNullValues = True
            XrSummary6.Running = DevExpress.XtraReports.UI.SummaryRunning.Report
            Me.XrLabel_total_NS.Summary = XrSummary6
            Me.XrLabel_total_NS.Text = "0"
            Me.XrLabel_total_NS.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrLabel_total_NS.WordWrap = False
            '
            'XrLabel_total_total
            '
            Me.XrLabel_total_total.BorderColor = System.Drawing.Color.Transparent
            Me.XrLabel_total_total.CanGrow = False
            Me.XrLabel_total_total.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel_total_total.ForeColor = System.Drawing.SystemColors.ControlText
            Me.XrLabel_total_total.Location = New System.Drawing.Point(667, 25)
            Me.XrLabel_total_total.Name = "XrLabel_total_total"
            Me.XrLabel_total_total.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_total_total.Size = New System.Drawing.Size(75, 16)
            Me.XrLabel_total_total.StylePriority.UseBorderColor = False
            Me.XrLabel_total_total.StylePriority.UseFont = False
            Me.XrLabel_total_total.StylePriority.UseForeColor = False
            Me.XrLabel_total_total.StylePriority.UseTextAlignment = False
            XrSummary7.FormatString = "{0:n0}"
            XrSummary7.IgnoreNullValues = True
            XrSummary7.Running = DevExpress.XtraReports.UI.SummaryRunning.Report
            Me.XrLabel_total_total.Summary = XrSummary7
            Me.XrLabel_total_total.Text = "0"
            Me.XrLabel_total_total.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrLabel_total_total.WordWrap = False
            '
            'XrLine1
            '
            Me.XrLine1.ForeColor = System.Drawing.Color.Teal
            Me.XrLine1.Location = New System.Drawing.Point(0, 8)
            Me.XrLine1.Name = "XrLine1"
            Me.XrLine1.Size = New System.Drawing.Size(742, 9)
            '
            'XrLabel5
            '
            Me.XrLabel5.CanGrow = False
            Me.XrLabel5.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel5.ForeColor = System.Drawing.Color.Teal
            Me.XrLabel5.Location = New System.Drawing.Point(0, 25)
            Me.XrLabel5.Name = "XrLabel5"
            Me.XrLabel5.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel5.Size = New System.Drawing.Size(308, 16)
            Me.XrLabel5.StylePriority.UseFont = False
            Me.XrLabel5.StylePriority.UseForeColor = False
            Me.XrLabel5.Text = "TOTALS"
            Me.XrLabel5.WordWrap = False
            '
            'ReportHeader
            '
            Me.ReportHeader.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel17})
            Me.ReportHeader.Height = 25
            Me.ReportHeader.KeepTogether = True
            Me.ReportHeader.Name = "ReportHeader"
            '
            'rpt_ProductionReport_apt_byStatus
            '
            Me.Bands.AddRange(New DevExpress.XtraReports.UI.Band() {Me.Detail, Me.PageHeader, Me.ReportFooter, Me.ReportHeader})
            Me.Margins = New System.Drawing.Printing.Margins(25, 25, 25, 25)
            Me.Version = "8.1"
            CType(Me.XrRichText1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me, System.ComponentModel.ISupportInitialize).EndInit()

        End Sub
        Friend WithEvents Detail As DevExpress.XtraReports.UI.DetailBand
        Friend WithEvents PageHeader As DevExpress.XtraReports.UI.PageHeaderBand
        Friend WithEvents XrLabel17 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrPanel1 As DevExpress.XtraReports.UI.XRPanel
        Friend WithEvents XrLabel4 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel3 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel2 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents ReportFooter As DevExpress.XtraReports.UI.ReportFooterBand
        Friend WithEvents XrLine1 As DevExpress.XtraReports.UI.XRLine
        Friend WithEvents XrLabel5 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel9 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel8 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel7 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel6 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel1 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_Total As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_W As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_CO As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_XC As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_RS As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_NS As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_PE As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_description As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrRichText1 As DevExpress.XtraReports.UI.XRRichText
        Friend WithEvents XrLabel_total_PE As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_total_W As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_total_CO As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_total_XC As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_total_RS As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_total_NS As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_total_total As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents ReportHeader As DevExpress.XtraReports.UI.ReportHeaderBand
    End Class
End Namespace