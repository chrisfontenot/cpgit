#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}

#End Region

Option Compare Binary
Option Explicit On
Option Strict On

Imports DevExpress.XtraReports.UI

Imports System.Data.SqlClient

Namespace Operations.Production
    Friend Class rpt_ProductionReport_ClientCount
        Inherits XtraReport


        Dim ds As New DataSet("ds")

        Public Sub PassParameters(ByVal From_Date As Date, ByVal To_Date As Date)
            Const TableName As String = "rpt_productionreport_clientcount"

            Dim cn As SqlConnection = New System.Data.SqlClient.SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
            Try
                cn.Open()
                Using cmd As SqlCommand = New System.Data.SqlClient.SqlCommand()
                    With cmd
                        .Connection = cn
                        .CommandText = "rpt_productionreport_clientcount"
                        .CommandType = CommandType.StoredProcedure
                        SqlCommandBuilder.DeriveParameters(cmd)

                        .Parameters(1).Value = From_Date.Date
                        .Parameters(2).Value = To_Date.Date.AddDays(1).AddSeconds(-1)
                        .CommandTimeout = 0
                    End With

                    Using da As New SqlDataAdapter(cmd)
                        da.Fill(ds, TableName)
                    End Using
                End Using

                ' Set the datasource for the report
                DataSource = New DataView(ds.Tables(TableName), String.Empty, "active_status", DataViewRowState.CurrentRows)

            Finally
                If cn IsNot Nothing Then cn.Dispose()
            End Try
        End Sub

#Region " Component Designer generated code "

        Public Sub New(ByVal Container As System.ComponentModel.IContainer)
            MyClass.New()

            'Required for Windows.Forms Class Composition Designer support
            Container.Add(Me)
        End Sub

        Public Sub New()
            MyBase.New()

            'This call is required by the Component Designer.
            InitializeComponent()

            'Add any initialization after the InitializeComponent() call
        End Sub

        'Component overrides dispose to clean up the component list.
        Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
            If disposing Then
                If components IsNot Nothing Then
                    components.Dispose()
                End If
            End If
            MyBase.Dispose(disposing)
        End Sub

        'Required by the Component Designer
        Private components As System.ComponentModel.IContainer
        Friend WithEvents Detail As DevExpress.XtraReports.UI.DetailBand

        'NOTE: The following procedure is required by the Component Designer
        'It can be modified using the Component Designer.
        'Do not modify it using the code editor.
        Friend WithEvents ReportHeaderBand1 As DevExpress.XtraReports.UI.ReportHeaderBand
        Friend WithEvents XrLabel1 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents ReportFooter As DevExpress.XtraReports.UI.ReportFooterBand
        Friend WithEvents XrLabel5 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_Total_Count As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_Count As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_Description As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel2 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel3 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrControlStyle1 As DevExpress.XtraReports.UI.XRControlStyle
        Friend WithEvents XrControlStyle2 As DevExpress.XtraReports.UI.XRControlStyle
        Friend WithEvents XrControlStyle3 As DevExpress.XtraReports.UI.XRControlStyle
        Friend WithEvents XrControlStyle4 As DevExpress.XtraReports.UI.XRControlStyle
        Friend WithEvents TopMarginBand1 As DevExpress.XtraReports.UI.TopMarginBand
        Friend WithEvents BottomMarginBand1 As DevExpress.XtraReports.UI.BottomMarginBand
        Friend XrSummary1 As DevExpress.XtraReports.UI.XRSummary

        <System.Diagnostics.DebuggerStepThrough()>
        Private Sub InitializeComponent()
            Dim XrSummary1 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary
            Me.XrControlStyle1 = New DevExpress.XtraReports.UI.XRControlStyle
            Me.XrControlStyle2 = New DevExpress.XtraReports.UI.XRControlStyle
            Me.XrControlStyle3 = New DevExpress.XtraReports.UI.XRControlStyle
            Me.XrControlStyle4 = New DevExpress.XtraReports.UI.XRControlStyle
            Me.Detail = New DevExpress.XtraReports.UI.DetailBand
            Me.XrLabel_Description = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_Count = New DevExpress.XtraReports.UI.XRLabel
            Me.ReportHeaderBand1 = New DevExpress.XtraReports.UI.ReportHeaderBand
            Me.XrLabel3 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel2 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel1 = New DevExpress.XtraReports.UI.XRLabel
            Me.ReportFooter = New DevExpress.XtraReports.UI.ReportFooterBand
            Me.XrLabel5 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_Total_Count = New DevExpress.XtraReports.UI.XRLabel
            Me.TopMarginBand1 = New DevExpress.XtraReports.UI.TopMarginBand
            Me.BottomMarginBand1 = New DevExpress.XtraReports.UI.BottomMarginBand
            CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
            '
            'XrControlStyle1
            '
            Me.XrControlStyle1.BackColor = System.Drawing.Color.Transparent
            Me.XrControlStyle1.BorderColor = System.Drawing.Color.Black
            Me.XrControlStyle1.BorderDashStyle = DevExpress.XtraPrinting.BorderDashStyle.Solid
            Me.XrControlStyle1.Borders = DevExpress.XtraPrinting.BorderSide.None
            Me.XrControlStyle1.BorderWidth = 1
            Me.XrControlStyle1.Font = New System.Drawing.Font("Times New Roman", 14.0!, System.Drawing.FontStyle.Bold)
            Me.XrControlStyle1.ForeColor = System.Drawing.Color.Maroon
            Me.XrControlStyle1.Name = "XrControlStyle1"
            '
            'XrControlStyle2
            '
            Me.XrControlStyle2.BackColor = System.Drawing.Color.Transparent
            Me.XrControlStyle2.BorderColor = System.Drawing.Color.Black
            Me.XrControlStyle2.BorderDashStyle = DevExpress.XtraPrinting.BorderDashStyle.Solid
            Me.XrControlStyle2.Borders = DevExpress.XtraPrinting.BorderSide.None
            Me.XrControlStyle2.BorderWidth = 1
            Me.XrControlStyle2.Font = New System.Drawing.Font("Times New Roman", 10.0!, System.Drawing.FontStyle.Bold)
            Me.XrControlStyle2.ForeColor = System.Drawing.Color.Black
            Me.XrControlStyle2.Name = "XrControlStyle2"
            '
            'XrControlStyle3
            '
            Me.XrControlStyle3.BackColor = System.Drawing.Color.Transparent
            Me.XrControlStyle3.BorderColor = System.Drawing.Color.Black
            Me.XrControlStyle3.BorderDashStyle = DevExpress.XtraPrinting.BorderDashStyle.Solid
            Me.XrControlStyle3.Borders = DevExpress.XtraPrinting.BorderSide.None
            Me.XrControlStyle3.BorderWidth = 1
            Me.XrControlStyle3.Font = New System.Drawing.Font("Times New Roman", 10.0!)
            Me.XrControlStyle3.ForeColor = System.Drawing.Color.Black
            Me.XrControlStyle3.Name = "XrControlStyle3"
            '
            'XrControlStyle4
            '
            Me.XrControlStyle4.BackColor = System.Drawing.Color.Transparent
            Me.XrControlStyle4.BorderColor = System.Drawing.Color.Black
            Me.XrControlStyle4.BorderDashStyle = DevExpress.XtraPrinting.BorderDashStyle.Solid
            Me.XrControlStyle4.Borders = DevExpress.XtraPrinting.BorderSide.None
            Me.XrControlStyle4.BorderWidth = 1
            Me.XrControlStyle4.Font = New System.Drawing.Font("Times New Roman", 10.0!)
            Me.XrControlStyle4.ForeColor = System.Drawing.Color.Black
            Me.XrControlStyle4.Name = "XrControlStyle4"
            '
            'Detail
            '
            Me.Detail.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel_Description, Me.XrLabel_Count})
            Me.Detail.HeightF = 18.0!
            Me.Detail.Name = "Detail"
            Me.Detail.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
            Me.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
            '
            'XrLabel_Description
            '
            Me.XrLabel_Description.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "description")})
            Me.XrLabel_Description.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
            Me.XrLabel_Description.Name = "XrLabel_Description"
            Me.XrLabel_Description.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96.0!)
            Me.XrLabel_Description.SizeF = New System.Drawing.SizeF(233.0!, 17.0!)
            Me.XrLabel_Description.Text = "XrLabel_Description"
            Me.XrLabel_Description.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
            '
            'XrLabel_Count
            '
            Me.XrLabel_Count.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "count", "{0:n0}")})
            Me.XrLabel_Count.LocationFloat = New DevExpress.Utils.PointFloat(233.0!, 0.0!)
            Me.XrLabel_Count.Name = "XrLabel_Count"
            Me.XrLabel_Count.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96.0!)
            Me.XrLabel_Count.SizeF = New System.Drawing.SizeF(67.0!, 17.00001!)
            Me.XrLabel_Count.Text = "XrLabel_Count"
            Me.XrLabel_Count.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'ReportHeaderBand1
            '
            Me.ReportHeaderBand1.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel3, Me.XrLabel2, Me.XrLabel1})
            Me.ReportHeaderBand1.HeightF = 68.0!
            Me.ReportHeaderBand1.Name = "ReportHeaderBand1"
            Me.ReportHeaderBand1.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
            Me.ReportHeaderBand1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
            '
            'XrLabel3
            '
            Me.XrLabel3.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel3.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 42.0!)
            Me.XrLabel3.Name = "XrLabel3"
            Me.XrLabel3.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96.0!)
            Me.XrLabel3.SizeF = New System.Drawing.SizeF(192.0!, 18.0!)
            Me.XrLabel3.Text = "Client Status"
            Me.XrLabel3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
            '
            'XrLabel2
            '
            Me.XrLabel2.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel2.LocationFloat = New DevExpress.Utils.PointFloat(200.0!, 42.0!)
            Me.XrLabel2.Name = "XrLabel2"
            Me.XrLabel2.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96.0!)
            Me.XrLabel2.SizeF = New System.Drawing.SizeF(100.0!, 18.0!)
            Me.XrLabel2.Text = "Count"
            Me.XrLabel2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel1
            '
            Me.XrLabel1.Font = New System.Drawing.Font("Times New Roman", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.XrLabel1.ForeColor = System.Drawing.Color.Maroon
            Me.XrLabel1.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 8.0!)
            Me.XrLabel1.Name = "XrLabel1"
            Me.XrLabel1.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96.0!)
            Me.XrLabel1.SizeF = New System.Drawing.SizeF(300.0!, 25.0!)
            Me.XrLabel1.Text = "Count of Clients"
            Me.XrLabel1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
            '
            'ReportFooter
            '
            Me.ReportFooter.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel5, Me.XrLabel_Total_Count})
            Me.ReportFooter.HeightF = 38.0!
            Me.ReportFooter.Name = "ReportFooter"
            Me.ReportFooter.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
            Me.ReportFooter.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
            '
            'XrLabel5
            '
            Me.XrLabel5.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel5.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 8.0!)
            Me.XrLabel5.Name = "XrLabel5"
            Me.XrLabel5.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96.0!)
            Me.XrLabel5.SizeF = New System.Drawing.SizeF(192.0!, 18.0!)
            Me.XrLabel5.Text = "TOTAL"
            Me.XrLabel5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
            '
            'XrLabel_Total_Count
            '
            Me.XrLabel_Total_Count.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "count")})
            Me.XrLabel_Total_Count.LocationFloat = New DevExpress.Utils.PointFloat(200.0!, 8.0!)
            Me.XrLabel_Total_Count.Name = "XrLabel_Total_Count"
            Me.XrLabel_Total_Count.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96.0!)
            Me.XrLabel_Total_Count.SizeF = New System.Drawing.SizeF(100.0!, 18.0!)
            XrSummary1.FormatString = "{0:n0}"
            XrSummary1.Running = DevExpress.XtraReports.UI.SummaryRunning.Report
            Me.XrLabel_Total_Count.Summary = XrSummary1
            Me.XrLabel_Total_Count.Text = "XrLabel_Total_Count"
            Me.XrLabel_Total_Count.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'TopMarginBand1
            '
            Me.TopMarginBand1.HeightF = 0.0!
            Me.TopMarginBand1.Name = "TopMarginBand1"
            '
            'BottomMarginBand1
            '
            Me.BottomMarginBand1.HeightF = 0.0!
            Me.BottomMarginBand1.Name = "BottomMarginBand1"
            '
            'rpt_ProductionReport_ClientCount
            '
            Me.Bands.AddRange(New DevExpress.XtraReports.UI.Band() {Me.Detail, Me.ReportHeaderBand1, Me.ReportFooter, Me.TopMarginBand1, Me.BottomMarginBand1})
            Me.Margins = New System.Drawing.Printing.Margins(100, 450, 0, 0)
            Me.StyleSheet.AddRange(New DevExpress.XtraReports.UI.XRControlStyle() {Me.XrControlStyle1, Me.XrControlStyle2, Me.XrControlStyle3, Me.XrControlStyle4})
            Me.Version = "10.2"
            CType(Me, System.ComponentModel.ISupportInitialize).EndInit()
        End Sub

#End Region
    End Class
End Namespace