#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}

#End Region

Imports System.ComponentModel
Imports DebtPlus.Reports.Template

Imports DebtPlus.Utils
Imports System.IO
Imports System.Reflection
Imports DevExpress.XtraReports.UI
Imports DebtPlus.Reports.Template.Forms

Namespace Operations.UnPaidClients
    Public Class UnPaidClientsReport
        Inherits TemplateXtraReportClass


        ''' <summary>
        '''     Create an instance of our report
        ''' </summary>
        Public Sub New()
            MyBase.New()

            Const ReportName As String = "DebtPlus.Reports.Operations.UnPaidClients.repx"

            ' See if there is a report reference that we can use
            Dim Fname As String = Path.Combine(DebtPlus.Configuration.Config.ReportsDirectory, ReportName)
            Dim UseDefault As Boolean = True
            Try
                If (Not DebtPlus.Configuration.Config.LocalReportsOnly) AndAlso System.IO.File.Exists(Fname) Then
                    LoadLayout(Fname)
                    UseDefault = False
                End If

            Catch ex As Exception
                DebtPlus.Svc.Common.ErrorHandling.HandleErrors(ex)
            End Try

            ' Load the standard report definition if we need a new item
            If UseDefault Then
                Dim asm As Assembly = Assembly.GetExecutingAssembly                                           ' changed
                Using ios As Stream = asm.GetManifestResourceStream(String.Format("DebtPlus.Reports.{0}", ReportName)) ' changed
                    If ios IsNot Nothing Then LoadLayout(ios) '                                                                       changed
                End Using
            End If

            ' Set the script references as needed
            ScriptReferences = DebtPlus.Utils.AssemblyTools.GetAllSiblingAssemblies()

            ' Copy the script references to the subreports as well
            For Each Band As Band In Bands
                For Each ctl As XRControl In Band.Controls
                    Dim subReport As XRSubreport = TryCast(ctl, XRSubreport)
                    If subReport IsNot Nothing Then
                        Dim rpt As XtraReport = subReport.ReportSource
                        If rpt IsNot Nothing Then
                            rpt.ScriptReferences = ScriptReferences
                        End If
                    End If
                Next
            Next

            ' Clear the parameters
            Parameter_Counselor = -2
        End Sub

        Public Property Parameter_Counselor() As Int32
            Get
                Return DebtPlus.Utils.Nulls.DInt(Parameters("ParameterCounselor").Value)
            End Get
            Set(ByVal value As Int32)
                Parameters("ParameterCounselor").Value = value
            End Set
        End Property

        Public Overrides Sub SetReportParameter(ByVal Parameter As String, ByVal Value As Object)
            Select Case Parameter
                Case "Counselor"
                    Parameter_Counselor = DebtPlus.Utils.Nulls.DInt(Value)
                Case Else
                    MyBase.SetReportParameter(Parameter, Value)
            End Select
        End Sub

        Public Overrides Function NeedParameters() As Boolean
            Return Parameter_Counselor = -2
        End Function

        Public Overrides Function RequestReportParameters() As DialogResult
            Dim answer As DialogResult = DialogResult.OK
            If NeedParameters() Then
                Using frm As New CounselorParametersForm()
                    frm.EnableAllCounselors = True
                    answer = frm.ShowDialog()
                    Parameter_Counselor = DebtPlus.Utils.Nulls.DInt(frm.Parameter_Counselor, -1)
                End Using
            End If
            Return answer
        End Function


        ''' <summary>
        '''     Return the report title information
        ''' </summary>
        Public Overrides ReadOnly Property ReportTitle() As String
            Get
                Return "UnPaid Clients"
            End Get
        End Property

        Public Overrides ReadOnly Property ReportSubTitle() As String
            Get
                If Parameter_Counselor <= 0 Then
                    Return "This report covers all counselors"
                Else
                    Return String.Format("This report covers counelor # {0:f0}", Parameter_Counselor)
                End If
            End Get
        End Property
    End Class
End Namespace
