#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}
#End Region

Namespace Operations.UnPaidClients
    Public Class UnpaidClientsReport


        ''' <summary>
        ''' Create an instance of our report
        ''' </summary>
        Public Sub New()
            MyBase.New()
            InitializeComponent()

            AddHandler BeforePrint, AddressOf UnpaidClientsReport_BeforePrint
            AddHandler XrLabel_client.BeforePrint, AddressOf XrLabel_client_BeforePrint
            AddHandler XrLabel_GroupHeader.BeforePrint, AddressOf XrLabel_GroupHeader_BeforePrint

            Parameter_Counselor = -2
        End Sub

        Public Property Parameter_Counselor() As Integer
            Get
                Return DebtPlus.Utils.Nulls.DInt(Parameters("ParameterCounselor").Value)
            End Get
            Set(ByVal value As Integer)
                Parameters("ParameterCounselor").Value = value
            End Set
        End Property

        Public Overrides Sub SetReportParameter(ByVal Parameter As String, ByVal Value As Object)
            Select Case Parameter
                Case "Counselor"
                    Parameter_Counselor = DebtPlus.Utils.Nulls.DInt(Value)
                Case Else
                    MyBase.SetReportParameter(Parameter, Value)
            End Select
        End Sub

        Public Overrides Function NeedParameters() As Boolean
            Return Parameter_Counselor = -2
        End Function

        Public Overrides Function RequestReportParameters() As System.Windows.Forms.DialogResult
            Dim Answer As System.Windows.Forms.DialogResult = DialogResult.OK
            If NeedParameters() Then
                Using frm As New DebtPlus.Reports.Template.Forms.CounselorParametersForm()
                    frm.EnableAllCounselors = True
                    Answer = frm.ShowDialog
                    Parameter_Counselor = DebtPlus.Utils.Nulls.DInt(frm.Parameter_Counselor, -1)
                End Using
            End If
            Return Answer
        End Function


        ''' <summary>
        ''' Return the report title information
        ''' </summary>
        Public Overrides ReadOnly Property ReportTitle() As String
            Get
                Return "UnPaid Clients"
            End Get
        End Property

        Public Overrides ReadOnly Property ReportSubTitle() As String
            Get
                If Parameter_Counselor <= 0 Then
                    Return "This report covers all counselors"
                Else
                    Return String.Format("This report covers counelor # {0:f0}", Parameter_Counselor)
                End If
            End Get
        End Property

        ''' <summary>
        ''' Refresh the dataset with the new information
        ''' </summary>
        Dim ds As New System.Data.DataSet("ds")
        Private Sub UnpaidClientsReport_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs)
            Dim rpt As DevExpress.XtraReports.UI.XtraReport = CType(sender, DevExpress.XtraReports.UI.XtraReport)

            '-- Read the data from the datasource
            Try
                Dim sqlInfo As DebtPlus.LINQ.SQLInfoClass = DebtPlus.LINQ.SQLInfoClass.getDefault()
                Using cn As System.Data.SqlClient.SqlConnection = New System.Data.SqlClient.SqlConnection(sqlInfo.ConnectionString)
                    cn.Open()
                    Using cmd As New System.Data.SqlClient.SqlCommand()
                        cmd.Connection = cn
                        cmd.CommandText = "rpt_unpaid_clients_2"
                        cmd.CommandType = System.Data.CommandType.StoredProcedure
                        System.Data.SqlClient.SqlCommandBuilder.DeriveParameters(cmd)

                        '-- Add the counselor to the list.
                        Dim Counselor As Object = rpt.Parameters("ParameterCounselor").Value
                        If DebtPlus.Utils.Nulls.DInt(Counselor) > 0 Then
                            cmd.Parameters(1).Value = Counselor
                        End If

                        cmd.CommandTimeout = 0

                        Using da As New System.Data.SqlClient.SqlDataAdapter(cmd)
                            da.Fill(ds, "rpt_unpaid_clients_2")
                            rpt.DataSource = New System.Data.DataView(ds.Tables("rpt_unpaid_clients_2"), String.Empty, "[group], [client]", System.Data.DataViewRowState.CurrentRows)
                        End Using
                    End Using
                End Using

            Catch ex As System.Data.SqlClient.SqlException
                Dim gdr As New DebtPlus.Repository.GetDataResult() : gdr.HandleException(ex) : DebtPlus.UI.Common.ErrorHandling.HandleErrors(gdr, "Error loading values for report")
            End Try
        End Sub

        Private Sub XrLabel_GroupHeader_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs)
            Dim Item As String = String.Empty

            With CType(sender, DevExpress.XtraReports.UI.XRLabel)
                Dim rpt As DevExpress.XtraReports.UI.XtraReport = CType(.Report, DevExpress.XtraReports.UI.XtraReport)
                Select Case DebtPlus.Utils.Nulls.DInt(rpt.GetCurrentColumnValue("group"))
                    Case 0
                        Item = "List of clients who have never paid"
                    Case 1
                        Item = "List of clients who paid this month"
                    Case 2
                        Item = "List of clients who have not paid in a month"
                    Case 3
                        Item = "List of clients who have not paid in two months"
                    Case 4
                        Item = "List of clients who have not paid in three months"
                    Case 5
                        Item = "List of clients who have not paid in four or more months"
                    Case Else
                        Item = String.Format("List of clients who have not paid in four or more months ({0:f0})", rpt.GetCurrentColumnValue("group"))
                End Select

                .Text = Item
            End With
        End Sub

        Private Sub XrLabel_client_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs)
            With CType(sender, DevExpress.XtraReports.UI.XRLabel)
                Dim rpt As DevExpress.XtraReports.UI.XtraReport = CType(.Report, DevExpress.XtraReports.UI.XtraReport)
                .Text = DebtPlus.Utils.Format.Client.FormatClientID(rpt.GetCurrentColumnValue("client"))
            End With
        End Sub
    End Class
End Namespace
