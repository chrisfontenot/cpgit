#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}
#End Region

Option Compare Binary
Option Explicit On 
Option Strict On

Namespace DailySummary.Refunds.Client
    Public Class DailySummaryClientRefundsReport

        ''' <summary>
        ''' Create an instance of our report
        ''' </summary>
        Public Sub New()
            MyBase.New()
            InitializeComponent()
            AddHandler BeforePrint, AddressOf DailySummaryClientRefundsReport_BeforePrint
        End Sub

        Public Overrides ReadOnly Property ReportTitle() As String
            Get
                Return "Client Refunds Checks"
            End Get
        End Property

        Dim ds As New System.Data.DataSet("ds")

        Private Sub DailySummaryClientRefundsReport_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs)
            Dim rpt As DevExpress.XtraReports.UI.XtraReport = CType(sender, DevExpress.XtraReports.UI.XtraReport)
            Dim sqlInfo As DebtPlus.LINQ.SQLInfoClass = DebtPlus.LINQ.SQLInfoClass.getDefault()

            Dim current_cursor As System.Windows.Forms.Cursor = System.Windows.Forms.Cursor.Current
            Try
                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.WaitCursor
                Using cn As New System.Data.SqlClient.SqlConnection(sqlInfo.ConnectionString)

                    '-- Read the transactions
                    Using cmd As New System.Data.SqlClient.SqlCommand()
                        cmd.Connection = cn
                        cmd.CommandText = "rpt_Summary_Client_Refund"
                        cmd.CommandType = System.Data.CommandType.StoredProcedure
                        cmd.Parameters.Add("@FromDate", System.Data.SqlDbType.DateTime).Value = rpt.Parameters("ParameterFromDate").Value
                        cmd.Parameters.Add("@ToDate", System.Data.SqlDbType.DateTime).Value = rpt.Parameters("ParameterToDate").Value
                        cmd.CommandTimeout = 0

                        Using da As New System.Data.SqlClient.SqlDataAdapter(cmd)
                            da.Fill(ds, "DailySummaryClientRefundsReport")
                            rpt.DataSource = New System.Data.DataView(ds.Tables("DailySummaryClientRefundsReport"), String.Empty, "item_date, client", System.Data.DataViewRowState.CurrentRows)

                            For Each calc As DevExpress.XtraReports.UI.CalculatedField In rpt.CalculatedFields
                                calc.Assign(rpt.DataSource, rpt.DataMember)
                            Next
                        End Using
                    End Using
                End Using

            Catch ex As System.Data.SqlClient.SqlException
                Using gdr As New DebtPlus.Repository.GetDataResult()
                    gdr.HandleException(ex)
                    DebtPlus.UI.Common.ErrorHandling.HandleErrors(gdr, "Error reading transactions")
                End Using

            Finally
                System.Windows.Forms.Cursor.Current = current_cursor
            End Try
        End Sub
    End Class
End Namespace
