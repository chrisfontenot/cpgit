#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}

#End Region

Imports System.ComponentModel
Imports DebtPlus.Reports.Disbursement.DetailInformation

Imports DebtPlus.Reports.Template.Forms
Imports System.Text
Imports System.Data.SqlClient

Namespace Disbursement.CheckRegister
    Public Class DisbursementCheckRegisterReport
        Inherits DisbursementDetailInformationReport

        Public Sub New()
            MyBase.New()
            InitializeComponent()
        End Sub

        Private privateDisbursementRegister As Int32 = -1

        Public Property Parameter_DisbursementRegister() As Int32
            Get
                Return privateDisbursementRegister
            End Get
            Set(ByVal value As Int32)
                privateDisbursementRegister = value
            End Set
        End Property

        Public Overrides ReadOnly Property ReportTitle() As String
            Get
                Return "Disbursement Check Register"
            End Get
        End Property

        Public Overrides ReadOnly Property ReportSubTitle() As String
            Get
                Return String.Format("This report is for batch # {0:f0}", Parameter_DisbursementRegister)
            End Get
        End Property

        Public Overrides Function NeedParameters() As Boolean
            Return MyBase.NeedParameters() OrElse Parameter_DisbursementRegister <= 0
        End Function

        Public Overrides Function RequestReportParameters() As DialogResult
            Dim answer As DialogResult = DialogResult.OK
            If NeedParameters() Then
                Using frm As New DisbursementParametersForm()
                    answer = frm.ShowDialog()
                    Parameter_DisbursementRegister = frm.Parameter_BatchID
                End Using
            End If
            Return answer
        End Function

        Protected Overrides Function DatabaseTable() As DataView
            Dim answer As New StringBuilder
            Dim ds As New DataSet("ds")

            answer.Append("select	convert(varchar(50),newid()) as id,")
            answer.Append("         tr.trust_register, ")
            answer.Append(" 		tr.bank, tr.checknum, ")
            answer.Append(" 		rcc.creditor, ")
            answer.Append(" 		cr.creditor_name, ")
            answer.Append(" 		tr.date_created, ")
            answer.Append(" 		SUM(rcc.debit_amt) as gross, ")
            answer.Append(" 		SUM(case when rcc.creditor_type in ('D','N') then 0 else rcc.fairshare_amt end) as billed, ")
            answer.Append(" 		SUM(case when rcc.creditor_type = 'D' then rcc.fairshare_amt else 0 end) as deducted, ")
            answer.Append(" 		len(tr.checknum) as len_checknum ")
            answer.Append("from     registers_client_creditor rcc ")
            answer.Append("inner join registers_trust tr on rcc.trust_register = tr.trust_register ")
            answer.Append("inner join creditors cr on rcc.creditor = cr.creditor ")
            answer.Append("where	rcc.tran_type in ('AD','MD','CM') ")
            answer.Append("and		rcc.disbursement_register = @disbursement_register ")
            answer.Append("group by tr.trust_register, tr.bank, tr.checknum, rcc.creditor, cr.creditor_name, tr.date_created; ")

            Dim tbl As DataTable = Nothing
            Using cmd As SqlCommand = New System.Data.SqlClient.SqlCommand()
                With cmd
                    .Connection = New System.Data.SqlClient.SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
                    .CommandText = answer.ToString()
                    .CommandType = CommandType.Text
                    .CommandTimeout = 0
                    .Parameters.Add("@disbursement_register", SqlDbType.Int).Value = Parameter_DisbursementRegister
                End With

                Using da As New SqlDataAdapter(cmd)
                    da.Fill(ds, "records")
                    tbl = ds.Tables("records")

                    With tbl
                        If Not .Columns.Contains("net") Then
                            .Columns.Add("net", GetType(Decimal), "[gross]-[deducted]")
                        End If
                        .PrimaryKey = New DataColumn() {.Columns("id")}
                    End With
                End Using
            End Using

            Return New DataView(tbl, String.Empty, "len_checknum, checknum, creditor", DataViewRowState.CurrentRows)
        End Function

        Protected Overrides Sub MyRunReport(ByVal obj As Object)
            Dim RecordsTable As DataTable = CType(DataSource, DataView).Table
            Dim row As DataRow = RecordsTable.Rows.Find(obj)
            Dim iRep As New DetailReport()
            Try
                With iRep
                    .Parameter_TrustRegister = Convert.ToInt32(row("trust_register"))
                    .Parameter_Creditor = Convert.ToString(row("creditor"))
                    .Parameter_DisbursementRegister = Parameter_DisbursementRegister
                    .CreateDocument()
                    .PrintingSystem.Document.Name = ReportTitle
                    .DisplayPreviewDialog()
                End With

            Finally
                iRep.Dispose()
            End Try
        End Sub
    End Class
End Namespace