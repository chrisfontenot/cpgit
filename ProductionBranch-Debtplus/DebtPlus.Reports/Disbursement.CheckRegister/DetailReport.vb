#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}

#End Region

Imports System.ComponentModel

Imports DevExpress.XtraReports.UI
Imports System.Text
Imports System.Data.SqlClient

Namespace Disbursement.CheckRegister
    Friend Class DetailReport
        Inherits DisbursementCheckRegisterReport

        Public Sub New()
            MyBase.New()
            InitializeComponent()

            With XrLabel_client
                .DataBindings.Add("Text", Nothing, "client")
            End With

            With XrLabel_client_name
                .DataBindings.Add("Text", Nothing, "client_name")
            End With

            With XrLabel_account_number
                .DataBindings.Add("Text", Nothing, "account_number")
            End With
        End Sub

        Private privateParameterTrustRegister As Int32 = -1

        Public Property Parameter_TrustRegister() As Int32
            Get
                Return privateParameterTrustRegister
            End Get
            Set(ByVal value As Int32)
                privateParameterTrustRegister = value
            End Set
        End Property

        Private privateParameterCreditor As String = String.Empty

        Public Property Parameter_Creditor() As String
            Get
                Return privateParameterCreditor
            End Get
            Set(ByVal value As String)
                privateParameterCreditor = value
            End Set
        End Property

        ' Do not do anything if someone should click on a detail item here
        Protected Overrides Sub Preview_Click(ByVal sender As Object, ByVal e As PreviewMouseEventArgs)
        End Sub

        Protected Overrides Function DatabaseTable() As DataView
            Dim answer As New StringBuilder
            Dim ds As New DataSet("ds")

            answer.Append("select	convert(varchar(50),null) as id,")
            answer.Append("         convert(int,null) as trust_register, ")
            answer.Append(" 		tr.bank, tr.checknum, ")
            answer.Append(" 		rcc.creditor, ")
            answer.Append(" 		cr.creditor_name, ")
            answer.Append(" 		tr.date_created, ")
            answer.Append("         rcc.client, ")
            answer.Append("         dbo.format_normal_name(pn.prefix,pn.first,pn.middle,pn.last,pn.suffix) as client_name, ")
            answer.Append(" 		rcc.debit_amt as gross, ")
            answer.Append(" 		convert(money,case when rcc.creditor_type in ('D','N') then 0 else rcc.fairshare_amt end) as billed, ")
            answer.Append(" 		convert(money,case when rcc.creditor_type = 'D' then rcc.fairshare_amt else 0 end) as deducted, ")
            answer.Append("         coalesce(rcc.account_number, cc.account_number, '') as account_number ")
            answer.Append("from     registers_client_creditor rcc ")
            answer.Append("left outer join client_creditor cc on rcc.client_creditor = cc.client_creditor ")
            answer.Append("inner join registers_trust tr on rcc.trust_register = tr.trust_register ")
            answer.Append("left outer join people p on rcc.client = p.client and 1 = p.relation ")
            answer.Append("left outer join names pn on p.nameid = pn.name ")
            answer.Append("left outer join creditors cr on rcc.creditor = cr.creditor ")
            answer.Append("where	rcc.tran_type in ('AD','MD','CM') ")
            answer.Append("and		rcc.trust_register = @TrustRegister ")
            answer.Append("and		rcc.creditor = @Creditor ")

            Using cmd As SqlCommand = New System.Data.SqlClient.SqlCommand()
                With cmd
                    .Connection = New System.Data.SqlClient.SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
                    .CommandText = answer.ToString()
                    .CommandType = CommandType.Text
                    .CommandTimeout = 0
                    .Parameters.Add("@TrustRegister", SqlDbType.Int).Value = Parameter_TrustRegister
                    .Parameters.Add("@Creditor", SqlDbType.VarChar, 10).Value = Parameter_Creditor
                End With

                Using da As New SqlDataAdapter(cmd)
                    da.Fill(ds, "records")
                End Using
            End Using

            Dim tbl As DataTable = ds.Tables("records")
            With tbl
                If Not .Columns.Contains("net") Then
                    .Columns.Add("net", GetType(Decimal), "[gross]-[deducted]")
                End If
            End With

            Return tbl.DefaultView
        End Function
    End Class
End Namespace
