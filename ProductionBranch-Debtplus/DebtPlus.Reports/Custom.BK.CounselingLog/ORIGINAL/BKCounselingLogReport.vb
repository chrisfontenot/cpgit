#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}
#End Region

Option Compare Binary
Option Explicit On 
Option Strict On

Namespace Custom.BK.CounselingLog
    Public Class BKCounselingLogReport

        ''' <summary>
        ''' Create an instance of our report
        ''' </summary>
        Public Sub New()
            MyBase.New()
            InitializeComponent()

            ParameterCounselor.Value = -1
            AddHandler Me.BeforePrint, AddressOf Report_BeforePrint

            ReportFilter.IsEnabled = True
        End Sub

        Public Overrides ReadOnly Property ReportTitle() As String
            Get
                Return "Bankruptcy Counseling Log"
            End Get
        End Property

        Public Property Parameter_Counselor() As Object
            Get
                Return Me.Parameters("ParameterCounselor")
            End Get
            Set(ByVal value As Object)
                Parameters("ParameterCounselor").Value = value
            End Set
        End Property

        Public Overrides Function NeedParameters() As Boolean
            Return MyBase.NeedParameters() OrElse (ParameterCounselor.Value Is Nothing)
        End Function

        Public Overrides Function RequestReportParameters() As System.Windows.Forms.DialogResult
            Dim Answer As System.Windows.Forms.DialogResult = DialogResult.OK
            If NeedParameters() Then
                Using frm As New DebtPlus.Reports.Template.Forms.DatedCounselorParametersForm(False) '-- FIX THIS True)
                    Answer = frm.ShowDialog
                    Parameter_FromDate = frm.Parameter_FromDate
                    Parameter_ToDate = frm.Parameter_ToDate
                    Parameter_Counselor = frm.Parameter_Counselor
                End Using
            End If
            Return Answer
        End Function

        Private ds As New System.Data.DataSet("ds")
        Private Sub Report_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs)
            Const TableName As String = "rpt_custom_bk_counseling_log"

            Dim rpt As DevExpress.XtraReports.UI.XtraReport = CType(sender, DevExpress.XtraReports.UI.XtraReport)
            Dim tbl As System.Data.DataTable = ds.Tables(TableName)
            If tbl Is Nothing Then
                Dim sqlInfo As DebtPlus.LINQ.SQLInfoClass = DebtPlus.LINQ.SQLInfoClass.getDefault()
                Dim cn As System.Data.SqlClient.SqlConnection = New System.Data.SqlClient.SqlConnection(sqlInfo.ConnectionString)
                Try
                    cn.Open()
                    Using cmd As New System.Data.SqlClient.SqlCommand()
                        With cmd
                            .Connection = cn
                            .CommandText = "rpt_custom_bk_counseling_log"
                            .CommandType = System.Data.CommandType.StoredProcedure
                            System.Data.SqlClient.SqlCommandBuilder.DeriveParameters(cmd)

                            .Parameters(1).Value = rpt.Parameters("ParameterFromDate").Value
                            .Parameters(2).Value = rpt.Parameters("ParameterToDate").Value
                            If DebtPlus.Utils.Nulls.DInt(rpt.Parameters("ParameterCounselor").Value) > 0 Then .Parameters(3).Value = rpt.Parameters("ParameterCounselor").Value
                            .CommandTimeout = 0
                        End With

                        Using da As New System.Data.SqlClient.SqlDataAdapter(cmd)
                            da.Fill(ds, TableName)
                            tbl = ds.Tables(TableName)
                        End Using
                    End Using

                Catch ex As System.Data.SqlClient.SqlException
                    Dim gdr As New DebtPlus.Repository.GetDataResult() : gdr.HandleException(ex) : DebtPlus.UI.Common.ErrorHandling.HandleErrors(gdr, "Error reading report information")

                Finally
                    If cn IsNot Nothing Then
                        If cn.State <> System.Data.ConnectionState.Closed Then cn.Close()
                        cn.Dispose()
                    End If
                End Try
            End If

            If tbl IsNot Nothing Then
                rpt.DataSource = New System.Data.DataView(tbl, CType(rpt, DebtPlus.Interfaces.Reports.IReportFilter).ReportFilter.ViewFilter, String.Empty, System.Data.DataViewRowState.CurrentRows)
                For Each calc As DevExpress.XtraReports.UI.CalculatedField In rpt.CalculatedFields
                    calc.Assign(rpt.DataSource, rpt.DataMember)
                Next
            End If

            '-- After the first time, enable the report filter for subsequent items
            CType(rpt, DebtPlus.Interfaces.Reports.IReportFilter).ReportFilter.IsEnabled = True
        End Sub
    End Class
End Namespace
