#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}

#End Region

Option Compare Binary
Option Explicit On
Option Strict On

Imports System.ComponentModel
Imports DebtPlus.Reports.Template
Imports System.IO
Imports System.Reflection
Imports DebtPlus.Utils
Imports DevExpress.XtraReports.UI
Imports DebtPlus.Reports.Template.Forms

Namespace Custom.BK.CounselingLog
    Public Class BKCounselingLogReport
        Inherits DatedTemplateXtraReportClass


        ''' <summary>
        '''     Create an instance of our report
        ''' </summary>
        Public Sub New()
            MyBase.New()
            InitializeComponent()
        End Sub

        Public Sub New(ByVal DateRange As DateRange)
            MyBase.New(DateRange)
            InitializeComponent()
        End Sub

        Public Sub InitializeComponent()
            Const ReportName As String = "DebtPlus.Reports.Custom.BK.CounselingLog.repx"

            ' See if there is a report reference that we can use
            Dim Fname As String = Path.Combine(DebtPlus.Configuration.Config.ReportsDirectory, ReportName)
            Dim UseDefault As Boolean = True
            Try
                If (Not DebtPlus.Configuration.Config.LocalReportsOnly) AndAlso System.IO.File.Exists(Fname) Then
                    LoadLayout(Fname)
                    UseDefault = False
                End If

            Catch ex As Exception
                DebtPlus.Svc.Common.ErrorHandling.HandleErrors(ex)
            End Try

            ' Load the standard report definition if we need a new item
            If UseDefault Then
                Dim asm As Assembly = Assembly.GetExecutingAssembly                                           ' changed
                Using ios As Stream = asm.GetManifestResourceStream(String.Format("DebtPlus.Reports.{0}", ReportName)) ' changed
                    If ios IsNot Nothing Then LoadLayout(ios) '                                                                       changed
                End Using
            End If

            ' Set the script references as needed
            ScriptReferences = DebtPlus.Utils.AssemblyTools.GetAllSiblingAssemblies()                                                        ' changed   

            ' Update the subreports as well
            For Each BandPtr As Band In Bands
                For Each CtlPtr As XRControl In BandPtr.Controls
                    Dim Rpt As XRSubreport = TryCast(CtlPtr, XRSubreport)
                    If Rpt IsNot Nothing Then
                        Dim RptSrc As XtraReport = TryCast(Rpt.ReportSource, XtraReport)
                        If RptSrc IsNot Nothing Then
                            RptSrc.ScriptReferences = ScriptReferences
                        End If
                    End If
                Next
            Next

            ' Enable the select expert
            ReportFilter.IsEnabled = True
            Parameters("ParameterCounselor").Value = -1
        End Sub

        Public Overrides ReadOnly Property ReportTitle() As String
            Get
                Return "Bankruptcy Counseling Log"
            End Get
        End Property

        Public Property Parameter_Counselor() As Object
            Get
                Return Parameters("ParameterCounselor")
            End Get
            Set(ByVal value As Object)
                Parameters("ParameterCounselor").Value = value
            End Set
        End Property

        Public Overrides Function NeedParameters() As Boolean
            Return MyBase.NeedParameters() OrElse DebtPlus.Utils.Nulls.DInt(Parameters("ParameterCounselor").Value) = -2
        End Function

        Public Overrides Function RequestReportParameters() As DialogResult
            Dim answer As DialogResult = DialogResult.OK
            If NeedParameters() Then
                Using frm As New DatedCounselorParametersForm(False) ' FIX THIS True)
                    answer = frm.ShowDialog()
                    Parameter_FromDate = frm.Parameter_FromDate
                    Parameter_ToDate = frm.Parameter_ToDate
                    Parameter_Counselor = frm.Parameter_Counselor
                End Using
            End If
            Return answer
        End Function
    End Class
End Namespace
