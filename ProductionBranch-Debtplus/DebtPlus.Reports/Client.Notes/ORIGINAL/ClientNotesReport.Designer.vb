Namespace Client.Notes
    <Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
    Partial Class ClientNotesReport
        Inherits DebtPlus.Reports.Template.TemplateXtraReportClass

        'XtraReport overrides dispose to clean up the component list.
        <System.Diagnostics.DebuggerNonUserCode()> _
        Protected Overrides Sub Dispose(ByVal disposing As Boolean)
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
            MyBase.Dispose(disposing)
        End Sub

        'Required by the Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Designer
        'It can be modified using the Designer.  
        'Do not modify it using the code editor.
        <System.Diagnostics.DebuggerStepThrough()> _
        Private Sub InitializeComponent()
            Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(ClientNotesReport))
            Me.XrTable1 = New DevExpress.XtraReports.UI.XRTable()
            Me.XrTableRow1 = New DevExpress.XtraReports.UI.XRTableRow()
            Me.XrTableCell1 = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableCell_date_created = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableCell9 = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableCell_type = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableCell10 = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableCell3 = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableRow3 = New DevExpress.XtraReports.UI.XRTableRow()
            Me.XrTableCell7 = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableCell8 = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableRow_creditor = New DevExpress.XtraReports.UI.XRTableRow()
            Me.XrTableCell_creditor_label = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableCell_creditor = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrRichText_note = New DevExpress.XtraReports.UI.XRRichText()
            Me.XrLine1 = New DevExpress.XtraReports.UI.XRLine()
            Me.ParameterClient = New DevExpress.XtraReports.Parameters.Parameter()
            Me.ClientName = New DevExpress.XtraReports.UI.CalculatedField()
            Me.XrLabel1 = New DevExpress.XtraReports.UI.XRLabel()
            CType(Me.XrTable1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.XrRichText_note, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
            '
            'PageHeader
            '
            Me.PageHeader.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel1})
            Me.PageHeader.HeightF = 123.0!
            Me.PageHeader.Controls.SetChildIndex(Me.XrLabel1, 0)
            Me.PageHeader.Controls.SetChildIndex(Me.XrPanel_AgencyAddress, 0)
            Me.PageHeader.Controls.SetChildIndex(Me.XrLabel_Title, 0)
            Me.PageHeader.Controls.SetChildIndex(Me.XrLine_Title, 0)
            Me.PageHeader.Controls.SetChildIndex(Me.XrLabel_Subtitle, 0)
            Me.PageHeader.Controls.SetChildIndex(Me.XrPageInfo1, 0)
            '
            'XrLabel_Title
            '
            Me.XrLabel_Title.StylePriority.UseFont = False
            '
            'XrLabel_Subtitle
            '
            Me.XrLabel_Subtitle.LocationFloat = New DevExpress.Utils.PointFloat(699.6667!, 85.00001!)
            Me.XrLabel_Subtitle.SizeF = New System.Drawing.SizeF(42.33331!, 17.0!)
            Me.XrLabel_Subtitle.Visible = False
            '
            'XrPageInfo_PageNumber
            '
            Me.XrPageInfo_PageNumber.StylePriority.UseTextAlignment = False
            '
            'XRLabel_Agency_Name
            '
            Me.XRLabel_Agency_Name.StylePriority.UseTextAlignment = False
            '
            'XrLabel_Agency_Address3
            '
            Me.XrLabel_Agency_Address3.StylePriority.UseFont = False
            Me.XrLabel_Agency_Address3.StylePriority.UseTextAlignment = False
            '
            'XrLabel_Agency_Address1
            '
            Me.XrLabel_Agency_Address1.StylePriority.UseFont = False
            Me.XrLabel_Agency_Address1.StylePriority.UseTextAlignment = False
            '
            'XrLabel_Agency_Phone
            '
            Me.XrLabel_Agency_Phone.StylePriority.UseFont = False
            Me.XrLabel_Agency_Phone.StylePriority.UseTextAlignment = False
            '
            'XrLabel_Agency_Address2
            '
            Me.XrLabel_Agency_Address2.StylePriority.UseFont = False
            Me.XrLabel_Agency_Address2.StylePriority.UseTextAlignment = False
            '
            'Detail
            '
            Me.Detail.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLine1, Me.XrRichText_note, Me.XrTable1})
            Me.Detail.HeightF = 117.0!
            Me.Detail.KeepTogether = True
            '
            'XrTable1
            '
            Me.XrTable1.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrTable1.ForeColor = System.Drawing.Color.Teal
            Me.XrTable1.LocationFloat = New DevExpress.Utils.PointFloat(8.0!, 8.0!)
            Me.XrTable1.Name = "XrTable1"
            Me.XrTable1.Rows.AddRange(New DevExpress.XtraReports.UI.XRTableRow() {Me.XrTableRow1, Me.XrTableRow3, Me.XrTableRow_creditor})
            Me.XrTable1.SizeF = New System.Drawing.SizeF(742.0!, 45.0!)
            Me.XrTable1.StylePriority.UseFont = False
            Me.XrTable1.StylePriority.UseForeColor = False
            '
            'XrTableRow1
            '
            Me.XrTableRow1.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.XrTableCell1, Me.XrTableCell_date_created, Me.XrTableCell9, Me.XrTableCell_type, Me.XrTableCell10, Me.XrTableCell3})
            Me.XrTableRow1.Name = "XrTableRow1"
            Me.XrTableRow1.Weight = 0.33333333333333331R
            '
            'XrTableCell1
            '
            Me.XrTableCell1.Name = "XrTableCell1"
            Me.XrTableCell1.Text = "DATE"
            Me.XrTableCell1.Weight = 0.13477088948787061R
            '
            'XrTableCell_date_created
            '
            Me.XrTableCell_date_created.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "date_created", "{0:d}")})
            Me.XrTableCell_date_created.Name = "XrTableCell_date_created"
            Me.XrTableCell_date_created.Weight = 0.15768194070080863R
            '
            'XrTableCell9
            '
            Me.XrTableCell9.Name = "XrTableCell9"
            Me.XrTableCell9.Text = "TYPE"
            Me.XrTableCell9.Weight = 0.090296495956873321R
            '
            'XrTableCell_type
            '
            Me.XrTableCell_type.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "type")})
            Me.XrTableCell_type.Name = "XrTableCell_type"
            Me.XrTableCell_type.Weight = 0.19137466307277629R
            '
            'XrTableCell10
            '
            Me.XrTableCell10.Name = "XrTableCell10"
            Me.XrTableCell10.Text = "CREATOR"
            Me.XrTableCell10.Weight = 0.11051212938005391R
            '
            'XrTableCell3
            '
            Me.XrTableCell3.Name = "XrTableCell3"
            Me.XrTableCell3.Text = "[created_by]"
            Me.XrTableCell3.Weight = 0.31536388140161725R
            '
            'XrTableRow3
            '
            Me.XrTableRow3.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.XrTableCell7, Me.XrTableCell8})
            Me.XrTableRow3.Name = "XrTableRow3"
            Me.XrTableRow3.Weight = 0.33333333333333331R
            '
            'XrTableCell7
            '
            Me.XrTableCell7.Name = "XrTableCell7"
            Me.XrTableCell7.Text = "SUBJECT"
            Me.XrTableCell7.Weight = 0.13477088948787061R
            '
            'XrTableCell8
            '
            Me.XrTableCell8.Name = "XrTableCell8"
            Me.XrTableCell8.Text = "[subject]"
            Me.XrTableCell8.Weight = 0.86522911051212936R
            '
            'XrTableRow_creditor
            '
            Me.XrTableRow_creditor.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.XrTableCell_creditor_label, Me.XrTableCell_creditor})
            Me.XrTableRow_creditor.Name = "XrTableRow_creditor"
            Me.XrTableRow_creditor.Weight = 0.33333333333333331R
            '
            'XrTableCell_creditor_label
            '
            Me.XrTableCell_creditor_label.Name = "XrTableCell_creditor_label"
            Me.XrTableCell_creditor_label.Scripts.OnBeforePrint = "XrTableCell_creditor_label_BeforePrint"
            Me.XrTableCell_creditor_label.Weight = 0.13477088948787061R
            '
            'XrTableCell_creditor
            '
            Me.XrTableCell_creditor.Name = "XrTableCell_creditor"
            Me.XrTableCell_creditor.Scripts.OnBeforePrint = "XrTableCell_creditor_BeforePrint"
            Me.XrTableCell_creditor.Text = "[creditor] [creditor_name]"
            Me.XrTableCell_creditor.Weight = 0.86522911051212936R
            '
            'XrRichText_note
            '
            Me.XrRichText_note.LocationFloat = New DevExpress.Utils.PointFloat(8.0!, 67.0!)
            Me.XrRichText_note.Name = "XrRichText_note"
            Me.XrRichText_note.Scripts.OnBeforePrint = "XrRichText_note_BeforePrint"
            Me.XrRichText_note.SerializableRtfString = resources.GetString("XrRichText_note.SerializableRtfString")
            Me.XrRichText_note.SizeF = New System.Drawing.SizeF(742.0!, 25.0!)
            '
            'XrLine1
            '
            Me.XrLine1.ForeColor = System.Drawing.Color.Maroon
            Me.XrLine1.LineWidth = 3
            Me.XrLine1.LocationFloat = New DevExpress.Utils.PointFloat(167.0!, 92.0!)
            Me.XrLine1.Name = "XrLine1"
            Me.XrLine1.SizeF = New System.Drawing.SizeF(475.0!, 25.0!)
            Me.XrLine1.StylePriority.UseForeColor = False
            '
            'ParameterClient
            '
            Me.ParameterClient.Description = "Client ID"
            Me.ParameterClient.Name = "ParameterClient"
            Me.ParameterClient.Type = GetType(Integer)
            Me.ParameterClient.Value = -1
            Me.ParameterClient.Visible = False
            '
            'ClientName
            '
            Me.ClientName.FieldType = DevExpress.XtraReports.UI.FieldType.[String]
            Me.ClientName.Name = "ClientName"
            Me.ClientName.Scripts.OnGetValue = "ClientName_GetValue"
            '
            'XrLabel1
            '
            Me.XrLabel1.LocationFloat = New DevExpress.Utils.PointFloat(8.0!, 85.00001!)
            Me.XrLabel1.Name = "XrLabel1"
            Me.XrLabel1.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel1.SizeF = New System.Drawing.SizeF(586.7916!, 17.0!)
            Me.XrLabel1.StyleName = "XrControlStyle_Header"
            Me.XrLabel1.Text = "[Parameters.ParameterClient!0000000] [ClientName]"
            '
            'ClientNotesReport
            '
            Me.Bands.AddRange(New DevExpress.XtraReports.UI.Band() {Me.TopMarginBand1, Me.Detail, Me.BottomMarginBand1, Me.PageHeader, Me.PageFooter})
            Me.CalculatedFields.AddRange(New DevExpress.XtraReports.UI.CalculatedField() {Me.ClientName})
            Me.Parameters.AddRange(New DevExpress.XtraReports.Parameters.Parameter() {Me.ParameterClient})
            Me.RequestParameters = False
            Me.ScriptLanguage = DevExpress.XtraReports.ScriptLanguage.VisualBasic
            Me.ScriptReferencesString = resources.GetString("$this.ScriptReferencesString")
            Me.Scripts.OnBeforePrint = "ClientNotesReport_BeforePrint"
            Me.ScriptsSource = resources.GetString("$this.ScriptsSource")
            Me.StyleSheet.AddRange(New DevExpress.XtraReports.UI.XRControlStyle() {Me.XrControlStyle_Header, Me.XrControlStyle_Totals, Me.XrControlStyle_HeaderPannel, Me.XrControlStyle_GroupHeader})
            Me.Version = "11.2"
            CType(Me.XrTable1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.XrRichText_note, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me, System.ComponentModel.ISupportInitialize).EndInit()

        End Sub
        Friend WithEvents XrTable1 As DevExpress.XtraReports.UI.XRTable
        Friend WithEvents XrTableRow1 As DevExpress.XtraReports.UI.XRTableRow
        Friend WithEvents XrTableCell1 As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents XrTableCell_type As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents XrTableCell3 As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents XrTableRow_creditor As DevExpress.XtraReports.UI.XRTableRow
        Friend WithEvents XrTableCell_creditor_label As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents XrTableCell_creditor As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents XrTableRow3 As DevExpress.XtraReports.UI.XRTableRow
        Friend WithEvents XrTableCell7 As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents XrTableCell8 As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents XrTableCell_date_created As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents XrTableCell9 As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents XrTableCell10 As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents XrLine1 As DevExpress.XtraReports.UI.XRLine
        Friend WithEvents XrRichText_note As DevExpress.XtraReports.UI.XRRichText
        Friend WithEvents ParameterClient As DevExpress.XtraReports.Parameters.Parameter
        Friend WithEvents XrLabel1 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents ClientName As DevExpress.XtraReports.UI.CalculatedField
    End Class
End Namespace
