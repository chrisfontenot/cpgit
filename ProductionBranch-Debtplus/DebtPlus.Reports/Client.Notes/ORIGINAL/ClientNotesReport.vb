#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}
#End Region

Namespace Client.Notes
    Public Class ClientNotesReport
        Implements DebtPlus.Interfaces.Client.IClient

        ''' <summary>
        ''' Create an instance of our report
        ''' </summary>
        Public Sub New()
            MyBase.New()
            InitializeComponent()

            AddHandler BeforePrint, AddressOf ClientNotesReport_BeforePrint
            AddHandler ClientName.GetValue, AddressOf ClientName_GetValue
            AddHandler XrTableCell_creditor_label.BeforePrint, AddressOf XrTableCell_creditor_label_BeforePrint
            AddHandler XrRichText_note.BeforePrint, AddressOf XrRichText_note_BeforePrint

            Parameter_Client = -1
        End Sub

        ''' <summary>
        ''' Client
        ''' </summary>
        Public Property ClientID As Int32 Implements DebtPlus.Interfaces.Client.IClient.ClientId
            Get
                Return Parameter_Client
            End Get
            Set(value As Int32)
                Parameter_Client = value
            End Set
        End Property

        ''' <summary>
        ''' Client
        ''' </summary>
        Public Property Parameter_Client() As System.Int32
            Get
                Dim parm As DevExpress.XtraReports.Parameters.Parameter = FindParameter("ParameterClient")
                If parm Is Nothing Then Return -1
                Return Convert.ToInt32(parm.Value)
            End Get
            Set(ByVal value As System.Int32)
                SetParameter("ParameterClient", GetType(Int32), value, "Client ID", False)
            End Set
        End Property

        ''' <summary>
        ''' Report Title block
        ''' </summary>
        Public Overrides ReadOnly Property ReportTitle() As String
            Get
                Return "Client Notes"
            End Get
        End Property

        ''' <summary>
        ''' Generic routine to set a parameter
        ''' </summary>
        Public Overrides Sub SetReportParameter(ByVal Parameter As String, ByVal Value As Object)
            If Parameter = "Client" Then
                Parameter_Client = Convert.ToInt32(Value)
            Else
                MyBase.SetReportParameter(Parameter, Value)
            End If
        End Sub

        ''' <summary>
        ''' Do we need to request the parameters from the user?
        ''' </summary>
        Public Overrides Function NeedParameters() As Boolean
            Return MyBase.NeedParameters OrElse Parameter_Client < 0
        End Function

        ''' <summary>
        ''' Run the dialog to obtain the parameter values
        ''' </summary>
        Public Overrides Function RequestReportParameters() As System.Windows.Forms.DialogResult
            Dim Answer As System.Windows.Forms.DialogResult = DialogResult.OK

            If NeedParameters() Then
                Using frm As New DebtPlus.Reports.Template.Forms.ClientParametersForm()
                    Answer = frm.ShowDialog
                    Parameter_Client = frm.Parameter_Client
                End Using
            End If

            Return Answer
        End Function

        Dim ds As New System.Data.DataSet("ds")
        ''' <summary>
        ''' Create the report before it is printed
        ''' </summary>
        Private Sub ClientNotesReport_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs)
            Const TableName As String = "client_notes"
            Dim rpt As DevExpress.XtraReports.UI.XtraReport = CType(sender, DevExpress.XtraReports.UI.XtraReport)
            Dim sqlInfo As DebtPlus.LINQ.SQLInfoClass = DebtPlus.LINQ.SQLInfoClass.getDefault()
            Dim cn As System.Data.SqlClient.SqlConnection = New System.Data.SqlClient.SqlConnection(sqlInfo.ConnectionString)
            Try
                cn.Open()

                Using cmd As New System.Data.SqlClient.SqlCommand()
                    With cmd
                        .Connection = cn
                        .CommandText = "SELECT n.creditor, n.creditor_name, n.subject, n.type, n.created_by, n.date_created, t.note FROM view_client_notes n WITH (NOLOCK) INNER JOIN client_notes t WITH (NOLOCK) ON n.note_id = t.client_note WHERE n.client=@client AND t.dont_print = 0"
                        .CommandType = System.Data.CommandType.Text
                        .Parameters.Add("@client", System.Data.SqlDbType.Int).Value = rpt.Parameters("ParameterClient").Value
                        .CommandTimeout = 0
                    End With

                    Using da As New System.Data.SqlClient.SqlDataAdapter(cmd)
                        da.Fill(ds, TableName)
                        Dim tbl As System.Data.DataTable = ds.Tables(TableName)
                        rpt.DataSource = New System.Data.DataView(tbl, String.Empty, "date_created", System.Data.DataViewRowState.CurrentRows)
                    End Using
                End Using

            Catch ex As SqlClient.SqlException
                Dim gdr As New DebtPlus.Repository.GetDataResult() : gdr.HandleException(ex) : DebtPlus.UI.Common.ErrorHandling.HandleErrors(gdr, "Error reading client Notes")

            Finally
                If cn IsNot Nothing Then
                    If cn.State <> ConnectionState.Closed Then cn.Close()
                    cn.Dispose()
                End If
            End Try
        End Sub

        ''' <summary>
        ''' Format the creditor label text area if there is a creditor
        ''' </summary>
        Private Sub XrTableCell_creditor_label_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs)
            With CType(sender, DevExpress.XtraReports.UI.XRTableCell)
                Dim rpt As DevExpress.XtraReports.UI.XtraReport = CType(.Report, DevExpress.XtraReports.UI.XtraReport)
                Dim creditor As String = DebtPlus.Utils.Nulls.DStr(rpt.GetCurrentColumnValue("creditor")).Trim
                Dim creditor_name As String = DebtPlus.Utils.Nulls.DStr(rpt.GetCurrentColumnValue("creditor_name")).Trim

                '-- Cancel the row if it will be blank
                e.Cancel = (creditor = String.Empty) AndAlso (creditor_name = String.Empty)
                If e.Cancel Then
                    .Text = String.Empty
                Else
                    .Text = "CREDITOR"
                End If
            End With
        End Sub

        ''' <summary>
        ''' Load the note text as either RTF or TEXT
        ''' </summary>
        Private Sub XrRichText_note_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs)
            Static SpaceCharacters() As Char = (String.Format("{0} {1}", Microsoft.VisualBasic.ControlChars.CrLf, Microsoft.VisualBasic.ControlChars.Tab)).ToCharArray

            '-- Discard leading and trailing blank spaces from the note buffer. They confuese the parser.
            With CType(sender, DevExpress.XtraReports.UI.XRRichText)
                Dim rpt As DevExpress.XtraReports.UI.XtraReport = CType(.Report, DevExpress.XtraReports.UI.XtraReport)
                Dim note_text As String = DebtPlus.Utils.Nulls.DStr(rpt.GetCurrentColumnValue("note")).Trim(SpaceCharacters)

                '-- If the note looks like it might be RTF then try to use RTF.
                If DebtPlus.Utils.Format.Strings.IsRTF(note_text) Then
                    Try
                        .Rtf = note_text
                        Return
                    Catch ex As Exception
                    End Try
                End If

                '-- If the note is HTML then load it as HTML
                If DebtPlus.Utils.Format.Strings.IsHTML(note_text) Then
                    Try
                        .Html = note_text
                        Return
                    Catch ex As Exception
                    End Try
                End If

                '-- Default to loading the item as ASCII TEXT.
                .Text = note_text
            End With
        End Sub

        Private Function GetClientRow(ByVal rpt As DevExpress.XtraReports.UI.XtraReport, ByVal Client As Int32) As System.Data.DataRow
            Dim sqlInfo As DebtPlus.LINQ.SQLInfoClass = DebtPlus.LINQ.SQLInfoClass.getDefault()
            Dim ds As System.Data.DataSet = CType(rpt.DataSource, System.Data.DataView).Table.DataSet
            Const TableName As String = "view_client_address"
            Dim tbl As System.Data.DataTable = ds.Tables(TableName)

            '-- If the value has already been cached then return the cache entry
            If tbl IsNot Nothing Then
                Dim row As System.Data.DataRow = tbl.Rows.Find(Client)
                If row IsNot Nothing Then Return row
            End If

            '-- Read the data from the database for the client's name
            Using cmd As New System.Data.SqlClient.SqlCommand()
                With cmd
                    .Connection = New System.Data.SqlClient.SqlConnection(sqlInfo.ConnectionString)
                    .CommandText = "SELECT client, name FROM view_client_address WHERE client=@client"
                    .CommandType = System.Data.CommandType.Text
                    .Parameters.Add("@client", System.Data.SqlDbType.Int).Value = Client
                End With

                Using da As New System.Data.SqlClient.SqlDataAdapter(cmd)
                    da.Fill(ds, TableName)
                    tbl = ds.Tables(TableName)
                    If tbl.PrimaryKey.GetUpperBound(0) < 0 Then
                        tbl.PrimaryKey = New System.Data.DataColumn() {tbl.Columns("client")}
                    End If
                End Using
            End Using

            '-- Try the operation again to retrieve the client row
            If tbl IsNot Nothing Then
                Dim row As System.Data.DataRow = tbl.Rows.Find(Client)
                If row IsNot Nothing Then Return row
            End If

            Return Nothing
        End Function

        Private Sub ClientName_GetValue(sender As Object, e As DevExpress.XtraReports.UI.GetValueEventArgs)
            Dim rpt As DevExpress.XtraReports.UI.XtraReport = CType(e.Report, DevExpress.XtraReports.UI.XtraReport)
            Dim Client As Int32 = Convert.ToInt32(Parameters("ParameterClient").Value)

            '-- Retrieve the client's name from the system
            Dim row As System.Data.DataRow = GetClientRow(rpt, Client)
            If row IsNot Nothing Then
                e.Value = Convert.ToString(row("name")).Trim
            End If
        End Sub
    End Class
End Namespace
