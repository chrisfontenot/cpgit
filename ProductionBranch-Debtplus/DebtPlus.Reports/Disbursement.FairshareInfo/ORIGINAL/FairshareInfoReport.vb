#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}
#End Region

Namespace Disbursement.FairshareInfo
    Public Class FairshareInfoReport

        ''' <summary>
        ''' Create an instance of our report
        ''' </summary>
        Public Sub New()
            MyBase.New()
            InitializeComponent()
            Parameter_DisbursementRegister = -1
            AddHandler BeforePrint, AddressOf FairshareInfoReport_BeforePrint
            AddHandler XrTable1.BeforePrint, AddressOf XrTable1_BeforePrint
        End Sub

        Public Property Parameter_DisbursementRegister() As System.Int32
            Get
                Return Convert.ToInt32(Parameters("ParameterDisbursementRegister").Value)
            End Get
            Set(ByVal value As System.Int32)
                Parameters("ParameterDisbursementRegister").Value = value
            End Set
        End Property

        Public Overrides Sub SetReportParameter(ByVal Parameter As String, ByVal Value As Object)
            Select Case Parameter
                Case "DisbursementRegister"
                    Parameter_DisbursementRegister = Convert.ToInt32(Value)
                Case Else
                    MyBase.SetReportParameter(Parameter, Value)
            End Select
        End Sub

        Public Overrides ReadOnly Property ReportTitle() As String
            Get
                Return "Deduction Fairshare Info"
            End Get
        End Property

        Public Overrides Function NeedParameters() As Boolean
            Return MyBase.NeedParameters() OrElse Parameter_DisbursementRegister <= 0
        End Function

        Public Overrides Function RequestReportParameters() As System.Windows.Forms.DialogResult
            Dim Answer As System.Windows.Forms.DialogResult = DialogResult.OK
            If NeedParameters() Then
                Using frm As New DebtPlus.Reports.Template.Forms.DisbursementParametersForm()
                    With frm
                        .Text = ReportTitle + " Report Parameters"
                        Answer = .ShowDialog
                        Parameter_DisbursementRegister = .Parameter_BatchID
                    End With
                End Using
            End If

            Return Answer
        End Function

        Dim ds As New System.Data.DataSet("ds")
        Private Sub FairshareInfoReport_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs)
            Const TableName As String = "rpt_disbursement_deduct"
            Dim rpt As DevExpress.XtraReports.UI.XtraReport = CType(sender, DevExpress.XtraReports.UI.XtraReport)
            Dim sqlInfo As DebtPlus.LINQ.SQLInfoClass = DebtPlus.LINQ.SQLInfoClass.getDefault()
            Dim cn As System.Data.SqlClient.SqlConnection = New System.Data.SqlClient.SqlConnection(sqlInfo.ConnectionString)
            Try
                cn.Open()
                Using cmd As New System.Data.SqlClient.SqlCommand()
                    With cmd
                        .Connection = cn
                        .CommandText = "rpt_disbursement_deduct"
                        .CommandType = System.Data.CommandType.StoredProcedure
                        System.Data.SqlClient.SqlCommandBuilder.DeriveParameters(cmd)
                        .Parameters(1).Value = rpt.Parameters("ParameterDisbursementRegister").Value
                        .CommandTimeout = 0
                    End With

                    Using da As New System.Data.SqlClient.SqlDataAdapter(cmd)
                        da.Fill(ds, TableName)
                    End Using
                End Using
                Dim tbl As System.Data.DataTable = ds.Tables(TableName)

                '-- Add the columns for the various formatting types
                With tbl
                    .Columns.Add("xact_type_1", GetType(String), "substring([xact_type],1,1)")
                    .Columns.Add("xact_type_2", GetType(String), "iif([xact_type_1]='2',substring([xact_type],2,len([xact_type])-1),null)")
                    .Columns.Add("credit_amt", GetType(Decimal), "iif([xact_type_1]='2',[deduct_amount],null)")
                    .Columns.Add("debit_amt", GetType(Decimal), "iif([xact_type_1]='3',0-[deduct_amount],null)")
                    .Columns.Add("bill_amt", GetType(Decimal), "iif([bill_amount]=0,null,[bill_amount])")
                    .Columns.Add("balance", GetType(Decimal))
                End With

                '-- Generate the balance information for the rows
                Dim vue As New System.Data.DataView(tbl, String.Empty, "xact_type_1, xact_type, description", System.Data.DataViewRowState.CurrentRows)
                rpt.DataSource = vue

                Dim CurrentBalance As Decimal = 0D
                For Each row As System.Data.DataRowView In vue
                    Dim Deduct As Decimal = DebtPlus.Utils.Nulls.DDec(row("deduct_amount"))
                    Select Case Convert.ToString(row("xact_type_1"))
                        Case "1", "4"
                            CurrentBalance = Deduct
                            row("balance") = CurrentBalance
                        Case "2"
                            If Deduct > 0D Then
                                CurrentBalance += Deduct
                                row("balance") = CurrentBalance
                            End If
                        Case "3"
                            If Deduct > 0D Then
                                CurrentBalance -= Deduct
                                row("balance") = CurrentBalance
                            End If
                        Case Else
                    End Select
                Next

            Catch ex As System.Data.SqlClient.SqlException
                Dim gdr As New DebtPlus.Repository.GetDataResult() : gdr.HandleException(ex) : DebtPlus.UI.Common.ErrorHandling.HandleErrors(gdr, "Error reading report information")

            Finally
                If cn IsNot Nothing Then
                    If cn.State <> System.Data.ConnectionState.Closed Then cn.Close()
                    cn.Dispose()
                End If
            End Try
        End Sub

        Private Sub XrTable1_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs)
            Dim tbl As DevExpress.XtraReports.UI.XRTable = CType(sender, DevExpress.XtraReports.UI.XRTable)
            Dim rpt As DevExpress.XtraReports.UI.XtraReport = CType(tbl.Report, DevExpress.XtraReports.UI.XtraReport)
            Dim sqlInfo As DebtPlus.LINQ.SQLInfoClass = DebtPlus.LINQ.SQLInfoClass.getDefault()
            Dim cn As System.Data.SqlClient.SqlConnection = New System.Data.SqlClient.SqlConnection(sqlInfo.ConnectionString)
            Dim rd As System.Data.SqlClient.SqlDataReader = Nothing

            Try
                cn.Open()
                Using cmd As System.Data.SqlClient.SqlCommand = New System.Data.SqlClient.SqlCommand()
                    cmd.Connection = cn
                    cmd.CommandText = "SELECT * FROM registers_disbursement WHERE disbursement_register=@disbursement_register"
                    cmd.CommandType = System.Data.CommandType.Text
                    cmd.Parameters.Add("@disbursement_register", System.Data.SqlDbType.Int).Value = rpt.Parameters("ParameterDisbursementRegister").Value
                    rd = cmd.ExecuteReader(System.Data.CommandBehavior.CloseConnection Or System.Data.CommandBehavior.SingleRow)
                End Using

                If rd IsNot Nothing AndAlso rd.Read Then
                    For FieldNo As System.Int32 = 0 To rd.FieldCount - 1
                        If Not rd.IsDBNull(FieldNo) Then
                            Select Case rd.GetName(FieldNo).ToLower
                                Case "date_created"
                                    Dim fld As DevExpress.XtraReports.UI.XRTableCell = TryCast(tbl.FindControl("XrTableCell_hdr_date_created", True), DevExpress.XtraReports.UI.XRTableCell)
                                    If fld IsNot Nothing Then fld.Text = String.Format("{0:d}", rd.GetDateTime(FieldNo))
                                Case "date_posted"
                                    Dim fld As DevExpress.XtraReports.UI.XRTableCell = TryCast(tbl.FindControl("XrTableCell_hdr_date_posted", True), DevExpress.XtraReports.UI.XRTableCell)
                                    fld.Text = String.Format("{0:d}", rd.GetDateTime(FieldNo))
                                Case "disbursement_register"
                                    Dim fld As DevExpress.XtraReports.UI.XRTableCell = TryCast(tbl.FindControl("XrTableCell_hdr_disbursement_register", True), DevExpress.XtraReports.UI.XRTableCell)
                                    fld.Text = String.Format("{0:f0}", rd.GetValue(FieldNo))
                                Case "created_by"
                                    Dim fld As DevExpress.XtraReports.UI.XRTableCell = TryCast(tbl.FindControl("XrTableCell_hdr_created_by", True), DevExpress.XtraReports.UI.XRTableCell)
                                    fld.Text = rd.GetString(FieldNo)
                                Case "tran_type"
                                    Dim fld As DevExpress.XtraReports.UI.XRTableCell = TryCast(tbl.FindControl("XrTableCell_hdr_tran_type", True), DevExpress.XtraReports.UI.XRTableCell)
                                    fld.Text = rd.GetString(FieldNo)
                                Case Else
                            End Select
                        End If
                    Next
                End If

            Catch ex As System.Data.SqlClient.SqlException
                Dim gdr As New DebtPlus.Repository.GetDataResult() : gdr.HandleException(ex) : DebtPlus.UI.Common.ErrorHandling.HandleErrors(gdr, "Error reading registers_disbursement table")

            Finally
                If rd IsNot Nothing AndAlso Not rd.IsClosed Then rd.Close()
                If cn IsNot Nothing Then
                    If cn.State <> System.Data.ConnectionState.Closed Then cn.Close()
                    cn.Dispose()
                End If
            End Try
        End Sub
    End Class
End Namespace
