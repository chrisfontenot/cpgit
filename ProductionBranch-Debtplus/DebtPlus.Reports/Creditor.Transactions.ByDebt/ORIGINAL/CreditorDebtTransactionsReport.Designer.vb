Namespace Creditor.Transactions.ByDebt
    <Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
    Partial Class CreditorDebtTransactionsReport
        Inherits DebtPlus.Reports.Template.DatedTemplateXtraReportClass

        'XtraReport overrides dispose to clean up the component list.
        <System.Diagnostics.DebuggerNonUserCode()> _
        Protected Overrides Sub Dispose(ByVal disposing As Boolean)
            If disposing Then
                If components IsNot Nothing Then
                    components.Dispose()
                End If
                ds.Dispose()
            End If
            MyBase.Dispose(disposing)
        End Sub

        'Required by the Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Designer
        'It can be modified using the Designer.  
        'Do not modify it using the code editor.
        <System.Diagnostics.DebuggerStepThrough()> _
        Private Sub InitializeComponent()
            Dim XrSummary1 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary()
            Dim XrSummary2 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary()
            Dim XrSummary3 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary()
            Dim XrSummary4 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary()
            Dim XrSummary5 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary()
            Dim XrSummary6 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary()
            Dim XrSummary7 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary()
            Dim XrSummary8 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary()
            Dim XrSummary9 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary()
            Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(CreditorDebtTransactionsReport))
            Me.ReportFooter = New DevExpress.XtraReports.UI.ReportFooterBand()
            Me.XrLabel_total_disbursement_factor = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_total_current_balance = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel20 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_total_total_payments = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel8 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLine1 = New DevExpress.XtraReports.UI.XRLine()
            Me.XrLabel_creditor = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_creditor_name = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrPanel2 = New DevExpress.XtraReports.UI.XRPanel()
            Me.XrLabel2 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel1 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel5 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel9 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel11 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel12 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel13 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel14 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel15 = New DevExpress.XtraReports.UI.XRLabel()
            Me.GroupHeader1 = New DevExpress.XtraReports.UI.GroupHeaderBand()
            Me.GroupHeader2 = New DevExpress.XtraReports.UI.GroupHeaderBand()
            Me.XrLabel_office_name = New DevExpress.XtraReports.UI.XRLabel()
            Me.GroupFooter1 = New DevExpress.XtraReports.UI.GroupFooterBand()
            Me.XrLabel_start_date = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_account_number = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_counselor_name = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_active_status = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_state = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_disbursement_factor = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_total_payments = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_current_balance = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_client = New DevExpress.XtraReports.UI.XRLabel()
            Me.GroupFooter2 = New DevExpress.XtraReports.UI.GroupFooterBand()
            Me.XrLabel_group_total_payments = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_group_disbursement_factor = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel28 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_group_current_balance = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel34 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel35 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel36 = New DevExpress.XtraReports.UI.XRLabel()
            Me.ParameterCreditor = New DevExpress.XtraReports.Parameters.Parameter()
            CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
            '
            'PageHeader
            '
            Me.PageHeader.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrPanel2, Me.XrLabel_creditor_name, Me.XrLabel_creditor})
            Me.PageHeader.HeightF = 175.0!
            Me.PageHeader.Controls.SetChildIndex(Me.XrPanel_AgencyAddress, 0)
            Me.PageHeader.Controls.SetChildIndex(Me.XrLabel_Title, 0)
            Me.PageHeader.Controls.SetChildIndex(Me.XrLine_Title, 0)
            Me.PageHeader.Controls.SetChildIndex(Me.XrLabel_Subtitle, 0)
            Me.PageHeader.Controls.SetChildIndex(Me.XrPageInfo1, 0)
            Me.PageHeader.Controls.SetChildIndex(Me.XrLabel_creditor, 0)
            Me.PageHeader.Controls.SetChildIndex(Me.XrLabel_creditor_name, 0)
            Me.PageHeader.Controls.SetChildIndex(Me.XrPanel2, 0)
            '
            'XrLabel_Title
            '
            Me.XrLabel_Title.StylePriority.UseFont = False
            '
            'XrPageInfo_PageNumber
            '
            Me.XrPageInfo_PageNumber.LocationFloat = New DevExpress.Utils.PointFloat(907.0!, 8.0!)
            Me.XrPageInfo_PageNumber.StylePriority.UseTextAlignment = False
            '
            'XrPanel_AgencyAddress
            '
            Me.XrPanel_AgencyAddress.LocationFloat = New DevExpress.Utils.PointFloat(742.0!, 0.0!)
            '
            'XRLabel_Agency_Name
            '
            Me.XRLabel_Agency_Name.StylePriority.UseTextAlignment = False
            '
            'XrLabel_Agency_Address3
            '
            Me.XrLabel_Agency_Address3.StylePriority.UseFont = False
            Me.XrLabel_Agency_Address3.StylePriority.UseTextAlignment = False
            '
            'XrLabel_Agency_Address1
            '
            Me.XrLabel_Agency_Address1.StylePriority.UseFont = False
            Me.XrLabel_Agency_Address1.StylePriority.UseTextAlignment = False
            '
            'XrLabel_Agency_Phone
            '
            Me.XrLabel_Agency_Phone.StylePriority.UseFont = False
            Me.XrLabel_Agency_Phone.StylePriority.UseTextAlignment = False
            '
            'XrLabel_Agency_Address2
            '
            Me.XrLabel_Agency_Address2.StylePriority.UseFont = False
            Me.XrLabel_Agency_Address2.StylePriority.UseTextAlignment = False
            '
            'Detail
            '
            Me.Detail.HeightF = 0.0!
            '
            'ReportFooter
            '
            Me.ReportFooter.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel_total_disbursement_factor, Me.XrLabel_total_current_balance, Me.XrLabel20, Me.XrLabel_total_total_payments})
            Me.ReportFooter.HeightF = 35.0!
            Me.ReportFooter.Name = "ReportFooter"
            Me.ReportFooter.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.ReportFooter.StyleName = "XrControlStyle_Totals"
            Me.ReportFooter.StylePriority.UsePadding = False
            '
            'XrLabel_total_disbursement_factor
            '
            Me.XrLabel_total_disbursement_factor.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "disbursement_factor", "{0:c}")})
            Me.XrLabel_total_disbursement_factor.LocationFloat = New DevExpress.Utils.PointFloat(500.6487!, 10.00001!)
            Me.XrLabel_total_disbursement_factor.Name = "XrLabel_total_disbursement_factor"
            Me.XrLabel_total_disbursement_factor.SizeF = New System.Drawing.SizeF(180.9703!, 14.99999!)
            XrSummary1.FormatString = "{0:c}"
            XrSummary1.Running = DevExpress.XtraReports.UI.SummaryRunning.Report
            Me.XrLabel_total_disbursement_factor.Summary = XrSummary1
            '
            'XrLabel_total_current_balance
            '
            Me.XrLabel_total_current_balance.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "current_balance", "{0:c}")})
            Me.XrLabel_total_current_balance.LocationFloat = New DevExpress.Utils.PointFloat(866.4047!, 10.00001!)
            Me.XrLabel_total_current_balance.Name = "XrLabel_total_current_balance"
            Me.XrLabel_total_current_balance.SizeF = New System.Drawing.SizeF(173.5953!, 14.99999!)
            XrSummary2.FormatString = "{0:c}"
            XrSummary2.Running = DevExpress.XtraReports.UI.SummaryRunning.Report
            Me.XrLabel_total_current_balance.Summary = XrSummary2
            '
            'XrLabel20
            '
            Me.XrLabel20.LocationFloat = New DevExpress.Utils.PointFloat(10.00001!, 10.00001!)
            Me.XrLabel20.Name = "XrLabel20"
            Me.XrLabel20.SizeF = New System.Drawing.SizeF(250.0!, 15.0!)
            Me.XrLabel20.StylePriority.UseTextAlignment = False
            Me.XrLabel20.Text = "TOTALS"
            Me.XrLabel20.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
            '
            'XrLabel_total_total_payments
            '
            Me.XrLabel_total_total_payments.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "total_payments", "{0:c}")})
            Me.XrLabel_total_total_payments.LocationFloat = New DevExpress.Utils.PointFloat(698.2142!, 10.00001!)
            Me.XrLabel_total_total_payments.Name = "XrLabel_total_total_payments"
            Me.XrLabel_total_total_payments.SizeF = New System.Drawing.SizeF(151.5953!, 14.99999!)
            XrSummary3.FormatString = "{0:c}"
            XrSummary3.Running = DevExpress.XtraReports.UI.SummaryRunning.Report
            Me.XrLabel_total_total_payments.Summary = XrSummary3
            '
            'XrLabel8
            '
            Me.XrLabel8.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 25.0!)
            Me.XrLabel8.Name = "XrLabel8"
            Me.XrLabel8.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel8.SizeF = New System.Drawing.SizeF(108.0!, 15.0!)
            Me.XrLabel8.Text = "TOTALS"
            Me.XrLabel8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            '
            'XrLine1
            '
            Me.XrLine1.ForeColor = System.Drawing.Color.Teal
            Me.XrLine1.LineWidth = 2
            Me.XrLine1.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 7.999992!)
            Me.XrLine1.Name = "XrLine1"
            Me.XrLine1.SizeF = New System.Drawing.SizeF(1040.0!, 9.000015!)
            Me.XrLine1.StylePriority.UseForeColor = False
            '
            'XrLabel_creditor
            '
            Me.XrLabel_creditor.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "creditor")})
            Me.XrLabel_creditor.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel_creditor.ForeColor = System.Drawing.Color.Teal
            Me.XrLabel_creditor.LocationFloat = New DevExpress.Utils.PointFloat(10.00001!, 110.0!)
            Me.XrLabel_creditor.Name = "XrLabel_creditor"
            Me.XrLabel_creditor.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_creditor.SizeF = New System.Drawing.SizeF(67.70834!, 15.0!)
            Me.XrLabel_creditor.StylePriority.UseFont = False
            Me.XrLabel_creditor.StylePriority.UseForeColor = False
            '
            'XrLabel_creditor_name
            '
            Me.XrLabel_creditor_name.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "creditor_name")})
            Me.XrLabel_creditor_name.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel_creditor_name.ForeColor = System.Drawing.Color.Teal
            Me.XrLabel_creditor_name.LocationFloat = New DevExpress.Utils.PointFloat(88.54166!, 110.0!)
            Me.XrLabel_creditor_name.Name = "XrLabel_creditor_name"
            Me.XrLabel_creditor_name.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_creditor_name.SizeF = New System.Drawing.SizeF(653.4583!, 14.99999!)
            Me.XrLabel_creditor_name.StylePriority.UseFont = False
            Me.XrLabel_creditor_name.StylePriority.UseForeColor = False
            '
            'XrPanel2
            '
            Me.XrPanel2.CanGrow = False
            Me.XrPanel2.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel2, Me.XrLabel1, Me.XrLabel5, Me.XrLabel9, Me.XrLabel11, Me.XrLabel12, Me.XrLabel13, Me.XrLabel14, Me.XrLabel15})
            Me.XrPanel2.LocationFloat = New DevExpress.Utils.PointFloat(7.999992!, 150.0!)
            Me.XrPanel2.Name = "XrPanel2"
            Me.XrPanel2.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrPanel2.SizeF = New System.Drawing.SizeF(1042.0!, 17.0!)
            Me.XrPanel2.StyleName = "XrControlStyle_HeaderPannel"
            Me.XrPanel2.StylePriority.UsePadding = False
            '
            'XrLabel2
            '
            Me.XrLabel2.LocationFloat = New DevExpress.Utils.PointFloat(858.4048!, 1.000007!)
            Me.XrLabel2.Name = "XrLabel2"
            Me.XrLabel2.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel2.SizeF = New System.Drawing.SizeF(66.0!, 15.0!)
            Me.XrLabel2.Text = "START"
            '
            'XrLabel1
            '
            Me.XrLabel1.LocationFloat = New DevExpress.Utils.PointFloat(941.0!, 1.0!)
            Me.XrLabel1.Name = "XrLabel1"
            Me.XrLabel1.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel1.SizeF = New System.Drawing.SizeF(91.0!, 15.0!)
            Me.XrLabel1.Text = "CURR BAL"
            '
            'XrLabel5
            '
            Me.XrLabel5.LocationFloat = New DevExpress.Utils.PointFloat(757.81!, 1.0!)
            Me.XrLabel5.Name = "XrLabel5"
            Me.XrLabel5.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel5.SizeF = New System.Drawing.SizeF(84.0!, 15.0!)
            Me.XrLabel5.Text = "PAYMENTS"
            '
            'XrLabel9
            '
            Me.XrLabel9.LocationFloat = New DevExpress.Utils.PointFloat(590.62!, 1.0!)
            Me.XrLabel9.Name = "XrLabel9"
            Me.XrLabel9.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel9.SizeF = New System.Drawing.SizeF(83.0!, 15.0!)
            Me.XrLabel9.Text = "DISB FACT"
            '
            'XrLabel11
            '
            Me.XrLabel11.LocationFloat = New DevExpress.Utils.PointFloat(391.74!, 1.0!)
            Me.XrLabel11.Name = "XrLabel11"
            Me.XrLabel11.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel11.SizeF = New System.Drawing.SizeF(116.0!, 15.0!)
            Me.XrLabel11.StylePriority.UseTextAlignment = False
            Me.XrLabel11.Text = "COUNSELOR"
            Me.XrLabel11.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            '
            'XrLabel12
            '
            Me.XrLabel12.LocationFloat = New DevExpress.Utils.PointFloat(690.21!, 1.0!)
            Me.XrLabel12.Name = "XrLabel12"
            Me.XrLabel12.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel12.SizeF = New System.Drawing.SizeF(51.0!, 15.0!)
            Me.XrLabel12.StylePriority.UseTextAlignment = False
            Me.XrLabel12.Text = "STATE"
            Me.XrLabel12.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            '
            'XrLabel13
            '
            Me.XrLabel13.LocationFloat = New DevExpress.Utils.PointFloat(525.48!, 1.0!)
            Me.XrLabel13.Name = "XrLabel13"
            Me.XrLabel13.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel13.SizeF = New System.Drawing.SizeF(42.83331!, 15.0!)
            Me.XrLabel13.StylePriority.UseTextAlignment = False
            Me.XrLabel13.Text = "STS"
            Me.XrLabel13.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            '
            'XrLabel14
            '
            Me.XrLabel14.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 1.0!)
            Me.XrLabel14.Name = "XrLabel14"
            Me.XrLabel14.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel14.SizeF = New System.Drawing.SizeF(250.0!, 15.0!)
            Me.XrLabel14.StylePriority.UseTextAlignment = False
            Me.XrLabel14.Text = "CLIENT"
            Me.XrLabel14.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            '
            'XrLabel15
            '
            Me.XrLabel15.LocationFloat = New DevExpress.Utils.PointFloat(258.0!, 1.0!)
            Me.XrLabel15.Name = "XrLabel15"
            Me.XrLabel15.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel15.SizeF = New System.Drawing.SizeF(116.0!, 15.0!)
            Me.XrLabel15.StylePriority.UseTextAlignment = False
            Me.XrLabel15.Text = "ACCOUNT"
            Me.XrLabel15.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            '
            'GroupHeader1
            '
            Me.GroupHeader1.GroupFields.AddRange(New DevExpress.XtraReports.UI.GroupField() {New DevExpress.XtraReports.UI.GroupField("client", DevExpress.XtraReports.UI.XRColumnSortOrder.Ascending)})
            Me.GroupHeader1.HeightF = 0.0!
            Me.GroupHeader1.Name = "GroupHeader1"
            '
            'GroupHeader2
            '
            Me.GroupHeader2.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel_office_name})
            Me.GroupHeader2.GroupFields.AddRange(New DevExpress.XtraReports.UI.GroupField() {New DevExpress.XtraReports.UI.GroupField("office", DevExpress.XtraReports.UI.XRColumnSortOrder.Ascending)})
            Me.GroupHeader2.GroupUnion = DevExpress.XtraReports.UI.GroupUnion.WithFirstDetail
            Me.GroupHeader2.HeightF = 46.0!
            Me.GroupHeader2.Level = 1
            Me.GroupHeader2.Name = "GroupHeader2"
            Me.GroupHeader2.RepeatEveryPage = True
            Me.GroupHeader2.StyleName = "XrControlStyle_GroupHeader"
            '
            'XrLabel_office_name
            '
            Me.XrLabel_office_name.BorderColor = System.Drawing.Color.Maroon
            Me.XrLabel_office_name.Borders = CType((((DevExpress.XtraPrinting.BorderSide.Left Or DevExpress.XtraPrinting.BorderSide.Top) _
                Or DevExpress.XtraPrinting.BorderSide.Right) _
                Or DevExpress.XtraPrinting.BorderSide.Bottom), DevExpress.XtraPrinting.BorderSide)
            Me.XrLabel_office_name.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "office_name")})
            Me.XrLabel_office_name.LocationFloat = New DevExpress.Utils.PointFloat(7.999992!, 10.00001!)
            Me.XrLabel_office_name.Name = "XrLabel_office_name"
            Me.XrLabel_office_name.Padding = New DevExpress.XtraPrinting.PaddingInfo(10, 10, 0, 0, 100.0!)
            Me.XrLabel_office_name.SizeF = New System.Drawing.SizeF(1032.0!, 22.99998!)
            Me.XrLabel_office_name.StylePriority.UseBorderColor = False
            Me.XrLabel_office_name.StylePriority.UseBorders = False
            Me.XrLabel_office_name.StylePriority.UsePadding = False
            '
            'GroupFooter1
            '
            Me.GroupFooter1.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel_start_date, Me.XrLabel_account_number, Me.XrLabel_counselor_name, Me.XrLabel_active_status, Me.XrLabel_state, Me.XrLabel_disbursement_factor, Me.XrLabel_total_payments, Me.XrLabel_current_balance, Me.XrLabel_client})
            Me.GroupFooter1.HeightF = 15.0!
            Me.GroupFooter1.Name = "GroupFooter1"
            Me.GroupFooter1.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.GroupFooter1.StylePriority.UsePadding = False
            '
            'XrLabel_start_date
            '
            Me.XrLabel_start_date.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "start_date", "{0:d}")})
            Me.XrLabel_start_date.LocationFloat = New DevExpress.Utils.PointFloat(866.4048!, 0.0!)
            Me.XrLabel_start_date.Name = "XrLabel_start_date"
            Me.XrLabel_start_date.SizeF = New System.Drawing.SizeF(66.0!, 15.0!)
            Me.XrLabel_start_date.StylePriority.UseTextAlignment = False
            Me.XrLabel_start_date.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
            '
            'XrLabel_account_number
            '
            Me.XrLabel_account_number.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "account_number")})
            Me.XrLabel_account_number.LocationFloat = New DevExpress.Utils.PointFloat(273.9998!, 0.0!)
            Me.XrLabel_account_number.Name = "XrLabel_account_number"
            Me.XrLabel_account_number.SizeF = New System.Drawing.SizeF(116.0!, 15.0!)
            Me.XrLabel_account_number.WordWrap = False
            '
            'XrLabel_counselor_name
            '
            Me.XrLabel_counselor_name.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "counselor_name")})
            Me.XrLabel_counselor_name.LocationFloat = New DevExpress.Utils.PointFloat(406.5951!, 0.0!)
            Me.XrLabel_counselor_name.Name = "XrLabel_counselor_name"
            Me.XrLabel_counselor_name.SizeF = New System.Drawing.SizeF(116.0!, 15.0!)
            Me.XrLabel_counselor_name.WordWrap = False
            '
            'XrLabel_active_status
            '
            Me.XrLabel_active_status.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "active_status")})
            Me.XrLabel_active_status.LocationFloat = New DevExpress.Utils.PointFloat(539.1904!, 0.0!)
            Me.XrLabel_active_status.Name = "XrLabel_active_status"
            Me.XrLabel_active_status.SizeF = New System.Drawing.SizeF(42.83331!, 15.0!)
            '
            'XrLabel_state
            '
            Me.XrLabel_state.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "state")})
            Me.XrLabel_state.LocationFloat = New DevExpress.Utils.PointFloat(698.2142!, 0.0!)
            Me.XrLabel_state.Name = "XrLabel_state"
            Me.XrLabel_state.SizeF = New System.Drawing.SizeF(51.0!, 15.0!)
            '
            'XrLabel_disbursement_factor
            '
            Me.XrLabel_disbursement_factor.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "disbursement_factor", "{0:c}")})
            Me.XrLabel_disbursement_factor.LocationFloat = New DevExpress.Utils.PointFloat(598.619!, 0.0!)
            Me.XrLabel_disbursement_factor.Name = "XrLabel_disbursement_factor"
            Me.XrLabel_disbursement_factor.SizeF = New System.Drawing.SizeF(83.0!, 15.0!)
            Me.XrLabel_disbursement_factor.StylePriority.UseTextAlignment = False
            XrSummary4.FormatString = "{0:c}"
            XrSummary4.Running = DevExpress.XtraReports.UI.SummaryRunning.Group
            Me.XrLabel_disbursement_factor.Summary = XrSummary4
            Me.XrLabel_disbursement_factor.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
            '
            'XrLabel_total_payments
            '
            Me.XrLabel_total_payments.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "total_payments", "{0:c}")})
            Me.XrLabel_total_payments.LocationFloat = New DevExpress.Utils.PointFloat(765.8095!, 0.0!)
            Me.XrLabel_total_payments.Name = "XrLabel_total_payments"
            Me.XrLabel_total_payments.SizeF = New System.Drawing.SizeF(84.0!, 15.0!)
            Me.XrLabel_total_payments.StylePriority.UseTextAlignment = False
            XrSummary5.FormatString = "{0:c}"
            XrSummary5.Running = DevExpress.XtraReports.UI.SummaryRunning.Group
            Me.XrLabel_total_payments.Summary = XrSummary5
            Me.XrLabel_total_payments.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
            '
            'XrLabel_current_balance
            '
            Me.XrLabel_current_balance.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "current_balance", "{0:c}")})
            Me.XrLabel_current_balance.LocationFloat = New DevExpress.Utils.PointFloat(949.0001!, 0.0!)
            Me.XrLabel_current_balance.Name = "XrLabel_current_balance"
            Me.XrLabel_current_balance.SizeF = New System.Drawing.SizeF(91.0!, 15.0!)
            Me.XrLabel_current_balance.StylePriority.UseTextAlignment = False
            XrSummary6.FormatString = "{0:c}"
            XrSummary6.Running = DevExpress.XtraReports.UI.SummaryRunning.Group
            Me.XrLabel_current_balance.Summary = XrSummary6
            Me.XrLabel_current_balance.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
            '
            'XrLabel_client
            '
            Me.XrLabel_client.LocationFloat = New DevExpress.Utils.PointFloat(11.49985!, 0.0!)
            Me.XrLabel_client.Name = "XrLabel_client"
            Me.XrLabel_client.Scripts.OnBeforePrint = "XrLabel_client_BeforePrint"
            Me.XrLabel_client.SizeF = New System.Drawing.SizeF(250.0!, 15.0!)
            Me.XrLabel_client.WordWrap = False
            '
            'GroupFooter2
            '
            Me.GroupFooter2.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel_group_total_payments, Me.XrLabel_group_disbursement_factor, Me.XrLabel28, Me.XrLabel_group_current_balance})
            Me.GroupFooter2.HeightF = 45.0!
            Me.GroupFooter2.Level = 1
            Me.GroupFooter2.Name = "GroupFooter2"
            Me.GroupFooter2.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.GroupFooter2.StyleName = "XrControlStyle_Totals"
            Me.GroupFooter2.StylePriority.UsePadding = False
            '
            'XrLabel_group_total_payments
            '
            Me.XrLabel_group_total_payments.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "total_payments", "{0:c}")})
            Me.XrLabel_group_total_payments.LocationFloat = New DevExpress.Utils.PointFloat(698.2142!, 10.00001!)
            Me.XrLabel_group_total_payments.Name = "XrLabel_group_total_payments"
            Me.XrLabel_group_total_payments.SizeF = New System.Drawing.SizeF(151.5953!, 14.99999!)
            XrSummary7.FormatString = "{0:c}"
            XrSummary7.Running = DevExpress.XtraReports.UI.SummaryRunning.Group
            Me.XrLabel_group_total_payments.Summary = XrSummary7
            '
            'XrLabel_group_disbursement_factor
            '
            Me.XrLabel_group_disbursement_factor.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "disbursement_factor", "{0:c}")})
            Me.XrLabel_group_disbursement_factor.LocationFloat = New DevExpress.Utils.PointFloat(500.6487!, 10.00001!)
            Me.XrLabel_group_disbursement_factor.Name = "XrLabel_group_disbursement_factor"
            Me.XrLabel_group_disbursement_factor.SizeF = New System.Drawing.SizeF(180.9703!, 14.99999!)
            XrSummary8.FormatString = "{0:c}"
            XrSummary8.Running = DevExpress.XtraReports.UI.SummaryRunning.Group
            Me.XrLabel_group_disbursement_factor.Summary = XrSummary8
            '
            'XrLabel28
            '
            Me.XrLabel28.LocationFloat = New DevExpress.Utils.PointFloat(11.49991!, 10.00001!)
            Me.XrLabel28.Name = "XrLabel28"
            Me.XrLabel28.SizeF = New System.Drawing.SizeF(250.0!, 15.0!)
            Me.XrLabel28.StylePriority.UseTextAlignment = False
            Me.XrLabel28.Text = "OFFICE SUBTOTAL"
            Me.XrLabel28.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
            '
            'XrLabel_group_current_balance
            '
            Me.XrLabel_group_current_balance.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "current_balance", "{0:c}")})
            Me.XrLabel_group_current_balance.LocationFloat = New DevExpress.Utils.PointFloat(866.4047!, 10.00001!)
            Me.XrLabel_group_current_balance.Name = "XrLabel_group_current_balance"
            Me.XrLabel_group_current_balance.SizeF = New System.Drawing.SizeF(173.5953!, 14.99999!)
            XrSummary9.FormatString = "{0:c}"
            XrSummary9.Running = DevExpress.XtraReports.UI.SummaryRunning.Group
            Me.XrLabel_group_current_balance.Summary = XrSummary9
            '
            'XrLabel34
            '
            Me.XrLabel34.LocationFloat = New DevExpress.Utils.PointFloat(661.4999!, 25.0!)
            Me.XrLabel34.Name = "XrLabel34"
            Me.XrLabel34.SizeF = New System.Drawing.SizeF(83.0!, 15.0!)
            Me.XrLabel34.Text = "DISB FACT"
            '
            'XrLabel35
            '
            Me.XrLabel35.LocationFloat = New DevExpress.Utils.PointFloat(761.4999!, 25.0!)
            Me.XrLabel35.Name = "XrLabel35"
            Me.XrLabel35.SizeF = New System.Drawing.SizeF(84.0!, 15.0!)
            Me.XrLabel35.Text = "PAYMENTS"
            '
            'XrLabel36
            '
            Me.XrLabel36.LocationFloat = New DevExpress.Utils.PointFloat(873.9999!, 25.0!)
            Me.XrLabel36.Name = "XrLabel36"
            Me.XrLabel36.SizeF = New System.Drawing.SizeF(91.0!, 15.0!)
            Me.XrLabel36.Text = "CURR BAL"
            '
            'ParameterCreditor
            '
            Me.ParameterCreditor.Description = "Creditor"
            Me.ParameterCreditor.Name = "ParameterCreditor"
            Me.ParameterCreditor.Value = ""
            '
            'CreditorDebtTransactionsReport
            '
            Me.Bands.AddRange(New DevExpress.XtraReports.UI.Band() {Me.Detail, Me.PageHeader, Me.PageFooter, Me.TopMarginBand1, Me.BottomMarginBand1, Me.ReportFooter, Me.GroupHeader1, Me.GroupHeader2, Me.GroupFooter1, Me.GroupFooter2})
            Me.Landscape = True
            Me.PageHeight = 850
            Me.PageWidth = 1100
            Me.Parameters.AddRange(New DevExpress.XtraReports.Parameters.Parameter() {Me.ParameterCreditor})
            Me.ScriptLanguage = DevExpress.XtraReports.ScriptLanguage.VisualBasic
            Me.ScriptReferencesString = "C:\Documents and Settings\Al Longyear\My Documents\Visual Studio 2008\Projects\De" & _
        "btPlus\Executables\DebtPlus.Data.dll"
            Me.Scripts.OnBeforePrint = "CreditorDebtTransactionsReport_BeforePrint"
            Me.ScriptsSource = resources.GetString("$this.ScriptsSource")
            Me.StyleSheet.AddRange(New DevExpress.XtraReports.UI.XRControlStyle() {Me.XrControlStyle_Header, Me.XrControlStyle_Totals, Me.XrControlStyle_HeaderPannel, Me.XrControlStyle_GroupHeader})
            Me.Version = "11.2"
            Me.Controls.SetChildIndex(Me.GroupFooter2, 0)
            Me.Controls.SetChildIndex(Me.GroupFooter1, 0)
            Me.Controls.SetChildIndex(Me.GroupHeader2, 0)
            Me.Controls.SetChildIndex(Me.GroupHeader1, 0)
            Me.Controls.SetChildIndex(Me.ReportFooter, 0)
            Me.Controls.SetChildIndex(Me.BottomMarginBand1, 0)
            Me.Controls.SetChildIndex(Me.TopMarginBand1, 0)
            Me.Controls.SetChildIndex(Me.PageFooter, 0)
            Me.Controls.SetChildIndex(Me.PageHeader, 0)
            Me.Controls.SetChildIndex(Me.Detail, 0)
            CType(Me, System.ComponentModel.ISupportInitialize).EndInit()

        End Sub
        Friend WithEvents ReportFooter As DevExpress.XtraReports.UI.ReportFooterBand
        Friend WithEvents XrLine1 As DevExpress.XtraReports.UI.XRLine
        Friend WithEvents XrLabel8 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_creditor As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_creditor_name As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrPanel2 As DevExpress.XtraReports.UI.XRPanel
        Friend WithEvents XrLabel1 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel5 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel9 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel11 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel12 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel13 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel14 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel15 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel2 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents GroupHeader1 As DevExpress.XtraReports.UI.GroupHeaderBand
        Friend WithEvents GroupHeader2 As DevExpress.XtraReports.UI.GroupHeaderBand
        Friend WithEvents XrLabel_office_name As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents GroupFooter1 As DevExpress.XtraReports.UI.GroupFooterBand
        Friend WithEvents GroupFooter2 As DevExpress.XtraReports.UI.GroupFooterBand
        Friend WithEvents XrLabel_start_date As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_account_number As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_counselor_name As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_active_status As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_state As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_disbursement_factor As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_total_payments As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_current_balance As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_client As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel35 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel34 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel36 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_group_total_payments As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_group_disbursement_factor As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel28 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_group_current_balance As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_total_disbursement_factor As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_total_current_balance As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel20 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_total_total_payments As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents ParameterCreditor As DevExpress.XtraReports.Parameters.Parameter
    End Class
End Namespace
