#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}
#End Region

Namespace Creditor.Transactions.ByDebt
    Public Class CreditorDebtTransactionsReport
        Implements DebtPlus.Interfaces.Creditor.ICreditor

        ''' <summary>
        ''' Create an instance of our report
        ''' </summary>
        Public Sub New()
            MyClass.New(DebtPlus.Utils.DateRange.Today)
        End Sub

        Public Sub New(ByVal DateRange As DebtPlus.Utils.DateRange)
            MyBase.New(DateRange)
            InitializeComponent()

            AddHandler BeforePrint, AddressOf Report_BeforePrint
            AddHandler XrLabel_client.BeforePrint, AddressOf XrLabel_client_BeforePrint
            AddHandler ParametersRequestBeforeShow, AddressOf Report_ParametersRequestBeforeShow
        End Sub

        Public Property Creditor As String Implements DebtPlus.Interfaces.Creditor.ICreditor.Creditor
            Get
                Return Parameter_Creditor
            End Get
            Set(value As String)
                Parameter_Creditor = value
            End Set
        End Property

        Public Property Parameter_Creditor() As String
            Get
                Dim parm As DevExpress.XtraReports.Parameters.Parameter = FindParameter("ParameterCreditor")
                If parm Is Nothing Then Return String.Empty
                Return Convert.ToString(parm)
            End Get
            Set(ByVal value As String)
                SetParameter("ParameterCreditor", GetType(String), value, "Creditor Label", False)
            End Set
        End Property

        Public Overrides ReadOnly Property ReportTitle() As String
            Get
                Return "Debt Transactions by Creditor"
            End Get
        End Property

        Public Overrides Sub SetReportParameter(ByVal Parameter As String, ByVal Value As Object)
            If Parameter = "Creditor" Then
                Parameter_Creditor = Convert.ToString(Value)
            Else
                MyBase.SetReportParameter(Parameter, Value)
            End If
        End Sub

        Public Overrides Function NeedParameters() As Boolean
            Return MyBase.NeedParameters OrElse Parameter_Creditor = String.Empty
        End Function

        Public Overrides Function RequestReportParameters() As System.Windows.Forms.DialogResult
            Dim Answer As System.Windows.Forms.DialogResult = DialogResult.OK

            If NeedParameters() Then
                Using frm As New DebtPlus.Reports.Template.Forms.DatedCreditorParametersForm(ReportDateRange)
                    frm.Parameter_Creditor = Parameter_Creditor
                    Answer = frm.ShowDialog()
                    Parameter_FromDate = frm.Parameter_FromDate
                    Parameter_ToDate = frm.Parameter_ToDate
                    Parameter_Creditor = frm.Parameter_Creditor
                End Using
            End If

            Return Answer
        End Function

        Dim ds As New System.Data.DataSet("ds")

        Private Sub Report_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs)
            Const TableName As String = "view_creditor_debt_transactions"
            Dim Rpt As DevExpress.XtraReports.UI.XtraReport = CType(sender, DevExpress.XtraReports.UI.XtraReport)
            Dim sqlInfo As DebtPlus.LINQ.SQLInfoClass = DebtPlus.LINQ.SQLInfoClass.getDefault()

            Dim tbl As System.Data.DataTable = ds.Tables(TableName)
            If tbl IsNot Nothing Then
                tbl.Clear()
            End If

            Try
                Using cn As New System.Data.SqlClient.SqlConnection(sqlInfo.ConnectionString)
                    Using cmd As New System.Data.SqlClient.SqlCommand()
                        cmd.Connection = New System.Data.SqlClient.SqlConnection(sqlInfo.ConnectionString)
                        cmd.CommandText = "SELECT * FROM view_creditor_debt_transactions WHERE [creditor]=@creditor AND [date_created] >= @FromDate AND [date_created] < @ToDate"
                        cmd.CommandType = System.Data.CommandType.Text
                        cmd.Parameters.Add("@Creditor", System.Data.SqlDbType.VarChar, 10).Value = Rpt.Parameters("ParameterCreditor").Value
                        cmd.Parameters.Add("@FromDate", System.Data.SqlDbType.DateTime).Value = CType(Rpt.Parameters("ParameterFromDate").Value, DateTime).Date
                        cmd.Parameters.Add("@ToDate", System.Data.SqlDbType.DateTime).Value = CType(Rpt.Parameters("ParameterToDate").Value, DateTime).Date.AddDays(1)
                        cmd.CommandTimeout = 0

                        Using da As New System.Data.SqlClient.SqlDataAdapter(cmd)
                            da.Fill(ds, TableName)
                            tbl = ds.Tables(TableName)

                            Rpt.DataSource = New System.Data.DataView(tbl, String.Empty, "office, client", System.Data.DataViewRowState.CurrentRows)

                            For Each calc As DevExpress.XtraReports.UI.CalculatedField In Rpt.CalculatedFields
                                calc.Assign(Rpt.DataSource, Rpt.DataMember)
                            Next
                        End Using
                    End Using
                End Using

            Catch ex As System.Data.SqlClient.SqlException
                Using gdr As New DebtPlus.Repository.GetDataResult()
                    gdr.HandleException(ex)
                    DebtPlus.UI.Common.ErrorHandling.HandleErrors(gdr, "Error reading creditor transactions")
                End Using
            End Try
        End Sub

        Private Sub XrLabel_client_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs)
            Dim lbl As DevExpress.XtraReports.UI.XRLabel = CType(sender, DevExpress.XtraReports.UI.XRLabel)
            lbl.Text = DebtPlus.Utils.Format.Client.FormatClientID(lbl.Report.GetCurrentColumnValue("client")) + " " + DebtPlus.Utils.Nulls.DStr(lbl.Report.GetCurrentColumnValue("client_name"))
        End Sub

        Private Sub Report_ParametersRequestBeforeShow(ByVal sender As Object, ByVal e As DevExpress.XtraReports.Parameters.ParametersRequestEventArgs)
            For Each pi As DevExpress.XtraReports.Parameters.ParameterInfo In e.ParametersInformation
                If pi.Parameter.Name = "ParameterCreditor" Then
                    pi.Editor = New DebtPlus.UI.Creditor.Widgets.Controls.CreditorID
                End If
            Next
        End Sub
    End Class
End Namespace
