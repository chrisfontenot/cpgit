#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}

#End Region

Option Compare Binary
Option Explicit On
Option Strict On

Imports System.ComponentModel
Imports DebtPlus.Reports.Template

Imports System.Drawing.Printing
Imports System.Data.SqlClient
Imports DevExpress.XtraReports.UI

Namespace Creditor.Distributions.Billed.Recvd
    Public Class CreditorDistributionsReport
        Inherits DatedTemplateXtraReportClass

        Public Sub New()
            MyBase.New()
            InitializeComponent()
            AddHandler Me.BeforePrint, AddressOf CreditorContributionsReport_BeforePrint
            AddHandler XrLabel_creditor_name.BeforePrint, AddressOf XrLabel_creditor_name_BeforePrint
        End Sub

        Public Overrides ReadOnly Property ReportTitle() As String
            Get
                Return "Creditor Disbursement Stats"
            End Get
        End Property

        Dim ds As New DataSet("ds")

        Private Sub CreditorContributionsReport_BeforePrint(ByVal sender As Object, ByVal e As PrintEventArgs)
            Dim tbl As DataTable = ds.Tables("results")

            If tbl Is Nothing Then
                Dim cn As SqlConnection = New System.Data.SqlClient.SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
                Dim txn As SqlTransaction = Nothing
                Try
                    cn.Open()
                    txn = cn.BeginTransaction(IsolationLevel.ReadUncommitted)

                    ' First, read the disbursement information
                    Using cmd As SqlCommand = New System.Data.SqlClient.SqlCommand()
                        With cmd
                            .Connection = cn
                            .Transaction = txn
                            .CommandText = "SELECT creditor, datepart(month, date_created) as 'month', datepart(year, date_created) as 'year', sum(debit_amt) as 'disbursed' FROM registers_client_creditor WHERE tran_type in ('AD','MD','CM','BW') AND date_created >= @from_date AND date_created < @to_date GROUP BY creditor, datepart(month, date_created), datepart(year, date_created)"
                            .CommandType = CommandType.Text
                            .CommandTimeout = 0
                            .Parameters.Add("@from_date", SqlDbType.DateTime).Value = Parameter_FromDate.Date
                            .Parameters.Add("@to_date", SqlDbType.DateTime).Value = Parameter_ToDate.AddDays(1).Date
                        End With

                        Using da As New SqlDataAdapter(cmd)
                            da.Fill(ds, "disbursements")
                        End Using

                        With ds.Tables("disbursements")
                            .PrimaryKey = New DataColumn() {.Columns(0), .Columns(1), .Columns(2)}
                        End With
                    End Using

                    ' Next read the billed information
                    Using cmd As SqlCommand = New System.Data.SqlClient.SqlCommand()
                        With cmd
                            .Connection = cn
                            .Transaction = txn
                            .CommandText = "SELECT creditor, datepart(month, date_created) as month, datepart(year, date_created) as year, sum(fairshare_amt) as billed FROM registers_client_creditor WHERE tran_type in ('AD','MD','CM','BW') AND creditor_type NOT IN ('D','N') AND date_created >= @from_date AND date_created < @to_date GROUP BY creditor, datepart(month, date_created), datepart(year, date_created)"
                            .CommandType = CommandType.Text
                            .CommandTimeout = 0
                            .Parameters.Add("@from_date", SqlDbType.DateTime).Value = Parameter_FromDate.Date
                            .Parameters.Add("@to_date", SqlDbType.DateTime).Value = Parameter_ToDate.AddDays(1).Date
                        End With

                        Using da As New SqlDataAdapter(cmd)
                            da.Fill(ds, "billed")
                        End Using

                        With ds.Tables("billed")
                            .PrimaryKey = New DataColumn() {.Columns(0), .Columns(1), .Columns(2)}
                        End With
                    End Using

                    ' Next read the billed information
                    Using cmd As SqlCommand = New System.Data.SqlClient.SqlCommand()
                        With cmd
                            .Connection = cn
                            .Transaction = txn
                            .CommandText = "SELECT creditor, datepart(month, date_created) as month, datepart(year, date_created) as year, sum(fairshare_amt) as deducted FROM registers_client_creditor WHERE tran_type in ('AD','MD','CM','BW') AND creditor_type = 'D' AND date_created >= @from_date AND date_created < @to_date GROUP BY creditor, datepart(month, date_created), datepart(year, date_created)"
                            .CommandType = CommandType.Text
                            .CommandTimeout = 0
                            .Parameters.Add("@from_date", SqlDbType.DateTime).Value = Parameter_FromDate.Date
                            .Parameters.Add("@to_date", SqlDbType.DateTime).Value = Parameter_ToDate.AddDays(1).Date
                        End With

                        Using da As New SqlDataAdapter(cmd)
                            da.Fill(ds, "deducted")
                        End Using

                        With ds.Tables("deducted")
                            .PrimaryKey = New DataColumn() {.Columns(0), .Columns(1), .Columns(2)}
                        End With
                    End Using

                    ' Next read the received information
                    Using cmd As SqlCommand = New System.Data.SqlClient.SqlCommand()
                        With cmd
                            .Connection = cn
                            .Transaction = txn
                            .CommandText = "SELECT creditor, datepart(month, date_created) as month, datepart(year, date_created) as year, sum(credit_amt) as received FROM registers_creditor WHERE tran_type = 'RC' AND date_created >= @from_date AND date_created < @to_date GROUP BY creditor, datepart(month, date_created), datepart(year, date_created)"
                            .CommandType = CommandType.Text
                            .CommandTimeout = 0
                            .Parameters.Add("@from_date", SqlDbType.DateTime).Value = Parameter_FromDate.Date
                            .Parameters.Add("@to_date", SqlDbType.DateTime).Value = Parameter_ToDate.AddDays(1).Date
                        End With

                        Using da As New SqlDataAdapter(cmd)
                            da.Fill(ds, "received")
                        End Using

                        With ds.Tables("received")
                            .PrimaryKey = New DataColumn() {.Columns(0), .Columns(1), .Columns(2)}
                        End With
                    End Using

                    ' Finally, pull in the creditor -> name mapping
                    Using cmd As SqlCommand = New System.Data.SqlClient.SqlCommand()
                        With cmd
                            .Connection = cn
                            .Transaction = txn
                            .CommandText = "SELECT creditor, creditor_name FROM creditors"
                            .CommandType = CommandType.Text
                            .CommandTimeout = 0
                        End With

                        Using da As New SqlDataAdapter(cmd)
                            da.Fill(ds, "creditors")
                        End Using

                        With ds.Tables("creditors")
                            .PrimaryKey = New DataColumn() {.Columns(0)}
                        End With
                    End Using

                    txn.Commit()
                    txn.Dispose()
                    txn = Nothing

                Catch ex As SqlException
                    DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error reading database information")

                Finally
                    If txn IsNot Nothing Then
                        Try
                            txn.Rollback()
                        Catch exRollback As Exception
                        End Try
                        txn.Dispose()
                        txn = Nothing
                    End If

                    If cn IsNot Nothing Then cn.Dispose()
                End Try

                ' To the creditor table, add columns to find if there are any references
                ' This prevents us from having to look at each creditor for all dates to
                ' find if there are any pending items.

                Dim rel1 As New DataRelation("creditor_disbursements", ds.Tables("creditors").Columns("creditor"), ds.Tables("disbursements").Columns("creditor"))
                ds.Relations.Add(rel1)

                Dim rel2 As New DataRelation("creditor_billed", ds.Tables("creditors").Columns("creditor"), ds.Tables("billed").Columns("creditor"))
                ds.Relations.Add(rel2)

                Dim rel3 As New DataRelation("creditor_deducted", ds.Tables("creditors").Columns("creditor"), ds.Tables("deducted").Columns("creditor"))
                ds.Relations.Add(rel3)

                Dim rel4 As New DataRelation("creditor_received", ds.Tables("creditors").Columns("creditor"), ds.Tables("received").Columns("creditor"))
                ds.Relations.Add(rel4)

                ' Now, add three columns to the creditors table for the count field
                With ds.Tables("creditors")
                    .Columns.Add("count_disbursements", GetType(Int32), "count(child(creditor_disbursements).creditor)")
                    .Columns.Add("count_billed", GetType(Int32), "count(child(creditor_billed).creditor)")
                    .Columns.Add("count_deducted", GetType(Int32), "count(child(creditor_deducted).creditor)")
                    .Columns.Add("count_received", GetType(Int32), "count(child(creditor_received).creditor)")
                End With

                ' Build a table showing the result information that we want to print.
                ' This is hard, but the report comes from three different sources and needs
                ' to be merged together to form the single entity that is reported

                tbl = New DataTable("results")
                With tbl
                    .Columns.Add("date_created", GetType(DateTime))
                    .Columns.Add("creditor", GetType(String))
                    .Columns.Add("creditor_name", GetType(String))

                    .Columns.Add("disbursed", GetType(Decimal)).AllowDBNull = True
                    .Columns.Add("billed", GetType(Decimal)).AllowDBNull = True
                    .Columns.Add("deducted", GetType(Decimal)).AllowDBNull = True
                    .Columns.Add("received", GetType(Decimal)).AllowDBNull = True

                    .PrimaryKey = New DataColumn() {.Columns(0), .Columns(1)}
                End With
                ds.Tables.Add(tbl)

                ' Load the data from the other three tables
                Dim CreditorView As New DataView(ds.Tables("creditors"), "[count_disbursements]<>0 OR [count_billed]<>0 OR [count_received]<>0", "creditor", DataViewRowState.CurrentRows)

                Dim TransactionDate As New Date(Parameter_FromDate.Year, Parameter_FromDate.Month, 1)
                Do While TransactionDate <= Parameter_ToDate
                    Dim Month As Int32 = TransactionDate.Month
                    Dim Year As Int32 = TransactionDate.Year

                    ' Find the creditors that have information in the other tables
                    For Each row As DataRowView In CreditorView

                        Dim Disbursed As Object = DBNull.Value
                        Dim Billed As Object = DBNull.Value
                        Dim Received As Object = DBNull.Value
                        Dim Deducted As Object = DBNull.Value

                        Dim Creditor As String = Convert.ToString(row("creditor"))
                        Dim DisbursedRow As DataRow = ds.Tables("disbursements").Rows.Find(New Object() {Creditor, Month, Year})
                        If DisbursedRow IsNot Nothing Then Disbursed = DisbursedRow("disbursed")

                        Dim BilledRow As DataRow = ds.Tables("billed").Rows.Find(New Object() {Creditor, Month, Year})
                        If BilledRow IsNot Nothing Then Billed = BilledRow("billed")

                        Dim DeductedRow As DataRow = ds.Tables("deducted").Rows.Find(New Object() {Creditor, Month, Year})
                        If DeductedRow IsNot Nothing Then Deducted = DeductedRow("deducted")

                        Dim ReceivedRow As DataRow = ds.Tables("received").Rows.Find(New Object() {Creditor, Month, Year})
                        If ReceivedRow IsNot Nothing Then Received = ReceivedRow("received")

                        ' Finally, add the item if there are values represented
                        If Disbursed IsNot DBNull.Value OrElse Billed IsNot DBNull.Value OrElse Received IsNot DBNull.Value Then
                            Dim NewRow As DataRow = ds.Tables("results").NewRow
                            NewRow("date_created") = TransactionDate
                            NewRow("creditor") = Creditor
                            NewRow("creditor_name") = row("creditor_name")
                            NewRow("disbursed") = Disbursed
                            NewRow("billed") = Billed
                            NewRow("deducted") = Deducted
                            NewRow("received") = Received

                            ds.Tables("results").Rows.Add(NewRow)
                        End If
                    Next
                    TransactionDate = TransactionDate.AddMonths(1)
                Loop
            End If

            ' Create a view to order the items
            Dim vue As New DataView(tbl, String.Empty, "date_created, creditor", DataViewRowState.CurrentRows)
            DataSource = vue

            ' Add the group breaks to the report
            Dim Group1 As New GroupField("date_created")
            GroupHeader1.GroupFields.Add(Group1)

            ' Bind the items to the columns in the result set
            XrLabel_Month.DataBindings.Add("Text", vue, "date_created", "{0:MMMM, yyyy}")

            XrLabel_disbursed.DataBindings.Add("Text", vue, "disbursed", "{0:c}")
            XrLabel_billed.DataBindings.Add("Text", vue, "billed", "{0:c}")
            XrLabel_deducted.DataBindings.Add("Text", vue, "deducted", "{0:c}")
            XrLabel_received.DataBindings.Add("Text", vue, "received", "{0:c}")

            XrLabel_group_disbursed.DataBindings.Add("Text", vue, "disbursed")
            XrLabel_group_billed.DataBindings.Add("Text", vue, "billed")
            XrLabel_group_deducted.DataBindings.Add("Text", vue, "deducted")
            XrLabel_group_received.DataBindings.Add("Text", vue, "received")

            XrLabel_total_disbursed.DataBindings.Add("Text", vue, "disbursed")
            XrLabel_total_billed.DataBindings.Add("Text", vue, "billed")
            XrLabel_total_deducted.DataBindings.Add("Text", vue, "deducted")
            XrLabel_total_received.DataBindings.Add("Text", vue, "received")
        End Sub

        Private Sub XrLabel_creditor_name_BeforePrint(ByVal sender As Object, ByVal e As PrintEventArgs)
            XrLabel_creditor_name.Text = "[" + Convert.ToString(GetCurrentColumnValue("creditor")) + "] " + Convert.ToString(GetCurrentColumnValue("creditor_name"))
        End Sub
    End Class

End Namespace
