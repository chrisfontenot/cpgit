Namespace Creditor.Distributions.Billed.Recvd
    Partial Class CreditorDistributionsReport

        'Component overrides dispose to clean up the component list.
        Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
            If disposing Then
                If components IsNot Nothing Then
                    components.Dispose()
                End If
            End If
            MyBase.Dispose(disposing)
        End Sub

        'Required by the Designer
        Private components As System.ComponentModel.IContainer

        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
            Dim XrSummary1 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary
            Dim XrSummary2 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary
            Dim XrSummary3 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary
            Dim XrSummary4 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary
            Dim XrSummary5 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary
            Dim XrSummary6 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary
            Dim XrSummary7 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary
            Dim XrSummary8 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary
            Me.XrLabel_creditor_name = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_disbursed = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_billed = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_deducted = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_received = New DevExpress.XtraReports.UI.XRLabel
            Me.XrPanel1 = New DevExpress.XtraReports.UI.XRPanel
            Me.XrLabel3 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel8 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel7 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel5 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel2 = New DevExpress.XtraReports.UI.XRLabel
            Me.GroupHeader1 = New DevExpress.XtraReports.UI.GroupHeaderBand
            Me.XrLabel_Month = New DevExpress.XtraReports.UI.XRLabel
            Me.GroupFooter1 = New DevExpress.XtraReports.UI.GroupFooterBand
            Me.XrLabel_group_disbursed = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_group_deducted = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_group_billed = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_group_received = New DevExpress.XtraReports.UI.XRLabel
            Me.ReportFooter = New DevExpress.XtraReports.UI.ReportFooterBand
            Me.XrLine1 = New DevExpress.XtraReports.UI.XRLine
            Me.XrLabel1 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_total_deducted = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_total_received = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_total_disbursed = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_total_billed = New DevExpress.XtraReports.UI.XRLabel
            CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
            '
            'Detail
            '
            Me.Detail.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel_received, Me.XrLabel_deducted, Me.XrLabel_billed, Me.XrLabel_disbursed, Me.XrLabel_creditor_name})
            Me.Detail.HeightF = 15.0!
            '
            'PageHeader
            '
            Me.PageHeader.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrPanel1})
            Me.PageHeader.HeightF = 160.0!
            Me.PageHeader.Controls.SetChildIndex(Me.XrPanel1, 0)
            Me.PageHeader.Controls.SetChildIndex(Me.XrPanel_AgencyAddress, 0)
            Me.PageHeader.Controls.SetChildIndex(Me.XrLabel_Title, 0)
            Me.PageHeader.Controls.SetChildIndex(Me.XrLine_Title, 0)
            Me.PageHeader.Controls.SetChildIndex(Me.XrLabel_Subtitle, 0)
            Me.PageHeader.Controls.SetChildIndex(Me.XrPageInfo1, 0)
            '
            'XrLabel_Title
            '
            Me.XrLabel_Title.StylePriority.UseFont = False
            '
            'XrPageInfo_PageNumber
            '
            Me.XrPageInfo_PageNumber.StylePriority.UseTextAlignment = False
            '
            'XRLabel_Agency_Name
            '
            Me.XRLabel_Agency_Name.StylePriority.UseTextAlignment = False
            '
            'XrLabel_Agency_Address3
            '
            Me.XrLabel_Agency_Address3.StylePriority.UseFont = False
            Me.XrLabel_Agency_Address3.StylePriority.UseTextAlignment = False
            '
            'XrLabel_Agency_Address1
            '
            Me.XrLabel_Agency_Address1.StylePriority.UseFont = False
            Me.XrLabel_Agency_Address1.StylePriority.UseTextAlignment = False
            '
            'XrLabel_Agency_Phone
            '
            Me.XrLabel_Agency_Phone.StylePriority.UseFont = False
            Me.XrLabel_Agency_Phone.StylePriority.UseTextAlignment = False
            '
            'XrLabel_Agency_Address2
            '
            Me.XrLabel_Agency_Address2.StylePriority.UseFont = False
            Me.XrLabel_Agency_Address2.StylePriority.UseTextAlignment = False
            '
            'XrLabel_creditor_name
            '
            Me.XrLabel_creditor_name.LocationFloat = New DevExpress.Utils.PointFloat(10.00001!, 0.0!)
            Me.XrLabel_creditor_name.Name = "XrLabel_creditor_name"
            Me.XrLabel_creditor_name.SizeF = New System.Drawing.SizeF(318.4167!, 15.0!)
            Me.XrLabel_creditor_name.StylePriority.UseTextAlignment = False
            Me.XrLabel_creditor_name.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            Me.XrLabel_creditor_name.WordWrap = False
            '
            'XrLabel_disbursed
            '
            Me.XrLabel_disbursed.CanGrow = False
            Me.XrLabel_disbursed.LocationFloat = New DevExpress.Utils.PointFloat(332.7917!, 0.0!)
            Me.XrLabel_disbursed.Name = "XrLabel_disbursed"
            Me.XrLabel_disbursed.SizeF = New System.Drawing.SizeF(126.6667!, 14.99999!)
            Me.XrLabel_disbursed.StylePriority.UseTextAlignment = False
            Me.XrLabel_disbursed.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrLabel_disbursed.WordWrap = False
            '
            'XrLabel_billed
            '
            Me.XrLabel_billed.CanGrow = False
            Me.XrLabel_billed.LocationFloat = New DevExpress.Utils.PointFloat(471.9583!, 0.0!)
            Me.XrLabel_billed.Name = "XrLabel_billed"
            Me.XrLabel_billed.SizeF = New System.Drawing.SizeF(91.0!, 15.0!)
            Me.XrLabel_billed.StylePriority.UseTextAlignment = False
            Me.XrLabel_billed.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrLabel_billed.WordWrap = False
            '
            'XrLabel_deducted
            '
            Me.XrLabel_deducted.CanGrow = False
            Me.XrLabel_deducted.LocationFloat = New DevExpress.Utils.PointFloat(575.4583!, 0.0!)
            Me.XrLabel_deducted.Name = "XrLabel_deducted"
            Me.XrLabel_deducted.SizeF = New System.Drawing.SizeF(100.0!, 15.0!)
            Me.XrLabel_deducted.StylePriority.UseTextAlignment = False
            Me.XrLabel_deducted.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrLabel_deducted.WordWrap = False
            '
            'XrLabel_received
            '
            Me.XrLabel_received.CanGrow = False
            Me.XrLabel_received.LocationFloat = New DevExpress.Utils.PointFloat(687.9583!, 0.0!)
            Me.XrLabel_received.Name = "XrLabel_received"
            Me.XrLabel_received.SizeF = New System.Drawing.SizeF(107.0!, 15.0!)
            Me.XrLabel_received.StylePriority.UseTextAlignment = False
            Me.XrLabel_received.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrLabel_received.WordWrap = False
            '
            'XrPanel1
            '
            Me.XrPanel1.BackColor = System.Drawing.Color.Teal
            Me.XrPanel1.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel3, Me.XrLabel8, Me.XrLabel7, Me.XrLabel5, Me.XrLabel2})
            Me.XrPanel1.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 133.0!)
            Me.XrPanel1.Name = "XrPanel1"
            Me.XrPanel1.SizeF = New System.Drawing.SizeF(800.0!, 17.0!)
            Me.XrPanel1.StyleName = "XrControlStyle_HeaderPannel"
            '
            'XrLabel3
            '
            Me.XrLabel3.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel3.ForeColor = System.Drawing.Color.White
            Me.XrLabel3.LocationFloat = New DevExpress.Utils.PointFloat(374.4583!, 1.0!)
            Me.XrLabel3.Name = "XrLabel3"
            Me.XrLabel3.SizeF = New System.Drawing.SizeF(85.0!, 15.0!)
            Me.XrLabel3.Text = "DISBURSED"
            Me.XrLabel3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
            '
            'XrLabel8
            '
            Me.XrLabel8.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel8.ForeColor = System.Drawing.Color.White
            Me.XrLabel8.LocationFloat = New DevExpress.Utils.PointFloat(575.4583!, 1.0!)
            Me.XrLabel8.Name = "XrLabel8"
            Me.XrLabel8.SizeF = New System.Drawing.SizeF(100.0!, 15.0!)
            Me.XrLabel8.Text = "DEDUCTED"
            Me.XrLabel8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
            '
            'XrLabel7
            '
            Me.XrLabel7.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel7.ForeColor = System.Drawing.Color.White
            Me.XrLabel7.LocationFloat = New DevExpress.Utils.PointFloat(471.9583!, 1.0!)
            Me.XrLabel7.Name = "XrLabel7"
            Me.XrLabel7.SizeF = New System.Drawing.SizeF(91.0!, 15.0!)
            Me.XrLabel7.Text = "INVOICED"
            Me.XrLabel7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
            '
            'XrLabel5
            '
            Me.XrLabel5.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel5.ForeColor = System.Drawing.Color.White
            Me.XrLabel5.LocationFloat = New DevExpress.Utils.PointFloat(687.9583!, 1.0!)
            Me.XrLabel5.Name = "XrLabel5"
            Me.XrLabel5.SizeF = New System.Drawing.SizeF(107.0!, 15.0!)
            Me.XrLabel5.Text = "RECEIVED"
            Me.XrLabel5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
            '
            'XrLabel2
            '
            Me.XrLabel2.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel2.ForeColor = System.Drawing.Color.White
            Me.XrLabel2.LocationFloat = New DevExpress.Utils.PointFloat(10.00001!, 2.000014!)
            Me.XrLabel2.Name = "XrLabel2"
            Me.XrLabel2.SizeF = New System.Drawing.SizeF(250.0!, 15.0!)
            Me.XrLabel2.StylePriority.UseTextAlignment = False
            Me.XrLabel2.Text = "CREDITOR"
            Me.XrLabel2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
            '
            'GroupHeader1
            '
            Me.GroupHeader1.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel_Month})
            Me.GroupHeader1.GroupFields.AddRange(New DevExpress.XtraReports.UI.GroupField() {New DevExpress.XtraReports.UI.GroupField("sic", DevExpress.XtraReports.UI.XRColumnSortOrder.Ascending)})
            Me.GroupHeader1.HeightF = 42.0!
            Me.GroupHeader1.Name = "GroupHeader1"
            '
            'XrLabel_Month
            '
            Me.XrLabel_Month.Font = New System.Drawing.Font("Times New Roman", 12.0!, System.Drawing.FontStyle.Bold)
            Me.XrLabel_Month.ForeColor = System.Drawing.Color.Maroon
            Me.XrLabel_Month.LocationFloat = New DevExpress.Utils.PointFloat(10.00001!, 7.000001!)
            Me.XrLabel_Month.Name = "XrLabel_Month"
            Me.XrLabel_Month.Padding = New DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 100.0!)
            Me.XrLabel_Month.SizeF = New System.Drawing.SizeF(786.0001!, 25.0!)
            Me.XrLabel_Month.StylePriority.UseFont = False
            Me.XrLabel_Month.StylePriority.UseForeColor = False
            Me.XrLabel_Month.StylePriority.UsePadding = False
            Me.XrLabel_Month.StylePriority.UseTextAlignment = False
            Me.XrLabel_Month.Text = "Janurary, 2010"
            Me.XrLabel_Month.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            '
            'GroupFooter1
            '
            Me.GroupFooter1.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel_group_disbursed, Me.XrLabel_group_deducted, Me.XrLabel_group_billed, Me.XrLabel_group_received})
            Me.GroupFooter1.HeightF = 27.0!
            Me.GroupFooter1.Name = "GroupFooter1"
            Me.GroupFooter1.StyleName = "XrControlStyle_Totals"
            '
            'XrLabel_group_disbursed
            '
            Me.XrLabel_group_disbursed.Borders = DevExpress.XtraPrinting.BorderSide.Top
            Me.XrLabel_group_disbursed.BorderWidth = 2
            Me.XrLabel_group_disbursed.LocationFloat = New DevExpress.Utils.PointFloat(332.7917!, 0.0!)
            Me.XrLabel_group_disbursed.Name = "XrLabel_group_disbursed"
            Me.XrLabel_group_disbursed.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_group_disbursed.SizeF = New System.Drawing.SizeF(126.6667!, 14.99999!)
            Me.XrLabel_group_disbursed.StylePriority.UseBorders = False
            Me.XrLabel_group_disbursed.StylePriority.UseBorderWidth = False
            Me.XrLabel_group_disbursed.StylePriority.UseTextAlignment = False
            XrSummary1.FormatString = "{0:c}"
            XrSummary1.IgnoreNullValues = True
            XrSummary1.Running = DevExpress.XtraReports.UI.SummaryRunning.Group
            Me.XrLabel_group_disbursed.Summary = XrSummary1
            Me.XrLabel_group_disbursed.Text = "$0.00"
            Me.XrLabel_group_disbursed.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel_group_deducted
            '
            Me.XrLabel_group_deducted.Borders = DevExpress.XtraPrinting.BorderSide.Top
            Me.XrLabel_group_deducted.BorderWidth = 2
            Me.XrLabel_group_deducted.LocationFloat = New DevExpress.Utils.PointFloat(575.4583!, 0.0!)
            Me.XrLabel_group_deducted.Name = "XrLabel_group_deducted"
            Me.XrLabel_group_deducted.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_group_deducted.SizeF = New System.Drawing.SizeF(100.0!, 15.0!)
            Me.XrLabel_group_deducted.StylePriority.UseBorders = False
            Me.XrLabel_group_deducted.StylePriority.UseBorderWidth = False
            Me.XrLabel_group_deducted.StylePriority.UseTextAlignment = False
            XrSummary2.FormatString = "{0:c}"
            XrSummary2.IgnoreNullValues = True
            XrSummary2.Running = DevExpress.XtraReports.UI.SummaryRunning.Group
            Me.XrLabel_group_deducted.Summary = XrSummary2
            Me.XrLabel_group_deducted.Text = "$0.00"
            Me.XrLabel_group_deducted.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel_group_billed
            '
            Me.XrLabel_group_billed.Borders = DevExpress.XtraPrinting.BorderSide.Top
            Me.XrLabel_group_billed.BorderWidth = 2
            Me.XrLabel_group_billed.LocationFloat = New DevExpress.Utils.PointFloat(471.9583!, 0.0!)
            Me.XrLabel_group_billed.Name = "XrLabel_group_billed"
            Me.XrLabel_group_billed.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_group_billed.SizeF = New System.Drawing.SizeF(91.0!, 15.0!)
            Me.XrLabel_group_billed.StylePriority.UseBorders = False
            Me.XrLabel_group_billed.StylePriority.UseBorderWidth = False
            Me.XrLabel_group_billed.StylePriority.UseTextAlignment = False
            XrSummary3.FormatString = "{0:c}"
            XrSummary3.IgnoreNullValues = True
            XrSummary3.Running = DevExpress.XtraReports.UI.SummaryRunning.Group
            Me.XrLabel_group_billed.Summary = XrSummary3
            Me.XrLabel_group_billed.Text = "$0.00"
            Me.XrLabel_group_billed.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel_group_received
            '
            Me.XrLabel_group_received.Borders = DevExpress.XtraPrinting.BorderSide.Top
            Me.XrLabel_group_received.BorderWidth = 2
            Me.XrLabel_group_received.LocationFloat = New DevExpress.Utils.PointFloat(687.9583!, 0.0!)
            Me.XrLabel_group_received.Name = "XrLabel_group_received"
            Me.XrLabel_group_received.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_group_received.SizeF = New System.Drawing.SizeF(107.0!, 15.0!)
            Me.XrLabel_group_received.StylePriority.UseBorders = False
            Me.XrLabel_group_received.StylePriority.UseBorderWidth = False
            Me.XrLabel_group_received.StylePriority.UseTextAlignment = False
            XrSummary4.FormatString = "{0:c}"
            XrSummary4.IgnoreNullValues = True
            XrSummary4.Running = DevExpress.XtraReports.UI.SummaryRunning.Group
            Me.XrLabel_group_received.Summary = XrSummary4
            Me.XrLabel_group_received.Text = "$0.00"
            Me.XrLabel_group_received.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'ReportFooter
            '
            Me.ReportFooter.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLine1, Me.XrLabel1, Me.XrLabel_total_deducted, Me.XrLabel_total_received, Me.XrLabel_total_disbursed, Me.XrLabel_total_billed})
            Me.ReportFooter.HeightF = 27.0!
            Me.ReportFooter.Name = "ReportFooter"
            Me.ReportFooter.StyleName = "XrControlStyle_Totals"
            '
            'XrLine1
            '
            Me.XrLine1.BorderWidth = 1
            Me.XrLine1.ForeColor = System.Drawing.Color.Teal
            Me.XrLine1.LineWidth = 3
            Me.XrLine1.LocationFloat = New DevExpress.Utils.PointFloat(332.7917!, 2.000014!)
            Me.XrLine1.Name = "XrLine1"
            Me.XrLine1.SizeF = New System.Drawing.SizeF(463.2083!, 7.999992!)
            Me.XrLine1.StylePriority.UseBorderWidth = False
            Me.XrLine1.StylePriority.UseForeColor = False
            '
            'XrLabel1
            '
            Me.XrLabel1.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 10.0!)
            Me.XrLabel1.Name = "XrLabel1"
            Me.XrLabel1.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel1.SizeF = New System.Drawing.SizeF(100.0!, 17.0!)
            Me.XrLabel1.StylePriority.UseTextAlignment = False
            Me.XrLabel1.Text = "TOTALS"
            Me.XrLabel1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
            '
            'XrLabel_total_deducted
            '
            Me.XrLabel_total_deducted.LocationFloat = New DevExpress.Utils.PointFloat(575.4583!, 10.00001!)
            Me.XrLabel_total_deducted.Name = "XrLabel_total_deducted"
            Me.XrLabel_total_deducted.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_total_deducted.SizeF = New System.Drawing.SizeF(100.0!, 15.0!)
            Me.XrLabel_total_deducted.StylePriority.UseTextAlignment = False
            XrSummary5.FormatString = "{0:c}"
            XrSummary5.IgnoreNullValues = True
            XrSummary5.Running = DevExpress.XtraReports.UI.SummaryRunning.Report
            Me.XrLabel_total_deducted.Summary = XrSummary5
            Me.XrLabel_total_deducted.Text = "$0.00"
            Me.XrLabel_total_deducted.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel_total_received
            '
            Me.XrLabel_total_received.LocationFloat = New DevExpress.Utils.PointFloat(687.9583!, 10.00001!)
            Me.XrLabel_total_received.Name = "XrLabel_total_received"
            Me.XrLabel_total_received.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_total_received.SizeF = New System.Drawing.SizeF(107.0!, 15.0!)
            Me.XrLabel_total_received.StylePriority.UseTextAlignment = False
            XrSummary6.FormatString = "{0:c}"
            XrSummary6.IgnoreNullValues = True
            XrSummary6.Running = DevExpress.XtraReports.UI.SummaryRunning.Report
            Me.XrLabel_total_received.Summary = XrSummary6
            Me.XrLabel_total_received.Text = "$0.00"
            Me.XrLabel_total_received.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel_total_disbursed
            '
            Me.XrLabel_total_disbursed.LocationFloat = New DevExpress.Utils.PointFloat(332.7917!, 10.00001!)
            Me.XrLabel_total_disbursed.Name = "XrLabel_total_disbursed"
            Me.XrLabel_total_disbursed.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_total_disbursed.SizeF = New System.Drawing.SizeF(126.6667!, 15.0!)
            Me.XrLabel_total_disbursed.StylePriority.UseTextAlignment = False
            XrSummary7.FormatString = "{0:c}"
            XrSummary7.IgnoreNullValues = True
            XrSummary7.Running = DevExpress.XtraReports.UI.SummaryRunning.Report
            Me.XrLabel_total_disbursed.Summary = XrSummary7
            Me.XrLabel_total_disbursed.Text = "$0.00"
            Me.XrLabel_total_disbursed.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel_total_billed
            '
            Me.XrLabel_total_billed.LocationFloat = New DevExpress.Utils.PointFloat(471.9583!, 10.00001!)
            Me.XrLabel_total_billed.Name = "XrLabel_total_billed"
            Me.XrLabel_total_billed.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_total_billed.SizeF = New System.Drawing.SizeF(91.0!, 15.0!)
            Me.XrLabel_total_billed.StylePriority.UseTextAlignment = False
            XrSummary8.FormatString = "{0:c}"
            XrSummary8.IgnoreNullValues = True
            XrSummary8.Running = DevExpress.XtraReports.UI.SummaryRunning.Report
            Me.XrLabel_total_billed.Summary = XrSummary8
            Me.XrLabel_total_billed.Text = "$0.00"
            Me.XrLabel_total_billed.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'CreditorDistributionsReport
            '
            Me.Bands.AddRange(New DevExpress.XtraReports.UI.Band() {Me.Detail, Me.PageHeader, Me.PageFooter, Me.GroupHeader1, Me.GroupFooter1, Me.ReportFooter})
            Me.Version = "9.3"
            Me.Controls.SetChildIndex(Me.ReportFooter, 0)
            Me.Controls.SetChildIndex(Me.GroupFooter1, 0)
            Me.Controls.SetChildIndex(Me.GroupHeader1, 0)
            Me.Controls.SetChildIndex(Me.PageFooter, 0)
            Me.Controls.SetChildIndex(Me.PageHeader, 0)
            Me.Controls.SetChildIndex(Me.Detail, 0)
            CType(Me, System.ComponentModel.ISupportInitialize).EndInit()

        End Sub
        Friend WithEvents XrLabel_creditor_name As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_disbursed As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_billed As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_deducted As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_received As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrPanel1 As DevExpress.XtraReports.UI.XRPanel
        Friend WithEvents XrLabel2 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel5 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel7 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel8 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents GroupHeader1 As DevExpress.XtraReports.UI.GroupHeaderBand
        Friend WithEvents GroupFooter1 As DevExpress.XtraReports.UI.GroupFooterBand
        Friend WithEvents XrLabel_group_disbursed As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_group_deducted As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_group_billed As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_group_received As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents ReportFooter As DevExpress.XtraReports.UI.ReportFooterBand
        Friend WithEvents XrLabel_total_deducted As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_total_received As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_total_disbursed As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_total_billed As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLine1 As DevExpress.XtraReports.UI.XRLine
        Friend WithEvents XrLabel1 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel3 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_Month As DevExpress.XtraReports.UI.XRLabel
    End Class
End Namespace
