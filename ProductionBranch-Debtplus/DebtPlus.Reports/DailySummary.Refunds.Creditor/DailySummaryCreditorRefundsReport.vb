#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}
#End Region

Option Compare Binary
Option Explicit On
Option Strict On

Imports DebtPlus.Reports.Template

Namespace DailySummary.Refunds.Creditor

    Public Class DailySummaryCreditorRefunds
        Inherits DatedTemplateXtraReportClass

        ''' <summary>
        ''' Create an instance of our report
        ''' </summary>
        Public Sub New()
            MyBase.New()
            InitializeComponent()
            AddHandler Me.BeforePrint, AddressOf DailySummaryCreditorRefunds_BeforePrint
            AddHandler XrLabel_client.BeforePrint, AddressOf XrLabel_client_BeforePrint
        End Sub

        ''' <summary>
        ''' Create an instance of our report
        ''' </summary>
        Public Sub New(ByVal Container As System.ComponentModel.IContainer)
            MyClass.New()
            Container.Add(Me)
        End Sub

#Region " Component Designer generated code "

        'Component overrides dispose to clean up the component list.
        Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
            If disposing Then
                If components IsNot Nothing Then
                    components.Dispose()
                End If
            End If
            MyBase.Dispose(disposing)
        End Sub
        Friend WithEvents XrPanel1 As DevExpress.XtraReports.UI.XRPanel
        Friend WithEvents XrLabel_net As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_creditor As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_client As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel8 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel5 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel9 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents GroupHeader1 As DevExpress.XtraReports.UI.GroupHeaderBand
        Friend WithEvents GroupFooter1 As DevExpress.XtraReports.UI.GroupFooterBand
        Friend WithEvents XrLabel2 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_item_date As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents ReportFooter As DevExpress.XtraReports.UI.ReportFooterBand
        Friend WithEvents XrLabel1 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_deduct As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_gross As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel3 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel4 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_group_gross As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_group_deduct As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_group_net As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_total_gross As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_total_deduct As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_total_net As DevExpress.XtraReports.UI.XRLabel

        'Required by the Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Designer
        'It can be modified using the Designer.
        'Do not modify it using the code editor.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
            Dim XrSummary1 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary
            Dim XrSummary2 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary
            Dim XrSummary3 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary
            Dim XrSummary4 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary
            Dim XrSummary5 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary
            Dim XrSummary6 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary
            Me.XrPanel1 = New DevExpress.XtraReports.UI.XRPanel
            Me.XrLabel8 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel5 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel9 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel3 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel4 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_client = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_creditor = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_net = New DevExpress.XtraReports.UI.XRLabel
            Me.GroupHeader1 = New DevExpress.XtraReports.UI.GroupHeaderBand
            Me.XrLabel_item_date = New DevExpress.XtraReports.UI.XRLabel
            Me.GroupFooter1 = New DevExpress.XtraReports.UI.GroupFooterBand
            Me.XrLabel2 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_group_gross = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_group_deduct = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_group_net = New DevExpress.XtraReports.UI.XRLabel
            Me.ReportFooter = New DevExpress.XtraReports.UI.ReportFooterBand
            Me.XrLabel1 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_total_gross = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_total_deduct = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_total_net = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_deduct = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_gross = New DevExpress.XtraReports.UI.XRLabel
            CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
            '
            'Detail
            '
            Me.Detail.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel_net, Me.XrLabel_creditor, Me.XrLabel_client, Me.XrLabel_deduct, Me.XrLabel_gross})
            Me.Detail.Height = 16
            '
            'PageHeader
            '
            Me.PageHeader.Height = 119
            '
            'XrPanel1
            '
            Me.XrPanel1.BackColor = System.Drawing.Color.Teal
            Me.XrPanel1.BorderColor = System.Drawing.Color.Teal
            Me.XrPanel1.CanGrow = False
            Me.XrPanel1.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel8, Me.XrLabel5, Me.XrLabel9, Me.XrLabel3, Me.XrLabel4})
            Me.XrPanel1.KeepTogether = False
            Me.XrPanel1.Location = New System.Drawing.Point(0, 42)
            Me.XrPanel1.Name = "XrPanel1"
            Me.XrPanel1.Size = New System.Drawing.Size(800, 18)
            Me.XrPanel1.StylePriority.UseBackColor = False
            Me.XrPanel1.StylePriority.UseBorderColor = False
            '
            'XrLabel8
            '
            Me.XrLabel8.BackColor = System.Drawing.Color.Transparent
            Me.XrLabel8.BorderColor = System.Drawing.Color.Transparent
            Me.XrLabel8.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel8.ForeColor = System.Drawing.Color.White
            Me.XrLabel8.Location = New System.Drawing.Point(333, 0)
            Me.XrLabel8.Name = "XrLabel8"
            Me.XrLabel8.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel8.Size = New System.Drawing.Size(191, 16)
            Me.XrLabel8.StylePriority.UseBackColor = False
            Me.XrLabel8.StylePriority.UseBorderColor = False
            Me.XrLabel8.StylePriority.UseFont = False
            Me.XrLabel8.StylePriority.UseForeColor = False
            Me.XrLabel8.StylePriority.UseTextAlignment = False
            Me.XrLabel8.Text = "CREDITOR"
            Me.XrLabel8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            '
            'XrLabel5
            '
            Me.XrLabel5.BackColor = System.Drawing.Color.Transparent
            Me.XrLabel5.BorderColor = System.Drawing.Color.Transparent
            Me.XrLabel5.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel5.ForeColor = System.Drawing.Color.White
            Me.XrLabel5.Location = New System.Drawing.Point(619, 0)
            Me.XrLabel5.Name = "XrLabel5"
            Me.XrLabel5.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel5.Size = New System.Drawing.Size(72, 16)
            Me.XrLabel5.StylePriority.UseBackColor = False
            Me.XrLabel5.StylePriority.UseBorderColor = False
            Me.XrLabel5.StylePriority.UseFont = False
            Me.XrLabel5.StylePriority.UseForeColor = False
            Me.XrLabel5.StylePriority.UseTextAlignment = False
            Me.XrLabel5.Text = "DEDUCT"
            Me.XrLabel5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel9
            '
            Me.XrLabel9.BackColor = System.Drawing.Color.Transparent
            Me.XrLabel9.BorderColor = System.Drawing.Color.Transparent
            Me.XrLabel9.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel9.ForeColor = System.Drawing.Color.White
            Me.XrLabel9.Location = New System.Drawing.Point(8, 0)
            Me.XrLabel9.Name = "XrLabel9"
            Me.XrLabel9.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel9.Size = New System.Drawing.Size(316, 16)
            Me.XrLabel9.StylePriority.UseBackColor = False
            Me.XrLabel9.StylePriority.UseBorderColor = False
            Me.XrLabel9.StylePriority.UseFont = False
            Me.XrLabel9.StylePriority.UseForeColor = False
            Me.XrLabel9.StylePriority.UseTextAlignment = False
            Me.XrLabel9.Text = "Client NAME"
            Me.XrLabel9.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            '
            'XrLabel3
            '
            Me.XrLabel3.BackColor = System.Drawing.Color.Transparent
            Me.XrLabel3.BorderColor = System.Drawing.Color.Transparent
            Me.XrLabel3.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel3.ForeColor = System.Drawing.Color.White
            Me.XrLabel3.Location = New System.Drawing.Point(700, 0)
            Me.XrLabel3.Name = "XrLabel3"
            Me.XrLabel3.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel3.Size = New System.Drawing.Size(83, 16)
            Me.XrLabel3.StylePriority.UseBackColor = False
            Me.XrLabel3.StylePriority.UseBorderColor = False
            Me.XrLabel3.StylePriority.UseFont = False
            Me.XrLabel3.StylePriority.UseForeColor = False
            Me.XrLabel3.StylePriority.UseTextAlignment = False
            Me.XrLabel3.Text = "GROSS"
            Me.XrLabel3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel4
            '
            Me.XrLabel4.BackColor = System.Drawing.Color.Transparent
            Me.XrLabel4.BorderColor = System.Drawing.Color.Transparent
            Me.XrLabel4.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel4.ForeColor = System.Drawing.Color.White
            Me.XrLabel4.Location = New System.Drawing.Point(535, 0)
            Me.XrLabel4.Name = "XrLabel4"
            Me.XrLabel4.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel4.Size = New System.Drawing.Size(75, 16)
            Me.XrLabel4.StylePriority.UseBackColor = False
            Me.XrLabel4.StylePriority.UseBorderColor = False
            Me.XrLabel4.StylePriority.UseFont = False
            Me.XrLabel4.StylePriority.UseForeColor = False
            Me.XrLabel4.StylePriority.UseTextAlignment = False
            Me.XrLabel4.Text = "NET"
            Me.XrLabel4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel_client
            '
            Me.XrLabel_client.CanGrow = False
            Me.XrLabel_client.Location = New System.Drawing.Point(8, 0)
            Me.XrLabel_client.Name = "XrLabel_client"
            Me.XrLabel_client.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_client.Size = New System.Drawing.Size(316, 16)
            Me.XrLabel_client.StylePriority.UseTextAlignment = False
            Me.XrLabel_client.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            Me.XrLabel_client.WordWrap = False
            '
            'XrLabel_creditor
            '
            Me.XrLabel_creditor.CanGrow = False
            Me.XrLabel_creditor.Location = New System.Drawing.Point(333, 0)
            Me.XrLabel_creditor.Name = "XrLabel_creditor"
            Me.XrLabel_creditor.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_creditor.Size = New System.Drawing.Size(191, 16)
            Me.XrLabel_creditor.StylePriority.UseTextAlignment = False
            Me.XrLabel_creditor.Text = "[[creditor]] [creditor_name]"
            Me.XrLabel_creditor.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            Me.XrLabel_creditor.WordWrap = False
            '
            'XrLabel_net
            '
            Me.XrLabel_net.CanGrow = False
            Me.XrLabel_net.Location = New System.Drawing.Point(535, 0)
            Me.XrLabel_net.Name = "XrLabel_net"
            Me.XrLabel_net.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_net.Size = New System.Drawing.Size(75, 16)
            Me.XrLabel_net.StylePriority.UseTextAlignment = False
            Me.XrLabel_net.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'GroupHeader1
            '
            Me.GroupHeader1.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel_item_date, Me.XrPanel1})
            Me.GroupHeader1.GroupFields.AddRange(New DevExpress.XtraReports.UI.GroupField() {New DevExpress.XtraReports.UI.GroupField("modified_item_date", DevExpress.XtraReports.UI.XRColumnSortOrder.Ascending)})
            Me.GroupHeader1.Height = 69
            Me.GroupHeader1.Name = "GroupHeader1"
            '
            'XrLabel_item_date
            '
            Me.XrLabel_item_date.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel_item_date.ForeColor = System.Drawing.Color.Maroon
            Me.XrLabel_item_date.Location = New System.Drawing.Point(0, 8)
            Me.XrLabel_item_date.Name = "XrLabel_item_date"
            Me.XrLabel_item_date.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_item_date.Size = New System.Drawing.Size(800, 17)
            Me.XrLabel_item_date.StylePriority.UseFont = False
            Me.XrLabel_item_date.StylePriority.UseForeColor = False
            Me.XrLabel_item_date.StylePriority.UseTextAlignment = False
            Me.XrLabel_item_date.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
            '
            'GroupFooter1
            '
            Me.GroupFooter1.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel2, Me.XrLabel_group_gross, Me.XrLabel_group_deduct, Me.XrLabel_group_net})
            Me.GroupFooter1.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.GroupFooter1.Height = 32
            Me.GroupFooter1.Name = "GroupFooter1"
            Me.GroupFooter1.StylePriority.UseFont = False
            Me.GroupFooter1.StylePriority.UseTextAlignment = False
            Me.GroupFooter1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel2
            '
            Me.XrLabel2.CanGrow = False
            Me.XrLabel2.Location = New System.Drawing.Point(0, 8)
            Me.XrLabel2.Name = "XrLabel2"
            Me.XrLabel2.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel2.Size = New System.Drawing.Size(314, 16)
            Me.XrLabel2.StylePriority.UseTextAlignment = False
            Me.XrLabel2.Text = "TOTAL"
            Me.XrLabel2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            '
            'XrLabel_group_gross
            '
            Me.XrLabel_group_gross.CanGrow = False
            Me.XrLabel_group_gross.Location = New System.Drawing.Point(700, 8)
            Me.XrLabel_group_gross.Name = "XrLabel_group_gross"
            Me.XrLabel_group_gross.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_group_gross.Size = New System.Drawing.Size(83, 16)
            Me.XrLabel_group_gross.StylePriority.UseTextAlignment = False
            XrSummary1.FormatString = "{0:c}"
            XrSummary1.Running = DevExpress.XtraReports.UI.SummaryRunning.Group
            Me.XrLabel_group_gross.Summary = XrSummary1
            Me.XrLabel_group_gross.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel_group_deduct
            '
            Me.XrLabel_group_deduct.CanGrow = False
            Me.XrLabel_group_deduct.Location = New System.Drawing.Point(619, 8)
            Me.XrLabel_group_deduct.Name = "XrLabel_group_deduct"
            Me.XrLabel_group_deduct.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_group_deduct.Size = New System.Drawing.Size(72, 16)
            Me.XrLabel_group_deduct.StylePriority.UseTextAlignment = False
            XrSummary2.FormatString = "{0:c}"
            XrSummary2.Running = DevExpress.XtraReports.UI.SummaryRunning.Group
            Me.XrLabel_group_deduct.Summary = XrSummary2
            Me.XrLabel_group_deduct.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel_group_net
            '
            Me.XrLabel_group_net.CanGrow = False
            Me.XrLabel_group_net.Location = New System.Drawing.Point(535, 8)
            Me.XrLabel_group_net.Name = "XrLabel_group_net"
            Me.XrLabel_group_net.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_group_net.Size = New System.Drawing.Size(75, 16)
            Me.XrLabel_group_net.StylePriority.UseTextAlignment = False
            XrSummary3.FormatString = "{0:c}"
            XrSummary3.Running = DevExpress.XtraReports.UI.SummaryRunning.Group
            Me.XrLabel_group_net.Summary = XrSummary3
            Me.XrLabel_group_net.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'ReportFooter
            '
            Me.ReportFooter.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel1, Me.XrLabel_total_gross, Me.XrLabel_total_deduct, Me.XrLabel_total_net})
            Me.ReportFooter.Height = 33
            Me.ReportFooter.Name = "ReportFooter"
            '
            'XrLabel1
            '
            Me.XrLabel1.CanGrow = False
            Me.XrLabel1.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel1.Location = New System.Drawing.Point(0, 8)
            Me.XrLabel1.Name = "XrLabel1"
            Me.XrLabel1.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel1.Size = New System.Drawing.Size(314, 16)
            Me.XrLabel1.StylePriority.UseFont = False
            Me.XrLabel1.StylePriority.UseTextAlignment = False
            Me.XrLabel1.Text = "GRAND TOTAL"
            Me.XrLabel1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            '
            'XrLabel_total_gross
            '
            Me.XrLabel_total_gross.CanGrow = False
            Me.XrLabel_total_gross.Location = New System.Drawing.Point(700, 8)
            Me.XrLabel_total_gross.Name = "XrLabel_total_gross"
            Me.XrLabel_total_gross.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_total_gross.Size = New System.Drawing.Size(83, 16)
            Me.XrLabel_total_gross.StylePriority.UseTextAlignment = False
            XrSummary4.FormatString = "{0:c}"
            XrSummary4.Running = DevExpress.XtraReports.UI.SummaryRunning.Report
            Me.XrLabel_total_gross.Summary = XrSummary4
            Me.XrLabel_total_gross.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel_total_deduct
            '
            Me.XrLabel_total_deduct.CanGrow = False
            Me.XrLabel_total_deduct.Location = New System.Drawing.Point(619, 8)
            Me.XrLabel_total_deduct.Name = "XrLabel_total_deduct"
            Me.XrLabel_total_deduct.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_total_deduct.Size = New System.Drawing.Size(72, 16)
            Me.XrLabel_total_deduct.StylePriority.UseTextAlignment = False
            XrSummary5.FormatString = "{0:c}"
            XrSummary5.Running = DevExpress.XtraReports.UI.SummaryRunning.Report
            Me.XrLabel_total_deduct.Summary = XrSummary5
            Me.XrLabel_total_deduct.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel_total_net
            '
            Me.XrLabel_total_net.CanGrow = False
            Me.XrLabel_total_net.Location = New System.Drawing.Point(535, 8)
            Me.XrLabel_total_net.Name = "XrLabel_total_net"
            Me.XrLabel_total_net.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_total_net.Size = New System.Drawing.Size(75, 16)
            Me.XrLabel_total_net.StylePriority.UseTextAlignment = False
            XrSummary6.FormatString = "{0:c}"
            XrSummary6.Running = DevExpress.XtraReports.UI.SummaryRunning.Report
            Me.XrLabel_total_net.Summary = XrSummary6
            Me.XrLabel_total_net.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel_deduct
            '
            Me.XrLabel_deduct.CanGrow = False
            Me.XrLabel_deduct.Location = New System.Drawing.Point(619, 0)
            Me.XrLabel_deduct.Name = "XrLabel_deduct"
            Me.XrLabel_deduct.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_deduct.Size = New System.Drawing.Size(72, 16)
            Me.XrLabel_deduct.StylePriority.UseTextAlignment = False
            Me.XrLabel_deduct.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel_gross
            '
            Me.XrLabel_gross.CanGrow = False
            Me.XrLabel_gross.Location = New System.Drawing.Point(700, 0)
            Me.XrLabel_gross.Name = "XrLabel_gross"
            Me.XrLabel_gross.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_gross.Size = New System.Drawing.Size(83, 16)
            Me.XrLabel_gross.StylePriority.UseTextAlignment = False
            Me.XrLabel_gross.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'DailySummaryCreditorRefunds
            '
            Me.Bands.AddRange(New DevExpress.XtraReports.UI.Band() {Me.Detail, Me.PageHeader, Me.PageFooter, Me.GroupHeader1, Me.GroupFooter1, Me.ReportFooter})
            Me.Version = "8.1"
            Me.Controls.SetChildIndex(Me.ReportFooter, 0)
            Me.Controls.SetChildIndex(Me.GroupFooter1, 0)
            Me.Controls.SetChildIndex(Me.GroupHeader1, 0)
            Me.Controls.SetChildIndex(Me.PageFooter, 0)
            Me.Controls.SetChildIndex(Me.PageHeader, 0)
            Me.Controls.SetChildIndex(Me.Detail, 0)
            CType(Me, System.ComponentModel.ISupportInitialize).EndInit()

        End Sub

#End Region

        Public Overrides ReadOnly Property ReportTitle() As String
            Get
                Return "Creditor Refunds"
            End Get
        End Property

        Dim ds As New System.Data.DataSet("ds")
        Dim vue As System.Data.DataView = Nothing

        Private Sub DailySummaryCreditorRefunds_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs)

            ' Read the transactions
            Dim tbl As System.Data.DataTable = ds.Tables("rpt_Summary_Creditor_Refund")
            If tbl Is Nothing Then
                Dim current_cursor As System.Windows.Forms.Cursor = System.Windows.Forms.Cursor.Current
                Dim cn As SqlClient.SqlConnection = New System.Data.SqlClient.SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
                Try
                    System.Windows.Forms.Cursor.Current = Cursors.WaitCursor
                    cn.Open()
                    Using cmd As SqlClient.SqlCommand = New System.Data.SqlClient.SqlCommand()
                        With cmd
                            .Connection = cn
                            .CommandText = "rpt_Summary_Creditor_Refund"
                            .CommandType = CommandType.StoredProcedure
                            SqlClient.SqlCommandBuilder.DeriveParameters(cmd)

                            ' Parameter(0) is the return value.
                            .Parameters(1).Value = Parameter_FromDate
                            .Parameters(2).Value = Parameter_ToDate
                            .CommandTimeout = 0
                        End With

                        Using da As New SqlClient.SqlDataAdapter(cmd)
                            da.Fill(ds, "rpt_Summary_Creditor_Refund")
                        End Using
                    End Using

                    tbl = ds.Tables("rpt_Summary_Creditor_Refund")
                    With tbl
                        .Columns.Add("modified_item_date", GetType(DateTime))
                        For Each row As System.Data.DataRow In .Rows
                            row("modified_item_date") = Convert.ToDateTime(row("item_date")).Date
                        Next
                        .Columns.Add("net", GetType(Decimal), "[gross]-[deducted]")
                    End With

                Catch ex As Exception
                    DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error reading transactions")

                Finally
                    If cn IsNot Nothing Then cn.Dispose()
                    System.Windows.Forms.Cursor.Current = current_cursor
                End Try
            End If

            ' Bind the datasource to the items
            vue = New System.Data.DataView(tbl, String.Empty, "modified_item_date, creditor, client", DataViewRowState.CurrentRows)
            DataSource = vue

            ' Net amounts
            With XrLabel_net
                .DataBindings.Clear()
                .DataBindings.Add("Text", vue, "net", "{0:c}")
            End With

            With XrLabel_group_net
                .DataBindings.Clear()
                .DataBindings.Add("Text", vue, "net")
            End With

            With XrLabel_total_net
                .DataBindings.Clear()
                .DataBindings.Add("Text", vue, "net")
            End With

            ' Gross amounts
            With XrLabel_gross
                .DataBindings.Clear()
                .DataBindings.Add("Text", vue, "gross", "{0:c}")
            End With

            With XrLabel_group_gross
                .DataBindings.Clear()
                .DataBindings.Add("Text", vue, "gross")
            End With

            With XrLabel_total_gross
                .DataBindings.Clear()
                .DataBindings.Add("Text", vue, "gross")
            End With

            ' Deducted amounts
            With XrLabel_deduct
                .DataBindings.Clear()
                .DataBindings.Add("Text", vue, "deducted", "{0:c}")
            End With

            With XrLabel_group_deduct
                .DataBindings.Clear()
                .DataBindings.Add("Text", vue, "deducted")
            End With

            With XrLabel_total_deduct
                .DataBindings.Clear()
                .DataBindings.Add("Text", vue, "deducted")
            End With

            With XrLabel_item_date
                .DataBindings.Clear()
                .DataBindings.Add("Text", vue, "modified_item_date", "CREDITOR REFUNDS MADE ON {0:d}")
            End With
        End Sub

        Private Sub XrLabel_client_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs)
            With CType(sender, DevExpress.XtraReports.UI.XRLabel)
                Dim ClientID As String = String.Format("{0:0000000}", Convert.ToInt32(GetCurrentColumnValue("client")))
                Dim ClientName As String = DebtPlus.Utils.Nulls.DStr(GetCurrentColumnValue("client_name"))
                If ClientName <> String.Empty Then
                    .Text = ClientID + " " + ClientName
                Else
                    .Text = ClientID
                End If
            End With
        End Sub
    End Class

End Namespace
