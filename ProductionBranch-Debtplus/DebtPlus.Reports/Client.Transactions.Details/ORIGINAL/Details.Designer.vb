Namespace Client.Transactions.Details
    <Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
    Partial Class ClientTransactionsDetailsReport
        Inherits DebtPlus.Reports.Template.DatedTemplateXtraReportClass

        'XtraReport overrides dispose to clean up the component list.
        <System.Diagnostics.DebuggerNonUserCode()> _
        Protected Overrides Sub Dispose(ByVal disposing As Boolean)
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
            MyBase.Dispose(disposing)
        End Sub

        'Required by the Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Designer
        'It can be modified using the Designer.  
        'Do not modify it using the code editor.
        <System.Diagnostics.DebuggerStepThrough()> _
        Private Sub InitializeComponent()
            Dim XrSummary1 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary()
            Dim XrSummary2 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary()
            Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(ClientTransactionsDetailsReport))
            Me.XrPanel1 = New DevExpress.XtraReports.UI.XRPanel()
            Me.XrLabel10 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel5 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel4 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel3 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel2 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel1 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_tran_type = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_date_created = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_credit = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_debit = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_check_number = New DevExpress.XtraReports.UI.XRLabel()
            Me.ReportFooter = New DevExpress.XtraReports.UI.ReportFooterBand()
            Me.XrLabel8 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLine1 = New DevExpress.XtraReports.UI.XRLine()
            Me.XrLabel_Total_Credit = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_Total_Debit = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_ClientID = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_creditor = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_creditor_name = New DevExpress.XtraReports.UI.XRLabel()
            Me.ParameterSubTitle = New DevExpress.XtraReports.Parameters.Parameter()
            Me.ParameterClientRegister = New DevExpress.XtraReports.Parameters.Parameter()
            Me.ClientName = New DevExpress.XtraReports.UI.CalculatedField()
            CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
            '
            'PageHeader
            '
            Me.PageHeader.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrPanel1})
            Me.PageHeader.HeightF = 150.0!
            Me.PageHeader.Controls.SetChildIndex(Me.XrPanel1, 0)
            Me.PageHeader.Controls.SetChildIndex(Me.XrPanel_AgencyAddress, 0)
            Me.PageHeader.Controls.SetChildIndex(Me.XrLabel_Title, 0)
            Me.PageHeader.Controls.SetChildIndex(Me.XrLine_Title, 0)
            Me.PageHeader.Controls.SetChildIndex(Me.XrLabel_Subtitle, 0)
            Me.PageHeader.Controls.SetChildIndex(Me.XrPageInfo1, 0)
            '
            'PageFooter
            '
            Me.PageFooter.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel_ClientID})
            Me.PageFooter.Controls.SetChildIndex(Me.XrLabel_ClientID, 0)
            Me.PageFooter.Controls.SetChildIndex(Me.XrPageInfo_PageNumber, 0)
            '
            'XrLabel_Title
            '
            Me.XrLabel_Title.StylePriority.UseFont = False
            '
            'XrPageInfo_PageNumber
            '
            Me.XrPageInfo_PageNumber.LocationFloat = New DevExpress.Utils.PointFloat(617.0!, 8.0!)
            Me.XrPageInfo_PageNumber.StylePriority.UseTextAlignment = False
            '
            'XRLabel_Agency_Name
            '
            Me.XRLabel_Agency_Name.StylePriority.UseTextAlignment = False
            '
            'XrLabel_Agency_Address3
            '
            Me.XrLabel_Agency_Address3.StylePriority.UseFont = False
            Me.XrLabel_Agency_Address3.StylePriority.UseTextAlignment = False
            '
            'XrLabel_Agency_Address1
            '
            Me.XrLabel_Agency_Address1.StylePriority.UseFont = False
            Me.XrLabel_Agency_Address1.StylePriority.UseTextAlignment = False
            '
            'XrLabel_Agency_Phone
            '
            Me.XrLabel_Agency_Phone.StylePriority.UseFont = False
            Me.XrLabel_Agency_Phone.StylePriority.UseTextAlignment = False
            '
            'XrLabel_Agency_Address2
            '
            Me.XrLabel_Agency_Address2.StylePriority.UseFont = False
            Me.XrLabel_Agency_Address2.StylePriority.UseTextAlignment = False
            '
            'Detail
            '
            Me.Detail.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel_creditor_name, Me.XrLabel_creditor, Me.XrLabel_check_number, Me.XrLabel_debit, Me.XrLabel_credit, Me.XrLabel_date_created, Me.XrLabel_tran_type})
            Me.Detail.HeightF = 15.0!
            '
            'XrPanel1
            '
            Me.XrPanel1.BackColor = System.Drawing.Color.Teal
            Me.XrPanel1.BorderColor = System.Drawing.Color.Teal
            Me.XrPanel1.CanGrow = False
            Me.XrPanel1.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel10, Me.XrLabel5, Me.XrLabel4, Me.XrLabel3, Me.XrLabel2, Me.XrLabel1})
            Me.XrPanel1.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 125.0!)
            Me.XrPanel1.Name = "XrPanel1"
            Me.XrPanel1.SizeF = New System.Drawing.SizeF(742.0!, 16.0!)
            '
            'XrLabel10
            '
            Me.XrLabel10.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel10.ForeColor = System.Drawing.Color.White
            Me.XrLabel10.LocationFloat = New DevExpress.Utils.PointFloat(125.0!, 0.0!)
            Me.XrLabel10.Name = "XrLabel10"
            Me.XrLabel10.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel10.SizeF = New System.Drawing.SizeF(308.0!, 15.0!)
            Me.XrLabel10.Text = "CREDITOR"
            Me.XrLabel10.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            '
            'XrLabel5
            '
            Me.XrLabel5.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel5.ForeColor = System.Drawing.Color.White
            Me.XrLabel5.LocationFloat = New DevExpress.Utils.PointFloat(675.0!, 0.0!)
            Me.XrLabel5.Name = "XrLabel5"
            Me.XrLabel5.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel5.SizeF = New System.Drawing.SizeF(50.0!, 15.0!)
            Me.XrLabel5.StylePriority.UseTextAlignment = False
            Me.XrLabel5.Text = "CHK #"
            Me.XrLabel5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            '
            'XrLabel4
            '
            Me.XrLabel4.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel4.ForeColor = System.Drawing.Color.White
            Me.XrLabel4.LocationFloat = New DevExpress.Utils.PointFloat(558.0!, 0.0!)
            Me.XrLabel4.Name = "XrLabel4"
            Me.XrLabel4.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel4.SizeF = New System.Drawing.SizeF(108.0!, 15.0!)
            Me.XrLabel4.Text = "DEBIT"
            Me.XrLabel4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel3
            '
            Me.XrLabel3.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel3.ForeColor = System.Drawing.Color.White
            Me.XrLabel3.LocationFloat = New DevExpress.Utils.PointFloat(442.0!, 0.0!)
            Me.XrLabel3.Name = "XrLabel3"
            Me.XrLabel3.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel3.SizeF = New System.Drawing.SizeF(108.0!, 15.0!)
            Me.XrLabel3.Text = "CREDIT"
            Me.XrLabel3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel2
            '
            Me.XrLabel2.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel2.ForeColor = System.Drawing.Color.White
            Me.XrLabel2.LocationFloat = New DevExpress.Utils.PointFloat(75.0!, 0.0!)
            Me.XrLabel2.Name = "XrLabel2"
            Me.XrLabel2.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel2.SizeF = New System.Drawing.SizeF(43.0!, 15.0!)
            Me.XrLabel2.Text = "XACT"
            Me.XrLabel2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            '
            'XrLabel1
            '
            Me.XrLabel1.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel1.ForeColor = System.Drawing.Color.White
            Me.XrLabel1.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
            Me.XrLabel1.Name = "XrLabel1"
            Me.XrLabel1.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel1.SizeF = New System.Drawing.SizeF(67.0!, 15.0!)
            Me.XrLabel1.StylePriority.UseTextAlignment = False
            Me.XrLabel1.Text = "DATE"
            Me.XrLabel1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel_tran_type
            '
            Me.XrLabel_tran_type.CanGrow = False
            Me.XrLabel_tran_type.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "tran_type")})
            Me.XrLabel_tran_type.Font = New System.Drawing.Font("Times New Roman", 9.75!)
            Me.XrLabel_tran_type.ForeColor = System.Drawing.SystemColors.WindowText
            Me.XrLabel_tran_type.LocationFloat = New DevExpress.Utils.PointFloat(75.0!, 0.0!)
            Me.XrLabel_tran_type.Name = "XrLabel_tran_type"
            Me.XrLabel_tran_type.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_tran_type.SizeF = New System.Drawing.SizeF(43.0!, 15.0!)
            Me.XrLabel_tran_type.StylePriority.UseFont = False
            Me.XrLabel_tran_type.StylePriority.UseForeColor = False
            Me.XrLabel_tran_type.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            '
            'XrLabel_date_created
            '
            Me.XrLabel_date_created.CanGrow = False
            Me.XrLabel_date_created.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "item_date", "{0:d}")})
            Me.XrLabel_date_created.Font = New System.Drawing.Font("Times New Roman", 9.75!)
            Me.XrLabel_date_created.ForeColor = System.Drawing.SystemColors.WindowText
            Me.XrLabel_date_created.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
            Me.XrLabel_date_created.Name = "XrLabel_date_created"
            Me.XrLabel_date_created.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_date_created.SizeF = New System.Drawing.SizeF(67.0!, 15.0!)
            Me.XrLabel_date_created.StylePriority.UseFont = False
            Me.XrLabel_date_created.StylePriority.UseForeColor = False
            Me.XrLabel_date_created.StylePriority.UseTextAlignment = False
            Me.XrLabel_date_created.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel_credit
            '
            Me.XrLabel_credit.CanGrow = False
            Me.XrLabel_credit.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "credit_amt", "{0:c}")})
            Me.XrLabel_credit.Font = New System.Drawing.Font("Times New Roman", 9.75!)
            Me.XrLabel_credit.ForeColor = System.Drawing.SystemColors.WindowText
            Me.XrLabel_credit.LocationFloat = New DevExpress.Utils.PointFloat(442.0!, 0.0!)
            Me.XrLabel_credit.Name = "XrLabel_credit"
            Me.XrLabel_credit.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_credit.SizeF = New System.Drawing.SizeF(108.0!, 15.0!)
            Me.XrLabel_credit.StylePriority.UseFont = False
            Me.XrLabel_credit.StylePriority.UseForeColor = False
            Me.XrLabel_credit.Text = "$0.00"
            Me.XrLabel_credit.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel_debit
            '
            Me.XrLabel_debit.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "debit_amt", "{0:c}")})
            Me.XrLabel_debit.Font = New System.Drawing.Font("Times New Roman", 9.75!)
            Me.XrLabel_debit.ForeColor = System.Drawing.SystemColors.WindowText
            Me.XrLabel_debit.LocationFloat = New DevExpress.Utils.PointFloat(558.0!, 0.0!)
            Me.XrLabel_debit.Name = "XrLabel_debit"
            Me.XrLabel_debit.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_debit.SizeF = New System.Drawing.SizeF(108.0!, 15.0!)
            Me.XrLabel_debit.StylePriority.UseFont = False
            Me.XrLabel_debit.StylePriority.UseForeColor = False
            Me.XrLabel_debit.Text = "$0.00"
            Me.XrLabel_debit.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel_check_number
            '
            Me.XrLabel_check_number.CanGrow = False
            Me.XrLabel_check_number.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "check_number")})
            Me.XrLabel_check_number.Font = New System.Drawing.Font("Times New Roman", 9.75!)
            Me.XrLabel_check_number.ForeColor = System.Drawing.SystemColors.WindowText
            Me.XrLabel_check_number.LocationFloat = New DevExpress.Utils.PointFloat(675.0!, 0.0!)
            Me.XrLabel_check_number.Name = "XrLabel_check_number"
            Me.XrLabel_check_number.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_check_number.SizeF = New System.Drawing.SizeF(117.0!, 15.0!)
            Me.XrLabel_check_number.StylePriority.UseFont = False
            Me.XrLabel_check_number.StylePriority.UseForeColor = False
            Me.XrLabel_check_number.StylePriority.UseTextAlignment = False
            Me.XrLabel_check_number.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            '
            'ReportFooter
            '
            Me.ReportFooter.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel8, Me.XrLine1, Me.XrLabel_Total_Credit, Me.XrLabel_Total_Debit})
            Me.ReportFooter.HeightF = 50.0!
            Me.ReportFooter.Name = "ReportFooter"
            '
            'XrLabel8
            '
            Me.XrLabel8.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel8.ForeColor = System.Drawing.SystemColors.WindowText
            Me.XrLabel8.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 25.0!)
            Me.XrLabel8.Name = "XrLabel8"
            Me.XrLabel8.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel8.SizeF = New System.Drawing.SizeF(108.0!, 15.0!)
            Me.XrLabel8.StylePriority.UseForeColor = False
            Me.XrLabel8.Text = "TOTALS"
            Me.XrLabel8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            '
            'XrLine1
            '
            Me.XrLine1.ForeColor = System.Drawing.Color.Teal
            Me.XrLine1.LineWidth = 2
            Me.XrLine1.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 8.0!)
            Me.XrLine1.Name = "XrLine1"
            Me.XrLine1.SizeF = New System.Drawing.SizeF(667.0!, 9.0!)
            Me.XrLine1.StylePriority.UseForeColor = False
            '
            'XrLabel_Total_Credit
            '
            Me.XrLabel_Total_Credit.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "credit_amt")})
            Me.XrLabel_Total_Credit.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel_Total_Credit.ForeColor = System.Drawing.SystemColors.WindowText
            Me.XrLabel_Total_Credit.LocationFloat = New DevExpress.Utils.PointFloat(442.0!, 25.0!)
            Me.XrLabel_Total_Credit.Name = "XrLabel_Total_Credit"
            Me.XrLabel_Total_Credit.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_Total_Credit.SizeF = New System.Drawing.SizeF(108.0!, 15.0!)
            Me.XrLabel_Total_Credit.StylePriority.UseForeColor = False
            XrSummary1.FormatString = "{0:c}"
            XrSummary1.Running = DevExpress.XtraReports.UI.SummaryRunning.Report
            Me.XrLabel_Total_Credit.Summary = XrSummary1
            Me.XrLabel_Total_Credit.Text = "$0.00"
            Me.XrLabel_Total_Credit.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel_Total_Debit
            '
            Me.XrLabel_Total_Debit.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "debit_amt")})
            Me.XrLabel_Total_Debit.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel_Total_Debit.ForeColor = System.Drawing.SystemColors.WindowText
            Me.XrLabel_Total_Debit.LocationFloat = New DevExpress.Utils.PointFloat(558.0!, 25.0!)
            Me.XrLabel_Total_Debit.Name = "XrLabel_Total_Debit"
            Me.XrLabel_Total_Debit.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_Total_Debit.SizeF = New System.Drawing.SizeF(108.0!, 15.0!)
            Me.XrLabel_Total_Debit.StylePriority.UseForeColor = False
            XrSummary2.FormatString = "{0:c}"
            XrSummary2.Running = DevExpress.XtraReports.UI.SummaryRunning.Report
            Me.XrLabel_Total_Debit.Summary = XrSummary2
            Me.XrLabel_Total_Debit.Text = "$0.00"
            Me.XrLabel_Total_Debit.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel_ClientID
            '
            Me.XrLabel_ClientID.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel_ClientID.ForeColor = System.Drawing.Color.Teal
            Me.XrLabel_ClientID.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 7.999992!)
            Me.XrLabel_ClientID.Name = "XrLabel_ClientID"
            Me.XrLabel_ClientID.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_ClientID.SizeF = New System.Drawing.SizeF(617.0!, 15.0!)
            Me.XrLabel_ClientID.StylePriority.UseFont = False
            Me.XrLabel_ClientID.StylePriority.UseForeColor = False
            Me.XrLabel_ClientID.Text = "[client!0000000] [ClientName]"
            Me.XrLabel_ClientID.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            '
            'XrLabel_creditor
            '
            Me.XrLabel_creditor.CanGrow = False
            Me.XrLabel_creditor.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "creditor")})
            Me.XrLabel_creditor.Font = New System.Drawing.Font("Times New Roman", 9.75!)
            Me.XrLabel_creditor.ForeColor = System.Drawing.SystemColors.WindowText
            Me.XrLabel_creditor.LocationFloat = New DevExpress.Utils.PointFloat(125.0!, 0.0!)
            Me.XrLabel_creditor.Name = "XrLabel_creditor"
            Me.XrLabel_creditor.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_creditor.SizeF = New System.Drawing.SizeF(50.0!, 15.0!)
            Me.XrLabel_creditor.StylePriority.UseFont = False
            Me.XrLabel_creditor.StylePriority.UseForeColor = False
            Me.XrLabel_creditor.Text = "XrLabel_creditor"
            Me.XrLabel_creditor.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            '
            'XrLabel_creditor_name
            '
            Me.XrLabel_creditor_name.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "creditor_name")})
            Me.XrLabel_creditor_name.Font = New System.Drawing.Font("Times New Roman", 9.75!)
            Me.XrLabel_creditor_name.ForeColor = System.Drawing.SystemColors.WindowText
            Me.XrLabel_creditor_name.LocationFloat = New DevExpress.Utils.PointFloat(183.0!, 0.0!)
            Me.XrLabel_creditor_name.Name = "XrLabel_creditor_name"
            Me.XrLabel_creditor_name.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_creditor_name.SizeF = New System.Drawing.SizeF(250.0!, 15.0!)
            Me.XrLabel_creditor_name.StylePriority.UseFont = False
            Me.XrLabel_creditor_name.StylePriority.UseForeColor = False
            Me.XrLabel_creditor_name.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            Me.XrLabel_creditor_name.WordWrap = False
            '
            'ParameterSubTitle
            '
            Me.ParameterSubTitle.Description = "Title string"
            Me.ParameterSubTitle.Name = "ParameterSubTitle"
            Me.ParameterSubTitle.Visible = False
            '
            'ParameterClientRegister
            '
            Me.ParameterClientRegister.Name = "ParameterClientRegister"
            Me.ParameterClientRegister.Type = GetType(Integer)
            Me.ParameterClientRegister.Value = -1
            Me.ParameterClientRegister.Visible = False
            '
            'ClientName
            '
            Me.ClientName.FieldType = DevExpress.XtraReports.UI.FieldType.[String]
            Me.ClientName.Name = "ClientName"
            Me.ClientName.Scripts.OnGetValue = "ClientName_GetValue"
            '
            'ClientTransactionsDetailsReport
            '
            Me.Bands.AddRange(New DevExpress.XtraReports.UI.Band() {Me.Detail, Me.PageHeader, Me.PageFooter, Me.ReportFooter, Me.TopMarginBand1, Me.BottomMarginBand1})
            Me.CalculatedFields.AddRange(New DevExpress.XtraReports.UI.CalculatedField() {Me.ClientName})
            Me.Parameters.AddRange(New DevExpress.XtraReports.Parameters.Parameter() {Me.ParameterSubTitle, Me.ParameterClientRegister})
            Me.ScriptLanguage = DevExpress.XtraReports.ScriptLanguage.VisualBasic
            Me.ScriptReferencesString = resources.GetString("$this.ScriptReferencesString")
            Me.Scripts.OnBeforePrint = "Details_BeforePrint"
            Me.ScriptsSource = resources.GetString("$this.ScriptsSource")
            Me.StyleSheet.AddRange(New DevExpress.XtraReports.UI.XRControlStyle() {Me.XrControlStyle_Header, Me.XrControlStyle_Totals, Me.XrControlStyle_HeaderPannel, Me.XrControlStyle_GroupHeader})
            Me.Version = "11.2"
            Me.Controls.SetChildIndex(Me.BottomMarginBand1, 0)
            Me.Controls.SetChildIndex(Me.TopMarginBand1, 0)
            Me.Controls.SetChildIndex(Me.ReportFooter, 0)
            Me.Controls.SetChildIndex(Me.PageFooter, 0)
            Me.Controls.SetChildIndex(Me.PageHeader, 0)
            Me.Controls.SetChildIndex(Me.Detail, 0)
            CType(Me, System.ComponentModel.ISupportInitialize).EndInit()

        End Sub
        Friend WithEvents XrPanel1 As DevExpress.XtraReports.UI.XRPanel
        Friend WithEvents XrLabel5 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel4 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel3 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel2 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel1 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_check_number As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_debit As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_credit As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_date_created As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_tran_type As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents ReportFooter As DevExpress.XtraReports.UI.ReportFooterBand
        Friend WithEvents XrLine1 As DevExpress.XtraReports.UI.XRLine
        Friend WithEvents XrLabel_Total_Credit As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_Total_Debit As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel8 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_ClientID As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_creditor_name As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_creditor As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel10 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents ParameterSubTitle As DevExpress.XtraReports.Parameters.Parameter
        Friend WithEvents ParameterClientRegister As DevExpress.XtraReports.Parameters.Parameter
        Friend WithEvents ClientName As DevExpress.XtraReports.UI.CalculatedField
    End Class
End Namespace
