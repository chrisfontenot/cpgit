#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}
#End Region

Namespace Client.Transactions.Details
    Partial Class ClientTransactionsDetailsReport
        Public Sub New()
            MyBase.New()
            InitializeComponent()
            AddHandler BeforePrint, AddressOf Report_BeforePrint
            AddHandler ClientName.GetValue, AddressOf ClientName_GetValue
        End Sub

        Public Overrides ReadOnly Property ReportTitle() As String
            Get
                Return "Debt Transactions"
            End Get
        End Property

        Public Overrides ReadOnly Property ReportSubTitle() As String
            Get
                Return Convert.ToString(Parameters("ParameterSubTitle").Value)
            End Get
        End Property

        '****************************************** MOVED TO SCRIPTS *********************************************

        Protected ds As New System.Data.DataSet("ds")

        Private Sub Report_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs)
            Const TableName As String = "rpt_transactions_cc_cl"
            Dim rpt As DevExpress.XtraReports.UI.XtraReport = CType(sender, DevExpress.XtraReports.UI.XtraReport)
            Dim sqlInfo As DebtPlus.LINQ.SQLInfoClass = DebtPlus.LINQ.SQLInfoClass.getDefault()
            Dim tbl As System.Data.DataTable = ds.Tables(TableName)

            If tbl Is Nothing Then
                Dim cn As New System.Data.SqlClient.SqlConnection(sqlInfo.ConnectionString)
                Dim dlg As New DevExpress.Utils.WaitDialogForm("Reading Debt Transactions")
                Try
                    dlg.Show()
                    cn.Open()

                    Using cmd As New System.Data.SqlClient.SqlCommand()
                        cmd.Connection = cn
                        cmd.CommandText = "rpt_transactions_cc_cl"
                        cmd.CommandType = System.Data.CommandType.StoredProcedure
                        System.Data.SqlClient.SqlCommandBuilder.DeriveParameters(cmd)

                        cmd.CommandTimeout = 0
                        cmd.Parameters(1).Value = rpt.Parameters("ParameterClientRegister").Value

                        Using da As New System.Data.SqlClient.SqlDataAdapter(cmd)
                            da.Fill(ds, TableName)
                            tbl = ds.Tables(TableName)
                        End Using
                    End Using

                Catch ex As System.Data.SqlClient.SqlException
                    Using gdr As New DebtPlus.Repository.GetDataResult()
                        gdr.HandleException(ex)
                        DebtPlus.UI.Common.ErrorHandling.HandleErrors(gdr, "Error reading report information")
                    End Using

                Finally
                    If cn IsNot Nothing Then
                        cn.Dispose()
                    End If

                    If dlg IsNot Nothing Then
                        dlg.Close()
                        dlg.Dispose()
                    End If
                End Try
            End If

            If tbl IsNot Nothing Then
                rpt.DataSource = New System.Data.DataView(tbl, String.Empty, "item_date", System.Data.DataViewRowState.CurrentRows)

                For Each fld As DevExpress.XtraReports.UI.CalculatedField In rpt.CalculatedFields
                    fld.Assign(rpt.DataSource, rpt.DataMember)
                Next
            End If
        End Sub

        Private Function GetClientRow(ByVal rpt As DevExpress.XtraReports.UI.XtraReport, ByVal Client As Int32) As System.Data.DataRow
            Dim sqlInfo As DebtPlus.LINQ.SQLInfoClass = DebtPlus.LINQ.SQLInfoClass.getDefault()
            Dim ds As System.Data.DataSet = CType(rpt.DataSource, System.Data.DataView).Table.DataSet
            Const TableName As String = "view_client_address"
            Dim tbl As System.Data.DataTable = ds.Tables(TableName)

            '-- If the value has already been cached then return the cache entry
            If tbl IsNot Nothing Then
                Dim row As System.Data.DataRow = tbl.Rows.Find(Client)
                If row IsNot Nothing Then Return row
            End If

            '-- Read the data from the database for the client's name
            Dim cn As New System.Data.SqlClient.SqlConnection(sqlInfo.ConnectionString)
            Try
                Using cmd As New System.Data.SqlClient.SqlCommand()
                    cmd.Connection = cn
                    cmd.CommandText = "SELECT client, name FROM view_client_address WHERE client=@client"
                    cmd.CommandType = System.Data.CommandType.Text
                    cmd.Parameters.Add("@client", System.Data.SqlDbType.Int).Value = Client

                    Using da As New System.Data.SqlClient.SqlDataAdapter(cmd)
                        da.Fill(ds, TableName)
                        tbl = ds.Tables(TableName)
                        If tbl.PrimaryKey.GetUpperBound(0) < 0 Then
                            tbl.PrimaryKey = New System.Data.DataColumn() {tbl.Columns("client")}
                        End If
                    End Using
                End Using

            Finally
                cn.Dispose()
            End Try

            '-- Try the operation again to retrieve the client row
            If tbl IsNot Nothing Then
                Dim row As System.Data.DataRow = tbl.Rows.Find(Client)
                If row IsNot Nothing Then Return row
            End If

            Return Nothing
        End Function

        Private Sub ClientName_GetValue(sender As Object, e As DevExpress.XtraReports.UI.GetValueEventArgs)
            Dim row As System.Data.DataRow = CType(e.Row, System.Data.DataRowView).Row
            Dim Client As Int32 = DebtPlus.Utils.Nulls.DInt(row("client"))

            '-- Retrieve the client's name from the system
            row = GetClientRow(CType(e.Report, DevExpress.XtraReports.UI.XtraReport), Client)
            If row IsNot Nothing Then
                e.Value = Convert.ToString(row("name")).Trim()
            End If
        End Sub
    End Class
End Namespace
