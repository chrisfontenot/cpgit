Namespace DailySummary.Voids.Creditor.Detail
    <Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
    Partial Class DailySummaryCreditorVoidsReport
        Inherits DevExpress.XtraReports.UI.XtraReport

        'XtraReport overrides dispose to clean up the component list.
        <System.Diagnostics.DebuggerNonUserCode()> _
        Protected Overrides Sub Dispose(ByVal disposing As Boolean)
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
            MyBase.Dispose(disposing)
        End Sub

        'Required by the Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Designer
        'It can be modified using the Designer.  
        'Do not modify it using the code editor.
        <System.Diagnostics.DebuggerStepThrough()> _
        Private Sub InitializeComponent()
            Dim PostNetGenerator1 As DevExpress.XtraPrinting.BarCode.PostNetGenerator = New DevExpress.XtraPrinting.BarCode.PostNetGenerator
            Me.Detail = New DevExpress.XtraReports.UI.DetailBand
            Me.AddressInformation = New DevExpress.XtraReports.UI.XRLabel
            Me.XrBarCode_Zipcode = New DevExpress.XtraReports.UI.XRBarCode
            CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
            '
            'Detail
            '
            Me.Detail.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.AddressInformation, Me.XrBarCode_Zipcode})
            Me.Detail.Name = "Detail"
            Me.Detail.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
            Me.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
            '
            'AddressInformation
            '
            Me.AddressInformation.CanGrow = False
            Me.AddressInformation.CanShrink = True
            Me.AddressInformation.Location = New System.Drawing.Point(0, 17)
            Me.AddressInformation.Multiline = True
            Me.AddressInformation.Name = "AddressInformation"
            Me.AddressInformation.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.AddressInformation.Size = New System.Drawing.Size(300, 83)
            Me.AddressInformation.WordWrap = False
            '
            'XrBarCode_Zipcode
            '
            Me.XrBarCode_Zipcode.BorderColor = System.Drawing.Color.Black
            Me.XrBarCode_Zipcode.Borders = DevExpress.XtraPrinting.BorderSide.None
            Me.XrBarCode_Zipcode.ForeColor = System.Drawing.Color.Black
            Me.XrBarCode_Zipcode.Location = New System.Drawing.Point(0, 0)
            Me.XrBarCode_Zipcode.Module = 3.0!
            Me.XrBarCode_Zipcode.Name = "XrBarCode_Zipcode"
            Me.XrBarCode_Zipcode.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
            Me.XrBarCode_Zipcode.ShowText = False
            Me.XrBarCode_Zipcode.Size = New System.Drawing.Size(300, 15)
            Me.XrBarCode_Zipcode.StylePriority.UseBorderColor = False
            Me.XrBarCode_Zipcode.StylePriority.UseBorders = False
            Me.XrBarCode_Zipcode.StylePriority.UseForeColor = False
            Me.XrBarCode_Zipcode.Symbology = PostNetGenerator1
            Me.XrBarCode_Zipcode.Visible = False
            '
            'DailySummaryCreditorVoidsReport
            '
            Me.Bands.AddRange(New DevExpress.XtraReports.UI.Band() {Me.Detail})
            Me.Version = "8.1"
            CType(Me, System.ComponentModel.ISupportInitialize).EndInit()

        End Sub
        Friend WithEvents Detail As DevExpress.XtraReports.UI.DetailBand
        Protected Friend WithEvents XrBarCode_Zipcode As DevExpress.XtraReports.UI.XRBarCode
        Friend WithEvents AddressInformation As DevExpress.XtraReports.UI.XRLabel
    End Class
End Namespace