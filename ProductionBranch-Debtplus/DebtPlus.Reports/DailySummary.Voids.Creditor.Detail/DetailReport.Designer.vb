Namespace DailySummary.Voids.Creditor.Detail
    <Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
    Partial Class DetailReport
        Inherits DebtPlus.Reports.Template.TemplateXtraReportClass

        'XtraReport overrides dispose to clean up the component list.
        <System.Diagnostics.DebuggerNonUserCode()> _
        Protected Overrides Sub Dispose(ByVal disposing As Boolean)
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
            MyBase.Dispose(disposing)
        End Sub

        'Required by the Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Designer
        'It can be modified using the Designer.  
        'Do not modify it using the code editor.
        <System.Diagnostics.DebuggerStepThrough()> _
        Private Sub InitializeComponent()
            Dim XrSummary1 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary
            Dim XrSummary2 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary
            Dim XrSummary3 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary
            Dim XrSummary4 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary
            Me.XrPanel1 = New DevExpress.XtraReports.UI.XRPanel
            Me.XrLabel6 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel4 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel5 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel3 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel9 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel8 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_account_number = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_deducted = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_billed = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_debit_amt = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_net = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_client_name = New DevExpress.XtraReports.UI.XRLabel
            Me.ReportFooter = New DevExpress.XtraReports.UI.ReportFooterBand
            Me.XrLabel17 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_total_debit_amt = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_total_deducted = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_total_billed = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_total_net = New DevExpress.XtraReports.UI.XRLabel
            Me.XrTable1 = New DevExpress.XtraReports.UI.XRTable
            Me.XrTableRow1 = New DevExpress.XtraReports.UI.XRTableRow
            Me.XrTableCell1 = New DevExpress.XtraReports.UI.XRTableCell
            Me.XrTableCell3 = New DevExpress.XtraReports.UI.XRTableCell
            Me.XrTableRow3 = New DevExpress.XtraReports.UI.XRTableRow
            Me.XrTableCell5 = New DevExpress.XtraReports.UI.XRTableCell
            Me.XrTableCell6 = New DevExpress.XtraReports.UI.XRTableCell
            Me.XrTableRow2 = New DevExpress.XtraReports.UI.XRTableRow
            Me.XrTableCell2 = New DevExpress.XtraReports.UI.XRTableCell
            Me.XrTableCell_date_created = New DevExpress.XtraReports.UI.XRTableCell
            Me.XrLabel_client = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel1 = New DevExpress.XtraReports.UI.XRLabel
            CType(Me.XrTable1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
            '
            'Detail
            '
            Me.Detail.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel_client, Me.XrLabel_client_name, Me.XrLabel_net, Me.XrLabel_debit_amt, Me.XrLabel_billed, Me.XrLabel_deducted, Me.XrLabel_account_number})
            Me.Detail.Height = 15
            '
            'PageHeader
            '
            Me.PageHeader.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrTable1, Me.XrPanel1})
            Me.PageHeader.Height = 182
            '
            'XrLabel_Subtitle
            '
            Me.XrLabel_Subtitle.Location = New System.Drawing.Point(442, 117)
            Me.XrLabel_Subtitle.Size = New System.Drawing.Size(134, 17)
            Me.XrLabel_Subtitle.Visible = False
            '
            'XrPanel1
            '
            Me.XrPanel1.BackColor = System.Drawing.Color.Teal
            Me.XrPanel1.BorderColor = System.Drawing.Color.Teal
            Me.XrPanel1.CanGrow = False
            Me.XrPanel1.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel1, Me.XrLabel6, Me.XrLabel4, Me.XrLabel5, Me.XrLabel3, Me.XrLabel9, Me.XrLabel8})
            Me.XrPanel1.KeepTogether = False
            Me.XrPanel1.Location = New System.Drawing.Point(0, 158)
            Me.XrPanel1.Name = "XrPanel1"
            Me.XrPanel1.Size = New System.Drawing.Size(800, 18)
            Me.XrPanel1.StylePriority.UseBackColor = False
            Me.XrPanel1.StylePriority.UseBorderColor = False
            '
            'XrLabel6
            '
            Me.XrLabel6.BackColor = System.Drawing.Color.Transparent
            Me.XrLabel6.BorderColor = System.Drawing.Color.Transparent
            Me.XrLabel6.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel6.ForeColor = System.Drawing.Color.White
            Me.XrLabel6.Location = New System.Drawing.Point(708, 0)
            Me.XrLabel6.Name = "XrLabel6"
            Me.XrLabel6.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel6.Size = New System.Drawing.Size(72, 16)
            Me.XrLabel6.StylePriority.UseBackColor = False
            Me.XrLabel6.StylePriority.UseBorderColor = False
            Me.XrLabel6.StylePriority.UseFont = False
            Me.XrLabel6.StylePriority.UseForeColor = False
            Me.XrLabel6.StylePriority.UseTextAlignment = False
            Me.XrLabel6.Text = "NET"
            Me.XrLabel6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel4
            '
            Me.XrLabel4.BackColor = System.Drawing.Color.Transparent
            Me.XrLabel4.BorderColor = System.Drawing.Color.Transparent
            Me.XrLabel4.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel4.ForeColor = System.Drawing.Color.White
            Me.XrLabel4.Location = New System.Drawing.Point(614, 0)
            Me.XrLabel4.Name = "XrLabel4"
            Me.XrLabel4.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel4.Size = New System.Drawing.Size(72, 16)
            Me.XrLabel4.StylePriority.UseBackColor = False
            Me.XrLabel4.StylePriority.UseBorderColor = False
            Me.XrLabel4.StylePriority.UseFont = False
            Me.XrLabel4.StylePriority.UseForeColor = False
            Me.XrLabel4.StylePriority.UseTextAlignment = False
            Me.XrLabel4.Text = "DEDUCT"
            Me.XrLabel4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel5
            '
            Me.XrLabel5.BackColor = System.Drawing.Color.Transparent
            Me.XrLabel5.BorderColor = System.Drawing.Color.Transparent
            Me.XrLabel5.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel5.ForeColor = System.Drawing.Color.White
            Me.XrLabel5.Location = New System.Drawing.Point(535, 0)
            Me.XrLabel5.Name = "XrLabel5"
            Me.XrLabel5.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel5.Size = New System.Drawing.Size(72, 16)
            Me.XrLabel5.StylePriority.UseBackColor = False
            Me.XrLabel5.StylePriority.UseBorderColor = False
            Me.XrLabel5.StylePriority.UseFont = False
            Me.XrLabel5.StylePriority.UseForeColor = False
            Me.XrLabel5.StylePriority.UseTextAlignment = False
            Me.XrLabel5.Text = "BILL"
            Me.XrLabel5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel3
            '
            Me.XrLabel3.BackColor = System.Drawing.Color.Transparent
            Me.XrLabel3.BorderColor = System.Drawing.Color.Transparent
            Me.XrLabel3.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel3.ForeColor = System.Drawing.Color.White
            Me.XrLabel3.Location = New System.Drawing.Point(291, 0)
            Me.XrLabel3.Name = "XrLabel3"
            Me.XrLabel3.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel3.Size = New System.Drawing.Size(150, 16)
            Me.XrLabel3.StylePriority.UseBackColor = False
            Me.XrLabel3.StylePriority.UseBorderColor = False
            Me.XrLabel3.StylePriority.UseFont = False
            Me.XrLabel3.StylePriority.UseForeColor = False
            Me.XrLabel3.StylePriority.UseTextAlignment = False
            Me.XrLabel3.Text = "ACCOUNT NUMBER"
            Me.XrLabel3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            '
            'XrLabel9
            '
            Me.XrLabel9.BackColor = System.Drawing.Color.Transparent
            Me.XrLabel9.BorderColor = System.Drawing.Color.Transparent
            Me.XrLabel9.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel9.ForeColor = System.Drawing.Color.White
            Me.XrLabel9.Location = New System.Drawing.Point(0, 0)
            Me.XrLabel9.Name = "XrLabel9"
            Me.XrLabel9.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel9.Size = New System.Drawing.Size(75, 15)
            Me.XrLabel9.StylePriority.UseBackColor = False
            Me.XrLabel9.StylePriority.UseBorderColor = False
            Me.XrLabel9.StylePriority.UseFont = False
            Me.XrLabel9.StylePriority.UseForeColor = False
            Me.XrLabel9.StylePriority.UseTextAlignment = False
            Me.XrLabel9.Text = "CLIENT"
            Me.XrLabel9.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel8
            '
            Me.XrLabel8.BackColor = System.Drawing.Color.Transparent
            Me.XrLabel8.BorderColor = System.Drawing.Color.Transparent
            Me.XrLabel8.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel8.ForeColor = System.Drawing.Color.White
            Me.XrLabel8.Location = New System.Drawing.Point(452, 0)
            Me.XrLabel8.Name = "XrLabel8"
            Me.XrLabel8.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel8.Size = New System.Drawing.Size(72, 16)
            Me.XrLabel8.StylePriority.UseBackColor = False
            Me.XrLabel8.StylePriority.UseBorderColor = False
            Me.XrLabel8.StylePriority.UseFont = False
            Me.XrLabel8.StylePriority.UseForeColor = False
            Me.XrLabel8.StylePriority.UseTextAlignment = False
            Me.XrLabel8.Text = "GROSS"
            Me.XrLabel8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel_account_number
            '
            Me.XrLabel_account_number.BackColor = System.Drawing.Color.Transparent
            Me.XrLabel_account_number.BorderColor = System.Drawing.Color.Transparent
            Me.XrLabel_account_number.Font = New System.Drawing.Font("Times New Roman", 9.75!)
            Me.XrLabel_account_number.ForeColor = System.Drawing.SystemColors.ControlText
            Me.XrLabel_account_number.Location = New System.Drawing.Point(291, 0)
            Me.XrLabel_account_number.Name = "XrLabel_account_number"
            Me.XrLabel_account_number.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_account_number.Size = New System.Drawing.Size(150, 15)
            Me.XrLabel_account_number.StylePriority.UseBackColor = False
            Me.XrLabel_account_number.StylePriority.UseBorderColor = False
            Me.XrLabel_account_number.StylePriority.UseFont = False
            Me.XrLabel_account_number.StylePriority.UseForeColor = False
            Me.XrLabel_account_number.StylePriority.UseTextAlignment = False
            Me.XrLabel_account_number.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            Me.XrLabel_account_number.WordWrap = False
            '
            'XrLabel_deducted
            '
            Me.XrLabel_deducted.BackColor = System.Drawing.Color.Transparent
            Me.XrLabel_deducted.BorderColor = System.Drawing.Color.Transparent
            Me.XrLabel_deducted.Font = New System.Drawing.Font("Times New Roman", 9.75!)
            Me.XrLabel_deducted.ForeColor = System.Drawing.SystemColors.ControlText
            Me.XrLabel_deducted.Location = New System.Drawing.Point(614, 0)
            Me.XrLabel_deducted.Name = "XrLabel_deducted"
            Me.XrLabel_deducted.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_deducted.Size = New System.Drawing.Size(72, 15)
            Me.XrLabel_deducted.StylePriority.UseBackColor = False
            Me.XrLabel_deducted.StylePriority.UseBorderColor = False
            Me.XrLabel_deducted.StylePriority.UseFont = False
            Me.XrLabel_deducted.StylePriority.UseForeColor = False
            Me.XrLabel_deducted.StylePriority.UseTextAlignment = False
            Me.XrLabel_deducted.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel_billed
            '
            Me.XrLabel_billed.BackColor = System.Drawing.Color.Transparent
            Me.XrLabel_billed.BorderColor = System.Drawing.Color.Transparent
            Me.XrLabel_billed.Font = New System.Drawing.Font("Times New Roman", 9.75!)
            Me.XrLabel_billed.ForeColor = System.Drawing.SystemColors.ControlText
            Me.XrLabel_billed.Location = New System.Drawing.Point(535, 0)
            Me.XrLabel_billed.Name = "XrLabel_billed"
            Me.XrLabel_billed.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_billed.Size = New System.Drawing.Size(72, 15)
            Me.XrLabel_billed.StylePriority.UseBackColor = False
            Me.XrLabel_billed.StylePriority.UseBorderColor = False
            Me.XrLabel_billed.StylePriority.UseFont = False
            Me.XrLabel_billed.StylePriority.UseForeColor = False
            Me.XrLabel_billed.StylePriority.UseTextAlignment = False
            Me.XrLabel_billed.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel_debit_amt
            '
            Me.XrLabel_debit_amt.BackColor = System.Drawing.Color.Transparent
            Me.XrLabel_debit_amt.BorderColor = System.Drawing.Color.Transparent
            Me.XrLabel_debit_amt.Font = New System.Drawing.Font("Times New Roman", 9.75!)
            Me.XrLabel_debit_amt.ForeColor = System.Drawing.SystemColors.ControlText
            Me.XrLabel_debit_amt.Location = New System.Drawing.Point(452, 0)
            Me.XrLabel_debit_amt.Name = "XrLabel_debit_amt"
            Me.XrLabel_debit_amt.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_debit_amt.Size = New System.Drawing.Size(72, 15)
            Me.XrLabel_debit_amt.StylePriority.UseBackColor = False
            Me.XrLabel_debit_amt.StylePriority.UseBorderColor = False
            Me.XrLabel_debit_amt.StylePriority.UseFont = False
            Me.XrLabel_debit_amt.StylePriority.UseForeColor = False
            Me.XrLabel_debit_amt.StylePriority.UseTextAlignment = False
            Me.XrLabel_debit_amt.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel_net
            '
            Me.XrLabel_net.BackColor = System.Drawing.Color.Transparent
            Me.XrLabel_net.BorderColor = System.Drawing.Color.Transparent
            Me.XrLabel_net.Font = New System.Drawing.Font("Times New Roman", 9.75!)
            Me.XrLabel_net.ForeColor = System.Drawing.SystemColors.ControlText
            Me.XrLabel_net.Location = New System.Drawing.Point(708, 0)
            Me.XrLabel_net.Name = "XrLabel_net"
            Me.XrLabel_net.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_net.Size = New System.Drawing.Size(72, 15)
            Me.XrLabel_net.StylePriority.UseBackColor = False
            Me.XrLabel_net.StylePriority.UseBorderColor = False
            Me.XrLabel_net.StylePriority.UseFont = False
            Me.XrLabel_net.StylePriority.UseForeColor = False
            Me.XrLabel_net.StylePriority.UseTextAlignment = False
            Me.XrLabel_net.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel_client_name
            '
            Me.XrLabel_client_name.BackColor = System.Drawing.Color.Transparent
            Me.XrLabel_client_name.BorderColor = System.Drawing.Color.Transparent
            Me.XrLabel_client_name.Font = New System.Drawing.Font("Times New Roman", 9.75!)
            Me.XrLabel_client_name.ForeColor = System.Drawing.SystemColors.WindowText
            Me.XrLabel_client_name.Location = New System.Drawing.Point(83, 0)
            Me.XrLabel_client_name.Name = "XrLabel_client_name"
            Me.XrLabel_client_name.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_client_name.Size = New System.Drawing.Size(208, 15)
            Me.XrLabel_client_name.StylePriority.UseBackColor = False
            Me.XrLabel_client_name.StylePriority.UseBorderColor = False
            Me.XrLabel_client_name.StylePriority.UseFont = False
            Me.XrLabel_client_name.StylePriority.UseForeColor = False
            Me.XrLabel_client_name.StylePriority.UseTextAlignment = False
            Me.XrLabel_client_name.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            Me.XrLabel_client_name.WordWrap = False
            '
            'ReportFooter
            '
            Me.ReportFooter.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel17, Me.XrLabel_total_debit_amt, Me.XrLabel_total_deducted, Me.XrLabel_total_billed, Me.XrLabel_total_net})
            Me.ReportFooter.Height = 39
            Me.ReportFooter.Name = "ReportFooter"
            '
            'XrLabel17
            '
            Me.XrLabel17.BackColor = System.Drawing.Color.Transparent
            Me.XrLabel17.BorderColor = System.Drawing.Color.Transparent
            Me.XrLabel17.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel17.ForeColor = System.Drawing.SystemColors.WindowText
            Me.XrLabel17.Location = New System.Drawing.Point(0, 17)
            Me.XrLabel17.Name = "XrLabel17"
            Me.XrLabel17.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel17.Size = New System.Drawing.Size(283, 16)
            Me.XrLabel17.StylePriority.UseBackColor = False
            Me.XrLabel17.StylePriority.UseBorderColor = False
            Me.XrLabel17.StylePriority.UseFont = False
            Me.XrLabel17.StylePriority.UseForeColor = False
            Me.XrLabel17.StylePriority.UseTextAlignment = False
            Me.XrLabel17.Text = "TOTALS"
            Me.XrLabel17.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            '
            'XrLabel_total_debit_amt
            '
            Me.XrLabel_total_debit_amt.BackColor = System.Drawing.Color.Transparent
            Me.XrLabel_total_debit_amt.BorderColor = System.Drawing.Color.Transparent
            Me.XrLabel_total_debit_amt.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel_total_debit_amt.ForeColor = System.Drawing.SystemColors.WindowText
            Me.XrLabel_total_debit_amt.Location = New System.Drawing.Point(452, 17)
            Me.XrLabel_total_debit_amt.Name = "XrLabel_total_debit_amt"
            Me.XrLabel_total_debit_amt.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_total_debit_amt.Size = New System.Drawing.Size(72, 16)
            Me.XrLabel_total_debit_amt.StylePriority.UseBackColor = False
            Me.XrLabel_total_debit_amt.StylePriority.UseBorderColor = False
            Me.XrLabel_total_debit_amt.StylePriority.UseFont = False
            Me.XrLabel_total_debit_amt.StylePriority.UseForeColor = False
            Me.XrLabel_total_debit_amt.StylePriority.UseTextAlignment = False
            XrSummary1.FormatString = "{0:c}"
            XrSummary1.Running = DevExpress.XtraReports.UI.SummaryRunning.Report
            Me.XrLabel_total_debit_amt.Summary = XrSummary1
            Me.XrLabel_total_debit_amt.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel_total_deducted
            '
            Me.XrLabel_total_deducted.BackColor = System.Drawing.Color.Transparent
            Me.XrLabel_total_deducted.BorderColor = System.Drawing.Color.Transparent
            Me.XrLabel_total_deducted.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel_total_deducted.ForeColor = System.Drawing.SystemColors.WindowText
            Me.XrLabel_total_deducted.Location = New System.Drawing.Point(614, 17)
            Me.XrLabel_total_deducted.Name = "XrLabel_total_deducted"
            Me.XrLabel_total_deducted.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_total_deducted.Size = New System.Drawing.Size(72, 16)
            Me.XrLabel_total_deducted.StylePriority.UseBackColor = False
            Me.XrLabel_total_deducted.StylePriority.UseBorderColor = False
            Me.XrLabel_total_deducted.StylePriority.UseFont = False
            Me.XrLabel_total_deducted.StylePriority.UseForeColor = False
            Me.XrLabel_total_deducted.StylePriority.UseTextAlignment = False
            XrSummary2.FormatString = "{0:c}"
            XrSummary2.Running = DevExpress.XtraReports.UI.SummaryRunning.Report
            Me.XrLabel_total_deducted.Summary = XrSummary2
            Me.XrLabel_total_deducted.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel_total_billed
            '
            Me.XrLabel_total_billed.BackColor = System.Drawing.Color.Transparent
            Me.XrLabel_total_billed.BorderColor = System.Drawing.Color.Transparent
            Me.XrLabel_total_billed.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel_total_billed.ForeColor = System.Drawing.SystemColors.WindowText
            Me.XrLabel_total_billed.Location = New System.Drawing.Point(535, 17)
            Me.XrLabel_total_billed.Name = "XrLabel_total_billed"
            Me.XrLabel_total_billed.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_total_billed.Size = New System.Drawing.Size(72, 16)
            Me.XrLabel_total_billed.StylePriority.UseBackColor = False
            Me.XrLabel_total_billed.StylePriority.UseBorderColor = False
            Me.XrLabel_total_billed.StylePriority.UseFont = False
            Me.XrLabel_total_billed.StylePriority.UseForeColor = False
            Me.XrLabel_total_billed.StylePriority.UseTextAlignment = False
            XrSummary3.FormatString = "{0:c}"
            XrSummary3.Running = DevExpress.XtraReports.UI.SummaryRunning.Report
            Me.XrLabel_total_billed.Summary = XrSummary3
            Me.XrLabel_total_billed.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel_total_net
            '
            Me.XrLabel_total_net.BackColor = System.Drawing.Color.Transparent
            Me.XrLabel_total_net.BorderColor = System.Drawing.Color.Transparent
            Me.XrLabel_total_net.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel_total_net.ForeColor = System.Drawing.SystemColors.WindowText
            Me.XrLabel_total_net.Location = New System.Drawing.Point(708, 17)
            Me.XrLabel_total_net.Name = "XrLabel_total_net"
            Me.XrLabel_total_net.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_total_net.Size = New System.Drawing.Size(72, 16)
            Me.XrLabel_total_net.StylePriority.UseBackColor = False
            Me.XrLabel_total_net.StylePriority.UseBorderColor = False
            Me.XrLabel_total_net.StylePriority.UseFont = False
            Me.XrLabel_total_net.StylePriority.UseForeColor = False
            Me.XrLabel_total_net.StylePriority.UseTextAlignment = False
            XrSummary4.FormatString = "{0:c}"
            XrSummary4.Running = DevExpress.XtraReports.UI.SummaryRunning.Report
            Me.XrLabel_total_net.Summary = XrSummary4
            Me.XrLabel_total_net.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrTable1
            '
            Me.XrTable1.Location = New System.Drawing.Point(8, 92)
            Me.XrTable1.Name = "XrTable1"
            Me.XrTable1.Rows.AddRange(New DevExpress.XtraReports.UI.XRTableRow() {Me.XrTableRow1, Me.XrTableRow3, Me.XrTableRow2})
            Me.XrTable1.Size = New System.Drawing.Size(425, 54)
            '
            'XrTableRow1
            '
            Me.XrTableRow1.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.XrTableCell1, Me.XrTableCell3})
            Me.XrTableRow1.Name = "XrTableRow1"
            Me.XrTableRow1.Size = New System.Drawing.Size(425, 18)
            '
            'XrTableCell1
            '
            Me.XrTableCell1.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrTableCell1.ForeColor = System.Drawing.Color.Teal
            Me.XrTableCell1.Location = New System.Drawing.Point(0, 0)
            Me.XrTableCell1.Name = "XrTableCell1"
            Me.XrTableCell1.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrTableCell1.Size = New System.Drawing.Size(117, 18)
            Me.XrTableCell1.StylePriority.UseFont = False
            Me.XrTableCell1.StylePriority.UseForeColor = False
            Me.XrTableCell1.Text = "Check Number:"
            '
            'XrTableCell3
            '
            Me.XrTableCell3.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrTableCell3.ForeColor = System.Drawing.Color.Teal
            Me.XrTableCell3.Location = New System.Drawing.Point(117, 0)
            Me.XrTableCell3.Name = "XrTableCell3"
            Me.XrTableCell3.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrTableCell3.Size = New System.Drawing.Size(308, 18)
            Me.XrTableCell3.StylePriority.UseFont = False
            Me.XrTableCell3.StylePriority.UseForeColor = False
            Me.XrTableCell3.Text = "[checknum]"
            Me.XrTableCell3.WordWrap = False
            '
            'XrTableRow3
            '
            Me.XrTableRow3.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.XrTableCell5, Me.XrTableCell6})
            Me.XrTableRow3.Name = "XrTableRow3"
            Me.XrTableRow3.Size = New System.Drawing.Size(425, 18)
            '
            'XrTableCell5
            '
            Me.XrTableCell5.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrTableCell5.ForeColor = System.Drawing.Color.Teal
            Me.XrTableCell5.Location = New System.Drawing.Point(0, 0)
            Me.XrTableCell5.Name = "XrTableCell5"
            Me.XrTableCell5.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrTableCell5.Size = New System.Drawing.Size(117, 18)
            Me.XrTableCell5.StylePriority.UseFont = False
            Me.XrTableCell5.StylePriority.UseForeColor = False
            Me.XrTableCell5.Text = "Creditor:"
            '
            'XrTableCell6
            '
            Me.XrTableCell6.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrTableCell6.ForeColor = System.Drawing.Color.Teal
            Me.XrTableCell6.Location = New System.Drawing.Point(117, 0)
            Me.XrTableCell6.Name = "XrTableCell6"
            Me.XrTableCell6.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrTableCell6.Size = New System.Drawing.Size(308, 18)
            Me.XrTableCell6.StylePriority.UseFont = False
            Me.XrTableCell6.StylePriority.UseForeColor = False
            Me.XrTableCell6.Text = "[[creditor]] [creditor_name]"
            Me.XrTableCell6.WordWrap = False
            '
            'XrTableRow2
            '
            Me.XrTableRow2.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.XrTableCell2, Me.XrTableCell_date_created})
            Me.XrTableRow2.Name = "XrTableRow2"
            Me.XrTableRow2.Size = New System.Drawing.Size(425, 18)
            '
            'XrTableCell2
            '
            Me.XrTableCell2.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrTableCell2.ForeColor = System.Drawing.Color.Teal
            Me.XrTableCell2.Location = New System.Drawing.Point(0, 0)
            Me.XrTableCell2.Name = "XrTableCell2"
            Me.XrTableCell2.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrTableCell2.Size = New System.Drawing.Size(117, 18)
            Me.XrTableCell2.StylePriority.UseFont = False
            Me.XrTableCell2.StylePriority.UseForeColor = False
            Me.XrTableCell2.Text = "Date Written:"
            '
            'XrTableCell_date_created
            '
            Me.XrTableCell_date_created.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrTableCell_date_created.ForeColor = System.Drawing.Color.Teal
            Me.XrTableCell_date_created.Location = New System.Drawing.Point(117, 0)
            Me.XrTableCell_date_created.Name = "XrTableCell_date_created"
            Me.XrTableCell_date_created.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrTableCell_date_created.Size = New System.Drawing.Size(308, 18)
            Me.XrTableCell_date_created.StylePriority.UseFont = False
            Me.XrTableCell_date_created.StylePriority.UseForeColor = False
            Me.XrTableCell_date_created.WordWrap = False
            '
            'XrLabel_client
            '
            Me.XrLabel_client.BackColor = System.Drawing.Color.Transparent
            Me.XrLabel_client.BorderColor = System.Drawing.Color.Transparent
            Me.XrLabel_client.Font = New System.Drawing.Font("Times New Roman", 9.75!)
            Me.XrLabel_client.ForeColor = System.Drawing.SystemColors.WindowText
            Me.XrLabel_client.Location = New System.Drawing.Point(0, 0)
            Me.XrLabel_client.Name = "XrLabel_client"
            Me.XrLabel_client.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_client.Size = New System.Drawing.Size(75, 15)
            Me.XrLabel_client.StylePriority.UseBackColor = False
            Me.XrLabel_client.StylePriority.UseBorderColor = False
            Me.XrLabel_client.StylePriority.UseFont = False
            Me.XrLabel_client.StylePriority.UseForeColor = False
            Me.XrLabel_client.StylePriority.UseTextAlignment = False
            Me.XrLabel_client.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel1
            '
            Me.XrLabel1.BackColor = System.Drawing.Color.Transparent
            Me.XrLabel1.BorderColor = System.Drawing.Color.Transparent
            Me.XrLabel1.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel1.ForeColor = System.Drawing.Color.White
            Me.XrLabel1.Location = New System.Drawing.Point(83, 0)
            Me.XrLabel1.Name = "XrLabel1"
            Me.XrLabel1.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel1.Size = New System.Drawing.Size(208, 15)
            Me.XrLabel1.StylePriority.UseBackColor = False
            Me.XrLabel1.StylePriority.UseBorderColor = False
            Me.XrLabel1.StylePriority.UseFont = False
            Me.XrLabel1.StylePriority.UseForeColor = False
            Me.XrLabel1.StylePriority.UseTextAlignment = False
            Me.XrLabel1.Text = "NAME"
            Me.XrLabel1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            '
            'DetailReport
            '
            Me.Bands.AddRange(New DevExpress.XtraReports.UI.Band() {Me.ReportFooter})
            Me.Version = "8.2"
            CType(Me.XrTable1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me, System.ComponentModel.ISupportInitialize).EndInit()

        End Sub
        Friend WithEvents XrPanel1 As DevExpress.XtraReports.UI.XRPanel
        Friend WithEvents XrLabel6 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel4 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel5 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel3 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel9 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel8 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_client_name As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_net As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_debit_amt As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_billed As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_deducted As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_account_number As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents ReportFooter As DevExpress.XtraReports.UI.ReportFooterBand
        Friend WithEvents XrLabel17 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_total_debit_amt As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_total_deducted As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_total_billed As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_total_net As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrTable1 As DevExpress.XtraReports.UI.XRTable
        Friend WithEvents XrTableRow1 As DevExpress.XtraReports.UI.XRTableRow
        Friend WithEvents XrTableCell1 As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents XrTableCell3 As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents XrTableRow3 As DevExpress.XtraReports.UI.XRTableRow
        Friend WithEvents XrTableCell5 As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents XrTableCell6 As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents XrTableRow2 As DevExpress.XtraReports.UI.XRTableRow
        Friend WithEvents XrTableCell2 As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents XrTableCell_date_created As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents XrLabel_client As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel1 As DevExpress.XtraReports.UI.XRLabel
    End Class
End Namespace