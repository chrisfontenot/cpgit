#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}

#End Region

Option Compare Binary
Option Explicit On
Option Strict On

Imports System.ComponentModel
Imports DebtPlus.Reports.Template

Imports System.Drawing.Printing
Imports DevExpress.XtraReports.UI
Imports System.Data.SqlClient
Imports System.Threading

Namespace DailySummary.Voids.Creditor.Detail
    Public Class DailySummaryCreditorVoids
        Inherits DatedTemplateXtraReportClass

        ''' <summary>
        ''' Create an instance of our report
        ''' </summary>
        Public Sub New()
            MyBase.New()
            InitializeComponent()
            AddHandler BeforePrint, AddressOf DailySummaryCreditorVoids_BeforePrint
        End Sub

#Region " Component Designer generated code "

        'Component overrides dispose to clean up the component list.
        Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
            If disposing Then
                If components IsNot Nothing Then
                    components.Dispose()
                End If
            End If
            MyBase.Dispose(disposing)
        End Sub

        Friend WithEvents XrPanel1 As DevExpress.XtraReports.UI.XRPanel
        Friend WithEvents XrLabel_net As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_date_written As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_checknum As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_creditor_name As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel5 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel3 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel9 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents GroupHeader1 As DevExpress.XtraReports.UI.GroupHeaderBand
        Friend WithEvents GroupFooter1 As DevExpress.XtraReports.UI.GroupFooterBand
        Friend WithEvents XrLabel2 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_group_amount As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_item_date As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents ReportFooter As DevExpress.XtraReports.UI.ReportFooterBand
        Friend WithEvents XrLabel_total_amount As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel1 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_fairshare_amt As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel4 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel6 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_amount As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel8 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_group_net As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_group_fairshare As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_total_net As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_total_fairshare As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_creditor As DevExpress.XtraReports.UI.XRLabel

        'Required by the Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Designer
        'It can be modified using the Designer.
        'Do not modify it using the code editor.
        <System.Diagnostics.DebuggerStepThrough()>
        Private Sub InitializeComponent()
            Dim XrSummary1 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary
            Dim XrSummary2 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary
            Dim XrSummary3 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary
            Dim XrSummary4 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary
            Dim XrSummary5 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary
            Dim XrSummary6 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary
            Me.XrPanel1 = New DevExpress.XtraReports.UI.XRPanel
            Me.XrLabel6 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel4 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel5 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel3 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel9 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel8 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_creditor_name = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_checknum = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_date_written = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_net = New DevExpress.XtraReports.UI.XRLabel
            Me.GroupHeader1 = New DevExpress.XtraReports.UI.GroupHeaderBand
            Me.XrLabel_item_date = New DevExpress.XtraReports.UI.XRLabel
            Me.GroupFooter1 = New DevExpress.XtraReports.UI.GroupFooterBand
            Me.XrLabel_group_net = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_group_fairshare = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel2 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_group_amount = New DevExpress.XtraReports.UI.XRLabel
            Me.ReportFooter = New DevExpress.XtraReports.UI.ReportFooterBand
            Me.XrLabel_total_net = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_total_fairshare = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_total_amount = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel1 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_fairshare_amt = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_amount = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_creditor = New DevExpress.XtraReports.UI.XRLabel
            CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
            '
            'Detail
            '
            Me.Detail.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel_creditor, Me.XrLabel_amount, Me.XrLabel_fairshare_amt, Me.XrLabel_net, Me.XrLabel_date_written, Me.XrLabel_checknum, Me.XrLabel_creditor_name})
            Me.Detail.Height = 15
            '
            'PageHeader
            '
            Me.PageHeader.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrPanel1})
            Me.PageHeader.Height = 156
            '
            'XrPanel1
            '
            Me.XrPanel1.BackColor = System.Drawing.Color.Teal
            Me.XrPanel1.BorderColor = System.Drawing.Color.Teal
            Me.XrPanel1.CanGrow = False
            Me.XrPanel1.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel6, Me.XrLabel4, Me.XrLabel5, Me.XrLabel3, Me.XrLabel9, Me.XrLabel8})
            Me.XrPanel1.KeepTogether = False
            Me.XrPanel1.Location = New System.Drawing.Point(0, 133)
            Me.XrPanel1.Name = "XrPanel1"
            Me.XrPanel1.Size = New System.Drawing.Size(800, 18)
            Me.XrPanel1.StylePriority.UseBackColor = False
            Me.XrPanel1.StylePriority.UseBorderColor = False
            '
            'XrLabel6
            '
            Me.XrLabel6.BackColor = System.Drawing.Color.Transparent
            Me.XrLabel6.BorderColor = System.Drawing.Color.Transparent
            Me.XrLabel6.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel6.ForeColor = System.Drawing.Color.White
            Me.XrLabel6.Location = New System.Drawing.Point(725, 0)
            Me.XrLabel6.Name = "XrLabel6"
            Me.XrLabel6.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel6.Size = New System.Drawing.Size(72, 16)
            Me.XrLabel6.StylePriority.UseBackColor = False
            Me.XrLabel6.StylePriority.UseBorderColor = False
            Me.XrLabel6.StylePriority.UseFont = False
            Me.XrLabel6.StylePriority.UseForeColor = False
            Me.XrLabel6.StylePriority.UseTextAlignment = False
            Me.XrLabel6.Text = "GROSS"
            Me.XrLabel6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel4
            '
            Me.XrLabel4.BackColor = System.Drawing.Color.Transparent
            Me.XrLabel4.BorderColor = System.Drawing.Color.Transparent
            Me.XrLabel4.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel4.ForeColor = System.Drawing.Color.White
            Me.XrLabel4.Location = New System.Drawing.Point(575, 0)
            Me.XrLabel4.Name = "XrLabel4"
            Me.XrLabel4.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel4.Size = New System.Drawing.Size(91, 16)
            Me.XrLabel4.StylePriority.UseBackColor = False
            Me.XrLabel4.StylePriority.UseBorderColor = False
            Me.XrLabel4.StylePriority.UseFont = False
            Me.XrLabel4.StylePriority.UseForeColor = False
            Me.XrLabel4.StylePriority.UseTextAlignment = False
            Me.XrLabel4.Text = "FAIRSHARE"
            Me.XrLabel4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel5
            '
            Me.XrLabel5.BackColor = System.Drawing.Color.Transparent
            Me.XrLabel5.BorderColor = System.Drawing.Color.Transparent
            Me.XrLabel5.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel5.ForeColor = System.Drawing.Color.White
            Me.XrLabel5.Location = New System.Drawing.Point(508, 0)
            Me.XrLabel5.Name = "XrLabel5"
            Me.XrLabel5.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel5.Size = New System.Drawing.Size(66, 16)
            Me.XrLabel5.StylePriority.UseBackColor = False
            Me.XrLabel5.StylePriority.UseBorderColor = False
            Me.XrLabel5.StylePriority.UseFont = False
            Me.XrLabel5.StylePriority.UseForeColor = False
            Me.XrLabel5.StylePriority.UseTextAlignment = False
            Me.XrLabel5.Text = "NET"
            Me.XrLabel5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel3
            '
            Me.XrLabel3.BackColor = System.Drawing.Color.Transparent
            Me.XrLabel3.BorderColor = System.Drawing.Color.Transparent
            Me.XrLabel3.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel3.ForeColor = System.Drawing.Color.White
            Me.XrLabel3.Location = New System.Drawing.Point(341, 0)
            Me.XrLabel3.Name = "XrLabel3"
            Me.XrLabel3.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel3.Size = New System.Drawing.Size(83, 16)
            Me.XrLabel3.StylePriority.UseBackColor = False
            Me.XrLabel3.StylePriority.UseBorderColor = False
            Me.XrLabel3.StylePriority.UseFont = False
            Me.XrLabel3.StylePriority.UseForeColor = False
            Me.XrLabel3.StylePriority.UseTextAlignment = False
            Me.XrLabel3.Text = "CHECK #"
            Me.XrLabel3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel9
            '
            Me.XrLabel9.BackColor = System.Drawing.Color.Transparent
            Me.XrLabel9.BorderColor = System.Drawing.Color.Transparent
            Me.XrLabel9.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel9.ForeColor = System.Drawing.Color.White
            Me.XrLabel9.Location = New System.Drawing.Point(0, 0)
            Me.XrLabel9.Name = "XrLabel9"
            Me.XrLabel9.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel9.Size = New System.Drawing.Size(83, 16)
            Me.XrLabel9.StylePriority.UseBackColor = False
            Me.XrLabel9.StylePriority.UseBorderColor = False
            Me.XrLabel9.StylePriority.UseFont = False
            Me.XrLabel9.StylePriority.UseForeColor = False
            Me.XrLabel9.StylePriority.UseTextAlignment = False
            Me.XrLabel9.Text = "CREDITOR"
            Me.XrLabel9.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            '
            'XrLabel8
            '
            Me.XrLabel8.BackColor = System.Drawing.Color.Transparent
            Me.XrLabel8.BorderColor = System.Drawing.Color.Transparent
            Me.XrLabel8.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel8.ForeColor = System.Drawing.Color.White
            Me.XrLabel8.Location = New System.Drawing.Point(433, 0)
            Me.XrLabel8.Name = "XrLabel8"
            Me.XrLabel8.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel8.Size = New System.Drawing.Size(66, 16)
            Me.XrLabel8.StylePriority.UseBackColor = False
            Me.XrLabel8.StylePriority.UseBorderColor = False
            Me.XrLabel8.StylePriority.UseFont = False
            Me.XrLabel8.StylePriority.UseForeColor = False
            Me.XrLabel8.StylePriority.UseTextAlignment = False
            Me.XrLabel8.Text = "ISSUED"
            Me.XrLabel8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel_creditor_name
            '
            Me.XrLabel_creditor_name.Location = New System.Drawing.Point(68, 0)
            Me.XrLabel_creditor_name.Name = "XrLabel_creditor_name"
            Me.XrLabel_creditor_name.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_creditor_name.Size = New System.Drawing.Size(267, 15)
            Me.XrLabel_creditor_name.StylePriority.UseTextAlignment = False
            Me.XrLabel_creditor_name.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            Me.XrLabel_creditor_name.WordWrap = False
            '
            'XrLabel_checknum
            '
            Me.XrLabel_checknum.CanGrow = False
            Me.XrLabel_checknum.Location = New System.Drawing.Point(341, 0)
            Me.XrLabel_checknum.Name = "XrLabel_checknum"
            Me.XrLabel_checknum.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_checknum.Size = New System.Drawing.Size(83, 15)
            Me.XrLabel_checknum.StylePriority.UseTextAlignment = False
            Me.XrLabel_checknum.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel_date_written
            '
            Me.XrLabel_date_written.CanGrow = False
            Me.XrLabel_date_written.Location = New System.Drawing.Point(433, 0)
            Me.XrLabel_date_written.Name = "XrLabel_date_written"
            Me.XrLabel_date_written.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_date_written.Size = New System.Drawing.Size(66, 15)
            Me.XrLabel_date_written.StylePriority.UseTextAlignment = False
            Me.XrLabel_date_written.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel_net
            '
            Me.XrLabel_net.CanGrow = False
            Me.XrLabel_net.Location = New System.Drawing.Point(500, 0)
            Me.XrLabel_net.Name = "XrLabel_net"
            Me.XrLabel_net.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_net.Size = New System.Drawing.Size(74, 15)
            Me.XrLabel_net.StylePriority.UseTextAlignment = False
            Me.XrLabel_net.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'GroupHeader1
            '
            Me.GroupHeader1.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel_item_date})
            Me.GroupHeader1.GroupFields.AddRange(New DevExpress.XtraReports.UI.GroupField() {New DevExpress.XtraReports.UI.GroupField("modified_item_date", DevExpress.XtraReports.UI.XRColumnSortOrder.Ascending)})
            Me.GroupHeader1.Height = 33
            Me.GroupHeader1.Name = "GroupHeader1"
            '
            'XrLabel_item_date
            '
            Me.XrLabel_item_date.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel_item_date.ForeColor = System.Drawing.Color.Maroon
            Me.XrLabel_item_date.Location = New System.Drawing.Point(0, 8)
            Me.XrLabel_item_date.Name = "XrLabel_item_date"
            Me.XrLabel_item_date.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_item_date.Size = New System.Drawing.Size(800, 17)
            Me.XrLabel_item_date.StylePriority.UseFont = False
            Me.XrLabel_item_date.StylePriority.UseForeColor = False
            Me.XrLabel_item_date.StylePriority.UseTextAlignment = False
            Me.XrLabel_item_date.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
            '
            'GroupFooter1
            '
            Me.GroupFooter1.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel_group_net, Me.XrLabel_group_fairshare, Me.XrLabel2, Me.XrLabel_group_amount})
            Me.GroupFooter1.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.GroupFooter1.Height = 32
            Me.GroupFooter1.Name = "GroupFooter1"
            Me.GroupFooter1.StylePriority.UseFont = False
            Me.GroupFooter1.StylePriority.UseTextAlignment = False
            Me.GroupFooter1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel_group_net
            '
            Me.XrLabel_group_net.CanGrow = False
            Me.XrLabel_group_net.Location = New System.Drawing.Point(444, 8)
            Me.XrLabel_group_net.Name = "XrLabel_group_net"
            Me.XrLabel_group_net.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_group_net.Size = New System.Drawing.Size(130, 16)
            Me.XrLabel_group_net.StylePriority.UseTextAlignment = False
            XrSummary1.FormatString = "{0:c}"
            XrSummary1.Running = DevExpress.XtraReports.UI.SummaryRunning.Group
            Me.XrLabel_group_net.Summary = XrSummary1
            Me.XrLabel_group_net.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel_group_fairshare
            '
            Me.XrLabel_group_fairshare.CanGrow = False
            Me.XrLabel_group_fairshare.Location = New System.Drawing.Point(575, 8)
            Me.XrLabel_group_fairshare.Name = "XrLabel_group_fairshare"
            Me.XrLabel_group_fairshare.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_group_fairshare.Size = New System.Drawing.Size(91, 16)
            Me.XrLabel_group_fairshare.StylePriority.UseTextAlignment = False
            XrSummary2.FormatString = "{0:c}"
            XrSummary2.Running = DevExpress.XtraReports.UI.SummaryRunning.Group
            Me.XrLabel_group_fairshare.Summary = XrSummary2
            Me.XrLabel_group_fairshare.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel2
            '
            Me.XrLabel2.CanGrow = False
            Me.XrLabel2.Location = New System.Drawing.Point(0, 8)
            Me.XrLabel2.Name = "XrLabel2"
            Me.XrLabel2.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel2.Size = New System.Drawing.Size(314, 16)
            Me.XrLabel2.StylePriority.UseTextAlignment = False
            Me.XrLabel2.Text = "TOTAL"
            Me.XrLabel2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            '
            'XrLabel_group_amount
            '
            Me.XrLabel_group_amount.CanGrow = False
            Me.XrLabel_group_amount.Location = New System.Drawing.Point(667, 8)
            Me.XrLabel_group_amount.Name = "XrLabel_group_amount"
            Me.XrLabel_group_amount.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_group_amount.Size = New System.Drawing.Size(130, 16)
            Me.XrLabel_group_amount.StylePriority.UseTextAlignment = False
            XrSummary3.FormatString = "{0:c}"
            XrSummary3.Running = DevExpress.XtraReports.UI.SummaryRunning.Group
            Me.XrLabel_group_amount.Summary = XrSummary3
            Me.XrLabel_group_amount.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'ReportFooter
            '
            Me.ReportFooter.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel_total_net, Me.XrLabel_total_fairshare, Me.XrLabel_total_amount, Me.XrLabel1})
            Me.ReportFooter.Height = 32
            Me.ReportFooter.Name = "ReportFooter"
            '
            'XrLabel_total_net
            '
            Me.XrLabel_total_net.CanGrow = False
            Me.XrLabel_total_net.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel_total_net.Location = New System.Drawing.Point(444, 8)
            Me.XrLabel_total_net.Name = "XrLabel_total_net"
            Me.XrLabel_total_net.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_total_net.Size = New System.Drawing.Size(130, 16)
            Me.XrLabel_total_net.StylePriority.UseFont = False
            Me.XrLabel_total_net.StylePriority.UseTextAlignment = False
            XrSummary4.FormatString = "{0:c}"
            XrSummary4.Running = DevExpress.XtraReports.UI.SummaryRunning.Report
            Me.XrLabel_total_net.Summary = XrSummary4
            Me.XrLabel_total_net.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel_total_fairshare
            '
            Me.XrLabel_total_fairshare.CanGrow = False
            Me.XrLabel_total_fairshare.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel_total_fairshare.Location = New System.Drawing.Point(575, 8)
            Me.XrLabel_total_fairshare.Name = "XrLabel_total_fairshare"
            Me.XrLabel_total_fairshare.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_total_fairshare.Size = New System.Drawing.Size(91, 16)
            Me.XrLabel_total_fairshare.StylePriority.UseFont = False
            Me.XrLabel_total_fairshare.StylePriority.UseTextAlignment = False
            XrSummary5.FormatString = "{0:c}"
            XrSummary5.Running = DevExpress.XtraReports.UI.SummaryRunning.Report
            Me.XrLabel_total_fairshare.Summary = XrSummary5
            Me.XrLabel_total_fairshare.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel_total_amount
            '
            Me.XrLabel_total_amount.CanGrow = False
            Me.XrLabel_total_amount.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel_total_amount.Location = New System.Drawing.Point(667, 8)
            Me.XrLabel_total_amount.Name = "XrLabel_total_amount"
            Me.XrLabel_total_amount.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_total_amount.Size = New System.Drawing.Size(130, 16)
            Me.XrLabel_total_amount.StylePriority.UseFont = False
            Me.XrLabel_total_amount.StylePriority.UseTextAlignment = False
            XrSummary6.FormatString = "{0:c}"
            XrSummary6.Running = DevExpress.XtraReports.UI.SummaryRunning.Report
            Me.XrLabel_total_amount.Summary = XrSummary6
            Me.XrLabel_total_amount.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel1
            '
            Me.XrLabel1.CanGrow = False
            Me.XrLabel1.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel1.Location = New System.Drawing.Point(0, 8)
            Me.XrLabel1.Name = "XrLabel1"
            Me.XrLabel1.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel1.Size = New System.Drawing.Size(314, 16)
            Me.XrLabel1.StylePriority.UseFont = False
            Me.XrLabel1.StylePriority.UseTextAlignment = False
            Me.XrLabel1.Text = "GRAND TOTAL"
            Me.XrLabel1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            '
            'XrLabel_fairshare_amt
            '
            Me.XrLabel_fairshare_amt.CanGrow = False
            Me.XrLabel_fairshare_amt.Location = New System.Drawing.Point(575, 0)
            Me.XrLabel_fairshare_amt.Name = "XrLabel_fairshare_amt"
            Me.XrLabel_fairshare_amt.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_fairshare_amt.Size = New System.Drawing.Size(91, 15)
            Me.XrLabel_fairshare_amt.StylePriority.UseTextAlignment = False
            Me.XrLabel_fairshare_amt.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel_amount
            '
            Me.XrLabel_amount.CanGrow = False
            Me.XrLabel_amount.Location = New System.Drawing.Point(667, 0)
            Me.XrLabel_amount.Name = "XrLabel_amount"
            Me.XrLabel_amount.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_amount.Size = New System.Drawing.Size(130, 15)
            Me.XrLabel_amount.StylePriority.UseTextAlignment = False
            Me.XrLabel_amount.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel_creditor
            '
            Me.XrLabel_creditor.CanGrow = False
            Me.XrLabel_creditor.Location = New System.Drawing.Point(0, 0)
            Me.XrLabel_creditor.Name = "XrLabel_creditor"
            Me.XrLabel_creditor.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_creditor.Size = New System.Drawing.Size(67, 15)
            Me.XrLabel_creditor.StylePriority.UseTextAlignment = False
            Me.XrLabel_creditor.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            '
            'DailySummaryCreditorVoids
            '
            Me.Bands.AddRange(New DevExpress.XtraReports.UI.Band() {Me.GroupHeader1, Me.GroupFooter1, Me.ReportFooter})
            Me.Version = "8.1"
            CType(Me, System.ComponentModel.ISupportInitialize).EndInit()
        End Sub

#End Region

        Public Overrides ReadOnly Property ReportTitle() As String
            Get
                Return "Creditor Checks Voided"
            End Get
        End Property

        Dim ds As New DataSet("ds")
        Dim vue As DataView = Nothing

        Private Sub DailySummaryCreditorVoids_BeforePrint(ByVal sender As Object, ByVal e As PrintEventArgs)

            ' Read the transactions
            Dim cmd As SqlCommand = New System.Data.SqlClient.SqlCommand()
            With cmd
                .Connection = New System.Data.SqlClient.SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
                .CommandText = "rpt_summary_check_cr_voids"
                .CommandType = CommandType.StoredProcedure
                .Parameters.Add("@FromDate", SqlDbType.DateTime).Value = Parameter_FromDate
                .Parameters.Add("@ToDate", SqlDbType.DateTime).Value = Parameter_ToDate
                .CommandTimeout = 0
            End With

            Dim current_cursor As Cursor = Cursor.Current
            Try
                Cursor.Current = Cursors.WaitCursor
                Dim da As New SqlDataAdapter(cmd)
                da.Fill(ds, "records")
                With ds.Tables(0)
                    .Columns.Add("net", GetType(Decimal), "[amount]-[fairshare_amt]")
                    .Columns.Add("modified_item_date", GetType(DateTime))
                    For Each row As DataRow In .Rows
                        row("modified_item_date") = Convert.ToDateTime(row("date_created")).Date
                    Next
                End With
                vue = New DataView(ds.Tables(0), String.Empty, "modified_item_date, Creditor", DataViewRowState.CurrentRows)

                ' Bind the datasource to the items
                DataSource = vue

                ' Bind the fields to the datasource
                With XrLabel_creditor
                    .DataBindings.Add("Text", vue, "creditor")
                    .DataBindings.Add("Tag", vue, "trust_register")
                    AddHandler .PreviewClick, AddressOf XrLabel_PreviewClick
                    AddHandler .PreviewMouseMove, AddressOf Field_PreviewMouseMove
                End With

                With XrLabel_creditor_name
                    .DataBindings.Add("Text", vue, "creditor_name")
                    .DataBindings.Add("Tag", vue, "trust_register")
                    AddHandler .PreviewClick, AddressOf XrLabel_PreviewClick
                    AddHandler .PreviewMouseMove, AddressOf Field_PreviewMouseMove
                End With

                With XrLabel_net
                    .DataBindings.Add("Text", vue, "gross", "{0:c}")
                    .DataBindings.Add("Tag", vue, "trust_register")
                    AddHandler .PreviewClick, AddressOf XrLabel_PreviewClick
                    AddHandler .PreviewMouseMove, AddressOf Field_PreviewMouseMove
                End With

                With XrLabel_amount
                    .DataBindings.Add("Text", vue, "amount", "{0:c}")
                    .DataBindings.Add("Tag", vue, "trust_register")
                    AddHandler .PreviewClick, AddressOf XrLabel_PreviewClick
                    AddHandler .PreviewMouseMove, AddressOf Field_PreviewMouseMove
                End With

                With XrLabel_date_written
                    .DataBindings.Add("Text", vue, "date_written", "{0:d}")
                    .DataBindings.Add("Tag", vue, "trust_register")
                    AddHandler .PreviewClick, AddressOf XrLabel_PreviewClick
                    AddHandler .PreviewMouseMove, AddressOf Field_PreviewMouseMove
                End With

                With XrLabel_checknum
                    .DataBindings.Add("Text", vue, "checknum")
                    .DataBindings.Add("Tag", vue, "trust_register")
                    AddHandler .PreviewClick, AddressOf XrLabel_PreviewClick
                    AddHandler .PreviewMouseMove, AddressOf Field_PreviewMouseMove
                End With

                With XrLabel_item_date
                    .DataBindings.Add("Text", vue, "modified_item_date", "CREDITOR CHECK ITEMS VOIDED ON {0:d}")
                End With

                With XrLabel_fairshare_amt
                    .DataBindings.Add("Text", vue, "fairshare_amt", "{0:c}")
                    .DataBindings.Add("Tag", vue, "trust_register")
                    AddHandler .PreviewClick, AddressOf XrLabel_PreviewClick
                    AddHandler .PreviewMouseMove, AddressOf Field_PreviewMouseMove
                End With

                With XrLabel_group_fairshare
                    .DataBindings.Add("Text", vue, "fairshare_amt")
                End With

                With XrLabel_total_fairshare
                    .DataBindings.Add("Text", vue, "fairshare_amt")
                End With

                With XrLabel_net
                    .DataBindings.Add("Text", vue, "net", "{0:c}")
                    .DataBindings.Add("Tag", vue, "trust_register")
                    AddHandler .PreviewClick, AddressOf XrLabel_PreviewClick
                    AddHandler .PreviewMouseMove, AddressOf Field_PreviewMouseMove
                End With

                With XrLabel_group_net
                    .DataBindings.Add("Text", vue, "net")
                End With

                With XrLabel_total_net
                    .DataBindings.Add("Text", vue, "net")
                End With

                With XrLabel_amount
                    .DataBindings.Add("Text", vue, "amount", "{0:c}")
                    .DataBindings.Add("Tag", vue, "trust_register")
                    AddHandler .PreviewClick, AddressOf XrLabel_PreviewClick
                    AddHandler .PreviewMouseMove, AddressOf Field_PreviewMouseMove
                End With

                With XrLabel_group_amount
                    .DataBindings.Add("Text", vue, "amount")
                End With

                With XrLabel_total_amount
                    .DataBindings.Add("Text", vue, "amount")
                End With

            Catch ex As Exception
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error reading transactions")

            Finally
                Cursor.Current = current_cursor
            End Try
        End Sub

        Private Sub Field_PreviewMouseMove(ByVal sender As Object, ByVal e As PreviewMouseEventArgs)
            Dim TrustRegister As Int32 = Convert.ToInt32(e.Brick.Value)
            If TrustRegister > 0 Then
                e.PreviewControl.TopLevelControl.Cursor = Cursors.Hand
            Else
                e.PreviewControl.TopLevelControl.Cursor = Cursors.Default
            End If
        End Sub

        Private Sub XrLabel_PreviewClick(ByVal sender As Object, ByVal e As PreviewMouseEventArgs)
            Dim trust_register As Int32 = Convert.ToInt32(e.Brick.Value)

            If trust_register > 0 Then
                With New Thread(New ParameterizedThreadStart(AddressOf DetailReportThread))
                    .IsBackground = True
                    .SetApartmentState(ApartmentState.STA)
                    .Start(trust_register)
                End With
            End If
        End Sub

        Private Sub DetailReportThread(ByVal obj As Object)
            Dim TrustRegister As Int32 = Convert.ToInt32(obj)
            Dim current_cursor As Cursor = Cursor.Current

            Try
                Cursor.Current = Cursors.AppStarting
                Dim ds As New DataSet("ds")
                Dim Answer As Boolean = False
                Dim cmd As SqlCommand = New System.Data.SqlClient.SqlCommand()
                With cmd
                    .Connection = New System.Data.SqlClient.SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
                    .CommandText = "SELECT [tran_type],[trust_register],[checknum],[cleared],[reconciled_date],[bank_xmit_date],[client],[client_creditor],[debit_amt],[fairshare_amt],[creditor_type],[date_created],[void],[disbursement_register],[creditor],[creditor_name],[account_number],[client_name] FROM view_check_details_CR WITH (NOLOCK) WHERE trust_register=@trust_register"
                    .CommandType = CommandType.Text
                    .Parameters.Add("@trust_register", SqlDbType.Int).Value = TrustRegister
                    .CommandTimeout = 0
                End With

                Dim da As New SqlDataAdapter(cmd)
                da.Fill(ds, "records")
                If ds.Tables(0).Rows.Count > 0 Then
                    Dim rpt As New DetailReport
                    With rpt
                        .SetSubreportParameters(ds)
                        .CreateDocument()
                        .DisplayPreviewDialog()
                        .Dispose()
                    End With
                End If

            Finally
                Cursor.Current = current_cursor
            End Try
        End Sub
    End Class

End Namespace
