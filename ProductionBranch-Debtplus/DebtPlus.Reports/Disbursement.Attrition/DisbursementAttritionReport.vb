#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}

#End Region

Option Compare Binary
Option Explicit On
Option Strict On

Imports System.ComponentModel
Imports DebtPlus.Reports.Template

Imports System.Drawing.Printing
Imports DevExpress.XtraReports.UI
Imports System.Data.SqlClient

Namespace Disbursement.Attrition
    Public Class DisbursementAttritionReport
        Inherits TemplateXtraReportClass


        ''' <summary>
        ''' Create an instance of our report
        ''' </summary>
        Public Sub New(ByVal Container As IContainer)
            MyClass.New()
            Container.Add(Me)
        End Sub

        Public Sub New()
            MyBase.New()
            InitializeComponent()

            AddHandler BeforePrint, AddressOf Report_BeforePrint
            AddHandler XrLabel_current.BeforePrint, AddressOf XrLabel_current_BeforePrint
            AddHandler XrLabel_client.BeforePrint, AddressOf XrLabel_client_BeforePrint
            AddHandler XrLabel_current_pct.BeforePrint, AddressOf XrLabel_current_pct_BeforePrint
            AddHandler XrLabel_prior_pct.BeforePrint, AddressOf XrLabel_prior_pct_BeforePrint
        End Sub

#Region " Component Designer generated code "

        'Component overrides dispose to clean up the component list.
        Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
            If disposing Then
                If components IsNot Nothing Then
                    components.Dispose()
                End If
                ds.Dispose()
            End If
            MyBase.Dispose(disposing)
        End Sub

        'Required by the Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Designer
        'It can be modified using the Designer.
        'Do not modify it using the code editor.
        Friend WithEvents XrLabel1 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel2 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel3 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel4 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel5 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrPanel1 As DevExpress.XtraReports.UI.XRPanel
        Friend WithEvents XrLabel14 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel15 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel25 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel26 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel27 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents Style_Detail_LText As DevExpress.XtraReports.UI.XRControlStyle
        Friend WithEvents Style_Total_Amount As DevExpress.XtraReports.UI.XRControlStyle
        Friend WithEvents Style_Heading_Amount As DevExpress.XtraReports.UI.XRControlStyle
        Friend WithEvents XrControlStyle4 As DevExpress.XtraReports.UI.XRControlStyle
        Friend WithEvents Style_Heading_Pannel As DevExpress.XtraReports.UI.XRControlStyle
        Friend WithEvents Style_Detail_Amount As DevExpress.XtraReports.UI.XRControlStyle
        Friend WithEvents Style_Heading_RText As DevExpress.XtraReports.UI.XRControlStyle
        Friend WithEvents Style_Total_LText As DevExpress.XtraReports.UI.XRControlStyle
        Friend WithEvents Style_Detail_RText As DevExpress.XtraReports.UI.XRControlStyle
        Friend WithEvents XrLabel_client As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_prior As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_current As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_expected As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_name As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents GroupHeader1 As DevExpress.XtraReports.UI.GroupHeaderBand
        Friend WithEvents XrLabel_office As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents GroupFooter1 As DevExpress.XtraReports.UI.GroupFooterBand
        Friend WithEvents XrLabel_group_current As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel13 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents FormattingRule_TotalDebt As DevExpress.XtraReports.UI.FormattingRule
        Friend WithEvents FormattingRule_variance As DevExpress.XtraReports.UI.FormattingRule
        Friend WithEvents XrLabel_group_expected As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_group_prior As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents ReportFooter As DevExpress.XtraReports.UI.ReportFooterBand
        Friend WithEvents XrLine1 As DevExpress.XtraReports.UI.XRLine
        Friend WithEvents XrLabel_total_prior As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_total_expected As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel6 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_total_current As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrRichText1 As DevExpress.XtraReports.UI.XRRichText
        Friend WithEvents XrLabel_prior_pct As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel10 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_current_pct As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents GroupFooter2 As DevExpress.XtraReports.UI.GroupFooterBand
        Friend WithEvents Style_Total_RText As DevExpress.XtraReports.UI.XRControlStyle

        <System.Diagnostics.DebuggerStepThrough()>
        Private Sub InitializeComponent()
            Dim XrSummary1 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary
            Dim XrSummary2 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary
            Dim XrSummary3 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary
            Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(DisbursementAttritionReport))
            Dim XrSummary4 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary
            Dim XrSummary5 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary
            Dim XrSummary6 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary
            Dim XrSummary7 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary
            Dim XrSummary8 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary
            Me.Style_Detail_LText = New DevExpress.XtraReports.UI.XRControlStyle
            Me.Style_Total_Amount = New DevExpress.XtraReports.UI.XRControlStyle
            Me.Style_Heading_Amount = New DevExpress.XtraReports.UI.XRControlStyle
            Me.XrControlStyle4 = New DevExpress.XtraReports.UI.XRControlStyle
            Me.Style_Heading_Pannel = New DevExpress.XtraReports.UI.XRControlStyle
            Me.Style_Detail_Amount = New DevExpress.XtraReports.UI.XRControlStyle
            Me.Style_Heading_RText = New DevExpress.XtraReports.UI.XRControlStyle
            Me.Style_Total_LText = New DevExpress.XtraReports.UI.XRControlStyle
            Me.Style_Detail_RText = New DevExpress.XtraReports.UI.XRControlStyle
            Me.Style_Total_RText = New DevExpress.XtraReports.UI.XRControlStyle
            Me.XrLabel1 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel2 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel3 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel4 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel5 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrPanel1 = New DevExpress.XtraReports.UI.XRPanel
            Me.XrLabel27 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel26 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel25 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel14 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel15 = New DevExpress.XtraReports.UI.XRLabel
            Me.FormattingRule_variance = New DevExpress.XtraReports.UI.FormattingRule
            Me.XrLabel_name = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_expected = New DevExpress.XtraReports.UI.XRLabel
            Me.FormattingRule_TotalDebt = New DevExpress.XtraReports.UI.FormattingRule
            Me.XrLabel_current = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_prior = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_client = New DevExpress.XtraReports.UI.XRLabel
            Me.GroupHeader1 = New DevExpress.XtraReports.UI.GroupHeaderBand
            Me.XrLabel_office = New DevExpress.XtraReports.UI.XRLabel
            Me.GroupFooter1 = New DevExpress.XtraReports.UI.GroupFooterBand
            Me.XrLabel_group_expected = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_group_prior = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_group_current = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel13 = New DevExpress.XtraReports.UI.XRLabel
            Me.ReportFooter = New DevExpress.XtraReports.UI.ReportFooterBand
            Me.XrRichText1 = New DevExpress.XtraReports.UI.XRRichText
            Me.XrLine1 = New DevExpress.XtraReports.UI.XRLine
            Me.XrLabel_total_prior = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_total_expected = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel6 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_total_current = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel10 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_current_pct = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_prior_pct = New DevExpress.XtraReports.UI.XRLabel
            Me.GroupFooter2 = New DevExpress.XtraReports.UI.GroupFooterBand
            CType(Me.XrRichText1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
            '
            'Detail
            '
            Me.Detail.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel_client, Me.XrLabel_prior, Me.XrLabel_current, Me.XrLabel_expected, Me.XrLabel_name})
            Me.Detail.HeightF = 17.0!
            '
            'PageHeader
            '
            Me.PageHeader.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrPanel1})
            Me.PageHeader.HeightF = 150.0!
            Me.PageHeader.Controls.SetChildIndex(Me.XrPanel1, 0)
            Me.PageHeader.Controls.SetChildIndex(Me.XrPanel_AgencyAddress, 0)
            Me.PageHeader.Controls.SetChildIndex(Me.XrLabel_Title, 0)
            Me.PageHeader.Controls.SetChildIndex(Me.XrLine_Title, 0)
            Me.PageHeader.Controls.SetChildIndex(Me.XrLabel_Subtitle, 0)
            Me.PageHeader.Controls.SetChildIndex(Me.XrPageInfo1, 0)
            '
            'XrLabel_Title
            '
            Me.XrLabel_Title.StylePriority.UseFont = False
            '
            'XrPageInfo_PageNumber
            '
            Me.XrPageInfo_PageNumber.StylePriority.UseTextAlignment = False
            '
            'XRLabel_Agency_Name
            '
            Me.XRLabel_Agency_Name.StylePriority.UseTextAlignment = False
            '
            'XrLabel_Agency_Address3
            '
            Me.XrLabel_Agency_Address3.StylePriority.UseFont = False
            Me.XrLabel_Agency_Address3.StylePriority.UseTextAlignment = False
            '
            'XrLabel_Agency_Address1
            '
            Me.XrLabel_Agency_Address1.StylePriority.UseFont = False
            Me.XrLabel_Agency_Address1.StylePriority.UseTextAlignment = False
            '
            'XrLabel_Agency_Phone
            '
            Me.XrLabel_Agency_Phone.StylePriority.UseFont = False
            Me.XrLabel_Agency_Phone.StylePriority.UseTextAlignment = False
            '
            'XrLabel_Agency_Address2
            '
            Me.XrLabel_Agency_Address2.StylePriority.UseFont = False
            Me.XrLabel_Agency_Address2.StylePriority.UseTextAlignment = False
            '
            'Style_Detail_LText
            '
            Me.Style_Detail_LText.BackColor = System.Drawing.Color.Transparent
            Me.Style_Detail_LText.Name = "Style_Detail_LText"
            Me.Style_Detail_LText.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            '
            'Style_Total_Amount
            '
            Me.Style_Total_Amount.BackColor = System.Drawing.Color.Transparent
            Me.Style_Total_Amount.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.Style_Total_Amount.Name = "Style_Total_Amount"
            '
            'Style_Heading_Amount
            '
            Me.Style_Heading_Amount.BackColor = System.Drawing.Color.Transparent
            Me.Style_Heading_Amount.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.Style_Heading_Amount.ForeColor = System.Drawing.Color.White
            Me.Style_Heading_Amount.Name = "Style_Heading_Amount"
            Me.Style_Heading_Amount.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrControlStyle4
            '
            Me.XrControlStyle4.BackColor = System.Drawing.Color.Transparent
            Me.XrControlStyle4.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrControlStyle4.ForeColor = System.Drawing.Color.White
            Me.XrControlStyle4.Name = "XrControlStyle4"
            Me.XrControlStyle4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            '
            'Style_Heading_Pannel
            '
            Me.Style_Heading_Pannel.BackColor = System.Drawing.Color.Teal
            Me.Style_Heading_Pannel.BorderColor = System.Drawing.Color.Teal
            Me.Style_Heading_Pannel.Borders = CType((((DevExpress.XtraPrinting.BorderSide.Left Or DevExpress.XtraPrinting.BorderSide.Top) _
                                                      Or DevExpress.XtraPrinting.BorderSide.Right) _
                                                     Or DevExpress.XtraPrinting.BorderSide.Bottom), 
                                                    DevExpress.XtraPrinting.BorderSide)
            Me.Style_Heading_Pannel.ForeColor = System.Drawing.Color.White
            Me.Style_Heading_Pannel.Name = "Style_Heading_Pannel"
            Me.Style_Heading_Pannel.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            '
            'Style_Detail_Amount
            '
            Me.Style_Detail_Amount.BackColor = System.Drawing.Color.Transparent
            Me.Style_Detail_Amount.Name = "Style_Detail_Amount"
            Me.Style_Detail_Amount.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'Style_Heading_RText
            '
            Me.Style_Heading_RText.BackColor = System.Drawing.Color.Transparent
            Me.Style_Heading_RText.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.Style_Heading_RText.ForeColor = System.Drawing.Color.White
            Me.Style_Heading_RText.Name = "Style_Heading_RText"
            Me.Style_Heading_RText.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'Style_Total_LText
            '
            Me.Style_Total_LText.BackColor = System.Drawing.Color.Transparent
            Me.Style_Total_LText.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.Style_Total_LText.Name = "Style_Total_LText"
            '
            'Style_Detail_RText
            '
            Me.Style_Detail_RText.BackColor = System.Drawing.Color.Transparent
            Me.Style_Detail_RText.Name = "Style_Detail_RText"
            Me.Style_Detail_RText.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'Style_Total_RText
            '
            Me.Style_Total_RText.BackColor = System.Drawing.Color.Transparent
            Me.Style_Total_RText.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.Style_Total_RText.Name = "Style_Total_RText"
            '
            'XrLabel1
            '
            Me.XrLabel1.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
            Me.XrLabel1.Name = "XrLabel1"
            Me.XrLabel1.SizeF = New System.Drawing.SizeF(100.0!, 23.0!)
            Me.XrLabel1.Text = "XrLabel1"
            '
            'XrLabel2
            '
            Me.XrLabel2.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
            Me.XrLabel2.Name = "XrLabel2"
            Me.XrLabel2.SizeF = New System.Drawing.SizeF(100.0!, 23.0!)
            Me.XrLabel2.Text = "XrLabel2"
            '
            'XrLabel3
            '
            Me.XrLabel3.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
            Me.XrLabel3.Name = "XrLabel3"
            Me.XrLabel3.SizeF = New System.Drawing.SizeF(100.0!, 23.0!)
            Me.XrLabel3.Text = "XrLabel3"
            '
            'XrLabel4
            '
            Me.XrLabel4.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
            Me.XrLabel4.Name = "XrLabel4"
            Me.XrLabel4.SizeF = New System.Drawing.SizeF(100.0!, 23.0!)
            Me.XrLabel4.Text = "XrLabel4"
            '
            'XrLabel5
            '
            Me.XrLabel5.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
            Me.XrLabel5.Name = "XrLabel5"
            Me.XrLabel5.SizeF = New System.Drawing.SizeF(100.0!, 23.0!)
            Me.XrLabel5.Text = "XrLabel5"
            '
            'XrPanel1
            '
            Me.XrPanel1.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel27, Me.XrLabel26, Me.XrLabel25, Me.XrLabel14, Me.XrLabel15})
            Me.XrPanel1.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 125.0!)
            Me.XrPanel1.Name = "XrPanel1"
            Me.XrPanel1.SizeF = New System.Drawing.SizeF(800.0!, 17.0!)
            Me.XrPanel1.StyleName = "Style_Heading_Pannel"
            '
            'XrLabel27
            '
            Me.XrLabel27.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel27.LocationFloat = New DevExpress.Utils.PointFloat(83.0!, 0.0!)
            Me.XrLabel27.Name = "XrLabel27"
            Me.XrLabel27.SizeF = New System.Drawing.SizeF(116.0!, 17.0!)
            Me.XrLabel27.StylePriority.UseFont = False
            Me.XrLabel27.Text = "NAME"
            Me.XrLabel27.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            '
            'XrLabel26
            '
            Me.XrLabel26.LocationFloat = New DevExpress.Utils.PointFloat(585.0!, 1.0!)
            Me.XrLabel26.Name = "XrLabel26"
            Me.XrLabel26.SizeF = New System.Drawing.SizeF(72.0!, 15.0!)
            Me.XrLabel26.StyleName = "Style_Heading_RText"
            Me.XrLabel26.Text = "CURRENT"
            Me.XrLabel26.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel25
            '
            Me.XrLabel25.LocationFloat = New DevExpress.Utils.PointFloat(702.0!, 1.0!)
            Me.XrLabel25.Name = "XrLabel25"
            Me.XrLabel25.SizeF = New System.Drawing.SizeF(72.0!, 15.0!)
            Me.XrLabel25.StyleName = "Style_Heading_RText"
            Me.XrLabel25.Text = "PRIOR"
            Me.XrLabel25.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel14
            '
            Me.XrLabel14.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
            Me.XrLabel14.Name = "XrLabel14"
            Me.XrLabel14.SizeF = New System.Drawing.SizeF(74.0!, 17.0!)
            Me.XrLabel14.StyleName = "Style_Heading_RText"
            Me.XrLabel14.Text = "CLIENT"
            Me.XrLabel14.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel15
            '
            Me.XrLabel15.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel15.LocationFloat = New DevExpress.Utils.PointFloat(450.0!, 1.0!)
            Me.XrLabel15.Name = "XrLabel15"
            Me.XrLabel15.SizeF = New System.Drawing.SizeF(83.0!, 15.0!)
            Me.XrLabel15.StylePriority.UseFont = False
            Me.XrLabel15.StylePriority.UseTextAlignment = False
            Me.XrLabel15.Text = "EXPECTED"
            Me.XrLabel15.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'FormattingRule_variance
            '
            Me.FormattingRule_variance.Condition = "[variance]<0"
            '
            '
            '
            Me.FormattingRule_variance.Formatting.ForeColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Int32), CType(CType(0, Byte), Int32), CType(CType(0, Byte), Int32))
            Me.FormattingRule_variance.Name = "FormattingRule_variance"
            '
            'XrLabel_name
            '
            Me.XrLabel_name.CanGrow = False
            Me.XrLabel_name.Font = New System.Drawing.Font("Times New Roman", 9.75!)
            Me.XrLabel_name.ForeColor = System.Drawing.SystemColors.ControlText
            Me.XrLabel_name.LocationFloat = New DevExpress.Utils.PointFloat(82.99999!, 0.0!)
            Me.XrLabel_name.Name = "XrLabel_name"
            Me.XrLabel_name.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_name.SizeF = New System.Drawing.SizeF(335.75!, 17.0!)
            Me.XrLabel_name.StylePriority.UseFont = False
            Me.XrLabel_name.StylePriority.UseForeColor = False
            Me.XrLabel_name.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            '
            'XrLabel_expected
            '
            Me.XrLabel_expected.CanGrow = False
            Me.XrLabel_expected.Font = New System.Drawing.Font("Times New Roman", 9.75!)
            Me.XrLabel_expected.ForeColor = System.Drawing.SystemColors.ControlText
            Me.XrLabel_expected.FormattingRules.Add(Me.FormattingRule_TotalDebt)
            Me.XrLabel_expected.LocationFloat = New DevExpress.Utils.PointFloat(433.0!, 0.0!)
            Me.XrLabel_expected.Name = "XrLabel_expected"
            Me.XrLabel_expected.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_expected.SizeF = New System.Drawing.SizeF(100.0!, 17.0!)
            Me.XrLabel_expected.StylePriority.UseFont = False
            Me.XrLabel_expected.StylePriority.UseForeColor = False
            Me.XrLabel_expected.StylePriority.UseTextAlignment = False
            Me.XrLabel_expected.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'FormattingRule_TotalDebt
            '
            Me.FormattingRule_TotalDebt.Condition = "[debt] <= [deposit]"
            '
            '
            '
            Me.FormattingRule_TotalDebt.Formatting.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Int32), CType(CType(255, Byte), Int32), CType(CType(128, Byte), Int32))
            Me.FormattingRule_TotalDebt.Name = "FormattingRule_TotalDebt"
            '
            'XrLabel_current
            '
            Me.XrLabel_current.CanGrow = False
            Me.XrLabel_current.Font = New System.Drawing.Font("Times New Roman", 9.75!)
            Me.XrLabel_current.ForeColor = System.Drawing.SystemColors.ControlText
            Me.XrLabel_current.LocationFloat = New DevExpress.Utils.PointFloat(582.0!, 0.0!)
            Me.XrLabel_current.Name = "XrLabel_current"
            Me.XrLabel_current.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_current.SizeF = New System.Drawing.SizeF(75.0!, 17.0!)
            Me.XrLabel_current.StyleName = "Style_Heading_RText"
            Me.XrLabel_current.StylePriority.UseFont = False
            Me.XrLabel_current.StylePriority.UseForeColor = False
            Me.XrLabel_current.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel_prior
            '
            Me.XrLabel_prior.CanGrow = False
            Me.XrLabel_prior.Font = New System.Drawing.Font("Times New Roman", 9.75!)
            Me.XrLabel_prior.ForeColor = System.Drawing.SystemColors.ControlText
            Me.XrLabel_prior.LocationFloat = New DevExpress.Utils.PointFloat(701.0!, 0.0!)
            Me.XrLabel_prior.Name = "XrLabel_prior"
            Me.XrLabel_prior.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_prior.SizeF = New System.Drawing.SizeF(73.0!, 17.0!)
            Me.XrLabel_prior.StyleName = "Style_Heading_RText"
            Me.XrLabel_prior.StylePriority.UseFont = False
            Me.XrLabel_prior.StylePriority.UseForeColor = False
            Me.XrLabel_prior.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel_client
            '
            Me.XrLabel_client.CanGrow = False
            Me.XrLabel_client.Font = New System.Drawing.Font("Times New Roman", 9.75!)
            Me.XrLabel_client.ForeColor = System.Drawing.SystemColors.ControlText
            Me.XrLabel_client.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
            Me.XrLabel_client.Name = "XrLabel_client"
            Me.XrLabel_client.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_client.SizeF = New System.Drawing.SizeF(74.0!, 17.0!)
            Me.XrLabel_client.StyleName = "Style_Heading_RText"
            Me.XrLabel_client.StylePriority.UseFont = False
            Me.XrLabel_client.StylePriority.UseForeColor = False
            Me.XrLabel_client.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'GroupHeader1
            '
            Me.GroupHeader1.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel_office})
            Me.GroupHeader1.GroupFields.AddRange(New DevExpress.XtraReports.UI.GroupField() {New DevExpress.XtraReports.UI.GroupField("office", DevExpress.XtraReports.UI.XRColumnSortOrder.Ascending)})
            Me.GroupHeader1.GroupUnion = DevExpress.XtraReports.UI.GroupUnion.WithFirstDetail
            Me.GroupHeader1.HeightF = 42.0!
            Me.GroupHeader1.KeepTogether = True
            Me.GroupHeader1.Name = "GroupHeader1"
            Me.GroupHeader1.RepeatEveryPage = True
            '
            'XrLabel_office
            '
            Me.XrLabel_office.BorderColor = System.Drawing.Color.Maroon
            Me.XrLabel_office.Borders = CType((((DevExpress.XtraPrinting.BorderSide.Left Or DevExpress.XtraPrinting.BorderSide.Top) _
                                                Or DevExpress.XtraPrinting.BorderSide.Right) _
                                               Or DevExpress.XtraPrinting.BorderSide.Bottom), 
                                              DevExpress.XtraPrinting.BorderSide)
            Me.XrLabel_office.Font = New System.Drawing.Font("Times New Roman", 14.0!, System.Drawing.FontStyle.Bold)
            Me.XrLabel_office.ForeColor = System.Drawing.Color.Maroon
            Me.XrLabel_office.LocationFloat = New DevExpress.Utils.PointFloat(12.5!, 5.0!)
            Me.XrLabel_office.Name = "XrLabel_office"
            Me.XrLabel_office.Padding = New DevExpress.XtraPrinting.PaddingInfo(5, 5, 5, 5, 100.0!)
            Me.XrLabel_office.SizeF = New System.Drawing.SizeF(782.0!, 32.0!)
            Me.XrLabel_office.StylePriority.UseBorderColor = False
            Me.XrLabel_office.StylePriority.UseBorders = False
            Me.XrLabel_office.StylePriority.UseFont = False
            Me.XrLabel_office.StylePriority.UseForeColor = False
            Me.XrLabel_office.StylePriority.UsePadding = False
            Me.XrLabel_office.StylePriority.UseTextAlignment = False
            Me.XrLabel_office.Text = "OFFICE"
            Me.XrLabel_office.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            '
            'GroupFooter1
            '
            Me.GroupFooter1.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel_group_expected, Me.XrLabel_group_prior, Me.XrLabel_group_current, Me.XrLabel13})
            Me.GroupFooter1.GroupUnion = DevExpress.XtraReports.UI.GroupFooterUnion.WithLastDetail
            Me.GroupFooter1.HeightF = 42.0!
            Me.GroupFooter1.Name = "GroupFooter1"
            '
            'XrLabel_group_expected
            '
            Me.XrLabel_group_expected.CanGrow = False
            Me.XrLabel_group_expected.ForeColor = System.Drawing.SystemColors.ControlText
            Me.XrLabel_group_expected.LocationFloat = New DevExpress.Utils.PointFloat(427.0001!, 10.00001!)
            Me.XrLabel_group_expected.Name = "XrLabel_group_expected"
            Me.XrLabel_group_expected.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_group_expected.SizeF = New System.Drawing.SizeF(106.0!, 17.0!)
            Me.XrLabel_group_expected.StyleName = "Style_Heading_RText"
            Me.XrLabel_group_expected.StylePriority.UseForeColor = False
            XrSummary1.FormatString = "{0:c}"
            XrSummary1.Running = DevExpress.XtraReports.UI.SummaryRunning.Group
            Me.XrLabel_group_expected.Summary = XrSummary1
            Me.XrLabel_group_expected.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel_group_prior
            '
            Me.XrLabel_group_prior.CanGrow = False
            Me.XrLabel_group_prior.ForeColor = System.Drawing.SystemColors.ControlText
            Me.XrLabel_group_prior.LocationFloat = New DevExpress.Utils.PointFloat(668.0!, 10.00001!)
            Me.XrLabel_group_prior.Name = "XrLabel_group_prior"
            Me.XrLabel_group_prior.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_group_prior.SizeF = New System.Drawing.SizeF(106.0!, 17.0!)
            Me.XrLabel_group_prior.StyleName = "Style_Heading_RText"
            Me.XrLabel_group_prior.StylePriority.UseForeColor = False
            XrSummary2.FormatString = "{0:c}"
            XrSummary2.Running = DevExpress.XtraReports.UI.SummaryRunning.Group
            Me.XrLabel_group_prior.Summary = XrSummary2
            Me.XrLabel_group_prior.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel_group_current
            '
            Me.XrLabel_group_current.CanGrow = False
            Me.XrLabel_group_current.ForeColor = System.Drawing.SystemColors.ControlText
            Me.XrLabel_group_current.LocationFloat = New DevExpress.Utils.PointFloat(543.125!, 10.00001!)
            Me.XrLabel_group_current.Name = "XrLabel_group_current"
            Me.XrLabel_group_current.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_group_current.SizeF = New System.Drawing.SizeF(113.875!, 17.00001!)
            Me.XrLabel_group_current.StyleName = "Style_Heading_RText"
            Me.XrLabel_group_current.StylePriority.UseForeColor = False
            XrSummary3.FormatString = "{0:c}"
            XrSummary3.Running = DevExpress.XtraReports.UI.SummaryRunning.Group
            Me.XrLabel_group_current.Summary = XrSummary3
            Me.XrLabel_group_current.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel13
            '
            Me.XrLabel13.CanGrow = False
            Me.XrLabel13.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel13.LocationFloat = New DevExpress.Utils.PointFloat(83.00002!, 10.00001!)
            Me.XrLabel13.Name = "XrLabel13"
            Me.XrLabel13.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel13.SizeF = New System.Drawing.SizeF(116.0!, 17.0!)
            Me.XrLabel13.StylePriority.UseFont = False
            Me.XrLabel13.Text = "SUB-TOTALS"
            Me.XrLabel13.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            '
            'ReportFooter
            '
            Me.ReportFooter.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrRichText1})
            Me.ReportFooter.HeightF = 115.5417!
            Me.ReportFooter.Name = "ReportFooter"
            Me.ReportFooter.PrintAtBottom = True
            '
            'XrRichText1
            '
            Me.XrRichText1.LocationFloat = New DevExpress.Utils.PointFloat(12.5!, 10.00001!)
            Me.XrRichText1.Name = "XrRichText1"
            Me.XrRichText1.SerializableRtfString = resources.GetString("XrRichText1.SerializableRtfString")
            Me.XrRichText1.SizeF = New System.Drawing.SizeF(761.5!, 105.5417!)
            '
            'XrLine1
            '
            Me.XrLine1.LocationFloat = New DevExpress.Utils.PointFloat(83.00002!, 11.45833!)
            Me.XrLine1.Name = "XrLine1"
            Me.XrLine1.SizeF = New System.Drawing.SizeF(691.0!, 5.208333!)
            '
            'XrLabel_total_prior
            '
            Me.XrLabel_total_prior.CanGrow = False
            Me.XrLabel_total_prior.ForeColor = System.Drawing.SystemColors.ControlText
            Me.XrLabel_total_prior.LocationFloat = New DevExpress.Utils.PointFloat(668.0!, 21.45831!)
            Me.XrLabel_total_prior.Name = "XrLabel_total_prior"
            Me.XrLabel_total_prior.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_total_prior.SizeF = New System.Drawing.SizeF(106.0!, 17.0!)
            Me.XrLabel_total_prior.StyleName = "Style_Heading_RText"
            Me.XrLabel_total_prior.StylePriority.UseForeColor = False
            XrSummary4.FormatString = "{0:c}"
            XrSummary4.Running = DevExpress.XtraReports.UI.SummaryRunning.Report
            Me.XrLabel_total_prior.Summary = XrSummary4
            Me.XrLabel_total_prior.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel_total_expected
            '
            Me.XrLabel_total_expected.CanGrow = False
            Me.XrLabel_total_expected.ForeColor = System.Drawing.SystemColors.ControlText
            Me.XrLabel_total_expected.LocationFloat = New DevExpress.Utils.PointFloat(427.0001!, 21.45831!)
            Me.XrLabel_total_expected.Name = "XrLabel_total_expected"
            Me.XrLabel_total_expected.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_total_expected.SizeF = New System.Drawing.SizeF(106.0!, 17.0!)
            Me.XrLabel_total_expected.StyleName = "Style_Heading_RText"
            Me.XrLabel_total_expected.StylePriority.UseForeColor = False
            XrSummary5.FormatString = "{0:c}"
            XrSummary5.Running = DevExpress.XtraReports.UI.SummaryRunning.Report
            Me.XrLabel_total_expected.Summary = XrSummary5
            Me.XrLabel_total_expected.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel6
            '
            Me.XrLabel6.CanGrow = False
            Me.XrLabel6.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel6.LocationFloat = New DevExpress.Utils.PointFloat(83.00002!, 21.45831!)
            Me.XrLabel6.Name = "XrLabel6"
            Me.XrLabel6.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel6.SizeF = New System.Drawing.SizeF(247.25!, 17.0!)
            Me.XrLabel6.StylePriority.UseFont = False
            Me.XrLabel6.Text = "TOTAL DOLLAR AMOUNTS"
            Me.XrLabel6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            '
            'XrLabel_total_current
            '
            Me.XrLabel_total_current.CanGrow = False
            Me.XrLabel_total_current.ForeColor = System.Drawing.SystemColors.ControlText
            Me.XrLabel_total_current.LocationFloat = New DevExpress.Utils.PointFloat(543.125!, 21.45831!)
            Me.XrLabel_total_current.Name = "XrLabel_total_current"
            Me.XrLabel_total_current.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_total_current.SizeF = New System.Drawing.SizeF(113.875!, 17.0!)
            Me.XrLabel_total_current.StyleName = "Style_Heading_RText"
            Me.XrLabel_total_current.StylePriority.UseForeColor = False
            XrSummary6.FormatString = "{0:c}"
            XrSummary6.Running = DevExpress.XtraReports.UI.SummaryRunning.Report
            Me.XrLabel_total_current.Summary = XrSummary6
            Me.XrLabel_total_current.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel10
            '
            Me.XrLabel10.CanGrow = False
            Me.XrLabel10.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel10.LocationFloat = New DevExpress.Utils.PointFloat(83.00005!, 38.45838!)
            Me.XrLabel10.Name = "XrLabel10"
            Me.XrLabel10.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel10.SizeF = New System.Drawing.SizeF(247.25!, 17.0!)
            Me.XrLabel10.StylePriority.UseFont = False
            Me.XrLabel10.Text = "PERCENTAGE TO TOTAL AMOUNTS"
            Me.XrLabel10.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            '
            'XrLabel_current_pct
            '
            Me.XrLabel_current_pct.CanGrow = False
            Me.XrLabel_current_pct.ForeColor = System.Drawing.SystemColors.ControlText
            Me.XrLabel_current_pct.LocationFloat = New DevExpress.Utils.PointFloat(543.0!, 38.45838!)
            Me.XrLabel_current_pct.Name = "XrLabel_current_pct"
            Me.XrLabel_current_pct.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_current_pct.SizeF = New System.Drawing.SizeF(113.875!, 17.0!)
            Me.XrLabel_current_pct.StyleName = "Style_Heading_RText"
            Me.XrLabel_current_pct.StylePriority.UseForeColor = False
            XrSummary7.FormatString = "{0:c}"
            XrSummary7.Running = DevExpress.XtraReports.UI.SummaryRunning.Group
            Me.XrLabel_current_pct.Summary = XrSummary7
            Me.XrLabel_current_pct.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel_prior_pct
            '
            Me.XrLabel_prior_pct.CanGrow = False
            Me.XrLabel_prior_pct.ForeColor = System.Drawing.SystemColors.ControlText
            Me.XrLabel_prior_pct.LocationFloat = New DevExpress.Utils.PointFloat(668.0!, 38.45838!)
            Me.XrLabel_prior_pct.Name = "XrLabel_prior_pct"
            Me.XrLabel_prior_pct.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_prior_pct.SizeF = New System.Drawing.SizeF(106.0!, 17.0!)
            Me.XrLabel_prior_pct.StyleName = "Style_Heading_RText"
            Me.XrLabel_prior_pct.StylePriority.UseForeColor = False
            XrSummary8.FormatString = "{0:c}"
            XrSummary8.Running = DevExpress.XtraReports.UI.SummaryRunning.Group
            Me.XrLabel_prior_pct.Summary = XrSummary8
            Me.XrLabel_prior_pct.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'GroupFooter2
            '
            Me.GroupFooter2.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel6, Me.XrLabel_total_current, Me.XrLabel_total_expected, Me.XrLabel_total_prior, Me.XrLine1, Me.XrLabel_current_pct, Me.XrLabel10, Me.XrLabel_prior_pct})
            Me.GroupFooter2.GroupUnion = DevExpress.XtraReports.UI.GroupFooterUnion.WithLastDetail
            Me.GroupFooter2.HeightF = 64.0!
            Me.GroupFooter2.KeepTogether = True
            Me.GroupFooter2.Level = 1
            Me.GroupFooter2.Name = "GroupFooter2"
            '
            Me.XrLabel_office.DataBindings.Add("Text", Nothing, "office")
            Me.XrLabel_name.DataBindings.Add("Text", Nothing, "name")
            Me.XrLabel_current.DataBindings.Add("Text", Nothing, "current", "{0:c}")
            Me.XrLabel_expected.DataBindings.Add("Text", Nothing, "expected", "{0:c}")
            Me.XrLabel_prior.DataBindings.Add("Text", Nothing, "prior", "{0:c}")
            Me.XrLabel_group_current.DataBindings.Add("Text", Nothing, "current")
            Me.XrLabel_group_expected.DataBindings.Add("Text", Nothing, "expected")
            Me.XrLabel_group_prior.DataBindings.Add("Text", Nothing, "prior")
            Me.XrLabel_total_current.DataBindings.Add("Text", Nothing, "current")
            Me.XrLabel_total_expected.DataBindings.Add("Text", Nothing, "expected")
            Me.XrLabel_total_prior.DataBindings.Add("Text", Nothing, "prior")
            '
            'DisbursementAttritionReport
            '
            Me.Bands.AddRange(New DevExpress.XtraReports.UI.Band() {Me.Detail, Me.PageHeader, Me.PageFooter, Me.TopMarginBand1, Me.BottomMarginBand1, Me.GroupHeader1, Me.GroupFooter1, Me.ReportFooter, Me.GroupFooter2})
            Me.FormattingRuleSheet.AddRange(New DevExpress.XtraReports.UI.FormattingRule() {Me.FormattingRule_TotalDebt, Me.FormattingRule_variance})
            Me.StyleSheet.AddRange(New DevExpress.XtraReports.UI.XRControlStyle() {Me.XrControlStyle_Header, Me.XrControlStyle_Totals, Me.XrControlStyle_HeaderPannel, Me.XrControlStyle_GroupHeader, Me.Style_Detail_LText, Me.Style_Total_Amount, Me.Style_Heading_Amount, Me.XrControlStyle4, Me.Style_Heading_Pannel, Me.Style_Detail_Amount, Me.Style_Heading_RText, Me.Style_Total_LText, Me.Style_Detail_RText, Me.Style_Total_RText})
            Me.Version = "10.2"
            Me.Controls.SetChildIndex(Me.GroupFooter2, 0)
            Me.Controls.SetChildIndex(Me.ReportFooter, 0)
            Me.Controls.SetChildIndex(Me.GroupFooter1, 0)
            Me.Controls.SetChildIndex(Me.GroupHeader1, 0)
            Me.Controls.SetChildIndex(Me.BottomMarginBand1, 0)
            Me.Controls.SetChildIndex(Me.TopMarginBand1, 0)
            Me.Controls.SetChildIndex(Me.PageFooter, 0)
            Me.Controls.SetChildIndex(Me.PageHeader, 0)
            Me.Controls.SetChildIndex(Me.Detail, 0)
            CType(Me.XrRichText1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me, System.ComponentModel.ISupportInitialize).EndInit()
        End Sub

#End Region


        ''' <summary>
        ''' Report Title string
        ''' </summary>
        Public Overrides ReadOnly Property ReportTitle() As String
            Get
                Return "Disbursement Attrition"
            End Get
        End Property

        Private ds As New DataSet("ds")

        ''' <summary>
        ''' Bind the dataset to the report
        ''' </summary>
        Private Sub Report_BeforePrint(ByVal sender As Object, ByVal e As PrintEventArgs)
            Dim rpt As XtraReport = CType(sender, XtraReport)
            Dim tbl As DataTable

            Using cmd As SqlCommand = CType(New System.Data.SqlClient.SqlCommand(), SqlCommand)
                With cmd
                    .Connection = New System.Data.SqlClient.SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
                    .CommandText = "rpt_DisbursementAttritrition"
                    .CommandType = CommandType.StoredProcedure
                    .CommandTimeout = 0
                End With

                Using da As New SqlDataAdapter(cmd)
                    da.Fill(ds, "rpt_DisbursementAttritrition")
                    tbl = ds.Tables("rpt_DisbursementAttritrition")
                End Using
            End Using

            rpt.DataSource = tbl.DefaultView
        End Sub


        ''' <summary>
        ''' Foramt the client ID when needed
        ''' </summary>
        Private Sub XrLabel_client_BeforePrint(ByVal sender As Object, ByVal e As PrintEventArgs)
            With CType(sender, XRLabel)
                Dim rpt As XtraReport = CType(.Report, XtraReport)
                .Text = String.Format("{0:0000000}", rpt.GetCurrentColumnValue("client"))
            End With
        End Sub


        ''' <summary>
        ''' Find the total expected disbursement amount
        ''' </summary>
        Private Function ExpectedAmount(ByVal rpt As XtraReport) As Decimal
            Dim tbl As DataTable = CType(rpt.DataSource, DataView).Table
            Dim obj As Object = tbl.Compute("sum(expected)", String.Empty)
            Return DebtPlus.Utils.Nulls.DDec(obj)
        End Function


        ''' <summary>
        ''' Format a percentage figure
        ''' </summary>
        Private Function Percentage(ByVal rpt As XtraReport, ByVal Disbursed As Object) As String
            Dim Answer As Double
            Dim ExpectedFigure As Decimal = ExpectedAmount(rpt)

            Try
                If ExpectedFigure > 0D Then
                    Answer = Convert.ToDouble(Disbursed) / Convert.ToDouble(ExpectedFigure)
                    If Not Double.IsInfinity(Answer) AndAlso Not Double.IsNaN(Answer) Then
                        Return String.Format("{0:p}", Answer)
                    End If
                End If

            Catch ex As Exception
                ' do nothing here
            End Try

            Return "n/a"
        End Function


        ''' <summary>
        ''' Prior month percentage disbursed
        ''' </summary>
        Private Sub XrLabel_prior_pct_BeforePrint(ByVal sender As Object, ByVal e As PrintEventArgs)
            With CType(sender, XRLabel)
                Dim rpt As XtraReport = CType(.Report, XtraReport)

                Dim tbl As DataTable = CType(rpt.DataSource, DataView).Table
                Dim obj As Object = tbl.Compute("sum(prior)", String.Empty)
                .Text = Percentage(rpt, obj)
            End With
        End Sub


        ''' <summary>
        ''' Current month percentage disbursed
        ''' </summary>
        Private Sub XrLabel_current_pct_BeforePrint(ByVal sender As Object, ByVal e As PrintEventArgs)
            With CType(sender, XRLabel)
                Dim rpt As XtraReport = CType(.Report, XtraReport)

                Dim tbl As DataTable = CType(rpt.DataSource, DataView).Table
                Dim obj As Object = tbl.Compute("sum(current)", String.Empty)
                .Text = Percentage(rpt, obj)
            End With
        End Sub

        Private Sub XrLabel_current_BeforePrint(ByVal sender As Object, ByVal e As PrintEventArgs)
            With CType(sender, XRLabel)
                Dim rpt As XtraReport = CType(.Report, XtraReport)
                Dim color As Color
                If DebtPlus.Utils.Nulls.DDec(rpt.GetCurrentColumnValue("current")) > DebtPlus.Utils.Nulls.DDec(rpt.GetCurrentColumnValue("expected")) Then
                    color = color.Red
                ElseIf DebtPlus.Utils.Nulls.DDec(rpt.GetCurrentColumnValue("expected")) * 0.8 > DebtPlus.Utils.Nulls.DDec(rpt.GetCurrentColumnValue("current")) Then
                    color = color.Blue
                Else
                    color = color.Black
                End If
                .ForeColor = color
            End With
        End Sub
    End Class
End Namespace
