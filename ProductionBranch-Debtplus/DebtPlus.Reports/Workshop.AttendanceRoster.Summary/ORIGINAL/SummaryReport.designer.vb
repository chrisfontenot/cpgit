﻿Namespace Workshop.AttendanceRoster.Summary
    Partial Class SummaryReport
        Inherits DebtPlus.Reports.Template.DatedTemplateXtraReportClass

        'Component overrides dispose to clean up the component list.
        Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
            Try
                If disposing Then
                    If components IsNot Nothing Then
                        components.Dispose()
                    End If
                    ds.Dispose()
                End If
            Finally
                MyBase.Dispose(disposing)
            End Try
        End Sub

        'Required by the Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Designer
        'It can be modified using the Designer.
        'Do not modify it using the code editor.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
            Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(SummaryReport))
            Me.XrPanel1 = New DevExpress.XtraReports.UI.XRPanel()
            Me.XrLabel2 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel15 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel14 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel13 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel12 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel11 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel4 = New DevExpress.XtraReports.UI.XRLabel()
            Me.ReportFooter = New DevExpress.XtraReports.UI.ReportFooterBand()
            Me.XrLabel1 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_workshop = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_attended = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_count_other = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_count_inactive = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_count_wks = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_count_active = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_no_show = New DevExpress.XtraReports.UI.XRLabel()
            CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
            '
            'PageHeader
            '
            Me.PageHeader.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrPanel1})
            Me.PageHeader.HeightF = 149.0!
            Me.PageHeader.Controls.SetChildIndex(Me.XrPanel1, 0)
            Me.PageHeader.Controls.SetChildIndex(Me.XrPanel_AgencyAddress, 0)
            Me.PageHeader.Controls.SetChildIndex(Me.XrLabel_Title, 0)
            Me.PageHeader.Controls.SetChildIndex(Me.XrLine_Title, 0)
            Me.PageHeader.Controls.SetChildIndex(Me.XrLabel_Subtitle, 0)
            Me.PageHeader.Controls.SetChildIndex(Me.XrPageInfo1, 0)
            '
            'XrLabel_Title
            '
            Me.XrLabel_Title.StylePriority.UseFont = False
            '
            'XrPageInfo_PageNumber
            '
            Me.XrPageInfo_PageNumber.StylePriority.UseTextAlignment = False
            '
            'XRLabel_Agency_Name
            '
            Me.XRLabel_Agency_Name.StylePriority.UseTextAlignment = False
            '
            'XrLabel_Agency_Address3
            '
            Me.XrLabel_Agency_Address3.StylePriority.UseFont = False
            Me.XrLabel_Agency_Address3.StylePriority.UseTextAlignment = False
            '
            'XrLabel_Agency_Address1
            '
            Me.XrLabel_Agency_Address1.StylePriority.UseFont = False
            Me.XrLabel_Agency_Address1.StylePriority.UseTextAlignment = False
            '
            'XrLabel_Agency_Phone
            '
            Me.XrLabel_Agency_Phone.StylePriority.UseFont = False
            Me.XrLabel_Agency_Phone.StylePriority.UseTextAlignment = False
            '
            'XrLabel_Agency_Address2
            '
            Me.XrLabel_Agency_Address2.StylePriority.UseFont = False
            Me.XrLabel_Agency_Address2.StylePriority.UseTextAlignment = False
            '
            'Detail
            '
            Me.Detail.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel_count_wks, Me.XrLabel_count_active, Me.XrLabel_no_show, Me.XrLabel_count_inactive, Me.XrLabel_workshop, Me.XrLabel_attended, Me.XrLabel_count_other})
            Me.Detail.HeightF = 15.0!
            '
            'XrPanel1
            '
            Me.XrPanel1.BackColor = System.Drawing.Color.Teal
            Me.XrPanel1.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel2, Me.XrLabel15, Me.XrLabel14, Me.XrLabel13, Me.XrLabel12, Me.XrLabel11, Me.XrLabel4})
            Me.XrPanel1.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 120.0!)
            Me.XrPanel1.Name = "XrPanel1"
            Me.XrPanel1.SizeF = New System.Drawing.SizeF(800.0!, 17.0!)
            '
            'XrLabel2
            '
            Me.XrLabel2.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.XrLabel2.ForeColor = System.Drawing.Color.White
            Me.XrLabel2.LocationFloat = New DevExpress.Utils.PointFloat(460.0!, 1.0!)
            Me.XrLabel2.Name = "XrLabel2"
            Me.XrLabel2.SizeF = New System.Drawing.SizeF(81.0!, 15.0!)
            Me.XrLabel2.StylePriority.UseTextAlignment = False
            Me.XrLabel2.Text = "INACTIVE"
            Me.XrLabel2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel15
            '
            Me.XrLabel15.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.XrLabel15.ForeColor = System.Drawing.Color.White
            Me.XrLabel15.LocationFloat = New DevExpress.Utils.PointFloat(716.0!, 1.0!)
            Me.XrLabel15.Name = "XrLabel15"
            Me.XrLabel15.SizeF = New System.Drawing.SizeF(75.0!, 15.0!)
            Me.XrLabel15.StylePriority.UseTextAlignment = False
            Me.XrLabel15.Text = "NO SHOW"
            Me.XrLabel15.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel14
            '
            Me.XrLabel14.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.XrLabel14.ForeColor = System.Drawing.Color.White
            Me.XrLabel14.LocationFloat = New DevExpress.Utils.PointFloat(625.0!, 1.0!)
            Me.XrLabel14.Name = "XrLabel14"
            Me.XrLabel14.SizeF = New System.Drawing.SizeF(83.0!, 15.0!)
            Me.XrLabel14.StylePriority.UseTextAlignment = False
            Me.XrLabel14.Text = "ATTENDED"
            Me.XrLabel14.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel13
            '
            Me.XrLabel13.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.XrLabel13.ForeColor = System.Drawing.Color.White
            Me.XrLabel13.LocationFloat = New DevExpress.Utils.PointFloat(550.0!, 1.0!)
            Me.XrLabel13.Name = "XrLabel13"
            Me.XrLabel13.SizeF = New System.Drawing.SizeF(66.0!, 15.0!)
            Me.XrLabel13.StylePriority.UseTextAlignment = False
            Me.XrLabel13.Text = "OTHER"
            Me.XrLabel13.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel12
            '
            Me.XrLabel12.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.XrLabel12.ForeColor = System.Drawing.Color.White
            Me.XrLabel12.LocationFloat = New DevExpress.Utils.PointFloat(397.0!, 1.0!)
            Me.XrLabel12.Name = "XrLabel12"
            Me.XrLabel12.SizeF = New System.Drawing.SizeF(60.0!, 15.0!)
            Me.XrLabel12.StylePriority.UseTextAlignment = False
            Me.XrLabel12.Text = "WKS"
            Me.XrLabel12.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel11
            '
            Me.XrLabel11.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.XrLabel11.ForeColor = System.Drawing.Color.White
            Me.XrLabel11.LocationFloat = New DevExpress.Utils.PointFloat(320.0!, 1.0!)
            Me.XrLabel11.Name = "XrLabel11"
            Me.XrLabel11.SizeF = New System.Drawing.SizeF(70.0!, 15.0!)
            Me.XrLabel11.StylePriority.UseTextAlignment = False
            Me.XrLabel11.Text = "ACTIVE"
            Me.XrLabel11.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel4
            '
            Me.XrLabel4.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.XrLabel4.ForeColor = System.Drawing.Color.White
            Me.XrLabel4.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 1.0!)
            Me.XrLabel4.Name = "XrLabel4"
            Me.XrLabel4.SizeF = New System.Drawing.SizeF(308.0!, 15.0!)
            Me.XrLabel4.StylePriority.UseTextAlignment = False
            Me.XrLabel4.Text = "WORKSHOP"
            Me.XrLabel4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            '
            'ReportFooter
            '
            Me.ReportFooter.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel1})
            Me.ReportFooter.HeightF = 40.0!
            Me.ReportFooter.Name = "ReportFooter"
            Me.ReportFooter.PrintAtBottom = True
            '
            'XrLabel1
            '
            Me.XrLabel1.Font = New System.Drawing.Font("Times New Roman", 8.0!)
            Me.XrLabel1.ForeColor = System.Drawing.Color.DarkGray
            Me.XrLabel1.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
            Me.XrLabel1.Multiline = True
            Me.XrLabel1.Name = "XrLabel1"
            Me.XrLabel1.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel1.SizeF = New System.Drawing.SizeF(750.0!, 39.99999!)
            Me.XrLabel1.StylePriority.UseFont = False
            Me.XrLabel1.StylePriority.UseForeColor = False
            Me.XrLabel1.StylePriority.UseTextAlignment = False
            Me.XrLabel1.Text = resources.GetString("XrLabel1.Text")
            Me.XrLabel1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            '
            'XrLabel_workshop
            '
            Me.XrLabel_workshop.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "workshop")})
            Me.XrLabel_workshop.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.XrLabel_workshop.ForeColor = System.Drawing.Color.Black
            Me.XrLabel_workshop.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
            Me.XrLabel_workshop.Name = "XrLabel_workshop"
            Me.XrLabel_workshop.SizeF = New System.Drawing.SizeF(308.0!, 15.0!)
            Me.XrLabel_workshop.StylePriority.UseForeColor = False
            Me.XrLabel_workshop.StylePriority.UseTextAlignment = False
            Me.XrLabel_workshop.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            '
            'XrLabel_attended
            '
            Me.XrLabel_attended.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "attended")})
            Me.XrLabel_attended.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.XrLabel_attended.ForeColor = System.Drawing.Color.Black
            Me.XrLabel_attended.LocationFloat = New DevExpress.Utils.PointFloat(625.0!, 0.0!)
            Me.XrLabel_attended.Name = "XrLabel_attended"
            Me.XrLabel_attended.SizeF = New System.Drawing.SizeF(83.0!, 15.0!)
            Me.XrLabel_attended.StylePriority.UseForeColor = False
            Me.XrLabel_attended.StylePriority.UseTextAlignment = False
            Me.XrLabel_attended.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel_count_other
            '
            Me.XrLabel_count_other.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "count_other")})
            Me.XrLabel_count_other.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.XrLabel_count_other.ForeColor = System.Drawing.Color.Black
            Me.XrLabel_count_other.LocationFloat = New DevExpress.Utils.PointFloat(550.0!, 0.0!)
            Me.XrLabel_count_other.Name = "XrLabel_count_other"
            Me.XrLabel_count_other.SizeF = New System.Drawing.SizeF(66.0!, 15.0!)
            Me.XrLabel_count_other.StylePriority.UseForeColor = False
            Me.XrLabel_count_other.StylePriority.UseTextAlignment = False
            Me.XrLabel_count_other.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel_count_inactive
            '
            Me.XrLabel_count_inactive.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "count_inactive")})
            Me.XrLabel_count_inactive.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.XrLabel_count_inactive.ForeColor = System.Drawing.Color.Black
            Me.XrLabel_count_inactive.LocationFloat = New DevExpress.Utils.PointFloat(460.0!, 0.0!)
            Me.XrLabel_count_inactive.Name = "XrLabel_count_inactive"
            Me.XrLabel_count_inactive.SizeF = New System.Drawing.SizeF(81.0!, 15.0!)
            Me.XrLabel_count_inactive.StylePriority.UseForeColor = False
            Me.XrLabel_count_inactive.StylePriority.UseTextAlignment = False
            Me.XrLabel_count_inactive.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel_count_wks
            '
            Me.XrLabel_count_wks.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "count_wks")})
            Me.XrLabel_count_wks.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.XrLabel_count_wks.ForeColor = System.Drawing.Color.Black
            Me.XrLabel_count_wks.LocationFloat = New DevExpress.Utils.PointFloat(397.0!, 0.0!)
            Me.XrLabel_count_wks.Name = "XrLabel_count_wks"
            Me.XrLabel_count_wks.SizeF = New System.Drawing.SizeF(60.0!, 15.0!)
            Me.XrLabel_count_wks.StylePriority.UseForeColor = False
            Me.XrLabel_count_wks.StylePriority.UseTextAlignment = False
            Me.XrLabel_count_wks.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel_count_active
            '
            Me.XrLabel_count_active.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "count_active")})
            Me.XrLabel_count_active.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.XrLabel_count_active.ForeColor = System.Drawing.Color.Black
            Me.XrLabel_count_active.LocationFloat = New DevExpress.Utils.PointFloat(320.0!, 0.0!)
            Me.XrLabel_count_active.Name = "XrLabel_count_active"
            Me.XrLabel_count_active.SizeF = New System.Drawing.SizeF(70.0!, 15.0!)
            Me.XrLabel_count_active.StylePriority.UseForeColor = False
            Me.XrLabel_count_active.StylePriority.UseTextAlignment = False
            Me.XrLabel_count_active.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel_no_show
            '
            Me.XrLabel_no_show.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "no_show")})
            Me.XrLabel_no_show.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.XrLabel_no_show.ForeColor = System.Drawing.Color.Black
            Me.XrLabel_no_show.LocationFloat = New DevExpress.Utils.PointFloat(716.0!, 0.0!)
            Me.XrLabel_no_show.Name = "XrLabel_no_show"
            Me.XrLabel_no_show.SizeF = New System.Drawing.SizeF(75.0!, 15.0!)
            Me.XrLabel_no_show.StylePriority.UseForeColor = False
            Me.XrLabel_no_show.StylePriority.UseTextAlignment = False
            Me.XrLabel_no_show.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'SummaryReport
            '
            Me.Bands.AddRange(New DevExpress.XtraReports.UI.Band() {Me.Detail, Me.PageHeader, Me.PageFooter, Me.ReportFooter, Me.TopMarginBand1, Me.BottomMarginBand1})
            Me.ScriptLanguage = DevExpress.XtraReports.ScriptLanguage.VisualBasic
            Me.ScriptReferencesString = "C:\Users\Al Longyear\Documents\Visual Studio 2008\Projects\DebtPlus\Executables\D" & _
        "ebtPlus.Data.dll"
            Me.Scripts.OnBeforePrint = "SummaryReport_BeforePrint"
            Me.ScriptsSource = resources.GetString("$this.ScriptsSource")
            Me.StyleSheet.AddRange(New DevExpress.XtraReports.UI.XRControlStyle() {Me.XrControlStyle_Header, Me.XrControlStyle_Totals, Me.XrControlStyle_HeaderPannel, Me.XrControlStyle_GroupHeader})
            Me.Version = "11.2"
            Me.Controls.SetChildIndex(Me.BottomMarginBand1, 0)
            Me.Controls.SetChildIndex(Me.TopMarginBand1, 0)
            Me.Controls.SetChildIndex(Me.ReportFooter, 0)
            Me.Controls.SetChildIndex(Me.PageFooter, 0)
            Me.Controls.SetChildIndex(Me.PageHeader, 0)
            Me.Controls.SetChildIndex(Me.Detail, 0)
            CType(Me, System.ComponentModel.ISupportInitialize).EndInit()

        End Sub

        Friend WithEvents XrPanel1 As DevExpress.XtraReports.UI.XRPanel
        Friend WithEvents XrLabel4 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel11 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel12 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel13 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel14 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel15 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents ReportFooter As DevExpress.XtraReports.UI.ReportFooterBand
        Friend WithEvents XrLabel1 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel2 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_count_wks As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_count_active As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_no_show As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_count_inactive As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_workshop As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_attended As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_count_other As DevExpress.XtraReports.UI.XRLabel
    End Class
End Namespace
