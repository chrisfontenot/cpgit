#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}
#End Region

Imports System.ComponentModel
Imports System.Drawing.Printing
Imports System.IO
Imports System.Threading
Imports DevExpress.XtraReports.Parameters
Imports DevExpress.XtraReports.UI

Namespace Template
    ''' <summary>
    ''' A base class for all report objects.
    ''' </summary>
    Partial Public Class BaseXtraReportClass
        Inherits DevExpress.XtraReports.UI.XtraReport
        Implements DebtPlus.Interfaces.Reports.IReportFilter
        Implements DebtPlus.Interfaces.Reports.IReports

        Public Sub New()
            MyBase.New()

            ReportFilter = New DebtPlus.Interfaces.Reports.ReportFilterItems()
            AllowParameterChangesByUser = False

            InitializeComponent()
            Margins = New System.Drawing.Printing.Margins(25, 25, 25, 25)
        End Sub

        Public ReportName As String = "No Name Yet"
        Public UsingDefaultLayout As Boolean = True

#Region "Initialize Components"
        Protected TopMarginBand1 As TopMarginBand
        Protected Detail As DetailBand
        Protected BottomMarginBand1 As BottomMarginBand

        Private Sub InitializeComponent()
            TopMarginBand1 = New TopMarginBand()
            Detail = New DetailBand()
            BottomMarginBand1 = New BottomMarginBand()
            DirectCast(Me, ISupportInitialize).BeginInit()
            '
            'TopMarginBand1
            '
            TopMarginBand1.HeightF = 25.0F
            TopMarginBand1.Name = "TopMarginBand1"
            '
            'Detail
            '
            Detail.Name = "Detail"
            '
            'BottomMarginBand1
            '
            BottomMarginBand1.HeightF = 25.0F
            BottomMarginBand1.Name = "BottomMarginBand1"
            '
            'BaseXtraReportClass
            '
            Bands.AddRange(New Band() {TopMarginBand1, Detail, BottomMarginBand1})
            Margins = New Margins(25, 25, 25, 25)
            Version = "11.2"
            DirectCast(Me, ISupportInitialize).EndInit()
        End Sub

#End Region

        ''' <summary>
        ''' Setup the report
        ''' </summary>
        Private Sub InitializeReport()
            ' Ensure that the parameters are all hidden.
            For Each parm As DevExpress.XtraReports.Parameters.Parameter In Me.Parameters
                parm.Visible = False
            Next

            PrintingSystem.Document.Name = If(String.IsNullOrEmpty(ReportTitle), "DebtPlus Report", ReportTitle)
        End Sub

        ''' <summary>
        ''' Hook into the routine called before the report is generated. Do any initialization
        ''' that is needed for the report at this time.
        ''' </summary>
        Protected Overrides Sub BeforeReportPrint()
            InitializeReport()
            MyBase.BeforeReportPrint()
        End Sub

        ''' <summary>
        ''' Do we need to show the parameters dialog?
        ''' </summary>
        ''' <returns>TRUE if the dialog should be shown</returns>
        Public Overridable Function NeedParameters() As Boolean
            Return False
        End Function

        ''' <summary>
        ''' Locate the parameter in the list by name
        ''' </summary>
        Protected Function FindParameter(ParameterName As String) As Parameter
            Return Parameters.AsQueryable().OfType(Of DevExpress.XtraReports.Parameters.Parameter)().Where(Function(s) String.Compare(s.Name, ParameterName) = 0).FirstOrDefault()
        End Function

        ''' <summary>
        ''' Set the parameter for the report
        ''' </summary>
        Protected Sub SetParameter(pname As String, type As Type, value As Object, description As String, canSee As Boolean)
            If String.IsNullOrWhiteSpace(pname) Then
                Throw New ArgumentNullException()
            End If

            If Parameters Is Nothing Then
                Throw New System.ArgumentException("Parameters must not be null")
            End If

            ' Find the parameter in the list of parameters
            Dim param As Parameter = FindParameter(pname)

            ' If the parameter is not found, add it
            If param Is Nothing Then
                param = New Parameter()
                param.Name = pname
                param.Type = type
                param.Description = description
                param.Visible = canSee
                Parameters.Add(param)
            End If

            ' Update the value for the parameter
            param.Value = value
        End Sub

        ''' <summary>
        ''' Handle the customize report event from the preview
        ''' </summary>
        Protected Sub CustomizeReportEvent(Sender As Object, e As EventArgs)
            Try
                ShowDesignerDialog()
            Catch ex As System.Exception
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error customizing report")
            End Try
        End Sub

#Region "DebtPlus.Reports.IReport interface routines"

#Region "ReportTitle"

        ''' <summary>
        ''' Event raised when the report wants the title. This is not cached. It is raised
        ''' with every request for the title string.
        ''' </summary>
        Public Event GetTitleString As TitleStringEventHandler

        ''' <summary>
        ''' Raise the GetTitleString event
        ''' </summary>
        ''' <param name="e">parameters to the event</param>
        Protected Sub RaiseReportTitle(e As TitleStringEventArgs)
            RaiseEvent GetTitleString(Me, e)
        End Sub

        ''' <summary>
        ''' Linkage to the GetTitleString event. This may be overridden.
        ''' </summary>
        ''' <param name="e">parameters to the event</param>
        Protected Overridable Sub OnReportTitle(e As TitleStringEventArgs)
            RaiseReportTitle(e)
        End Sub

        ''' <summary>
        ''' Title associated with the report. This is on the report, the PreviewDialog, and the report printer document name
        ''' </summary>
        Public Overridable ReadOnly Property ReportTitle() As String Implements DebtPlus.Interfaces.Reports.IReports.ReportTitle
            Get
                Dim e = New TitleStringEventArgs()
                OnReportTitle(e)
                Return e.Title
            End Get
        End Property

#End Region

#Region "ReportSubTitle"

        ''' <summary>
        ''' Event raised when the report wants the title. This is not cached. It is raised
        ''' with every request for the title string.
        ''' </summary>
        Public Event GetSubTitleString As TitleStringEventHandler

        ''' <summary>
        ''' Raise the GetSubTitleString event
        ''' </summary>
        ''' <param name="e">parameters to the event</param>
        Protected Sub RaiseReportSubTitle(e As TitleStringEventArgs)
            RaiseEvent GetSubTitleString(Me, e)
        End Sub

        ''' <summary>
        ''' Linkage to the GetSubTitleString event. This may be overridden.
        ''' </summary>
        ''' <param name="e">parameters to the event</param>
        Protected Overridable Sub OnReportSubTitle(e As TitleStringEventArgs)
            RaiseReportSubTitle(e)
        End Sub

        ''' <summary>
        ''' SubTitle associated with the report. This is on the report, the PreviewDialog, and the report printer document name
        ''' </summary>
        Public Overridable ReadOnly Property ReportSubTitle() As String Implements DebtPlus.Interfaces.Reports.IReports.ReportSubTitle
            Get
                Dim e = New TitleStringEventArgs()
                OnReportSubTitle(e)
                Return e.Title
            End Get
        End Property

#End Region

        ''' <summary>
        ''' Do we allow the parameters to be changed by the user or not?
        ''' </summary>
        Public Property AllowParameterChangesByUser() As Boolean Implements DebtPlus.Interfaces.Reports.IReports.AllowParameterChangesByUser
            Get
                Return m_AllowParameterChangesByUser
            End Get
            Set
                m_AllowParameterChangesByUser = Value
            End Set
        End Property
        Private m_AllowParameterChangesByUser As Boolean

        ''' <summary>
        ''' Routine to externally set a report parameter by name
        ''' </summary>
        ''' <param name="parameterName">Name of the parameter to be modified</param>
        ''' <param name="value">Value to be used for the parameter</param>
        ''' <remarks></remarks>
        Public Overridable Sub SetReportParameter(parameterName As String, value As Object) Implements DebtPlus.Interfaces.Reports.IReports.SetReportParameter
            ' Do nothing
        End Sub

        ''' <summary>
        ''' Request the parameters for the report, should always be called by report requesting code
        ''' </summary>
        Public Overridable Function RequestReportParameters() As DialogResult Implements DebtPlus.Interfaces.Reports.IReports.RequestReportParameters
            Return DialogResult.OK
        End Function

        ''' <summary>
        ''' Return the dictionary of the current parameter arguments.
        ''' </summary>
        ''' <returns></returns>
        Public Overridable Function ParametersDictionary() As Dictionary(Of String, String) Implements DebtPlus.Interfaces.Reports.IReports.ParametersDictionary
            Return Parameters.AsQueryable().OfType(Of DevExpress.XtraReports.Parameters.Parameter)().ToDictionary(Function(p) p.Name, Function(p) p.Value.ToString())
        End Function

        ''' <summary>
        ''' Thread procedure to run the report.
        ''' </summary>
        Public Sub RunReport() Implements DebtPlus.Interfaces.Reports.IReports.RunReport
            InitializeReport()

            ' Ask for the parameters and if OK, show the preview dialog.
            If RequestReportParameters() = DialogResult.OK Then
                DisplayPreviewDialog()
            End If
        End Sub

        ''' <summary>
        ''' Start a separate thread to run the report.
        ''' </summary>
        Public Sub RunReportInSeparateThread() Implements DebtPlus.Interfaces.Reports.IReports.RunReportInSeparateThread
            Dim thrd = New Thread(AddressOf RunReport)
            thrd.SetApartmentState(ApartmentState.STA)
            thrd.IsBackground = True
            thrd.Name = Guid.NewGuid().ToString()
            thrd.Start()
        End Sub

        ''' <summary>
        ''' Display the PageSetup dialog.
        ''' </summary>
        Public Function DisplayPageSetupDialog() As System.Windows.Forms.DialogResult Implements DebtPlus.Interfaces.Reports.IReports.DisplayPageSetupDialog
            Return Me.ShowPageSetupDialog()
        End Function

        ''' <summary>
        ''' Show the preview of the report
        ''' </summary>
        Public Overridable Sub DisplayPreviewDialog() Implements DebtPlus.Interfaces.Reports.IReports.DisplayPreviewDialog
            Using frm = New PrintPreviewForm(Me)
                AddHandler frm.CustomizeReport, AddressOf CustomizeReportEvent
                frm.ShowDialog()
                RemoveHandler frm.CustomizeReport, AddressOf CustomizeReportEvent
            End Using
        End Sub

        ''' <summary>
        ''' Show the preview of the report
        ''' </summary>
        Public Overridable Sub DisplayPreviewDialog(lookAndFeel As DevExpress.LookAndFeel.UserLookAndFeel)
            Using frm = New PrintPreviewForm(Me)
                frm.LookAndFeel.Assign(lookAndFeel)
                AddHandler frm.CustomizeReport, AddressOf CustomizeReportEvent
                frm.ShowDialog()
                RemoveHandler frm.CustomizeReport, AddressOf CustomizeReportEvent
            End Using
        End Sub

        ''' <summary>
        ''' Print the report document.
        ''' </summary>
        Public Sub PrintDocument() Implements DebtPlus.Interfaces.Reports.IReports.PrintDocument
            Me.Print()
        End Sub

        ''' <summary>
        ''' Print the report document.
        ''' </summary>
        Public Sub PrintDocument(printerName As String) Implements DebtPlus.Interfaces.Reports.IReports.PrintDocument
            Me.Print(printerName)
        End Sub

        ''' <summary>
        ''' Create an RTF output file to a disk file
        ''' </summary>
        Public Sub CreateRtfDocument(Path As String) Implements DebtPlus.Interfaces.Reports.IReports.CreateRtfDocument
            Me.ExportToRtf(Path)
        End Sub

        ''' <summary>
        ''' Create an RTF output file to a stream
        ''' </summary>
        Public Sub CreateRtfDocument(Stream As Stream) Implements DebtPlus.Interfaces.Reports.IReports.CreateRtfDocument
            Me.ExportToRtf(Stream)
        End Sub

        ''' <summary>
        ''' Create an PDF output file to a disk file
        ''' </summary>
        Public Sub CreatePdfDocument(Path As String) Implements DebtPlus.Interfaces.Reports.IReports.CreatePdfDocument
            Me.ExportOptions.Pdf.NeverEmbeddedFonts = DebtPlus.Data.Fonts.GetExcludeList()
            Me.ExportToPdf(Path)
        End Sub

        ''' <summary>
        ''' Create an PDF output file to a stream
        ''' </summary>
        Public Sub CreatePdfDocument(Stream As Stream) Implements DebtPlus.Interfaces.Reports.IReports.CreatePdfDocument
            Me.ExportOptions.Pdf.NeverEmbeddedFonts = DebtPlus.Data.Fonts.GetExcludeList()
            Me.ExportToPdf(Stream)
        End Sub

#End Region

#Region "Dispose function"

        ''' <summary>
        ''' Dispose of the current report object
        ''' </summary>
        Protected Overrides Sub OnDisposing()

            Try
                ' Clean up the report filter item storage
                If ReportFilter IsNot Nothing Then
                    ReportFilter.Dispose()
                    ReportFilter = Nothing
                End If

            Finally
                ' Do the standard cleanup on the report at this point.
                MyBase.OnDisposing()
            End Try
        End Sub
#End Region

#Region "ReportFilter"

        Public Overridable Property ReportFilter() As DebtPlus.Interfaces.Reports.ReportFilterItems Implements DebtPlus.Interfaces.Reports.IReportFilter.ReportFilter

#End Region
    End Class
End Namespace