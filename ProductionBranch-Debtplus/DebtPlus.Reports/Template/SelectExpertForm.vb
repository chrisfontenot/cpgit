#Region "Copyright 2000-2012 DebtPlus, L.L.C."

'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}

#End Region
Imports DevExpress.XtraEditors.Controls
Imports DebtPlus.Data.Forms
Imports DevExpress.Data.Filtering
Imports DevExpress.XtraEditors.Filtering
Imports DevExpress.XtraEditors.Repository
Imports DevExpress.XtraReports.UI
Imports DevExpress.XtraEditors
Imports DevExpress.Utils
Imports DebtPlus.Interfaces.Reports

Namespace Template
    Public Class SelectExpertForm
        Inherits DebtPlusForm

        ''' <summary>
        ''' Return the critera for the filter. This is the original parse tree from the filter control.
        ''' </summary>
        Public ReadOnly Property CriteriaFilter As CriteriaOperator
            Get
                Return FilterControl1.FilterCriteria
            End Get
        End Property

        Private ReadOnly ParentReport As IReports

        ''' <summary>
        ''' Initialize the blank form
        ''' </summary>
        ''' <remarks>This will use the default database access methods</remarks>
        Public Sub New()
            MyBase.New()
            InitializeComponent()
            AddHandler Load, AddressOf Form_Load
        End Sub

        Public Sub New(ByVal ParentReport As IReports)
            MyClass.New()
            Me.ParentReport = ParentReport
        End Sub

        Public Sub New(ByVal parentReport As IReports, ByVal filterCriteria As CriteriaOperator)
            MyClass.New(parentReport)
            FilterControl1.FilterCriteria = filterCriteria
        End Sub

        ''' <summary>
        ''' Perform the LOAD event processing on the form.
        ''' </summary>
        Private Sub Form_Load(sender As Object, e As EventArgs)

            ' Obtain a pointer to the dataset if needed
            Dim dataSource As Object = TryCast(ParentReport, XtraReportBase).DataSource
            Dim dataMember As Object = TryCast(ParentReport, XtraReportBase).DataMember

            If dataSource IsNot Nothing Then
                If TypeOf dataSource Is DataSet Then
                    ConfigureDataSource(CType(dataSource, DataSet).Tables(dataMember))
                ElseIf TypeOf dataSource Is DataView Then
                    ConfigureDataSource(CType(dataSource, DataView).Table)
                ElseIf TypeOf dataSource Is DataTable Then
                    ConfigureDataSource(dataSource)
                ElseIf TypeOf dataSource Is IList Then
                    Throw New NotImplementedException("Filtering IList items is not supported")
                    'ConfigureDataSource(dataSource)
                End If
            End If
        End Sub

        Dim ItemCount As Int32

        Private Sub ConfigureDataSource(ByVal tbl As DataTable)

            ' Set the pointer to the editing source
            With FilterControl1
                .SortFilterColumns = True
                .UseMenuForOperandsAndOperators = True
                .SourceControl = tbl
                AddHandler .BeforeShowValueEditor, AddressOf FilterControl1_BeforeShowValueEditor

                ' Set the number of items in the status information
                ItemCount = tbl.Rows.Count
                BarStaticItem_ItemCount.Caption = String.Format("{0:n0} item(s) in the database", ItemCount)
            End With
        End Sub

        ''' <summary>
        ''' Update the filter control with the possible values for the item
        ''' </summary>
        Private Sub FilterControl1_BeforeShowValueEditor(sender As Object, e As ShowValueEditorEventArgs)
            ProcessDataTable(sender, e)
        End Sub

        Private Sub ProcessDataTable(ByVal Sender As Object, ByVal e As ShowValueEditorEventArgs)
            Dim tbl As DataTable = TryCast(CType(Sender, FilterControl).SourceControl, DataTable)
            If tbl IsNot Nothing Then
                Dim FieldName As String = e.CurrentNode.FirstOperand.PropertyName
                Dim col As Int32 = tbl.Columns.IndexOf(FieldName)
                If col >= 0 Then
                    Dim PropertyType As Type = tbl.Columns(col).DataType

                    ' Determine the type of control based upon the nature of the field
                    If PropertyType Is GetType(Decimal) Then
                        e.CustomRepositoryItem = GetCalcEdit()

                    ElseIf PropertyType Is GetType(DateTime) OrElse PropertyType Is GetType(Date) Then
                        e.CustomRepositoryItem = GetDateEdit()

                        ' For bit fields, just use the single checkbox that is standard.
                    ElseIf PropertyType Is GetType(Boolean) Then
                        e.CustomRepositoryItem = Nothing

                        ' The default is to make a combobox item. But, do not do this for LARGE datasets! Just leave it as a text box.
                    ElseIf ItemCount < 10000 Then
                        e.CustomRepositoryItem = GetTableCombobox(tbl, col)
                    End If
                End If
            End If
        End Sub

        ''' <summary>
        ''' Return a combobox with the items that we are searching in the text string
        ''' </summary>
        Private Shared Function GetTableCombobox(ByRef Tbl As DataTable, ByVal Col As Int32) As RepositoryItem
            Dim item As New RepositoryItemComboBox

            ' Create a combobox item list. We want to show the strings but allow the edit operation as well.
            With item

                ' Do a little amount of configuration for the standard fields.
                .NullValuePrompt = String.Empty
                .ImmediatePopup = True
                .TextEditStyle = TextEditStyles.Standard
                .AutoComplete = False
                .Sorted = True

                ' Process each row in the source dataset. This can take a while on a big table.
                For Each row As DataRow In Tbl.Rows

                    ' Find the current column value for the current row.
                    Dim value As Object = row.Item(Col)
                    If value IsNot Nothing AndAlso value IsNot DBNull.Value Then

                        ' Convert the item to a string so that we can put it in our list
                        Dim stringValue As String
                        If TypeOf value Is Decimal Then
                            stringValue = Convert.ToDecimal(value).ToString("0.00")
                            ' I don't like the default of 4 decimal places!!
                        Else
                            stringValue = Convert.ToString(value)
                        End If

                        ' If there is a string then add it
                        If stringValue <> String.Empty Then
                            ' Look to see if the item is already in the list. Don't add duplicates.
                            Dim isDuplicated As Boolean = False
                            For Each listItem As String In .Items
                                If String.Compare(listItem, stringValue, True) = 0 Then
                                    isDuplicated = True
                                    Exit For
                                End If
                            Next

                            ' If the item is not duplicated then add it
                            If Not isDuplicated Then
                                .Items.Add(stringValue)
                            End If
                        End If
                    End If
                Next
            End With

            Return item
        End Function

        ''' <summary>
        ''' Return a calculator field for the decimal numbers
        ''' </summary>
        Private Shared Function GetCalcEdit() As RepositoryItem
            Dim item As New RepositoryItemCalcEdit

            ' Configure the item
            With item
                With .DisplayFormat
                    .FormatString = "{0:c}"
                    .FormatType = FormatType.Numeric
                End With

                With .EditFormat
                    .FormatString = "{0:f2}"
                    .FormatType = FormatType.Numeric
                End With

                .EditMask = "c"
                .Mask.BeepOnError = True
                .NullText = String.Empty
                .Precision = 2
            End With

            Return item
        End Function

        ''' <summary>
        ''' Return a Date field
        ''' </summary>
        Private Shared Function GetDateEdit() As RepositoryItem
            Dim item As New RepositoryItemDateEdit

            ' Configure the item
            With item
                .ShowClear = True
                .ShowToday = True
                .ShowWeekNumbers = False
                .VistaDisplayMode = DefaultBoolean.True

                With .DisplayFormat
                    .FormatString = "{0:d}"
                    .FormatType = FormatType.DateTime
                End With

                With .EditFormat
                    .FormatString = "{0:d}"
                    .FormatType = FormatType.DateTime
                End With

                .EditMask = "d"
                .Mask.BeepOnError = True
                .NullText = String.Empty
            End With

            Return item
        End Function

    End Class
End Namespace
