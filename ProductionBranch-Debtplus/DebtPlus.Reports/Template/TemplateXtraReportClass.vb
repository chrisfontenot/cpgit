#Region "Copyright 2000-2012 DebtPlus, L.L.C."

'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}

#End Region

Option Compare Binary
Option Explicit On
Option Strict On

Imports System.Drawing.Printing
Imports DevExpress.XtraPrinting
Imports DevExpress.XtraReports.UI

Namespace Template
    Public Class TemplateXtraReportClass  ' Do not make this MustInherit !!
        Inherits BaseXtraReportClass

        Public Sub New()
            MyBase.New()
            InitializeComponent()
            RegisterHandlers()
        End Sub

        Private Sub RegisterHandlers()
            AddHandler XrLabel_Title.BeforePrint, AddressOf XrLabel_Title_BeforePrint
            AddHandler PrintingSystem.PageSettingsChanged, AddressOf HandlePageSettingsChanged
            AddHandler PrintingSystem.AfterMarginsChange, AddressOf HandleAfterMarginsChange
            AddHandler XrLabel_Subtitle.BeforePrint, AddressOf XrLabel_Subtitle_BeforePrint
            AddHandler XrPanel_AgencyAddress.BeforePrint, AddressOf XrPanel_AgencyAddress_BeforePrint
        End Sub

        Private Sub UnRegisterHandlers()
            RemoveHandler XrLabel_Title.BeforePrint, AddressOf XrLabel_Title_BeforePrint
            RemoveHandler PrintingSystem.PageSettingsChanged, AddressOf HandlePageSettingsChanged
            RemoveHandler PrintingSystem.AfterMarginsChange, AddressOf HandleAfterMarginsChange
            RemoveHandler XrLabel_Subtitle.BeforePrint, AddressOf XrLabel_Subtitle_BeforePrint
            RemoveHandler XrPanel_AgencyAddress.BeforePrint, AddressOf XrPanel_AgencyAddress_BeforePrint
        End Sub

#Region " Component Designer generated code "

        'Component overrides dispose to clean up the component list.
        Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
            If disposing Then
                If components IsNot Nothing Then
                    components.Dispose()
                End If
            End If
            MyBase.Dispose(disposing)
        End Sub

        'Required by the Component Designer
        Private components As System.ComponentModel.IContainer
        Protected WithEvents PageHeader As DevExpress.XtraReports.UI.PageHeaderBand
        Protected WithEvents PageFooter As DevExpress.XtraReports.UI.PageFooterBand

        'NOTE: The following procedure is required by the Component Designer
        'It can be modified using the Component Designer.
        'Do not modify it using the code editor.
        Protected WithEvents XrLabel_Title As DevExpress.XtraReports.UI.XRLabel
        Protected WithEvents XrLabel_Subtitle As DevExpress.XtraReports.UI.XRLabel
        Protected WithEvents XrPageInfo_PageNumber As DevExpress.XtraReports.UI.XRPageInfo
        Protected WithEvents XrPageInfo1 As DevExpress.XtraReports.UI.XRPageInfo
        Protected WithEvents XrPanel_AgencyAddress As DevExpress.XtraReports.UI.XRPanel
        Protected WithEvents XRLabel_Agency_Name As DevExpress.XtraReports.UI.XRLabel
        Protected WithEvents XrLine_Title As DevExpress.XtraReports.UI.XRLine
        Protected WithEvents XrLabel_Agency_Address3 As DevExpress.XtraReports.UI.XRLabel
        Protected WithEvents XrLabel_Agency_Address1 As DevExpress.XtraReports.UI.XRLabel
        Protected WithEvents XrLabel_Agency_Phone As DevExpress.XtraReports.UI.XRLabel
        Protected WithEvents XrControlStyle_Header As DevExpress.XtraReports.UI.XRControlStyle
        Protected WithEvents XrControlStyle_Totals As DevExpress.XtraReports.UI.XRControlStyle
        Protected WithEvents XrControlStyle_HeaderPannel As DevExpress.XtraReports.UI.XRControlStyle
        Protected WithEvents XrControlStyle_GroupHeader As DevExpress.XtraReports.UI.XRControlStyle
        Protected WithEvents XrLabel_Agency_Address2 As DevExpress.XtraReports.UI.XRLabel

        Private Sub InitializeComponent()
            Me.PageHeader = New DevExpress.XtraReports.UI.PageHeaderBand
            Me.XrPageInfo1 = New DevExpress.XtraReports.UI.XRPageInfo
            Me.XrLabel_Subtitle = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLine_Title = New DevExpress.XtraReports.UI.XRLine
            Me.XrLabel_Title = New DevExpress.XtraReports.UI.XRLabel
            Me.XrPanel_AgencyAddress = New DevExpress.XtraReports.UI.XRPanel
            Me.XrLabel_Agency_Address3 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_Agency_Address1 = New DevExpress.XtraReports.UI.XRLabel
            Me.XRLabel_Agency_Name = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_Agency_Phone = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_Agency_Address2 = New DevExpress.XtraReports.UI.XRLabel
            Me.PageFooter = New DevExpress.XtraReports.UI.PageFooterBand
            Me.XrPageInfo_PageNumber = New DevExpress.XtraReports.UI.XRPageInfo
            Me.XrControlStyle_Header = New DevExpress.XtraReports.UI.XRControlStyle
            Me.XrControlStyle_Totals = New DevExpress.XtraReports.UI.XRControlStyle
            Me.XrControlStyle_HeaderPannel = New DevExpress.XtraReports.UI.XRControlStyle
            Me.XrControlStyle_GroupHeader = New DevExpress.XtraReports.UI.XRControlStyle
            CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
            '
            'Detail
            '
            Me.Detail.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
            Me.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
            '
            'PageHeader
            '
            Me.PageHeader.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrPageInfo1, Me.XrLabel_Subtitle, Me.XrLine_Title, Me.XrLabel_Title, Me.XrPanel_AgencyAddress})
            Me.PageHeader.HeightF = 108.0!
            Me.PageHeader.Name = "PageHeader"
            Me.PageHeader.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
            Me.PageHeader.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
            '
            'XrPageInfo1
            '
            Me.XrPageInfo1.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrPageInfo1.ForeColor = System.Drawing.Color.Teal
            Me.XrPageInfo1.Format = "{0:MMMM dd, yyyy          h:mm tt}"
            Me.XrPageInfo1.LocationFloat = New DevExpress.Utils.PointFloat(8.0!, 67.0!)
            Me.XrPageInfo1.Name = "XrPageInfo1"
            Me.XrPageInfo1.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrPageInfo1.PageInfo = DevExpress.XtraPrinting.PageInfo.DateTime
            Me.XrPageInfo1.SizeF = New System.Drawing.SizeF(217.0!, 17.0!)
            Me.XrPageInfo1.StyleName = "XrControlStyle_Header"
            Me.XrPageInfo1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
            '
            'XrLabel_Subtitle
            '
            Me.XrLabel_Subtitle.CanShrink = True
            Me.XrLabel_Subtitle.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel_Subtitle.ForeColor = System.Drawing.Color.Teal
            Me.XrLabel_Subtitle.LocationFloat = New DevExpress.Utils.PointFloat(8.0!, 85.00001!)
            Me.XrLabel_Subtitle.Multiline = True
            Me.XrLabel_Subtitle.Name = "XrLabel_Subtitle"
            Me.XrLabel_Subtitle.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_Subtitle.SizeF = New System.Drawing.SizeF(734.0!, 17.0!)
            Me.XrLabel_Subtitle.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
            '
            'XrLine_Title
            '
            Me.XrLine_Title.ForeColor = System.Drawing.Color.Teal
            Me.XrLine_Title.LineWidth = 2
            Me.XrLine_Title.LocationFloat = New DevExpress.Utils.PointFloat(8.0!, 50.0!)
            Me.XrLine_Title.Name = "XrLine_Title"
            Me.XrLine_Title.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
            Me.XrLine_Title.SizeF = New System.Drawing.SizeF(425.0!, 8.0!)
            '
            'XrLabel_Title
            '
            Me.XrLabel_Title.CanShrink = True
            Me.XrLabel_Title.Font = New System.Drawing.Font("Comic Sans MS", 20.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.XrLabel_Title.ForeColor = System.Drawing.Color.Teal
            Me.XrLabel_Title.LocationFloat = New DevExpress.Utils.PointFloat(8.0!, 8.0!)
            Me.XrLabel_Title.Name = "XrLabel_Title"
            Me.XrLabel_Title.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_Title.SizeF = New System.Drawing.SizeF(425.0!, 42.0!)
            Me.XrLabel_Title.StyleName = "XrControlStyle_Header"
            Me.XrLabel_Title.StylePriority.UseFont = False
            Me.XrLabel_Title.Text = "Template Report Title"
            Me.XrLabel_Title.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
            '
            'XrPanel_AgencyAddress
            '
            Me.XrPanel_AgencyAddress.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel_Agency_Address3, Me.XrLabel_Agency_Address1, Me.XRLabel_Agency_Name, Me.XrLabel_Agency_Phone, Me.XrLabel_Agency_Address2})
            Me.XrPanel_AgencyAddress.LocationFloat = New DevExpress.Utils.PointFloat(442.0!, 0.0!)
            Me.XrPanel_AgencyAddress.Name = "XrPanel_AgencyAddress"
            Me.XrPanel_AgencyAddress.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
            Me.XrPanel_AgencyAddress.SizeF = New System.Drawing.SizeF(308.0!, 85.00001!)
            '
            'XrLabel_Agency_Address3
            '
            Me.XrLabel_Agency_Address3.CanShrink = True
            Me.XrLabel_Agency_Address3.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.XrLabel_Agency_Address3.ForeColor = System.Drawing.Color.Teal
            Me.XrLabel_Agency_Address3.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 51.0!)
            Me.XrLabel_Agency_Address3.Name = "XrLabel_Agency_Address3"
            Me.XrLabel_Agency_Address3.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_Agency_Address3.SizeF = New System.Drawing.SizeF(300.0!, 17.0!)
            Me.XrLabel_Agency_Address3.StyleName = "XrControlStyle_Header"
            Me.XrLabel_Agency_Address3.StylePriority.UseFont = False
            Me.XrLabel_Agency_Address3.StylePriority.UseTextAlignment = False
            Me.XrLabel_Agency_Address3.Text = "Address3"
            Me.XrLabel_Agency_Address3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel_Agency_Address1
            '
            Me.XrLabel_Agency_Address1.CanShrink = True
            Me.XrLabel_Agency_Address1.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.XrLabel_Agency_Address1.ForeColor = System.Drawing.Color.Teal
            Me.XrLabel_Agency_Address1.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 17.0!)
            Me.XrLabel_Agency_Address1.Name = "XrLabel_Agency_Address1"
            Me.XrLabel_Agency_Address1.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_Agency_Address1.SizeF = New System.Drawing.SizeF(300.0!, 17.0!)
            Me.XrLabel_Agency_Address1.StyleName = "XrControlStyle_Header"
            Me.XrLabel_Agency_Address1.StylePriority.UseFont = False
            Me.XrLabel_Agency_Address1.StylePriority.UseTextAlignment = False
            Me.XrLabel_Agency_Address1.Text = "Address1"
            Me.XrLabel_Agency_Address1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XRLabel_Agency_Name
            '
            Me.XRLabel_Agency_Name.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XRLabel_Agency_Name.ForeColor = System.Drawing.Color.Teal
            Me.XRLabel_Agency_Name.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
            Me.XRLabel_Agency_Name.Name = "XRLabel_Agency_Name"
            Me.XRLabel_Agency_Name.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XRLabel_Agency_Name.SizeF = New System.Drawing.SizeF(300.0!, 17.0!)
            Me.XRLabel_Agency_Name.StyleName = "XrControlStyle_Header"
            Me.XRLabel_Agency_Name.StylePriority.UseTextAlignment = False
            Me.XRLabel_Agency_Name.Text = "AgencyName"
            Me.XRLabel_Agency_Name.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel_Agency_Phone
            '
            Me.XrLabel_Agency_Phone.CanShrink = True
            Me.XrLabel_Agency_Phone.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.XrLabel_Agency_Phone.ForeColor = System.Drawing.Color.Teal
            Me.XrLabel_Agency_Phone.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 68.0!)
            Me.XrLabel_Agency_Phone.Name = "XrLabel_Agency_Phone"
            Me.XrLabel_Agency_Phone.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_Agency_Phone.SizeF = New System.Drawing.SizeF(300.0!, 17.0!)
            Me.XrLabel_Agency_Phone.StyleName = "XrControlStyle_Header"
            Me.XrLabel_Agency_Phone.StylePriority.UseFont = False
            Me.XrLabel_Agency_Phone.StylePriority.UseTextAlignment = False
            Me.XrLabel_Agency_Phone.Text = "Phone: (000) 555-1212"
            Me.XrLabel_Agency_Phone.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel_Agency_Address2
            '
            Me.XrLabel_Agency_Address2.CanShrink = True
            Me.XrLabel_Agency_Address2.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.XrLabel_Agency_Address2.ForeColor = System.Drawing.Color.Teal
            Me.XrLabel_Agency_Address2.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 34.0!)
            Me.XrLabel_Agency_Address2.Name = "XrLabel_Agency_Address2"
            Me.XrLabel_Agency_Address2.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_Agency_Address2.SizeF = New System.Drawing.SizeF(300.0!, 17.0!)
            Me.XrLabel_Agency_Address2.StyleName = "XrControlStyle_Header"
            Me.XrLabel_Agency_Address2.StylePriority.UseFont = False
            Me.XrLabel_Agency_Address2.StylePriority.UseTextAlignment = False
            Me.XrLabel_Agency_Address2.Text = "Address2"
            Me.XrLabel_Agency_Address2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'PageFooter
            '
            Me.PageFooter.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrPageInfo_PageNumber})
            Me.PageFooter.HeightF = 30.0!
            Me.PageFooter.Name = "PageFooter"
            Me.PageFooter.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
            Me.PageFooter.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
            '
            'XrPageInfo_PageNumber
            '
            Me.XrPageInfo_PageNumber.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrPageInfo_PageNumber.ForeColor = System.Drawing.Color.Teal
            Me.XrPageInfo_PageNumber.Format = "{0:Page #,#}"
            Me.XrPageInfo_PageNumber.LocationFloat = New DevExpress.Utils.PointFloat(608.0!, 8.0!)
            Me.XrPageInfo_PageNumber.Name = "XrPageInfo_PageNumber"
            Me.XrPageInfo_PageNumber.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrPageInfo_PageNumber.PageInfo = DevExpress.XtraPrinting.PageInfo.Number
            Me.XrPageInfo_PageNumber.SizeF = New System.Drawing.SizeF(133.0!, 17.0!)
            Me.XrPageInfo_PageNumber.StyleName = "XrControlStyle_Header"
            Me.XrPageInfo_PageNumber.StylePriority.UseTextAlignment = False
            Me.XrPageInfo_PageNumber.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrControlStyle_Header
            '
            Me.XrControlStyle_Header.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.XrControlStyle_Header.ForeColor = System.Drawing.Color.Teal
            Me.XrControlStyle_Header.Name = "XrControlStyle_Header"
            Me.XrControlStyle_Header.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
            Me.XrControlStyle_Header.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            '
            'XrControlStyle_Totals
            '
            Me.XrControlStyle_Totals.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.XrControlStyle_Totals.Name = "XrControlStyle_Totals"
            Me.XrControlStyle_Totals.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
            Me.XrControlStyle_Totals.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrControlStyle_HeaderPannel
            '
            Me.XrControlStyle_HeaderPannel.BackColor = System.Drawing.Color.Teal
            Me.XrControlStyle_HeaderPannel.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.XrControlStyle_HeaderPannel.ForeColor = System.Drawing.Color.White
            Me.XrControlStyle_HeaderPannel.Name = "XrControlStyle_HeaderPannel"
            Me.XrControlStyle_HeaderPannel.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
            Me.XrControlStyle_HeaderPannel.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrControlStyle_GroupHeader
            '
            Me.XrControlStyle_GroupHeader.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.XrControlStyle_GroupHeader.ForeColor = System.Drawing.Color.Maroon
            Me.XrControlStyle_GroupHeader.Name = "XrControlStyle_GroupHeader"
            Me.XrControlStyle_GroupHeader.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            '
            'TemplateXtraReportClass
            '
            Me.Bands.AddRange(New DevExpress.XtraReports.UI.Band() {Me.PageHeader, Me.PageFooter})
            Me.StyleSheet.AddRange(New DevExpress.XtraReports.UI.XRControlStyle() {Me.XrControlStyle_Header, Me.XrControlStyle_Totals, Me.XrControlStyle_HeaderPannel, Me.XrControlStyle_GroupHeader})
            Me.Version = "9.3"
            CType(Me, System.ComponentModel.ISupportInitialize).EndInit()
        End Sub

#End Region

        Public Overrides Function RequestReportParameters() As DialogResult
            Return DialogResult.OK
        End Function

        Private firstTime As Boolean = True
        Private Sub XrPanel_AgencyAddress_BeforePrint(ByVal sender As Object, ByVal e As PrintEventArgs)

            ' Do this logic once and once only. The agency name/address never changes from one page to another
            If firstTime Then
                firstTime = False

                ' Find the agency information from the database via the procedure xpr_OrganizationAddress
                Using drm As New DebtPlus.LINQ.BusinessContext()
                    Dim query = (From sp In drm.xpr_OrganizationAddress() Select sp).Single()

                    ' Load the items into the controls
                    If query IsNot Nothing Then
                        XRLabel_Agency_Name.Text = query.name.Trim()
                        XrLabel_Agency_Address1.Text = query.addr1.ToString().Trim()
                        XrLabel_Agency_Address2.Text = query.addr2.ToString().Trim()
                        XrLabel_Agency_Address3.Text = query.addr3.ToString().Trim()

                        ' Include the phone number if supplied
                        If Not String.IsNullOrEmpty(query.telephone) Then
                            XrLabel_Agency_Phone.Text = String.Format("Phone: {0}", query.telephone)
                        End If
                    End If
                End Using
            End If
        End Sub

        Protected Overridable Sub XrLabel_Title_BeforePrint(ByVal sender As Object, ByVal e As PrintEventArgs)
            With CType(sender, XRLabel)
                .Text = ReportTitle
            End With
        End Sub

        Protected Overridable Sub XrLabel_Subtitle_BeforePrint(ByVal sender As Object, ByVal e As PrintEventArgs)
            With CType(sender, XRLabel)
                .Text = ReportSubTitle
            End With
        End Sub

        ''' <summary>
        ''' Should the page layout change, recalculate the locations
        ''' </summary>
        Protected Sub HandlePageSettingsChanged(ByVal sender As Object, ByVal e As EventArgs)
            RightAlignFields(sender)
        End Sub

        Protected Sub HandleAfterMarginsChange(ByVal sender As Object, ByVal e As MarginsChangeEventArgs)
            RightAlignFields(sender)
        End Sub

        Protected Sub RightAlignFields(ByVal sender As Object)
            UnRegisterHandlers()
            Try
                Dim ps As PrintingSystemBase = TryCast(sender, PrintingSystemBase)
                Dim newPageWidth As Int32 = ps.PageBounds.Width - ps.PageMargins.Left - ps.PageMargins.Right
                Dim currentPageWidth As Int32 = PageWidth - Margins.Left - Margins.Right
                Dim shift As Int32 = currentPageWidth - newPageWidth

                ' Align the title box to the right edge
                If shift <> 0 Then
                    XrPanel_AgencyAddress.Location = New Point(newPageWidth - XrPanel_AgencyAddress.Width, XrPanel_AgencyAddress.Location.Y)
                    XrPageInfo_PageNumber.Location = New Point(newPageWidth - XrPageInfo_PageNumber.Width, XrPageInfo_PageNumber.Location.Y)

                    ' Adjust the margins
                    Margins.Top = ps.PageMargins.Top
                    Margins.Bottom = ps.PageMargins.Bottom
                    Margins.Left = ps.PageMargins.Left
                    Margins.Right = ps.PageMargins.Right

                    ' Adjust the paper settings
                    PaperKind = ps.PageSettings.PaperKind
                    PaperName = ps.PageSettings.PaperName
                    Landscape = ps.PageSettings.Landscape

                    ' Recreate the document
                    CreateDocument()
                End If

            Finally
                RegisterHandlers()
            End Try
        End Sub
    End Class
End Namespace
