#Region "Copyright 2000-2012 DebtPlus, L.L.C."

'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}

#End Region
Imports System.ComponentModel
Imports System.Xml.Serialization
Imports DebtPlus.Interfaces.Client
Imports DevExpress.XtraReports.UI
Imports System.Globalization

Namespace Template.Packets
    Public Class PacketBaseSubreport
        Implements IClient

        Public Sub New()
            MyBase.New()
            Me.InitializeComponent()
        End Sub

        <XmlIgnore(), Browsable(False)>
        Public Property ClientId() As Int32 Implements IClient.ClientId
            Get
                Dim rpt As XtraReport = MasterReport
                If DesignMode OrElse rpt Is Nothing Then Return -1
                Return Convert.ToInt32(rpt.Parameters("ParameterClient").Value, CultureInfo.InvariantCulture)
            End Get
            Set(ByVal value As Int32)
                ' Do not allow the client to be changed. It is set only in the master report.
            End Set
        End Property
    End Class
End Namespace
