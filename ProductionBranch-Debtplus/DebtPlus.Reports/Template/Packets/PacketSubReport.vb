#Region "Copyright 2000-2012 DebtPlus, L.L.C."

'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}

#End Region

Imports System.ComponentModel
Imports DevExpress.XtraReports.UI
Imports System.IO
Imports System.Reflection
Imports DebtPlus.Utils
Imports DebtPlus.Data.Forms

Namespace Template.Packets
    Public Class PacketSubreport
        Inherits XRSubreport

        Public Sub New()
            MyBase.New()
        End Sub

        Private privateReportDefinition As String = String.Empty

        <Category("Data"), Description("Subreport's REPX source file for the design information"),
            DefaultValue(GetType(String), ""), Browsable(True)>
        Public Property ReportDefinition() As String
            Get
                Return privateReportDefinition
            End Get
            Set(ByVal value As String)
                If value <> privateReportDefinition Then
                    privateReportDefinition = value
                End If
            End Set
        End Property

        Protected Overrides Sub BeforeReportPrint()
            Try

                ' Load the report before we trip the "BeforePrint" events
                If ReportSource Is Nothing AndAlso ReportDefinition <> String.Empty Then

                    ' Try for the item in the reports directory.
                    Dim Fname As String = Path.Combine(DebtPlus.Configuration.Config.ReportsDirectory, ReportDefinition)
                    If Not System.IO.File.Exists(Fname) Then

                        ' If it is not there then try for it in the current code file directory
                        Dim asm As Assembly = Assembly.GetExecutingAssembly()
                        Dim Path As String = System.IO.Path.GetDirectoryName(asm.Location)
                        Fname = System.IO.Path.Combine(Path, ReportDefinition)
                    End If

                    Dim rpt As New PacketBaseSubreport()
                    Try
                        rpt.LoadLayout(Fname)
                        rpt.ScriptReferences = DebtPlus.Utils.AssemblyTools.GetAllSiblingAssemblies()
                        ReportSource = rpt

                    Catch ex As Exception
                        MessageBox.Show(
                            String.Format(
                                "An error occurred loading the required subreport. The subreport information has not been included. Please correct the reference and retry.{1}{1}Error: {2}{1}Filename: {0}",
                                Fname, Environment.NewLine, ex.Message), "Error loading subreport", MessageBoxButtons.OK,
                            MessageBoxIcon.Error)
                    End Try
                End If

            Finally
                ' Do the standard logic for the BeforePrint event
                MyBase.BeforeReportPrint()
            End Try
        End Sub
    End Class
End Namespace
