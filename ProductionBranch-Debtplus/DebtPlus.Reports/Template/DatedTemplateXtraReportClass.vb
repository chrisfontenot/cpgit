#Region "Copyright 2000-2012 DebtPlus, L.L.C."

'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}

#End Region

Option Compare Binary
Option Explicit On
Option Strict On

Imports System.ComponentModel
Imports DevExpress.XtraReports.Parameters

Imports DebtPlus.Reports.Template.Forms

Namespace Template
    Public Class DatedTemplateXtraReportClass
        Inherits TemplateXtraReportClass

        Protected ReportDateRange As DebtPlus.Utils.DateRange = DefaultDateRange
        Protected FromDateDefined As Boolean = False
        Protected ToDateDefined As Boolean = False

        ''' <summary>
        ''' Default to be used for the date range if one is not specified
        ''' </summary>
        Protected Shared ReadOnly Property DefaultDateRange As DebtPlus.Utils.DateRange
            Get
                Return DebtPlus.Utils.DateRange.Today
            End Get
        End Property

        Public Sub New(ByVal dateRange As DebtPlus.Utils.DateRange)
            MyClass.New()
            ReportDateRange = dateRange
        End Sub

        Public Sub New()
            MyBase.New()
            InitializeComponent()

            ' Initialize the date as not having been set
            ReportDateRange = DefaultDateRange
            Parameter_FromDate = DateTime.Now.Date
            Parameter_ToDate = Parameter_FromDate
            FromDateDefined = False
            ToDateDefined = False
        End Sub

        ''' <summary>
        ''' Define the starting date for the date range
        ''' </summary>
        Protected Friend Property Parameter_FromDate() As DateTime
            Get
                Dim parm As DevExpress.XtraReports.Parameters.Parameter = FindParameter("ParameterFromDate")
                If parm Is Nothing OrElse parm.Value Is Nothing Then Return DateTime.MinValue
                Return Convert.ToDateTime(parm.Value)
            End Get
            Set(ByVal Value As DateTime)
                SetParameter("ParameterFromDate", GetType(DateTime), Value, "Starting Date", False)
                FromDateDefined = True
            End Set
        End Property

        ''' <summary>
        ''' Define the ending date for the date range
        ''' </summary>
        Protected Property Parameter_ToDate() As DateTime
            Get
                Dim parm As DevExpress.XtraReports.Parameters.Parameter = FindParameter("ParameterToDate")
                If parm Is Nothing Then Return DateTime.MaxValue
                Return Convert.ToDateTime(parm.Value)
            End Get
            Set(ByVal Value As Date)
                SetParameter("ParameterToDate", GetType(DateTime), Value, "Starting Date", False)
                ToDateDefined = True
            End Set
        End Property

        ''' <summary>
        ''' Are parameters required to be asked?
        ''' </summary>
        Protected Function DatedReport_NeedParameters() As Boolean
            Return MyBase.NeedParameters() OrElse Not ToDateDefined OrElse Not FromDateDefined
        End Function

        Public Overrides Function NeedParameters() As Boolean
            Return DatedReport_NeedParameters()
        End Function

#Region " Component Designer generated code "

        'Component overrides dispose to clean up the component list.
        Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
            If disposing Then
                If components IsNot Nothing Then
                    components.Dispose()
                End If
            End If
            MyBase.Dispose(disposing)
        End Sub

        'Required by the Component Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Component Designer
        'It can be modified using the Component Designer.
        'Do not modify it using the code editor.
        <System.Diagnostics.DebuggerStepThrough()>
        Private Sub InitializeComponent()
            CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
            '
            'XrLabel_Title
            '
            Me.XrLabel_Title.StylePriority.UseFont = False
            '
            'XrPageInfo_PageNumber
            '
            Me.XrPageInfo_PageNumber.StylePriority.UseTextAlignment = False
            '
            'XRLabel_Agency_Name
            '
            Me.XRLabel_Agency_Name.StylePriority.UseTextAlignment = False
            '
            'XrLabel_Agency_Address3
            '
            Me.XrLabel_Agency_Address3.StylePriority.UseFont = False
            Me.XrLabel_Agency_Address3.StylePriority.UseTextAlignment = False
            '
            'XrLabel_Agency_Address1
            '
            Me.XrLabel_Agency_Address1.StylePriority.UseFont = False
            Me.XrLabel_Agency_Address1.StylePriority.UseTextAlignment = False
            '
            'XrLabel_Agency_Phone
            '
            Me.XrLabel_Agency_Phone.StylePriority.UseFont = False
            Me.XrLabel_Agency_Phone.StylePriority.UseTextAlignment = False
            '
            'XrLabel_Agency_Address2
            '
            Me.XrLabel_Agency_Address2.StylePriority.UseFont = False
            Me.XrLabel_Agency_Address2.StylePriority.UseTextAlignment = False
            '
            'DatedTemplateXtraReportClass
            '
            Me.Bands.AddRange(New DevExpress.XtraReports.UI.Band() _
                                 {Me.Detail, Me.PageHeader, Me.PageFooter, Me.TopMarginBand1, Me.BottomMarginBand1})
            Me.RequestParameters = False
            Me.StyleSheet.AddRange(New DevExpress.XtraReports.UI.XRControlStyle() _
                                      {Me.XrControlStyle_Header, Me.XrControlStyle_Totals, Me.XrControlStyle_HeaderPannel,
                                       Me.XrControlStyle_GroupHeader})
            Me.Version = "10.1"
            CType(Me, System.ComponentModel.ISupportInitialize).EndInit()
        End Sub

#End Region

        ''' <summary>
        ''' Define the subtitle for the report
        ''' </summary>
        Protected Function DatedReport_ReportSubtitle() As String

            ' Special logic for the all dates range
            If Parameter_ToDate = New DateTime(2099, 12, 31) AndAlso Parameter_FromDate = New DateTime(1900, 1, 1) Then
                Return "This report covers all dates"
            End If

            If Parameter_FromDate.Date = DateTime.Now.Date AndAlso Parameter_ToDate.Date = DateTime.Now.Date Then
                Return "This report is for today"
            End If

            Return String.Format("This report covers dates from {0:d} through {1:d}", Parameter_FromDate, Parameter_ToDate)
        End Function

        Public Overrides ReadOnly Property ReportSubtitle() As String
            Get
                Return DatedReport_ReportSubtitle()
            End Get
        End Property

        ''' <summary>
        ''' Request the report parameters
        ''' </summary>
        Protected Function DatedReport_RequestParameters() As DialogResult
            Dim answer As DialogResult = DialogResult.OK

            ' If we need parameters then ask for them
            If NeedParameters() Then

                ' Ask for the date range from the user.
                Using frm As New DateReportParametersForm(ReportDateRange)
                    With frm
                        answer = .ShowDialog()
                        Parameter_FromDate = .Parameter_FromDate
                        Parameter_ToDate = .Parameter_ToDate
                    End With
                End Using
            End If

            ' The last answer that we received is our response.
            Return answer
        End Function

        Public Overrides Function RequestReportParameters() As DialogResult
            Return DatedReport_RequestParameters()
        End Function
    End Class
End Namespace
