#Region "Copyright 2000-2012 DebtPlus, L.L.C."

'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}

#End Region

Imports DevExpress.Utils

Namespace Template.Forms
    Public Class CounselorParametersForm

        ' TRUE if "[All Counselors]" is in the list
        Public Property EnableAllCounselors As Boolean = True

        Public Sub New()
            MyClass.New(False)
        End Sub

        Public Sub New(ByVal enableAllCounselors As Boolean)
            InitializeComponent()
            AddHandler Me.Load, AddressOf DateCounselorReportParametersForm_Load
            Me.EnableAllCounselors = enableAllCounselors
        End Sub

        ''' <summary>
        ''' Return the counselor selected in the form
        ''' </summary>
        Public ReadOnly Property Parameter_Counselor() As Object
            Get
                Dim value As Int32 = DebtPlus.Utils.Nulls.v_Int32(XrCounselor_param_02_1.EditValue).GetValueOrDefault(-1)
                If value < 0 AndAlso EnableAllCounselors Then
                    Return System.DBNull.Value
                End If
                Return value
            End Get
        End Property

        ''' <summary>
        ''' Process the form load event
        ''' </summary>
        Private Sub DateCounselorReportParametersForm_Load(ByVal sender As Object, ByVal e As EventArgs)
            CounselorDropDown_Load()
            ButtonOK.Enabled = Not HasErrors()
        End Sub

        ''' <summary>
        ''' Load the list of counselors
        ''' </summary>
        Protected Sub CounselorDropDown_Load()

            ' Set the parameters that define the counselor lookup
            Me.XrCounselor_param_02_1.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[False]
            Me.XrCounselor_param_02_1.Properties.Columns.AddRange(New DevExpress.XtraEditors.Controls.LookUpColumnInfo() {New DevExpress.XtraEditors.Controls.LookUpColumnInfo("Id", "ID", 5, DevExpress.Utils.FormatType.Numeric, "f0", False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Data.ColumnSortOrder.None, DevExpress.Utils.DefaultBoolean.[True]), New DevExpress.XtraEditors.Controls.LookUpColumnInfo("Name", "Name", 50, DevExpress.Utils.FormatType.None, "", True, DevExpress.Utils.HorzAlignment.Near, DevExpress.Data.ColumnSortOrder.Ascending, DevExpress.Utils.DefaultBoolean.[True])})
            Me.XrCounselor_param_02_1.Properties.DisplayMember = "Name"
            Me.XrCounselor_param_02_1.ToolTip = "Choose the counselor to which you wish to limit the report"
            Me.XrCounselor_param_02_1.Properties.ValueMember = "Id"

            ' If we don't want all counselors then just load the standard list
            If Not EnableAllCounselors Then
                XrCounselor_param_02_1.Properties.DataSource = DebtPlus.LINQ.Cache.counselor.getList()
                XrCounselor_param_02_1.EditValue = DebtPlus.LINQ.Cache.counselor.getDefault()

            Else

                ' Populate the current list of counselors. We need to copy it first before we add a new item to the list.
                Dim colCounselors As New System.Collections.Generic.List(Of DebtPlus.LINQ.counselor)()
                colCounselors.AddRange(DebtPlus.LINQ.Cache.counselor.getList())

                ' Create a fake counselor by the name "[All Counselors]" and make that the default item.
                Dim nameRecord As DebtPlus.LINQ.Name = DebtPlus.LINQ.Factory.Manufacture_Name()
                nameRecord.Last = "[All Counselors]"
                nameRecord.Id = -1

                Dim counselorRecord As DebtPlus.LINQ.counselor = DebtPlus.LINQ.Factory.Manufacture_counselor()
                counselorRecord.Name = nameRecord
                counselorRecord.NameID = -1
                counselorRecord.Id = -1
                counselorRecord.Default = True
                colCounselors.Add(counselorRecord)

                XrCounselor_param_02_1.Properties.DataSource = colCounselors
                XrCounselor_param_02_1.EditValue = -1
            End If

            AddHandler XrCounselor_param_02_1.EditValueChanging, AddressOf DebtPlus.Data.Validation.LookUpEdit_ActiveTest
            AddHandler XrCounselor_param_02_1.EditValueChanged, AddressOf FormChanged
        End Sub

        ''' <summary>
        ''' Look for errors in the counselor list
        ''' </summary>
        Protected Overrides Function HasErrors() As Boolean
            Return MyBase.HasErrors() OrElse (XrCounselor_param_02_1.EditValue Is Nothing)
        End Function
    End Class
End Namespace
