﻿
Imports DebtPlus.UI.Client.Widgets.Controls

Namespace Template.Forms
    <Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
    Partial Class ClientParametersForm
        Inherits Forms.ReportParametersForm

        'Form overrides dispose to clean up the component list.
        <System.Diagnostics.DebuggerNonUserCode()> _
        Protected Overrides Sub Dispose(ByVal disposing As Boolean)
            Try
                If disposing AndAlso components IsNot Nothing Then
                    components.Dispose()
                End If
            Finally
                MyBase.Dispose(disposing)
            End Try
        End Sub

        'Required by the Windows Form Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Windows Form Designer
        'It can be modified using the Windows Form Designer.  
        'Do not modify it using the code editor.
        <System.Diagnostics.DebuggerStepThrough()> _
        Private Sub InitializeComponent()
            Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(ClientParametersForm))
            Dim SerializableAppearanceObject1 As DevExpress.Utils.SerializableAppearanceObject = New DevExpress.Utils.SerializableAppearanceObject
            Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl
            Me.XrClient_param_01_1 = New DebtPlus.UI.Client.Widgets.Controls.ClientID
            CType(Me.DxErrorProvider1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.XrClient_param_01_1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.SuspendLayout()
            '
            'ButtonOK
            '
            Me.ButtonOK.TabIndex = 2
            '
            'ButtonCancel
            '
            Me.ButtonCancel.TabIndex = 3
            '
            'LabelControl1
            '
            Me.LabelControl1.Location = New System.Drawing.Point(13, 28)
            Me.LabelControl1.Name = "LabelControl1"
            Me.LabelControl1.Size = New System.Drawing.Size(45, 13)
            Me.LabelControl1.TabIndex = 0
            Me.LabelControl1.Text = "Client ID:"
            '
            'ClientID1
            '
            Me.XrClient_param_01_1.Location = New System.Drawing.Point(74, 25)
            Me.XrClient_param_01_1.Name = "ClientID1"
            Me.XrClient_param_01_1.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[False]
            Me.XrClient_param_01_1.Properties.Appearance.Options.UseTextOptions = True
            Me.XrClient_param_01_1.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.XrClient_param_01_1.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DebtPlus.UI.Common.Controls.SearchButton()})
            Me.XrClient_param_01_1.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
            Me.XrClient_param_01_1.Properties.DisplayFormat.FormatString = "0000000"
            Me.XrClient_param_01_1.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom
            Me.XrClient_param_01_1.Properties.EditFormat.FormatString = "f0"
            Me.XrClient_param_01_1.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.XrClient_param_01_1.Properties.Mask.BeepOnError = True
            Me.XrClient_param_01_1.Properties.Mask.EditMask = "\d*"
            Me.XrClient_param_01_1.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx
            Me.XrClient_param_01_1.Properties.ValidateOnEnterKey = True
            Me.XrClient_param_01_1.Size = New System.Drawing.Size(100, 20)
            Me.XrClient_param_01_1.TabIndex = 1
            '
            'ClientParametersForm
            '
            Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
            Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
            Me.ClientSize = New System.Drawing.Size(336, 135)
            Me.Controls.Add(Me.LabelControl1)
            Me.Controls.Add(Me.XrClient_param_01_1)
            Me.Name = "ClientParametersForm"
            Me.Controls.SetChildIndex(Me.XrClient_param_01_1, 0)
            Me.Controls.SetChildIndex(Me.LabelControl1, 0)
            Me.Controls.SetChildIndex(Me.ButtonOK, 0)
            Me.Controls.SetChildIndex(Me.ButtonCancel, 0)
            CType(Me.DxErrorProvider1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.XrClient_param_01_1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            Me.ResumeLayout(False)
            Me.PerformLayout()

        End Sub
        Protected Friend WithEvents XrClient_param_01_1 As DebtPlus.UI.Client.Widgets.Controls.ClientID
        Protected Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
    End Class
End Namespace
