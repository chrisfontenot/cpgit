#Region "Copyright 2000-2012 DebtPlus, L.L.C."

'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}

#End Region
Imports System.ComponentModel

Namespace Template.Forms
    Public Class ClientParametersForm

        Public Sub New()
            MyBase.New()
            InitializeComponent()
            AddHandler Me.Load, AddressOf RequestParametersForm_Load
            AddHandler XrClient_param_01_1.Validating, AddressOf ClientID1_Validating
        End Sub

        Public Property Parameter_Client() As Int32
            Get
                Return XrClient_param_01_1.EditValue.GetValueOrDefault(-1)
            End Get
            Set(ByVal value As Int32)
                If value < 0 Then
                    XrClient_param_01_1.EditValue = Nothing
                Else
                    XrClient_param_01_1.EditValue = value
                End If
            End Set
        End Property

        Private Sub RequestParametersForm_Load(ByVal sender As Object, ByVal e As EventArgs)
            ButtonOK.Enabled = Not HasErrors()
        End Sub

        Private Sub ClientID1_Validating(ByVal sender As Object, ByVal e As CancelEventArgs)
            ButtonOK.Enabled = Not HasErrors()
        End Sub

        Protected Overrides Function HasErrors() As Boolean
            Return MyBase.HasErrors() OrElse (Parameter_Client < 0)
        End Function
    End Class
End Namespace
