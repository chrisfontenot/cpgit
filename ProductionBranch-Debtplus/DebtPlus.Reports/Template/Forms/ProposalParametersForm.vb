#Region "Copyright 2000-2012 DebtPlus, L.L.C."

'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}

#End Region

Option Compare Binary
Option Explicit On
Option Strict On

Imports DebtPlus.LINQ
Imports System.Linq
Imports DebtPlus.UI.Common
Imports DevExpress.Data
Imports DevExpress.XtraEditors.Controls
Imports DebtPlus.Utils
Imports DevExpress.Utils
Imports System.Data.SqlClient

Namespace Template.Forms
    Public Class ProposalParametersForm
        Inherits DepositParametersForm

        ''' <summary>
        ''' Should closed batches be shown?
        ''' </summary>
        Public Property ShowClosedBatches() As Boolean = True

        Public Sub New()
            MyBase.New()
            InitializeComponent()
        End Sub

        Protected Overrides Function getRecords() As System.Collections.Generic.List(Of DepositParametersForm.Lookup_Item)
            Using bc As New BusinessContext()

                ' If we want closed batches then do not restrict it to only open batches. Take them all.
                If ShowClosedBatches Then
                    Return (From batch In bc.proposal_batch_ids
                             Join bnk In bc.banks On batch.bank Equals bnk.Id
                             Order By batch.date_created Descending
                             Take 50
                             Select New DepositParametersForm.Lookup_Item() With {
                                 .Id = batch.Id,
                                 .bank = bnk.bank_name,
                                 .bank_type = bnk.type,
                                 .note = batch.note,
                                 .date_created = batch.date_created,
                                 .created_by = batch.created_by,
                                 .proposal_type = batch.proposal_type,
                                 .date_transmitted = batch.date_transmitted,
                                 .date_closed = batch.date_closed}).ToList()
                End If

                Return (From batch In bc.proposal_batch_ids
                        Join bnk In bc.banks On batch.bank Equals bnk.Id
                        Where batch.date_transmitted Is Nothing AndAlso batch.date_closed Is Nothing
                        Order By batch.date_created Descending
                        Take 50
                        Select New DepositParametersForm.Lookup_Item() With {
                            .Id = batch.Id,
                            .bank = bnk.bank_name,
                            .bank_type = bnk.type,
                            .note = batch.note,
                            .date_created = batch.date_created,
                            .created_by = batch.created_by,
                            .proposal_type = batch.proposal_type,
                            .date_transmitted = batch.date_transmitted,
                            .date_closed = batch.date_closed}).ToList()
            End Using
        End Function

        Protected Overrides Sub load_Lookup()
            MyBase.load_Lookup()

            ' Reconfigure the lookup for proposals
            Me.XrLookup_param_09_1.ToolTip = "Choose a proposal batch from the list if desired"

            XrLookup_param_09_1.Properties.Columns.Clear()
            Me.XrLookup_param_09_1.Properties.Columns.AddRange(New LookUpColumnInfo() {
                                                               New LookUpColumnInfo("Id", "ID", 5, FormatType.Numeric, "f0", True, HorzAlignment.Far),
                                                               New LookUpColumnInfo("note", "description", 20, FormatType.None, "", True, HorzAlignment.Near, ColumnSortOrder.Ascending),
                                                               New LookUpColumnInfo("date_created", "Date", 8, FormatType.DateTime, "d", True, HorzAlignment.Far, ColumnSortOrder.Descending),
                                                               New LookUpColumnInfo("formatted_bank_type", "Bank", 8, FormatType.None, "", True, HorzAlignment.[Default], ColumnSortOrder.Ascending),
                                                               New LookUpColumnInfo("formatted_proposal_type", "Type", 9, FormatType.None, "", True, HorzAlignment.[Default], ColumnSortOrder.Ascending)})

            ' Sort the item by the date
            XrLookup_param_09_1.Properties.SortColumnIndex = 2
        End Sub
    End Class
End Namespace
