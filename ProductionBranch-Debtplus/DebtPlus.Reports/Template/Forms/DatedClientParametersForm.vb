#Region "Copyright 2000-2012 DebtPlus, L.L.C."

'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}

#End Region
Imports DevExpress.XtraEditors.Controls

Imports DebtPlus.Utils.Format

Namespace Template.Forms
    Public Class DatedClientParametersForm
        Inherits DateReportParametersForm

        Public Sub New()
            MyClass.New(DebtPlus.Utils.DateRange.Today)
        End Sub

        Public Sub New(ByVal defaultItem As DebtPlus.Utils.DateRange)
            MyBase.New(defaultItem)
            InitializeComponent()
            AddHandler Me.Load, AddressOf RequestParametersForm_Load
        End Sub

        Private privateClient As Int32 = -1
        Public Property Parameter_Client() As Int32
            Get
                Return privateClient
            End Get
            Set(ByVal value As Int32)
                privateClient = value
            End Set
        End Property

        Private Sub RequestParametersForm_Load(ByVal sender As Object, ByVal e As EventArgs)

            ' Load the client id into the control
            If Parameter_Client >= 0 Then
                XrClient_param_04_1.Enabled = False
                XrClient_param_04_1.Text = String.Format("{0:0000000}", Parameter_Client)
            Else
                XrClient_param_04_1.EditValue = Nothing
                XrClient_param_04_1.Enabled = True
            End If

            ButtonOK.Enabled = Not HasErrors()

            ' Add the handler to detect changes in the client id
            AddHandler XrClient_param_04_1.EditValueChanging, AddressOf XrClient_param_04_1_EditValueChanging
        End Sub

        Private Sub XrClient_param_04_1_EditValueChanging(ByVal sender As Object, ByVal e As ChangingEventArgs)
            Dim NewClientID As Int32 = -1

            If e.NewValue Is Nothing Then
                Parameter_Client = -1
                ButtonOK.Enabled = False
            ElseIf e.NewValue Is DBNull.Value OrElse Not Int32.TryParse(Convert.ToString(e.NewValue), NewClientID) _
                Then
                Parameter_Client = -1
                ButtonOK.Enabled = False
            Else
                Parameter_Client = NewClientID
                ButtonOK.Enabled = Not HasErrors()
            End If
        End Sub

        Protected Overrides Function HasErrors() As Boolean
            Dim answer As Boolean = MyBase.HasErrors
            If Not answer AndAlso Parameter_Client < 0 Then answer = True
            Return answer
        End Function
    End Class
End Namespace
