#Region "Copyright 2000-2012 DebtPlus, L.L.C."

'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}

#End Region
Imports DevExpress.XtraEditors.Controls

Namespace Template.Forms
    Public Class DatedCreditorParametersForm

        Private privateCreditor As String = String.Empty
        Public Property Parameter_Creditor() As String
            Get
                Return privateCreditor
            End Get
            Set(ByVal value As String)
                privateCreditor = value
            End Set
        End Property

        Public Sub New(ByVal defaultItem As DebtPlus.Utils.DateRange)
            MyBase.New(defaultItem)
            InitializeComponent()
            AddHandler Me.Load, AddressOf RequestParametersForm_Load
        End Sub

        Public Sub New()
            MyBase.New()
            InitializeComponent()
            AddHandler Me.Load, AddressOf RequestParametersForm_Load
        End Sub

        Private Sub RequestParametersForm_Load(ByVal sender As Object, ByVal e As EventArgs)

            ' Define the period for the item
            XrCombo_param_08_1.SelectedIndex = DebtPlus.Utils.DateRange.[Last_Month]

            ' Load the creditor id into the control
            If Parameter_Creditor <> String.Empty Then
                XrCreditor_param_06_1.Enabled = False
                XrCreditor_param_06_1.Text = Parameter_Creditor
            Else
                XrCreditor_param_06_1.EditValue = Nothing
                XrCreditor_param_06_1.Enabled = True
            End If

            ButtonOK.Enabled = Not HasErrors()

            ' Add the handler to detect changes in the creditor id
            AddHandler XrCreditor_param_06_1.EditValueChanging, AddressOf CreditorID1_EditValueChanging
        End Sub

        Private Sub CreditorID1_EditValueChanging(ByVal sender As Object, ByVal e As ChangingEventArgs)

            If e.NewValue Is Nothing Then
                Parameter_Creditor = String.Empty
                ButtonOK.Enabled = False
            ElseIf e.NewValue Is DBNull.Value Then
                Parameter_Creditor = String.Empty
                ButtonOK.Enabled = False
            Else
                Parameter_Creditor = Convert.ToString(e.NewValue)
                ButtonOK.Enabled = Not HasErrors()
            End If
        End Sub

        Protected Overrides Function HasErrors() As Boolean
            Return MyBase.HasErrors OrElse Parameter_Creditor = String.Empty
        End Function
    End Class
End Namespace
