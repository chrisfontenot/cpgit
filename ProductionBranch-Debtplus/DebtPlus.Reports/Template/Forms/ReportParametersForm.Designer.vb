Namespace Template.Forms
    <Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
    Partial Class ReportParametersForm

        'Form overrides dispose to clean up the component list.
        Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
            If disposing Then
                If components IsNot Nothing Then
                    components.Dispose()
                End If
            End If
            MyBase.Dispose(disposing)
        End Sub

        'Required by the Windows Form Designer
        Private components As System.ComponentModel.IContainer

        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
            Me.ButtonOK = New DevExpress.XtraEditors.SimpleButton
            Me.ButtonCancel = New DevExpress.XtraEditors.SimpleButton
            CType(Me.DxErrorProvider1, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.SuspendLayout()

            '
            'ButtonOK
            '
            Me.ButtonOK.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.ButtonOK.Enabled = False
            Me.ButtonOK.Location = New System.Drawing.Point(248, 17)
            Me.ButtonOK.Name = "ButtonOK"
            Me.ButtonOK.Size = New System.Drawing.Size(80, 26)
            Me.ButtonOK.TabIndex = 80
            Me.ButtonOK.Text = "&OK"
            Me.ButtonOK.ToolTip = "Press here to start the report"
            Me.ButtonOK.ToolTipController = Me.ToolTipController1
            '
            'ButtonCancel
            '
            Me.ButtonCancel.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.ButtonCancel.Location = New System.Drawing.Point(248, 52)
            Me.ButtonCancel.Name = "ButtonCancel"
            Me.ButtonCancel.Size = New System.Drawing.Size(80, 26)
            Me.ButtonCancel.TabIndex = 81
            Me.ButtonCancel.Text = "&Cancel"
            Me.ButtonCancel.ToolTip = "Press here to cancel the report and return to the previous screen"
            Me.ButtonCancel.ToolTipController = Me.ToolTipController1
            '
            'ReportParametersForm
            '
            Me.AcceptButton = Me.ButtonOK
            Me.AutoScaleBaseSize = New System.Drawing.Size(5, 14)
            Me.CancelButton = Me.ButtonCancel
            Me.ClientSize = New System.Drawing.Size(336, 135)
            Me.ControlBox = False
            Me.Controls.Add(Me.ButtonCancel)
            Me.Controls.Add(Me.ButtonOK)
            Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
            Me.MaximizeBox = False
            Me.MinimizeBox = False
            Me.Name = "ReportParametersForm"
            Me.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide
            Me.ToolTipController1.SetSuperTip(Me, Nothing)
            Me.Text = "Report Parameters"
            CType(Me.DxErrorProvider1, System.ComponentModel.ISupportInitialize).EndInit()
            Me.TopMost = True

            Me.ResumeLayout(False)
        End Sub
        Protected Friend WithEvents ButtonOK As DevExpress.XtraEditors.SimpleButton
        Protected Friend WithEvents ButtonCancel As DevExpress.XtraEditors.SimpleButton
    End Class
End Namespace
