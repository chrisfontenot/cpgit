#Region "Copyright 2000-2012 DebtPlus, L.L.C."

'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}

#End Region
Imports DebtPlus.UI.Common
Imports DebtPlus.LINQ
Imports System.Linq
Imports DevExpress.Utils
Imports DevExpress.XtraEditors

Namespace Template.Forms
    Public Class DatedOfficeParametersForm
        Inherits DateReportParametersForm

        ' TRUE if "[All Offices]" is in the list
        Protected Property EnableAllOffices As Boolean = True

        Public Sub New()
            MyClass.New(DebtPlus.Utils.DateRange.Today, True)
        End Sub

        Public Sub New(ByVal defaultItem As DebtPlus.Utils.DateRange)
            MyClass.New(defaultItem, True)
        End Sub

        Public Sub New(ByVal enableAllOffices As Boolean)
            MyClass.New(DebtPlus.Utils.DateRange.Today, enableAllOffices)
        End Sub

        Public Sub New(ByVal defaultItem As DebtPlus.Utils.DateRange, ByVal EnableAllOffices As Boolean)
            MyBase.New(defaultItem)
            InitializeComponent()
            AddHandler Me.Load, AddressOf DateCounselorReportParametersForm_Load
            Me.EnableAllOffices = EnableAllOffices
        End Sub

        ''' <summary>
        ''' Return the office selected in the form
        ''' </summary>
        Public ReadOnly Property Parameter_Office() As Object
            Get
                Dim value As Int32 = DebtPlus.Utils.Nulls.v_Int32(XrOffice_param_07_1.EditValue).GetValueOrDefault(-1)
                If value < 0 AndAlso EnableAllOffices Then
                    Return System.DBNull.Value
                End If
                Return value
            End Get
        End Property

        ''' <summary>
        ''' Process the form load event
        ''' </summary>
        Private Sub DateCounselorReportParametersForm_Load(ByVal sender As Object, ByVal e As EventArgs)
            OfficeDropDown_Load()
            ButtonOK.Enabled = Not HasErrors()
        End Sub

        Protected Sub OfficeDropDown_Load()

            ' Set the parameters that define the office lookup
            Me.XrOffice_param_07_1.Properties.AllowNullInput = DefaultBoolean.False
            Me.XrOffice_param_07_1.Properties.Columns.AddRange(New DevExpress.XtraEditors.Controls.LookUpColumnInfo() {New DevExpress.XtraEditors.Controls.LookUpColumnInfo("Id", "ID", 5, DevExpress.Utils.FormatType.Numeric, "f0", True, DevExpress.Utils.HorzAlignment.[Default]), New DevExpress.XtraEditors.Controls.LookUpColumnInfo("name", "Name", 20, DevExpress.Utils.FormatType.None, "", True, DevExpress.Utils.HorzAlignment.[Default], DevExpress.Data.ColumnSortOrder.Descending), New DevExpress.XtraEditors.Controls.LookUpColumnInfo("formatted_type", 15, "Type")})
            Me.XrOffice_param_07_1.Properties.DisplayMember = "name"
            Me.XrOffice_param_07_1.Properties.ValueMember = "Id"

            ' If we don't want all offices then just load the standard list
            If Not EnableAllOffices Then
                XrOffice_param_07_1.Properties.DataSource = DebtPlus.LINQ.Cache.office.getList()
                XrOffice_param_07_1.EditValue = DebtPlus.LINQ.Cache.office.getDefault()

            Else

                ' Populate the current list of offices. We need to copy it first before we add a new item to the list.
                Dim colOffices As New System.Collections.Generic.List(Of DebtPlus.LINQ.office)()
                colOffices.AddRange(DebtPlus.LINQ.Cache.office.getList())

                ' Create a fake office by the name "[All Offices]" and make that the default item.
                Dim officeRecord As DebtPlus.LINQ.office = DebtPlus.LINQ.Factory.Manufacture_office()
                officeRecord.name = "[All Offices]"
                officeRecord.Id = -1
                officeRecord.Default = True
                colOffices.Add(officeRecord)

                XrOffice_param_07_1.Properties.DataSource = colOffices
                XrOffice_param_07_1.EditValue = -1
            End If

            AddHandler XrOffice_param_07_1.EditValueChanging, AddressOf DebtPlus.Data.Validation.LookUpEdit_ActiveTest
            AddHandler XrOffice_param_07_1.EditValueChanged, AddressOf FormChanged
        End Sub

        ''' <summary>
        ''' Look for errors in the counselor list
        ''' </summary>
        Protected Overrides Function HasErrors() As Boolean
            Return MyBase.HasErrors() OrElse (Not EnableAllOffices AndAlso XrOffice_param_07_1.EditValue Is DBNull.Value)
        End Function
    End Class
End Namespace
