#Region "Copyright 2000-2012 DebtPlus, L.L.C."

'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}

#End Region

Option Compare Binary
Option Explicit On
Option Strict On

Imports DebtPlus.LINQ
Imports System.Linq
Imports DebtPlus.UI.Common
Imports DebtPlus.Data.Forms
Imports DevExpress.XtraEditors

Namespace Template.Forms
    Public Class DepositParametersForm
        Inherits BaseBatchParametersForm

        Public Sub New()
            MyBase.New()
            InitializeComponent()

            If Not InDesignMode Then
                RegisterHandlers()
            End If
        End Sub

        Private Sub RegisterHandlers()
            AddHandler Load, AddressOf RequestParametersForm_Load
        End Sub

        Private Sub UnRegisterHandlers()
            RemoveHandler Load, AddressOf RequestParametersForm_Load
        End Sub

        ''' <summary>
        ''' Deposit batch ID
        ''' </summary>
        Public Property Parameter_BatchID() As Int32
            Get
                Return getSelectedID().GetValueOrDefault(-1)
            End Get
            Set(value As Int32)
                XrLookup_param_09_1.EditValue = value
                XrText_param_09_1.EditValue = value
            End Set
        End Property

#Region " Windows Form Designer generated code "

        'Form overrides dispose to clean up the component list.
        Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
            If disposing Then
                If components IsNot Nothing Then
                    components.Dispose()
                End If
            End If
            MyBase.Dispose(disposing)
        End Sub

        'Required by the Windows Form Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Windows Form Designer
        'It can be modified using the Windows Form Designer.  
        'Do not modify it using the code editor.
        <System.Diagnostics.DebuggerStepThrough()> _
        Private Sub InitializeComponent()
            Me.SuspendLayout()
            Me.Text = "Deposit Batch Selection"
            Me.XrText_param_09_1.ToolTip = "Enter a specific deposit batch if it is not included in the above list"
            Me.XrGroup_param_09_1.Text = " Deposit Batch ID "
            Me.ResumeLayout(False)
        End Sub

#End Region

        Private Sub RequestParametersForm_Load(ByVal sender As Object, ByVal e As EventArgs)
            UnRegisterHandlers()
            Try
                load_Lookup()
                ButtonOK.Enabled = Not HasErrors()
            Finally
                RegisterHandlers()
            End Try
        End Sub

        Protected Class Lookup_Item
            Public Property Id As Int32
            Public Property date_created As DateTime
            Public Property created_by As String
            Public Property note As String
            Public Property bank As String
            Public Property bank_type As String

            ' Used for proposals
            Public Property proposal_type As Int32?
            Public Property date_closed As DateTime?
            Public Property date_transmitted As DateTime?

            Public ReadOnly Property IsClosed As Boolean
                Get
                    Return (date_closed.HasValue OrElse date_transmitted.HasValue)
                End Get
            End Property

            Public ReadOnly Property formatted_proposal_type As String
                Get
                    If proposal_type.HasValue Then
                        Select Case proposal_type.Value
                            Case 1
                                Return "Standard"
                            Case 2
                                Return "FULL"
                            Case 3
                                Return "EFT"
                            Case 4
                                Return "EFT"
                            Case Else
                        End Select
                    End If
                    Return String.Empty
                End Get
            End Property

            Public ReadOnly Property formatted_bank_type As String
                Get
                    If bank_type IsNot Nothing Then
                        Select Case bank_type
                            Case "A"
                                Return "ACH"
                            Case "C"
                                Return "Printed"
                            Case "D"
                                Return "Deposit"
                            Case "R"
                                Return "RPPS"
                            Case Else
                        End Select
                    End If
                    Return String.Empty
                End Get
            End Property

            Public Sub New()
            End Sub
        End Class

        Dim colRecords As System.Collections.Generic.List(Of Lookup_Item)

        Protected Overridable Function getRecords() As System.Collections.Generic.List(Of Lookup_Item)
            Using bc As New BusinessContext()
                Return (From ids In bc.deposit_batch_ids
                        Join bnk In bc.banks On ids.bank Equals bnk.Id
                        Where ids.date_posted Is Nothing AndAlso ids.batch_type = "CL"
                        Order By ids.date_created Descending
                        Take 50
                        Select New Lookup_Item() With _
                               {
                                   .Id = ids.Id,
                                   .date_created = ids.date_created,
                                   .created_by = ids.created_by,
                                   .bank = bnk.bank_name,
                                   .note = ids.note
                               }).ToList()
            End Using
        End Function

        Protected Overridable Sub load_Lookup()

            Me.XrLookup_param_09_1.Properties.Columns.AddRange(New DevExpress.XtraEditors.Controls.LookUpColumnInfo() {New DevExpress.XtraEditors.Controls.LookUpColumnInfo("Id", "ID", 20, DevExpress.Utils.FormatType.Numeric, "f0", True, DevExpress.Utils.HorzAlignment.[Default]),
                                                                                                                       New DevExpress.XtraEditors.Controls.LookUpColumnInfo("date_created", "Date", 20, DevExpress.Utils.FormatType.DateTime, "d", True, DevExpress.Utils.HorzAlignment.[Default], DevExpress.Data.ColumnSortOrder.Ascending),
                                                                                                                       New DevExpress.XtraEditors.Controls.LookUpColumnInfo("created_by", "Creator", 20, DevExpress.Utils.FormatType.None, "", True, DevExpress.Utils.HorzAlignment.[Default], DevExpress.Data.ColumnSortOrder.Ascending),
                                                                                                                       New DevExpress.XtraEditors.Controls.LookUpColumnInfo("bank", "Bank", 20, DevExpress.Utils.FormatType.None, "", True, DevExpress.Utils.HorzAlignment.[Default], DevExpress.Data.ColumnSortOrder.Ascending),
                                                                                                                       New DevExpress.XtraEditors.Controls.LookUpColumnInfo("note", "Note", 20, DevExpress.Utils.FormatType.None, "", True, DevExpress.Utils.HorzAlignment.[Default], DevExpress.Data.ColumnSortOrder.Ascending)})
            Me.XrLookup_param_09_1.Properties.DisplayMember = "date_created"
            Me.XrLookup_param_09_1.Properties.ValueMember = "Id"
            Me.XrLookup_param_09_1.ToolTip = "Choose a deposit batch from the list if desired"

            Try
                Using cm As New DebtPlus.UI.Common.CursorManager()
                    colRecords = getRecords()

                    ' Force a change in the control's value if the value is defined.
                    Dim obj As Object = XrLookup_param_09_1.EditValue
                    XrLookup_param_09_1.EditValue = Nothing
                    XrLookup_param_09_1.EditValue = obj
                End Using

            Catch ex As System.Data.SqlClient.SqlException
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex)
                Return
            End Try

            ' Define the lookup data source
            XrLookup_param_09_1.Properties.DataSource = colRecords
            If colRecords IsNot Nothing AndAlso colRecords.Count > 0 Then
                Parameter_BatchID = colRecords(0).Id
            End If
        End Sub
    End Class
End Namespace
