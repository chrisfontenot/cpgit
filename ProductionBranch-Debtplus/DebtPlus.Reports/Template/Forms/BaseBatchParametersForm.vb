#Region "Copyright 2000-2012 DebtPlus, L.L.C."

'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}

#End Region

Option Compare Binary
Option Explicit On
Option Strict On

Imports DebtPlus.LINQ
Imports System.Linq
Imports DebtPlus.UI.Common
Imports DebtPlus.Data.Forms
Imports DevExpress.XtraEditors

Namespace Template.Forms
    Public Class BaseBatchParametersForm
        Inherits ReportParametersForm

        Public Sub New()
            MyBase.New()
            InitializeComponent()

            ' Allow for the form to be designed without trapping.
            If Not InDesignMode Then
                RegisterHandlers()
            End If
        End Sub

        Private Sub RegisterHandlers()
            AddHandler Load, AddressOf RequestParametersForm_Load
            AddHandler XrRadio_param_09_1.EditValueChanging, AddressOf XrRadio_param_09_1_EditValueChanging
            AddHandler XrText_param_09_1.EditValueChanged, AddressOf Control_EditValueChanged
            AddHandler XrLookup_param_09_1.EditValueChanged, AddressOf Control_EditValueChanged
        End Sub

        Private Sub UnRegisterHandlers()
            RemoveHandler Load, AddressOf RequestParametersForm_Load
            RemoveHandler XrRadio_param_09_1.EditValueChanging, AddressOf XrRadio_param_09_1_EditValueChanging
            RemoveHandler XrText_param_09_1.EditValueChanged, AddressOf Control_EditValueChanged
            RemoveHandler XrLookup_param_09_1.EditValueChanged, AddressOf Control_EditValueChanged
        End Sub

        ''' <summary>
        ''' Get the selected ID from the input fields
        ''' </summary>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Protected Function getSelectedID() As System.Nullable(Of Int32)

            ' Use the value from the lookup control.
            If XrRadio_param_09_1.Checked Then
                Return DebtPlus.Utils.Nulls.v_Int32(XrLookup_param_09_1.EditValue)
            End If

            ' Retrieve the value from the text buffer
            Return DebtPlus.Utils.Nulls.v_Int32(XrText_param_09_1.EditValue)
        End Function

        ''' <summary>
        ''' Handle the change in the selection radio button
        ''' </summary>
        Private Sub XrRadio_param_09_1_EditValueChanging(sender As Object, e As DevExpress.XtraEditors.Controls.ChangingEventArgs)
            UnRegisterHandlers()
            Try
                Dim value As System.Nullable(Of System.Int32) = getSelectedID()

                ' Correct the "new" control's value with the old one.
                XrLookup_param_09_1.EditValue = value
                XrText_param_09_1.EditValue = value

                ' Enable or disable the input field accordingly
                XrText_param_09_1.Enabled = Not DirectCast(e.NewValue, Boolean)
                XrLookup_param_09_1.Enabled = Not XrText_param_09_1.Enabled

                ButtonOK.Enabled = Not HasErrors()
            Finally
                RegisterHandlers()
            End Try
        End Sub

#Region " Windows Form Designer generated code "

        'Form overrides dispose to clean up the component list.
        Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
            If disposing Then
                If components IsNot Nothing Then
                    components.Dispose()
                End If
            End If
            MyBase.Dispose(disposing)
        End Sub

        'Required by the Windows Form Designer
        Private components As System.ComponentModel.IContainer
        Protected Friend WithEvents XrGroup_param_09_1 As System.Windows.Forms.GroupBox
        Protected Friend WithEvents XrRadio_param_09_1 As DevExpress.XtraEditors.CheckEdit
        Protected Friend WithEvents XrRadio_param_09_2 As DevExpress.XtraEditors.CheckEdit
        Protected Friend WithEvents XrLbl_param_09_1 As DevExpress.XtraEditors.LabelControl
        Protected Friend WithEvents XrText_param_09_1 As DevExpress.XtraEditors.TextEdit
        Protected Friend WithEvents XrLookup_param_09_1 As DevExpress.XtraEditors.LookUpEdit

        'NOTE: The following procedure is required by the Windows Form Designer
        'It can be modified using the Windows Form Designer.  
        'Do not modify it using the code editor.
        <System.Diagnostics.DebuggerStepThrough()> _
        Private Sub InitializeComponent()
            Me.XrGroup_param_09_1 = New System.Windows.Forms.GroupBox()
            Me.XrLbl_param_09_1 = New DevExpress.XtraEditors.LabelControl()
            Me.XrRadio_param_09_2 = New DevExpress.XtraEditors.CheckEdit()
            Me.XrRadio_param_09_1 = New DevExpress.XtraEditors.CheckEdit()
            Me.XrText_param_09_1 = New DevExpress.XtraEditors.TextEdit()
            Me.XrLookup_param_09_1 = New DevExpress.XtraEditors.LookUpEdit()
            CType(Me.DxErrorProvider1, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.XrGroup_param_09_1.SuspendLayout()
            CType(Me.XrRadio_param_09_2.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.XrRadio_param_09_1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.XrText_param_09_1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.XrLookup_param_09_1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.SuspendLayout()
            '
            'ButtonOK
            '
            Me.ButtonOK.Location = New System.Drawing.Point(248, 18)
            Me.ButtonOK.Size = New System.Drawing.Size(80, 28)
            Me.ButtonOK.TabIndex = 3
            '
            'ButtonCancel
            '
            Me.ButtonCancel.Location = New System.Drawing.Point(248, 56)
            Me.ButtonCancel.Size = New System.Drawing.Size(80, 28)
            Me.ButtonCancel.TabIndex = 4
            '
            'DefaultLookAndFeel1
            '
            Me.DefaultLookAndFeel1.LookAndFeel.SkinName = "McSkin"
            '
            'XrGroup_param_09_1
            '
            Me.XrGroup_param_09_1.Controls.Add(Me.XrLbl_param_09_1)
            Me.XrGroup_param_09_1.Controls.Add(Me.XrRadio_param_09_2)
            Me.XrGroup_param_09_1.Controls.Add(Me.XrRadio_param_09_1)
            Me.XrGroup_param_09_1.Controls.Add(Me.XrText_param_09_1)
            Me.XrGroup_param_09_1.Controls.Add(Me.XrLookup_param_09_1)
            Me.XrGroup_param_09_1.Location = New System.Drawing.Point(8, 9)
            Me.XrGroup_param_09_1.Name = "XrGroup_param_09_1"
            Me.XrGroup_param_09_1.Size = New System.Drawing.Size(232, 112)
            Me.XrGroup_param_09_1.TabIndex = 0
            Me.XrGroup_param_09_1.TabStop = False
            Me.XrGroup_param_09_1.Text = " Batch ID  "
            '
            'XrLbl_param_09_1
            '
            Me.XrLbl_param_09_1.Appearance.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.XrLbl_param_09_1.Location = New System.Drawing.Point(32, 54)
            Me.XrLbl_param_09_1.Name = "XrLbl_param_09_1"
            Me.XrLbl_param_09_1.Size = New System.Drawing.Size(122, 13)
            Me.XrLbl_param_09_1.TabIndex = 2
            Me.XrLbl_param_09_1.Text = "Or enter a specific ID"
            '
            'XrRadio_param_09_2
            '
            Me.XrRadio_param_09_2.AllowDrop = True
            Me.XrRadio_param_09_2.Location = New System.Drawing.Point(8, 78)
            Me.XrRadio_param_09_2.Name = "XrRadio_param_09_2"
            Me.XrRadio_param_09_2.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
            Me.XrRadio_param_09_2.Properties.RadioGroupIndex = 0
            Me.XrRadio_param_09_2.Size = New System.Drawing.Size(20, 21)
            Me.XrRadio_param_09_2.TabIndex = 3
            Me.XrRadio_param_09_2.TabStop = False
            Me.XrRadio_param_09_2.Tag = "text"
            '
            'XrRadio_param_09_1
            '
            Me.XrRadio_param_09_1.EditValue = True
            Me.XrRadio_param_09_1.Location = New System.Drawing.Point(8, 28)
            Me.XrRadio_param_09_1.Name = "XrRadio_param_09_1"
            Me.XrRadio_param_09_1.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
            Me.XrRadio_param_09_1.Properties.RadioGroupIndex = 0
            Me.XrRadio_param_09_1.Size = New System.Drawing.Size(20, 21)
            Me.XrRadio_param_09_1.TabIndex = 0
            Me.XrRadio_param_09_1.Tag = "list"
            '
            'XrText_param_09_1
            '
            Me.XrText_param_09_1.Enabled = False
            Me.XrText_param_09_1.Location = New System.Drawing.Point(32, 78)
            Me.XrText_param_09_1.Name = "XrText_param_09_1"
            Me.XrText_param_09_1.Properties.Appearance.Options.UseTextOptions = True
            Me.XrText_param_09_1.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.XrText_param_09_1.Properties.AppearanceDisabled.Options.UseTextOptions = True
            Me.XrText_param_09_1.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.XrText_param_09_1.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
            Me.XrText_param_09_1.Properties.DisplayFormat.FormatString = "f0"
            Me.XrText_param_09_1.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.XrText_param_09_1.Properties.EditFormat.FormatString = "f0"
            Me.XrText_param_09_1.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.XrText_param_09_1.Properties.Mask.BeepOnError = True
            Me.XrText_param_09_1.Properties.Mask.EditMask = "\d*"
            Me.XrText_param_09_1.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx
            Me.XrText_param_09_1.Properties.ValidateOnEnterKey = True
            Me.XrText_param_09_1.Size = New System.Drawing.Size(64, 20)
            Me.XrText_param_09_1.TabIndex = 4
            Me.XrText_param_09_1.ToolTip = "Enter a specific batch if it is not included in the above list"
            Me.XrText_param_09_1.ToolTipController = Me.ToolTipController1
            '
            'XrLookup_param_09_1
            '
            Me.XrLookup_param_09_1.Location = New System.Drawing.Point(32, 26)
            Me.XrLookup_param_09_1.Name = "XrLookup_param_09_1"
            Me.XrLookup_param_09_1.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.XrLookup_param_09_1.Properties.NullText = ""
            Me.XrLookup_param_09_1.Properties.PopupWidth = 500
            Me.XrLookup_param_09_1.Properties.ShowFooter = False
            Me.XrLookup_param_09_1.Properties.SortColumnIndex = 1
            Me.XrLookup_param_09_1.Size = New System.Drawing.Size(184, 20)
            Me.XrLookup_param_09_1.TabIndex = 1
            Me.XrLookup_param_09_1.ToolTipController = Me.ToolTipController1
            '
            'BaseBatchParametersForm
            '
            Me.ClientSize = New System.Drawing.Size(336, 133)
            Me.Controls.Add(Me.XrGroup_param_09_1)
            Me.Name = "BaseBatchParametersForm"
            Me.Text = "Batch Selection"
            Me.Controls.SetChildIndex(Me.ButtonOK, 0)
            Me.Controls.SetChildIndex(Me.ButtonCancel, 0)
            Me.Controls.SetChildIndex(Me.XrGroup_param_09_1, 0)
            CType(Me.DxErrorProvider1, System.ComponentModel.ISupportInitialize).EndInit()
            Me.XrGroup_param_09_1.ResumeLayout(False)
            Me.XrGroup_param_09_1.PerformLayout()
            CType(Me.XrRadio_param_09_2.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.XrRadio_param_09_1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.XrText_param_09_1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.XrLookup_param_09_1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            Me.ResumeLayout(False)
        End Sub
#End Region

        ''' <summary>
        ''' Handle the load event for the form
        ''' </summary>
        Private Sub RequestParametersForm_Load(ByVal sender As Object, ByVal e As EventArgs)
            UnRegisterHandlers()
            Try
                ButtonOK.Enabled = Not HasErrors()
            Finally
                RegisterHandlers()
            End Try
        End Sub

        ''' <summary>
        ''' Determine if the input form is valid for a submission
        ''' </summary>
        Protected Overrides Function HasErrors() As Boolean
            Return Not getSelectedID().HasValue
        End Function

        ''' <summary>
        ''' Handle a change in the text box's value
        ''' </summary>
        Private Sub Control_EditValueChanged(sender As Object, e As System.EventArgs)
            ButtonOK.Enabled = Not HasErrors()
        End Sub
    End Class
End Namespace
