#Region "Copyright 2000-2012 DebtPlus, L.L.C."

'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}

#End Region

Option Compare Binary
Option Explicit On
Option Strict On

Imports System.ComponentModel
Imports DebtPlus.Data.Controls

Imports DevExpress.XtraEditors

Namespace Template.Forms
    Public Class DateReportParametersForm
        Inherits ReportParametersForm

        Public Sub New()
            MyClass.New(DebtPlus.Utils.DateRange.Today)
        End Sub

        Public Sub New(ByVal defaultItem As DebtPlus.Utils.DateRange)
            MyBase.New()
            InitializeComponent()
            AddHandler Me.Load, AddressOf DateReportParametersForm_Load
            DefaultSelectedItem = defaultItem
        End Sub

        ' Default item for the new form
        Dim DefaultSelectedItem As DebtPlus.Utils.DateRange = DebtPlus.Utils.DateRange.Today

        Public Overridable ReadOnly Property Parameter_FromDate() As DateTime
            Get
                Return Convert.ToDateTime(XrDate_param_08_1.EditValue)
            End Get
        End Property

        Public Overridable ReadOnly Property Parameter_ToDate() As DateTime
            Get
                Return Convert.ToDateTime(XrDate_param_08_2.EditValue)
            End Get
        End Property

        Private Sub DateReportParametersForm_Load(ByVal sender As Object, ByVal e As EventArgs)

            With XrDate_param_08_1
                .EditValue = Now.Date
                .Properties.ShowClear = False
                .Properties.ValidateOnEnterKey = True
            End With

            With XrDate_param_08_2
                .EditValue = Now.Date
                .Properties.ShowClear = False
                .Properties.ValidateOnEnterKey = True
            End With

            With XrCombo_param_08_1
                With .Properties
                    With .Items
                        .Clear()
                        .Add(New DebtPlus.Data.Controls.ComboboxItem("Today", DebtPlus.Utils.DateRange.Today))
                        .Add(New DebtPlus.Data.Controls.ComboboxItem("Tomorrow", DebtPlus.Utils.DateRange.Tomorrow))
                        .Add(New DebtPlus.Data.Controls.ComboboxItem("Yesterday", DebtPlus.Utils.DateRange.Yesterday))
                        .Add(New DebtPlus.Data.Controls.ComboboxItem("This Week", DebtPlus.Utils.DateRange.This_Week))
                        .Add(New DebtPlus.Data.Controls.ComboboxItem("Last Week", DebtPlus.Utils.DateRange.Last_Week))
                        .Add(New DebtPlus.Data.Controls.ComboboxItem("This Month", DebtPlus.Utils.DateRange.This_Month))
                        .Add(New DebtPlus.Data.Controls.ComboboxItem("Last Month", DebtPlus.Utils.DateRange.[Last_Month]))
                        .Add(New DebtPlus.Data.Controls.ComboboxItem("Last 30 Days", DebtPlus.Utils.DateRange.[Days_30]))
                        .Add(New DebtPlus.Data.Controls.ComboboxItem("Last 60 Days", DebtPlus.Utils.DateRange.[Days_60]))
                        .Add(New DebtPlus.Data.Controls.ComboboxItem("Last 90 Days", DebtPlus.Utils.DateRange.[Days_90]))
                        .Add(New DebtPlus.Data.Controls.ComboboxItem("Quarter to date", DebtPlus.Utils.DateRange.Quarter_To_Date))
                        .Add(New DebtPlus.Data.Controls.ComboboxItem("Last Quarter", DebtPlus.Utils.DateRange.Last_Quarter))
                        .Add(New DebtPlus.Data.Controls.ComboboxItem("Year to date", DebtPlus.Utils.DateRange.Year_To_Date))
                        .Add(New DebtPlus.Data.Controls.ComboboxItem("Last year", DebtPlus.Utils.DateRange.Last_Year))
                        .Add(New DebtPlus.Data.Controls.ComboboxItem("All Transactions", DebtPlus.Utils.DateRange.All_Transactions))
                        .Add(New DebtPlus.Data.Controls.ComboboxItem("Specific Date Range", DebtPlus.Utils.DateRange.Specific_Date_Range))
                    End With
                End With

                ' Find the item selected as the default
                For Each item As ComboboxItem In XrCombo_param_08_1.Properties.Items
                    If CType(item.value, DebtPlus.Utils.DateRange) = DefaultSelectedItem Then
                        XrCombo_param_08_1.SelectedIndex = XrCombo_param_08_1.Properties.Items.IndexOf(item)
                        Exit For
                    End If
                Next
            End With

            ' Correct the date values if we have chosen something in the list by default
            AdjustDateValues(DefaultSelectedItem)

            ' Add the parameters
            AddHandler XrDate_param_08_1.Validating, AddressOf DateEdit_Validating
            AddHandler XrDate_param_08_2.Validating, AddressOf DateEdit_Validating
            AddHandler XrCombo_param_08_1.SelectedIndexChanged, AddressOf ComboBoxEdit_DateRange_EditValueChanged

            ' Enable or disable the OK button
            EnableOK()
        End Sub

        Protected Friend Sub DateEdit_Validating(ByVal sender As Object, ByVal e As CancelEventArgs)
            Dim errorMessage As String = String.Empty

            ' Try to convert the date
            With CType(sender, DateEdit)
                Dim testDate As DateTime
                If Not Date.TryParse(.Text, testDate) Then
                    errorMessage = "Invalid Date. Use a format like MM/DD/YYYY"
                    e.Cancel = True
                End If
            End With

            ' Set the error text
            DxErrorProvider1.SetError(CType(sender, Control), errorMessage)

            ' Enable or disable the OK button
            EnableOK()
        End Sub

        Protected Overrides Function HasErrors() As Boolean

            ' Look for any error condition. Disable the OK button if the form is not complete.
            Do
                If DxErrorProvider1.GetError(XrDate_param_08_2) <> String.Empty Then Exit Do
                If DxErrorProvider1.GetError(XrDate_param_08_1) <> String.Empty Then Exit Do
                If DxErrorProvider1.GetError(XrCombo_param_08_1) <> String.Empty Then Exit Do

                ' There is no shown error.
                Return False
            Loop

            ' There is a pending error. Disable the OK button.
            Return True
        End Function

        Private Sub EnableOK()
            ButtonOK.Enabled = Not HasErrors()
        End Sub

        Protected Friend Overridable Sub ComboBoxEdit_DateRange_EditValueChanged(ByVal sender As Object,
                                                                                 ByVal e As EventArgs)
            AdjustDateValues(CType(CType(XrCombo_param_08_1.SelectedItem, DebtPlus.Data.Controls.ComboboxItem).value, DebtPlus.Utils.DateRange))
        End Sub

        Protected Sub AdjustDateValues(ByVal SelectedItem As DebtPlus.Utils.DateRange)

            ' Enable or disable the date fields
            XrDate_param_08_2.Enabled = False
            XrDate_param_08_1.Enabled = False

            ' Determine the relative dates for the period
            Select Case SelectedItem
                Case DebtPlus.Utils.DateRange.Today
                    XrDate_param_08_1.EditValue = Today
                    XrDate_param_08_2.EditValue = Today

                Case DebtPlus.Utils.DateRange.Yesterday
                    XrDate_param_08_1.EditValue = Today.AddDays(-1)
                    XrDate_param_08_2.EditValue = Today.AddDays(-1)

                Case DebtPlus.Utils.DateRange.Tomorrow
                    XrDate_param_08_1.EditValue = Today.AddDays(1)
                    XrDate_param_08_2.EditValue = Today.AddDays(1)

                Case DebtPlus.Utils.DateRange.This_Week
                    XrDate_param_08_1.EditValue = Today.AddDays(0 - Today.DayOfWeek)
                    XrDate_param_08_2.EditValue = Today

                Case DebtPlus.Utils.DateRange.Last_Week
                    Dim LastWeek As Date = Today.AddDays(-1 - Today.DayOfWeek)
                    XrDate_param_08_2.EditValue() = LastWeek
                    XrDate_param_08_1.EditValue = LastWeek.AddDays(-6)

                Case DebtPlus.Utils.DateRange.This_Month
                    XrDate_param_08_1.EditValue = New Date(Today.Year, Today.Month, 1)
                    XrDate_param_08_2.EditValue = Today

                Case DebtPlus.Utils.DateRange.[Last_Month]
                    Dim ThisMonth As New Date(Today.Year, Today.Month, 1)
                    XrDate_param_08_1.EditValue = ThisMonth.AddMonths(-1)
                    XrDate_param_08_2.EditValue = ThisMonth.AddDays(-1)

                Case DebtPlus.Utils.DateRange.[Days_30]
                    Dim ThisMonth As Date = Today.Date
                    XrDate_param_08_2.EditValue = ThisMonth
                    XrDate_param_08_1.EditValue = ThisMonth.AddMonths(-1)

                Case DebtPlus.Utils.DateRange.[Days_60]
                    Dim ThisMonth As Date = Today.Date
                    XrDate_param_08_2.EditValue = ThisMonth
                    XrDate_param_08_1.EditValue = ThisMonth.AddMonths(-2)

                Case DebtPlus.Utils.DateRange.[Days_90]
                    Dim ThisMonth As Date = Today.Date
                    XrDate_param_08_2.EditValue = ThisMonth
                    XrDate_param_08_1.EditValue = ThisMonth.AddMonths(-3)

                Case DebtPlus.Utils.DateRange.Quarter_To_Date
                    Dim ThisMonth As New Date(Today.Year, Today.Month, 1)
                    Select Case ThisMonth.Month
                        Case 1, 4, 7, 10
                        Case 2, 5, 8, 11
                            ThisMonth = ThisMonth.AddMonths(-1)
                        Case 3, 6, 9, 12
                            ThisMonth = ThisMonth.AddMonths(-2)
                    End Select
                    XrDate_param_08_1.EditValue = ThisMonth
                    XrDate_param_08_2.EditValue = Today

                Case DebtPlus.Utils.DateRange.Last_Quarter
                    Dim ThisMonth As New Date(Today.Year, Today.Month, 1)
                    Select Case ThisMonth.Month
                        Case 1, 4, 7, 10
                        Case 2, 5, 8, 11
                            ThisMonth = ThisMonth.AddMonths(-1)
                        Case 3, 6, 9, 12
                            ThisMonth = ThisMonth.AddMonths(-2)
                    End Select
                    XrDate_param_08_1.EditValue = ThisMonth.AddMonths(-3)
                    XrDate_param_08_2.EditValue = ThisMonth.AddDays(-1)

                Case DebtPlus.Utils.DateRange.Year_To_Date
                    XrDate_param_08_1.EditValue = New Date(Today.Year, 1, 1)
                    XrDate_param_08_2.EditValue = Today.Date

                Case DebtPlus.Utils.DateRange.Last_Year
                    Dim ThisYear As New Date(Today.Year, 1, 1)
                    XrDate_param_08_1.EditValue = ThisYear.AddYears(-1)
                    XrDate_param_08_2.EditValue = ThisYear.AddDays(-1)

                Case DebtPlus.Utils.DateRange.All_Transactions
                    XrDate_param_08_1.EditValue = New DateTime(1900, 1, 1)
                    XrDate_param_08_2.EditValue = New DateTime(2099, 12, 31)

                Case DebtPlus.Utils.DateRange.Specific_Date_Range
                    XrDate_param_08_2.Enabled = True
                    XrDate_param_08_1.Enabled = True
            End Select
        End Sub
    End Class
End Namespace
