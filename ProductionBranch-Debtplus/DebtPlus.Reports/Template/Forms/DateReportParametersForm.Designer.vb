Namespace Template.Forms
    <Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
    Partial Class DateReportParametersForm

        'Form overrides dispose to clean up the component list.
        Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
            If disposing Then
                If components IsNot Nothing Then
                    components.Dispose()
                End If
            End If
            MyBase.Dispose(disposing)
        End Sub

        'Required by the Windows Form Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Windows Form Designer
        'It can be modified using the Windows Form Designer.  
        'Do not modify it using the code editor.
        Protected Friend WithEvents XrGroup_param_08_1 As DevExpress.XtraEditors.GroupControl
        Protected Friend WithEvents XrCombo_param_08_1 As DevExpress.XtraEditors.ComboBoxEdit
        Protected Friend WithEvents XrDate_param_08_2 As DevExpress.XtraEditors.DateEdit
        Protected Friend WithEvents XrDate_param_08_1 As DevExpress.XtraEditors.DateEdit
        Protected Friend WithEvents Label3 As DevExpress.XtraEditors.LabelControl
        Protected Friend WithEvents Label4 As DevExpress.XtraEditors.LabelControl
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
            Me.XrGroup_param_08_1 = New DevExpress.XtraEditors.GroupControl
            Me.XrCombo_param_08_1 = New DevExpress.XtraEditors.ComboBoxEdit
            Me.XrDate_param_08_2 = New DevExpress.XtraEditors.DateEdit
            Me.XrDate_param_08_1 = New DevExpress.XtraEditors.DateEdit
            Me.Label3 = New DevExpress.XtraEditors.LabelControl
            Me.Label4 = New DevExpress.XtraEditors.LabelControl
            CType(Me.DxErrorProvider1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.XrGroup_param_08_1, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.XrGroup_param_08_1.SuspendLayout()
            CType(Me.XrCombo_param_08_1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.XrDate_param_08_2.Properties.VistaTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.XrDate_param_08_2.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.XrDate_param_08_1.Properties.VistaTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.XrDate_param_08_1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.SuspendLayout()
            '
            'GroupBox1
            '
            Me.XrGroup_param_08_1.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowOnly
            Me.XrGroup_param_08_1.Controls.Add(Me.XrCombo_param_08_1)
            Me.XrGroup_param_08_1.Controls.Add(Me.XrDate_param_08_2)
            Me.XrGroup_param_08_1.Controls.Add(Me.XrDate_param_08_1)
            Me.XrGroup_param_08_1.Controls.Add(Me.Label3)
            Me.XrGroup_param_08_1.Controls.Add(Me.Label4)
            Me.XrGroup_param_08_1.Location = New System.Drawing.Point(8, 9)
            Me.XrGroup_param_08_1.Name = "GroupBox1"
            Me.XrGroup_param_08_1.Size = New System.Drawing.Size(216, 112)
            Me.ToolTipController1.SetSuperTip(Me.XrGroup_param_08_1, Nothing)
            Me.XrGroup_param_08_1.TabIndex = 0
            Me.XrGroup_param_08_1.Text = " Date Range "
            '
            'ComboboxEdit_DateRange
            '
            Me.XrCombo_param_08_1.EditValue = ""
            Me.XrCombo_param_08_1.Location = New System.Drawing.Point(16, 26)
            Me.XrCombo_param_08_1.Name = "ComboboxEdit_DateRange"
            Me.XrCombo_param_08_1.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.XrCombo_param_08_1.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
            Me.XrCombo_param_08_1.Size = New System.Drawing.Size(184, 20)
            Me.XrCombo_param_08_1.TabIndex = 0
            Me.XrCombo_param_08_1.ToolTip = "Select a standard period for the date ranges"
            Me.XrCombo_param_08_1.ToolTipController = Me.ToolTipController1
            '
            'DateEdit_Ending
            '
            Me.XrDate_param_08_2.EditValue = New Date(2005, 12, 24, 0, 0, 0, 0)
            Me.XrDate_param_08_2.Location = New System.Drawing.Point(112, 78)
            Me.XrDate_param_08_2.Name = "DateEdit_Ending"
            Me.XrDate_param_08_2.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.XrDate_param_08_2.Properties.NullDate = ""
            Me.XrDate_param_08_2.Properties.VistaTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton})
            Me.XrDate_param_08_2.Size = New System.Drawing.Size(88, 20)
            Me.XrDate_param_08_2.TabIndex = 4
            Me.XrDate_param_08_2.ToolTip = "Enter the ending date for the report"
            Me.XrDate_param_08_2.ToolTipController = Me.ToolTipController1
            '
            'DateEdit_Starting
            '
            Me.XrDate_param_08_1.EditValue = New Date(2005, 8, 24, 0, 0, 0, 0)
            Me.XrDate_param_08_1.Location = New System.Drawing.Point(112, 52)
            Me.XrDate_param_08_1.Name = "DateEdit_Starting"
            Me.XrDate_param_08_1.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.XrDate_param_08_1.Properties.NullDate = ""
            Me.XrDate_param_08_1.Properties.VistaTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton})
            Me.XrDate_param_08_1.Size = New System.Drawing.Size(88, 20)
            Me.XrDate_param_08_1.TabIndex = 2
            Me.XrDate_param_08_1.ToolTip = "Enter the starting date for the report"
            Me.XrDate_param_08_1.ToolTipController = Me.ToolTipController1
            '
            'Label3
            '
            Me.Label3.Location = New System.Drawing.Point(16, 80)
            Me.Label3.Name = "Label3"
            Me.Label3.Size = New System.Drawing.Size(69, 13)
            Me.Label3.TabIndex = 3
            Me.Label3.Text = "&Ending Period:"
            Me.Label3.ToolTipController = Me.ToolTipController1
            '
            'Label4
            '
            Me.Label4.Location = New System.Drawing.Point(16, 54)
            Me.Label4.Name = "Label4"
            Me.Label4.Size = New System.Drawing.Size(75, 13)
            Me.Label4.TabIndex = 1
            Me.Label4.Text = "&Starting Period:"
            Me.Label4.ToolTipController = Me.ToolTipController1
            '
            'DateReportParametersForm
            '
            Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
            Me.ClientSize = New System.Drawing.Size(336, 137)
            Me.Controls.Add(Me.XrGroup_param_08_1)
            Me.Name = "DateReportParametersForm"
            Me.ToolTipController1.SetSuperTip(Me, Nothing)
            Me.Controls.SetChildIndex(Me.ButtonOK, 0)
            Me.Controls.SetChildIndex(Me.ButtonCancel, 0)
            Me.Controls.SetChildIndex(Me.XrGroup_param_08_1, 0)
            CType(Me.DxErrorProvider1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.XrGroup_param_08_1, System.ComponentModel.ISupportInitialize).EndInit()
            Me.XrGroup_param_08_1.ResumeLayout(False)
            Me.XrGroup_param_08_1.PerformLayout()
            CType(Me.XrCombo_param_08_1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.XrDate_param_08_2.Properties.VistaTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.XrDate_param_08_2.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.XrDate_param_08_1.Properties.VistaTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.XrDate_param_08_1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            Me.ResumeLayout(False)
        End Sub
    End Class
End Namespace
