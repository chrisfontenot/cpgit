﻿Namespace Template.Forms
    <Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
    Partial Class CounselorParametersForm
        Inherits Forms.ReportParametersForm

        'Form overrides dispose to clean up the component list.
        Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
            If disposing Then
                If components IsNot Nothing Then
                    components.Dispose()
                End If
            End If
            MyBase.Dispose(disposing)
        End Sub

        'Required by the Windows Form Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Windows Form Designer
        'It can be modified using the Windows Form Designer.  
        'Do not modify it using the code editor.
        Protected Friend WithEvents Label1 As DevExpress.XtraEditors.LabelControl
        Protected Friend WithEvents XrCounselor_param_02_1 As DevExpress.XtraEditors.LookUpEdit
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
            Me.Label1 = New DevExpress.XtraEditors.LabelControl()
            Me.XrCounselor_param_02_1 = New DevExpress.XtraEditors.LookUpEdit()
            CType(Me.DxErrorProvider1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.XrCounselor_param_02_1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.SuspendLayout()
            '
            'ButtonOK
            '
            Me.ButtonOK.Location = New System.Drawing.Point(247, 16)
            Me.ButtonOK.TabIndex = 2
            '
            'ButtonCancel
            '
            Me.ButtonCancel.Location = New System.Drawing.Point(247, 48)
            Me.ButtonCancel.TabIndex = 3
            '
            'Label1
            '
            Me.Label1.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
            Me.Label1.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center
            Me.Label1.Location = New System.Drawing.Point(8, 35)
            Me.Label1.Name = "Label1"
            Me.Label1.Size = New System.Drawing.Size(52, 13)
            Me.Label1.TabIndex = 0
            Me.Label1.Text = "Counse&lor:"
            '
            'XrCounselor_param_02_1
            '
            Me.XrCounselor_param_02_1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.XrCounselor_param_02_1.Location = New System.Drawing.Point(66, 32)
            Me.XrCounselor_param_02_1.Name = "XrCounselor_param_02_1"
            Me.XrCounselor_param_02_1.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.XrCounselor_param_02_1.Properties.PopupWidth = 300
            Me.XrCounselor_param_02_1.Properties.ShowFooter = False
            Me.XrCounselor_param_02_1.Properties.ShowHeader = False
            Me.XrCounselor_param_02_1.Properties.SortColumnIndex = 1
            Me.XrCounselor_param_02_1.Size = New System.Drawing.Size(175, 20)
            Me.XrCounselor_param_02_1.TabIndex = 1
            Me.XrCounselor_param_02_1.ToolTipController = Me.ToolTipController1
            '
            'CounselorParametersForm
            '
            Me.ClientSize = New System.Drawing.Size(335, 157)
            Me.Controls.Add(Me.XrCounselor_param_02_1)
            Me.Controls.Add(Me.Label1)
            Me.Name = "CounselorParametersForm"
            Me.Controls.SetChildIndex(Me.ButtonOK, 0)
            Me.Controls.SetChildIndex(Me.ButtonCancel, 0)
            Me.Controls.SetChildIndex(Me.Label1, 0)
            Me.Controls.SetChildIndex(Me.XrCounselor_param_02_1, 0)
            CType(Me.DxErrorProvider1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.XrCounselor_param_02_1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            Me.ResumeLayout(False)
            Me.PerformLayout()

        End Sub
    End Class
End Namespace