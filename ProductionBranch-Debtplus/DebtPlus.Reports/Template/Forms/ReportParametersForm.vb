#Region "Copyright 2000-2012 DebtPlus, L.L.C."

'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}

#End Region

Option Compare Binary
Option Explicit On
Option Strict On

Imports DebtPlus.Data.Forms


Namespace Template.Forms
    Public Class ReportParametersForm
        Inherits DebtPlusForm

        Public Sub New()
            InitializeComponent()
            RegisterHandlers()

            ' For some strange reason, the dialog results keep getting set. DON'T SET THEM!!!!
            ButtonCancel.DialogResult = Windows.Forms.DialogResult.None
            ButtonOK.DialogResult = Windows.Forms.DialogResult.None
        End Sub

        Private Sub RegisterHandlers()
            AddHandler ButtonCancel.Click, AddressOf ButtonCancel_Click
            AddHandler ButtonOK.Click, AddressOf ButtonOK_Click
        End Sub

        Private Sub UnRegisterHandlers()
            RemoveHandler ButtonCancel.Click, AddressOf ButtonCancel_Click
            RemoveHandler ButtonOK.Click, AddressOf ButtonOK_Click
        End Sub

        '' DO NOT DELETE THIS PROCEDURE!!!!
        Protected Overridable Sub ButtonOK_Click(ByVal sender As Object, ByVal e As System.EventArgs)
            DialogResult = Windows.Forms.DialogResult.OK
            If Not Modal Then
                Close()
            End If
        End Sub

        '' DO NOT DELETE THIS PROCEDURE!!!!
        Protected Overridable Sub ButtonCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs)
            DialogResult = Windows.Forms.DialogResult.Cancel
            If Not Modal Then
                Close()
            End If
        End Sub

        Protected Overridable Function HasErrors() As Boolean
            Return False
        End Function

        Protected Overridable Sub FormChanged(sender As Object, e As EventArgs)
            ButtonOK.Enabled = Not HasErrors()
        End Sub
    End Class
End Namespace
