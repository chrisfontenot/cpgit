#Region "Copyright 2000-2012 DebtPlus, L.L.C."

'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}

#End Region

Option Compare Binary
Option Explicit On
Option Strict On

Imports System.Linq
Imports DebtPlus.LINQ
Imports DevExpress.XtraEditors.Controls
Imports DebtPlus.UI.Common
Imports System.Data.SqlClient
Imports DevExpress.Utils

Namespace Template.Forms
    Public Class DisbursementParametersForm
        Inherits DepositParametersForm

        Public Sub New()
            MyBase.New()
            InitializeComponent()
        End Sub

#Region " Windows Form Designer generated code "

        'Form overrides dispose to clean up the component list.
        Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
            If disposing Then
                If components IsNot Nothing Then
                    components.Dispose()
                End If
            End If
            MyBase.Dispose(disposing)
        End Sub

        'Required by the Windows Form Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Windows Form Designer
        'It can be modified using the Windows Form Designer.
        'Do not modify it using the code editor.
        <System.Diagnostics.DebuggerStepThrough()>
        Private Sub InitializeComponent()
            Me.XrGroup_param_09_1.SuspendLayout()
            CType(Me.XrText_param_09_1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.XrLookup_param_09_1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.DxErrorProvider1, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.SuspendLayout()
            '
            'XrGroup_param_09_1
            '
            Me.XrGroup_param_09_1.Text = " Disbursement Batch ID  "
            '
            'XrRadio_param_09_1
            '
            Me.XrRadio_param_09_1.Checked = False
            Me.XrRadio_param_09_1.TabStop = False
            '
            'XrRadio_param_09_2
            '
            Me.XrRadio_param_09_2.Checked = True
            Me.XrRadio_param_09_2.TabStop = True
            '
            'Label1
            '
            Me.XrLbl_param_09_1.Appearance.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!,
                                                                System.Drawing.FontStyle.Bold,
                                                                System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.XrLbl_param_09_1.Appearance.Options.UseFont = True
            '
            'TextEdit_deposit_batch_id
            '
            Me.XrText_param_09_1.Properties.Appearance.Options.UseTextOptions = True
            Me.XrText_param_09_1.Properties.Appearance.TextOptions.HAlignment =
                DevExpress.Utils.HorzAlignment.Far
            Me.XrText_param_09_1.Properties.AppearanceDisabled.Options.UseTextOptions = True
            Me.XrText_param_09_1.Properties.AppearanceDisabled.TextOptions.HAlignment =
                DevExpress.Utils.HorzAlignment.Far
            Me.XrText_param_09_1.Properties.DisplayFormat.FormatString = "f0"
            Me.XrText_param_09_1.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.XrText_param_09_1.Properties.EditFormat.FormatString = "f0"
            Me.XrText_param_09_1.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            '
            'DisbursementParametersForm
            '
            Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
            Me.ClientSize = New System.Drawing.Size(336, 133)
            Me.Name = "DisbursementParametersForm"
            Me.XrGroup_param_09_1.ResumeLayout(False)
            Me.XrGroup_param_09_1.PerformLayout()
            CType(Me.XrText_param_09_1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.XrLookup_param_09_1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.DxErrorProvider1, System.ComponentModel.ISupportInitialize).EndInit()
            Me.ResumeLayout(False)
        End Sub
#End Region

        Protected Overrides Function getRecords() As System.Collections.Generic.List(Of DepositParametersForm.Lookup_Item)
            Using bc As New BusinessContext()
                Return (From rd In bc.registers_disbursements
                        Order By rd.date_created Descending
                        Take 50
                        Select New DepositParametersForm.Lookup_Item() With _
                               {
                                   .Id = rd.Id,
                                   .note = rd.note,
                                   .date_created = rd.date_created,
                                   .created_by = rd.created_by
                               }).ToList()
            End Using
        End Function

        Protected Overrides Sub load_Lookup()
            MyBase.load_Lookup()

            Me.XrLookup_param_09_1.Properties.Columns.Clear()
            Me.XrLookup_param_09_1.Properties.Columns.AddRange(New DevExpress.XtraEditors.Controls.LookUpColumnInfo() _
                                                               {
                                                                   New DevExpress.XtraEditors.Controls.LookUpColumnInfo("Id", "ID", 20, DevExpress.Utils.FormatType.Numeric, "f0", True, DevExpress.Utils.HorzAlignment.[Default]),
                                                                   New DevExpress.XtraEditors.Controls.LookUpColumnInfo("date_created", "Date", 20, DevExpress.Utils.FormatType.DateTime, "d", True, DevExpress.Utils.HorzAlignment.[Default], DevExpress.Data.ColumnSortOrder.Ascending),
                                                                   New DevExpress.XtraEditors.Controls.LookUpColumnInfo("created_by", "Creator", 20, DevExpress.Utils.FormatType.None, "", True, DevExpress.Utils.HorzAlignment.[Default], DevExpress.Data.ColumnSortOrder.Ascending),
                                                                   New DevExpress.XtraEditors.Controls.LookUpColumnInfo("note", "Note", 20, DevExpress.Utils.FormatType.None, "", True, DevExpress.Utils.HorzAlignment.[Default], DevExpress.Data.ColumnSortOrder.Ascending)
                                                               })
            Me.XrLookup_param_09_1.Properties.DisplayMember = "note"
            Me.XrLookup_param_09_1.ToolTip = "Choose a disbursement batch from the list if desired"
        End Sub
    End Class
End Namespace