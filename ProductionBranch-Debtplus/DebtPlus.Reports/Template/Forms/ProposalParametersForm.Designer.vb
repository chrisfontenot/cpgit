﻿Namespace Template.Forms
    <Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
    Partial Class ProposalParametersForm

        'Form overrides dispose to clean up the component list.
        <System.Diagnostics.DebuggerNonUserCode()> _
        Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
            If disposing Then
                If components IsNot Nothing Then
                    components.Dispose()
                End If
            End If
            MyBase.Dispose(disposing)
        End Sub

        'Required by the Windows Form Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Windows Form Designer
        'It can be modified using the Windows Form Designer.  
        'Do not modify it using the code editor.
        <System.Diagnostics.DebuggerStepThrough()>
        Private Sub InitializeComponent()
            Me.XrGroup_param_09_1.SuspendLayout()
            CType(Me.XrText_param_09_1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.XrLookup_param_09_1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.DxErrorProvider1, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.SuspendLayout()
            '
            'GroupBox1
            '
            Me.XrGroup_param_09_1.Text = " Proposal Batch ID "
            '
            'Label1
            '
            Me.XrLbl_param_09_1.Appearance.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!,
                                                                System.Drawing.FontStyle.Bold,
                                                                System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            'Me.Label1.Appearance.Options.UseFont = True
            '
            'TextEdit_deposit_batch_id
            '
            Me.XrText_param_09_1.Properties.Appearance.Options.UseTextOptions = True
            Me.XrText_param_09_1.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.XrText_param_09_1.Properties.AppearanceDisabled.Options.UseTextOptions = True
            Me.XrText_param_09_1.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.XrText_param_09_1.Properties.DisplayFormat.FormatString = "f0"
            Me.XrText_param_09_1.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.XrText_param_09_1.Properties.EditFormat.FormatString = "f0"
            Me.XrText_param_09_1.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            '
            'ProposalParametersForm
            '
            Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
            Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
            Me.ClientSize = New System.Drawing.Size(346, 131)
            Me.Name = "ProposalParametersForm"
            Me.XrGroup_param_09_1.ResumeLayout(False)
            Me.XrGroup_param_09_1.PerformLayout()
            CType(Me.XrText_param_09_1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.XrLookup_param_09_1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.DxErrorProvider1, System.ComponentModel.ISupportInitialize).EndInit()
            Me.ResumeLayout(False)
        End Sub

    End Class
End Namespace
