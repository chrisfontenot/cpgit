﻿#Region "Copyright 2000-2012 DebtPlus, L.L.C."

'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}

#End Region

Option Compare Binary
Option Explicit On
Option Strict On

Imports DebtPlus.Data.Forms

Namespace Template.Forms
    Friend Class SFTPUploadForm
        Inherits DebtPlusForm

        Friend Delegate Sub PerformUploadEventHandler(ByVal sender As Object, ByVal e As PerformUploadEventArgs)
        Friend Event PerformUpload As PerformUploadEventHandler
        Protected Sub RaisePerformUpload(ByVal e As PerformUploadEventArgs)
            RaiseEvent PerformUpload(Me, e)
        End Sub

        Protected Overridable Sub OnPerformUpload(ByVal e As PerformUploadEventArgs)
            RaisePerformUpload(e)
        End Sub

        Public Sub New()
            MyBase.New()
            InitializeComponent()
        End Sub

        Dim parm As SFTPUloadParameers = Nothing
        Public Sub New(parm As SFTPUloadParameers)
            MyClass.New()
            Me.parm = parm
            RegisterHandlers()
        End Sub

        ''' <summary>
        ''' Add the event handler routines
        ''' </summary>
        ''' <remarks></remarks>
        Private Sub RegisterHandlers()
            AddHandler Me.Load, AddressOf SFTPUploadForm_Load
            AddHandler TextEdit1.Validating, AddressOf TextEdit1_Validating
            AddHandler TextEdit1.EditValueChanged, AddressOf FormChanged
            AddHandler TextEdit_Directory.EditValueChanged, AddressOf FormChanged
            AddHandler TextEdit_Password.EditValueChanged, AddressOf FormChanged
            AddHandler TextEdit_Site.EditValueChanged, AddressOf FormChanged
            AddHandler TextEdit_UserName.EditValueChanged, AddressOf FormChanged
            AddHandler CheckEdit_SFTP_Upload.CheckedChanged, AddressOf FormChanged
            AddHandler SimpleButton1.Click, AddressOf SimpleButton1_Click
        End Sub

        ''' <summary>
        ''' Remove the event handler routines
        ''' </summary>
        ''' <remarks></remarks>
        Private Sub UnRegisterHandlers()
            RemoveHandler Me.Load, AddressOf SFTPUploadForm_Load
            RemoveHandler TextEdit1.Validating, AddressOf TextEdit1_Validating
            RemoveHandler TextEdit1.EditValueChanged, AddressOf FormChanged
            RemoveHandler TextEdit_Directory.EditValueChanged, AddressOf FormChanged
            RemoveHandler TextEdit_Password.EditValueChanged, AddressOf FormChanged
            RemoveHandler TextEdit_Site.EditValueChanged, AddressOf FormChanged
            RemoveHandler TextEdit_UserName.EditValueChanged, AddressOf FormChanged
            RemoveHandler CheckEdit_SFTP_Upload.CheckedChanged, AddressOf FormChanged
            RemoveHandler SimpleButton1.Click, AddressOf SimpleButton1_Click
        End Sub

        ''' <summary>
        ''' Handle a change in the input controls
        ''' </summary>
        Private Sub FormChanged(sender As Object, e As System.EventArgs)
            SimpleButton1.Enabled = Not HasErrors()
        End Sub

        ''' <summary>
        ''' Look for an error condition in the input form data
        ''' </summary>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Private Function HasErrors() As Boolean

            ' Some fields are required to be present if we are to have a valid upload
            If String.IsNullOrWhiteSpace(TextEdit_Directory.Text) OrElse String.IsNullOrWhiteSpace(TextEdit_Site.Text) OrElse String.IsNullOrWhiteSpace(TextEdit1.Text) Then
                Return True
            End If

            Return False
        End Function

        Private Sub SFTPUploadForm_Load(sender As Object, e As System.EventArgs)
            UnRegisterHandlers()
            Try
                EmptySpaceItem_Left.Width = (EmptySpaceItem_Left.Width + EmptySpaceItem_Right.Width) \ 2

                TextEdit_Directory.EditValue = parm.Directory
                TextEdit_Password.EditValue = parm.Password
                TextEdit_Site.EditValue = parm.Site
                TextEdit_UserName.EditValue = parm.UserName
                TextEdit1.EditValue = parm.FileName
                CheckEdit_SFTP_Upload.Checked = parm.isSecure

                SimpleButton1.Enabled = Not HasErrors()
            Finally
                RegisterHandlers()
            End Try
        End Sub

        Private Sub TextEdit1_Validating(sender As Object, e As System.ComponentModel.CancelEventArgs)

            UnRegisterHandlers()
            Try
                ' Get the file name. If the entry is not defined then the error routine will catch it later.
                Dim strValue As String = DebtPlus.Utils.Nulls.v_String(TextEdit1.EditValue)
                If String.IsNullOrWhiteSpace(strValue) Then
                    Return
                End If

                ' Ensure that the extension name is "PDF"
                Dim extName As String = System.IO.Path.GetExtension(strValue)
                If String.Compare(extName, ".pdf", True) <> 0 Then
                    strValue = System.IO.Path.ChangeExtension(strValue, ".pdf")
                    TextEdit1.EditValue = strValue
                End If

            Finally
                RegisterHandlers()
            End Try
        End Sub

        Private Sub SimpleButton1_Click(sender As Object, e As System.EventArgs)

            ' Retrieve the record parameters from the edit controls
            parm.Directory = DebtPlus.Utils.Nulls.v_String(TextEdit_Directory.EditValue)
            parm.Password = DebtPlus.Utils.Nulls.v_String(TextEdit_Password.EditValue)
            parm.Site = DebtPlus.Utils.Nulls.v_String(TextEdit_Site.EditValue)
            parm.UserName = DebtPlus.Utils.Nulls.v_String(TextEdit_UserName.EditValue)
            parm.FileName = DebtPlus.Utils.Nulls.v_String(TextEdit1.EditValue)
            parm.isSecure = CheckEdit_SFTP_Upload.Checked

            ' Tell the caller that we have processed the OK button and to do the upload now.
            Dim eProcess As New PerformUploadEventArgs() With {.Cancel = False, .Parameters = parm}
            OnPerformUpload(eProcess)

            ' Complete the dialog unless the event is canceled.
            If Not eProcess.Cancel Then
                DialogResult = Windows.Forms.DialogResult.OK
            End If
        End Sub

    End Class
End Namespace

Namespace Template.Forms
    Public Class SFTPUloadParameers

        Public Sub New()
            Me.UserName = Nothing
            Me.FileName = Nothing
            Me.Password = Nothing
            Me.Site = Nothing
            Me.Directory = Nothing
            Me.isSecure = True
        End Sub

        Public Property UserName As String
        Public Property Password As String
        Public Property Site As String
        Public Property Directory As String
        Public Property FileName As String
        Public Property isSecure As Boolean
    End Class

    Friend Class PerformUploadEventArgs
        Inherits System.ComponentModel.CancelEventArgs

        Public Sub New()
            MyBase.New()
        End Sub

        Public Sub New(ByVal Cancel As Boolean)
            MyBase.New(Cancel)
        End Sub

        Public Parameters As SFTPUloadParameers
    End Class

End Namespace

