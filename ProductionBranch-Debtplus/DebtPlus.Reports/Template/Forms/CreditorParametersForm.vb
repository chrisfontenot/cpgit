#Region "Copyright 2000-2012 DebtPlus, L.L.C."

'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}

#End Region

Option Compare Binary
Option Explicit On
Option Strict On

Imports DebtPlus.UI.Creditor.Widgets.Controls

Imports DebtPlus.UI.Creditor.Widgets

Namespace Template.Forms
    Public Class CreditorParametersForm
        Inherits ReportParametersForm

        Public ReadOnly Property Parameter_Creditor() As String
            Get
                With XrCreditor_param_03_1
                    If .Text.Length < 3 Then Return String.Empty
                    Return .Text.Trim()
                End With
            End Get
        End Property

        Public Sub New()
            MyBase.New()
            InitializeComponent()
            AddHandler Me.Load, AddressOf ParametersForm_Load
            AddHandler XrCreditor_param_03_1.EditValueChanged, AddressOf CreditorID1_EditValueChanged
        End Sub

#Region " Windows Form Designer generated code "

        'Form overrides dispose to clean up the component list.
        Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
            If disposing Then
                If components IsNot Nothing Then
                    components.Dispose()
                End If
            End If
            MyBase.Dispose(disposing)
        End Sub

        'Required by the Windows Form Designer
        Private components As System.ComponentModel.IContainer
        Protected Friend WithEvents XrCreditor_param_03_1 As CreditorID
        Protected Friend WithEvents Label1 As DevExpress.XtraEditors.LabelControl

        'NOTE: The following procedure is required by the Windows Form Designer
        'It can be modified using the Windows Form Designer.  
        'Do not modify it using the code editor.
        <System.Diagnostics.DebuggerStepThrough()>
        Private Sub InitializeComponent()
            Dim resources As System.ComponentModel.ComponentResourceManager =
                    New System.ComponentModel.ComponentResourceManager(GetType(CreditorParametersForm))
            Dim SerializableAppearanceObject1 As DevExpress.Utils.SerializableAppearanceObject =
                    New DevExpress.Utils.SerializableAppearanceObject
            Me.Label1 = New DevExpress.XtraEditors.LabelControl
            Me.XrCreditor_param_03_1 = New CreditorID
            CType(Me.DxErrorProvider1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.XrCreditor_param_03_1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.SuspendLayout()
            '
            'ButtonOK
            '
            Me.ButtonOK.Location = New System.Drawing.Point(162, 16)
            Me.ButtonOK.TabIndex = 2
            '
            'ButtonCancel
            '
            Me.ButtonCancel.Location = New System.Drawing.Point(162, 48)
            Me.ButtonCancel.TabIndex = 3
            '
            'Label1
            '
            Me.Label1.Location = New System.Drawing.Point(16, 21)
            Me.Label1.Name = "Label1"
            Me.Label1.Size = New System.Drawing.Size(43, 13)
            Me.Label1.TabIndex = 0
            Me.Label1.Text = "Creditor:"
            '
            'CreditorID1
            '
            Me.XrCreditor_param_03_1.EditValue = Nothing
            Me.XrCreditor_param_03_1.Location = New System.Drawing.Point(65, 18)
            Me.XrCreditor_param_03_1.Name = "CreditorID1"
            Me.XrCreditor_param_03_1.Properties.Buttons.Add(New DebtPlus.UI.Common.Controls.SearchButton())
            Me.XrCreditor_param_03_1.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
            Me.XrCreditor_param_03_1.Properties.Mask.AutoComplete = DevExpress.XtraEditors.Mask.AutoCompleteType.Strong
            Me.XrCreditor_param_03_1.Properties.Mask.BeepOnError = True
            Me.XrCreditor_param_03_1.Properties.Mask.EditMask = "[A-Z]{1,2}[0-9]{4,}"
            Me.XrCreditor_param_03_1.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx
            Me.XrCreditor_param_03_1.Properties.Mask.UseMaskAsDisplayFormat = True
            Me.XrCreditor_param_03_1.Properties.MaxLength = 10
            Me.XrCreditor_param_03_1.Size = New System.Drawing.Size(86, 20)
            Me.XrCreditor_param_03_1.TabIndex = 1
            '
            'CreditorParametersForm
            '
            Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
            Me.ClientSize = New System.Drawing.Size(250, 88)
            Me.Controls.Add(Me.Label1)
            Me.Controls.Add(Me.XrCreditor_param_03_1)
            Me.Name = "CreditorParametersForm"
            Me.TopMost = False
            Me.Controls.SetChildIndex(Me.XrCreditor_param_03_1, 0)
            Me.Controls.SetChildIndex(Me.Label1, 0)
            Me.Controls.SetChildIndex(Me.ButtonOK, 0)
            Me.Controls.SetChildIndex(Me.ButtonCancel, 0)
            CType(Me.DxErrorProvider1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.XrCreditor_param_03_1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            Me.ResumeLayout(False)
            Me.PerformLayout()
        End Sub

#End Region

        Protected Overrides Function HasErrors() As Boolean
            Return Parameter_Creditor = String.Empty
        End Function

        Private Sub ParametersForm_Load(ByVal sender As Object, ByVal e As EventArgs)
            ButtonOK.Enabled = Not HasErrors()
        End Sub

        Private Sub CreditorID1_EditValueChanged(ByVal sender As Object, ByVal e As EventArgs)
            ButtonOK.Enabled = Not HasErrors()
        End Sub
    End Class
End Namespace
