﻿Namespace Template.Forms
    <Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
    Partial Class SFTPUploadForm

        'Form overrides dispose to clean up the component list.
        <System.Diagnostics.DebuggerNonUserCode()> _
        Protected Overrides Sub Dispose(ByVal disposing As Boolean)
            Try
                If disposing AndAlso components IsNot Nothing Then
                    components.Dispose()
                End If
            Finally
                MyBase.Dispose(disposing)
            End Try
        End Sub

        'Required by the Windows Form Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Windows Form Designer
        'It can be modified using the Windows Form Designer.  
        'Do not modify it using the code editor.
        <System.Diagnostics.DebuggerStepThrough()> _
        Private Sub InitializeComponent()
            Me.LayoutControl1 = New DevExpress.XtraLayout.LayoutControl()
            Me.TextEdit1 = New DevExpress.XtraEditors.TextEdit()
            Me.SimpleButton2 = New DevExpress.XtraEditors.SimpleButton()
            Me.SimpleButton1 = New DevExpress.XtraEditors.SimpleButton()
            Me.CheckEdit_SFTP_Upload = New DevExpress.XtraEditors.CheckEdit()
            Me.TextEdit_Password = New DevExpress.XtraEditors.TextEdit()
            Me.TextEdit_UserName = New DevExpress.XtraEditors.TextEdit()
            Me.TextEdit_Directory = New DevExpress.XtraEditors.TextEdit()
            Me.TextEdit_Site = New DevExpress.XtraEditors.TextEdit()
            Me.LayoutControlGroup1 = New DevExpress.XtraLayout.LayoutControlGroup()
            Me.LayoutControlItem1 = New DevExpress.XtraLayout.LayoutControlItem()
            Me.LayoutControlItem2 = New DevExpress.XtraLayout.LayoutControlItem()
            Me.LayoutControlItem3 = New DevExpress.XtraLayout.LayoutControlItem()
            Me.LayoutControlItem4 = New DevExpress.XtraLayout.LayoutControlItem()
            Me.LayoutControlItem5 = New DevExpress.XtraLayout.LayoutControlItem()
            Me.LayoutControlItem6 = New DevExpress.XtraLayout.LayoutControlItem()
            Me.LayoutControlItem7 = New DevExpress.XtraLayout.LayoutControlItem()
            Me.EmptySpaceItem1 = New DevExpress.XtraLayout.EmptySpaceItem()
            Me.EmptySpaceItem_Left = New DevExpress.XtraLayout.EmptySpaceItem()
            Me.EmptySpaceItem_Right = New DevExpress.XtraLayout.EmptySpaceItem()
            Me.LayoutControlItem8 = New DevExpress.XtraLayout.LayoutControlItem()
            CType(Me.DxErrorProvider1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControl1, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.LayoutControl1.SuspendLayout()
            CType(Me.TextEdit1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.CheckEdit_SFTP_Upload.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.TextEdit_Password.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.TextEdit_UserName.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.TextEdit_Directory.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.TextEdit_Site.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlGroup1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem2, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem3, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem4, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem5, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem6, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem7, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.EmptySpaceItem1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.EmptySpaceItem_Left, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.EmptySpaceItem_Right, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem8, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.SuspendLayout()
            '
            'DefaultLookAndFeel1
            '
            Me.DefaultLookAndFeel1.LookAndFeel.SkinName = "McSkin"
            '
            'LayoutControl1
            '
            Me.LayoutControl1.Controls.Add(Me.TextEdit1)
            Me.LayoutControl1.Controls.Add(Me.SimpleButton2)
            Me.LayoutControl1.Controls.Add(Me.SimpleButton1)
            Me.LayoutControl1.Controls.Add(Me.CheckEdit_SFTP_Upload)
            Me.LayoutControl1.Controls.Add(Me.TextEdit_Password)
            Me.LayoutControl1.Controls.Add(Me.TextEdit_UserName)
            Me.LayoutControl1.Controls.Add(Me.TextEdit_Directory)
            Me.LayoutControl1.Controls.Add(Me.TextEdit_Site)
            Me.LayoutControl1.Dock = System.Windows.Forms.DockStyle.Fill
            Me.LayoutControl1.Location = New System.Drawing.Point(0, 0)
            Me.LayoutControl1.Name = "LayoutControl1"
            Me.LayoutControl1.Root = Me.LayoutControlGroup1
            Me.LayoutControl1.Size = New System.Drawing.Size(405, 206)
            Me.LayoutControl1.TabIndex = 0
            Me.LayoutControl1.Text = "LayoutControl1"
            '
            'TextEdit1
            '
            Me.TextEdit1.Location = New System.Drawing.Point(67, 60)
            Me.TextEdit1.Name = "TextEdit1"
            Me.TextEdit1.Size = New System.Drawing.Size(326, 20)
            Me.TextEdit1.StyleController = Me.LayoutControl1
            Me.TextEdit1.TabIndex = 11
            '
            'SimpleButton2
            '
            Me.SimpleButton2.DialogResult = System.Windows.Forms.DialogResult.Cancel
            Me.SimpleButton2.Location = New System.Drawing.Point(195, 168)
            Me.SimpleButton2.MaximumSize = New System.Drawing.Size(75, 26)
            Me.SimpleButton2.MinimumSize = New System.Drawing.Size(75, 26)
            Me.SimpleButton2.Name = "SimpleButton2"
            Me.SimpleButton2.Size = New System.Drawing.Size(75, 26)
            Me.SimpleButton2.StyleController = Me.LayoutControl1
            Me.SimpleButton2.TabIndex = 10
            Me.SimpleButton2.Text = "&Cancel"
            '
            'SimpleButton1
            '
            Me.SimpleButton1.Location = New System.Drawing.Point(116, 168)
            Me.SimpleButton1.MaximumSize = New System.Drawing.Size(75, 26)
            Me.SimpleButton1.MinimumSize = New System.Drawing.Size(75, 26)
            Me.SimpleButton1.Name = "SimpleButton1"
            Me.SimpleButton1.Size = New System.Drawing.Size(75, 26)
            Me.SimpleButton1.StyleController = Me.LayoutControl1
            Me.SimpleButton1.TabIndex = 9
            Me.SimpleButton1.Text = "&Send File"
            '
            'CheckEdit_SFTP_Upload
            '
            Me.CheckEdit_SFTP_Upload.Location = New System.Drawing.Point(12, 132)
            Me.CheckEdit_SFTP_Upload.Name = "CheckEdit_SFTP_Upload"
            Me.CheckEdit_SFTP_Upload.Properties.Caption = "Use Secure FTP for the transfer"
            Me.CheckEdit_SFTP_Upload.Size = New System.Drawing.Size(381, 20)
            Me.CheckEdit_SFTP_Upload.StyleController = Me.LayoutControl1
            Me.CheckEdit_SFTP_Upload.TabIndex = 8
            '
            'TextEdit_Password
            '
            Me.TextEdit_Password.Location = New System.Drawing.Point(67, 108)
            Me.TextEdit_Password.Name = "TextEdit_Password"
            Me.TextEdit_Password.Size = New System.Drawing.Size(326, 20)
            Me.TextEdit_Password.StyleController = Me.LayoutControl1
            Me.TextEdit_Password.TabIndex = 7
            '
            'TextEdit_UserName
            '
            Me.TextEdit_UserName.Location = New System.Drawing.Point(67, 84)
            Me.TextEdit_UserName.Name = "TextEdit_UserName"
            Me.TextEdit_UserName.Size = New System.Drawing.Size(326, 20)
            Me.TextEdit_UserName.StyleController = Me.LayoutControl1
            Me.TextEdit_UserName.TabIndex = 6
            '
            'TextEdit_Directory
            '
            Me.TextEdit_Directory.Location = New System.Drawing.Point(67, 36)
            Me.TextEdit_Directory.Name = "TextEdit_Directory"
            Me.TextEdit_Directory.Size = New System.Drawing.Size(326, 20)
            Me.TextEdit_Directory.StyleController = Me.LayoutControl1
            Me.TextEdit_Directory.TabIndex = 5
            '
            'TextEdit_Site
            '
            Me.TextEdit_Site.Location = New System.Drawing.Point(67, 12)
            Me.TextEdit_Site.Name = "TextEdit_Site"
            Me.TextEdit_Site.Size = New System.Drawing.Size(326, 20)
            Me.TextEdit_Site.StyleController = Me.LayoutControl1
            Me.TextEdit_Site.TabIndex = 4
            '
            'LayoutControlGroup1
            '
            Me.LayoutControlGroup1.CustomizationFormText = "LayoutControlGroup1"
            Me.LayoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.[True]
            Me.LayoutControlGroup1.GroupBordersVisible = False
            Me.LayoutControlGroup1.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.LayoutControlItem1, Me.LayoutControlItem2, Me.LayoutControlItem3, Me.LayoutControlItem4, Me.LayoutControlItem5, Me.LayoutControlItem6, Me.LayoutControlItem7, Me.EmptySpaceItem1, Me.EmptySpaceItem_Left, Me.EmptySpaceItem_Right, Me.LayoutControlItem8})
            Me.LayoutControlGroup1.Location = New System.Drawing.Point(0, 0)
            Me.LayoutControlGroup1.Name = "LayoutControlGroup1"
            Me.LayoutControlGroup1.Size = New System.Drawing.Size(405, 206)
            Me.LayoutControlGroup1.Text = "LayoutControlGroup1"
            Me.LayoutControlGroup1.TextVisible = False
            '
            'LayoutControlItem1
            '
            Me.LayoutControlItem1.Control = Me.TextEdit_Site
            Me.LayoutControlItem1.CustomizationFormText = "Site"
            Me.LayoutControlItem1.Location = New System.Drawing.Point(0, 0)
            Me.LayoutControlItem1.Name = "LayoutControlItem1"
            Me.LayoutControlItem1.Size = New System.Drawing.Size(385, 24)
            Me.LayoutControlItem1.Text = "Site"
            Me.LayoutControlItem1.TextSize = New System.Drawing.Size(52, 13)
            '
            'LayoutControlItem2
            '
            Me.LayoutControlItem2.Control = Me.TextEdit_Directory
            Me.LayoutControlItem2.CustomizationFormText = "Directory"
            Me.LayoutControlItem2.Location = New System.Drawing.Point(0, 24)
            Me.LayoutControlItem2.Name = "LayoutControlItem2"
            Me.LayoutControlItem2.Size = New System.Drawing.Size(385, 24)
            Me.LayoutControlItem2.Text = "Directory"
            Me.LayoutControlItem2.TextSize = New System.Drawing.Size(52, 13)
            '
            'LayoutControlItem3
            '
            Me.LayoutControlItem3.Control = Me.TextEdit_UserName
            Me.LayoutControlItem3.CustomizationFormText = "User Name"
            Me.LayoutControlItem3.Location = New System.Drawing.Point(0, 72)
            Me.LayoutControlItem3.Name = "LayoutControlItem3"
            Me.LayoutControlItem3.Size = New System.Drawing.Size(385, 24)
            Me.LayoutControlItem3.Text = "User Name"
            Me.LayoutControlItem3.TextSize = New System.Drawing.Size(52, 13)
            '
            'LayoutControlItem4
            '
            Me.LayoutControlItem4.Control = Me.TextEdit_Password
            Me.LayoutControlItem4.CustomizationFormText = "Password"
            Me.LayoutControlItem4.Location = New System.Drawing.Point(0, 96)
            Me.LayoutControlItem4.Name = "LayoutControlItem4"
            Me.LayoutControlItem4.Size = New System.Drawing.Size(385, 24)
            Me.LayoutControlItem4.Text = "Password"
            Me.LayoutControlItem4.TextSize = New System.Drawing.Size(52, 13)
            '
            'LayoutControlItem5
            '
            Me.LayoutControlItem5.Control = Me.CheckEdit_SFTP_Upload
            Me.LayoutControlItem5.CustomizationFormText = "LayoutControlItem5"
            Me.LayoutControlItem5.Location = New System.Drawing.Point(0, 120)
            Me.LayoutControlItem5.Name = "LayoutControlItem5"
            Me.LayoutControlItem5.Size = New System.Drawing.Size(385, 24)
            Me.LayoutControlItem5.Text = "LayoutControlItem5"
            Me.LayoutControlItem5.TextSize = New System.Drawing.Size(0, 0)
            Me.LayoutControlItem5.TextToControlDistance = 0
            Me.LayoutControlItem5.TextVisible = False
            '
            'LayoutControlItem6
            '
            Me.LayoutControlItem6.Control = Me.SimpleButton1
            Me.LayoutControlItem6.CustomizationFormText = "LayoutControlItem6"
            Me.LayoutControlItem6.Location = New System.Drawing.Point(104, 156)
            Me.LayoutControlItem6.Name = "LayoutControlItem6"
            Me.LayoutControlItem6.Size = New System.Drawing.Size(79, 30)
            Me.LayoutControlItem6.Text = "LayoutControlItem6"
            Me.LayoutControlItem6.TextSize = New System.Drawing.Size(0, 0)
            Me.LayoutControlItem6.TextToControlDistance = 0
            Me.LayoutControlItem6.TextVisible = False
            '
            'LayoutControlItem7
            '
            Me.LayoutControlItem7.Control = Me.SimpleButton2
            Me.LayoutControlItem7.CustomizationFormText = "LayoutControlItem7"
            Me.LayoutControlItem7.Location = New System.Drawing.Point(183, 156)
            Me.LayoutControlItem7.Name = "LayoutControlItem7"
            Me.LayoutControlItem7.Size = New System.Drawing.Size(79, 30)
            Me.LayoutControlItem7.Text = "LayoutControlItem7"
            Me.LayoutControlItem7.TextSize = New System.Drawing.Size(0, 0)
            Me.LayoutControlItem7.TextToControlDistance = 0
            Me.LayoutControlItem7.TextVisible = False
            '
            'EmptySpaceItem1
            '
            Me.EmptySpaceItem1.AllowHotTrack = False
            Me.EmptySpaceItem1.CustomizationFormText = "EmptySpaceItem1"
            Me.EmptySpaceItem1.Location = New System.Drawing.Point(0, 144)
            Me.EmptySpaceItem1.Name = "EmptySpaceItem1"
            Me.EmptySpaceItem1.Size = New System.Drawing.Size(385, 12)
            Me.EmptySpaceItem1.Text = "EmptySpaceItem1"
            Me.EmptySpaceItem1.TextSize = New System.Drawing.Size(0, 0)
            '
            'EmptySpaceItem_Left
            '
            Me.EmptySpaceItem_Left.AllowHotTrack = False
            Me.EmptySpaceItem_Left.CustomizationFormText = "Left Edge Spacer"
            Me.EmptySpaceItem_Left.Location = New System.Drawing.Point(0, 156)
            Me.EmptySpaceItem_Left.Name = "EmptySpaceItem_Left"
            Me.EmptySpaceItem_Left.Size = New System.Drawing.Size(104, 30)
            Me.EmptySpaceItem_Left.Text = "Left"
            Me.EmptySpaceItem_Left.TextSize = New System.Drawing.Size(0, 0)
            '
            'EmptySpaceItem_Right
            '
            Me.EmptySpaceItem_Right.AllowHotTrack = False
            Me.EmptySpaceItem_Right.CustomizationFormText = "Right Edge Spacer"
            Me.EmptySpaceItem_Right.Location = New System.Drawing.Point(262, 156)
            Me.EmptySpaceItem_Right.Name = "EmptySpaceItem_Right"
            Me.EmptySpaceItem_Right.Size = New System.Drawing.Size(123, 30)
            Me.EmptySpaceItem_Right.Text = "Right"
            Me.EmptySpaceItem_Right.TextSize = New System.Drawing.Size(0, 0)
            '
            'LayoutControlItem8
            '
            Me.LayoutControlItem8.Control = Me.TextEdit1
            Me.LayoutControlItem8.CustomizationFormText = "FileName"
            Me.LayoutControlItem8.Location = New System.Drawing.Point(0, 48)
            Me.LayoutControlItem8.Name = "LayoutControlItem8"
            Me.LayoutControlItem8.Size = New System.Drawing.Size(385, 24)
            Me.LayoutControlItem8.Text = "FileName"
            Me.LayoutControlItem8.TextSize = New System.Drawing.Size(52, 13)
            '
            'SFTPUploadForm
            '
            Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
            Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
            Me.CancelButton = Me.SimpleButton2
            Me.ClientSize = New System.Drawing.Size(405, 206)
            Me.Controls.Add(Me.LayoutControl1)
            Me.Name = "SFTPUploadForm"
            Me.Text = "FTP Upload"
            CType(Me.DxErrorProvider1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControl1, System.ComponentModel.ISupportInitialize).EndInit()
            Me.LayoutControl1.ResumeLayout(False)
            CType(Me.TextEdit1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.CheckEdit_SFTP_Upload.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.TextEdit_Password.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.TextEdit_UserName.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.TextEdit_Directory.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.TextEdit_Site.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlGroup1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem2, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem3, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem4, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem5, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem6, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem7, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.EmptySpaceItem1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.EmptySpaceItem_Left, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.EmptySpaceItem_Right, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem8, System.ComponentModel.ISupportInitialize).EndInit()
            Me.ResumeLayout(False)

        End Sub
        Friend WithEvents LayoutControl1 As DevExpress.XtraLayout.LayoutControl
        Friend WithEvents LayoutControlGroup1 As DevExpress.XtraLayout.LayoutControlGroup
        Friend WithEvents TextEdit1 As DevExpress.XtraEditors.TextEdit
        Friend WithEvents SimpleButton2 As DevExpress.XtraEditors.SimpleButton
        Friend WithEvents SimpleButton1 As DevExpress.XtraEditors.SimpleButton
        Friend WithEvents CheckEdit_SFTP_Upload As DevExpress.XtraEditors.CheckEdit
        Friend WithEvents TextEdit_Password As DevExpress.XtraEditors.TextEdit
        Friend WithEvents TextEdit_UserName As DevExpress.XtraEditors.TextEdit
        Friend WithEvents TextEdit_Directory As DevExpress.XtraEditors.TextEdit
        Friend WithEvents TextEdit_Site As DevExpress.XtraEditors.TextEdit
        Friend WithEvents LayoutControlItem1 As DevExpress.XtraLayout.LayoutControlItem
        Friend WithEvents LayoutControlItem2 As DevExpress.XtraLayout.LayoutControlItem
        Friend WithEvents LayoutControlItem3 As DevExpress.XtraLayout.LayoutControlItem
        Friend WithEvents LayoutControlItem4 As DevExpress.XtraLayout.LayoutControlItem
        Friend WithEvents LayoutControlItem5 As DevExpress.XtraLayout.LayoutControlItem
        Friend WithEvents LayoutControlItem6 As DevExpress.XtraLayout.LayoutControlItem
        Friend WithEvents LayoutControlItem7 As DevExpress.XtraLayout.LayoutControlItem
        Friend WithEvents EmptySpaceItem1 As DevExpress.XtraLayout.EmptySpaceItem
        Friend WithEvents EmptySpaceItem_Left As DevExpress.XtraLayout.EmptySpaceItem
        Friend WithEvents EmptySpaceItem_Right As DevExpress.XtraLayout.EmptySpaceItem
        Friend WithEvents LayoutControlItem8 As DevExpress.XtraLayout.LayoutControlItem
    End Class
End Namespace
