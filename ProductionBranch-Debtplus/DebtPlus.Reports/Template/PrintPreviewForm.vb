#Region "Copyright 2000-2012 DebtPlus, L.L.C."

'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}

#End Region

Imports DevExpress.XtraPrinting
Imports DevExpress.XtraBars
Imports DevExpress.XtraReports.Parameters

Namespace Template
    Public Class PrintPreviewForm
        Inherits DebtPlus.Data.Forms.DebtPlusForm

        ''' <summary>
        ''' Name of the key in the registry for saving the location/size of the form
        ''' </summary>
        Private Const ReportPlacementName As String = "Reports"

        ''' <summary>
        ''' Current printing subsystem
        ''' </summary>
        Private WithEvents PrintingSystem As PrintingSystemBase

        Dim DocumentCreated As Boolean

        ''' <summary>
        ''' Pointer to the report currently loaded
        ''' </summary>
        Protected Property Report() As BaseXtraReportClass

        ''' <summary>
        ''' Return the status for the report filter
        ''' </summary>
        Private ReadOnly Property SupportFilter() As Boolean
            Get
                If Report Is Nothing Then Return False
                If Not TypeOf Report Is DebtPlus.Interfaces.Reports.IReportFilter Then Return False
                Return CType(Report, DebtPlus.Interfaces.Reports.IReportFilter).ReportFilter.IsEnabled
            End Get
        End Property

        ''' <summary>
        ''' Event to process the Customize Report function
        ''' </summary>
        Public Event CustomizeReport As EventHandler
        Protected Overridable Sub OnCustomizeReport(ByVal e As EventArgs)
            RaiseEvent CustomizeReport(Me, e)
        End Sub

        ''' <summary>
        ''' Raise the CustomizeReport event
        ''' </summary>
        Protected Sub RaiseCustomizeReport()
            OnCustomizeReport(EventArgs.Empty)
        End Sub

        Public Sub New(ByVal rpt As BaseXtraReportClass)
            MyBase.New()
            Report = rpt
            InitializeComponent()
            RegisterHandlers()
        End Sub

        Private Sub RegisterHandlers()
            AddHandler Timer1.Tick, AddressOf Timer1_Tick
            AddHandler Load, AddressOf Form_Load
            AddHandler BarButtonItemCustomizeReport.ItemClick, AddressOf BarButtonItemCustomizeReport_ItemClick
            AddHandler BarButtonItem_Filter.ItemClick, AddressOf BarButtonItem_Filter_ItemClick
            AddHandler BarButtonItem2.ItemClick, AddressOf BarButtonItem2_ItemClick

            If PrintingSystem IsNot Nothing Then
                AddHandler PrintingSystem.AfterBuildPages, AddressOf PrintingSystem_AfterBuildPages
                AddHandler PrintingSystem.BeforeBuildPages, AddressOf PrintingSystem_BeforeBuildPages
            End If
        End Sub


        Private Sub UnRegisterHandlers()
            RemoveHandler Timer1.Tick, AddressOf Timer1_Tick
            RemoveHandler Load, AddressOf Form_Load
            RemoveHandler BarButtonItemCustomizeReport.ItemClick, AddressOf BarButtonItemCustomizeReport_ItemClick
            RemoveHandler BarButtonItem_Filter.ItemClick, AddressOf BarButtonItem_Filter_ItemClick
            RemoveHandler BarButtonItem2.ItemClick, AddressOf BarButtonItem2_ItemClick

            If PrintingSystem IsNot Nothing Then
                RemoveHandler PrintingSystem.AfterBuildPages, AddressOf PrintingSystem_AfterBuildPages
                RemoveHandler PrintingSystem.BeforeBuildPages, AddressOf PrintingSystem_BeforeBuildPages
            End If
        End Sub

        ''' <summary>
        ''' Handle the tick of the timer
        ''' </summary>
        Private Sub Timer1_Tick(ByVal sender As Object, ByVal e As EventArgs)
            UnRegisterHandlers()
            Try
                ' Do this the first time that the report is being displayed.
                If Not DocumentCreated Then
                    DocumentCreated = True
                    Report.CreateDocument(True)
                    Report.PrintingSystem.ShowMarginsWarning = False
                End If

                Timer1.Enabled = False

            Catch ex As Exception
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error creating report document")

            Finally
                RegisterHandlers()
            End Try
        End Sub

        ''' <summary>
        ''' Handle the load event for the preview form
        ''' </summary>
        Private Sub Form_Load(ByVal sender As Object, ByVal e As EventArgs)
            UnRegisterHandlers()

            Try
                ' Do not embed PDF fonts
                '
                ' We need to set this before we show the PDF dialog so do it now. If you really want to
                ' override this and export the PDF document yourself, then just set the NeverEmbeddedFonts
                ' string to an empty string.
                Report.ExportOptions.Pdf.NeverEmbeddedFonts = DebtPlus.Data.Fonts.GetExcludeList()

            Catch ex As Exception
            End Try

            Try
                ' Restore the form placement
                LoadPlacement(ReportPlacementName)

                ' Use it for our text as well
                If Report.ReportTitle <> String.Empty Then
                    Text = String.Format("Preview - {0}", Report.ReportTitle)
                Else
                    Text = "Preview - Report"
                End If

                ' Connect to the report
                PrintingSystem = Report.PrintingSystem
                PrintControl1.PrintingSystem = PrintingSystem

                ' The report should not show the parameter form unless requested by the report.
                If Not Report.RequestParameters Then
                    For ItemNo As Int32 = Report.Parameters.Count - 1 To 0 Step -1
                        Dim parm As Parameter = Report.Parameters(ItemNo)
                        parm.Visible = False
                    Next
                End If

                ' Enable the select expert if the report supports the filtering
                BarButtonItem_Filter.Enabled = SupportFilter

            Finally
                RegisterHandlers()
            End Try

            ' Start a timer to trip a callback when the form is processed and painted
            If Report.PrintingSystem Is Nothing OrElse Report.PrintingSystem.Document.PageCount = 0 Then
                Timer1.Interval = 10
                Timer1.Enabled = True
            End If
        End Sub

        ''' <summary>
        ''' Process the click on the Customize button
        ''' </summary>
        Private Sub BarButtonItemCustomizeReport_ItemClick(ByVal sender As Object, ByVal e As ItemClickEventArgs)
            RaiseCustomizeReport()
        End Sub

        ''' <summary>
        ''' Handle the condition once the report has been built
        ''' </summary>
        Private Sub PrintingSystem_AfterBuildPages(ByVal sender As Object, ByVal e As EventArgs)

            ' Hide the accessing information
            BarStaticItemAccessingDatabase.Visibility = BarItemVisibility.Never

            ' Set the name of the document for export
            If Report.ReportTitle <> String.Empty Then
                PrintingSystem.Document.Name = Report.ReportTitle
            Else
                PrintingSystem.Document.Name = "DebtPlus Report"
            End If
        End Sub

        ''' <summary>
        ''' Handle the condition before building the report
        ''' </summary>
        Private Sub PrintingSystem_BeforeBuildPages(ByVal sender As Object, ByVal e As EventArgs)
            BarStaticItemAccessingDatabase.Visibility = BarItemVisibility.Always
        End Sub

        ''' <summary>
        ''' Process the click event on the Select Expert button
        ''' </summary>
        Private Sub BarButtonItem_Filter_ItemClick(sender As Object, e As ItemClickEventArgs)
            If Not SupportFilter Then Return

            UnRegisterHandlers()
            Try
                Using frm As New SelectExpertForm(Report, CType(Report, DebtPlus.Interfaces.Reports.IReportFilter).ReportFilter.CriteriaFilter)
                    If frm.ShowDialog() = DialogResult.OK Then
                        CType(Report, DebtPlus.Interfaces.Reports.IReportFilter).ReportFilter.CriteriaFilter = frm.CriteriaFilter
                        Report.CreateDocument()
                    End If
                End Using
            Finally
                RegisterHandlers()
            End Try
        End Sub

        ''' <summary>
        ''' Process the menu pick to upload the file to an FTP server
        ''' </summary>
        Private Sub BarButtonItem2_ItemClick(sender As System.Object, e As DevExpress.XtraBars.ItemClickEventArgs)
            Dim parm As New DebtPlus.Reports.Template.Forms.SFTPUloadParameers()

            ' Read the current structure from the saved parameter file
            Dim savePath As String = System.IO.Path.Combine(System.Environment.GetFolderPath(System.Environment.SpecialFolder.LocalApplicationData), "DebtPlus")
            Dim saveFile As String = "DebtPlus.Reports.Template.PrintPreviewForm.FTP.xml"
            Dim fname As String = System.IO.Path.Combine(savePath, saveFile)

            Try
                If System.IO.File.Exists(fname) Then
                    Using fs As System.IO.FileStream = System.IO.File.OpenRead(fname)
                        Dim ser As System.Xml.Serialization.XmlSerializer = New System.Xml.Serialization.XmlSerializer(GetType(DebtPlus.Reports.Template.Forms.SFTPUloadParameers), String.Empty)
                        parm = TryCast(ser.Deserialize(fs), DebtPlus.Reports.Template.Forms.SFTPUloadParameers)
                    End Using
                End If
            Catch
            End Try

            ' The file name is the only real variable here. So ensure that it is consistent.
            parm.FileName = String.Empty

            ' Request the parameters for the upload event
            Try
                Using frmUpload As New DebtPlus.Reports.Template.Forms.SFTPUploadForm(parm)
                    AddHandler frmUpload.PerformUpload, AddressOf sftpUpload
                    frmUpload.ShowDialog()
                    RemoveHandler frmUpload.PerformUpload, AddressOf sftpUpload
                End Using

                ' Save the parameters that were given to the resource for the next operation
                If Not System.IO.Directory.Exists(savePath) Then
                    System.IO.Directory.CreateDirectory(savePath)
                End If

                Using fs As System.IO.FileStream = System.IO.File.OpenWrite(fname)
                    Dim ser As System.Xml.Serialization.XmlSerializer = New System.Xml.Serialization.XmlSerializer(GetType(DebtPlus.Reports.Template.Forms.SFTPUloadParameers), String.Empty)
                    ser.Serialize(fs, parm)
                End Using

            Catch ex As System.Exception
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error doing the operation request")
            End Try

        End Sub

        ''' <summary>
        ''' Upload the file
        ''' </summary>
        Private Sub sftpUpload(ByVal sender As Object, ByVal e As DebtPlus.Reports.Template.Forms.PerformUploadEventArgs)

            ' Render the document as a PDF file
            Dim fileName As String = System.IO.Path.GetTempFileName()
            Try
                Using outStream As New System.IO.FileStream(fileName, IO.FileMode.Open)
                    Report.CreatePdfDocument(outStream)
                    outStream.Flush()
                    outStream.Close()
                End Using

                ' Perform the upload operation
                If e.Parameters.isSecure Then
                    e.Cancel = uploadSFTP(e.Parameters, fileName)
                Else
                    e.Cancel = uploadFTP(e.Parameters, fileName)
                End If

            Catch ex As Exception
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error sending data to FTP site")
                e.Cancel = True
            Finally
                DeleteTempoaryFile(fileName)
            End Try
        End Sub

        ''' <summary>
        ''' Delete the temporary file from the system. Since it is temporary and we are on the way
        ''' out, we don't really care about errors at this point. Just ignore them.
        ''' </summary>
        Private Sub DeleteTempoaryFile(FileName As String)

            Try
                ' Remove the temporary file from the system.
                System.IO.File.Delete(FileName)
            Catch
            End Try
        End Sub

        ''' <summary>
        ''' Upload the file with SFTP protocol
        ''' </summary>
        Private Function uploadSFTP(param As DebtPlus.Reports.Template.Forms.SFTPUloadParameers, ByVal fileName As String)
            Dim port As Int32 = 0
            Dim host As String = String.Empty

            ' Split the port from the server designation
            Dim exp As New System.Text.RegularExpressions.Regex("^([^:]+):(\d+)$", System.Text.RegularExpressions.RegexOptions.IgnoreCase Or System.Text.RegularExpressions.RegexOptions.Singleline)
            Dim m As System.Text.RegularExpressions.Match = exp.Match(param.Site)
            If m.Success Then
                Dim pStr As String = m.Groups(2).Captures(0).Value
                port = System.Int32.Parse(pStr)
                host = m.Groups(1).Captures(0).Value
            Else
                port = 22
                host = param.Site
            End If

            ' Create the FTP client to the host
            Dim ftpClient As New Tamir.SharpSsh.Sftp(host, param.UserName, param.Password)

            ' Send the file to the remote host
            ftpClient.Connect(port)
            Dim targetName As String = System.IO.Path.Combine(param.Directory.TrimEnd("/"c) + "/", param.FileName)
            ftpClient.Put(fileName, targetName)

            ' Return the valid status code.
            Return False
        End Function

        ''' <summary>
        ''' Upload the file with FTP protocol
        ''' </summary>
        Private Function uploadFTP(param As DebtPlus.Reports.Template.Forms.SFTPUloadParameers, ByVal fileName As String)
            Dim port As Int32 = 0
            Dim host As String = String.Empty

            ' Split the port from the server designation
            Dim exp As New System.Text.RegularExpressions.Regex("^([^:]+):(\d+)$", System.Text.RegularExpressions.RegexOptions.IgnoreCase Or System.Text.RegularExpressions.RegexOptions.Singleline)
            Dim m As System.Text.RegularExpressions.Match = exp.Match(param.Site)
            If m.Success Then
                Dim pStr As String = m.Groups(2).Captures(0).Value
                port = System.Int32.Parse(pStr)
                host = m.Groups(1).Captures(0).Value
            Else
                port = 21
                host = param.Site
            End If

            ' Regenerate the base string to include the fields that we need to specify
            Dim newFileName As String = String.Format("{0}{1}:{2:f0}/{3}", System.Uri.UriSchemeFtp, host, port, param.Directory.TrimEnd("/"c) + "/" + param.FileName)

            ' Get the object used to communicate with the server.
            Dim request As System.Net.FtpWebRequest = CType(System.Net.WebRequest.Create(newFileName), System.Net.FtpWebRequest)
            request.Method = System.Net.WebRequestMethods.Ftp.UploadFile
            request.EnableSsl = False
            request.UseBinary = True
            request.UsePassive = True

            ' Use the credentials if they are given. If not, use anonymous.
            If Not String.IsNullOrWhiteSpace(param.UserName) Then
                request.Credentials = New System.Net.NetworkCredential(param.UserName, param.Password)
            Else
                request.Credentials = New System.Net.NetworkCredential("anonymous", "DoNotReply@clearpointccs.org")
            End If

            ' Transmit the binary data to the FTP server
            Dim fileContents() As Byte
            Using ifs As New System.IO.StreamReader(fileName, System.Text.Encoding.UTF8)
                fileContents = System.Text.Encoding.UTF8.GetBytes(ifs.ReadToEnd())
            End Using
            request.ContentLength = fileContents.Length

            Dim requestStream As System.IO.Stream = request.GetRequestStream()
            requestStream.Write(fileContents, 0, fileContents.Length)
            requestStream.Close()

            ' Obtain the response from the server to determine the validity.
            Dim response As System.Net.FtpWebResponse = CType(request.GetResponse(), System.Net.FtpWebResponse)

            ' If successful, return the valid status to complete the operation.
            If response.StatusCode = Net.FtpStatusCode.CommandOK Then
                Return False
            End If

            ' Otherwise display the transfer error condition
            If Not String.IsNullOrWhiteSpace(response.StatusDescription) Then
                DebtPlus.Data.Forms.MessageBox.Show(response.StatusDescription, "Transfer Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                Return True
            End If

            ' Something strange happened
            DebtPlus.Data.Forms.MessageBox.Show(String.Format("Transfer failed with response code: {0:f0}", response.StatusCode), "Transfer Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Return True
        End Function
    End Class
End Namespace
