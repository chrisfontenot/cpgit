#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}
#End Region

Option Compare Binary
Option Explicit On
Option Strict On

Imports DebtPlus.Reports.Template
Imports System.IO
Imports DevExpress.XtraReports.UI
Imports DebtPlus.Utils

Namespace Creditor.Invoice.Aging

    Public Class CreditorInvoiceAgingReport
        Inherits DatedTemplateXtraReportClass

        ''' <summary>
        ''' Create an instance of our report
        ''' </summary>
        Public Sub New()
            MyBase.New()

            Const ReportName As String = "DebtPlus.Reports.Creditor.Invoice.Aging.repx"

            ' See if there is a report reference that we can use
            Dim Fname As String = System.IO.Path.Combine(DebtPlus.Configuration.Config.ReportsDirectory, ReportName)
            Dim UseDefault As Boolean = True
            Try
                If (Not DebtPlus.Configuration.Config.LocalReportsOnly) AndAlso System.IO.File.Exists(Fname) Then
                    LoadLayout(Fname)
                    UseDefault = False
                End If
            Catch ex As Exception
            End Try

            ' Load the standard report definition if we need a new item
            If UseDefault Then
                Dim asm As Reflection.Assembly = Reflection.Assembly.GetExecutingAssembly                                           ' changed
                Using ios As Stream = asm.GetManifestResourceStream(String.Format("DebtPlus.Reports.{0}", ReportName))              ' changed
                    If ios IsNot Nothing Then LoadLayout(ios) '                                                                       changed
                End Using
            End If

            ' Set the script references as needed
            ScriptReferences = DebtPlus.Utils.AssemblyTools.GetAllSiblingAssemblies()

            ' Copy the script references to the sub-reports as well
            For Each Band As Band In Bands
                For Each ctl As XRControl In Band.Controls
                    Dim rpt As XRSubreport = TryCast(ctl, XRSubreport)
                    If rpt IsNot Nothing Then
                        Dim SubRpt As XtraReport = TryCast(rpt.ReportSource, XtraReport)
                        If SubRpt IsNot Nothing Then
                            SubRpt.ScriptReferences = DebtPlus.Utils.AssemblyTools.GetAllSiblingAssemblies()
                        End If
                    End If
                Next
            Next

            ' Ensure that the items are initialized as "empty"
            Parameter_SortOrder = -1
        End Sub


        ''' <summary>
        ''' Report Title
        ''' </summary>
        Public Overrides ReadOnly Property ReportTitle() As String
            Get
                Return "Invoice Aging"
            End Get
        End Property


        ''' <summary>
        ''' Specific report parameter for the sort order
        ''' </summary>
        Public Property Parameter_SortOrder() As System.Int32
            Get
                Return Convert.ToInt32(Parameters("ParameterSortOrder").Value)
            End Get
            Set(ByVal value As System.Int32)
                Select Case value
                    Case 0, 1, 2, 3, -1
                        Parameters("ParameterSortOrder").Value = value
                    Case Else
                        Throw New System.ArgumentOutOfRangeException("Parameter_SortOrder", "Value must be 0, 1, 2, or 3")
                End Select
            End Set
        End Property


        ''' <summary>
        ''' Do we need to ask for paraemters before running the report?
        ''' </summary>
        Public Overrides Function NeedParameters() As Boolean
            Return MyBase.NeedParameters() OrElse (Parameter_SortOrder < 0)
        End Function


        ''' <summary>
        ''' Common interface to set a parameter
        ''' </summary>
        Public Overrides Sub SetReportParameter(ByVal Parameter As String, ByVal Value As Object)
            Select Case Parameter
                Case "SortOrder"
                    Parameter_SortOrder = Convert.ToInt32(Value)
                Case Else
                    MyBase.SetReportParameter(Parameter, Value)
            End Select
        End Sub


        ''' <summary>
        ''' Obtain the report parameters when they are not supplied
        ''' </summary>
        Public Overrides Function RequestReportParameters() As System.Windows.Forms.DialogResult
            Dim answer As System.Windows.Forms.DialogResult = DialogResult.OK
            If NeedParameters() Then
                Using frm As New CreditorInvoiceAgingReport_ParametersForm()
                    answer = frm.ShowDialog()
                    Parameter_SortOrder = frm.Parameter_SortOrder
                    Parameter_FromDate = frm.Parameter_FromDate
                    Parameter_ToDate = frm.Parameter_ToDate
                End Using
            End If
            Return answer
        End Function
    End Class
End Namespace
