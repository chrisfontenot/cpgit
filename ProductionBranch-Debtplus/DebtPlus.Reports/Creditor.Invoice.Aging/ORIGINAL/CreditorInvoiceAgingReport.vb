#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}
#End Region

Option Compare Binary
Option Explicit On 
Option Strict On

Namespace Creditor.Invoice.Aging
    Public Class CreditorInvoiceAgingReport
        Inherits DebtPlus.Reports.Template.DatedTemplateXtraReportClass

        ''' <summary>
        '''     Create an instance of our report
        ''' </summary>
        Public Sub New()
            MyBase.New()
            InitializeComponent()

            AddHandler BeforePrint, AddressOf CreditorInvoiceAgingReport_BeforePrint
            Parameter_SortOrder = -1
        End Sub

        ''' <summary>
        '''     Report Title
        ''' </summary>
        Public Overrides ReadOnly Property ReportTitle() As String
            Get
                Return "Invoice Aging"
            End Get
        End Property

        ''' <summary>
        '''     Specific report parameter for the sort order
        ''' </summary>
        Public Property Parameter_SortOrder() As System.Int32
            Get
                Dim parm As DevExpress.XtraReports.Parameters.Parameter = FindParameter("ParameterSortOrder")
                If parm Is Nothing Then Return 0
                Return Convert.ToInt32(parm.Value)
            End Get
            Set(ByVal value As System.Int32)
                Select Case value
                    Case 0, 1, 2, 3, -1
                        SetParameter("ParameterSortOrder", GetType(Int32), value, "Sort Order", False)
                    Case Else
                        Throw New System.ArgumentOutOfRangeException("Parameter_SortOrder", "Value must be 0, 1, 2, or 3")
                End Select
            End Set
        End Property

        ''' <summary>
        '''     Do we need to ask for parameters before running the report?
        ''' </summary>
        Public Overrides Function NeedParameters() As Boolean
            Return MyBase.NeedParameters() OrElse (Parameter_SortOrder < 0)
        End Function

        ''' <summary>
        '''     Common interface to set a parameter
        ''' </summary>
        Public Overrides Sub SetReportParameter(ByVal Parameter As String, ByVal Value As Object)
            Select Case Parameter
                Case "SortOrder"
                    Parameter_SortOrder = Convert.ToInt32(Value)
                Case Else
                    MyBase.SetReportParameter(Parameter, Value)
            End Select
        End Sub

        ''' <summary>
        '''     Obtain the report parameters when they are not supplied
        ''' </summary>
        Public Overrides Function RequestReportParameters() As System.Windows.Forms.DialogResult
            Dim answer As System.Windows.Forms.DialogResult = DialogResult.OK
            If NeedParameters() Then
                Using frm As New CreditorInvoiceAgingReport_ParametersForm()
                    answer = frm.ShowDialog()
                    Parameter_SortOrder = frm.Parameter_SortOrder
                    Parameter_FromDate = frm.Parameter_FromDate
                    Parameter_ToDate = frm.Parameter_ToDate
                End Using
            End If
            Return answer
        End Function

        ''' <summary>
        '''     Load the report on the first instance
        ''' </summary>
        Dim ds As New System.Data.DataSet("ds")
        Private Sub CreditorInvoiceAgingReport_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs)
            Dim rpt As DevExpress.XtraReports.UI.XtraReport = CType(sender, DevExpress.XtraReports.UI.XtraReport)
            Dim sqlInfo As DebtPlus.LINQ.SQLInfoClass = DebtPlus.LINQ.SQLInfoClass.getDefault()
            Const TableName As String = "rpt_ContributionAging"

            Try
                Using cn As System.Data.SqlClient.SqlConnection = New System.Data.SqlClient.SqlConnection(sqlInfo.ConnectionString)
                    cn.Open()
                    Using cmd As New System.Data.SqlClient.SqlCommand()
                        cmd.Connection = cn
                        cmd.CommandText = "rpt_ContributionAging"
                        cmd.CommandType = System.Data.CommandType.StoredProcedure
                        System.Data.SqlClient.SqlCommandBuilder.DeriveParameters(cmd)

                        cmd.Parameters(1).Value = rpt.Parameters("ParameterFromDate").Value
                        cmd.Parameters(2).Value = rpt.Parameters("ParameterToDate").Value
                        cmd.CommandTimeout = 0

                        Using da As New System.Data.SqlClient.SqlDataAdapter(cmd)
                            da.Fill(ds, TableName)
                            Dim tbl As System.Data.DataTable = ds.Tables(TableName)
                            tbl.Columns.Add("total", GetType(Decimal), "[net_current]+[net_30]+[net_60]+[net_90]+[net_120]")

                            '-- Sort the data according to the desired order
                            Dim vue As System.Data.DataView
                            Select Case Convert.ToInt32(rpt.Parameters("ParameterSortOrder").Value)
                                Case 0
                                    vue = New System.Data.DataView(tbl, String.Empty, "[creditor]", System.Data.DataViewRowState.CurrentRows)
                                Case 1
                                    vue = New System.Data.DataView(tbl, String.Empty, "[creditor_name], [creditor]", System.Data.DataViewRowState.CurrentRows)
                                Case 3
                                    vue = New System.Data.DataView(tbl, String.Empty, "[contrib_cycle], [creditor]", System.Data.DataViewRowState.CurrentRows)
                                Case Else
                                    vue = New System.Data.DataView(tbl, String.Empty, "[total] desc, [creditor]", System.Data.DataViewRowState.CurrentRows)
                            End Select

                            '-- Set the data source for the report
                            rpt.DataSource = vue
                        End Using
                    End Using
                End Using

            Catch ex As Exception
                Using gdr As New DebtPlus.Repository.GetDataResult()
                    gdr.HandleException(ex)
                    DebtPlus.UI.Common.ErrorHandling.HandleErrors(gdr, "Error reading report information")
                End Using
            End Try
        End Sub
    End Class
End Namespace
