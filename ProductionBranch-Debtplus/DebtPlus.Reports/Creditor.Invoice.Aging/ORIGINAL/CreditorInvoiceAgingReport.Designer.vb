Namespace Creditor.Invoice.Aging
    <Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
    Partial Class CreditorInvoiceAgingReport

        'XtraReport overrides dispose to clean up the component list.
        <System.Diagnostics.DebuggerNonUserCode()> _
        Protected Overrides Sub Dispose(ByVal disposing As Boolean)
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
            MyBase.Dispose(disposing)
        End Sub

        'Required by the Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Designer
        'It can be modified using the Designer.  
        'Do not modify it using the code editor.
        <System.Diagnostics.DebuggerStepThrough()> _
        Private Sub InitializeComponent()
            Dim XrSummary1 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary()
            Dim XrSummary2 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary()
            Dim XrSummary3 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary()
            Dim XrSummary4 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary()
            Dim XrSummary5 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary()
            Dim XrSummary6 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary()
            Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(CreditorInvoiceAgingReport))
            Me.XrPanel1 = New DevExpress.XtraReports.UI.XRPanel()
            Me.XrLabel10 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel8 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel7 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel6 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel5 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel4 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel3 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel2 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel1 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_60 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_contact = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_phone = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_current = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_30 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_creditor = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_90 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_120 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_total = New DevExpress.XtraReports.UI.XRLabel()
            Me.ReportFooter = New DevExpress.XtraReports.UI.ReportFooterBand()
            Me.XrLabel15 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel14 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel13 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel12 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel11 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_grand_total = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_total_120 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_total_90 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_total_60 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_total_30 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLine1 = New DevExpress.XtraReports.UI.XRLine()
            Me.XrLabel_total_current = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel9 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_contrib_cycle = New DevExpress.XtraReports.UI.XRLabel()
            Me.ParameterSortOrder = New DevExpress.XtraReports.Parameters.Parameter()
            CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
            '
            'PageHeader
            '
            Me.PageHeader.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrPanel1})
            Me.PageHeader.HeightF = 154.0!
            Me.PageHeader.Controls.SetChildIndex(Me.XrPanel1, 0)
            Me.PageHeader.Controls.SetChildIndex(Me.XrPanel_AgencyAddress, 0)
            Me.PageHeader.Controls.SetChildIndex(Me.XrLabel_Title, 0)
            Me.PageHeader.Controls.SetChildIndex(Me.XrLine_Title, 0)
            Me.PageHeader.Controls.SetChildIndex(Me.XrLabel_Subtitle, 0)
            Me.PageHeader.Controls.SetChildIndex(Me.XrPageInfo1, 0)
            '
            'XrLabel_Title
            '
            Me.XrLabel_Title.StylePriority.UseFont = False
            '
            'XrPageInfo_PageNumber
            '
            Me.XrPageInfo_PageNumber.LocationFloat = New DevExpress.Utils.PointFloat(917.0!, 8.0!)
            Me.XrPageInfo_PageNumber.StylePriority.UseTextAlignment = False
            '
            'XrPanel_AgencyAddress
            '
            Me.XrPanel_AgencyAddress.LocationFloat = New DevExpress.Utils.PointFloat(742.0!, 0.0!)
            '
            'XRLabel_Agency_Name
            '
            Me.XRLabel_Agency_Name.StylePriority.UseTextAlignment = False
            '
            'XrLabel_Agency_Address3
            '
            Me.XrLabel_Agency_Address3.StylePriority.UseFont = False
            Me.XrLabel_Agency_Address3.StylePriority.UseTextAlignment = False
            '
            'XrLabel_Agency_Address1
            '
            Me.XrLabel_Agency_Address1.StylePriority.UseFont = False
            Me.XrLabel_Agency_Address1.StylePriority.UseTextAlignment = False
            '
            'XrLabel_Agency_Phone
            '
            Me.XrLabel_Agency_Phone.StylePriority.UseFont = False
            Me.XrLabel_Agency_Phone.StylePriority.UseTextAlignment = False
            '
            'XrLabel_Agency_Address2
            '
            Me.XrLabel_Agency_Address2.StylePriority.UseFont = False
            Me.XrLabel_Agency_Address2.StylePriority.UseTextAlignment = False
            '
            'Detail
            '
            Me.Detail.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel_contrib_cycle, Me.XrLabel_total, Me.XrLabel_120, Me.XrLabel_90, Me.XrLabel_creditor, Me.XrLabel_30, Me.XrLabel_current, Me.XrLabel_phone, Me.XrLabel_contact, Me.XrLabel_60})
            Me.Detail.HeightF = 15.0!
            '
            'XrPanel1
            '
            Me.XrPanel1.BackColor = System.Drawing.Color.Teal
            Me.XrPanel1.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel10, Me.XrLabel8, Me.XrLabel7, Me.XrLabel6, Me.XrLabel5, Me.XrLabel4, Me.XrLabel3, Me.XrLabel2, Me.XrLabel1})
            Me.XrPanel1.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 133.0!)
            Me.XrPanel1.Name = "XrPanel1"
            Me.XrPanel1.SizeF = New System.Drawing.SizeF(1050.0!, 17.0!)
            Me.XrPanel1.StylePriority.UseBackColor = False
            '
            'XrLabel10
            '
            Me.XrLabel10.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel10.ForeColor = System.Drawing.Color.White
            Me.XrLabel10.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 1.0!)
            Me.XrLabel10.Name = "XrLabel10"
            Me.XrLabel10.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel10.SizeF = New System.Drawing.SizeF(300.0!, 15.0!)
            Me.XrLabel10.StylePriority.UseFont = False
            Me.XrLabel10.StylePriority.UseForeColor = False
            Me.XrLabel10.StylePriority.UseTextAlignment = False
            Me.XrLabel10.Text = "CREDITOR"
            Me.XrLabel10.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
            '
            'XrLabel8
            '
            Me.XrLabel8.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel8.ForeColor = System.Drawing.Color.White
            Me.XrLabel8.LocationFloat = New DevExpress.Utils.PointFloat(307.0!, 1.0!)
            Me.XrLabel8.Name = "XrLabel8"
            Me.XrLabel8.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel8.SizeF = New System.Drawing.SizeF(117.0!, 15.0!)
            Me.XrLabel8.StylePriority.UseFont = False
            Me.XrLabel8.StylePriority.UseForeColor = False
            Me.XrLabel8.StylePriority.UseTextAlignment = False
            Me.XrLabel8.Text = "CONTACT INFO"
            Me.XrLabel8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
            '
            'XrLabel7
            '
            Me.XrLabel7.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel7.ForeColor = System.Drawing.Color.White
            Me.XrLabel7.LocationFloat = New DevExpress.Utils.PointFloat(608.0!, 1.0!)
            Me.XrLabel7.Name = "XrLabel7"
            Me.XrLabel7.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel7.SizeF = New System.Drawing.SizeF(71.0!, 15.0!)
            Me.XrLabel7.StylePriority.UseFont = False
            Me.XrLabel7.StylePriority.UseForeColor = False
            Me.XrLabel7.StylePriority.UseTextAlignment = False
            Me.XrLabel7.Text = "CURRENT"
            Me.XrLabel7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
            '
            'XrLabel6
            '
            Me.XrLabel6.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel6.ForeColor = System.Drawing.Color.White
            Me.XrLabel6.LocationFloat = New DevExpress.Utils.PointFloat(975.0!, 1.0!)
            Me.XrLabel6.Name = "XrLabel6"
            Me.XrLabel6.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel6.SizeF = New System.Drawing.SizeF(70.0!, 15.0!)
            Me.XrLabel6.StylePriority.UseFont = False
            Me.XrLabel6.StylePriority.UseForeColor = False
            Me.XrLabel6.StylePriority.UseTextAlignment = False
            Me.XrLabel6.Text = "TOTAL"
            Me.XrLabel6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
            '
            'XrLabel5
            '
            Me.XrLabel5.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel5.ForeColor = System.Drawing.Color.White
            Me.XrLabel5.LocationFloat = New DevExpress.Utils.PointFloat(903.0!, 1.0!)
            Me.XrLabel5.Name = "XrLabel5"
            Me.XrLabel5.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel5.SizeF = New System.Drawing.SizeF(61.0!, 15.0!)
            Me.XrLabel5.StylePriority.UseFont = False
            Me.XrLabel5.StylePriority.UseForeColor = False
            Me.XrLabel5.StylePriority.UseTextAlignment = False
            Me.XrLabel5.Text = "120+"
            Me.XrLabel5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
            '
            'XrLabel4
            '
            Me.XrLabel4.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel4.ForeColor = System.Drawing.Color.White
            Me.XrLabel4.LocationFloat = New DevExpress.Utils.PointFloat(833.0!, 1.0!)
            Me.XrLabel4.Name = "XrLabel4"
            Me.XrLabel4.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel4.SizeF = New System.Drawing.SizeF(54.0!, 15.0!)
            Me.XrLabel4.StylePriority.UseFont = False
            Me.XrLabel4.StylePriority.UseForeColor = False
            Me.XrLabel4.StylePriority.UseTextAlignment = False
            Me.XrLabel4.Text = "90"
            Me.XrLabel4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
            '
            'XrLabel3
            '
            Me.XrLabel3.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel3.ForeColor = System.Drawing.Color.White
            Me.XrLabel3.LocationFloat = New DevExpress.Utils.PointFloat(766.0!, 1.0!)
            Me.XrLabel3.Name = "XrLabel3"
            Me.XrLabel3.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel3.SizeF = New System.Drawing.SizeF(54.0!, 15.0!)
            Me.XrLabel3.StylePriority.UseFont = False
            Me.XrLabel3.StylePriority.UseForeColor = False
            Me.XrLabel3.StylePriority.UseTextAlignment = False
            Me.XrLabel3.Text = "60"
            Me.XrLabel3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
            '
            'XrLabel2
            '
            Me.XrLabel2.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel2.ForeColor = System.Drawing.Color.White
            Me.XrLabel2.LocationFloat = New DevExpress.Utils.PointFloat(702.0!, 1.0!)
            Me.XrLabel2.Name = "XrLabel2"
            Me.XrLabel2.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel2.SizeF = New System.Drawing.SizeF(54.0!, 15.0!)
            Me.XrLabel2.StylePriority.UseFont = False
            Me.XrLabel2.StylePriority.UseForeColor = False
            Me.XrLabel2.StylePriority.UseTextAlignment = False
            Me.XrLabel2.Text = "30"
            Me.XrLabel2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
            '
            'XrLabel1
            '
            Me.XrLabel1.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel1.ForeColor = System.Drawing.Color.White
            Me.XrLabel1.LocationFloat = New DevExpress.Utils.PointFloat(500.0!, 1.0!)
            Me.XrLabel1.Name = "XrLabel1"
            Me.XrLabel1.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel1.SizeF = New System.Drawing.SizeF(105.0!, 15.0!)
            Me.XrLabel1.StylePriority.UseFont = False
            Me.XrLabel1.StylePriority.UseForeColor = False
            Me.XrLabel1.StylePriority.UseTextAlignment = False
            Me.XrLabel1.Text = "PHONE"
            Me.XrLabel1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
            '
            'XrLabel_60
            '
            Me.XrLabel_60.CanGrow = False
            Me.XrLabel_60.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "net_60", "{0:c}")})
            Me.XrLabel_60.Font = New System.Drawing.Font("Times New Roman", 9.75!)
            Me.XrLabel_60.ForeColor = System.Drawing.SystemColors.WindowText
            Me.XrLabel_60.LocationFloat = New DevExpress.Utils.PointFloat(758.0!, 0.0!)
            Me.XrLabel_60.Name = "XrLabel_60"
            Me.XrLabel_60.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_60.SizeF = New System.Drawing.SizeF(62.0!, 15.0!)
            Me.XrLabel_60.StylePriority.UseFont = False
            Me.XrLabel_60.StylePriority.UseForeColor = False
            Me.XrLabel_60.StylePriority.UseTextAlignment = False
            Me.XrLabel_60.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
            '
            'XrLabel_contact
            '
            Me.XrLabel_contact.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "contact")})
            Me.XrLabel_contact.Font = New System.Drawing.Font("Times New Roman", 9.75!)
            Me.XrLabel_contact.ForeColor = System.Drawing.SystemColors.WindowText
            Me.XrLabel_contact.LocationFloat = New DevExpress.Utils.PointFloat(308.0!, 0.0!)
            Me.XrLabel_contact.Name = "XrLabel_contact"
            Me.XrLabel_contact.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_contact.SizeF = New System.Drawing.SizeF(192.0!, 15.0!)
            Me.XrLabel_contact.StylePriority.UseFont = False
            Me.XrLabel_contact.StylePriority.UseForeColor = False
            Me.XrLabel_contact.StylePriority.UseTextAlignment = False
            Me.XrLabel_contact.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
            '
            'XrLabel_phone
            '
            Me.XrLabel_phone.Font = New System.Drawing.Font("Times New Roman", 9.75!)
            Me.XrLabel_phone.ForeColor = System.Drawing.SystemColors.WindowText
            Me.XrLabel_phone.LocationFloat = New DevExpress.Utils.PointFloat(500.0!, 0.0!)
            Me.XrLabel_phone.Name = "XrLabel_phone"
            Me.XrLabel_phone.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_phone.SizeF = New System.Drawing.SizeF(105.0!, 15.0!)
            Me.XrLabel_phone.StylePriority.UseFont = False
            Me.XrLabel_phone.StylePriority.UseForeColor = False
            Me.XrLabel_phone.StylePriority.UseTextAlignment = False
            Me.XrLabel_phone.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
            Me.XrLabel_phone.WordWrap = False
            '
            'XrLabel_current
            '
            Me.XrLabel_current.CanGrow = False
            Me.XrLabel_current.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "net_current", "{0:c}")})
            Me.XrLabel_current.Font = New System.Drawing.Font("Times New Roman", 9.75!)
            Me.XrLabel_current.ForeColor = System.Drawing.SystemColors.WindowText
            Me.XrLabel_current.LocationFloat = New DevExpress.Utils.PointFloat(608.0!, 0.0!)
            Me.XrLabel_current.Name = "XrLabel_current"
            Me.XrLabel_current.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_current.SizeF = New System.Drawing.SizeF(71.0!, 15.0!)
            Me.XrLabel_current.StylePriority.UseFont = False
            Me.XrLabel_current.StylePriority.UseForeColor = False
            Me.XrLabel_current.StylePriority.UseTextAlignment = False
            Me.XrLabel_current.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
            '
            'XrLabel_30
            '
            Me.XrLabel_30.CanGrow = False
            Me.XrLabel_30.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "net_30", "{0:c}")})
            Me.XrLabel_30.Font = New System.Drawing.Font("Times New Roman", 9.75!)
            Me.XrLabel_30.ForeColor = System.Drawing.SystemColors.WindowText
            Me.XrLabel_30.LocationFloat = New DevExpress.Utils.PointFloat(685.0!, 0.0!)
            Me.XrLabel_30.Name = "XrLabel_30"
            Me.XrLabel_30.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_30.SizeF = New System.Drawing.SizeF(71.0!, 15.0!)
            Me.XrLabel_30.StylePriority.UseFont = False
            Me.XrLabel_30.StylePriority.UseForeColor = False
            Me.XrLabel_30.StylePriority.UseTextAlignment = False
            Me.XrLabel_30.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
            '
            'XrLabel_creditor
            '
            Me.XrLabel_creditor.Font = New System.Drawing.Font("Times New Roman", 9.75!)
            Me.XrLabel_creditor.ForeColor = System.Drawing.SystemColors.WindowText
            Me.XrLabel_creditor.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
            Me.XrLabel_creditor.Name = "XrLabel_creditor"
            Me.XrLabel_creditor.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_creditor.SizeF = New System.Drawing.SizeF(283.0!, 15.0!)
            Me.XrLabel_creditor.StylePriority.UseFont = False
            Me.XrLabel_creditor.StylePriority.UseForeColor = False
            Me.XrLabel_creditor.StylePriority.UseTextAlignment = False
            Me.XrLabel_creditor.Text = "[[creditor]] [creditor_name]"
            Me.XrLabel_creditor.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
            Me.XrLabel_creditor.WordWrap = False
            '
            'XrLabel_90
            '
            Me.XrLabel_90.CanGrow = False
            Me.XrLabel_90.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "net_90", "{0:c}")})
            Me.XrLabel_90.Font = New System.Drawing.Font("Times New Roman", 9.75!)
            Me.XrLabel_90.ForeColor = System.Drawing.SystemColors.WindowText
            Me.XrLabel_90.LocationFloat = New DevExpress.Utils.PointFloat(825.0!, 0.0!)
            Me.XrLabel_90.Name = "XrLabel_90"
            Me.XrLabel_90.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_90.SizeF = New System.Drawing.SizeF(62.0!, 15.0!)
            Me.XrLabel_90.StylePriority.UseFont = False
            Me.XrLabel_90.StylePriority.UseForeColor = False
            Me.XrLabel_90.StylePriority.UseTextAlignment = False
            Me.XrLabel_90.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
            '
            'XrLabel_120
            '
            Me.XrLabel_120.CanGrow = False
            Me.XrLabel_120.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "net_120", "{0:c}")})
            Me.XrLabel_120.Font = New System.Drawing.Font("Times New Roman", 9.75!)
            Me.XrLabel_120.ForeColor = System.Drawing.SystemColors.WindowText
            Me.XrLabel_120.LocationFloat = New DevExpress.Utils.PointFloat(894.0!, 0.0!)
            Me.XrLabel_120.Name = "XrLabel_120"
            Me.XrLabel_120.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_120.SizeF = New System.Drawing.SizeF(70.0!, 15.0!)
            Me.XrLabel_120.StylePriority.UseFont = False
            Me.XrLabel_120.StylePriority.UseForeColor = False
            Me.XrLabel_120.StylePriority.UseTextAlignment = False
            Me.XrLabel_120.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
            '
            'XrLabel_total
            '
            Me.XrLabel_total.CanGrow = False
            Me.XrLabel_total.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "total", "{0:c}")})
            Me.XrLabel_total.Font = New System.Drawing.Font("Times New Roman", 9.75!)
            Me.XrLabel_total.ForeColor = System.Drawing.SystemColors.WindowText
            Me.XrLabel_total.LocationFloat = New DevExpress.Utils.PointFloat(967.0!, 0.0!)
            Me.XrLabel_total.Name = "XrLabel_total"
            Me.XrLabel_total.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_total.SizeF = New System.Drawing.SizeF(78.0!, 15.0!)
            Me.XrLabel_total.StylePriority.UseFont = False
            Me.XrLabel_total.StylePriority.UseForeColor = False
            Me.XrLabel_total.StylePriority.UseTextAlignment = False
            Me.XrLabel_total.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
            '
            'ReportFooter
            '
            Me.ReportFooter.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel15, Me.XrLabel14, Me.XrLabel13, Me.XrLabel12, Me.XrLabel11, Me.XrLabel_grand_total, Me.XrLabel_total_120, Me.XrLabel_total_90, Me.XrLabel_total_60, Me.XrLabel_total_30, Me.XrLine1, Me.XrLabel_total_current, Me.XrLabel9})
            Me.ReportFooter.HeightF = 128.0!
            Me.ReportFooter.KeepTogether = True
            Me.ReportFooter.Name = "ReportFooter"
            Me.ReportFooter.PrintAtBottom = True
            '
            'XrLabel15
            '
            Me.XrLabel15.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel15.ForeColor = System.Drawing.Color.Teal
            Me.XrLabel15.LocationFloat = New DevExpress.Utils.PointFloat(217.0!, 102.0!)
            Me.XrLabel15.Name = "XrLabel15"
            Me.XrLabel15.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel15.SizeF = New System.Drawing.SizeF(267.0!, 17.0!)
            Me.XrLabel15.StylePriority.UseFont = False
            Me.XrLabel15.StylePriority.UseForeColor = False
            Me.XrLabel15.Text = "GRAND TOTAL"
            '
            'XrLabel14
            '
            Me.XrLabel14.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel14.ForeColor = System.Drawing.Color.Teal
            Me.XrLabel14.LocationFloat = New DevExpress.Utils.PointFloat(217.0!, 85.0!)
            Me.XrLabel14.Name = "XrLabel14"
            Me.XrLabel14.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel14.SizeF = New System.Drawing.SizeF(267.0!, 17.0!)
            Me.XrLabel14.StylePriority.UseFont = False
            Me.XrLabel14.StylePriority.UseForeColor = False
            Me.XrLabel14.Text = "TOTAL 120+ DAYS"
            '
            'XrLabel13
            '
            Me.XrLabel13.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel13.ForeColor = System.Drawing.Color.Teal
            Me.XrLabel13.LocationFloat = New DevExpress.Utils.PointFloat(217.0!, 68.0!)
            Me.XrLabel13.Name = "XrLabel13"
            Me.XrLabel13.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel13.SizeF = New System.Drawing.SizeF(267.0!, 17.0!)
            Me.XrLabel13.StylePriority.UseFont = False
            Me.XrLabel13.StylePriority.UseForeColor = False
            Me.XrLabel13.Text = "TOTAL 90 DAYS"
            '
            'XrLabel12
            '
            Me.XrLabel12.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel12.ForeColor = System.Drawing.Color.Teal
            Me.XrLabel12.LocationFloat = New DevExpress.Utils.PointFloat(217.0!, 51.0!)
            Me.XrLabel12.Name = "XrLabel12"
            Me.XrLabel12.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel12.SizeF = New System.Drawing.SizeF(267.0!, 17.0!)
            Me.XrLabel12.StylePriority.UseFont = False
            Me.XrLabel12.StylePriority.UseForeColor = False
            Me.XrLabel12.Text = "TOTAL 60 DAYS"
            '
            'XrLabel11
            '
            Me.XrLabel11.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel11.ForeColor = System.Drawing.Color.Teal
            Me.XrLabel11.LocationFloat = New DevExpress.Utils.PointFloat(217.0!, 34.0!)
            Me.XrLabel11.Name = "XrLabel11"
            Me.XrLabel11.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel11.SizeF = New System.Drawing.SizeF(267.0!, 17.0!)
            Me.XrLabel11.StylePriority.UseFont = False
            Me.XrLabel11.StylePriority.UseForeColor = False
            Me.XrLabel11.Text = "TOTAL 30 DAYS"
            '
            'XrLabel_grand_total
            '
            Me.XrLabel_grand_total.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "total", "{0:c}")})
            Me.XrLabel_grand_total.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel_grand_total.ForeColor = System.Drawing.Color.Teal
            Me.XrLabel_grand_total.LocationFloat = New DevExpress.Utils.PointFloat(492.0!, 102.0!)
            Me.XrLabel_grand_total.Name = "XrLabel_grand_total"
            Me.XrLabel_grand_total.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_grand_total.SizeF = New System.Drawing.SizeF(192.0!, 17.0!)
            Me.XrLabel_grand_total.StylePriority.UseFont = False
            Me.XrLabel_grand_total.StylePriority.UseForeColor = False
            Me.XrLabel_grand_total.StylePriority.UseTextAlignment = False
            XrSummary1.FormatString = "{0:c}"
            XrSummary1.Running = DevExpress.XtraReports.UI.SummaryRunning.Report
            Me.XrLabel_grand_total.Summary = XrSummary1
            Me.XrLabel_grand_total.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
            '
            'XrLabel_total_120
            '
            Me.XrLabel_total_120.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "net_120", "{0:c}")})
            Me.XrLabel_total_120.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel_total_120.ForeColor = System.Drawing.Color.Teal
            Me.XrLabel_total_120.LocationFloat = New DevExpress.Utils.PointFloat(492.0!, 85.0!)
            Me.XrLabel_total_120.Name = "XrLabel_total_120"
            Me.XrLabel_total_120.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_total_120.SizeF = New System.Drawing.SizeF(192.0!, 17.0!)
            Me.XrLabel_total_120.StylePriority.UseFont = False
            Me.XrLabel_total_120.StylePriority.UseForeColor = False
            Me.XrLabel_total_120.StylePriority.UseTextAlignment = False
            XrSummary2.FormatString = "{0:c}"
            XrSummary2.Running = DevExpress.XtraReports.UI.SummaryRunning.Report
            Me.XrLabel_total_120.Summary = XrSummary2
            Me.XrLabel_total_120.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
            '
            'XrLabel_total_90
            '
            Me.XrLabel_total_90.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "net_90", "{0:c}")})
            Me.XrLabel_total_90.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel_total_90.ForeColor = System.Drawing.Color.Teal
            Me.XrLabel_total_90.LocationFloat = New DevExpress.Utils.PointFloat(492.0!, 68.0!)
            Me.XrLabel_total_90.Name = "XrLabel_total_90"
            Me.XrLabel_total_90.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_total_90.SizeF = New System.Drawing.SizeF(192.0!, 17.0!)
            Me.XrLabel_total_90.StylePriority.UseFont = False
            Me.XrLabel_total_90.StylePriority.UseForeColor = False
            Me.XrLabel_total_90.StylePriority.UseTextAlignment = False
            XrSummary3.FormatString = "{0:c}"
            XrSummary3.Running = DevExpress.XtraReports.UI.SummaryRunning.Report
            Me.XrLabel_total_90.Summary = XrSummary3
            Me.XrLabel_total_90.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
            '
            'XrLabel_total_60
            '
            Me.XrLabel_total_60.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "net_60", "{0:c}")})
            Me.XrLabel_total_60.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel_total_60.ForeColor = System.Drawing.Color.Teal
            Me.XrLabel_total_60.LocationFloat = New DevExpress.Utils.PointFloat(492.0!, 51.0!)
            Me.XrLabel_total_60.Name = "XrLabel_total_60"
            Me.XrLabel_total_60.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_total_60.SizeF = New System.Drawing.SizeF(192.0!, 17.0!)
            Me.XrLabel_total_60.StylePriority.UseFont = False
            Me.XrLabel_total_60.StylePriority.UseForeColor = False
            Me.XrLabel_total_60.StylePriority.UseTextAlignment = False
            XrSummary4.FormatString = "{0:c}"
            XrSummary4.Running = DevExpress.XtraReports.UI.SummaryRunning.Report
            Me.XrLabel_total_60.Summary = XrSummary4
            Me.XrLabel_total_60.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
            '
            'XrLabel_total_30
            '
            Me.XrLabel_total_30.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "net_30", "{0:c}")})
            Me.XrLabel_total_30.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel_total_30.ForeColor = System.Drawing.Color.Teal
            Me.XrLabel_total_30.LocationFloat = New DevExpress.Utils.PointFloat(492.0!, 34.0!)
            Me.XrLabel_total_30.Name = "XrLabel_total_30"
            Me.XrLabel_total_30.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_total_30.SizeF = New System.Drawing.SizeF(192.0!, 17.0!)
            Me.XrLabel_total_30.StylePriority.UseFont = False
            Me.XrLabel_total_30.StylePriority.UseForeColor = False
            Me.XrLabel_total_30.StylePriority.UseTextAlignment = False
            XrSummary5.FormatString = "{0:c}"
            XrSummary5.Running = DevExpress.XtraReports.UI.SummaryRunning.Report
            Me.XrLabel_total_30.Summary = XrSummary5
            Me.XrLabel_total_30.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
            '
            'XrLine1
            '
            Me.XrLine1.ForeColor = System.Drawing.Color.Teal
            Me.XrLine1.LineWidth = 4
            Me.XrLine1.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
            Me.XrLine1.Name = "XrLine1"
            Me.XrLine1.SizeF = New System.Drawing.SizeF(1050.0!, 9.0!)
            Me.XrLine1.StylePriority.UseForeColor = False
            '
            'XrLabel_total_current
            '
            Me.XrLabel_total_current.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "net_current", "{0:c}")})
            Me.XrLabel_total_current.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel_total_current.ForeColor = System.Drawing.Color.Teal
            Me.XrLabel_total_current.LocationFloat = New DevExpress.Utils.PointFloat(492.0!, 17.0!)
            Me.XrLabel_total_current.Name = "XrLabel_total_current"
            Me.XrLabel_total_current.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_total_current.SizeF = New System.Drawing.SizeF(192.0!, 17.0!)
            Me.XrLabel_total_current.StylePriority.UseFont = False
            Me.XrLabel_total_current.StylePriority.UseForeColor = False
            Me.XrLabel_total_current.StylePriority.UseTextAlignment = False
            XrSummary6.FormatString = "{0:c}"
            XrSummary6.Running = DevExpress.XtraReports.UI.SummaryRunning.Report
            Me.XrLabel_total_current.Summary = XrSummary6
            Me.XrLabel_total_current.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
            '
            'XrLabel9
            '
            Me.XrLabel9.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel9.ForeColor = System.Drawing.Color.Teal
            Me.XrLabel9.LocationFloat = New DevExpress.Utils.PointFloat(217.0!, 17.0!)
            Me.XrLabel9.Name = "XrLabel9"
            Me.XrLabel9.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel9.SizeF = New System.Drawing.SizeF(267.0!, 17.0!)
            Me.XrLabel9.StylePriority.UseFont = False
            Me.XrLabel9.StylePriority.UseForeColor = False
            Me.XrLabel9.Text = "TOTAL CURRENT"
            '
            'XrLabel_contrib_cycle
            '
            Me.XrLabel_contrib_cycle.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "contrib_cycle")})
            Me.XrLabel_contrib_cycle.LocationFloat = New DevExpress.Utils.PointFloat(287.0!, 0.0!)
            Me.XrLabel_contrib_cycle.Name = "XrLabel_contrib_cycle"
            Me.XrLabel_contrib_cycle.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_contrib_cycle.SizeF = New System.Drawing.SizeF(17.0!, 15.0!)
            '
            'ParameterSortOrder
            '
            Me.ParameterSortOrder.Description = "Desired sort order"
            Me.ParameterSortOrder.Name = "ParameterSortOrder"
            Me.ParameterSortOrder.Type = GetType(Integer)
            Me.ParameterSortOrder.Value = -1
            Me.ParameterSortOrder.Visible = False
            '
            'CreditorInvoiceAgingReport
            '
            Me.Bands.AddRange(New DevExpress.XtraReports.UI.Band() {Me.Detail, Me.PageHeader, Me.PageFooter, Me.TopMarginBand1, Me.BottomMarginBand1, Me.ReportFooter})
            Me.Landscape = True
            Me.PageHeight = 850
            Me.PageWidth = 1100
            Me.Parameters.AddRange(New DevExpress.XtraReports.Parameters.Parameter() {Me.ParameterSortOrder})
            Me.ScriptLanguage = DevExpress.XtraReports.ScriptLanguage.VisualBasic
            Me.ScriptReferencesString = resources.GetString("$this.ScriptReferencesString")
            Me.Scripts.OnBeforePrint = "CreditorInvoiceAgingReport_BeforePrint"
            Me.ScriptsSource = resources.GetString("$this.ScriptsSource")
            Me.StyleSheet.AddRange(New DevExpress.XtraReports.UI.XRControlStyle() {Me.XrControlStyle_Header, Me.XrControlStyle_Totals, Me.XrControlStyle_HeaderPannel, Me.XrControlStyle_GroupHeader})
            Me.Version = "11.2"
            Me.Controls.SetChildIndex(Me.ReportFooter, 0)
            Me.Controls.SetChildIndex(Me.BottomMarginBand1, 0)
            Me.Controls.SetChildIndex(Me.TopMarginBand1, 0)
            Me.Controls.SetChildIndex(Me.PageFooter, 0)
            Me.Controls.SetChildIndex(Me.PageHeader, 0)
            Me.Controls.SetChildIndex(Me.Detail, 0)
            CType(Me, System.ComponentModel.ISupportInitialize).EndInit()

        End Sub
        Friend WithEvents XrPanel1 As DevExpress.XtraReports.UI.XRPanel
        Friend WithEvents XrLabel6 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel5 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel4 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel3 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel2 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel1 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel10 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel8 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel7 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_total As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_120 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_90 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_creditor As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_30 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_current As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_phone As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_contact As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_60 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents ReportFooter As DevExpress.XtraReports.UI.ReportFooterBand
        Friend WithEvents XrLine1 As DevExpress.XtraReports.UI.XRLine
        Friend WithEvents XrLabel_total_current As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel9 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_grand_total As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_total_120 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_total_90 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_total_60 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_total_30 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_contrib_cycle As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel15 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel14 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel13 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel12 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel11 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents ParameterSortOrder As DevExpress.XtraReports.Parameters.Parameter
    End Class
End Namespace
