#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}
#End Region
Imports DebtPlus.Reports.Template.Forms


Namespace Creditor.Invoice.Aging
    Friend Class CreditorInvoiceAgingReport_ParametersForm
        Inherits DateReportParametersForm

        Public Sub New()
            MyBase.New()
            InitializeComponent()
            AddHandler Me.Load, AddressOf ParametersForm_Load
        End Sub

        Public ReadOnly Property Parameter_SortOrder() As System.Int32
            Get
                Return Convert.ToInt32(CType(ComboBoxEdit1.SelectedItem, DebtPlus.Data.Controls.ComboboxItem).value)
            End Get
        End Property

        Private Sub ParametersForm_Load(ByVal sender As Object, ByVal e As System.EventArgs)
            With ComboBoxEdit1
                With .Properties
                    With .Items
                        .Clear()
                        .Add(New DebtPlus.Data.Controls.ComboboxItem("Creditor ID, Ascending", 0))
                        .Add(New DebtPlus.Data.Controls.ComboboxItem("Creditor Name, Ascending", 1))
                        .Add(New DebtPlus.Data.Controls.ComboboxItem("Total Amount, Descending", 2))
                        .Add(New DebtPlus.Data.Controls.ComboboxItem("Billing Period, Ascending", 3))
                    End With
                End With
                .SelectedIndex = 0
            End With
        End Sub
    End Class
End Namespace