#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}
#End Region

Option Compare Binary
Option Explicit On 
Option Strict On

Namespace Appointments.ByOffice
    Public Class AppointmentByOffice

        ''' <summary>
        ''' Create the generic report
        ''' </summary>
        Public Sub New()
            MyBase.New()
            InitializeComponent()
            AddHandler BeforePrint, AddressOf Report_BeforePrint
            AddHandler XrSubreportCounselorList.BeforePrint, AddressOf XrSubreportCounselorList_BeforePrint

            '-- Add the select expert
            ReportFilter.IsEnabled = True
        End Sub

        ''' <summary>
        ''' Request the dates and the office value
        ''' </summary>
        Public Overrides Function RequestReportParameters() As System.Windows.Forms.DialogResult
            Dim Answer As System.Windows.Forms.DialogResult = DialogResult.OK
            If NeedParameters() Then
                Using frm As New DebtPlus.Reports.Template.Forms.DatedOfficeParametersForm(True)
                    Answer = frm.ShowDialog
                    Parameter_FromDate = frm.Parameter_FromDate
                    Parameter_ToDate = frm.Parameter_ToDate
                    If frm.Parameter_Office Is Nothing OrElse frm.Parameter_Office Is System.DBNull.Value Then
                        Parameter_Office = -1
                    Else
                        Parameter_Office = Convert.ToInt32(frm.Parameter_Office)
                    End If
                End Using
            End If
            Return MyBase.RequestReportParameters()
        End Function

        ''' <summary>
        ''' Office number
        ''' </summary>
        Public Property Parameter_Office() As Int32
            Get
                Dim parm As DevExpress.XtraReports.Parameters.Parameter = FindParameter("ParameterOffice")
                If parm Is Nothing Then Return -1
                Return Convert.ToInt32(parm.Value)
            End Get
            Set(ByVal value As Int32)
                SetParameter("ParameterOffice", GetType(Int32), value, "Office ID", False)
            End Set
        End Property

        ''' <summary>
        ''' Title associated with the report
        ''' </summary>
        Public Overrides ReadOnly Property ReportTitle() As String
            Get
                Return "Appointments By Office"
            End Get
        End Property

        Dim ds As New System.Data.DataSet("ds")

        Private Sub AppointmentByOffice_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs)
            Const TableName As String = "rpt_PendingAppointments_ByOffice"
            Dim Rpt As DevExpress.XtraReports.UI.XtraReport = CType(sender, DevExpress.XtraReports.UI.XtraReport)
            Dim tbl As System.Data.DataTable = ds.Tables(TableName)

            If tbl Is Nothing Then
                Try
                    Dim sqlInfo As DebtPlus.LINQ.SQLInfoClass = DebtPlus.LINQ.SQLInfoClass.getDefault()
                    Using cn As System.Data.SqlClient.SqlConnection = New System.Data.SqlClient.SqlConnection(sqlInfo.ConnectionString)
                        cn.Open()
                        Using cmd As New System.Data.SqlClient.SqlCommand()
                            cmd.Connection = cn
                            cmd.CommandText = "rpt_PendingAppointments_ByOffice"
                            cmd.CommandType = System.Data.CommandType.StoredProcedure
                            System.Data.SqlClient.SqlCommandBuilder.DeriveParameters(cmd)

                            cmd.Parameters(1).Value = Rpt.Parameters("ParameterFromDate").Value
                            cmd.Parameters(2).Value = Rpt.Parameters("ParameterToDate").Value

                            If cmd.Parameters.Contains("@office") Then
                                If Convert.ToInt32(Rpt.Parameters("ParameterOffice").Value) > 0 Then
                                    cmd.Parameters("@office").Value = Rpt.Parameters("ParameterOffice").Value
                                End If
                            End If

                            cmd.CommandTimeout = 0

                            Using da As New System.Data.SqlClient.SqlDataAdapter(cmd)
                                da.Fill(ds, TableName)
                                tbl = ds.Tables(TableName)

                                Rpt.DataSource = New System.Data.DataView(tbl, CType(Rpt, DebtPlus.Interfaces.Reports.IReportFilter).ReportFilter.ViewFilter, "office_id, time", System.Data.DataViewRowState.CurrentRows)
                                For Each calc As DevExpress.XtraReports.UI.CalculatedField In Rpt.CalculatedFields
                                    calc.Assign(Rpt.DataSource, Rpt.DataMember)
                                Next
                            End Using
                        End Using
                    End Using

                Catch ex As System.Data.SqlClient.SqlException
                    Using gdr As New DebtPlus.Repository.GetDataResult()
                        gdr.HandleException(ex)
                        DebtPlus.UI.Common.ErrorHandling.HandleErrors(gdr, "Error reading report information")
                    End Using
                End Try
            End If
        End Sub

        Private Sub XrSubreportCounselorList_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs)
            Dim SubReport As DevExpress.XtraReports.UI.XRSubreport = CType(sender, DevExpress.XtraReports.UI.XRSubreport)
            Dim Rpt As DevExpress.XtraReports.UI.XtraReport = CType(SubReport.ReportSource, DevExpress.XtraReports.UI.XtraReport)
            Dim MasterRpt As DevExpress.XtraReports.UI.XtraReport = CType(Rpt.MasterReport, DevExpress.XtraReports.UI.XtraReport)

            Dim CurrentRow As System.Data.DataRowView = CType(MasterRpt.GetCurrentRow(), System.Data.DataRowView)
            If CurrentRow IsNot Nothing Then
                Const TableName As String = "rpt_PendingAppointments_ByOffice_Counselors"
                Dim tbl As System.Data.DataTable = ds.Tables(TableName)
                If tbl IsNot Nothing Then
                    tbl.Clear()
                End If

                Try
                    Dim sqlInfo As DebtPlus.LINQ.SQLInfoClass = DebtPlus.LINQ.SQLInfoClass.getDefault()
                    Using cn As System.Data.SqlClient.SqlConnection = New System.Data.SqlClient.SqlConnection(sqlInfo.ConnectionString)
                        cn.Open()
                        Using cmd As New System.Data.SqlClient.SqlCommand()
                            cmd.Connection = cn
                            cmd.CommandText = "rpt_PendingAppointments_ByOffice_Counselors"
                            cmd.CommandType = System.Data.CommandType.StoredProcedure
                            System.Data.SqlClient.SqlCommandBuilder.DeriveParameters(cmd)

                            cmd.Parameters(1).Value = Convert.ToDateTime(CurrentRow("time")).Date
                            cmd.Parameters(2).Value = CurrentRow("office_id")

                            Using da As New System.Data.SqlClient.SqlDataAdapter(cmd)
                                da.Fill(ds, TableName)
                                tbl = ds.Tables(TableName)

                                If tbl.Rows.Count > 0 Then
                                    Rpt.DataSource = tbl.DefaultView
                                    Return
                                End If
                            End Using
                        End Using
                    End Using

                Catch ex As System.Data.SqlClient.SqlException
                    Using gdr As New DebtPlus.Repository.GetDataResult()
                        gdr.HandleException(ex)
                        DebtPlus.UI.Common.ErrorHandling.HandleErrors(gdr, "Error reading counselor list")
                    End Using
                End Try
            End If

            e.Cancel = True
        End Sub
    End Class
End Namespace
