Namespace Appointments.ByOffice
    <Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
    Partial Class CounselorSubReport
        Inherits DevExpress.XtraReports.UI.XtraReport

        'XtraReport overrides dispose to clean up the component list.
        <System.Diagnostics.DebuggerNonUserCode()> _
        Protected Overrides Sub Dispose(ByVal disposing As Boolean)
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
            MyBase.Dispose(disposing)
        End Sub

        'Required by the Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Designer
        'It can be modified using the Designer.  
        'Do not modify it using the code editor.
        <System.Diagnostics.DebuggerStepThrough()> _
        Private Sub InitializeComponent()
            Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(CounselorSubReport))
            Me.Detail = New DevExpress.XtraReports.UI.DetailBand
            Me.XrLabel_time_5 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_time_4 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_time_3 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_time_2 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_time_1 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_counselor = New DevExpress.XtraReports.UI.XRLabel
            Me.TopMargin = New DevExpress.XtraReports.UI.TopMarginBand
            Me.BottomMargin = New DevExpress.XtraReports.UI.BottomMarginBand
            Me.ReportHeader = New DevExpress.XtraReports.UI.ReportHeaderBand
            Me.XrLabel1 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrControlStyle_Header = New DevExpress.XtraReports.UI.XRControlStyle
            Me.XrControlStyle_Totals = New DevExpress.XtraReports.UI.XRControlStyle
            Me.XrControlStyle_HeaderPannel = New DevExpress.XtraReports.UI.XRControlStyle
            Me.XrControlStyle_GroupHeader = New DevExpress.XtraReports.UI.XRControlStyle
            CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
            '
            'Detail
            '
            Me.Detail.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel_time_5, Me.XrLabel_time_4, Me.XrLabel_time_3, Me.XrLabel_time_2, Me.XrLabel_time_1, Me.XrLabel_counselor})
            Me.Detail.HeightF = 15.0!
            Me.Detail.Name = "Detail"
            Me.Detail.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
            Me.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
            '
            'XrLabel_time_5
            '
            Me.XrLabel_time_5.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "time_5")})
            Me.XrLabel_time_5.LocationFloat = New DevExpress.Utils.PointFloat(663.2501!, 0.0!)
            Me.XrLabel_time_5.Name = "XrLabel_time_5"
            Me.XrLabel_time_5.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_time_5.SizeF = New System.Drawing.SizeF(83.0!, 15.0!)
            Me.XrLabel_time_5.StylePriority.UseTextAlignment = False
            Me.XrLabel_time_5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
            Me.XrLabel_time_5.WordWrap = False
            '
            'XrLabel_time_4
            '
            Me.XrLabel_time_4.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "time_4")})
            Me.XrLabel_time_4.LocationFloat = New DevExpress.Utils.PointFloat(562.8918!, 0.0!)
            Me.XrLabel_time_4.Name = "XrLabel_time_4"
            Me.XrLabel_time_4.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_time_4.SizeF = New System.Drawing.SizeF(83.0!, 15.0!)
            Me.XrLabel_time_4.StylePriority.UseTextAlignment = False
            Me.XrLabel_time_4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
            Me.XrLabel_time_4.WordWrap = False
            '
            'XrLabel_time_3
            '
            Me.XrLabel_time_3.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "time_3")})
            Me.XrLabel_time_3.LocationFloat = New DevExpress.Utils.PointFloat(462.5334!, 0.0!)
            Me.XrLabel_time_3.Name = "XrLabel_time_3"
            Me.XrLabel_time_3.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_time_3.SizeF = New System.Drawing.SizeF(83.0!, 15.0!)
            Me.XrLabel_time_3.StylePriority.UseTextAlignment = False
            Me.XrLabel_time_3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
            Me.XrLabel_time_3.WordWrap = False
            '
            'XrLabel_time_2
            '
            Me.XrLabel_time_2.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "time_2")})
            Me.XrLabel_time_2.LocationFloat = New DevExpress.Utils.PointFloat(362.175!, 0.0!)
            Me.XrLabel_time_2.Name = "XrLabel_time_2"
            Me.XrLabel_time_2.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_time_2.SizeF = New System.Drawing.SizeF(83.0!, 15.0!)
            Me.XrLabel_time_2.StylePriority.UseTextAlignment = False
            Me.XrLabel_time_2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
            Me.XrLabel_time_2.WordWrap = False
            '
            'XrLabel_time_1
            '
            Me.XrLabel_time_1.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "time_1")})
            Me.XrLabel_time_1.LocationFloat = New DevExpress.Utils.PointFloat(264.9417!, 0.0!)
            Me.XrLabel_time_1.Name = "XrLabel_time_1"
            Me.XrLabel_time_1.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_time_1.SizeF = New System.Drawing.SizeF(83.0!, 15.0!)
            Me.XrLabel_time_1.StylePriority.UseTextAlignment = False
            Me.XrLabel_time_1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
            Me.XrLabel_time_1.WordWrap = False
            '
            'XrLabel_counselor
            '
            Me.XrLabel_counselor.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "counselor")})
            Me.XrLabel_counselor.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
            Me.XrLabel_counselor.Name = "XrLabel_counselor"
            Me.XrLabel_counselor.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_counselor.ProcessDuplicates = DevExpress.XtraReports.UI.ValueSuppressType.Suppress
            Me.XrLabel_counselor.SizeF = New System.Drawing.SizeF(251.75!, 15.0!)
            Me.XrLabel_counselor.WordWrap = False
            '
            'TopMargin
            '
            Me.TopMargin.HeightF = 0.0!
            Me.TopMargin.Name = "TopMargin"
            Me.TopMargin.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
            Me.TopMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
            '
            'BottomMargin
            '
            Me.BottomMargin.HeightF = 0.0!
            Me.BottomMargin.Name = "BottomMargin"
            Me.BottomMargin.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
            Me.BottomMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
            '
            'ReportHeader
            '
            Me.ReportHeader.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel1})
            Me.ReportHeader.HeightF = 49.0!
            Me.ReportHeader.Name = "ReportHeader"
            Me.ReportHeader.StyleName = "XrControlStyle_Header"
            '
            'XrLabel1
            '
            Me.XrLabel1.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 12.5!)
            Me.XrLabel1.Name = "XrLabel1"
            Me.XrLabel1.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel1.SizeF = New System.Drawing.SizeF(746.2501!, 23.0!)
            Me.XrLabel1.Text = "List of the counselors who are expected to work on this office today"
            '
            'XrControlStyle_Header
            '
            Me.XrControlStyle_Header.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.XrControlStyle_Header.ForeColor = System.Drawing.Color.Teal
            Me.XrControlStyle_Header.Name = "XrControlStyle_Header"
            Me.XrControlStyle_Header.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
            Me.XrControlStyle_Header.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            '
            'XrControlStyle_Totals
            '
            Me.XrControlStyle_Totals.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.XrControlStyle_Totals.Name = "XrControlStyle_Totals"
            Me.XrControlStyle_Totals.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
            Me.XrControlStyle_Totals.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrControlStyle_HeaderPannel
            '
            Me.XrControlStyle_HeaderPannel.BackColor = System.Drawing.Color.Teal
            Me.XrControlStyle_HeaderPannel.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.XrControlStyle_HeaderPannel.ForeColor = System.Drawing.Color.White
            Me.XrControlStyle_HeaderPannel.Name = "XrControlStyle_HeaderPannel"
            Me.XrControlStyle_HeaderPannel.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
            Me.XrControlStyle_HeaderPannel.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrControlStyle_GroupHeader
            '
            Me.XrControlStyle_GroupHeader.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.XrControlStyle_GroupHeader.ForeColor = System.Drawing.Color.Maroon
            Me.XrControlStyle_GroupHeader.Name = "XrControlStyle_GroupHeader"
            Me.XrControlStyle_GroupHeader.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            '
            'CounselorSubReport
            '
            Me.Bands.AddRange(New DevExpress.XtraReports.UI.Band() {Me.Detail, Me.TopMargin, Me.BottomMargin, Me.ReportHeader})
            Me.Margins = New System.Drawing.Printing.Margins(0, 0, 0, 0)
            Me.ScriptLanguage = DevExpress.XtraReports.ScriptLanguage.VisualBasic
            Me.ScriptReferencesString = resources.GetString("$this.ScriptReferencesString")
            Me.StyleSheet.AddRange(New DevExpress.XtraReports.UI.XRControlStyle() {Me.XrControlStyle_Header, Me.XrControlStyle_Totals, Me.XrControlStyle_HeaderPannel, Me.XrControlStyle_GroupHeader})
            Me.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            Me.Version = "10.1"
            CType(Me, System.ComponentModel.ISupportInitialize).EndInit()

        End Sub
        Friend WithEvents Detail As DevExpress.XtraReports.UI.DetailBand
        Friend WithEvents TopMargin As DevExpress.XtraReports.UI.TopMarginBand
        Friend WithEvents BottomMargin As DevExpress.XtraReports.UI.BottomMarginBand
        Friend WithEvents ReportHeader As DevExpress.XtraReports.UI.ReportHeaderBand
        Friend WithEvents XrLabel_time_5 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_time_4 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_time_3 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_time_2 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_time_1 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_counselor As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel1 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrControlStyle_Header As DevExpress.XtraReports.UI.XRControlStyle
        Friend WithEvents XrControlStyle_Totals As DevExpress.XtraReports.UI.XRControlStyle
        Friend WithEvents XrControlStyle_HeaderPannel As DevExpress.XtraReports.UI.XRControlStyle
        Friend WithEvents XrControlStyle_GroupHeader As DevExpress.XtraReports.UI.XRControlStyle
    End Class
End Namespace
