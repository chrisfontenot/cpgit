#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}
#End Region
Imports DebtPlus.Reports.Template
Imports System.IO
Imports DebtPlus.Reports.Template.Forms
Imports DebtPlus.Utils

Namespace Appointments.ByOffice
    Public Class AppointmentByOffice
        Inherits DatedTemplateXtraReportClass

        Public Sub New()
            MyBase.New()

            ' Name of the report file
            Const ReportName As String = "DebtPlus.Reports.Appointments.ByOffice.repx"

            ' See if there is a report reference that we can use
            Dim Fname As String = System.IO.Path.Combine(DebtPlus.Configuration.Config.ReportsDirectory, ReportName)
            Dim UseDefault As Boolean = True
            Try
                If (Not DebtPlus.Configuration.Config.LocalReportsOnly) AndAlso System.IO.File.Exists(Fname) Then
                    LoadLayout(Fname)
                    UseDefault = False
                End If

            Catch ex As System.Exception
                DebtPlus.Svc.Common.ErrorHandling.HandleErrors(ex)
            End Try

            ' Load the standard report definition if we need a new item
            If UseDefault Then
                Dim asm As Reflection.Assembly = Reflection.Assembly.GetExecutingAssembly                                           ' changed
                Using ios As Stream = asm.GetManifestResourceStream(String.Format("DebtPlus.Reports.{0}", ReportName))              ' changed
                    If ios IsNot Nothing Then LoadLayout(ios) '                                                                       changed
                End Using
            End If

            ' Set the script references as needed
            ScriptReferences = DebtPlus.Utils.AssemblyTools.GetAllSiblingAssemblies()

            ' Update the subreports as well
            For Each BandPtr As DevExpress.XtraReports.UI.Band In Bands
                For Each CtlPtr As DevExpress.XtraReports.UI.XRControl In BandPtr.Controls
                    Dim Rpt As DevExpress.XtraReports.UI.XRSubreport = TryCast(CtlPtr, DevExpress.XtraReports.UI.XRSubreport)
                    If Rpt IsNot Nothing Then
                        Dim RptSrc As DevExpress.XtraReports.UI.XtraReport = TryCast(Rpt.ReportSource, DevExpress.XtraReports.UI.XtraReport)
                        If RptSrc IsNot Nothing Then
                            RptSrc.ScriptReferences = DebtPlus.Utils.AssemblyTools.GetAllSiblingAssemblies()
                        End If
                    End If
                Next
            Next

            ' Enable the select expert
            ReportFilter.IsEnabled = True

            ' Ensure that the parameters are undefined
            Parameters("ParameterOffice").Value = -1
        End Sub


        ''' <summary>
        '''     Title associated with the report
        ''' </summary>
        Public Overrides ReadOnly Property ReportTitle() As String
            Get
                Return "Appointments By Office"
            End Get
        End Property


        ''' <summary>
        '''     Request the dates and the office value
        ''' </summary>
        Public Overrides Function RequestReportParameters() As System.Windows.Forms.DialogResult
            Dim answer As System.Windows.Forms.DialogResult = DialogResult.OK
            If NeedParameters() Then
                Using frm As New DatedOfficeParametersForm(True)
                    answer = frm.ShowDialog()

                    Parameter_FromDate = frm.Parameter_FromDate
                    Parameter_ToDate = frm.Parameter_ToDate
                    If frm.Parameter_Office Is Nothing OrElse frm.Parameter_Office Is System.DBNull.Value Then
                        Parameter_Office = -1
                    Else
                        Parameter_Office = Convert.ToInt32(frm.Parameter_Office)
                    End If
                End Using
            End If
            Return answer
        End Function


        ''' <summary>
        '''     Office number parameter
        ''' </summary>
        Public Property Parameter_Office() As Int32
            Get
                Return CType(Parameters("ParameterOffice").Value, Int32)
            End Get
            Set(ByVal value As Int32)
                Try
                    Parameters("ParameterOffice").Value = value
                Catch ex As Exception
                    DebtPlus.Data.Forms.MessageBox.Show("You have an old version of DebtPlus.Reports.Appointments.ByOffice.repx in the reports share", "Sorry, old version")
                    Throw
                End Try
            End Set
        End Property
    End Class
End Namespace
