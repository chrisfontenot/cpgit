#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}
#End Region

Imports DebtPlus.Interfaces.Client
Imports DebtPlus.Utils

Namespace Client.Packet
    Public Class PacketReport
        Inherits DebtPlus.Reports.Template.BaseXtraReportClass
        Implements DebtPlus.Interfaces.Client.IClient

        ' this constructor only used for test
        Public Sub New()
            MyClass.New(String.Empty)
        End Sub

        Public Sub New(ByVal reportDefinition As String)
            MyBase.New()

            ' Remove the argument from the filename for the report
            ReportName = reportDefinition
            If ReportName <> String.Empty Then
                Dim iPos As Int32 = ReportName.IndexOf(";"c, 1)
                If iPos > 0 Then
                    ReportName = ReportName.Substring(0, iPos).TrimEnd()
                End If
            End If

            ' Change the extension to always be "REPX". It is the only thing that we can load.
            Dim Fname As String = System.IO.Path.ChangeExtension(reportDefinition, ".repx")

            ' See if there is a report reference that we can use
            If Not System.IO.File.Exists(ReportName) Then
                Fname = System.IO.Path.Combine(DebtPlus.Configuration.Config.ReportsDirectory, ReportName)
                If Not System.IO.File.Exists(Fname) Then
                    Dim Path As String = System.Reflection.Assembly.GetExecutingAssembly.Location
                    Path = System.IO.Path.GetDirectoryName(Path)
                    Fname = System.IO.Path.Combine(Path, ReportName)
                End If
            End If

            Using cn As New DebtPlus.UI.Common.CursorManager()
                LoadLayout(Fname)
            End Using

            ' Ensure that the parameters are defaulted to their "missing" status
            Parameter_Client = -1

            ' Ensure that the DevExpress parameter dialog is not used on the report.
            RequestParameters = False
            For Each parm As DevExpress.XtraReports.Parameters.Parameter In Parameters
                parm.Visible = False
            Next

            ' Set the script references to the current (running) values.
            ScriptReferences = DebtPlus.Utils.AssemblyTools.GetAllSiblingAssemblies

            ' Copy the script references to the sub-reports as well
            For Each Band As DevExpress.XtraReports.UI.Band In Bands
                For Each ctl As DevExpress.XtraReports.UI.XRControl In Band.Controls
                    Dim subReport As DevExpress.XtraReports.UI.XRSubreport = TryCast(ctl, DevExpress.XtraReports.UI.XRSubreport)
                    If subReport IsNot Nothing Then
                        Dim rpt As DevExpress.XtraReports.UI.XtraReport = subReport.ReportSource
                        If rpt IsNot Nothing Then
                            rpt.ScriptReferences = ScriptReferences
                        End If
                    End If
                Next
            Next
        End Sub

        ''' <summary>
        ''' Client ID. This is the primary key to the clients table.
        ''' </summary>
        Public Property ClientID As Int32 Implements DebtPlus.Interfaces.Client.IClient.ClientId
            Get
                Return Parameter_Client
            End Get
            Set(value As Int32)
                Parameter_Client = value
            End Set
        End Property

        ''' <summary>
        ''' Client ID. This is the primary key to the clients table.
        ''' </summary>
        Public Property Parameter_Client() As System.Int32
            Get
                Dim parm As DevExpress.XtraReports.Parameters.Parameter = FindParameter("ParameterClient")
                If parm Is Nothing Then Return -1
                Return Convert.ToInt32(parm.Value)
            End Get
            Set(ByVal value As System.Int32)
                SetParameter("ParameterClient", GetType(Int32), value, "Client ID", False)
            End Set
        End Property

        ''' <summary>
        ''' Does the report need to ask for parameters? If so, we request the parameters for the reprot
        ''' </summary>
        Public Overrides Function NeedParameters() As Boolean
            Return MyBase.NeedParameters() OrElse (Parameter_Client <= 0)
        End Function

        ''' <summary>
        ''' This is the title shown on the title bar for the preview dialog. It has no other purpose for the client packets
        ''' </summary>
        Public Overrides ReadOnly Property ReportTitle() As String
            Get
                Return "Client Packet"
            End Get
        End Property

        ''' <summary>
        ''' Common interface to set a parameter value.
        ''' </summary>
        ''' <param name="Parameter">Name of the parameter to be set.</param>
        ''' <param name="Value">Value associated with that parameter</param>
        Public Overrides Sub SetReportParameter(ByVal Parameter As String, ByVal Value As Object)
            Select Case Parameter
                Case "Client"
                    Parameter_Client = Convert.ToInt32(Value)
                Case Else
                    MyBase.SetReportParameter(Parameter, Value)
            End Select
        End Sub

        ''' <summary>
        ''' Ask for the report parameters if they are needed
        ''' </summary>
        Public Overrides Function RequestReportParameters() As System.Windows.Forms.DialogResult
            Dim answer As System.Windows.Forms.DialogResult = DialogResult.OK
            If NeedParameters() Then
                Using frm As New DebtPlus.Reports.Template.Forms.ClientParametersForm()
                    answer = frm.ShowDialog()
                    Parameter_Client = frm.Parameter_Client
                End Using
            End If
            Return answer
        End Function
    End Class
End Namespace