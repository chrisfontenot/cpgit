#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}

#End Region

Option Compare Binary
Option Explicit On
Option Strict On

Imports System.ComponentModel

Imports System.Drawing.Printing
Imports DevExpress.XtraReports.UI
Imports System.Data.SqlClient

Namespace Operations.Client.FirstDeposit
    Public Class FirstDepositReport
        Inherits DebtPlus.Reports.Template.DatedTemplateXtraReportClass

        Public Sub New(ByVal Container As IContainer)
            MyClass.New()
            Container.Add(Me)
        End Sub

        Public Sub New()
            MyBase.New()
            InitializeComponent()

            XrLabel_active_status.DataBindings.Add("Text", Nothing, "active_status")
            XrLabel_first_deposit_date.DataBindings.Add("Text", Nothing, "first_deposit_date", "{0:d}")
            XrLabel_last_deposit_amount.DataBindings.Add("Text", Nothing, "last_deposit_amount", "{0:c}")
            XrLabel_last_deposit_date.DataBindings.Add("Text", Nothing, "last_deposit_date", "{0:d}")

            AddHandler Me.BeforePrint, AddressOf Report_BeforePrint
            AddHandler XrLabel_client.BeforePrint, AddressOf XrLabel_client_BeforePrint
        End Sub

        Public Overrides ReadOnly Property ReportTitle() As String
            Get
                Return "First Client Deposits"
            End Get
        End Property

        Private ds As New DataSet("ds")

        Private Sub Report_BeforePrint(ByVal sender As Object, ByVal e As PrintEventArgs)

            Dim tbl As DataTable = ds.Tables("rpt_FirstDeposits")
            If tbl IsNot Nothing Then
                tbl.Clear()
                tbl = Nothing
            End If

            Dim rpt As XtraReport = CType(sender, XtraReport)
            Dim cn As SqlConnection = New System.Data.SqlClient.SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
            Try
                cn.Open()
                Using cmd As SqlCommand = New System.Data.SqlClient.SqlCommand()
                    With cmd
                        .Connection = cn
                        .CommandText = "rpt_FirstDeposits"
                        .CommandType = CommandType.StoredProcedure
                        SqlCommandBuilder.DeriveParameters(cmd)

                        .CommandTimeout = 0
                        .Parameters(1).Value = rpt.Parameters("ParameterFromDate").Value
                        .Parameters(2).Value = rpt.Parameters("ParameterToDate").Value
                    End With

                    Using da As New SqlDataAdapter(cmd)
                        da.Fill(ds, "rpt_FirstDeposits")
                        tbl = ds.Tables("rpt_FirstDeposits")
                        rpt.DataSource = tbl.DefaultView
                    End Using
                End Using

            Catch ex As SqlException
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error reading report information")

            Finally
                If cn IsNot Nothing Then cn.Dispose()
            End Try
        End Sub

        Private Sub XrLabel_client_BeforePrint(ByVal sender As Object, ByVal e As PrintEventArgs)
            With CType(sender, XRLabel)
                Dim ClientString As String = String.Format("{0:0000000}", GetCurrentColumnValue("client"))
                Dim ClientName As String = String.Empty
                If GetCurrentColumnValue("name") IsNot DBNull.Value Then ClientName = Convert.ToString(GetCurrentColumnValue("name"))
                .Text = ClientString + " " + ClientName
            End With
        End Sub
    End Class
End Namespace