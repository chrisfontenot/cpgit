#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}

#End Region

Imports System.ComponentModel
Imports DebtPlus.Reports.Operations.Client.Count.ByZipcode.Template
Imports System.Drawing.Printing
Imports System.Data.SqlClient

Namespace Operations.Client.Count.ByOffice
    Public Class ClientByOfficeReport
        Inherits ClientByZipcodeTemplateReport

        Public Sub New()
            MyBase.New()
            InitializeComponent()
            AddHandler Me.BeforePrint, AddressOf Report_BeforePrint
        End Sub

        Public Overrides ReadOnly Property ReportTitle() As String
            Get
                Return "Client Count by Office"
            End Get
        End Property

#Region " Component Designer generated code "

        'Component overrides dispose to clean up the component list.
        Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
            If disposing Then
                If components IsNot Nothing Then
                    components.Dispose()
                End If
            End If
            MyBase.Dispose(disposing)
        End Sub

        Friend WithEvents GroupHeader1 As DevExpress.XtraReports.UI.GroupHeaderBand
        Friend WithEvents XrLabel14 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents GroupFooter1 As DevExpress.XtraReports.UI.GroupFooterBand
        Protected Friend WithEvents XrLabel_subtotal_total As DevExpress.XtraReports.UI.XRLabel
        Protected Friend WithEvents XrLabel_subtotal_cre As DevExpress.XtraReports.UI.XRLabel
        Protected Friend WithEvents XrLabel_subtotal_apt As DevExpress.XtraReports.UI.XRLabel
        Protected Friend WithEvents XrLabel_subtotal_wks As DevExpress.XtraReports.UI.XRLabel
        Protected Friend WithEvents XrLabel_subtotal_pnd As DevExpress.XtraReports.UI.XRLabel
        Protected Friend WithEvents XrLabel_subtotal_rdy As DevExpress.XtraReports.UI.XRLabel
        Protected Friend WithEvents XrLabel_subtotal_pro As DevExpress.XtraReports.UI.XRLabel
        Protected Friend WithEvents XrLabel_subtotal_a As DevExpress.XtraReports.UI.XRLabel
        Protected Friend WithEvents XrLabel_subtotal_ar As DevExpress.XtraReports.UI.XRLabel
        Protected Friend WithEvents XrLabel_subtotal_ex As DevExpress.XtraReports.UI.XRLabel
        Protected Friend WithEvents XrLabel_subtotal_i As DevExpress.XtraReports.UI.XRLabel
        Protected Friend WithEvents XrLabel15 As DevExpress.XtraReports.UI.XRLabel

        'Required by the Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Designer
        'It can be modified using the Designer.
        'Do not modify it using the code editor.
        <System.Diagnostics.DebuggerStepThrough()>
        Private Sub InitializeComponent()
            Dim XrSummary1 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary
            Dim XrSummary2 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary
            Dim XrSummary3 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary
            Dim XrSummary4 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary
            Dim XrSummary5 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary
            Dim XrSummary6 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary
            Dim XrSummary7 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary
            Dim XrSummary8 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary
            Dim XrSummary9 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary
            Dim XrSummary10 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary
            Dim XrSummary11 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary
            Me.GroupHeader1 = New DevExpress.XtraReports.UI.GroupHeaderBand
            Me.XrLabel14 = New DevExpress.XtraReports.UI.XRLabel
            Me.GroupFooter1 = New DevExpress.XtraReports.UI.GroupFooterBand
            Me.XrLabel_subtotal_total = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_subtotal_cre = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_subtotal_apt = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_subtotal_wks = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_subtotal_pnd = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_subtotal_rdy = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_subtotal_pro = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_subtotal_a = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_subtotal_ar = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_subtotal_ex = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_subtotal_i = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel15 = New DevExpress.XtraReports.UI.XRLabel
            CType(Me.ds, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
            '
            'XrPanel1
            '
            Me.XrPanel1.StylePriority.UseBackColor = False
            Me.XrPanel1.StylePriority.UseBorderColor = False
            '
            'XrLabel1
            '
            Me.XrLabel1.StylePriority.UsePadding = False
            Me.XrLabel1.StylePriority.UseTextAlignment = False
            '
            'XrLabel12
            '
            Me.XrLabel12.StylePriority.UseTextAlignment = False
            '
            'XrLabel11
            '
            Me.XrLabel11.StylePriority.UseTextAlignment = False
            '
            'XrLabel10
            '
            Me.XrLabel10.StylePriority.UseTextAlignment = False
            '
            'XrLabel9
            '
            Me.XrLabel9.StylePriority.UseTextAlignment = False
            '
            'XrLabel8
            '
            Me.XrLabel8.StylePriority.UseTextAlignment = False
            '
            'XrLabel7
            '
            Me.XrLabel7.StylePriority.UseTextAlignment = False
            '
            'XrLabel6
            '
            Me.XrLabel6.StylePriority.UseTextAlignment = False
            '
            'XrLabel5
            '
            Me.XrLabel5.StylePriority.UseTextAlignment = False
            '
            'XrLabel4
            '
            Me.XrLabel4.StylePriority.UseTextAlignment = False
            '
            'XrLabel3
            '
            Me.XrLabel3.StylePriority.UseTextAlignment = False
            '
            'XrLabel2
            '
            Me.XrLabel2.StylePriority.UseTextAlignment = False
            '
            'XrLabel_total
            '
            Me.XrLabel_total.StylePriority.UseTextAlignment = False
            '
            'XrLabel_cre
            '
            Me.XrLabel_cre.StylePriority.UseTextAlignment = False
            '
            'XrLabel_apt
            '
            Me.XrLabel_apt.StylePriority.UseTextAlignment = False
            '
            'XrLabel_wks
            '
            Me.XrLabel_wks.StylePriority.UseTextAlignment = False
            '
            'XrLabel_pnd
            '
            Me.XrLabel_pnd.StylePriority.UseTextAlignment = False
            '
            'XrLabel_rdy
            '
            Me.XrLabel_rdy.StylePriority.UseTextAlignment = False
            '
            'XrLabel_pro
            '
            Me.XrLabel_pro.StylePriority.UseTextAlignment = False
            '
            'XrLabel_a
            '
            Me.XrLabel_a.StylePriority.UseTextAlignment = False
            '
            'XrLabel_ar
            '
            Me.XrLabel_ar.StylePriority.UseTextAlignment = False
            '
            'XrLabel_ex
            '
            Me.XrLabel_ex.StylePriority.UseTextAlignment = False
            '
            'XrLabel_i
            '
            Me.XrLabel_i.StylePriority.UseTextAlignment = False
            '
            'XrLabel_zipcode
            '
            Me.XrLabel_zipcode.StylePriority.UsePadding = False
            Me.XrLabel_zipcode.StylePriority.UseTextAlignment = False
            '
            'XrLabel_total_total
            '
            Me.XrLabel_total_total.StylePriority.UseTextAlignment = False
            '
            'XrLabel_total_cre
            '
            Me.XrLabel_total_cre.StylePriority.UseTextAlignment = False
            '
            'XrLabel_total_apt
            '
            Me.XrLabel_total_apt.StylePriority.UseTextAlignment = False
            '
            'XrLabel_total_wks
            '
            Me.XrLabel_total_wks.StylePriority.UseTextAlignment = False
            '
            'XrLabel_total_pnd
            '
            Me.XrLabel_total_pnd.StylePriority.UseTextAlignment = False
            '
            'XrLabel_total_rdy
            '
            Me.XrLabel_total_rdy.StylePriority.UseTextAlignment = False
            '
            'XrLabel_total_pro
            '
            Me.XrLabel_total_pro.StylePriority.UseTextAlignment = False
            '
            'XrLabel_total_a
            '
            Me.XrLabel_total_a.StylePriority.UseTextAlignment = False
            '
            'XrLabel_total_ar
            '
            Me.XrLabel_total_ar.StylePriority.UseTextAlignment = False
            '
            'XrLabel_total_ex
            '
            Me.XrLabel_total_ex.StylePriority.UseTextAlignment = False
            '
            'XrLabel_total_i
            '
            Me.XrLabel_total_i.StylePriority.UseTextAlignment = False
            '
            'XrLabel13
            '
            Me.XrLabel13.StylePriority.UsePadding = False
            Me.XrLabel13.StylePriority.UseTextAlignment = False
            '
            'GroupHeader1
            '
            Me.GroupHeader1.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel14})
            Me.GroupHeader1.GroupFields.AddRange(New DevExpress.XtraReports.UI.GroupField() {New DevExpress.XtraReports.UI.GroupField("office", DevExpress.XtraReports.UI.XRColumnSortOrder.Ascending)})
            Me.GroupHeader1.GroupUnion = DevExpress.XtraReports.UI.GroupUnion.WithFirstDetail
            Me.GroupHeader1.Height = 51
            Me.GroupHeader1.Name = "GroupHeader1"
            '
            'XrLabel14
            '
            Me.XrLabel14.Font = New System.Drawing.Font("Times New Roman", 14.0!, System.Drawing.FontStyle.Bold)
            Me.XrLabel14.ForeColor = System.Drawing.Color.Maroon
            Me.XrLabel14.Location = New System.Drawing.Point(0, 8)
            Me.XrLabel14.Name = "XrLabel14"
            Me.XrLabel14.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel14.Size = New System.Drawing.Size(750, 25)
            Me.XrLabel14.StylePriority.UseFont = False
            Me.XrLabel14.StylePriority.UseForeColor = False
            Me.XrLabel14.Text = "Office: [office]"
            '
            'GroupFooter1
            '
            Me.GroupFooter1.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel_subtotal_total, Me.XrLabel_subtotal_cre, Me.XrLabel_subtotal_apt, Me.XrLabel_subtotal_wks, Me.XrLabel_subtotal_pnd, Me.XrLabel_subtotal_rdy, Me.XrLabel_subtotal_pro, Me.XrLabel_subtotal_a, Me.XrLabel_subtotal_ar, Me.XrLabel_subtotal_ex, Me.XrLabel_subtotal_i, Me.XrLabel15})
            Me.GroupFooter1.GroupUnion = DevExpress.XtraReports.UI.GroupFooterUnion.WithLastDetail
            Me.GroupFooter1.Height = 34
            Me.GroupFooter1.Name = "GroupFooter1"
            '
            'XrLabel_subtotal_total
            '
            Me.XrLabel_subtotal_total.CanGrow = False
            Me.XrLabel_subtotal_total.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel_subtotal_total.Location = New System.Drawing.Point(667, 8)
            Me.XrLabel_subtotal_total.Name = "XrLabel_subtotal_total"
            Me.XrLabel_subtotal_total.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_subtotal_total.Size = New System.Drawing.Size(82, 15)
            Me.XrLabel_subtotal_total.StylePriority.UseFont = False
            Me.XrLabel_subtotal_total.StylePriority.UseTextAlignment = False
            XrSummary1.FormatString = "{0:n0}"
            XrSummary1.Running = DevExpress.XtraReports.UI.SummaryRunning.Group
            Me.XrLabel_subtotal_total.Summary = XrSummary1
            Me.XrLabel_subtotal_total.Text = "0"
            Me.XrLabel_subtotal_total.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrLabel_subtotal_total.WordWrap = False
            '
            'XrLabel_subtotal_cre
            '
            Me.XrLabel_subtotal_cre.CanGrow = False
            Me.XrLabel_subtotal_cre.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel_subtotal_cre.Location = New System.Drawing.Point(83, 8)
            Me.XrLabel_subtotal_cre.Name = "XrLabel_subtotal_cre"
            Me.XrLabel_subtotal_cre.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_subtotal_cre.Size = New System.Drawing.Size(57, 15)
            Me.XrLabel_subtotal_cre.StylePriority.UseFont = False
            Me.XrLabel_subtotal_cre.StylePriority.UseTextAlignment = False
            XrSummary2.FormatString = "{0:n0}"
            XrSummary2.Running = DevExpress.XtraReports.UI.SummaryRunning.Group
            Me.XrLabel_subtotal_cre.Summary = XrSummary2
            Me.XrLabel_subtotal_cre.Text = "0"
            Me.XrLabel_subtotal_cre.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrLabel_subtotal_cre.WordWrap = False
            '
            'XrLabel_subtotal_apt
            '
            Me.XrLabel_subtotal_apt.CanGrow = False
            Me.XrLabel_subtotal_apt.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel_subtotal_apt.Location = New System.Drawing.Point(142, 8)
            Me.XrLabel_subtotal_apt.Name = "XrLabel_subtotal_apt"
            Me.XrLabel_subtotal_apt.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_subtotal_apt.Size = New System.Drawing.Size(57, 15)
            Me.XrLabel_subtotal_apt.StylePriority.UseFont = False
            Me.XrLabel_subtotal_apt.StylePriority.UseTextAlignment = False
            XrSummary3.FormatString = "{0:n0}"
            XrSummary3.Running = DevExpress.XtraReports.UI.SummaryRunning.Group
            Me.XrLabel_subtotal_apt.Summary = XrSummary3
            Me.XrLabel_subtotal_apt.Text = "0"
            Me.XrLabel_subtotal_apt.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrLabel_subtotal_apt.WordWrap = False
            '
            'XrLabel_subtotal_wks
            '
            Me.XrLabel_subtotal_wks.CanGrow = False
            Me.XrLabel_subtotal_wks.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel_subtotal_wks.Location = New System.Drawing.Point(200, 8)
            Me.XrLabel_subtotal_wks.Name = "XrLabel_subtotal_wks"
            Me.XrLabel_subtotal_wks.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_subtotal_wks.Size = New System.Drawing.Size(56, 15)
            Me.XrLabel_subtotal_wks.StylePriority.UseFont = False
            Me.XrLabel_subtotal_wks.StylePriority.UseTextAlignment = False
            XrSummary4.FormatString = "{0:n0}"
            XrSummary4.Running = DevExpress.XtraReports.UI.SummaryRunning.Group
            Me.XrLabel_subtotal_wks.Summary = XrSummary4
            Me.XrLabel_subtotal_wks.Text = "0"
            Me.XrLabel_subtotal_wks.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrLabel_subtotal_wks.WordWrap = False
            '
            'XrLabel_subtotal_pnd
            '
            Me.XrLabel_subtotal_pnd.CanGrow = False
            Me.XrLabel_subtotal_pnd.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel_subtotal_pnd.Location = New System.Drawing.Point(258, 8)
            Me.XrLabel_subtotal_pnd.Name = "XrLabel_subtotal_pnd"
            Me.XrLabel_subtotal_pnd.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_subtotal_pnd.Size = New System.Drawing.Size(57, 15)
            Me.XrLabel_subtotal_pnd.StylePriority.UseFont = False
            Me.XrLabel_subtotal_pnd.StylePriority.UseTextAlignment = False
            XrSummary5.FormatString = "{0:n0}"
            XrSummary5.Running = DevExpress.XtraReports.UI.SummaryRunning.Group
            Me.XrLabel_subtotal_pnd.Summary = XrSummary5
            Me.XrLabel_subtotal_pnd.Text = "0"
            Me.XrLabel_subtotal_pnd.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrLabel_subtotal_pnd.WordWrap = False
            '
            'XrLabel_subtotal_rdy
            '
            Me.XrLabel_subtotal_rdy.CanGrow = False
            Me.XrLabel_subtotal_rdy.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel_subtotal_rdy.Location = New System.Drawing.Point(317, 8)
            Me.XrLabel_subtotal_rdy.Name = "XrLabel_subtotal_rdy"
            Me.XrLabel_subtotal_rdy.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_subtotal_rdy.Size = New System.Drawing.Size(57, 15)
            Me.XrLabel_subtotal_rdy.StylePriority.UseFont = False
            Me.XrLabel_subtotal_rdy.StylePriority.UseTextAlignment = False
            XrSummary6.FormatString = "{0:n0}"
            XrSummary6.Running = DevExpress.XtraReports.UI.SummaryRunning.Group
            Me.XrLabel_subtotal_rdy.Summary = XrSummary6
            Me.XrLabel_subtotal_rdy.Text = "0"
            Me.XrLabel_subtotal_rdy.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrLabel_subtotal_rdy.WordWrap = False
            '
            'XrLabel_subtotal_pro
            '
            Me.XrLabel_subtotal_pro.CanGrow = False
            Me.XrLabel_subtotal_pro.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel_subtotal_pro.Location = New System.Drawing.Point(375, 8)
            Me.XrLabel_subtotal_pro.Name = "XrLabel_subtotal_pro"
            Me.XrLabel_subtotal_pro.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_subtotal_pro.Size = New System.Drawing.Size(49, 15)
            Me.XrLabel_subtotal_pro.StylePriority.UseFont = False
            Me.XrLabel_subtotal_pro.StylePriority.UseTextAlignment = False
            XrSummary7.FormatString = "{0:n0}"
            XrSummary7.Running = DevExpress.XtraReports.UI.SummaryRunning.Group
            Me.XrLabel_subtotal_pro.Summary = XrSummary7
            Me.XrLabel_subtotal_pro.Text = "0"
            Me.XrLabel_subtotal_pro.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrLabel_subtotal_pro.WordWrap = False
            '
            'XrLabel_subtotal_a
            '
            Me.XrLabel_subtotal_a.CanGrow = False
            Me.XrLabel_subtotal_a.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel_subtotal_a.Location = New System.Drawing.Point(425, 8)
            Me.XrLabel_subtotal_a.Name = "XrLabel_subtotal_a"
            Me.XrLabel_subtotal_a.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_subtotal_a.Size = New System.Drawing.Size(66, 15)
            Me.XrLabel_subtotal_a.StylePriority.UseFont = False
            Me.XrLabel_subtotal_a.StylePriority.UseTextAlignment = False
            XrSummary8.FormatString = "{0:n0}"
            XrSummary8.Running = DevExpress.XtraReports.UI.SummaryRunning.Group
            Me.XrLabel_subtotal_a.Summary = XrSummary8
            Me.XrLabel_subtotal_a.Text = "0"
            Me.XrLabel_subtotal_a.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrLabel_subtotal_a.WordWrap = False
            '
            'XrLabel_subtotal_ar
            '
            Me.XrLabel_subtotal_ar.CanGrow = False
            Me.XrLabel_subtotal_ar.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel_subtotal_ar.Location = New System.Drawing.Point(492, 8)
            Me.XrLabel_subtotal_ar.Name = "XrLabel_subtotal_ar"
            Me.XrLabel_subtotal_ar.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_subtotal_ar.Size = New System.Drawing.Size(57, 15)
            Me.XrLabel_subtotal_ar.StylePriority.UseFont = False
            Me.XrLabel_subtotal_ar.StylePriority.UseTextAlignment = False
            XrSummary9.FormatString = "{0:n0}"
            XrSummary9.Running = DevExpress.XtraReports.UI.SummaryRunning.Group
            Me.XrLabel_subtotal_ar.Summary = XrSummary9
            Me.XrLabel_subtotal_ar.Text = "0"
            Me.XrLabel_subtotal_ar.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrLabel_subtotal_ar.WordWrap = False
            '
            'XrLabel_subtotal_ex
            '
            Me.XrLabel_subtotal_ex.CanGrow = False
            Me.XrLabel_subtotal_ex.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel_subtotal_ex.Location = New System.Drawing.Point(550, 8)
            Me.XrLabel_subtotal_ex.Name = "XrLabel_subtotal_ex"
            Me.XrLabel_subtotal_ex.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_subtotal_ex.Size = New System.Drawing.Size(56, 15)
            Me.XrLabel_subtotal_ex.StylePriority.UseFont = False
            Me.XrLabel_subtotal_ex.StylePriority.UseTextAlignment = False
            XrSummary10.FormatString = "{0:n0}"
            XrSummary10.Running = DevExpress.XtraReports.UI.SummaryRunning.Group
            Me.XrLabel_subtotal_ex.Summary = XrSummary10
            Me.XrLabel_subtotal_ex.Text = "0"
            Me.XrLabel_subtotal_ex.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrLabel_subtotal_ex.WordWrap = False
            '
            'XrLabel_subtotal_i
            '
            Me.XrLabel_subtotal_i.CanGrow = False
            Me.XrLabel_subtotal_i.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel_subtotal_i.Location = New System.Drawing.Point(608, 8)
            Me.XrLabel_subtotal_i.Name = "XrLabel_subtotal_i"
            Me.XrLabel_subtotal_i.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_subtotal_i.Size = New System.Drawing.Size(58, 15)
            Me.XrLabel_subtotal_i.StylePriority.UseFont = False
            Me.XrLabel_subtotal_i.StylePriority.UseTextAlignment = False
            XrSummary11.FormatString = "{0:n0}"
            XrSummary11.Running = DevExpress.XtraReports.UI.SummaryRunning.Group
            Me.XrLabel_subtotal_i.Summary = XrSummary11
            Me.XrLabel_subtotal_i.Text = "0"
            Me.XrLabel_subtotal_i.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrLabel_subtotal_i.WordWrap = False
            '
            'XrLabel15
            '
            Me.XrLabel15.CanGrow = False
            Me.XrLabel15.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel15.Location = New System.Drawing.Point(0, 8)
            Me.XrLabel15.Name = "XrLabel15"
            Me.XrLabel15.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
            Me.XrLabel15.Size = New System.Drawing.Size(75, 15)
            Me.XrLabel15.StylePriority.UseFont = False
            Me.XrLabel15.StylePriority.UsePadding = False
            Me.XrLabel15.StylePriority.UseTextAlignment = False
            Me.XrLabel15.Text = "SUBTOTAL"
            Me.XrLabel15.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            Me.XrLabel15.WordWrap = False
            '
            'ClientByOfficeReport
            '
            Me.Bands.AddRange(New DevExpress.XtraReports.UI.Band() {Me.GroupHeader1, Me.GroupFooter1})
            Me.Version = "8.1"
            CType(Me.ds, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me, System.ComponentModel.ISupportInitialize).EndInit()
        End Sub

#End Region

        Protected Overrides Function GetDataSource() As DataView
            Dim vue As DataView = Nothing
            Dim cmd As SqlCommand = New System.Data.SqlClient.SqlCommand()
            With cmd
                .Connection = New System.Data.SqlClient.SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
                .CommandText = "rpt_clients_by_zipcode"
                .CommandType = CommandType.StoredProcedure
                .CommandTimeout = 0
            End With

            Dim current_cursor As Cursor = Cursor.Current
            Try
                Cursor.Current = Cursors.WaitCursor
                Dim da As New SqlDataAdapter(cmd)
                da.Fill(ds, "clients")
                Dim tbl As DataTable = ds.Tables("clients")
                With tbl
                    .Columns.Add("total", GetType(Int32), "[cre]+[apt]+[wks]+[pnd]+[rdy]+[pro]+[a]+[ar]+[ex]+[i]")
                End With

                vue = New DataView(tbl, String.Empty, "office, zipcode", DataViewRowState.CurrentRows)

            Catch ex As Exception
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error reading data from database")

            Finally
                Cursor.Current = current_cursor
            End Try

            Return vue
        End Function

        Private Sub Report_BeforePrint(ByVal sender As Object, ByVal e As PrintEventArgs)

            ' Bind the subtotals to the report source
            XrLabel_subtotal_cre.DataBindings.Add("Text", vue, "cre")
            XrLabel_subtotal_apt.DataBindings.Add("Text", vue, "apt")
            XrLabel_subtotal_wks.DataBindings.Add("Text", vue, "wks")
            XrLabel_subtotal_pnd.DataBindings.Add("Text", vue, "pnd")
            XrLabel_subtotal_rdy.DataBindings.Add("Text", vue, "rdy")
            XrLabel_subtotal_pro.DataBindings.Add("Text", vue, "pro")
            XrLabel_subtotal_a.DataBindings.Add("Text", vue, "a")
            XrLabel_subtotal_ar.DataBindings.Add("Text", vue, "ar")
            XrLabel_subtotal_ex.DataBindings.Add("Text", vue, "ex")
            XrLabel_subtotal_i.DataBindings.Add("Text", vue, "i")
            XrLabel_subtotal_total.DataBindings.Add("Text", vue, "total")
        End Sub
    End Class
End Namespace