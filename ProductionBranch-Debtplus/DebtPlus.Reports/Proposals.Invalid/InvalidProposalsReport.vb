#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}
#End Region

Option Compare Binary
Option Explicit On 
Option Strict On

Namespace Proposals.Invalid
    Public Class InvalidProposalsReport
        Inherits Template.TemplateXtraReportClass

        Public Overrides ReadOnly Property ReportTitle() As String
            Get
                Return "Invalid Proposals"
            End Get
        End Property

        ''' <summary>
        ''' Create an instance of our report
        ''' </summary>
        Public Sub New()
            MyBase.New()
            InitializeComponent()
            AddHandler XrLabel_client.BeforePrint, AddressOf XrLabel_Client_BeforePrint
            AddHandler Me.BeforePrint, AddressOf Report_BeforePrint
        End Sub

#Region " Component Designer generated code "

        'Component overrides dispose to clean up the component list.
        Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
            If disposing Then
                If components IsNot Nothing Then
                    components.Dispose()
                End If
            End If
            MyBase.Dispose(disposing)
        End Sub

        'Required by the Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Designer
        'It can be modified using the Designer.
        'Do not modify it using the code editor.
        Friend WithEvents XrPanel1 As DevExpress.XtraReports.UI.XRPanel
        Friend WithEvents XrLabel4 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel5 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel3 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel6 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_account_number As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_proposal_date As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel10 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_client_name As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_client As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel1 As DevExpress.XtraReports.UI.XRLabel
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
            Me.XrPanel1 = New DevExpress.XtraReports.UI.XRPanel
            Me.XrLabel1 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel4 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel5 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel3 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel6 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_account_number = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_proposal_date = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel10 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_client_name = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_client = New DevExpress.XtraReports.UI.XRLabel
            CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
            '
            'Detail
            '
            Me.Detail.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel_account_number, Me.XrLabel_proposal_date, Me.XrLabel10, Me.XrLabel_client_name, Me.XrLabel_client})
            Me.Detail.Height = 15
            '
            'PageHeader
            '
            Me.PageHeader.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrPanel1})
            Me.PageHeader.Height = 141
            Me.PageHeader.Controls.SetChildIndex(Me.XrPanel1, 0)
            Me.PageHeader.Controls.SetChildIndex(Me.XrPanel_AgencyAddress, 0)
            Me.PageHeader.Controls.SetChildIndex(Me.XrLabel_Title, 0)
            Me.PageHeader.Controls.SetChildIndex(Me.XrLine_Title, 0)
            Me.PageHeader.Controls.SetChildIndex(Me.XrLabel_Subtitle, 0)
            Me.PageHeader.Controls.SetChildIndex(Me.XrPageInfo1, 0)
            '
            'XrPanel1
            '
            Me.XrPanel1.BackColor = System.Drawing.Color.Teal
            Me.XrPanel1.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel1, Me.XrLabel4, Me.XrLabel5, Me.XrLabel3, Me.XrLabel6})
            Me.XrPanel1.Location = New System.Drawing.Point(0, 125)
            Me.XrPanel1.Name = "XrPanel1"
            Me.XrPanel1.Size = New System.Drawing.Size(750, 16)
            Me.XrPanel1.StyleName = "XrControlStyle_HeaderPannel"
            '
            'XrLabel1
            '
            Me.XrLabel1.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel1.ForeColor = System.Drawing.Color.White
            Me.XrLabel1.Location = New System.Drawing.Point(4, 0)
            Me.XrLabel1.Name = "XrLabel1"
            Me.XrLabel1.Size = New System.Drawing.Size(70, 15)
            Me.XrLabel1.Text = "CLIENT"
            '
            'XrLabel4
            '
            Me.XrLabel4.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel4.ForeColor = System.Drawing.Color.White
            Me.XrLabel4.Location = New System.Drawing.Point(83, 0)
            Me.XrLabel4.Name = "XrLabel4"
            Me.XrLabel4.Size = New System.Drawing.Size(116, 15)
            Me.XrLabel4.StylePriority.UseTextAlignment = False
            Me.XrLabel4.Text = "NAME"
            Me.XrLabel4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            '
            'XrLabel5
            '
            Me.XrLabel5.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel5.ForeColor = System.Drawing.Color.White
            Me.XrLabel5.Location = New System.Drawing.Point(241, 0)
            Me.XrLabel5.Name = "XrLabel5"
            Me.XrLabel5.Size = New System.Drawing.Size(116, 15)
            Me.XrLabel5.StylePriority.UseTextAlignment = False
            Me.XrLabel5.Text = "CREDITOR"
            Me.XrLabel5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            '
            'XrLabel3
            '
            Me.XrLabel3.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel3.ForeColor = System.Drawing.Color.White
            Me.XrLabel3.Location = New System.Drawing.Point(675, 0)
            Me.XrLabel3.Name = "XrLabel3"
            Me.XrLabel3.Size = New System.Drawing.Size(65, 15)
            Me.XrLabel3.Text = "DATE"
            '
            'XrLabel6
            '
            Me.XrLabel6.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel6.ForeColor = System.Drawing.Color.White
            Me.XrLabel6.Location = New System.Drawing.Point(517, 0)
            Me.XrLabel6.Name = "XrLabel6"
            Me.XrLabel6.Size = New System.Drawing.Size(127, 15)
            Me.XrLabel6.StylePriority.UseTextAlignment = False
            Me.XrLabel6.Text = "ACCT #"
            Me.XrLabel6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            '
            'XrLabel_account_number
            '
            Me.XrLabel_account_number.Font = New System.Drawing.Font("Times New Roman", 9.75!)
            Me.XrLabel_account_number.ForeColor = System.Drawing.SystemColors.ControlText
            Me.XrLabel_account_number.Location = New System.Drawing.Point(517, 0)
            Me.XrLabel_account_number.Name = "XrLabel_account_number"
            Me.XrLabel_account_number.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 0, 0, 0, 100.0!)
            Me.XrLabel_account_number.Size = New System.Drawing.Size(150, 15)
            Me.XrLabel_account_number.StylePriority.UseFont = False
            Me.XrLabel_account_number.StylePriority.UseForeColor = False
            Me.XrLabel_account_number.StylePriority.UsePadding = False
            Me.XrLabel_account_number.StylePriority.UseTextAlignment = False
            Me.XrLabel_account_number.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            Me.XrLabel_account_number.WordWrap = False
            '
            'XrLabel_proposal_date
            '
            Me.XrLabel_proposal_date.Font = New System.Drawing.Font("Times New Roman", 9.75!)
            Me.XrLabel_proposal_date.ForeColor = System.Drawing.SystemColors.ControlText
            Me.XrLabel_proposal_date.Location = New System.Drawing.Point(675, 0)
            Me.XrLabel_proposal_date.Name = "XrLabel_proposal_date"
            Me.XrLabel_proposal_date.Size = New System.Drawing.Size(65, 15)
            Me.XrLabel_proposal_date.StylePriority.UseFont = False
            Me.XrLabel_proposal_date.StylePriority.UseForeColor = False
            Me.XrLabel_proposal_date.StylePriority.UseTextAlignment = False
            Me.XrLabel_proposal_date.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel10
            '
            Me.XrLabel10.Font = New System.Drawing.Font("Times New Roman", 9.75!)
            Me.XrLabel10.ForeColor = System.Drawing.SystemColors.ControlText
            Me.XrLabel10.Location = New System.Drawing.Point(242, 0)
            Me.XrLabel10.Name = "XrLabel10"
            Me.XrLabel10.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 0, 0, 0, 100.0!)
            Me.XrLabel10.Size = New System.Drawing.Size(267, 15)
            Me.XrLabel10.StylePriority.UseFont = False
            Me.XrLabel10.StylePriority.UseForeColor = False
            Me.XrLabel10.StylePriority.UsePadding = False
            Me.XrLabel10.StylePriority.UseTextAlignment = False
            Me.XrLabel10.Text = "[creditor] [creditor_name]"
            Me.XrLabel10.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            Me.XrLabel10.WordWrap = False
            '
            'XrLabel_client_name
            '
            Me.XrLabel_client_name.Font = New System.Drawing.Font("Times New Roman", 9.75!)
            Me.XrLabel_client_name.ForeColor = System.Drawing.SystemColors.ControlText
            Me.XrLabel_client_name.Location = New System.Drawing.Point(83, 0)
            Me.XrLabel_client_name.Name = "XrLabel_client_name"
            Me.XrLabel_client_name.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 0, 0, 0, 100.0!)
            Me.XrLabel_client_name.ProcessDuplicates = DevExpress.XtraReports.UI.ValueSuppressType.Suppress
            Me.XrLabel_client_name.Size = New System.Drawing.Size(150, 15)
            Me.XrLabel_client_name.StylePriority.UseFont = False
            Me.XrLabel_client_name.StylePriority.UseForeColor = False
            Me.XrLabel_client_name.StylePriority.UsePadding = False
            Me.XrLabel_client_name.StylePriority.UseTextAlignment = False
            Me.XrLabel_client_name.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            Me.XrLabel_client_name.WordWrap = False
            '
            'XrLabel_client
            '
            Me.XrLabel_client.Font = New System.Drawing.Font("Times New Roman", 9.75!)
            Me.XrLabel_client.ForeColor = System.Drawing.SystemColors.ControlText
            Me.XrLabel_client.Location = New System.Drawing.Point(0, 0)
            Me.XrLabel_client.Name = "XrLabel_client"
            Me.XrLabel_client.ProcessDuplicates = DevExpress.XtraReports.UI.ValueSuppressType.Suppress
            Me.XrLabel_client.Size = New System.Drawing.Size(75, 15)
            Me.XrLabel_client.StylePriority.UseFont = False
            Me.XrLabel_client.StylePriority.UseForeColor = False
            Me.XrLabel_client.StylePriority.UseTextAlignment = False
            Me.XrLabel_client.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            Me.XrLabel_account_number.DataBindings.Add("Text", Nothing, "account_number")
            Me.XrLabel_proposal_date.DataBindings.Add("Text", Nothing, "proposal_date", "{0:d}")
            Me.XrLabel_client_name.DataBindings.Add("Text", Nothing, "client_name")
            '
            'InvalidProposalsReport
            '
            Me.Bands.AddRange(New DevExpress.XtraReports.UI.Band() {Me.Detail, Me.PageHeader, Me.PageFooter})
            Me.ScriptLanguage = DevExpress.XtraReports.ScriptLanguage.VisualBasic
            Me.Version = "8.3"
            CType(Me, System.ComponentModel.ISupportInitialize).EndInit()

        End Sub

#End Region

        Dim ds As New System.Data.DataSet("ds")
        Private Sub Report_BeforePrint(ByVal sender As Object, ByVal e As System.EventArgs)
            Dim tbl As System.Data.DataTable = ds.Tables("rpt_invalid_proposals")

            If tbl Is Nothing Then
                Try
                    Using cn As SqlClient.SqlConnection = New System.Data.SqlClient.SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
                        cn.Open()
                        Using cmd As SqlClient.SqlCommand = New System.Data.SqlClient.SqlCommand()
                            cmd.Connection = cn
                            cmd.CommandText = "rpt_invalid_proposals"
                            cmd.CommandType = CommandType.StoredProcedure
                            cmd.CommandTimeout = 0

                            Using da As New SqlClient.SqlDataAdapter(cmd)
                                da.Fill(ds, "rpt_invalid_proposals")
                                tbl = ds.Tables("rpt_invalid_proposals")
                            End Using
                        End Using
                    End Using

                Catch ex As SqlClient.SqlException
                    Using gdr As New Repository.GetDataResult()
                        gdr.HandleException(ex)
                        DebtPlus.UI.Common.ErrorHandling.HandleErrors(gdr, "Error reading rpt_proposals_pending")
                    End Using
                End Try
            End If

            If tbl IsNot Nothing Then
                Dim vue As System.Data.DataView = New System.Data.DataView(tbl, String.Empty, "client, creditor, account_number", DataViewRowState.CurrentRows)
                DataSource = vue
            End If
        End Sub

        Private Sub XrLabel_Client_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs)
            XrLabel_client.Text = String.Format("{0:0000000}", GetCurrentColumnValue("client"))
        End Sub
    End Class
End Namespace
