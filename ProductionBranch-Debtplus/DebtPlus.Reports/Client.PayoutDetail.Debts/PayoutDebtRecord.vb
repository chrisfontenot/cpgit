Imports DebtPlus.Svc.Debt
Imports DebtPlus.Utils

Namespace Client.PayoutDetail.Debts
    Friend Class PayoutDebtRecord
        Inherits DebtRecord

        Private Class PayoutRecordStorage
            Inherits RecordStorage

            Public Sub New()
                MyBase.New()
            End Sub

            Public Sub New(ByVal Source As RecordStorage)
                MyBase.New(Source)
                With Source
                    m_Active = .IsActive
                End With
            End Sub

            Public Sub New(ByVal Source As PayoutRecordStorage)
                MyBase.New(Source)
                With Source
                    m_Active = .m_Active
                    m_PayoutInterest = .m_PayoutInterest
                    m_PayoutPayments = .m_PayoutPayments
                End With
            End Sub

            Public Shadows Function Clone() As Object
                Return New PayoutRecordStorage(Me)
            End Function

            Public m_Active As Object = True
            Public m_PayoutPayments As Decimal = 0D
            Public m_PayoutInterest As Decimal = 0D
        End Class

        Private PayoutCurrentStorage As PayoutRecordStorage


        ''' <summary>
        ''' Create a new instance of our class
        ''' </summary>
        Public Sub New()
            MyBase.New()
            PayoutCurrentStorage = New PayoutRecordStorage
        End Sub

        Public Sub New(ByVal Source As DebtRecord)
            MyBase.New(Source)
            PayoutCurrentStorage = New PayoutRecordStorage
        End Sub

        Public Sub New(ByVal Source As PayoutDebtRecord)
            MyBase.New(Source)
            PayoutCurrentStorage = New PayoutRecordStorage
            With Source.PayoutCurrentStorage
                PayoutCurrentStorage.m_Active = .m_Active
                PayoutCurrentStorage.m_PayoutPayments = .m_PayoutPayments
                PayoutCurrentStorage.m_PayoutInterest = .m_PayoutInterest
            End With
        End Sub


        ''' <summary>
        ''' Do the clone function here
        ''' </summary>
        Public Shadows Function Clone() As Object
            Return New PayoutDebtRecord(Me)
        End Function


        ''' <summary>
        ''' Is the debt currently active?
        ''' </summary>
        Public Overrides Property IsActive() As Boolean
            Get
                Return MyBase.IsActive AndAlso PayoutCurrentStorage.m_Active
            End Get
            Set(ByVal value As Boolean)
                PayoutCurrentStorage.m_Active = value
            End Set
        End Property


        ''' <summary>
        ''' Current payments dollar amount applied to the debt
        ''' </summary>
        Public Overrides Property total_payments() As Decimal
            Get
                Return MyBase.total_payments + PayoutCurrentStorage.m_PayoutPayments
            End Get
            Set(ByVal value As Decimal)
                PayoutCurrentStorage.m_PayoutPayments = value - MyBase.total_payments
            End Set
        End Property


        ''' <summary>
        ''' Current interest dollar amount applied to the debt
        ''' </summary>
        Public Overrides Property total_interest() As Decimal
            Get
                Return MyBase.total_interest + PayoutCurrentStorage.m_PayoutInterest
            End Get
            Set(ByVal value As Decimal)
                PayoutCurrentStorage.m_PayoutInterest = value - MyBase.total_interest
            End Set
        End Property


        ''' <summary>
        ''' Compute the approximate amount of interest for this month
        ''' </summary>
        Public ReadOnly Property interest_amount() As Decimal
            Get
                Dim rate As Double

                ' Prefer the payout interest figure
                If dmp_payout_interest IsNot Nothing AndAlso dmp_payout_interest IsNot DBNull.Value AndAlso Convert.ToDouble(dmp_payout_interest) > 0.0 Then
                    rate = dmp_payout_interest

                    ' If there is no payout interest then use the client's override to the rate
                ElseIf dmp_interest IsNot Nothing AndAlso dmp_interest IsNot DBNull.Value AndAlso Convert.ToDouble(dmp_interest) > 0.0 Then
                    rate = dmp_interest

                    ' If there is no override then use the creditor's rate
                ElseIf creditor_interest IsNot Nothing AndAlso creditor_interest IsNot DBNull.Value AndAlso Convert.ToDouble(creditor_interest) > 0.0 Then
                    rate = creditor_interest

                    ' If there is nothing then there is no interest
                Else
                    Return 0D
                End If

                ' There is a rate. Use it.
                Return System.Math.Truncate(Convert.ToDecimal(Convert.ToDouble(current_balance) * rate / 12.0#) * 100D) / 100D
            End Get
        End Property
    End Class
End NameSpace
