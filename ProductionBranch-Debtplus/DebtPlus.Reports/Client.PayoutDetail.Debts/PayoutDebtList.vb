Imports DebtPlus.Interfaces.Debt
Imports DebtPlus.Svc.Debt

Namespace Client.PayoutDetail.Debts
    Friend Class PayoutProrate
        Inherits ProrateDebts

        Private AllDebtsInPayoff As Boolean
        Public Sub New(DebtList As DebtPlus.Interfaces.Debt.IProratableList, AllDebtsInPayoff As Boolean)
            MyBase.New(DebtList)
            Me.AllDebtsInPayoff = AllDebtsInPayoff
        End Sub

        Protected Overrides Sub fees_QueryValue(sender As Object, e As Events.ParameterValueEventArgs)
            ' If the current month is the final item then look only at the sum of the disbursements
            ' to find the proper fee. We don't use anything else but the dollars disbursed.
            If e.Name.Equals(DebtPlus.Events.ParameterValueEventArgs.name_InProrate) Then
                e.Value = Not AllDebtsInPayoff
                Return
            End If

            ' Otherwise, do the normal processing
            MyBase.fees_QueryValue(sender, e)
        End Sub
    End Class

    Friend Class PayoutDebtList
        Inherits DebtRecordList

        ''' <summary>
        ''' Create a new instance of our class
        ''' </summary>
        Public Sub New()
            MyBase.New(GetType(PayoutDebtRecord))
        End Sub

        ''' <summary>
        ''' Calculate the total of the payments for the current list of creditors
        ''' </summary>
        Public Function CreditorPayments() As Decimal
            Dim Answer As Decimal = 0D

            For Each Record As PayoutDebtRecord In Me
                With Record
                    Select Case .DebtType
                        Case DebtPlus.Interfaces.Debt.DebtTypeEnum.FixedAgencyFee
                            If .IsActive Then Answer += .debit_amt
                        Case DebtPlus.Interfaces.Debt.DebtTypeEnum.Normal, DebtPlus.Interfaces.Debt.DebtTypeEnum.AgencyFee
                            If .IsActive Then Answer += .debit_amt
                    End Select
                End With
            Next

            Return Answer
        End Function

        ''' <summary>
        ''' Generate a duplicate copy of the debt list
        ''' This is normally used only in the payout report when called from the client interface
        ''' and we want to pass the current client debt list to the payout report.
        ''' </summary>
        Public Sub CloneDebtList(ByVal OriginalList As DebtRecordList)

            Clear()

            ' First, duplicate the list storage items. These are the creditor IDs, etc.
            DebtListCurrentStorage = CType(OriginalList.DebtListCurrentStorage.Clone(), DebtListStorage)

            ' Then duplicate the debts in the list, casting them to the proper format as needed
            ' We can not use clone() since we are changing types from a clientupdatedebtlist to a payoutdebtlist.
            For Each Record As DebtRecord In OriginalList
                Dim NewRecord As New PayoutDebtRecord(Record)
                Add(NewRecord)

                ' These fields are changed for the payout. Copy them from the originals
                With NewRecord
                    .total_interest = Record.total_interest
                    .total_payments = Record.total_payments
                    .total_sched_payment = Record.total_sched_payment
                    .debit_amt = DebtPlus.Utils.Nulls.DDec(Record.disbursement_factor)
                End With
            Next
        End Sub
    End Class
End Namespace
