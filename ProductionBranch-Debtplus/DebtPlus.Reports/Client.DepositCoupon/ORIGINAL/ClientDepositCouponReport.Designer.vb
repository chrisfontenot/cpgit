Namespace Client.DepositCoupon
    <Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
    Partial Public Class ClientDepositCouponReport
        Inherits DebtPlus.Reports.Template.BaseXtraReportClass

        'XtraReport overrides dispose to clean up the component list.
        <System.Diagnostics.DebuggerNonUserCode()> _
        Protected Overrides Sub Dispose(ByVal disposing As Boolean)
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
            MyBase.Dispose(disposing)
        End Sub

        'Required by the Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Designer
        'It can be modified using the Designer.  
        'Do not modify it using the code editor.
        <System.Diagnostics.DebuggerStepThrough()> _
        Private Sub InitializeComponent()
            Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(ClientDepositCouponReport))
            Me.XrLabel_CutHere = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_BarCode = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrPanel4 = New DevExpress.XtraReports.UI.XRPanel()
            Me.XrLabel1_deposit_amount = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_Client = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_due_date = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel9 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel8 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_ClientNameAddress = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrRichText1 = New DevExpress.XtraReports.UI.XRRichText()
            Me.GroupHeader1 = New DevExpress.XtraReports.UI.GroupHeaderBand()
            Me.GroupFooter1 = New DevExpress.XtraReports.UI.GroupFooterBand()
            Me.CalculatedField_due_date = New DevExpress.XtraReports.UI.CalculatedField()
            Me.ParameterClient = New DevExpress.XtraReports.Parameters.Parameter()
            CType(Me.XrRichText1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
            '
            'Detail
            '
            Me.Detail.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel_CutHere, Me.XrLabel_BarCode, Me.XrPanel4, Me.XrLabel_ClientNameAddress, Me.XrRichText1})
            Me.Detail.HeightF = 393.125!
            Me.Detail.KeepTogether = True
            '
            'XrLabel_CutHere
            '
            Me.XrLabel_CutHere.BorderDashStyle = DevExpress.XtraPrinting.BorderDashStyle.Dot
            Me.XrLabel_CutHere.Borders = DevExpress.XtraPrinting.BorderSide.Top
            Me.XrLabel_CutHere.CanShrink = True
            Me.XrLabel_CutHere.Font = New System.Drawing.Font("Times New Roman", 8.0!)
            Me.XrLabel_CutHere.ForeColor = System.Drawing.Color.Gray
            Me.XrLabel_CutHere.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 368.125!)
            Me.XrLabel_CutHere.Name = "XrLabel_CutHere"
            Me.XrLabel_CutHere.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_CutHere.Scripts.OnDraw = "XrLabel_CutHere_Draw"
            Me.XrLabel_CutHere.SizeF = New System.Drawing.SizeF(800.0!, 25.0!)
            Me.XrLabel_CutHere.StylePriority.UseBorderDashStyle = False
            Me.XrLabel_CutHere.StylePriority.UseBorders = False
            Me.XrLabel_CutHere.StylePriority.UseFont = False
            Me.XrLabel_CutHere.StylePriority.UseForeColor = False
            Me.XrLabel_CutHere.StylePriority.UseTextAlignment = False
            Me.XrLabel_CutHere.Text = "8<    PLEASE CUT HERE   8<"
            Me.XrLabel_CutHere.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
            '
            'XrLabel_BarCode
            '
            Me.XrLabel_BarCode.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold)
            Me.XrLabel_BarCode.LocationFloat = New DevExpress.Utils.PointFloat(9.999998!, 339.6667!)
            Me.XrLabel_BarCode.Name = "XrLabel_BarCode"
            Me.XrLabel_BarCode.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_BarCode.Scripts.OnBeforePrint = "XrLabel_BarCode_BeforePrint"
            Me.XrLabel_BarCode.SizeF = New System.Drawing.SizeF(583.0!, 17.0!)
            Me.XrLabel_BarCode.StylePriority.UseFont = False
            Me.XrLabel_BarCode.Text = "00000000 000000000 00000000 00000000"
            '
            'XrPanel4
            '
            Me.XrPanel4.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel1_deposit_amount, Me.XrLabel_Client, Me.XrLabel_due_date, Me.XrLabel9, Me.XrLabel8})
            Me.XrPanel4.LocationFloat = New DevExpress.Utils.PointFloat(366.0001!, 208.0!)
            Me.XrPanel4.Name = "XrPanel4"
            Me.XrPanel4.SizeF = New System.Drawing.SizeF(308.0!, 108.0!)
            '
            'XrLabel1_deposit_amount
            '
            Me.XrLabel1_deposit_amount.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "deposit_amount", "{0:c}")})
            Me.XrLabel1_deposit_amount.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel1_deposit_amount.LocationFloat = New DevExpress.Utils.PointFloat(142.0!, 33.0!)
            Me.XrLabel1_deposit_amount.Name = "XrLabel1_deposit_amount"
            Me.XrLabel1_deposit_amount.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel1_deposit_amount.SizeF = New System.Drawing.SizeF(149.0!, 17.0!)
            Me.XrLabel1_deposit_amount.StylePriority.UseFont = False
            '
            'XrLabel_Client
            '
            Me.XrLabel_Client.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "client")})
            Me.XrLabel_Client.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel_Client.LocationFloat = New DevExpress.Utils.PointFloat(141.0!, 9.0!)
            Me.XrLabel_Client.Name = "XrLabel_Client"
            Me.XrLabel_Client.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_Client.Scripts.OnBeforePrint = "XrLabel_Client_BeforePrint"
            Me.XrLabel_Client.SizeF = New System.Drawing.SizeF(150.0!, 17.0!)
            Me.XrLabel_Client.StylePriority.UseFont = False
            '
            'XrLabel_due_date
            '
            Me.XrLabel_due_date.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "CalculatedField_due_date", "Due date: {0} of each month." & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Please mail 5 days in advance.")})
            Me.XrLabel_due_date.LocationFloat = New DevExpress.Utils.PointFloat(8.0!, 58.0!)
            Me.XrLabel_due_date.Multiline = True
            Me.XrLabel_due_date.Name = "XrLabel_due_date"
            Me.XrLabel_due_date.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_due_date.SizeF = New System.Drawing.SizeF(283.0!, 33.0!)
            '
            'XrLabel9
            '
            Me.XrLabel9.LocationFloat = New DevExpress.Utils.PointFloat(8.0!, 33.0!)
            Me.XrLabel9.Name = "XrLabel9"
            Me.XrLabel9.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel9.SizeF = New System.Drawing.SizeF(108.0!, 17.0!)
            Me.XrLabel9.Text = "Deposit Amount:"
            '
            'XrLabel8
            '
            Me.XrLabel8.LocationFloat = New DevExpress.Utils.PointFloat(8.0!, 9.0!)
            Me.XrLabel8.Name = "XrLabel8"
            Me.XrLabel8.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel8.SizeF = New System.Drawing.SizeF(50.0!, 17.0!)
            Me.XrLabel8.Text = "Client:"
            '
            'XrLabel_ClientNameAddress
            '
            Me.XrLabel_ClientNameAddress.LocationFloat = New DevExpress.Utils.PointFloat(50.0!, 208.0!)
            Me.XrLabel_ClientNameAddress.Multiline = True
            Me.XrLabel_ClientNameAddress.Name = "XrLabel_ClientNameAddress"
            Me.XrLabel_ClientNameAddress.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_ClientNameAddress.Scripts.OnBeforePrint = "XrLabel_ClientNameAddress_BeforePrint"
            Me.XrLabel_ClientNameAddress.SizeF = New System.Drawing.SizeF(225.0!, 108.0!)
            Me.XrLabel_ClientNameAddress.Text = "Client Name And Address"
            '
            'XrRichText1
            '
            Me.XrRichText1.LocationFloat = New DevExpress.Utils.PointFloat(312.5!, 0.0!)
            Me.XrRichText1.Name = "XrRichText1"
            Me.XrRichText1.Padding = New DevExpress.XtraPrinting.PaddingInfo(5, 5, 5, 5, 100.0!)
            Me.XrRichText1.SerializableRtfString = resources.GetString("XrRichText1.SerializableRtfString")
            Me.XrRichText1.SizeF = New System.Drawing.SizeF(178.1249!, 58.0!)
            Me.XrRichText1.StylePriority.UsePadding = False
            '
            'GroupHeader1
            '
            Me.GroupHeader1.GroupFields.AddRange(New DevExpress.XtraReports.UI.GroupField() {New DevExpress.XtraReports.UI.GroupField("client", DevExpress.XtraReports.UI.XRColumnSortOrder.Ascending)})
            Me.GroupHeader1.GroupUnion = DevExpress.XtraReports.UI.GroupUnion.WholePage
            Me.GroupHeader1.HeightF = 0.0!
            Me.GroupHeader1.Name = "GroupHeader1"
            Me.GroupHeader1.PageBreak = DevExpress.XtraReports.UI.PageBreak.BeforeBand
            '
            'GroupFooter1
            '
            Me.GroupFooter1.HeightF = 0.0!
            Me.GroupFooter1.Name = "GroupFooter1"
            Me.GroupFooter1.PrintAtBottom = True
            '
            'CalculatedField_due_date
            '
            Me.CalculatedField_due_date.FieldType = DevExpress.XtraReports.UI.FieldType.[String]
            Me.CalculatedField_due_date.Name = "CalculatedField_due_date"
            Me.CalculatedField_due_date.Scripts.OnGetValue = "CalculatedField_due_date_GetValue"
            '
            'ParameterClient
            '
            Me.ParameterClient.Name = "ParameterClient"
            Me.ParameterClient.Type = GetType(Integer)
            Me.ParameterClient.Value = -1
            Me.ParameterClient.Visible = False
            '
            'ClientDepositCouponReport
            '
            Me.Bands.AddRange(New DevExpress.XtraReports.UI.Band() {Me.TopMarginBand1, Me.Detail, Me.BottomMarginBand1, Me.GroupHeader1, Me.GroupFooter1})
            Me.CalculatedFields.AddRange(New DevExpress.XtraReports.UI.CalculatedField() {Me.CalculatedField_due_date})
            Me.Parameters.AddRange(New DevExpress.XtraReports.Parameters.Parameter() {Me.ParameterClient})
            Me.RequestParameters = False
            Me.ScriptLanguage = DevExpress.XtraReports.ScriptLanguage.VisualBasic
            Me.ScriptReferencesString = resources.GetString("$this.ScriptReferencesString")
            Me.Scripts.OnBeforePrint = "ClientDepositCouponReport_BeforePrint"
            Me.ScriptsSource = resources.GetString("$this.ScriptsSource")
            Me.Version = "11.2"
            Me.Controls.SetChildIndex(Me.GroupFooter1, 0)
            Me.Controls.SetChildIndex(Me.GroupHeader1, 0)
            Me.Controls.SetChildIndex(Me.BottomMarginBand1, 0)
            Me.Controls.SetChildIndex(Me.Detail, 0)
            Me.Controls.SetChildIndex(Me.TopMarginBand1, 0)
            CType(Me.XrRichText1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me, System.ComponentModel.ISupportInitialize).EndInit()

        End Sub

        Friend WithEvents GroupHeader1 As DevExpress.XtraReports.UI.GroupHeaderBand
        Friend WithEvents GroupFooter1 As DevExpress.XtraReports.UI.GroupFooterBand
        Friend WithEvents XrRichText1 As DevExpress.XtraReports.UI.XRRichText
        Friend WithEvents XrLabel_ClientNameAddress As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrPanel4 As DevExpress.XtraReports.UI.XRPanel
        Friend WithEvents XrLabel1_deposit_amount As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_Client As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_due_date As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel9 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel8 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_BarCode As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_CutHere As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents CalculatedField_due_date As DevExpress.XtraReports.UI.CalculatedField
        Friend WithEvents ParameterClient As DevExpress.XtraReports.Parameters.Parameter
    End Class
End Namespace
