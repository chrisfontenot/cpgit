#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}
#End Region

Namespace Client.DepositCoupon
    Public Class ClientDepositCouponReport
        Implements DebtPlus.Interfaces.Client.IClient

        ''' <summary>
        '''     Create an instance of our report
        ''' </summary>
        Public Sub New()
            MyBase.New()
            InitializeComponent()

            ' Ensure that the parameters have their default values and not saved by the scripting.
            ClientID = -1

            AddHandler BeforePrint, AddressOf ClientDepositCouponReport_BeforePrint
            AddHandler XrLabel_ClientNameAddress.BeforePrint, AddressOf XrLabel_ClientNameAddress_BeforePrint
            AddHandler XrLabel_Client.BeforePrint, AddressOf XrLabel_Client_BeforePrint
            AddHandler XrLabel_CutHere.Draw, AddressOf XrLabel_CutHere_Draw
            AddHandler CalculatedField_due_date.GetValue, AddressOf CalculatedField_due_date_GetValue
            AddHandler XrLabel_BarCode.BeforePrint, AddressOf XrLabel_BarCode_BeforePrint
        End Sub

        Public Property ClientID As Int32 Implements DebtPlus.Interfaces.Client.IClient.ClientId
            Get
                Return Parameter_Client
            End Get
            Set(value As Int32)
                Parameter_Client = value
            End Set
        End Property

        Private Property Parameter_Client() As Int32
            Get
                Dim parm As DevExpress.XtraReports.Parameters.Parameter = FindParameter("ParameterClient")
                If parm Is Nothing Then Return -1
                Return Convert.ToInt32(parm.Value)
            End Get
            Set(ByVal value As Int32)
                SetParameter("ParameterClient", GetType(Int32), value, "Client ID", False)
            End Set
        End Property

        Public Overrides Sub SetReportParameter(ByVal Parameter As String, ByVal Value As Object)
            If Parameter = "Client" Then
                Parameter_Client = Convert.ToInt32(Value)
            Else
                MyBase.SetReportParameter(Parameter, Value)
            End If
        End Sub

        Public Overrides Function NeedParameters() As Boolean
            Return Parameter_Client < 0
        End Function

        Public Overrides Function RequestReportParameters() As System.Windows.Forms.DialogResult
            Dim Answer As System.Windows.Forms.DialogResult = DialogResult.OK
            If NeedParameters() Then
                Using frm As New DepositCouponParametersForm()
                    With frm
                        Answer = .ShowDialog
                        Parameter_Client = .Parameter_Client
                    End With
                End Using
            End If
            Return Answer
        End Function

        ' ********************************************* MOVED TO SCRIPTS ***************************************************

        Private Sub XrLabel_CutHere_Draw(ByVal sender As Object, ByVal e As DevExpress.XtraReports.UI.DrawEventArgs)
            Dim factor As Int32

            With CType(sender, DevExpress.XtraReports.UI.XRLabel)
                Dim rpt As DevExpress.XtraReports.UI.XtraReport = CType(.Report, DevExpress.XtraReports.UI.XtraReport)

                '-- Find a graphics drawing item for the desktop (general purpose)
                Using gr As Graphics = Graphics.FromHwnd(IntPtr.Zero)

                    '-- Erase the drawing rectangle
                    Using back_br As System.Drawing.SolidBrush = New System.Drawing.SolidBrush(e.Brick.BackColor)
                        gr.FillRectangle(back_br, e.Bounds)
                    End Using

                    '-- Determine the scaling factor for the report based upon US v.s. International
                    If rpt.ReportUnit = DevExpress.XtraReports.UI.ReportUnit.HundredthsOfAnInch Then
                        gr.PageUnit = GraphicsUnit.Inch
                        factor = 100
                    Else
                        gr.PageUnit = GraphicsUnit.Millimeter
                        factor = 10
                    End If

                    '-- Create a windings font for the first character of the pattern
                    Using WindingsFont As System.Drawing.Font = New System.Drawing.Font("Windings", 10, FontStyle.Regular, GraphicsUnit.Document, 1)

                        '-- This is the cut symbol from the windings font
                        Dim CutSymbol As String = Convert.ToString(""""c)

                        '-- Calculate the location for the text buffer
                        Dim charactersFitted As System.Int32 = 0
                        Dim linesFilled As System.Int32 = 0
                        Dim WindingsFontRectangle As System.Drawing.SizeF = gr.MeasureString(CutSymbol, WindingsFont, New System.Drawing.SizeF(CSng(e.Bounds.Width), CSng(e.Bounds.Height)), New System.Drawing.StringFormat(StringFormatFlags.NoWrap), charactersFitted, linesFilled)

                        '-- Draw the cut symbol at the location of the font
                        gr.DrawString(CutSymbol, WindingsFont, Brushes.Black, e.Bounds.X, e.Bounds.Y)

                        '-- Calculate the new location for the remainder of the string
                        Dim NewBounds As New System.Drawing.RectangleF(e.Bounds.X + (WindingsFontRectangle.Width * factor), e.Bounds.Y, e.Bounds.Width - (WindingsFontRectangle.Width * factor), e.Bounds.Height)

                        '-- Draw the remainder of the string
                        gr.DrawString(" Cut here", .Font, Brushes.Black, NewBounds.X, NewBounds.Y)

                        '-- Draw an underline below the text
                        Using pn As System.Drawing.Pen = New System.Drawing.Pen(Color.Black, 2)
                            gr.DrawLine(pn, New System.Drawing.PointF(e.Bounds.X, e.Bounds.Y + (WindingsFontRectangle.Height * factor) + 3), New System.Drawing.PointF(e.Bounds.X + e.Bounds.Width, e.Bounds.Y + (WindingsFontRectangle.Height * factor) + 3))
                        End Using
                    End Using
                End Using
            End With
        End Sub

        Dim ds As New System.Data.DataSet("ds")
        Private Sub ClientDepositCouponReport_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs)
            Dim rpt As DevExpress.XtraReports.UI.XtraReport = CType(sender, DevExpress.XtraReports.UI.XtraReport)
            Dim sqlInfo As DebtPlus.LINQ.SQLInfoClass = DebtPlus.LINQ.SQLInfoClass.getDefault()

            Dim current_cursor As System.Windows.Forms.Cursor = System.Windows.Forms.Cursor.Current
            Try
                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.WaitCursor
                Using cn As System.Data.SqlClient.SqlConnection = New System.Data.SqlClient.SqlConnection(sqlInfo.ConnectionString)
                    cn.Open()
                    Using cmd As New System.Data.SqlClient.SqlCommand()
                        cmd.Connection = cn
                        cmd.CommandText = "rpt_Deposit_Coupon"
                        cmd.CommandType = System.Data.CommandType.StoredProcedure
                        System.Data.SqlClient.SqlCommandBuilder.DeriveParameters(cmd)
                        cmd.CommandTimeout = 0

                        If Convert.ToInt32(rpt.Parameters("ParameterClient").Value) >= 0 Then
                            cmd.Parameters(1).Value = rpt.Parameters("ParameterClient").Value
                        End If

                        Using da As New System.Data.SqlClient.SqlDataAdapter(cmd)
                            da.Fill(ds, "deposit_coupon")
                        End Using
                    End Using
                End Using

                '-- Bind the data source to a new view by client
                rpt.DataSource = New System.Data.DataView(ds.Tables(0), String.Empty, "client", System.Data.DataViewRowState.CurrentRows)
                For Each fld As DevExpress.XtraReports.UI.CalculatedField In rpt.CalculatedFields
                    fld.Assign(rpt.DataSource, rpt.DataMember)
                Next

            Catch ex As System.Data.SqlClient.SqlException
                Using gdr As New DebtPlus.Repository.GetDataResult()
                    gdr.HandleException(ex)
                    DebtPlus.UI.Common.ErrorHandling.HandleErrors(gdr, "Error reading deposit information")
                End Using

            Finally
                System.Windows.Forms.Cursor.Current = current_cursor
            End Try
        End Sub

        Private Sub XrLabel_ClientNameAddress_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs)
            With CType(sender, DevExpress.XtraReports.UI.XRLabel)
                Dim rpt As DevExpress.XtraReports.UI.XtraReport = CType(.Report, DevExpress.XtraReports.UI.XtraReport)
                Dim sqlInfo As DebtPlus.LINQ.SQLInfoClass = DebtPlus.LINQ.SQLInfoClass.getDefault()
                Dim sb As New System.Text.StringBuilder

                Dim ClientID As System.Int32 = -1
                If rpt.GetCurrentColumnValue("client") IsNot Nothing AndAlso rpt.GetCurrentColumnValue("client") IsNot System.DBNull.Value Then ClientID = Convert.ToInt32(rpt.GetCurrentColumnValue("client"))

                '-- If we have not been initialized then do not do the report
                If sqlInfo IsNot Nothing AndAlso ClientID >= 0 Then
                    Dim rd As System.Data.SqlClient.SqlDataReader = Nothing
                    Dim current_cursor As System.Windows.Forms.Cursor = System.Windows.Forms.Cursor.Current
                    Try
                        System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.WaitCursor
                        Using cn As System.Data.SqlClient.SqlConnection = New System.Data.SqlClient.SqlConnection(sqlInfo.ConnectionString)
                            cn.Open()
                            Using cmd As New System.Data.SqlClient.SqlCommand()
                                cmd.Connection = cn
                                cmd.CommandText = "SELECT isnull(name,'') as name, isnull(addr1,'') as addr1, isnull(addr2,'') as addr2, isnull(addr3,'') as addr3 FROM view_client_address WITH (NOLOCK) WHERE client=@client"
                                cmd.CommandType = System.Data.CommandType.Text
                                cmd.Parameters.Add("@client", System.Data.SqlDbType.Int).Value = ClientID
                                rd = cmd.ExecuteReader(System.Data.CommandBehavior.CloseConnection Or System.Data.CommandBehavior.SingleRow)
                            End Using

                            '-- If the client ID is desired then it goes before the name
                            sb.Append(Environment.NewLine)
                            sb.Append(DebtPlus.Utils.Format.Client.FormatClientID(ClientID))

                            '-- Include the name and address information
                            If rd IsNot Nothing AndAlso rd.Read Then
                                For fldID As Integer = 0 To 3
                                    If Not rd.IsDBNull(fldID) Then
                                        Dim str As String = rd.GetString(fldID).Trim
                                        If str <> String.Empty Then
                                            sb.Append(System.Environment.NewLine)
                                            sb.Append(str)
                                        End If
                                    End If
                                Next
                                If sb.Length > 0 Then
                                    sb.Remove(0, 2)
                                End If
                            End If
                        End Using

                    Catch ex As System.Data.SqlClient.SqlException
                        Using gdr As New DebtPlus.Repository.GetDataResult()
                            gdr.HandleException(ex)
                            DebtPlus.UI.Common.ErrorHandling.HandleErrors(gdr, "Error reading deposit information")
                        End Using

                    Finally
                        If rd IsNot Nothing Then
                            rd.Dispose()
                        End If
                        System.Windows.Forms.Cursor.Current = current_cursor
                    End Try
                End If

                .Text = sb.ToString
            End With
        End Sub

        Private Sub XrLabel_Client_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs)
            With CType(sender, DevExpress.XtraReports.UI.XRLabel)
                Dim rpt As DevExpress.XtraReports.UI.XtraReport = CType(.Report, DevExpress.XtraReports.UI.XtraReport)
                .Text = DebtPlus.Utils.Format.Client.FormatClientID(rpt.GetCurrentColumnValue("client"))
            End With
        End Sub

        Private Sub CalculatedField_due_date_GetValue(ByVal sender As Object, ByVal e As DevExpress.XtraReports.UI.GetValueEventArgs)
            With CType(sender, DevExpress.XtraReports.UI.CalculatedField)
                Dim formattedDay As String = String.Empty
                Dim drv As System.Data.DataRowView = CType(e.Row, System.Data.DataRowView)
                Dim obj As Object = drv("deposit_day")

                If obj IsNot Nothing AndAlso obj IsNot System.DBNull.Value Then
                    Dim DepositDay As System.Int32 = Convert.ToInt32(obj)
                    If DepositDay > 0 Then
                        If DepositDay >= 10 And DepositDay < 20 Then
                            formattedDay = String.Format("{0:f0}th", DepositDay)
                        Else
                            Select Case Convert.ToInt32(DepositDay) Mod 10
                                Case 1
                                    formattedDay = String.Format("{0:f0}st", DepositDay)
                                Case 2
                                    formattedDay = String.Format("{0:f0}nd", DepositDay)
                                Case 3
                                    formattedDay = String.Format("{0:f0}rd", DepositDay)
                                Case Else
                                    formattedDay = String.Format("{0:f0}th", DepositDay)
                            End Select
                        End If
                    End If
                End If

                e.Value = formattedDay
            End With
        End Sub

        Private Sub XrLabel_BarCode_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs)
            With CType(sender, DevExpress.XtraReports.UI.XRLabel)
                Dim rpt As DevExpress.XtraReports.UI.XtraReport = CType(.Report, DevExpress.XtraReports.UI.XtraReport)
                Dim Answer As String = "00001247"      '-- Canned value for the company id

                '-- The client ID follows
                Dim Client As Int32 = Convert.ToInt32(rpt.GetCurrentColumnValue("client"))
                If Client < 0 Then
                    Client = 0
                End If

                Dim TempString As String = String.Format("{0:000000000}", Client)
                TempString += ChecksumRL(TempString)
                Answer += " " + TempString

                '-- The normal deposit amount comes next
                Dim Amount As Decimal = Convert.ToDecimal(rpt.GetCurrentColumnValue("deposit_amount"))
                If Amount < 0D Then
                    Amount = 0D
                End If

                Dim Cents As Int64 = Convert.ToInt64(Amount * 100D)
                TempString = String.Format("{0:000000000}", Cents)
                TempString += ChecksumRL(TempString)
                Answer += " " + TempString

                '-- The total amount owing comes last
                '-- Since we don't track that for the coupon, just use the same deposit amount
                Answer += " " + TempString

                .Text = Answer
            End With
        End Sub

        Private Shared Function ChecksumRL(ByVal InputString As String) As String
            Dim Checksum As Int32 = 0
            Dim bytes() As Byte = (New System.Text.ASCIIEncoding).GetBytes(InputString)
            Dim Multiplier As Int32 = 2
            For index As Integer = bytes.GetUpperBound(0) To bytes.GetLowerBound(0) Step -1
                Dim Value As Int32 = Convert.ToInt32(bytes(index)) - &H30
                Multiplier = 3 - Multiplier
                Value = Value * Multiplier
                Checksum += Value
            Next
            If Checksum >= 10 Then
                Checksum = Checksum Mod 10
            End If
            Return String.Format("{0:0}", Checksum)
        End Function

#If 0 Then ' this is used if the company id is variable.
    Private Shared Function ChecksumLR(ByVal InputString As String) As String
        Dim Checksum As Int32 = 0
        Dim bytes() As Byte = (New System.Text.ASCIIEncoding).GetBytes(InputString)
        Dim Multiplier As Int32 = 1
        For index As Integer = bytes.GetLowerBound(0) To bytes.GetUpperBound(0)
            Dim Value As Int32 = Convert.ToInt32(bytes(index)) - &H30
            Multiplier = 3 - Multiplier
            Value = Value * Multiplier
            Checksum += Value
        Next
        If Checksum >= 10 Then
            Checksum = Checksum Mod 10
        End If
        Return String.Format("{0:0}", Checksum)
    End Function
#End If
    End Class
End Namespace
