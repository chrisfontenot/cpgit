
Imports DebtPlus.UI.Client.Widgets.Controls

Namespace Client.DepositCoupon
    <Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
    Partial Class DepositCouponParametersForm

        'Form overrides dispose to clean up the component list.
        <System.Diagnostics.DebuggerNonUserCode()> _
        Protected Overrides Sub Dispose(ByVal disposing As Boolean)
            Try
                If disposing AndAlso components IsNot Nothing Then
                    components.Dispose()
                End If
            Finally
                MyBase.Dispose(disposing)
            End Try
        End Sub

        'Required by the Windows Form Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Windows Form Designer
        'It can be modified using the Windows Form Designer.  
        'Do not modify it using the code editor.
        <System.Diagnostics.DebuggerStepThrough()> _
        Private Sub InitializeComponent()
            Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(RPPS.Responses.RequestParametersForm))
            Dim SerializableAppearanceObject1 As DevExpress.Utils.SerializableAppearanceObject = New DevExpress.Utils.SerializableAppearanceObject
            Me.CheckEdit_All = New DevExpress.XtraEditors.CheckEdit
            Me.CheckEdit_Specific = New DevExpress.XtraEditors.CheckEdit
            Me.ClientID1 = New DebtPlus.UI.Client.Widgets.Controls.ClientID
            CType(Me.DxErrorProvider1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.CheckEdit_All.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.CheckEdit_Specific.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.ClientID1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.SuspendLayout()
            '
            'ButtonOK
            '
            Me.ButtonOK.TabIndex = 3
            '
            'ButtonCancel
            '
            Me.ButtonCancel.TabIndex = 4
            '
            'CheckEdit_All
            '
            Me.CheckEdit_All.Location = New System.Drawing.Point(4, 50)
            Me.CheckEdit_All.Name = "CheckEdit_All"
            Me.CheckEdit_All.Properties.Caption = "Use all Active Clients"
            Me.CheckEdit_All.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
            Me.CheckEdit_All.Properties.RadioGroupIndex = 0
            Me.CheckEdit_All.Size = New System.Drawing.Size(151, 19)
            Me.CheckEdit_All.TabIndex = 2
            Me.CheckEdit_All.TabStop = False
            '
            'CheckEdit_Specific
            '
            Me.CheckEdit_Specific.EditValue = True
            Me.CheckEdit_Specific.Location = New System.Drawing.Point(4, 24)
            Me.CheckEdit_Specific.Name = "CheckEdit_Specific"
            Me.CheckEdit_Specific.Properties.Caption = "Specific client ID:"
            Me.CheckEdit_Specific.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
            Me.CheckEdit_Specific.Properties.RadioGroupIndex = 0
            Me.CheckEdit_Specific.Size = New System.Drawing.Size(114, 19)
            Me.CheckEdit_Specific.TabIndex = 0
            '
            'ClientID1
            '
            Me.ClientID1.Location = New System.Drawing.Point(124, 23)
            Me.ClientID1.Name = "ClientID1"
            Me.ClientID1.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[False]
            Me.ClientID1.Properties.Appearance.Options.UseTextOptions = True
            Me.ClientID1.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.ClientID1.Properties.Buttons.Add(New DebtPlus.UI.Common.Controls.SearchButton())
            Me.ClientID1.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
            Me.ClientID1.Properties.DisplayFormat.FormatString = "0000000"
            Me.ClientID1.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom
            Me.ClientID1.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.ClientID1.Properties.Mask.BeepOnError = True
            Me.ClientID1.Properties.Mask.EditMask = "\d*"
            Me.ClientID1.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx
            Me.ClientID1.Properties.ValidateOnEnterKey = True
            Me.ClientID1.Size = New System.Drawing.Size(100, 20)
            Me.ClientID1.TabIndex = 1
            '
            'RequestParametersForm
            '
            Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
            Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
            Me.ClientSize = New System.Drawing.Size(336, 135)
            Me.Controls.Add(Me.ClientID1)
            Me.Controls.Add(Me.CheckEdit_Specific)
            Me.Controls.Add(Me.CheckEdit_All)
            Me.Name = "DepositCouponParametersForm"
            Me.Controls.SetChildIndex(Me.CheckEdit_All, 0)
            Me.Controls.SetChildIndex(Me.ButtonOK, 0)
            Me.Controls.SetChildIndex(Me.ButtonCancel, 0)
            Me.Controls.SetChildIndex(Me.CheckEdit_Specific, 0)
            Me.Controls.SetChildIndex(Me.ClientID1, 0)
            CType(Me.DxErrorProvider1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.CheckEdit_All.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.CheckEdit_Specific.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.ClientID1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            Me.ResumeLayout(False)

        End Sub
        Friend WithEvents CheckEdit_All As DevExpress.XtraEditors.CheckEdit
        Friend WithEvents CheckEdit_Specific As DevExpress.XtraEditors.CheckEdit
        Friend WithEvents ClientID1 As DebtPlus.UI.Client.Widgets.Controls.ClientID
    End Class
End Namespace
