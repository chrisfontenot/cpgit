#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}
#End Region

Namespace Client.DepositCoupon

    Friend Class DepositCouponParametersForm
        Inherits DebtPlus.Reports.Template.Forms.ReportParametersForm

        Public Sub New()
            MyBase.New()
            InitializeComponent()
            AddHandler Load, AddressOf Form_Load
            AddHandler ClientID1.EditValueChanged, AddressOf ClientID1_EditValueChanged
            AddHandler CheckEdit_All.CheckedChanged, AddressOf CheckEdit_CheckedChanged
            AddHandler CheckEdit_Specific.CheckedChanged, AddressOf CheckEdit_CheckedChanged
        End Sub

        Public Property Parameter_Client() As Int32
            Get
                If Not CheckEdit_All.Checked Then
                    Dim Answer As Int32 = ClientID1.EditValue.GetValueOrDefault(-1)
                    If Answer >= 0 Then
                        Return Answer
                    End If
                End If

                Return -1
            End Get
            Set(ByVal value As Int32)
                ClientID1.EditValue = value
            End Set
        End Property

        Protected Overrides Function HasErrors() As Boolean
            Dim Answer As Boolean = True

            ' Accept all clients if so desired
            If CheckEdit_All.Checked Then
                Answer = False
            ElseIf Parameter_Client > 0 Then
                Answer = False
            End If

            Return Answer
        End Function

        Private Sub Form_Load(ByVal sender As Object, ByVal e As System.EventArgs)

            ' Set the radio button values
            If Parameter_Client < 0 Then
                CheckEdit_Specific.Checked = False
                CheckEdit_All.Checked = True
                ClientID1.Enabled = False
            Else
                ClientID1.Enabled = True
                CheckEdit_All.Checked = False
                CheckEdit_Specific.Checked = True
                ClientID1.EditValue = DebtPlus.Utils.Nulls.v_Int32(Parameter_Client)
            End If

            ' If there is an error then inhibit the OK button.
            ButtonOK.Enabled = Not HasErrors()
        End Sub

        Private Sub CheckEdit_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
            ClientID1.Enabled = CheckEdit_Specific.Checked
            ButtonOK.Enabled = Not HasErrors()
        End Sub

        Private Sub ClientID1_EditValueChanged(ByVal sender As Object, ByVal e As System.EventArgs)
            ButtonOK.Enabled = Not HasErrors()
        End Sub
    End Class
End Namespace