#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}
#End Region

Option Compare Binary
Option Explicit On
Option Strict On

Imports System.Reflection

Public Module ReportLoader
    Public Function LoadReport(ByVal typeName As String) As DebtPlus.Interfaces.Reports.IReports
        Return LoadReport(typeName, String.Empty)
    End Function

    Public Function LoadReport(ByVal typeName As String, ByVal argumentString As String) As DebtPlus.Interfaces.Reports.IReports
        Dim rptInstance As DebtPlus.Interfaces.Reports.IReports = Nothing

        ' Find the report in the list of types for the system
        Dim reportType As System.Type = DebtPlus.Utils.Modules.FindTypeFromName(typeName)
        If reportType IsNot Nothing Then

            ' A parameter string may be passed to the "New" function so handle that condition here.
            If Not String.IsNullOrEmpty(argumentString) Then
                Dim constructorInfoObj As ConstructorInfo = reportType.GetConstructor(New System.Type() {GetType(System.String)})
                If constructorInfoObj IsNot Nothing Then
                    rptInstance = TryCast(constructorInfoObj.Invoke(New Object() {argumentString}), DebtPlus.Interfaces.Reports.IReports)
                End If

            Else

                ' There is no parameter string. Just use the standard "New" function
                Dim constructorInfoObj As ConstructorInfo = reportType.GetConstructor(New System.Type() {})
                If constructorInfoObj IsNot Nothing Then
                    rptInstance = TryCast(constructorInfoObj.Invoke(Nothing), DebtPlus.Interfaces.Reports.IReports)
                End If
            End If
        End If

        Return rptInstance
    End Function
End Module
