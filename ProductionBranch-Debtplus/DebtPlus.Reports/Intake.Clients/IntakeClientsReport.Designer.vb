﻿Namespace Intake.Clients
    <Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
    Partial Class IntakeClientsReport
        Inherits DebtPlus.Reports.Template.TemplateXtraReportClass

        'XtraReport overrides dispose to clean up the component list.
        <System.Diagnostics.DebuggerNonUserCode()> _
        Protected Overrides Sub Dispose(ByVal disposing As Boolean)
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
            MyBase.Dispose(disposing)
        End Sub

        'Required by the Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Designer
        'It can be modified using the Designer.  
        'Do not modify it using the code editor.
        <System.Diagnostics.DebuggerStepThrough()> _
        Private Sub InitializeComponent()
            Dim XrSummary1 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary
            Me.XrPanel1 = New DevExpress.XtraReports.UI.XRPanel
            Me.XrLabel6 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel5 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel3 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel2 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel1 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel4 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_intake_client = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_home_ph = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_name_and_address = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_start_date = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_active_status = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_client_status = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_intake_id = New DevExpress.XtraReports.UI.XRLabel
            Me.GroupHeader1 = New DevExpress.XtraReports.UI.GroupHeaderBand
            Me.XrLabel_group_header_state = New DevExpress.XtraReports.UI.XRLabel
            Me.GroupFooter1 = New DevExpress.XtraReports.UI.GroupFooterBand
            Me.XrLabel_goop_footer_count = New DevExpress.XtraReports.UI.XRLabel
            Me.FromDate = New DevExpress.XtraReports.Parameters.Parameter
            Me.ToDate = New DevExpress.XtraReports.Parameters.Parameter
            CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
            '
            'Detail
            '
            Me.Detail.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel_intake_id, Me.XrLabel_client_status, Me.XrLabel_active_status, Me.XrLabel_start_date, Me.XrLabel_home_ph, Me.XrLabel_intake_client, Me.XrLabel_name_and_address})
            Me.Detail.Height = 35
            '
            'PageHeader
            '
            Me.PageHeader.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrPanel1})
            Me.PageHeader.Height = 154
            '
            'XrPanel1
            '
            Me.XrPanel1.BackColor = System.Drawing.Color.Teal
            Me.XrPanel1.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel6, Me.XrLabel5, Me.XrLabel3, Me.XrLabel2, Me.XrLabel1, Me.XrLabel4})
            Me.XrPanel1.Location = New System.Drawing.Point(0, 133)
            Me.XrPanel1.Name = "XrPanel1"
            Me.XrPanel1.Size = New System.Drawing.Size(750, 17)
            Me.XrPanel1.StylePriority.UseBackColor = False
            '
            'XrLabel6
            '
            Me.XrLabel6.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel6.ForeColor = System.Drawing.Color.White
            Me.XrLabel6.Location = New System.Drawing.Point(683, 0)
            Me.XrLabel6.Name = "XrLabel6"
            Me.XrLabel6.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel6.Size = New System.Drawing.Size(66, 15)
            Me.XrLabel6.StylePriority.UseFont = False
            Me.XrLabel6.StylePriority.UseForeColor = False
            Me.XrLabel6.StylePriority.UseTextAlignment = False
            Me.XrLabel6.Text = "ID"
            Me.XrLabel6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
            '
            'XrLabel5
            '
            Me.XrLabel5.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel5.ForeColor = System.Drawing.Color.White
            Me.XrLabel5.Location = New System.Drawing.Point(533, 0)
            Me.XrLabel5.Name = "XrLabel5"
            Me.XrLabel5.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel5.Size = New System.Drawing.Size(92, 15)
            Me.XrLabel5.StylePriority.UseFont = False
            Me.XrLabel5.StylePriority.UseForeColor = False
            Me.XrLabel5.StylePriority.UseTextAlignment = False
            Me.XrLabel5.Text = "PHONE"
            Me.XrLabel5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
            '
            'XrLabel3
            '
            Me.XrLabel3.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel3.ForeColor = System.Drawing.Color.White
            Me.XrLabel3.Location = New System.Drawing.Point(167, 0)
            Me.XrLabel3.Name = "XrLabel3"
            Me.XrLabel3.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel3.Size = New System.Drawing.Size(66, 15)
            Me.XrLabel3.StylePriority.UseFont = False
            Me.XrLabel3.StylePriority.UseForeColor = False
            Me.XrLabel3.StylePriority.UseTextAlignment = False
            Me.XrLabel3.Text = "START"
            Me.XrLabel3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
            '
            'XrLabel2
            '
            Me.XrLabel2.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel2.ForeColor = System.Drawing.Color.White
            Me.XrLabel2.Location = New System.Drawing.Point(75, 0)
            Me.XrLabel2.Name = "XrLabel2"
            Me.XrLabel2.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel2.Size = New System.Drawing.Size(75, 15)
            Me.XrLabel2.StylePriority.UseFont = False
            Me.XrLabel2.StylePriority.UseForeColor = False
            Me.XrLabel2.StylePriority.UseTextAlignment = False
            Me.XrLabel2.Text = "STATUS"
            Me.XrLabel2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
            '
            'XrLabel1
            '
            Me.XrLabel1.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel1.ForeColor = System.Drawing.Color.White
            Me.XrLabel1.Location = New System.Drawing.Point(8, 0)
            Me.XrLabel1.Name = "XrLabel1"
            Me.XrLabel1.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel1.Size = New System.Drawing.Size(58, 15)
            Me.XrLabel1.StylePriority.UseFont = False
            Me.XrLabel1.StylePriority.UseForeColor = False
            Me.XrLabel1.StylePriority.UseTextAlignment = False
            Me.XrLabel1.Text = "CLIENT"
            Me.XrLabel1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
            '
            'XrLabel4
            '
            Me.XrLabel4.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel4.ForeColor = System.Drawing.Color.White
            Me.XrLabel4.Location = New System.Drawing.Point(242, 0)
            Me.XrLabel4.Name = "XrLabel4"
            Me.XrLabel4.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel4.Size = New System.Drawing.Size(275, 15)
            Me.XrLabel4.StylePriority.UseFont = False
            Me.XrLabel4.StylePriority.UseForeColor = False
            Me.XrLabel4.Text = "NAME AND ADDRESS"
            '
            'XrLabel_intake_client
            '
            Me.XrLabel_intake_client.CanGrow = False
            Me.XrLabel_intake_client.Font = New System.Drawing.Font("Times New Roman", 9.75!)
            Me.XrLabel_intake_client.ForeColor = System.Drawing.SystemColors.WindowText
            Me.XrLabel_intake_client.Location = New System.Drawing.Point(6, 0)
            Me.XrLabel_intake_client.Name = "XrLabel_intake_client"
            Me.XrLabel_intake_client.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_intake_client.Size = New System.Drawing.Size(60, 15)
            Me.XrLabel_intake_client.StylePriority.UseFont = False
            Me.XrLabel_intake_client.StylePriority.UseForeColor = False
            Me.XrLabel_intake_client.StylePriority.UseTextAlignment = False
            Me.XrLabel_intake_client.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
            '
            'XrLabel_home_ph
            '
            Me.XrLabel_home_ph.CanGrow = False
            Me.XrLabel_home_ph.Font = New System.Drawing.Font("Times New Roman", 9.75!)
            Me.XrLabel_home_ph.ForeColor = System.Drawing.SystemColors.WindowText
            Me.XrLabel_home_ph.Location = New System.Drawing.Point(533, 0)
            Me.XrLabel_home_ph.Name = "XrLabel_home_ph"
            Me.XrLabel_home_ph.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_home_ph.Size = New System.Drawing.Size(92, 15)
            Me.XrLabel_home_ph.StylePriority.UseFont = False
            Me.XrLabel_home_ph.StylePriority.UseForeColor = False
            Me.XrLabel_home_ph.StylePriority.UseTextAlignment = False
            Me.XrLabel_home_ph.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
            '
            'XrLabel_name_and_address
            '
            Me.XrLabel_name_and_address.Font = New System.Drawing.Font("Times New Roman", 9.75!)
            Me.XrLabel_name_and_address.ForeColor = System.Drawing.SystemColors.WindowText
            Me.XrLabel_name_and_address.KeepTogether = True
            Me.XrLabel_name_and_address.Location = New System.Drawing.Point(242, 0)
            Me.XrLabel_name_and_address.Multiline = True
            Me.XrLabel_name_and_address.Name = "XrLabel_name_and_address"
            Me.XrLabel_name_and_address.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_name_and_address.Size = New System.Drawing.Size(283, 15)
            Me.XrLabel_name_and_address.StylePriority.UseFont = False
            Me.XrLabel_name_and_address.StylePriority.UseForeColor = False
            '
            'XrLabel_start_date
            '
            Me.XrLabel_start_date.CanGrow = False
            Me.XrLabel_start_date.Font = New System.Drawing.Font("Times New Roman", 9.75!)
            Me.XrLabel_start_date.ForeColor = System.Drawing.SystemColors.WindowText
            Me.XrLabel_start_date.Location = New System.Drawing.Point(167, 0)
            Me.XrLabel_start_date.Name = "XrLabel_start_date"
            Me.XrLabel_start_date.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_start_date.Size = New System.Drawing.Size(67, 15)
            Me.XrLabel_start_date.StylePriority.UseFont = False
            Me.XrLabel_start_date.StylePriority.UseForeColor = False
            Me.XrLabel_start_date.StylePriority.UseTextAlignment = False
            Me.XrLabel_start_date.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
            '
            'XrLabel_active_status
            '
            Me.XrLabel_active_status.CanGrow = False
            Me.XrLabel_active_status.Font = New System.Drawing.Font("Times New Roman", 9.75!)
            Me.XrLabel_active_status.ForeColor = System.Drawing.SystemColors.WindowText
            Me.XrLabel_active_status.Location = New System.Drawing.Point(75, 0)
            Me.XrLabel_active_status.Name = "XrLabel_active_status"
            Me.XrLabel_active_status.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_active_status.Size = New System.Drawing.Size(42, 15)
            Me.XrLabel_active_status.StylePriority.UseFont = False
            Me.XrLabel_active_status.StylePriority.UseForeColor = False
            Me.XrLabel_active_status.StylePriority.UseTextAlignment = False
            Me.XrLabel_active_status.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
            '
            'XrLabel_client_status
            '
            Me.XrLabel_client_status.CanGrow = False
            Me.XrLabel_client_status.Font = New System.Drawing.Font("Times New Roman", 9.75!)
            Me.XrLabel_client_status.ForeColor = System.Drawing.SystemColors.WindowText
            Me.XrLabel_client_status.Location = New System.Drawing.Point(117, 0)
            Me.XrLabel_client_status.Name = "XrLabel_client_status"
            Me.XrLabel_client_status.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_client_status.Size = New System.Drawing.Size(42, 15)
            Me.XrLabel_client_status.StylePriority.UseFont = False
            Me.XrLabel_client_status.StylePriority.UseForeColor = False
            '
            'XrLabel_intake_id
            '
            Me.XrLabel_intake_id.CanGrow = False
            Me.XrLabel_intake_id.Font = New System.Drawing.Font("Times New Roman", 9.75!)
            Me.XrLabel_intake_id.ForeColor = System.Drawing.SystemColors.WindowText
            Me.XrLabel_intake_id.Location = New System.Drawing.Point(633, 0)
            Me.XrLabel_intake_id.Name = "XrLabel_intake_id"
            Me.XrLabel_intake_id.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_intake_id.Size = New System.Drawing.Size(116, 15)
            Me.XrLabel_intake_id.StylePriority.UseFont = False
            Me.XrLabel_intake_id.StylePriority.UseForeColor = False
            Me.XrLabel_intake_id.StylePriority.UseTextAlignment = False
            Me.XrLabel_intake_id.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
            '
            'GroupHeader1
            '
            Me.GroupHeader1.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel_group_header_state})
            Me.GroupHeader1.GroupUnion = DevExpress.XtraReports.UI.GroupUnion.WithFirstDetail
            Me.GroupHeader1.Height = 42
            Me.GroupHeader1.Name = "GroupHeader1"
            '
            'XrLabel_group_header_state
            '
            Me.XrLabel_group_header_state.Font = New System.Drawing.Font("Times New Roman", 12.0!, System.Drawing.FontStyle.Bold)
            Me.XrLabel_group_header_state.ForeColor = System.Drawing.Color.Maroon
            Me.XrLabel_group_header_state.Location = New System.Drawing.Point(0, 8)
            Me.XrLabel_group_header_state.Name = "XrLabel_group_header_state"
            Me.XrLabel_group_header_state.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_group_header_state.Size = New System.Drawing.Size(750, 25)
            Me.XrLabel_group_header_state.StylePriority.UseFont = False
            Me.XrLabel_group_header_state.StylePriority.UseForeColor = False
            '
            'GroupFooter1
            '
            Me.GroupFooter1.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel_goop_footer_count})
            Me.GroupFooter1.Height = 42
            Me.GroupFooter1.Name = "GroupFooter1"
            '
            'XrLabel_goop_footer_count
            '
            Me.XrLabel_goop_footer_count.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel_goop_footer_count.ForeColor = System.Drawing.Color.Teal
            Me.XrLabel_goop_footer_count.Location = New System.Drawing.Point(0, 8)
            Me.XrLabel_goop_footer_count.Name = "XrLabel_goop_footer_count"
            Me.XrLabel_goop_footer_count.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_goop_footer_count.Size = New System.Drawing.Size(750, 17)
            Me.XrLabel_goop_footer_count.StylePriority.UseFont = False
            Me.XrLabel_goop_footer_count.StylePriority.UseForeColor = False
            XrSummary1.FormatString = "Number of clients: {0:n0}"
            XrSummary1.Func = DevExpress.XtraReports.UI.SummaryFunc.Count
            XrSummary1.Running = DevExpress.XtraReports.UI.SummaryRunning.Group
            Me.XrLabel_goop_footer_count.Summary = XrSummary1
            '
            'FromDate
            '
            Me.FromDate.Description = "Starting Date"
            Me.FromDate.Name = "FromDate"
            Me.FromDate.Type = GetType(System.DateTime)
            Me.FromDate.Value = New Date(CType(0, Long))
            '
            'ToDate
            '
            Me.ToDate.Description = "Ending Date"
            Me.ToDate.Name = "ToDate"
            Me.ToDate.Type = GetType(System.DateTime)
            Me.ToDate.Value = New Date(CType(0, Long))
            '
            'IntakeClientsReport
            '
            Me.Bands.AddRange(New DevExpress.XtraReports.UI.Band() {Me.GroupHeader1, Me.GroupFooter1})
            Me.Parameters.AddRange(New DevExpress.XtraReports.Parameters.Parameter() {Me.FromDate, Me.ToDate})
            Me.RequestParameters = False
            Me.ScriptLanguage = DevExpress.XtraReports.ScriptLanguage.VisualBasic
            Me.Version = "8.2"
            CType(Me, System.ComponentModel.ISupportInitialize).EndInit()

        End Sub
        Friend WithEvents XrPanel1 As DevExpress.XtraReports.UI.XRPanel
        Friend WithEvents XrLabel6 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel5 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel4 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel3 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel2 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel1 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_active_status As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_start_date As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_name_and_address As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_home_ph As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_intake_client As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_client_status As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_intake_id As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents GroupHeader1 As DevExpress.XtraReports.UI.GroupHeaderBand
        Friend WithEvents XrLabel_group_header_state As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents GroupFooter1 As DevExpress.XtraReports.UI.GroupFooterBand
        Friend WithEvents XrLabel_goop_footer_count As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents FromDate As DevExpress.XtraReports.Parameters.Parameter
        Friend WithEvents ToDate As DevExpress.XtraReports.Parameters.Parameter
    End Class
End Namespace