#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}

#End Region

Option Compare Binary
Option Explicit On
Option Strict On

Imports System.ComponentModel

Imports System.Drawing.Printing
Imports System.Data.SqlClient
Imports DevExpress.XtraReports.UI
Imports System.Text

Namespace Intake.Clients
    Public Class IntakeClientsReport

        ''' <summary>
        ''' Report Title
        ''' </summary>
        Public Overrides ReadOnly Property ReportTitle() As String
            Get
                Return "WWW Intake Clients"
            End Get
        End Property


        ''' <summary>
        ''' Create an instance of our report
        ''' </summary>
        Public Sub New()
            MyBase.New()
            InitializeComponent()
            AddHandler Me.BeforePrint, AddressOf IntakeClientsReport_BeforePrint
            AddHandler XrLabel_name_and_address.BeforePrint, AddressOf XrLabel_name_and_address_BeforePrint
            AddHandler XrLabel_intake_client.BeforePrint, AddressOf XrLabel_intake_client_BeforePrint
            AddHandler XrLabel_group_header_state.BeforePrint, AddressOf XrLabel_group_header_state_BeforePrint
        End Sub

        ''' <summary>
        ''' Load the report on the first instance
        ''' </summary>
        Dim tbl As DataTable = Nothing

        Private Sub IntakeClientsReport_BeforePrint(ByVal sender As Object, ByVal e As PrintEventArgs)
            Dim cn As SqlConnection = New System.Data.SqlClient.SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
            Dim cmd As SqlCommand = New System.Data.SqlClient.SqlCommand()
            With cmd
                .Connection = cn
                .CommandText = "rpt_intake_clients"
                .CommandType = CommandType.StoredProcedure
                .CommandTimeout = 0
            End With

            ' Set the group fields by state
            GroupHeader1.GroupFields.Add(New GroupField("state", XRColumnSortOrder.Ascending))

            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet("clients")
            da.Fill(ds)
            tbl = ds.Tables(0)

            Dim vue As DataView = New DataView(tbl, String.Empty, "state, client, date_created", DataViewRowState.CurrentRows)
            DataSource = vue

            With XrLabel_start_date
                .DataBindings.Add("Text", vue, "start_date", "{0:d}")
            End With

            With XrLabel_home_ph
                .DataBindings.Add("Text", vue, "phone")
            End With

            With XrLabel_intake_client
                .DataBindings.Add("Text", vue, "intake_client", "{0:f0}")
            End With

            With XrLabel_intake_id
                .DataBindings.Add("Text", vue, "intake_id")
            End With

            With XrLabel_client_status
                .DataBindings.Add("Text", vue, "client_status")
            End With

            With XrLabel_active_status
                .DataBindings.Add("Text", vue, "active_status")
            End With
        End Sub

        Private Sub XrLabel_name_and_address_BeforePrint(ByVal sender As Object, ByVal e As PrintEventArgs)
            Dim sb As New StringBuilder

            For Each FieldName As String In New String() {"name", "addr1", "addr2", "addr3"}
                Dim Value As Object = GetCurrentColumnValue(FieldName)
                If Value IsNot Nothing AndAlso Value IsNot DBNull.Value Then
                    sb.Append(Environment.NewLine)
                    sb.Append(Convert.ToString(Value))
                End If
            Next

            If sb.Length > 0 Then sb.Remove(0, 2)

            With CType(sender, XRLabel)
                .Text = sb.ToString()
            End With
        End Sub

        Private Sub XrLabel_intake_client_BeforePrint(ByVal sender As Object, ByVal e As PrintEventArgs)
            With CType(sender, XRLabel)
                .Text = String.Format("{0:0000000}", Convert.ToInt32(Me.GetCurrentColumnValue("client")))
            End With
        End Sub

        Private Sub XrLabel_group_header_state_BeforePrint(ByVal sender As Object, ByVal e As PrintEventArgs)
            With CType(sender, XRLabel)
                .Text = "Clients in state: " + Convert.ToString(GetCurrentColumnValue("state"))
            End With
        End Sub
    End Class
End Namespace
