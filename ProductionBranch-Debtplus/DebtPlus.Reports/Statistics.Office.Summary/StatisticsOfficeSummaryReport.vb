#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}
#End Region

Option Compare Binary
Option Explicit On
Option Strict On

Imports DevExpress.XtraPivotGrid
Imports DevExpress.XtraReports.UI.PivotGrid

Namespace Statistics.Office.Summary
    Public Class StatisticsCounselorReport
        Inherits Template.TemplateXtraReportClass

        ''' <summary>
        ''' Create an instance of our report
        ''' </summary>
        Public Sub New()
            MyBase.new()
            InitializeComponent()
            AddHandler Me.BeforePrint, AddressOf TrustBalanceReportClass_BeforePrint
        End Sub

#Region " Component Designer generated code "

        'Component overrides dispose to clean up the component list.
        Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
            If disposing Then
                If components IsNot Nothing Then
                    components.Dispose()
                End If
            End If
            MyBase.Dispose(disposing)
        End Sub
        Friend WithEvents XrPivotGrid1 As DevExpress.XtraReports.UI.XRPivotGrid

        'Required by the Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Designer
        'It can be modified using the Designer.
        'Do not modify it using the code editor.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
            Me.XrLabel1 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel2 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel3 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel4 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel5 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrPivotGrid1 = New DevExpress.XtraReports.UI.XRPivotGrid
            CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
            '
            'Detail
            '
            Me.Detail.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrPivotGrid1})
            Me.Detail.HeightF = 70.0!
            '
            'XrLabel_Title
            '
            Me.XrLabel_Title.StylePriority.UseFont = False
            '
            'XrLabel_Subtitle
            '
            Me.XrLabel_Subtitle.LocationFloat = New DevExpress.Utils.PointFloat(7.999992!, 85.00001!)
            Me.XrLabel_Subtitle.SizeF = New System.Drawing.SizeF(706.0!, 17.00001!)
            '
            'XrPageInfo_PageNumber
            '
            Me.XrPageInfo_PageNumber.LocationFloat = New DevExpress.Utils.PointFloat(926.5833!, 10.00001!)
            Me.XrPageInfo_PageNumber.SizeF = New System.Drawing.SizeF(113.4167!, 17.00001!)
            Me.XrPageInfo_PageNumber.StylePriority.UseTextAlignment = False
            '
            'XrPanel_AgencyAddress
            '
            Me.XrPanel_AgencyAddress.LocationFloat = New DevExpress.Utils.PointFloat(732.0!, 0.0!)
            '
            'XRLabel_Agency_Name
            '
            Me.XRLabel_Agency_Name.StylePriority.UseTextAlignment = False
            '
            'XrLabel_Agency_Address3
            '
            Me.XrLabel_Agency_Address3.StylePriority.UseFont = False
            Me.XrLabel_Agency_Address3.StylePriority.UseTextAlignment = False
            '
            'XrLabel_Agency_Address1
            '
            Me.XrLabel_Agency_Address1.StylePriority.UseFont = False
            Me.XrLabel_Agency_Address1.StylePriority.UseTextAlignment = False
            '
            'XrLabel_Agency_Phone
            '
            Me.XrLabel_Agency_Phone.StylePriority.UseFont = False
            Me.XrLabel_Agency_Phone.StylePriority.UseTextAlignment = False
            '
            'XrLabel_Agency_Address2
            '
            Me.XrLabel_Agency_Address2.StylePriority.UseFont = False
            Me.XrLabel_Agency_Address2.StylePriority.UseTextAlignment = False
            '
            'XrLabel1
            '
            Me.XrLabel1.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
            Me.XrLabel1.Name = "XrLabel1"
            Me.XrLabel1.SizeF = New System.Drawing.SizeF(100.0!, 23.0!)
            Me.XrLabel1.Text = "XrLabel1"
            '
            'XrLabel2
            '
            Me.XrLabel2.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
            Me.XrLabel2.Name = "XrLabel2"
            Me.XrLabel2.SizeF = New System.Drawing.SizeF(100.0!, 23.0!)
            Me.XrLabel2.Text = "XrLabel2"
            '
            'XrLabel3
            '
            Me.XrLabel3.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
            Me.XrLabel3.Name = "XrLabel3"
            Me.XrLabel3.SizeF = New System.Drawing.SizeF(100.0!, 23.0!)
            Me.XrLabel3.Text = "XrLabel3"
            '
            'XrLabel4
            '
            Me.XrLabel4.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
            Me.XrLabel4.Name = "XrLabel4"
            Me.XrLabel4.SizeF = New System.Drawing.SizeF(100.0!, 23.0!)
            Me.XrLabel4.Text = "XrLabel4"
            '
            'XrLabel5
            '
            Me.XrLabel5.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
            Me.XrLabel5.Name = "XrLabel5"
            Me.XrLabel5.SizeF = New System.Drawing.SizeF(100.0!, 23.0!)
            Me.XrLabel5.Text = "XrLabel5"
            '
            'XrPivotGrid1
            '
            Me.XrPivotGrid1.FieldHeaderStyleName = "XrControlStyle_Totals"
            Me.XrPivotGrid1.FieldValueGrandTotalStyleName = "XrControlStyle_Totals"
            Me.XrPivotGrid1.GrandTotalCellStyleName = "XrControlStyle_Totals"
            Me.XrPivotGrid1.HeaderGroupLineStyleName = "XrControlStyle_HeaderPannel"
            Me.XrPivotGrid1.LocationFloat = New DevExpress.Utils.PointFloat(10.00001!, 10.00001!)
            Me.XrPivotGrid1.Name = "XrPivotGrid1"
            Me.XrPivotGrid1.SizeF = New System.Drawing.SizeF(1030.0!, 50.0!)
            Me.XrPivotGrid1.TotalCellStyleName = "XrControlStyle_Totals"
            '
            'StatisticsCounselorReport
            '
            Me.Bands.AddRange(New DevExpress.XtraReports.UI.Band() {Me.Detail, Me.PageHeader, Me.PageFooter})
            Me.Landscape = True
            Me.PageHeight = 850
            Me.PageWidth = 1100
            Me.Version = "10.1"
            CType(Me, System.ComponentModel.ISupportInitialize).EndInit()

        End Sub

        Friend WithEvents XrLabel1 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel2 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel3 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel4 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel5 As DevExpress.XtraReports.UI.XRLabel
#End Region

        ''' <summary>
        ''' Report Title string
        ''' </summary>
        Public Overrides ReadOnly Property ReportTitle() As String
            Get
                Return "Office Statistics Summary"
            End Get
        End Property

        Private privateSubtitle As String = String.Empty
        Public Overrides ReadOnly Property ReportSubTitle() As String
            Get
                Return privateSubtitle
            End Get
        End Property

        Dim ds As New System.Data.DataSet("ds")

        ''' <summary>
        ''' Bind the dataset to the report
        ''' </summary>
        Private Sub TrustBalanceReportClass_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs)
            Dim tbl As System.Data.DataTable = ds.Tables("rpt_statistics_offices")
            If tbl Is Nothing Then
                Using cmd As SqlClient.SqlCommand = New System.Data.SqlClient.SqlCommand()
                    With cmd
                        .Connection = New System.Data.SqlClient.SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
                        .CommandTimeout = 0
                        .CommandType = CommandType.StoredProcedure
                        .CommandText = "rpt_statistics_offices"
                    End With

                    Using da As New SqlClient.SqlDataAdapter(cmd)
                        da.Fill(ds, "rpt_statistics_offices")
                        tbl = ds.Tables("rpt_statistics_offices")
                    End Using
                End Using
            End If

            If tbl IsNot Nothing Then

                ' Find the last date referenced
                Dim MaxDate As Object = tbl.Compute("max(period_start)", String.Empty)
                If MaxDate Is System.DBNull.Value Then MaxDate = Now.Date

                ' Go back 5 months from that date
                Dim cutoff_date As DateTime = Convert.ToDateTime(MaxDate).Date.AddMonths(-6)
                privateSubtitle = String.Format("This report covers dates from {0:d} through {1:d}", cutoff_date, MaxDate)
                Dim vue As New System.Data.DataView(tbl, String.Format("[period_start]>='{0:d}'", cutoff_date), "period_start", DataViewRowState.CurrentRows)

                ' Set the datasource for the limited period
                XrPivotGrid1.DataSource = vue

                ' Create column fields and bind them to the 'OrderDate' datasource field.
                Dim fieldYear As PivotGridField = New PivotGridField("period_start", PivotArea.ColumnArea)
                Dim fieldMonth As PivotGridField = New PivotGridField("period_start", PivotArea.ColumnArea)

                ' Add the fields to the field collection.
                XrPivotGrid1.Fields.Add(fieldYear)
                XrPivotGrid1.Fields.Add(fieldMonth)

                ' Set the caption and group mode of the fields.
                fieldYear.GroupInterval = PivotGroupInterval.DateYear
                fieldYear.Caption = "Year"
                fieldMonth.GroupInterval = PivotGroupInterval.DateMonth
                fieldMonth.Caption = "Month"

                Dim Name_Field As New XRPivotGridField("name", PivotArea.RowArea)
                XrPivotGrid1.Fields.Add(Name_Field)

                Dim Actual_Disbursement_Field As New XRPivotGridField("actual_disbursement", PivotArea.DataArea)
                XrPivotGrid1.Fields.Add(Actual_Disbursement_Field)
            End If
        End Sub
    End Class

End Namespace
