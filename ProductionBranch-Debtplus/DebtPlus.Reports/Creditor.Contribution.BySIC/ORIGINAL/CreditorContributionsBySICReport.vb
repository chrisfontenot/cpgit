#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}
#End Region

Option Compare Binary
Option Explicit On 
Option Strict On

Imports DevExpress.XtraReports.UI

Namespace Creditor.Contribution.BySIC
    Public Class CreditorContributionsBySICReport

        ''' <summary>
        ''' Create an instance of our report
        ''' </summary>
        Public Sub New()
            MyBase.New()
            InitializeComponent()
            AddHandler Me.BeforePrint, AddressOf CreditorContributionsBySICReport_BeforePrint
            AddHandler XrLabel_sic.BeforePrint, AddressOf XrLabel_sic_BeforePrint
        End Sub

        Public Overrides ReadOnly Property ReportTitle() As String
            Get
                Return "Creditor Contributions"
            End Get
        End Property

        Protected ds As New System.Data.DataSet("ds")

        Protected Overridable Sub CreditorContributionsBySICReport_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs)
            Const TableName As String = "rpt_Creditor_Contributions"
            Dim rpt As DevExpress.XtraReports.UI.XtraReport = CType(sender, DevExpress.XtraReports.UI.XtraReport)
            Dim sqlInfo As DebtPlus.LINQ.SQLInfoClass = DebtPlus.LINQ.SQLInfoClass.getDefault()

            Using cn As System.Data.SqlClient.SqlConnection = New System.Data.SqlClient.SqlConnection(sqlInfo.ConnectionString)
                cn.Open()
                Using cmd As New System.Data.SqlClient.SqlCommand()
                    cmd.Connection = cn
                    cmd.CommandText = "rpt_Creditor_Contributions"
                    cmd.CommandType = System.Data.CommandType.StoredProcedure
                    System.Data.SqlClient.SqlCommandBuilder.DeriveParameters(cmd)

                    cmd.Parameters(1).Value = rpt.Parameters("ParameterFromDate").Value
                    cmd.Parameters(2).Value = rpt.Parameters("ParameterToDate").Value
                    cmd.CommandTimeout = 0

                    Using da As New System.Data.SqlClient.SqlDataAdapter(cmd)
                        da.Fill(ds, TableName)
                    End Using
                End Using
            End Using

            Dim tbl As System.Data.DataTable = ds.Tables(TableName)
            tbl.Columns.Add("total", GetType(System.Decimal), "[receipts]+[deducted]")
            rpt.DataSource = New System.Data.DataView(tbl, String.Empty, "sic, creditor_name", System.Data.DataViewRowState.CurrentRows)
        End Sub

        Protected Overridable Sub XrLabel_sic_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs)
            Dim lbl As DevExpress.XtraReports.UI.XRLabel = CType(sender, DevExpress.XtraReports.UI.XRLabel)
            Dim rpt As DevExpress.XtraReports.UI.XtraReport = CType(lbl.Report, DevExpress.XtraReports.UI.XtraReport)
            lbl.Text = FormatSIC(rpt.GetCurrentColumnValue("sic"))
        End Sub

        Public Function FormatSIC(ByVal arg As Object) As String
            Dim Answer As String = String.Empty
            If arg IsNot Nothing AndAlso arg IsNot System.DBNull.Value Then
                Dim SicCode As String = Convert.ToString(arg).Trim
                If SicCode <> String.Empty Then
                    Dim ChrLeft As Char = SicCode.ToCharArray(0, 1)(0)
                    If Char.IsLetter(ChrLeft) Then
                        SicCode = SicCode.Substring(1)
                        Answer = ChrLeft
                    End If
                    SicCode = SicCode.PadRight(10, "0"c)
                    Answer += SicCode.Substring(0, 4) + "-" + SicCode.Substring(4, 2) + "-" + SicCode.Substring(6)
                End If
            End If

            Return Answer
        End Function
    End Class
End Namespace
