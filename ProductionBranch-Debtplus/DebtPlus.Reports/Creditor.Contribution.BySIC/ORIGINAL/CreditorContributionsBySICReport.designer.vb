Namespace Creditor.Contribution.BySIC
    Partial Class CreditorContributionsBySICReport
        Inherits DebtPlus.Reports.Template.DatedTemplateXtraReportClass

        'Component overrides dispose to clean up the component list.
        Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
            If disposing Then
                If components IsNot Nothing Then
                    components.Dispose()
                End If
            End If
            MyBase.Dispose(disposing)
        End Sub

        'Required by the Designer
        Private components As System.ComponentModel.IContainer

        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
            Dim XrSummary1 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary()
            Dim XrSummary2 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary()
            Dim XrSummary3 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary()
            Dim XrSummary4 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary()
            Dim XrSummary5 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary()
            Dim XrSummary6 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary()
            Dim XrSummary7 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary()
            Dim XrSummary8 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary()
            Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(CreditorContributionsBySICReport))
            Me.XrLabel_sic = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_creditor_name = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_billed = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_deducted = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_receipts = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_total = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrPanel1 = New DevExpress.XtraReports.UI.XRPanel()
            Me.XrLabel4 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel8 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel7 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel6 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel5 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel2 = New DevExpress.XtraReports.UI.XRLabel()
            Me.GroupHeader1 = New DevExpress.XtraReports.UI.GroupHeaderBand()
            Me.GroupFooter1 = New DevExpress.XtraReports.UI.GroupFooterBand()
            Me.XrLabel_group_billed = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_group_receipts = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_group_deducted = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_group_total = New DevExpress.XtraReports.UI.XRLabel()
            Me.ReportFooter = New DevExpress.XtraReports.UI.ReportFooterBand()
            Me.XrLine1 = New DevExpress.XtraReports.UI.XRLine()
            Me.XrLabel1 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_grand_receipts = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_grand_total = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_grand_billed = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_grand_deducted = New DevExpress.XtraReports.UI.XRLabel()
            CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
            '
            'PageHeader
            '
            Me.PageHeader.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrPanel1})
            Me.PageHeader.HeightF = 160.0!
            Me.PageHeader.Controls.SetChildIndex(Me.XrPanel1, 0)
            Me.PageHeader.Controls.SetChildIndex(Me.XrPanel_AgencyAddress, 0)
            Me.PageHeader.Controls.SetChildIndex(Me.XrLabel_Title, 0)
            Me.PageHeader.Controls.SetChildIndex(Me.XrLine_Title, 0)
            Me.PageHeader.Controls.SetChildIndex(Me.XrLabel_Subtitle, 0)
            Me.PageHeader.Controls.SetChildIndex(Me.XrPageInfo1, 0)
            '
            'XrLabel_Title
            '
            Me.XrLabel_Title.StylePriority.UseFont = False
            '
            'XrPageInfo_PageNumber
            '
            Me.XrPageInfo_PageNumber.StylePriority.UseTextAlignment = False
            '
            'XRLabel_Agency_Name
            '
            Me.XRLabel_Agency_Name.StylePriority.UseTextAlignment = False
            '
            'XrLabel_Agency_Address3
            '
            Me.XrLabel_Agency_Address3.StylePriority.UseFont = False
            Me.XrLabel_Agency_Address3.StylePriority.UseTextAlignment = False
            '
            'XrLabel_Agency_Address1
            '
            Me.XrLabel_Agency_Address1.StylePriority.UseFont = False
            Me.XrLabel_Agency_Address1.StylePriority.UseTextAlignment = False
            '
            'XrLabel_Agency_Phone
            '
            Me.XrLabel_Agency_Phone.StylePriority.UseFont = False
            Me.XrLabel_Agency_Phone.StylePriority.UseTextAlignment = False
            '
            'XrLabel_Agency_Address2
            '
            Me.XrLabel_Agency_Address2.StylePriority.UseFont = False
            Me.XrLabel_Agency_Address2.StylePriority.UseTextAlignment = False
            '
            'Detail
            '
            Me.Detail.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel_total, Me.XrLabel_receipts, Me.XrLabel_deducted, Me.XrLabel_billed, Me.XrLabel_creditor_name, Me.XrLabel_sic})
            Me.Detail.HeightF = 15.0!
            '
            'XrLabel_sic
            '
            Me.XrLabel_sic.CanGrow = False
            Me.XrLabel_sic.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
            Me.XrLabel_sic.Name = "XrLabel_sic"
            Me.XrLabel_sic.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_sic.ProcessDuplicates = DevExpress.XtraReports.UI.ValueSuppressType.Suppress
            Me.XrLabel_sic.Scripts.OnBeforePrint = "XrLabel_sic_BeforePrint"
            Me.XrLabel_sic.SizeF = New System.Drawing.SizeF(100.0!, 15.0!)
            Me.XrLabel_sic.StylePriority.UsePadding = False
            Me.XrLabel_sic.StylePriority.UseTextAlignment = False
            Me.XrLabel_sic.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
            Me.XrLabel_sic.WordWrap = False
            '
            'XrLabel_creditor_name
            '
            Me.XrLabel_creditor_name.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "creditor_name")})
            Me.XrLabel_creditor_name.LocationFloat = New DevExpress.Utils.PointFloat(108.0!, 0.0!)
            Me.XrLabel_creditor_name.Name = "XrLabel_creditor_name"
            Me.XrLabel_creditor_name.SizeF = New System.Drawing.SizeF(258.0!, 15.0!)
            Me.XrLabel_creditor_name.WordWrap = False
            '
            'XrLabel_billed
            '
            Me.XrLabel_billed.CanGrow = False
            Me.XrLabel_billed.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "billed", "{0:c}")})
            Me.XrLabel_billed.LocationFloat = New DevExpress.Utils.PointFloat(375.0!, 0.0!)
            Me.XrLabel_billed.Name = "XrLabel_billed"
            Me.XrLabel_billed.SizeF = New System.Drawing.SizeF(85.0!, 15.0!)
            Me.XrLabel_billed.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
            Me.XrLabel_billed.WordWrap = False
            '
            'XrLabel_deducted
            '
            Me.XrLabel_deducted.CanGrow = False
            Me.XrLabel_deducted.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "deducted", "{0:c}")})
            Me.XrLabel_deducted.LocationFloat = New DevExpress.Utils.PointFloat(466.0!, 0.0!)
            Me.XrLabel_deducted.Name = "XrLabel_deducted"
            Me.XrLabel_deducted.SizeF = New System.Drawing.SizeF(91.0!, 15.0!)
            Me.XrLabel_deducted.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
            Me.XrLabel_deducted.WordWrap = False
            '
            'XrLabel_receipts
            '
            Me.XrLabel_receipts.CanGrow = False
            Me.XrLabel_receipts.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "receipts", "{0:c}")})
            Me.XrLabel_receipts.LocationFloat = New DevExpress.Utils.PointFloat(583.0!, 0.0!)
            Me.XrLabel_receipts.Name = "XrLabel_receipts"
            Me.XrLabel_receipts.SizeF = New System.Drawing.SizeF(75.0!, 15.0!)
            Me.XrLabel_receipts.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
            Me.XrLabel_receipts.WordWrap = False
            '
            'XrLabel_total
            '
            Me.XrLabel_total.CanGrow = False
            Me.XrLabel_total.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "total", "{0:c}")})
            Me.XrLabel_total.LocationFloat = New DevExpress.Utils.PointFloat(683.0!, 0.0!)
            Me.XrLabel_total.Name = "XrLabel_total"
            Me.XrLabel_total.SizeF = New System.Drawing.SizeF(91.0!, 15.0!)
            Me.XrLabel_total.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
            Me.XrLabel_total.WordWrap = False
            '
            'XrPanel1
            '
            Me.XrPanel1.BackColor = System.Drawing.Color.Teal
            Me.XrPanel1.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel4, Me.XrLabel8, Me.XrLabel7, Me.XrLabel6, Me.XrLabel5, Me.XrLabel2})
            Me.XrPanel1.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 133.0!)
            Me.XrPanel1.Name = "XrPanel1"
            Me.XrPanel1.SizeF = New System.Drawing.SizeF(800.0!, 17.0!)
            Me.XrPanel1.StyleName = "XrControlStyle_HeaderPannel"
            '
            'XrLabel4
            '
            Me.XrLabel4.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel4.ForeColor = System.Drawing.Color.White
            Me.XrLabel4.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 1.0!)
            Me.XrLabel4.Name = "XrLabel4"
            Me.XrLabel4.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel4.SizeF = New System.Drawing.SizeF(100.0!, 15.0!)
            Me.XrLabel4.StylePriority.UsePadding = False
            Me.XrLabel4.StylePriority.UseTextAlignment = False
            Me.XrLabel4.Text = "SIC CODE"
            Me.XrLabel4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
            '
            'XrLabel8
            '
            Me.XrLabel8.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel8.ForeColor = System.Drawing.Color.White
            Me.XrLabel8.LocationFloat = New DevExpress.Utils.PointFloat(466.0!, 1.0!)
            Me.XrLabel8.Name = "XrLabel8"
            Me.XrLabel8.SizeF = New System.Drawing.SizeF(91.0!, 15.0!)
            Me.XrLabel8.Text = "DEDUCTED"
            Me.XrLabel8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
            '
            'XrLabel7
            '
            Me.XrLabel7.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel7.ForeColor = System.Drawing.Color.White
            Me.XrLabel7.LocationFloat = New DevExpress.Utils.PointFloat(375.0!, 1.0!)
            Me.XrLabel7.Name = "XrLabel7"
            Me.XrLabel7.SizeF = New System.Drawing.SizeF(85.0!, 15.0!)
            Me.XrLabel7.Text = "INVOICED"
            Me.XrLabel7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
            '
            'XrLabel6
            '
            Me.XrLabel6.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel6.ForeColor = System.Drawing.Color.White
            Me.XrLabel6.LocationFloat = New DevExpress.Utils.PointFloat(683.0!, 1.0!)
            Me.XrLabel6.Name = "XrLabel6"
            Me.XrLabel6.SizeF = New System.Drawing.SizeF(91.0!, 15.0!)
            Me.XrLabel6.Text = "TOTAL"
            Me.XrLabel6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
            '
            'XrLabel5
            '
            Me.XrLabel5.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel5.ForeColor = System.Drawing.Color.White
            Me.XrLabel5.LocationFloat = New DevExpress.Utils.PointFloat(583.0!, 1.0!)
            Me.XrLabel5.Name = "XrLabel5"
            Me.XrLabel5.SizeF = New System.Drawing.SizeF(75.0!, 15.0!)
            Me.XrLabel5.Text = "RECEIPTS"
            Me.XrLabel5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
            '
            'XrLabel2
            '
            Me.XrLabel2.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel2.ForeColor = System.Drawing.Color.White
            Me.XrLabel2.LocationFloat = New DevExpress.Utils.PointFloat(108.0!, 1.0!)
            Me.XrLabel2.Name = "XrLabel2"
            Me.XrLabel2.SizeF = New System.Drawing.SizeF(250.0!, 15.0!)
            Me.XrLabel2.StylePriority.UseTextAlignment = False
            Me.XrLabel2.Text = "CREDITOR"
            Me.XrLabel2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
            '
            'GroupHeader1
            '
            Me.GroupHeader1.GroupFields.AddRange(New DevExpress.XtraReports.UI.GroupField() {New DevExpress.XtraReports.UI.GroupField("sic", DevExpress.XtraReports.UI.XRColumnSortOrder.Ascending)})
            Me.GroupHeader1.HeightF = 0.0!
            Me.GroupHeader1.Name = "GroupHeader1"
            '
            'GroupFooter1
            '
            Me.GroupFooter1.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel_group_billed, Me.XrLabel_group_receipts, Me.XrLabel_group_deducted, Me.XrLabel_group_total})
            Me.GroupFooter1.HeightF = 27.0!
            Me.GroupFooter1.Name = "GroupFooter1"
            Me.GroupFooter1.StyleName = "XrControlStyle_Totals"
            '
            'XrLabel_group_billed
            '
            Me.XrLabel_group_billed.Borders = DevExpress.XtraPrinting.BorderSide.Top
            Me.XrLabel_group_billed.BorderWidth = 2
            Me.XrLabel_group_billed.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "billed")})
            Me.XrLabel_group_billed.LocationFloat = New DevExpress.Utils.PointFloat(375.0!, 0.0!)
            Me.XrLabel_group_billed.Name = "XrLabel_group_billed"
            Me.XrLabel_group_billed.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_group_billed.SizeF = New System.Drawing.SizeF(85.0!, 15.0!)
            Me.XrLabel_group_billed.StylePriority.UseBorders = False
            Me.XrLabel_group_billed.StylePriority.UseBorderWidth = False
            XrSummary1.FormatString = "{0:c}"
            XrSummary1.Running = DevExpress.XtraReports.UI.SummaryRunning.Group
            Me.XrLabel_group_billed.Summary = XrSummary1
            Me.XrLabel_group_billed.Text = "$0.00"
            Me.XrLabel_group_billed.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
            '
            'XrLabel_group_receipts
            '
            Me.XrLabel_group_receipts.Borders = DevExpress.XtraPrinting.BorderSide.Top
            Me.XrLabel_group_receipts.BorderWidth = 2
            Me.XrLabel_group_receipts.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "receipts")})
            Me.XrLabel_group_receipts.LocationFloat = New DevExpress.Utils.PointFloat(583.0!, 0.0!)
            Me.XrLabel_group_receipts.Name = "XrLabel_group_receipts"
            Me.XrLabel_group_receipts.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_group_receipts.SizeF = New System.Drawing.SizeF(75.0!, 15.0!)
            Me.XrLabel_group_receipts.StylePriority.UseBorders = False
            Me.XrLabel_group_receipts.StylePriority.UseBorderWidth = False
            XrSummary2.FormatString = "{0:c}"
            XrSummary2.Running = DevExpress.XtraReports.UI.SummaryRunning.Group
            Me.XrLabel_group_receipts.Summary = XrSummary2
            Me.XrLabel_group_receipts.Text = "$0.00"
            Me.XrLabel_group_receipts.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
            '
            'XrLabel_group_deducted
            '
            Me.XrLabel_group_deducted.Borders = DevExpress.XtraPrinting.BorderSide.Top
            Me.XrLabel_group_deducted.BorderWidth = 2
            Me.XrLabel_group_deducted.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "deducted")})
            Me.XrLabel_group_deducted.LocationFloat = New DevExpress.Utils.PointFloat(467.0!, 0.0!)
            Me.XrLabel_group_deducted.Name = "XrLabel_group_deducted"
            Me.XrLabel_group_deducted.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_group_deducted.SizeF = New System.Drawing.SizeF(91.0!, 15.0!)
            Me.XrLabel_group_deducted.StylePriority.UseBorders = False
            Me.XrLabel_group_deducted.StylePriority.UseBorderWidth = False
            XrSummary3.FormatString = "{0:c}"
            XrSummary3.Running = DevExpress.XtraReports.UI.SummaryRunning.Group
            Me.XrLabel_group_deducted.Summary = XrSummary3
            Me.XrLabel_group_deducted.Text = "$0.00"
            Me.XrLabel_group_deducted.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
            '
            'XrLabel_group_total
            '
            Me.XrLabel_group_total.Borders = DevExpress.XtraPrinting.BorderSide.Top
            Me.XrLabel_group_total.BorderWidth = 2
            Me.XrLabel_group_total.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "total")})
            Me.XrLabel_group_total.LocationFloat = New DevExpress.Utils.PointFloat(667.0!, 0.0!)
            Me.XrLabel_group_total.Name = "XrLabel_group_total"
            Me.XrLabel_group_total.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_group_total.SizeF = New System.Drawing.SizeF(107.0!, 15.0!)
            Me.XrLabel_group_total.StylePriority.UseBorders = False
            Me.XrLabel_group_total.StylePriority.UseBorderWidth = False
            XrSummary4.FormatString = "{0:c}"
            XrSummary4.Running = DevExpress.XtraReports.UI.SummaryRunning.Group
            Me.XrLabel_group_total.Summary = XrSummary4
            Me.XrLabel_group_total.Text = "$0.00"
            Me.XrLabel_group_total.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
            '
            'ReportFooter
            '
            Me.ReportFooter.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLine1, Me.XrLabel1, Me.XrLabel_grand_receipts, Me.XrLabel_grand_total, Me.XrLabel_grand_billed, Me.XrLabel_grand_deducted})
            Me.ReportFooter.HeightF = 27.0!
            Me.ReportFooter.Name = "ReportFooter"
            Me.ReportFooter.StyleName = "XrControlStyle_Totals"
            '
            'XrLine1
            '
            Me.XrLine1.BorderWidth = 1
            Me.XrLine1.ForeColor = System.Drawing.Color.Teal
            Me.XrLine1.LineWidth = 3
            Me.XrLine1.LocationFloat = New DevExpress.Utils.PointFloat(375.0!, 0.0!)
            Me.XrLine1.Name = "XrLine1"
            Me.XrLine1.SizeF = New System.Drawing.SizeF(400.0!, 8.0!)
            Me.XrLine1.StylePriority.UseBorderWidth = False
            Me.XrLine1.StylePriority.UseForeColor = False
            '
            'XrLabel1
            '
            Me.XrLabel1.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 10.0!)
            Me.XrLabel1.Name = "XrLabel1"
            Me.XrLabel1.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel1.SizeF = New System.Drawing.SizeF(100.0!, 17.0!)
            Me.XrLabel1.StylePriority.UseTextAlignment = False
            Me.XrLabel1.Text = "TOTALS"
            Me.XrLabel1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
            '
            'XrLabel_grand_receipts
            '
            Me.XrLabel_grand_receipts.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "receipts")})
            Me.XrLabel_grand_receipts.LocationFloat = New DevExpress.Utils.PointFloat(558.0!, 10.0!)
            Me.XrLabel_grand_receipts.Name = "XrLabel_grand_receipts"
            Me.XrLabel_grand_receipts.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_grand_receipts.SizeF = New System.Drawing.SizeF(100.0!, 15.0!)
            XrSummary5.FormatString = "{0:c}"
            XrSummary5.Running = DevExpress.XtraReports.UI.SummaryRunning.Report
            Me.XrLabel_grand_receipts.Summary = XrSummary5
            Me.XrLabel_grand_receipts.Text = "$0.00"
            Me.XrLabel_grand_receipts.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
            '
            'XrLabel_grand_total
            '
            Me.XrLabel_grand_total.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "total")})
            Me.XrLabel_grand_total.LocationFloat = New DevExpress.Utils.PointFloat(667.0!, 10.0!)
            Me.XrLabel_grand_total.Name = "XrLabel_grand_total"
            Me.XrLabel_grand_total.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_grand_total.SizeF = New System.Drawing.SizeF(107.0!, 15.0!)
            XrSummary6.FormatString = "{0:c}"
            XrSummary6.Running = DevExpress.XtraReports.UI.SummaryRunning.Report
            Me.XrLabel_grand_total.Summary = XrSummary6
            Me.XrLabel_grand_total.Text = "$0.00"
            Me.XrLabel_grand_total.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
            '
            'XrLabel_grand_billed
            '
            Me.XrLabel_grand_billed.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "billed")})
            Me.XrLabel_grand_billed.LocationFloat = New DevExpress.Utils.PointFloat(375.0!, 10.0!)
            Me.XrLabel_grand_billed.Name = "XrLabel_grand_billed"
            Me.XrLabel_grand_billed.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_grand_billed.SizeF = New System.Drawing.SizeF(85.0!, 15.0!)
            XrSummary7.FormatString = "{0:c}"
            XrSummary7.Running = DevExpress.XtraReports.UI.SummaryRunning.Report
            Me.XrLabel_grand_billed.Summary = XrSummary7
            Me.XrLabel_grand_billed.Text = "$0.00"
            Me.XrLabel_grand_billed.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
            '
            'XrLabel_grand_deducted
            '
            Me.XrLabel_grand_deducted.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "deducted")})
            Me.XrLabel_grand_deducted.LocationFloat = New DevExpress.Utils.PointFloat(467.0!, 10.0!)
            Me.XrLabel_grand_deducted.Name = "XrLabel_grand_deducted"
            Me.XrLabel_grand_deducted.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_grand_deducted.SizeF = New System.Drawing.SizeF(91.0!, 15.0!)
            XrSummary8.FormatString = "{0:c}"
            XrSummary8.Running = DevExpress.XtraReports.UI.SummaryRunning.Report
            Me.XrLabel_grand_deducted.Summary = XrSummary8
            Me.XrLabel_grand_deducted.Text = "$0.00"
            Me.XrLabel_grand_deducted.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
            '
            'CreditorContributionsBySICReport
            '
            Me.Bands.AddRange(New DevExpress.XtraReports.UI.Band() {Me.Detail, Me.PageHeader, Me.PageFooter, Me.TopMarginBand1, Me.BottomMarginBand1, Me.GroupHeader1, Me.GroupFooter1, Me.ReportFooter})
            Me.ScriptLanguage = DevExpress.XtraReports.ScriptLanguage.VisualBasic
            Me.ScriptReferencesString = resources.GetString("$this.ScriptReferencesString")
            Me.Scripts.OnBeforePrint = "CreditorContributionsBySICReport_BeforePrint"
            Me.ScriptsSource = resources.GetString("$this.ScriptsSource")
            Me.StyleSheet.AddRange(New DevExpress.XtraReports.UI.XRControlStyle() {Me.XrControlStyle_Header, Me.XrControlStyle_Totals, Me.XrControlStyle_HeaderPannel, Me.XrControlStyle_GroupHeader})
            Me.Version = "11.2"
            Me.Controls.SetChildIndex(Me.ReportFooter, 0)
            Me.Controls.SetChildIndex(Me.GroupFooter1, 0)
            Me.Controls.SetChildIndex(Me.GroupHeader1, 0)
            Me.Controls.SetChildIndex(Me.BottomMarginBand1, 0)
            Me.Controls.SetChildIndex(Me.TopMarginBand1, 0)
            Me.Controls.SetChildIndex(Me.PageFooter, 0)
            Me.Controls.SetChildIndex(Me.PageHeader, 0)
            Me.Controls.SetChildIndex(Me.Detail, 0)
            CType(Me, System.ComponentModel.ISupportInitialize).EndInit()

        End Sub
        Protected Friend WithEvents XrLabel_sic As DevExpress.XtraReports.UI.XRLabel
        Protected Friend WithEvents XrLabel_creditor_name As DevExpress.XtraReports.UI.XRLabel
        Protected Friend WithEvents XrLabel_billed As DevExpress.XtraReports.UI.XRLabel
        Protected Friend WithEvents XrLabel_deducted As DevExpress.XtraReports.UI.XRLabel
        Protected Friend WithEvents XrLabel_receipts As DevExpress.XtraReports.UI.XRLabel
        Protected Friend WithEvents XrLabel_total As DevExpress.XtraReports.UI.XRLabel
        Protected Friend WithEvents XrPanel1 As DevExpress.XtraReports.UI.XRPanel
        Protected Friend WithEvents XrLabel2 As DevExpress.XtraReports.UI.XRLabel
        Protected Friend WithEvents XrLabel5 As DevExpress.XtraReports.UI.XRLabel
        Protected Friend WithEvents XrLabel6 As DevExpress.XtraReports.UI.XRLabel
        Protected Friend WithEvents XrLabel7 As DevExpress.XtraReports.UI.XRLabel
        Protected Friend WithEvents XrLabel8 As DevExpress.XtraReports.UI.XRLabel
        Protected Friend WithEvents XrLabel4 As DevExpress.XtraReports.UI.XRLabel
        Protected Friend WithEvents GroupHeader1 As DevExpress.XtraReports.UI.GroupHeaderBand
        Protected Friend WithEvents GroupFooter1 As DevExpress.XtraReports.UI.GroupFooterBand
        Protected Friend WithEvents XrLabel_group_billed As DevExpress.XtraReports.UI.XRLabel
        Protected Friend WithEvents XrLabel_group_receipts As DevExpress.XtraReports.UI.XRLabel
        Protected Friend WithEvents XrLabel_group_deducted As DevExpress.XtraReports.UI.XRLabel
        Protected Friend WithEvents XrLabel_group_total As DevExpress.XtraReports.UI.XRLabel
        Protected Friend WithEvents ReportFooter As DevExpress.XtraReports.UI.ReportFooterBand
        Protected Friend WithEvents XrLabel_grand_receipts As DevExpress.XtraReports.UI.XRLabel
        Protected Friend WithEvents XrLabel_grand_total As DevExpress.XtraReports.UI.XRLabel
        Protected Friend WithEvents XrLabel_grand_billed As DevExpress.XtraReports.UI.XRLabel
        Protected Friend WithEvents XrLabel_grand_deducted As DevExpress.XtraReports.UI.XRLabel
        Protected Friend WithEvents XrLine1 As DevExpress.XtraReports.UI.XRLine
        Protected Friend WithEvents XrLabel1 As DevExpress.XtraReports.UI.XRLabel
    End Class
End Namespace
