﻿Namespace Appointments.Schedule.Analysis
    <Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
    Partial Class ScheduleAnalysisReport
        Inherits DebtPlus.Reports.Template.DatedTemplateXtraReportClass

        'XtraReport overrides dispose to clean up the component list.
        <System.Diagnostics.DebuggerNonUserCode()> _
        Protected Overrides Sub Dispose(ByVal disposing As Boolean)
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
            MyBase.Dispose(disposing)
        End Sub

        'Required by the Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Designer
        'It can be modified using the Designer.  
        'Do not modify it using the code editor.
        <System.Diagnostics.DebuggerStepThrough()> _
        Private Sub InitializeComponent()
            Dim XrSummary1 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary()
            Dim XrSummary2 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary()
            Dim XrSummary3 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary()
            Dim XrSummary4 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary()
            Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(ScheduleAnalysisReport))
            Me.XrPanel1 = New DevExpress.XtraReports.UI.XRPanel()
            Me.XrLabel3 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel2 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel1 = New DevExpress.XtraReports.UI.XRLabel()
            Me.GroupHeader1 = New DevExpress.XtraReports.UI.GroupHeaderBand()
            Me.XrLabel_date = New DevExpress.XtraReports.UI.XRLabel()
            Me.GroupHeader2 = New DevExpress.XtraReports.UI.GroupHeaderBand()
            Me.XrLabel_office_name = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_appt_name = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_slots = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_kept = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_start_time = New DevExpress.XtraReports.UI.XRLabel()
            Me.GroupFooter1 = New DevExpress.XtraReports.UI.GroupFooterBand()
            Me.XrLine1 = New DevExpress.XtraReports.UI.XRLine()
            Me.XrLabel_slots_g1 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel12 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_kept_g1 = New DevExpress.XtraReports.UI.XRLabel()
            Me.GroupFooter2 = New DevExpress.XtraReports.UI.GroupFooterBand()
            Me.XrLabel_slots_g2 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel15 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_kept_g2 = New DevExpress.XtraReports.UI.XRLabel()
            Me.ParameterOffice = New DevExpress.XtraReports.Parameters.Parameter()
            Me.CalculatedFieldDate = New DevExpress.XtraReports.UI.CalculatedField()
            CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
            '
            'PageHeader
            '
            Me.PageHeader.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrPanel1})
            Me.PageHeader.HeightF = 137.4167!
            Me.PageHeader.Controls.SetChildIndex(Me.XrPanel1, 0)
            Me.PageHeader.Controls.SetChildIndex(Me.XrPanel_AgencyAddress, 0)
            Me.PageHeader.Controls.SetChildIndex(Me.XrLabel_Title, 0)
            Me.PageHeader.Controls.SetChildIndex(Me.XrLine_Title, 0)
            Me.PageHeader.Controls.SetChildIndex(Me.XrLabel_Subtitle, 0)
            Me.PageHeader.Controls.SetChildIndex(Me.XrPageInfo1, 0)
            '
            'XrLabel_Title
            '
            Me.XrLabel_Title.StylePriority.UseFont = False
            '
            'XrPageInfo_PageNumber
            '
            Me.XrPageInfo_PageNumber.StylePriority.UseTextAlignment = False
            '
            'XRLabel_Agency_Name
            '
            Me.XRLabel_Agency_Name.StylePriority.UseTextAlignment = False
            '
            'XrLabel_Agency_Address3
            '
            Me.XrLabel_Agency_Address3.StylePriority.UseFont = False
            Me.XrLabel_Agency_Address3.StylePriority.UseTextAlignment = False
            '
            'XrLabel_Agency_Address1
            '
            Me.XrLabel_Agency_Address1.StylePriority.UseFont = False
            Me.XrLabel_Agency_Address1.StylePriority.UseTextAlignment = False
            '
            'XrLabel_Agency_Phone
            '
            Me.XrLabel_Agency_Phone.StylePriority.UseFont = False
            Me.XrLabel_Agency_Phone.StylePriority.UseTextAlignment = False
            '
            'XrLabel_Agency_Address2
            '
            Me.XrLabel_Agency_Address2.StylePriority.UseFont = False
            Me.XrLabel_Agency_Address2.StylePriority.UseTextAlignment = False
            '
            'Detail
            '
            Me.Detail.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel_kept, Me.XrLabel_start_time, Me.XrLabel_appt_name, Me.XrLabel_slots})
            Me.Detail.HeightF = 15.0!
            '
            'XrPanel1
            '
            Me.XrPanel1.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel3, Me.XrLabel2, Me.XrLabel1})
            Me.XrPanel1.LocationFloat = New DevExpress.Utils.PointFloat(10.0!, 115.0!)
            Me.XrPanel1.Name = "XrPanel1"
            Me.XrPanel1.SizeF = New System.Drawing.SizeF(740.0!, 17.0!)
            Me.XrPanel1.StyleName = "XrControlStyle_HeaderPannel"
            '
            'XrLabel3
            '
            Me.XrLabel3.LocationFloat = New DevExpress.Utils.PointFloat(277.5!, 1.0!)
            Me.XrLabel3.Name = "XrLabel3"
            Me.XrLabel3.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel3.SizeF = New System.Drawing.SizeF(100.0!, 15.0!)
            Me.XrLabel3.Text = "COUNSELED"
            '
            'XrLabel2
            '
            Me.XrLabel2.LocationFloat = New DevExpress.Utils.PointFloat(177.5!, 1.0!)
            Me.XrLabel2.Name = "XrLabel2"
            Me.XrLabel2.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel2.SizeF = New System.Drawing.SizeF(100.0!, 15.0!)
            Me.XrLabel2.Text = "SCHEDULED"
            '
            'XrLabel1
            '
            Me.XrLabel1.LocationFloat = New DevExpress.Utils.PointFloat(77.5!, 1.0!)
            Me.XrLabel1.Name = "XrLabel1"
            Me.XrLabel1.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel1.SizeF = New System.Drawing.SizeF(100.0!, 15.0!)
            Me.XrLabel1.Text = "TIME"
            '
            'GroupHeader1
            '
            Me.GroupHeader1.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel_date})
            Me.GroupHeader1.GroupFields.AddRange(New DevExpress.XtraReports.UI.GroupField() {New DevExpress.XtraReports.UI.GroupField("CalculatedFieldDate", DevExpress.XtraReports.UI.XRColumnSortOrder.Ascending)})
            Me.GroupHeader1.HeightF = 38.0!
            Me.GroupHeader1.Name = "GroupHeader1"
            Me.GroupHeader1.RepeatEveryPage = True
            Me.GroupHeader1.StyleName = "XrControlStyle_GroupHeader"
            '
            'XrLabel_date
            '
            Me.XrLabel_date.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "start_time", "DATE: {0:d}")})
            Me.XrLabel_date.ForeColor = System.Drawing.Color.Blue
            Me.XrLabel_date.LocationFloat = New DevExpress.Utils.PointFloat(50.0!, 10.0!)
            Me.XrLabel_date.Name = "XrLabel_date"
            Me.XrLabel_date.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_date.SizeF = New System.Drawing.SizeF(675.0!, 16.0!)
            Me.XrLabel_date.StylePriority.UseForeColor = False
            Me.XrLabel_date.Text = "DAY: MM/DD/YYYY"
            '
            'GroupHeader2
            '
            Me.GroupHeader2.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel_office_name})
            Me.GroupHeader2.GroupFields.AddRange(New DevExpress.XtraReports.UI.GroupField() {New DevExpress.XtraReports.UI.GroupField("office", DevExpress.XtraReports.UI.XRColumnSortOrder.Ascending)})
            Me.GroupHeader2.HeightF = 38.0!
            Me.GroupHeader2.Level = 1
            Me.GroupHeader2.Name = "GroupHeader2"
            Me.GroupHeader2.RepeatEveryPage = True
            Me.GroupHeader2.StyleName = "XrControlStyle_GroupHeader"
            '
            'XrLabel_office_name
            '
            Me.XrLabel_office_name.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "office_name")})
            Me.XrLabel_office_name.LocationFloat = New DevExpress.Utils.PointFloat(8.0!, 10.0!)
            Me.XrLabel_office_name.Name = "XrLabel_office_name"
            Me.XrLabel_office_name.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_office_name.SizeF = New System.Drawing.SizeF(742.0!, 16.0!)
            Me.XrLabel_office_name.Text = "Office"
            '
            'XrLabel_appt_name
            '
            Me.XrLabel_appt_name.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "appt_name")})
            Me.XrLabel_appt_name.LocationFloat = New DevExpress.Utils.PointFloat(400.0!, 0.0!)
            Me.XrLabel_appt_name.Name = "XrLabel_appt_name"
            Me.XrLabel_appt_name.SizeF = New System.Drawing.SizeF(325.0!, 14.99999!)
            Me.XrLabel_appt_name.StylePriority.UseTextAlignment = False
            Me.XrLabel_appt_name.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            '
            'XrLabel_slots
            '
            Me.XrLabel_slots.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "slots", "{0:n0}")})
            Me.XrLabel_slots.LocationFloat = New DevExpress.Utils.PointFloat(187.5!, 0.0!)
            Me.XrLabel_slots.Name = "XrLabel_slots"
            Me.XrLabel_slots.SizeF = New System.Drawing.SizeF(100.0!, 15.0!)
            Me.XrLabel_slots.StylePriority.UseTextAlignment = False
            Me.XrLabel_slots.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
            '
            'XrLabel_kept
            '
            Me.XrLabel_kept.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "kept", "{0:n0}")})
            Me.XrLabel_kept.LocationFloat = New DevExpress.Utils.PointFloat(287.5!, 0.0!)
            Me.XrLabel_kept.Name = "XrLabel_kept"
            Me.XrLabel_kept.SizeF = New System.Drawing.SizeF(100.0!, 15.0!)
            Me.XrLabel_kept.StylePriority.UseTextAlignment = False
            Me.XrLabel_kept.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
            '
            'XrLabel_start_time
            '
            Me.XrLabel_start_time.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "start_time", "{0:t}")})
            Me.XrLabel_start_time.LocationFloat = New DevExpress.Utils.PointFloat(20.00001!, 0.0!)
            Me.XrLabel_start_time.Name = "XrLabel_start_time"
            Me.XrLabel_start_time.SizeF = New System.Drawing.SizeF(167.5!, 14.99999!)
            Me.XrLabel_start_time.StylePriority.UseTextAlignment = False
            Me.XrLabel_start_time.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
            '
            'GroupFooter1
            '
            Me.GroupFooter1.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLine1, Me.XrLabel_slots_g1, Me.XrLabel12, Me.XrLabel_kept_g1})
            Me.GroupFooter1.HeightF = 33.00001!
            Me.GroupFooter1.Name = "GroupFooter1"
            Me.GroupFooter1.StyleName = "XrControlStyle_Totals"
            '
            'XrLine1
            '
            Me.XrLine1.LocationFloat = New DevExpress.Utils.PointFloat(20.00001!, 0.0!)
            Me.XrLine1.Name = "XrLine1"
            Me.XrLine1.SizeF = New System.Drawing.SizeF(704.1667!, 5.916691!)
            '
            'XrLabel_slots_g1
            '
            Me.XrLabel_slots_g1.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "slots")})
            Me.XrLabel_slots_g1.LocationFloat = New DevExpress.Utils.PointFloat(187.5!, 10.00001!)
            Me.XrLabel_slots_g1.Name = "XrLabel_slots_g1"
            Me.XrLabel_slots_g1.SizeF = New System.Drawing.SizeF(100.0!, 15.0!)
            Me.XrLabel_slots_g1.StylePriority.UseTextAlignment = False
            XrSummary1.FormatString = "{0:n0}"
            XrSummary1.Running = DevExpress.XtraReports.UI.SummaryRunning.Group
            Me.XrLabel_slots_g1.Summary = XrSummary1
            Me.XrLabel_slots_g1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel12
            '
            Me.XrLabel12.LocationFloat = New DevExpress.Utils.PointFloat(20.00001!, 10.00001!)
            Me.XrLabel12.Name = "XrLabel12"
            Me.XrLabel12.SizeF = New System.Drawing.SizeF(167.5!, 14.99999!)
            Me.XrLabel12.StylePriority.UseTextAlignment = False
            Me.XrLabel12.Text = "DAILY TOTALS"
            Me.XrLabel12.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            '
            'XrLabel_kept_g1
            '
            Me.XrLabel_kept_g1.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "kept")})
            Me.XrLabel_kept_g1.LocationFloat = New DevExpress.Utils.PointFloat(287.5!, 10.00001!)
            Me.XrLabel_kept_g1.Name = "XrLabel_kept_g1"
            Me.XrLabel_kept_g1.SizeF = New System.Drawing.SizeF(100.0!, 15.0!)
            Me.XrLabel_kept_g1.StylePriority.UseTextAlignment = False
            XrSummary2.FormatString = "{0:n0}"
            XrSummary2.Running = DevExpress.XtraReports.UI.SummaryRunning.Group
            Me.XrLabel_kept_g1.Summary = XrSummary2
            Me.XrLabel_kept_g1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'GroupFooter2
            '
            Me.GroupFooter2.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel_slots_g2, Me.XrLabel15, Me.XrLabel_kept_g2})
            Me.GroupFooter2.HeightF = 25.00001!
            Me.GroupFooter2.Level = 1
            Me.GroupFooter2.Name = "GroupFooter2"
            Me.GroupFooter2.PageBreak = DevExpress.XtraReports.UI.PageBreak.AfterBand
            Me.GroupFooter2.StyleName = "XrControlStyle_Totals"
            '
            'XrLabel_slots_g2
            '
            Me.XrLabel_slots_g2.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "slots")})
            Me.XrLabel_slots_g2.LocationFloat = New DevExpress.Utils.PointFloat(187.5!, 10.00001!)
            Me.XrLabel_slots_g2.Name = "XrLabel_slots_g2"
            Me.XrLabel_slots_g2.SizeF = New System.Drawing.SizeF(100.0!, 15.0!)
            Me.XrLabel_slots_g2.StylePriority.UseTextAlignment = False
            XrSummary3.FormatString = "{0:n0}"
            XrSummary3.Running = DevExpress.XtraReports.UI.SummaryRunning.Group
            Me.XrLabel_slots_g2.Summary = XrSummary3
            Me.XrLabel_slots_g2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel15
            '
            Me.XrLabel15.LocationFloat = New DevExpress.Utils.PointFloat(20.00001!, 10.00001!)
            Me.XrLabel15.Name = "XrLabel15"
            Me.XrLabel15.SizeF = New System.Drawing.SizeF(167.5!, 14.99999!)
            Me.XrLabel15.StylePriority.UseTextAlignment = False
            Me.XrLabel15.Text = "OFFICE TOTALS"
            Me.XrLabel15.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            '
            'XrLabel_kept_g2
            '
            Me.XrLabel_kept_g2.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "kept")})
            Me.XrLabel_kept_g2.LocationFloat = New DevExpress.Utils.PointFloat(287.5!, 10.00001!)
            Me.XrLabel_kept_g2.Name = "XrLabel_kept_g2"
            Me.XrLabel_kept_g2.SizeF = New System.Drawing.SizeF(100.0!, 15.0!)
            Me.XrLabel_kept_g2.StylePriority.UseTextAlignment = False
            XrSummary4.FormatString = "{0:n0}"
            XrSummary4.Running = DevExpress.XtraReports.UI.SummaryRunning.Group
            Me.XrLabel_kept_g2.Summary = XrSummary4
            Me.XrLabel_kept_g2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'ParameterOffice
            '
            Me.ParameterOffice.Name = "ParameterOffice"
            Me.ParameterOffice.Type = GetType(Integer)
            Me.ParameterOffice.Value = -1
            Me.ParameterOffice.Visible = False
            '
            'CalculatedFieldDate
            '
            Me.CalculatedFieldDate.Expression = "GetDate([start_time])"
            Me.CalculatedFieldDate.FieldType = DevExpress.XtraReports.UI.FieldType.DateTime
            Me.CalculatedFieldDate.Name = "CalculatedFieldDate"
            '
            'ScheduleAnalysisReport
            '
            Me.Bands.AddRange(New DevExpress.XtraReports.UI.Band() {Me.Detail, Me.PageHeader, Me.PageFooter, Me.TopMarginBand1, Me.BottomMarginBand1, Me.GroupHeader1, Me.GroupHeader2, Me.GroupFooter1, Me.GroupFooter2})
            Me.CalculatedFields.AddRange(New DevExpress.XtraReports.UI.CalculatedField() {Me.CalculatedFieldDate})
            Me.Parameters.AddRange(New DevExpress.XtraReports.Parameters.Parameter() {Me.ParameterOffice})
            Me.ScriptLanguage = DevExpress.XtraReports.ScriptLanguage.VisualBasic
            Me.ScriptReferencesString = resources.GetString("$this.ScriptReferencesString")
            Me.Scripts.OnBeforePrint = "ScheduleAnalysisReport_BeforePrint"
            Me.ScriptsSource = resources.GetString("$this.ScriptsSource")
            Me.StyleSheet.AddRange(New DevExpress.XtraReports.UI.XRControlStyle() {Me.XrControlStyle_Header, Me.XrControlStyle_Totals, Me.XrControlStyle_HeaderPannel, Me.XrControlStyle_GroupHeader})
            Me.Version = "11.2"
            Me.Controls.SetChildIndex(Me.GroupFooter2, 0)
            Me.Controls.SetChildIndex(Me.GroupFooter1, 0)
            Me.Controls.SetChildIndex(Me.GroupHeader2, 0)
            Me.Controls.SetChildIndex(Me.GroupHeader1, 0)
            Me.Controls.SetChildIndex(Me.BottomMarginBand1, 0)
            Me.Controls.SetChildIndex(Me.TopMarginBand1, 0)
            Me.Controls.SetChildIndex(Me.PageFooter, 0)
            Me.Controls.SetChildIndex(Me.PageHeader, 0)
            Me.Controls.SetChildIndex(Me.Detail, 0)
            CType(Me, System.ComponentModel.ISupportInitialize).EndInit()

        End Sub
        Friend WithEvents XrPanel1 As DevExpress.XtraReports.UI.XRPanel
        Friend WithEvents XrLabel2 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel1 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_kept As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_start_time As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_appt_name As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_slots As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel3 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents GroupHeader1 As DevExpress.XtraReports.UI.GroupHeaderBand
        Friend WithEvents XrLabel_date As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents GroupHeader2 As DevExpress.XtraReports.UI.GroupHeaderBand
        Friend WithEvents XrLabel_office_name As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents GroupFooter1 As DevExpress.XtraReports.UI.GroupFooterBand
        Friend WithEvents XrLabel12 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_kept_g1 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents GroupFooter2 As DevExpress.XtraReports.UI.GroupFooterBand
        Friend WithEvents XrLabel15 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_kept_g2 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_slots_g1 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_slots_g2 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents ParameterOffice As DevExpress.XtraReports.Parameters.Parameter
        Friend WithEvents CalculatedFieldDate As DevExpress.XtraReports.UI.CalculatedField
        Friend WithEvents XrLine1 As DevExpress.XtraReports.UI.XRLine
    End Class
End Namespace
