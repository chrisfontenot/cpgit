#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}
#End Region

Imports System.IO
Imports System.Reflection
Imports DebtPlus.Utils

Namespace Tables.Creditors
    Public Class CreditorListReport
        Inherits Template.TemplateXtraReportClass

        Public Enum SortOrder As System.Int32
            [Unspecified] = -1
            [ByID] = 0
            [ByName] = 1
        End Enum

        Public Property Parameter_SortOrder() As SortOrder
            Get
                Return CType(Convert.ToInt32(Parameters("ParameterSortOrder").Value), SortOrder)
            End Get
            Set(ByVal value As SortOrder)
                Parameters("ParameterSortOrder").Value = Convert.ToInt32(value)
            End Set
        End Property

        Public Overrides Sub SetReportParameter(ByVal Parameter As String, ByVal Value As Object)
            Select Case Parameter
                Case "SortOrder"
                    Parameter_SortOrder = CType(Value, SortOrder)
                Case Else
                    MyBase.SetReportParameter(Parameter, Value)
            End Select
        End Sub

        Public Overrides Function NeedParameters() As Boolean
            Return MyBase.NeedParameters() OrElse Convert.ToInt32(Parameters("ParameterSortOrder").Value) < 0 OrElse Convert.ToInt32(Parameters("ParameterSortOrder").Value) < 0
        End Function

        Public Overrides Function RequestReportParameters() As System.Windows.Forms.DialogResult
            Dim answer As System.Windows.Forms.DialogResult
            Using frm As New ReportParameters()
                answer = frm.ShowDialog()
                Parameter_SortOrder = CType(frm.ParameterSortOrder, SortOrder)
            End Using
            Return answer
        End Function


        ''' <summary>
        '''     Create an instance of our report
        ''' </summary>
        Public Sub New()
            MyBase.new()

            Const ReportName As String = "DebtPlus.Reports.Tables.Creditors.repx"

            ' See if there is a report reference that we can use
            Dim Fname As String = System.IO.Path.Combine(DebtPlus.Configuration.Config.ReportsDirectory, ReportName)
            Dim UseDefault As Boolean = True
            Try
                If (Not DebtPlus.Configuration.Config.LocalReportsOnly) AndAlso System.IO.File.Exists(Fname) Then
                    LoadLayout(Fname)
                    UseDefault = False
                End If
            Catch ex As Exception
            End Try

            ' Load the standard report definition if we need a new item
            If UseDefault Then
                Dim asm As Assembly = Assembly.GetExecutingAssembly                                                    ' changed
                Using ios As Stream = asm.GetManifestResourceStream(String.Format("DebtPlus.Reports.{0}", ReportName)) ' changed
                    If ios IsNot Nothing Then LoadLayout(ios) ' changed
                End Using
            End If

            ' Set the script references as needed
            ScriptReferences = DebtPlus.Utils.AssemblyTools.GetAllSiblingAssemblies()

            ' Ensure that the paramters have a value
            Parameter_SortOrder = SortOrder.Unspecified

            ' Bind the known fields to the datasource so that there are some fields defined.
            Dim ctl As DevExpress.XtraReports.UI.XRControl = FindControl("XrLabel_creditor", True)
            If ctl IsNot Nothing Then ctl.DataBindings.Add("Text", Nothing, "creditor")

            ctl = FindControl("XrLabel_sic", True)
            If ctl IsNot Nothing Then ctl.DataBindings.Add("Text", Nothing, "sic")

            ' Add the ability for the select expert
            ReportFilter.IsEnabled = True
        End Sub

        Public Overrides ReadOnly Property ReportTitle() As String
            Get
                Return "Creditor List"
            End Get
        End Property
    End Class
End Namespace
