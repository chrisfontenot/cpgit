#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}
#End Region

Namespace Tables.Creditors
    Public Class ReportParameters
        Inherits Template.Forms.ReportParametersForm

        Public Sub New()
            MyBase.New()
            InitializeComponent()
            AddHandler Me.Load, AddressOf ReportParameters_Load
        End Sub

        Public ReadOnly Property ParameterSortOrder() As Int32
            Get
                If ComboBoxEdit1.SelectedIndex < 0 Then
                    Return -1
                End If
                Return Convert.ToInt32(CType(ComboBoxEdit1.SelectedItem, DebtPlus.Data.Controls.ComboboxItem).value)
            End Get
        End Property

        Private Sub ReportParameters_Load(ByVal sender As System.Object, ByVal e As System.EventArgs)
            With ComboBoxEdit1
                With .Properties
                    With .Items
                        .Clear()
                        .Add(New DebtPlus.Data.Controls.ComboboxItem("Creditor ID", 0))
                        .Add(New DebtPlus.Data.Controls.ComboboxItem("Creditor Name", 1))
                    End With
                    .TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
                End With
                .SelectedIndex = 0
            End With
        End Sub
    End Class
End Namespace