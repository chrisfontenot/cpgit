#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}
#End Region

Option Compare Binary
Option Explicit On 
Option Strict On

Namespace Tables.Creditors
    Public Class CreditorListReport

        Public Enum SortOrder As System.Int32
            [Unspecified] = -1
            [ByID] = 0
            [ByName] = 1
        End Enum

        Public Property Parameter_SortOrder() As SortOrder
            Get
                Return CType(Convert.ToInt32(ParameterSortOrder.Value), SortOrder)
            End Get
            Set(ByVal value As SortOrder)
                ParameterSortOrder.Value = Convert.ToInt32(value)
            End Set
        End Property

        ''' <summary>
        ''' Create an instance of our report
        ''' </summary>
        Public Sub New()
            MyBase.New()
            Parameters.Clear()
            InitializeComponent()
            Parameter_SortOrder = SortOrder.Unspecified

            Dim ctl As DevExpress.XtraReports.UI.XRControl = FindControl("XrLabel_creditor", True)
            If ctl IsNot Nothing Then ctl.DataBindings.Add("Text", Nothing, "creditor")

            ctl = FindControl("XrLabel_sic", True)
            If ctl IsNot Nothing Then ctl.DataBindings.Add("Text", Nothing, "sic")

            AddHandler XrLabel_apr_1.BeforePrint, AddressOf XrLabel_apr_1_BeforePrint
            AddHandler XrLabel_apr_2.BeforePrint, AddressOf XrLabel_apr_2_BeforePrint
            AddHandler XrLabel_apr_3.BeforePrint, AddressOf XrLabel_apr_3_BeforePrint
            AddHandler XrLabel_eft.BeforePrint, AddressOf XrLabel_eft_BeforePrint
            AddHandler XrLabel_check.BeforePrint, AddressOf XrLabel_check_BeforePrint
            AddHandler XrLabel_creditor_name.BeforePrint, AddressOf XrLabel_creditor_name_BeforePrint
            AddHandler BeforePrint, AddressOf Report_BeforePrint
            AddHandler ParametersRequestBeforeShow, AddressOf Report_ParametersRequestBeforeShow

            '-- Add the select expert
            ReportFilter.IsEnabled = True
        End Sub

        Public Overrides ReadOnly Property ReportTitle() As String
            Get
                Return "Creditor List"
            End Get
        End Property

        Public Overrides Sub SetReportParameter(ByVal Parameter As String, ByVal Value As Object)
            Select Case Parameter
                Case "SortOrder"
                    Parameter_SortOrder = CType(Value, SortOrder)
                Case Else
                    MyBase.SetReportParameter(Parameter, Value)
            End Select
        End Sub

        Public Overrides Function NeedParameters() As Boolean
            Return MyBase.NeedParameters() OrElse Convert.ToInt32(ParameterSortOrder.Value) < 0 OrElse Convert.ToInt32(ParameterSortOrder.Value) < 0
        End Function

        Public Overrides Function RequestReportParameters() As System.Windows.Forms.DialogResult
            Dim Answer As System.Windows.Forms.DialogResult
            Using frm As New ReportParameters()
                Answer = frm.ShowDialog
                Parameter_SortOrder = CType(frm.ParameterSortOrder, SortOrder)
            End Using
            Return Answer
        End Function

        Private ReadOnly ds As New System.Data.DataSet("ds")
        Private Sub Report_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs)
            Const TableName As String = "creditor_name_and_addresses"
            Dim rpt As DevExpress.XtraReports.UI.XtraReport = CType(sender, DevExpress.XtraReports.UI.XtraReport)
            Dim tbl As System.Data.DataTable = ds.Tables(TableName)

            If tbl Is Nothing Then
                Dim sqlInfo As DebtPlus.LINQ.SQLInfoClass = DebtPlus.LINQ.SQLInfoClass.getDefault()
                Using cmd As New System.Data.SqlClient.SqlCommand()
                    With cmd
                        .Connection = New System.Data.SqlClient.SqlConnection(sqlInfo.ConnectionString)
                        .CommandText = "SELECT * from view_creditor_addresses_report"
                        .CommandType = System.Data.CommandType.Text
                        .CommandTimeout = 0
                    End With

                    Using da As New System.Data.SqlClient.SqlDataAdapter(cmd)
                        da.Fill(ds, TableName)
                        tbl = ds.Tables(TableName)
                    End Using
                End Using
            End If

            If tbl IsNot Nothing Then
                Select Case Convert.ToInt32(rpt.Parameters("ParameterSortOrder").Value)
                    Case 1
                        rpt.DataSource = New System.Data.DataView(tbl, CType(rpt, DebtPlus.Interfaces.Reports.IReportFilter).ReportFilter.ViewFilter, "[addr_1],[creditor]", System.Data.DataViewRowState.CurrentRows)
                    Case Else
                        rpt.DataSource = New System.Data.DataView(tbl, CType(rpt, DebtPlus.Interfaces.Reports.IReportFilter).ReportFilter.ViewFilter, "[creditor],[addr_1]", System.Data.DataViewRowState.CurrentRows)
                End Select
            End If
        End Sub

        Private Sub XrLabel_apr_1_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs)
            With CType(sender, DevExpress.XtraReports.UI.XRLabel)
                Dim rpt As DevExpress.XtraReports.UI.XtraReport = CType(.Report, DevExpress.XtraReports.UI.XtraReport)
                .Text = FormatAPR(rpt.GetCurrentColumnValue("apr_1"))
            End With
        End Sub

        Private Sub XrLabel_apr_2_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs)
            With CType(sender, DevExpress.XtraReports.UI.XRLabel)
                Dim rpt As DevExpress.XtraReports.UI.XtraReport = CType(.Report, DevExpress.XtraReports.UI.XtraReport)
                .Text = FormatAPR(rpt.GetCurrentColumnValue("apr_2"))
            End With
        End Sub

        Private Sub XrLabel_apr_3_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs)
            With CType(sender, DevExpress.XtraReports.UI.XRLabel)
                Dim rpt As DevExpress.XtraReports.UI.XtraReport = CType(.Report, DevExpress.XtraReports.UI.XtraReport)
                .Text = FormatAPR(rpt.GetCurrentColumnValue("apr_3"))
            End With
        End Sub

        Private Sub XrLabel_check_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs)
            With CType(sender, DevExpress.XtraReports.UI.XRLabel)
                Dim rpt As DevExpress.XtraReports.UI.XtraReport = CType(.Report, DevExpress.XtraReports.UI.XtraReport)
                .Text = FormatContribution(rpt.GetCurrentColumnValue("creditor_type_check"), Me.GetCurrentColumnValue("fairshare_pct_check"))
            End With
        End Sub

        Private Sub XrLabel_eft_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs)
            With CType(sender, DevExpress.XtraReports.UI.XRLabel)
                Dim rpt As DevExpress.XtraReports.UI.XtraReport = CType(.Report, DevExpress.XtraReports.UI.XtraReport)
                .Text = FormatContribution(rpt.GetCurrentColumnValue("creditor_type_eft"), Me.GetCurrentColumnValue("fairshare_pct_eft"))
            End With
        End Sub

        Private Sub XrLabel_creditor_name_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs) Handles XrLabel_creditor_name.BeforePrint
            With CType(sender, DevExpress.XtraReports.UI.XRLabel)
                Dim rpt As DevExpress.XtraReports.UI.XtraReport = CType(.Report, DevExpress.XtraReports.UI.XtraReport)
                Dim sb As New System.Text.StringBuilder
                For Each Key As String In New String() {"addr_1", "addr_2", "addr_3", "addr_4", "addr_5", "addr_6"}
                    Dim txt As String = String.Empty
                    Dim obj As Object = rpt.GetCurrentColumnValue(Key)
                    If obj IsNot Nothing AndAlso obj IsNot System.DBNull.Value Then txt = Convert.ToString(obj)
                    If txt <> String.Empty Then
                        sb.Append(Environment.NewLine)
                        sb.Append(txt)
                    End If
                Next

                If sb.Length > 0 Then sb.Remove(0, 2)
                .Text = sb.ToString
            End With
        End Sub

        Private Function FormatAPR(ByVal item As Object) As String
            Dim Answer As String = ""

            Dim ItemValue As Double
            If item IsNot Nothing AndAlso item IsNot System.DBNull.Value Then
                ItemValue = Convert.ToDouble(item)
            Else
                ItemValue = 0.0
            End If

            If ItemValue <= 0.0 Then
                Answer = "n/a"
            Else
                While ItemValue >= 1.0#
                    ItemValue /= 100.0#
                End While
                Answer = String.Format("{0:p}", ItemValue)
            End If

            Return Answer
        End Function

        Private Function FormatContribution(ByVal ItemType As Object, ByVal ItemRate As Object) As String
            Dim Answer As String = "N"
            Dim Rate As Double = 0.0#
            If ItemRate IsNot Nothing AndAlso ItemRate IsNot System.DBNull.Value Then Rate = Convert.ToDouble(ItemRate)

            Dim TypeString As String = "N"
            If ItemType IsNot Nothing AndAlso ItemType IsNot System.DBNull.Value Then TypeString = Convert.ToString(ItemType)

            If TypeString = "N" OrElse Rate <= 0.0# Then
                Answer = "N"
            Else
                While Rate >= 1.0#
                    Rate /= 100.0#
                End While
                Answer = String.Format("{0}({1:p})", TypeString, Rate)
            End If
            Return Answer
        End Function

        Private Sub Report_ParametersRequestBeforeShow(ByVal sender As Object, ByVal e As DevExpress.XtraReports.Parameters.ParametersRequestEventArgs)
            For Each item As DevExpress.XtraReports.Parameters.ParameterInfo In e.ParametersInformation
                If item.Parameter.Name = "ParameterSortOrder" Then
                    item.Editor = GetSortOrderEditor()
                End If
            Next
        End Sub

        Private Function GetSortOrderEditor() As System.Windows.Forms.Control
            Dim ctl As New DevExpress.XtraEditors.LookUpEdit()
            With ctl
                With .Properties
                    .DataSource = SortOrderDataSource()
                    .DisplayMember = "description"
                    .ValueMember = "value"
                    .Columns.AddRange(New DevExpress.XtraEditors.Controls.LookUpColumnInfo() {New DevExpress.XtraEditors.Controls.LookUpColumnInfo("value", "ID", 20, DevExpress.Utils.FormatType.Numeric, "f0", False, DevExpress.Utils.HorzAlignment.Far), New DevExpress.XtraEditors.Controls.LookUpColumnInfo("description", "Description", 20, DevExpress.Utils.FormatType.None, "", True, DevExpress.Utils.HorzAlignment.Near)})
                    .ShowHeader = False
                    .ShowFooter = False
                End With
            End With
            Return ctl
        End Function

        Private Function SortOrderDataSource() As System.Data.DataTable
            Dim tbl As New System.Data.DataTable("SortOrder")
            With tbl
                .Columns.Add("value", GetType(System.Int32))
                .Columns.Add("description", GetType(String))
                .Rows.Add(New Object() {0, "By ID"})
                .Rows.Add(New Object() {1, "By Name"})
                .AcceptChanges()
            End With
            Return tbl
        End Function
    End Class
End Namespace