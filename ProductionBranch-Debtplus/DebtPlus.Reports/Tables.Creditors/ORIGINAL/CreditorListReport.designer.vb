Namespace Tables.Creditors
    Partial Class CreditorListReport
        Inherits DebtPlus.Reports.Template.TemplateXtraReportClass

        'Component overrides dispose to clean up the component list.
        Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
            If disposing Then
                If components IsNot Nothing Then
                    components.Dispose()
                End If
            End If
            MyBase.Dispose(disposing)
        End Sub

        'Required by the Designer
        Private components As System.ComponentModel.IContainer

        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
            Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(CreditorListReport))
            Me.XrPanel1 = New DevExpress.XtraReports.UI.XRPanel()
            Me.XrLabel9 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel4 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel3 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel8 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel7 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel5 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel2 = New DevExpress.XtraReports.UI.XRLabel()
            Me.ParameterSortOrder = New DevExpress.XtraReports.Parameters.Parameter()
            Me.XrLabel_creditor_name = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_eft = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_apr_1 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_apr_2 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_apr_3 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_sic = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_check = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_creditor = New DevExpress.XtraReports.UI.XRLabel()
            CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
            '
            'PageHeader
            '
            Me.PageHeader.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrPanel1})
            Me.PageHeader.HeightF = 160.0!
            Me.PageHeader.Controls.SetChildIndex(Me.XrPanel1, 0)
            Me.PageHeader.Controls.SetChildIndex(Me.XrPanel_AgencyAddress, 0)
            Me.PageHeader.Controls.SetChildIndex(Me.XrLabel_Title, 0)
            Me.PageHeader.Controls.SetChildIndex(Me.XrLine_Title, 0)
            Me.PageHeader.Controls.SetChildIndex(Me.XrLabel_Subtitle, 0)
            Me.PageHeader.Controls.SetChildIndex(Me.XrPageInfo1, 0)
            '
            'XrLabel_Title
            '
            Me.XrLabel_Title.StylePriority.UseFont = False
            '
            'XrPageInfo_PageNumber
            '
            Me.XrPageInfo_PageNumber.StylePriority.UseTextAlignment = False
            '
            'XRLabel_Agency_Name
            '
            Me.XRLabel_Agency_Name.StylePriority.UseTextAlignment = False
            '
            'XrLabel_Agency_Address3
            '
            Me.XrLabel_Agency_Address3.StylePriority.UseFont = False
            Me.XrLabel_Agency_Address3.StylePriority.UseTextAlignment = False
            '
            'XrLabel_Agency_Address1
            '
            Me.XrLabel_Agency_Address1.StylePriority.UseFont = False
            Me.XrLabel_Agency_Address1.StylePriority.UseTextAlignment = False
            '
            'XrLabel_Agency_Phone
            '
            Me.XrLabel_Agency_Phone.StylePriority.UseFont = False
            Me.XrLabel_Agency_Phone.StylePriority.UseTextAlignment = False
            '
            'XrLabel_Agency_Address2
            '
            Me.XrLabel_Agency_Address2.StylePriority.UseFont = False
            Me.XrLabel_Agency_Address2.StylePriority.UseTextAlignment = False
            '
            'Detail
            '
            Me.Detail.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel_creditor, Me.XrLabel_apr_3, Me.XrLabel_sic, Me.XrLabel_check, Me.XrLabel_apr_2, Me.XrLabel_creditor_name, Me.XrLabel_eft, Me.XrLabel_apr_1})
            Me.Detail.HeightF = 30.0!
            '
            'XrPanel1
            '
            Me.XrPanel1.BackColor = System.Drawing.Color.Teal
            Me.XrPanel1.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel9, Me.XrLabel4, Me.XrLabel3, Me.XrLabel8, Me.XrLabel7, Me.XrLabel5, Me.XrLabel2})
            Me.XrPanel1.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 133.0!)
            Me.XrPanel1.Name = "XrPanel1"
            Me.XrPanel1.SizeF = New System.Drawing.SizeF(800.0!, 17.0!)
            Me.XrPanel1.StyleName = "XrControlStyle_HeaderPannel"
            '
            'XrLabel9
            '
            Me.XrLabel9.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel9.ForeColor = System.Drawing.Color.White
            Me.XrLabel9.LocationFloat = New DevExpress.Utils.PointFloat(654.0001!, 1.000015!)
            Me.XrLabel9.Name = "XrLabel9"
            Me.XrLabel9.SizeF = New System.Drawing.SizeF(135.9999!, 15.0!)
            Me.XrLabel9.StylePriority.UseTextAlignment = False
            Me.XrLabel9.Text = "SIC"
            Me.XrLabel9.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
            '
            'XrLabel4
            '
            Me.XrLabel4.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel4.ForeColor = System.Drawing.Color.White
            Me.XrLabel4.LocationFloat = New DevExpress.Utils.PointFloat(547.0!, 1.0!)
            Me.XrLabel4.Name = "XrLabel4"
            Me.XrLabel4.SizeF = New System.Drawing.SizeF(41.0!, 15.0!)
            Me.XrLabel4.Text = "APR"
            Me.XrLabel4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
            '
            'XrLabel3
            '
            Me.XrLabel3.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel3.ForeColor = System.Drawing.Color.White
            Me.XrLabel3.LocationFloat = New DevExpress.Utils.PointFloat(600.5!, 1.0!)
            Me.XrLabel3.Name = "XrLabel3"
            Me.XrLabel3.SizeF = New System.Drawing.SizeF(41.0!, 15.0!)
            Me.XrLabel3.Text = "APR"
            Me.XrLabel3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
            '
            'XrLabel8
            '
            Me.XrLabel8.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel8.ForeColor = System.Drawing.Color.White
            Me.XrLabel8.LocationFloat = New DevExpress.Utils.PointFloat(423.0!, 1.0!)
            Me.XrLabel8.Name = "XrLabel8"
            Me.XrLabel8.SizeF = New System.Drawing.SizeF(58.0!, 15.0!)
            Me.XrLabel8.Text = "EFT"
            Me.XrLabel8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
            '
            'XrLabel7
            '
            Me.XrLabel7.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel7.ForeColor = System.Drawing.Color.White
            Me.XrLabel7.LocationFloat = New DevExpress.Utils.PointFloat(352.5!, 1.0!)
            Me.XrLabel7.Name = "XrLabel7"
            Me.XrLabel7.SizeF = New System.Drawing.SizeF(58.0!, 15.0!)
            Me.XrLabel7.Text = "CHECK"
            Me.XrLabel7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
            '
            'XrLabel5
            '
            Me.XrLabel5.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel5.ForeColor = System.Drawing.Color.White
            Me.XrLabel5.LocationFloat = New DevExpress.Utils.PointFloat(493.5!, 1.0!)
            Me.XrLabel5.Name = "XrLabel5"
            Me.XrLabel5.SizeF = New System.Drawing.SizeF(41.0!, 15.0!)
            Me.XrLabel5.Text = "APR"
            Me.XrLabel5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
            '
            'XrLabel2
            '
            Me.XrLabel2.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel2.ForeColor = System.Drawing.Color.White
            Me.XrLabel2.LocationFloat = New DevExpress.Utils.PointFloat(7.999992!, 0.0!)
            Me.XrLabel2.Name = "XrLabel2"
            Me.XrLabel2.SizeF = New System.Drawing.SizeF(340.0!, 15.0!)
            Me.XrLabel2.StylePriority.UseTextAlignment = False
            Me.XrLabel2.Text = "CREDITOR ID AND NAME"
            Me.XrLabel2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
            '
            'ParameterSortOrder
            '
            Me.ParameterSortOrder.Description = "Sort Order"
            Me.ParameterSortOrder.Name = "ParameterSortOrder"
            Me.ParameterSortOrder.Type = GetType(Integer)
            Me.ParameterSortOrder.Value = 0
            Me.ParameterSortOrder.Visible = False
            '
            'XrLabel_creditor_name
            '
            Me.XrLabel_creditor_name.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.XrLabel_creditor_name.ForeColor = System.Drawing.SystemColors.WindowText
            Me.XrLabel_creditor_name.LocationFloat = New DevExpress.Utils.PointFloat(100.0833!, 0.0!)
            Me.XrLabel_creditor_name.Multiline = True
            Me.XrLabel_creditor_name.Name = "XrLabel_creditor_name"
            Me.XrLabel_creditor_name.Scripts.OnBeforePrint = "XrLabel_creditor_name_BeforePrint"
            Me.XrLabel_creditor_name.SizeF = New System.Drawing.SizeF(236.875!, 15.0!)
            Me.XrLabel_creditor_name.StylePriority.UseFont = False
            Me.XrLabel_creditor_name.StylePriority.UseForeColor = False
            Me.XrLabel_creditor_name.StylePriority.UseTextAlignment = False
            Me.XrLabel_creditor_name.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
            Me.XrLabel_creditor_name.WordWrap = False
            '
            'XrLabel_eft
            '
            Me.XrLabel_eft.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.XrLabel_eft.ForeColor = System.Drawing.SystemColors.WindowText
            Me.XrLabel_eft.LocationFloat = New DevExpress.Utils.PointFloat(423.0!, 0.0!)
            Me.XrLabel_eft.Name = "XrLabel_eft"
            Me.XrLabel_eft.Scripts.OnBeforePrint = "XrLabel_eft_BeforePrint"
            Me.XrLabel_eft.SizeF = New System.Drawing.SizeF(58.0!, 15.0!)
            Me.XrLabel_eft.StylePriority.UseFont = False
            Me.XrLabel_eft.StylePriority.UseForeColor = False
            Me.XrLabel_eft.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
            Me.XrLabel_eft.WordWrap = False
            '
            'XrLabel_apr_1
            '
            Me.XrLabel_apr_1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.XrLabel_apr_1.ForeColor = System.Drawing.SystemColors.WindowText
            Me.XrLabel_apr_1.LocationFloat = New DevExpress.Utils.PointFloat(493.5!, 0.0!)
            Me.XrLabel_apr_1.Name = "XrLabel_apr_1"
            Me.XrLabel_apr_1.Scripts.OnBeforePrint = "XrLabel_apr_1_BeforePrint"
            Me.XrLabel_apr_1.SizeF = New System.Drawing.SizeF(41.0!, 15.0!)
            Me.XrLabel_apr_1.StylePriority.UseFont = False
            Me.XrLabel_apr_1.StylePriority.UseForeColor = False
            Me.XrLabel_apr_1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
            Me.XrLabel_apr_1.WordWrap = False
            '
            'XrLabel_apr_2
            '
            Me.XrLabel_apr_2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.XrLabel_apr_2.ForeColor = System.Drawing.SystemColors.WindowText
            Me.XrLabel_apr_2.LocationFloat = New DevExpress.Utils.PointFloat(547.0!, 0.0!)
            Me.XrLabel_apr_2.Name = "XrLabel_apr_2"
            Me.XrLabel_apr_2.Scripts.OnBeforePrint = "XrLabel_apr_2_BeforePrint"
            Me.XrLabel_apr_2.SizeF = New System.Drawing.SizeF(41.0!, 15.0!)
            Me.XrLabel_apr_2.StylePriority.UseFont = False
            Me.XrLabel_apr_2.StylePriority.UseForeColor = False
            Me.XrLabel_apr_2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
            Me.XrLabel_apr_2.WordWrap = False
            '
            'XrLabel_apr_3
            '
            Me.XrLabel_apr_3.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.XrLabel_apr_3.ForeColor = System.Drawing.SystemColors.WindowText
            Me.XrLabel_apr_3.LocationFloat = New DevExpress.Utils.PointFloat(600.5!, 0.0!)
            Me.XrLabel_apr_3.Name = "XrLabel_apr_3"
            Me.XrLabel_apr_3.Scripts.OnBeforePrint = "XrLabel_apr_3_BeforePrint"
            Me.XrLabel_apr_3.SizeF = New System.Drawing.SizeF(41.0!, 15.0!)
            Me.XrLabel_apr_3.StylePriority.UseFont = False
            Me.XrLabel_apr_3.StylePriority.UseForeColor = False
            Me.XrLabel_apr_3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
            Me.XrLabel_apr_3.WordWrap = False
            '
            'XrLabel_sic
            '
            Me.XrLabel_sic.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.XrLabel_sic.ForeColor = System.Drawing.SystemColors.WindowText
            Me.XrLabel_sic.LocationFloat = New DevExpress.Utils.PointFloat(654.0001!, 0.0!)
            Me.XrLabel_sic.Name = "XrLabel_sic"
            Me.XrLabel_sic.SizeF = New System.Drawing.SizeF(135.9999!, 15.0!)
            Me.XrLabel_sic.StylePriority.UseFont = False
            Me.XrLabel_sic.StylePriority.UseForeColor = False
            Me.XrLabel_sic.StylePriority.UseTextAlignment = False
            Me.XrLabel_sic.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
            Me.XrLabel_sic.WordWrap = False
            '
            'XrLabel_check
            '
            Me.XrLabel_check.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.XrLabel_check.ForeColor = System.Drawing.SystemColors.WindowText
            Me.XrLabel_check.LocationFloat = New DevExpress.Utils.PointFloat(352.5!, 0.0!)
            Me.XrLabel_check.Name = "XrLabel_check"
            Me.XrLabel_check.Scripts.OnBeforePrint = "XrLabel_check_BeforePrint"
            Me.XrLabel_check.SizeF = New System.Drawing.SizeF(58.0!, 15.0!)
            Me.XrLabel_check.StylePriority.UseFont = False
            Me.XrLabel_check.StylePriority.UseForeColor = False
            Me.XrLabel_check.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
            Me.XrLabel_check.WordWrap = False
            '
            'XrLabel_creditor
            '
            Me.XrLabel_creditor.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.XrLabel_creditor.ForeColor = System.Drawing.SystemColors.WindowText
            Me.XrLabel_creditor.LocationFloat = New DevExpress.Utils.PointFloat(7.999992!, 0.0!)
            Me.XrLabel_creditor.Name = "XrLabel_creditor"
            Me.XrLabel_creditor.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_creditor.SizeF = New System.Drawing.SizeF(92.08333!, 15.0!)
            Me.XrLabel_creditor.StylePriority.UseFont = False
            Me.XrLabel_creditor.StylePriority.UseForeColor = False
            Me.XrLabel_creditor.WordWrap = False
            '
            'CreditorListReport
            '
            Me.Bands.AddRange(New DevExpress.XtraReports.UI.Band() {Me.TopMarginBand1, Me.Detail, Me.BottomMarginBand1, Me.PageHeader, Me.PageFooter})
            Me.Parameters.AddRange(New DevExpress.XtraReports.Parameters.Parameter() {Me.ParameterSortOrder})
            Me.RequestParameters = False
            Me.ScriptLanguage = DevExpress.XtraReports.ScriptLanguage.VisualBasic
            Me.ScriptReferencesString = "C:\Documents and Settings\Al Longyear\My Documents\Visual Studio 2008\Projects\De" & _
        "btPlus\Executables\DebtPlus.Data.dll"
            Me.Scripts.OnBeforePrint = "CreditorListReport_BeforePrint"
            Me.Scripts.OnParametersRequestBeforeShow = "CreditorListReport_ParametersRequestBeforeShow"
            Me.ScriptsSource = resources.GetString("$this.ScriptsSource")
            Me.StyleSheet.AddRange(New DevExpress.XtraReports.UI.XRControlStyle() {Me.XrControlStyle_Header, Me.XrControlStyle_Totals, Me.XrControlStyle_HeaderPannel, Me.XrControlStyle_GroupHeader})
            Me.Version = "11.2"
            CType(Me, System.ComponentModel.ISupportInitialize).EndInit()

        End Sub
        Friend WithEvents XrPanel1 As DevExpress.XtraReports.UI.XRPanel
        Friend WithEvents XrLabel2 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel5 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel7 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel8 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents ParameterSortOrder As DevExpress.XtraReports.Parameters.Parameter
        Friend WithEvents XrLabel9 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel4 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel3 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_apr_3 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_sic As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_check As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_apr_2 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_creditor_name As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_eft As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_apr_1 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_creditor As DevExpress.XtraReports.UI.XRLabel
    End Class
End Namespace
