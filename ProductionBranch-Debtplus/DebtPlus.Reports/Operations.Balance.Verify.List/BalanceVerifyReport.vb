#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}

#End Region

Imports System.ComponentModel

Imports System.Drawing.Printing
Imports DebtPlus.Reports.Template.Forms
Imports System.Data.SqlClient
Imports DevExpress.XtraReports.UI

Namespace Operations.Balance.Verify.List
    Public Class BalanceVerifyReport

        Public Sub New()
            MyBase.New()
            InitializeComponent()
            AddHandler Me.BeforePrint, AddressOf Report_BeforePrint
            AddHandler XrLabel_client.BeforePrint, AddressOf XrLabel_client_BeforePrint
        End Sub

        Private privateParameterCounselor As Object = DBNull.Value

        Public Property ParameterCounselor() As Object
            Get
                Return privateParameterCounselor
            End Get
            Set(ByVal value As Object)
                privateParameterCounselor = value
            End Set
        End Property

        Public Overrides ReadOnly Property ReportTitle() As String
            Get
                Return "Verify Balance List"
            End Get
        End Property

        Public Overrides ReadOnly Property ReportSubTitle() As String
            Get
                Dim Answer As String = MyBase.ReportSubTitle
                If ParameterCounselor IsNot Nothing AndAlso ParameterCounselor IsNot DBNull.Value Then
                    Answer += String.Format(" for counselor #{0:f0}", Convert.ToInt32(ParameterCounselor))
                End If
                Return Answer
            End Get
        End Property

        Public Overrides Sub SetReportParameter(ByVal Parameter As String, ByVal Value As Object)
            Select Case Parameter
                Case "Counselor"
                    ParameterCounselor = Value
                Case Else
                    MyBase.SetReportParameter(Parameter, Value)
            End Select
        End Sub

        Public Overrides Function RequestReportParameters() As DialogResult
            Dim answer As DialogResult = DialogResult.OK
            If NeedParameters() Then
                With New DatedCounselorParametersForm(True)
                    answer = .ShowDialog()
                    Parameter_FromDate = .Parameter_FromDate
                    Parameter_ToDate = .Parameter_ToDate
                    ParameterCounselor = .Parameter_Counselor
                    .Dispose()
                End With
            End If
            Return answer
        End Function

        Dim ds As New DataSet("ds")

        Private Sub Report_BeforePrint(ByVal sender As Object, ByVal e As PrintEventArgs)
            Dim tbl As DataTable = ds.Tables("rpt_verify_balance")

            If tbl Is Nothing Then
                Dim cn As SqlConnection = New System.Data.SqlClient.SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
                Try
                    cn.Open()
                    Using cmd As SqlCommand = New System.Data.SqlClient.SqlCommand()
                        With cmd
                            .Connection = cn
                            .CommandText = "rpt_verify_balance"
                            .CommandType = CommandType.StoredProcedure
                            SqlCommandBuilder.DeriveParameters(cmd)

                            .CommandTimeout = 0
                            .Parameters(1).Value = Parameter_FromDate
                            .Parameters(2).Value = Parameter_ToDate
                            .Parameters(3).Value = ParameterCounselor
                        End With

                        Using da As New SqlDataAdapter(cmd)
                            da.Fill(ds, "rpt_verify_balance")
                            tbl = ds.Tables("rpt_verify_balance")
                        End Using
                    End Using

                Catch ex As Exception
                    Using gdr As New DebtPlus.Repository.GetDataResult()
                        gdr.HandleException(ex)
                        DebtPlus.UI.Common.ErrorHandling.HandleErrors(gdr, String.Format("Error in loading {0} report", ReportTitle))
                    End Using

                Finally
                    If cn IsNot Nothing Then cn.Dispose()
                End Try
            End If

            If tbl IsNot Nothing Then
                Dim vue As New DataView(tbl, String.Empty, "creditor, account_number, client", DataViewRowState.CurrentRows)

                ' Set the datasource
                DataSource = vue

                ' Bind the columns to the items.
                With XrLabel_account_number
                    .DataBindings.Clear()
                    .DataBindings.Add("Text", vue, "account_number")
                End With

                With XrLabel_balance
                    .DataBindings.Clear()
                    .DataBindings.Add("Text", vue, "balance", "{0:c}")
                End With

                With XrLabel_last_payment_date
                    .DataBindings.Clear()
                    .DataBindings.Add("Text", vue, "last_payment_date", "{0:d}")
                End With

                With XrLabel_name
                    .DataBindings.Clear()
                    .DataBindings.Add("Text", vue, "name")
                End With

                With XrLabel_payment_amount
                    .DataBindings.Clear()
                    .DataBindings.Add("Text", vue, "payment_amount", "{0:c}")
                End With
            End If
        End Sub

        Private Sub XrLabel_client_BeforePrint(ByVal sender As Object, ByVal e As PrintEventArgs)
            With CType(sender, XRLabel)
                .Text = String.Format("{0:0000000}", Convert.ToInt32(GetCurrentColumnValue("client")))
            End With
        End Sub
    End Class
End Namespace