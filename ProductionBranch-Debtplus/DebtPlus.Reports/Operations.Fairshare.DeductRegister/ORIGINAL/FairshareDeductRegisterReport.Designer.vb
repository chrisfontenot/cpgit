Namespace Operations.Fairshare.DeductRegister
    <Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
    Partial Public Class FairshareDeductRegisterReport
        Inherits DebtPlus.Reports.Template.DatedTemplateXtraReportClass

        'XtraReport overrides dispose to clean up the component list.
        <System.Diagnostics.DebuggerNonUserCode()> _
        Protected Overrides Sub Dispose(ByVal disposing As Boolean)
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
            MyBase.Dispose(disposing)
        End Sub

        'Required by the Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Designer
        'It can be modified using the Designer.  
        'Do not modify it using the code editor.
        <System.Diagnostics.DebuggerStepThrough()> _
        Private Sub InitializeComponent()
            Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FairshareDeductRegisterReport))
            Me.GroupHeader1 = New DevExpress.XtraReports.UI.GroupHeaderBand()
            Me.XrPanel1 = New DevExpress.XtraReports.UI.XRPanel()
            Me.XrLabel7 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel6 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel5 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel4 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel3 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel2 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel1 = New DevExpress.XtraReports.UI.XRLabel()
            Me.GroupFooter1 = New DevExpress.XtraReports.UI.GroupFooterBand()
            Me.XrLabel8 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_balance = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_date_created = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_created_by = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_check_number = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_credit_amt = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_debit_amt = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_tran_type = New DevExpress.XtraReports.UI.XRLabel()
            Me.DateField = New DevExpress.XtraReports.UI.CalculatedField()
            CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
            '
            'PageHeader
            '
            Me.PageHeader.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrPanel1})
            Me.PageHeader.HeightF = 135.0!
            Me.PageHeader.StylePriority.UseTextAlignment = False
            Me.PageHeader.Controls.SetChildIndex(Me.XrPanel_AgencyAddress, 0)
            Me.PageHeader.Controls.SetChildIndex(Me.XrLabel_Title, 0)
            Me.PageHeader.Controls.SetChildIndex(Me.XrLine_Title, 0)
            Me.PageHeader.Controls.SetChildIndex(Me.XrLabel_Subtitle, 0)
            Me.PageHeader.Controls.SetChildIndex(Me.XrPageInfo1, 0)
            Me.PageHeader.Controls.SetChildIndex(Me.XrPanel1, 0)
            '
            'XrLabel_Title
            '
            Me.XrLabel_Title.StylePriority.UseFont = False
            '
            'XrPageInfo_PageNumber
            '
            Me.XrPageInfo_PageNumber.StylePriority.UseTextAlignment = False
            '
            'XRLabel_Agency_Name
            '
            Me.XRLabel_Agency_Name.StylePriority.UseTextAlignment = False
            '
            'XrLabel_Agency_Address3
            '
            Me.XrLabel_Agency_Address3.StylePriority.UseFont = False
            Me.XrLabel_Agency_Address3.StylePriority.UseTextAlignment = False
            '
            'XrLabel_Agency_Address1
            '
            Me.XrLabel_Agency_Address1.StylePriority.UseFont = False
            Me.XrLabel_Agency_Address1.StylePriority.UseTextAlignment = False
            '
            'XrLabel_Agency_Phone
            '
            Me.XrLabel_Agency_Phone.StylePriority.UseFont = False
            Me.XrLabel_Agency_Phone.StylePriority.UseTextAlignment = False
            '
            'XrLabel_Agency_Address2
            '
            Me.XrLabel_Agency_Address2.StylePriority.UseFont = False
            Me.XrLabel_Agency_Address2.StylePriority.UseTextAlignment = False
            '
            'Detail
            '
            Me.Detail.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel_credit_amt, Me.XrLabel_debit_amt, Me.XrLabel_tran_type, Me.XrLabel_check_number, Me.XrLabel_balance, Me.XrLabel_date_created, Me.XrLabel_created_by})
            Me.Detail.HeightF = 15.00001!
            '
            'GroupHeader1
            '
            Me.GroupHeader1.GroupFields.AddRange(New DevExpress.XtraReports.UI.GroupField() {New DevExpress.XtraReports.UI.GroupField("DateField", DevExpress.XtraReports.UI.XRColumnSortOrder.Ascending)})
            Me.GroupHeader1.GroupUnion = DevExpress.XtraReports.UI.GroupUnion.WithFirstDetail
            Me.GroupHeader1.HeightF = 0.0!
            Me.GroupHeader1.Name = "GroupHeader1"
            '
            'XrPanel1
            '
            Me.XrPanel1.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel7, Me.XrLabel6, Me.XrLabel5, Me.XrLabel4, Me.XrLabel3, Me.XrLabel2, Me.XrLabel1})
            Me.XrPanel1.LocationFloat = New DevExpress.Utils.PointFloat(10.0!, 112.0!)
            Me.XrPanel1.Name = "XrPanel1"
            Me.XrPanel1.SizeF = New System.Drawing.SizeF(740.0!, 17.0!)
            Me.XrPanel1.StyleName = "XrControlStyle_HeaderPannel"
            '
            'XrLabel7
            '
            Me.XrLabel7.LocationFloat = New DevExpress.Utils.PointFloat(640.0!, 1.000007!)
            Me.XrLabel7.Name = "XrLabel7"
            Me.XrLabel7.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel7.SizeF = New System.Drawing.SizeF(100.0!, 15.0!)
            Me.XrLabel7.StylePriority.UseTextAlignment = False
            Me.XrLabel7.Text = "BALANCE"
            Me.XrLabel7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel6
            '
            Me.XrLabel6.LocationFloat = New DevExpress.Utils.PointFloat(540.0!, 1.000007!)
            Me.XrLabel6.Name = "XrLabel6"
            Me.XrLabel6.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel6.SizeF = New System.Drawing.SizeF(100.0!, 15.0!)
            Me.XrLabel6.StylePriority.UseTextAlignment = False
            Me.XrLabel6.Text = "DEBIT"
            Me.XrLabel6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel5
            '
            Me.XrLabel5.LocationFloat = New DevExpress.Utils.PointFloat(440.0!, 1.000007!)
            Me.XrLabel5.Name = "XrLabel5"
            Me.XrLabel5.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel5.SizeF = New System.Drawing.SizeF(100.0!, 15.0!)
            Me.XrLabel5.StylePriority.UseTextAlignment = False
            Me.XrLabel5.Text = "CREDIT"
            Me.XrLabel5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel4
            '
            Me.XrLabel4.LocationFloat = New DevExpress.Utils.PointFloat(340.0!, 1.000007!)
            Me.XrLabel4.Name = "XrLabel4"
            Me.XrLabel4.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel4.SizeF = New System.Drawing.SizeF(100.0!, 15.0!)
            Me.XrLabel4.StylePriority.UseTextAlignment = False
            Me.XrLabel4.Text = "CHECK#"
            Me.XrLabel4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel3
            '
            Me.XrLabel3.LocationFloat = New DevExpress.Utils.PointFloat(134.7917!, 1.000008!)
            Me.XrLabel3.Name = "XrLabel3"
            Me.XrLabel3.Padding = New DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 100.0!)
            Me.XrLabel3.SizeF = New System.Drawing.SizeF(205.2083!, 14.99999!)
            Me.XrLabel3.StylePriority.UsePadding = False
            Me.XrLabel3.StylePriority.UseTextAlignment = False
            Me.XrLabel3.Text = "CREATED BY"
            Me.XrLabel3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            '
            'XrLabel2
            '
            Me.XrLabel2.LocationFloat = New DevExpress.Utils.PointFloat(47.91667!, 0.0!)
            Me.XrLabel2.Name = "XrLabel2"
            Me.XrLabel2.Padding = New DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 100.0!)
            Me.XrLabel2.SizeF = New System.Drawing.SizeF(86.875!, 14.99999!)
            Me.XrLabel2.StylePriority.UsePadding = False
            Me.XrLabel2.StylePriority.UseTextAlignment = False
            Me.XrLabel2.Text = "DATE"
            Me.XrLabel2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel1
            '
            Me.XrLabel1.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.9999924!)
            Me.XrLabel1.Name = "XrLabel1"
            Me.XrLabel1.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel1.SizeF = New System.Drawing.SizeF(47.91666!, 15.00001!)
            Me.XrLabel1.StylePriority.UseTextAlignment = False
            Me.XrLabel1.Text = "TYPE"
            Me.XrLabel1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            '
            'GroupFooter1
            '
            Me.GroupFooter1.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel8})
            Me.GroupFooter1.HeightF = 15.0!
            Me.GroupFooter1.Name = "GroupFooter1"
            '
            'XrLabel8
            '
            Me.XrLabel8.LocationFloat = New DevExpress.Utils.PointFloat(7.999992!, 0.0!)
            Me.XrLabel8.Name = "XrLabel8"
            Me.XrLabel8.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel8.SizeF = New System.Drawing.SizeF(100.0!, 15.0!)
            '
            'XrLabel_balance
            '
            Me.XrLabel_balance.LocationFloat = New DevExpress.Utils.PointFloat(650.0001!, 0.0!)
            Me.XrLabel_balance.Name = "XrLabel_balance"
            Me.XrLabel_balance.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_balance.Scripts.OnBeforePrint = "XrLabel_balance_BeforePrint"
            Me.XrLabel_balance.SizeF = New System.Drawing.SizeF(100.0!, 15.0!)
            Me.XrLabel_balance.StylePriority.UseTextAlignment = False
            Me.XrLabel_balance.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
            '
            'XrLabel_date_created
            '
            Me.XrLabel_date_created.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "date_created", "{0:d}")})
            Me.XrLabel_date_created.LocationFloat = New DevExpress.Utils.PointFloat(57.91667!, 0.0!)
            Me.XrLabel_date_created.Name = "XrLabel_date_created"
            Me.XrLabel_date_created.Padding = New DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 100.0!)
            Me.XrLabel_date_created.SizeF = New System.Drawing.SizeF(86.875!, 14.99999!)
            Me.XrLabel_date_created.StylePriority.UsePadding = False
            Me.XrLabel_date_created.StylePriority.UseTextAlignment = False
            Me.XrLabel_date_created.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel_created_by
            '
            Me.XrLabel_created_by.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "created_by")})
            Me.XrLabel_created_by.LocationFloat = New DevExpress.Utils.PointFloat(144.7917!, 0.0!)
            Me.XrLabel_created_by.Name = "XrLabel_created_by"
            Me.XrLabel_created_by.Padding = New DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 100.0!)
            Me.XrLabel_created_by.SizeF = New System.Drawing.SizeF(205.2083!, 14.99999!)
            Me.XrLabel_created_by.StylePriority.UsePadding = False
            Me.XrLabel_created_by.StylePriority.UseTextAlignment = False
            Me.XrLabel_created_by.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            '
            'XrLabel_check_number
            '
            Me.XrLabel_check_number.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "check_number")})
            Me.XrLabel_check_number.LocationFloat = New DevExpress.Utils.PointFloat(350.0!, 0.0!)
            Me.XrLabel_check_number.Name = "XrLabel_check_number"
            Me.XrLabel_check_number.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_check_number.SizeF = New System.Drawing.SizeF(100.0!, 15.0!)
            Me.XrLabel_check_number.StylePriority.UseTextAlignment = False
            Me.XrLabel_check_number.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel_credit_amt
            '
            Me.XrLabel_credit_amt.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "credit_amt", "{0:c}")})
            Me.XrLabel_credit_amt.LocationFloat = New DevExpress.Utils.PointFloat(450.0001!, 0.0!)
            Me.XrLabel_credit_amt.Name = "XrLabel_credit_amt"
            Me.XrLabel_credit_amt.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_credit_amt.SizeF = New System.Drawing.SizeF(100.0!, 15.0!)
            Me.XrLabel_credit_amt.StylePriority.UseTextAlignment = False
            Me.XrLabel_credit_amt.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
            '
            'XrLabel_debit_amt
            '
            Me.XrLabel_debit_amt.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "debit_amt", "{0:c}")})
            Me.XrLabel_debit_amt.LocationFloat = New DevExpress.Utils.PointFloat(550.0001!, 0.0!)
            Me.XrLabel_debit_amt.Name = "XrLabel_debit_amt"
            Me.XrLabel_debit_amt.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_debit_amt.SizeF = New System.Drawing.SizeF(100.0!, 15.0!)
            Me.XrLabel_debit_amt.StylePriority.UseTextAlignment = False
            Me.XrLabel_debit_amt.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
            '
            'XrLabel_tran_type
            '
            Me.XrLabel_tran_type.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "tran_type")})
            Me.XrLabel_tran_type.LocationFloat = New DevExpress.Utils.PointFloat(10.00001!, 0.0!)
            Me.XrLabel_tran_type.Name = "XrLabel_tran_type"
            Me.XrLabel_tran_type.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_tran_type.SizeF = New System.Drawing.SizeF(47.91666!, 15.00001!)
            Me.XrLabel_tran_type.StylePriority.UseTextAlignment = False
            Me.XrLabel_tran_type.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            '
            'DateField
            '
            Me.DateField.DisplayName = "DateField"
            Me.DateField.Expression = "GetDate([date_created])"
            Me.DateField.FieldType = DevExpress.XtraReports.UI.FieldType.DateTime
            Me.DateField.Name = "DateField"
            '
            'FairshareDeductRegisterReport
            '
            Me.Bands.AddRange(New DevExpress.XtraReports.UI.Band() {Me.Detail, Me.PageHeader, Me.PageFooter, Me.TopMarginBand1, Me.BottomMarginBand1, Me.GroupHeader1, Me.GroupFooter1})
            Me.CalculatedFields.AddRange(New DevExpress.XtraReports.UI.CalculatedField() {Me.DateField})
            Me.ScriptLanguage = DevExpress.XtraReports.ScriptLanguage.VisualBasic
            Me.ScriptReferencesString = "C:\Documents and Settings\Al Longyear\My Documents\Visual Studio 2008\Projects\De" & _
        "btPlus\Executables\DebtPlus.Data.dll"
            Me.Scripts.OnBeforePrint = "FairshareDeductRegisterReport_BeforePrint"
            Me.ScriptsSource = resources.GetString("$this.ScriptsSource")
            Me.StyleSheet.AddRange(New DevExpress.XtraReports.UI.XRControlStyle() {Me.XrControlStyle_Header, Me.XrControlStyle_Totals, Me.XrControlStyle_HeaderPannel, Me.XrControlStyle_GroupHeader})
            Me.Version = "11.2"
            Me.Controls.SetChildIndex(Me.GroupFooter1, 0)
            Me.Controls.SetChildIndex(Me.GroupHeader1, 0)
            Me.Controls.SetChildIndex(Me.BottomMarginBand1, 0)
            Me.Controls.SetChildIndex(Me.TopMarginBand1, 0)
            Me.Controls.SetChildIndex(Me.PageFooter, 0)
            Me.Controls.SetChildIndex(Me.PageHeader, 0)
            Me.Controls.SetChildIndex(Me.Detail, 0)
            CType(Me, System.ComponentModel.ISupportInitialize).EndInit()

        End Sub
        Friend WithEvents XrPanel1 As DevExpress.XtraReports.UI.XRPanel
        Friend WithEvents XrLabel7 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel6 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel5 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel4 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel3 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel2 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel1 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents GroupHeader1 As DevExpress.XtraReports.UI.GroupHeaderBand
        Friend WithEvents GroupFooter1 As DevExpress.XtraReports.UI.GroupFooterBand
        Friend WithEvents XrLabel_credit_amt As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_debit_amt As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_tran_type As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_check_number As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_balance As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_date_created As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_created_by As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents DateField As DevExpress.XtraReports.UI.CalculatedField
        Friend WithEvents XrLabel8 As DevExpress.XtraReports.UI.XRLabel
    End Class
End Namespace
