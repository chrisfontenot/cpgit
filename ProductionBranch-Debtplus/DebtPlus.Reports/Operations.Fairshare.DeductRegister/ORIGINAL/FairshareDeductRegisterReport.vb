#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}
#End Region

Option Compare Binary
Option Explicit On
Option Strict On

Namespace Operations.Fairshare.DeductRegister
    Public Class FairshareDeductRegisterReport

        Public Sub New()
            MyBase.New()
            InitializeComponent()

            'Me.XrLabel_check_number.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "check_number", "")})
            'Me.XrLabel_date_created.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "date_created", "{0:d}")})
            'Me.XrLabel_created_by.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "created_by", "")})
            'Me.XrLabel_credit_amt.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "credit_amt", "{0:c}")})
            'Me.XrLabel_debit_amt.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "debit_amt", "{0:c}")})
            'Me.XrLabel_tran_type.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "tran_type", "")})

            AddHandler XrLabel_balance.BeforePrint, AddressOf XrLabel_balance_BeforePrint
            AddHandler BeforePrint, AddressOf Report_BeforePrint
        End Sub

        Public Overrides ReadOnly Property ReportTitle() As String
            Get
                Return "Fairshare Deduct Register"
            End Get
        End Property

        Private ds As New System.Data.DataSet("ds")
        Dim CurrentBalance As Decimal = 0D

        Private Sub Report_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs)
            Dim rpt As DevExpress.XtraReports.UI.XtraReport = CType(sender, DevExpress.XtraReports.UI.XtraReport)

            With rpt
                Const TableName As String = "rpt_Fairshare_Register"
                Dim tbl As System.Data.DataTable = ds.Tables(TableName)
                If tbl IsNot Nothing Then
                    tbl.Clear()
                    tbl = Nothing
                End If

                '-- Ensure that the current balance starts with $0.00
                CurrentBalance = 0D

                Dim sqlInfo As DebtPlus.LINQ.SQLInfoClass = DebtPlus.LINQ.SQLInfoClass.getDefault()
                Try
                    Using cn As System.Data.SqlClient.SqlConnection = New System.Data.SqlClient.SqlConnection(sqlInfo.ConnectionString)
                        cn.Open()
                        Using cmd As New System.Data.SqlClient.SqlCommand()
                            cmd.Connection = cn
                            cmd.CommandText = TableName
                            cmd.CommandType = System.Data.CommandType.StoredProcedure
                            System.Data.SqlClient.SqlCommandBuilder.DeriveParameters(cmd)
                            cmd.Parameters(1).Value = rpt.Parameters("ParameterFromDate").Value
                            cmd.Parameters(2).Value = rpt.Parameters("ParameterToDate").Value

                            Using da As New System.Data.SqlClient.SqlDataAdapter(cmd)
                                da.Fill(ds, TableName)
                                tbl = ds.Tables(TableName)

                                '-- Assign the datasource to the report and the calculated fields
                                rpt.DataSource = New System.Data.DataView(tbl, String.Empty, "date_created", System.Data.DataViewRowState.CurrentRows)
                                For Each calc As DevExpress.XtraReports.UI.CalculatedField In rpt.CalculatedFields
                                    calc.Assign(rpt.DataSource, rpt.DataMember)
                                Next
                            End Using
                        End Using
                    End Using

                Catch ex As System.Data.SqlClient.SqlException
                    Dim gdr As New DebtPlus.Repository.GetDataResult() : gdr.HandleException(ex) : DebtPlus.UI.Common.ErrorHandling.HandleErrors(gdr, "Error reading report records")
                End Try
            End With
        End Sub

        Private Sub XrLabel_balance_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs)
            With CType(sender, DevExpress.XtraReports.UI.XRLabel)
                Dim rpt As DevExpress.XtraReports.UI.XtraReport = CType(.Report, DevExpress.XtraReports.UI.XtraReport)
                Dim CurrentDate As Object = GetCurrentColumnValue("date_created")
                Dim NextDate As Object = GetNextColumnValue("date_created")

                '-- Determine if we are on the last row in the recordset
                Dim DataSource As System.Data.DataView = CType(rpt.DataSource, System.Data.DataView)
                Dim ShouldPrint As Boolean = DataSource.Count < 1 OrElse DataSource(DataSource.Count - 1) Is GetCurrentRow()

                '-- Should we print the value or not?
                If CurrentDate Is Nothing OrElse NextDate Is Nothing OrElse NextDate Is System.DBNull.Value OrElse CurrentDate Is System.DBNull.Value Then
                    ShouldPrint = True
                ElseIf Convert.ToDateTime(CurrentDate).Date <> Convert.ToDateTime(NextDate).Date Then
                    ShouldPrint = True
                End If

                '-- Compute the new balance
                Dim TranType As String = String.Empty
                Dim CreditAmt As Decimal = 0D
                Dim DebitAmt As Decimal = 0D
                If GetCurrentColumnValue("tran_type") IsNot Nothing AndAlso GetCurrentColumnValue("tran_type") IsNot System.DBNull.Value Then TranType = Convert.ToString(GetCurrentColumnValue("tran_type")).Trim
                If GetCurrentColumnValue("credit_amt") IsNot Nothing AndAlso GetCurrentColumnValue("credit_amt") IsNot System.DBNull.Value Then CreditAmt = Convert.ToDecimal(GetCurrentColumnValue("credit_amt"))
                If GetCurrentColumnValue("debit_amt") IsNot Nothing AndAlso GetCurrentColumnValue("debit_amt") IsNot System.DBNull.Value Then DebitAmt = Convert.ToDecimal(GetCurrentColumnValue("debit_amt"))

                Select Case TranType.ToUpper
                    Case "EB"
                        CurrentBalance = DebitAmt
                    Case "BB"
                        CurrentBalance = CreditAmt
                    Case Else
                        CurrentBalance += CreditAmt - DebitAmt
                End Select

                '-- If we need to format the balance then update the text else use blanks
                If ShouldPrint Then
                    .Text = String.Format("{0:c}", CurrentBalance)
                Else
                    .Text = String.Empty
                End If
            End With
        End Sub
    End Class
End Namespace
