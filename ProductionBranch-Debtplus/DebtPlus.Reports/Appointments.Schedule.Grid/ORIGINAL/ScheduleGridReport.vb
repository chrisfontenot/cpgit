#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}
#End Region

Namespace Appointments.Schedule.Grid
    Public Class ScheduleGridReport

        Public Sub New()
            MyBase.New()
            InitializeComponent()

            AddHandler Me.BeforePrint, AddressOf ScheduleGridReport_BeforePrint
        End Sub

        Public Property Parameter_Office() As Int32
            Get
                Dim parm As DevExpress.XtraReports.Parameters.Parameter = FindParameter("ParameterOffice")
                If parm Is Nothing Then Return -1
                Return Convert.ToInt32(parm.Value)
            End Get
            Set(ByVal value As Int32)
                SetParameter("ParameterOffice", GetType(Int32), value, "Office ID", False)
            End Set
        End Property

        Public Overrides Function RequestReportParameters() As System.Windows.Forms.DialogResult
            Dim Answer As System.Windows.Forms.DialogResult = DialogResult.OK

            If NeedParameters() Then
                Using frm As New DebtPlus.Reports.Template.Forms.DatedOfficeParametersForm(True)
                    Answer = frm.ShowDialog()
                    Parameter_FromDate = frm.Parameter_FromDate
                    Parameter_ToDate = frm.Parameter_ToDate
                    Parameter_Office = If(frm.Parameter_Office Is System.DBNull.Value, -1, Convert.ToInt32(frm.Parameter_Office))
                End Using
            End If

            Return Answer
        End Function

        Public Overrides ReadOnly Property ReportTitle() As String
            Get
                Return "Appts Scheduled Grid"
            End Get
        End Property

        Protected ds As New System.Data.DataSet("ds")

        Private Sub ScheduleGridReport_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs)
            Const TableName As String = "rpt_appts_scheduled_analysis"
            Dim Rpt As DevExpress.XtraReports.UI.XtraReport = CType(sender, DevExpress.XtraReports.UI.XtraReport)
            Dim sqlInfo As DebtPlus.LINQ.SQLInfoClass = DebtPlus.LINQ.SQLInfoClass.getDefault()

            Dim tbl As System.Data.DataTable = ds.Tables(TableName)
            If tbl IsNot Nothing Then
                tbl.Clear()
            End If

            Try
                Using cn As System.Data.SqlClient.SqlConnection = New System.Data.SqlClient.SqlConnection(sqlInfo.ConnectionString)
                    cn.Open()
                    Using cmd As New System.Data.SqlClient.SqlCommand()
                        cmd.Connection = cn
                        cmd.CommandText = TableName
                        cmd.CommandType = System.Data.CommandType.StoredProcedure

                        System.Data.SqlClient.SqlCommandBuilder.DeriveParameters(cmd)
                        cmd.Parameters(1).Value = Rpt.Parameters("ParameterFromDate").Value
                        cmd.Parameters(2).Value = Rpt.Parameters("ParameterToDate").Value

                        ' Include the office if there is a selection. Otherwise, let it default to "all"
                        Dim v As Object = Rpt.Parameters("ParameterOffice").Value
                        If v Is Nothing OrElse v Is System.DBNull.Value OrElse Convert.ToInt32(v) <= 0 Then
                            v = System.DBNull.Value
                        End If
                        cmd.Parameters(3).Value = v

                        cmd.CommandTimeout = 0

                        Using da As New System.Data.SqlClient.SqlDataAdapter(cmd)
                            da.Fill(ds, TableName)
                            tbl = ds.Tables(TableName)

                            SetGrid1(Rpt, New System.Data.DataView(ds.Tables(TableName), "[start_time]<'12:00'", String.Empty, System.Data.DataViewRowState.CurrentRows))
                            SetGrid2(Rpt, New System.Data.DataView(ds.Tables(TableName), "[start_time]>='12:00' AND [start_time]<'17:00'", String.Empty, System.Data.DataViewRowState.CurrentRows))
                            SetGrid3(Rpt, New System.Data.DataView(ds.Tables(TableName), "[start_time]>='17:00'", String.Empty, System.Data.DataViewRowState.CurrentRows))
                        End Using
                    End Using
                End Using

            Catch ex As System.Data.SqlClient.SqlException
                Using gdr As New DebtPlus.Repository.GetDataResult()
                    gdr.HandleException(ex)
                    DebtPlus.UI.Common.ErrorHandling.HandleErrors(gdr, "Error reading report information")
                End Using
            End Try
        End Sub

        Private Sub SetGrid1(ByRef rpt As DevExpress.XtraReports.UI.XtraReport, ByVal vue As System.Data.DataView)
            Dim grid As DevExpress.XtraReports.UI.XRPivotGrid = CType(rpt.FindControl("XrPivotGrid1", True), DevExpress.XtraReports.UI.XRPivotGrid)
            If vue.Count <= 0 Then
                grid.Visible = False
                Dim brk As DevExpress.XtraReports.UI.XRPageBreak = CType(rpt.FindControl("XrPageBreak1", True), DevExpress.XtraReports.UI.XRPageBreak)
                brk.Visible = False
            Else
                grid.DataSource = vue

                ' Create a row pivot grid field bound to the Office Name data source field.
                Dim fieldOfficeName As New DevExpress.XtraPivotGrid.PivotGridField("office_name", DevExpress.XtraPivotGrid.PivotArea.RowArea)
                fieldOfficeName.Caption = "Office"
                fieldOfficeName.AreaIndex = 0

                Dim fieldStartDate As New DevExpress.XtraPivotGrid.PivotGridField("start_date", DevExpress.XtraPivotGrid.PivotArea.RowArea)
                fieldStartDate.Caption = "Date"
                fieldStartDate.GroupInterval = DevExpress.XtraPivotGrid.PivotGroupInterval.Date
                fieldStartDate.Width = 80
                fieldStartDate.AreaIndex = 1

                Dim fieldStartTime As New DevExpress.XtraPivotGrid.PivotGridField("start_time", DevExpress.XtraPivotGrid.PivotArea.ColumnArea)
                fieldStartTime.Caption = "Time"
                fieldStartTime.Width = 35
                fieldStartTime.AreaIndex = 0
                'fieldStartTime.TopValueType = DevExpress.XtraPivotGrid.PivotTopValueType.Absolute
                'fieldStartTime.TopValueCount = 20

                Dim fieldAppointmentCount As New DevExpress.XtraPivotGrid.PivotGridField("appt_count", DevExpress.XtraPivotGrid.PivotArea.DataArea)
                fieldAppointmentCount.Caption = "Count"
                fieldAppointmentCount.CellFormat.FormatString = "n0"

                '-- Add the fields to the grid
                grid.Fields.Clear()
                grid.Fields.AddRange(New DevExpress.XtraPivotGrid.PivotGridField() {fieldOfficeName, fieldStartDate, fieldStartTime, fieldAppointmentCount})

                grid.OptionsPrint.PrintHeadersOnEveryPage = True
            End If
        End Sub

        Private Sub SetGrid2(ByRef rpt As DevExpress.XtraReports.UI.XtraReport, ByVal vue As System.Data.DataView)
            Dim grid As DevExpress.XtraReports.UI.XRPivotGrid = CType(rpt.FindControl("XrPivotGrid2", True), DevExpress.XtraReports.UI.XRPivotGrid)
            If vue.Count <= 0 Then
                grid.Visible = False
                Dim brk As DevExpress.XtraReports.UI.XRPageBreak = CType(rpt.FindControl("XrPageBreak2", True), DevExpress.XtraReports.UI.XRPageBreak)
                brk.Visible = False
            Else
                grid.DataSource = vue

                ' Create a row pivot grid field bound to the Office Name datasource field.
                Dim fieldOfficeName As New DevExpress.XtraPivotGrid.PivotGridField("office_name", DevExpress.XtraPivotGrid.PivotArea.RowArea)
                fieldOfficeName.Caption = "Office"
                fieldOfficeName.AreaIndex = 0

                Dim fieldStartDate As New DevExpress.XtraPivotGrid.PivotGridField("start_date", DevExpress.XtraPivotGrid.PivotArea.RowArea)
                fieldStartDate.Caption = "Date"
                fieldStartDate.GroupInterval = DevExpress.XtraPivotGrid.PivotGroupInterval.Date
                fieldStartDate.Width = 80
                fieldStartDate.AreaIndex = 1

                Dim fieldStartTime As New DevExpress.XtraPivotGrid.PivotGridField("start_time", DevExpress.XtraPivotGrid.PivotArea.ColumnArea)
                fieldStartTime.Caption = "Time"
                fieldStartTime.Width = 35
                fieldStartTime.AreaIndex = 0
                'fieldStartTime.TopValueType = DevExpress.XtraPivotGrid.PivotTopValueType.Absolute
                'fieldStartTime.TopValueCount = 20

                Dim fieldAppointmentCount As New DevExpress.XtraPivotGrid.PivotGridField("appt_count", DevExpress.XtraPivotGrid.PivotArea.DataArea)
                fieldAppointmentCount.Caption = "Count"
                fieldAppointmentCount.CellFormat.FormatString = "n0"

                '-- Add the fields to the grid
                grid.Fields.Clear()
                grid.Fields.AddRange(New DevExpress.XtraPivotGrid.PivotGridField() {fieldOfficeName, fieldStartDate, fieldStartTime, fieldAppointmentCount})

                grid.OptionsPrint.PrintHeadersOnEveryPage = True
            End If
        End Sub

        Private Sub SetGrid3(ByRef rpt As DevExpress.XtraReports.UI.XtraReport, ByVal vue As System.Data.DataView)
            Dim grid As DevExpress.XtraReports.UI.XRPivotGrid = CType(rpt.FindControl("XrPivotGrid3", True), DevExpress.XtraReports.UI.XRPivotGrid)
            If vue.Count <= 0 Then
                grid.Visible = False
            Else
                grid.DataSource = vue

                ' Create a row pivot grid field bound to the Office Name datasource field.
                Dim fieldOfficeName As New DevExpress.XtraPivotGrid.PivotGridField("office_name", DevExpress.XtraPivotGrid.PivotArea.RowArea)
                fieldOfficeName.Caption = "Office"
                fieldOfficeName.AreaIndex = 0

                Dim fieldStartDate As New DevExpress.XtraPivotGrid.PivotGridField("start_date", DevExpress.XtraPivotGrid.PivotArea.RowArea)
                fieldStartDate.Caption = "Date"
                fieldStartDate.GroupInterval = DevExpress.XtraPivotGrid.PivotGroupInterval.Date
                fieldStartDate.Width = 80
                fieldStartDate.AreaIndex = 1

                Dim fieldStartTime As New DevExpress.XtraPivotGrid.PivotGridField("start_time", DevExpress.XtraPivotGrid.PivotArea.ColumnArea)
                fieldStartTime.Caption = "Time"
                fieldStartTime.Width = 35
                fieldStartTime.AreaIndex = 0
                'fieldStartTime.TopValueType = DevExpress.XtraPivotGrid.PivotTopValueType.Absolute
                'fieldStartTime.TopValueCount = 20

                Dim fieldAppointmentCount As New DevExpress.XtraPivotGrid.PivotGridField("appt_count", DevExpress.XtraPivotGrid.PivotArea.DataArea)
                fieldAppointmentCount.Caption = "Count"
                fieldAppointmentCount.CellFormat.FormatString = "n0"

                '-- Add the fields to the grid
                grid.Fields.Clear()
                grid.Fields.AddRange(New DevExpress.XtraPivotGrid.PivotGridField() {fieldOfficeName, fieldStartDate, fieldStartTime, fieldAppointmentCount})

                grid.OptionsPrint.PrintHeadersOnEveryPage = True
            End If
        End Sub
    End Class
End Namespace
