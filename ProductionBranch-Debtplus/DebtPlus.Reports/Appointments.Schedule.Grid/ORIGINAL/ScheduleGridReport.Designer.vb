Namespace Appointments.Schedule.Grid
    <Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
    Partial Class ScheduleGridReport
        Inherits DebtPlus.Reports.Template.DatedTemplateXtraReportClass

        'XtraReport overrides dispose to clean up the component list.
        <System.Diagnostics.DebuggerNonUserCode()> _
        Protected Overrides Sub Dispose(ByVal disposing As Boolean)
            If disposing Then
                If components IsNot Nothing Then
                    components.Dispose()
                End If
            End If
            MyBase.Dispose(disposing)
        End Sub

        'Required by the Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Designer
        'It can be modified using the Designer.  
        'Do not modify it using the code editor.
        <System.Diagnostics.DebuggerStepThrough()> _
        Private Sub InitializeComponent()
            Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(ScheduleGridReport))
            Me.XrPivotGrid1 = New DevExpress.XtraReports.UI.XRPivotGrid()
            Me.ParameterOffice = New DevExpress.XtraReports.Parameters.Parameter()
            Me.XrControlStyle1 = New DevExpress.XtraReports.UI.XRControlStyle()
            Me.XrPivotGrid2 = New DevExpress.XtraReports.UI.XRPivotGrid()
            Me.XrPageBreak1 = New DevExpress.XtraReports.UI.XRPageBreak()
            Me.XrPivotGrid3 = New DevExpress.XtraReports.UI.XRPivotGrid()
            Me.XrPageBreak2 = New DevExpress.XtraReports.UI.XRPageBreak()
            CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
            '
            'PageHeader
            '
            Me.PageHeader.HeightF = 130.0!
            '
            'XrLabel_Title
            '
            Me.XrLabel_Title.StylePriority.UseFont = False
            '
            'XrPageInfo_PageNumber
            '
            Me.XrPageInfo_PageNumber.LocationFloat = New DevExpress.Utils.PointFloat(899.0!, 8.0!)
            Me.XrPageInfo_PageNumber.StylePriority.UseTextAlignment = False
            '
            'XrPanel_AgencyAddress
            '
            Me.XrPanel_AgencyAddress.LocationFloat = New DevExpress.Utils.PointFloat(732.0!, 0.0!)
            '
            'XRLabel_Agency_Name
            '
            Me.XRLabel_Agency_Name.StylePriority.UseTextAlignment = False
            '
            'XrLabel_Agency_Address3
            '
            Me.XrLabel_Agency_Address3.StylePriority.UseFont = False
            Me.XrLabel_Agency_Address3.StylePriority.UseTextAlignment = False
            '
            'XrLabel_Agency_Address1
            '
            Me.XrLabel_Agency_Address1.StylePriority.UseFont = False
            Me.XrLabel_Agency_Address1.StylePriority.UseTextAlignment = False
            '
            'XrLabel_Agency_Phone
            '
            Me.XrLabel_Agency_Phone.StylePriority.UseFont = False
            Me.XrLabel_Agency_Phone.StylePriority.UseTextAlignment = False
            '
            'XrLabel_Agency_Address2
            '
            Me.XrLabel_Agency_Address2.StylePriority.UseFont = False
            Me.XrLabel_Agency_Address2.StylePriority.UseTextAlignment = False
            '
            'Detail
            '
            Me.Detail.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrPageBreak2, Me.XrPivotGrid3, Me.XrPageBreak1, Me.XrPivotGrid2, Me.XrPivotGrid1})
            Me.Detail.HeightF = 154.0!
            '
            'BottomMarginBand1
            '
            Me.BottomMarginBand1.HeightF = 50.0!
            '
            'XrPivotGrid1
            '
            Me.XrPivotGrid1.CustomTotalCellStyleName = "XrControlStyle_Totals"
            Me.XrPivotGrid1.FieldHeaderStyleName = "XrControlStyle_HeaderPannel"
            Me.XrPivotGrid1.FieldValueGrandTotalStyleName = "XrControlStyle_Totals"
            Me.XrPivotGrid1.FieldValueStyleName = "XrControlStyle_HeaderPannel"
            Me.XrPivotGrid1.FieldValueTotalStyleName = "XrControlStyle_Totals"
            Me.XrPivotGrid1.HeaderGroupLineStyleName = "XrControlStyle_HeaderPannel"
            Me.XrPivotGrid1.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
            Me.XrPivotGrid1.Name = "XrPivotGrid1"
            Me.XrPivotGrid1.OptionsChartDataSource.UpdateDelay = 300
            Me.XrPivotGrid1.SizeF = New System.Drawing.SizeF(211.4583!, 50.0!)
            Me.XrPivotGrid1.TotalCellStyleName = "XrControlStyle_Totals"
            '
            'ParameterOffice
            '
            Me.ParameterOffice.Description = "Office"
            Me.ParameterOffice.Name = "ParameterOffice"
            Me.ParameterOffice.Type = GetType(Integer)
            Me.ParameterOffice.Value = 0
            Me.ParameterOffice.Visible = False
            '
            'XrControlStyle1
            '
            Me.XrControlStyle1.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(192, Byte), Integer))
            Me.XrControlStyle1.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(255, Byte), Integer))
            Me.XrControlStyle1.Name = "XrControlStyle1"
            Me.XrControlStyle1.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrControlStyle1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrPivotGrid2
            '
            Me.XrPivotGrid2.CustomTotalCellStyleName = "XrControlStyle_Totals"
            Me.XrPivotGrid2.FieldHeaderStyleName = "XrControlStyle_HeaderPannel"
            Me.XrPivotGrid2.FieldValueGrandTotalStyleName = "XrControlStyle_Totals"
            Me.XrPivotGrid2.FieldValueStyleName = "XrControlStyle_HeaderPannel"
            Me.XrPivotGrid2.FieldValueTotalStyleName = "XrControlStyle_Totals"
            Me.XrPivotGrid2.HeaderGroupLineStyleName = "XrControlStyle_HeaderPannel"
            Me.XrPivotGrid2.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 52.0!)
            Me.XrPivotGrid2.Name = "XrPivotGrid2"
            Me.XrPivotGrid2.OptionsChartDataSource.UpdateDelay = 300
            Me.XrPivotGrid2.SizeF = New System.Drawing.SizeF(211.4583!, 50.0!)
            Me.XrPivotGrid2.TotalCellStyleName = "XrControlStyle_Totals"
            '
            'XrPageBreak1
            '
            Me.XrPageBreak1.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 50.0!)
            Me.XrPageBreak1.Name = "XrPageBreak1"
            '
            'XrPivotGrid3
            '
            Me.XrPivotGrid3.CustomTotalCellStyleName = "XrControlStyle_Totals"
            Me.XrPivotGrid3.FieldHeaderStyleName = "XrControlStyle_HeaderPannel"
            Me.XrPivotGrid3.FieldValueGrandTotalStyleName = "XrControlStyle_Totals"
            Me.XrPivotGrid3.FieldValueStyleName = "XrControlStyle_HeaderPannel"
            Me.XrPivotGrid3.FieldValueTotalStyleName = "XrControlStyle_Totals"
            Me.XrPivotGrid3.HeaderGroupLineStyleName = "XrControlStyle_HeaderPannel"
            Me.XrPivotGrid3.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 104.0!)
            Me.XrPivotGrid3.Name = "XrPivotGrid3"
            Me.XrPivotGrid3.OptionsChartDataSource.UpdateDelay = 300
            Me.XrPivotGrid3.SizeF = New System.Drawing.SizeF(211.4583!, 50.0!)
            Me.XrPivotGrid3.TotalCellStyleName = "XrControlStyle_Totals"
            '
            'XrPageBreak2
            '
            Me.XrPageBreak2.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 102.0!)
            Me.XrPageBreak2.Name = "XrPageBreak2"
            '
            'ScheduleGridReport
            '
            Me.Bands.AddRange(New DevExpress.XtraReports.UI.Band() {Me.TopMarginBand1, Me.Detail, Me.BottomMarginBand1, Me.PageHeader, Me.PageFooter})
            Me.Landscape = True
            Me.Margins = New System.Drawing.Printing.Margins(25, 25, 25, 50)
            Me.PageHeight = 850
            Me.PageWidth = 1100
            Me.Parameters.AddRange(New DevExpress.XtraReports.Parameters.Parameter() {Me.ParameterOffice})
            Me.ScriptLanguage = DevExpress.XtraReports.ScriptLanguage.VisualBasic
            Me.ScriptReferencesString = "C:\Documents and Settings\Al Longyear\My Documents\Visual Studio 2008\Projects\De" & _
        "btPlus\Executables\DebtPlus.Data.dll"
            Me.Scripts.OnBeforePrint = "ScheduleAnalysisReport_BeforePrint"
            Me.ScriptsSource = resources.GetString("$this.ScriptsSource")
            Me.StyleSheet.AddRange(New DevExpress.XtraReports.UI.XRControlStyle() {Me.XrControlStyle_Header, Me.XrControlStyle_Totals, Me.XrControlStyle_HeaderPannel, Me.XrControlStyle_GroupHeader, Me.XrControlStyle1})
            Me.Version = "11.2"
            CType(Me, System.ComponentModel.ISupportInitialize).EndInit()

        End Sub

        Friend WithEvents XrPivotGrid1 As DevExpress.XtraReports.UI.XRPivotGrid
        Friend WithEvents ParameterOffice As DevExpress.XtraReports.Parameters.Parameter
        Friend WithEvents XrControlStyle1 As DevExpress.XtraReports.UI.XRControlStyle
        Friend WithEvents XrPageBreak1 As DevExpress.XtraReports.UI.XRPageBreak
        Friend WithEvents XrPivotGrid2 As DevExpress.XtraReports.UI.XRPivotGrid
        Friend WithEvents XrPageBreak2 As DevExpress.XtraReports.UI.XRPageBreak
        Friend WithEvents XrPivotGrid3 As DevExpress.XtraReports.UI.XRPivotGrid
    End Class
End Namespace
