Namespace Creditor.CheckVoucher
    <Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
    Partial Class CheckVoucherParametersForm

        'Form overrides dispose to clean up the component list.
        <System.Diagnostics.DebuggerNonUserCode()> _
        Protected Overrides Sub Dispose(ByVal disposing As Boolean)
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
            MyBase.Dispose(disposing)
        End Sub

        'Required by the Windows Form Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Windows Form Designer
        'It can be modified using the Windows Form Designer.  
        'Do not modify it using the code editor.
        <System.Diagnostics.DebuggerStepThrough()> _
        Private Sub InitializeComponent()
            Me.LookUpEdit_bank = New DevExpress.XtraEditors.LookUpEdit()
            Me.LabelControl2 = New DevExpress.XtraEditors.LabelControl()
            Me.TextEdit_checknum = New DevExpress.XtraEditors.TextEdit()
            Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl()
            CType(Me.XrCreditor_param_03_1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.DxErrorProvider1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LookUpEdit_bank.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.TextEdit_checknum.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.SuspendLayout()
            '
            'XrCreditor_param_03_1
            '
            Me.XrCreditor_param_03_1.Location = New System.Drawing.Point(93, 65)
            Me.XrCreditor_param_03_1.Properties.Mask.AutoComplete = DevExpress.XtraEditors.Mask.AutoCompleteType.Strong
            Me.XrCreditor_param_03_1.Properties.Mask.BeepOnError = True
            Me.XrCreditor_param_03_1.Properties.Mask.EditMask = "[A-Z]{1,2}[0-9]{4,}"
            Me.XrCreditor_param_03_1.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx
            Me.XrCreditor_param_03_1.Properties.Mask.UseMaskAsDisplayFormat = True
            Me.XrCreditor_param_03_1.TabIndex = 5
            '
            'Label1
            '
            Me.Label1.Location = New System.Drawing.Point(16, 68)
            Me.Label1.TabIndex = 4
            '
            'ButtonOK
            '
            Me.ButtonOK.Location = New System.Drawing.Point(276, 16)
            Me.ButtonOK.TabIndex = 6
            '
            'ButtonCancel
            '
            Me.ButtonCancel.Location = New System.Drawing.Point(276, 48)
            Me.ButtonCancel.TabIndex = 7
            '
            'DefaultLookAndFeel1
            '
            Me.DefaultLookAndFeel1.LookAndFeel.SkinName = "McSkin"
            '
            'LookUpEdit_bank
            '
            Me.LookUpEdit_bank.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.LookUpEdit_bank.Location = New System.Drawing.Point(93, 39)
            Me.LookUpEdit_bank.Name = "LookUpEdit_bank"
            Me.LookUpEdit_bank.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.LookUpEdit_bank.Properties.Columns.AddRange(New DevExpress.XtraEditors.Controls.LookUpColumnInfo() {New DevExpress.XtraEditors.Controls.LookUpColumnInfo("bank", "ID", 5, DevExpress.Utils.FormatType.Numeric, "f0", True, DevExpress.Utils.HorzAlignment.Far), New DevExpress.XtraEditors.Controls.LookUpColumnInfo("description", "Description", 20, DevExpress.Utils.FormatType.None, "", True, DevExpress.Utils.HorzAlignment.[Default], DevExpress.Data.ColumnSortOrder.Ascending), New DevExpress.XtraEditors.Controls.LookUpColumnInfo("formatted_type", 10, "Type")})
            Me.LookUpEdit_bank.Properties.DisplayMember = "description"
            Me.LookUpEdit_bank.Properties.NullText = ""
            Me.LookUpEdit_bank.Properties.ShowFooter = False
            Me.LookUpEdit_bank.Properties.SortColumnIndex = 1
            Me.LookUpEdit_bank.Properties.ValueMember = "bank"
            Me.LookUpEdit_bank.Size = New System.Drawing.Size(162, 20)
            Me.LookUpEdit_bank.TabIndex = 3
            '
            'LabelControl2
            '
            Me.LabelControl2.Location = New System.Drawing.Point(14, 42)
            Me.LabelControl2.Name = "LabelControl2"
            Me.LabelControl2.Size = New System.Drawing.Size(27, 13)
            Me.LabelControl2.TabIndex = 2
            Me.LabelControl2.Text = "Bank:"
            '
            'TextEdit_checknum
            '
            Me.TextEdit_checknum.Location = New System.Drawing.Point(93, 12)
            Me.TextEdit_checknum.Name = "TextEdit_checknum"
            Me.TextEdit_checknum.Properties.Appearance.Options.UseTextOptions = True
            Me.TextEdit_checknum.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.TextEdit_checknum.Properties.DisplayFormat.FormatString = "f0"
            Me.TextEdit_checknum.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.TextEdit_checknum.Properties.EditFormat.FormatString = "f0"
            Me.TextEdit_checknum.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.TextEdit_checknum.Properties.Mask.BeepOnError = True
            Me.TextEdit_checknum.Properties.Mask.EditMask = "f0"
            Me.TextEdit_checknum.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
            Me.TextEdit_checknum.Size = New System.Drawing.Size(100, 20)
            Me.TextEdit_checknum.TabIndex = 1
            '
            'LabelControl1
            '
            Me.LabelControl1.Location = New System.Drawing.Point(14, 15)
            Me.LabelControl1.Name = "LabelControl1"
            Me.LabelControl1.Size = New System.Drawing.Size(73, 13)
            Me.LabelControl1.TabIndex = 0
            Me.LabelControl1.Text = "Check Number:"
            '
            'CheckVoucherParametersForm
            '
            Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
            Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
            Me.ClientSize = New System.Drawing.Size(364, 101)
            Me.Controls.Add(Me.LookUpEdit_bank)
            Me.Controls.Add(Me.LabelControl2)
            Me.Controls.Add(Me.TextEdit_checknum)
            Me.Controls.Add(Me.LabelControl1)
            Me.Name = "CheckVoucherParametersForm"
            Me.Controls.SetChildIndex(Me.LabelControl1, 0)
            Me.Controls.SetChildIndex(Me.XrCreditor_param_03_1, 0)
            Me.Controls.SetChildIndex(Me.Label1, 0)
            Me.Controls.SetChildIndex(Me.ButtonOK, 0)
            Me.Controls.SetChildIndex(Me.ButtonCancel, 0)
            Me.Controls.SetChildIndex(Me.TextEdit_checknum, 0)
            Me.Controls.SetChildIndex(Me.LabelControl2, 0)
            Me.Controls.SetChildIndex(Me.LookUpEdit_bank, 0)
            CType(Me.XrCreditor_param_03_1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.DxErrorProvider1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LookUpEdit_bank.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.TextEdit_checknum.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            Me.ResumeLayout(False)
            Me.PerformLayout()

        End Sub
        Friend WithEvents LookUpEdit_bank As DevExpress.XtraEditors.LookUpEdit
        Friend WithEvents LabelControl2 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents TextEdit_checknum As DevExpress.XtraEditors.TextEdit
        Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
    End Class
End Namespace
