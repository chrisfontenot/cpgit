#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}

#End Region

Option Compare Binary
Option Explicit On
Option Strict On

Imports System.ComponentModel
Imports DebtPlus.Reports.Template
Imports System.IO
Imports System.Reflection
Imports DebtPlus.Utils
Imports System.Globalization

Namespace Creditor.CheckVoucher
    Public Class CheckVoucherReport
        Inherits DatedTemplateXtraReportClass

        Public Sub New()
            MyBase.New()

            Const ReportName As String = "DebtPlus.Reports.Creditor.CheckVoucher.repx"

            ' See if there is a report reference that we can use
            Dim Fname As String = Path.Combine(DebtPlus.Configuration.Config.ReportsDirectory, ReportName)
            Dim UseDefault As Boolean = True
            Try
                If (Not DebtPlus.Configuration.Config.LocalReportsOnly) AndAlso System.IO.File.Exists(Fname) Then
                    LoadLayout(Fname)
                    UseDefault = False
                End If
            Catch ex As Exception
            End Try

            ' Load the standard report definition if we need a new item
            If UseDefault Then
                Dim asm As Assembly = Assembly.GetExecutingAssembly                                           ' changed
                Using ios As Stream = asm.GetManifestResourceStream(String.Format("DebtPlus.Reports.{0}", ReportName)) ' changed
                    If ios IsNot Nothing Then LoadLayout(ios) '                                                                       changed
                End Using
            End If

            ' Set the script references as needed
            ScriptReferences = DebtPlus.Utils.AssemblyTools.GetAllSiblingAssemblies()                                                        ' changed   
        End Sub


        ''' <summary>
        ''' Report title information
        ''' </summary>
        Public Overrides ReadOnly Property ReportTitle() As String
            Get
                Return "Payment Voucher"
            End Get
        End Property

        Public Overrides ReadOnly Property ReportSubtitle() As String
            Get
                Return String.Empty
            End Get
        End Property


        ''' <summary>
        ''' Trust register pointer
        ''' </summary>
        Public Property Parameter_TrustRegister() As Int32
            Get
                Return Convert.ToInt32(Me.Parameters("ParameterTrustRegister").Value, CultureInfo.InvariantCulture)
            End Get
            Set(ByVal value As Int32)
                Me.Parameters("ParameterTrustRegister").Value = value
            End Set
        End Property


        ''' <summary>
        ''' Bank number for the check number
        ''' </summary>
        Public Property Parameter_Bank() As Int32
            Get
                Return Convert.ToInt32(Me.Parameters("ParameterBank").Value, CultureInfo.InvariantCulture)
            End Get
            Set(ByVal value As Int32)
                Me.Parameters("ParameterBank").Value = value
            End Set
        End Property


        ''' <summary>
        ''' If EFT, which creditor is desired
        ''' </summary>
        Public Property Parameter_Creditor() As String
            Get
                Return Convert.ToString(Me.Parameters("ParameterCreditor").Value, CultureInfo.InvariantCulture)
            End Get
            Set(ByVal value As String)
                Me.Parameters("ParameterCreditor").Value = value
            End Set
        End Property


        ''' <summary>
        ''' Check number
        ''' </summary>
        Public Property Parameter_checknum() As Int64
            Get
                Dim parm As DevExpress.XtraReports.Parameters.Parameter = FindParameter("ParameterCheckNum")
                If parm Is Nothing Then Return 0
                Return Convert.ToInt64(parm.Value)
            End Get
            Set(ByVal value As Int64)
                SetParameter("ParameterCheckNum", GetType(Int64), value, "Check Number", False)
            End Set
        End Property


        ''' <summary>
        ''' Change the parameter value for this report
        ''' </summary>
        Public Overrides Sub SetReportParameter(ByVal Parameter As String, ByVal Value As Object)
            Select Case Parameter
                Case "TrustRegister"
                    Me.Parameters("ParameterTrustRegister").Value = Value
                Case "checknum"
                    Me.Parameters("ParameterCheckNum").Value = Value
                Case "Bank"
                    Me.Parameters("ParameterBank").Value = Value
                Case "Creditor"
                    Me.Parameters("ParameterCreditor").Value = Value
                Case Else
                    MyBase.SetReportParameter(Parameter, Value)
            End Select
        End Sub

        Public Overrides Function NeedParameters() As Boolean
            Return MyBase.NeedParameters() OrElse (Parameter_TrustRegister < 1 AndAlso Parameter_checknum < 1)
        End Function


        ''' <summary>
        ''' Request the parameters for the report from the user
        ''' </summary>
        Public Overrides Function RequestReportParameters() As DialogResult
            Dim answer As DialogResult = DialogResult.OK
            If NeedParameters() Then
                Using frm As New CheckVoucherParametersForm()
                    With frm
                        answer = .ShowDialog()
                        Me.Parameters("ParameterCreditor").Value = .Parameter_Creditor
                        Me.Parameters("ParameterBank").Value = .Parameter_Bank
                        Me.Parameters("ParameterCheckNum").Value = .Parameter_checknum
                    End With
                End Using
            End If
            Return answer
        End Function
    End Class
End Namespace
