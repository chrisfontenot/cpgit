#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}

#End Region

Imports DebtPlus.Reports.Template.Forms

Imports System.Data.SqlClient

Namespace Creditor.CheckVoucher
    Friend Class CheckVoucherParametersForm
        Inherits CreditorParametersForm


        Public Sub New()
            MyBase.New()
            InitializeComponent()
            AddHandler XrCreditor_param_03_1.EditValueChanged, AddressOf FormChanged
            AddHandler LookUpEdit_bank.EditValueChanged, AddressOf FormChanged
            AddHandler TextEdit_checknum.EditValueChanged, AddressOf FormChanged
            AddHandler Load, AddressOf ParametersForm_Load
            AddHandler LookUpEdit_bank.EditValueChanged, AddressOf LookUpEdit_bank_EditValueChanged
        End Sub

        Friend ReadOnly Property Parameter_Bank() As Int32
            Get
                With LookUpEdit_bank
                    If .EditValue Is Nothing OrElse .EditValue Is DBNull.Value Then Return -1
                    Return Convert.ToInt32(.EditValue)
                End With
            End Get
        End Property

        Friend ReadOnly Property Parameter_checknum() As Int64
            Get
                With TextEdit_checknum
                    Return Convert.ToInt64(.EditValue)
                End With
            End Get
        End Property

        Protected Overrides Function HasErrors() As Boolean

            ' There must be a check number or we have an error condition.
            If DebtPlus.Utils.Nulls.DInt(TextEdit_checknum.EditValue) <= 0 Then
                Return True
            End If

            ' A creditor is required if the bank is EFT
            If (Parameter_Creditor = String.Empty AndAlso NeedCreditor()) Then
                Return True
            End If

            Return False
        End Function

        Dim ds As New DataSet("ds")

        Private Sub ParametersForm_Load(ByVal sender As Object, ByVal e As EventArgs)
            ' Load the list of banks suitable for a check voucher
            Using cmd As SqlCommand = New System.Data.SqlClient.SqlCommand()
                With cmd
                    .Connection = New System.Data.SqlClient.SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
                    .CommandText = "SELECT bank, description, [activeflag], [type], case type when 'R' then 'RPPS' else 'Checking' end as formatted_type, case when type='C' and [default] <> 0 then 1 else 0 end as 'default' FROM banks WITH (NOLOCK) WHERE type in ('R','C') ORDER BY description"
                    .CommandType = CommandType.Text
                    .CommandTimeout = 0
                End With

                Using da As New SqlDataAdapter(cmd)
                    da.Fill(ds, "banks")
                End Using
            End Using
            Dim tbl As DataTable = ds.Tables(0)
            tbl.PrimaryKey = New DataColumn() {tbl.Columns("bank")}

            With LookUpEdit_bank
                .Properties.DataSource = tbl.DefaultView
                Dim rows() As DataRow = tbl.Select("[default]<>0 AND [activeflag]<>0", "bank")
                If rows.GetUpperBound(0) >= 0 Then
                    .EditValue = rows(0)("bank")
                End If
            End With

            ButtonOK.Enabled = Not HasErrors()

            ' Enable the creditor when needed
            Dim enabled As Boolean = NeedCreditor()
            Label1.Visible = enabled
            XrCreditor_param_03_1.Visible = enabled
        End Sub

        Private Sub LookUpEdit_bank_EditValueChanged(ByVal sender As Object, ByVal e As EventArgs)
            Dim enabled As Boolean = NeedCreditor()
            Label1.Visible = enabled
            XrCreditor_param_03_1.Visible = enabled
        End Sub

        Private Function NeedCreditor() As Boolean
            Dim Answer As Boolean = False
            With LookUpEdit_bank

                Dim Bank As Int32 = Parameter_Bank
                If Bank > 0 Then
                    Dim row As DataRow = ds.Tables(0).Rows.Find(Bank)
                    If row IsNot Nothing Then
                        Dim Type As String = String.Empty
                        If row("type") IsNot Nothing AndAlso row("type") IsNot DBNull.Value Then
                            Type = Convert.ToString(row("type"))
                        End If
                        If Type <> "C" Then
                            Answer = True
                        End If
                    End If
                End If
            End With

            Return Answer
        End Function
    End Class
End Namespace