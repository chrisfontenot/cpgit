#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}
#End Region

Option Compare Binary
Option Explicit On
Option Strict On

Imports DevExpress.XtraReports.UI

Namespace Creditor.CheckVoucher
    Public Class CheckVoucherReport
        Implements DebtPlus.Interfaces.Creditor.ICreditor

        ''' <summary>
        ''' Create an instance of our report
        ''' </summary>
        Public Sub New()
            MyBase.New()
            InitializeComponent()

            ' Do not do this statement when the routines are moved to the scripting block
            ' RegisterHandlers()
        End Sub

        ''' <summary>
        ''' Register the event handlers
        ''' </summary>
        Private Sub RegisterHandlers()
            AddHandler BeforePrint, AddressOf CheckVoucherReport_BeforePrint
            AddHandler XrLabel_ClientID.BeforePrint, AddressOf XrLabel_ClientID_BeforePrint
            AddHandler XrPanel_CreditorAddress.BeforePrint, AddressOf XrPanel_CreditorAddress_BeforePrint
        End Sub

        ''' <summary>
        ''' Report title information
        ''' </summary>
        Public Overrides ReadOnly Property ReportTitle() As String
            Get
                Return "Payment Voucher"
            End Get
        End Property

        Public Overrides ReadOnly Property ReportSubtitle() As String
            Get
                Return String.Empty
            End Get
        End Property

        ''' <summary>
        ''' Trust register pointer
        ''' </summary>
        Public Property Parameter_TrustRegister() As System.Int32
            Get
                Dim parm As DevExpress.XtraReports.Parameters.Parameter = FindParameter("ParameterTrustRegister")
                If parm Is Nothing Then Return -1
                Return Convert.ToInt32(parm.Value, System.Globalization.CultureInfo.InvariantCulture)
            End Get
            Set(ByVal value As System.Int32)
                SetParameter("ParameterTrustRegister", GetType(Int32), value, "Trust Register ID", False)
            End Set
        End Property

        ''' <summary>
        ''' Bank number for the check number
        ''' </summary>
        Public Property Parameter_Bank() As System.Int32
            Get
                Dim parm As DevExpress.XtraReports.Parameters.Parameter = FindParameter("ParameterBank")
                If parm Is Nothing Then Return -1
                Return Convert.ToInt32(parm.Value, System.Globalization.CultureInfo.InvariantCulture)
            End Get
            Set(ByVal value As System.Int32)
                SetParameter("ParameterBank", GetType(Int32), value, "Bank ID", False)
            End Set
        End Property

        Public Property Creditor As String Implements DebtPlus.Interfaces.Creditor.ICreditor.Creditor
            Get
                Return Parameter_Creditor
            End Get
            Set(value As String)
                Parameter_Creditor = value
            End Set
        End Property

        ''' <summary>
        ''' If EFT, which creditor is desired
        ''' </summary>
        Public Property Parameter_Creditor() As String
            Get
                Dim parm As DevExpress.XtraReports.Parameters.Parameter = FindParameter("ParameterCreditor")
                If parm Is Nothing Then Return String.Empty
                Return Convert.ToString(parm.Value)
            End Get
            Set(ByVal value As String)
                SetParameter("ParameterCreditor", GetType(String), value, "Creditor Label", False)
            End Set
        End Property

        ''' <summary>
        ''' Check number
        ''' </summary>
        Public Property Parameter_checknum() As Int64
            Get
                Dim parm As DevExpress.XtraReports.Parameters.Parameter = FindParameter("ParameterCheckNum")
                If parm Is Nothing Then Return 0
                Return Convert.ToInt64(parm.Value)
            End Get
            Set(ByVal value As Int64)
                SetParameter("ParameterCheckNum", GetType(Int64), value, "Check Number", False)
            End Set
        End Property

        ''' <summary>
        ''' Change the parameter value for this report
        ''' </summary>
        Public Overrides Sub SetReportParameter(ByVal Parameter As String, ByVal Value As Object)
            Select Case Parameter
                Case "TrustRegister"
                    Me.Parameters("ParameterTrustRegister").Value = Value
                Case "CheckNum"
                    Me.Parameters("ParameterCheckNum").Value = Value
                Case "Bank"
                    Me.Parameters("ParameterBank").Value = Value
                Case "Creditor"
                    Me.Parameters("ParameterCreditor").Value = Value
                Case Else
                    MyBase.SetReportParameter(Parameter, Value)
            End Select
        End Sub

        Public Overrides Function NeedParameters() As Boolean
            Return MyBase.NeedParameters() OrElse (Parameter_TrustRegister < 1 AndAlso Parameter_checknum = 0)
        End Function

        ''' <summary>
        ''' Request the parameters for the report from the user
        ''' </summary>
        Public Overrides Function RequestReportParameters() As System.Windows.Forms.DialogResult
            Dim Answer As System.Windows.Forms.DialogResult = DialogResult.OK

            If NeedParameters() Then
                Using frm As New CheckVoucherParametersForm()
                    Answer = frm.ShowDialog()
                    If Answer = DialogResult.OK Then
                        Parameter_Creditor = frm.Parameter_Creditor
                        Parameter_Bank = frm.Parameter_Bank
                        Parameter_checknum = frm.Parameter_checknum
                    End If
                End Using
            End If

            Return Answer
        End Function

        Dim ds As New System.Data.DataSet("ds")

        Private Sub CheckVoucherReport_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs)
            Dim rpt As DevExpress.XtraReports.UI.XtraReport = CType(sender, DevExpress.XtraReports.UI.XtraReport)

            '-- Find the parameter values
            Dim TrustRegister As Int32 = Convert.ToInt32(rpt.Parameters("ParameterTrustRegister").Value, System.Globalization.CultureInfo.InvariantCulture)
            Dim Bank As Int32 = Convert.ToInt32(rpt.Parameters("ParameterBank").Value, System.Globalization.CultureInfo.InvariantCulture)
            Dim Creditor As String = Convert.ToString(rpt.Parameters("ParameterCreditor").Value, System.Globalization.CultureInfo.InvariantCulture)
            Dim checknum As Int64 = Convert.ToInt64(rpt.Parameters("ParameterCheckNum").Value, System.Globalization.CultureInfo.InvariantCulture)
            Dim ctl As DevExpress.XtraReports.UI.XRTableCell

            Try
                Using cn As System.Data.SqlClient.SqlConnection = New System.Data.SqlClient.SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
                    cn.Open()

                    '-- Try to find the trust register given the check number and bank number
                    If TrustRegister <= 0 Then
                        Using cmd As New System.Data.SqlClient.SqlCommand()
                            cmd.Connection = cn
                            cmd.CommandText = "SELECT TOP 1 [trust_register] FROM registers_trust WHERE [tran_type] in ('AD','MD','CM','CR','BW') AND [checknum]=@checknum AND [cleared]<>'D'"
                            cmd.Parameters.Add("@checknum", System.Data.SqlDbType.BigInt).Value = checknum

                            If Bank > 0 Then
                                cmd.CommandText += " AND [bank]=@bank"
                                cmd.Parameters.Add("@bank", System.Data.SqlDbType.Int).Value = Bank
                            End If

                            cmd.CommandType = System.Data.CommandType.Text
                            Dim objAnswer As Object = cmd.ExecuteScalar()
                            If objAnswer IsNot Nothing AndAlso objAnswer IsNot System.DBNull.Value Then
                                TrustRegister = Convert.ToInt32(objAnswer, System.Globalization.CultureInfo.InvariantCulture)
                            End If
                        End Using
                    End If

                    If TrustRegister > 0 Then

                        '-- Find the label from the bank
                        Using cmd As New System.Data.SqlClient.SqlCommand()
                            cmd.Connection = cn
                            cmd.CommandText = "SELECT description FROM banks WHERE bank=@bank"
                            cmd.CommandType = System.Data.CommandType.Text
                            cmd.Parameters.Add("@bank", System.Data.SqlDbType.Int, 0).Value = Bank

                            Dim answer As Object = cmd.ExecuteScalar()
                            If answer IsNot Nothing AndAlso answer IsNot System.DBNull.Value Then
                                ctl = TryCast(rpt.FindControl("XrTableCell_bank_name", True), DevExpress.XtraReports.UI.XRTableCell)
                                If ctl IsNot Nothing Then
                                    ctl.Text = Convert.ToString(answer)
                                End If
                            End If
                        End Using

                        '-- Determine the information for the header fields
                        Dim DateCreated As Date = Date.MinValue
                        Dim DateReconciled As Date = Date.MinValue
                        Dim Status As String = String.Empty
                        Dim TranType As String = String.Empty

                        Using cmd As New System.Data.SqlClient.SqlCommand()
                            cmd.Connection = cn
                            cmd.CommandType = System.Data.CommandType.Text
                            cmd.CommandText = "SELECT * FROM registers_trust WHERE trust_register=@trust_register"
                            cmd.Parameters.Add("@trust_register", System.Data.SqlDbType.Int).Value = TrustRegister

                            Using rd As System.Data.SqlClient.SqlDataReader = cmd.ExecuteReader(System.Data.CommandBehavior.SingleRow)
                                If rd.Read() Then

                                    '-- Obtain the standard information for all checks.
                                    Dim ordinal As System.Int32 = rd.GetOrdinal("tran_type")
                                    If Not rd.IsDBNull(ordinal) Then
                                        TranType = rd.GetString(ordinal).Trim
                                    End If

                                    ordinal = rd.GetOrdinal("date_created")
                                    If Not rd.IsDBNull(ordinal) Then
                                        DateCreated = rd.GetDateTime(ordinal)
                                    End If

                                    ordinal = rd.GetOrdinal("reconciled_date")
                                    If Not rd.IsDBNull(ordinal) Then
                                        DateReconciled = rd.GetDateTime(ordinal)
                                    End If

                                    ordinal = rd.GetOrdinal("cleared")
                                    If Not rd.IsDBNull(ordinal) Then
                                        Status = rd.GetString(ordinal).Trim
                                    End If

                                    '-- If there is no check number then use it from the trust register.
                                    If checknum < 1 Then
                                        ordinal = rd.GetOrdinal("checknum") : If Not rd.IsDBNull(ordinal) Then checknum = rd.GetInt64(ordinal)
                                    End If

                                    Select Case TranType
                                        Case "AD", "MD", "CM"
                                            ordinal = rd.GetOrdinal("creditor")
                                            If Not rd.IsDBNull(ordinal) Then
                                                Creditor = rd.GetString(ordinal).Trim()
                                            End If
                                        Case Else
                                    End Select
                                End If
                            End Using
                        End Using

                        '-- Format the header fields with the information
                        ctl = TryCast(rpt.FindControl("XrTableCell_Creditor", True), DevExpress.XtraReports.UI.XRTableCell)
                        If ctl IsNot Nothing Then
                            ctl.Text = Creditor
                        End If

                        ctl = TryCast(rpt.FindControl("XrTableCell_checknum", True), DevExpress.XtraReports.UI.XRTableCell)
                        If ctl IsNot Nothing Then
                            ctl.Text = checknum.ToString()
                        End If

                        ctl = TryCast(rpt.FindControl("XrTableCell_CheckDate", True), DevExpress.XtraReports.UI.XRTableCell)
                        If ctl IsNot Nothing Then
                            ctl.Text = DateCreated.ToShortDateString()
                        End If

                        Select Case Status
                            Case ""
                                Status = "Pending reconciliation"
                            Case "V", "D"
                                Status = "Voided"
                                If DateReconciled <> Date.MinValue Then Status += String.Format(" on {0:d}", DateReconciled)
                            Case "R"
                                Status = "Reconciled"
                                If DateReconciled <> Date.MinValue Then Status += String.Format(" on {0:d}", DateReconciled)
                            Case "P"
                                Status = "Pending to be printed"
                        End Select

                        ctl = TryCast(rpt.FindControl("XrTableCell_CheckStatus", True), DevExpress.XtraReports.UI.XRTableCell)
                        If ctl IsNot Nothing Then
                            ctl.Text = Status
                        End If
                    End If

                    '-- Build the check voucher information
                    Using cmd As New System.Data.SqlClient.SqlCommand()
                        cmd.Connection = cn
                        cmd.CommandType = System.Data.CommandType.StoredProcedure
                        cmd.CommandText = "rpt_CheckVoucher_Detail"
                        cmd.Parameters.Add("@TrustRegister", System.Data.SqlDbType.Int).Value = TrustRegister
                        cmd.Parameters.Add("@Creditor", System.Data.SqlDbType.VarChar, 20).Value = Creditor
                        cmd.CommandTimeout = 0

                        Using da As New System.Data.SqlClient.SqlDataAdapter(cmd)
                            da.Fill(ds, "transactions")

                            '-- Set the view to the detail information
                            Dim tbl As System.Data.DataTable = ds.Tables("transactions")
                            If tbl IsNot Nothing Then
                                tbl.Columns.Add("net", GetType(Decimal), "[amount]-[deducted]")
                                rpt.DataSource = tbl.DefaultView
                            End If
                        End Using
                    End Using

                    '-- Update the parameters since they are used later
                    rpt.Parameters("ParameterTrustRegister").Value = TrustRegister
                    rpt.Parameters("ParameterBank").Value = Bank
                    rpt.Parameters("ParameterCreditor").Value = Creditor
                    rpt.Parameters("ParameterCheckNum").Value = checknum
                End Using

            Catch ex As System.Data.SqlClient.SqlException
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error reading voucher information")
            End Try
        End Sub

        Private Sub XrLabel_ClientID_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs)
            Dim lbl As DevExpress.XtraReports.UI.XRLabel = CType(sender, DevExpress.XtraReports.UI.XRLabel)
            Dim rpt As DevExpress.XtraReports.UI.XtraReport = CType(lbl.Report, DevExpress.XtraReports.UI.XtraReport)
            lbl.Text = DebtPlus.Utils.Format.Client.FormatClientID(rpt.GetCurrentColumnValue("client"))
        End Sub

        Private Sub XrPanel_CreditorAddress_BeforePrint(ByVal sender As System.Object, ByVal e As System.Drawing.Printing.PrintEventArgs)
            Dim pnl As DevExpress.XtraReports.UI.XRPanel = CType(sender, DevExpress.XtraReports.UI.XRPanel)
            Dim rpt As DevExpress.XtraReports.UI.XtraReport = CType(pnl.Report, DevExpress.XtraReports.UI.XtraReport)

            Dim Creditor As String = Convert.ToString(rpt.Parameters("ParameterCreditor").Value, System.Globalization.CultureInfo.InvariantCulture)

            Try
                Dim sb As New System.Text.StringBuilder
                Dim postalcode As String = System.String.Empty

                Using cn As System.Data.SqlClient.SqlConnection = New System.Data.SqlClient.SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
                    cn.Open()
                    Using cmd As New System.Data.SqlClient.SqlCommand()
                        cmd.Connection = cn
                        cmd.CommandText = "rpt_CreditorAddress_P"
                        cmd.CommandType = System.Data.CommandType.StoredProcedure
                        cmd.Parameters.Add("@creditor", System.Data.SqlDbType.VarChar, 10).Value = Creditor

                        Using rd As System.Data.SqlClient.SqlDataReader = cmd.ExecuteReader(System.Data.CommandBehavior.CloseConnection Or System.Data.CommandBehavior.SingleRow)
                            If rd.Read() Then

                                '-- We need the postal code by itself. Find it first.
                                Dim col As System.Int32 = rd.GetOrdinal("zipcode")
                                If Not rd.IsDBNull(col) Then
                                    postalcode = rd.GetString(col).Trim
                                End If

                                '-- Build the address string for the report.
                                For Each NameString As String In New String() {"addr1", "addr2", "addr3", "addr4", "addr5", "addr6", "addr7"}
                                    Dim Value As String
                                    col = rd.GetOrdinal(NameString)
                                    If Not rd.IsDBNull(col) Then
                                        Value = rd.GetString(col).Trim
                                        If Value <> System.String.Empty Then
                                            sb.Append(Environment.NewLine)
                                            sb.Append(Value)
                                        End If
                                    End If
                                Next NameString
                            End If
                        End Using
                    End Using
                End Using

                '-- Set the address string
                If sb.Length > 0 Then sb.Remove(0, 2)
                Dim ctlText As DevExpress.XtraReports.UI.XRLabel = TryCast(rpt.FindControl("XrLabel_Creditor_Address", True), DevExpress.XtraReports.UI.XRLabel)
                If ctlText IsNot Nothing Then
                    ctlText.Text = sb.ToString
                End If

                '-- Set the postal code into the post-net field
                Dim ctlBarCode As DevExpress.XtraReports.UI.XRBarCode = TryCast(rpt.FindControl("XrBarCode_PostalCode", True), DevExpress.XtraReports.UI.XRBarCode)
                If ctlBarCode IsNot Nothing Then
                    If postalcode <> System.String.Empty Then
                        ctlBarCode.Text = DebtPlus.Utils.Format.Strings.DigitsOnly(postalcode)
                        ctlBarCode.Visible = True
                    Else
                        ctlBarCode.Text = String.Empty
                        ctlBarCode.Visible = False
                    End If
                End If

            Catch ex As System.Data.SqlClient.SqlException
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error reading creditor address")
            End Try
        End Sub
    End Class
End Namespace
