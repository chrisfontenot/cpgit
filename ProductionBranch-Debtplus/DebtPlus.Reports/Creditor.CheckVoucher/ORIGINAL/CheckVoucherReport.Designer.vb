Namespace Creditor.CheckVoucher
    <Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
    Partial Class CheckVoucherReport
        Inherits DebtPlus.Reports.Template.TemplateXtraReportClass

        'XtraReport overrides dispose to clean up the component list.
        <System.Diagnostics.DebuggerNonUserCode()> _
        Protected Overrides Sub Dispose(ByVal disposing As Boolean)
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
            MyBase.Dispose(disposing)
        End Sub

        'Required by the Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Designer
        'It can be modified using the Designer.  
        'Do not modify it using the code editor.
        <System.Diagnostics.DebuggerStepThrough()> _
        Private Sub InitializeComponent()
            Dim PostNetGenerator1 As DevExpress.XtraPrinting.BarCode.PostNetGenerator = New DevExpress.XtraPrinting.BarCode.PostNetGenerator()
            Dim XrSummary1 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary()
            Dim XrSummary2 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary()
            Dim XrSummary3 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary()
            Dim XrSummary4 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary()
            Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(CheckVoucherReport))
            Me.XrPanel1 = New DevExpress.XtraReports.UI.XRPanel()
            Me.XrLabel14 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel8 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel9 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel5 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel3 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel2 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel1 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrPanel_CreditorAddress = New DevExpress.XtraReports.UI.XRPanel()
            Me.XrLabel_Creditor_Address = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrBarCode_PostalCode = New DevExpress.XtraReports.UI.XRBarCode()
            Me.XrTable1 = New DevExpress.XtraReports.UI.XRTable()
            Me.XrTableRow5 = New DevExpress.XtraReports.UI.XRTableRow()
            Me.XrTableCell3 = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableCell_bank_name = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableCell30 = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableCell19 = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableCell_Creditor = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableRow1 = New DevExpress.XtraReports.UI.XRTableRow()
            Me.XrTableCell2 = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableCell_checknum = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableCell34 = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableCell6 = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableCell_CheckDate = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableRow2 = New DevExpress.XtraReports.UI.XRTableRow()
            Me.XrTableCell1 = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableCell_CheckStatus = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableCell31 = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableCell8 = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableCell16 = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrLabel_Net = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_ClientID = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_ClientName = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_Gross = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_Deducted = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_Billed = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_AccountNumber = New DevExpress.XtraReports.UI.XRLabel()
            Me.ReportFooter = New DevExpress.XtraReports.UI.ReportFooterBand()
            Me.XrLabel_TotalBilled = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_TotalGross = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_TotalNet = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_TotalDeducted = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel4 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLine1 = New DevExpress.XtraReports.UI.XRLine()
            Me.ParameterTrustRegister = New DevExpress.XtraReports.Parameters.Parameter()
            Me.ParameterBank = New DevExpress.XtraReports.Parameters.Parameter()
            Me.ParameterCheckNum = New DevExpress.XtraReports.Parameters.Parameter()
            Me.ParameterCreditor = New DevExpress.XtraReports.Parameters.Parameter()
            CType(Me.XrTable1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
            '
            'PageHeader
            '
            Me.PageHeader.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrTable1, Me.XrPanel_CreditorAddress, Me.XrPanel1})
            Me.PageHeader.HeightF = 349.0!
            Me.PageHeader.Controls.SetChildIndex(Me.XrPanel_AgencyAddress, 0)
            Me.PageHeader.Controls.SetChildIndex(Me.XrLabel_Title, 0)
            Me.PageHeader.Controls.SetChildIndex(Me.XrLine_Title, 0)
            Me.PageHeader.Controls.SetChildIndex(Me.XrLabel_Subtitle, 0)
            Me.PageHeader.Controls.SetChildIndex(Me.XrPageInfo1, 0)
            Me.PageHeader.Controls.SetChildIndex(Me.XrPanel1, 0)
            Me.PageHeader.Controls.SetChildIndex(Me.XrPanel_CreditorAddress, 0)
            Me.PageHeader.Controls.SetChildIndex(Me.XrTable1, 0)
            '
            'XrLabel_Title
            '
            Me.XrLabel_Title.StylePriority.UseFont = False
            '
            'XrPageInfo_PageNumber
            '
            Me.XrPageInfo_PageNumber.StylePriority.UseTextAlignment = False
            '
            'XRLabel_Agency_Name
            '
            Me.XRLabel_Agency_Name.StylePriority.UseTextAlignment = False
            '
            'XrLabel_Agency_Address3
            '
            Me.XrLabel_Agency_Address3.StylePriority.UseFont = False
            Me.XrLabel_Agency_Address3.StylePriority.UseTextAlignment = False
            '
            'XrLabel_Agency_Address1
            '
            Me.XrLabel_Agency_Address1.StylePriority.UseFont = False
            Me.XrLabel_Agency_Address1.StylePriority.UseTextAlignment = False
            '
            'XrLabel_Agency_Phone
            '
            Me.XrLabel_Agency_Phone.StylePriority.UseFont = False
            Me.XrLabel_Agency_Phone.StylePriority.UseTextAlignment = False
            '
            'XrLabel_Agency_Address2
            '
            Me.XrLabel_Agency_Address2.StylePriority.UseFont = False
            Me.XrLabel_Agency_Address2.StylePriority.UseTextAlignment = False
            '
            'Detail
            '
            Me.Detail.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel_Deducted, Me.XrLabel_Billed, Me.XrLabel_AccountNumber, Me.XrLabel_Gross, Me.XrLabel_Net, Me.XrLabel_ClientID, Me.XrLabel_ClientName})
            Me.Detail.HeightF = 15.0!
            '
            'XrPanel1
            '
            Me.XrPanel1.BackColor = System.Drawing.Color.Teal
            Me.XrPanel1.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel14, Me.XrLabel8, Me.XrLabel9, Me.XrLabel5, Me.XrLabel3, Me.XrLabel2, Me.XrLabel1})
            Me.XrPanel1.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 320.5!)
            Me.XrPanel1.Name = "XrPanel1"
            Me.XrPanel1.SizeF = New System.Drawing.SizeF(800.0!, 16.0!)
            '
            'XrLabel14
            '
            Me.XrLabel14.CanGrow = False
            Me.XrLabel14.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel14.ForeColor = System.Drawing.Color.White
            Me.XrLabel14.LocationFloat = New DevExpress.Utils.PointFloat(242.0!, 1.0!)
            Me.XrLabel14.Name = "XrLabel14"
            Me.XrLabel14.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel14.SizeF = New System.Drawing.SizeF(176.0!, 15.0!)
            Me.XrLabel14.StylePriority.UseTextAlignment = False
            Me.XrLabel14.Text = "NAME"
            Me.XrLabel14.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            '
            'XrLabel8
            '
            Me.XrLabel8.CanGrow = False
            Me.XrLabel8.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel8.ForeColor = System.Drawing.Color.White
            Me.XrLabel8.LocationFloat = New DevExpress.Utils.PointFloat(159.0!, 1.0!)
            Me.XrLabel8.Name = "XrLabel8"
            Me.XrLabel8.Padding = New DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 100.0!)
            Me.XrLabel8.SizeF = New System.Drawing.SizeF(83.0!, 15.0!)
            Me.XrLabel8.StylePriority.UsePadding = False
            Me.XrLabel8.StylePriority.UseTextAlignment = False
            Me.XrLabel8.Text = "CLIENT ID"
            Me.XrLabel8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel9
            '
            Me.XrLabel9.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel9.ForeColor = System.Drawing.Color.White
            Me.XrLabel9.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 1.0!)
            Me.XrLabel9.Name = "XrLabel9"
            Me.XrLabel9.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel9.SizeF = New System.Drawing.SizeF(150.0!, 15.0!)
            Me.XrLabel9.Text = "ACCOUNT"
            '
            'XrLabel5
            '
            Me.XrLabel5.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel5.ForeColor = System.Drawing.Color.White
            Me.XrLabel5.LocationFloat = New DevExpress.Utils.PointFloat(516.0!, 1.0!)
            Me.XrLabel5.Name = "XrLabel5"
            Me.XrLabel5.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
            Me.XrLabel5.SizeF = New System.Drawing.SizeF(92.0!, 15.0!)
            Me.XrLabel5.StylePriority.UsePadding = False
            Me.XrLabel5.Text = "DEDUCTED"
            Me.XrLabel5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel3
            '
            Me.XrLabel3.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel3.ForeColor = System.Drawing.Color.White
            Me.XrLabel3.LocationFloat = New DevExpress.Utils.PointFloat(424.0!, 1.0!)
            Me.XrLabel3.Name = "XrLabel3"
            Me.XrLabel3.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
            Me.XrLabel3.SizeF = New System.Drawing.SizeF(92.0!, 15.0!)
            Me.XrLabel3.StylePriority.UsePadding = False
            Me.XrLabel3.Text = "GROSS"
            Me.XrLabel3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel2
            '
            Me.XrLabel2.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel2.ForeColor = System.Drawing.Color.White
            Me.XrLabel2.LocationFloat = New DevExpress.Utils.PointFloat(700.0!, 0.0!)
            Me.XrLabel2.Name = "XrLabel2"
            Me.XrLabel2.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
            Me.XrLabel2.SizeF = New System.Drawing.SizeF(92.0!, 15.0!)
            Me.XrLabel2.StylePriority.UsePadding = False
            Me.XrLabel2.Text = "NET"
            Me.XrLabel2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel1
            '
            Me.XrLabel1.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel1.ForeColor = System.Drawing.Color.White
            Me.XrLabel1.LocationFloat = New DevExpress.Utils.PointFloat(608.0!, 1.0!)
            Me.XrLabel1.Name = "XrLabel1"
            Me.XrLabel1.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
            Me.XrLabel1.SizeF = New System.Drawing.SizeF(92.0!, 15.0!)
            Me.XrLabel1.StylePriority.UsePadding = False
            Me.XrLabel1.Text = "BILLED"
            Me.XrLabel1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrPanel_CreditorAddress
            '
            Me.XrPanel_CreditorAddress.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel_Creditor_Address, Me.XrBarCode_PostalCode})
            Me.XrPanel_CreditorAddress.LocationFloat = New DevExpress.Utils.PointFloat(57.0!, 191.0!)
            Me.XrPanel_CreditorAddress.Name = "XrPanel_CreditorAddress"
            Me.XrPanel_CreditorAddress.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
            Me.XrPanel_CreditorAddress.Scripts.OnBeforePrint = "XrPanel_CreditorAddress_BeforePrint"
            Me.XrPanel_CreditorAddress.SizeF = New System.Drawing.SizeF(300.0!, 117.0!)
            '
            'XrLabel_Creditor_Address
            '
            Me.XrLabel_Creditor_Address.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 17.00001!)
            Me.XrLabel_Creditor_Address.Multiline = True
            Me.XrLabel_Creditor_Address.Name = "XrLabel_Creditor_Address"
            Me.XrLabel_Creditor_Address.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_Creditor_Address.SizeF = New System.Drawing.SizeF(300.0!, 100.0!)
            Me.XrLabel_Creditor_Address.Text = "XrLabel_Creditor_Address"
            Me.XrLabel_Creditor_Address.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
            '
            'XrBarCode_PostalCode
            '
            Me.XrBarCode_PostalCode.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
            Me.XrBarCode_PostalCode.Name = "XrBarCode_PostalCode"
            Me.XrBarCode_PostalCode.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
            Me.XrBarCode_PostalCode.ShowText = False
            Me.XrBarCode_PostalCode.SizeF = New System.Drawing.SizeF(300.0!, 16.0!)
            Me.XrBarCode_PostalCode.Symbology = PostNetGenerator1
            Me.XrBarCode_PostalCode.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            '
            'XrTable1
            '
            Me.XrTable1.LocationFloat = New DevExpress.Utils.PointFloat(10.00001!, 112.0!)
            Me.XrTable1.Name = "XrTable1"
            Me.XrTable1.Rows.AddRange(New DevExpress.XtraReports.UI.XRTableRow() {Me.XrTableRow5, Me.XrTableRow1, Me.XrTableRow2})
            Me.XrTable1.SizeF = New System.Drawing.SizeF(732.0!, 44.99999!)
            Me.XrTable1.StyleName = "XrControlStyle_Header"
            '
            'XrTableRow5
            '
            Me.XrTableRow5.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.XrTableCell3, Me.XrTableCell_bank_name, Me.XrTableCell30, Me.XrTableCell19, Me.XrTableCell_Creditor})
            Me.XrTableRow5.Name = "XrTableRow5"
            Me.XrTableRow5.Weight = 0.51498151678851134R
            '
            'XrTableCell3
            '
            Me.XrTableCell3.Name = "XrTableCell3"
            Me.XrTableCell3.Padding = New DevExpress.XtraPrinting.PaddingInfo(10, 0, 0, 0, 100.0!)
            Me.XrTableCell3.StylePriority.UsePadding = False
            Me.XrTableCell3.Text = "Bank ID:"
            Me.XrTableCell3.Weight = 0.51907936471407523R
            '
            'XrTableCell_bank_name
            '
            Me.XrTableCell_bank_name.Name = "XrTableCell_bank_name"
            Me.XrTableCell_bank_name.Padding = New DevExpress.XtraPrinting.PaddingInfo(10, 0, 0, 0, 100.0!)
            Me.XrTableCell_bank_name.StylePriority.UsePadding = False
            Me.XrTableCell_bank_name.Weight = 0.90305165775486673R
            '
            'XrTableCell30
            '
            Me.XrTableCell30.Name = "XrTableCell30"
            Me.XrTableCell30.Weight = 0.17528283791463881R
            '
            'XrTableCell19
            '
            Me.XrTableCell19.Name = "XrTableCell19"
            Me.XrTableCell19.Padding = New DevExpress.XtraPrinting.PaddingInfo(10, 0, 0, 0, 100.0!)
            Me.XrTableCell19.StylePriority.UsePadding = False
            Me.XrTableCell19.Text = "Creditor ID:"
            Me.XrTableCell19.Weight = 0.42544543156858361R
            '
            'XrTableCell_Creditor
            '
            Me.XrTableCell_Creditor.Name = "XrTableCell_Creditor"
            Me.XrTableCell_Creditor.Padding = New DevExpress.XtraPrinting.PaddingInfo(10, 0, 0, 0, 100.0!)
            Me.XrTableCell_Creditor.StylePriority.UsePadding = False
            Me.XrTableCell_Creditor.Weight = 0.97714070804783559R
            '
            'XrTableRow1
            '
            Me.XrTableRow1.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.XrTableCell2, Me.XrTableCell_checknum, Me.XrTableCell34, Me.XrTableCell6, Me.XrTableCell_CheckDate})
            Me.XrTableRow1.Name = "XrTableRow1"
            Me.XrTableRow1.Weight = 0.51498151678851145R
            '
            'XrTableCell2
            '
            Me.XrTableCell2.Name = "XrTableCell2"
            Me.XrTableCell2.Padding = New DevExpress.XtraPrinting.PaddingInfo(10, 0, 0, 0, 100.0!)
            Me.XrTableCell2.StyleName = "XrControlStyle_Header"
            Me.XrTableCell2.StylePriority.UsePadding = False
            Me.XrTableCell2.Text = "Check Number:"
            Me.XrTableCell2.Weight = 0.51907931029443166R
            '
            'XrTableCell_checknum
            '
            Me.XrTableCell_checknum.Name = "XrTableCell_checknum"
            Me.XrTableCell_checknum.Padding = New DevExpress.XtraPrinting.PaddingInfo(10, 0, 0, 0, 100.0!)
            Me.XrTableCell_checknum.StylePriority.UsePadding = False
            Me.XrTableCell_checknum.Weight = 0.90305191368685811R
            Me.XrTableCell_checknum.WordWrap = False
            '
            'XrTableCell34
            '
            Me.XrTableCell34.Name = "XrTableCell34"
            Me.XrTableCell34.Weight = 0.17528271863050565R
            '
            'XrTableCell6
            '
            Me.XrTableCell6.Name = "XrTableCell6"
            Me.XrTableCell6.Padding = New DevExpress.XtraPrinting.PaddingInfo(10, 0, 0, 0, 100.0!)
            Me.XrTableCell6.StylePriority.UsePadding = False
            Me.XrTableCell6.Text = "Check Date:"
            Me.XrTableCell6.Weight = 0.42544542172248689R
            '
            'XrTableCell_CheckDate
            '
            Me.XrTableCell_CheckDate.Name = "XrTableCell_CheckDate"
            Me.XrTableCell_CheckDate.Padding = New DevExpress.XtraPrinting.PaddingInfo(10, 0, 0, 0, 100.0!)
            Me.XrTableCell_CheckDate.StylePriority.UsePadding = False
            Me.XrTableCell_CheckDate.Weight = 0.977140635665718R
            '
            'XrTableRow2
            '
            Me.XrTableRow2.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.XrTableCell1, Me.XrTableCell_CheckStatus, Me.XrTableCell31, Me.XrTableCell8, Me.XrTableCell16})
            Me.XrTableRow2.Name = "XrTableRow2"
            Me.XrTableRow2.Weight = 0.51498151678851145R
            '
            'XrTableCell1
            '
            Me.XrTableCell1.Name = "XrTableCell1"
            Me.XrTableCell1.Padding = New DevExpress.XtraPrinting.PaddingInfo(10, 0, 0, 0, 100.0!)
            Me.XrTableCell1.StyleName = "XrControlStyle_Header"
            Me.XrTableCell1.StylePriority.UsePadding = False
            Me.XrTableCell1.Text = "Status:"
            Me.XrTableCell1.Weight = 0.51907931029443166R
            '
            'XrTableCell_CheckStatus
            '
            Me.XrTableCell_CheckStatus.Name = "XrTableCell_CheckStatus"
            Me.XrTableCell_CheckStatus.Padding = New DevExpress.XtraPrinting.PaddingInfo(10, 0, 0, 0, 100.0!)
            Me.XrTableCell_CheckStatus.StylePriority.UsePadding = False
            Me.XrTableCell_CheckStatus.Weight = 0.90305170059869377R
            Me.XrTableCell_CheckStatus.WordWrap = False
            '
            'XrTableCell31
            '
            Me.XrTableCell31.Name = "XrTableCell31"
            Me.XrTableCell31.Weight = 0.17528283299159028R
            '
            'XrTableCell8
            '
            Me.XrTableCell8.Name = "XrTableCell8"
            Me.XrTableCell8.Weight = 0.42544570805762871R
            '
            'XrTableCell16
            '
            Me.XrTableCell16.Name = "XrTableCell16"
            Me.XrTableCell16.Padding = New DevExpress.XtraPrinting.PaddingInfo(10, 0, 0, 0, 100.0!)
            Me.XrTableCell16.StylePriority.UsePadding = False
            Me.XrTableCell16.Weight = 0.97714044805765576R
            '
            'XrLabel_Net
            '
            Me.XrLabel_Net.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "net", "{0:c}")})
            Me.XrLabel_Net.Font = New System.Drawing.Font("Times New Roman", 9.75!)
            Me.XrLabel_Net.ForeColor = System.Drawing.SystemColors.ControlText
            Me.XrLabel_Net.LocationFloat = New DevExpress.Utils.PointFloat(700.0!, 0.0!)
            Me.XrLabel_Net.Name = "XrLabel_Net"
            Me.XrLabel_Net.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_Net.SizeF = New System.Drawing.SizeF(92.0!, 15.0!)
            Me.XrLabel_Net.StylePriority.UseFont = False
            Me.XrLabel_Net.StylePriority.UseForeColor = False
            Me.XrLabel_Net.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel_ClientID
            '
            Me.XrLabel_ClientID.CanGrow = False
            Me.XrLabel_ClientID.Font = New System.Drawing.Font("Times New Roman", 9.75!)
            Me.XrLabel_ClientID.ForeColor = System.Drawing.SystemColors.ControlText
            Me.XrLabel_ClientID.LocationFloat = New DevExpress.Utils.PointFloat(150.0!, 0.0!)
            Me.XrLabel_ClientID.Name = "XrLabel_ClientID"
            Me.XrLabel_ClientID.Padding = New DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 100.0!)
            Me.XrLabel_ClientID.Scripts.OnBeforePrint = "XrLabel_ClientID_BeforePrint"
            Me.XrLabel_ClientID.SizeF = New System.Drawing.SizeF(91.99998!, 14.99999!)
            Me.XrLabel_ClientID.StylePriority.UseFont = False
            Me.XrLabel_ClientID.StylePriority.UseForeColor = False
            Me.XrLabel_ClientID.StylePriority.UsePadding = False
            Me.XrLabel_ClientID.StylePriority.UseTextAlignment = False
            Me.XrLabel_ClientID.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel_ClientName
            '
            Me.XrLabel_ClientName.CanGrow = False
            Me.XrLabel_ClientName.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "name")})
            Me.XrLabel_ClientName.Font = New System.Drawing.Font("Times New Roman", 9.75!)
            Me.XrLabel_ClientName.ForeColor = System.Drawing.SystemColors.ControlText
            Me.XrLabel_ClientName.LocationFloat = New DevExpress.Utils.PointFloat(242.0!, 0.0!)
            Me.XrLabel_ClientName.Name = "XrLabel_ClientName"
            Me.XrLabel_ClientName.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_ClientName.SizeF = New System.Drawing.SizeF(182.0!, 14.99999!)
            Me.XrLabel_ClientName.StylePriority.UseFont = False
            Me.XrLabel_ClientName.StylePriority.UseForeColor = False
            Me.XrLabel_ClientName.StylePriority.UseTextAlignment = False
            Me.XrLabel_ClientName.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            '
            'XrLabel_Gross
            '
            Me.XrLabel_Gross.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "amount", "{0:c}")})
            Me.XrLabel_Gross.Font = New System.Drawing.Font("Times New Roman", 9.75!)
            Me.XrLabel_Gross.ForeColor = System.Drawing.SystemColors.ControlText
            Me.XrLabel_Gross.LocationFloat = New DevExpress.Utils.PointFloat(424.0!, 0.0!)
            Me.XrLabel_Gross.Name = "XrLabel_Gross"
            Me.XrLabel_Gross.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_Gross.SizeF = New System.Drawing.SizeF(92.0!, 15.0!)
            Me.XrLabel_Gross.StylePriority.UseFont = False
            Me.XrLabel_Gross.StylePriority.UseForeColor = False
            Me.XrLabel_Gross.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel_Deducted
            '
            Me.XrLabel_Deducted.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "deducted", "{0:c}")})
            Me.XrLabel_Deducted.Font = New System.Drawing.Font("Times New Roman", 9.75!)
            Me.XrLabel_Deducted.ForeColor = System.Drawing.SystemColors.ControlText
            Me.XrLabel_Deducted.LocationFloat = New DevExpress.Utils.PointFloat(516.0!, 0.0!)
            Me.XrLabel_Deducted.Name = "XrLabel_Deducted"
            Me.XrLabel_Deducted.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_Deducted.SizeF = New System.Drawing.SizeF(92.0!, 15.0!)
            Me.XrLabel_Deducted.StylePriority.UseFont = False
            Me.XrLabel_Deducted.StylePriority.UseForeColor = False
            Me.XrLabel_Deducted.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel_Billed
            '
            Me.XrLabel_Billed.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "billed", "{0:c}")})
            Me.XrLabel_Billed.Font = New System.Drawing.Font("Times New Roman", 9.75!)
            Me.XrLabel_Billed.ForeColor = System.Drawing.SystemColors.ControlText
            Me.XrLabel_Billed.LocationFloat = New DevExpress.Utils.PointFloat(608.0!, 0.0!)
            Me.XrLabel_Billed.Name = "XrLabel_Billed"
            Me.XrLabel_Billed.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_Billed.SizeF = New System.Drawing.SizeF(92.0!, 15.0!)
            Me.XrLabel_Billed.StylePriority.UseFont = False
            Me.XrLabel_Billed.StylePriority.UseForeColor = False
            Me.XrLabel_Billed.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel_AccountNumber
            '
            Me.XrLabel_AccountNumber.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "account_number")})
            Me.XrLabel_AccountNumber.Font = New System.Drawing.Font("Times New Roman", 9.75!)
            Me.XrLabel_AccountNumber.ForeColor = System.Drawing.SystemColors.ControlText
            Me.XrLabel_AccountNumber.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
            Me.XrLabel_AccountNumber.Name = "XrLabel_AccountNumber"
            Me.XrLabel_AccountNumber.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_AccountNumber.SizeF = New System.Drawing.SizeF(150.0!, 15.0!)
            Me.XrLabel_AccountNumber.StylePriority.UseFont = False
            Me.XrLabel_AccountNumber.StylePriority.UseForeColor = False
            '
            'ReportFooter
            '
            Me.ReportFooter.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel_TotalBilled, Me.XrLabel_TotalGross, Me.XrLabel_TotalNet, Me.XrLabel_TotalDeducted, Me.XrLabel4, Me.XrLine1})
            Me.ReportFooter.HeightF = 25.00001!
            Me.ReportFooter.Name = "ReportFooter"
            Me.ReportFooter.StyleName = "XrControlStyle_Totals"
            '
            'XrLabel_TotalBilled
            '
            Me.XrLabel_TotalBilled.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "billed")})
            Me.XrLabel_TotalBilled.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel_TotalBilled.ForeColor = System.Drawing.SystemColors.ControlText
            Me.XrLabel_TotalBilled.LocationFloat = New DevExpress.Utils.PointFloat(608.0!, 10.00001!)
            Me.XrLabel_TotalBilled.Name = "XrLabel_TotalBilled"
            Me.XrLabel_TotalBilled.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_TotalBilled.SizeF = New System.Drawing.SizeF(92.0!, 15.0!)
            Me.XrLabel_TotalBilled.StylePriority.UseFont = False
            Me.XrLabel_TotalBilled.StylePriority.UseForeColor = False
            XrSummary1.FormatString = "{0:c}"
            XrSummary1.IgnoreNullValues = True
            XrSummary1.Running = DevExpress.XtraReports.UI.SummaryRunning.Report
            Me.XrLabel_TotalBilled.Summary = XrSummary1
            Me.XrLabel_TotalBilled.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel_TotalGross
            '
            Me.XrLabel_TotalGross.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "amount")})
            Me.XrLabel_TotalGross.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel_TotalGross.ForeColor = System.Drawing.SystemColors.ControlText
            Me.XrLabel_TotalGross.LocationFloat = New DevExpress.Utils.PointFloat(423.0!, 10.00001!)
            Me.XrLabel_TotalGross.Name = "XrLabel_TotalGross"
            Me.XrLabel_TotalGross.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_TotalGross.SizeF = New System.Drawing.SizeF(92.0!, 15.0!)
            Me.XrLabel_TotalGross.StylePriority.UseFont = False
            Me.XrLabel_TotalGross.StylePriority.UseForeColor = False
            XrSummary2.FormatString = "{0:c}"
            XrSummary2.IgnoreNullValues = True
            XrSummary2.Running = DevExpress.XtraReports.UI.SummaryRunning.Report
            Me.XrLabel_TotalGross.Summary = XrSummary2
            Me.XrLabel_TotalGross.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel_TotalNet
            '
            Me.XrLabel_TotalNet.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "net")})
            Me.XrLabel_TotalNet.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel_TotalNet.ForeColor = System.Drawing.SystemColors.ControlText
            Me.XrLabel_TotalNet.LocationFloat = New DevExpress.Utils.PointFloat(700.3334!, 10.00001!)
            Me.XrLabel_TotalNet.Name = "XrLabel_TotalNet"
            Me.XrLabel_TotalNet.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_TotalNet.SizeF = New System.Drawing.SizeF(91.66656!, 14.99999!)
            Me.XrLabel_TotalNet.StylePriority.UseFont = False
            Me.XrLabel_TotalNet.StylePriority.UseForeColor = False
            XrSummary3.FormatString = "{0:c}"
            XrSummary3.IgnoreNullValues = True
            XrSummary3.Running = DevExpress.XtraReports.UI.SummaryRunning.Report
            Me.XrLabel_TotalNet.Summary = XrSummary3
            Me.XrLabel_TotalNet.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel_TotalDeducted
            '
            Me.XrLabel_TotalDeducted.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "deducted")})
            Me.XrLabel_TotalDeducted.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel_TotalDeducted.ForeColor = System.Drawing.SystemColors.ControlText
            Me.XrLabel_TotalDeducted.LocationFloat = New DevExpress.Utils.PointFloat(515.0!, 10.00001!)
            Me.XrLabel_TotalDeducted.Name = "XrLabel_TotalDeducted"
            Me.XrLabel_TotalDeducted.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_TotalDeducted.SizeF = New System.Drawing.SizeF(92.0!, 15.0!)
            Me.XrLabel_TotalDeducted.StylePriority.UseFont = False
            Me.XrLabel_TotalDeducted.StylePriority.UseForeColor = False
            XrSummary4.FormatString = "{0:c}"
            XrSummary4.IgnoreNullValues = True
            XrSummary4.Running = DevExpress.XtraReports.UI.SummaryRunning.Report
            Me.XrLabel_TotalDeducted.Summary = XrSummary4
            Me.XrLabel_TotalDeducted.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel4
            '
            Me.XrLabel4.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel4.ForeColor = System.Drawing.SystemColors.ControlText
            Me.XrLabel4.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 10.00001!)
            Me.XrLabel4.Name = "XrLabel4"
            Me.XrLabel4.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel4.SizeF = New System.Drawing.SizeF(150.0!, 15.0!)
            Me.XrLabel4.StylePriority.UseFont = False
            Me.XrLabel4.StylePriority.UseForeColor = False
            Me.XrLabel4.StylePriority.UseTextAlignment = False
            Me.XrLabel4.Text = "TOTALS"
            Me.XrLabel4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            '
            'XrLine1
            '
            Me.XrLine1.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
            Me.XrLine1.Name = "XrLine1"
            Me.XrLine1.SizeF = New System.Drawing.SizeF(800.0!, 10.0!)
            Me.XrLine1.StyleName = "XrControlStyle_Totals"
            '
            'ParameterTrustRegister
            '
            Me.ParameterTrustRegister.Description = "Trust Register"
            Me.ParameterTrustRegister.Name = "ParameterTrustRegister"
            Me.ParameterTrustRegister.Type = GetType(Integer)
            Me.ParameterTrustRegister.Value = 0
            Me.ParameterTrustRegister.Visible = False
            '
            'ParameterBank
            '
            Me.ParameterBank.Description = "Bank"
            Me.ParameterBank.Name = "ParameterBank"
            Me.ParameterBank.Type = GetType(Integer)
            Me.ParameterBank.Value = 0
            Me.ParameterBank.Visible = False
            '
            'ParameterCheckNum
            '
            Me.ParameterCheckNum.Description = "Check Number"
            Me.ParameterCheckNum.Name = "ParameterCheckNum"
            Me.ParameterCheckNum.Type = GetType(Long)
            Me.ParameterCheckNum.Value = 0
            Me.ParameterCheckNum.Visible = False
            '
            'ParameterCreditor
            '
            Me.ParameterCreditor.Description = "Creditor"
            Me.ParameterCreditor.Name = "ParameterCreditor"
            Me.ParameterCreditor.Value = ""
            Me.ParameterCreditor.Visible = False
            '
            'CheckVoucherReport
            '
            Me.Bands.AddRange(New DevExpress.XtraReports.UI.Band() {Me.Detail, Me.PageHeader, Me.PageFooter, Me.TopMarginBand1, Me.BottomMarginBand1, Me.ReportFooter})
            Me.Parameters.AddRange(New DevExpress.XtraReports.Parameters.Parameter() {Me.ParameterTrustRegister, Me.ParameterBank, Me.ParameterCheckNum, Me.ParameterCreditor})
            Me.RequestParameters = False
            Me.ScriptLanguage = DevExpress.XtraReports.ScriptLanguage.VisualBasic
            Me.ScriptReferencesString = resources.GetString("$this.ScriptReferencesString")
            Me.Scripts.OnBeforePrint = "CheckVoucherReport_BeforePrint"
            Me.ScriptsSource = resources.GetString("$this.ScriptsSource")
            Me.StyleSheet.AddRange(New DevExpress.XtraReports.UI.XRControlStyle() {Me.XrControlStyle_Header, Me.XrControlStyle_Totals, Me.XrControlStyle_HeaderPannel, Me.XrControlStyle_GroupHeader})
            Me.Version = "11.2"
            Me.Controls.SetChildIndex(Me.ReportFooter, 0)
            Me.Controls.SetChildIndex(Me.BottomMarginBand1, 0)
            Me.Controls.SetChildIndex(Me.TopMarginBand1, 0)
            Me.Controls.SetChildIndex(Me.PageFooter, 0)
            Me.Controls.SetChildIndex(Me.PageHeader, 0)
            Me.Controls.SetChildIndex(Me.Detail, 0)
            CType(Me.XrTable1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me, System.ComponentModel.ISupportInitialize).EndInit()

        End Sub
        Friend WithEvents XrTable1 As DevExpress.XtraReports.UI.XRTable
        Friend WithEvents XrTableRow1 As DevExpress.XtraReports.UI.XRTableRow
        Friend WithEvents XrTableCell2 As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents XrTableCell_checknum As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents XrTableRow2 As DevExpress.XtraReports.UI.XRTableRow
        Friend WithEvents XrTableCell1 As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents XrTableCell_CheckStatus As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents XrPanel_CreditorAddress As DevExpress.XtraReports.UI.XRPanel
        Friend WithEvents XrLabel_Creditor_Address As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrBarCode_PostalCode As DevExpress.XtraReports.UI.XRBarCode
        Friend WithEvents XrPanel1 As DevExpress.XtraReports.UI.XRPanel
        Friend WithEvents XrLabel14 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel8 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel9 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel5 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel3 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel2 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel1 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_Deducted As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_Billed As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_AccountNumber As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_Gross As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_Net As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_ClientID As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_ClientName As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents ReportFooter As DevExpress.XtraReports.UI.ReportFooterBand
        Friend WithEvents XrLabel_TotalBilled As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_TotalGross As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_TotalNet As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_TotalDeducted As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel4 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLine1 As DevExpress.XtraReports.UI.XRLine
        Friend WithEvents ParameterTrustRegister As DevExpress.XtraReports.Parameters.Parameter
        Friend WithEvents ParameterBank As DevExpress.XtraReports.Parameters.Parameter
        Friend WithEvents ParameterCheckNum As DevExpress.XtraReports.Parameters.Parameter
        Friend WithEvents ParameterCreditor As DevExpress.XtraReports.Parameters.Parameter
        Friend WithEvents XrTableRow5 As DevExpress.XtraReports.UI.XRTableRow
        Friend WithEvents XrTableCell3 As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents XrTableCell_bank_name As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents XrTableCell30 As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents XrTableCell19 As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents XrTableCell_Creditor As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents XrTableCell34 As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents XrTableCell6 As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents XrTableCell_CheckDate As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents XrTableCell31 As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents XrTableCell8 As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents XrTableCell16 As DevExpress.XtraReports.UI.XRTableCell
    End Class
End Namespace
