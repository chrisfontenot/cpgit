#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}
#End Region

Option Compare Binary
Option Explicit On
Option Strict On

Imports System.IO
Imports System.Reflection
Imports DebtPlus.Utils

Namespace Tables.Clients
    Public Class ClientsListReport
        Inherits Template.TemplateXtraReportClass

        ''' <summary>
        '''     Create an instance of our report
        ''' </summary>
        Public Sub New()
            MyBase.new()

            Const ReportName As String = "DebtPlus.Reports.Tables.Clients.repx"

            ' See if there is a report reference that we can use
            Dim Fname As String = System.IO.Path.Combine(DebtPlus.Configuration.Config.ReportsDirectory, ReportName)
            Dim UseDefault As Boolean = True
            Try
                If System.IO.File.Exists(Fname) Then
                    LoadLayout(Fname)
                    UseDefault = False
                End If
            Catch ex As Exception
            End Try

            ' Load the standard report definition if we need a new item
            If UseDefault Then
                Dim asm As Assembly = Assembly.GetExecutingAssembly                                                    ' changed
                Using ios As Stream = asm.GetManifestResourceStream(String.Format("DebtPlus.Reports.{0}", ReportName)) ' changed
                    If ios IsNot Nothing Then LoadLayout(ios) ' changed
                End Using
            End If

            ' Set the script references as needed
            ScriptReferences = DebtPlus.Utils.AssemblyTools.GetAllSiblingAssemblies()

            ' Default the parameter values
            Parameter_SortOrder = SortOrderType.client
            Parameter_ReverseOrder = False
            Parameter_IncludeAddress = False
            Parameter_RowCount = 0

            ' Enable the selection expert
            ReportFilter.IsEnabled = True
        End Sub

        Public Enum SortOrderType As System.Int32
            [client] = 0
            [active_status] = 1
            [counselor] = 2
            [last_name] = 3
            [language] = 4
            [office] = 5
            [start_date] = 6
        End Enum

        Public Property Parameter_RowCount() As Int32
            Get
                Return CType(Parameters("ParameterRecordCount").Value, Int32)
            End Get
            Set(ByVal value As System.Int32)
                Parameters("ParameterRecordCount").Value = value
            End Set
        End Property

        Public Property Parameter_SortOrder() As SortOrderType
            Get
                Return CType(Parameters("ParameterSortOrder").Value, SortOrderType)
            End Get
            Set(ByVal value As SortOrderType)
                Parameters("ParameterSortOrder").Value = CType(value, System.Int32)
            End Set
        End Property

        Public Property Parameter_IncludeAddress() As Boolean
            Get
                Return CType(Parameters("ParameterIncludeAddresses").Value, Boolean)
            End Get
            Set(ByVal value As Boolean)
                Parameters("ParameterIncludeAddresses").Value = value
            End Set
        End Property

        Public Property Parameter_ReverseOrder() As Boolean
            Get
                Return CType(Parameters("ParameterReverseOrder").Value, Boolean)
            End Get
            Set(ByVal value As Boolean)
                Parameters("ParameterReverseOrder").Value = value
            End Set
        End Property

        Public Overrides Sub SetReportParameter(ByVal Parameter As String, ByVal Value As Object)
            Select Case Parameter
                Case "IncludeAddress"
                    Parameter_IncludeAddress = Convert.ToBoolean(Value)
                Case "ReverseOrder"
                    Parameter_ReverseOrder = Convert.ToBoolean(Value)
                Case "ItemCount"
                    Parameter_RowCount = Convert.ToInt32(Value)
                Case "SortOrder"
                    Try
                        Dim EnumValue As SortOrderType = CType(Value, SortOrderType)
                        Parameter_SortOrder = EnumValue
                    Catch ex As Exception
                        Parameter_SortOrder = SortOrderType.client
                    End Try
                Case Else
                    MyBase.SetReportParameter(Parameter, Value)
            End Select
        End Sub

        Public Overrides ReadOnly Property ReportTitle() As String
            Get
                Return "Clients"
            End Get
        End Property

        Public Overrides Function NeedParameters() As Boolean
            Return True
        End Function

        Public Overrides Function RequestReportParameters() As System.Windows.Forms.DialogResult
            Dim answer As System.Windows.Forms.DialogResult = DialogResult.OK
            If NeedParameters() Then
                Using frm As New Parameters_Form()
                    answer = frm.ShowDialog
                    Parameter_IncludeAddress = frm.Parameter_Include_Addresses
                    Parameter_SortOrder = frm.Parameter_SortOrder
                    Parameter_ReverseOrder = frm.Parameter_ReverseOrder
                    Parameter_RowCount = frm.Parameter_RowCount
                End Using
            End If
            Return answer
        End Function
    End Class
End Namespace
