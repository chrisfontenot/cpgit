#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}
#End Region

Namespace Tables.Clients
    Public Class Parameters_Form
        Inherits Template.Forms.ReportParametersForm

        Public Parameter_Include_Addresses As Boolean = False
        Public Parameter_ReverseOrder As Boolean = False
        Public Parameter_SortOrder As ClientsListReport.SortOrderType
        Public Parameter_RowCount As System.Int32 = 0

        Public Sub New()
            MyBase.New()
            InitializeComponent()
            AddHandler Me.Load, AddressOf Parameters_Form_Load
            AddHandler TextEdit1.EditValueChanging, AddressOf TextEdit1_EditValueChanging
        End Sub

        Protected Overrides Sub ButtonOK_Click(ByVal Sender As Object, ByVal e As System.EventArgs)
            MyBase.ButtonOK_Click(Sender, e)
            Parameter_Include_Addresses = CheckEdit1.Checked
            Parameter_ReverseOrder = CheckEdit2.Checked
            Parameter_SortOrder = CType(ComboBoxEdit1.SelectedIndex, ClientsListReport.SortOrderType)
            Parameter_RowCount = Convert.ToInt32(TextEdit1.EditValue)
        End Sub

        Private Sub Parameters_Form_Load(ByVal sender As Object, ByVal e As System.EventArgs)
            ButtonOK.Enabled = True
        End Sub

        Private Sub TextEdit1_EditValueChanging(ByVal sender As Object, ByVal e As DevExpress.XtraEditors.Controls.ChangingEventArgs)
            Dim NewValue As System.Int32 = DebtPlus.Utils.Nulls.DInt(e.NewValue)
            e.Cancel = (NewValue < 0)
        End Sub
    End Class
End Namespace