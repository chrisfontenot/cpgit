#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}
#End Region

Option Compare Binary
Option Explicit On 
Option Strict On

Imports DevExpress.XtraReports.UI

Namespace Tables.Clients
    Public Class ClientsListReport
        Inherits DebtPlus.Reports.Template.TemplateXtraReportClass

        ''' <summary>
        ''' Create an instance of our report
        ''' </summary>
        Public Sub New()
            MyBase.New()
            InitializeComponent()

            '-- Default the parameter values
            Parameter_SortOrder = SortOrderType.client
            Parameter_ReverseOrder = False
            Parameter_IncludeAddress = False
            Parameter_RowCount = 0

            AddHandler BeforePrint, AddressOf ClientsListReport_BeforePrint
            AddHandler ParametersRequestBeforeShow, AddressOf ClientsListReport_ParametersRequestBeforeShow
            AddHandler XrLabel_name.BeforePrint, AddressOf XrLabel_name_BeforePrint

            '-- Enable the selection expert
            ReportFilter.IsEnabled = True
        End Sub

        Public Enum SortOrderType As System.Int32
            [client] = 0
            [active_status] = 1
            [counselor] = 2
            [last_name] = 3
            [language] = 4
            [office] = 5
            [start_date] = 6
        End Enum

        Public Property Parameter_RowCount() As Int32
            Get
                Return CType(Parameters("ParameterRecordCount").Value, Int32)
            End Get
            Set(ByVal value As System.Int32)
                Parameters("ParameterRecordCount").Value = value
            End Set
        End Property

        Public Property Parameter_SortOrder() As SortOrderType
            Get
                Return CType(Parameters("ParameterSortOrder").Value, SortOrderType)
            End Get
            Set(ByVal value As SortOrderType)
                Parameters("ParameterSortOrder").Value = CType(value, System.Int32)
            End Set
        End Property

        Public Property Parameter_IncludeAddress() As Boolean
            Get
                Return CType(Parameters("ParameterIncludeAddresses").Value, Boolean)
            End Get
            Set(ByVal value As Boolean)
                Parameters("ParameterIncludeAddresses").Value = value
            End Set
        End Property

        Public Property Parameter_ReverseOrder() As Boolean
            Get
                Return CType(Parameters("ParameterReverseOrder").Value, Boolean)
            End Get
            Set(ByVal value As Boolean)
                Parameters("ParameterReverseOrder").Value = value
            End Set
        End Property

        Public Overrides Sub SetReportParameter(ByVal Parameter As String, ByVal Value As Object)
            Select Case Parameter
                Case "IncludeAddress"
                    Parameter_IncludeAddress = Convert.ToBoolean(Value)
                Case "ReverseOrder"
                    Parameter_ReverseOrder = Convert.ToBoolean(Value)
                Case "ItemCount"
                    Parameter_RowCount = Convert.ToInt32(Value)
                Case "SortOrder"
                    Try
                        Dim EnumValue As SortOrderType = CType(Value, SortOrderType)
                        Parameter_SortOrder = EnumValue
                    Catch ex As Exception
                        Parameter_SortOrder = SortOrderType.client
                    End Try
                Case Else
                    MyBase.SetReportParameter(Parameter, Value)
            End Select
        End Sub

        Public Overrides ReadOnly Property ReportTitle() As String
            Get
                Return "Clients"
            End Get
        End Property

        Public Overrides Function NeedParameters() As Boolean
            Return True
        End Function

        Public Overrides Function RequestReportParameters() As System.Windows.Forms.DialogResult
            Dim Answer As System.Windows.Forms.DialogResult = DialogResult.OK
            If NeedParameters() Then
                Using frm As New Parameters_Form()
                    Answer = frm.ShowDialog()
                    Parameter_IncludeAddress = frm.Parameter_Include_Addresses
                    Parameter_SortOrder = frm.Parameter_SortOrder
                    Parameter_ReverseOrder = frm.Parameter_ReverseOrder
                    Parameter_RowCount = frm.Parameter_RowCount
                End Using
            End If
            Return Answer
        End Function

#Region " Component Designer generated code "

        'Component overrides dispose to clean up the component list.
        Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
            If disposing Then
                If components IsNot Nothing Then
                    components.Dispose()
                End If
            End If
            MyBase.Dispose(disposing)
        End Sub

        'Required by the Designer
        Private components As System.ComponentModel.IContainer
        Friend WithEvents XrLabel5 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel7 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel6 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel8 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel4 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel3 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel2 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel1 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_name As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_active_status As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_start_date As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_phone As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_language As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_counselor As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_office As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_client As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents ParameterSortOrder As DevExpress.XtraReports.Parameters.Parameter
        Friend WithEvents ParameterIncludeAddresses As DevExpress.XtraReports.Parameters.Parameter
        Friend WithEvents ParameterReverseOrder As DevExpress.XtraReports.Parameters.Parameter
        Friend WithEvents ParameterRecordCount As DevExpress.XtraReports.Parameters.Parameter

        'NOTE: The following procedure is required by the Designer
        'It can be modified using the Designer.
        'Do not modify it using the code editor.
        Friend WithEvents XrPanel1 As DevExpress.XtraReports.UI.XRPanel
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
            Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(ClientsListReport))
            Me.XrPanel1 = New DevExpress.XtraReports.UI.XRPanel()
            Me.XrLabel5 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel7 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel6 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel8 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel4 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel3 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel2 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel1 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_name = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_active_status = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_start_date = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_phone = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_language = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_counselor = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_office = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_client = New DevExpress.XtraReports.UI.XRLabel()
            Me.ParameterSortOrder = New DevExpress.XtraReports.Parameters.Parameter()
            Me.ParameterIncludeAddresses = New DevExpress.XtraReports.Parameters.Parameter()
            Me.ParameterReverseOrder = New DevExpress.XtraReports.Parameters.Parameter()
            Me.ParameterRecordCount = New DevExpress.XtraReports.Parameters.Parameter()
            CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
            '
            'PageHeader
            '
            Me.PageHeader.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrPanel1})
            Me.PageHeader.HeightF = 142.0!
            Me.PageHeader.Controls.SetChildIndex(Me.XrPanel1, 0)
            Me.PageHeader.Controls.SetChildIndex(Me.XrPanel_AgencyAddress, 0)
            Me.PageHeader.Controls.SetChildIndex(Me.XrLabel_Title, 0)
            Me.PageHeader.Controls.SetChildIndex(Me.XrLine_Title, 0)
            Me.PageHeader.Controls.SetChildIndex(Me.XrLabel_Subtitle, 0)
            Me.PageHeader.Controls.SetChildIndex(Me.XrPageInfo1, 0)
            '
            'PageFooter
            '
            Me.PageFooter.StylePriority.UseTextAlignment = False
            Me.PageFooter.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter
            '
            'XrLabel_Title
            '
            Me.XrLabel_Title.StylePriority.UseFont = False
            '
            'XrPageInfo_PageNumber
            '
            Me.XrPageInfo_PageNumber.StylePriority.UseTextAlignment = False
            '
            'XRLabel_Agency_Name
            '
            Me.XRLabel_Agency_Name.StylePriority.UseTextAlignment = False
            '
            'XrLabel_Agency_Address3
            '
            Me.XrLabel_Agency_Address3.StylePriority.UseFont = False
            Me.XrLabel_Agency_Address3.StylePriority.UseTextAlignment = False
            '
            'XrLabel_Agency_Address1
            '
            Me.XrLabel_Agency_Address1.StylePriority.UseFont = False
            Me.XrLabel_Agency_Address1.StylePriority.UseTextAlignment = False
            '
            'XrLabel_Agency_Phone
            '
            Me.XrLabel_Agency_Phone.StylePriority.UseFont = False
            Me.XrLabel_Agency_Phone.StylePriority.UseTextAlignment = False
            '
            'XrLabel_Agency_Address2
            '
            Me.XrLabel_Agency_Address2.StylePriority.UseFont = False
            Me.XrLabel_Agency_Address2.StylePriority.UseTextAlignment = False
            '
            'Detail
            '
            Me.Detail.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel_name, Me.XrLabel_active_status, Me.XrLabel_start_date, Me.XrLabel_phone, Me.XrLabel_language, Me.XrLabel_counselor, Me.XrLabel_office, Me.XrLabel_client})
            Me.Detail.HeightF = 15.0!
            '
            'XrPanel1
            '
            Me.XrPanel1.BackColor = System.Drawing.Color.Teal
            Me.XrPanel1.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel5, Me.XrLabel7, Me.XrLabel6, Me.XrLabel8, Me.XrLabel4, Me.XrLabel3, Me.XrLabel2, Me.XrLabel1})
            Me.XrPanel1.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 117.0!)
            Me.XrPanel1.Name = "XrPanel1"
            Me.XrPanel1.SizeF = New System.Drawing.SizeF(800.0!, 17.0!)
            Me.XrPanel1.StyleName = "XrControlStyle_HeaderPannel"
            '
            'XrLabel5
            '
            Me.XrLabel5.LocationFloat = New DevExpress.Utils.PointFloat(466.0!, 1.0!)
            Me.XrLabel5.Name = "XrLabel5"
            Me.XrLabel5.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel5.SizeF = New System.Drawing.SizeF(326.0!, 15.0!)
            Me.XrLabel5.StylePriority.UseTextAlignment = False
            Me.XrLabel5.Text = "NAME"
            Me.XrLabel5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            '
            'XrLabel7
            '
            Me.XrLabel7.LocationFloat = New DevExpress.Utils.PointFloat(351.1429!, 1.0!)
            Me.XrLabel7.Name = "XrLabel7"
            Me.XrLabel7.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel7.SizeF = New System.Drawing.SizeF(50.0!, 15.0!)
            Me.XrLabel7.StylePriority.UseTextAlignment = False
            Me.XrLabel7.Text = "CNSLR"
            Me.XrLabel7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            '
            'XrLabel6
            '
            Me.XrLabel6.LocationFloat = New DevExpress.Utils.PointFloat(429.5714!, 1.0!)
            Me.XrLabel6.Name = "XrLabel6"
            Me.XrLabel6.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel6.SizeF = New System.Drawing.SizeF(33.0!, 15.0!)
            Me.XrLabel6.StylePriority.UseTextAlignment = False
            Me.XrLabel6.Text = "OFF"
            Me.XrLabel6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel8
            '
            Me.XrLabel8.LocationFloat = New DevExpress.Utils.PointFloat(9.0!, 1.0!)
            Me.XrLabel8.Name = "XrLabel8"
            Me.XrLabel8.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel8.SizeF = New System.Drawing.SizeF(58.0!, 15.0!)
            Me.XrLabel8.StylePriority.UseTextAlignment = False
            Me.XrLabel8.Text = "CLIENT"
            Me.XrLabel8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel4
            '
            Me.XrLabel4.LocationFloat = New DevExpress.Utils.PointFloat(296.7143!, 1.0!)
            Me.XrLabel4.Name = "XrLabel4"
            Me.XrLabel4.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel4.SizeF = New System.Drawing.SizeF(51.0!, 15.0!)
            Me.XrLabel4.StylePriority.UseTextAlignment = False
            Me.XrLabel4.Text = "LANG"
            Me.XrLabel4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            '
            'XrLabel3
            '
            Me.XrLabel3.LocationFloat = New DevExpress.Utils.PointFloat(70.42857!, 1.0!)
            Me.XrLabel3.Name = "XrLabel3"
            Me.XrLabel3.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel3.SizeF = New System.Drawing.SizeF(33.0!, 15.0!)
            Me.XrLabel3.StylePriority.UseTextAlignment = False
            Me.XrLabel3.Text = "STS"
            Me.XrLabel3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
            '
            'XrLabel2
            '
            Me.XrLabel2.LocationFloat = New DevExpress.Utils.PointFloat(113.8571!, 1.0!)
            Me.XrLabel2.Name = "XrLabel2"
            Me.XrLabel2.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel2.SizeF = New System.Drawing.SizeF(58.0!, 15.0!)
            Me.XrLabel2.StylePriority.UseTextAlignment = False
            Me.XrLabel2.Text = "START"
            Me.XrLabel2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel1
            '
            Me.XrLabel1.LocationFloat = New DevExpress.Utils.PointFloat(175.2857!, 1.0!)
            Me.XrLabel1.Name = "XrLabel1"
            Me.XrLabel1.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel1.SizeF = New System.Drawing.SizeF(116.0!, 15.0!)
            Me.XrLabel1.StylePriority.UseTextAlignment = False
            Me.XrLabel1.Text = "PHONE"
            Me.XrLabel1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            '
            'XrLabel_name
            '
            Me.XrLabel_name.LocationFloat = New DevExpress.Utils.PointFloat(466.0!, 0.0!)
            Me.XrLabel_name.Multiline = True
            Me.XrLabel_name.Name = "XrLabel_name"
            Me.XrLabel_name.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_name.Scripts.OnBeforePrint = "XrLabel_name_BeforePrint"
            Me.XrLabel_name.SizeF = New System.Drawing.SizeF(326.0!, 15.0!)
            Me.XrLabel_name.StylePriority.UseTextAlignment = False
            Me.XrLabel_name.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            Me.XrLabel_name.WordWrap = False
            '
            'XrLabel_active_status
            '
            Me.XrLabel_active_status.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "active_status")})
            Me.XrLabel_active_status.LocationFloat = New DevExpress.Utils.PointFloat(70.42857!, 0.0!)
            Me.XrLabel_active_status.Name = "XrLabel_active_status"
            Me.XrLabel_active_status.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_active_status.SizeF = New System.Drawing.SizeF(33.0!, 15.0!)
            Me.XrLabel_active_status.StylePriority.UseTextAlignment = False
            Me.XrLabel_active_status.Text = "XrLabel_active_status"
            Me.XrLabel_active_status.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
            Me.XrLabel_active_status.WordWrap = False
            '
            'XrLabel_start_date
            '
            Me.XrLabel_start_date.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "start_date", "{0:d}")})
            Me.XrLabel_start_date.LocationFloat = New DevExpress.Utils.PointFloat(106.8571!, 0.0!)
            Me.XrLabel_start_date.Name = "XrLabel_start_date"
            Me.XrLabel_start_date.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_start_date.SizeF = New System.Drawing.SizeF(65.0!, 15.0!)
            Me.XrLabel_start_date.StylePriority.UseTextAlignment = False
            Me.XrLabel_start_date.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrLabel_start_date.WordWrap = False
            '
            'XrLabel_phone
            '
            Me.XrLabel_phone.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "phone")})
            Me.XrLabel_phone.LocationFloat = New DevExpress.Utils.PointFloat(175.2857!, 0.0!)
            Me.XrLabel_phone.Name = "XrLabel_phone"
            Me.XrLabel_phone.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_phone.SizeF = New System.Drawing.SizeF(118.0!, 15.0!)
            Me.XrLabel_phone.StylePriority.UseTextAlignment = False
            Me.XrLabel_phone.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            Me.XrLabel_phone.WordWrap = False
            '
            'XrLabel_language
            '
            Me.XrLabel_language.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "language")})
            Me.XrLabel_language.LocationFloat = New DevExpress.Utils.PointFloat(296.7143!, 0.0!)
            Me.XrLabel_language.Name = "XrLabel_language"
            Me.XrLabel_language.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_language.SizeF = New System.Drawing.SizeF(51.0!, 15.0!)
            Me.XrLabel_language.StylePriority.UseTextAlignment = False
            Me.XrLabel_language.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            Me.XrLabel_language.WordWrap = False
            '
            'XrLabel_counselor
            '
            Me.XrLabel_counselor.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "counselor")})
            Me.XrLabel_counselor.LocationFloat = New DevExpress.Utils.PointFloat(351.1429!, 0.0!)
            Me.XrLabel_counselor.Name = "XrLabel_counselor"
            Me.XrLabel_counselor.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_counselor.SizeF = New System.Drawing.SizeF(75.0!, 15.0!)
            Me.XrLabel_counselor.StylePriority.UseTextAlignment = False
            Me.XrLabel_counselor.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            Me.XrLabel_counselor.WordWrap = False
            '
            'XrLabel_office
            '
            Me.XrLabel_office.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "office")})
            Me.XrLabel_office.LocationFloat = New DevExpress.Utils.PointFloat(429.5714!, 0.0!)
            Me.XrLabel_office.Name = "XrLabel_office"
            Me.XrLabel_office.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_office.SizeF = New System.Drawing.SizeF(33.0!, 15.0!)
            Me.XrLabel_office.StylePriority.UseTextAlignment = False
            Me.XrLabel_office.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrLabel_office.WordWrap = False
            '
            'XrLabel_client
            '
            Me.XrLabel_client.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "client", "{0:0000000}")})
            Me.XrLabel_client.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
            Me.XrLabel_client.Name = "XrLabel_client"
            Me.XrLabel_client.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_client.Scripts.OnBeforePrint = "XrLabel_client_BeforePrint"
            Me.XrLabel_client.SizeF = New System.Drawing.SizeF(67.0!, 15.0!)
            Me.XrLabel_client.StylePriority.UseTextAlignment = False
            Me.XrLabel_client.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrLabel_client.WordWrap = False
            '
            'ParameterSortOrder
            '
            Me.ParameterSortOrder.Description = "Sort Order"
            Me.ParameterSortOrder.Name = "ParameterSortOrder"
            Me.ParameterSortOrder.Type = GetType(Integer)
            Me.ParameterSortOrder.Value = 0
            Me.ParameterSortOrder.Visible = False
            '
            'ParameterIncludeAddresses
            '
            Me.ParameterIncludeAddresses.Description = "Include Addresses"
            Me.ParameterIncludeAddresses.Name = "ParameterIncludeAddresses"
            Me.ParameterIncludeAddresses.Type = GetType(Boolean)
            Me.ParameterIncludeAddresses.Value = False
            Me.ParameterIncludeAddresses.Visible = False
            '
            'ParameterReverseOrder
            '
            Me.ParameterReverseOrder.Description = "Reverse Order"
            Me.ParameterReverseOrder.Name = "ParameterReverseOrder"
            Me.ParameterReverseOrder.Type = GetType(Boolean)
            Me.ParameterReverseOrder.Value = False
            Me.ParameterReverseOrder.Visible = False
            '
            'ParameterRecordCount
            '
            Me.ParameterRecordCount.Description = "Records"
            Me.ParameterRecordCount.Name = "ParameterRecordCount"
            Me.ParameterRecordCount.Type = GetType(Integer)
            Me.ParameterRecordCount.Value = 0
            Me.ParameterRecordCount.Visible = False
            '
            'ClientsListReport
            '
            Me.Bands.AddRange(New DevExpress.XtraReports.UI.Band() {Me.TopMarginBand1, Me.Detail, Me.BottomMarginBand1, Me.PageHeader, Me.PageFooter})
            Me.Parameters.AddRange(New DevExpress.XtraReports.Parameters.Parameter() {Me.ParameterSortOrder, Me.ParameterIncludeAddresses, Me.ParameterReverseOrder, Me.ParameterRecordCount})
            Me.RequestParameters = False
            Me.ScriptLanguage = DevExpress.XtraReports.ScriptLanguage.VisualBasic
            Me.ScriptReferencesString = "C:\Documents and Settings\Al Longyear\My Documents\Visual Studio 2008\Projects\De" & _
        "btPlus\Executables\DebtPlus.Data.dll"
            Me.Scripts.OnBeforePrint = "ClientsListReport_BeforePrint"
            Me.Scripts.OnParametersRequestBeforeShow = "ClientsListReport_ParametersRequestBeforeShow"
            Me.ScriptsSource = resources.GetString("$this.ScriptsSource")
            Me.StyleSheet.AddRange(New DevExpress.XtraReports.UI.XRControlStyle() {Me.XrControlStyle_Header, Me.XrControlStyle_Totals, Me.XrControlStyle_HeaderPannel, Me.XrControlStyle_GroupHeader})
            Me.Version = "11.2"
            CType(Me, System.ComponentModel.ISupportInitialize).EndInit()

        End Sub

#End Region

        Dim ds As New System.Data.DataSet("ds")

        Private Sub ClientsListReport_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs)
            Dim Rpt As DevExpress.XtraReports.UI.XtraReport = CType(sender, DevExpress.XtraReports.UI.XtraReport)

            Const TableName As String = "clients"
            Dim tbl As System.Data.DataTable = ds.Tables(TableName)
            If tbl Is Nothing Then

                Try
                    Using cmd As New System.Data.SqlClient.SqlCommand()
                        With cmd
                            Dim sb As New System.Text.StringBuilder
                            sb.Append("SELECT")

                            If CType(Rpt.Parameters("ParameterRecordCount").Value, Int32) > 0 Then
                                sb.AppendFormat(" TOP {0:f0}", Rpt.Parameters("ParameterRecordCount").Value)
                            End If

                            sb.Append(" [client], [active_status], [start_date], [phone], [language], [office], [counselor], [last_name_first] as 'name'")
                            If CType(Rpt.Parameters("ParameterIncludeAddresses").Value, Boolean) Then sb.Append(", [addr1], [addr2], [addr3]")
                            sb.Append(" FROM [view_client_address] v WITH (NOLOCK)")

                            Dim OrderString As String
                            If CType(Rpt.Parameters("ParameterReverseOrder").Value, Boolean) Then
                                OrderString = "DESC"
                            Else
                                OrderString = "ASC"
                            End If

                            Select Case Convert.ToInt32(Rpt.Parameters("ParameterSortOrder").Value)
                                Case 1
                                    sb.AppendFormat(" ORDER BY [active_status] {0}, [client] {0}", OrderString)
                                Case 2
                                    sb.AppendFormat(" ORDER BY [counselor] {0}, [client] {0}", OrderString)
                                Case 3
                                    sb.AppendFormat(" ORDER BY [last_name_first] {0}, [client] {0}", OrderString)
                                Case 4
                                    sb.AppendFormat(" ORDER BY [language] {0}, [client] {0}", OrderString)
                                Case 5
                                    sb.AppendFormat(" ORDER BY [office] {0}, [client] {0}", OrderString)
                                Case 6
                                    sb.AppendFormat(" ORDER BY [start_date] {0}, [client] {0}", OrderString)
                                Case Else
                                    sb.AppendFormat(" ORDER BY [client] {0}", OrderString)
                            End Select

                            .CommandText = sb.ToString
                            .CommandType = System.Data.CommandType.Text
                            .CommandTimeout = 0
                            .Connection = New System.Data.SqlClient.SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
                        End With

                        Using da As New System.Data.SqlClient.SqlDataAdapter(cmd)
                            da.Fill(ds, TableName)
                            tbl = ds.Tables(TableName)
                        End Using
                    End Using

                Catch ex As System.Data.SqlClient.SqlException
                    Dim gdr As New DebtPlus.Repository.GetDataResult() : gdr.HandleException(ex) : DebtPlus.UI.Common.ErrorHandling.HandleErrors(gdr, "Error reading client information")
                End Try
            End If

            If tbl IsNot Nothing Then
                Dim Filter As String = CType(Rpt, DebtPlus.Interfaces.Reports.IReportFilter).ReportFilter.ViewFilter
                Dim Ctl As DevExpress.XtraReports.UI.XRControl = Rpt.FindControl("XrLabel_ReportFilter_ViewString", True)
                If Ctl IsNot Nothing Then
                    Ctl.Text = Filter
                End If
                Rpt.DataSource = New System.Data.DataView(tbl, Filter, String.Empty, System.Data.DataViewRowState.CurrentRows)
            End If
        End Sub

        Private Sub XrLabel_name_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs)

            With CType(sender, DevExpress.XtraReports.UI.XRLabel)
                Dim Rpt As DevExpress.XtraReports.UI.XtraReport = CType(.Report, DevExpress.XtraReports.UI.XtraReport)
                Dim sb As New System.Text.StringBuilder
                Dim txt As String

                If Rpt.GetCurrentColumnValue("name") IsNot System.DBNull.Value Then
                    txt = Convert.ToString(Rpt.GetCurrentColumnValue("name")).Trim
                    sb.Append(Environment.NewLine)
                    sb.Append(txt)
                End If

                If CType(Rpt.Parameters("ParameterIncludeAddresses").Value, Boolean) Then
                    For Each Item As String In New String() {"addr1", "addr2", "addr3"}

                        If Rpt.GetCurrentColumnValue(Item) IsNot Nothing AndAlso Rpt.GetCurrentColumnValue(Item) IsNot System.DBNull.Value Then
                            txt = Convert.ToString(Rpt.GetCurrentColumnValue(Item)).Trim
                            If txt <> String.Empty Then
                                sb.Append(Environment.NewLine)
                                sb.Append(txt)
                            End If
                        End If

                    Next
                End If

                If sb.Length > 0 Then sb.Remove(0, 2)
                .Text = sb.ToString
            End With
        End Sub

        Private Sub ClientsListReport_ParametersRequestBeforeShow(ByVal sender As Object, ByVal e As DevExpress.XtraReports.Parameters.ParametersRequestEventArgs)
            For Each item As DevExpress.XtraReports.Parameters.ParameterInfo In e.ParametersInformation
                If item.Parameter.Name = "ParameterSortOrder" Then
                    item.Editor = GetSortOrderEditor()
                End If
            Next
        End Sub

        Private Function GetSortOrderEditor() As System.Windows.Forms.Control
            Dim ctl As New DevExpress.XtraEditors.LookUpEdit()
            With ctl
                With .Properties
                    .DataSource = SortOrderDataSource()
                    .DisplayMember = "description"
                    .ValueMember = "value"
                    .Columns.AddRange(New DevExpress.XtraEditors.Controls.LookUpColumnInfo() {New DevExpress.XtraEditors.Controls.LookUpColumnInfo("value", "ID", 20, DevExpress.Utils.FormatType.Numeric, "f0", False, DevExpress.Utils.HorzAlignment.Far), New DevExpress.XtraEditors.Controls.LookUpColumnInfo("description", "Description", 20, DevExpress.Utils.FormatType.None, "", True, DevExpress.Utils.HorzAlignment.Near)})
                    .ShowHeader = False
                    .ShowFooter = False
                End With
            End With
            Return ctl
        End Function

        Private Function SortOrderDataSource() As System.Data.DataTable
            Dim tbl As New System.Data.DataTable("SortOrder")
            With tbl
                .Columns.Add("value", GetType(System.Int32))
                .Columns.Add("description", GetType(String))
                .Rows.Add(New Object() {0, "By Client ID"})
                .Rows.Add(New Object() {1, "By Active Status"})
                .Rows.Add(New Object() {2, "By Counselor"})
                .Rows.Add(New Object() {3, "By Last Name"})
                .Rows.Add(New Object() {4, "By Language"})
                .Rows.Add(New Object() {5, "By Office"})
                .Rows.Add(New Object() {6, "By Start Date"})
                .AcceptChanges()
            End With
            Return tbl
        End Function
    End Class
End Namespace
