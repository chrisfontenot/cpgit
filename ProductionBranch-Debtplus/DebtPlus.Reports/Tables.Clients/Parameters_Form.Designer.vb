﻿Namespace Tables.Clients
    <Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
    Partial Class Parameters_Form

        'Form overrides dispose to clean up the component list.
        <System.Diagnostics.DebuggerNonUserCode()> _
        Protected Overrides Sub Dispose(ByVal disposing As Boolean)
            Try
                If disposing AndAlso components IsNot Nothing Then
                    components.Dispose()
                End If
            Finally
                MyBase.Dispose(disposing)
            End Try
        End Sub

        'Required by the Windows Form Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Windows Form Designer
        'It can be modified using the Windows Form Designer.  
        'Do not modify it using the code editor.
        <System.Diagnostics.DebuggerStepThrough()> _
        Private Sub InitializeComponent()
            Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl
            Me.ComboBoxEdit1 = New DevExpress.XtraEditors.ComboBoxEdit
            Me.CheckEdit1 = New DevExpress.XtraEditors.CheckEdit
            Me.CheckEdit2 = New DevExpress.XtraEditors.CheckEdit
            Me.LabelControl2 = New DevExpress.XtraEditors.LabelControl
            Me.TextEdit1 = New DevExpress.XtraEditors.TextEdit
            CType(Me.DxErrorProvider1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.ComboBoxEdit1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.CheckEdit1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.CheckEdit2.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.TextEdit1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.SuspendLayout()
            '
            'ButtonOK
            '
            Me.ButtonOK.TabIndex = 6
            '
            'ButtonCancel
            '
            Me.ButtonCancel.TabIndex = 7
            '
            'LabelControl1
            '
            Me.LabelControl1.Location = New System.Drawing.Point(13, 28)
            Me.LabelControl1.Name = "LabelControl1"
            Me.LabelControl1.Size = New System.Drawing.Size(51, 13)
            Me.LabelControl1.TabIndex = 0
            Me.LabelControl1.Text = "Sort Order"
            '
            'ComboBoxEdit1
            '
            Me.ComboBoxEdit1.EditValue = "Client ID"
            Me.ComboBoxEdit1.Location = New System.Drawing.Point(83, 25)
            Me.ComboBoxEdit1.Name = "ComboBoxEdit1"
            Me.ComboBoxEdit1.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[False]
            Me.ComboBoxEdit1.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.ComboBoxEdit1.Properties.Items.AddRange(New Object() {"Client ID", "Active Status", "Counselor", "Name", "Language", "Office", "Start Date"})
            Me.ComboBoxEdit1.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
            Me.ComboBoxEdit1.Size = New System.Drawing.Size(146, 20)
            Me.ComboBoxEdit1.TabIndex = 1
            '
            'CheckEdit1
            '
            Me.CheckEdit1.Location = New System.Drawing.Point(12, 104)
            Me.CheckEdit1.Name = "CheckEdit1"
            Me.CheckEdit1.Properties.Caption = "Include Addresses"
            Me.CheckEdit1.Size = New System.Drawing.Size(217, 19)
            Me.CheckEdit1.TabIndex = 5
            Me.CheckEdit1.ToolTip = "Include the client address information?"
            '
            'CheckEdit2
            '
            Me.CheckEdit2.Location = New System.Drawing.Point(12, 79)
            Me.CheckEdit2.Name = "CheckEdit2"
            Me.CheckEdit2.Properties.Caption = "Reverse Sort Direction"
            Me.CheckEdit2.Size = New System.Drawing.Size(217, 19)
            Me.CheckEdit2.TabIndex = 4
            Me.CheckEdit2.ToolTip = "Reverse the sort direction so that it sorts from highest to lowest rather than lo" & _
                "west to highest"
            '
            'LabelControl2
            '
            Me.LabelControl2.Location = New System.Drawing.Point(14, 55)
            Me.LabelControl2.Name = "LabelControl2"
            Me.LabelControl2.Size = New System.Drawing.Size(38, 13)
            Me.LabelControl2.TabIndex = 2
            Me.LabelControl2.Text = "# Items"
            '
            'TextEdit1
            '
            Me.TextEdit1.EditValue = "0"
            Me.TextEdit1.Location = New System.Drawing.Point(83, 52)
            Me.TextEdit1.Name = "TextEdit1"
            Me.TextEdit1.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[False]
            Me.TextEdit1.Properties.Appearance.Options.UseTextOptions = True
            Me.TextEdit1.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.TextEdit1.Properties.DisplayFormat.FormatString = "f0"
            Me.TextEdit1.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.TextEdit1.Properties.EditFormat.FormatString = "f0"
            Me.TextEdit1.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.TextEdit1.Properties.Mask.BeepOnError = True
            Me.TextEdit1.Properties.Mask.EditMask = "f0"
            Me.TextEdit1.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
            Me.TextEdit1.Size = New System.Drawing.Size(146, 20)
            Me.TextEdit1.TabIndex = 3
            Me.TextEdit1.ToolTip = "Number of Items to print. 0 means print ""all available items"""
            '
            'Parameters_Form
            '
            Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
            Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
            Me.ClientSize = New System.Drawing.Size(336, 135)
            Me.Controls.Add(Me.TextEdit1)
            Me.Controls.Add(Me.LabelControl2)
            Me.Controls.Add(Me.CheckEdit2)
            Me.Controls.Add(Me.LabelControl1)
            Me.Controls.Add(Me.CheckEdit1)
            Me.Controls.Add(Me.ComboBoxEdit1)
            Me.Name = "Parameters_Form"
            Me.Controls.SetChildIndex(Me.ComboBoxEdit1, 0)
            Me.Controls.SetChildIndex(Me.CheckEdit1, 0)
            Me.Controls.SetChildIndex(Me.LabelControl1, 0)
            Me.Controls.SetChildIndex(Me.ButtonOK, 0)
            Me.Controls.SetChildIndex(Me.ButtonCancel, 0)
            Me.Controls.SetChildIndex(Me.CheckEdit2, 0)
            Me.Controls.SetChildIndex(Me.LabelControl2, 0)
            Me.Controls.SetChildIndex(Me.TextEdit1, 0)
            CType(Me.DxErrorProvider1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.ComboBoxEdit1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.CheckEdit1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.CheckEdit2.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.TextEdit1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            Me.ResumeLayout(False)
            Me.PerformLayout()

        End Sub
        Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents ComboBoxEdit1 As DevExpress.XtraEditors.ComboBoxEdit
        Friend WithEvents CheckEdit1 As DevExpress.XtraEditors.CheckEdit
        Friend WithEvents CheckEdit2 As DevExpress.XtraEditors.CheckEdit
        Friend WithEvents LabelControl2 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents TextEdit1 As DevExpress.XtraEditors.TextEdit
    End Class
End Namespace