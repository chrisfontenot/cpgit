Namespace DailySummary.CreditorPayments
    Partial Class DailySummaryCreditorPayments
        Inherits DebtPlus.Reports.Template.DatedTemplateXtraReportClass

        'Component overrides dispose to clean up the component list.
        Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
            If disposing Then
                If components IsNot Nothing Then
                    components.Dispose()
                End If
            End If
            MyBase.Dispose(disposing)
        End Sub

        'Required by the Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Designer
        'It can be modified using the Designer.
        'Do not modify it using the code editor.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
            Dim XrSummary1 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary()
            Dim XrSummary2 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary()
            Dim XrSummary3 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary()
            Dim XrSummary4 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary()
            Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(DailySummaryCreditorPayments))
            Me.GroupHeader1 = New DevExpress.XtraReports.UI.GroupHeaderBand()
            Me.XrPanel1 = New DevExpress.XtraReports.UI.XRPanel()
            Me.XrLabel6 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel4 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel5 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel3 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel9 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel7 = New DevExpress.XtraReports.UI.XRLabel()
            Me.GroupFooter1 = New DevExpress.XtraReports.UI.GroupFooterBand()
            Me.XrLabel_total_check_amount = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel15 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_total_adjustment_amount = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLine1 = New DevExpress.XtraReports.UI.XRLine()
            Me.XrLabel_check_amount = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_clientID = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_invoice = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_reference = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_adjustment_amount = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_date_created = New DevExpress.XtraReports.UI.XRLabel()
            Me.GroupFooter2 = New DevExpress.XtraReports.UI.GroupFooterBand()
            Me.XrLabel_report_payment_amount = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLine2 = New DevExpress.XtraReports.UI.XRLine()
            Me.XrLabel_report_adjustment_amount = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel2 = New DevExpress.XtraReports.UI.XRLabel()
            CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
            '
            'PageHeader
            '
            Me.PageHeader.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrPanel1})
            Me.PageHeader.HeightF = 155.0!
            Me.PageHeader.Controls.SetChildIndex(Me.XrPanel1, 0)
            Me.PageHeader.Controls.SetChildIndex(Me.XrPanel_AgencyAddress, 0)
            Me.PageHeader.Controls.SetChildIndex(Me.XrLabel_Title, 0)
            Me.PageHeader.Controls.SetChildIndex(Me.XrLine_Title, 0)
            Me.PageHeader.Controls.SetChildIndex(Me.XrLabel_Subtitle, 0)
            Me.PageHeader.Controls.SetChildIndex(Me.XrPageInfo1, 0)
            '
            'XrLabel_Title
            '
            Me.XrLabel_Title.StylePriority.UseFont = False
            '
            'XrPageInfo_PageNumber
            '
            Me.XrPageInfo_PageNumber.StylePriority.UseTextAlignment = False
            '
            'XRLabel_Agency_Name
            '
            Me.XRLabel_Agency_Name.StylePriority.UseTextAlignment = False
            '
            'XrLabel_Agency_Address3
            '
            Me.XrLabel_Agency_Address3.StylePriority.UseFont = False
            Me.XrLabel_Agency_Address3.StylePriority.UseTextAlignment = False
            '
            'XrLabel_Agency_Address1
            '
            Me.XrLabel_Agency_Address1.StylePriority.UseFont = False
            Me.XrLabel_Agency_Address1.StylePriority.UseTextAlignment = False
            '
            'XrLabel_Agency_Phone
            '
            Me.XrLabel_Agency_Phone.StylePriority.UseFont = False
            Me.XrLabel_Agency_Phone.StylePriority.UseTextAlignment = False
            '
            'XrLabel_Agency_Address2
            '
            Me.XrLabel_Agency_Address2.StylePriority.UseFont = False
            Me.XrLabel_Agency_Address2.StylePriority.UseTextAlignment = False
            '
            'Detail
            '
            Me.Detail.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel_check_amount, Me.XrLabel_clientID, Me.XrLabel_invoice, Me.XrLabel_reference, Me.XrLabel_adjustment_amount, Me.XrLabel_date_created})
            Me.Detail.HeightF = 17.0!
            '
            'GroupHeader1
            '
            Me.GroupHeader1.GroupFields.AddRange(New DevExpress.XtraReports.UI.GroupField() {New DevExpress.XtraReports.UI.GroupField("creditor", DevExpress.XtraReports.UI.XRColumnSortOrder.Ascending)})
            Me.GroupHeader1.HeightF = 0.0!
            Me.GroupHeader1.Name = "GroupHeader1"
            '
            'XrPanel1
            '
            Me.XrPanel1.BackColor = System.Drawing.Color.Teal
            Me.XrPanel1.BorderColor = System.Drawing.Color.Teal
            Me.XrPanel1.CanGrow = False
            Me.XrPanel1.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel6, Me.XrLabel4, Me.XrLabel5, Me.XrLabel3, Me.XrLabel9, Me.XrLabel7})
            Me.XrPanel1.KeepTogether = False
            Me.XrPanel1.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 125.0!)
            Me.XrPanel1.Name = "XrPanel1"
            Me.XrPanel1.SizeF = New System.Drawing.SizeF(800.0!, 18.0!)
            Me.XrPanel1.StylePriority.UseBackColor = False
            Me.XrPanel1.StylePriority.UseBorderColor = False
            '
            'XrLabel6
            '
            Me.XrLabel6.BackColor = System.Drawing.Color.Transparent
            Me.XrLabel6.BorderColor = System.Drawing.Color.Transparent
            Me.XrLabel6.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel6.ForeColor = System.Drawing.Color.White
            Me.XrLabel6.LocationFloat = New DevExpress.Utils.PointFloat(433.0!, 1.0!)
            Me.XrLabel6.Name = "XrLabel6"
            Me.XrLabel6.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel6.SizeF = New System.Drawing.SizeF(83.0!, 15.0!)
            Me.XrLabel6.StylePriority.UseBackColor = False
            Me.XrLabel6.StylePriority.UseBorderColor = False
            Me.XrLabel6.StylePriority.UseFont = False
            Me.XrLabel6.StylePriority.UseForeColor = False
            Me.XrLabel6.StylePriority.UseTextAlignment = False
            Me.XrLabel6.Text = "PAYMENT"
            Me.XrLabel6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel4
            '
            Me.XrLabel4.BackColor = System.Drawing.Color.Transparent
            Me.XrLabel4.BorderColor = System.Drawing.Color.Transparent
            Me.XrLabel4.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel4.ForeColor = System.Drawing.Color.White
            Me.XrLabel4.LocationFloat = New DevExpress.Utils.PointFloat(616.0!, 1.0!)
            Me.XrLabel4.Name = "XrLabel4"
            Me.XrLabel4.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel4.SizeF = New System.Drawing.SizeF(150.0!, 15.0!)
            Me.XrLabel4.StylePriority.UseBackColor = False
            Me.XrLabel4.StylePriority.UseBorderColor = False
            Me.XrLabel4.StylePriority.UseFont = False
            Me.XrLabel4.StylePriority.UseForeColor = False
            Me.XrLabel4.StylePriority.UseTextAlignment = False
            Me.XrLabel4.Text = "REFERENCE"
            Me.XrLabel4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            '
            'XrLabel5
            '
            Me.XrLabel5.BackColor = System.Drawing.Color.Transparent
            Me.XrLabel5.BorderColor = System.Drawing.Color.Transparent
            Me.XrLabel5.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel5.ForeColor = System.Drawing.Color.White
            Me.XrLabel5.LocationFloat = New DevExpress.Utils.PointFloat(525.0!, 1.0!)
            Me.XrLabel5.Name = "XrLabel5"
            Me.XrLabel5.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel5.SizeF = New System.Drawing.SizeF(83.0!, 15.0!)
            Me.XrLabel5.StylePriority.UseBackColor = False
            Me.XrLabel5.StylePriority.UseBorderColor = False
            Me.XrLabel5.StylePriority.UseFont = False
            Me.XrLabel5.StylePriority.UseForeColor = False
            Me.XrLabel5.StylePriority.UseTextAlignment = False
            Me.XrLabel5.Text = "ADJUSTED"
            Me.XrLabel5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel3
            '
            Me.XrLabel3.BackColor = System.Drawing.Color.Transparent
            Me.XrLabel3.BorderColor = System.Drawing.Color.Transparent
            Me.XrLabel3.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel3.ForeColor = System.Drawing.Color.White
            Me.XrLabel3.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 1.0!)
            Me.XrLabel3.Name = "XrLabel3"
            Me.XrLabel3.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel3.SizeF = New System.Drawing.SizeF(250.0!, 15.0!)
            Me.XrLabel3.StylePriority.UseBackColor = False
            Me.XrLabel3.StylePriority.UseBorderColor = False
            Me.XrLabel3.StylePriority.UseFont = False
            Me.XrLabel3.StylePriority.UseForeColor = False
            Me.XrLabel3.StylePriority.UseTextAlignment = False
            Me.XrLabel3.Text = "CREDITOR ID AND NAME"
            Me.XrLabel3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            '
            'XrLabel9
            '
            Me.XrLabel9.BackColor = System.Drawing.Color.Transparent
            Me.XrLabel9.BorderColor = System.Drawing.Color.Transparent
            Me.XrLabel9.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel9.ForeColor = System.Drawing.Color.White
            Me.XrLabel9.LocationFloat = New DevExpress.Utils.PointFloat(350.0!, 1.0!)
            Me.XrLabel9.Name = "XrLabel9"
            Me.XrLabel9.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel9.SizeF = New System.Drawing.SizeF(83.0!, 15.0!)
            Me.XrLabel9.StylePriority.UseBackColor = False
            Me.XrLabel9.StylePriority.UseBorderColor = False
            Me.XrLabel9.StylePriority.UseFont = False
            Me.XrLabel9.StylePriority.UseForeColor = False
            Me.XrLabel9.StylePriority.UseTextAlignment = False
            Me.XrLabel9.Text = "INVOICE"
            Me.XrLabel9.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel7
            '
            Me.XrLabel7.BackColor = System.Drawing.Color.Transparent
            Me.XrLabel7.BorderColor = System.Drawing.Color.Transparent
            Me.XrLabel7.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel7.ForeColor = System.Drawing.Color.White
            Me.XrLabel7.LocationFloat = New DevExpress.Utils.PointFloat(266.0!, 1.0!)
            Me.XrLabel7.Name = "XrLabel7"
            Me.XrLabel7.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel7.SizeF = New System.Drawing.SizeF(75.0!, 15.0!)
            Me.XrLabel7.StylePriority.UseBackColor = False
            Me.XrLabel7.StylePriority.UseBorderColor = False
            Me.XrLabel7.StylePriority.UseFont = False
            Me.XrLabel7.StylePriority.UseForeColor = False
            Me.XrLabel7.StylePriority.UseTextAlignment = False
            Me.XrLabel7.Text = "DATE"
            Me.XrLabel7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'GroupFooter1
            '
            Me.GroupFooter1.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel_total_check_amount, Me.XrLabel15, Me.XrLabel_total_adjustment_amount, Me.XrLine1})
            Me.GroupFooter1.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.GroupFooter1.HeightF = 52.0!
            Me.GroupFooter1.Name = "GroupFooter1"
            Me.GroupFooter1.StylePriority.UseFont = False
            Me.GroupFooter1.StylePriority.UseTextAlignment = False
            Me.GroupFooter1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel_total_check_amount
            '
            Me.XrLabel_total_check_amount.BackColor = System.Drawing.Color.Transparent
            Me.XrLabel_total_check_amount.BorderColor = System.Drawing.Color.Transparent
            Me.XrLabel_total_check_amount.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "check_amount")})
            Me.XrLabel_total_check_amount.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel_total_check_amount.ForeColor = System.Drawing.SystemColors.ControlText
            Me.XrLabel_total_check_amount.LocationFloat = New DevExpress.Utils.PointFloat(433.0!, 17.0!)
            Me.XrLabel_total_check_amount.Name = "XrLabel_total_check_amount"
            Me.XrLabel_total_check_amount.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_total_check_amount.SizeF = New System.Drawing.SizeF(83.0!, 15.0!)
            Me.XrLabel_total_check_amount.StylePriority.UseBackColor = False
            Me.XrLabel_total_check_amount.StylePriority.UseBorderColor = False
            Me.XrLabel_total_check_amount.StylePriority.UseFont = False
            Me.XrLabel_total_check_amount.StylePriority.UseForeColor = False
            Me.XrLabel_total_check_amount.StylePriority.UseTextAlignment = False
            XrSummary1.FormatString = "{0:c}"
            XrSummary1.Running = DevExpress.XtraReports.UI.SummaryRunning.Group
            Me.XrLabel_total_check_amount.Summary = XrSummary1
            Me.XrLabel_total_check_amount.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel15
            '
            Me.XrLabel15.BackColor = System.Drawing.Color.Transparent
            Me.XrLabel15.BorderColor = System.Drawing.Color.Transparent
            Me.XrLabel15.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel15.ForeColor = System.Drawing.SystemColors.ControlText
            Me.XrLabel15.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 17.0!)
            Me.XrLabel15.Name = "XrLabel15"
            Me.XrLabel15.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel15.SizeF = New System.Drawing.SizeF(250.0!, 15.0!)
            Me.XrLabel15.StylePriority.UseBackColor = False
            Me.XrLabel15.StylePriority.UseBorderColor = False
            Me.XrLabel15.StylePriority.UseFont = False
            Me.XrLabel15.StylePriority.UseForeColor = False
            Me.XrLabel15.StylePriority.UseTextAlignment = False
            Me.XrLabel15.Text = "TOTALS"
            Me.XrLabel15.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            '
            'XrLabel_total_adjustment_amount
            '
            Me.XrLabel_total_adjustment_amount.BackColor = System.Drawing.Color.Transparent
            Me.XrLabel_total_adjustment_amount.BorderColor = System.Drawing.Color.Transparent
            Me.XrLabel_total_adjustment_amount.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "adjustment_amount")})
            Me.XrLabel_total_adjustment_amount.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel_total_adjustment_amount.ForeColor = System.Drawing.SystemColors.ControlText
            Me.XrLabel_total_adjustment_amount.LocationFloat = New DevExpress.Utils.PointFloat(525.0!, 17.0!)
            Me.XrLabel_total_adjustment_amount.Name = "XrLabel_total_adjustment_amount"
            Me.XrLabel_total_adjustment_amount.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_total_adjustment_amount.SizeF = New System.Drawing.SizeF(83.0!, 15.0!)
            Me.XrLabel_total_adjustment_amount.StylePriority.UseBackColor = False
            Me.XrLabel_total_adjustment_amount.StylePriority.UseBorderColor = False
            Me.XrLabel_total_adjustment_amount.StylePriority.UseFont = False
            Me.XrLabel_total_adjustment_amount.StylePriority.UseForeColor = False
            Me.XrLabel_total_adjustment_amount.StylePriority.UseTextAlignment = False
            XrSummary2.FormatString = "{0:c}"
            XrSummary2.Running = DevExpress.XtraReports.UI.SummaryRunning.Group
            Me.XrLabel_total_adjustment_amount.Summary = XrSummary2
            Me.XrLabel_total_adjustment_amount.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLine1
            '
            Me.XrLine1.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 8.0!)
            Me.XrLine1.Name = "XrLine1"
            Me.XrLine1.SizeF = New System.Drawing.SizeF(767.0!, 8.0!)
            '
            'XrLabel_check_amount
            '
            Me.XrLabel_check_amount.BackColor = System.Drawing.Color.Transparent
            Me.XrLabel_check_amount.BorderColor = System.Drawing.Color.Transparent
            Me.XrLabel_check_amount.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "check_amount", "{0:c}")})
            Me.XrLabel_check_amount.Font = New System.Drawing.Font("Times New Roman", 9.75!)
            Me.XrLabel_check_amount.ForeColor = System.Drawing.SystemColors.ControlText
            Me.XrLabel_check_amount.LocationFloat = New DevExpress.Utils.PointFloat(433.0!, 0.0!)
            Me.XrLabel_check_amount.Name = "XrLabel_check_amount"
            Me.XrLabel_check_amount.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_check_amount.SizeF = New System.Drawing.SizeF(83.0!, 15.0!)
            Me.XrLabel_check_amount.StylePriority.UseBackColor = False
            Me.XrLabel_check_amount.StylePriority.UseBorderColor = False
            Me.XrLabel_check_amount.StylePriority.UseFont = False
            Me.XrLabel_check_amount.StylePriority.UseForeColor = False
            Me.XrLabel_check_amount.StylePriority.UseTextAlignment = False
            Me.XrLabel_check_amount.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel_clientID
            '
            Me.XrLabel_clientID.BackColor = System.Drawing.Color.Transparent
            Me.XrLabel_clientID.BorderColor = System.Drawing.Color.Transparent
            Me.XrLabel_clientID.Font = New System.Drawing.Font("Times New Roman", 9.75!)
            Me.XrLabel_clientID.ForeColor = System.Drawing.SystemColors.ControlText
            Me.XrLabel_clientID.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
            Me.XrLabel_clientID.Name = "XrLabel_clientID"
            Me.XrLabel_clientID.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_clientID.ProcessDuplicates = DevExpress.XtraReports.UI.ValueSuppressType.Suppress
            Me.XrLabel_clientID.SizeF = New System.Drawing.SizeF(250.0!, 15.0!)
            Me.XrLabel_clientID.StylePriority.UseBackColor = False
            Me.XrLabel_clientID.StylePriority.UseBorderColor = False
            Me.XrLabel_clientID.StylePriority.UseFont = False
            Me.XrLabel_clientID.StylePriority.UseForeColor = False
            Me.XrLabel_clientID.StylePriority.UseTextAlignment = False
            Me.XrLabel_clientID.Text = "[[creditor]] [creditor_name]"
            Me.XrLabel_clientID.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            Me.XrLabel_clientID.WordWrap = False
            '
            'XrLabel_invoice
            '
            Me.XrLabel_invoice.BackColor = System.Drawing.Color.Transparent
            Me.XrLabel_invoice.BorderColor = System.Drawing.Color.Transparent
            Me.XrLabel_invoice.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "invoice")})
            Me.XrLabel_invoice.Font = New System.Drawing.Font("Times New Roman", 9.75!)
            Me.XrLabel_invoice.ForeColor = System.Drawing.SystemColors.ControlText
            Me.XrLabel_invoice.LocationFloat = New DevExpress.Utils.PointFloat(350.0!, 0.0!)
            Me.XrLabel_invoice.Name = "XrLabel_invoice"
            Me.XrLabel_invoice.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_invoice.SizeF = New System.Drawing.SizeF(83.0!, 15.0!)
            Me.XrLabel_invoice.StylePriority.UseBackColor = False
            Me.XrLabel_invoice.StylePriority.UseBorderColor = False
            Me.XrLabel_invoice.StylePriority.UseFont = False
            Me.XrLabel_invoice.StylePriority.UseForeColor = False
            Me.XrLabel_invoice.StylePriority.UseTextAlignment = False
            Me.XrLabel_invoice.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel_reference
            '
            Me.XrLabel_reference.BackColor = System.Drawing.Color.Transparent
            Me.XrLabel_reference.BorderColor = System.Drawing.Color.Transparent
            Me.XrLabel_reference.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "reference")})
            Me.XrLabel_reference.Font = New System.Drawing.Font("Times New Roman", 9.75!)
            Me.XrLabel_reference.ForeColor = System.Drawing.SystemColors.ControlText
            Me.XrLabel_reference.LocationFloat = New DevExpress.Utils.PointFloat(616.0!, 0.0!)
            Me.XrLabel_reference.Name = "XrLabel_reference"
            Me.XrLabel_reference.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_reference.SizeF = New System.Drawing.SizeF(150.0!, 15.0!)
            Me.XrLabel_reference.StylePriority.UseBackColor = False
            Me.XrLabel_reference.StylePriority.UseBorderColor = False
            Me.XrLabel_reference.StylePriority.UseFont = False
            Me.XrLabel_reference.StylePriority.UseForeColor = False
            Me.XrLabel_reference.StylePriority.UseTextAlignment = False
            Me.XrLabel_reference.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            Me.XrLabel_reference.WordWrap = False
            '
            'XrLabel_adjustment_amount
            '
            Me.XrLabel_adjustment_amount.BackColor = System.Drawing.Color.Transparent
            Me.XrLabel_adjustment_amount.BorderColor = System.Drawing.Color.Transparent
            Me.XrLabel_adjustment_amount.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "adjustment_amount", "{0:c}")})
            Me.XrLabel_adjustment_amount.Font = New System.Drawing.Font("Times New Roman", 9.75!)
            Me.XrLabel_adjustment_amount.ForeColor = System.Drawing.SystemColors.ControlText
            Me.XrLabel_adjustment_amount.LocationFloat = New DevExpress.Utils.PointFloat(525.0!, 0.0!)
            Me.XrLabel_adjustment_amount.Name = "XrLabel_adjustment_amount"
            Me.XrLabel_adjustment_amount.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_adjustment_amount.SizeF = New System.Drawing.SizeF(83.0!, 15.0!)
            Me.XrLabel_adjustment_amount.StylePriority.UseBackColor = False
            Me.XrLabel_adjustment_amount.StylePriority.UseBorderColor = False
            Me.XrLabel_adjustment_amount.StylePriority.UseFont = False
            Me.XrLabel_adjustment_amount.StylePriority.UseForeColor = False
            Me.XrLabel_adjustment_amount.StylePriority.UseTextAlignment = False
            Me.XrLabel_adjustment_amount.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel_date_created
            '
            Me.XrLabel_date_created.BackColor = System.Drawing.Color.Transparent
            Me.XrLabel_date_created.BorderColor = System.Drawing.Color.Transparent
            Me.XrLabel_date_created.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "date_created", "{0:d}")})
            Me.XrLabel_date_created.Font = New System.Drawing.Font("Times New Roman", 9.75!)
            Me.XrLabel_date_created.ForeColor = System.Drawing.SystemColors.ControlText
            Me.XrLabel_date_created.LocationFloat = New DevExpress.Utils.PointFloat(266.0!, 0.0!)
            Me.XrLabel_date_created.Name = "XrLabel_date_created"
            Me.XrLabel_date_created.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_date_created.SizeF = New System.Drawing.SizeF(75.0!, 15.0!)
            Me.XrLabel_date_created.StylePriority.UseBackColor = False
            Me.XrLabel_date_created.StylePriority.UseBorderColor = False
            Me.XrLabel_date_created.StylePriority.UseFont = False
            Me.XrLabel_date_created.StylePriority.UseForeColor = False
            Me.XrLabel_date_created.StylePriority.UseTextAlignment = False
            Me.XrLabel_date_created.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'GroupFooter2
            '
            Me.GroupFooter2.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel_report_payment_amount, Me.XrLine2, Me.XrLabel_report_adjustment_amount, Me.XrLabel2})
            Me.GroupFooter2.HeightF = 25.0!
            Me.GroupFooter2.Level = 1
            Me.GroupFooter2.Name = "GroupFooter2"
            '
            'XrLabel_report_payment_amount
            '
            Me.XrLabel_report_payment_amount.BackColor = System.Drawing.Color.Transparent
            Me.XrLabel_report_payment_amount.BorderColor = System.Drawing.Color.Transparent
            Me.XrLabel_report_payment_amount.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "check_amount")})
            Me.XrLabel_report_payment_amount.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel_report_payment_amount.ForeColor = System.Drawing.SystemColors.ControlText
            Me.XrLabel_report_payment_amount.LocationFloat = New DevExpress.Utils.PointFloat(433.0!, 10.0!)
            Me.XrLabel_report_payment_amount.Name = "XrLabel_report_payment_amount"
            Me.XrLabel_report_payment_amount.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_report_payment_amount.SizeF = New System.Drawing.SizeF(83.0!, 15.0!)
            Me.XrLabel_report_payment_amount.StylePriority.UseBackColor = False
            Me.XrLabel_report_payment_amount.StylePriority.UseBorderColor = False
            Me.XrLabel_report_payment_amount.StylePriority.UseFont = False
            Me.XrLabel_report_payment_amount.StylePriority.UseForeColor = False
            Me.XrLabel_report_payment_amount.StylePriority.UseTextAlignment = False
            XrSummary3.FormatString = "{0:c}"
            XrSummary3.Running = DevExpress.XtraReports.UI.SummaryRunning.Report
            Me.XrLabel_report_payment_amount.Summary = XrSummary3
            Me.XrLabel_report_payment_amount.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLine2
            '
            Me.XrLine2.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
            Me.XrLine2.Name = "XrLine2"
            Me.XrLine2.SizeF = New System.Drawing.SizeF(767.0!, 8.0!)
            '
            'XrLabel_report_adjustment_amount
            '
            Me.XrLabel_report_adjustment_amount.BackColor = System.Drawing.Color.Transparent
            Me.XrLabel_report_adjustment_amount.BorderColor = System.Drawing.Color.Transparent
            Me.XrLabel_report_adjustment_amount.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "adjustment_amount")})
            Me.XrLabel_report_adjustment_amount.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel_report_adjustment_amount.ForeColor = System.Drawing.SystemColors.ControlText
            Me.XrLabel_report_adjustment_amount.LocationFloat = New DevExpress.Utils.PointFloat(525.0!, 10.0!)
            Me.XrLabel_report_adjustment_amount.Name = "XrLabel_report_adjustment_amount"
            Me.XrLabel_report_adjustment_amount.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_report_adjustment_amount.SizeF = New System.Drawing.SizeF(83.0!, 15.0!)
            Me.XrLabel_report_adjustment_amount.StylePriority.UseBackColor = False
            Me.XrLabel_report_adjustment_amount.StylePriority.UseBorderColor = False
            Me.XrLabel_report_adjustment_amount.StylePriority.UseFont = False
            Me.XrLabel_report_adjustment_amount.StylePriority.UseForeColor = False
            Me.XrLabel_report_adjustment_amount.StylePriority.UseTextAlignment = False
            XrSummary4.FormatString = "{0:c}"
            XrSummary4.Running = DevExpress.XtraReports.UI.SummaryRunning.Report
            Me.XrLabel_report_adjustment_amount.Summary = XrSummary4
            Me.XrLabel_report_adjustment_amount.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel2
            '
            Me.XrLabel2.BackColor = System.Drawing.Color.Transparent
            Me.XrLabel2.BorderColor = System.Drawing.Color.Transparent
            Me.XrLabel2.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel2.ForeColor = System.Drawing.SystemColors.ControlText
            Me.XrLabel2.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 10.0!)
            Me.XrLabel2.Name = "XrLabel2"
            Me.XrLabel2.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel2.SizeF = New System.Drawing.SizeF(250.0!, 15.0!)
            Me.XrLabel2.StylePriority.UseBackColor = False
            Me.XrLabel2.StylePriority.UseBorderColor = False
            Me.XrLabel2.StylePriority.UseFont = False
            Me.XrLabel2.StylePriority.UseForeColor = False
            Me.XrLabel2.StylePriority.UseTextAlignment = False
            Me.XrLabel2.Text = "GRAND TOTALS"
            Me.XrLabel2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            '
            'DailySummaryCreditorPayments
            '
            Me.Bands.AddRange(New DevExpress.XtraReports.UI.Band() {Me.Detail, Me.PageHeader, Me.PageFooter, Me.GroupHeader1, Me.GroupFooter1, Me.TopMarginBand1, Me.BottomMarginBand1, Me.GroupFooter2})
            Me.ScriptLanguage = DevExpress.XtraReports.ScriptLanguage.VisualBasic
            Me.ScriptReferencesString = resources.GetString("$this.ScriptReferencesString")
            Me.Scripts.OnBeforePrint = "DailySummaryCreditorPayments_BeforePrint"
            Me.ScriptsSource = resources.GetString("$this.ScriptsSource")
            Me.StyleSheet.AddRange(New DevExpress.XtraReports.UI.XRControlStyle() {Me.XrControlStyle_Header, Me.XrControlStyle_Totals, Me.XrControlStyle_HeaderPannel, Me.XrControlStyle_GroupHeader})
            Me.Version = "11.2"
            Me.Controls.SetChildIndex(Me.GroupFooter2, 0)
            Me.Controls.SetChildIndex(Me.BottomMarginBand1, 0)
            Me.Controls.SetChildIndex(Me.TopMarginBand1, 0)
            Me.Controls.SetChildIndex(Me.GroupFooter1, 0)
            Me.Controls.SetChildIndex(Me.GroupHeader1, 0)
            Me.Controls.SetChildIndex(Me.PageFooter, 0)
            Me.Controls.SetChildIndex(Me.PageHeader, 0)
            Me.Controls.SetChildIndex(Me.Detail, 0)
            CType(Me, System.ComponentModel.ISupportInitialize).EndInit()

        End Sub

        Friend WithEvents GroupHeader1 As DevExpress.XtraReports.UI.GroupHeaderBand
        Friend WithEvents GroupFooter1 As DevExpress.XtraReports.UI.GroupFooterBand
        Friend WithEvents XrPanel1 As DevExpress.XtraReports.UI.XRPanel
        Friend WithEvents XrLabel6 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel4 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel5 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel3 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel9 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel7 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_check_amount As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_clientID As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_invoice As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_reference As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_adjustment_amount As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_date_created As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_total_check_amount As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel15 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_total_adjustment_amount As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLine1 As DevExpress.XtraReports.UI.XRLine
        Friend WithEvents GroupFooter2 As DevExpress.XtraReports.UI.GroupFooterBand
        Friend WithEvents XrLabel_report_payment_amount As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLine2 As DevExpress.XtraReports.UI.XRLine
        Friend WithEvents XrLabel_report_adjustment_amount As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel2 As DevExpress.XtraReports.UI.XRLabel
    End Class
End Namespace
