#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}
#End Region
Imports DebtPlus.Reports.Template
Imports System.IO
Imports DebtPlus.Utils

Namespace Appointments.AvailableAnalysis

    Public Class AvailableAnalysisReport
        Inherits DatedTemplateXtraReportClass

        Public Sub New()
            MyBase.New()

            Const ReportName As String = "DebtPlus.Reports.Appointments.Available.Analysis.repx"

            ' See if there is a report reference that we can use
            Dim Fname As String = System.IO.Path.Combine(DebtPlus.Configuration.Config.ReportsDirectory, ReportName)
            Dim UseDefault As Boolean = True
            Try
                If (Not DebtPlus.Configuration.Config.LocalReportsOnly) AndAlso System.IO.File.Exists(Fname) Then
                    LoadLayout(Fname)
                    UseDefault = False
                End If
            Catch ex As Exception
            End Try

            ' Load the standard report definition if we need a new item
            If UseDefault Then
                Dim asm As Reflection.Assembly = Reflection.Assembly.GetExecutingAssembly                                           ' changed
                Using ios As Stream = asm.GetManifestResourceStream(String.Format("DebtPlus.Reports.{0}", ReportName))              ' changed
                    If ios IsNot Nothing Then LoadLayout(ios) '                                                                       changed
                End Using
            End If

            ' Set the script references as needed
            ScriptReferences = DebtPlus.Utils.AssemblyTools.GetAllSiblingAssemblies()

            ' Set the parameters to the default values
            Parameter_Office = -1

            ' Enable the select expert
            ReportFilter.IsEnabled = True
        End Sub

        Public Overrides ReadOnly Property ReportTitle() As String
            Get
                Return "Appt Avaiability Time Analysis"
            End Get
        End Property

        Public Property Parameter_Office() As Int32
            Get
                Return Convert.ToInt32(Parameters("ParameterOffice").Value)
            End Get
            Set(ByVal value As Int32)
                Parameters("ParameterOffice").Value = value
            End Set
        End Property

        Public Overrides Sub SetReportParameter(ByVal Parameter As String, ByVal Value As Object)
            If String.Compare(Parameter, "Office", False) = 0 Then
                Parameter_Office = Convert.ToInt32(Value)
            Else
                MyBase.SetReportParameter(Parameter, Value)
            End If
        End Sub

        Public Overrides ReadOnly Property ReportSubTitle() As String
            Get
                Return MyBase.ReportSubtitle
            End Get
        End Property

        Public Overrides Function RequestReportParameters() As System.Windows.Forms.DialogResult
            Dim answer As System.Windows.Forms.DialogResult = DialogResult.OK
            If NeedParameters() Then
                Using frm As New DebtPlus.Reports.Template.Forms.DatedOfficeParametersForm(True)
                    answer = frm.ShowDialog()
                    Parameter_FromDate = frm.Parameter_FromDate
                    Parameter_ToDate = frm.Parameter_ToDate
                    Parameter_Office = If(frm.Parameter_Office Is System.DBNull.Value, -1, Convert.ToInt32(frm.Parameter_Office))
                End Using
            End If
            Return answer
        End Function
    End Class
End Namespace