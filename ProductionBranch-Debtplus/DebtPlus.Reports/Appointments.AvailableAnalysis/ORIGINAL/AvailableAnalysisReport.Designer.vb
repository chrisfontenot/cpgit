Namespace Appointments.AvailableAnalysis
    <Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
    Partial Class AvailableAnalysisReport
        Inherits DebtPlus.Reports.Template.DatedTemplateXtraReportClass

        'XtraReport overrides dispose to clean up the component list.
        <System.Diagnostics.DebuggerNonUserCode()> _
        Protected Overrides Sub Dispose(ByVal disposing As Boolean)
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
            MyBase.Dispose(disposing)
        End Sub

        'Required by the Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Designer
        'It can be modified using the Designer.  
        'Do not modify it using the code editor.
        <System.Diagnostics.DebuggerStepThrough()> _
        Private Sub InitializeComponent()
            Dim XrSummary2 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary()
            Dim XrSummary3 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary()
            Dim XrSummary1 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary()
            Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(AvailableAnalysisReport))
            Me.GroupHeader0 = New DevExpress.XtraReports.UI.GroupHeaderBand()
            Me.XrLabel_start_date = New DevExpress.XtraReports.UI.XRLabel()
            Me.GroupHeader1 = New DevExpress.XtraReports.UI.GroupHeaderBand()
            Me.XrLabel_office_name = New DevExpress.XtraReports.UI.XRLabel()
            Me.GroupFooter1 = New DevExpress.XtraReports.UI.GroupFooterBand()
            Me.XrLabel_appt_name = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_booked = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_start_time = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_available = New DevExpress.XtraReports.UI.XRLabel()
            Me.GroupFooter2 = New DevExpress.XtraReports.UI.GroupFooterBand()
            Me.ParameterOffice = New DevExpress.XtraReports.Parameters.Parameter()
            Me.CalculatedFieldDate = New DevExpress.XtraReports.UI.CalculatedField()
            Me.XrLabel_counselor_name = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrPanel1 = New DevExpress.XtraReports.UI.XRPanel()
            Me.XrLabel2 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel10 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel9 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel8 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel1 = New DevExpress.XtraReports.UI.XRLabel()
            Me.GroupHeader2 = New DevExpress.XtraReports.UI.GroupHeaderBand()
            Me.GroupHeader3 = New DevExpress.XtraReports.UI.GroupHeaderBand()
            Me.XrLabel_detail_booked = New DevExpress.XtraReports.UI.XRLabel()
            CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
            '
            'PageHeader
            '
            Me.PageHeader.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrPanel1})
            Me.PageHeader.HeightF = 137.4167!
            Me.PageHeader.Controls.SetChildIndex(Me.XrPanel_AgencyAddress, 0)
            Me.PageHeader.Controls.SetChildIndex(Me.XrLabel_Title, 0)
            Me.PageHeader.Controls.SetChildIndex(Me.XrLine_Title, 0)
            Me.PageHeader.Controls.SetChildIndex(Me.XrLabel_Subtitle, 0)
            Me.PageHeader.Controls.SetChildIndex(Me.XrPageInfo1, 0)
            Me.PageHeader.Controls.SetChildIndex(Me.XrPanel1, 0)
            '
            'XrLabel_Title
            '
            Me.XrLabel_Title.StylePriority.UseFont = False
            '
            'XrPageInfo_PageNumber
            '
            Me.XrPageInfo_PageNumber.StylePriority.UseTextAlignment = False
            '
            'XRLabel_Agency_Name
            '
            Me.XRLabel_Agency_Name.StylePriority.UseTextAlignment = False
            '
            'XrLabel_Agency_Address3
            '
            Me.XrLabel_Agency_Address3.StylePriority.UseFont = False
            Me.XrLabel_Agency_Address3.StylePriority.UseTextAlignment = False
            '
            'XrLabel_Agency_Address1
            '
            Me.XrLabel_Agency_Address1.StylePriority.UseFont = False
            Me.XrLabel_Agency_Address1.StylePriority.UseTextAlignment = False
            '
            'XrLabel_Agency_Phone
            '
            Me.XrLabel_Agency_Phone.StylePriority.UseFont = False
            Me.XrLabel_Agency_Phone.StylePriority.UseTextAlignment = False
            '
            'XrLabel_Agency_Address2
            '
            Me.XrLabel_Agency_Address2.StylePriority.UseFont = False
            Me.XrLabel_Agency_Address2.StylePriority.UseTextAlignment = False
            '
            'Detail
            '
            Me.Detail.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel_detail_booked, Me.XrLabel_counselor_name})
            Me.Detail.HeightF = 15.0!
            Me.Detail.Visible = False
            '
            'GroupHeader0
            '
            Me.GroupHeader0.GroupFields.AddRange(New DevExpress.XtraReports.UI.GroupField() {New DevExpress.XtraReports.UI.GroupField("appt_name", DevExpress.XtraReports.UI.XRColumnSortOrder.Ascending)})
            Me.GroupHeader0.HeightF = 0.0!
            Me.GroupHeader0.Name = "GroupHeader0"
            Me.GroupHeader0.StyleName = "XrControlStyle_GroupHeader"
            Me.GroupHeader0.Visible = False
            '
            'XrLabel_start_date
            '
            Me.XrLabel_start_date.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "start_time", "DATE: {0:d}")})
            Me.XrLabel_start_date.ForeColor = System.Drawing.Color.Blue
            Me.XrLabel_start_date.LocationFloat = New DevExpress.Utils.PointFloat(50.0!, 10.00001!)
            Me.XrLabel_start_date.Name = "XrLabel_start_date"
            Me.XrLabel_start_date.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_start_date.SizeF = New System.Drawing.SizeF(152.0833!, 15.0!)
            Me.XrLabel_start_date.StylePriority.UseForeColor = False
            '
            'GroupHeader1
            '
            Me.GroupHeader1.GroupFields.AddRange(New DevExpress.XtraReports.UI.GroupField() {New DevExpress.XtraReports.UI.GroupField("appt_time", DevExpress.XtraReports.UI.XRColumnSortOrder.Ascending)})
            Me.GroupHeader1.HeightF = 0.0!
            Me.GroupHeader1.Level = 1
            Me.GroupHeader1.Name = "GroupHeader1"
            Me.GroupHeader1.StyleName = "XrControlStyle_GroupHeader"
            Me.GroupHeader1.Visible = False
            '
            'XrLabel_office_name
            '
            Me.XrLabel_office_name.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "office_name")})
            Me.XrLabel_office_name.LocationFloat = New DevExpress.Utils.PointFloat(8.0!, 10.00001!)
            Me.XrLabel_office_name.Name = "XrLabel_office_name"
            Me.XrLabel_office_name.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_office_name.SizeF = New System.Drawing.SizeF(734.0!, 16.0!)
            Me.XrLabel_office_name.Text = "Office"
            '
            'GroupFooter1
            '
            Me.GroupFooter1.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel_appt_name, Me.XrLabel_booked, Me.XrLabel_start_time, Me.XrLabel_available})
            Me.GroupFooter1.HeightF = 15.0!
            Me.GroupFooter1.Name = "GroupFooter1"
            '
            'XrLabel_appt_name
            '
            Me.XrLabel_appt_name.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "appt_name")})
            Me.XrLabel_appt_name.LocationFloat = New DevExpress.Utils.PointFloat(195.9167!, 0.0!)
            Me.XrLabel_appt_name.Name = "XrLabel_appt_name"
            Me.XrLabel_appt_name.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_appt_name.SizeF = New System.Drawing.SizeF(228.875!, 14.99999!)
            '
            'XrLabel_booked
            '
            Me.XrLabel_booked.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "booked")})
            Me.XrLabel_booked.LocationFloat = New DevExpress.Utils.PointFloat(491.4584!, 0.0!)
            Me.XrLabel_booked.Name = "XrLabel_booked"
            Me.XrLabel_booked.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_booked.SizeF = New System.Drawing.SizeF(57.29169!, 15.0!)
            Me.XrLabel_booked.StylePriority.UseTextAlignment = False
            XrSummary2.FormatString = "{0:n0}"
            XrSummary2.Running = DevExpress.XtraReports.UI.SummaryRunning.Group
            Me.XrLabel_booked.Summary = XrSummary2
            Me.XrLabel_booked.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
            '
            'XrLabel_start_time
            '
            Me.XrLabel_start_time.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "start_time", "{0:t}")})
            Me.XrLabel_start_time.LocationFloat = New DevExpress.Utils.PointFloat(93.5416!, 0.0!)
            Me.XrLabel_start_time.Name = "XrLabel_start_time"
            Me.XrLabel_start_time.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_start_time.SizeF = New System.Drawing.SizeF(85.41668!, 15.0!)
            Me.XrLabel_start_time.StylePriority.UseTextAlignment = False
            Me.XrLabel_start_time.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
            '
            'XrLabel_available
            '
            Me.XrLabel_available.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "available")})
            Me.XrLabel_available.LocationFloat = New DevExpress.Utils.PointFloat(424.7917!, 0.0!)
            Me.XrLabel_available.Name = "XrLabel_available"
            Me.XrLabel_available.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_available.SizeF = New System.Drawing.SizeF(57.29169!, 15.0!)
            Me.XrLabel_available.StylePriority.UseTextAlignment = False
            XrSummary3.FormatString = "{0:n0}"
            XrSummary3.Running = DevExpress.XtraReports.UI.SummaryRunning.Group
            Me.XrLabel_available.Summary = XrSummary3
            Me.XrLabel_available.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
            '
            'GroupFooter2
            '
            Me.GroupFooter2.HeightF = 0.0!
            Me.GroupFooter2.Level = 1
            Me.GroupFooter2.Name = "GroupFooter2"
            Me.GroupFooter2.PageBreak = DevExpress.XtraReports.UI.PageBreak.AfterBand
            Me.GroupFooter2.StyleName = "XrControlStyle_Totals"
            Me.GroupFooter2.Visible = False
            '
            'ParameterOffice
            '
            Me.ParameterOffice.Name = "ParameterOffice"
            Me.ParameterOffice.Type = GetType(Integer)
            Me.ParameterOffice.Value = -1
            Me.ParameterOffice.Visible = False
            '
            'CalculatedFieldDate
            '
            Me.CalculatedFieldDate.Expression = "GetDate([start_time])"
            Me.CalculatedFieldDate.FieldType = DevExpress.XtraReports.UI.FieldType.DateTime
            Me.CalculatedFieldDate.Name = "CalculatedFieldDate"
            '
            'XrLabel_counselor_name
            '
            Me.XrLabel_counselor_name.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "counselor_name")})
            Me.XrLabel_counselor_name.LocationFloat = New DevExpress.Utils.PointFloat(568.75!, 0.0!)
            Me.XrLabel_counselor_name.Name = "XrLabel_counselor_name"
            Me.XrLabel_counselor_name.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_counselor_name.SizeF = New System.Drawing.SizeF(181.25!, 15.0!)
            '
            'XrPanel1
            '
            Me.XrPanel1.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel2, Me.XrLabel10, Me.XrLabel9, Me.XrLabel8, Me.XrLabel1})
            Me.XrPanel1.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 110.4167!)
            Me.XrPanel1.Name = "XrPanel1"
            Me.XrPanel1.SizeF = New System.Drawing.SizeF(740.0!, 17.0!)
            Me.XrPanel1.StyleName = "XrControlStyle_HeaderPannel"
            '
            'XrLabel2
            '
            Me.XrLabel2.LocationFloat = New DevExpress.Utils.PointFloat(568.75!, 0.0!)
            Me.XrLabel2.Name = "XrLabel2"
            Me.XrLabel2.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel2.SizeF = New System.Drawing.SizeF(171.25!, 14.99999!)
            Me.XrLabel2.StylePriority.UseTextAlignment = False
            Me.XrLabel2.Text = "COUNSELOR"
            Me.XrLabel2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            Me.XrLabel2.Visible = False
            '
            'XrLabel10
            '
            Me.XrLabel10.LocationFloat = New DevExpress.Utils.PointFloat(482.0834!, 0.0!)
            Me.XrLabel10.Name = "XrLabel10"
            Me.XrLabel10.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel10.SizeF = New System.Drawing.SizeF(66.66669!, 15.0!)
            Me.XrLabel10.StylePriority.UseTextAlignment = False
            Me.XrLabel10.Text = "BOOKED"
            Me.XrLabel10.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel9
            '
            Me.XrLabel9.LocationFloat = New DevExpress.Utils.PointFloat(424.7917!, 0.0!)
            Me.XrLabel9.Name = "XrLabel9"
            Me.XrLabel9.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel9.SizeF = New System.Drawing.SizeF(57.29169!, 15.0!)
            Me.XrLabel9.Text = "AVAIL"
            '
            'XrLabel8
            '
            Me.XrLabel8.LocationFloat = New DevExpress.Utils.PointFloat(191.4583!, 0.0!)
            Me.XrLabel8.Name = "XrLabel8"
            Me.XrLabel8.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel8.SizeF = New System.Drawing.SizeF(233.3334!, 15.0!)
            Me.XrLabel8.StylePriority.UseTextAlignment = False
            Me.XrLabel8.Text = "TYPE"
            Me.XrLabel8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            '
            'XrLabel1
            '
            Me.XrLabel1.LocationFloat = New DevExpress.Utils.PointFloat(26.87494!, 0.0!)
            Me.XrLabel1.Name = "XrLabel1"
            Me.XrLabel1.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel1.SizeF = New System.Drawing.SizeF(152.0833!, 15.0!)
            Me.XrLabel1.Text = "TIME"
            '
            'GroupHeader2
            '
            Me.GroupHeader2.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel_start_date})
            Me.GroupHeader2.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.GroupHeader2.GroupFields.AddRange(New DevExpress.XtraReports.UI.GroupField() {New DevExpress.XtraReports.UI.GroupField("CalculatedFieldDate", DevExpress.XtraReports.UI.XRColumnSortOrder.Ascending)})
            Me.GroupHeader2.HeightF = 33.33333!
            Me.GroupHeader2.Level = 2
            Me.GroupHeader2.Name = "GroupHeader2"
            Me.GroupHeader2.RepeatEveryPage = True
            Me.GroupHeader2.StylePriority.UseFont = False
            '
            'GroupHeader3
            '
            Me.GroupHeader3.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel_office_name})
            Me.GroupHeader3.GroupFields.AddRange(New DevExpress.XtraReports.UI.GroupField() {New DevExpress.XtraReports.UI.GroupField("office", DevExpress.XtraReports.UI.XRColumnSortOrder.Ascending)})
            Me.GroupHeader3.HeightF = 34.375!
            Me.GroupHeader3.Level = 3
            Me.GroupHeader3.Name = "GroupHeader3"
            Me.GroupHeader3.RepeatEveryPage = True
            Me.GroupHeader3.StyleName = "XrControlStyle_GroupHeader"
            '
            'XrLabel_detail_booked
            '
            Me.XrLabel_detail_booked.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "booked")})
            Me.XrLabel_detail_booked.LocationFloat = New DevExpress.Utils.PointFloat(491.4584!, 0.0!)
            Me.XrLabel_detail_booked.Name = "XrLabel_detail_booked"
            Me.XrLabel_detail_booked.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_detail_booked.SizeF = New System.Drawing.SizeF(57.29169!, 15.0!)
            Me.XrLabel_detail_booked.StylePriority.UseTextAlignment = False
            XrSummary1.FormatString = "{0:n0}"
            Me.XrLabel_detail_booked.Summary = XrSummary1
            Me.XrLabel_detail_booked.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
            '
            'AvailableAnalysisReport
            '
            Me.Bands.AddRange(New DevExpress.XtraReports.UI.Band() {Me.Detail, Me.PageHeader, Me.PageFooter, Me.TopMarginBand1, Me.BottomMarginBand1, Me.GroupHeader0, Me.GroupHeader1, Me.GroupFooter1, Me.GroupFooter2, Me.GroupHeader2, Me.GroupHeader3})
            Me.CalculatedFields.AddRange(New DevExpress.XtraReports.UI.CalculatedField() {Me.CalculatedFieldDate})
            Me.Parameters.AddRange(New DevExpress.XtraReports.Parameters.Parameter() {Me.ParameterOffice})
            Me.ScriptLanguage = DevExpress.XtraReports.ScriptLanguage.VisualBasic
            Me.ScriptReferencesString = resources.GetString("$this.ScriptReferencesString")
            Me.Scripts.OnBeforePrint = "AvailableAnalysisReport_BeforePrint"
            Me.ScriptsSource = resources.GetString("$this.ScriptsSource")
            Me.StyleSheet.AddRange(New DevExpress.XtraReports.UI.XRControlStyle() {Me.XrControlStyle_Header, Me.XrControlStyle_Totals, Me.XrControlStyle_HeaderPannel, Me.XrControlStyle_GroupHeader})
            Me.Version = "11.2"
            Me.Controls.SetChildIndex(Me.GroupHeader3, 0)
            Me.Controls.SetChildIndex(Me.GroupHeader2, 0)
            Me.Controls.SetChildIndex(Me.GroupFooter2, 0)
            Me.Controls.SetChildIndex(Me.GroupFooter1, 0)
            Me.Controls.SetChildIndex(Me.GroupHeader1, 0)
            Me.Controls.SetChildIndex(Me.GroupHeader0, 0)
            Me.Controls.SetChildIndex(Me.BottomMarginBand1, 0)
            Me.Controls.SetChildIndex(Me.TopMarginBand1, 0)
            Me.Controls.SetChildIndex(Me.PageFooter, 0)
            Me.Controls.SetChildIndex(Me.PageHeader, 0)
            Me.Controls.SetChildIndex(Me.Detail, 0)
            CType(Me, System.ComponentModel.ISupportInitialize).EndInit()

        End Sub
        Friend WithEvents GroupHeader0 As DevExpress.XtraReports.UI.GroupHeaderBand
        Friend WithEvents GroupHeader1 As DevExpress.XtraReports.UI.GroupHeaderBand
        Friend WithEvents XrLabel_office_name As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents GroupFooter1 As DevExpress.XtraReports.UI.GroupFooterBand
        Friend WithEvents GroupFooter2 As DevExpress.XtraReports.UI.GroupFooterBand
        Friend WithEvents ParameterOffice As DevExpress.XtraReports.Parameters.Parameter
        Friend WithEvents CalculatedFieldDate As DevExpress.XtraReports.UI.CalculatedField
        Friend WithEvents XrLabel_appt_name As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_booked As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_counselor_name As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_start_time As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_available As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrPanel1 As DevExpress.XtraReports.UI.XRPanel
        Friend WithEvents XrLabel2 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel10 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel9 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel8 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel1 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_start_date As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents GroupHeader2 As DevExpress.XtraReports.UI.GroupHeaderBand
        Friend WithEvents GroupHeader3 As DevExpress.XtraReports.UI.GroupHeaderBand
        Friend WithEvents XrLabel_detail_booked As DevExpress.XtraReports.UI.XRLabel
    End Class
End Namespace
