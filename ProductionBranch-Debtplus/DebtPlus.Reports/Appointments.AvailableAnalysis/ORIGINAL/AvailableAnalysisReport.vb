#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}
#End Region

Namespace Appointments.AvailableAnalysis
    Public Class AvailableAnalysisReport
        Public Sub New()
            MyBase.New()
            InitializeComponent()
            AddHandler BeforePrint, AddressOf AvailableAnalysisReport_BeforePrint
            ReportFilter.IsEnabled = True
        End Sub

        Public Overrides ReadOnly Property ReportTitle() As String
            Get
                Return "Appt Avaiability Time Analysis"
            End Get
        End Property

        Public Property Parameter_Office() As Int32
            Get
                Dim parm As DevExpress.XtraReports.Parameters.Parameter = FindParameter("ParameterOffice")
                If parm Is Nothing Then Return -1
                Return Convert.ToInt32(parm.Value)
            End Get
            Set(ByVal value As Int32)
                SetParameter("ParameterOffice", GetType(Int32), value, "Office ID", False)
            End Set
        End Property

        Public Overrides Sub SetReportParameter(ByVal Parameter As String, ByVal Value As Object)
            If String.Compare(Parameter, "Office", False) = 0 Then
                Parameter_Office = Convert.ToInt32(Value)
            Else
                MyBase.SetReportParameter(Parameter, Value)
            End If
        End Sub

        Public Overrides ReadOnly Property ReportSubTitle() As String
            Get
                Return MyBase.ReportSubtitle
            End Get
        End Property

        Public Overrides Function RequestReportParameters() As System.Windows.Forms.DialogResult
            Dim Answer As System.Windows.Forms.DialogResult = DialogResult.OK
            If NeedParameters() Then
                Using frm As New DebtPlus.Reports.Template.Forms.DatedOfficeParametersForm(True)
                    Answer = frm.ShowDialog()
                    Parameter_FromDate = frm.Parameter_FromDate
                    Parameter_ToDate = frm.Parameter_ToDate
                    Parameter_Office = If(frm.Parameter_Office Is System.DBNull.Value, -1, Convert.ToInt32(frm.Parameter_Office))
                End Using
            End If
            Return Answer
        End Function

        Private ds As New System.Data.DataSet("ds")

        Private Sub AvailableAnalysisReport_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs)
            Const TableName As String = "rpt_appt_available_analysis"
            Dim rpt As DevExpress.XtraReports.UI.XtraReport = CType(sender, DevExpress.XtraReports.UI.XtraReport)
            Dim tbl As System.Data.DataTable = ds.Tables(TableName)

            If tbl Is Nothing Then
                Dim sqlInfo As DebtPlus.LINQ.SQLInfoClass = DebtPlus.LINQ.SQLInfoClass.getDefault()
                Using cn As System.Data.SqlClient.SqlConnection = New System.Data.SqlClient.SqlConnection(sqlInfo.ConnectionString)
                    cn.Open()

                    Using cmd As New System.Data.SqlClient.SqlCommand()
                        With cmd
                            cmd.Connection = cn
                            cmd.CommandText = "rpt_appt_available_analysis"
                            cmd.CommandType = System.Data.CommandType.StoredProcedure
                            System.Data.SqlClient.SqlCommandBuilder.DeriveParameters(cmd)
                            cmd.Parameters(1).Value = rpt.Parameters("ParameterFromDate").Value
                            cmd.Parameters(2).Value = rpt.Parameters("ParameterToDate").Value

                            If Convert.ToInt32(rpt.Parameters("ParameterOffice").Value) > 0 Then
                                cmd.Parameters(3).Value = rpt.Parameters("ParameterOffice").Value
                            End If

                            cmd.CommandTimeout = 0
                        End With

                        Using da As New System.Data.SqlClient.SqlDataAdapter(cmd)
                            da.Fill(ds, TableName)
                            tbl = ds.Tables(TableName)

                            rpt.DataSource = New System.Data.DataView(tbl, CType(rpt, DebtPlus.Interfaces.Reports.IReportFilter).ReportFilter.ViewFilter, "office, start_time, appt_time, appt_name, counselor_name", System.Data.DataViewRowState.CurrentRows)
                            For Each item As DevExpress.XtraReports.UI.CalculatedField In rpt.CalculatedFields
                                item.Assign(rpt.DataSource, rpt.DataMember)
                            Next
                        End Using
                    End Using
                End Using
            End If
        End Sub
    End Class
End Namespace
