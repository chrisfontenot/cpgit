#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}

#End Region

Imports DevExpress.XtraEditors.Controls
Imports DebtPlus.Reports.Template.Forms

Imports System.Globalization

Namespace Creditor.PolicyMatrix.Top30
    Public Class ParametersForm
        Inherits ReportParametersForm

        Public Sub New()
            MyBase.New()
            InitializeComponent()
            AddHandler SpinEdit1.EditValueChanging, AddressOf SpinEdit1_EditValueChanging
            AddHandler Load, AddressOf ParametersForm_Load
        End Sub

        Public Property Parameter_Count() As Int32
            Get
                Return Convert.ToInt32(SpinEdit1.EditValue, CultureInfo.InvariantCulture)
            End Get
            Set(ByVal value As Int32)
                SpinEdit1.EditValue = value
            End Set
        End Property

        Private Sub SpinEdit1_EditValueChanging(ByVal sender As Object, ByVal e As ChangingEventArgs)
            ButtonOK.Enabled = Not HasErrors()
        End Sub

        Private Shadows Function HasErrors() As Boolean
            If SpinEdit1.EditValue Is Nothing OrElse SpinEdit1.EditValue Is DBNull.Value Then Return True
            If Convert.ToInt32(SpinEdit1.EditValue) <= 0 Then Return True
            Return False
        End Function

        Private Sub ParametersForm_Load(ByVal sender As Object, ByVal e As EventArgs)
            SpinEdit1.EditValue = 30
            ButtonOK.Enabled = Not HasErrors()
        End Sub
    End Class
End Namespace
