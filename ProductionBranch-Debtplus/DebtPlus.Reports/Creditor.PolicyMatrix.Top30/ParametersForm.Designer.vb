Namespace Creditor.PolicyMatrix.Top30
    <Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
    Partial Class ParametersForm

        'Form overrides dispose to clean up the component list.
        <System.Diagnostics.DebuggerNonUserCode()> _
        Protected Overrides Sub Dispose(ByVal disposing As Boolean)
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
            MyBase.Dispose(disposing)
        End Sub

        'Required by the Windows Form Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Windows Form Designer
        'It can be modified using the Windows Form Designer.  
        'Do not modify it using the code editor.
        <System.Diagnostics.DebuggerStepThrough()> _
        Private Sub InitializeComponent()
            Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl
            Me.SpinEdit1 = New DevExpress.XtraEditors.SpinEdit
            CType(Me.DxErrorProvider1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.SpinEdit1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.SuspendLayout()
            '
            'ButtonOK
            '
            Me.ButtonOK.TabIndex = 2
            '
            'ButtonCancel
            '
            Me.ButtonCancel.TabIndex = 3
            '
            'LabelControl1
            '
            Me.LabelControl1.Location = New System.Drawing.Point(13, 28)
            Me.LabelControl1.Name = "LabelControl1"
            Me.LabelControl1.Size = New System.Drawing.Size(71, 13)
            Me.LabelControl1.TabIndex = 0
            Me.LabelControl1.Text = "Creditor Count"
            '
            'SpinEdit1
            '
            Me.SpinEdit1.EditValue = New Decimal(New Int32() {1, 0, 0, 0})
            Me.SpinEdit1.Location = New System.Drawing.Point(90, 25)
            Me.SpinEdit1.Name = "SpinEdit1"
            Me.SpinEdit1.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton})
            Me.SpinEdit1.Properties.DisplayFormat.FormatString = "f0"
            Me.SpinEdit1.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.SpinEdit1.Properties.EditFormat.FormatString = "f0"
            Me.SpinEdit1.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.SpinEdit1.Properties.IsFloatValue = False
            Me.SpinEdit1.Properties.Mask.BeepOnError = True
            Me.SpinEdit1.Properties.Mask.EditMask = "f0"
            Me.SpinEdit1.Properties.MaxValue = New Decimal(New Int32() {800000, 0, 0, 0})
            Me.SpinEdit1.Properties.MinValue = New Decimal(New Int32() {1, 0, 0, 0})
            Me.SpinEdit1.Size = New System.Drawing.Size(72, 20)
            Me.SpinEdit1.TabIndex = 1
            '
            'ParametersForm
            '
            Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
            Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
            Me.ClientSize = New System.Drawing.Size(336, 135)
            Me.Controls.Add(Me.LabelControl1)
            Me.Controls.Add(Me.SpinEdit1)
            Me.LookAndFeel.UseDefaultLookAndFeel = True
            Me.Name = "ParametersForm"
            Me.Controls.SetChildIndex(Me.SpinEdit1, 0)
            Me.Controls.SetChildIndex(Me.LabelControl1, 0)
            Me.Controls.SetChildIndex(Me.ButtonOK, 0)
            Me.Controls.SetChildIndex(Me.ButtonCancel, 0)
            CType(Me.DxErrorProvider1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.SpinEdit1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            Me.ResumeLayout(False)
            Me.PerformLayout()

        End Sub
        Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents SpinEdit1 As DevExpress.XtraEditors.SpinEdit
    End Class
End Namespace
