﻿Namespace Creditor.PolicyMatrix.Top30
    Partial Class CreditorPolicyMatrixTop30Report
        Inherits DebtPlus.Reports.Template.TemplateXtraReportClass

        'Component overrides dispose to clean up the component list.
        Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
            If disposing Then
                If components IsNot Nothing Then
                    components.Dispose()
                End If
            End If
            MyBase.Dispose(disposing)
        End Sub

        'Required by the Designer
        Private components As System.ComponentModel.IContainer

        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
            Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(CreditorPolicyMatrixTop30Report))
            Me.ParameterItemCount = New DevExpress.XtraReports.Parameters.Parameter()
            Me.XrSubreport_Policy = New DevExpress.XtraReports.UI.XRSubreport()
            Me.XtraReport_Policy1 = New DebtPlus.Reports.Creditor.PolicyMatrix.Top30.XtraReport_Policy()
            Me.XrSubreport_Comments = New DevExpress.XtraReports.UI.XRSubreport()
            Me.XtraReport_Comments1 = New DebtPlus.Reports.Creditor.PolicyMatrix.Top30.XtraReport_Comments()
            Me.XrSubreport_Name = New DevExpress.XtraReports.UI.XRSubreport()
            Me.XtraReport_CreditorName1 = New DebtPlus.Reports.Creditor.PolicyMatrix.Top30.XtraReport_CreditorName()
            Me.XrSubreport_CreditorHeader = New DevExpress.XtraReports.UI.XRSubreport()
            Me.XtraReport_CreditorHeader1 = New DebtPlus.Reports.Creditor.PolicyMatrix.Top30.XtraReport_CreditorHeader()
            CType(Me.XtraReport_Policy1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.XtraReport_Comments1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.XtraReport_CreditorName1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.XtraReport_CreditorHeader1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
            '
            'PageHeader
            '
            Me.PageHeader.HeightF = 121.4583!
            '
            'XrLabel_Title
            '
            Me.XrLabel_Title.StylePriority.UseFont = False
            '
            'XrPageInfo_PageNumber
            '
            Me.XrPageInfo_PageNumber.StylePriority.UseTextAlignment = False
            '
            'XRLabel_Agency_Name
            '
            Me.XRLabel_Agency_Name.StylePriority.UseTextAlignment = False
            '
            'XrLabel_Agency_Address3
            '
            Me.XrLabel_Agency_Address3.StylePriority.UseFont = False
            Me.XrLabel_Agency_Address3.StylePriority.UseTextAlignment = False
            '
            'XrLabel_Agency_Address1
            '
            Me.XrLabel_Agency_Address1.StylePriority.UseFont = False
            Me.XrLabel_Agency_Address1.StylePriority.UseTextAlignment = False
            '
            'XrLabel_Agency_Phone
            '
            Me.XrLabel_Agency_Phone.StylePriority.UseFont = False
            Me.XrLabel_Agency_Phone.StylePriority.UseTextAlignment = False
            '
            'XrLabel_Agency_Address2
            '
            Me.XrLabel_Agency_Address2.StylePriority.UseFont = False
            Me.XrLabel_Agency_Address2.StylePriority.UseTextAlignment = False
            '
            'Detail
            '
            Me.Detail.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrSubreport_Comments, Me.XrSubreport_Policy, Me.XrSubreport_Name, Me.XrSubreport_CreditorHeader})
            Me.Detail.HeightF = 91.99995!
            Me.Detail.PageBreak = DevExpress.XtraReports.UI.PageBreak.AfterBand
            '
            'ParameterItemCount
            '
            Me.ParameterItemCount.Name = "ParameterItemCount"
            Me.ParameterItemCount.Type = GetType(Integer)
            Me.ParameterItemCount.Value = 30
            Me.ParameterItemCount.Visible = False
            '
            'XrSubreport_Policy
            '
            Me.XrSubreport_Policy.LocationFloat = New DevExpress.Utils.PointFloat(8.0!, 45.99997!)
            Me.XrSubreport_Policy.Name = "XrSubreport_Policy"
            Me.XrSubreport_Policy.ReportSource = Me.XtraReport_Policy1
            Me.XrSubreport_Policy.SizeF = New System.Drawing.SizeF(742.0001!, 22.99998!)
            '
            'XrSubreport_Comments
            '
            Me.XrSubreport_Comments.LocationFloat = New DevExpress.Utils.PointFloat(8.0!, 68.99995!)
            Me.XrSubreport_Comments.Name = "XrSubreport_Comments"
            Me.XrSubreport_Comments.ReportSource = Me.XtraReport_Comments1
            Me.XrSubreport_Comments.SizeF = New System.Drawing.SizeF(742.0001!, 22.99999!)
            '
            'XrSubreport_Name
            '
            Me.XrSubreport_Name.LocationFloat = New DevExpress.Utils.PointFloat(8.0!, 22.99999!)
            Me.XrSubreport_Name.Name = "XrSubreport_Name"
            Me.XrSubreport_Name.ReportSource = Me.XtraReport_CreditorName1
            Me.XrSubreport_Name.SizeF = New System.Drawing.SizeF(742.0001!, 22.99999!)
            '
            'XrSubreport_CreditorHeader
            '
            Me.XrSubreport_CreditorHeader.LocationFloat = New DevExpress.Utils.PointFloat(7.999921!, 0.0!)
            Me.XrSubreport_CreditorHeader.Name = "XrSubreport_CreditorHeader"
            Me.XrSubreport_CreditorHeader.ReportSource = Me.XtraReport_CreditorHeader1
            Me.XrSubreport_CreditorHeader.SizeF = New System.Drawing.SizeF(742.0001!, 23.0!)
            '
            'CreditorPolicyMatrixTop30Report
            '
            Me.Bands.AddRange(New DevExpress.XtraReports.UI.Band() {Me.TopMarginBand1, Me.Detail, Me.BottomMarginBand1, Me.PageHeader, Me.PageFooter})
            Me.Parameters.AddRange(New DevExpress.XtraReports.Parameters.Parameter() {Me.ParameterItemCount})
            Me.RequestParameters = False
            Me.ScriptLanguage = DevExpress.XtraReports.ScriptLanguage.VisualBasic
            Me.ScriptReferencesString = resources.GetString("$this.ScriptReferencesString")
            Me.Scripts.OnBeforePrint = "CreditorPolicyMatrixTop30Report_BeforePrint"
            Me.ScriptsSource = resources.GetString("$this.ScriptsSource")
            Me.StyleSheet.AddRange(New DevExpress.XtraReports.UI.XRControlStyle() {Me.XrControlStyle_Header, Me.XrControlStyle_Totals, Me.XrControlStyle_HeaderPannel, Me.XrControlStyle_GroupHeader})
            Me.Version = "11.2"
            CType(Me.XtraReport_Policy1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.XtraReport_Comments1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.XtraReport_CreditorName1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.XtraReport_CreditorHeader1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me, System.ComponentModel.ISupportInitialize).EndInit()

        End Sub
        Friend WithEvents ParameterItemCount As DevExpress.XtraReports.Parameters.Parameter
        Friend WithEvents XrSubreport_Comments As DevExpress.XtraReports.UI.XRSubreport
        Friend WithEvents XrSubreport_Policy As DevExpress.XtraReports.UI.XRSubreport
        Friend WithEvents XrSubreport_Name As DevExpress.XtraReports.UI.XRSubreport
        Friend WithEvents XrSubreport_CreditorHeader As DevExpress.XtraReports.UI.XRSubreport
        Private WithEvents XtraReport_CreditorHeader1 As DebtPlus.Reports.Creditor.PolicyMatrix.Top30.XtraReport_CreditorHeader
        Private WithEvents XtraReport_CreditorName1 As DebtPlus.Reports.Creditor.PolicyMatrix.Top30.XtraReport_CreditorName
        Private WithEvents XtraReport_Policy1 As DebtPlus.Reports.Creditor.PolicyMatrix.Top30.XtraReport_Policy
        Private WithEvents XtraReport_Comments1 As DebtPlus.Reports.Creditor.PolicyMatrix.Top30.XtraReport_Comments
    End Class
End Namespace
