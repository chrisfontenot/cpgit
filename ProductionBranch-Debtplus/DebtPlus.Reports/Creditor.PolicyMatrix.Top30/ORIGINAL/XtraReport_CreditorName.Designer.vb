Namespace Creditor.PolicyMatrix.Top30
    <Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
    Partial Class XtraReport_CreditorName
        Inherits DevExpress.XtraReports.UI.XtraReport

        'XtraReport overrides dispose to clean up the component list.
        <System.Diagnostics.DebuggerNonUserCode()> _
        Protected Overrides Sub Dispose(ByVal disposing As Boolean)
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
            MyBase.Dispose(disposing)
        End Sub

        'Required by the Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Designer
        'It can be modified using the Designer.  
        'Do not modify it using the code editor.
        <System.Diagnostics.DebuggerStepThrough()> _
        Private Sub InitializeComponent()
            Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(XtraReport_CreditorName))
            Me.Detail = New DevExpress.XtraReports.UI.DetailBand()
            Me.XrLabel_division = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_name = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel15 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel14 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_full_disclosure = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel12 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_edi_creditor = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel10 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_eft_creditor = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel8 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_updated = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel6 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_received = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel4 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_installed = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel2 = New DevExpress.XtraReports.UI.XRLabel()
            Me.TopMargin = New DevExpress.XtraReports.UI.TopMarginBand()
            Me.BottomMargin = New DevExpress.XtraReports.UI.BottomMarginBand()
            Me.ReportHeader = New DevExpress.XtraReports.UI.ReportHeaderBand()
            Me.XrLabel1 = New DevExpress.XtraReports.UI.XRLabel()
            CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
            '
            'Detail
            '
            Me.Detail.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel_division, Me.XrLabel_name, Me.XrLabel15, Me.XrLabel14, Me.XrLabel_full_disclosure, Me.XrLabel12, Me.XrLabel_edi_creditor, Me.XrLabel10, Me.XrLabel_eft_creditor, Me.XrLabel8, Me.XrLabel_updated, Me.XrLabel6, Me.XrLabel_received, Me.XrLabel4, Me.XrLabel_installed, Me.XrLabel2})
            Me.Detail.HeightF = 59.99998!
            Me.Detail.Name = "Detail"
            Me.Detail.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
            Me.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
            '
            'XrLabel_division
            '
            Me.XrLabel_division.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "division")})
            Me.XrLabel_division.LocationFloat = New DevExpress.Utils.PointFloat(140.625!, 44.99998!)
            Me.XrLabel_division.Name = "XrLabel_division"
            Me.XrLabel_division.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_division.SizeF = New System.Drawing.SizeF(522.9166!, 14.99999!)
            '
            'XrLabel_name
            '
            Me.XrLabel_name.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "name")})
            Me.XrLabel_name.LocationFloat = New DevExpress.Utils.PointFloat(140.625!, 29.99999!)
            Me.XrLabel_name.Name = "XrLabel_name"
            Me.XrLabel_name.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_name.SizeF = New System.Drawing.SizeF(522.9166!, 14.99999!)
            '
            'XrLabel15
            '
            Me.XrLabel15.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel15.LocationFloat = New DevExpress.Utils.PointFloat(50.0!, 44.99998!)
            Me.XrLabel15.Name = "XrLabel15"
            Me.XrLabel15.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel15.SizeF = New System.Drawing.SizeF(76.04166!, 14.99999!)
            Me.XrLabel15.StylePriority.UseFont = False
            Me.XrLabel15.Text = "DIVISION"
            '
            'XrLabel14
            '
            Me.XrLabel14.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel14.LocationFloat = New DevExpress.Utils.PointFloat(50.0!, 29.99999!)
            Me.XrLabel14.Name = "XrLabel14"
            Me.XrLabel14.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel14.SizeF = New System.Drawing.SizeF(76.04166!, 14.99999!)
            Me.XrLabel14.StylePriority.UseFont = False
            Me.XrLabel14.Text = "NAME"
            '
            'XrLabel_full_disclosure
            '
            Me.XrLabel_full_disclosure.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "full_disclosure")})
            Me.XrLabel_full_disclosure.LocationFloat = New DevExpress.Utils.PointFloat(373.9584!, 14.99999!)
            Me.XrLabel_full_disclosure.Name = "XrLabel_full_disclosure"
            Me.XrLabel_full_disclosure.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_full_disclosure.SizeF = New System.Drawing.SizeF(22.91666!, 14.99999!)
            '
            'XrLabel12
            '
            Me.XrLabel12.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel12.LocationFloat = New DevExpress.Utils.PointFloat(233.3333!, 14.99999!)
            Me.XrLabel12.Name = "XrLabel12"
            Me.XrLabel12.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel12.SizeF = New System.Drawing.SizeF(140.625!, 14.99999!)
            Me.XrLabel12.StylePriority.UseFont = False
            Me.XrLabel12.Text = "FULL DISCLOSURE"
            '
            'XrLabel_edi_creditor
            '
            Me.XrLabel_edi_creditor.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "edi_creditor")})
            Me.XrLabel_edi_creditor.LocationFloat = New DevExpress.Utils.PointFloat(177.0833!, 14.99999!)
            Me.XrLabel_edi_creditor.Name = "XrLabel_edi_creditor"
            Me.XrLabel_edi_creditor.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_edi_creditor.SizeF = New System.Drawing.SizeF(22.91666!, 14.99999!)
            '
            'XrLabel10
            '
            Me.XrLabel10.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel10.LocationFloat = New DevExpress.Utils.PointFloat(140.625!, 14.99999!)
            Me.XrLabel10.Name = "XrLabel10"
            Me.XrLabel10.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel10.SizeF = New System.Drawing.SizeF(36.45831!, 14.99999!)
            Me.XrLabel10.StylePriority.UseFont = False
            Me.XrLabel10.Text = "EDI"
            '
            'XrLabel_eft_creditor
            '
            Me.XrLabel_eft_creditor.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "eft_creditor")})
            Me.XrLabel_eft_creditor.LocationFloat = New DevExpress.Utils.PointFloat(103.125!, 14.99999!)
            Me.XrLabel_eft_creditor.Name = "XrLabel_eft_creditor"
            Me.XrLabel_eft_creditor.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_eft_creditor.SizeF = New System.Drawing.SizeF(22.91666!, 14.99999!)
            '
            'XrLabel8
            '
            Me.XrLabel8.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel8.LocationFloat = New DevExpress.Utils.PointFloat(50.0!, 14.99999!)
            Me.XrLabel8.Name = "XrLabel8"
            Me.XrLabel8.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel8.SizeF = New System.Drawing.SizeF(53.125!, 14.99999!)
            Me.XrLabel8.StylePriority.UseFont = False
            Me.XrLabel8.Text = "EFT"
            '
            'XrLabel_updated
            '
            Me.XrLabel_updated.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "updated", "{0:d}")})
            Me.XrLabel_updated.LocationFloat = New DevExpress.Utils.PointFloat(564.5833!, 0.0!)
            Me.XrLabel_updated.Name = "XrLabel_updated"
            Me.XrLabel_updated.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_updated.SizeF = New System.Drawing.SizeF(98.95833!, 14.99999!)
            '
            'XrLabel6
            '
            Me.XrLabel6.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel6.LocationFloat = New DevExpress.Utils.PointFloat(486.4583!, 0.0!)
            Me.XrLabel6.Name = "XrLabel6"
            Me.XrLabel6.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel6.SizeF = New System.Drawing.SizeF(78.12497!, 14.99999!)
            Me.XrLabel6.StylePriority.UseFont = False
            Me.XrLabel6.Text = "UPDATED"
            '
            'XrLabel_received
            '
            Me.XrLabel_received.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "received", "{0:d}")})
            Me.XrLabel_received.LocationFloat = New DevExpress.Utils.PointFloat(373.9583!, 0.0!)
            Me.XrLabel_received.Name = "XrLabel_received"
            Me.XrLabel_received.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_received.SizeF = New System.Drawing.SizeF(98.95833!, 14.99999!)
            '
            'XrLabel4
            '
            Me.XrLabel4.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel4.LocationFloat = New DevExpress.Utils.PointFloat(288.5417!, 0.0!)
            Me.XrLabel4.Name = "XrLabel4"
            Me.XrLabel4.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel4.SizeF = New System.Drawing.SizeF(85.41669!, 14.99999!)
            Me.XrLabel4.StylePriority.UseFont = False
            Me.XrLabel4.Text = "RECEIVED"
            '
            'XrLabel_installed
            '
            Me.XrLabel_installed.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "installed", "{0:d}")})
            Me.XrLabel_installed.LocationFloat = New DevExpress.Utils.PointFloat(177.0833!, 0.0!)
            Me.XrLabel_installed.Name = "XrLabel_installed"
            Me.XrLabel_installed.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_installed.SizeF = New System.Drawing.SizeF(98.95833!, 14.99999!)
            '
            'XrLabel2
            '
            Me.XrLabel2.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel2.LocationFloat = New DevExpress.Utils.PointFloat(50.0!, 0.0!)
            Me.XrLabel2.Name = "XrLabel2"
            Me.XrLabel2.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel2.SizeF = New System.Drawing.SizeF(127.0833!, 14.99999!)
            Me.XrLabel2.StylePriority.UseFont = False
            Me.XrLabel2.Text = "DATE INSTALLED"
            '
            'TopMargin
            '
            Me.TopMargin.HeightF = 25.0!
            Me.TopMargin.Name = "TopMargin"
            Me.TopMargin.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
            Me.TopMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
            '
            'BottomMargin
            '
            Me.BottomMargin.HeightF = 25.0!
            Me.BottomMargin.Name = "BottomMargin"
            Me.BottomMargin.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
            Me.BottomMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
            '
            'ReportHeader
            '
            Me.ReportHeader.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel1})
            Me.ReportHeader.HeightF = 41.66667!
            Me.ReportHeader.Name = "ReportHeader"
            '
            'XrLabel1
            '
            Me.XrLabel1.Font = New System.Drawing.Font("Times New Roman", 12.0!, System.Drawing.FontStyle.Bold)
            Me.XrLabel1.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 20.00001!)
            Me.XrLabel1.Name = "XrLabel1"
            Me.XrLabel1.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel1.SizeF = New System.Drawing.SizeF(373.9584!, 21.66666!)
            Me.XrLabel1.StylePriority.UseFont = False
            Me.XrLabel1.Text = "INFORMATION FROM MATRIX PROVIDER"
            '
            'XtraReport_CreditorName
            '
            Me.Bands.AddRange(New DevExpress.XtraReports.UI.Band() {Me.Detail, Me.TopMargin, Me.BottomMargin, Me.ReportHeader})
            Me.Margins = New System.Drawing.Printing.Margins(25, 25, 25, 25)
            Me.ReportPrintOptions.DetailCountOnEmptyDataSource = 0
            Me.ScriptLanguage = DevExpress.XtraReports.ScriptLanguage.VisualBasic
            Me.ScriptReferencesString = resources.GetString("$this.ScriptReferencesString")
            Me.Scripts.OnBeforePrint = "XtraReport_CreditorName_BeforePrint"
            Me.ScriptsSource = resources.GetString("$this.ScriptsSource")
            Me.Version = "11.2"
            CType(Me, System.ComponentModel.ISupportInitialize).EndInit()

        End Sub
        Friend WithEvents Detail As DevExpress.XtraReports.UI.DetailBand
        Friend WithEvents TopMargin As DevExpress.XtraReports.UI.TopMarginBand
        Friend WithEvents BottomMargin As DevExpress.XtraReports.UI.BottomMarginBand
        Friend WithEvents XrLabel_updated As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel6 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_received As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel4 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_installed As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel2 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents ReportHeader As DevExpress.XtraReports.UI.ReportHeaderBand
        Friend WithEvents XrLabel1 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_division As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_name As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel15 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel14 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_full_disclosure As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel12 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_edi_creditor As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel10 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_eft_creditor As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel8 As DevExpress.XtraReports.UI.XRLabel
    End Class
End Namespace
