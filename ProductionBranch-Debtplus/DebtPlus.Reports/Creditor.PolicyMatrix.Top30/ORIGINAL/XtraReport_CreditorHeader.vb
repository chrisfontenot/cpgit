#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}
#End Region

Namespace Creditor.PolicyMatrix.Top30
    Public Class XtraReport_CreditorHeader

        Public Sub New()
            MyBase.New()
            InitializeComponent()
            AddHandler XrLabel_creditor_name.BeforePrint, AddressOf XrLabel_creditor_name_BeforePrint
            AddHandler XrLabel_creditor.BeforePrint, AddressOf XrLabel_creditor_name_BeforePrint
        End Sub

        Private Sub XrLabel_creditor_name_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs)
            Dim lbl As DevExpress.XtraReports.UI.XRLabel = CType(sender, DevExpress.XtraReports.UI.XRLabel)
            Dim rpt As DevExpress.XtraReports.UI.XtraReport = CType(lbl.Report, DevExpress.XtraReports.UI.XtraReport)

            Dim MasterRpt As DevExpress.XtraReports.UI.XtraReport = rpt.MasterReport
            Dim Creditor As String = Convert.ToString(MasterRpt.GetCurrentColumnValue("creditor"))
            If Creditor <> String.Empty Then
                Dim sqlInfo As DebtPlus.LINQ.SQLInfoClass = DebtPlus.LINQ.SQLInfoClass.getDefault()

                Dim creditor_name As String = String.Empty
                Dim division As String = String.Empty
                Dim comment As String = String.Empty
                Dim sic As String = String.Empty

                Using cn As System.Data.SqlClient.SqlConnection = New System.Data.SqlClient.SqlConnection(sqlInfo.ConnectionString)
                    cn.Open()
                    Using cmd As New System.Data.SqlClient.SqlCommand()
                        cmd.Connection = cn
                        cmd.CommandText = "rpt_PolicyMatrix_Creditor"
                        cmd.CommandType = System.Data.CommandType.StoredProcedure

                        System.Data.SqlClient.SqlCommandBuilder.DeriveParameters(cmd)
                        cmd.CommandTimeout = 0
                        cmd.Parameters(1).Value = Creditor

                        Using rd As System.Data.SqlClient.SqlDataReader = cmd.ExecuteReader(System.Data.CommandBehavior.CloseConnection Or System.Data.CommandBehavior.SingleRow)
                            If rd.Read() Then
                                For FldNo As Integer = rd.FieldCount - 1 To 0 Step -1
                                    If Not rd.IsDBNull(FldNo) Then
                                        Select Case rd.GetName(FldNo).ToLower
                                            Case "creditor_name"
                                                creditor_name = rd.GetString(FldNo).Trim
                                            Case "division"
                                                division = rd.GetString(FldNo).Trim
                                            Case "comment"
                                                comment = rd.GetString(FldNo).Trim
                                            Case "sic"
                                                sic = rd.GetString(FldNo).Trim
                                            Case Else
                                        End Select
                                    End If
                                Next
                            End If
                        End Using
                    End Using
                End Using

                Dim sb As New System.Text.StringBuilder()
                If creditor_name <> String.Empty Then
                    sb.Append(Environment.NewLine)
                    sb.Append(creditor_name)
                End If

                If division <> String.Empty Then
                    sb.Append(Environment.NewLine)
                    sb.Append(division)
                End If

                If comment <> String.Empty Then
                    sb.Append(Environment.NewLine)
                    sb.Append(comment)
                End If

                If sic <> String.Empty Then
                    Dim GroupName As String = String.Empty

                    If System.Char.IsLetter(sic) Then
                        GroupName = sic.Substring(0, 1)
                        sic = sic.Substring(1)
                    End If

                    If sic.Length >= 10 Then
                        sic = String.Format("{0}-{1}-{2}", sic.Substring(0, 4), sic.Substring(4, 2), sic.Substring(6))
                    End If

                    sic = GroupName + sic

                    sb.Append(Environment.NewLine)
                    sb.Append(Environment.NewLine)
                    sb.Append(sic)
                End If

                If sb.Length > 0 Then
                    sb.Remove(0, 2)
                End If

                lbl.Text = sb.ToString()
            End If
        End Sub

        Private Sub XrLabel1_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs)
            Dim lbl As DevExpress.XtraReports.UI.XRLabel = CType(sender, DevExpress.XtraReports.UI.XRLabel)
            Dim rpt As DevExpress.XtraReports.UI.XtraReport = CType(lbl.Report, DevExpress.XtraReports.UI.XtraReport)

            Dim MasterRpt As DevExpress.XtraReports.UI.XtraReport = CType(rpt.MasterReport, DevExpress.XtraReports.UI.XtraReport)
            If MasterRpt IsNot Nothing Then

                Dim Creditor As String = DebtPlus.Utils.Nulls.DStr(MasterRpt.GetCurrentColumnValue("creditor")).Trim()
                If Creditor <> String.Empty Then
                    lbl.Text = Creditor
                End If
            End If
        End Sub
    End Class
End Namespace
