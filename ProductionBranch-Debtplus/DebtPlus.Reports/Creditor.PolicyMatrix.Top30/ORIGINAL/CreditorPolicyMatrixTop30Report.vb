#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}
#End Region

Option Compare Binary
Option Explicit On 
Option Strict On

Namespace Creditor.PolicyMatrix.Top30
    Public Class CreditorPolicyMatrixTop30Report

        Public Sub New()
            MyBase.New()
            InitializeComponent()
        End Sub

        Public Overrides ReadOnly Property ReportTitle() As String
            Get
                Return "Policy Matrix"
            End Get
        End Property

        Public Overrides ReadOnly Property ReportSubTitle() As String
            Get
                Return Convert.ToString(GetCurrentColumnValue("creditor"))
            End Get
        End Property

        Public Property Parameter_ItemCount() As Int32
            Get
                Dim parm As DevExpress.XtraReports.Parameters.Parameter = FindParameter("ParameterItemCount")
                If parm Is Nothing Then Return -1
                Return Convert.ToInt32(parm.Value)
            End Get
            Set(ByVal value As Int32)
                SetParameter("ParameterItemCount", GetType(Int32), value, "Item Count", False)
            End Set
        End Property

        Public Overrides Sub SetReportParameter(ByVal Parameter As String, ByVal Value As Object)
            If Parameter = "ItemCount" Then
                Parameter_ItemCount = Convert.ToInt32(Value, System.Globalization.CultureInfo.InvariantCulture)
            Else
                MyBase.SetReportParameter(Parameter, Value)
            End If
        End Sub

        Public Overrides Function NeedParameters() As Boolean
            Return True
        End Function

        Public Overrides Function RequestReportParameters() As System.Windows.Forms.DialogResult
            Dim Answer As System.Windows.Forms.DialogResult
            Using frm As New ParametersForm()
                Answer = frm.ShowDialog()
                Parameter_ItemCount = frm.Parameter_Count
            End Using
            Return Answer
        End Function

        Protected ds As New System.Data.DataSet("ds")
        Protected Overridable Sub Report_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs) Handles Me.BeforePrint
            Const TableName As String = "rpt_PolicyMatrix_Top30"
            Dim rpt As DevExpress.XtraReports.UI.XtraReport = CType(sender, DevExpress.XtraReports.UI.XtraReport)
            Dim sqlInfo As DebtPlus.LINQ.SQLInfoClass = DebtPlus.LINQ.SQLInfoClass.getDefault()

            Using cn As System.Data.SqlClient.SqlConnection = New System.Data.SqlClient.SqlConnection(sqlInfo.ConnectionString)
                cn.Open()

                Using cmd As New System.Data.SqlClient.SqlCommand()
                    With cmd
                        cmd.Connection = cn
                        cmd.CommandText = "rpt_PolicyMatrix_Top30"
                        cmd.CommandType = System.Data.CommandType.StoredProcedure
                        System.Data.SqlClient.SqlCommandBuilder.DeriveParameters(cmd)

                        cmd.CommandTimeout = 0
                        If Convert.ToInt32(rpt.Parameters("ParameterItemCount").Value) > 0 Then
                            cmd.Parameters(1).Value = rpt.Parameters("ParameterItemCount").Value
                        End If
                    End With

                    Using da As New System.Data.SqlClient.SqlDataAdapter(cmd)
                        da.Fill(ds, TableName)
                    End Using
                End Using
            End Using

            Dim tbl As System.Data.DataTable = ds.Tables(TableName)
            DataSource = tbl.DefaultView
        End Sub
    End Class
End Namespace
