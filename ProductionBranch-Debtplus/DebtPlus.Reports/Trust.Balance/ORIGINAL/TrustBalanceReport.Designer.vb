﻿Namespace Trust.Balance
    Partial Class TrustBalanceReport
        Inherits DebtPlus.Reports.Template.DatedTemplateXtraReportClass

        'Component overrides dispose to clean up the component list.
        Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
            If disposing Then
                If components IsNot Nothing Then
                    components.Dispose()
                End If
            End If
            MyBase.Dispose(disposing)
        End Sub

        'Required by the Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Designer
        'It can be modified using the Designer.
        'Do not modify it using the code editor.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
            Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(TrustBalanceReport))
            Me.XrLabel1 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLine1 = New DevExpress.XtraReports.UI.XRLine()
            Me.XrLabel2 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel21 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel16 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel20 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLine2 = New DevExpress.XtraReports.UI.XRLine()
            Me.XrLabel28 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_cc = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel30 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_rs = New DevExpress.XtraReports.UI.XRLabel()
            Me.ReportFooter = New DevExpress.XtraReports.UI.ReportFooterBand()
            Me.XrLabel33 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel31 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel32 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel34 = New DevExpress.XtraReports.UI.XRLabel()
            Me.total_credit = New DevExpress.XtraReports.UI.CalculatedField()
            Me.total_debit = New DevExpress.XtraReports.UI.CalculatedField()
            Me.XrTable1 = New DevExpress.XtraReports.UI.XRTable()
            Me.XrTableRow1 = New DevExpress.XtraReports.UI.XRTableRow()
            Me.XrTableCell1 = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableCell_dp = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableCell3 = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableRow2 = New DevExpress.XtraReports.UI.XRTableRow()
            Me.XrTableCell4 = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableCell_rf = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableCell6 = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableRow3 = New DevExpress.XtraReports.UI.XRTableRow()
            Me.XrTableCell7 = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableCell_rr = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableCell9 = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableRow4 = New DevExpress.XtraReports.UI.XRTableRow()
            Me.XrTableCell10 = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableCell_vd = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableCell12 = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableRow5 = New DevExpress.XtraReports.UI.XRTableRow()
            Me.XrTableCell13 = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableCell_vr = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableCell15 = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableRow6 = New DevExpress.XtraReports.UI.XRTableRow()
            Me.XrTableCell16 = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableCell_oc = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableCell_total_credit = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTable2 = New DevExpress.XtraReports.UI.XRTable()
            Me.XrTableRow7 = New DevExpress.XtraReports.UI.XRTableRow()
            Me.XrTableCell2 = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableCell_ad = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableCell8 = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableRow8 = New DevExpress.XtraReports.UI.XRTableRow()
            Me.XrTableCell11 = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableCell_md = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableCell17 = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableRow9 = New DevExpress.XtraReports.UI.XRTableRow()
            Me.XrTableCell18 = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableCell_cm = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableCell20 = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableRow10 = New DevExpress.XtraReports.UI.XRTableRow()
            Me.XrTableCell21 = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableCell_cr = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableCell23 = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableRow12 = New DevExpress.XtraReports.UI.XRTableRow()
            Me.XrTableCell27 = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableCell_co = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableCell_total_debit = New DevExpress.XtraReports.UI.XRTableCell()
            CType(Me.XrTable1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.XrTable2, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
            '
            'XrLabel_Title
            '
            Me.XrLabel_Title.StylePriority.UseFont = False
            '
            'XrPageInfo_PageNumber
            '
            Me.XrPageInfo_PageNumber.StylePriority.UseTextAlignment = False
            '
            'XRLabel_Agency_Name
            '
            Me.XRLabel_Agency_Name.StylePriority.UseTextAlignment = False
            '
            'XrLabel_Agency_Address3
            '
            Me.XrLabel_Agency_Address3.StylePriority.UseFont = False
            Me.XrLabel_Agency_Address3.StylePriority.UseTextAlignment = False
            '
            'XrLabel_Agency_Address1
            '
            Me.XrLabel_Agency_Address1.StylePriority.UseFont = False
            Me.XrLabel_Agency_Address1.StylePriority.UseTextAlignment = False
            '
            'XrLabel_Agency_Phone
            '
            Me.XrLabel_Agency_Phone.StylePriority.UseFont = False
            Me.XrLabel_Agency_Phone.StylePriority.UseTextAlignment = False
            '
            'XrLabel_Agency_Address2
            '
            Me.XrLabel_Agency_Address2.StylePriority.UseFont = False
            Me.XrLabel_Agency_Address2.StylePriority.UseTextAlignment = False
            '
            'Detail
            '
            Me.Detail.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrTable2, Me.XrTable1, Me.XrLabel_rs, Me.XrLabel30, Me.XrLabel_cc, Me.XrLabel28, Me.XrLine2, Me.XrLabel20, Me.XrLabel16, Me.XrLabel21, Me.XrLabel2, Me.XrLine1, Me.XrLabel1})
            Me.Detail.HeightF = 647.2083!
            '
            'XrLabel1
            '
            Me.XrLabel1.Font = New System.Drawing.Font("Times New Roman", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.XrLabel1.LocationFloat = New DevExpress.Utils.PointFloat(8.0!, 67.0!)
            Me.XrLabel1.Name = "XrLabel1"
            Me.XrLabel1.SizeF = New System.Drawing.SizeF(245.0!, 23.0!)
            Me.XrLabel1.Text = "Starting Trust Balance"
            '
            'XrLine1
            '
            Me.XrLine1.LineWidth = 2
            Me.XrLine1.LocationFloat = New DevExpress.Utils.PointFloat(550.0!, 67.0!)
            Me.XrLine1.Name = "XrLine1"
            Me.XrLine1.SizeF = New System.Drawing.SizeF(100.0!, 23.0!)
            '
            'XrLabel2
            '
            Me.XrLabel2.CanGrow = False
            Me.XrLabel2.Font = New System.Drawing.Font("Times New Roman", 12.0!, System.Drawing.FontStyle.Bold)
            Me.XrLabel2.LocationFloat = New DevExpress.Utils.PointFloat(8.0!, 115.0!)
            Me.XrLabel2.Name = "XrLabel2"
            Me.XrLabel2.SizeF = New System.Drawing.SizeF(134.0!, 25.0!)
            Me.XrLabel2.Text = "Credits (*)"
            Me.XrLabel2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            Me.XrLabel2.WordWrap = False
            '
            'XrLabel21
            '
            Me.XrLabel21.CanGrow = False
            Me.XrLabel21.Font = New System.Drawing.Font("Times New Roman", 12.0!, System.Drawing.FontStyle.Bold)
            Me.XrLabel21.LocationFloat = New DevExpress.Utils.PointFloat(8.0!, 275.0!)
            Me.XrLabel21.Name = "XrLabel21"
            Me.XrLabel21.SizeF = New System.Drawing.SizeF(134.0!, 25.0!)
            Me.XrLabel21.Text = "Debits"
            Me.XrLabel21.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            Me.XrLabel21.WordWrap = False
            '
            'XrLabel16
            '
            Me.XrLabel16.CanGrow = False
            Me.XrLabel16.Font = New System.Drawing.Font("Times New Roman", 12.0!, System.Drawing.FontStyle.Bold)
            Me.XrLabel16.LocationFloat = New DevExpress.Utils.PointFloat(8.0!, 408.0!)
            Me.XrLabel16.Name = "XrLabel16"
            Me.XrLabel16.SizeF = New System.Drawing.SizeF(317.0!, 25.0!)
            Me.XrLabel16.Text = "Reserved for In-Process Disbursements"
            Me.XrLabel16.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            Me.XrLabel16.WordWrap = False
            '
            'XrLabel20
            '
            Me.XrLabel20.Font = New System.Drawing.Font("Times New Roman", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.XrLabel20.LocationFloat = New DevExpress.Utils.PointFloat(8.0!, 458.0!)
            Me.XrLabel20.Name = "XrLabel20"
            Me.XrLabel20.SizeF = New System.Drawing.SizeF(245.0!, 23.0!)
            Me.XrLabel20.Text = "Ending Trust Balance"
            '
            'XrLine2
            '
            Me.XrLine2.LineWidth = 2
            Me.XrLine2.LocationFloat = New DevExpress.Utils.PointFloat(550.0!, 458.0!)
            Me.XrLine2.Name = "XrLine2"
            Me.XrLine2.SizeF = New System.Drawing.SizeF(100.0!, 23.0!)
            '
            'XrLabel28
            '
            Me.XrLabel28.CanGrow = False
            Me.XrLabel28.Font = New System.Drawing.Font("Times New Roman", 12.0!, System.Drawing.FontStyle.Bold)
            Me.XrLabel28.LocationFloat = New DevExpress.Utils.PointFloat(8.0!, 508.0!)
            Me.XrLabel28.Name = "XrLabel28"
            Me.XrLabel28.SizeF = New System.Drawing.SizeF(317.0!, 25.0!)
            Me.XrLabel28.Text = "Other Miscellaneous Items"
            Me.XrLabel28.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            Me.XrLabel28.WordWrap = False
            '
            'XrLabel_cc
            '
            Me.XrLabel_cc.CanGrow = False
            Me.XrLabel_cc.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "cd", "{0:c}")})
            Me.XrLabel_cc.LocationFloat = New DevExpress.Utils.PointFloat(217.0!, 550.0!)
            Me.XrLabel_cc.Name = "XrLabel_cc"
            Me.XrLabel_cc.SizeF = New System.Drawing.SizeF(142.0!, 17.0!)
            Me.XrLabel_cc.Text = "$0.00"
            Me.XrLabel_cc.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrLabel_cc.WordWrap = False
            '
            'XrLabel30
            '
            Me.XrLabel30.LocationFloat = New DevExpress.Utils.PointFloat(25.0!, 550.0!)
            Me.XrLabel30.Name = "XrLabel30"
            Me.XrLabel30.SizeF = New System.Drawing.SizeF(150.0!, 17.0!)
            Me.XrLabel30.Text = "Client to Client Transfers"
            '
            'XrLabel_rs
            '
            Me.XrLabel_rs.CanGrow = False
            Me.XrLabel_rs.ForeColor = System.Drawing.Color.Red
            Me.XrLabel_rs.LocationFloat = New DevExpress.Utils.PointFloat(483.0!, 417.0!)
            Me.XrLabel_rs.Name = "XrLabel_rs"
            Me.XrLabel_rs.Scripts.OnBeforePrint = "XrLabel_rs_BeforePrint"
            Me.XrLabel_rs.SizeF = New System.Drawing.SizeF(167.0!, 17.0!)
            Me.XrLabel_rs.Text = "$0.00"
            Me.XrLabel_rs.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrLabel_rs.WordWrap = False
            '
            'ReportFooter
            '
            Me.ReportFooter.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel33, Me.XrLabel31, Me.XrLabel32, Me.XrLabel34})
            Me.ReportFooter.HeightF = 116.0!
            Me.ReportFooter.Name = "ReportFooter"
            Me.ReportFooter.PrintAtBottom = True
            '
            'XrLabel33
            '
            Me.XrLabel33.ForeColor = System.Drawing.Color.Gray
            Me.XrLabel33.LocationFloat = New DevExpress.Utils.PointFloat(33.0!, 67.0!)
            Me.XrLabel33.Name = "XrLabel33"
            Me.XrLabel33.SizeF = New System.Drawing.SizeF(717.0!, 42.0!)
            Me.XrLabel33.Text = "The ""Other Checks"" field lists the other miscallenous checks, such as the check f" & _
        "or the fairshare generated during the disbursement process."
            '
            'XrLabel31
            '
            Me.XrLabel31.CanGrow = False
            Me.XrLabel31.ForeColor = System.Drawing.Color.Gray
            Me.XrLabel31.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 9.0!)
            Me.XrLabel31.Name = "XrLabel31"
            Me.XrLabel31.SizeF = New System.Drawing.SizeF(25.0!, 16.0!)
            Me.XrLabel31.Text = "*"
            Me.XrLabel31.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
            Me.XrLabel31.WordWrap = False
            '
            'XrLabel32
            '
            Me.XrLabel32.ForeColor = System.Drawing.Color.Gray
            Me.XrLabel32.LocationFloat = New DevExpress.Utils.PointFloat(33.0!, 8.0!)
            Me.XrLabel32.Name = "XrLabel32"
            Me.XrLabel32.SizeF = New System.Drawing.SizeF(717.0!, 50.0!)
            Me.XrLabel32.Text = resources.GetString("XrLabel32.Text")
            '
            'XrLabel34
            '
            Me.XrLabel34.CanGrow = False
            Me.XrLabel34.ForeColor = System.Drawing.Color.Gray
            Me.XrLabel34.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 68.0!)
            Me.XrLabel34.Name = "XrLabel34"
            Me.XrLabel34.SizeF = New System.Drawing.SizeF(25.0!, 16.0!)
            Me.XrLabel34.Text = "**"
            Me.XrLabel34.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
            Me.XrLabel34.WordWrap = False
            '
            'total_credit
            '
            Me.total_credit.Expression = "[dp]+[vd]+[rf]+[rr]+[vr]+[dm]"
            Me.total_credit.FieldType = DevExpress.XtraReports.UI.FieldType.[Decimal]
            Me.total_credit.Name = "total_credit"
            '
            'total_debit
            '
            Me.total_debit.Expression = "[ad]+[md]+[cm]+[cr]+[mf]"
            Me.total_debit.FieldType = DevExpress.XtraReports.UI.FieldType.[Decimal]
            Me.total_debit.Name = "total_debit"
            '
            'XrTable1
            '
            Me.XrTable1.LocationFloat = New DevExpress.Utils.PointFloat(33.0!, 158.0!)
            Me.XrTable1.Name = "XrTable1"
            Me.XrTable1.Rows.AddRange(New DevExpress.XtraReports.UI.XRTableRow() {Me.XrTableRow1, Me.XrTableRow2, Me.XrTableRow3, Me.XrTableRow4, Me.XrTableRow5, Me.XrTableRow6})
            Me.XrTable1.SizeF = New System.Drawing.SizeF(617.0!, 90.0!)
            '
            'XrTableRow1
            '
            Me.XrTableRow1.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.XrTableCell1, Me.XrTableCell_dp, Me.XrTableCell3})
            Me.XrTableRow1.Name = "XrTableRow1"
            Me.XrTableRow1.Weight = 0.6R
            '
            'XrTableCell1
            '
            Me.XrTableCell1.Name = "XrTableCell1"
            Me.XrTableCell1.StylePriority.UseTextAlignment = False
            Me.XrTableCell1.Text = "Deposits"
            Me.XrTableCell1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            Me.XrTableCell1.Weight = 1.0R
            '
            'XrTableCell_dp
            '
            Me.XrTableCell_dp.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "dp", "{0:c}")})
            Me.XrTableCell_dp.Name = "XrTableCell_dp"
            Me.XrTableCell_dp.StylePriority.UseTextAlignment = False
            Me.XrTableCell_dp.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrTableCell_dp.Weight = 0.58468385569666737R
            Me.XrTableCell_dp.WordWrap = False
            '
            'XrTableCell3
            '
            Me.XrTableCell3.Name = "XrTableCell3"
            Me.XrTableCell3.StylePriority.UseTextAlignment = False
            Me.XrTableCell3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrTableCell3.Weight = 1.4153161443033326R
            '
            'XrTableRow2
            '
            Me.XrTableRow2.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.XrTableCell4, Me.XrTableCell_rf, Me.XrTableCell6})
            Me.XrTableRow2.Name = "XrTableRow2"
            Me.XrTableRow2.Weight = 0.6R
            '
            'XrTableCell4
            '
            Me.XrTableCell4.Name = "XrTableCell4"
            Me.XrTableCell4.StylePriority.UseTextAlignment = False
            Me.XrTableCell4.Text = "Creditor Refunds"
            Me.XrTableCell4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            Me.XrTableCell4.Weight = 1.0R
            '
            'XrTableCell_rf
            '
            Me.XrTableCell_rf.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "rf", "{0:c}")})
            Me.XrTableCell_rf.Name = "XrTableCell_rf"
            Me.XrTableCell_rf.StylePriority.UseTextAlignment = False
            Me.XrTableCell_rf.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrTableCell_rf.Weight = 0.58468385569666737R
            Me.XrTableCell_rf.WordWrap = False
            '
            'XrTableCell6
            '
            Me.XrTableCell6.Name = "XrTableCell6"
            Me.XrTableCell6.StylePriority.UseTextAlignment = False
            Me.XrTableCell6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrTableCell6.Weight = 1.4153161443033326R
            '
            'XrTableRow3
            '
            Me.XrTableRow3.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.XrTableCell7, Me.XrTableCell_rr, Me.XrTableCell9})
            Me.XrTableRow3.Name = "XrTableRow3"
            Me.XrTableRow3.Weight = 0.6R
            '
            'XrTableCell7
            '
            Me.XrTableCell7.Name = "XrTableCell7"
            Me.XrTableCell7.StylePriority.UseTextAlignment = False
            Me.XrTableCell7.Text = "RPS Refunds"
            Me.XrTableCell7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            Me.XrTableCell7.Weight = 1.0R
            '
            'XrTableCell_rr
            '
            Me.XrTableCell_rr.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "rr", "{0:c}")})
            Me.XrTableCell_rr.Name = "XrTableCell_rr"
            Me.XrTableCell_rr.StylePriority.UseTextAlignment = False
            Me.XrTableCell_rr.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrTableCell_rr.Weight = 0.58468385569666737R
            Me.XrTableCell_rr.WordWrap = False
            '
            'XrTableCell9
            '
            Me.XrTableCell9.Name = "XrTableCell9"
            Me.XrTableCell9.StylePriority.UseTextAlignment = False
            Me.XrTableCell9.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrTableCell9.Weight = 1.4153161443033326R
            '
            'XrTableRow4
            '
            Me.XrTableRow4.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.XrTableCell10, Me.XrTableCell_vd, Me.XrTableCell12})
            Me.XrTableRow4.Name = "XrTableRow4"
            Me.XrTableRow4.Weight = 0.6R
            '
            'XrTableCell10
            '
            Me.XrTableCell10.Name = "XrTableCell10"
            Me.XrTableCell10.StylePriority.UseTextAlignment = False
            Me.XrTableCell10.Text = "Creditor Void Checks"
            Me.XrTableCell10.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            Me.XrTableCell10.Weight = 1.0R
            '
            'XrTableCell_vd
            '
            Me.XrTableCell_vd.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "vd", "{0:c}")})
            Me.XrTableCell_vd.Name = "XrTableCell_vd"
            Me.XrTableCell_vd.StylePriority.UseTextAlignment = False
            Me.XrTableCell_vd.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrTableCell_vd.Weight = 0.58468385569666737R
            Me.XrTableCell_vd.WordWrap = False
            '
            'XrTableCell12
            '
            Me.XrTableCell12.Name = "XrTableCell12"
            Me.XrTableCell12.StylePriority.UseTextAlignment = False
            Me.XrTableCell12.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrTableCell12.Weight = 1.4153161443033326R
            '
            'XrTableRow5
            '
            Me.XrTableRow5.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.XrTableCell13, Me.XrTableCell_vr, Me.XrTableCell15})
            Me.XrTableRow5.Name = "XrTableRow5"
            Me.XrTableRow5.Weight = 0.6R
            '
            'XrTableCell13
            '
            Me.XrTableCell13.Name = "XrTableCell13"
            Me.XrTableCell13.StylePriority.UseTextAlignment = False
            Me.XrTableCell13.Text = "Client Void Checks"
            Me.XrTableCell13.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            Me.XrTableCell13.Weight = 1.0R
            '
            'XrTableCell_vr
            '
            Me.XrTableCell_vr.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "vr", "{0:c}")})
            Me.XrTableCell_vr.Name = "XrTableCell_vr"
            Me.XrTableCell_vr.StylePriority.UseTextAlignment = False
            Me.XrTableCell_vr.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrTableCell_vr.Weight = 0.58468385569666737R
            Me.XrTableCell_vr.WordWrap = False
            '
            'XrTableCell15
            '
            Me.XrTableCell15.Name = "XrTableCell15"
            Me.XrTableCell15.StylePriority.UseTextAlignment = False
            Me.XrTableCell15.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrTableCell15.Weight = 1.4153161443033326R
            '
            'XrTableRow6
            '
            Me.XrTableRow6.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.XrTableCell16, Me.XrTableCell_oc, Me.XrTableCell_total_credit})
            Me.XrTableRow6.Name = "XrTableRow6"
            Me.XrTableRow6.Weight = 0.6R
            '
            'XrTableCell16
            '
            Me.XrTableCell16.Name = "XrTableCell16"
            Me.XrTableCell16.StylePriority.UseTextAlignment = False
            Me.XrTableCell16.Text = "Operating to Client Transfers"
            Me.XrTableCell16.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            Me.XrTableCell16.Weight = 1.0R
            '
            'XrTableCell_oc
            '
            Me.XrTableCell_oc.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "dm", "{0:c}")})
            Me.XrTableCell_oc.Name = "XrTableCell_oc"
            Me.XrTableCell_oc.StylePriority.UseTextAlignment = False
            Me.XrTableCell_oc.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrTableCell_oc.Weight = 0.58468385569666737R
            Me.XrTableCell_oc.WordWrap = False
            '
            'XrTableCell_total_credit
            '
            Me.XrTableCell_total_credit.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "total_credit", "{0:c}")})
            Me.XrTableCell_total_credit.Name = "XrTableCell_total_credit"
            Me.XrTableCell_total_credit.StylePriority.UseTextAlignment = False
            Me.XrTableCell_total_credit.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrTableCell_total_credit.Weight = 1.4153161443033326R
            '
            'XrTable2
            '
            Me.XrTable2.LocationFloat = New DevExpress.Utils.PointFloat(33.0!, 308.0!)
            Me.XrTable2.Name = "XrTable2"
            Me.XrTable2.Rows.AddRange(New DevExpress.XtraReports.UI.XRTableRow() {Me.XrTableRow7, Me.XrTableRow8, Me.XrTableRow9, Me.XrTableRow10, Me.XrTableRow12})
            Me.XrTable2.SizeF = New System.Drawing.SizeF(617.0!, 75.0!)
            '
            'XrTableRow7
            '
            Me.XrTableRow7.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.XrTableCell2, Me.XrTableCell_ad, Me.XrTableCell8})
            Me.XrTableRow7.Name = "XrTableRow7"
            Me.XrTableRow7.Weight = 0.6R
            '
            'XrTableCell2
            '
            Me.XrTableCell2.Name = "XrTableCell2"
            Me.XrTableCell2.StylePriority.UseTextAlignment = False
            Me.XrTableCell2.Text = "Auto Disbursements"
            Me.XrTableCell2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            Me.XrTableCell2.Weight = 1.0R
            '
            'XrTableCell_ad
            '
            Me.XrTableCell_ad.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "ad", "{0:c}")})
            Me.XrTableCell_ad.Name = "XrTableCell_ad"
            Me.XrTableCell_ad.StylePriority.UseTextAlignment = False
            Me.XrTableCell_ad.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrTableCell_ad.Weight = 0.58468385569666737R
            Me.XrTableCell_ad.WordWrap = False
            '
            'XrTableCell8
            '
            Me.XrTableCell8.Name = "XrTableCell8"
            Me.XrTableCell8.StylePriority.UseTextAlignment = False
            Me.XrTableCell8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrTableCell8.Weight = 1.4153161443033326R
            '
            'XrTableRow8
            '
            Me.XrTableRow8.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.XrTableCell11, Me.XrTableCell_md, Me.XrTableCell17})
            Me.XrTableRow8.Name = "XrTableRow8"
            Me.XrTableRow8.Weight = 0.6R
            '
            'XrTableCell11
            '
            Me.XrTableCell11.Name = "XrTableCell11"
            Me.XrTableCell11.StylePriority.UseTextAlignment = False
            Me.XrTableCell11.Text = "Manual Disbursements"
            Me.XrTableCell11.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            Me.XrTableCell11.Weight = 1.0R
            '
            'XrTableCell_md
            '
            Me.XrTableCell_md.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "md", "{0:c}")})
            Me.XrTableCell_md.Name = "XrTableCell_md"
            Me.XrTableCell_md.StylePriority.UseTextAlignment = False
            Me.XrTableCell_md.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrTableCell_md.Weight = 0.58468385569666737R
            Me.XrTableCell_md.WordWrap = False
            '
            'XrTableCell17
            '
            Me.XrTableCell17.Name = "XrTableCell17"
            Me.XrTableCell17.StylePriority.UseTextAlignment = False
            Me.XrTableCell17.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrTableCell17.Weight = 1.4153161443033326R
            '
            'XrTableRow9
            '
            Me.XrTableRow9.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.XrTableCell18, Me.XrTableCell_cm, Me.XrTableCell20})
            Me.XrTableRow9.Name = "XrTableRow9"
            Me.XrTableRow9.Weight = 0.6R
            '
            'XrTableCell18
            '
            Me.XrTableCell18.Name = "XrTableCell18"
            Me.XrTableCell18.StylePriority.UseTextAlignment = False
            Me.XrTableCell18.Text = "Other Checks (**)"
            Me.XrTableCell18.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            Me.XrTableCell18.Weight = 1.0R
            '
            'XrTableCell_cm
            '
            Me.XrTableCell_cm.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "cm", "{0:c}")})
            Me.XrTableCell_cm.Name = "XrTableCell_cm"
            Me.XrTableCell_cm.StylePriority.UseTextAlignment = False
            Me.XrTableCell_cm.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrTableCell_cm.Weight = 0.58468385569666737R
            Me.XrTableCell_cm.WordWrap = False
            '
            'XrTableCell20
            '
            Me.XrTableCell20.Name = "XrTableCell20"
            Me.XrTableCell20.StylePriority.UseTextAlignment = False
            Me.XrTableCell20.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrTableCell20.Weight = 1.4153161443033326R
            '
            'XrTableRow10
            '
            Me.XrTableRow10.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.XrTableCell21, Me.XrTableCell_cr, Me.XrTableCell23})
            Me.XrTableRow10.Name = "XrTableRow10"
            Me.XrTableRow10.Weight = 0.6R
            '
            'XrTableCell21
            '
            Me.XrTableCell21.Name = "XrTableCell21"
            Me.XrTableCell21.StylePriority.UseTextAlignment = False
            Me.XrTableCell21.Text = "Client Refunds"
            Me.XrTableCell21.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            Me.XrTableCell21.Weight = 1.0R
            '
            'XrTableCell_cr
            '
            Me.XrTableCell_cr.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "cr", "{0:c}")})
            Me.XrTableCell_cr.Name = "XrTableCell_cr"
            Me.XrTableCell_cr.StylePriority.UseTextAlignment = False
            Me.XrTableCell_cr.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrTableCell_cr.Weight = 0.58468385569666737R
            Me.XrTableCell_cr.WordWrap = False
            '
            'XrTableCell23
            '
            Me.XrTableCell23.Name = "XrTableCell23"
            Me.XrTableCell23.StylePriority.UseTextAlignment = False
            Me.XrTableCell23.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrTableCell23.Weight = 1.4153161443033326R
            '
            'XrTableRow12
            '
            Me.XrTableRow12.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.XrTableCell27, Me.XrTableCell_co, Me.XrTableCell_total_debit})
            Me.XrTableRow12.Name = "XrTableRow12"
            Me.XrTableRow12.Weight = 0.6R
            '
            'XrTableCell27
            '
            Me.XrTableCell27.Name = "XrTableCell27"
            Me.XrTableCell27.StylePriority.UseTextAlignment = False
            Me.XrTableCell27.Text = "Client to Operating Transfers"
            Me.XrTableCell27.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            Me.XrTableCell27.Weight = 1.0R
            '
            'XrTableCell_co
            '
            Me.XrTableCell_co.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "mf", "{0:c}")})
            Me.XrTableCell_co.Name = "XrTableCell_co"
            Me.XrTableCell_co.StylePriority.UseTextAlignment = False
            Me.XrTableCell_co.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrTableCell_co.Weight = 0.58468385569666737R
            Me.XrTableCell_co.WordWrap = False
            '
            'XrTableCell_total_debit
            '
            Me.XrTableCell_total_debit.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "total_debit", "{0:c}")})
            Me.XrTableCell_total_debit.ForeColor = System.Drawing.Color.Red
            Me.XrTableCell_total_debit.Name = "XrTableCell_total_debit"
            Me.XrTableCell_total_debit.StylePriority.UseForeColor = False
            Me.XrTableCell_total_debit.StylePriority.UseTextAlignment = False
            Me.XrTableCell_total_debit.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrTableCell_total_debit.Weight = 1.4153161443033326R
            '
            'TrustBalanceReport
            '
            Me.Bands.AddRange(New DevExpress.XtraReports.UI.Band() {Me.Detail, Me.PageHeader, Me.PageFooter, Me.ReportFooter, Me.TopMarginBand1, Me.BottomMarginBand1})
            Me.CalculatedFields.AddRange(New DevExpress.XtraReports.UI.CalculatedField() {Me.total_credit, Me.total_debit})
            Me.ScriptLanguage = DevExpress.XtraReports.ScriptLanguage.VisualBasic
            Me.ScriptReferencesString = resources.GetString("$this.ScriptReferencesString")
            Me.Scripts.OnBeforePrint = "TrustBalanceReport_BeforePrint"
            Me.ScriptsSource = resources.GetString("$this.ScriptsSource")
            Me.StyleSheet.AddRange(New DevExpress.XtraReports.UI.XRControlStyle() {Me.XrControlStyle_Header, Me.XrControlStyle_Totals, Me.XrControlStyle_HeaderPannel, Me.XrControlStyle_GroupHeader})
            Me.Version = "11.2"
            Me.Controls.SetChildIndex(Me.BottomMarginBand1, 0)
            Me.Controls.SetChildIndex(Me.TopMarginBand1, 0)
            Me.Controls.SetChildIndex(Me.ReportFooter, 0)
            Me.Controls.SetChildIndex(Me.PageFooter, 0)
            Me.Controls.SetChildIndex(Me.PageHeader, 0)
            Me.Controls.SetChildIndex(Me.Detail, 0)
            CType(Me.XrTable1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.XrTable2, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me, System.ComponentModel.ISupportInitialize).EndInit()

        End Sub

        Friend WithEvents XrLabel1 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLine1 As DevExpress.XtraReports.UI.XRLine
        Friend WithEvents XrLabel2 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel21 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel16 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel20 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLine2 As DevExpress.XtraReports.UI.XRLine
        Friend WithEvents XrLabel28 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_cc As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel30 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_rs As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents ReportFooter As DevExpress.XtraReports.UI.ReportFooterBand
        Friend WithEvents XrLabel34 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel32 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel31 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel33 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents total_credit As DevExpress.XtraReports.UI.CalculatedField
        Friend WithEvents total_debit As DevExpress.XtraReports.UI.CalculatedField
        Friend WithEvents XrTable1 As DevExpress.XtraReports.UI.XRTable
        Friend WithEvents XrTableRow1 As DevExpress.XtraReports.UI.XRTableRow
        Friend WithEvents XrTableCell1 As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents XrTableCell_dp As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents XrTableCell3 As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents XrTableRow2 As DevExpress.XtraReports.UI.XRTableRow
        Friend WithEvents XrTableCell4 As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents XrTableCell_rf As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents XrTableCell6 As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents XrTableRow3 As DevExpress.XtraReports.UI.XRTableRow
        Friend WithEvents XrTableCell7 As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents XrTableCell_rr As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents XrTableCell9 As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents XrTableRow4 As DevExpress.XtraReports.UI.XRTableRow
        Friend WithEvents XrTableCell10 As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents XrTableCell_vd As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents XrTableCell12 As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents XrTableRow5 As DevExpress.XtraReports.UI.XRTableRow
        Friend WithEvents XrTableCell13 As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents XrTableCell_vr As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents XrTableCell15 As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents XrTableRow6 As DevExpress.XtraReports.UI.XRTableRow
        Friend WithEvents XrTableCell16 As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents XrTableCell_oc As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents XrTableCell_total_credit As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents XrTable2 As DevExpress.XtraReports.UI.XRTable
        Friend WithEvents XrTableRow7 As DevExpress.XtraReports.UI.XRTableRow
        Friend WithEvents XrTableCell2 As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents XrTableCell_ad As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents XrTableCell8 As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents XrTableRow8 As DevExpress.XtraReports.UI.XRTableRow
        Friend WithEvents XrTableCell11 As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents XrTableCell_md As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents XrTableCell17 As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents XrTableRow9 As DevExpress.XtraReports.UI.XRTableRow
        Friend WithEvents XrTableCell18 As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents XrTableCell_cm As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents XrTableCell20 As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents XrTableRow10 As DevExpress.XtraReports.UI.XRTableRow
        Friend WithEvents XrTableCell21 As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents XrTableCell_cr As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents XrTableCell23 As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents XrTableRow12 As DevExpress.XtraReports.UI.XRTableRow
        Friend WithEvents XrTableCell27 As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents XrTableCell_co As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents XrTableCell_total_debit As DevExpress.XtraReports.UI.XRTableCell
    End Class
End Namespace
