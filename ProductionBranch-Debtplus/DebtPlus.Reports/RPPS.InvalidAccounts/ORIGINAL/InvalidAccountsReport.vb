#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}
#End Region

Namespace RPPS.InvalidAccounts
    Public Class InvalidAccountsReport

        ''' <summary>
        ''' Create an instance of our report
        ''' </summary>
        Public Sub New()
            MyBase.New()
            InitializeComponent()

            AddHandler BeforePrint, AddressOf InvalidAccountsReport_BeforePrint
            AddHandler XrLabel_cause.BeforePrint, AddressOf XrLabel_cause_BeforePrint

            ReportFilter.IsEnabled = True
        End Sub

        Public Sub New(ByVal ShowWaitingDialog As Boolean)
            MyBase.New(ShowWaitingDialog)
            InitializeComponent()

            AddHandler BeforePrint, AddressOf InvalidAccountsReport_BeforePrint
            AddHandler XrLabel_cause.BeforePrint, AddressOf XrLabel_cause_BeforePrint

            ReportFilter.IsEnabled = True
        End Sub


        ''' <summary>
        ''' Configure the report's datasource as needed
        ''' </summary>
        Private ReadOnly ds As New System.Data.DataSet("ds")

        Private Sub InvalidAccountsReport_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs)
            Dim rpt As DevExpress.XtraReports.UI.XtraReport = CType(sender, DevExpress.XtraReports.UI.XtraReport)
            Const TableName As String = "rpt_rpps_invalid_accounts_2"
            Dim tbl As System.Data.DataTable = ds.Tables(TableName)
            If tbl Is Nothing Then
                Dim sqlInfo As DebtPlus.LINQ.SQLInfoClass = DebtPlus.LINQ.SQLInfoClass.getDefault()

                '-- Retrieve the list of account numbers that we must update
                Using cmd As New System.Data.SqlClient.SqlCommand()
                    With cmd
                        .Connection = New System.Data.SqlClient.SqlConnection(sqlInfo.ConnectionString)
                        .CommandText = "rpt_rpps_invalid_accounts_2"
                        .CommandType = System.Data.CommandType.StoredProcedure
                        .CommandTimeout = 0
                    End With

                    Using da As New System.Data.SqlClient.SqlDataAdapter(cmd)
                        da.Fill(ds, TableName)
                        tbl = ds.Tables(TableName)
                    End Using
                End Using
            End If

            If tbl IsNot Nothing Then
                rpt.DataSource = New System.Data.DataView(tbl, CType(rpt, DebtPlus.Interfaces.Reports.IReportFilter).ReportFilter.ViewFilter, "creditor,client,account_number", System.Data.DataViewRowState.CurrentRows)
                For Each calc As DevExpress.XtraReports.UI.CalculatedField In rpt.CalculatedFields
                    calc.Assign(rpt.DataSource, rpt.DataMember)
                Next
            End If
        End Sub


        ''' <summary>
        ''' Change the formatting on the error condtiion column
        ''' </summary>
        Private Sub XrLabel_cause_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs)
            With CType(sender, DevExpress.XtraReports.UI.XRLabel)
                Dim rpt As DevExpress.XtraReports.UI.XtraReport = CType(.Report, DevExpress.XtraReports.UI.XtraReport)
                Dim Reason As Object = rpt.GetCurrentColumnValue("error_condition")
                If Reason Is Nothing OrElse Reason Is System.DBNull.Value Then Reason = String.Empty

                Select Case Convert.ToString(Reason)
                    Case "BILLER"
                        .ForeColor = System.Drawing.Color.Red
                        If Not .Font.Bold Then .Font = New System.Drawing.Font(.Font, System.Drawing.FontStyle.Bold)
                    Case "MASK"
                        .ForeColor = System.Drawing.Color.Blue
                        If .Font.Bold Then .Font = New System.Drawing.Font(.Font, System.Drawing.FontStyle.Regular)
                    Case Else
                        .ForeColor = System.Drawing.Color.FromKnownColor(System.Drawing.KnownColor.WindowText)
                        If .Font.Bold Then .Font = New System.Drawing.Font(.Font, System.Drawing.FontStyle.Regular)
                End Select
            End With
        End Sub


        ''' <summary>
        ''' Report title string
        ''' </summary>
        Public Overrides ReadOnly Property ReportTitle() As String
            Get
                Return "Invalid Account Numbers"
            End Get
        End Property
    End Class
End Namespace
