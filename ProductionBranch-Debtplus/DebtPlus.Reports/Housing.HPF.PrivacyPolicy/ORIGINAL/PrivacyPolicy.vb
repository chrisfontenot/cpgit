﻿Namespace Housing.HPF.PrivacyPolicy
    Public Class PrivacyPolicy  ' DebtPlus.Reports.Housing.HPF.PrivacyPolicy.PrivacyPolicy
        Inherits DebtPlus.Reports.Template.BaseXtraReportClass

        Public Sub New()
            MyBase.New()
            InitializeComponent()

            ' Add the report handler events
            'AddHandler XrRichText_NameAndAddress.BeforePrint, AddressOf XrRichText_NameAndAddress_BeforePrint
            'AddHandler BeforePrint, AddressOf PrivacyPolicy_BeforePrint
        End Sub

        ' Dataset for the report record source
        Private ds As System.Data.DataSet = New System.Data.DataSet("ds")

        Private Sub XrRichText_NameAndAddress_BeforePrint(sender As Object, e As System.Drawing.Printing.PrintEventArgs)
            Dim ctl As DevExpress.XtraReports.UI.XRRichText = DirectCast(sender, DevExpress.XtraReports.UI.XRRichText)
            Dim rpt As DevExpress.XtraReports.UI.XtraReportBase = ctl.Report
            Dim drv As System.Data.DataRowView = DirectCast(rpt.GetCurrentRow(), System.Data.DataRowView)

            ' Find the fields for the name. The name is the first line in the field. 
            Dim nameAndAddress As System.String = DebtPlus.Utils.Format.Strings.toHTML(DebtPlus.Utils.Nulls.DStr(drv("address")), False)
            If nameAndAddress.Length > 0 Then

                ' Remove the leading and ending paragraph markers
                nameAndAddress = nameAndAddress.Replace("</p>", "").Replace("<p>", "")

                ' Emphasize the first line in the buffer. This is the name field.
                Dim iPos As Int32 = nameAndAddress.IndexOf("<br/>")
                If iPos >= 0 Then
                    nameAndAddress = "<b>" + nameAndAddress.Insert(iPos, "</b>")
                End If

                ' Paste the converted HTML text into the name/address block.
                ctl.Html = "<p>" + nameAndAddress + "</p>"
            Else
                ctl.Text = String.Empty
            End If
        End Sub

        Private Sub PrivacyPolicy_BeforePrint(sender As Object, e As System.Drawing.Printing.PrintEventArgs)
            Dim rpt As DevExpress.XtraReports.UI.XtraReport = DirectCast(sender, DevExpress.XtraReports.UI.XtraReport)
            Dim tableName As String = "rpt_HPF_PrivacyPolicy"

            Dim tbl As System.Data.DataTable = ds.Tables(tableName)
            If tbl Is Nothing Then
                Try
                    Using cn As New System.Data.SqlClient.SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
                        Using cmd As New System.Data.SqlClient.SqlCommand()
                            cmd.Connection = cn
                            cmd.CommandText = tableName
                            cmd.CommandType = System.Data.CommandType.StoredProcedure

                            ' DEBUGGING ONLY -- Print the entire list. Don't bother with the fact that it was previously printed.
                            ' cmd.Parameters.Add("@from_date", SqlDbType.DateTime).Value = New System.DateTime(1900, 1, 1)

                            Using da As New System.Data.SqlClient.SqlDataAdapter(cmd)
                                da.Fill(ds, tableName)
                                tbl = ds.Tables(tableName)
                            End Using
                        End Using
                    End Using

                Catch ex As System.Data.SqlClient.SqlException
                    DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error reading report information")
                End Try
            End If

            If tbl IsNot Nothing Then
                rpt.DataSource = New System.Data.DataView(tbl, String.Empty, "client", System.Data.DataViewRowState.CurrentRows)
            End If
        End Sub
    End Class
End Namespace
