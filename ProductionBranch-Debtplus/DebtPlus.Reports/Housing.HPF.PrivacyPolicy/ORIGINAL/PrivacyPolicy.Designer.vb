﻿Namespace Housing.HPF.PrivacyPolicy
    <Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
    Partial Public Class PrivacyPolicy

        'XtraReport overrides dispose to clean up the component list.
        <System.Diagnostics.DebuggerNonUserCode()> _
        Protected Overrides Sub Dispose(ByVal disposing As Boolean)
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
            MyBase.Dispose(disposing)
        End Sub

        'Required by the Designer
        Private components As System.ComponentModel.IContainer = Nothing

        'NOTE: The following procedure is required by the Designer
        'It can be modified using the Designer.  
        'Do not modify it using the code editor.
        <System.Diagnostics.DebuggerStepThrough()> _
        Private Sub InitializeComponent()
            Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(PrivacyPolicy))
            Me.XrSubreport_enUS = New DevExpress.XtraReports.UI.XRSubreport()
            Me.HpF_PrivacyPolicy1 = New DebtPlus.Reports.Housing.HPF.PrivacyPolicy.HPF_PrivacyPolicy_enUS()
            Me.GroupHeader_CoverPage = New DevExpress.XtraReports.UI.GroupHeaderBand()
            Me.XrRichText_NameAndAddress = New DevExpress.XtraReports.UI.XRRichText()
            Me.XrLabel_CoverPageOnly = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrPictureBox1 = New DevExpress.XtraReports.UI.XRPictureBox()
            Me.GroupHeader_BlankPage = New DevExpress.XtraReports.UI.GroupHeaderBand()
            Me.XrLabel1 = New DevExpress.XtraReports.UI.XRLabel()
            CType(Me.HpF_PrivacyPolicy1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.XrRichText_NameAndAddress, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
            '
            'Detail
            '
            Me.Detail.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrSubreport_enUS})
            Me.Detail.HeightF = 23.0!
            Me.Detail.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
            Me.Detail.PageBreak = DevExpress.XtraReports.UI.PageBreak.BeforeBand
            Me.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
            '
            'XrSubreport_enUS
            '
            Me.XrSubreport_enUS.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
            Me.XrSubreport_enUS.Name = "XrSubreport_enUS"
            Me.XrSubreport_enUS.ReportSource = Me.HpF_PrivacyPolicy1
            Me.XrSubreport_enUS.SizeF = New System.Drawing.SizeF(799.0!, 23.0!)
            '
            'GroupHeader_CoverPage
            '
            Me.GroupHeader_CoverPage.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrRichText_NameAndAddress, Me.XrLabel_CoverPageOnly, Me.XrPictureBox1})
            Me.GroupHeader_CoverPage.GroupFields.AddRange(New DevExpress.XtraReports.UI.GroupField() {New DevExpress.XtraReports.UI.GroupField("client", DevExpress.XtraReports.UI.XRColumnSortOrder.Ascending)})
            Me.GroupHeader_CoverPage.HeightF = 802.4999!
            Me.GroupHeader_CoverPage.Level = 1
            Me.GroupHeader_CoverPage.Name = "GroupHeader_CoverPage"
            Me.GroupHeader_CoverPage.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
            Me.GroupHeader_CoverPage.PageBreak = DevExpress.XtraReports.UI.PageBreak.BeforeBand
            Me.GroupHeader_CoverPage.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
            '
            'XrRichText_NameAndAddress
            '
            Me.XrRichText_NameAndAddress.LocationFloat = New DevExpress.Utils.PointFloat(35.42!, 171.88!)
            Me.XrRichText_NameAndAddress.Name = "XrRichText_NameAndAddress"
            Me.XrRichText_NameAndAddress.Scripts.OnBeforePrint = "XrRichText_NameAndAddress_BeforePrint"
            Me.XrRichText_NameAndAddress.SerializableRtfString = resources.GetString("XrRichText_NameAndAddress.SerializableRtfString")
            Me.XrRichText_NameAndAddress.SizeF = New System.Drawing.SizeF(401.5625!, 124.125!)
            '
            'XrLabel_CoverPageOnly
            '
            Me.XrLabel_CoverPageOnly.Font = New System.Drawing.Font("Arial", 9.75!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Underline), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.XrLabel_CoverPageOnly.LocationFloat = New DevExpress.Utils.PointFloat(287.5!, 387.5!)
            Me.XrLabel_CoverPageOnly.Name = "XrLabel_CoverPageOnly"
            Me.XrLabel_CoverPageOnly.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_CoverPageOnly.SizeF = New System.Drawing.SizeF(169.7917!, 41.75!)
            Me.XrLabel_CoverPageOnly.StylePriority.UseFont = False
            Me.XrLabel_CoverPageOnly.StylePriority.UseTextAlignment = False
            Me.XrLabel_CoverPageOnly.Text = "Cover Page Only"
            Me.XrLabel_CoverPageOnly.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
            '
            'XrPictureBox1
            '
            Me.XrPictureBox1.Image = CType(resources.GetObject("XrPictureBox1.Image"), System.Drawing.Image)
            Me.XrPictureBox1.LocationFloat = New DevExpress.Utils.PointFloat(250.0!, 600.0!)
            Me.XrPictureBox1.Name = "XrPictureBox1"
            Me.XrPictureBox1.SizeF = New System.Drawing.SizeF(263.5417!, 154.25!)
            Me.XrPictureBox1.Sizing = DevExpress.XtraPrinting.ImageSizeMode.StretchImage
            '
            'GroupHeader_BlankPage
            '
            Me.GroupHeader_BlankPage.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel1})
            Me.GroupHeader_BlankPage.GroupFields.AddRange(New DevExpress.XtraReports.UI.GroupField() {New DevExpress.XtraReports.UI.GroupField("client", DevExpress.XtraReports.UI.XRColumnSortOrder.Ascending)})
            Me.GroupHeader_BlankPage.HeightF = 23.0!
            Me.GroupHeader_BlankPage.Name = "GroupHeader_BlankPage"
            Me.GroupHeader_BlankPage.PageBreak = DevExpress.XtraReports.UI.PageBreak.BeforeBand
            '
            'XrLabel1
            '
            Me.XrLabel1.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
            Me.XrLabel1.Name = "XrLabel1"
            Me.XrLabel1.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel1.SizeF = New System.Drawing.SizeF(100.0!, 23.0!)
            '
            'PrivacyPolicy
            '
            Me.Bands.AddRange(New DevExpress.XtraReports.UI.Band() {Me.Detail, Me.BottomMarginBand1, Me.TopMarginBand1, Me.GroupHeader_CoverPage, Me.GroupHeader_BlankPage})
            Me.ScriptLanguage = DevExpress.XtraReports.ScriptLanguage.VisualBasic
            Me.Scripts.OnBeforePrint = "PrivacyPolicy_BeforePrint"
            Me.ScriptsSource = resources.GetString("$this.ScriptsSource")
            Me.Version = "11.2"
            Me.Controls.SetChildIndex(Me.GroupHeader_BlankPage, 0)
            Me.Controls.SetChildIndex(Me.GroupHeader_CoverPage, 0)
            Me.Controls.SetChildIndex(Me.TopMarginBand1, 0)
            Me.Controls.SetChildIndex(Me.BottomMarginBand1, 0)
            Me.Controls.SetChildIndex(Me.Detail, 0)
            CType(Me.HpF_PrivacyPolicy1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.XrRichText_NameAndAddress, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me, System.ComponentModel.ISupportInitialize).EndInit()

        End Sub
        Friend WithEvents XrSubreport_enUS As DevExpress.XtraReports.UI.XRSubreport
        Friend WithEvents GroupHeader_CoverPage As DevExpress.XtraReports.UI.GroupHeaderBand
        Friend WithEvents XrPictureBox1 As DevExpress.XtraReports.UI.XRPictureBox
        Friend WithEvents XrLabel_CoverPageOnly As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrRichText_NameAndAddress As DevExpress.XtraReports.UI.XRRichText
        Private WithEvents HpF_PrivacyPolicy1 As DebtPlus.Reports.Housing.HPF.PrivacyPolicy.HPF_PrivacyPolicy_enUS
        Friend WithEvents GroupHeader_BlankPage As DevExpress.XtraReports.UI.GroupHeaderBand
        Friend WithEvents XrLabel1 As DevExpress.XtraReports.UI.XRLabel
    End Class
End Namespace
