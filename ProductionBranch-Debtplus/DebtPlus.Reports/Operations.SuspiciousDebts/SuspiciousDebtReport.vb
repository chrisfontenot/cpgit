#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}

#End Region

Imports System.ComponentModel
Imports DebtPlus.Reports.Template

Imports System.Data.SqlClient
Imports System.Drawing.Printing
Imports DevExpress.XtraReports.UI

Namespace Operations.SuspiciousDebts
    Public Class SuspiciousDebtReport
        Inherits TemplateXtraReportClass


        ''' <summary>
        ''' Return the report title information
        ''' </summary>
        Public Overrides ReadOnly Property ReportTitle() As String
            Get
                Return "Suspicious Debts"
            End Get
        End Property


        ''' <summary>
        ''' Return the report subtitle information
        ''' </summary>
        Public Overrides ReadOnly Property ReportSubtitle() As String
            Get
                Return "Report shows active clients with debts that do not cover the interest charges " & Environment.NewLine & "and therefore could never be paid off."
            End Get
        End Property

        ''' <summary>
        ''' Create an instance of our report
        ''' </summary>
        Public Sub New()
            MyBase.New()
            InitializeComponent()
            AddHandler XRLabel_GroupHeader_ClientName.BeforePrint, AddressOf XRLabel_GroupHeader_ClientName_BeforePrint
            AddHandler XrLabel_DisbFactor.BeforePrint, AddressOf XrLabel_DisbFactor_BeforePrint
            AddHandler XrLabel_Creditor.BeforePrint, AddressOf XrLabel_Creditor_BeforePrint
            AddHandler XrLabel_Rate.BeforePrint, AddressOf XrLabel_Rate_BeforePrint
            AddHandler Me.BeforePrint, AddressOf SuspiciousDebtReport_RefreshDataReport
        End Sub

#Region " Component Designer generated code "

        'Component overrides dispose to clean up the component list.
        Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
            If disposing Then
                If components IsNot Nothing Then
                    components.Dispose()
                End If
            End If
            MyBase.Dispose(disposing)
        End Sub

        'Required by the Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Designer
        'It can be modified using the Designer.
        'Do not modify it using the code editor.
        Friend WithEvents GroupHeader1 As DevExpress.XtraReports.UI.GroupHeaderBand
        Friend WithEvents XrPanel1 As DevExpress.XtraReports.UI.XRPanel
        Friend WithEvents XrLabel1 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents CREDITOR As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel2 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel4 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel5 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel6 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XRLabel_GroupHeader_ClientName As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_Creditor As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_DisbFactor As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_Balance As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_Rate As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_Interest As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_Shortage As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents GroupFooter1 As DevExpress.XtraReports.UI.GroupFooterBand
        Friend WithEvents XrLabel_GroupTotal_Shortage As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel7 As DevExpress.XtraReports.UI.XRLabel

        <System.Diagnostics.DebuggerStepThrough()>
        Private Sub InitializeComponent()
            Me.GroupHeader1 = New DevExpress.XtraReports.UI.GroupHeaderBand
            Me.XrPanel1 = New DevExpress.XtraReports.UI.XRPanel
            Me.XrLabel1 = New DevExpress.XtraReports.UI.XRLabel
            Me.CREDITOR = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel2 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel4 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel5 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel6 = New DevExpress.XtraReports.UI.XRLabel
            Me.XRLabel_GroupHeader_ClientName = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_Creditor = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_DisbFactor = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_Balance = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_Rate = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_Interest = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_Shortage = New DevExpress.XtraReports.UI.XRLabel
            Me.GroupFooter1 = New DevExpress.XtraReports.UI.GroupFooterBand
            Me.XrLabel_GroupTotal_Shortage = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel7 = New DevExpress.XtraReports.UI.XRLabel
            CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
            '
            'XrLine_Title
            '
            Me.XrLine_Title.ParentStyleUsing.UseForeColor = False
            '
            'XrLabel_Agency_Address3
            '
            Me.XrLabel_Agency_Address3.ParentStyleUsing.UseFont = False
            Me.XrLabel_Agency_Address3.ParentStyleUsing.UseForeColor = False
            '
            'XRLabel_Agency_Name
            '
            Me.XRLabel_Agency_Name.ParentStyleUsing.UseFont = False
            Me.XRLabel_Agency_Name.ParentStyleUsing.UseForeColor = False
            '
            'XrLabel_Agency_Address1
            '
            Me.XrLabel_Agency_Address1.ParentStyleUsing.UseFont = False
            Me.XrLabel_Agency_Address1.ParentStyleUsing.UseForeColor = False
            '
            'XrLabel_Agency_Phone
            '
            Me.XrLabel_Agency_Phone.ParentStyleUsing.UseFont = False
            Me.XrLabel_Agency_Phone.ParentStyleUsing.UseForeColor = False
            '
            'XrLabel_Agency_Address2
            '
            Me.XrLabel_Agency_Address2.ParentStyleUsing.UseFont = False
            Me.XrLabel_Agency_Address2.ParentStyleUsing.UseForeColor = False
            '
            'XrPageInfo1
            '
            Me.XrPageInfo1.ParentStyleUsing.UseFont = False
            Me.XrPageInfo1.ParentStyleUsing.UseForeColor = False
            '
            'XrPageInfo_PageNumber
            '
            Me.XrPageInfo_PageNumber.ParentStyleUsing.UseFont = False
            Me.XrPageInfo_PageNumber.ParentStyleUsing.UseForeColor = False
            '
            'Detail
            '
            Me.Detail.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel_Shortage, Me.XrLabel_Interest, Me.XrLabel_Rate, Me.XrLabel_Balance, Me.XrLabel_DisbFactor, Me.XrLabel_Creditor})
            Me.Detail.Height = 15
            '
            'XrLabel_Subtitle
            '
            Me.XrLabel_Subtitle.ParentStyleUsing.UseFont = False
            Me.XrLabel_Subtitle.ParentStyleUsing.UseForeColor = False
            '
            'PageHeader
            '
            Me.PageHeader.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrPanel1})
            Me.PageHeader.Height = 141
            '
            'XrLabel_Title
            '
            Me.XrLabel_Title.ParentStyleUsing.UseFont = False
            Me.XrLabel_Title.ParentStyleUsing.UseForeColor = False
            '
            'GroupHeader1
            '
            Me.GroupHeader1.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XRLabel_GroupHeader_ClientName})
            Me.GroupHeader1.GroupUnion = DevExpress.XtraReports.UI.GroupUnion.WithFirstDetail
            Me.GroupHeader1.Height = 32
            Me.GroupHeader1.Name = "GroupHeader1"
            Me.GroupHeader1.RepeatEveryPage = True
            '
            'XrPanel1
            '
            Me.XrPanel1.BackColor = System.Drawing.Color.Teal
            Me.XrPanel1.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel6, Me.XrLabel5, Me.XrLabel4, Me.XrLabel2, Me.CREDITOR, Me.XrLabel1})
            Me.XrPanel1.Location = New System.Drawing.Point(0, 117)
            Me.XrPanel1.Name = "XrPanel1"
            Me.XrPanel1.ParentStyleUsing.UseBackColor = False
            Me.XrPanel1.Size = New System.Drawing.Size(800, 17)
            '
            'XrLabel1
            '
            Me.XrLabel1.CanGrow = False
            Me.XrLabel1.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel1.ForeColor = System.Drawing.Color.White
            Me.XrLabel1.Location = New System.Drawing.Point(425, 1)
            Me.XrLabel1.Name = "XrLabel1"
            Me.XrLabel1.ParentStyleUsing.UseFont = False
            Me.XrLabel1.ParentStyleUsing.UseForeColor = False
            Me.XrLabel1.Size = New System.Drawing.Size(41, 15)
            Me.XrLabel1.Text = "RATE"
            Me.XrLabel1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrLabel1.WordWrap = False
            '
            'CREDITOR
            '
            Me.CREDITOR.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.CREDITOR.ForeColor = System.Drawing.Color.White
            Me.CREDITOR.Location = New System.Drawing.Point(8, 1)
            Me.CREDITOR.Name = "CREDITOR"
            Me.CREDITOR.ParentStyleUsing.UseFont = False
            Me.CREDITOR.ParentStyleUsing.UseForeColor = False
            Me.CREDITOR.Size = New System.Drawing.Size(110, 15)
            Me.CREDITOR.Text = "CREDITOR"
            '
            'XrLabel2
            '
            Me.XrLabel2.CanGrow = False
            Me.XrLabel2.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel2.ForeColor = System.Drawing.Color.White
            Me.XrLabel2.Location = New System.Drawing.Point(325, 1)
            Me.XrLabel2.Name = "XrLabel2"
            Me.XrLabel2.ParentStyleUsing.UseFont = False
            Me.XrLabel2.ParentStyleUsing.UseForeColor = False
            Me.XrLabel2.Size = New System.Drawing.Size(75, 15)
            Me.XrLabel2.Text = "BALANCE"
            Me.XrLabel2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrLabel2.WordWrap = False
            '
            'XrLabel4
            '
            Me.XrLabel4.CanGrow = False
            Me.XrLabel4.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel4.ForeColor = System.Drawing.Color.White
            Me.XrLabel4.Location = New System.Drawing.Point(466, 1)
            Me.XrLabel4.Name = "XrLabel4"
            Me.XrLabel4.ParentStyleUsing.UseFont = False
            Me.XrLabel4.ParentStyleUsing.UseForeColor = False
            Me.XrLabel4.Size = New System.Drawing.Size(91, 15)
            Me.XrLabel4.Text = "INTEREST"
            Me.XrLabel4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrLabel4.WordWrap = False
            '
            'XrLabel5
            '
            Me.XrLabel5.CanGrow = False
            Me.XrLabel5.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel5.ForeColor = System.Drawing.Color.White
            Me.XrLabel5.Location = New System.Drawing.Point(558, 1)
            Me.XrLabel5.Name = "XrLabel5"
            Me.XrLabel5.ParentStyleUsing.UseFont = False
            Me.XrLabel5.ParentStyleUsing.UseForeColor = False
            Me.XrLabel5.Size = New System.Drawing.Size(102, 15)
            Me.XrLabel5.Text = "DISB FACTOR"
            Me.XrLabel5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrLabel5.WordWrap = False
            '
            'XrLabel6
            '
            Me.XrLabel6.CanGrow = False
            Me.XrLabel6.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel6.ForeColor = System.Drawing.Color.White
            Me.XrLabel6.Location = New System.Drawing.Point(675, 1)
            Me.XrLabel6.Name = "XrLabel6"
            Me.XrLabel6.ParentStyleUsing.UseFont = False
            Me.XrLabel6.ParentStyleUsing.UseForeColor = False
            Me.XrLabel6.Size = New System.Drawing.Size(100, 15)
            Me.XrLabel6.Text = "SHORTAGE"
            Me.XrLabel6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrLabel6.WordWrap = False
            '
            'XRLabel_GroupHeader_ClientName
            '
            Me.XRLabel_GroupHeader_ClientName.CanGrow = False
            Me.XRLabel_GroupHeader_ClientName.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.XRLabel_GroupHeader_ClientName.ForeColor = System.Drawing.Color.Maroon
            Me.XRLabel_GroupHeader_ClientName.Location = New System.Drawing.Point(8, 0)
            Me.XRLabel_GroupHeader_ClientName.Name = "XRLabel_GroupHeader_ClientName"
            Me.XRLabel_GroupHeader_ClientName.ParentStyleUsing.UseFont = False
            Me.XRLabel_GroupHeader_ClientName.ParentStyleUsing.UseForeColor = False
            Me.XRLabel_GroupHeader_ClientName.Size = New System.Drawing.Size(792, 23)
            Me.XRLabel_GroupHeader_ClientName.Tag = "Client: {0} {1}"
            Me.XRLabel_GroupHeader_ClientName.Text = "Client: ID Name"
            Me.XRLabel_GroupHeader_ClientName.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            Me.XRLabel_GroupHeader_ClientName.WordWrap = False
            '
            'XrLabel_Creditor
            '
            Me.XrLabel_Creditor.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.XrLabel_Creditor.Location = New System.Drawing.Point(8, 0)
            Me.XrLabel_Creditor.Name = "XrLabel_Creditor"
            Me.XrLabel_Creditor.ParentStyleUsing.UseFont = False
            Me.XrLabel_Creditor.ParentStyleUsing.UseForeColor = False
            Me.XrLabel_Creditor.Size = New System.Drawing.Size(309, 15)
            '
            'XrLabel_DisbFactor
            '
            Me.XrLabel_DisbFactor.CanGrow = False
            Me.XrLabel_DisbFactor.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.XrLabel_DisbFactor.Location = New System.Drawing.Point(558, 0)
            Me.XrLabel_DisbFactor.Name = "XrLabel_DisbFactor"
            Me.XrLabel_DisbFactor.ParentStyleUsing.UseFont = False
            Me.XrLabel_DisbFactor.ParentStyleUsing.UseForeColor = False
            Me.XrLabel_DisbFactor.Size = New System.Drawing.Size(102, 15)
            Me.XrLabel_DisbFactor.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrLabel_DisbFactor.WordWrap = False
            '
            'XrLabel_Balance
            '
            Me.XrLabel_Balance.CanGrow = False
            Me.XrLabel_Balance.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.XrLabel_Balance.Location = New System.Drawing.Point(325, 0)
            Me.XrLabel_Balance.Name = "XrLabel_Balance"
            Me.XrLabel_Balance.ParentStyleUsing.UseFont = False
            Me.XrLabel_Balance.ParentStyleUsing.UseForeColor = False
            Me.XrLabel_Balance.Size = New System.Drawing.Size(75, 15)
            Me.XrLabel_Balance.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrLabel_Balance.WordWrap = False
            '
            'XrLabel_Rate
            '
            Me.XrLabel_Rate.CanGrow = False
            Me.XrLabel_Rate.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.XrLabel_Rate.Location = New System.Drawing.Point(425, 0)
            Me.XrLabel_Rate.Name = "XrLabel_Rate"
            Me.XrLabel_Rate.ParentStyleUsing.UseFont = False
            Me.XrLabel_Rate.ParentStyleUsing.UseForeColor = False
            Me.XrLabel_Rate.Size = New System.Drawing.Size(41, 15)
            Me.XrLabel_Rate.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrLabel_Rate.WordWrap = False
            '
            'XrLabel_Interest
            '
            Me.XrLabel_Interest.CanGrow = False
            Me.XrLabel_Interest.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.XrLabel_Interest.Location = New System.Drawing.Point(466, 0)
            Me.XrLabel_Interest.Name = "XrLabel_Interest"
            Me.XrLabel_Interest.ParentStyleUsing.UseFont = False
            Me.XrLabel_Interest.ParentStyleUsing.UseForeColor = False
            Me.XrLabel_Interest.Size = New System.Drawing.Size(91, 15)
            Me.XrLabel_Interest.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrLabel_Interest.WordWrap = False
            '
            'XrLabel_Shortage
            '
            Me.XrLabel_Shortage.CanGrow = False
            Me.XrLabel_Shortage.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.XrLabel_Shortage.Location = New System.Drawing.Point(675, 0)
            Me.XrLabel_Shortage.Name = "XrLabel_Shortage"
            Me.XrLabel_Shortage.ParentStyleUsing.UseFont = False
            Me.XrLabel_Shortage.ParentStyleUsing.UseForeColor = False
            Me.XrLabel_Shortage.Size = New System.Drawing.Size(100, 15)
            Me.XrLabel_Shortage.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrLabel_Shortage.WordWrap = False
            '
            'GroupFooter1
            '
            Me.GroupFooter1.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel7, Me.XrLabel_GroupTotal_Shortage})
            Me.GroupFooter1.Height = 31
            Me.GroupFooter1.Name = "GroupFooter1"
            '
            'XrLabel_GroupTotal_Shortage
            '
            Me.XrLabel_GroupTotal_Shortage.CanGrow = False
            Me.XrLabel_GroupTotal_Shortage.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.XrLabel_GroupTotal_Shortage.Location = New System.Drawing.Point(675, 8)
            Me.XrLabel_GroupTotal_Shortage.Name = "XrLabel_GroupTotal_Shortage"
            Me.XrLabel_GroupTotal_Shortage.ParentStyleUsing.UseFont = False
            Me.XrLabel_GroupTotal_Shortage.ParentStyleUsing.UseForeColor = False
            Me.XrLabel_GroupTotal_Shortage.Size = New System.Drawing.Size(100, 15)
            Me.XrLabel_GroupTotal_Shortage.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrLabel_GroupTotal_Shortage.WordWrap = False
            '
            'XrLabel7
            '
            Me.XrLabel7.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.XrLabel7.Location = New System.Drawing.Point(608, 8)
            Me.XrLabel7.Name = "XrLabel7"
            Me.XrLabel7.ParentStyleUsing.UseFont = False
            Me.XrLabel7.ParentStyleUsing.UseForeColor = False
            Me.XrLabel7.Size = New System.Drawing.Size(59, 15)
            Me.XrLabel7.Text = "TOTAL"
            Me.XrLabel7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'SuspiciousDebtReport
            '
            Me.Bands.AddRange(New DevExpress.XtraReports.UI.Band() {Me.GroupHeader1, Me.GroupFooter1})
            CType(Me, System.ComponentModel.ISupportInitialize).EndInit()
        End Sub

#End Region


        ''' <summary>
        ''' Refresh the dataset with the new information
        ''' </summary>
        Private Sub SuspiciousDebtReport_RefreshDataReport(ByVal sender As Object, ByVal e As EventArgs)

            ' Read the data from the datasource
            Dim ds As New DataSet("suspicious_debts")
            Using cmd As SqlCommand = SelectCommand()
                Using da As New SqlDataAdapter(cmd)
                    da.Fill(ds, "suspicious_debts")
                End Using
            End Using

            ' Add the shortage column to the table as a caluclation of the differences
            Dim tbl As DataTable = ds.Tables(0)
            tbl.Columns.Add(New DataColumn("shortage", GetType(Decimal), "interest_burden-disbursement_factor"))

            ' Bind the datasource to the report
            Dim vue As DataView = tbl.DefaultView
            DataSource = vue

            ' Bind the fields to the data
            XrLabel_Balance.DataBindings.Add("Text", vue, "balance", "{0:c}")
            XrLabel_DisbFactor.DataBindings.Add("Text", vue, "disbursement_factor", "{0:c}")
            XrLabel_Rate.DataBindings.Add("Text", vue, "dmp_interest_rate", "{0:####.00#}%")
            XrLabel_Interest.DataBindings.Add("Text", vue, "interest_burden", "{0:c}")
            XrLabel_Shortage.DataBindings.Add("Text", vue, "shortage", "{0:c}")

            ' Define the summary information for the total
            XrLabel_GroupTotal_Shortage.DataBindings.Add("Text", vue, "shortage", "{0:c}")
            Dim xrXummary1 As New XRSummary
            With xrXummary1
                .FormatString = "{0:c2}"
                .Func = SummaryFunc.Sum
                .IgnoreNullValues = True
                .Running = SummaryRunning.Group
                XrLabel_GroupTotal_Shortage.Summary = xrXummary1
            End With

            ' Group the information by the client ID
            GroupHeader1.GroupFields.Clear()
            GroupHeader1.GroupFields.Add(New GroupField("client"))
        End Sub


        ''' <summary>
        ''' Return the command to select the data set
        ''' </summary>
        Private Function SelectCommand() As SqlCommand
            Dim cmd As SqlCommand = New System.Data.SqlClient.SqlCommand()
            With cmd
                .Connection = New System.Data.SqlClient.SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
                .CommandText = "rpt_impossible_debts"
                .CommandType = CommandType.StoredProcedure
                .CommandTimeout = 0
            End With
            Return cmd
        End Function


        ''' <summary>
        ''' Do a bit of highlighting on the interest rate
        ''' </summary>
        Private Sub XrLabel_Rate_BeforePrint(ByVal sender As Object, ByVal e As PrintEventArgs)

            Dim rate As Double = Convert.ToDouble(GetCurrentColumnValue("dmp_interest_rate"))
            If rate < 1.0 Then rate *= 100.0

            With XrLabel_Rate
                If rate > 45.0 Then
                    .BackColor = Color.Yellow
                Else
                    .BackColor = XrLabel_Creditor.BackColor
                End If
            End With
        End Sub


        ''' <summary>
        ''' Format the creditor ID and Name
        ''' </summary>
        Private Sub XrLabel_Creditor_BeforePrint(ByVal sender As Object, ByVal e As PrintEventArgs)
            Dim CreditorID As String = String.Empty
            Dim CreditorName As String = String.Empty

            If GetCurrentColumnValue("creditor") IsNot Nothing Then CreditorID = Convert.ToString(GetCurrentColumnValue("creditor"))
            If GetCurrentColumnValue("creditor_name") IsNot Nothing Then CreditorName = Convert.ToString(GetCurrentColumnValue("creditor_name"))
            If CreditorID <> String.Empty AndAlso CreditorName <> String.Empty Then
                CreditorID = CreditorID & " " & CreditorName
            Else
                CreditorID = CreditorID & CreditorName
            End If

            With XrLabel_Creditor
                .Text = CreditorID
            End With
        End Sub


        ''' <summary>
        ''' Do a bit of highlighting on the disbursement factor
        ''' </summary>
        Private Sub XrLabel_DisbFactor_BeforePrint(ByVal sender As Object, ByVal e As PrintEventArgs)
            Dim disb_factor As Decimal = 0D
            If GetCurrentColumnValue("disbursement_factor") IsNot Nothing Then disb_factor = Convert.ToDecimal(GetCurrentColumnValue("disbursement_factor"))

            With XrLabel_DisbFactor
                If disb_factor < 10D Then
                    .BackColor = Color.Yellow
                Else
                    .BackColor = XrLabel_Creditor.BackColor
                End If
            End With
        End Sub

        Private Sub XRLabel_GroupHeader_ClientName_BeforePrint(ByVal sender As Object, ByVal e As PrintEventArgs)
            Dim client As Int32 = 0
            Dim clientid As String = String.Empty
            Dim client_name As String = String.Empty

            If GetCurrentColumnValue("client") IsNot Nothing Then client = Convert.ToInt32(Me.GetCurrentColumnValue("client"))
            If GetCurrentColumnValue("client_name") IsNot Nothing Then client_name = Convert.ToString(Me.GetCurrentColumnValue("client_name"))

            clientid = String.Format("{0:0000000}", client)
            If clientid <> String.Empty AndAlso client_name <> String.Empty Then
                clientid = clientid & " " & client_name
            Else
                clientid = clientid & client_name
            End If

            With XRLabel_GroupHeader_ClientName
                .Text = clientid
            End With
        End Sub
    End Class
End Namespace
