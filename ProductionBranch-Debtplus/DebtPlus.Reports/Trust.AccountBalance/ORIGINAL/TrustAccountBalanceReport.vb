#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}
#End Region

Option Compare Binary
Option Explicit On 
Option Strict On

Namespace Trust.AccountBalance
    Public Class TrustAccountBalanceReport

        ''' <summary>
        ''' Create an instance of our report
        ''' </summary>
        Public Sub New()
            MyBase.New()
            InitializeComponent()

            '-- Indicate that we can filter this report.
            CType(Me, DebtPlus.Interfaces.Reports.IReportFilter).ReportFilter.IsEnabled = True

            AddHandler BeforePrint, AddressOf Report_BeforePrint

            '-- Ensure that the parameters are empty
            ParameterState.Value = String.Empty
        End Sub


        ''' <summary>
        ''' Report Title string
        ''' </summary>
        Public Overrides ReadOnly Property ReportTitle() As String
            Get
                Return "Trust Balance"
            End Get
        End Property


        ''' <summary>
        ''' Include the selection criteria where needed
        ''' </summary>
        Public Overrides ReadOnly Property ReportSubTitle As String
            Get
                If Convert.ToString(ParameterState.Value) <> String.Empty Then
                    Return String.Format("This report is for the state of {0}", ParameterState.Value)
                End If

                Return MyBase.ReportSubTitle
            End Get
        End Property

        Dim ds As New System.Data.DataSet("ds")

        ''' <summary>
        ''' Bind the dataset to the report
        ''' </summary>
        Private Sub TrustAccountBalanceReport_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs)
            Const TableName As String = "view_held_in_trust"
            Dim rpt As DevExpress.XtraReports.UI.XtraReport = CType(sender, DevExpress.XtraReports.UI.XtraReport)
            Dim sqlInfo As DebtPlus.LINQ.SQLInfoClass = DebtPlus.LINQ.SQLInfoClass.getDefault()
            Dim tbl As System.Data.DataTable = ds.Tables(TableName)

            If tbl Is Nothing Then
                Using cn As New System.Data.SqlClient.SqlConnection(sqlInfo.ConnectionString)
                    cn.Open()
                    Using cmd As New System.Data.SqlClient.SqlCommand()
                        cmd.Connection = cn
                        cmd.CommandText = "SELECT * FROM view_held_in_trust WITH (NOLOCK) WHERE ([held_in_trust] <> 0)"

                        '-- Limit the values to the state if desired
                        Dim State As String
                        If rpt.Parameters("ParameterState").Value IsNot Nothing AndAlso rpt.Parameters("ParameterState").Value IsNot System.DBNull.Value Then
                            State = Convert.ToString(rpt.Parameters("ParameterState").Value)
                            If State <> String.Empty Then
                                cmd.CommandText += " AND ([state]=@state)"
                                cmd.Parameters.Add("@state", System.Data.SqlDbType.VarChar, 10).Value = rpt.Parameters("ParameterState").Value
                            End If
                        End If

                        cmd.CommandType = System.Data.CommandType.Text
                        cmd.CommandTimeout = 0

                        Using da As New System.Data.SqlClient.SqlDataAdapter(cmd)
                            da.Fill(ds, TableName)

                            tbl = ds.Tables(TableName)
                            tbl.Columns.Add("AmountHold", GetType(System.Decimal), "iif([group]=1,[held_in_trust],null)")
                            tbl.Columns.Add("AmountSmall", GetType(System.Decimal), "iif([group]=2,[held_in_trust],null)")
                            tbl.Columns.Add("AmountLarge", GetType(System.Decimal), "iif([group]=3,[held_in_trust],null)")
                            tbl.Columns.Add("AmountNegative", GetType(System.Decimal), "iif([group]=4,[held_in_trust],null)")
                            tbl.Columns.Add("AmountInactive", GetType(System.Decimal), "iif([group]=5,[held_in_trust],null)")
                        End Using
                    End Using
                End Using
            End If

            '-- Generate the data source for the report
            rpt.DataSource = New System.Data.DataView(tbl, CType(rpt, DebtPlus.Interfaces.Reports.IReportFilter).ReportFilter.ViewFilter, "client", System.Data.DataViewRowState.CurrentRows)
        End Sub
    End Class
End Namespace
