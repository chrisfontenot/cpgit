﻿Namespace Trust.AccountBalance
    <Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
        Partial Class TrustAccountBalanceReport
        Inherits DebtPlus.Reports.Template.TemplateXtraReportClass

        'XtraReport overrides dispose to clean up the component list.
        <System.Diagnostics.DebuggerNonUserCode()> _
        Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
            If disposing Then
                If components IsNot Nothing Then
                    components.Dispose()
                End If
            End If
            MyBase.Dispose(disposing)
        End Sub

        'Required by the Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Designer
        'It can be modified using the Designer.
        'Do not modify it using the code editor.
        <System.Diagnostics.DebuggerStepThrough()> _
        Private Sub InitializeComponent()
            Dim XrSummary1 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary()
            Dim XrSummary2 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary()
            Dim XrSummary3 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary()
            Dim XrSummary4 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary()
            Dim XrSummary5 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary()
            Dim XrSummary6 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary()
            Dim XrSummary7 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary()
            Dim XrSummary8 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary()
            Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(TrustAccountBalanceReport))
            Me.XrLabel1 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel2 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel3 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel4 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel5 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_ClientID = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_ClientName = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_Balance = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_Reserved = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_Hold = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_Greater500 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_Inactive = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_Cycle = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_Less500 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_Negative = New DevExpress.XtraReports.UI.XRLabel()
            Me.ReportFooter = New DevExpress.XtraReports.UI.ReportFooterBand()
            Me.XrLine1 = New DevExpress.XtraReports.UI.XRLine()
            Me.XrLabel_Total_Balance = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_Total_Hold = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_Total_Negative = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_Total_Greater500 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_Total_Less500 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_Total_Reserved = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_Total_Inactive = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_Total = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrPanel1 = New DevExpress.XtraReports.UI.XRPanel()
            Me.XrLabel31 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel30 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel29 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel28 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel27 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel26 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel25 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel24 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel14 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel15 = New DevExpress.XtraReports.UI.XRLabel()
            Me.ParameterState = New DevExpress.XtraReports.Parameters.Parameter()
            CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
            '
            'PageHeader
            '
            Me.PageHeader.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrPanel1})
            Me.PageHeader.HeightF = 150.0!
            Me.PageHeader.Controls.SetChildIndex(Me.XrPanel1, 0)
            Me.PageHeader.Controls.SetChildIndex(Me.XrPanel_AgencyAddress, 0)
            Me.PageHeader.Controls.SetChildIndex(Me.XrLabel_Title, 0)
            Me.PageHeader.Controls.SetChildIndex(Me.XrLine_Title, 0)
            Me.PageHeader.Controls.SetChildIndex(Me.XrLabel_Subtitle, 0)
            Me.PageHeader.Controls.SetChildIndex(Me.XrPageInfo1, 0)
            '
            'XrLabel_Title
            '
            Me.XrLabel_Title.StylePriority.UseFont = False
            '
            'XrPageInfo_PageNumber
            '
            Me.XrPageInfo_PageNumber.StylePriority.UseTextAlignment = False
            '
            'XRLabel_Agency_Name
            '
            Me.XRLabel_Agency_Name.StylePriority.UseTextAlignment = False
            '
            'XrLabel_Agency_Address3
            '
            Me.XrLabel_Agency_Address3.StylePriority.UseFont = False
            Me.XrLabel_Agency_Address3.StylePriority.UseTextAlignment = False
            '
            'XrLabel_Agency_Address1
            '
            Me.XrLabel_Agency_Address1.StylePriority.UseFont = False
            Me.XrLabel_Agency_Address1.StylePriority.UseTextAlignment = False
            '
            'XrLabel_Agency_Phone
            '
            Me.XrLabel_Agency_Phone.StylePriority.UseFont = False
            Me.XrLabel_Agency_Phone.StylePriority.UseTextAlignment = False
            '
            'XrLabel_Agency_Address2
            '
            Me.XrLabel_Agency_Address2.StylePriority.UseFont = False
            Me.XrLabel_Agency_Address2.StylePriority.UseTextAlignment = False
            '
            'Detail
            '
            Me.Detail.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel_Negative, Me.XrLabel_Less500, Me.XrLabel_Cycle, Me.XrLabel_Inactive, Me.XrLabel_Greater500, Me.XrLabel_Hold, Me.XrLabel_Reserved, Me.XrLabel_Balance, Me.XrLabel_ClientName, Me.XrLabel_ClientID})
            Me.Detail.HeightF = 17.0!
            '
            'XrLabel1
            '
            Me.XrLabel1.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
            Me.XrLabel1.Name = "XrLabel1"
            Me.XrLabel1.SizeF = New System.Drawing.SizeF(100.0!, 23.0!)
            '
            'XrLabel2
            '
            Me.XrLabel2.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
            Me.XrLabel2.Name = "XrLabel2"
            Me.XrLabel2.SizeF = New System.Drawing.SizeF(100.0!, 23.0!)
            '
            'XrLabel3
            '
            Me.XrLabel3.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
            Me.XrLabel3.Name = "XrLabel3"
            Me.XrLabel3.SizeF = New System.Drawing.SizeF(100.0!, 23.0!)
            '
            'XrLabel4
            '
            Me.XrLabel4.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
            Me.XrLabel4.Name = "XrLabel4"
            Me.XrLabel4.SizeF = New System.Drawing.SizeF(100.0!, 23.0!)
            '
            'XrLabel5
            '
            Me.XrLabel5.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
            Me.XrLabel5.Name = "XrLabel5"
            Me.XrLabel5.SizeF = New System.Drawing.SizeF(100.0!, 23.0!)
            '
            'XrLabel_ClientID
            '
            Me.XrLabel_ClientID.CanGrow = False
            Me.XrLabel_ClientID.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "client", "{0:0000000}")})
            Me.XrLabel_ClientID.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
            Me.XrLabel_ClientID.Name = "XrLabel_ClientID"
            Me.XrLabel_ClientID.SizeF = New System.Drawing.SizeF(74.0!, 17.0!)
            Me.XrLabel_ClientID.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrLabel_ClientID.WordWrap = False
            '
            'XrLabel_ClientName
            '
            Me.XrLabel_ClientName.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "name")})
            Me.XrLabel_ClientName.LocationFloat = New DevExpress.Utils.PointFloat(79.0!, 0.0!)
            Me.XrLabel_ClientName.Name = "XrLabel_ClientName"
            Me.XrLabel_ClientName.SizeF = New System.Drawing.SizeF(116.0!, 17.0!)
            Me.XrLabel_ClientName.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            Me.XrLabel_ClientName.WordWrap = False
            '
            'XrLabel_Balance
            '
            Me.XrLabel_Balance.CanGrow = False
            Me.XrLabel_Balance.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "held_in_trust", "{0:c}")})
            Me.XrLabel_Balance.LocationFloat = New DevExpress.Utils.PointFloat(239.0!, 0.0!)
            Me.XrLabel_Balance.Name = "XrLabel_Balance"
            Me.XrLabel_Balance.SizeF = New System.Drawing.SizeF(77.0!, 17.0!)
            Me.XrLabel_Balance.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrLabel_Balance.WordWrap = False
            '
            'XrLabel_Reserved
            '
            Me.XrLabel_Reserved.CanGrow = False
            Me.XrLabel_Reserved.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "reserved_in_trust", "{0:$0.00;$0.00; }")})
            Me.XrLabel_Reserved.LocationFloat = New DevExpress.Utils.PointFloat(322.0!, 0.0!)
            Me.XrLabel_Reserved.Name = "XrLabel_Reserved"
            Me.XrLabel_Reserved.SizeF = New System.Drawing.SizeF(73.0!, 17.0!)
            Me.XrLabel_Reserved.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrLabel_Reserved.WordWrap = False
            '
            'XrLabel_Hold
            '
            Me.XrLabel_Hold.CanGrow = False
            Me.XrLabel_Hold.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "AmountHold", "{0:c}")})
            Me.XrLabel_Hold.LocationFloat = New DevExpress.Utils.PointFloat(401.0!, 0.0!)
            Me.XrLabel_Hold.Name = "XrLabel_Hold"
            Me.XrLabel_Hold.SizeF = New System.Drawing.SizeF(73.0!, 17.0!)
            Me.XrLabel_Hold.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrLabel_Hold.WordWrap = False
            '
            'XrLabel_Greater500
            '
            Me.XrLabel_Greater500.CanGrow = False
            Me.XrLabel_Greater500.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "AmountLarge", "{0:c}")})
            Me.XrLabel_Greater500.LocationFloat = New DevExpress.Utils.PointFloat(559.0!, 0.0!)
            Me.XrLabel_Greater500.Name = "XrLabel_Greater500"
            Me.XrLabel_Greater500.SizeF = New System.Drawing.SizeF(73.0!, 17.0!)
            Me.XrLabel_Greater500.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrLabel_Greater500.WordWrap = False
            '
            'XrLabel_Inactive
            '
            Me.XrLabel_Inactive.CanGrow = False
            Me.XrLabel_Inactive.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "AmountInactive", "{0:c}")})
            Me.XrLabel_Inactive.LocationFloat = New DevExpress.Utils.PointFloat(717.0!, 0.0!)
            Me.XrLabel_Inactive.Name = "XrLabel_Inactive"
            Me.XrLabel_Inactive.SizeF = New System.Drawing.SizeF(73.0!, 17.0!)
            Me.XrLabel_Inactive.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrLabel_Inactive.WordWrap = False
            '
            'XrLabel_Cycle
            '
            Me.XrLabel_Cycle.CanGrow = False
            Me.XrLabel_Cycle.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "disbursement_cycle")})
            Me.XrLabel_Cycle.LocationFloat = New DevExpress.Utils.PointFloat(200.0!, 0.0!)
            Me.XrLabel_Cycle.Name = "XrLabel_Cycle"
            Me.XrLabel_Cycle.SizeF = New System.Drawing.SizeF(33.0!, 16.0!)
            Me.XrLabel_Cycle.StylePriority.UseTextAlignment = False
            Me.XrLabel_Cycle.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter
            Me.XrLabel_Cycle.WordWrap = False
            '
            'XrLabel_Less500
            '
            Me.XrLabel_Less500.CanGrow = False
            Me.XrLabel_Less500.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "AmountSmall", "{0:c}")})
            Me.XrLabel_Less500.LocationFloat = New DevExpress.Utils.PointFloat(480.0!, 0.0!)
            Me.XrLabel_Less500.Name = "XrLabel_Less500"
            Me.XrLabel_Less500.SizeF = New System.Drawing.SizeF(73.0!, 17.0!)
            Me.XrLabel_Less500.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrLabel_Less500.WordWrap = False
            '
            'XrLabel_Negative
            '
            Me.XrLabel_Negative.CanGrow = False
            Me.XrLabel_Negative.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "AmountNegative", "{0:c}")})
            Me.XrLabel_Negative.LocationFloat = New DevExpress.Utils.PointFloat(638.0!, 0.0!)
            Me.XrLabel_Negative.Name = "XrLabel_Negative"
            Me.XrLabel_Negative.SizeF = New System.Drawing.SizeF(73.0!, 17.0!)
            Me.XrLabel_Negative.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrLabel_Negative.WordWrap = False
            '
            'ReportFooter
            '
            Me.ReportFooter.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLine1, Me.XrLabel_Total_Balance, Me.XrLabel_Total_Hold, Me.XrLabel_Total_Negative, Me.XrLabel_Total_Greater500, Me.XrLabel_Total_Less500, Me.XrLabel_Total_Reserved, Me.XrLabel_Total_Inactive, Me.XrLabel_Total})
            Me.ReportFooter.HeightF = 50.0!
            Me.ReportFooter.Name = "ReportFooter"
            Me.ReportFooter.PrintAtBottom = True
            Me.ReportFooter.StyleName = "XrControlStyle_Totals"
            '
            'XrLine1
            '
            Me.XrLine1.ForeColor = System.Drawing.Color.Teal
            Me.XrLine1.LineWidth = 3
            Me.XrLine1.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 4.0!)
            Me.XrLine1.Name = "XrLine1"
            Me.XrLine1.SizeF = New System.Drawing.SizeF(800.0!, 9.0!)
            '
            'XrLabel_Total_Balance
            '
            Me.XrLabel_Total_Balance.CanGrow = False
            Me.XrLabel_Total_Balance.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "held_in_trust")})
            Me.XrLabel_Total_Balance.LocationFloat = New DevExpress.Utils.PointFloat(175.0!, 17.00001!)
            Me.XrLabel_Total_Balance.Name = "XrLabel_Total_Balance"
            Me.XrLabel_Total_Balance.SizeF = New System.Drawing.SizeF(141.0!, 17.0!)
            XrSummary1.FormatString = "{0:c}"
            XrSummary1.Running = DevExpress.XtraReports.UI.SummaryRunning.Report
            Me.XrLabel_Total_Balance.Summary = XrSummary1
            Me.XrLabel_Total_Balance.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrLabel_Total_Balance.WordWrap = False
            '
            'XrLabel_Total_Hold
            '
            Me.XrLabel_Total_Hold.CanGrow = False
            Me.XrLabel_Total_Hold.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "held_in_trust")})
            Me.XrLabel_Total_Hold.LocationFloat = New DevExpress.Utils.PointFloat(401.0!, 17.0!)
            Me.XrLabel_Total_Hold.Name = "XrLabel_Total_Hold"
            Me.XrLabel_Total_Hold.SizeF = New System.Drawing.SizeF(73.0!, 17.0!)
            XrSummary2.FormatString = "{0:c}"
            XrSummary2.Running = DevExpress.XtraReports.UI.SummaryRunning.Report
            Me.XrLabel_Total_Hold.Summary = XrSummary2
            Me.XrLabel_Total_Hold.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrLabel_Total_Hold.WordWrap = False
            '
            'XrLabel_Total_Negative
            '
            Me.XrLabel_Total_Negative.CanGrow = False
            Me.XrLabel_Total_Negative.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "AmountNegative")})
            Me.XrLabel_Total_Negative.LocationFloat = New DevExpress.Utils.PointFloat(638.0!, 17.0!)
            Me.XrLabel_Total_Negative.Name = "XrLabel_Total_Negative"
            Me.XrLabel_Total_Negative.SizeF = New System.Drawing.SizeF(73.0!, 17.0!)
            XrSummary3.FormatString = "{0:c}"
            XrSummary3.Running = DevExpress.XtraReports.UI.SummaryRunning.Report
            Me.XrLabel_Total_Negative.Summary = XrSummary3
            Me.XrLabel_Total_Negative.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrLabel_Total_Negative.WordWrap = False
            '
            'XrLabel_Total_Greater500
            '
            Me.XrLabel_Total_Greater500.CanGrow = False
            Me.XrLabel_Total_Greater500.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "AmountLarge")})
            Me.XrLabel_Total_Greater500.LocationFloat = New DevExpress.Utils.PointFloat(559.0!, 17.0!)
            Me.XrLabel_Total_Greater500.Name = "XrLabel_Total_Greater500"
            Me.XrLabel_Total_Greater500.SizeF = New System.Drawing.SizeF(73.0!, 17.0!)
            XrSummary4.FormatString = "{0:c}"
            XrSummary4.Running = DevExpress.XtraReports.UI.SummaryRunning.Report
            Me.XrLabel_Total_Greater500.Summary = XrSummary4
            Me.XrLabel_Total_Greater500.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrLabel_Total_Greater500.WordWrap = False
            '
            'XrLabel_Total_Less500
            '
            Me.XrLabel_Total_Less500.CanGrow = False
            Me.XrLabel_Total_Less500.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "AmountSmall")})
            Me.XrLabel_Total_Less500.LocationFloat = New DevExpress.Utils.PointFloat(480.0!, 17.0!)
            Me.XrLabel_Total_Less500.Name = "XrLabel_Total_Less500"
            Me.XrLabel_Total_Less500.SizeF = New System.Drawing.SizeF(73.0!, 17.0!)
            XrSummary5.FormatString = "{0:c}"
            XrSummary5.Running = DevExpress.XtraReports.UI.SummaryRunning.Report
            Me.XrLabel_Total_Less500.Summary = XrSummary5
            Me.XrLabel_Total_Less500.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrLabel_Total_Less500.WordWrap = False
            '
            'XrLabel_Total_Reserved
            '
            Me.XrLabel_Total_Reserved.CanGrow = False
            Me.XrLabel_Total_Reserved.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "reserved_in_trust")})
            Me.XrLabel_Total_Reserved.LocationFloat = New DevExpress.Utils.PointFloat(322.0!, 17.0!)
            Me.XrLabel_Total_Reserved.Name = "XrLabel_Total_Reserved"
            Me.XrLabel_Total_Reserved.SizeF = New System.Drawing.SizeF(73.0!, 17.0!)
            XrSummary6.FormatString = "{0:c}"
            XrSummary6.Running = DevExpress.XtraReports.UI.SummaryRunning.Report
            Me.XrLabel_Total_Reserved.Summary = XrSummary6
            Me.XrLabel_Total_Reserved.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrLabel_Total_Reserved.WordWrap = False
            '
            'XrLabel_Total_Inactive
            '
            Me.XrLabel_Total_Inactive.CanGrow = False
            Me.XrLabel_Total_Inactive.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "AmountInactive")})
            Me.XrLabel_Total_Inactive.LocationFloat = New DevExpress.Utils.PointFloat(717.0!, 17.0!)
            Me.XrLabel_Total_Inactive.Name = "XrLabel_Total_Inactive"
            Me.XrLabel_Total_Inactive.SizeF = New System.Drawing.SizeF(73.0!, 17.0!)
            XrSummary7.FormatString = "{0:c}"
            XrSummary7.Running = DevExpress.XtraReports.UI.SummaryRunning.Report
            Me.XrLabel_Total_Inactive.Summary = XrSummary7
            Me.XrLabel_Total_Inactive.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrLabel_Total_Inactive.WordWrap = False
            '
            'XrLabel_Total
            '
            Me.XrLabel_Total.CanGrow = False
            Me.XrLabel_Total.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "client")})
            Me.XrLabel_Total.LocationFloat = New DevExpress.Utils.PointFloat(7.999992!, 17.00001!)
            Me.XrLabel_Total.Name = "XrLabel_Total"
            Me.XrLabel_Total.SizeF = New System.Drawing.SizeF(167.0!, 17.0!)
            XrSummary8.FormatString = "Total ({0:n0} Clients)"
            XrSummary8.Func = DevExpress.XtraReports.UI.SummaryFunc.Count
            XrSummary8.Running = DevExpress.XtraReports.UI.SummaryRunning.Report
            Me.XrLabel_Total.Summary = XrSummary8
            Me.XrLabel_Total.Text = "Total (0 Clients)"
            Me.XrLabel_Total.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            Me.XrLabel_Total.WordWrap = False
            '
            'XrPanel1
            '
            Me.XrPanel1.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel31, Me.XrLabel30, Me.XrLabel29, Me.XrLabel28, Me.XrLabel27, Me.XrLabel26, Me.XrLabel25, Me.XrLabel24, Me.XrLabel14, Me.XrLabel15})
            Me.XrPanel1.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 125.0!)
            Me.XrPanel1.Name = "XrPanel1"
            Me.XrPanel1.SizeF = New System.Drawing.SizeF(800.0!, 17.0!)
            Me.XrPanel1.StyleName = "XrControlStyle_HeaderPannel"
            '
            'XrLabel31
            '
            Me.XrLabel31.LocationFloat = New DevExpress.Utils.PointFloat(562.0!, 0.0!)
            Me.XrLabel31.Name = "XrLabel31"
            Me.XrLabel31.SizeF = New System.Drawing.SizeF(73.0!, 17.0!)
            Me.XrLabel31.Text = "> $500"
            Me.XrLabel31.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel30
            '
            Me.XrLabel30.LocationFloat = New DevExpress.Utils.PointFloat(404.0!, 0.0!)
            Me.XrLabel30.Name = "XrLabel30"
            Me.XrLabel30.SizeF = New System.Drawing.SizeF(73.0!, 17.0!)
            Me.XrLabel30.Text = "HOLD"
            Me.XrLabel30.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel29
            '
            Me.XrLabel29.LocationFloat = New DevExpress.Utils.PointFloat(641.0!, 0.0!)
            Me.XrLabel29.Name = "XrLabel29"
            Me.XrLabel29.SizeF = New System.Drawing.SizeF(73.0!, 17.0!)
            Me.XrLabel29.Text = "< $0.00"
            Me.XrLabel29.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel28
            '
            Me.XrLabel28.LocationFloat = New DevExpress.Utils.PointFloat(483.0!, 0.0!)
            Me.XrLabel28.Name = "XrLabel28"
            Me.XrLabel28.SizeF = New System.Drawing.SizeF(73.0!, 17.0!)
            Me.XrLabel28.Text = "<= $500"
            Me.XrLabel28.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel27
            '
            Me.XrLabel27.LocationFloat = New DevExpress.Utils.PointFloat(83.0!, 0.0!)
            Me.XrLabel27.Name = "XrLabel27"
            Me.XrLabel27.SizeF = New System.Drawing.SizeF(116.0!, 17.0!)
            Me.XrLabel27.Text = "NAME"
            Me.XrLabel27.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            '
            'XrLabel26
            '
            Me.XrLabel26.LocationFloat = New DevExpress.Utils.PointFloat(239.0!, 0.0!)
            Me.XrLabel26.Name = "XrLabel26"
            Me.XrLabel26.SizeF = New System.Drawing.SizeF(77.0!, 17.0!)
            Me.XrLabel26.Text = "BALANCE"
            Me.XrLabel26.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel25
            '
            Me.XrLabel25.LocationFloat = New DevExpress.Utils.PointFloat(322.0!, 0.0!)
            Me.XrLabel25.Name = "XrLabel25"
            Me.XrLabel25.SizeF = New System.Drawing.SizeF(73.0!, 17.0!)
            Me.XrLabel25.Text = "RESVD"
            Me.XrLabel25.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel24
            '
            Me.XrLabel24.LocationFloat = New DevExpress.Utils.PointFloat(720.0!, 0.0!)
            Me.XrLabel24.Name = "XrLabel24"
            Me.XrLabel24.SizeF = New System.Drawing.SizeF(73.0!, 17.0!)
            Me.XrLabel24.Text = "INACTIVE"
            Me.XrLabel24.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel14
            '
            Me.XrLabel14.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
            Me.XrLabel14.Name = "XrLabel14"
            Me.XrLabel14.SizeF = New System.Drawing.SizeF(74.0!, 17.0!)
            Me.XrLabel14.Text = "CLIENT"
            Me.XrLabel14.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel15
            '
            Me.XrLabel15.LocationFloat = New DevExpress.Utils.PointFloat(200.0!, 0.0!)
            Me.XrLabel15.Name = "XrLabel15"
            Me.XrLabel15.SizeF = New System.Drawing.SizeF(33.0!, 17.0!)
            Me.XrLabel15.StylePriority.UseTextAlignment = False
            Me.XrLabel15.Text = "CYC"
            Me.XrLabel15.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
            '
            'ParameterState
            '
            Me.ParameterState.Description = "State ID"
            Me.ParameterState.Name = "ParameterState"
            Me.ParameterState.Value = ""
            Me.ParameterState.Visible = False
            '
            'TrustAccountBalanceReport
            '
            Me.Bands.AddRange(New DevExpress.XtraReports.UI.Band() {Me.Detail, Me.PageHeader, Me.PageFooter, Me.ReportFooter, Me.TopMarginBand1, Me.BottomMarginBand1})
            Me.Parameters.AddRange(New DevExpress.XtraReports.Parameters.Parameter() {Me.ParameterState})
            Me.RequestParameters = False
            Me.ScriptLanguage = DevExpress.XtraReports.ScriptLanguage.VisualBasic
            Me.ScriptReferencesString = resources.GetString("$this.ScriptReferencesString")
            Me.Scripts.OnBeforePrint = "TrustAccountBalanceReport_BeforePrint"
            Me.ScriptsSource = resources.GetString("$this.ScriptsSource")
            Me.StyleSheet.AddRange(New DevExpress.XtraReports.UI.XRControlStyle() {Me.XrControlStyle_Header, Me.XrControlStyle_Totals, Me.XrControlStyle_HeaderPannel, Me.XrControlStyle_GroupHeader})
            Me.Version = "11.2"
            Me.Controls.SetChildIndex(Me.BottomMarginBand1, 0)
            Me.Controls.SetChildIndex(Me.TopMarginBand1, 0)
            Me.Controls.SetChildIndex(Me.ReportFooter, 0)
            Me.Controls.SetChildIndex(Me.PageFooter, 0)
            Me.Controls.SetChildIndex(Me.PageHeader, 0)
            Me.Controls.SetChildIndex(Me.Detail, 0)
            CType(Me, System.ComponentModel.ISupportInitialize).EndInit()

        End Sub

        Friend WithEvents XrLabel1 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel2 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel3 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel4 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel5 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_ClientID As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_Cycle As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents ReportFooter As DevExpress.XtraReports.UI.ReportFooterBand
        Friend WithEvents XrPanel1 As DevExpress.XtraReports.UI.XRPanel
        Friend WithEvents XrLabel14 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel15 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel24 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel25 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel26 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel27 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel28 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel29 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel30 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel31 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_ClientName As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_Balance As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_Reserved As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_Hold As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_Greater500 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_Inactive As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_Less500 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_Negative As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_Total_Inactive As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_Total_Reserved As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_Total_Balance As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_Total As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_Total_Less500 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_Total_Greater500 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_Total_Negative As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_Total_Hold As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLine1 As DevExpress.XtraReports.UI.XRLine
        Friend WithEvents ParameterState As DevExpress.XtraReports.Parameters.Parameter
    End Class
End Namespace
