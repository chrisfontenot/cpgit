#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}

#End Region

Option Compare Binary
Option Explicit On
Option Strict On

Imports DebtPlus.Data.Controls
Imports DebtPlus.Reports.Template.Forms

Imports System.Data.SqlClient
Imports DevExpress.Utils
Imports DebtPlus.Data.Forms
Imports DevExpress.XtraEditors

Namespace Deposits.Batch.ByTrustRegister
    Friend Class DepositBatchByTrustRegisterParametersForm
        Inherits ReportParametersForm
        Private _Parameter_BatchID As Int32 = 0

        Public Sub New()
            MyBase.New()
            InitializeComponent()

            AddHandler TextEdit_deposit_batch_id.GotFocus, AddressOf TextEdit_deposit_batch_id_GotFocus
            AddHandler LookUpEdit_Deposit.EditValueChanged, AddressOf LookUpEdit_Disbursement_EditValueChanged
            AddHandler TextEdit_deposit_batch_id.EditValueChanged, AddressOf TextEdit_disbursement_register_EditValueChanged
            AddHandler TextEdit_deposit_batch_id.KeyPress, AddressOf TextEdit_disbursement_register_KeyPress
            AddHandler RadioButton_List.CheckedChanged, AddressOf RadioButton_List_CheckedChanged
            AddHandler RadioButton_Text.CheckedChanged, AddressOf RadioButton_List_CheckedChanged
            AddHandler Load, AddressOf Form_Load
        End Sub

        ''' <summary>
        ''' Deposit batch ID
        ''' </summary>
        Friend Property Parameter_BatchID() As Int32
            Get
                Return _Parameter_BatchID
            End Get

            Set(ByVal Value As Int32)
                _Parameter_BatchID = Value
            End Set
        End Property

        Friend ReadOnly Property sort_order() As String
            Get
                Dim item As ComboboxItem
                With ComboBoxEdit_Sorting
                    If .SelectedIndex < 0 Then Return String.Empty
                    item = CType(.SelectedItem, DebtPlus.Data.Controls.ComboboxItem)
                    Return Convert.ToString(item.value)
                End With
            End Get
        End Property

#Region " Windows Form Designer generated code "

        'Form overrides dispose to clean up the component list.
        Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
            If disposing Then
                If components IsNot Nothing Then
                    components.Dispose()
                End If
                ds.Dispose()
            End If

            ' Add the handling routines
            RemoveHandler LookUpEdit_Deposit.EditValueChanged, AddressOf LookUpEdit_Disbursement_EditValueChanged
            RemoveHandler TextEdit_deposit_batch_id.EditValueChanged, AddressOf TextEdit_disbursement_register_EditValueChanged
            RemoveHandler TextEdit_deposit_batch_id.KeyPress, AddressOf TextEdit_disbursement_register_KeyPress
            RemoveHandler RadioButton_List.CheckedChanged, AddressOf RadioButton_List_CheckedChanged
            RemoveHandler RadioButton_Text.CheckedChanged, AddressOf RadioButton_List_CheckedChanged

            MyBase.Dispose(disposing)
        End Sub

        'Required by the Windows Form Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Windows Form Designer
        'It can be modified using the Windows Form Designer.  
        'Do not modify it using the code editor.
        Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
        Friend WithEvents RadioButton_List As System.Windows.Forms.RadioButton
        Friend WithEvents RadioButton_Text As System.Windows.Forms.RadioButton
        Friend WithEvents Label1 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents Label2 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents ComboBoxEdit_Sorting As DevExpress.XtraEditors.ComboBoxEdit
        Friend WithEvents TextEdit_deposit_batch_id As DevExpress.XtraEditors.TextEdit
        Friend WithEvents LookUpEdit_Deposit As DevExpress.XtraEditors.LookUpEdit

        <System.Diagnostics.DebuggerStepThrough()>
        Private Sub InitializeComponent()
            Me.GroupBox1 = New System.Windows.Forms.GroupBox
            Me.Label1 = New DevExpress.XtraEditors.LabelControl
            Me.RadioButton_Text = New System.Windows.Forms.RadioButton
            Me.RadioButton_List = New System.Windows.Forms.RadioButton
            Me.TextEdit_deposit_batch_id = New DevExpress.XtraEditors.TextEdit
            Me.LookUpEdit_Deposit = New DevExpress.XtraEditors.LookUpEdit
            Me.Label2 = New DevExpress.XtraEditors.LabelControl
            Me.ComboBoxEdit_Sorting = New DevExpress.XtraEditors.ComboBoxEdit

            Me.GroupBox1.SuspendLayout()
            CType(Me.TextEdit_deposit_batch_id.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LookUpEdit_Deposit.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.ComboBoxEdit_Sorting.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.SuspendLayout()
            '
            'ButtonCancel
            '
            Me.ButtonCancel.Location = New System.Drawing.Point(248, 56)
            Me.ButtonCancel.Name = "ButtonCancel"
            Me.ButtonCancel.Size = New System.Drawing.Size(80, 28)
            Me.ButtonCancel.TabIndex = 4
            '
            'ButtonOK
            '
            Me.ButtonOK.Location = New System.Drawing.Point(248, 18)
            Me.ButtonOK.Name = "ButtonOK"
            Me.ButtonOK.Size = New System.Drawing.Size(80, 28)
            Me.ButtonOK.TabIndex = 3

            '
            'GroupBox1
            '
            Me.GroupBox1.Controls.Add(Me.Label1)
            Me.GroupBox1.Controls.Add(Me.RadioButton_Text)
            Me.GroupBox1.Controls.Add(Me.RadioButton_List)
            Me.GroupBox1.Controls.Add(Me.TextEdit_deposit_batch_id)
            Me.GroupBox1.Controls.Add(Me.LookUpEdit_Deposit)
            Me.GroupBox1.Location = New System.Drawing.Point(8, 9)
            Me.GroupBox1.Name = "GroupBox1"
            Me.GroupBox1.Size = New System.Drawing.Size(232, 112)
            Me.ToolTipController1.SetSuperTip(Me.GroupBox1, Nothing)
            Me.GroupBox1.TabIndex = 0
            Me.GroupBox1.TabStop = False
            Me.GroupBox1.Text = " Deposit Batch ID  "
            '
            'Label1
            '
            Me.Label1.Appearance.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.Label1.Appearance.Options.UseFont = True
            Me.Label1.Location = New System.Drawing.Point(32, 54)
            Me.Label1.Name = "Label1"
            Me.Label1.Size = New System.Drawing.Size(152, 17)
            Me.Label1.TabIndex = 2
            Me.Label1.Text = "Or enter a specific ID"
            '
            'RadioButton_Text
            '
            Me.RadioButton_Text.AllowDrop = True
            Me.RadioButton_Text.Location = New System.Drawing.Point(8, 78)
            Me.RadioButton_Text.Name = "RadioButton_Text"
            Me.RadioButton_Text.Size = New System.Drawing.Size(16, 17)
            Me.ToolTipController1.SetSuperTip(Me.RadioButton_Text, Nothing)
            Me.RadioButton_Text.TabIndex = 3
            Me.RadioButton_Text.Tag = "text"
            Me.RadioButton_Text.TextAlign = System.Drawing.ContentAlignment.MiddleRight
            '
            'RadioButton_List
            '
            Me.RadioButton_List.Checked = True
            Me.RadioButton_List.Location = New System.Drawing.Point(8, 28)
            Me.RadioButton_List.Name = "RadioButton_List"
            Me.RadioButton_List.Size = New System.Drawing.Size(16, 17)
            Me.ToolTipController1.SetSuperTip(Me.RadioButton_List, Nothing)
            Me.RadioButton_List.TabIndex = 0
            Me.RadioButton_List.TabStop = True
            Me.RadioButton_List.Tag = "list"
            '
            'TextEdit_deposit_batch_id
            '
            Me.TextEdit_deposit_batch_id.EditValue = ""
            Me.TextEdit_deposit_batch_id.Location = New System.Drawing.Point(32, 78)
            Me.TextEdit_deposit_batch_id.Name = "TextEdit_deposit_batch_id"
            '
            'TextEdit_deposit_batch_id.Properties
            '
            Me.TextEdit_deposit_batch_id.Properties.Appearance.Options.UseTextOptions = True
            Me.TextEdit_deposit_batch_id.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.TextEdit_deposit_batch_id.Properties.AppearanceDisabled.Options.UseTextOptions = True
            Me.TextEdit_deposit_batch_id.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.TextEdit_deposit_batch_id.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
            Me.TextEdit_deposit_batch_id.Properties.DisplayFormat.FormatString = "f0"
            Me.TextEdit_deposit_batch_id.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.TextEdit_deposit_batch_id.Properties.EditFormat.FormatString = "f0"
            Me.TextEdit_deposit_batch_id.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.TextEdit_deposit_batch_id.Properties.ValidateOnEnterKey = True
            Me.TextEdit_deposit_batch_id.Size = New System.Drawing.Size(64, 20)
            Me.TextEdit_deposit_batch_id.TabIndex = 4
            Me.TextEdit_deposit_batch_id.ToolTip = "Enter a specific disbursement if it is not included in the above list"
            Me.TextEdit_deposit_batch_id.ToolTipController = Me.ToolTipController1
            '
            'LookUpEdit_Deposit
            '
            Me.LookUpEdit_Deposit.Location = New System.Drawing.Point(32, 26)
            Me.LookUpEdit_Deposit.Name = "LookUpEdit_Deposit"
            '
            'LookUpEdit_Deposit.Properties
            '
            Me.LookUpEdit_Deposit.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.LookUpEdit_Deposit.Properties.NullText = ""
            Me.LookUpEdit_Deposit.Properties.PopupWidth = 500
            Me.LookUpEdit_Deposit.Size = New System.Drawing.Size(184, 20)
            Me.LookUpEdit_Deposit.TabIndex = 1
            Me.LookUpEdit_Deposit.ToolTip = "Choose a disbursement from the list if desired"
            Me.LookUpEdit_Deposit.ToolTipController = Me.ToolTipController1
            '
            'Label2
            '
            Me.Label2.Location = New System.Drawing.Point(8, 129)
            Me.Label2.Name = "Label2"
            Me.Label2.Size = New System.Drawing.Size(56, 17)
            Me.Label2.TabIndex = 1
            Me.Label2.Text = "Sorting:"
            '
            'ComboBoxEdit_Sorting
            '
            Me.ComboBoxEdit_Sorting.EditValue = ""
            Me.ComboBoxEdit_Sorting.Location = New System.Drawing.Point(64, 127)
            Me.ComboBoxEdit_Sorting.Name = "ComboBoxEdit_Sorting"
            '
            'ComboBoxEdit_Sorting.Properties
            '
            Me.ComboBoxEdit_Sorting.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.ComboBoxEdit_Sorting.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
            Me.ComboBoxEdit_Sorting.Size = New System.Drawing.Size(176, 20)
            Me.ComboBoxEdit_Sorting.TabIndex = 2
            Me.ComboBoxEdit_Sorting.ToolTip = "How should the report be sorted?"
            Me.ComboBoxEdit_Sorting.ToolTipController = Me.ToolTipController1
            '
            'RequestParametersForm
            '
            Me.AutoScaleBaseSize = New System.Drawing.Size(5, 14)
            Me.ClientSize = New System.Drawing.Size(336, 163)
            Me.Controls.Add(Me.ComboBoxEdit_Sorting)
            Me.Controls.Add(Me.Label2)
            Me.Controls.Add(Me.GroupBox1)
            Me.Name = "RequestParametersForm"
            Me.ToolTipController1.SetSuperTip(Me, Nothing)
            Me.Text = "Deposit Batch Report Parameters"
            Me.Controls.SetChildIndex(Me.ButtonOK, 0)
            Me.Controls.SetChildIndex(Me.ButtonCancel, 0)
            Me.Controls.SetChildIndex(Me.GroupBox1, 0)
            Me.Controls.SetChildIndex(Me.Label2, 0)
            Me.Controls.SetChildIndex(Me.ComboBoxEdit_Sorting, 0)

            Me.GroupBox1.ResumeLayout(False)
            CType(Me.TextEdit_deposit_batch_id.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LookUpEdit_Deposit.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.ComboBoxEdit_Sorting.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            Me.ResumeLayout(False)
        End Sub

#End Region

        Private InInit As Boolean = True

        Private Sub Form_Load(ByVal sender As Object, ByVal e As EventArgs)
            InInit = True

            ' Populate the sorting order dropdown
            With ComboBoxEdit_Sorting
                With .Properties
                    With .Items
                        .Clear()
                        .Add(New DebtPlus.Data.Controls.ComboboxItem("Entry Order", "deposit_batch_detail"))
                        .Add(New DebtPlus.Data.Controls.ComboboxItem("Client ID", "client"))
                        .Add(New DebtPlus.Data.Controls.ComboboxItem("Amount", "credit_amt"))
                        .Add(New DebtPlus.Data.Controls.ComboboxItem("Client Name", "client_name"))
                        .Add(New DebtPlus.Data.Controls.ComboboxItem("Deposit Type", "tran_subtype_description"))
                        .Add(New DebtPlus.Data.Controls.ComboboxItem("Date/Time Entered", "date_created"))
                        .Add(New DebtPlus.Data.Controls.ComboboxItem("Instrument Date", "item_date"))
                        .Add(New DebtPlus.Data.Controls.ComboboxItem("Reference Field", "reference"))
                    End With
                End With
                .SelectedIndex = 0      ' Make the item default to the first position
            End With

            ' If a batch is specified then don't bother to allow the user to change it.
            ' It comes from the application and it's value is correct.
            If Parameter_BatchID > 0 Then
                TextEdit_deposit_batch_id.EditValue = Parameter_BatchID

                RadioButton_List.Checked = False
                RadioButton_Text.Checked = True

                ' Disable the controls for selecting the batch number
                GroupBox1.Enabled = False

            Else

                ' Load the deposit registers
                load_deposit_registers()
            End If

            ' Enable or disable the OK button
            EnableOK()
            InInit = False
        End Sub

        Dim ds As New DataSet("ds")

        Private Sub load_deposit_registers()

            ' Retrieve the database connection logic
            Try
                Using cmd As SqlCommand = New System.Data.SqlClient.SqlCommand()
                    With cmd
                        .Connection = New System.Data.SqlClient.SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
                        .CommandText = "SELECT TOP 50 tr.trust_register AS 'ID', tr.date_created AS 'Date', tr.created_by AS 'Creator', coalesce(bnk.description,'Bank #' + convert(varchar,tr.bank),'') as 'Bank', isnull(tr.sequence_number,'') as 'Note', coalesce(tr.sequence_number, 'Unlabeled batch #' + convert(varchar, tr.trust_register), '') AS DisplayMember FROM registers_trust tr WITH (NOLOCK) LEFT OUTER JOIN banks bnk WITH (NOLOCK) ON tr.bank = bnk.bank WHERE tr.tran_type = 'DP' ORDER BY tr.trust_register desc"
                        .CommandType = CommandType.Text
                        .CommandTimeout = 0
                    End With

                    ' Retrieve the disbursement table from the system
                    Using da As New SqlDataAdapter(cmd)
                        da.Fill(ds, "deposit_batch_ids")
                    End Using
                End Using

                Dim tbl As DataTable = ds.Tables(0)

                ' Define the values for the lookup control
                With LookUpEdit_Deposit
                    With .Properties
                        .DataSource = New DataView(tbl, "", "Date", DataViewRowState.CurrentRows)
                        .ShowFooter = False
                        .PopulateColumns()

                        ' The date and time are the default values
                        With .Columns("Date")
                            .FormatString = "MM/dd/yyyy hh:mm tt"
                            .Width = 90
                        End With

                        ' Populate the ID column with the disbursement register, showing it as a number
                        With .Columns("ID")
                            .FormatString = "f0"
                            .Alignment = HorzAlignment.Far
                            .Width = 40
                        End With

                        With .Columns("DisplayMember")
                            .Visible = False
                        End With

                        ' The display is the note, the value is the ID
                        .DisplayMember = "DisplayMember"
                        .ValueMember = "ID"
                    End With

                    ' Set the value to the first item
                    If tbl.Rows.Count > 0 Then
                        Dim row As DataRow = tbl.Rows(0)
                        .EditValue = row("ID")
                    End If

                    ' Populate the input value with the ID from the listbox
                    If .EditValue IsNot DBNull.Value Then
                        Parameter_BatchID = Convert.ToInt32(.EditValue)
                        TextEdit_deposit_batch_id.Text = Parameter_BatchID.ToString()
                    End If
                End With

            Catch ex As SqlException
                MessageBox.Show(ex.ToString(), "Error retrieving batch list", MessageBoxButtons.OK, MessageBoxIcon.Error)
            End Try
        End Sub

        Private Sub TextEdit_deposit_batch_id_GotFocus(ByVal sender As Object, ByVal e As EventArgs)
            CType(sender, TextEdit).SelectAll()
        End Sub

        Private Sub TextEdit_disbursement_register_EditValueChanged(ByVal sender As Object, ByVal e As EventArgs)
            Parameter_BatchID = Convert.ToInt32(TextEdit_deposit_batch_id.EditValue)
            EnableOK()
        End Sub

        Private Sub TextEdit_disbursement_register_KeyPress(ByVal sender As Object, ByVal e As KeyPressEventArgs)
            If Not Char.IsControl(e.KeyChar) Then
                If Not Char.IsDigit(e.KeyChar) Then e.Handled = True
            End If
        End Sub

        Private Sub LookUpEdit_Disbursement_EditValueChanged(ByVal sender As Object, ByVal e As EventArgs)
            TextEdit_deposit_batch_id.EditValue = LookUpEdit_Deposit.EditValue
            Parameter_BatchID = Convert.ToInt32(TextEdit_deposit_batch_id.EditValue)
            EnableOK()
        End Sub

        Private Sub EnableOK()
            Try
                Dim NewValue As Int32 = CType(TextEdit_deposit_batch_id.EditValue, Int32)
                If NewValue > 0 Then
                    ButtonOK.Enabled = True
                    Exit Sub
                End If
            Catch
            End Try

            ' The disbursement register is not valid. Disable the button.
            ButtonOK.Enabled = False
        End Sub

        Private Sub RadioButton_List_CheckedChanged(ByVal sender As Object, ByVal e As EventArgs)
            If Not InInit Then
                ' Enable or disable the controls based upon the radio buttons
                TextEdit_deposit_batch_id.Enabled = RadioButton_Text.Checked
                LookUpEdit_Deposit.Enabled = RadioButton_List.Checked

                ' Syncrhonize the edit value with the selected item
                TextEdit_deposit_batch_id.EditValue = LookUpEdit_Deposit.EditValue

                ' Go to the next control
                CType(sender, RadioButton).SelectNextControl(CType(sender, Control), True, True, False, True)
            End If
        End Sub
    End Class
End Namespace
