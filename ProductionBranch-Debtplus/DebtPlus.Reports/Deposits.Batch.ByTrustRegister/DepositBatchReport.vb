#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}

#End Region

Option Compare Binary
Option Explicit On
Option Strict On

Imports DebtPlus.Reports.Template

Imports DebtPlus.Utils
Imports System.IO
Imports System.Reflection
Imports DevExpress.XtraReports.UI

Namespace Deposits.Batch.ByTrustRegister
    Public Class DepositBatchReport
        Inherits TemplateXtraReportClass


        ''' <summary>
        '''     Provide the linkage to set the parameters
        ''' </summary>
        Public Overrides Sub SetReportParameter(ByVal Parameter As String, ByVal Value As Object)
            Select Case Parameter
                Case "DepositBatchID"
                    Parameter_DepositBatchID = Convert.ToInt32(Value)
                Case "SortString"
                    Parameter_SortString = Convert.ToString(Value)
                Case Else
                    MyBase.SetReportParameter(Parameter, Value)
            End Select
        End Sub


        ''' <summary>
        '''     Deposit Batch ID parameter for the report
        ''' </summary>
        Public Property Parameter_DepositBatchID() As Int32
            Get
                Return Convert.ToInt32(Parameters("ParameterBatchID").Value)
            End Get
            Set(ByVal Value As Int32)
                Parameters("ParameterBatchID").Value = Value
            End Set
        End Property


        ''' <summary>
        '''     Report listing order
        ''' </summary>
        Public Property Parameter_SortString() As String
            Get
                Return Convert.ToString(Parameters("ParameterSortOrder").Value)
            End Get
            Set(ByVal Value As String)
                Parameters("ParameterSortOrder").Value = Value
            End Set
        End Property


        ''' <summary>
        '''     Create an instance of the class
        ''' </summary>
        Public Sub New()
            MyBase.New()

            Const ReportName As String = "DebtPlus.Reports.Deposits.Batch.ByTrustRegister.repx"

            ' See if there is a report reference that we can use
            Dim Fname As String = Path.Combine(DebtPlus.Configuration.Config.ReportsDirectory, ReportName)
            Dim UseDefault As Boolean = True
            Try
                If (Not DebtPlus.Configuration.Config.LocalReportsOnly) AndAlso System.IO.File.Exists(Fname) Then
                    LoadLayout(Fname)
                    UseDefault = False
                End If
            Catch ex As Exception
            End Try

            ' Load the standard report definition if we need a new item
            If UseDefault Then
                Dim asm As Assembly = Assembly.GetExecutingAssembly                                           ' changed
                Using ios As Stream = asm.GetManifestResourceStream(String.Format("DebtPlus.Reports.{0}", ReportName)) ' changed
                    If ios IsNot Nothing Then LoadLayout(ios) '                                                                       changed
                End Using
            End If

            ScriptReferences = DebtPlus.Utils.AssemblyTools.GetAllSiblingAssemblies()

            ' Copy the script references to the subreports as well
            For Each Band As Band In Bands
                For Each ctl As XRControl In Band.Controls
                    Dim subReport As XRSubreport = TryCast(ctl, XRSubreport)
                    If subReport IsNot Nothing Then
                        Dim rpt As XtraReport = subReport.ReportSource
                        If rpt IsNot Nothing Then
                            rpt.ScriptReferences = ScriptReferences
                        End If
                    End If
                Next
            Next

            ' Clear the parameters
            Parameter_DepositBatchID = -1
            Parameter_SortString = String.Empty
        End Sub


        ''' <summary>
        '''     Title associated with the report
        ''' </summary>
        Public Overrides ReadOnly Property ReportTitle() As String
            Get
                Return "Client Deposit Batch"
            End Get
        End Property


        ''' <summary>
        '''     Determine if we need to ask for the parameters
        ''' </summary>
        Public Overrides Function NeedParameters() As Boolean
            Return MyBase.NeedParameters() OrElse Parameter_DepositBatchID < 0
        End Function


        ''' <summary>
        '''     Request the parameters for the report
        ''' </summary>
        Public Overrides Function RequestReportParameters() As DialogResult
            Dim answer As DialogResult = DialogResult.OK
            If NeedParameters() Then
                Using dialogForm As New DepositBatchByTrustRegisterParametersForm()
                    With dialogForm
                        .Parameter_BatchID = Parameter_DepositBatchID
                        answer = .ShowDialog()
                        Parameter_DepositBatchID = .Parameter_BatchID
                        Parameter_SortString = .sort_order
                    End With
                End Using
            End If
            Return answer
        End Function
    End Class
End Namespace
