Namespace Deposits.Batch.ByTrustRegister
    Partial Class DepositBatchReport
        Inherits DebtPlus.Reports.Template.TemplateXtraReportClass

        'Component overrides dispose to clean up the component list.
        Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
            If disposing Then
                If components IsNot Nothing Then
                    components.Dispose()
                End If
            End If
            MyBase.Dispose(disposing)
        End Sub

        'Required by the Component Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Component Designer
        'It can be modified using the Component Designer.
        'Do not modify it using the code editor.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
            Dim XrSummary1 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary()
            Dim XrSummary2 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary()
            Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(DepositBatchReport))
            Me.StyleColumnHeaderText = New DevExpress.XtraReports.UI.XRControlStyle()
            Me.XrControlStyle2 = New DevExpress.XtraReports.UI.XRControlStyle()
            Me.XrPanel1 = New DevExpress.XtraReports.UI.XRPanel()
            Me.XrLabel10 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel9 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel8 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel7 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel6 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel5 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_TranType = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_ClientID = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_ClientName = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_Amount = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_Date = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_Reference = New DevExpress.XtraReports.UI.XRLabel()
            Me.ReportFooter = New DevExpress.XtraReports.UI.ReportFooterBand()
            Me.XrLine1 = New DevExpress.XtraReports.UI.XRLine()
            Me.XrLabel_TotalAmount = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_TotalsLabel = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrTable_BatchHeader = New DevExpress.XtraReports.UI.XRTable()
            Me.XrTableRow1 = New DevExpress.XtraReports.UI.XRTableRow()
            Me.XrTableCell1 = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableCell_BatchID = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableRow2 = New DevExpress.XtraReports.UI.XRTableRow()
            Me.XrTableCell2 = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableCell_BatchCreatedOn = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableRow3 = New DevExpress.XtraReports.UI.XRTableRow()
            Me.XrTableCell5 = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableCell_Bank = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableRow4 = New DevExpress.XtraReports.UI.XRTableRow()
            Me.XrTableCell7 = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableCell_BankAccount = New DevExpress.XtraReports.UI.XRTableCell()
            Me.ParameterBatchID = New DevExpress.XtraReports.Parameters.Parameter()
            Me.ParameterSortOrder = New DevExpress.XtraReports.Parameters.Parameter()
            CType(Me.XrTable_BatchHeader, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
            '
            'PageHeader
            '
            Me.PageHeader.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrTable_BatchHeader, Me.XrPanel1})
            Me.PageHeader.HeightF = 206.0!
            Me.PageHeader.Controls.SetChildIndex(Me.XrPanel1, 0)
            Me.PageHeader.Controls.SetChildIndex(Me.XrPanel_AgencyAddress, 0)
            Me.PageHeader.Controls.SetChildIndex(Me.XrLabel_Title, 0)
            Me.PageHeader.Controls.SetChildIndex(Me.XrLine_Title, 0)
            Me.PageHeader.Controls.SetChildIndex(Me.XrLabel_Subtitle, 0)
            Me.PageHeader.Controls.SetChildIndex(Me.XrPageInfo1, 0)
            Me.PageHeader.Controls.SetChildIndex(Me.XrTable_BatchHeader, 0)
            '
            'XrLabel_Title
            '
            Me.XrLabel_Title.StylePriority.UseFont = False
            '
            'XrPageInfo_PageNumber
            '
            Me.XrPageInfo_PageNumber.StylePriority.UseTextAlignment = False
            '
            'XRLabel_Agency_Name
            '
            Me.XRLabel_Agency_Name.StylePriority.UseTextAlignment = False
            '
            'XrLabel_Agency_Address3
            '
            Me.XrLabel_Agency_Address3.StylePriority.UseFont = False
            Me.XrLabel_Agency_Address3.StylePriority.UseTextAlignment = False
            '
            'XrLabel_Agency_Address1
            '
            Me.XrLabel_Agency_Address1.StylePriority.UseFont = False
            Me.XrLabel_Agency_Address1.StylePriority.UseTextAlignment = False
            '
            'XrLabel_Agency_Phone
            '
            Me.XrLabel_Agency_Phone.StylePriority.UseFont = False
            Me.XrLabel_Agency_Phone.StylePriority.UseTextAlignment = False
            '
            'XrLabel_Agency_Address2
            '
            Me.XrLabel_Agency_Address2.StylePriority.UseFont = False
            Me.XrLabel_Agency_Address2.StylePriority.UseTextAlignment = False
            '
            'Detail
            '
            Me.Detail.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel_Reference, Me.XrLabel_Date, Me.XrLabel_Amount, Me.XrLabel_ClientName, Me.XrLabel_ClientID, Me.XrLabel_TranType})
            Me.Detail.HeightF = 18.0!
            '
            'StyleColumnHeaderText
            '
            Me.StyleColumnHeaderText.BackColor = System.Drawing.Color.Transparent
            Me.StyleColumnHeaderText.BorderColor = System.Drawing.Color.Transparent
            Me.StyleColumnHeaderText.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.StyleColumnHeaderText.ForeColor = System.Drawing.Color.White
            Me.StyleColumnHeaderText.Name = "StyleColumnHeaderText"
            '
            'XrControlStyle2
            '
            Me.XrControlStyle2.BackColor = System.Drawing.Color.Transparent
            Me.XrControlStyle2.ForeColor = System.Drawing.Color.Black
            Me.XrControlStyle2.Name = "XrControlStyle2"
            '
            'XrPanel1
            '
            Me.XrPanel1.BackColor = System.Drawing.Color.Teal
            Me.XrPanel1.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel10, Me.XrLabel9, Me.XrLabel8, Me.XrLabel7, Me.XrLabel6, Me.XrLabel5})
            Me.XrPanel1.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 183.0!)
            Me.XrPanel1.Name = "XrPanel1"
            Me.XrPanel1.SizeF = New System.Drawing.SizeF(750.0!, 17.0!)
            '
            'XrLabel10
            '
            Me.XrLabel10.LocationFloat = New DevExpress.Utils.PointFloat(592.0!, 0.0!)
            Me.XrLabel10.Name = "XrLabel10"
            Me.XrLabel10.SizeF = New System.Drawing.SizeF(150.0!, 17.0!)
            Me.XrLabel10.StyleName = "StyleColumnHeaderText"
            Me.XrLabel10.Text = "Reference ID"
            '
            'XrLabel9
            '
            Me.XrLabel9.LocationFloat = New DevExpress.Utils.PointFloat(517.0!, 0.0!)
            Me.XrLabel9.Name = "XrLabel9"
            Me.XrLabel9.SizeF = New System.Drawing.SizeF(59.0!, 17.0!)
            Me.XrLabel9.StyleName = "StyleColumnHeaderText"
            Me.XrLabel9.Text = "Date"
            Me.XrLabel9.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
            '
            'XrLabel8
            '
            Me.XrLabel8.LocationFloat = New DevExpress.Utils.PointFloat(425.0!, 0.0!)
            Me.XrLabel8.Name = "XrLabel8"
            Me.XrLabel8.SizeF = New System.Drawing.SizeF(66.0!, 17.0!)
            Me.XrLabel8.StyleName = "StyleColumnHeaderText"
            Me.XrLabel8.Text = "Amount"
            Me.XrLabel8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
            '
            'XrLabel7
            '
            Me.XrLabel7.LocationFloat = New DevExpress.Utils.PointFloat(167.0!, 0.0!)
            Me.XrLabel7.Name = "XrLabel7"
            Me.XrLabel7.SizeF = New System.Drawing.SizeF(250.0!, 17.0!)
            Me.XrLabel7.StyleName = "StyleColumnHeaderText"
            Me.XrLabel7.Text = "Client Name"
            '
            'XrLabel6
            '
            Me.XrLabel6.LocationFloat = New DevExpress.Utils.PointFloat(92.0!, 0.0!)
            Me.XrLabel6.Name = "XrLabel6"
            Me.XrLabel6.SizeF = New System.Drawing.SizeF(66.0!, 17.0!)
            Me.XrLabel6.StyleName = "StyleColumnHeaderText"
            Me.XrLabel6.Text = "Client ID"
            Me.XrLabel6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
            '
            'XrLabel5
            '
            Me.XrLabel5.LocationFloat = New DevExpress.Utils.PointFloat(8.0!, 0.0!)
            Me.XrLabel5.Name = "XrLabel5"
            Me.XrLabel5.SizeF = New System.Drawing.SizeF(75.0!, 17.0!)
            Me.XrLabel5.StyleName = "StyleColumnHeaderText"
            Me.XrLabel5.Text = "Item Type"
            '
            'XrLabel_TranType
            '
            Me.XrLabel_TranType.CanGrow = False
            Me.XrLabel_TranType.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "tran_subtype_description")})
            Me.XrLabel_TranType.LocationFloat = New DevExpress.Utils.PointFloat(8.0!, 0.0!)
            Me.XrLabel_TranType.Name = "XrLabel_TranType"
            Me.XrLabel_TranType.SizeF = New System.Drawing.SizeF(75.0!, 17.0!)
            Me.XrLabel_TranType.StylePriority.UseTextAlignment = False
            Me.XrLabel_TranType.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            Me.XrLabel_TranType.WordWrap = False
            '
            'XrLabel_ClientID
            '
            Me.XrLabel_ClientID.CanGrow = False
            Me.XrLabel_ClientID.LocationFloat = New DevExpress.Utils.PointFloat(83.0!, 0.0!)
            Me.XrLabel_ClientID.Name = "XrLabel_ClientID"
            Me.XrLabel_ClientID.Scripts.OnBeforePrint = "XrLabel_ClientID_BeforePrint"
            Me.XrLabel_ClientID.SizeF = New System.Drawing.SizeF(75.0!, 17.0!)
            Me.XrLabel_ClientID.StylePriority.UseTextAlignment = False
            Me.XrLabel_ClientID.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel_ClientName
            '
            Me.XrLabel_ClientName.CanGrow = False
            Me.XrLabel_ClientName.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "client_name")})
            Me.XrLabel_ClientName.LocationFloat = New DevExpress.Utils.PointFloat(167.0!, 0.0!)
            Me.XrLabel_ClientName.Multiline = True
            Me.XrLabel_ClientName.Name = "XrLabel_ClientName"
            Me.XrLabel_ClientName.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_ClientName.SizeF = New System.Drawing.SizeF(250.0!, 17.0!)
            Me.XrLabel_ClientName.StylePriority.UsePadding = False
            Me.XrLabel_ClientName.StylePriority.UseTextAlignment = False
            Me.XrLabel_ClientName.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            '
            'XrLabel_Amount
            '
            Me.XrLabel_Amount.CanGrow = False
            Me.XrLabel_Amount.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "credit_amt", "{0:c}")})
            Me.XrLabel_Amount.LocationFloat = New DevExpress.Utils.PointFloat(425.0!, 0.0!)
            Me.XrLabel_Amount.Name = "XrLabel_Amount"
            Me.XrLabel_Amount.SizeF = New System.Drawing.SizeF(66.0!, 17.0!)
            Me.XrLabel_Amount.StylePriority.UseTextAlignment = False
            Me.XrLabel_Amount.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel_Date
            '
            Me.XrLabel_Date.CanGrow = False
            Me.XrLabel_Date.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "item_date", "{0:d}")})
            Me.XrLabel_Date.LocationFloat = New DevExpress.Utils.PointFloat(500.0!, 0.0!)
            Me.XrLabel_Date.Name = "XrLabel_Date"
            Me.XrLabel_Date.SizeF = New System.Drawing.SizeF(76.0!, 17.0!)
            Me.XrLabel_Date.StylePriority.UseTextAlignment = False
            Me.XrLabel_Date.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel_Reference
            '
            Me.XrLabel_Reference.CanGrow = False
            Me.XrLabel_Reference.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "reference")})
            Me.XrLabel_Reference.LocationFloat = New DevExpress.Utils.PointFloat(592.0!, 0.0!)
            Me.XrLabel_Reference.Name = "XrLabel_Reference"
            Me.XrLabel_Reference.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_Reference.SizeF = New System.Drawing.SizeF(167.0!, 17.0!)
            Me.XrLabel_Reference.StylePriority.UsePadding = False
            Me.XrLabel_Reference.StylePriority.UseTextAlignment = False
            Me.XrLabel_Reference.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            '
            'ReportFooter
            '
            Me.ReportFooter.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLine1, Me.XrLabel_TotalAmount, Me.XrLabel_TotalsLabel})
            Me.ReportFooter.HeightF = 42.0!
            Me.ReportFooter.Name = "ReportFooter"
            '
            'XrLine1
            '
            Me.XrLine1.LocationFloat = New DevExpress.Utils.PointFloat(375.0!, 8.0!)
            Me.XrLine1.Name = "XrLine1"
            Me.XrLine1.SizeF = New System.Drawing.SizeF(117.0!, 9.0!)
            '
            'XrLabel_TotalAmount
            '
            Me.XrLabel_TotalAmount.CanGrow = False
            Me.XrLabel_TotalAmount.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "credit_amt")})
            Me.XrLabel_TotalAmount.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.XrLabel_TotalAmount.ForeColor = System.Drawing.Color.Black
            Me.XrLabel_TotalAmount.LocationFloat = New DevExpress.Utils.PointFloat(275.0!, 25.0!)
            Me.XrLabel_TotalAmount.Name = "XrLabel_TotalAmount"
            Me.XrLabel_TotalAmount.SizeF = New System.Drawing.SizeF(216.0!, 17.0!)
            XrSummary1.FormatString = "{0:c}"
            XrSummary1.Running = DevExpress.XtraReports.UI.SummaryRunning.Report
            Me.XrLabel_TotalAmount.Summary = XrSummary1
            Me.XrLabel_TotalAmount.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
            '
            'XrLabel_TotalsLabel
            '
            Me.XrLabel_TotalsLabel.CanGrow = False
            Me.XrLabel_TotalsLabel.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel_TotalsLabel.ForeColor = System.Drawing.Color.Blue
            Me.XrLabel_TotalsLabel.LocationFloat = New DevExpress.Utils.PointFloat(8.0!, 25.0!)
            Me.XrLabel_TotalsLabel.Name = "XrLabel_TotalsLabel"
            Me.XrLabel_TotalsLabel.SizeF = New System.Drawing.SizeF(217.0!, 17.0!)
            XrSummary2.FormatString = "Grand Total of Batch ({0:n0} items)"
            XrSummary2.Func = DevExpress.XtraReports.UI.SummaryFunc.Count
            XrSummary2.Running = DevExpress.XtraReports.UI.SummaryRunning.Report
            Me.XrLabel_TotalsLabel.Summary = XrSummary2
            '
            'XrTable_BatchHeader
            '
            Me.XrTable_BatchHeader.LocationFloat = New DevExpress.Utils.PointFloat(7.999992!, 112.0!)
            Me.XrTable_BatchHeader.Name = "XrTable_BatchHeader"
            Me.XrTable_BatchHeader.Rows.AddRange(New DevExpress.XtraReports.UI.XRTableRow() {Me.XrTableRow1, Me.XrTableRow2, Me.XrTableRow3, Me.XrTableRow4})
            Me.XrTable_BatchHeader.SizeF = New System.Drawing.SizeF(733.0!, 60.00001!)
            Me.XrTable_BatchHeader.StyleName = "XrControlStyle_Header"
            '
            'XrTableRow1
            '
            Me.XrTableRow1.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.XrTableCell1, Me.XrTableCell_BatchID})
            Me.XrTableRow1.Name = "XrTableRow1"
            Me.XrTableRow1.Weight = 0.6R
            '
            'XrTableCell1
            '
            Me.XrTableCell1.Name = "XrTableCell1"
            Me.XrTableCell1.Text = "Batch Number:"
            Me.XrTableCell1.Weight = 0.60749597900723684R
            '
            'XrTableCell_BatchID
            '
            Me.XrTableCell_BatchID.Name = "XrTableCell_BatchID"
            Me.XrTableCell_BatchID.Weight = 2.3925040209927633R
            '
            'XrTableRow2
            '
            Me.XrTableRow2.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.XrTableCell2, Me.XrTableCell_BatchCreatedOn})
            Me.XrTableRow2.Name = "XrTableRow2"
            Me.XrTableRow2.Weight = 0.6R
            '
            'XrTableCell2
            '
            Me.XrTableCell2.Name = "XrTableCell2"
            Me.XrTableCell2.Text = "Batch Created On:"
            Me.XrTableCell2.Weight = 0.60749597900723684R
            '
            'XrTableCell_BatchCreatedOn
            '
            Me.XrTableCell_BatchCreatedOn.Name = "XrTableCell_BatchCreatedOn"
            Me.XrTableCell_BatchCreatedOn.Weight = 2.3925040209927633R
            '
            'XrTableRow3
            '
            Me.XrTableRow3.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.XrTableCell5, Me.XrTableCell_Bank})
            Me.XrTableRow3.Name = "XrTableRow3"
            Me.XrTableRow3.Weight = 0.6R
            '
            'XrTableCell5
            '
            Me.XrTableCell5.Name = "XrTableCell5"
            Me.XrTableCell5.Text = "Bank Name:"
            Me.XrTableCell5.Weight = 0.60749597900723684R
            '
            'XrTableCell_Bank
            '
            Me.XrTableCell_Bank.Name = "XrTableCell_Bank"
            Me.XrTableCell_Bank.Weight = 2.3925040209927633R
            '
            'XrTableRow4
            '
            Me.XrTableRow4.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.XrTableCell7, Me.XrTableCell_BankAccount})
            Me.XrTableRow4.Name = "XrTableRow4"
            Me.XrTableRow4.Weight = 0.6R
            '
            'XrTableCell7
            '
            Me.XrTableCell7.Name = "XrTableCell7"
            Me.XrTableCell7.Text = "Bank Account Number:"
            Me.XrTableCell7.Weight = 0.60749597900723684R
            '
            'XrTableCell_BankAccount
            '
            Me.XrTableCell_BankAccount.Name = "XrTableCell_BankAccount"
            Me.XrTableCell_BankAccount.Weight = 2.3925040209927633R
            '
            'ParameterBatchID
            '
            Me.ParameterBatchID.Description = "Deposit Batch ID"
            Me.ParameterBatchID.Name = "ParameterBatchID"
            Me.ParameterBatchID.Type = GetType(Integer)
            Me.ParameterBatchID.Value = -1
            Me.ParameterBatchID.Visible = False
            '
            'ParameterSortOrder
            '
            Me.ParameterSortOrder.Description = "Sort Order"
            Me.ParameterSortOrder.Name = "ParameterSortOrder"
            Me.ParameterSortOrder.Value = ""
            Me.ParameterSortOrder.Visible = False
            '
            'DepositBatchReport
            '
            Me.Bands.AddRange(New DevExpress.XtraReports.UI.Band() {Me.Detail, Me.PageHeader, Me.PageFooter, Me.ReportFooter, Me.TopMarginBand1, Me.BottomMarginBand1})
            Me.Parameters.AddRange(New DevExpress.XtraReports.Parameters.Parameter() {Me.ParameterBatchID, Me.ParameterSortOrder})
            Me.RequestParameters = False
            Me.ScriptLanguage = DevExpress.XtraReports.ScriptLanguage.VisualBasic
            Me.ScriptReferencesString = resources.GetString("$this.ScriptReferencesString")
            Me.Scripts.OnBeforePrint = "DepositBatchReport_BeforePrint"
            Me.ScriptsSource = resources.GetString("$this.ScriptsSource")
            Me.StyleSheet.AddRange(New DevExpress.XtraReports.UI.XRControlStyle() {Me.XrControlStyle_Header, Me.XrControlStyle_Totals, Me.XrControlStyle_HeaderPannel, Me.XrControlStyle_GroupHeader, Me.StyleColumnHeaderText, Me.XrControlStyle2})
            Me.Version = "11.2"
            Me.Controls.SetChildIndex(Me.BottomMarginBand1, 0)
            Me.Controls.SetChildIndex(Me.TopMarginBand1, 0)
            Me.Controls.SetChildIndex(Me.ReportFooter, 0)
            Me.Controls.SetChildIndex(Me.PageFooter, 0)
            Me.Controls.SetChildIndex(Me.PageHeader, 0)
            Me.Controls.SetChildIndex(Me.Detail, 0)
            CType(Me.XrTable_BatchHeader, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me, System.ComponentModel.ISupportInitialize).EndInit()

        End Sub
        Friend WithEvents XrPanel1 As DevExpress.XtraReports.UI.XRPanel
        Friend WithEvents XrLabel5 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel6 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel7 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel8 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel9 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel10 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_ClientName As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents ReportFooter As DevExpress.XtraReports.UI.ReportFooterBand
        Friend WithEvents XrLabel_TotalsLabel As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLine1 As DevExpress.XtraReports.UI.XRLine
        Friend WithEvents XrLabel_TranType As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_ClientID As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_Amount As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_Date As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_Reference As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_TotalAmount As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents StyleColumnHeaderText As DevExpress.XtraReports.UI.XRControlStyle
        Friend WithEvents XrControlStyle2 As DevExpress.XtraReports.UI.XRControlStyle
        Friend WithEvents XrTable_BatchHeader As DevExpress.XtraReports.UI.XRTable
        Friend WithEvents XrTableRow1 As DevExpress.XtraReports.UI.XRTableRow
        Friend WithEvents XrTableCell1 As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents XrTableCell_BatchID As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents XrTableRow2 As DevExpress.XtraReports.UI.XRTableRow
        Friend WithEvents XrTableCell2 As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents XrTableCell_BatchCreatedOn As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents XrTableRow3 As DevExpress.XtraReports.UI.XRTableRow
        Friend WithEvents XrTableCell5 As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents XrTableCell_Bank As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents XrTableRow4 As DevExpress.XtraReports.UI.XRTableRow
        Friend WithEvents XrTableCell7 As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents XrTableCell_BankAccount As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents ParameterBatchID As DevExpress.XtraReports.Parameters.Parameter
        Friend WithEvents ParameterSortOrder As DevExpress.XtraReports.Parameters.Parameter

    End Class
End Namespace
