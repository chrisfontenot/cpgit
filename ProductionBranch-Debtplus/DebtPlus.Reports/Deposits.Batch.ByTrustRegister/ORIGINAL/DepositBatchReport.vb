#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}
#End Region

Option Compare Binary
Option Explicit On 
Option Strict On

Namespace Deposits.Batch.ByTrustRegister
    Public Class DepositBatchReport

        ''' <summary>
        ''' Provide the linkage to set the parameters
        ''' </summary>
        Public Overrides Sub SetReportParameter(ByVal Parameter As String, ByVal Value As Object)
            Select Case Parameter
                Case "DepositBatchID"
                    Parameter_DepositBatchID = Convert.ToInt32(Value)
                Case "SortString"
                    Parameter_SortString = Convert.ToString(Value)
                Case Else
                    MyBase.SetReportParameter(Parameter, Value)
            End Select
        End Sub


        ''' <summary>
        ''' Deposit Batch ID parameter for the report
        ''' </summary>
        Public Property Parameter_DepositBatchID() As System.Int32
            Get
                Return Convert.ToInt32(Parameters("ParameterBatchID").Value)
            End Get
            Set(ByVal Value As System.Int32)
                Parameters("ParameterBatchID").Value = Value
            End Set
        End Property


        ''' <summary>
        ''' Report listing order
        ''' </summary>
        Public Property Parameter_SortString() As String
            Get
                Return Convert.ToString(Parameters("ParameterSortOrder").Value)
            End Get
            Set(ByVal Value As String)
                Parameters("ParameterSortOrder").Value = Value
            End Set
        End Property

        ''' <summary>
        ''' Create an instance of the class
        ''' </summary>
        Public Sub New()
            MyBase.New()
            InitializeComponent()
            AddHandler BeforePrint, AddressOf DepositBatchReport_BeforePrint
            AddHandler XrLabel_ClientID.BeforePrint, AddressOf XrLabel_ClientID_BeforePrint
        End Sub

        ''' <summary>
        ''' Title associated with the report
        ''' </summary>
        Public Overrides ReadOnly Property ReportTitle() As String
            Get
                Return "Client Deposit Batch"
            End Get
        End Property


        ''' <summary>
        ''' Determine if we need to ask for the parameters
        ''' </summary>
        Public Overrides Function NeedParameters() As Boolean
            Return MyBase.NeedParameters() OrElse Parameter_DepositBatchID < 0
        End Function


        ''' <summary>
        ''' Request the parameters for the report
        ''' </summary>
        Public Overrides Function RequestReportParameters() As System.Windows.Forms.DialogResult

            '-- Request the parameter value from the lower form
            Dim Answer As System.Windows.Forms.DialogResult = DialogResult.OK
            If NeedParameters() Then

                '-- Request the disbursement register from the user
                Using DialogForm As New DepositBatchByTrustRegisterParametersForm()
                    DialogForm.Parameter_BatchID = Parameter_DepositBatchID
                    Answer = DialogForm.ShowDialog()
                    Parameter_DepositBatchID = DialogForm.Parameter_BatchID
                    Parameter_SortString = DialogForm.sort_order
                End Using
            End If

            '-- The answer should be OK if we want to show the report.
            Return Answer
        End Function

        Dim ds As New System.Data.DataSet("ds")

        ''' <summary>
        ''' Bind the dataset to the report
        ''' </summary>
        Private Sub DepositBatchReport_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs)
            Dim rpt As DevExpress.XtraReports.UI.XtraReport = CType(sender, DevExpress.XtraReports.UI.XtraReport)
            Dim sqlInfo As DebtPlus.LINQ.SQLInfoClass = DebtPlus.LINQ.SQLInfoClass.getDefault()
            Dim cn As System.Data.SqlClient.SqlConnection = New System.Data.SqlClient.SqlConnection(sqlInfo.ConnectionString)
            Dim rd As System.Data.SqlClient.SqlDataReader = Nothing
            Const TableName As String = "view_client_deposits"
            Dim SortOrder As String = Convert.ToString(rpt.Parameters("ParameterSortOrder").Value).Trim

            Try
                cn.Open()

                Using cmd As New System.Data.SqlClient.SqlCommand()
                    cmd.Connection = cn
                    cmd.CommandText = "SELECT * FROM view_client_deposits WITH (NOLOCK) WHERE deposit_batch_id=@deposit_batch_id AND ok_to_post = 1"
                    If SortOrder <> System.String.Empty Then cmd.CommandText += " ORDER BY " + SortOrder
                    cmd.Parameters.Add("@deposit_batch_id", System.Data.SqlDbType.Int).Value = rpt.Parameters("ParameterBatchID").Value
                    cmd.CommandTimeout = 0

                    Using da As New System.Data.SqlClient.SqlDataAdapter(cmd)
                        da.Fill(ds, TableName)
                    End Using
                End Using

                rpt.DataSource = ds.Tables(TableName).DefaultView

                '-- Read the batch header information
                Using cmd As New System.Data.SqlClient.SqlCommand()
                    cmd.Connection = cn
                    cmd.CommandText = "SELECT tr.sequence_number as deposit_batch_id, tr.date_created, dbo.format_counselor_name(tr.created_by) as created_by, coalesce(b.description,'Bank #' + convert(varchar,tr.bank),'') as BankName, isnull(b.account_number,'') as BankAccountNumber FROM registers_trust tr LEFT OUTER JOIN banks b ON tr.bank=b.bank WHERE [trust_register]=@deposit_batch_id"
                    cmd.CommandType = System.Data.CommandType.Text
                    cmd.Parameters.Add("@deposit_batch_id", System.Data.SqlDbType.Int).Value = rpt.Parameters("ParameterBatchID").Value
                    rd = cmd.ExecuteReader(System.Data.CommandBehavior.SingleRow Or System.Data.CommandBehavior.CloseConnection)
                End Using

                '-- Populate the display information with the first row
                If rd IsNot Nothing AndAlso rd.Read Then
                    Dim cell As DevExpress.XtraReports.UI.XRTableCell
                    cell = CType(rpt.FindControl("XrTableCell_BatchID", True), DevExpress.XtraReports.UI.XRTableCell)
                    If cell IsNot Nothing Then
                        If Not rd.IsDBNull(0) Then
                            cell.Text = rd.GetString(0)
                        End If
                    End If

                    cell = CType(rpt.FindControl("XrTableCell_BatchCreatedOn", True), DevExpress.XtraReports.UI.XRTableCell)
                    If cell IsNot Nothing Then cell.Text = String.Format("{0:d} by {1}", rd.GetDateTime(1), rd.GetString(2))

                    cell = CType(rpt.FindControl("XrTableCell_Bank", True), DevExpress.XtraReports.UI.XRTableCell)
                    If cell IsNot Nothing Then
                        If Not rd.IsDBNull(3) Then
                            cell.Text = rd.GetString(3)
                        End If
                    End If

                    cell = CType(rpt.FindControl("XrTableCell_BankAccount", True), DevExpress.XtraReports.UI.XRTableCell)
                    If cell IsNot Nothing Then
                        If Not rd.IsDBNull(4) Then
                            cell.Text = rd.GetString(4)
                        End If
                    End If
                End If

            Catch ex As System.Data.SqlClient.SqlException
                Using gdr As New DebtPlus.Repository.GetDataResult()
                    gdr.HandleException(ex)
                    DebtPlus.UI.Common.ErrorHandling.HandleErrors(gdr, "Error reading report information")
                End Using

            Finally
                If rd IsNot Nothing Then rd.Dispose()
                cn.Dispose()
            End Try
        End Sub

        ''' <summary>
        ''' Format the client ID if the value is not null
        ''' </summary>
        Private Sub XrLabel_ClientID_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs)
            Dim lbl As DevExpress.XtraReports.UI.XRLabel = CType(sender, DevExpress.XtraReports.UI.XRLabel)
            Dim rpt As DevExpress.XtraReports.UI.XtraReport = CType(lbl.Report, DevExpress.XtraReports.UI.XtraReport)
            lbl.Text = DebtPlus.Utils.Format.Client.FormatClientID(DebtPlus.Utils.Nulls.DInt(rpt.GetCurrentColumnValue("client")))
        End Sub
    End Class
End Namespace
