#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}

#End Region


Imports System.Drawing.Printing
Imports System.Data.SqlClient
Imports DevExpress.Utils

Namespace Debt.Transactions.Client.Transactions
    Public Class ClientTransactionsSubReport
        Protected Client As Int32 = -1
        Protected ds As New DataSet("ds")
        Protected tbl As DataTable = Nothing
        Private client_register As Int32 = -1

        Public Sub New()
            MyBase.New()
            InitializeComponent()
            AddHandler Me.BeforePrint, AddressOf Report_BeforePrint
        End Sub

        Public Sub New(ByVal DateRange As DebtPlus.Utils.DateRange)
            MyBase.New(DateRange)
            InitializeComponent()
            AddHandler Me.BeforePrint, AddressOf Report_BeforePrint
        End Sub

        Public Overrides ReadOnly Property ReportTitle() As String
            Get
                Return "Debt Transactions"
            End Get
        End Property

        Private privateSubtitle As String = String.Empty

        Public Overridable Shadows ReadOnly Property ReportSubTitle() As String
            Get
                Return privateSubtitle
            End Get
        End Property

        Protected ReadOnly Property OriginalReportSubtitle() As String
            Get
                Return MyBase.ReportSubTitle
            End Get
        End Property

        Public Sub SetSubReportParameters(ByVal Subtitle As String, ByVal client_register As Int32)
            Me.privateSubtitle = Subtitle
            SetSubReportParameters(client_register)
        End Sub

        Public Sub SetSubReportParameters(ByVal client_register As Int32)
            Me.client_register = client_register
        End Sub

        Protected Overridable Sub Report_BeforePrint(ByVal sender As Object, ByVal e As PrintEventArgs)

            ' Load the transactions if needed
            tbl = ds.Tables("rpt_transactions_cc_cl")
            If tbl Is Nothing Then
                Dim cn As SqlConnection = New System.Data.SqlClient.SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
                Dim dlg As New WaitDialogForm("Reading Debt Transactions")
                dlg.Show()

                Try
                    cn.Open()
                    Using cmd As SqlCommand = New System.Data.SqlClient.SqlCommand()
                        With cmd
                            .Connection = cn
                            .CommandText = "rpt_transactions_cc_cl"
                            .CommandType = CommandType.StoredProcedure
                            SqlCommandBuilder.DeriveParameters(cmd)
                            .CommandTimeout = 0
                            .Parameters(1).Value = client_register
                        End With

                        Using da As New SqlDataAdapter(cmd)
                            da.Fill(ds, "rpt_transactions_cc_cl")
                        End Using
                    End Using
                    tbl = ds.Tables("rpt_transactions_cc_cl")

                Finally
                    If cn IsNot Nothing Then cn.Dispose()

                    ' Dispose of the "please wait" dialog now.
                    If dlg IsNot Nothing Then
                        dlg.Close()
                        dlg.Dispose()
                    End If
                End Try
            End If

            If tbl IsNot Nothing Then
                BindReportFields(tbl)
            End If
        End Sub

        Protected Sub BindReportFields(ByVal tbl As DataTable)

            ' Bind the report to the dataview
            Dim vue As DataView = New DataView(tbl, String.Empty, "item_date", DataViewRowState.CurrentRows)
            DataSource = vue

            ' Find the client from the information
            If vue.Count > 0 AndAlso vue(0)("client") IsNot Nothing AndAlso vue(0)("client") IsNot DBNull.Value Then
                Client = Convert.ToInt32(vue(0)("client"))
                XrLabel_ClientID.Text = String.Format("{0} {1}", String.Format("{0:0000000}", Client), ClientNameFromID(Client))
            End If
        End Sub

        Private Function ClientNameFromID(ByVal Client As Int32) As String
            Dim Answer As String = String.Empty
            Dim cn As SqlConnection = New System.Data.SqlClient.SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
            Dim Current_Cursor As Cursor = Cursor.Current
            Try
                Cursor.Current = Cursors.WaitCursor
                cn.Open()

                Using cmd As SqlCommand = New System.Data.SqlClient.SqlCommand()
                    With cmd
                        .Connection = cn
                        .CommandText = "SELECT name FROM view_client_address WHERE [client]=@client"
                        .CommandType = CommandType.Text
                        .Parameters.Add("@client", SqlDbType.Int).Value = Client
                        .CommandTimeout = 0
                        Dim objAnswer As Object = .ExecuteScalar
                        If objAnswer IsNot Nothing AndAlso objAnswer IsNot DBNull.Value Then
                            Answer = Convert.ToString(objAnswer)
                        End If
                    End With
                End Using

            Catch ex As SqlException
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Database Error")

            Finally
                If cn IsNot Nothing Then cn.Dispose()
                Cursor.Current = Current_Cursor
            End Try

            Return Answer
        End Function
    End Class
End Namespace