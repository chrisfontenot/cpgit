#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}

#End Region

Imports System.ComponentModel
Imports DebtPlus.Reports.Debt.Transactions.Client.Transactions

Imports System.Drawing.Printing
Imports System.Data.SqlClient
Imports System.Text

Namespace Debt.Transactions
    Public Class DebtTransactionsReport
        Inherits ClientTransactionsSubReport

        ''' <summary>
        ''' Create an instance of our report
        ''' </summary>
        Public Sub New()
            MyBase.New()
            InitializeComponent()
        End Sub

        Public Sub New(ByVal DateRange As DebtPlus.Utils.DateRange)
            MyBase.New(DateRange)
            InitializeComponent()
        End Sub

        Private Sub InitializeComponent()
        End Sub

        Private privateClientCreditor As Int32 = -1

        Public Property Parameter_ClientCreditor() As Int32
            Get
                Return privateClientCreditor
            End Get
            Set(ByVal value As Int32)
                privateClientCreditor = value
            End Set
        End Property

        Public Overrides ReadOnly Property ReportSubTitle() As String
            Get
                Return MyBase.OriginalReportSubtitle
            End Get
        End Property

        Public Overrides Sub SetReportParameter(ByVal Parameter As String, ByVal Value As Object)
            Select Case Parameter
                Case "ClientCreditor"
                    Parameter_ClientCreditor = Convert.ToInt32(Value)
                Case Else
                    MyBase.SetReportParameter(Parameter, Value)
            End Select
        End Sub

        Protected Overrides Sub Report_BeforePrint(ByVal sender As Object, ByVal e As PrintEventArgs)

            tbl = ds.Tables("registers_client_creditor")
            If tbl Is Nothing Then

                ' Read the data from the database to match the report specific fields
                Using cmd As SqlCommand = New System.Data.SqlClient.SqlCommand()
                    With cmd
                        .Connection = New System.Data.SqlClient.SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
                        .CommandTimeout = 0

                        Dim sb As New StringBuilder
                        sb.Append("SELECT d.tran_type, ")
                        sb.Append("d.date_created as 'item_date', ")
                        sb.Append("d.debit_amt as 'debit_amt', ")
                        sb.Append("d.credit_amt as 'credit_amt', ")
                        sb.Append("cc.client as 'client', ")
                        sb.Append("cc.creditor as 'creditor', ")
                        sb.Append("cr.creditor_name as 'creditor_name', ")
                        sb.Append("tr.checknum as 'checknum', ")
                        sb.Append("tr.reconciled_date as 'item_reconciled', ")
                        sb.Append("coalesce(d.account_number,cc.account_number,'') as 'account_number' ")
                        sb.Append("FROM	registers_client_creditor d	WITH (NOLOCK) ")
                        sb.Append("LEFT OUTER JOIN client_creditor cc WITH (NOLOCK) ON @client_creditor = cc.client_creditor ")
                        sb.Append("LEFT OUTER JOIN creditors cr WITH (NOLOCK) ON cc.creditor = cr.creditor ")
                        sb.Append("LEFT OUTER JOIN creditor_contribution_pcts pct with (nolock) on cr.creditor_contribution_pct = pct.creditor_contribution_pct ")
                        sb.Append("LEFT OUTER JOIN registers_trust tr WITH (NOLOCK) ON d.trust_register = tr.trust_register ")
                        sb.Append("WHERE d.client_creditor = @client_creditor ")
                        sb.Append("AND d.tran_type IS NOT NULL ")   ' Just to force the system to use an index
                        sb.Append("AND d.date_created >= @From_Date AND d.date_created < @To_Date")

                        .CommandText = sb.ToString()
                        .CommandType = CommandType.Text
                        .Parameters.Add("@client_creditor", SqlDbType.Int).Value = Parameter_ClientCreditor
                        .Parameters.Add("@from_date", SqlDbType.DateTime).Value = Parameter_FromDate.Date
                        .Parameters.Add("@to_date", SqlDbType.DateTime).Value = Parameter_ToDate.Date.AddDays(1)
                        .CommandTimeout = 0
                    End With

                    Using da As New SqlDataAdapter(cmd)
                        da.Fill(ds, "registers_client_creditor")
                        tbl = ds.Tables("registers_client_creditor")
                    End Using
                End Using
            End If

            ' Bind the report to the data
            If tbl IsNot Nothing Then
                BindReportFields(tbl)
            End If
        End Sub
    End Class
End Namespace
