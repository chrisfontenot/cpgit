#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}
#End Region

Option Compare Binary
Option Explicit On 
Option Strict On

Imports DebtPlus.Reports.Template

Namespace Disbursement.DetailInformation

    Public Class DisbursementDetailInformationReport
        Inherits DatedTemplateXtraReportClass

        ''' <summary>
        ''' Create an instance of our report
        ''' </summary>

        Public Sub New()
            MyBase.New()
            InitializeComponent()

            AddHandler Me.BeforePrint, AddressOf Report_BeforePrint
            AddHandler XrLabel_Billed.PreviewClick, AddressOf Preview_Click
            AddHandler XrLabel_Deducted.PreviewClick, AddressOf Preview_Click
            AddHandler XrLabel_Gross.PreviewClick, AddressOf Preview_Click
            AddHandler XrLabel_Net.PreviewClick, AddressOf Preview_Click
            AddHandler XrLabel_CheckNumber.PreviewClick, AddressOf Preview_Click
            AddHandler XrLabel_Creditor.PreviewClick, AddressOf Preview_Click
            AddHandler XrLabel_CreditorName.PreviewClick, AddressOf Preview_Click
            AddHandler XrLabel_Date.PreviewClick, AddressOf Preview_Click
        End Sub


        ''' <summary>
        ''' Report title
        ''' </summary>
        Public Overrides ReadOnly Property ReportTitle() As String
            Get
                Return "Disbursement Detail Creditor Information"
            End Get
        End Property


        ''' <summary>
        ''' Report Before Print
        ''' </summary>
        Protected Overridable Sub Report_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs)
            DataSource = DatabaseTable()
        End Sub


        ''' <summary>
        ''' Retrieve the Database information
        ''' </summary>
        Protected Overridable Function DatabaseTable() As System.Data.DataView
            Dim answer As New System.Text.StringBuilder
            Dim ds As New System.Data.DataSet("ds")

            answer.Append("select	convert(varchar(50),newid()) as id,")
            answer.Append("         tr.trust_register, ")
            answer.Append(" 		tr.bank, tr.checknum, ")
            answer.Append(" 		rcc.creditor, ")
            answer.Append(" 		cr.creditor_name, ")
            answer.Append(" 		tr.date_created, ")
            answer.Append(" 		SUM(rcc.debit_amt) as gross, ")
            answer.Append(" 		SUM(case when rcc.creditor_type in ('D','N') then 0 else rcc.fairshare_amt end) as billed, ")
            answer.Append(" 		SUM(case when rcc.creditor_type = 'D' then rcc.fairshare_amt else 0 end) as deducted, ")
            answer.Append(" 		LEN(tr.checknum) as len_checknum ")
            answer.Append("from     registers_client_creditor rcc ")
            answer.Append("inner join registers_trust tr on rcc.trust_register = tr.trust_register ")
            answer.Append("inner join creditors cr on rcc.creditor = cr.creditor ")
            answer.Append("where	rcc.tran_type in ('AD','MD','CM','BW') ")
            answer.Append("and		rcc.date_created between @FromDate and @ToDate ")
            answer.Append("group by tr.trust_register, tr.bank, tr.checknum, rcc.creditor, cr.creditor_name, tr.date_created; ")

            Dim tbl As System.Data.DataTable = Nothing
            Using cmd As SqlClient.SqlCommand = New System.Data.SqlClient.SqlCommand()
                With cmd
                    .Connection = New System.Data.SqlClient.SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
                    .CommandText = answer.ToString()
                    .CommandType = CommandType.Text
                    .CommandTimeout = 0
                    .Parameters.Add("@FromDate", SqlDbType.DateTime).Value = Parameter_FromDate
                    .Parameters.Add("@ToDate", SqlDbType.DateTime).Value = Parameter_ToDate
                End With

                Using da As New SqlClient.SqlDataAdapter(cmd)
                    da.Fill(ds, "records")
                    tbl = ds.Tables("records")

                    With tbl
                        If Not .Columns.Contains("net") Then
                            .Columns.Add("net", GetType(Decimal), "[gross]-[deducted]")
                        End If
                        .PrimaryKey = New System.Data.DataColumn() {.Columns("id")}
                    End With
                End Using
            End Using

            Return New System.Data.DataView(tbl, String.Empty, "[date_created], [bank], [len_checknum], [checknum], [creditor]", DataViewRowState.CurrentRows)
        End Function


        ''' <summary>
        ''' Handle the preview click event
        ''' </summary>
        Protected Overridable Sub Preview_Click(ByVal sender As Object, ByVal e As DevExpress.XtraReports.UI.PreviewMouseEventArgs)
            If e.Brick.Value IsNot Nothing Then
                DetailReport(Convert.ToString(e.Brick.Value))
            End If
        End Sub

        Private Sub DetailReport(ByVal tag As String)
            Dim thrd As New System.Threading.Thread(New System.Threading.ParameterizedThreadStart(AddressOf MyRunReport))
            With thrd
                .SetApartmentState(Threading.ApartmentState.STA)
                .IsBackground = True
                .Name = "DetailReport"
                .Start(tag)
            End With
        End Sub


        ''' <summary>
        ''' Run the subreport
        ''' </summary>
        Protected Overridable Sub MyRunReport(ByVal obj As Object)
            Dim RecordsTable As System.Data.DataTable = CType(DataSource, System.Data.DataView).Table
            Dim row As System.Data.DataRow = RecordsTable.Rows.Find(obj)
            Dim iRep As New Disbursement.DetailInformation.DetailReport()
            Try
                With iRep
                    .Parameter_TrustRegister = Convert.ToInt32(row("trust_register"))
                    .Parameter_Creditor = Convert.ToString(row("creditor"))
                    .Parameter_FromDate = Parameter_FromDate
                    .Parameter_ToDate = Parameter_ToDate
                    .CreateDocument()
                    .PrintingSystem.Document.Name = ReportTitle
                    .DisplayPreviewDialog()
                End With

            Finally
                iRep.Dispose()
            End Try
        End Sub
    End Class

End Namespace
