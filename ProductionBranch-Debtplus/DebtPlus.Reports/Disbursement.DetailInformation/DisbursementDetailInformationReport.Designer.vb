﻿Namespace Disbursement.DetailInformation

    Partial Class DisbursementDetailInformationReport

        'Component overrides dispose to clean up the component list.
        Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
            If disposing Then
                If components IsNot Nothing Then
                    components.Dispose()
                End If
            End If
            MyBase.Dispose(disposing)
        End Sub

        'Required by the Designer
        Private components As System.ComponentModel.IContainer

        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
            Dim XrSummary1 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary
            Dim XrSummary2 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary
            Dim XrSummary3 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary
            Dim XrSummary4 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary
            Me.XrLabel_Date = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_CheckNumber = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_Creditor = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_CreditorName = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_Gross = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_Deducted = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_Billed = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_Net = New DevExpress.XtraReports.UI.XRLabel
            Me.XrPanel1 = New DevExpress.XtraReports.UI.XRPanel
            Me.XrLabel8 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel7 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel6 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel5 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel4 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel2 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel1 = New DevExpress.XtraReports.UI.XRLabel
            Me.ReportFooter = New DevExpress.XtraReports.UI.ReportFooterBand
            Me.XrLine1 = New DevExpress.XtraReports.UI.XRLine
            Me.XrLabel11A = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel10A = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel9A = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel3A = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_total_net = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_total_deduct = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_total_billed = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_total_gross = New DevExpress.XtraReports.UI.XRLabel
            CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
            '
            'Detail
            '
            Me.Detail.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel_Net, Me.XrLabel_Billed, Me.XrLabel_Deducted, Me.XrLabel_Gross, Me.XrLabel_CreditorName, Me.XrLabel_Creditor, Me.XrLabel_CheckNumber, Me.XrLabel_Date})
            Me.Detail.HeightF = 15.0!
            '
            'PageHeader
            '
            Me.PageHeader.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrPanel1})
            Me.PageHeader.HeightF = 160.0!
            Me.PageHeader.Controls.SetChildIndex(Me.XrPanel1, 0)
            Me.PageHeader.Controls.SetChildIndex(Me.XrPanel_AgencyAddress, 0)
            Me.PageHeader.Controls.SetChildIndex(Me.XrLabel_Title, 0)
            Me.PageHeader.Controls.SetChildIndex(Me.XrLine_Title, 0)
            Me.PageHeader.Controls.SetChildIndex(Me.XrLabel_Subtitle, 0)
            Me.PageHeader.Controls.SetChildIndex(Me.XrPageInfo1, 0)
            '
            'XrLabel_Title
            '
            Me.XrLabel_Title.StylePriority.UseFont = False
            '
            'XrPageInfo_PageNumber
            '
            Me.XrPageInfo_PageNumber.LocationFloat = New DevExpress.Utils.PointFloat(617.0!, 8.0!)
            Me.XrPageInfo_PageNumber.StylePriority.UseTextAlignment = False
            '
            'XRLabel_Agency_Name
            '
            Me.XRLabel_Agency_Name.StylePriority.UseTextAlignment = False
            '
            'XrLabel_Agency_Address3
            '
            Me.XrLabel_Agency_Address3.StylePriority.UseFont = False
            Me.XrLabel_Agency_Address3.StylePriority.UseTextAlignment = False
            '
            'XrLabel_Agency_Address1
            '
            Me.XrLabel_Agency_Address1.StylePriority.UseFont = False
            Me.XrLabel_Agency_Address1.StylePriority.UseTextAlignment = False
            '
            'XrLabel_Agency_Phone
            '
            Me.XrLabel_Agency_Phone.StylePriority.UseFont = False
            Me.XrLabel_Agency_Phone.StylePriority.UseTextAlignment = False
            '
            'XrLabel_Agency_Address2
            '
            Me.XrLabel_Agency_Address2.StylePriority.UseFont = False
            Me.XrLabel_Agency_Address2.StylePriority.UseTextAlignment = False
            '
            'XrLabel_Date
            '
            Me.XrLabel_Date.LocationFloat = New DevExpress.Utils.PointFloat(308.0!, 0.0!)
            Me.XrLabel_Date.Name = "XrLabel_Date"
            Me.XrLabel_Date.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_Date.SizeF = New System.Drawing.SizeF(67.0!, 15.0!)
            Me.XrLabel_Date.StylePriority.UseTextAlignment = False
            Me.XrLabel_Date.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel_CheckNumber
            '
            Me.XrLabel_CheckNumber.LocationFloat = New DevExpress.Utils.PointFloat(383.0!, 0.0!)
            Me.XrLabel_CheckNumber.Name = "XrLabel_CheckNumber"
            Me.XrLabel_CheckNumber.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_CheckNumber.SizeF = New System.Drawing.SizeF(67.0!, 15.0!)
            Me.XrLabel_CheckNumber.StylePriority.UseTextAlignment = False
            Me.XrLabel_CheckNumber.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel_Creditor
            '
            Me.XrLabel_Creditor.LocationFloat = New DevExpress.Utils.PointFloat(1.0!, 0.0!)
            Me.XrLabel_Creditor.Name = "XrLabel_Creditor"
            Me.XrLabel_Creditor.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_Creditor.SizeF = New System.Drawing.SizeF(67.0!, 15.0!)
            Me.XrLabel_Creditor.StylePriority.UseTextAlignment = False
            Me.XrLabel_Creditor.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            '
            'XrLabel_CreditorName
            '
            Me.XrLabel_CreditorName.LocationFloat = New DevExpress.Utils.PointFloat(75.0!, 0.0!)
            Me.XrLabel_CreditorName.Name = "XrLabel_CreditorName"
            Me.XrLabel_CreditorName.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_CreditorName.SizeF = New System.Drawing.SizeF(225.0!, 15.0!)
            Me.XrLabel_CreditorName.StylePriority.UseTextAlignment = False
            Me.XrLabel_CreditorName.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            Me.XrLabel_CreditorName.WordWrap = False
            '
            'XrLabel_Gross
            '
            Me.XrLabel_Gross.LocationFloat = New DevExpress.Utils.PointFloat(458.0!, 0.0!)
            Me.XrLabel_Gross.Name = "XrLabel_Gross"
            Me.XrLabel_Gross.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_Gross.SizeF = New System.Drawing.SizeF(67.0!, 15.0!)
            Me.XrLabel_Gross.StylePriority.UseTextAlignment = False
            Me.XrLabel_Gross.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel_Deducted
            '
            Me.XrLabel_Deducted.LocationFloat = New DevExpress.Utils.PointFloat(533.0!, 0.0!)
            Me.XrLabel_Deducted.Name = "XrLabel_Deducted"
            Me.XrLabel_Deducted.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_Deducted.SizeF = New System.Drawing.SizeF(67.0!, 15.0!)
            Me.XrLabel_Deducted.StylePriority.UseTextAlignment = False
            Me.XrLabel_Deducted.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel_Billed
            '
            Me.XrLabel_Billed.LocationFloat = New DevExpress.Utils.PointFloat(608.0!, 0.0!)
            Me.XrLabel_Billed.Name = "XrLabel_Billed"
            Me.XrLabel_Billed.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_Billed.SizeF = New System.Drawing.SizeF(67.0!, 15.0!)
            Me.XrLabel_Billed.StylePriority.UseTextAlignment = False
            Me.XrLabel_Billed.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel_Net
            '
            Me.XrLabel_Net.LocationFloat = New DevExpress.Utils.PointFloat(683.0!, 0.0!)
            Me.XrLabel_Net.Name = "XrLabel_Net"
            Me.XrLabel_Net.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_Net.SizeF = New System.Drawing.SizeF(67.0!, 15.0!)
            Me.XrLabel_Net.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
            '
            'XrPanel1
            '
            Me.XrPanel1.BackColor = System.Drawing.Color.Teal
            Me.XrPanel1.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel8, Me.XrLabel7, Me.XrLabel6, Me.XrLabel5, Me.XrLabel4, Me.XrLabel2, Me.XrLabel1})
            Me.XrPanel1.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 133.0!)
            Me.XrPanel1.Name = "XrPanel1"
            Me.XrPanel1.SizeF = New System.Drawing.SizeF(750.0!, 17.0!)
            '
            'XrLabel8
            '
            Me.XrLabel8.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel8.ForeColor = System.Drawing.Color.White
            Me.XrLabel8.LocationFloat = New DevExpress.Utils.PointFloat(533.0!, 1.0!)
            Me.XrLabel8.Name = "XrLabel8"
            Me.XrLabel8.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel8.SizeF = New System.Drawing.SizeF(67.0!, 15.0!)
            Me.XrLabel8.Text = "DEDUCT"
            Me.XrLabel8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
            '
            'XrLabel7
            '
            Me.XrLabel7.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel7.ForeColor = System.Drawing.Color.White
            Me.XrLabel7.LocationFloat = New DevExpress.Utils.PointFloat(458.0!, 1.0!)
            Me.XrLabel7.Name = "XrLabel7"
            Me.XrLabel7.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel7.SizeF = New System.Drawing.SizeF(67.0!, 15.0!)
            Me.XrLabel7.Text = "GROSS"
            Me.XrLabel7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
            '
            'XrLabel6
            '
            Me.XrLabel6.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel6.ForeColor = System.Drawing.Color.White
            Me.XrLabel6.LocationFloat = New DevExpress.Utils.PointFloat(683.0!, 1.0!)
            Me.XrLabel6.Name = "XrLabel6"
            Me.XrLabel6.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel6.SizeF = New System.Drawing.SizeF(67.0!, 15.0!)
            Me.XrLabel6.Text = "NET"
            Me.XrLabel6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
            '
            'XrLabel5
            '
            Me.XrLabel5.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel5.ForeColor = System.Drawing.Color.White
            Me.XrLabel5.LocationFloat = New DevExpress.Utils.PointFloat(608.0!, 1.0!)
            Me.XrLabel5.Name = "XrLabel5"
            Me.XrLabel5.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel5.SizeF = New System.Drawing.SizeF(67.0!, 15.0!)
            Me.XrLabel5.Text = "BILLED"
            Me.XrLabel5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
            '
            'XrLabel4
            '
            Me.XrLabel4.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel4.ForeColor = System.Drawing.Color.White
            Me.XrLabel4.LocationFloat = New DevExpress.Utils.PointFloat(383.0!, 1.0!)
            Me.XrLabel4.Name = "XrLabel4"
            Me.XrLabel4.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel4.SizeF = New System.Drawing.SizeF(67.0!, 15.0!)
            Me.XrLabel4.Text = "CHECK#"
            Me.XrLabel4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
            '
            'XrLabel2
            '
            Me.XrLabel2.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel2.ForeColor = System.Drawing.Color.White
            Me.XrLabel2.LocationFloat = New DevExpress.Utils.PointFloat(1.0!, 1.0!)
            Me.XrLabel2.Name = "XrLabel2"
            Me.XrLabel2.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel2.SizeF = New System.Drawing.SizeF(192.0!, 15.0!)
            Me.XrLabel2.Text = "CREDITOR AND NAME"
            '
            'XrLabel1
            '
            Me.XrLabel1.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel1.ForeColor = System.Drawing.Color.White
            Me.XrLabel1.LocationFloat = New DevExpress.Utils.PointFloat(308.0!, 1.0!)
            Me.XrLabel1.Name = "XrLabel1"
            Me.XrLabel1.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel1.SizeF = New System.Drawing.SizeF(67.0!, 15.0!)
            Me.XrLabel1.Text = "DATE"
            Me.XrLabel1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
            '
            'ReportFooter
            '
            Me.ReportFooter.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLine1, Me.XrLabel11A, Me.XrLabel10A, Me.XrLabel9A, Me.XrLabel3A, Me.XrLabel_total_net, Me.XrLabel_total_deduct, Me.XrLabel_total_billed, Me.XrLabel_total_gross})
            Me.ReportFooter.HeightF = 105.0!
            Me.ReportFooter.KeepTogether = True
            Me.ReportFooter.Name = "ReportFooter"
            '
            'XrLine1
            '
            Me.XrLine1.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 17.0!)
            Me.XrLine1.Name = "XrLine1"
            Me.XrLine1.SizeF = New System.Drawing.SizeF(750.0!, 8.0!)
            '
            'XrLabel11A
            '
            Me.XrLabel11A.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel11A.LocationFloat = New DevExpress.Utils.PointFloat(225.0!, 84.0!)
            Me.XrLabel11A.Name = "XrLabel11A"
            Me.XrLabel11A.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel11A.SizeF = New System.Drawing.SizeF(150.0!, 17.0!)
            Me.XrLabel11A.StylePriority.UseFont = False
            Me.XrLabel11A.Text = "TOTAL NET"
            '
            'XrLabel10A
            '
            Me.XrLabel10A.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel10A.LocationFloat = New DevExpress.Utils.PointFloat(225.0!, 67.0!)
            Me.XrLabel10A.Name = "XrLabel10A"
            Me.XrLabel10A.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel10A.SizeF = New System.Drawing.SizeF(150.0!, 17.0!)
            Me.XrLabel10A.StylePriority.UseFont = False
            Me.XrLabel10A.Text = "TOTAL BILLED"
            '
            'XrLabel9A
            '
            Me.XrLabel9A.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel9A.LocationFloat = New DevExpress.Utils.PointFloat(225.0!, 50.0!)
            Me.XrLabel9A.Name = "XrLabel9A"
            Me.XrLabel9A.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel9A.SizeF = New System.Drawing.SizeF(150.0!, 17.0!)
            Me.XrLabel9A.StylePriority.UseFont = False
            Me.XrLabel9A.Text = "TOTAL DEDUCT"
            '
            'XrLabel3A
            '
            Me.XrLabel3A.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel3A.LocationFloat = New DevExpress.Utils.PointFloat(225.0!, 33.0!)
            Me.XrLabel3A.Name = "XrLabel3A"
            Me.XrLabel3A.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel3A.SizeF = New System.Drawing.SizeF(150.0!, 17.0!)
            Me.XrLabel3A.StylePriority.UseFont = False
            Me.XrLabel3A.Text = "TOTAL GROSS"
            '
            'XrLabel_total_net
            '
            Me.XrLabel_total_net.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel_total_net.LocationFloat = New DevExpress.Utils.PointFloat(383.0!, 84.0!)
            Me.XrLabel_total_net.Name = "XrLabel_total_net"
            Me.XrLabel_total_net.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_total_net.SizeF = New System.Drawing.SizeF(142.0!, 15.0!)
            Me.XrLabel_total_net.StylePriority.UseFont = False
            XrSummary1.FormatString = "{0:c}"
            XrSummary1.Running = DevExpress.XtraReports.UI.SummaryRunning.Report
            Me.XrLabel_total_net.Summary = XrSummary1
            Me.XrLabel_total_net.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
            '
            'XrLabel_total_deduct
            '
            Me.XrLabel_total_deduct.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel_total_deduct.LocationFloat = New DevExpress.Utils.PointFloat(383.0!, 50.0!)
            Me.XrLabel_total_deduct.Name = "XrLabel_total_deduct"
            Me.XrLabel_total_deduct.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_total_deduct.SizeF = New System.Drawing.SizeF(142.0!, 15.0!)
            Me.XrLabel_total_deduct.StylePriority.UseFont = False
            XrSummary2.FormatString = "{0:c}"
            XrSummary2.Running = DevExpress.XtraReports.UI.SummaryRunning.Report
            Me.XrLabel_total_deduct.Summary = XrSummary2
            Me.XrLabel_total_deduct.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
            '
            'XrLabel_total_billed
            '
            Me.XrLabel_total_billed.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel_total_billed.LocationFloat = New DevExpress.Utils.PointFloat(383.0!, 67.0!)
            Me.XrLabel_total_billed.Name = "XrLabel_total_billed"
            Me.XrLabel_total_billed.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_total_billed.SizeF = New System.Drawing.SizeF(142.0!, 15.0!)
            Me.XrLabel_total_billed.StylePriority.UseFont = False
            XrSummary3.FormatString = "{0:c}"
            XrSummary3.Running = DevExpress.XtraReports.UI.SummaryRunning.Report
            Me.XrLabel_total_billed.Summary = XrSummary3
            Me.XrLabel_total_billed.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
            '
            'XrLabel_total_gross
            '
            Me.XrLabel_total_gross.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel_total_gross.LocationFloat = New DevExpress.Utils.PointFloat(383.0!, 33.0!)
            Me.XrLabel_total_gross.Name = "XrLabel_total_gross"
            Me.XrLabel_total_gross.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_total_gross.SizeF = New System.Drawing.SizeF(142.0!, 15.0!)
            Me.XrLabel_total_gross.StylePriority.UseFont = False
            XrSummary4.FormatString = "{0:c}"
            XrSummary4.Running = DevExpress.XtraReports.UI.SummaryRunning.Report
            Me.XrLabel_total_gross.Summary = XrSummary4
            Me.XrLabel_total_gross.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
            '
            Me.XrLabel_total_gross.DataBindings.Add("Text", Nothing, "gross")
            Me.XrLabel_total_billed.DataBindings.Add("Text", Nothing, "billed")
            Me.XrLabel_total_deduct.DataBindings.Add("Text", Nothing, "deducted")
            Me.XrLabel_total_net.DataBindings.Add("Text", Nothing, "net")
            Me.XrLabel_Billed.DataBindings.Add("Text", Nothing, "billed", "{0:c}")
            Me.XrLabel_Billed.DataBindings.Add("Tag", Nothing, "id")
            Me.XrLabel_Deducted.DataBindings.Add("Text", Nothing, "deducted", "{0:c}")
            Me.XrLabel_Deducted.DataBindings.Add("Tag", Nothing, "id")
            Me.XrLabel_Gross.DataBindings.Add("Text", Nothing, "gross", "{0:c}")
            Me.XrLabel_Gross.DataBindings.Add("Tag", Nothing, "id")
            Me.XrLabel_Net.DataBindings.Add("Text", Nothing, "net", "{0:c}")
            Me.XrLabel_Net.DataBindings.Add("Tag", Nothing, "id")
            Me.XrLabel_CheckNumber.DataBindings.Add("Text", Nothing, "checknum")
            Me.XrLabel_CheckNumber.DataBindings.Add("Tag", Nothing, "id")
            Me.XrLabel_Creditor.DataBindings.Add("Text", Nothing, "creditor")
            Me.XrLabel_Creditor.DataBindings.Add("Tag", Nothing, "id")
            Me.XrLabel_CreditorName.DataBindings.Add("Text", Nothing, "creditor_name")
            Me.XrLabel_CreditorName.DataBindings.Add("Tag", Nothing, "id")
            Me.XrLabel_Date.DataBindings.Add("Text", Nothing, "date_created", "{0:d}")
            Me.XrLabel_Date.DataBindings.Add("Tag", Nothing, "id")
            '
            'DisbursementDetailInformationReport
            '
            Me.Bands.AddRange(New DevExpress.XtraReports.UI.Band() {Me.Detail, Me.PageHeader, Me.PageFooter, Me.ReportFooter})
            Me.Version = "9.3"
            Me.Controls.SetChildIndex(Me.ReportFooter, 0)
            Me.Controls.SetChildIndex(Me.PageFooter, 0)
            Me.Controls.SetChildIndex(Me.PageHeader, 0)
            Me.Controls.SetChildIndex(Me.Detail, 0)
            CType(Me, System.ComponentModel.ISupportInitialize).EndInit()

        End Sub
        Protected Friend WithEvents XrLabel_Date As DevExpress.XtraReports.UI.XRLabel
        Protected Friend WithEvents XrLabel_CheckNumber As DevExpress.XtraReports.UI.XRLabel
        Protected Friend WithEvents XrLabel_Creditor As DevExpress.XtraReports.UI.XRLabel
        Protected Friend WithEvents XrLabel_CreditorName As DevExpress.XtraReports.UI.XRLabel
        Protected Friend WithEvents XrLabel_Gross As DevExpress.XtraReports.UI.XRLabel
        Protected Friend WithEvents XrLabel_Deducted As DevExpress.XtraReports.UI.XRLabel
        Protected Friend WithEvents XrLabel_Billed As DevExpress.XtraReports.UI.XRLabel
        Protected Friend WithEvents XrLabel_Net As DevExpress.XtraReports.UI.XRLabel
        Protected Friend WithEvents XrPanel1 As DevExpress.XtraReports.UI.XRPanel
        Protected Friend WithEvents XrLabel1 As DevExpress.XtraReports.UI.XRLabel
        Protected Friend WithEvents XrLabel2 As DevExpress.XtraReports.UI.XRLabel
        Protected Friend WithEvents XrLabel4 As DevExpress.XtraReports.UI.XRLabel
        Protected Friend WithEvents XrLabel5 As DevExpress.XtraReports.UI.XRLabel
        Protected Friend WithEvents XrLabel6 As DevExpress.XtraReports.UI.XRLabel
        Protected Friend WithEvents XrLabel7 As DevExpress.XtraReports.UI.XRLabel
        Protected Friend WithEvents XrLabel8 As DevExpress.XtraReports.UI.XRLabel
        Protected Friend WithEvents XrLabel_total_net As DevExpress.XtraReports.UI.XRLabel
        Protected Friend WithEvents XrLabel_total_deduct As DevExpress.XtraReports.UI.XRLabel
        Protected Friend WithEvents XrLabel_total_billed As DevExpress.XtraReports.UI.XRLabel
        Protected Friend WithEvents XrLabel_total_gross As DevExpress.XtraReports.UI.XRLabel
        Protected Friend WithEvents XrLine1 As DevExpress.XtraReports.UI.XRLine
        Protected Friend WithEvents XrLabel11A As DevExpress.XtraReports.UI.XRLabel
        Protected Friend WithEvents XrLabel10A As DevExpress.XtraReports.UI.XRLabel
        Protected Friend WithEvents XrLabel9A As DevExpress.XtraReports.UI.XRLabel
        Protected Friend WithEvents XrLabel3A As DevExpress.XtraReports.UI.XRLabel
        Protected Friend WithEvents ReportFooter As DevExpress.XtraReports.UI.ReportFooterBand
    End Class

End Namespace