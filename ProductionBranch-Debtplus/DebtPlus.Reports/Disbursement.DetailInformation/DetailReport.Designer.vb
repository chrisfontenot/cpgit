Namespace Disbursement.DetailInformation

    <Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
    Partial Class DetailReport

        'XtraReport overrides dispose to clean up the component list.
        <System.Diagnostics.DebuggerNonUserCode()> _
        Protected Overrides Sub Dispose(ByVal disposing As Boolean)
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
            MyBase.Dispose(disposing)
        End Sub

        'Required by the Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Designer
        'It can be modified using the Designer.  
        'Do not modify it using the code editor.
        <System.Diagnostics.DebuggerStepThrough()> _
        Private Sub InitializeComponent()
            Me.XrLabel_client = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_client_name = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel3 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel9 = New DevExpress.XtraReports.UI.XRLabel
            CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
            '
            'XrLabel_Gross
            '
            Me.XrLabel_Gross.Location = New System.Drawing.Point(758, 0)
            '
            'XrLabel_Deducted
            '
            Me.XrLabel_Deducted.Location = New System.Drawing.Point(833, 0)
            '
            'XrLabel_Billed
            '
            Me.XrLabel_Billed.Location = New System.Drawing.Point(908, 0)
            '
            'XrLabel_Net
            '
            Me.XrLabel_Net.Location = New System.Drawing.Point(983, 0)
            '
            'XrPanel1
            '
            Me.XrPanel1.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel9, Me.XrLabel3})
            Me.XrPanel1.Size = New System.Drawing.Size(1050, 17)
            '
            'XrLabel5
            '
            Me.XrLabel5.Location = New System.Drawing.Point(908, 1)
            '
            'XrLabel6
            '
            Me.XrLabel6.Location = New System.Drawing.Point(983, 1)
            '
            'XrLabel7
            '
            Me.XrLabel7.Location = New System.Drawing.Point(758, 1)
            '
            'XrLabel8
            '
            Me.XrLabel8.Location = New System.Drawing.Point(833, 1)
            '
            'XrLabel_total_net
            '
            Me.XrLabel_total_net.Location = New System.Drawing.Point(683, 84)
            Me.XrLabel_total_net.StylePriority.UseFont = False
            '
            'XrLabel_total_deduct
            '
            Me.XrLabel_total_deduct.Location = New System.Drawing.Point(683, 50)
            Me.XrLabel_total_deduct.StylePriority.UseFont = False
            '
            'XrLabel_total_billed
            '
            Me.XrLabel_total_billed.Location = New System.Drawing.Point(683, 67)
            Me.XrLabel_total_billed.StylePriority.UseFont = False
            '
            'XrLabel_total_gross
            '
            Me.XrLabel_total_gross.Location = New System.Drawing.Point(683, 33)
            Me.XrLabel_total_gross.StylePriority.UseFont = False
            '
            'XrLine1
            '
            Me.XrLine1.Size = New System.Drawing.Size(1050, 8)
            '
            'XrLabel11A
            '
            Me.XrLabel11A.Location = New System.Drawing.Point(525, 84)
            Me.XrLabel11A.StylePriority.UseFont = False
            '
            'XrLabel10A
            '
            Me.XrLabel10A.Location = New System.Drawing.Point(525, 67)
            Me.XrLabel10A.StylePriority.UseFont = False
            '
            'XrLabel9A
            '
            Me.XrLabel9A.Location = New System.Drawing.Point(525, 50)
            Me.XrLabel9A.StylePriority.UseFont = False
            '
            'XrLabel3A
            '
            Me.XrLabel3A.Location = New System.Drawing.Point(525, 33)
            Me.XrLabel3A.StylePriority.UseFont = False
            '
            'Detail
            '
            Me.Detail.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel_client_name, Me.XrLabel_client})
            '
            'XrPageInfo_PageNumber
            '
            Me.XrPageInfo_PageNumber.Location = New System.Drawing.Point(917, 8)
            '
            'XrPanel_AgencyAddress
            '
            Me.XrPanel_AgencyAddress.Location = New System.Drawing.Point(742, 0)
            '
            'XrLabel_client
            '
            Me.XrLabel_client.Location = New System.Drawing.Point(459, 0)
            Me.XrLabel_client.Name = "XrLabel_client"
            Me.XrLabel_client.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_client.Size = New System.Drawing.Size(75, 15)
            Me.XrLabel_client.StylePriority.UseTextAlignment = False
            Me.XrLabel_client.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
            '
            'XrLabel_client_name
            '
            Me.XrLabel_client_name.Location = New System.Drawing.Point(542, 0)
            Me.XrLabel_client_name.Name = "XrLabel_client_name"
            Me.XrLabel_client_name.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_client_name.Size = New System.Drawing.Size(208, 15)
            '
            'XrLabel3
            '
            Me.XrLabel3.BorderColor = System.Drawing.Color.White
            Me.XrLabel3.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel3.ForeColor = System.Drawing.Color.White
            Me.XrLabel3.Location = New System.Drawing.Point(467, 1)
            Me.XrLabel3.Name = "XrLabel3"
            Me.XrLabel3.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel3.Size = New System.Drawing.Size(67, 15)
            Me.XrLabel3.StylePriority.UseBorderColor = False
            Me.XrLabel3.StylePriority.UseFont = False
            Me.XrLabel3.StylePriority.UseForeColor = False
            Me.XrLabel3.StylePriority.UseTextAlignment = False
            Me.XrLabel3.Text = "CLIENT"
            Me.XrLabel3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
            '
            'XrLabel9
            '
            Me.XrLabel9.BorderColor = System.Drawing.Color.White
            Me.XrLabel9.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel9.ForeColor = System.Drawing.Color.White
            Me.XrLabel9.Location = New System.Drawing.Point(542, 1)
            Me.XrLabel9.Name = "XrLabel9"
            Me.XrLabel9.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel9.Size = New System.Drawing.Size(67, 15)
            Me.XrLabel9.StylePriority.UseBorderColor = False
            Me.XrLabel9.StylePriority.UseFont = False
            Me.XrLabel9.StylePriority.UseForeColor = False
            Me.XrLabel9.StylePriority.UseTextAlignment = False
            Me.XrLabel9.Text = "NAME"
            Me.XrLabel9.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
            '
            'DetailReport
            '
            Me.Landscape = True
            Me.PageHeight = 850
            Me.PageWidth = 1100
            Me.Version = "8.2"
            CType(Me, System.ComponentModel.ISupportInitialize).EndInit()

        End Sub
        Friend WithEvents XrLabel9 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel3 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_client_name As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_client As DevExpress.XtraReports.UI.XRLabel
    End Class

End Namespace