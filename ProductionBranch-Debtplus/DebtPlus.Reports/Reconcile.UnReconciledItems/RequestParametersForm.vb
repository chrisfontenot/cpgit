#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}
#End Region

Option Compare Binary
Option Explicit On
Option Strict On

Namespace Reconcile.UnReconciledItems
    Public Class RequestParametersForm
        Inherits DebtPlus.Reports.Template.Forms.DateReportParametersForm

#Region " Windows Form Designer generated code "

        Public Sub New()
            MyBase.New()

            'This call is required by the Windows Form Designer.
            InitializeComponent()

            'Add any initialization after the InitializeComponent() call

        End Sub

        'Form overrides dispose to clean up the component list.
        Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
            If disposing Then
                If components IsNot Nothing Then
                    components.Dispose()
                End If
            End If
            MyBase.Dispose(disposing)
        End Sub

        'Required by the Windows Form Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Windows Form Designer
        'It can be modified using the Windows Form Designer.  
        'Do not modify it using the code editor.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
            Dim ComboboxItem1 As DebtPlus.Data.Controls.ComboboxItem = New DebtPlus.Data.Controls.ComboboxItem()
            CType(Me.XrGroup_param_08_1, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.XrGroup_param_08_1.SuspendLayout()
            CType(Me.XrCombo_param_08_1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.XrDate_param_08_2.Properties.VistaTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.XrDate_param_08_2.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.XrDate_param_08_1.Properties.VistaTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.XrDate_param_08_1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.DxErrorProvider1, System.ComponentModel.ISupportInitialize).BeginInit()

            Me.SuspendLayout()
            '
            'XrGroup_param_08_1
            '
            Me.XrGroup_param_08_1.Size = New System.Drawing.Size(216, 80)
            '
            'XrCombo_param_08_1
            '
            ComboboxItem1.tag = Nothing
            ComboboxItem1.value = DebtPlus.Utils.DateRange.Today
            Me.XrCombo_param_08_1.EditValue = ComboboxItem1
            '
            'XrDate_param_08_2
            '
            Me.XrDate_param_08_2.Location = New System.Drawing.Point(112, 52)
            '
            'XrDate_param_08_1
            '
            Me.XrDate_param_08_1.Location = New System.Drawing.Point(112, 88)
            Me.XrDate_param_08_1.Visible = False
            '
            'Label3
            '
            Me.Label3.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center
            Me.Label3.Location = New System.Drawing.Point(16, 55)
            Me.Label3.Size = New System.Drawing.Size(61, 13)
            Me.Label3.Text = "&Cutoff Date:"
            '
            'Label4
            '
            Me.Label4.Location = New System.Drawing.Point(16, 88)
            Me.Label4.Visible = False
            'RequestParametersForm
            '
            Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
            Me.ClientSize = New System.Drawing.Size(336, 96)
            Me.Name = "RequestParametersForm"

            CType(Me.XrGroup_param_08_1, System.ComponentModel.ISupportInitialize).EndInit()
            Me.XrGroup_param_08_1.ResumeLayout(False)
            Me.XrGroup_param_08_1.PerformLayout()
            CType(Me.XrCombo_param_08_1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.XrDate_param_08_2.Properties.VistaTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.XrDate_param_08_2.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.XrDate_param_08_1.Properties.VistaTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.XrDate_param_08_1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.DxErrorProvider1, System.ComponentModel.ISupportInitialize).EndInit()
            Me.ResumeLayout(False)

        End Sub

#End Region

    End Class
End Namespace