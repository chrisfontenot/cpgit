#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}
#End Region
Imports DebtPlus.Interfaces.Reports

Namespace Reconcile.UnReconciledItems
    Public Class UnReconciledItemsReport
        Inherits Template.TemplateXtraReportClass

        ''' <summary>
        ''' Create an instance of our report
        ''' </summary>
        Public Sub New()
            MyBase.new()
            InitializeComponent()
            AddHandler XrPanel_Detail.BeforePrint, AddressOf XrPanel_Detail_BeforePrint
            AddHandler BeforePrint, AddressOf Report_BeforePrint
            ReportFilter.IsEnabled = True
        End Sub

        ''' <summary>
        ''' Cutoff date for the report
        ''' </summary>
        Private _Parameter_ToDate As Date = Today
        Public Property Parameter_ToDate() As Date
            Get
                Return _Parameter_ToDate
            End Get
            Set(ByVal Value As Date)
                _Parameter_ToDate = Value
            End Set
        End Property


        ''' <summary>
        ''' Set the parameters for the report
        ''' </summary>
        Public Overrides Sub SetReportParameter(ByVal Parameter As String, ByVal Value As Object)
            Select Case Parameter
                Case "FromDate"
                    ' This is not a valid item
                Case Else
                    MyBase.SetReportParameter(Parameter, Value)
            End Select
        End Sub


        ''' <summary>
        ''' Report subtitle
        ''' </summary>
        Public Overrides ReadOnly Property ReportSubtitle() As String
            Get
                Return System.String.Format("This report covers all checks up to and including {0:d}", Parameter_ToDate)
            End Get
        End Property


        ''' <summary>
        ''' Report title
        ''' </summary>
        Public Overrides ReadOnly Property ReportTitle() As String
            Get
                Return "UnReconciled Bank Items"
            End Get
        End Property


        ''' <summary>
        ''' Request the parameters from the user
        ''' </summary>
        Public Overrides Function RequestReportParameters() As System.Windows.Forms.DialogResult
            Dim answer As System.Windows.Forms.DialogResult
            Using frm As New RequestParametersForm
                answer = frm.ShowDialog()
                If answer = DialogResult.OK Then Parameter_ToDate = frm.Parameter_ToDate
            End Using
            Return answer
        End Function

#Region " Component Designer generated code "

        'Component overrides dispose to clean up the component list.
        Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
            If disposing Then
                If components IsNot Nothing Then
                    components.Dispose()
                End If
            End If
            MyBase.Dispose(disposing)
        End Sub

        'Required by the Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Designer
        'It can be modified using the Designer.
        'Do not modify it using the code editor.
        Friend WithEvents XrPanel1 As DevExpress.XtraReports.UI.XRPanel
        Friend WithEvents XrLabel1 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel2 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel3 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel4 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel5 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents ReportFooter As DevExpress.XtraReports.UI.ReportFooterBand
        Friend WithEvents XrLabel10 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLine1 As DevExpress.XtraReports.UI.XRLine
        Friend WithEvents XrLabel_Total_Credit As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_Total_Debit As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_Total_Payee As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_Note As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrPanel_Detail As DevExpress.XtraReports.UI.XRPanel
        Friend WithEvents XrLabel_Credit As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_Debit As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_Date As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_checknum As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_Payee As DevExpress.XtraReports.UI.XRLabel
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
            Dim XrSummary1 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary
            Dim XrSummary2 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary
            Dim XrSummary3 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary
            Me.XrPanel1 = New DevExpress.XtraReports.UI.XRPanel
            Me.XrLabel5 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel4 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel3 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel2 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel1 = New DevExpress.XtraReports.UI.XRLabel
            Me.ReportFooter = New DevExpress.XtraReports.UI.ReportFooterBand
            Me.XrLabel_Note = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_Total_Payee = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_Total_Debit = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_Total_Credit = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLine1 = New DevExpress.XtraReports.UI.XRLine
            Me.XrLabel10 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrPanel_Detail = New DevExpress.XtraReports.UI.XRPanel
            Me.XrLabel_Payee = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_checknum = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_Date = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_Debit = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_Credit = New DevExpress.XtraReports.UI.XRLabel
            CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
            '
            'XRLabel_Agency_Name
            '
            Me.XRLabel_Agency_Name.ParentStyleUsing.UseFont = False
            Me.XRLabel_Agency_Name.ParentStyleUsing.UseForeColor = False
            '
            'XrLine_Title
            '
            Me.XrLine_Title.ParentStyleUsing.UseForeColor = False
            '
            'XrPageInfo1
            '
            Me.XrPageInfo1.ParentStyleUsing.UseFont = False
            Me.XrPageInfo1.ParentStyleUsing.UseForeColor = False
            '
            'XrPageInfo_PageNumber
            '
            Me.XrPageInfo_PageNumber.ParentStyleUsing.UseFont = False
            Me.XrPageInfo_PageNumber.ParentStyleUsing.UseForeColor = False
            '
            'XrLabel_Subtitle
            '
            Me.XrLabel_Subtitle.ParentStyleUsing.UseFont = False
            Me.XrLabel_Subtitle.ParentStyleUsing.UseForeColor = False
            '
            'Detail
            '
            Me.Detail.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrPanel_Detail})
            Me.Detail.Height = 15
            '
            'XrLabel_Title
            '
            Me.XrLabel_Title.ParentStyleUsing.UseFont = False
            Me.XrLabel_Title.ParentStyleUsing.UseForeColor = False
            '
            'XrLabel_Agency_Address3
            '
            Me.XrLabel_Agency_Address3.ParentStyleUsing.UseFont = False
            Me.XrLabel_Agency_Address3.ParentStyleUsing.UseForeColor = False
            '
            'XrLabel_Agency_Address2
            '
            Me.XrLabel_Agency_Address2.ParentStyleUsing.UseFont = False
            Me.XrLabel_Agency_Address2.ParentStyleUsing.UseForeColor = False
            '
            'XrLabel_Agency_Address1
            '
            Me.XrLabel_Agency_Address1.ParentStyleUsing.UseFont = False
            Me.XrLabel_Agency_Address1.ParentStyleUsing.UseForeColor = False
            '
            'XrLabel_Agency_Phone
            '
            Me.XrLabel_Agency_Phone.ParentStyleUsing.UseFont = False
            Me.XrLabel_Agency_Phone.ParentStyleUsing.UseForeColor = False
            '
            'PageHeader
            '
            Me.PageHeader.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrPanel1})
            Me.PageHeader.Height = 139
            '
            'XrPanel1
            '
            Me.XrPanel1.BackColor = System.Drawing.Color.Teal
            Me.XrPanel1.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel5, Me.XrLabel4, Me.XrLabel3, Me.XrLabel2, Me.XrLabel1})
            Me.XrPanel1.Location = New System.Drawing.Point(0, 117)
            Me.XrPanel1.Name = "XrPanel1"
            Me.XrPanel1.ParentStyleUsing.UseBackColor = False
            Me.XrPanel1.Size = New System.Drawing.Size(750, 15)
            '
            'XrLabel5
            '
            Me.XrLabel5.CanGrow = False
            Me.XrLabel5.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel5.ForeColor = System.Drawing.Color.White
            Me.XrLabel5.Location = New System.Drawing.Point(277, 0)
            Me.XrLabel5.Name = "XrLabel5"
            Me.XrLabel5.ParentStyleUsing.UseFont = False
            Me.XrLabel5.ParentStyleUsing.UseForeColor = False
            Me.XrLabel5.Size = New System.Drawing.Size(72, 15)
            Me.XrLabel5.Text = "DEBIT"
            Me.XrLabel5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrLabel5.WordWrap = False
            '
            'XrLabel4
            '
            Me.XrLabel4.CanGrow = False
            Me.XrLabel4.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel4.ForeColor = System.Drawing.Color.White
            Me.XrLabel4.Location = New System.Drawing.Point(358, 0)
            Me.XrLabel4.Name = "XrLabel4"
            Me.XrLabel4.ParentStyleUsing.UseFont = False
            Me.XrLabel4.ParentStyleUsing.UseForeColor = False
            Me.XrLabel4.Size = New System.Drawing.Size(390, 15)
            Me.XrLabel4.Text = "PAYEE"
            Me.XrLabel4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            Me.XrLabel4.WordWrap = False
            '
            'XrLabel3
            '
            Me.XrLabel3.CanGrow = False
            Me.XrLabel3.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel3.ForeColor = System.Drawing.Color.White
            Me.XrLabel3.Location = New System.Drawing.Point(166, 0)
            Me.XrLabel3.Name = "XrLabel3"
            Me.XrLabel3.ParentStyleUsing.UseFont = False
            Me.XrLabel3.ParentStyleUsing.UseForeColor = False
            Me.XrLabel3.Size = New System.Drawing.Size(100, 15)
            Me.XrLabel3.Text = "CHECK #"
            Me.XrLabel3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrLabel3.WordWrap = False
            '
            'XrLabel2
            '
            Me.XrLabel2.CanGrow = False
            Me.XrLabel2.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel2.ForeColor = System.Drawing.Color.White
            Me.XrLabel2.Location = New System.Drawing.Point(85, 0)
            Me.XrLabel2.Name = "XrLabel2"
            Me.XrLabel2.ParentStyleUsing.UseFont = False
            Me.XrLabel2.ParentStyleUsing.UseForeColor = False
            Me.XrLabel2.Size = New System.Drawing.Size(72, 15)
            Me.XrLabel2.Text = "CREDIT"
            Me.XrLabel2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrLabel2.WordWrap = False
            '
            'XrLabel1
            '
            Me.XrLabel1.CanGrow = False
            Me.XrLabel1.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel1.ForeColor = System.Drawing.Color.White
            Me.XrLabel1.Location = New System.Drawing.Point(8, 0)
            Me.XrLabel1.Name = "XrLabel1"
            Me.XrLabel1.ParentStyleUsing.UseFont = False
            Me.XrLabel1.ParentStyleUsing.UseForeColor = False
            Me.XrLabel1.Size = New System.Drawing.Size(66, 15)
            Me.XrLabel1.Text = "DATE"
            Me.XrLabel1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrLabel1.WordWrap = False
            '
            'ReportFooter
            '
            Me.ReportFooter.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel_Note, Me.XrLabel_Total_Payee, Me.XrLabel_Total_Debit, Me.XrLabel_Total_Credit, Me.XrLine1, Me.XrLabel10})
            Me.ReportFooter.Height = 76
            Me.ReportFooter.Name = "ReportFooter"
            '
            'XrLabel_Note
            '
            Me.XrLabel_Note.ForeColor = System.Drawing.Color.Gray
            Me.XrLabel_Note.Location = New System.Drawing.Point(8, 42)
            Me.XrLabel_Note.Multiline = True
            Me.XrLabel_Note.Name = "XrLabel_Note"
            Me.XrLabel_Note.ParentStyleUsing.UseForeColor = False
            Me.XrLabel_Note.Size = New System.Drawing.Size(400, 34)
            Me.XrLabel_Note.Text = "Note" & Microsoft.VisualBasic.ChrW(13) & Microsoft.VisualBasic.ChrW(10) & "   Items which have been voided after the cutoff date are shown in red."
            '
            'XrLabel_Total_Payee
            '
            Me.XrLabel_Total_Payee.Location = New System.Drawing.Point(358, 17)
            Me.XrLabel_Total_Payee.Name = "XrLabel_Total_Payee"
            Me.XrLabel_Total_Payee.Size = New System.Drawing.Size(390, 15)
            XrSummary1.FormatString = "{0:#,#} Item(s)"
            XrSummary1.Func = DevExpress.XtraReports.UI.SummaryFunc.Count
            XrSummary1.Running = DevExpress.XtraReports.UI.SummaryRunning.Report
            Me.XrLabel_Total_Payee.Summary = XrSummary1
            Me.XrLabel_Total_Payee.Text = "XrLabel_Total_Payee"
            '
            'XrLabel_Total_Debit
            '
            Me.XrLabel_Total_Debit.Location = New System.Drawing.Point(167, 17)
            Me.XrLabel_Total_Debit.Name = "XrLabel_Total_Debit"
            Me.XrLabel_Total_Debit.Size = New System.Drawing.Size(182, 15)
            XrSummary2.FormatString = "{0:c}"
            XrSummary2.Running = DevExpress.XtraReports.UI.SummaryRunning.Report
            Me.XrLabel_Total_Debit.Summary = XrSummary2
            Me.XrLabel_Total_Debit.Text = "XrLabel_Total_Debit"
            Me.XrLabel_Total_Debit.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel_Total_Credit
            '
            Me.XrLabel_Total_Credit.Location = New System.Drawing.Point(58, 17)
            Me.XrLabel_Total_Credit.Name = "XrLabel_Total_Credit"
            Me.XrLabel_Total_Credit.Size = New System.Drawing.Size(99, 15)
            XrSummary3.FormatString = "{0:c}"
            XrSummary3.Running = DevExpress.XtraReports.UI.SummaryRunning.Report
            Me.XrLabel_Total_Credit.Summary = XrSummary3
            Me.XrLabel_Total_Credit.Text = "XrLabel_Total_Credit"
            Me.XrLabel_Total_Credit.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLine1
            '
            Me.XrLine1.Location = New System.Drawing.Point(0, 8)
            Me.XrLine1.Name = "XrLine1"
            Me.XrLine1.Size = New System.Drawing.Size(484, 9)
            '
            'XrLabel10
            '
            Me.XrLabel10.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel10.Location = New System.Drawing.Point(8, 17)
            Me.XrLabel10.Name = "XrLabel10"
            Me.XrLabel10.ParentStyleUsing.UseFont = False
            Me.XrLabel10.Size = New System.Drawing.Size(59, 15)
            Me.XrLabel10.Text = "TOTAL"
            '
            'XrPanel_Detail
            '
            Me.XrPanel_Detail.BorderColor = System.Drawing.Color.Black
            Me.XrPanel_Detail.BorderWidth = 0
            Me.XrPanel_Detail.CanGrow = False
            Me.XrPanel_Detail.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel_Payee, Me.XrLabel_checknum, Me.XrLabel_Date, Me.XrLabel_Debit, Me.XrLabel_Credit})
            Me.XrPanel_Detail.Location = New System.Drawing.Point(0, 0)
            Me.XrPanel_Detail.Name = "XrPanel_Detail"
            Me.XrPanel_Detail.ParentStyleUsing.UseBorderColor = False
            Me.XrPanel_Detail.ParentStyleUsing.UseBorderWidth = False
            Me.XrPanel_Detail.ParentStyleUsing.UseForeColor = False
            Me.XrPanel_Detail.Size = New System.Drawing.Size(758, 15)
            '
            'XrLabel_Payee
            '
            Me.XrLabel_Payee.Location = New System.Drawing.Point(358, 0)
            Me.XrLabel_Payee.Name = "XrLabel_Payee"
            Me.XrLabel_Payee.Size = New System.Drawing.Size(390, 15)
            '
            'XrLabel_checknum
            '
            Me.XrLabel_checknum.Location = New System.Drawing.Point(166, 0)
            Me.XrLabel_checknum.Name = "XrLabel_checknum"
            Me.XrLabel_checknum.Size = New System.Drawing.Size(100, 15)
            Me.XrLabel_checknum.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel_Date
            '
            Me.XrLabel_Date.Location = New System.Drawing.Point(0, 0)
            Me.XrLabel_Date.Name = "XrLabel_Date"
            Me.XrLabel_Date.Size = New System.Drawing.Size(74, 15)
            Me.XrLabel_Date.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel_Debit
            '
            Me.XrLabel_Debit.Location = New System.Drawing.Point(277, 0)
            Me.XrLabel_Debit.Name = "XrLabel_Debit"
            Me.XrLabel_Debit.Size = New System.Drawing.Size(72, 15)
            Me.XrLabel_Debit.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel_Credit
            '
            Me.XrLabel_Credit.Location = New System.Drawing.Point(85, 0)
            Me.XrLabel_Credit.Name = "XrLabel_Credit"
            Me.XrLabel_Credit.Size = New System.Drawing.Size(72, 15)
            Me.XrLabel_Credit.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            Me.XrLabel_Date.DataBindings.Add("Text", Nothing, "date", "{0:d}")
            Me.XrLabel_Total_Payee.DataBindings.Add("Text", Nothing, "date", "{0:d}")
            Me.XrLabel_checknum.DataBindings.Add("Text", Nothing, "checknum")
            Me.XrLabel_Payee.DataBindings.Add("Text", Nothing, "payee")
            Me.XrLabel_Credit.DataBindings.Add("Text", Nothing, "credit", "{0:c}")
            Me.XrLabel_Total_Credit.DataBindings.Add("Text", Nothing, "credit")
            Me.XrLabel_Debit.DataBindings.Add("Text", Nothing, "debit", "{0:c}")
            Me.XrLabel_Total_Debit.DataBindings.Add("Text", Nothing, "debit")
            '
            'UnReconciledItemsReport
            '
            Me.Bands.AddRange(New DevExpress.XtraReports.UI.Band() {Me.ReportFooter})
            CType(Me, System.ComponentModel.ISupportInitialize).EndInit()

        End Sub

#End Region


        ''' <summary>
        ''' If the item is void then make the detail line RED. Otherwise BLACK
        ''' </summary>
        Private Sub XrPanel_Detail_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs)

            ' Change the void items to RED. All others are Black
            Dim Cleared As String = Convert.ToString(GetCurrentColumnValue("cleared")).ToUpper()
            If Cleared = "V" Then
                XrPanel_Detail.ForeColor = System.Drawing.Color.Red
            Else
                XrPanel_Detail.ForeColor = System.Drawing.Color.Black
            End If
        End Sub

        Dim ds As New System.Data.DataSet("ds")

        ''' <summary>
        ''' Process the initial start of the report
        ''' </summary>
        Private Sub Report_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs)
            Dim rpt As DevExpress.XtraReports.UI.XtraReport = CType(sender, DevExpress.XtraReports.UI.XtraReport)
            Const TableName As String = "rpt_UnReconciled_Bank_Items"
            Dim tbl As System.Data.DataTable = ds.Tables(TableName)

            If tbl Is Nothing Then
                Using cmd As SqlClient.SqlCommand = New System.Data.SqlClient.SqlCommand()
                    With cmd
                        .Connection = New System.Data.SqlClient.SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
                        .CommandText = "rpt_UnReconciled_Bank_Items"
                        .CommandType = CommandType.StoredProcedure
                        .Parameters.Add("@ToDate", System.Data.SqlDbType.DateTime).Value = Parameter_ToDate
                        .CommandTimeout = 0
                    End With

                    Using da As New SqlClient.SqlDataAdapter(cmd)
                        da.Fill(ds, TableName)
                        tbl = ds.Tables(TableName)
                    End Using
                End Using
            End If

            If tbl IsNot Nothing Then
                rpt.DataSource = New System.Data.DataView(tbl, CType(rpt, DebtPlus.Interfaces.Reports.IReportFilter).ReportFilter.ViewFilter, String.Empty, System.Data.DataViewRowState.CurrentRows)
                For Each calc As DevExpress.XtraReports.UI.CalculatedField In rpt.CalculatedFields
                    calc.Assign(rpt.DataSource, rpt.DataMember)
                Next
            End If
        End Sub

    End Class
End Namespace
