#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}
#End Region

Option Compare Binary
Option Explicit On 
Option Strict On

Imports DevExpress.XtraReports.UI

Namespace Appointments.Template
    Public Class AppointmentTemplateReport

        ''' <summary>
        ''' Pass the outer parameters to the sub-report
        ''' </summary>
        Public Sub New()
            MyBase.New()
            InitializeComponent()
            AddHandler Me.BeforePrint, AddressOf AppointmentTemplateClass_BeforePrint
        End Sub

        Private Parameter_DOW_set As Boolean = False
        Public Property Parameter_DOW() As Object
            Get
                Return Me.Parameters("ParameterDOW").Value
            End Get
            Set(ByVal value As Object)
                Me.Parameters("ParameterDOW").Value = value
                Parameter_DOW_set = True
            End Set
        End Property

        Private Parameter_Office_Set As Boolean = False
        Public Property Parameter_Office() As Object
            Get
                Return Me.Parameters("ParameterOffice").Value
            End Get
            Set(ByVal value As Object)
                Me.Parameters("ParameterOffice").Value = value
                Parameter_Office_Set = True
            End Set
        End Property

        ''' <summary>
        ''' Provide the linkage to set the parameters
        ''' </summary>
        Public Overrides Sub SetReportParameter(ByVal Parameter As String, ByVal Value As Object)
            Select Case Parameter
                Case "Office"
                    Parameter_Office = Value
                Case "DOW"
                    Parameter_DOW = Value
                Case Else
                    MyBase.SetReportParameter(Parameter, Value)
            End Select
        End Sub

        Public Overrides Function NeedParameters() As Boolean
            Return MyBase.NeedParameters() OrElse (Not Parameter_Office_Set) OrElse (Not Parameter_DOW_set)
        End Function

        ''' <summary>
        ''' Title associated with the report
        ''' </summary>
        Public Overrides ReadOnly Property ReportTitle() As String
            Get
                Return "Appointment Template"
            End Get
        End Property

        ''' <summary>
        ''' SubTitle associated with the report
        ''' </summary>
        Public Overrides ReadOnly Property ReportSubTitle() As String
            Get
                Dim DayString As String = DebtPlus.Utils.Nulls.DStr(Me.Parameters("ParameterDOW").Value).Trim
                If DayString = String.Empty Then DayString = "all days"

                Dim OfficeString As String = "all offices"
                Dim Office As System.Int32 = DebtPlus.Utils.Nulls.DInt(Me.Parameters("ParameterOffice").Value, -1)
                If Office > 0 Then
                    OfficeString = String.Format("office # {0:f0}", Office)
                End If

                Return "This report covers " + DayString + " and " + OfficeString
            End Get
        End Property

        ''' <summary>
        ''' Request the parameters for the report
        ''' </summary>
        Public Overrides Function RequestReportParameters() As System.Windows.Forms.DialogResult
            Dim Answer As System.Windows.Forms.DialogResult = DialogResult.OK

            If NeedParameters() Then
                Using frm As New AppointmentTemplateParametersForm()
                    Answer = frm.ShowDialog
                    Parameter_DOW = frm.Parameter_DOW
                    Parameter_Office = frm.Parameter_Office
                End Using
            End If

            '-- The answer should be OK if we want to show the report.
            Return Answer
        End Function

        Dim ds As New System.Data.DataSet("ds")

        ''' <summary>
        ''' Bind the dataset to the report
        ''' </summary>
        Private Sub AppointmentTemplateReport_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs)
            Dim rpt As DevExpress.XtraReports.UI.XtraReport = CType(sender, DevExpress.XtraReports.UI.XtraReport)
            Const TableName As String = "rpt_appointment_template"

            '-- Create a dataset for the report fields
            Try
                Dim sqlInfo As DebtPlus.LINQ.SQLInfoClass = DebtPlus.LINQ.SQLInfoClass.getDefault()
                Using cn As System.Data.SqlClient.SqlConnection = New System.Data.SqlClient.SqlConnection(sqlInfo.ConnectionString)
                    cn.Open()
                    Using cmd As New System.Data.SqlClient.SqlCommand()
                        cmd.Connection = cn
                        cmd.CommandText = "rpt_appointment_template"
                        cmd.CommandType = System.Data.CommandType.StoredProcedure
                        System.Data.SqlClient.SqlCommandBuilder.DeriveParameters(cmd)

                        Dim IntValue As Int32 = DebtPlus.Utils.Nulls.DInt(rpt.Parameters("ParameterOffice").Value)
                        If IntValue > 0 Then
                            cmd.Parameters(1).Value = IntValue
                        End If

                        Dim StrValue As String = DebtPlus.Utils.Nulls.DStr(rpt.Parameters("ParameterDOW").Value)
                        If StrValue <> String.Empty Then
                            cmd.Parameters(2).Value = StrValue
                        End If

                        cmd.CommandTimeout = 0

                        Using da As New System.Data.SqlClient.SqlDataAdapter(cmd)
                            da.Fill(ds, TableName)
                            rpt.DataSource = New System.Data.DataView(ds.Tables(TableName), String.Empty, "office, dow, start_hour, counselor", System.Data.DataViewRowState.CurrentRows)
                        End Using
                    End Using
                End Using

            Catch ex As System.Data.SqlClient.SqlException
                Using gdr As New DebtPlus.Repository.GetDataResult()
                    gdr.HandleException(ex)
                    DebtPlus.UI.Common.ErrorHandling.HandleErrors(gdr, "Error reading report information")
                End Using
            End Try
        End Sub
    End Class
End Namespace
