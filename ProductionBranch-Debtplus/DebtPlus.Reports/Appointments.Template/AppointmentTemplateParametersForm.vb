#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}
#End Region

Option Compare Binary
Option Explicit On
Option Strict On

Imports DebtPlus.Reports.Template.Forms

Namespace Appointments.Template

    Friend Class AppointmentTemplateParametersForm
        Inherits ReportParametersForm

        Public Sub New()
            MyBase.New()
            InitializeComponent()
            AddHandler Me.Load, AddressOf DateReportParametersForm_Load
        End Sub

        Public ReadOnly Property Parameter_DOW() As Object
            Get
                If ComboBoxEdit_day.SelectedIndex = 0 Then Return System.DBNull.Value
                Return ComboBoxEdit_day.Text
            End Get
        End Property

        Public ReadOnly Property Parameter_Office() As Object
            Get
                If LookUpEdit_office.EditValue Is Nothing Then Return System.DBNull.Value
                Return LookUpEdit_office.EditValue
            End Get
        End Property

#Region " Windows Form Designer generated code "

        'Form overrides dispose to clean up the component list.
        Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
            If disposing Then
                If components IsNot Nothing Then
                    components.Dispose()
                End If
            End If
            MyBase.Dispose(disposing)
        End Sub
        Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents LookUpEdit_office As DevExpress.XtraEditors.LookUpEdit
        Friend WithEvents LabelControl2 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents ComboBoxEdit_day As DevExpress.XtraEditors.ComboBoxEdit

        'Required by the Windows Form Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Windows Form Designer
        'It can be modified using the Windows Form Designer.  
        'Do not modify it using the code editor.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
            Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl
            Me.LookUpEdit_office = New DevExpress.XtraEditors.LookUpEdit
            Me.LabelControl2 = New DevExpress.XtraEditors.LabelControl
            Me.ComboBoxEdit_day = New DevExpress.XtraEditors.ComboBoxEdit
            CType(Me.DxErrorProvider1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LookUpEdit_office.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.ComboBoxEdit_day.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.SuspendLayout()
            '
            'ButtonOK
            '
            Me.ButtonOK.Enabled = True
            Me.ButtonOK.TabIndex = 4
            '
            'ButtonCancel
            '
            Me.ButtonCancel.TabIndex = 5
            '
            'LabelControl1
            '
            Me.LabelControl1.Location = New System.Drawing.Point(10, 52)
            Me.LabelControl1.Name = "LabelControl1"
            Me.LabelControl1.Size = New System.Drawing.Size(29, 13)
            Me.LabelControl1.TabIndex = 2
            Me.LabelControl1.Text = "Office"
            '
            'LookUpEdit_office
            '
            Me.LookUpEdit_office.Location = New System.Drawing.Point(45, 49)
            Me.LookUpEdit_office.Name = "LookUpEdit_office"
            Me.LookUpEdit_office.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
            Me.LookUpEdit_office.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.LookUpEdit_office.Properties.Columns.AddRange(New DevExpress.XtraEditors.Controls.LookUpColumnInfo() {New DevExpress.XtraEditors.Controls.LookUpColumnInfo("office", "ID", 20, DevExpress.Utils.FormatType.Numeric, "f0", False, DevExpress.Utils.HorzAlignment.[Default], DevExpress.Data.ColumnSortOrder.None), New DevExpress.XtraEditors.Controls.LookUpColumnInfo("name", "Name", 20, DevExpress.Utils.FormatType.None, "", True, DevExpress.Utils.HorzAlignment.[Default], DevExpress.Data.ColumnSortOrder.None)})
            Me.LookUpEdit_office.Properties.DisplayMember = "name"
            Me.LookUpEdit_office.Properties.NullText = "All Offices"
            Me.LookUpEdit_office.Properties.ShowFooter = False
            Me.LookUpEdit_office.Properties.ShowHeader = False
            Me.LookUpEdit_office.Properties.ValueMember = "office"
            Me.LookUpEdit_office.Size = New System.Drawing.Size(185, 20)
            Me.LookUpEdit_office.TabIndex = 3
            Me.LookUpEdit_office.Properties.SortColumnIndex = 1
            '
            'LabelControl2
            '
            Me.LabelControl2.Location = New System.Drawing.Point(10, 26)
            Me.LabelControl2.Name = "LabelControl2"
            Me.LabelControl2.Size = New System.Drawing.Size(19, 13)
            Me.LabelControl2.TabIndex = 0
            Me.LabelControl2.Text = "Day"
            '
            'ComboBoxEdit_day
            '
            Me.ComboBoxEdit_day.EditValue = "All Days"
            Me.ComboBoxEdit_day.Location = New System.Drawing.Point(45, 23)
            Me.ComboBoxEdit_day.Name = "ComboBoxEdit_day"
            Me.ComboBoxEdit_day.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[False]
            Me.ComboBoxEdit_day.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.ComboBoxEdit_day.Properties.Items.AddRange(New Object() {"All Days", "Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"})
            Me.ComboBoxEdit_day.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
            Me.ComboBoxEdit_day.Size = New System.Drawing.Size(185, 20)
            Me.ComboBoxEdit_day.TabIndex = 1
            '
            'ParametersForm
            '
            Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
            Me.ClientSize = New System.Drawing.Size(336, 96)
            Me.Controls.Add(Me.ComboBoxEdit_day)
            Me.Controls.Add(Me.LabelControl2)
            Me.Controls.Add(Me.LookUpEdit_office)
            Me.Controls.Add(Me.LabelControl1)
            Me.Name = "ParametersForm"
            Me.Controls.SetChildIndex(Me.LabelControl1, 0)
            Me.Controls.SetChildIndex(Me.LookUpEdit_office, 0)
            Me.Controls.SetChildIndex(Me.ButtonOK, 0)
            Me.Controls.SetChildIndex(Me.ButtonCancel, 0)
            Me.Controls.SetChildIndex(Me.LabelControl2, 0)
            Me.Controls.SetChildIndex(Me.ComboBoxEdit_day, 0)
            CType(Me.DxErrorProvider1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LookUpEdit_office.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.ComboBoxEdit_day.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            Me.ResumeLayout(False)
            Me.PerformLayout()

        End Sub

#End Region

        ''' <summary>
        ''' Process the LOAD event
        ''' </summary>
        Private Sub DateReportParametersForm_Load(ByVal sender As Object, ByVal e As System.EventArgs)
            ' Load the office location information

            ' Select the data from the database
            Dim cmd As SqlClient.SqlCommand = New System.Data.SqlClient.SqlCommand()
            With cmd
                .Connection = New System.Data.SqlClient.SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
                .CommandText = "SELECT [default], ActiveFlag, office as office, name as name FROM offices"
                .CommandType = CommandType.Text
            End With

            Dim ds As New System.Data.DataSet("ds")
            Dim da As New SqlClient.SqlDataAdapter(cmd)
            da.Fill(ds, "offices")

            ' Bind the data to the list
            Dim vue As DataView = ds.Tables("offices").DefaultView

            ' Bind the office list to the data
            With LookUpEdit_office
                With .Properties
                    .DataSource = vue
                End With
                .EditValue = Nothing
            End With
        End Sub
    End Class
End Namespace
