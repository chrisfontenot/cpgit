#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}
#End Region
Imports DebtPlus.Reports.Template
Imports System.IO
Imports DebtPlus.Utils

Namespace Appointments.Template
    Public Class AppointmentTemplateReport
        Inherits TemplateXtraReportClass

        Public Sub New()
            MyBase.New()

            ' Name of the report file
            Const ReportName As String = "DebtPlus.Reports.Appointments.Template.repx"

            ' See if there is a report reference that we can use
            Dim Fname As String = System.IO.Path.Combine(DebtPlus.Configuration.Config.ReportsDirectory, ReportName)
            Dim UseDefault As Boolean = True
            Try
                If (Not DebtPlus.Configuration.Config.LocalReportsOnly) AndAlso System.IO.File.Exists(Fname) Then
                    LoadLayout(Fname)
                    UseDefault = False
                End If

            Catch ex As System.Exception
                DebtPlus.Svc.Common.ErrorHandling.HandleErrors(ex)
            End Try

            ' Load the standard report definition if we need a new item
            If UseDefault Then
                Dim asm As Reflection.Assembly = Reflection.Assembly.GetExecutingAssembly                                           ' changed
                Using ios As Stream = asm.GetManifestResourceStream(String.Format("DebtPlus.Reports.{0}", ReportName))              ' changed
                    If ios IsNot Nothing Then LoadLayout(ios) '                                                                       changed
                End Using
            End If

            ' Set the script references as needed
            ScriptReferences = DebtPlus.Utils.AssemblyTools.GetAllSiblingAssemblies()

            ' Update the subreports as well
            For Each BandPtr As DevExpress.XtraReports.UI.Band In Bands
                For Each CtlPtr As DevExpress.XtraReports.UI.XRControl In BandPtr.Controls
                    Dim Rpt As DevExpress.XtraReports.UI.XRSubreport = TryCast(CtlPtr, DevExpress.XtraReports.UI.XRSubreport)
                    If Rpt IsNot Nothing Then
                        Dim RptSrc As DevExpress.XtraReports.UI.XtraReport = TryCast(Rpt.ReportSource, DevExpress.XtraReports.UI.XtraReport)
                        If RptSrc IsNot Nothing Then
                            RptSrc.ScriptReferences = ScriptReferences
                        End If
                    End If
                Next
            Next
        End Sub

        Private Parameter_DOW_set As Boolean = False
        Public Property Parameter_DOW() As Object
            Get
                Return Me.Parameters("ParameterDOW").Value
            End Get
            Set(ByVal value As Object)
                Me.Parameters("ParameterDOW").Value = value
                Parameter_DOW_set = True
            End Set
        End Property

        Private Parameter_Office_Set As Boolean = False
        Public Property Parameter_Office() As Object
            Get
                Return Me.Parameters("ParameterOffice").Value
            End Get
            Set(ByVal value As Object)
                Me.Parameters("ParameterOffice").Value = value
                Parameter_Office_Set = True
            End Set
        End Property


        ''' <summary>
        '''     Provide the linkage to set the parameters
        ''' </summary>
        Public Overrides Sub SetReportParameter(ByVal Parameter As String, ByVal Value As Object)
            Select Case Parameter
                Case "Office"
                    Parameter_Office = Value
                Case "DOW"
                    Parameter_DOW = Value
                Case Else
                    MyBase.SetReportParameter(Parameter, Value)
            End Select
        End Sub

        Public Overrides Function NeedParameters() As Boolean
            Return MyBase.NeedParameters() OrElse (Not Parameter_Office_Set) OrElse (Not Parameter_DOW_set)
        End Function


        ''' <summary>
        '''     Title associated with the report
        ''' </summary>
        Public Overrides ReadOnly Property ReportTitle() As String
            Get
                Return "Appointment Template"
            End Get
        End Property


        ''' <summary>
        '''     SubTitle associated with the report
        ''' </summary>
        Public Overrides ReadOnly Property ReportSubTitle() As String
            Get
                Dim DayString As String = DebtPlus.Utils.Nulls.DStr(Me.Parameters("ParameterDOW").Value).Trim()
                If DayString = String.Empty Then DayString = "all days"

                Dim OfficeString As String = "all offices"
                Dim Office As System.Int32 = DebtPlus.Utils.Nulls.DInt(Me.Parameters("ParameterOffice").Value, -1)
                If Office > 0 Then
                    OfficeString = String.Format("office # {0:f0}", Office)
                End If

                Return "This report covers " + DayString + " and " + OfficeString
            End Get
        End Property


        ''' <summary>
        '''     Request the parameters for the report
        ''' </summary>
        Public Overrides Function RequestReportParameters() As System.Windows.Forms.DialogResult
            Dim Answer As System.Windows.Forms.DialogResult = DialogResult.OK

            If NeedParameters() Then
                Dim dialogForm As New AppointmentTemplateParametersForm()
                With dialogForm
                    Answer = .ShowDialog()
                    Parameter_DOW = .Parameter_DOW
                    Parameter_Office = .Parameter_Office
                    .Dispose()
                End With
            End If

            ' The answer should be OK if we want to show the report.
            Return Answer
        End Function
    End Class
End Namespace
