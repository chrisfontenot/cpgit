#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}
#End Region

Partial Class BalanceVerifyReport


    ''' <summary>
    '''     Obtain the zipcode for the creditor address
    ''' </summary>
    Private Function GetCreditorZipcode(ByVal Creditor As String) As String
        Dim Answer As String = String.Empty
        Dim row As System.Data.DataRow = FindCreditorAddressRow_L(Creditor)
        If row IsNot Nothing Then
            Dim addressID As System.Int32 = DebtPlus.Utils.Nulls.DInt(row("AddressID"))
            If addressID > 0 Then
                row = FindAddressRow(addressID)
                If row IsNot Nothing Then
                    Answer = DebtPlus.Utils.Nulls.DStr(row("postalcode")).Replace("-", "").Trim
                End If
            End If
        End If

        If Answer.StartsWith("00000") Then
            Answer = String.Empty
        End If
        Return Answer
    End Function


    ''' <summary>
    '''     Obtain the address information for the creditor
    ''' </summary>
    Private Function GetCreditorAddress(ByVal Creditor As String) As String
        Dim sb As New System.Text.StringBuilder
        Dim row As System.Data.DataRow = FindCreditorAddressRow_L(Creditor)
        If row IsNot Nothing Then
            Dim addressID As System.Int32 = DebtPlus.Utils.Nulls.DInt(row("AddressID"))
            If addressID > 0 Then
                Dim txt As String = DebtPlus.Utils.Nulls.DStr(row("attn")).Trim
                If txt <> String.Empty Then
                    sb.AppendFormat("{0}ATTN: {1}", System.Environment.NewLine, txt)
                End If
                txt = Format_Address(addressID)
                If txt <> String.Empty Then
                    sb.AppendFormat("{0}{1}", System.Environment.NewLine, txt)
                End If
            End If
        End If
        If sb.Length > 0 Then
            sb.Remove(0, 2)
        End If
        Return sb.ToString
    End Function


    ''' <summary>
    '''     Format the address block pointed to by the address ID
    ''' </summary>
    Private Function Format_Address(ByVal AddressID As Object) As String
        Dim row As System.Data.DataRow = Nothing
        If AddressID IsNot Nothing AndAlso AddressID IsNot System.DBNull.Value Then
            row = FindAddressRow(Convert.ToInt32(AddressID))
        End If
        Return Format_Address(row)
    End Function


    ''' <summary>
    '''     Return the formatted address for the indicated address row
    ''' </summary>
    Private Function Format_Address(ByVal row As System.Data.DataRow) As String
        Dim sb As New System.Text.StringBuilder
        Dim txt As String

        If row IsNot Nothing Then
            txt = DebtPlus.Utils.Nulls.DStr(row("creditor_prefix_1")).Trim
            If txt <> String.Empty Then
                sb.Append(System.Environment.NewLine)
                sb.Append(txt)
            End If

            txt = DebtPlus.Utils.Nulls.DStr(row("creditor_prefix_2")).Trim
            If txt <> String.Empty Then
                sb.Append(System.Environment.NewLine)
                sb.Append(txt)
            End If

            txt = format_address_line_1(row("house"), row("direction"), row("street"), row("suffix"), row("modifier"), row("modifier_value"))
            If txt <> String.Empty Then
                sb.Append(System.Environment.NewLine)
                sb.Append(txt)
            End If

            txt = DebtPlus.Utils.Nulls.DStr(row("address_line_2")).Trim
            If txt <> String.Empty Then
                sb.Append(System.Environment.NewLine)
                sb.Append(txt)
            End If

            txt = DebtPlus.Utils.Nulls.DStr(row("address_line_3")).Trim
            If txt <> String.Empty Then
                sb.Append(System.Environment.NewLine)
                sb.Append(txt)
            End If

            txt = format_city_state_zip(row("city"), row("state"), row("postalcode"))
            If txt <> String.Empty Then
                sb.Append(System.Environment.NewLine)
                sb.Append(txt)
            End If
        End If

        If sb.Length > 0 Then
            sb.Remove(0, 2)
        End If

        Return sb.ToString
    End Function


    ''' <summary>
    '''     Format the first line of a client's address (house,street, etc.)
    ''' </summary>
    Private Function format_address_line_1(ByVal house As Object, ByVal direction As Object, ByVal street As Object, ByVal suffix As Object, ByVal modifier As Object, ByVal modifier_value As Object) As String
        Dim sb As New System.Text.StringBuilder
        Dim txt As String = DebtPlus.Utils.Nulls.DStr(house).Trim
        If txt <> String.Empty Then
            sb.Append(" ")
            sb.Append(txt)
        End If

        txt = DebtPlus.Utils.Nulls.DStr(direction).Trim
        If txt <> String.Empty Then
            sb.Append(" ")
            sb.Append(txt)
        End If

        txt = DebtPlus.Utils.Nulls.DStr(street).Trim
        If txt <> String.Empty Then
            sb.Append(" ")
            sb.Append(txt)
        End If

        txt = DebtPlus.Utils.Nulls.DStr(suffix).Trim
        If txt <> String.Empty Then
            sb.Append(" ")
            sb.Append(txt)
        End If

        txt = DebtPlus.Utils.Nulls.DStr(modifier).Trim
        If txt <> String.Empty Then
            sb.Append(" ")
            sb.Append(txt)
        End If

        txt = DebtPlus.Utils.Nulls.DStr(modifier_value).Trim
        If txt <> String.Empty Then
            sb.Append(" ")
            sb.Append(txt)
        End If

        Return sb.ToString.Trim
    End Function


    ''' <summary>
    '''     Format the last line of a client's address (city,state,zipcode, etc.)
    ''' </summary>
    Private Function format_city_state_zip(ByVal city As Object, ByVal state As Object, ByVal postalcode As Object) As String
        Dim Answer As String = String.Empty
        Dim CityString As String = DebtPlus.Utils.Nulls.DStr(city)
        Dim StateID As System.Int32 = DebtPlus.Utils.Nulls.DInt(state)
        Dim PostalCodeString As String = DebtPlus.Utils.Nulls.DStr(postalcode)
        Dim CountryName As String = String.Empty
        Dim StateName As String = String.Empty
        Dim AddressFormat As String = "{0} {1}  {2}"

        '-- Find the state in the states table
        Dim row As System.Data.DataRow = GetStateRow(StateID)
        If row IsNot Nothing Then
            StateName = DebtPlus.Utils.Nulls.DStr(row("mailingcode")).Trim
            If StateName.Length > 2 Then
                StateName = StateName.Substring(0, 2)
            End If

            '-- Find the country name
            Dim countryID As System.Int32 = DebtPlus.Utils.Nulls.DInt(row("country"))
            Dim CountryRow As System.Data.DataRow = GetCountryRow(countryID)
            If CountryRow IsNot Nothing Then
                CountryName = DebtPlus.Utils.Nulls.DStr(CountryRow("description"))
            End If

            '-- Format the postalcode if this is a USA Address
            postalcode = Format_Zipcode(postalcode, row("USAFormat"))

            '-- Find the address format string
            AddressFormat = DebtPlus.Utils.Nulls.DStr(row("AddressFormat")).Replace("\n", ControlChars.Lf).Replace("\r", ControlChars.Cr)
        End If

        '-- Format the address block
        Try
            Answer = String.Format(AddressFormat, CityString, StateName, PostalCodeString, CountryName)
        Catch ex As Exception
            Answer = String.Format("{0} {1}  {2}", CityString, StateName, PostalCodeString, CountryName)
        End Try

        Return Answer
    End Function


    ''' <summary>
    '''     Format the postal code based upon the rules
    ''' </summary>
    Private Function Format_Zipcode(ByVal Postalcode As Object, ByVal USAFormat As Object) As String
        Dim isUSA As Boolean = DebtPlus.Utils.Nulls.DBool(USAFormat)
        Dim Result As String = DebtPlus.Utils.Nulls.DStr(Postalcode)

        If isUSA Then
            Result = Result.Replace("-", "")
            If Result.Length = 9 AndAlso Result.EndsWith("0000") Then
                Result = Result.Substring(0, 5)
            End If

            If Result.Length = 9 Then
                Result = Result.Substring(0, 5) + "-" + Result.Substring(5)
            End If

            '-- Do not include zipcodes of 00000
            If Result.StartsWith("00000") Then
                Result = String.Empty
            End If
        End If

        Return Result
    End Function


    ''' <summary>
    '''     Get the pointer to the country row in the countries table
    ''' </summary>
    Private Function GetCountryRow(ByVal CountryID As Object) As System.Data.DataRow
        Dim tbl As System.Data.DataTable = ds.Tables("countries")
        Dim row As System.Data.DataRow = Nothing
        If tbl IsNot Nothing Then
            row = tbl.Rows.Find(CountryID)
            If row IsNot Nothing Then
                Return row
            End If
        End If

        Dim cn As System.Data.SqlClient.SqlConnection = New System.Data.SqlClient.SqlConnection(sqlInfo.ConnectionString)
        Try
            cn.Open()
            Using cmd As New System.Data.SqlClient.SqlCommand()
                With cmd
                    .Connection = cn
                    .CommandText = "SELECT [country],[description],[default],[activeflag] FROM countries WHERE [country]=@CountryID"
                    .CommandType = System.Data.CommandType.Text
                    .Parameters.Add("@CountryID", SqlDbType.Int).Value = CountryID
                End With
                Using da As New System.Data.SqlClient.SqlDataAdapter(cmd)
                    da.Fill(ds, "countries")
                End Using
                With ds.Tables("countries")
                    If .PrimaryKey.GetUpperBound(0) < 0 Then
                        .PrimaryKey = New System.Data.DataColumn() {.Columns("country")}
                    End If
                    row = .Rows.Find(CountryID)
                End With
            End Using
        Finally
            If cn IsNot Nothing Then
                If cn.State <> ConnectionState.Closed Then
                    cn.Close()
                End If
                cn.Dispose()
            End If
        End Try
        Return row
    End Function


    ''' <summary>
    '''     Get the pointer to the state row in the states table
    ''' </summary>
    Private Function GetStateRow(ByVal StateID As Object) As System.Data.DataRow
        Dim tbl As System.Data.DataTable = ds.Tables("states")
        Dim row As System.Data.DataRow = Nothing
        If tbl IsNot Nothing Then
            row = tbl.Rows.Find(StateID)
            If row IsNot Nothing Then
                Return row
            End If
        End If

        Dim cn As System.Data.SqlClient.SqlConnection = New System.Data.SqlClient.SqlConnection(sqlInfo.ConnectionString)
        Try
            cn.Open()
            Using cmd As New System.Data.SqlClient.SqlCommand()
                With cmd
                    .Connection = cn
                    .CommandText = "SELECT [state],[mailingcode],[USAFormat],[AddressFormat],[country],[name],[default],[activeflag] FROM states WHERE [state]=@StateID"
                    .CommandType = System.Data.CommandType.Text
                    .Parameters.Add("@StateID", SqlDbType.Int).Value = StateID
                End With
                Using da As New System.Data.SqlClient.SqlDataAdapter(cmd)
                    da.Fill(ds, "states")
                End Using
                With ds.Tables("states")
                    If .PrimaryKey.GetUpperBound(0) < 0 Then
                        .PrimaryKey = New System.Data.DataColumn() {.Columns("state")}
                    End If
                    row = .Rows.Find(StateID)
                End With
            End Using
        Finally
            If cn IsNot Nothing Then
                If cn.State <> ConnectionState.Closed Then
                    cn.Close()
                End If
                cn.Dispose()
            End If
        End Try
        Return row
    End Function


    ''' <summary>
    '''     Find the creditor address row for the creditor and its type
    ''' </summary>
    Private Function FindCreditorAddressRow_L(ByVal Creditor As String) As System.Data.DataRow
        Dim row As System.Data.DataRow = FindCreditorAddressRow(Creditor, "L")
        If row Is Nothing Then
            row = FindCreditorAddressRow(Creditor, "P")
        End If
        Return row
    End Function

    Private Function FindCreditorAddressRow_I(ByVal Creditor As String) As System.Data.DataRow
        Dim row As System.Data.DataRow = FindCreditorAddressRow(Creditor, "I")
        If row Is Nothing Then
            row = FindCreditorAddressRow(Creditor, "P")
        End If
        Return row
    End Function

    Private Function FindCreditorAddressRow_P(ByVal Creditor As String) As System.Data.DataRow
        Return FindCreditorAddressRow(Creditor, "P")
    End Function


    ''' <summary>
    '''     Find the creditor address row for the indicated type
    ''' </summary>
    Private Function FindCreditorAddressRow(ByVal Creditor As String, ByVal Type As String) As System.Data.DataRow
        Dim row As System.Data.DataRow = Nothing
        Dim tbl As System.Data.DataTable = CreditorAddressesTable()
        If tbl IsNot Nothing Then
            row = tbl.Rows.Find(New Object() {Creditor, Type})
            If row Is Nothing Then
                ReadCreditorAddress(Creditor, Type)
                row = tbl.Rows.Find(New Object() {Creditor, Type})
            End If
        End If
        Return row
    End Function


    ''' <summary>
    '''     Read the creditor address row
    ''' </summary>
    Private Sub ReadCreditorAddress(ByVal Creditor As String, ByVal Type As String)
        Dim cn As System.Data.SqlClient.SqlConnection = New System.Data.SqlClient.SqlConnection(sqlInfo.ConnectionString)
        Try
            Using cmd As New System.Data.SqlClient.SqlCommand()
                With cmd
                    .Connection = cn
                    .CommandText = "SELECT [creditor],[type],[attn],[addressid] FROM creditor_addresses WHERE [creditor]=@creditor and [type]=@type"
                    .CommandType = System.Data.CommandType.Text
                    .Parameters.Add("@creditor", SqlDbType.VarChar, 10).Value = Creditor
                    .Parameters.Add("@type", SqlDbType.VarChar, 10).Value = Type
                End With

                Using da As New System.Data.SqlClient.SqlDataAdapter(cmd)
                    da.Fill(ds, "creditor_addresses")
                End Using
            End Using

        Finally
            If cn IsNot Nothing Then
                If cn.State <> ConnectionState.Closed Then cn.Close()
                cn.Dispose()
            End If
        End Try
    End Sub


    ''' <summary>
    '''     Find the creditor address table in the dataset or load the schema
    ''' </summary>
    Private Function CreditorAddressesTable() As System.Data.DataTable
        Dim tbl As System.Data.DataTable = ds.Tables("creditor_addresses")
        If tbl Is Nothing Then
            Dim cn As System.Data.SqlClient.SqlConnection = New System.Data.SqlClient.SqlConnection(sqlInfo.ConnectionString)
            Try
                Using cmd As New System.Data.SqlClient.SqlCommand()
                    With cmd
                        .Connection = cn
                        .CommandText = "SELECT [creditor],[type],[attn],[addressid] FROM creditor_addresses"
                        .CommandType = System.Data.CommandType.Text
                    End With

                    Using da As New System.Data.SqlClient.SqlDataAdapter(cmd)
                        da.FillSchema(ds, SchemaType.Source, "creditor_addresses")
                    End Using
                    tbl = ds.Tables("creditor_addresses")
                    With tbl
                        .PrimaryKey = New System.Data.DataColumn() {.Columns("creditor"), .Columns("type")}
                    End With
                End Using

            Finally
                If cn IsNot Nothing Then
                    If cn.State <> ConnectionState.Closed Then cn.Close()
                    cn.Dispose()
                End If
            End Try
        End If
        Return tbl
    End Function


    ''' <summary>
    '''     Read the address row from the indicated ID
    ''' </summary>
    Private Function FindAddressRow(ByVal AddressID As System.Int32) As System.Data.DataRow
        Dim tbl As System.Data.DataTable = AddressesTable()
        Dim row As System.Data.DataRow = tbl.Rows.Find(AddressID)
        If row Is Nothing Then
            ReadAddressRow(AddressID)
            row = tbl.Rows.Find(AddressID)
        End If
        Return row
    End Function


    ''' <summary>
    '''     Read the address row from the indicated ID
    ''' </summary>
    Private Sub ReadAddressRow(ByVal AddressID As System.Int32)
        Dim tbl As System.Data.DataTable = ds.Tables("Addresses")
        Dim cn As System.Data.SqlClient.SqlConnection = New System.Data.SqlClient.SqlConnection(sqlInfo.ConnectionString)
        Try
            Using cmd As New System.Data.SqlClient.SqlCommand()
                With cmd
                    .Connection = cn
                    .CommandText = "SELECT [address],[creditor_prefix_1],[creditor_prefix_2],[house],[direction],[street],[suffix],[modifier],[modifier_value],[address_line_2],[address_line_3],[city],[state],[postalcode] FROM Addresses WHERE [Address]=@AddressID"
                    .CommandType = System.Data.CommandType.Text
                    .Parameters.Add("@AddressID", SqlDbType.Int).Value = AddressID
                End With

                Using da As New System.Data.SqlClient.SqlDataAdapter(cmd)
                    da.Fill(ds, "Addresses")
                End Using
            End Using

        Finally
            If cn IsNot Nothing Then
                If cn.State <> ConnectionState.Closed Then cn.Close()
                cn.Dispose()
            End If
        End Try
    End Sub


    ''' <summary>
    '''     Find the address table in the dataset or load the schema
    ''' </summary>
    Private Function AddressesTable() As System.Data.DataTable
        Dim tbl As System.Data.DataTable = ds.Tables("Addresses")
        If tbl Is Nothing Then
            Dim cn As System.Data.SqlClient.SqlConnection = New System.Data.SqlClient.SqlConnection(sqlInfo.ConnectionString)
            Try
                Using cmd As New System.Data.SqlClient.SqlCommand()
                    With cmd
                        .Connection = cn
                        .CommandText = "SELECT [address],[creditor_prefix_1],[creditor_prefix_2],[house],[direction],[street],[suffix],[modifier],[modifier_value],[address_line_2],[address_line_3],[city],[state],[postalcode] FROM Addresses"
                        .CommandType = System.Data.CommandType.Text
                    End With

                    Using da As New System.Data.SqlClient.SqlDataAdapter(cmd)
                        da.FillSchema(ds, SchemaType.Source, "Addresses")
                    End Using
                    tbl = ds.Tables("Addresses")
                    With tbl
                        .PrimaryKey = New System.Data.DataColumn() {.Columns("address")}
                    End With
                End Using

            Finally
                If cn IsNot Nothing Then
                    If cn.State <> ConnectionState.Closed Then cn.Close()
                    cn.Dispose()
                End If
            End Try
        End If
        Return tbl
    End Function
End Class
