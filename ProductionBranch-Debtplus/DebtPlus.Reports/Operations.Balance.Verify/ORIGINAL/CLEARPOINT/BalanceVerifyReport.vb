#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}
#End Region

Namespace Operations.Balance.Verify
    Public Class BalanceVerifyReport
        Inherits DebtPlus.Reports.Template.BaseXtraReportClass

        ''' <summary>
        ''' Create an instance of our report
        ''' </summary>
        Public Sub New()
            MyBase.New()
            InitializeComponent()

            '-- Define event handlers
            AddHandler XrLabel_OfficeAddress.BeforePrint, AddressOf XrLabel_OfficeAddress_BeforePrint
            AddHandler XrLabel_client.BeforePrint, AddressOf XrLabel_client_BeforePrint
            AddHandler XrLabel_Creditor_Address.BeforePrint, AddressOf XrLabel_Creditor_Address_BeforePrint
            AddHandler XrBarCode_PostalCode.BeforePrint, AddressOf XrBarCode_PostalCode_BeforePrint
            AddHandler XrLabel_organization_address.BeforePrint, AddressOf XrLabel_organization_address_BeforePrint
            AddHandler BeforePrint, AddressOf BalanceVerifyReport_BeforePrint
        End Sub

        Dim ds As New System.Data.DataSet("ds")

        ''' <summary>
        ''' Read the datasource and bind it to the report
        ''' </summary>
        Private Sub BalanceVerifyReport_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs)
            Dim Rpt As DevExpress.XtraReports.UI.XtraReport = CType(sender, DevExpress.XtraReports.UI.XtraReport)
            Dim sqlInfo As DebtPlus.LINQ.SQLInfoClass = DebtPlus.LINQ.SQLInfoClass.getDefault()

            Dim cn As System.Data.SqlClient.SqlConnection = New System.Data.SqlClient.SqlConnection(sqlInfo.ConnectionString)
            Dim rd As System.Data.SqlClient.SqlDataReader = Nothing
            Dim tbl As System.Data.DataTable = Nothing
            Try
                cn.Open()
                Using cmd As New System.Data.SqlClient.SqlCommand()
                    With cmd
                        .Connection = cn
                        .CommandText = "rpt_BalanceVerify"
                        .CommandType = System.Data.CommandType.StoredProcedure
                        .CommandTimeout = 0
                    End With

                    Using da As New System.Data.SqlClient.SqlDataAdapter(cmd)
                        da.Fill(ds, "rpt_BalanceVerify")
                        tbl = ds.Tables("rpt_BalanceVerify")
                    End Using
                End Using

                '-- Set the report source to the table
                With Rpt
                    .DataSource = New System.Data.DataView(tbl, String.Empty, "creditor, account_number, client", System.Data.DataViewRowState.CurrentRows)

                    '-- Bind the fields to the report information
                    .FindControl("XrLabel_account_number", True).DataBindings.Add("Text", Nothing, "account_number")
                    .FindControl("XrLabel_balance", True).DataBindings.Add("Text", Nothing, "balance", "{0:c}")
                    .FindControl("XrLabel_client_name", True).DataBindings.Add("Text", Nothing, "name")
                End With

            Catch ex As System.Data.SqlClient.SqlException
                Using gdr As New DebtPlus.Repository.GetDataResult()
                    gdr.HandleException(ex)
                    DebtPlus.UI.Common.ErrorHandling.HandleErrors(gdr, "Error reading report data")
                End Using

            Finally
                If rd IsNot Nothing AndAlso Not rd.IsClosed Then rd.Close()
                If cn IsNot Nothing Then
                    If cn.State <> System.Data.ConnectionState.Closed Then cn.Close()
                    cn.Dispose()
                End If
            End Try
        End Sub


        ''' <summary>
        ''' Format the postal barcode before it is printed
        ''' </summary>
        Private Sub XrBarCode_PostalCode_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs)
            With CType(sender, DevExpress.XtraReports.UI.XRBarCode)
                Dim MasterRpt As DevExpress.XtraReports.UI.XtraReport = CType(.Report, DevExpress.XtraReports.UI.XtraReport)
                Dim sqlInfo As DebtPlus.LINQ.SQLInfoClass = DebtPlus.LINQ.SQLInfoClass.getDefault()
                Dim ds As System.Data.DataSet = CType(MasterRpt.DataSource, System.Data.DataView).Table.DataSet
                Dim tbl As System.Data.DataTable = ds.Tables("rpt_CreditorAddress_L")
                Dim row As System.Data.DataRow = Nothing

                '-- The creditor is part of the current row being printed
                Dim Creditor As String = String.Empty
                If GetCurrentColumnValue("creditor") IsNot System.DBNull.Value Then Creditor = Convert.ToString(GetCurrentColumnValue("creditor"))

                '-- If there is a table then find the row
                If tbl IsNot Nothing Then
                    row = tbl.Rows.Find(Creditor)
                End If

                If row Is Nothing Then
                    Dim cn As System.Data.SqlClient.SqlConnection = New System.Data.SqlClient.SqlConnection(sqlInfo.ConnectionString)
                    Try
                        cn.Open()
                        Using cmd As New System.Data.SqlClient.SqlCommand()
                            With cmd
                                .Connection = cn
                                .CommandText = "rpt_CreditorAddress_L"
                                .CommandType = System.Data.CommandType.StoredProcedure
                                .Parameters.Add("@creditor", System.Data.SqlDbType.VarChar, 10).Value = Creditor
                                .CommandTimeout = 0
                            End With

                            Using da As New System.Data.SqlClient.SqlDataAdapter(cmd)
                                da.Fill(ds, "rpt_CreditorAddress_L")
                                tbl = ds.Tables("rpt_CreditorAddress_L")
                                With tbl
                                    If .PrimaryKey.GetUpperBound(0) < 0 Then
                                        .PrimaryKey = New System.Data.DataColumn() {.Columns("creditor")}
                                    End If
                                End With
                            End Using
                        End Using

                        If tbl IsNot Nothing Then
                            row = tbl.Rows.Find(Creditor)
                        End If

                    Catch ex As System.Data.SqlClient.SqlException
                        Using gdr As New DebtPlus.Repository.GetDataResult()
                            gdr.HandleException(ex)
                            DebtPlus.UI.Common.ErrorHandling.HandleErrors(gdr, "Error reading creditor address")
                        End Using
                    End Try
                End If

                Dim text_block As String = String.Empty
                If row IsNot Nothing Then
                    If row("zipcode") IsNot Nothing AndAlso row("zipcode") IsNot System.DBNull.Value Then
                        text_block = Convert.ToString(row("zipcode"))
                    End If
                    text_block = System.Text.RegularExpressions.Regex.Replace(text_block, "[^0-9]", String.Empty)
                End If

                If text_block = String.Empty Then
                    e.Cancel = True
                Else
                    .Text = text_block
                End If
            End With
        End Sub


        ''' <summary>
        ''' Format the creditor address before it is printed
        ''' </summary>
        Private Sub XrLabel_Creditor_Address_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs)
            Const TableName As String = "creditor_addresses"
            With CType(sender, DevExpress.XtraReports.UI.XRLabel)
                Dim MasterRpt As DevExpress.XtraReports.UI.XtraReport = CType(.Report, DevExpress.XtraReports.UI.XtraReport)
                If MasterRpt IsNot Nothing Then
                    Dim sqlInfo As DebtPlus.LINQ.SQLInfoClass = DebtPlus.LINQ.SQLInfoClass.getDefault()
                    Dim tbl As System.Data.DataTable = ds.Tables(TableName)

                    '-- The creditor is part of the current row being printed
                    Dim Creditor As String = DebtPlus.Utils.Nulls.DStr(GetCurrentColumnValue("creditor"))

                    '-- If there is a table then find the row
                    Dim row As System.Data.DataRow = Nothing
                    If tbl IsNot Nothing Then
                        row = tbl.Rows.Find(Creditor)
                    End If

                    If row Is Nothing Then
                        Dim cn As System.Data.SqlClient.SqlConnection = New System.Data.SqlClient.SqlConnection(sqlInfo.ConnectionString)
                        Try
                            cn.Open()
                            Using cmd As New System.Data.SqlClient.SqlCommand()
                                With cmd
                                    .Connection = cn
                                    .CommandText = "rpt_CreditorAddress_L"
                                    .CommandType = System.Data.CommandType.StoredProcedure
                                    .Parameters.Add("@creditor", System.Data.SqlDbType.VarChar, 10).Value = Creditor
                                    .CommandTimeout = 0
                                End With

                                Using da As New System.Data.SqlClient.SqlDataAdapter(cmd)
                                    da.Fill(ds, TableName)
                                    tbl = ds.Tables(TableName)
                                    With tbl
                                        If .PrimaryKey.GetUpperBound(0) < 0 Then
                                            .PrimaryKey = New System.Data.DataColumn() {.Columns("creditor")}
                                        End If
                                    End With
                                End Using
                            End Using

                            If tbl IsNot Nothing Then
                                row = tbl.Rows.Find(Creditor)
                            End If

                        Catch ex As System.Data.SqlClient.SqlException
                            Using gdr As New DebtPlus.Repository.GetDataResult()
                                gdr.HandleException(ex)
                                DebtPlus.UI.Common.ErrorHandling.HandleErrors(gdr, "Error reading creditor address")
                            End Using
                        End Try
                    End If

                    Dim text_block As New System.Text.StringBuilder
                    If row IsNot Nothing Then
                        For Each NameString As String In New String() {"addr1", "addr2", "addr3", "addr4", "addr5", "addr6", "addr7"}
                            Dim Value As String
                            If row(NameString) IsNot Nothing AndAlso row(NameString) IsNot System.DBNull.Value Then
                                Value = Convert.ToString(row(NameString)).Trim
                                If Value <> String.Empty Then
                                    text_block.Append(Environment.NewLine)
                                    text_block.Append(Value)
                                End If
                            End If
                        Next
                    End If

                    If text_block.Length > 0 Then
                        text_block.Remove(0, 2)
                    End If

                    .Text = text_block.ToString
                End If
            End With
        End Sub


        ''' <summary>
        ''' Format the client ID before it is printed
        ''' </summary>
        Private Sub XrLabel_client_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs)
            With CType(sender, DevExpress.XtraReports.UI.XRLabel)
                .Text = DebtPlus.Utils.Format.Client.FormatClientID(System.Convert.ToInt32(GetCurrentColumnValue("client")))
            End With
        End Sub


        ''' <summary>
        ''' Format the header office address before it is printed
        ''' </summary>
        Private Sub XrLabel_OfficeAddress_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs)
            With CType(sender, DevExpress.XtraReports.UI.XRLabel)
                Dim rpt As DevExpress.XtraReports.UI.XtraReport = CType(.Report, DevExpress.XtraReports.UI.XtraReport)
                Dim sqlInfo As DebtPlus.LINQ.SQLInfoClass = DebtPlus.LINQ.SQLInfoClass.getDefault()

                Dim Answer As String = String.Empty
                Dim client As System.Int32 = -1
                If GetCurrentColumnValue("client") IsNot Nothing AndAlso GetCurrentColumnValue("client") IsNot System.DBNull.Value Then client = Convert.ToInt32(GetCurrentColumnValue("client"))

                If client >= 0 Then
                    Dim cn As New System.Data.SqlClient.SqlConnection(sqlInfo.ConnectionString)
                    Dim rd As System.Data.SqlClient.SqlDataReader = Nothing
                    Try
                        cn.Open()
                        Using cmd As New System.Data.SqlClient.SqlCommand()
                            With cmd
                                .Connection = cn
                                .CommandText = "SELECT [office_name], [office_addr1], [office_addr2], [office_addr3], [counselor_phone], [counselor_fax] FROM view_client_counselor_office WHERE [client]=@client"
                                .CommandType = System.Data.CommandType.Text
                                .Parameters.Add("@client", System.Data.SqlDbType.Int).Value = client
                                rd = .ExecuteReader(System.Data.CommandBehavior.CloseConnection Or System.Data.CommandBehavior.SingleRow)

                                If rd IsNot Nothing AndAlso rd.Read Then
                                    Dim sb As New System.Text.StringBuilder

                                    '-- Name
                                    Dim txt As String = DebtPlus.Utils.Nulls.DStr(rd.GetValue(0)).Trim
                                    If txt <> String.Empty Then
                                        sb.Append(System.Environment.NewLine)
                                        sb.Append(txt)
                                    End If

                                    '-- Addr 1
                                    txt = DebtPlus.Utils.Nulls.DStr(rd.GetValue(1)).Trim
                                    If txt <> String.Empty Then
                                        sb.Append(System.Environment.NewLine)
                                        sb.Append(txt)
                                    End If

                                    '-- Addr 2
                                    txt = DebtPlus.Utils.Nulls.DStr(rd.GetValue(2)).Trim
                                    If txt <> String.Empty Then
                                        sb.Append(System.Environment.NewLine)
                                        sb.Append(txt)
                                    End If

                                    '-- Addr 3
                                    txt = DebtPlus.Utils.Nulls.DStr(rd.GetValue(3)).Trim
                                    If txt <> String.Empty Then
                                        sb.Append(System.Environment.NewLine)
                                        sb.Append(txt)
                                    End If

                                    '-- Phone
                                    txt = DebtPlus.Utils.Nulls.DStr(rd.GetValue(4)).Trim
                                    If txt <> String.Empty Then
                                        sb.Append(System.Environment.NewLine)
                                        sb.Append(txt)
                                    End If

                                    '-- FAX
                                    txt = DebtPlus.Utils.Nulls.DStr(rd.GetValue(5)).Trim
                                    If txt <> String.Empty Then
                                        sb.Append(System.Environment.NewLine)
                                        sb.Append(txt)
                                    End If

                                    If sb.Length <> 0 Then sb.Remove(0, 2)
                                    Answer = sb.ToString
                                End If
                            End With
                        End Using

                    Catch ex As Exception
                        Using gdr As New DebtPlus.Repository.GetDataResult()
                            gdr.HandleException(ex)
                            DebtPlus.UI.Common.ErrorHandling.HandleErrors(gdr, "Error reading office information for client")
                        End Using

                    Finally
                        If rd IsNot Nothing AndAlso Not rd.IsClosed Then rd.Close()
                        If cn IsNot Nothing Then
                            If cn.State <> System.Data.ConnectionState.Closed Then cn.Close()
                            cn.Dispose()
                        End If
                    End Try
                End If

                .Text = Answer
            End With
        End Sub

        ''' <summary>
        ''' Format the remit address before it is printed
        ''' </summary>
        Private Sub XrLabel_organization_address_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs)
            With CType(sender, DevExpress.XtraReports.UI.XRLabel)
                Dim rpt As DevExpress.XtraReports.UI.XtraReport = CType(.Report, DevExpress.XtraReports.UI.XtraReport)
                Dim sqlInfo As DebtPlus.LINQ.SQLInfoClass = DebtPlus.LINQ.SQLInfoClass.getDefault()

                If .Text = String.Empty Then
                    Dim cn As New System.Data.SqlClient.SqlConnection(sqlInfo.ConnectionString)
                    Dim rd As System.Data.SqlClient.SqlDataReader = Nothing

                    Try
                        cn.Open()

                        '-- Read the organization address
                        Dim text_block As New System.Text.StringBuilder
                        Using cmd As New System.Data.SqlClient.SqlCommand()
                            With cmd
                                .Connection = cn
                                .CommandText = "xpr_OrganizationAddress"
                                .CommandType = System.Data.CommandType.StoredProcedure
                                .CommandTimeout = 0
                                rd = .ExecuteReader(System.Data.CommandBehavior.CloseConnection Or System.Data.CommandBehavior.SingleRow)
                            End With
                        End Using

                        If rd IsNot Nothing AndAlso rd.Read Then

                            '-- Build the address string for the report.
                            For Each id As String In New String() {"name", "addr1", "addr2", "addr3"}
                                Dim col As System.Int32 = rd.GetOrdinal(id)
                                If Not rd.IsDBNull(col) Then
                                    Dim txt As String = rd.GetString(col).Trim
                                    If txt <> String.Empty Then
                                        text_block.Append(System.Environment.NewLine)
                                        text_block.Append(txt)
                                    End If
                                End If
                            Next
                        End If

                        '-- Set the address string
                        If text_block.Length > 0 Then text_block.Remove(0, 2)
                        .Text = text_block.ToString

                    Catch ex As System.Data.SqlClient.SqlException
                        Using gdr As New DebtPlus.Repository.GetDataResult()
                            gdr.HandleException(ex)
                            DebtPlus.UI.Common.ErrorHandling.HandleErrors(gdr, "Error reading organization address")
                        End Using

                    Finally
                        If rd IsNot Nothing AndAlso Not rd.IsClosed Then rd.Close()
                        If cn IsNot Nothing Then
                            If cn.State <> System.Data.ConnectionState.Closed Then cn.Close()
                            cn.Dispose()
                        End If
                    End Try
                End If
            End With
        End Sub
    End Class
End Namespace
