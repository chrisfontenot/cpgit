#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}

#End Region

Option Compare Binary
Option Explicit On
Option Strict On

Imports System.ComponentModel
Imports DebtPlus.Reports.Template

Imports System.Drawing.Printing

Namespace Operations.Client.Count.ByZipcode.Template
    Public Class ClientByZipcodeTemplateReport
        Inherits TemplateXtraReportClass

        Protected ds As New DataSet("ds")
        Protected vue As DataView = Nothing

        Protected Overridable Function GetDataSource() As DataView
            Return Nothing
        End Function

        Public Sub New()
            MyBase.New()
            InitializeComponent()
            AddHandler Me.BeforePrint, AddressOf Report_BeforePrint
        End Sub

#Region " Component Designer generated code "

        'Component overrides dispose to clean up the component list.
        Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
            If disposing Then
                If components IsNot Nothing Then
                    components.Dispose()
                End If
            End If
            MyBase.Dispose(disposing)
        End Sub

        Protected Friend WithEvents XrPanel1 As DevExpress.XtraReports.UI.XRPanel
        Protected Friend WithEvents XrControlStyle1 As DevExpress.XtraReports.UI.XRControlStyle
        Protected Friend WithEvents XrLabel1 As DevExpress.XtraReports.UI.XRLabel
        Protected Friend WithEvents XrLabel12 As DevExpress.XtraReports.UI.XRLabel
        Protected Friend WithEvents XrLabel11 As DevExpress.XtraReports.UI.XRLabel
        Protected Friend WithEvents XrLabel10 As DevExpress.XtraReports.UI.XRLabel
        Protected Friend WithEvents XrLabel9 As DevExpress.XtraReports.UI.XRLabel
        Protected Friend WithEvents XrLabel8 As DevExpress.XtraReports.UI.XRLabel
        Protected Friend WithEvents XrLabel7 As DevExpress.XtraReports.UI.XRLabel
        Protected Friend WithEvents XrLabel6 As DevExpress.XtraReports.UI.XRLabel
        Protected Friend WithEvents XrLabel5 As DevExpress.XtraReports.UI.XRLabel
        Protected Friend WithEvents XrLabel4 As DevExpress.XtraReports.UI.XRLabel
        Protected Friend WithEvents XrLabel3 As DevExpress.XtraReports.UI.XRLabel
        Protected Friend WithEvents XrLabel2 As DevExpress.XtraReports.UI.XRLabel
        Protected Friend WithEvents XrLabel_total As DevExpress.XtraReports.UI.XRLabel
        Protected Friend WithEvents XrLabel_cre As DevExpress.XtraReports.UI.XRLabel
        Protected Friend WithEvents XrLabel_apt As DevExpress.XtraReports.UI.XRLabel
        Protected Friend WithEvents XrLabel_wks As DevExpress.XtraReports.UI.XRLabel
        Protected Friend WithEvents XrLabel_pnd As DevExpress.XtraReports.UI.XRLabel
        Protected Friend WithEvents XrLabel_rdy As DevExpress.XtraReports.UI.XRLabel
        Protected Friend WithEvents XrLabel_pro As DevExpress.XtraReports.UI.XRLabel
        Protected Friend WithEvents XrLabel_a As DevExpress.XtraReports.UI.XRLabel
        Protected Friend WithEvents XrLabel_ar As DevExpress.XtraReports.UI.XRLabel
        Protected Friend WithEvents XrLabel_ex As DevExpress.XtraReports.UI.XRLabel
        Protected Friend WithEvents XrLabel_i As DevExpress.XtraReports.UI.XRLabel
        Protected Friend WithEvents XrLabel_zipcode As DevExpress.XtraReports.UI.XRLabel
        Protected Friend WithEvents ReportFooter As DevExpress.XtraReports.UI.ReportFooterBand
        Protected Friend WithEvents XrLabel_total_total As DevExpress.XtraReports.UI.XRLabel
        Protected Friend WithEvents XrLabel_total_cre As DevExpress.XtraReports.UI.XRLabel
        Protected Friend WithEvents XrLabel_total_apt As DevExpress.XtraReports.UI.XRLabel
        Protected Friend WithEvents XrLabel_total_wks As DevExpress.XtraReports.UI.XRLabel
        Protected Friend WithEvents XrLabel_total_pnd As DevExpress.XtraReports.UI.XRLabel
        Protected Friend WithEvents XrLabel_total_rdy As DevExpress.XtraReports.UI.XRLabel
        Protected Friend WithEvents XrLabel_total_pro As DevExpress.XtraReports.UI.XRLabel
        Protected Friend WithEvents XrLabel_total_a As DevExpress.XtraReports.UI.XRLabel
        Protected Friend WithEvents XrLabel_total_ar As DevExpress.XtraReports.UI.XRLabel
        Protected Friend WithEvents XrLabel_total_ex As DevExpress.XtraReports.UI.XRLabel
        Protected Friend WithEvents XrLabel_total_i As DevExpress.XtraReports.UI.XRLabel
        Protected Friend WithEvents XrLabel13 As DevExpress.XtraReports.UI.XRLabel
        Protected Friend WithEvents XrControlStyle2 As DevExpress.XtraReports.UI.XRControlStyle

        'Required by the Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Designer
        'It can be modified using the Designer.
        'Do not modify it using the code editor.
        <System.Diagnostics.DebuggerStepThrough()>
        Private Sub InitializeComponent()
            Dim XrSummary1 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary
            Dim XrSummary2 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary
            Dim XrSummary3 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary
            Dim XrSummary4 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary
            Dim XrSummary5 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary
            Dim XrSummary6 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary
            Dim XrSummary7 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary
            Dim XrSummary8 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary
            Dim XrSummary9 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary
            Dim XrSummary10 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary
            Dim XrSummary11 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary
            Me.XrPanel1 = New DevExpress.XtraReports.UI.XRPanel
            Me.XrLabel12 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel11 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel10 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel9 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel8 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel7 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel6 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel5 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel4 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel3 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel2 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel1 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrControlStyle1 = New DevExpress.XtraReports.UI.XRControlStyle
            Me.ReportFooter = New DevExpress.XtraReports.UI.ReportFooterBand
            Me.XrLabel_total_total = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_total_cre = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_total_apt = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_total_wks = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_total_pnd = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_total_rdy = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_total_pro = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_total_a = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_total_ar = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_total_ex = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_total_i = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel13 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrControlStyle2 = New DevExpress.XtraReports.UI.XRControlStyle
            Me.XrLabel_zipcode = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_i = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_ex = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_ar = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_a = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_pro = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_rdy = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_pnd = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_wks = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_apt = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_cre = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_total = New DevExpress.XtraReports.UI.XRLabel
            CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
            '
            'Detail
            '
            Me.Detail.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel_total, Me.XrLabel_cre, Me.XrLabel_apt, Me.XrLabel_wks, Me.XrLabel_pnd, Me.XrLabel_rdy, Me.XrLabel_pro, Me.XrLabel_a, Me.XrLabel_ar, Me.XrLabel_ex, Me.XrLabel_i, Me.XrLabel_zipcode})
            Me.Detail.Height = 15
            '
            'PageHeader
            '
            Me.PageHeader.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrPanel1})
            Me.PageHeader.Height = 145
            '
            'XrPanel1
            '
            Me.XrPanel1.BackColor = System.Drawing.Color.Teal
            Me.XrPanel1.BorderColor = System.Drawing.Color.Teal
            Me.XrPanel1.CanGrow = False
            Me.XrPanel1.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel12, Me.XrLabel11, Me.XrLabel10, Me.XrLabel9, Me.XrLabel8, Me.XrLabel7, Me.XrLabel6, Me.XrLabel5, Me.XrLabel4, Me.XrLabel3, Me.XrLabel2, Me.XrLabel1})
            Me.XrPanel1.KeepTogether = False
            Me.XrPanel1.Location = New System.Drawing.Point(0, 117)
            Me.XrPanel1.Name = "XrPanel1"
            Me.XrPanel1.Size = New System.Drawing.Size(800, 18)
            Me.XrPanel1.StyleName = "XrControlStyle1"
            Me.XrPanel1.StylePriority.UseBackColor = False
            Me.XrPanel1.StylePriority.UseBorderColor = False
            '
            'XrLabel12
            '
            Me.XrLabel12.Location = New System.Drawing.Point(697, 0)
            Me.XrLabel12.Name = "XrLabel12"
            Me.XrLabel12.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel12.Size = New System.Drawing.Size(52, 15)
            Me.XrLabel12.StylePriority.UseTextAlignment = False
            Me.XrLabel12.Text = "TOTAL"
            Me.XrLabel12.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel11
            '
            Me.XrLabel11.Location = New System.Drawing.Point(625, 0)
            Me.XrLabel11.Name = "XrLabel11"
            Me.XrLabel11.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel11.Size = New System.Drawing.Size(42, 15)
            Me.XrLabel11.StylePriority.UseTextAlignment = False
            Me.XrLabel11.Text = "I"
            Me.XrLabel11.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel10
            '
            Me.XrLabel10.Location = New System.Drawing.Point(566, 0)
            Me.XrLabel10.Name = "XrLabel10"
            Me.XrLabel10.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel10.Size = New System.Drawing.Size(41, 15)
            Me.XrLabel10.StylePriority.UseTextAlignment = False
            Me.XrLabel10.Text = "EX"
            Me.XrLabel10.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel9
            '
            Me.XrLabel9.Location = New System.Drawing.Point(508, 0)
            Me.XrLabel9.Name = "XrLabel9"
            Me.XrLabel9.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel9.Size = New System.Drawing.Size(41, 15)
            Me.XrLabel9.StylePriority.UseTextAlignment = False
            Me.XrLabel9.Text = "AR"
            Me.XrLabel9.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel8
            '
            Me.XrLabel8.Location = New System.Drawing.Point(450, 0)
            Me.XrLabel8.Name = "XrLabel8"
            Me.XrLabel8.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel8.Size = New System.Drawing.Size(41, 15)
            Me.XrLabel8.StylePriority.UseTextAlignment = False
            Me.XrLabel8.Text = "A"
            Me.XrLabel8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel7
            '
            Me.XrLabel7.Location = New System.Drawing.Point(158, 0)
            Me.XrLabel7.Name = "XrLabel7"
            Me.XrLabel7.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel7.Size = New System.Drawing.Size(41, 15)
            Me.XrLabel7.StylePriority.UseTextAlignment = False
            Me.XrLabel7.Text = "APT"
            Me.XrLabel7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel6
            '
            Me.XrLabel6.Location = New System.Drawing.Point(216, 0)
            Me.XrLabel6.Name = "XrLabel6"
            Me.XrLabel6.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel6.Size = New System.Drawing.Size(41, 15)
            Me.XrLabel6.StylePriority.UseTextAlignment = False
            Me.XrLabel6.Text = "WKS"
            Me.XrLabel6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel5
            '
            Me.XrLabel5.Location = New System.Drawing.Point(275, 0)
            Me.XrLabel5.Name = "XrLabel5"
            Me.XrLabel5.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel5.Size = New System.Drawing.Size(41, 15)
            Me.XrLabel5.StylePriority.UseTextAlignment = False
            Me.XrLabel5.Text = "PND"
            Me.XrLabel5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel4
            '
            Me.XrLabel4.Location = New System.Drawing.Point(333, 0)
            Me.XrLabel4.Name = "XrLabel4"
            Me.XrLabel4.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel4.Size = New System.Drawing.Size(41, 15)
            Me.XrLabel4.StylePriority.UseTextAlignment = False
            Me.XrLabel4.Text = "RDY"
            Me.XrLabel4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel3
            '
            Me.XrLabel3.Location = New System.Drawing.Point(383, 0)
            Me.XrLabel3.Name = "XrLabel3"
            Me.XrLabel3.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel3.Size = New System.Drawing.Size(41, 15)
            Me.XrLabel3.StylePriority.UseTextAlignment = False
            Me.XrLabel3.Text = "PRO"
            Me.XrLabel3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel2
            '
            Me.XrLabel2.Location = New System.Drawing.Point(100, 0)
            Me.XrLabel2.Name = "XrLabel2"
            Me.XrLabel2.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel2.Size = New System.Drawing.Size(41, 15)
            Me.XrLabel2.StylePriority.UseTextAlignment = False
            Me.XrLabel2.Text = "CRE"
            Me.XrLabel2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel1
            '
            Me.XrLabel1.Location = New System.Drawing.Point(0, 1)
            Me.XrLabel1.Name = "XrLabel1"
            Me.XrLabel1.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 0, 0, 0, 100.0!)
            Me.XrLabel1.Size = New System.Drawing.Size(65, 15)
            Me.XrLabel1.StylePriority.UsePadding = False
            Me.XrLabel1.StylePriority.UseTextAlignment = False
            Me.XrLabel1.Text = "ZIPCODE"
            Me.XrLabel1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrControlStyle1
            '
            Me.XrControlStyle1.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.XrControlStyle1.ForeColor = System.Drawing.Color.White
            Me.XrControlStyle1.Name = "XrControlStyle1"
            Me.XrControlStyle1.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
            Me.XrControlStyle1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            '
            'ReportFooter
            '
            Me.ReportFooter.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel_total_total, Me.XrLabel_total_cre, Me.XrLabel_total_apt, Me.XrLabel_total_wks, Me.XrLabel_total_pnd, Me.XrLabel_total_rdy, Me.XrLabel_total_pro, Me.XrLabel_total_a, Me.XrLabel_total_ar, Me.XrLabel_total_ex, Me.XrLabel_total_i, Me.XrLabel13})
            Me.ReportFooter.Height = 42
            Me.ReportFooter.Name = "ReportFooter"
            Me.ReportFooter.StyleName = "XrControlStyle2"
            '
            'XrLabel_total_total
            '
            Me.XrLabel_total_total.CanGrow = False
            Me.XrLabel_total_total.Location = New System.Drawing.Point(667, 17)
            Me.XrLabel_total_total.Name = "XrLabel_total_total"
            Me.XrLabel_total_total.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_total_total.Size = New System.Drawing.Size(82, 15)
            Me.XrLabel_total_total.StylePriority.UseTextAlignment = False
            XrSummary1.FormatString = "{0:n0}"
            XrSummary1.Running = DevExpress.XtraReports.UI.SummaryRunning.Report
            Me.XrLabel_total_total.Summary = XrSummary1
            Me.XrLabel_total_total.Text = "0"
            Me.XrLabel_total_total.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrLabel_total_total.WordWrap = False
            '
            'XrLabel_total_cre
            '
            Me.XrLabel_total_cre.CanGrow = False
            Me.XrLabel_total_cre.Location = New System.Drawing.Point(83, 17)
            Me.XrLabel_total_cre.Name = "XrLabel_total_cre"
            Me.XrLabel_total_cre.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_total_cre.Size = New System.Drawing.Size(57, 15)
            Me.XrLabel_total_cre.StylePriority.UseTextAlignment = False
            XrSummary2.FormatString = "{0:n0}"
            XrSummary2.Running = DevExpress.XtraReports.UI.SummaryRunning.Report
            Me.XrLabel_total_cre.Summary = XrSummary2
            Me.XrLabel_total_cre.Text = "0"
            Me.XrLabel_total_cre.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrLabel_total_cre.WordWrap = False
            '
            'XrLabel_total_apt
            '
            Me.XrLabel_total_apt.CanGrow = False
            Me.XrLabel_total_apt.Location = New System.Drawing.Point(142, 17)
            Me.XrLabel_total_apt.Name = "XrLabel_total_apt"
            Me.XrLabel_total_apt.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_total_apt.Size = New System.Drawing.Size(57, 15)
            Me.XrLabel_total_apt.StylePriority.UseTextAlignment = False
            XrSummary3.FormatString = "{0:n0}"
            XrSummary3.Running = DevExpress.XtraReports.UI.SummaryRunning.Report
            Me.XrLabel_total_apt.Summary = XrSummary3
            Me.XrLabel_total_apt.Text = "0"
            Me.XrLabel_total_apt.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrLabel_total_apt.WordWrap = False
            '
            'XrLabel_total_wks
            '
            Me.XrLabel_total_wks.CanGrow = False
            Me.XrLabel_total_wks.Location = New System.Drawing.Point(200, 17)
            Me.XrLabel_total_wks.Name = "XrLabel_total_wks"
            Me.XrLabel_total_wks.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_total_wks.Size = New System.Drawing.Size(56, 15)
            Me.XrLabel_total_wks.StylePriority.UseTextAlignment = False
            XrSummary4.FormatString = "{0:n0}"
            XrSummary4.Running = DevExpress.XtraReports.UI.SummaryRunning.Report
            Me.XrLabel_total_wks.Summary = XrSummary4
            Me.XrLabel_total_wks.Text = "0"
            Me.XrLabel_total_wks.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrLabel_total_wks.WordWrap = False
            '
            'XrLabel_total_pnd
            '
            Me.XrLabel_total_pnd.CanGrow = False
            Me.XrLabel_total_pnd.Location = New System.Drawing.Point(258, 17)
            Me.XrLabel_total_pnd.Name = "XrLabel_total_pnd"
            Me.XrLabel_total_pnd.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_total_pnd.Size = New System.Drawing.Size(57, 15)
            Me.XrLabel_total_pnd.StylePriority.UseTextAlignment = False
            XrSummary5.FormatString = "{0:n0}"
            XrSummary5.Running = DevExpress.XtraReports.UI.SummaryRunning.Report
            Me.XrLabel_total_pnd.Summary = XrSummary5
            Me.XrLabel_total_pnd.Text = "0"
            Me.XrLabel_total_pnd.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrLabel_total_pnd.WordWrap = False
            '
            'XrLabel_total_rdy
            '
            Me.XrLabel_total_rdy.CanGrow = False
            Me.XrLabel_total_rdy.Location = New System.Drawing.Point(317, 17)
            Me.XrLabel_total_rdy.Name = "XrLabel_total_rdy"
            Me.XrLabel_total_rdy.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_total_rdy.Size = New System.Drawing.Size(57, 15)
            Me.XrLabel_total_rdy.StylePriority.UseTextAlignment = False
            XrSummary6.FormatString = "{0:n0}"
            XrSummary6.Running = DevExpress.XtraReports.UI.SummaryRunning.Report
            Me.XrLabel_total_rdy.Summary = XrSummary6
            Me.XrLabel_total_rdy.Text = "0"
            Me.XrLabel_total_rdy.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrLabel_total_rdy.WordWrap = False
            '
            'XrLabel_total_pro
            '
            Me.XrLabel_total_pro.CanGrow = False
            Me.XrLabel_total_pro.Location = New System.Drawing.Point(375, 17)
            Me.XrLabel_total_pro.Name = "XrLabel_total_pro"
            Me.XrLabel_total_pro.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_total_pro.Size = New System.Drawing.Size(49, 15)
            Me.XrLabel_total_pro.StylePriority.UseTextAlignment = False
            XrSummary7.FormatString = "{0:n0}"
            XrSummary7.Running = DevExpress.XtraReports.UI.SummaryRunning.Report
            Me.XrLabel_total_pro.Summary = XrSummary7
            Me.XrLabel_total_pro.Text = "0"
            Me.XrLabel_total_pro.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrLabel_total_pro.WordWrap = False
            '
            'XrLabel_total_a
            '
            Me.XrLabel_total_a.CanGrow = False
            Me.XrLabel_total_a.Location = New System.Drawing.Point(425, 17)
            Me.XrLabel_total_a.Name = "XrLabel_total_a"
            Me.XrLabel_total_a.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_total_a.Size = New System.Drawing.Size(66, 15)
            Me.XrLabel_total_a.StylePriority.UseTextAlignment = False
            XrSummary8.FormatString = "{0:n0}"
            XrSummary8.Running = DevExpress.XtraReports.UI.SummaryRunning.Report
            Me.XrLabel_total_a.Summary = XrSummary8
            Me.XrLabel_total_a.Text = "0"
            Me.XrLabel_total_a.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrLabel_total_a.WordWrap = False
            '
            'XrLabel_total_ar
            '
            Me.XrLabel_total_ar.CanGrow = False
            Me.XrLabel_total_ar.Location = New System.Drawing.Point(492, 17)
            Me.XrLabel_total_ar.Name = "XrLabel_total_ar"
            Me.XrLabel_total_ar.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_total_ar.Size = New System.Drawing.Size(57, 15)
            Me.XrLabel_total_ar.StylePriority.UseTextAlignment = False
            XrSummary9.FormatString = "{0:n0}"
            XrSummary9.Running = DevExpress.XtraReports.UI.SummaryRunning.Report
            Me.XrLabel_total_ar.Summary = XrSummary9
            Me.XrLabel_total_ar.Text = "0"
            Me.XrLabel_total_ar.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrLabel_total_ar.WordWrap = False
            '
            'XrLabel_total_ex
            '
            Me.XrLabel_total_ex.CanGrow = False
            Me.XrLabel_total_ex.Location = New System.Drawing.Point(550, 17)
            Me.XrLabel_total_ex.Name = "XrLabel_total_ex"
            Me.XrLabel_total_ex.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_total_ex.Size = New System.Drawing.Size(56, 15)
            Me.XrLabel_total_ex.StylePriority.UseTextAlignment = False
            XrSummary10.FormatString = "{0:n0}"
            XrSummary10.Running = DevExpress.XtraReports.UI.SummaryRunning.Report
            Me.XrLabel_total_ex.Summary = XrSummary10
            Me.XrLabel_total_ex.Text = "0"
            Me.XrLabel_total_ex.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrLabel_total_ex.WordWrap = False
            '
            'XrLabel_total_i
            '
            Me.XrLabel_total_i.CanGrow = False
            Me.XrLabel_total_i.Location = New System.Drawing.Point(608, 17)
            Me.XrLabel_total_i.Name = "XrLabel_total_i"
            Me.XrLabel_total_i.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_total_i.Size = New System.Drawing.Size(58, 15)
            Me.XrLabel_total_i.StylePriority.UseTextAlignment = False
            XrSummary11.FormatString = "{0:n0}"
            XrSummary11.Running = DevExpress.XtraReports.UI.SummaryRunning.Report
            Me.XrLabel_total_i.Summary = XrSummary11
            Me.XrLabel_total_i.Text = "0"
            Me.XrLabel_total_i.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrLabel_total_i.WordWrap = False
            '
            'XrLabel13
            '
            Me.XrLabel13.CanGrow = False
            Me.XrLabel13.Location = New System.Drawing.Point(0, 17)
            Me.XrLabel13.Name = "XrLabel13"
            Me.XrLabel13.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel13.Size = New System.Drawing.Size(65, 15)
            Me.XrLabel13.StylePriority.UsePadding = False
            Me.XrLabel13.StylePriority.UseTextAlignment = False
            Me.XrLabel13.Text = "TOTAL"
            Me.XrLabel13.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            Me.XrLabel13.WordWrap = False
            '
            'XrControlStyle2
            '
            Me.XrControlStyle2.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.XrControlStyle2.Name = "XrControlStyle2"
            Me.XrControlStyle2.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
            Me.XrControlStyle2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel_zipcode
            '
            Me.XrLabel_zipcode.Location = New System.Drawing.Point(0, 0)
            Me.XrLabel_zipcode.Name = "XrLabel_zipcode"
            Me.XrLabel_zipcode.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_zipcode.Size = New System.Drawing.Size(65, 15)
            Me.XrLabel_zipcode.StylePriority.UsePadding = False
            Me.XrLabel_zipcode.StylePriority.UseTextAlignment = False
            Me.XrLabel_zipcode.Text = "00000"
            Me.XrLabel_zipcode.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            '
            'XrLabel_i
            '
            Me.XrLabel_i.Location = New System.Drawing.Point(625, 0)
            Me.XrLabel_i.Name = "XrLabel_i"
            Me.XrLabel_i.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_i.Size = New System.Drawing.Size(42, 15)
            Me.XrLabel_i.StylePriority.UseTextAlignment = False
            Me.XrLabel_i.Text = "0"
            Me.XrLabel_i.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel_ex
            '
            Me.XrLabel_ex.Location = New System.Drawing.Point(566, 0)
            Me.XrLabel_ex.Name = "XrLabel_ex"
            Me.XrLabel_ex.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_ex.Size = New System.Drawing.Size(41, 15)
            Me.XrLabel_ex.StylePriority.UseTextAlignment = False
            Me.XrLabel_ex.Text = "0"
            Me.XrLabel_ex.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel_ar
            '
            Me.XrLabel_ar.Location = New System.Drawing.Point(508, 0)
            Me.XrLabel_ar.Name = "XrLabel_ar"
            Me.XrLabel_ar.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_ar.Size = New System.Drawing.Size(41, 15)
            Me.XrLabel_ar.StylePriority.UseTextAlignment = False
            Me.XrLabel_ar.Text = "0"
            Me.XrLabel_ar.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel_a
            '
            Me.XrLabel_a.Location = New System.Drawing.Point(450, 0)
            Me.XrLabel_a.Name = "XrLabel_a"
            Me.XrLabel_a.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_a.Size = New System.Drawing.Size(41, 15)
            Me.XrLabel_a.StylePriority.UseTextAlignment = False
            Me.XrLabel_a.Text = "0"
            Me.XrLabel_a.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel_pro
            '
            Me.XrLabel_pro.Location = New System.Drawing.Point(383, 0)
            Me.XrLabel_pro.Name = "XrLabel_pro"
            Me.XrLabel_pro.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_pro.Size = New System.Drawing.Size(41, 15)
            Me.XrLabel_pro.StylePriority.UseTextAlignment = False
            Me.XrLabel_pro.Text = "0"
            Me.XrLabel_pro.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel_rdy
            '
            Me.XrLabel_rdy.Location = New System.Drawing.Point(333, 0)
            Me.XrLabel_rdy.Name = "XrLabel_rdy"
            Me.XrLabel_rdy.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_rdy.Size = New System.Drawing.Size(41, 15)
            Me.XrLabel_rdy.StylePriority.UseTextAlignment = False
            Me.XrLabel_rdy.Text = "0"
            Me.XrLabel_rdy.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel_pnd
            '
            Me.XrLabel_pnd.Location = New System.Drawing.Point(275, 0)
            Me.XrLabel_pnd.Name = "XrLabel_pnd"
            Me.XrLabel_pnd.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_pnd.Size = New System.Drawing.Size(41, 15)
            Me.XrLabel_pnd.StylePriority.UseTextAlignment = False
            Me.XrLabel_pnd.Text = "0"
            Me.XrLabel_pnd.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel_wks
            '
            Me.XrLabel_wks.Location = New System.Drawing.Point(216, 0)
            Me.XrLabel_wks.Name = "XrLabel_wks"
            Me.XrLabel_wks.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_wks.Size = New System.Drawing.Size(41, 15)
            Me.XrLabel_wks.StylePriority.UseTextAlignment = False
            Me.XrLabel_wks.Text = "0"
            Me.XrLabel_wks.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel_apt
            '
            Me.XrLabel_apt.Location = New System.Drawing.Point(158, 0)
            Me.XrLabel_apt.Name = "XrLabel_apt"
            Me.XrLabel_apt.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_apt.Size = New System.Drawing.Size(41, 15)
            Me.XrLabel_apt.StylePriority.UseTextAlignment = False
            Me.XrLabel_apt.Text = "0"
            Me.XrLabel_apt.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel_cre
            '
            Me.XrLabel_cre.Location = New System.Drawing.Point(100, 0)
            Me.XrLabel_cre.Name = "XrLabel_cre"
            Me.XrLabel_cre.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_cre.Size = New System.Drawing.Size(41, 15)
            Me.XrLabel_cre.StylePriority.UseTextAlignment = False
            Me.XrLabel_cre.Text = "0"
            Me.XrLabel_cre.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel_total
            '
            Me.XrLabel_total.Location = New System.Drawing.Point(697, 0)
            Me.XrLabel_total.Name = "XrLabel_total"
            Me.XrLabel_total.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_total.Size = New System.Drawing.Size(52, 15)
            Me.XrLabel_total.StylePriority.UseTextAlignment = False
            Me.XrLabel_total.Text = "0"
            Me.XrLabel_total.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'ClientByZipcodeTemplateReport
            '
            Me.Bands.AddRange(New DevExpress.XtraReports.UI.Band() {Me.ReportFooter})
            Me.StyleSheet.AddRange(New DevExpress.XtraReports.UI.XRControlStyle() {Me.XrControlStyle1, Me.XrControlStyle2})
            Me.Version = "8.1"
            CType(Me, System.ComponentModel.ISupportInitialize).EndInit()
        End Sub

#End Region

        Public Overrides ReadOnly Property ReportTitle() As String
            Get
                Return "Client Count by Zipcode"
            End Get
        End Property

        Private Sub Report_BeforePrint(ByVal sender As Object, ByVal e As PrintEventArgs)

            ' Read the appointment list
            vue = GetDataSource()

            ' Bind the datasource to the items
            DataSource = vue

            XrLabel_zipcode.DataBindings.Add("Text", vue, "zipcode")

            XrLabel_cre.DataBindings.Add("Text", vue, "cre", "{0:n0}")
            XrLabel_apt.DataBindings.Add("Text", vue, "apt", "{0:n0}")
            XrLabel_wks.DataBindings.Add("Text", vue, "wks", "{0:n0}")
            XrLabel_pnd.DataBindings.Add("Text", vue, "pnd", "{0:n0}")
            XrLabel_rdy.DataBindings.Add("Text", vue, "rdy", "{0:n0}")
            XrLabel_pro.DataBindings.Add("Text", vue, "pro", "{0:n0}")
            XrLabel_a.DataBindings.Add("Text", vue, "a", "{0:n0}")
            XrLabel_ar.DataBindings.Add("Text", vue, "ar", "{0:n0}")
            XrLabel_ex.DataBindings.Add("Text", vue, "ex", "{0:n0}")
            XrLabel_i.DataBindings.Add("Text", vue, "i", "{0:n0}")
            XrLabel_total.DataBindings.Add("Text", vue, "total", "{0:n0}")

            ' Do the same to the report totals
            XrLabel_total_cre.DataBindings.Add("Text", vue, "cre")
            XrLabel_total_apt.DataBindings.Add("Text", vue, "apt")
            XrLabel_total_wks.DataBindings.Add("Text", vue, "wks")
            XrLabel_total_pnd.DataBindings.Add("Text", vue, "pnd")
            XrLabel_total_rdy.DataBindings.Add("Text", vue, "rdy")
            XrLabel_total_pro.DataBindings.Add("Text", vue, "pro")
            XrLabel_total_a.DataBindings.Add("Text", vue, "a")
            XrLabel_total_ar.DataBindings.Add("Text", vue, "ar")
            XrLabel_total_ex.DataBindings.Add("Text", vue, "ex")
            XrLabel_total_i.DataBindings.Add("Text", vue, "i")
            XrLabel_total_total.DataBindings.Add("Text", vue, "total")
        End Sub
    End Class
End Namespace
