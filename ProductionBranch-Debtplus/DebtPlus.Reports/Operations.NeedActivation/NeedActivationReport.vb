#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}

#End Region

Imports System.ComponentModel

Imports System.Drawing.Printing
Imports System.Data.SqlClient
Imports DevExpress.XtraReports.UI

Namespace Operations.NeedActivation
    Public Class NeedActivationReport

        Public Sub New()
            MyBase.New()
            InitializeComponent()
            AddHandler Me.BeforePrint, AddressOf Report_BeforePrint
        End Sub

        ''' <summary>
        ''' Return the report title information
        ''' </summary>
        Public Overrides ReadOnly Property ReportTitle() As String
            Get
                Return "Need Activation Report"
            End Get
        End Property


        ''' <summary>
        ''' Refresh the dataset with the new information
        ''' </summary>
        Dim ds As New DataSet("ds")

        Private Sub Report_BeforePrint(ByVal sender As Object, ByVal e As EventArgs)
            Dim tbl As DataTable = ds.Tables("rpt_activated")

            If tbl Is Nothing Then
                Dim cn As SqlConnection = New System.Data.SqlClient.SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
                Try
                    cn.Open()
                    Using cmd As SqlCommand = New System.Data.SqlClient.SqlCommand()
                        With cmd
                            .Connection = cn
                            .CommandText = "rpt_activated"
                            .CommandType = CommandType.StoredProcedure
                            .CommandTimeout = 0
                        End With

                        Using da As New SqlDataAdapter(cmd)
                            da.Fill(ds, "rpt_activated")
                            tbl = ds.Tables("rpt_activated")
                        End Using
                    End Using

                Catch ex As Exception
                    DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Database error")

                Finally
                    If cn IsNot Nothing Then cn.Dispose()
                End Try
            End If

            If tbl IsNot Nothing Then
                Dim vue As DataView = tbl.DefaultView
                DataSource = vue

                With GroupHeader1
                    .GroupFields.Clear()
                    .GroupFields.Add(New GroupField("counselor_name"))
                End With

                With XrLabel_client
                    AddHandler .BeforePrint, AddressOf XrLabel_client_BeforePrint
                End With

                With XrLabel_held_in_trust
                    .DataBindings.Clear()
                    .DataBindings.Add("Text", vue, "held_in_trust", "{0:c}")
                End With

                With XrLabel_start_date
                    .DataBindings.Clear()
                    .DataBindings.Add("Text", vue, "start_date", "{0:d}")
                End With

                With XrLabel_name
                    .DataBindings.Clear()
                    .DataBindings.Add("Text", vue, "name")
                End With
            End If
        End Sub


        ''' <summary>
        ''' Print the client using the standard formatter
        ''' </summary>
        Private Sub XrLabel_client_BeforePrint(ByVal sender As Object, ByVal e As PrintEventArgs)
            XrLabel_client.Text = String.Format("{0:0000000}", GetCurrentColumnValue("client"))
        End Sub
    End Class
End Namespace
