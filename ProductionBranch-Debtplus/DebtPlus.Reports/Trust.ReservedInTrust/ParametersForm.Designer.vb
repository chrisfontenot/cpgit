Namespace Trust.ReservedInTrust
    <Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
    Partial Class ParametersForm

        'Form overrides dispose to clean up the component list.
        <System.Diagnostics.DebuggerNonUserCode()> _
        Protected Overrides Sub Dispose(ByVal disposing As Boolean)
            If disposing Then
                If components IsNot Nothing Then
                    components.Dispose()
                End If
            End If
            MyBase.Dispose(disposing)
        End Sub

        'Required by the Windows Form Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Windows Form Designer
        'It can be modified using the Windows Form Designer.  
        'Do not modify it using the code editor.
        <System.Diagnostics.DebuggerStepThrough()> _
        Private Sub InitializeComponent()
            Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl
            Me.LabelControl2 = New DevExpress.XtraEditors.LabelControl
            Me.LookUpEdit_counselor = New DevExpress.XtraEditors.LookUpEdit
            Me.LookUpEdit_region = New DevExpress.XtraEditors.LookUpEdit
            CType(Me.DxErrorProvider1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LookUpEdit_counselor.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LookUpEdit_region.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.SuspendLayout()
            '
            'ButtonOK
            '
            Me.ButtonOK.TabIndex = 4
            '
            'ButtonCancel
            '
            Me.ButtonCancel.TabIndex = 5
            '
            'LabelControl1
            '
            Me.LabelControl1.Location = New System.Drawing.Point(14, 26)
            Me.LabelControl1.Name = "LabelControl1"
            Me.LabelControl1.Size = New System.Drawing.Size(48, 13)
            Me.LabelControl1.TabIndex = 0
            Me.LabelControl1.Text = "Counselor"
            '
            'LabelControl2
            '
            Me.LabelControl2.Location = New System.Drawing.Point(12, 53)
            Me.LabelControl2.Name = "LabelControl2"
            Me.LabelControl2.Size = New System.Drawing.Size(33, 13)
            Me.LabelControl2.TabIndex = 2
            Me.LabelControl2.Text = "Region"
            '
            'LookUpEdit_counselor
            '
            Me.LookUpEdit_counselor.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                        Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.LookUpEdit_counselor.Location = New System.Drawing.Point(68, 23)
            Me.LookUpEdit_counselor.Name = "LookUpEdit_counselor"
            Me.LookUpEdit_counselor.Properties.ActionButtonIndex = 1
            Me.LookUpEdit_counselor.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
            Me.LookUpEdit_counselor.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Delete), New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.LookUpEdit_counselor.Properties.Columns.AddRange(New DevExpress.XtraEditors.Controls.LookUpColumnInfo() {New DevExpress.XtraEditors.Controls.LookUpColumnInfo("item_key", "ID", 20, DevExpress.Utils.FormatType.Numeric, "f0", False, DevExpress.Utils.HorzAlignment.[Default], DevExpress.Data.ColumnSortOrder.None), New DevExpress.XtraEditors.Controls.LookUpColumnInfo("description", "Description", 20, DevExpress.Utils.FormatType.None, "", True, DevExpress.Utils.HorzAlignment.[Default], DevExpress.Data.ColumnSortOrder.None)})
            Me.LookUpEdit_counselor.Properties.DisplayMember = "description"
            Me.LookUpEdit_counselor.Properties.NullText = "All Values"
            Me.LookUpEdit_counselor.Properties.ShowFooter = False
            Me.LookUpEdit_counselor.Properties.ShowHeader = False
            Me.LookUpEdit_counselor.Properties.ValueMember = "item_key"
            Me.LookUpEdit_counselor.Size = New System.Drawing.Size(174, 20)
            Me.LookUpEdit_counselor.TabIndex = 1
            Me.LookUpEdit_counselor.Properties.SortColumnIndex = 1
            '
            'LookUpEdit_region
            '
            Me.LookUpEdit_region.Location = New System.Drawing.Point(68, 50)
            Me.LookUpEdit_region.Name = "LookUpEdit_region"
            Me.LookUpEdit_region.Properties.ActionButtonIndex = 1
            Me.LookUpEdit_region.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Delete), New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.LookUpEdit_region.Properties.Columns.AddRange(New DevExpress.XtraEditors.Controls.LookUpColumnInfo() {New DevExpress.XtraEditors.Controls.LookUpColumnInfo("item_key", "ID", 20, DevExpress.Utils.FormatType.Numeric, "f0", False, DevExpress.Utils.HorzAlignment.[Default], DevExpress.Data.ColumnSortOrder.None), New DevExpress.XtraEditors.Controls.LookUpColumnInfo("description", "Description", 20, DevExpress.Utils.FormatType.None, "", True, DevExpress.Utils.HorzAlignment.[Default], DevExpress.Data.ColumnSortOrder.None)})
            Me.LookUpEdit_region.Properties.DisplayMember = "description"
            Me.LookUpEdit_region.Properties.NullText = "All Values"
            Me.LookUpEdit_region.Properties.ShowFooter = False
            Me.LookUpEdit_region.Properties.ShowHeader = False
            Me.LookUpEdit_region.Properties.ValueMember = "item_key"
            Me.LookUpEdit_region.Size = New System.Drawing.Size(174, 20)
            Me.LookUpEdit_region.TabIndex = 3
            Me.LookUpEdit_region.Properties.SortColumnIndex = 1
            '
            'ParametersForm
            '
            Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
            Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
            Me.ClientSize = New System.Drawing.Size(336, 135)
            Me.Controls.Add(Me.LookUpEdit_counselor)
            Me.Controls.Add(Me.LabelControl1)
            Me.Controls.Add(Me.LookUpEdit_region)
            Me.Controls.Add(Me.LabelControl2)
            Me.Name = "ParametersForm"
            Me.ToolTipController1.SetSuperTip(Me, Nothing)
            Me.Controls.SetChildIndex(Me.LabelControl2, 0)
            Me.Controls.SetChildIndex(Me.LookUpEdit_region, 0)
            Me.Controls.SetChildIndex(Me.LabelControl1, 0)
            Me.Controls.SetChildIndex(Me.LookUpEdit_counselor, 0)
            Me.Controls.SetChildIndex(Me.ButtonOK, 0)
            Me.Controls.SetChildIndex(Me.ButtonCancel, 0)
            CType(Me.DxErrorProvider1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LookUpEdit_counselor.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LookUpEdit_region.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            Me.ResumeLayout(False)
            Me.PerformLayout()

        End Sub
        Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents LabelControl2 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents LookUpEdit_counselor As DevExpress.XtraEditors.LookUpEdit
        Friend WithEvents LookUpEdit_region As DevExpress.XtraEditors.LookUpEdit
    End Class
End Namespace