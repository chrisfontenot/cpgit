#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}
#End Region

Namespace Trust.ReservedInTrust
    Friend Class ParametersForm
        Inherits DebtPlus.Reports.Template.Forms.ReportParametersForm

        Private ReportParent As ReservedInTrustReport = Nothing
        Public Sub New(ByVal Parent As ReservedInTrustReport)
            MyClass.New()
            ReportParent = Parent
        End Sub

        Public Sub New()
            MyBase.New()
            InitializeComponent()
            AddHandler Me.Load, AddressOf ParametersForm_Load
            AddHandler LookUpEdit_counselor.ButtonClick, AddressOf LookUpEdit_ButtonClick
            AddHandler LookUpEdit_region.ButtonClick, AddressOf LookUpEdit_ButtonClick
        End Sub

        Public ReadOnly Property Parameter_Counselor() As Object
            Get
                If LookUpEdit_counselor.EditValue Is Nothing OrElse LookUpEdit_counselor.EditValue Is System.DBNull.Value Then Return System.DBNull.Value
                Return Convert.ToInt32(LookUpEdit_counselor.EditValue)
            End Get
        End Property

        Public ReadOnly Property Parameter_Region() As Object
            Get
                If LookUpEdit_region.EditValue Is Nothing OrElse LookUpEdit_region.EditValue Is System.DBNull.Value Then Return System.DBNull.Value
                Return Convert.ToInt32(LookUpEdit_region.EditValue)
            End Get
        End Property

        Private Sub ParametersForm_Load(ByVal sender As Object, ByVal e As System.EventArgs)

            With LookUpEdit_counselor
                .Properties.DataSource = ReportParent.GetCounselorsTable().DefaultView
            End With

            With LookUpEdit_region
                .Properties.DataSource = ReportParent.GetRegionsTable().DefaultView
            End With

            ButtonOK.Enabled = True
        End Sub

        Private Sub LookUpEdit_ButtonClick(ByVal sender As Object, ByVal e As DevExpress.XtraEditors.Controls.ButtonPressedEventArgs)
            With CType(sender, DevExpress.XtraEditors.LookUpEdit)
                If e.Button.Kind = DevExpress.XtraEditors.Controls.ButtonPredefines.Delete Then
                    .EditValue = Nothing
                End If
            End With
        End Sub
    End Class
End Namespace