#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}

#End Region

Imports DebtPlus.Reports.Template
Imports System.IO
Imports System.Reflection
Imports DebtPlus.Utils
Imports DevExpress.XtraReports.UI
Imports System.Data.SqlClient

Namespace Trust.ReservedInTrust
    Public Class ReservedInTrustReport
        Inherits DatedTemplateXtraReportClass

        ''' <summary>
        '''     Create an instance of our report
        ''' </summary>
        Public Sub New()
            MyBase.New()

            ' Name of the report file
            Const ReportName As String = "DebtPlus.Reports.Trust.ReservedInTrust.repx"

            ' See if there is a report reference that we can use
            Dim Fname As String = Path.Combine(DebtPlus.Configuration.Config.ReportsDirectory, ReportName)
            Dim UseDefault As Boolean = True
            Try
                If (Not DebtPlus.Configuration.Config.LocalReportsOnly) AndAlso System.IO.File.Exists(Fname) Then
                    LoadLayout(Fname)
                    UseDefault = False
                End If

            Catch ex As Exception
                DebtPlus.Svc.Common.ErrorHandling.HandleErrors(ex)
            End Try

            ' Load the standard report definition if we need a new item
            If UseDefault Then
                Dim asm As Assembly = Assembly.GetExecutingAssembly                                           ' changed
                Using ios As Stream = asm.GetManifestResourceStream(String.Format("DebtPlus.Reports.{0}", ReportName)) ' changed
                    If ios IsNot Nothing Then LoadLayout(ios)
                End Using
            End If

            ' Set the script references as needed
            ScriptReferences = DebtPlus.Utils.AssemblyTools.GetAllSiblingAssemblies()

            ' Update the sub-reports as well
            For Each BandPtr As Band In Bands
                For Each CtlPtr As XRControl In BandPtr.Controls
                    Dim Rpt As XRSubreport = TryCast(CtlPtr, XRSubreport)
                    If Rpt IsNot Nothing Then
                        Dim RptSrc As XtraReport = TryCast(Rpt.ReportSource, XtraReport)
                        If RptSrc IsNot Nothing Then
                            RptSrc.ScriptReferences = ScriptReferences
                        End If
                    End If
                Next
            Next

            ReportFilter.IsEnabled = True
        End Sub

        ''' <summary>
        '''     Title associated with the report
        ''' </summary>
        Public Overrides ReadOnly Property ReportTitle() As String
            Get
                Return "Trust Funds Reserved"
            End Get
        End Property

        ''' <summary>
        '''     SubTitle associated with the report
        ''' </summary>
        Public Overrides ReadOnly Property ReportSubTitle() As String
            Get
                Return "This report covers " + CounselorName() + " and " + regionName()
            End Get
        End Property

        ''' <summary>
        '''     Desired counselor ID
        ''' </summary>
        Public Property Parameter_Counselor() As Int32
            Get
                Return CType(Parameters("ParameterCounselor").Value, Int32)
            End Get
            Set(ByVal value As Int32)
                Parameters("ParameterCounselor").Value = value
            End Set
        End Property

        ''' <summary>
        '''     Desired region ID
        ''' </summary>
        Public Property Parameter_Region() As Int32
            Get
                Return CType(Parameters("ParameterRegion").Value, Int32)
            End Get
            Set(ByVal value As Int32)
                Parameters("ParameterRegion").Value = value
            End Set
        End Property

        ''' <summary>
        '''     Determine if parameters are needed to be asked
        ''' </summary>
        Public Overrides Function NeedParameters() As Boolean
            Return Parameter_Counselor < 0 OrElse Parameter_Region < 0
        End Function

        ''' <summary>
        '''     Set parameters generically
        ''' </summary>
        Public Overrides Sub SetReportParameter(ByVal Parameter As String, ByVal Value As Object)
            Select Case Parameter
                Case "Counselor"
                    Parameter_Counselor = DebtPlus.Utils.Nulls.DInt(Value, -1)
                Case "Region"
                    Parameter_Region = DebtPlus.Utils.Nulls.DInt(Value, -1)
                Case Else
                    MyBase.SetReportParameter(Parameter, Value)
            End Select
        End Sub

        ''' <summary>
        '''     Ask for the parameters if needed
        ''' </summary>
        Public Overrides Function RequestReportParameters() As DialogResult
            Dim answer As DialogResult = DialogResult.OK
            If NeedParameters() Then
                Using frm As New ParametersForm(Me)
                    answer = frm.ShowDialog()
                    Parameter_Counselor = DebtPlus.Utils.Nulls.DInt(frm.Parameter_Counselor, -1)
                    Parameter_Region = DebtPlus.Utils.Nulls.DInt(frm.Parameter_Region, -1)
                End Using
            End If
            Return answer
        End Function

        ''' <summary>
        '''     Obtain the name associated with the counselor ID
        ''' </summary>
        Private Function CounselorName() As String
            Dim Answer As String = String.Empty
            If Parameter_Counselor < 0 Then
                Answer = "all counselors"
            Else
                Dim tbl As DataTable = GetCounselorsTable()
                If tbl IsNot Nothing Then
                    Dim row As DataRow = tbl.Rows.Find(Parameter_Counselor)
                    If row IsNot Nothing AndAlso row("description") IsNot DBNull.Value Then
                        Answer = Convert.ToString(row("description")).Trim()
                    End If
                End If
            End If

            Return Answer
        End Function

        ''' <summary>
        '''     Obtain the name associated with the region ID
        ''' </summary>
        Private Function regionName() As String
            Dim Answer As String = String.Empty
            If Parameter_Region < 0 Then
                Answer = "all regions"
            Else
                Dim tbl As DataTable = GetRegionsTable()
                If tbl IsNot Nothing Then
                    Dim row As DataRow = tbl.Rows.Find(Parameter_Region)
                    If row IsNot Nothing Then
                        Answer = Convert.ToString(row("description")).Trim()
                    End If
                End If
            End If

            Return Answer
        End Function

        ''' <summary>
        '''     Read the list of counselors
        ''' </summary>
        Friend Function GetCounselorsTable() As DataTable
            Const TableName As String = "counselors"
            Dim tbl As DataTable = ds.Tables(TableName)

            If tbl Is Nothing Then
                Using cmd As SqlCommand = New System.Data.SqlClient.SqlCommand()
                    With cmd
                        .Connection = New System.Data.SqlClient.SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
                        .CommandText = "lst_counselors"
                        .CommandType = CommandType.StoredProcedure
                    End With

                    Using da As New SqlDataAdapter(cmd)
                        da.Fill(ds, TableName)
                    End Using
                End Using

                tbl = ds.Tables(TableName)
                With tbl
                    .PrimaryKey = New DataColumn() {.Columns("item_key")}

                    Dim row As DataRow = tbl.NewRow
                    With row
                        .Item("item_key") = -1
                        .Item("description") = "[All Counselors]"
                        .Item("default") = False
                        .Item("ActiveFlag") = True
                    End With
                    .Rows.InsertAt(row, 0)
                End With
            End If

            Return tbl
        End Function

        ''' <summary>
        '''     Read the list of regions
        ''' </summary>
        Friend Function GetRegionsTable() As DataTable
            Const TableName As String = "regions"
            Dim tbl As DataTable = ds.Tables(TableName)

            If tbl Is Nothing Then
                Using cmd As SqlCommand = New System.Data.SqlClient.SqlCommand()
                    With cmd
                        .Connection = New System.Data.SqlClient.SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
                        .CommandText = "lst_Regions"
                        .CommandType = CommandType.StoredProcedure
                    End With

                    Using da As New SqlDataAdapter(cmd)
                        da.Fill(ds, TableName)
                    End Using
                End Using

                tbl = ds.Tables(TableName)
                With tbl
                    .PrimaryKey = New DataColumn() {.Columns("item_key")}

                    Dim row As DataRow = tbl.NewRow
                    With row
                        .Item("item_key") = -1
                        .Item("description") = "[All Regions]"
                        .Item("default") = False
                        .Item("ActiveFlag") = True
                    End With
                    .Rows.InsertAt(row, 0)
                End With
            End If

            Return tbl
        End Function

        Private ds As New DataSet("ds")
    End Class
End Namespace
