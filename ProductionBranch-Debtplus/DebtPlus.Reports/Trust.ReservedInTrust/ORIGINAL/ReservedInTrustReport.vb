#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}
#End Region

Namespace Trust.ReservedInTrust
    Public Class ReservedInTrustReport

        ''' <summary>
        ''' Create an instance of our report
        ''' </summary>
        Public Sub New()
            MyBase.New()
            InitializeComponent()

            AddHandler BeforePrint, AddressOf Report_BeforePrint

            Parameter_Counselor = -1
            Parameter_Region = -1
            ReportFilter.IsEnabled = True
        End Sub

        Public Overrides ReadOnly Property ReportTitle() As String
            Get
                Return "Trust Funds Reserved"
            End Get
        End Property

        Public Overrides ReadOnly Property ReportSubTitle() As String
            Get
                Return "This report covers " + CounselorName() + " and " + regionName()
            End Get
        End Property

        Public Property Parameter_Counselor() As Int32
            Get
                Return CType(Parameters("ParameterCounselor").Value, Int32)
            End Get
            Set(ByVal value As Int32)
                Parameters("ParameterCounselor").Value = value
            End Set
        End Property

        Public Property Parameter_Region() As Int32
            Get
                Return CType(Parameters("ParameterRegion").Value, Int32)
            End Get
            Set(ByVal value As Int32)
                Parameters("ParameterRegion").Value = value
            End Set
        End Property

        Public Overrides Function NeedParameters() As Boolean
            Return Parameter_Counselor < 0 OrElse Parameter_Region < 0
        End Function

        Public Overrides Sub SetReportParameter(ByVal Parameter As String, ByVal Value As Object)
            Select Case Parameter
                Case "Counselor"
                    Parameter_Counselor = DebtPlus.Utils.Nulls.DInt(Value, -1)
                Case "Region"
                    Parameter_Region = DebtPlus.Utils.Nulls.DInt(Value, -1)
                Case Else
                    MyBase.SetReportParameter(Parameter, Value)
            End Select
        End Sub

        Public Overrides Function RequestReportParameters() As System.Windows.Forms.DialogResult
            Dim Answer As System.Windows.Forms.DialogResult = DialogResult.OK
            If NeedParameters() Then
                Using frm As New ParametersForm(Me)
                    Answer = frm.ShowDialog
                    Parameter_Counselor = DebtPlus.Utils.Nulls.DInt(frm.Parameter_Counselor, -1)
                    Parameter_Region = DebtPlus.Utils.Nulls.DInt(frm.Parameter_Region, -1)
                End Using
            End If
            Return Answer
        End Function

        Private ds As New System.Data.DataSet("ds")

        Private Sub ReservedInTrustReport_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs)
            Const TableName As String = "rpt_reserved_in_trust"
            Dim rpt As DevExpress.XtraReports.UI.XtraReport = CType(sender, DevExpress.XtraReports.UI.XtraReport)
            Dim sqlInfo As DebtPlus.LINQ.SQLInfoClass = DebtPlus.LINQ.SQLInfoClass.getDefault()

            Dim tbl As System.Data.DataTable = ds.Tables(TableName)
            If tbl Is Nothing Then
                Try
                    Using cmd As New System.Data.SqlClient.SqlCommand()
                        cmd.Connection = New System.Data.SqlClient.SqlConnection(sqlInfo.ConnectionString)
                        cmd.CommandText = "rpt_reserved_in_trust"
                        cmd.CommandType = System.Data.CommandType.StoredProcedure

                        Dim v As Int32 = DebtPlus.Utils.Nulls.DInt(rpt.Parameters("ParameterCounselor").Value)
                        If v >= 0 Then cmd.Parameters.Add("@counselor", System.Data.SqlDbType.Int).Value = v

                        v = DebtPlus.Utils.Nulls.DInt(rpt.Parameters("ParameterRegion").Value)
                        If v >= 0 Then cmd.Parameters.Add("@region", System.Data.SqlDbType.Int).Value = rpt.Parameters("ParameterRegion").Value
                        cmd.CommandTimeout = 0

                        Using da As New System.Data.SqlClient.SqlDataAdapter(cmd)
                            da.Fill(ds, TableName)
                            tbl = ds.Tables(TableName)
                        End Using
                    End Using

                Catch ex As System.Data.SqlClient.SqlException
                    Using gdr As New DebtPlus.Repository.GetDataResult()
                        gdr.HandleException(ex)
                        DebtPlus.UI.Common.ErrorHandling.HandleErrors(gdr, "Error reading report information")
                    End Using
                End Try
            End If

            If tbl IsNot Nothing Then
                rpt.DataSource = New System.Data.DataView(tbl, CType(rpt, DebtPlus.Interfaces.Reports.IReportFilter).ReportFilter.ViewFilter, "region, counselor, client", System.Data.DataViewRowState.CurrentRows)
            End If
        End Sub

        Private Function CounselorName() As String
            Dim Answer As String = String.Empty
            If Parameter_Counselor < 0 Then
                Answer = "all counselors"
            Else
                Dim tbl As System.Data.DataTable = GetCounselorsTable()
                If tbl IsNot Nothing Then
                    Dim row As System.Data.DataRow = tbl.Rows.Find(Parameter_Counselor)
                    If row IsNot Nothing AndAlso row("description") IsNot System.DBNull.Value Then
                        Answer = Convert.ToString(row("description")).Trim
                    End If
                End If
            End If

            Return Answer
        End Function

        Private Function regionName() As String
            Dim Answer As String = String.Empty
            If Parameter_Region < 0 Then
                Answer = "all regions"
            Else
                Dim tbl As System.Data.DataTable = GetRegionsTable()
                If tbl IsNot Nothing Then
                    Dim row As System.Data.DataRow = tbl.Rows.Find(Parameter_Region)
                    If row IsNot Nothing Then
                        Answer = Convert.ToString(row("description")).Trim
                    End If
                End If
            End If

            Return Answer
        End Function

        Friend Function GetCounselorsTable() As System.Data.DataTable
            Const TableName As String = "counselors"
            Dim tbl As System.Data.DataTable = ds.Tables(TableName)
            Dim sqlInfo As DebtPlus.LINQ.SQLInfoClass = DebtPlus.LINQ.SQLInfoClass.getDefault()

            If tbl Is Nothing Then
                Using cn As New System.Data.SqlClient.SqlConnection(SqlInfo.ConnectionString)
                    cn.Open()
                    Using cmd As New System.Data.SqlClient.SqlCommand()
                        cmd.Connection = cn
                        cmd.CommandText = "lst_counselors"
                        cmd.CommandType = System.Data.CommandType.StoredProcedure

                        Using da As New System.Data.SqlClient.SqlDataAdapter(cmd)
                            da.Fill(ds, TableName)
                        End Using
                    End Using
                End Using

                tbl = ds.Tables(TableName)
                tbl.PrimaryKey = New System.Data.DataColumn() {tbl.Columns("item_key")}

                Dim row As System.Data.DataRow = tbl.NewRow
                row.Item("item_key") = -1
                row.Item("description") = "[All Counselors]"
                row.Item("default") = False
                row.Item("ActiveFlag") = True
                tbl.Rows.InsertAt(row, 0)
            End If

            Return tbl
        End Function

        Friend Function GetRegionsTable() As System.Data.DataTable
            Const TableName As String = "regions"
            Dim tbl As System.Data.DataTable = ds.Tables(TableName)
            Dim sqlInfo As DebtPlus.LINQ.SQLInfoClass = DebtPlus.LINQ.SQLInfoClass.getDefault()

            If tbl Is Nothing Then
                Using cn As New System.Data.SqlClient.SqlConnection(SqlInfo.ConnectionString)
                    cn.Open()
                    Using cmd As New System.Data.SqlClient.SqlCommand()
                        cmd.Connection = cn
                        cmd.CommandText = "lst_Regions"
                        cmd.CommandType = System.Data.CommandType.StoredProcedure

                        Using da As New System.Data.SqlClient.SqlDataAdapter(cmd)
                            da.Fill(ds, TableName)
                        End Using
                    End Using
                End Using

                tbl = ds.Tables(TableName)
                tbl.PrimaryKey = New System.Data.DataColumn() {tbl.Columns("item_key")}

                Dim row As System.Data.DataRow = tbl.NewRow
                row.Item("item_key") = -1
                row.Item("description") = "[All Regions]"
                row.Item("default") = False
                row.Item("ActiveFlag") = True
                tbl.Rows.InsertAt(row, 0)
            End If

            Return tbl
        End Function
    End Class
End Namespace
