Namespace Trust.ReservedInTrust
    <Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
    Partial Class ReservedInTrustReport
        Inherits DebtPlus.Reports.Template.TemplateXtraReportClass

        'XtraReport overrides dispose to clean up the component list.
        <System.Diagnostics.DebuggerNonUserCode()> _
        Protected Overrides Sub Dispose(ByVal disposing As Boolean)
            If disposing Then
                If components IsNot Nothing Then
                    components.Dispose()
                End If
            End If
            MyBase.Dispose(disposing)
        End Sub

        'Required by the Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Designer
        'It can be modified using the Designer.  
        'Do not modify it using the code editor.
        <System.Diagnostics.DebuggerStepThrough()> _
        Private Sub InitializeComponent()
            Dim XrSummary1 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary()
            Dim XrSummary2 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary()
            Dim XrSummary3 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary()
            Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(ReservedInTrustReport))
            Me.GroupFooter1 = New DevExpress.XtraReports.UI.GroupFooterBand()
            Me.XrLine1 = New DevExpress.XtraReports.UI.XRLine()
            Me.XrLabel10 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_total_reserved_in_trust = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_total_held_in_trust = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_total_deposit_in_trust = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrControlStyle1 = New DevExpress.XtraReports.UI.XRControlStyle()
            Me.GroupFooter2 = New DevExpress.XtraReports.UI.GroupFooterBand()
            Me.GroupHeader1 = New DevExpress.XtraReports.UI.GroupHeaderBand()
            Me.XrLabel_counselor = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrPanel1 = New DevExpress.XtraReports.UI.XRPanel()
            Me.XrLabel4 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel5 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel3 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel2 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel1 = New DevExpress.XtraReports.UI.XRLabel()
            Me.GroupHeader2 = New DevExpress.XtraReports.UI.GroupHeaderBand()
            Me.XrLabel_region = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_deposit_in_trust = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_reserved_in_trust = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_reserved_in_trust_cutoff = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_client = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_held_in_trust = New DevExpress.XtraReports.UI.XRLabel()
            Me.ParameterCounselor = New DevExpress.XtraReports.Parameters.Parameter()
            Me.ParameterRegion = New DevExpress.XtraReports.Parameters.Parameter()
            CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
            '
            'PageHeader
            '
            Me.PageHeader.HeightF = 118.0!
            '
            'XrLabel_Title
            '
            Me.XrLabel_Title.StylePriority.UseFont = False
            '
            'XrPageInfo_PageNumber
            '
            Me.XrPageInfo_PageNumber.StylePriority.UseTextAlignment = False
            '
            'XRLabel_Agency_Name
            '
            Me.XRLabel_Agency_Name.StylePriority.UseTextAlignment = False
            '
            'XrLabel_Agency_Address3
            '
            Me.XrLabel_Agency_Address3.StylePriority.UseFont = False
            Me.XrLabel_Agency_Address3.StylePriority.UseTextAlignment = False
            '
            'XrLabel_Agency_Address1
            '
            Me.XrLabel_Agency_Address1.StylePriority.UseFont = False
            Me.XrLabel_Agency_Address1.StylePriority.UseTextAlignment = False
            '
            'XrLabel_Agency_Phone
            '
            Me.XrLabel_Agency_Phone.StylePriority.UseFont = False
            Me.XrLabel_Agency_Phone.StylePriority.UseTextAlignment = False
            '
            'XrLabel_Agency_Address2
            '
            Me.XrLabel_Agency_Address2.StylePriority.UseFont = False
            Me.XrLabel_Agency_Address2.StylePriority.UseTextAlignment = False
            '
            'Detail
            '
            Me.Detail.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel_held_in_trust, Me.XrLabel_client, Me.XrLabel_reserved_in_trust_cutoff, Me.XrLabel_reserved_in_trust, Me.XrLabel_deposit_in_trust})
            Me.Detail.HeightF = 17.0!
            Me.Detail.StylePriority.UseTextAlignment = False
            Me.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            '
            'GroupFooter1
            '
            Me.GroupFooter1.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLine1, Me.XrLabel10, Me.XrLabel_total_reserved_in_trust, Me.XrLabel_total_held_in_trust, Me.XrLabel_total_deposit_in_trust})
            Me.GroupFooter1.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.GroupFooter1.HeightF = 46.0!
            Me.GroupFooter1.Name = "GroupFooter1"
            Me.GroupFooter1.StylePriority.UseFont = False
            Me.GroupFooter1.StylePriority.UseTextAlignment = False
            Me.GroupFooter1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            '
            'XrLine1
            '
            Me.XrLine1.LocationFloat = New DevExpress.Utils.PointFloat(8.0!, 8.0!)
            Me.XrLine1.Name = "XrLine1"
            Me.XrLine1.SizeF = New System.Drawing.SizeF(733.0!, 8.0!)
            '
            'XrLabel10
            '
            Me.XrLabel10.LocationFloat = New DevExpress.Utils.PointFloat(8.0!, 17.0!)
            Me.XrLabel10.Name = "XrLabel10"
            Me.XrLabel10.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel10.SizeF = New System.Drawing.SizeF(100.0!, 15.0!)
            Me.XrLabel10.Text = "TOTALS"
            '
            'XrLabel_total_reserved_in_trust
            '
            Me.XrLabel_total_reserved_in_trust.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "reserved_in_trust")})
            Me.XrLabel_total_reserved_in_trust.LocationFloat = New DevExpress.Utils.PointFloat(442.0!, 17.0!)
            Me.XrLabel_total_reserved_in_trust.Name = "XrLabel_total_reserved_in_trust"
            Me.XrLabel_total_reserved_in_trust.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_total_reserved_in_trust.SizeF = New System.Drawing.SizeF(92.0!, 15.0!)
            Me.XrLabel_total_reserved_in_trust.StylePriority.UseTextAlignment = False
            XrSummary1.FormatString = "{0:c}"
            XrSummary1.Running = DevExpress.XtraReports.UI.SummaryRunning.Group
            Me.XrLabel_total_reserved_in_trust.Summary = XrSummary1
            Me.XrLabel_total_reserved_in_trust.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel_total_held_in_trust
            '
            Me.XrLabel_total_held_in_trust.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "held_in_trust")})
            Me.XrLabel_total_held_in_trust.LocationFloat = New DevExpress.Utils.PointFloat(250.0!, 17.0!)
            Me.XrLabel_total_held_in_trust.Name = "XrLabel_total_held_in_trust"
            Me.XrLabel_total_held_in_trust.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_total_held_in_trust.SizeF = New System.Drawing.SizeF(191.0!, 15.0!)
            Me.XrLabel_total_held_in_trust.StylePriority.UseTextAlignment = False
            XrSummary2.FormatString = "{0:c}"
            XrSummary2.Running = DevExpress.XtraReports.UI.SummaryRunning.Group
            Me.XrLabel_total_held_in_trust.Summary = XrSummary2
            Me.XrLabel_total_held_in_trust.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel_total_deposit_in_trust
            '
            Me.XrLabel_total_deposit_in_trust.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "deposit_in_trust")})
            Me.XrLabel_total_deposit_in_trust.LocationFloat = New DevExpress.Utils.PointFloat(650.0!, 17.0!)
            Me.XrLabel_total_deposit_in_trust.Name = "XrLabel_total_deposit_in_trust"
            Me.XrLabel_total_deposit_in_trust.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
            Me.XrLabel_total_deposit_in_trust.SizeF = New System.Drawing.SizeF(92.0!, 15.0!)
            Me.XrLabel_total_deposit_in_trust.StylePriority.UsePadding = False
            Me.XrLabel_total_deposit_in_trust.StylePriority.UseTextAlignment = False
            XrSummary3.FormatString = "{0:c}"
            XrSummary3.Running = DevExpress.XtraReports.UI.SummaryRunning.Group
            Me.XrLabel_total_deposit_in_trust.Summary = XrSummary3
            Me.XrLabel_total_deposit_in_trust.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrLabel_total_deposit_in_trust.WordWrap = False
            '
            'XrControlStyle1
            '
            Me.XrControlStyle1.BackColor = System.Drawing.Color.Teal
            Me.XrControlStyle1.BorderColor = System.Drawing.Color.Teal
            Me.XrControlStyle1.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.XrControlStyle1.ForeColor = System.Drawing.Color.White
            Me.XrControlStyle1.Name = "XrControlStyle1"
            Me.XrControlStyle1.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
            Me.XrControlStyle1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            '
            'GroupFooter2
            '
            Me.GroupFooter2.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.GroupFooter2.HeightF = 0.0!
            Me.GroupFooter2.Level = 1
            Me.GroupFooter2.Name = "GroupFooter2"
            Me.GroupFooter2.StylePriority.UseFont = False
            Me.GroupFooter2.StylePriority.UseTextAlignment = False
            Me.GroupFooter2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            '
            'GroupHeader1
            '
            Me.GroupHeader1.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel_counselor, Me.XrPanel1})
            Me.GroupHeader1.GroupFields.AddRange(New DevExpress.XtraReports.UI.GroupField() {New DevExpress.XtraReports.UI.GroupField("counselor", DevExpress.XtraReports.UI.XRColumnSortOrder.Ascending)})
            Me.GroupHeader1.HeightF = 74.0!
            Me.GroupHeader1.Name = "GroupHeader1"
            Me.GroupHeader1.RepeatEveryPage = True
            '
            'XrLabel_counselor
            '
            Me.XrLabel_counselor.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "counselor")})
            Me.XrLabel_counselor.Font = New System.Drawing.Font("Times New Roman", 12.0!, System.Drawing.FontStyle.Bold)
            Me.XrLabel_counselor.LocationFloat = New DevExpress.Utils.PointFloat(42.0!, 0.0!)
            Me.XrLabel_counselor.Name = "XrLabel_counselor"
            Me.XrLabel_counselor.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_counselor.SizeF = New System.Drawing.SizeF(708.0!, 25.0!)
            Me.XrLabel_counselor.StylePriority.UseFont = False
            Me.XrLabel_counselor.StylePriority.UseTextAlignment = False
            Me.XrLabel_counselor.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            '
            'XrPanel1
            '
            Me.XrPanel1.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel4, Me.XrLabel5, Me.XrLabel3, Me.XrLabel2, Me.XrLabel1})
            Me.XrPanel1.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 33.0!)
            Me.XrPanel1.Name = "XrPanel1"
            Me.XrPanel1.SizeF = New System.Drawing.SizeF(750.0!, 33.0!)
            Me.XrPanel1.StyleName = "XrControlStyle1"
            '
            'XrLabel4
            '
            Me.XrLabel4.LocationFloat = New DevExpress.Utils.PointFloat(541.0!, 18.0!)
            Me.XrLabel4.Name = "XrLabel4"
            Me.XrLabel4.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel4.SizeF = New System.Drawing.SizeF(100.0!, 15.0!)
            Me.XrLabel4.StylePriority.UseTextAlignment = False
            Me.XrLabel4.Text = "RELEASE"
            Me.XrLabel4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel5
            '
            Me.XrLabel5.LocationFloat = New DevExpress.Utils.PointFloat(650.0!, 1.0!)
            Me.XrLabel5.Multiline = True
            Me.XrLabel5.Name = "XrLabel5"
            Me.XrLabel5.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel5.SizeF = New System.Drawing.SizeF(92.0!, 32.0!)
            Me.XrLabel5.StylePriority.UsePadding = False
            Me.XrLabel5.StylePriority.UseTextAlignment = False
            Me.XrLabel5.Text = "PENDING" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "DEPOSIT"
            Me.XrLabel5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrLabel5.WordWrap = False
            '
            'XrLabel3
            '
            Me.XrLabel3.LocationFloat = New DevExpress.Utils.PointFloat(458.0!, 18.0!)
            Me.XrLabel3.Name = "XrLabel3"
            Me.XrLabel3.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel3.SizeF = New System.Drawing.SizeF(75.0!, 15.0!)
            Me.XrLabel3.StylePriority.UseTextAlignment = False
            Me.XrLabel3.Text = "RESERVE"
            Me.XrLabel3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel2
            '
            Me.XrLabel2.LocationFloat = New DevExpress.Utils.PointFloat(350.0!, 18.0!)
            Me.XrLabel2.Name = "XrLabel2"
            Me.XrLabel2.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel2.SizeF = New System.Drawing.SizeF(91.0!, 15.0!)
            Me.XrLabel2.StylePriority.UseTextAlignment = False
            Me.XrLabel2.Text = "TRUST BAL"
            Me.XrLabel2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel1
            '
            Me.XrLabel1.LocationFloat = New DevExpress.Utils.PointFloat(8.0!, 18.0!)
            Me.XrLabel1.Name = "XrLabel1"
            Me.XrLabel1.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel1.SizeF = New System.Drawing.SizeF(100.0!, 15.0!)
            Me.XrLabel1.Text = "CLIENT"
            '
            'GroupHeader2
            '
            Me.GroupHeader2.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel_region})
            Me.GroupHeader2.GroupFields.AddRange(New DevExpress.XtraReports.UI.GroupField() {New DevExpress.XtraReports.UI.GroupField("region", DevExpress.XtraReports.UI.XRColumnSortOrder.Ascending)})
            Me.GroupHeader2.HeightF = 35.0!
            Me.GroupHeader2.Level = 1
            Me.GroupHeader2.Name = "GroupHeader2"
            Me.GroupHeader2.RepeatEveryPage = True
            '
            'XrLabel_region
            '
            Me.XrLabel_region.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "region")})
            Me.XrLabel_region.Font = New System.Drawing.Font("Times New Roman", 12.0!, System.Drawing.FontStyle.Bold)
            Me.XrLabel_region.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
            Me.XrLabel_region.Name = "XrLabel_region"
            Me.XrLabel_region.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_region.SizeF = New System.Drawing.SizeF(750.0!, 25.0!)
            Me.XrLabel_region.StylePriority.UseFont = False
            Me.XrLabel_region.StylePriority.UseTextAlignment = False
            Me.XrLabel_region.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            '
            'XrLabel_deposit_in_trust
            '
            Me.XrLabel_deposit_in_trust.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "deposit_in_trust", "{0:c}")})
            Me.XrLabel_deposit_in_trust.LocationFloat = New DevExpress.Utils.PointFloat(650.0!, 0.0!)
            Me.XrLabel_deposit_in_trust.Name = "XrLabel_deposit_in_trust"
            Me.XrLabel_deposit_in_trust.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
            Me.XrLabel_deposit_in_trust.SizeF = New System.Drawing.SizeF(92.0!, 15.0!)
            Me.XrLabel_deposit_in_trust.StylePriority.UsePadding = False
            Me.XrLabel_deposit_in_trust.StylePriority.UseTextAlignment = False
            Me.XrLabel_deposit_in_trust.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrLabel_deposit_in_trust.WordWrap = False
            '
            'XrLabel_reserved_in_trust
            '
            Me.XrLabel_reserved_in_trust.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "reserved_in_trust", "{0:c}")})
            Me.XrLabel_reserved_in_trust.LocationFloat = New DevExpress.Utils.PointFloat(458.0!, 0.0!)
            Me.XrLabel_reserved_in_trust.Name = "XrLabel_reserved_in_trust"
            Me.XrLabel_reserved_in_trust.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_reserved_in_trust.SizeF = New System.Drawing.SizeF(75.0!, 15.0!)
            Me.XrLabel_reserved_in_trust.StylePriority.UseTextAlignment = False
            Me.XrLabel_reserved_in_trust.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel_reserved_in_trust_cutoff
            '
            Me.XrLabel_reserved_in_trust_cutoff.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "reserved_in_trust_cutoff", "{0:d}")})
            Me.XrLabel_reserved_in_trust_cutoff.LocationFloat = New DevExpress.Utils.PointFloat(541.0!, 0.0!)
            Me.XrLabel_reserved_in_trust_cutoff.Name = "XrLabel_reserved_in_trust_cutoff"
            Me.XrLabel_reserved_in_trust_cutoff.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_reserved_in_trust_cutoff.SizeF = New System.Drawing.SizeF(100.0!, 15.0!)
            Me.XrLabel_reserved_in_trust_cutoff.StylePriority.UseTextAlignment = False
            Me.XrLabel_reserved_in_trust_cutoff.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel_client
            '
            Me.XrLabel_client.LocationFloat = New DevExpress.Utils.PointFloat(8.0!, 0.0!)
            Me.XrLabel_client.Name = "XrLabel_client"
            Me.XrLabel_client.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_client.SizeF = New System.Drawing.SizeF(333.0!, 15.0!)
            Me.XrLabel_client.Text = "[client] [name]"
            '
            'XrLabel_held_in_trust
            '
            Me.XrLabel_held_in_trust.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "held_in_trust", "{0:c}")})
            Me.XrLabel_held_in_trust.LocationFloat = New DevExpress.Utils.PointFloat(350.0!, 0.0!)
            Me.XrLabel_held_in_trust.Name = "XrLabel_held_in_trust"
            Me.XrLabel_held_in_trust.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_held_in_trust.SizeF = New System.Drawing.SizeF(91.0!, 15.0!)
            Me.XrLabel_held_in_trust.StylePriority.UseTextAlignment = False
            Me.XrLabel_held_in_trust.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'ParameterCounselor
            '
            Me.ParameterCounselor.Description = "Counselor"
            Me.ParameterCounselor.Name = "ParameterCounselor"
            Me.ParameterCounselor.Type = GetType(Integer)
            Me.ParameterCounselor.Value = -1
            Me.ParameterCounselor.Visible = False
            '
            'ParameterRegion
            '
            Me.ParameterRegion.Description = "Region"
            Me.ParameterRegion.Name = "ParameterRegion"
            Me.ParameterRegion.Type = GetType(Integer)
            Me.ParameterRegion.Value = -1
            Me.ParameterRegion.Visible = False
            '
            'ReservedInTrustReport
            '
            Me.Bands.AddRange(New DevExpress.XtraReports.UI.Band() {Me.Detail, Me.PageHeader, Me.PageFooter, Me.TopMarginBand1, Me.BottomMarginBand1, Me.GroupFooter1, Me.GroupFooter2, Me.GroupHeader1, Me.GroupHeader2})
            Me.Parameters.AddRange(New DevExpress.XtraReports.Parameters.Parameter() {Me.ParameterCounselor, Me.ParameterRegion})
            Me.RequestParameters = False
            Me.ScriptLanguage = DevExpress.XtraReports.ScriptLanguage.VisualBasic
            Me.ScriptReferencesString = resources.GetString("$this.ScriptReferencesString")
            Me.Scripts.OnBeforePrint = "ReservedInTrustReport_BeforePrint"
            Me.ScriptsSource = resources.GetString("$this.ScriptsSource")
            Me.StyleSheet.AddRange(New DevExpress.XtraReports.UI.XRControlStyle() {Me.XrControlStyle_Header, Me.XrControlStyle_Totals, Me.XrControlStyle_HeaderPannel, Me.XrControlStyle_GroupHeader, Me.XrControlStyle1})
            Me.Version = "11.2"
            Me.Controls.SetChildIndex(Me.GroupHeader2, 0)
            Me.Controls.SetChildIndex(Me.GroupHeader1, 0)
            Me.Controls.SetChildIndex(Me.GroupFooter2, 0)
            Me.Controls.SetChildIndex(Me.GroupFooter1, 0)
            Me.Controls.SetChildIndex(Me.BottomMarginBand1, 0)
            Me.Controls.SetChildIndex(Me.TopMarginBand1, 0)
            Me.Controls.SetChildIndex(Me.PageFooter, 0)
            Me.Controls.SetChildIndex(Me.PageHeader, 0)
            Me.Controls.SetChildIndex(Me.Detail, 0)
            CType(Me, System.ComponentModel.ISupportInitialize).EndInit()

        End Sub
        Friend WithEvents GroupFooter1 As DevExpress.XtraReports.UI.GroupFooterBand
        Friend WithEvents XrControlStyle1 As DevExpress.XtraReports.UI.XRControlStyle
        Friend WithEvents GroupFooter2 As DevExpress.XtraReports.UI.GroupFooterBand
        Friend WithEvents GroupHeader1 As DevExpress.XtraReports.UI.GroupHeaderBand
        Friend WithEvents XrLabel_counselor As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrPanel1 As DevExpress.XtraReports.UI.XRPanel
        Friend WithEvents GroupHeader2 As DevExpress.XtraReports.UI.GroupHeaderBand
        Friend WithEvents XrLabel_region As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel4 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel5 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel3 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel2 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel1 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_held_in_trust As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_client As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_reserved_in_trust_cutoff As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_reserved_in_trust As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_deposit_in_trust As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLine1 As DevExpress.XtraReports.UI.XRLine
        Friend WithEvents XrLabel10 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_total_reserved_in_trust As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_total_held_in_trust As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_total_deposit_in_trust As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents ParameterCounselor As DevExpress.XtraReports.Parameters.Parameter
        Friend WithEvents ParameterRegion As DevExpress.XtraReports.Parameters.Parameter
    End Class
End Namespace
