#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}

#End Region

Imports System.Drawing.Printing
Imports DevExpress.XtraReports.UI

Namespace Disbursement.AfterDisbursement

    Public Class AfterDisbursementErrorReport

        Private tbl As DataTable = Nothing
        Private total_unallocated_rps As Decimal = 0D

        Public Sub New(ByVal tbl As DataTable, ByVal total_unallocated_rps As Decimal)
            MyBase.New()
            Me.tbl = tbl
            Me.total_unallocated_rps = total_unallocated_rps
            InitializeComponent()
            AddHandler Me.BeforePrint, AddressOf ErrorReport_BeforePrint
            AddHandler XrLabel_client.BeforePrint, AddressOf XrLabel_client_BeforePrint
        End Sub

        Private Sub ErrorReport_BeforePrint(ByVal sender As Object, ByVal e As PrintEventArgs)
            Dim vue As DataView = tbl.DefaultView
            DataSource = vue

            XrLabel_header.Text = String.Format("The trust balances will be off by {0:c} due to having creditors which are configured to use RPPS and have a biller ID field which is not in the list of valid biller IDs from Mastercard. These funds can not be disbursed until the biller ID is added to the biller table. The specific transactions are listed below:", total_unallocated_rps)
        End Sub

        Private Sub XrLabel_client_BeforePrint(ByVal sender As Object, ByVal e As PrintEventArgs)
            With CType(sender, XRLabel)
                .Text = String.Format("{0:0000000}", Convert.ToInt32(GetCurrentColumnValue("client")))
            End With
        End Sub

    End Class
End Namespace