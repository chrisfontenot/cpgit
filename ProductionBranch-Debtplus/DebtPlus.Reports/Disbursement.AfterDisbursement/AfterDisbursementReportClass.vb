#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}

#End Region

Option Compare Binary
Option Explicit On
Option Strict On

Imports System.ComponentModel
Imports DebtPlus.Reports.Template
Imports DevExpress.XtraReports.UI

Imports System.Drawing.Printing
Imports DebtPlus.Reports.Template.Forms
Imports System.Data.SqlClient

Namespace Disbursement.AfterDisbursement
    Public Class AfterDisbursementReport
        Inherits TemplateXtraReportClass

        Private _disbursement_register As Int32 = 0
        Friend WithEvents XrSubreport1 As XRSubreport
        Private SubTitleString As String = String.Empty

        ''' <summary>
        ''' Create an instance of our report
        ''' </summary>
        Public Sub New()
            MyBase.New()
            InitializeComponent()
            AddHandler BeforePrint, AddressOf AfterDisbursementReport_BeforePrint
            AddHandler XrTableCell_Deducted_RPPS.BeforePrint, AddressOf XrTableCell_Deducted_RPPS_BeforePrint
            AddHandler XrTableCell_Gross_Total.BeforePrint, AddressOf XrTableCell_Gross_Total_BeforePrint
            AddHandler XrTableCell_Deducted_Total.BeforePrint, AddressOf XrTableCell_Deducted_Total_BeforePrint
            AddHandler XrTableCell_Net_Total.BeforePrint, AddressOf XrTableCell_Net_Total_BeforePrint
            AddHandler XrTableCell_Billed_Total.BeforePrint, AddressOf XrTableCell_Billed_Total_BeforePrint
        End Sub

        ''' <summary>
        ''' Create an instance of our report
        ''' </summary>
        Public Sub New(ByVal Container As IContainer)
            MyClass.New()
            Container.Add(Me)
        End Sub


        ''' <summary>
        ''' External linkage to set a parameter value
        ''' </summary>
        Public Overrides Sub SetReportParameter(ByVal Parameter As String, ByVal Value As Object)
            Select Case Parameter
                Case "DisbursementRegister"
                    Parameter_DisbursementRegister = Convert.ToInt32(Value)
                Case Else
                    MyBase.SetReportParameter(Parameter, Value)
            End Select
        End Sub


        ''' <summary>
        ''' Disbursement Register parameter for the report
        ''' </summary>
        Public Property Parameter_DisbursementRegister() As Int32
            Get
                Return _disbursement_register
            End Get
            Set(ByVal Value As Int32)
                _disbursement_register = Value
            End Set
        End Property

#Region " Component Designer generated code "

        'Component overrides dispose to clean up the component list.
        Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
            If disposing Then
                If components IsNot Nothing Then
                    components.Dispose()
                End If
            End If
            MyBase.Dispose(disposing)
        End Sub

        'Required by the Component Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Component Designer
        'It can be modified using the Component Designer.
        'Do not modify it using the code editor.
        Friend WithEvents XrLabel1 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrTable_Disbursements As DevExpress.XtraReports.UI.XRTable
        Friend WithEvents XrTableRow1 As DevExpress.XtraReports.UI.XRTableRow
        Friend WithEvents XrTableCell1 As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents XrTableCell2 As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents XrTableCell3 As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents XrTableRow2 As DevExpress.XtraReports.UI.XRTableRow
        Friend WithEvents XrTableCell4 As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents XrTableCell_Gross_Checks As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents XrTableCell_Gross_RPPS As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents XrTableRow3 As DevExpress.XtraReports.UI.XRTableRow
        Friend WithEvents XrTableCell7 As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents XrTableCell_Net_Checks As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents XrTableCell_Net_RPPS As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents XrTableRow4 As DevExpress.XtraReports.UI.XRTableRow
        Friend WithEvents XrTableCell10 As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents XrTableCell_Deducted_Checks As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents XrTableCell_Deducted_RPPS As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents XrTableCell13 As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents XrTableCell_Gross_Total As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents XrTableCell_Deducted_Total As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents XrTableCell_Net_Total As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents XrTableRow5 As DevExpress.XtraReports.UI.XRTableRow
        Friend WithEvents XrTableCell17 As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents XrTableCell_Billed_Checks As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents XrTableCell_Billed_RPPS As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents XrTableCell_Billed_Total As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents XrLabel2 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrTable_Trust_Balances As DevExpress.XtraReports.UI.XRTable
        Friend WithEvents XrTableRow6 As DevExpress.XtraReports.UI.XRTableRow
        Friend WithEvents XrTableCell5 As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents XrTableCell_Trust_Inactive As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents XrTableRow7 As DevExpress.XtraReports.UI.XRTableRow
        Friend WithEvents XrTableCell6 As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents XrTableCell_Trust_Total As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents XrTableRow8 As DevExpress.XtraReports.UI.XRTableRow
        Friend WithEvents XrTableCell11 As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents XrTableCell_Trust_Negative As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents XrTableRow9 As DevExpress.XtraReports.UI.XRTableRow
        Friend WithEvents XrTableCell14 As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents XrTableCell_Trust_Positive As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents XrLabel3 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrTable_Adjustments As DevExpress.XtraReports.UI.XRTable
        Friend WithEvents XrTableRow10 As DevExpress.XtraReports.UI.XRTableRow
        Friend WithEvents XrTableCell8 As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents XrTableRow11 As DevExpress.XtraReports.UI.XRTableRow
        Friend WithEvents XrTableCell12 As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents XrTableRow12 As DevExpress.XtraReports.UI.XRTableRow
        Friend WithEvents XrTableCell16 As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents XrTableCell_deduct_balance As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents XrTableCell_prior_balance As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents XrTableCell_post_balance As DevExpress.XtraReports.UI.XRTableCell

        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
            Me.XrLabel1 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrTable_Disbursements = New DevExpress.XtraReports.UI.XRTable()
            Me.XrTableRow1 = New DevExpress.XtraReports.UI.XRTableRow()
            Me.XrTableCell1 = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableCell2 = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableCell3 = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableCell13 = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableRow2 = New DevExpress.XtraReports.UI.XRTableRow()
            Me.XrTableCell4 = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableCell_Gross_Checks = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableCell_Gross_RPPS = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableCell_Gross_Total = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableRow4 = New DevExpress.XtraReports.UI.XRTableRow()
            Me.XrTableCell10 = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableCell_Deducted_Checks = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableCell_Deducted_RPPS = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableCell_Deducted_Total = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableRow3 = New DevExpress.XtraReports.UI.XRTableRow()
            Me.XrTableCell7 = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableCell_Net_Checks = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableCell_Net_RPPS = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableCell_Net_Total = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableRow5 = New DevExpress.XtraReports.UI.XRTableRow()
            Me.XrTableCell17 = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableCell_Billed_Checks = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableCell_Billed_RPPS = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableCell_Billed_Total = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrLabel2 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrTable_Trust_Balances = New DevExpress.XtraReports.UI.XRTable()
            Me.XrTableRow6 = New DevExpress.XtraReports.UI.XRTableRow()
            Me.XrTableCell5 = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableCell_Trust_Inactive = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableRow9 = New DevExpress.XtraReports.UI.XRTableRow()
            Me.XrTableCell14 = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableCell_Trust_Positive = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableRow8 = New DevExpress.XtraReports.UI.XRTableRow()
            Me.XrTableCell11 = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableCell_Trust_Negative = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableRow7 = New DevExpress.XtraReports.UI.XRTableRow()
            Me.XrTableCell6 = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableCell_Trust_Total = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrLabel3 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrTable_Adjustments = New DevExpress.XtraReports.UI.XRTable()
            Me.XrTableRow10 = New DevExpress.XtraReports.UI.XRTableRow()
            Me.XrTableCell8 = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableCell_deduct_balance = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableRow11 = New DevExpress.XtraReports.UI.XRTableRow()
            Me.XrTableCell12 = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableCell_prior_balance = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableRow12 = New DevExpress.XtraReports.UI.XRTableRow()
            Me.XrTableCell16 = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableCell_post_balance = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrSubreport1 = New DevExpress.XtraReports.UI.XRSubreport()
            CType(Me.XrTable_Disbursements, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.XrTable_Trust_Balances, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.XrTable_Adjustments, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
            '
            'XrLabel_Title
            '
            Me.XrLabel_Title.StylePriority.UseFont = False
            '
            'XrPageInfo_PageNumber
            '
            Me.XrPageInfo_PageNumber.StylePriority.UseTextAlignment = False
            '
            'XRLabel_Agency_Name
            '
            Me.XRLabel_Agency_Name.StylePriority.UseTextAlignment = False
            '
            'XrLabel_Agency_Address3
            '
            Me.XrLabel_Agency_Address3.StylePriority.UseFont = False
            Me.XrLabel_Agency_Address3.StylePriority.UseTextAlignment = False
            '
            'XrLabel_Agency_Address1
            '
            Me.XrLabel_Agency_Address1.StylePriority.UseFont = False
            Me.XrLabel_Agency_Address1.StylePriority.UseTextAlignment = False
            '
            'XrLabel_Agency_Phone
            '
            Me.XrLabel_Agency_Phone.StylePriority.UseFont = False
            Me.XrLabel_Agency_Phone.StylePriority.UseTextAlignment = False
            '
            'XrLabel_Agency_Address2
            '
            Me.XrLabel_Agency_Address2.StylePriority.UseFont = False
            Me.XrLabel_Agency_Address2.StylePriority.UseTextAlignment = False
            '
            'Detail
            '
            Me.Detail.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrSubreport1, Me.XrTable_Adjustments, Me.XrLabel3, Me.XrTable_Trust_Balances, Me.XrLabel2, Me.XrTable_Disbursements, Me.XrLabel1})
            Me.Detail.HeightF = 575.0!
            '
            'XrLabel1
            '
            Me.XrLabel1.CanGrow = False
            Me.XrLabel1.Font = New System.Drawing.Font("Times New Roman", 12.0!, System.Drawing.FontStyle.Bold)
            Me.XrLabel1.LocationFloat = New DevExpress.Utils.PointFloat(8.0!, 33.0!)
            Me.XrLabel1.Name = "XrLabel1"
            Me.XrLabel1.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel1.SizeF = New System.Drawing.SizeF(217.0!, 25.0!)
            Me.XrLabel1.Text = "DISBURSEMENTS"
            '
            'XrTable_Disbursements
            '
            Me.XrTable_Disbursements.LocationFloat = New DevExpress.Utils.PointFloat(33.0!, 75.0!)
            Me.XrTable_Disbursements.Name = "XrTable_Disbursements"
            Me.XrTable_Disbursements.Rows.AddRange(New DevExpress.XtraReports.UI.XRTableRow() {Me.XrTableRow1, Me.XrTableRow2, Me.XrTableRow4, Me.XrTableRow3, Me.XrTableRow5})
            Me.XrTable_Disbursements.SizeF = New System.Drawing.SizeF(667.0!, 125.0!)
            '
            'XrTableRow1
            '
            Me.XrTableRow1.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.XrTableCell1, Me.XrTableCell2, Me.XrTableCell3, Me.XrTableCell13})
            Me.XrTableRow1.Name = "XrTableRow1"
            Me.XrTableRow1.Weight = 0.2R
            '
            'XrTableCell1
            '
            Me.XrTableCell1.CanGrow = False
            Me.XrTableCell1.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrTableCell1.Name = "XrTableCell1"
            Me.XrTableCell1.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrTableCell1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            Me.XrTableCell1.Weight = 0.25037481259370314R
            '
            'XrTableCell2
            '
            Me.XrTableCell2.CanGrow = False
            Me.XrTableCell2.Font = New System.Drawing.Font("Times New Roman", 12.0!, System.Drawing.FontStyle.Bold)
            Me.XrTableCell2.Name = "XrTableCell2"
            Me.XrTableCell2.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrTableCell2.Text = "Checks"
            Me.XrTableCell2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrTableCell2.Weight = 0.27436281859070466R
            '
            'XrTableCell3
            '
            Me.XrTableCell3.CanGrow = False
            Me.XrTableCell3.Font = New System.Drawing.Font("Times New Roman", 12.0!, System.Drawing.FontStyle.Bold)
            Me.XrTableCell3.Name = "XrTableCell3"
            Me.XrTableCell3.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrTableCell3.Text = "EFT"
            Me.XrTableCell3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrTableCell3.Weight = 0.21289355322338829R
            '
            'XrTableCell13
            '
            Me.XrTableCell13.CanGrow = False
            Me.XrTableCell13.Font = New System.Drawing.Font("Times New Roman", 12.0!, System.Drawing.FontStyle.Bold)
            Me.XrTableCell13.Name = "XrTableCell13"
            Me.XrTableCell13.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrTableCell13.Text = "Totals"
            Me.XrTableCell13.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrTableCell13.Weight = 0.26236881559220387R
            '
            'XrTableRow2
            '
            Me.XrTableRow2.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.XrTableCell4, Me.XrTableCell_Gross_Checks, Me.XrTableCell_Gross_RPPS, Me.XrTableCell_Gross_Total})
            Me.XrTableRow2.Name = "XrTableRow2"
            Me.XrTableRow2.Weight = 0.2R
            '
            'XrTableCell4
            '
            Me.XrTableCell4.CanGrow = False
            Me.XrTableCell4.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrTableCell4.Name = "XrTableCell4"
            Me.XrTableCell4.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrTableCell4.Text = "Gross"
            Me.XrTableCell4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            Me.XrTableCell4.Weight = 0.25037481259370314R
            '
            'XrTableCell_Gross_Checks
            '
            Me.XrTableCell_Gross_Checks.CanGrow = False
            Me.XrTableCell_Gross_Checks.Name = "XrTableCell_Gross_Checks"
            Me.XrTableCell_Gross_Checks.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrTableCell_Gross_Checks.Text = "$0.00"
            Me.XrTableCell_Gross_Checks.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrTableCell_Gross_Checks.Weight = 0.27436281859070466R
            '
            'XrTableCell_Gross_RPPS
            '
            Me.XrTableCell_Gross_RPPS.CanGrow = False
            Me.XrTableCell_Gross_RPPS.Name = "XrTableCell_Gross_RPPS"
            Me.XrTableCell_Gross_RPPS.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrTableCell_Gross_RPPS.Text = "$0.00"
            Me.XrTableCell_Gross_RPPS.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrTableCell_Gross_RPPS.Weight = 0.21289355322338829R
            '
            'XrTableCell_Gross_Total
            '
            Me.XrTableCell_Gross_Total.CanGrow = False
            Me.XrTableCell_Gross_Total.Name = "XrTableCell_Gross_Total"
            Me.XrTableCell_Gross_Total.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrTableCell_Gross_Total.Text = "$0.00"
            Me.XrTableCell_Gross_Total.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrTableCell_Gross_Total.Weight = 0.26236881559220387R
            '
            'XrTableRow4
            '
            Me.XrTableRow4.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.XrTableCell10, Me.XrTableCell_Deducted_Checks, Me.XrTableCell_Deducted_RPPS, Me.XrTableCell_Deducted_Total})
            Me.XrTableRow4.Name = "XrTableRow4"
            Me.XrTableRow4.Weight = 0.2R
            '
            'XrTableCell10
            '
            Me.XrTableCell10.CanGrow = False
            Me.XrTableCell10.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrTableCell10.Name = "XrTableCell10"
            Me.XrTableCell10.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrTableCell10.Text = "Deducted"
            Me.XrTableCell10.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            Me.XrTableCell10.Weight = 0.25037481259370314R
            '
            'XrTableCell_Deducted_Checks
            '
            Me.XrTableCell_Deducted_Checks.CanGrow = False
            Me.XrTableCell_Deducted_Checks.Name = "XrTableCell_Deducted_Checks"
            Me.XrTableCell_Deducted_Checks.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrTableCell_Deducted_Checks.Text = "$0.00"
            Me.XrTableCell_Deducted_Checks.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrTableCell_Deducted_Checks.Weight = 0.27436281859070466R
            '
            'XrTableCell_Deducted_RPPS
            '
            Me.XrTableCell_Deducted_RPPS.CanGrow = False
            Me.XrTableCell_Deducted_RPPS.Name = "XrTableCell_Deducted_RPPS"
            Me.XrTableCell_Deducted_RPPS.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrTableCell_Deducted_RPPS.Text = "$0.00"
            Me.XrTableCell_Deducted_RPPS.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrTableCell_Deducted_RPPS.Weight = 0.21289355322338829R
            '
            'XrTableCell_Deducted_Total
            '
            Me.XrTableCell_Deducted_Total.CanGrow = False
            Me.XrTableCell_Deducted_Total.Name = "XrTableCell_Deducted_Total"
            Me.XrTableCell_Deducted_Total.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrTableCell_Deducted_Total.Text = "$0.00"
            Me.XrTableCell_Deducted_Total.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrTableCell_Deducted_Total.Weight = 0.26236881559220387R
            '
            'XrTableRow3
            '
            Me.XrTableRow3.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.XrTableCell7, Me.XrTableCell_Net_Checks, Me.XrTableCell_Net_RPPS, Me.XrTableCell_Net_Total})
            Me.XrTableRow3.Name = "XrTableRow3"
            Me.XrTableRow3.Weight = 0.2R
            '
            'XrTableCell7
            '
            Me.XrTableCell7.CanGrow = False
            Me.XrTableCell7.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrTableCell7.Name = "XrTableCell7"
            Me.XrTableCell7.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrTableCell7.Text = "Net"
            Me.XrTableCell7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft
            Me.XrTableCell7.Weight = 0.25037481259370314R
            '
            'XrTableCell_Net_Checks
            '
            Me.XrTableCell_Net_Checks.Borders = DevExpress.XtraPrinting.BorderSide.Top
            Me.XrTableCell_Net_Checks.BorderWidth = 2
            Me.XrTableCell_Net_Checks.CanGrow = False
            Me.XrTableCell_Net_Checks.Name = "XrTableCell_Net_Checks"
            Me.XrTableCell_Net_Checks.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrTableCell_Net_Checks.Text = "$0.00"
            Me.XrTableCell_Net_Checks.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight
            Me.XrTableCell_Net_Checks.Weight = 0.27436281859070466R
            '
            'XrTableCell_Net_RPPS
            '
            Me.XrTableCell_Net_RPPS.Borders = DevExpress.XtraPrinting.BorderSide.Top
            Me.XrTableCell_Net_RPPS.BorderWidth = 2
            Me.XrTableCell_Net_RPPS.CanGrow = False
            Me.XrTableCell_Net_RPPS.Name = "XrTableCell_Net_RPPS"
            Me.XrTableCell_Net_RPPS.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrTableCell_Net_RPPS.Text = "$0.00"
            Me.XrTableCell_Net_RPPS.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight
            Me.XrTableCell_Net_RPPS.Weight = 0.21289355322338829R
            '
            'XrTableCell_Net_Total
            '
            Me.XrTableCell_Net_Total.Borders = DevExpress.XtraPrinting.BorderSide.Top
            Me.XrTableCell_Net_Total.BorderWidth = 2
            Me.XrTableCell_Net_Total.CanGrow = False
            Me.XrTableCell_Net_Total.Name = "XrTableCell_Net_Total"
            Me.XrTableCell_Net_Total.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrTableCell_Net_Total.Text = "$0.00"
            Me.XrTableCell_Net_Total.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight
            Me.XrTableCell_Net_Total.Weight = 0.26236881559220387R
            '
            'XrTableRow5
            '
            Me.XrTableRow5.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.XrTableCell17, Me.XrTableCell_Billed_Checks, Me.XrTableCell_Billed_RPPS, Me.XrTableCell_Billed_Total})
            Me.XrTableRow5.Name = "XrTableRow5"
            Me.XrTableRow5.Weight = 0.2R
            '
            'XrTableCell17
            '
            Me.XrTableCell17.CanGrow = False
            Me.XrTableCell17.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrTableCell17.Name = "XrTableCell17"
            Me.XrTableCell17.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrTableCell17.Text = "Billed"
            Me.XrTableCell17.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft
            Me.XrTableCell17.Weight = 0.25037481259370314R
            '
            'XrTableCell_Billed_Checks
            '
            Me.XrTableCell_Billed_Checks.CanGrow = False
            Me.XrTableCell_Billed_Checks.Name = "XrTableCell_Billed_Checks"
            Me.XrTableCell_Billed_Checks.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrTableCell_Billed_Checks.Text = "$0.00"
            Me.XrTableCell_Billed_Checks.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight
            Me.XrTableCell_Billed_Checks.Weight = 0.27436281859070466R
            '
            'XrTableCell_Billed_RPPS
            '
            Me.XrTableCell_Billed_RPPS.CanGrow = False
            Me.XrTableCell_Billed_RPPS.Name = "XrTableCell_Billed_RPPS"
            Me.XrTableCell_Billed_RPPS.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrTableCell_Billed_RPPS.Text = "$0.00"
            Me.XrTableCell_Billed_RPPS.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight
            Me.XrTableCell_Billed_RPPS.Weight = 0.21289355322338829R
            '
            'XrTableCell_Billed_Total
            '
            Me.XrTableCell_Billed_Total.CanGrow = False
            Me.XrTableCell_Billed_Total.Name = "XrTableCell_Billed_Total"
            Me.XrTableCell_Billed_Total.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrTableCell_Billed_Total.Text = "$0.00"
            Me.XrTableCell_Billed_Total.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight
            Me.XrTableCell_Billed_Total.Weight = 0.26236881559220387R
            '
            'XrLabel2
            '
            Me.XrLabel2.CanGrow = False
            Me.XrLabel2.Font = New System.Drawing.Font("Times New Roman", 12.0!, System.Drawing.FontStyle.Bold)
            Me.XrLabel2.LocationFloat = New DevExpress.Utils.PointFloat(8.0!, 267.0!)
            Me.XrLabel2.Name = "XrLabel2"
            Me.XrLabel2.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel2.SizeF = New System.Drawing.SizeF(217.0!, 25.0!)
            Me.XrLabel2.Text = "TRUST BALANCES"
            '
            'XrTable_Trust_Balances
            '
            Me.XrTable_Trust_Balances.LocationFloat = New DevExpress.Utils.PointFloat(33.0!, 308.0!)
            Me.XrTable_Trust_Balances.Name = "XrTable_Trust_Balances"
            Me.XrTable_Trust_Balances.Rows.AddRange(New DevExpress.XtraReports.UI.XRTableRow() {Me.XrTableRow6, Me.XrTableRow9, Me.XrTableRow8, Me.XrTableRow7})
            Me.XrTable_Trust_Balances.SizeF = New System.Drawing.SizeF(492.0!, 100.0!)
            '
            'XrTableRow6
            '
            Me.XrTableRow6.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.XrTableCell5, Me.XrTableCell_Trust_Inactive})
            Me.XrTableRow6.Name = "XrTableRow6"
            Me.XrTableRow6.Weight = 0.25R
            '
            'XrTableCell5
            '
            Me.XrTableCell5.CanGrow = False
            Me.XrTableCell5.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.XrTableCell5.Name = "XrTableCell5"
            Me.XrTableCell5.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrTableCell5.Text = "Inactive Accounts Trust Balance"
            Me.XrTableCell5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            Me.XrTableCell5.Weight = 0.71138211382113825R
            '
            'XrTableCell_Trust_Inactive
            '
            Me.XrTableCell_Trust_Inactive.CanGrow = False
            Me.XrTableCell_Trust_Inactive.Name = "XrTableCell_Trust_Inactive"
            Me.XrTableCell_Trust_Inactive.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrTableCell_Trust_Inactive.Text = "$0.00"
            Me.XrTableCell_Trust_Inactive.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrTableCell_Trust_Inactive.Weight = 0.2886178861788618R
            '
            'XrTableRow9
            '
            Me.XrTableRow9.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.XrTableCell14, Me.XrTableCell_Trust_Positive})
            Me.XrTableRow9.Name = "XrTableRow9"
            Me.XrTableRow9.Weight = 0.25R
            '
            'XrTableCell14
            '
            Me.XrTableCell14.CanGrow = False
            Me.XrTableCell14.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.XrTableCell14.Name = "XrTableCell14"
            Me.XrTableCell14.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrTableCell14.Text = "Active Accounts with Positive Balance"
            Me.XrTableCell14.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            Me.XrTableCell14.Weight = 0.71138211382113825R
            '
            'XrTableCell_Trust_Positive
            '
            Me.XrTableCell_Trust_Positive.CanGrow = False
            Me.XrTableCell_Trust_Positive.Name = "XrTableCell_Trust_Positive"
            Me.XrTableCell_Trust_Positive.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrTableCell_Trust_Positive.Text = "$0.00"
            Me.XrTableCell_Trust_Positive.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrTableCell_Trust_Positive.Weight = 0.2886178861788618R
            '
            'XrTableRow8
            '
            Me.XrTableRow8.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.XrTableCell11, Me.XrTableCell_Trust_Negative})
            Me.XrTableRow8.Name = "XrTableRow8"
            Me.XrTableRow8.Weight = 0.25R
            '
            'XrTableCell11
            '
            Me.XrTableCell11.CanGrow = False
            Me.XrTableCell11.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.XrTableCell11.Name = "XrTableCell11"
            Me.XrTableCell11.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrTableCell11.Text = "Active Accounts with Negative Balance"
            Me.XrTableCell11.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            Me.XrTableCell11.Weight = 0.71138211382113825R
            '
            'XrTableCell_Trust_Negative
            '
            Me.XrTableCell_Trust_Negative.CanGrow = False
            Me.XrTableCell_Trust_Negative.Name = "XrTableCell_Trust_Negative"
            Me.XrTableCell_Trust_Negative.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrTableCell_Trust_Negative.Text = "$0.00"
            Me.XrTableCell_Trust_Negative.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrTableCell_Trust_Negative.Weight = 0.2886178861788618R
            '
            'XrTableRow7
            '
            Me.XrTableRow7.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.XrTableCell6, Me.XrTableCell_Trust_Total})
            Me.XrTableRow7.Name = "XrTableRow7"
            Me.XrTableRow7.Weight = 0.25R
            '
            'XrTableCell6
            '
            Me.XrTableCell6.CanGrow = False
            Me.XrTableCell6.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.XrTableCell6.Name = "XrTableCell6"
            Me.XrTableCell6.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrTableCell6.Text = "Current Trust Balance"
            Me.XrTableCell6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            Me.XrTableCell6.Weight = 0.71138211382113825R
            '
            'XrTableCell_Trust_Total
            '
            Me.XrTableCell_Trust_Total.Borders = DevExpress.XtraPrinting.BorderSide.Top
            Me.XrTableCell_Trust_Total.BorderWidth = 2
            Me.XrTableCell_Trust_Total.CanGrow = False
            Me.XrTableCell_Trust_Total.Name = "XrTableCell_Trust_Total"
            Me.XrTableCell_Trust_Total.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrTableCell_Trust_Total.Text = "$0.00"
            Me.XrTableCell_Trust_Total.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrTableCell_Trust_Total.Weight = 0.2886178861788618R
            '
            'XrLabel3
            '
            Me.XrLabel3.CanGrow = False
            Me.XrLabel3.Font = New System.Drawing.Font("Times New Roman", 12.0!, System.Drawing.FontStyle.Bold)
            Me.XrLabel3.LocationFloat = New DevExpress.Utils.PointFloat(8.0!, 442.0!)
            Me.XrLabel3.Name = "XrLabel3"
            Me.XrLabel3.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel3.SizeF = New System.Drawing.SizeF(217.0!, 25.0!)
            Me.XrLabel3.Text = "ADJUSTMENTS"
            '
            'XrTable_Adjustments
            '
            Me.XrTable_Adjustments.LocationFloat = New DevExpress.Utils.PointFloat(33.0!, 483.0!)
            Me.XrTable_Adjustments.Name = "XrTable_Adjustments"
            Me.XrTable_Adjustments.Rows.AddRange(New DevExpress.XtraReports.UI.XRTableRow() {Me.XrTableRow10, Me.XrTableRow11, Me.XrTableRow12})
            Me.XrTable_Adjustments.SizeF = New System.Drawing.SizeF(492.0!, 75.0!)
            '
            'XrTableRow10
            '
            Me.XrTableRow10.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.XrTableCell8, Me.XrTableCell_deduct_balance})
            Me.XrTableRow10.Name = "XrTableRow10"
            Me.XrTableRow10.Weight = 0.33333333333333331R
            '
            'XrTableCell8
            '
            Me.XrTableCell8.CanGrow = False
            Me.XrTableCell8.CanShrink = True
            Me.XrTableCell8.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.XrTableCell8.Name = "XrTableCell8"
            Me.XrTableCell8.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrTableCell8.Text = "Corrections to deduction check for prior transactions"
            Me.XrTableCell8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            Me.XrTableCell8.Weight = 0.71138211382113825R
            '
            'XrTableCell_deduct_balance
            '
            Me.XrTableCell_deduct_balance.CanGrow = False
            Me.XrTableCell_deduct_balance.CanShrink = True
            Me.XrTableCell_deduct_balance.Name = "XrTableCell_deduct_balance"
            Me.XrTableCell_deduct_balance.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrTableCell_deduct_balance.Text = "$0.00"
            Me.XrTableCell_deduct_balance.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrTableCell_deduct_balance.Weight = 0.2886178861788618R
            '
            'XrTableRow11
            '
            Me.XrTableRow11.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.XrTableCell12, Me.XrTableCell_prior_balance})
            Me.XrTableRow11.Name = "XrTableRow11"
            Me.XrTableRow11.Weight = 0.33333333333333331R
            '
            'XrTableCell12
            '
            Me.XrTableCell12.CanGrow = False
            Me.XrTableCell12.CanShrink = True
            Me.XrTableCell12.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.XrTableCell12.Name = "XrTableCell12"
            Me.XrTableCell12.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrTableCell12.Text = "Trust account balance before disbursement process"
            Me.XrTableCell12.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            Me.XrTableCell12.Weight = 0.71138211382113825R
            '
            'XrTableCell_prior_balance
            '
            Me.XrTableCell_prior_balance.CanGrow = False
            Me.XrTableCell_prior_balance.CanShrink = True
            Me.XrTableCell_prior_balance.Name = "XrTableCell_prior_balance"
            Me.XrTableCell_prior_balance.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrTableCell_prior_balance.Text = "$0.00"
            Me.XrTableCell_prior_balance.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrTableCell_prior_balance.Weight = 0.2886178861788618R
            '
            'XrTableRow12
            '
            Me.XrTableRow12.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.XrTableCell16, Me.XrTableCell_post_balance})
            Me.XrTableRow12.Name = "XrTableRow12"
            Me.XrTableRow12.Weight = 0.33333333333333331R
            '
            Me.XrTableCell_Gross_Checks.DataBindings.Add("Text", vue, "total_gross_checks", "{0:c}")
            Me.XrTableCell_Billed_Checks.DataBindings.Add("Text", vue, "total_billed_checks", "{0:c}")
            Me.XrTableCell_Deducted_Checks.DataBindings.Add("Text", vue, "total_deduct_checks", "{0:c}")
            Me.XrTableCell_Net_Checks.DataBindings.Add("Text", vue, "total_net_checks", "{0:c}")
            Me.XrTableCell_Gross_RPPS.DataBindings.Add("Text", vue, "total_gross_rps", "{0:c}")
            Me.XrTableCell_Billed_RPPS.DataBindings.Add("Text", vue, "total_billed_rps", "{0:c}")
            Me.XrTableCell_Net_RPPS.DataBindings.Add("Text", vue, "total_net_rps", "{0:c}")
            Me.XrTableCell_Trust_Inactive.DataBindings.Add("Text", vue, "inactive_trust_balance", "{0:c}")
            Me.XrTableCell_Trust_Positive.DataBindings.Add("Text", vue, "posative_trust_balance", "{0:c}")
            Me.XrTableCell_Trust_Negative.DataBindings.Add("Text", vue, "negative_trust_balance", "{0:c}")
            Me.XrTableCell_Trust_Total.DataBindings.Add("Text", vue, "total_trust_balance", "{0:c}")
            Me.XrTableCell_deduct_balance.DataBindings.Add("Text", vue, "deduct_balance", "{0:c}")
            Me.XrTableCell_prior_balance.DataBindings.Add("Text", vue, "prior_balance", "{0:c}")
            Me.XrTableCell_post_balance.DataBindings.Add("Text", vue, "post_balance", "{0:c}")
            '
            'XrTableCell16
            '
            Me.XrTableCell16.CanGrow = False
            Me.XrTableCell16.CanShrink = True
            Me.XrTableCell16.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.XrTableCell16.Name = "XrTableCell16"
            Me.XrTableCell16.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrTableCell16.Text = "Calculated trust balance after post-disbursement process"
            Me.XrTableCell16.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            Me.XrTableCell16.Weight = 0.71138211382113825R
            '
            'XrTableCell_post_balance
            '
            Me.XrTableCell_post_balance.CanGrow = False
            Me.XrTableCell_post_balance.CanShrink = True
            Me.XrTableCell_post_balance.Name = "XrTableCell_post_balance"
            Me.XrTableCell_post_balance.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrTableCell_post_balance.Text = "$0.00"
            Me.XrTableCell_post_balance.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrTableCell_post_balance.Weight = 0.2886178861788618R
            '
            'XrSubreport1
            '
            Me.XrSubreport1.LocationFloat = New DevExpress.Utils.PointFloat(8.0!, 225.0!)
            Me.XrSubreport1.Name = "XrSubreport1"
            Me.XrSubreport1.SizeF = New System.Drawing.SizeF(742.0!, 25.0!)
            '
            'AfterDisbursementReport
            '
            Me.Bands.AddRange(New DevExpress.XtraReports.UI.Band() {Me.TopMarginBand1, Me.Detail, Me.BottomMarginBand1, Me.PageHeader, Me.PageFooter})
            Me.ShowPrintMarginsWarning = False
            Me.ShowPrintStatusDialog = False
            Me.StyleSheet.AddRange(New DevExpress.XtraReports.UI.XRControlStyle() {Me.XrControlStyle_Header, Me.XrControlStyle_Totals, Me.XrControlStyle_HeaderPannel, Me.XrControlStyle_GroupHeader})
            Me.Version = "11.2"
            CType(Me.XrTable_Disbursements, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.XrTable_Trust_Balances, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.XrTable_Adjustments, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me, System.ComponentModel.ISupportInitialize).EndInit()

        End Sub

#End Region


        ''' <summary>
        ''' Title associated with the report
        ''' </summary>
        Public Overrides ReadOnly Property ReportTitle() As String
            Get
                Return "After Disbursement Report"
            End Get
        End Property


        ''' <summary>
        ''' SubTitle associated with the report
        ''' </summary>
        Public Overrides ReadOnly Property ReportSubTitle() As String
            Get
                Return SubTitleString
            End Get
        End Property

        Public Overrides Function NeedParameters() As Boolean
            Return MyBase.NeedParameters() OrElse (Parameter_DisbursementRegister <= 0)
        End Function


        ''' <summary>
        ''' Request the parameters for the report
        ''' </summary>
        Public Overrides Function RequestReportParameters() As DialogResult
            Dim answer As DialogResult = DialogResult.OK
            If NeedParameters() Then
                Using dialogForm As New DisbursementParametersForm()
                    answer = dialogForm.ShowDialog()
                    Parameter_DisbursementRegister = dialogForm.Parameter_BatchID
                End Using
            End If
            Return answer
        End Function


        ''' <summary>
        ''' Bind the dataset to the report
        ''' </summary>
        Dim ds As New DataSet("ds")
        Dim tbl As DataTable
        Dim vue As DataView

        Private Sub AfterDisbursementReport_BeforePrint(ByVal sender As Object, ByVal e As PrintEventArgs)
            Dim cn As SqlConnection = New System.Data.SqlClient.SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)

            ' Read the date from the system
            Using cmd As SqlCommand = New System.Data.SqlClient.SqlCommand()
                cmd.Connection = cn
                cmd.CommandText = "rpt_AfterDisbursement_Report"
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.Add("@DisbursementBatch", SqlDbType.Int).Value = Parameter_DisbursementRegister
                cmd.CommandTimeout = 0

                Using da As New SqlDataAdapter(cmd)
                    da.Fill(ds, "rpt_AfterDisbursement_Report")
                End Using
            End Using
            tbl = ds.Tables(0)
            vue = tbl.DefaultView

            DataSource = vue

            ' Include the subreport if indicated
            BindSubreport()

            ' Save the subtitle information for later retrieval
            SubTitleString = String.Format("Disbursement batch # {0:f0} created on {1:d}", Parameter_DisbursementRegister, Disbursement_Date(Parameter_DisbursementRegister))
        End Sub


        ''' <summary>
        ''' Request the parameters for the report
        ''' </summary>
        Private Function Disbursement_Date(ByVal Disbursement_Register As Int32) As Date
            Dim SelectString As String = "SELECT date_created FROM registers_disbursement WITH (NOLOCK) WHERE disbursement_register=@disbursement_register"
            Dim date_result As Object = DBNull.Value
            Dim cn As SqlConnection = New System.Data.SqlClient.SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)

            Try
                ' Open the database connection
                cn.Open()

                ' Read the date from the system
                Dim cmd As SqlCommand = New System.Data.SqlClient.SqlCommand()
                cmd.Connection = cn
                cmd.CommandText = SelectString
                cmd.Parameters.Add("@disbursement_register", SqlDbType.Int).Value = Disbursement_Register
                date_result = cmd.ExecuteScalar()

            Catch

            Finally
                ' Ensure that the database is closed
                If cn IsNot Nothing Then cn.Dispose()
            End Try

            ' Default a date if there is none given
            If date_result Is DBNull.Value Then
                date_result = New DateTime(1900, 1, 1)
            End If
            Return Convert.ToDateTime(date_result)
        End Function


        ''' <summary>
        ''' Retrieve a value from the resulting record set
        ''' </summary>
        Private Function col(ByVal Name As String) As Decimal
            Dim val As Object = Me.GetCurrentColumnValue(Name)
            If val Is DBNull.Value Then val = 0D
            Return Convert.ToDecimal(val)
        End Function


        ''' <summary>
        ''' Find the EFT deduction amount
        ''' </summary>
        Private Function deduct_rps() As Decimal
            Return col("total_gross_rps") - col("total_net_rps")
        End Function


        ''' <summary>
        ''' Display the EFT deduction amount
        ''' </summary>
        Private Sub XrTableCell_Deducted_RPPS_BeforePrint(ByVal sender As Object, ByVal e As PrintEventArgs)
            With CType(sender, XRTableCell)
                .Text = String.Format("{0:c}", deduct_rps)
            End With
        End Sub


        ''' <summary>
        ''' Display the Total Gross amount
        ''' </summary>
        Private Sub XrTableCell_Gross_Total_BeforePrint(ByVal sender As Object, ByVal e As PrintEventArgs)
            With CType(sender, XRTableCell)
                .Text = String.Format("{0:c}", col("total_gross_checks") + col("total_gross_rps"))
            End With
        End Sub


        ''' <summary>
        ''' Display the Total Deducted amount
        ''' </summary>
        Private Sub XrTableCell_Deducted_Total_BeforePrint(ByVal sender As Object, ByVal e As PrintEventArgs)
            With CType(sender, XRTableCell)
                .Text = String.Format("{0:c}", col("total_deduct_checks") + deduct_rps())
            End With
        End Sub


        ''' <summary>
        ''' Display the Total Net amount
        ''' </summary>
        Private Sub XrTableCell_Net_Total_BeforePrint(ByVal sender As Object, ByVal e As PrintEventArgs)
            With CType(sender, XRTableCell)
                .Text = String.Format("{0:c}", col("total_net_checks") + col("total_net_rps"))
            End With
        End Sub


        ''' <summary>
        ''' Display the Total Billed amount
        ''' </summary>
        Private Sub XrTableCell_Billed_Total_BeforePrint(ByVal sender As Object, ByVal e As PrintEventArgs)
            With CType(sender, XRTableCell)
                .Text = String.Format("{0:c}", col("total_billed_checks") + col("total_billed_rps"))
            End With
        End Sub

        Private Sub BindSubreport()
            Dim total_unallocated_rps As Decimal = Convert.ToDecimal(vue(0)("total_unallocated_rps"))

            If total_unallocated_rps > 0D Then
                Dim ds As New DataSet("ds")

                Using cmd As SqlCommand = New System.Data.SqlClient.SqlCommand()
                    cmd.Connection = New System.Data.SqlClient.SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
                    cmd.CommandText = "rpt_AfterDisbursement_RPS_Errors"
                    cmd.CommandType = CommandType.StoredProcedure
                    cmd.CommandTimeout = 0

                    Using da As New SqlDataAdapter(cmd)
                        da.Fill(ds, "rpt_AfterDisbursement_RPS_Errors")
                    End Using
                End Using
                Dim tbl As DataTable = ds.Tables(0)

                Dim rpt As New AfterDisbursementErrorReport(tbl, total_unallocated_rps)
                XrSubreport1.ReportSource = rpt
            End If
        End Sub
    End Class
End Namespace
