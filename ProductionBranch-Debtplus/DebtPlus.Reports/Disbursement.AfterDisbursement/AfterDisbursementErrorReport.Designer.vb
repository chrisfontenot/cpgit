Namespace Disbursement.AfterDisbursement
    <Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
    Partial Class AfterDisbursementErrorReport
        Inherits DevExpress.XtraReports.UI.XtraReport

        'XtraReport overrides dispose to clean up the component list.
        <System.Diagnostics.DebuggerNonUserCode()> _
        Protected Overrides Sub Dispose(ByVal disposing As Boolean)
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
            MyBase.Dispose(disposing)
        End Sub

        'Required by the Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Designer
        'It can be modified using the Designer.  
        'Do not modify it using the code editor.
        <System.Diagnostics.DebuggerStepThrough()> _
        Private Sub InitializeComponent()
            Dim XrSummary1 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary()
            Dim XrSummary2 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary()
            Me.Detail = New DevExpress.XtraReports.UI.DetailBand()
            Me.XrLabel_account_number = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_client = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_deducted = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_amount = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_creditor = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_rpps_biller_id = New DevExpress.XtraReports.UI.XRLabel()
            Me.PageHeader = New DevExpress.XtraReports.UI.PageHeaderBand()
            Me.XrPanel1 = New DevExpress.XtraReports.UI.XRPanel()
            Me.XrLabel1 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel9 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel7 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel6 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel4 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel3 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_header = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel2 = New DevExpress.XtraReports.UI.XRLabel()
            Me.ReportHeader = New DevExpress.XtraReports.UI.ReportHeaderBand()
            Me.ReportFooter = New DevExpress.XtraReports.UI.ReportFooterBand()
            Me.XrLine1 = New DevExpress.XtraReports.UI.XRLine()
            Me.XrLabel16 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_total_deducted = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_total_amount = New DevExpress.XtraReports.UI.XRLabel()
            Me.TopMarginBand1 = New DevExpress.XtraReports.UI.TopMarginBand()
            Me.BottomMarginBand1 = New DevExpress.XtraReports.UI.BottomMarginBand()
            CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
            '
            'Detail
            '
            Me.Detail.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel_account_number, Me.XrLabel_client, Me.XrLabel_deducted, Me.XrLabel_amount, Me.XrLabel_creditor, Me.XrLabel_rpps_biller_id})
            Me.Detail.HeightF = 15.0!
            Me.Detail.Name = "Detail"
            Me.Detail.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
            Me.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
            '
            'XrLabel_account_number
            '
            Me.XrLabel_account_number.BorderColor = System.Drawing.Color.White
            Me.XrLabel_account_number.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "account_number")})
            Me.XrLabel_account_number.Font = New System.Drawing.Font("Times New Roman", 9.75!)
            Me.XrLabel_account_number.ForeColor = System.Drawing.SystemColors.WindowText
            Me.XrLabel_account_number.LocationFloat = New DevExpress.Utils.PointFloat(192.0!, 0.0!)
            Me.XrLabel_account_number.Name = "XrLabel_account_number"
            Me.XrLabel_account_number.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_account_number.SizeF = New System.Drawing.SizeF(167.0!, 15.0!)
            Me.XrLabel_account_number.StylePriority.UseBorderColor = False
            Me.XrLabel_account_number.StylePriority.UseFont = False
            Me.XrLabel_account_number.StylePriority.UseForeColor = False
            '
            'XrLabel_client
            '
            Me.XrLabel_client.BorderColor = System.Drawing.Color.White
            Me.XrLabel_client.Font = New System.Drawing.Font("Times New Roman", 9.75!)
            Me.XrLabel_client.ForeColor = System.Drawing.SystemColors.WindowText
            Me.XrLabel_client.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
            Me.XrLabel_client.Name = "XrLabel_client"
            Me.XrLabel_client.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_client.SizeF = New System.Drawing.SizeF(68.0!, 15.0!)
            Me.XrLabel_client.StylePriority.UseBorderColor = False
            Me.XrLabel_client.StylePriority.UseFont = False
            Me.XrLabel_client.StylePriority.UseForeColor = False
            Me.XrLabel_client.StylePriority.UseTextAlignment = False
            Me.XrLabel_client.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
            '
            'XrLabel_deducted
            '
            Me.XrLabel_deducted.BorderColor = System.Drawing.Color.White
            Me.XrLabel_deducted.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "deducted", "{0:c}")})
            Me.XrLabel_deducted.Font = New System.Drawing.Font("Times New Roman", 9.75!)
            Me.XrLabel_deducted.ForeColor = System.Drawing.SystemColors.WindowText
            Me.XrLabel_deducted.LocationFloat = New DevExpress.Utils.PointFloat(450.0!, 0.0!)
            Me.XrLabel_deducted.Name = "XrLabel_deducted"
            Me.XrLabel_deducted.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_deducted.SizeF = New System.Drawing.SizeF(83.0!, 15.0!)
            Me.XrLabel_deducted.StylePriority.UseBorderColor = False
            Me.XrLabel_deducted.StylePriority.UseFont = False
            Me.XrLabel_deducted.StylePriority.UseForeColor = False
            Me.XrLabel_deducted.StylePriority.UseTextAlignment = False
            Me.XrLabel_deducted.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
            '
            'XrLabel_amount
            '
            Me.XrLabel_amount.BorderColor = System.Drawing.Color.White
            Me.XrLabel_amount.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "amount", "{0:c}")})
            Me.XrLabel_amount.Font = New System.Drawing.Font("Times New Roman", 9.75!)
            Me.XrLabel_amount.ForeColor = System.Drawing.SystemColors.WindowText
            Me.XrLabel_amount.LocationFloat = New DevExpress.Utils.PointFloat(367.0!, 0.0!)
            Me.XrLabel_amount.Name = "XrLabel_amount"
            Me.XrLabel_amount.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_amount.SizeF = New System.Drawing.SizeF(72.0!, 15.0!)
            Me.XrLabel_amount.StylePriority.UseBorderColor = False
            Me.XrLabel_amount.StylePriority.UseFont = False
            Me.XrLabel_amount.StylePriority.UseForeColor = False
            Me.XrLabel_amount.StylePriority.UseTextAlignment = False
            Me.XrLabel_amount.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
            '
            'XrLabel_creditor
            '
            Me.XrLabel_creditor.BorderColor = System.Drawing.Color.White
            Me.XrLabel_creditor.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "creditor")})
            Me.XrLabel_creditor.Font = New System.Drawing.Font("Times New Roman", 9.75!)
            Me.XrLabel_creditor.ForeColor = System.Drawing.SystemColors.WindowText
            Me.XrLabel_creditor.LocationFloat = New DevExpress.Utils.PointFloat(83.0!, 0.0!)
            Me.XrLabel_creditor.Name = "XrLabel_creditor"
            Me.XrLabel_creditor.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_creditor.SizeF = New System.Drawing.SizeF(100.0!, 15.0!)
            Me.XrLabel_creditor.StylePriority.UseBorderColor = False
            Me.XrLabel_creditor.StylePriority.UseFont = False
            Me.XrLabel_creditor.StylePriority.UseForeColor = False
            '
            'XrLabel_rpps_biller_id
            '
            Me.XrLabel_rpps_biller_id.BorderColor = System.Drawing.Color.White
            Me.XrLabel_rpps_biller_id.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "biller_id")})
            Me.XrLabel_rpps_biller_id.Font = New System.Drawing.Font("Times New Roman", 9.75!)
            Me.XrLabel_rpps_biller_id.ForeColor = System.Drawing.SystemColors.WindowText
            Me.XrLabel_rpps_biller_id.LocationFloat = New DevExpress.Utils.PointFloat(542.0!, 0.0!)
            Me.XrLabel_rpps_biller_id.Name = "XrLabel_rpps_biller_id"
            Me.XrLabel_rpps_biller_id.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_rpps_biller_id.SizeF = New System.Drawing.SizeF(100.0!, 15.0!)
            Me.XrLabel_rpps_biller_id.StylePriority.UseBorderColor = False
            Me.XrLabel_rpps_biller_id.StylePriority.UseFont = False
            Me.XrLabel_rpps_biller_id.StylePriority.UseForeColor = False
            '
            'PageHeader
            '
            Me.PageHeader.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrPanel1})
            Me.PageHeader.HeightF = 22.0!
            Me.PageHeader.Name = "PageHeader"
            '
            'XrPanel1
            '
            Me.XrPanel1.BackColor = System.Drawing.Color.Transparent
            Me.XrPanel1.Borders = DevExpress.XtraPrinting.BorderSide.Bottom
            Me.XrPanel1.BorderWidth = 3
            Me.XrPanel1.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel1, Me.XrLabel9, Me.XrLabel7, Me.XrLabel6, Me.XrLabel4, Me.XrLabel3})
            Me.XrPanel1.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
            Me.XrPanel1.Name = "XrPanel1"
            Me.XrPanel1.SizeF = New System.Drawing.SizeF(650.0!, 17.0!)
            Me.XrPanel1.StylePriority.UseBackColor = False
            Me.XrPanel1.StylePriority.UseBorders = False
            Me.XrPanel1.StylePriority.UseBorderWidth = False
            '
            'XrLabel1
            '
            Me.XrLabel1.BorderColor = System.Drawing.Color.White
            Me.XrLabel1.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel1.ForeColor = System.Drawing.SystemColors.WindowText
            Me.XrLabel1.LocationFloat = New DevExpress.Utils.PointFloat(192.0!, 0.0!)
            Me.XrLabel1.Name = "XrLabel1"
            Me.XrLabel1.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel1.SizeF = New System.Drawing.SizeF(167.0!, 15.0!)
            Me.XrLabel1.StylePriority.UseBorderColor = False
            Me.XrLabel1.StylePriority.UseFont = False
            Me.XrLabel1.StylePriority.UseForeColor = False
            Me.XrLabel1.Text = "ACCT NUMBER"
            '
            'XrLabel9
            '
            Me.XrLabel9.BorderColor = System.Drawing.Color.White
            Me.XrLabel9.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel9.ForeColor = System.Drawing.SystemColors.WindowText
            Me.XrLabel9.LocationFloat = New DevExpress.Utils.PointFloat(542.0!, 0.0!)
            Me.XrLabel9.Name = "XrLabel9"
            Me.XrLabel9.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel9.SizeF = New System.Drawing.SizeF(100.0!, 15.0!)
            Me.XrLabel9.StylePriority.UseBorderColor = False
            Me.XrLabel9.StylePriority.UseFont = False
            Me.XrLabel9.StylePriority.UseForeColor = False
            Me.XrLabel9.Text = "BILLER ID"
            '
            'XrLabel7
            '
            Me.XrLabel7.BorderColor = System.Drawing.Color.White
            Me.XrLabel7.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel7.ForeColor = System.Drawing.SystemColors.WindowText
            Me.XrLabel7.LocationFloat = New DevExpress.Utils.PointFloat(450.0!, 0.0!)
            Me.XrLabel7.Name = "XrLabel7"
            Me.XrLabel7.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel7.SizeF = New System.Drawing.SizeF(83.0!, 15.0!)
            Me.XrLabel7.StylePriority.UseBorderColor = False
            Me.XrLabel7.StylePriority.UseFont = False
            Me.XrLabel7.StylePriority.UseForeColor = False
            Me.XrLabel7.StylePriority.UseTextAlignment = False
            Me.XrLabel7.Text = "DEDUCTED"
            Me.XrLabel7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
            '
            'XrLabel6
            '
            Me.XrLabel6.BorderColor = System.Drawing.Color.White
            Me.XrLabel6.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel6.ForeColor = System.Drawing.SystemColors.WindowText
            Me.XrLabel6.LocationFloat = New DevExpress.Utils.PointFloat(367.0!, 0.0!)
            Me.XrLabel6.Name = "XrLabel6"
            Me.XrLabel6.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel6.SizeF = New System.Drawing.SizeF(72.0!, 15.0!)
            Me.XrLabel6.StylePriority.UseBorderColor = False
            Me.XrLabel6.StylePriority.UseFont = False
            Me.XrLabel6.StylePriority.UseForeColor = False
            Me.XrLabel6.StylePriority.UseTextAlignment = False
            Me.XrLabel6.Text = "AMOUNT"
            Me.XrLabel6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
            '
            'XrLabel4
            '
            Me.XrLabel4.BorderColor = System.Drawing.Color.White
            Me.XrLabel4.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel4.ForeColor = System.Drawing.SystemColors.WindowText
            Me.XrLabel4.LocationFloat = New DevExpress.Utils.PointFloat(83.0!, 1.0!)
            Me.XrLabel4.Name = "XrLabel4"
            Me.XrLabel4.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel4.SizeF = New System.Drawing.SizeF(100.0!, 15.0!)
            Me.XrLabel4.StylePriority.UseBorderColor = False
            Me.XrLabel4.StylePriority.UseFont = False
            Me.XrLabel4.StylePriority.UseForeColor = False
            Me.XrLabel4.Text = "CREDITOR"
            '
            'XrLabel3
            '
            Me.XrLabel3.BorderColor = System.Drawing.Color.White
            Me.XrLabel3.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel3.ForeColor = System.Drawing.SystemColors.WindowText
            Me.XrLabel3.LocationFloat = New DevExpress.Utils.PointFloat(8.0!, 1.0!)
            Me.XrLabel3.Name = "XrLabel3"
            Me.XrLabel3.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel3.SizeF = New System.Drawing.SizeF(60.0!, 15.0!)
            Me.XrLabel3.StylePriority.UseBorderColor = False
            Me.XrLabel3.StylePriority.UseFont = False
            Me.XrLabel3.StylePriority.UseForeColor = False
            Me.XrLabel3.StylePriority.UseTextAlignment = False
            Me.XrLabel3.Text = "CLIENT"
            Me.XrLabel3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
            '
            'XrLabel_header
            '
            Me.XrLabel_header.LocationFloat = New DevExpress.Utils.PointFloat(83.0!, 8.0!)
            Me.XrLabel_header.Name = "XrLabel_header"
            Me.XrLabel_header.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_header.SizeF = New System.Drawing.SizeF(567.0!, 58.0!)
            '
            'XrLabel2
            '
            Me.XrLabel2.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel2.ForeColor = System.Drawing.Color.Red
            Me.XrLabel2.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 8.0!)
            Me.XrLabel2.Name = "XrLabel2"
            Me.XrLabel2.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel2.SizeF = New System.Drawing.SizeF(83.0!, 17.0!)
            Me.XrLabel2.StylePriority.UseFont = False
            Me.XrLabel2.StylePriority.UseForeColor = False
            Me.XrLabel2.Text = "WARNING:"
            '
            'ReportHeader
            '
            Me.ReportHeader.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel2, Me.XrLabel_header})
            Me.ReportHeader.HeightF = 75.0!
            Me.ReportHeader.Name = "ReportHeader"
            '
            'ReportFooter
            '
            Me.ReportFooter.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLine1, Me.XrLabel16, Me.XrLabel_total_deducted, Me.XrLabel_total_amount})
            Me.ReportFooter.HeightF = 36.0!
            Me.ReportFooter.Name = "ReportFooter"
            '
            'XrLine1
            '
            Me.XrLine1.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
            Me.XrLine1.Name = "XrLine1"
            Me.XrLine1.SizeF = New System.Drawing.SizeF(650.0!, 17.0!)
            '
            'XrLabel16
            '
            Me.XrLabel16.BorderColor = System.Drawing.Color.White
            Me.XrLabel16.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel16.ForeColor = System.Drawing.SystemColors.WindowText
            Me.XrLabel16.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 17.0!)
            Me.XrLabel16.Name = "XrLabel16"
            Me.XrLabel16.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel16.SizeF = New System.Drawing.SizeF(217.0!, 15.0!)
            Me.XrLabel16.StylePriority.UseBorderColor = False
            Me.XrLabel16.StylePriority.UseFont = False
            Me.XrLabel16.StylePriority.UseForeColor = False
            Me.XrLabel16.Text = "TOTALS"
            '
            'XrLabel_total_deducted
            '
            Me.XrLabel_total_deducted.BorderColor = System.Drawing.Color.White
            Me.XrLabel_total_deducted.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "deducted")})
            Me.XrLabel_total_deducted.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel_total_deducted.ForeColor = System.Drawing.SystemColors.WindowText
            Me.XrLabel_total_deducted.LocationFloat = New DevExpress.Utils.PointFloat(550.0!, 17.0!)
            Me.XrLabel_total_deducted.Name = "XrLabel_total_deducted"
            Me.XrLabel_total_deducted.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_total_deducted.SizeF = New System.Drawing.SizeF(92.0!, 15.0!)
            Me.XrLabel_total_deducted.StylePriority.UseBorderColor = False
            Me.XrLabel_total_deducted.StylePriority.UseFont = False
            Me.XrLabel_total_deducted.StylePriority.UseForeColor = False
            Me.XrLabel_total_deducted.StylePriority.UseTextAlignment = False
            XrSummary1.FormatString = "{0:c}"
            XrSummary1.Running = DevExpress.XtraReports.UI.SummaryRunning.Report
            Me.XrLabel_total_deducted.Summary = XrSummary1
            Me.XrLabel_total_deducted.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
            '
            'XrLabel_total_amount
            '
            Me.XrLabel_total_amount.BorderColor = System.Drawing.Color.White
            Me.XrLabel_total_amount.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "amount")})
            Me.XrLabel_total_amount.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel_total_amount.ForeColor = System.Drawing.SystemColors.WindowText
            Me.XrLabel_total_amount.LocationFloat = New DevExpress.Utils.PointFloat(436.0!, 17.0!)
            Me.XrLabel_total_amount.Name = "XrLabel_total_amount"
            Me.XrLabel_total_amount.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_total_amount.SizeF = New System.Drawing.SizeF(97.0!, 15.0!)
            Me.XrLabel_total_amount.StylePriority.UseBorderColor = False
            Me.XrLabel_total_amount.StylePriority.UseFont = False
            Me.XrLabel_total_amount.StylePriority.UseForeColor = False
            Me.XrLabel_total_amount.StylePriority.UseTextAlignment = False
            XrSummary2.FormatString = "{0:c}"
            XrSummary2.Running = DevExpress.XtraReports.UI.SummaryRunning.Report
            Me.XrLabel_total_amount.Summary = XrSummary2
            Me.XrLabel_total_amount.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
            '
            'TopMarginBand1
            '
            Me.TopMarginBand1.HeightF = 25.0!
            Me.TopMarginBand1.Name = "TopMarginBand1"
            '
            'BottomMarginBand1
            '
            Me.BottomMarginBand1.HeightF = 25.0!
            Me.BottomMarginBand1.Name = "BottomMarginBand1"
            '
            'ErrorReport
            '
            Me.Bands.AddRange(New DevExpress.XtraReports.UI.Band() {Me.Detail, Me.PageHeader, Me.ReportHeader, Me.ReportFooter, Me.TopMarginBand1, Me.BottomMarginBand1})
            Me.Margins = New System.Drawing.Printing.Margins(25, 25, 25, 25)
            Me.RequestParameters = False
            Me.ShowPrintMarginsWarning = False
            Me.ShowPrintStatusDialog = False
            Me.Version = "11.2"
            CType(Me, System.ComponentModel.ISupportInitialize).EndInit()

        End Sub
        Friend WithEvents Detail As DevExpress.XtraReports.UI.DetailBand
        Friend WithEvents PageHeader As DevExpress.XtraReports.UI.PageHeaderBand
        Friend WithEvents XrLabel2 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_header As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrPanel1 As DevExpress.XtraReports.UI.XRPanel
        Friend WithEvents XrLabel7 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel6 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel4 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel3 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents ReportHeader As DevExpress.XtraReports.UI.ReportHeaderBand
        Friend WithEvents XrLabel9 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_client As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_deducted As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_amount As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_creditor As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_rpps_biller_id As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents ReportFooter As DevExpress.XtraReports.UI.ReportFooterBand
        Friend WithEvents XrLine1 As DevExpress.XtraReports.UI.XRLine
        Friend WithEvents XrLabel16 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_total_deducted As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_total_amount As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_account_number As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel1 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents TopMarginBand1 As DevExpress.XtraReports.UI.TopMarginBand
        Friend WithEvents BottomMarginBand1 As DevExpress.XtraReports.UI.BottomMarginBand
    End Class
End Namespace