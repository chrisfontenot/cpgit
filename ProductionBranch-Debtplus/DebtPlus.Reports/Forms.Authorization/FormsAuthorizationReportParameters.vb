#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}

#End Region

Imports DebtPlus.Reports.Template.Forms

Imports System.Data.SqlClient

Namespace Forms.Authorization
    Friend Class FormsAuthorizationReportParameters
        Inherits ClientParametersForm


        Public ReadOnly Property Parameter_State() As Object
            Get
                Return LookUpEdit1.EditValue
            End Get
        End Property

        Public ReadOnly Property Parameter_Language() As Object
            Get
                Return LookUpEdit_Language.EditValue
            End Get
        End Property

        Public Sub New()
            MyBase.New()
            InitializeComponent()
            AddHandler Me.Load, AddressOf ReportParameters_Load
        End Sub

        Private Sub ReportParameters_Load(ByVal sender As Object, ByVal e As EventArgs)
            Dim ds As New DataSet("ds")

            Using cmd As SqlCommand = New System.Data.SqlClient.SqlCommand()
                With cmd
                    .Connection = New System.Data.SqlClient.SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
                    .CommandText = "SELECT st.state, left(st.MailingCode, 2) as MailingCode, st.name as description, c.description as Country, st.[Default], st.ActiveFlag, st.[DoingBusiness] FROM states st WITH (NOLOCK) LEFT OUTER JOIN countries c WITH (NOLOCK) ON st.country = c.country"
                    .CommandType = CommandType.Text
                End With

                Using da As New SqlDataAdapter(cmd)
                    da.Fill(ds, "states")
                End Using
            End Using

            LookUpEdit1.Properties.DataSource = New DataView(ds.Tables("states"), "[DoingBusiness]<>false", "description", DataViewRowState.CurrentRows)
            LookUpEdit1.Properties.PopupWidth = LookUpEdit1.Width + 140
            AddHandler LookUpEdit1.EditValueChanging, AddressOf DebtPlus.Data.Validation.LookUpEdit_ActiveTest

            Using cmd As SqlCommand = New System.Data.SqlClient.SqlCommand()
                With cmd
                    .Connection = New System.Data.SqlClient.SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
                    .CommandText = "SELECT oID, Attribute FROM AttributeTypes WHERE [ActiveFlag] = 1 AND [grouping] = 'LANGUAGE'"
                    .CommandType = CommandType.Text
                End With

                Using da As New SqlDataAdapter(cmd)
                    da.Fill(ds, "languages")
                End Using
            End Using

            LookUpEdit_Language.Properties.DataSource = New DataView(ds.Tables("languages"), String.Empty, "Attribute", DataViewRowState.CurrentRows)
            LookUpEdit_Language.Properties.PopupWidth = LookUpEdit_Language.Width + 140
            AddHandler LookUpEdit_Language.EditValueChanging, AddressOf DebtPlus.Data.Validation.LookUpEdit_ActiveTest
        End Sub
    End Class
End Namespace