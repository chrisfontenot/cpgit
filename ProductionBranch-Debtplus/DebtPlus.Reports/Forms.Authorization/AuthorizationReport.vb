#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}
#End Region

Imports DebtPlus.Interfaces.Client
Imports DebtPlus.Reports.Template
Imports DebtPlus.Utils
Imports System.IO
Imports System.Reflection
Imports DevExpress.XtraReports.UI

Namespace Forms.Authorization
    Public Class AuthorizationReport
        Inherits DebtPlus.Reports.Template.BaseXtraReportClass
        Implements DebtPlus.Interfaces.Client.IClient

        ''' <summary>
        '''     Create an instance of our report
        ''' </summary>
        Public Sub New()
            MyBase.New()
            InitializeComponent()

            ' Reset the parameter values since they can be scripted and we don't want old values.
            Parameter_Client = -1
            Parameter_Language = System.DBNull.Value
            Parameter_State = System.DBNull.Value

            ' Set the script references as needed
            ScriptReferences = DebtPlus.Utils.AssemblyTools.GetAllSiblingAssemblies()

            ' Update the sub-reports as well
            For Each BandPtr As DevExpress.XtraReports.UI.Band In Bands
                For Each CtlPtr As DevExpress.XtraReports.UI.XRControl In BandPtr.Controls
                    Dim Rpt As DevExpress.XtraReports.UI.XRSubreport = TryCast(CtlPtr, DevExpress.XtraReports.UI.XRSubreport)
                    If Rpt IsNot Nothing Then
                        Dim RptSrc As DevExpress.XtraReports.UI.XtraReport = TryCast(Rpt.ReportSource, DevExpress.XtraReports.UI.XtraReport)
                        If RptSrc IsNot Nothing Then
                            RptSrc.ScriptReferences = ScriptReferences
                        End If
                    End If
                Next
            Next
        End Sub

        Private Sub InitializeComponent()

            ' Name of the report file
            Const ReportName As String = "DebtPlus.Reports.Forms.Authorization.repx"

            ' See if there is a report reference that we can use
            Dim Fname As String = Path.Combine(DebtPlus.Configuration.Config.ReportsDirectory, ReportName)
            Dim UseDefault As Boolean = True
            Try
                If (Not DebtPlus.Configuration.Config.LocalReportsOnly) AndAlso System.IO.File.Exists(Fname) Then
                    LoadLayout(Fname)
                    UseDefault = False
                End If

            Catch ex As Exception
                DebtPlus.Svc.Common.ErrorHandling.HandleErrors(ex)
            End Try

            ' Load the standard report definition if we need a new item
            If UseDefault Then
                Dim asm As Assembly = Assembly.GetExecutingAssembly
                Using ios As Stream = asm.GetManifestResourceStream(String.Format("DebtPlus.Reports.{0}", ReportName))
                    If ios IsNot Nothing Then LoadLayout(ios)
                End Using
            End If
        End Sub

        Public Property Parameter_State() As Object
            Get
                Dim v As String = Convert.ToString(Parameters("ParameterState").Value)
                If v = String.Empty Then Return DBNull.Value
                Return v
            End Get
            Set(ByVal value As Object)
                If value Is DBNull.Value OrElse value Is Nothing Then value = String.Empty
                SetParameter("ParameterState", GetType(String), value, "State ID", False)
            End Set
        End Property

        Public Property Parameter_Language() As Object
            Get
                Dim v As Int32 = Convert.ToInt32(Parameters("ParameterLanguage").Value)
                If v <= 0 Then Return DBNull.Value
                Return v
            End Get
            Set(ByVal value As Object)
                If value Is DBNull.Value OrElse value Is Nothing Then value = -1
                SetParameter("ParameterLanguage", GetType(Int32), value, "Language ID", False)
            End Set
        End Property

        Public Property ClientId() As Int32 Implements DebtPlus.Interfaces.Client.IClient.ClientId
            Get
                Return Parameter_Client
            End Get
            Set(value As Int32)
                Parameter_Client = value
            End Set
        End Property

        Public Property Parameter_Client() As Int32
            Get
                Dim parm As DevExpress.XtraReports.Parameters.Parameter = FindParameter("ParameterClient")
                If parm Is Nothing Then Return -1
                Return Convert.ToInt32(parm.Value)
            End Get
            Set(ByVal value As Int32)
                SetParameter("ParameterClient", GetType(Int32), value, "Client ID", False)
            End Set
        End Property

        Public Overrides Function NeedParameters() As Boolean
            ' We always need to ask for the parameters.
            Return True
        End Function

        Public Overrides Function RequestReportParameters() As DialogResult
            Dim Answer As DialogResult = DialogResult.OK
            If NeedParameters() Then
                Using frm As New FormsAuthorizationReportParameters()
                    With frm
                        .Parameter_Client = Parameter_Client
                        Answer = .ShowDialog()
                        Parameter_Client = .Parameter_Client
                        Parameter_State = .Parameter_State
                        Parameter_Language = .Parameter_Language
                    End With
                End Using
            End If
            Return Answer
        End Function
    End Class
End Namespace