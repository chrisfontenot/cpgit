Namespace Forms.Authorization
    <Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
    Partial Class FormsAuthorizationReportParameters

        'Form overrides dispose to clean up the component list.
        <System.Diagnostics.DebuggerNonUserCode()> _
        Protected Overrides Sub Dispose(ByVal disposing As Boolean)
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
            MyBase.Dispose(disposing)
        End Sub

        'Required by the Windows Form Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Windows Form Designer
        'It can be modified using the Windows Form Designer.  
        'Do not modify it using the code editor.
        <System.Diagnostics.DebuggerStepThrough()> _
        Private Sub InitializeComponent()
            Me.LabelControl2 = New DevExpress.XtraEditors.LabelControl
            Me.LookUpEdit1 = New DevExpress.XtraEditors.LookUpEdit
            Me.LookUpEdit_Language = New DevExpress.XtraEditors.LookUpEdit
            Me.LabelControl3 = New DevExpress.XtraEditors.LabelControl
            CType(Me.XrClient_param_01_1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.DxErrorProvider1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LookUpEdit1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LookUpEdit_Language.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.SuspendLayout()
            '
            'LabelControl1
            '
            Me.LabelControl1.Location = New System.Drawing.Point(13, 28)
            '
            'XrClient_param_01_1
            '
            Me.XrClient_param_01_1.Location = New System.Drawing.Point(74, 25)
            Me.XrClient_param_01_1.Properties.Appearance.Options.UseTextOptions = True
            Me.XrClient_param_01_1.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.XrClient_param_01_1.Properties.DisplayFormat.FormatString = "0000000"
            Me.XrClient_param_01_1.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom
            Me.XrClient_param_01_1.Properties.EditFormat.FormatString = "f0"
            Me.XrClient_param_01_1.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.XrClient_param_01_1.Properties.Mask.BeepOnError = True
            Me.XrClient_param_01_1.Properties.Mask.EditMask = "\d*"
            Me.XrClient_param_01_1.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx
            '
            'ButtonOK
            '
            Me.ButtonOK.TabIndex = 6
            '
            'ButtonCancel
            '
            Me.ButtonCancel.TabIndex = 7
            '
            'LabelControl2
            '
            Me.LabelControl2.Location = New System.Drawing.Point(13, 54)
            Me.LabelControl2.Name = "LabelControl2"
            Me.LabelControl2.Size = New System.Drawing.Size(30, 13)
            Me.LabelControl2.TabIndex = 2
            Me.LabelControl2.Text = "State:"
            '
            'LookUpEdit1
            '
            Me.LookUpEdit1.Location = New System.Drawing.Point(74, 51)
            Me.LookUpEdit1.Name = "LookUpEdit1"
            Me.LookUpEdit1.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
            Me.LookUpEdit1.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.LookUpEdit1.Properties.Columns.AddRange(New DevExpress.XtraEditors.Controls.LookUpColumnInfo() {New DevExpress.XtraEditors.Controls.LookUpColumnInfo("state", "ID", 20, DevExpress.Utils.FormatType.Numeric, "f0", False, DevExpress.Utils.HorzAlignment.[Default]), New DevExpress.XtraEditors.Controls.LookUpColumnInfo("MailingCode", "Abbrev", 20, DevExpress.Utils.FormatType.None, "", True, DevExpress.Utils.HorzAlignment.[Default], DevExpress.Data.ColumnSortOrder.Ascending), New DevExpress.XtraEditors.Controls.LookUpColumnInfo("description", 40, "Name"), New DevExpress.XtraEditors.Controls.LookUpColumnInfo("Country", 40, "Country")})
            Me.LookUpEdit1.Properties.DisplayMember = "description"
            Me.LookUpEdit1.Properties.NullText = "Use client's home state"
            Me.LookUpEdit1.Properties.ShowFooter = False
            Me.LookUpEdit1.Properties.SortColumnIndex = 1
            Me.LookUpEdit1.Properties.ValueMember = "MailingCode"
            Me.LookUpEdit1.Size = New System.Drawing.Size(151, 20)
            Me.LookUpEdit1.TabIndex = 3
            '
            'LookUpEdit_Language
            '
            Me.LookUpEdit_Language.Location = New System.Drawing.Point(74, 77)
            Me.LookUpEdit_Language.Name = "LookUpEdit_Language"
            Me.LookUpEdit_Language.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
            Me.LookUpEdit_Language.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.LookUpEdit_Language.Properties.Columns.AddRange(New DevExpress.XtraEditors.Controls.LookUpColumnInfo() {New DevExpress.XtraEditors.Controls.LookUpColumnInfo("oID", "ID", 20, DevExpress.Utils.FormatType.Numeric, "f0", False, DevExpress.Utils.HorzAlignment.[Default]), New DevExpress.XtraEditors.Controls.LookUpColumnInfo("Attribute", "Language", 20, DevExpress.Utils.FormatType.None, "", True, DevExpress.Utils.HorzAlignment.[Default], DevExpress.Data.ColumnSortOrder.Ascending)})
            Me.LookUpEdit_Language.Properties.DisplayMember = "Attribute"
            Me.LookUpEdit_Language.Properties.NullText = "Use client's language"
            Me.LookUpEdit_Language.Properties.ShowFooter = False
            Me.LookUpEdit_Language.Properties.SortColumnIndex = 1
            Me.LookUpEdit_Language.Properties.ValueMember = "oID"
            Me.LookUpEdit_Language.Size = New System.Drawing.Size(151, 20)
            Me.LookUpEdit_Language.TabIndex = 5
            '
            'LabelControl3
            '
            Me.LabelControl3.Location = New System.Drawing.Point(13, 80)
            Me.LabelControl3.Name = "LabelControl3"
            Me.LabelControl3.Size = New System.Drawing.Size(51, 13)
            Me.LabelControl3.TabIndex = 4
            Me.LabelControl3.Text = "Language:"
            '
            'ReportParameters
            '
            Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
            Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
            Me.ClientSize = New System.Drawing.Size(336, 135)
            Me.Controls.Add(Me.LookUpEdit_Language)
            Me.Controls.Add(Me.LabelControl3)
            Me.Controls.Add(Me.LookUpEdit1)
            Me.Controls.Add(Me.LabelControl2)
            Me.Name = "ReportParameters"
            Me.Controls.SetChildIndex(Me.LabelControl2, 0)
            Me.Controls.SetChildIndex(Me.XrClient_param_01_1, 0)
            Me.Controls.SetChildIndex(Me.LabelControl1, 0)
            Me.Controls.SetChildIndex(Me.LookUpEdit1, 0)
            Me.Controls.SetChildIndex(Me.ButtonOK, 0)
            Me.Controls.SetChildIndex(Me.ButtonCancel, 0)
            Me.Controls.SetChildIndex(Me.LabelControl3, 0)
            Me.Controls.SetChildIndex(Me.LookUpEdit_Language, 0)
            CType(Me.XrClient_param_01_1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.DxErrorProvider1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LookUpEdit1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LookUpEdit_Language.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            Me.ResumeLayout(False)
            Me.PerformLayout()

        End Sub
        Friend WithEvents LabelControl2 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents LookUpEdit1 As DevExpress.XtraEditors.LookUpEdit
        Friend WithEvents LookUpEdit_Language As DevExpress.XtraEditors.LookUpEdit
        Friend WithEvents LabelControl3 As DevExpress.XtraEditors.LabelControl
    End Class
End Namespace
