#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}
#End Region

Namespace Forms.Authorization
    Public Class AuthorizationReport
        Implements DebtPlus.Interfaces.Client.IClient

        ''' <summary>
        ''' Create an instance of our report
        ''' </summary>
        Public Sub New()
            MyBase.New()
            InitializeComponent()
            ' The report was moved to the script. do not do initialization twice.
            ' AddHandler Me.BeforePrint, AddressOf AuthorizationReport_BeforePrint
        End Sub

        Public Property Parameter_State() As Object
            Get
                Dim v As String = Convert.ToString(Parameters("ParameterState").Value)
                If v = String.Empty Then Return DBNull.Value
                Return v
            End Get
            Set(ByVal value As Object)
                If value Is DBNull.Value OrElse value Is Nothing Then value = String.Empty
                SetParameter("ParameterState", GetType(String), value, "State ID", False)
            End Set
        End Property

        Public Property Parameter_Language() As Object
            Get
                Dim v As Int32 = Convert.ToInt32(Parameters("ParameterLanguage").Value)
                If v <= 0 Then Return DBNull.Value
                Return v
            End Get
            Set(ByVal value As Object)
                If value Is DBNull.Value OrElse value Is Nothing Then value = -1
                SetParameter("ParameterLanguage", GetType(Int32), value, "Language ID", False)
            End Set
        End Property

        Public Property ClientId() As Int32 Implements DebtPlus.Interfaces.Client.IClient.ClientId
            Get
                Return Parameter_Client
            End Get
            Set(value As Int32)
                Parameter_Client = value
            End Set
        End Property

        Public Property Parameter_Client() As Int32
            Get
                Dim parm As DevExpress.XtraReports.Parameters.Parameter = FindParameter("ParameterClient")
                If parm Is Nothing Then Return -1
                Return Convert.ToInt32(parm.Value)
            End Get
            Set(ByVal value As Int32)
                SetParameter("ParameterClient", GetType(Int32), value, "Client ID", False)
            End Set
        End Property

        Public Overrides Function NeedParameters() As Boolean
            Return True
        End Function

        Public Overrides Function RequestReportParameters() As DialogResult
            Dim Answer As DialogResult = DialogResult.OK
            If NeedParameters() Then
                Using frm As New FormsAuthorizationReportParameters()
                    With frm
                        .Parameter_Client = Parameter_Client
                        Answer = .ShowDialog()
                        Parameter_Client = .Parameter_Client
                        Parameter_State = .Parameter_State
                        Parameter_Language = .Parameter_Language
                    End With
                End Using
            End If
            Return Answer
        End Function

        Private ds As New System.Data.DataSet("ds")
        Private LanguageName As String = "en-US"
        Private Record As DebtPlus.Reports.Forms.Authorization.RecordDefinition

        Private Sub AuthorizationReport_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs)
            Dim rpt As DevExpress.XtraReports.UI.XtraReport = CType(sender, DevExpress.XtraReports.UI.XtraReport)

            '-- Use the reports directory to find the AuthoritzationForms
            Dim BaseDir As String = System.IO.Path.Combine(DebtPlus.Configuration.Config.ReportsDirectory, "AuthorizationForms")

            '-- Read the list of revision information entries. Do nothing if the file can not be found.
            Dim colRevisions As System.Collections.Generic.List(Of RevisionInfo) = getRevisionInfo(BaseDir)
            If colRevisions Is Nothing Then
                Return
            End If

            '-- Set the page footers
            XrLabel_page_footer.Text = String.Empty
            XrLabel_revision.Text = String.Empty

            Try
                Using cn As System.Data.SqlClient.SqlConnection = New System.Data.SqlClient.SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
                    cn.Open()

                    '-- Find the client state
                    Using cmd As New System.Data.SqlClient.SqlCommand()
                        cmd.Connection = cn
                        cmd.CommandText = "SELECT c.client, c.salutation, c.AddressID, c.HomeTelephoneID, c.MsgTelephoneID, c.county, c.region, c.active_status, c.active_status_date, c.dmp_status_date, c.client_status, c.client_status_date, c.disbursement_date, c.mail_error_date, c.start_date, c.restart_date, c.drop_date, c.drop_reason, c.drop_reason_other, c.referred_by, c.cause_fin_problem1, c.cause_fin_problem2, c.cause_fin_problem3, c.cause_fin_problem4, c.office, c.counselor, c.csr, c.hold_disbursements, c.personal_checks, c.ach_active, c.stack_proration, c.mortgage_problems, c.intake_agreement, c.ElectronicCorrespondence, c.ElectronicStatements, c.bankruptcy_class, c.satisfaction_score, c.fed_tax_owed, c.state_tax_owed, c.local_tax_owed, c.fed_tax_months, c.state_tax_months, c.local_tax_months, c.held_in_trust, c.deposit_in_trust, c.reserved_in_trust, c.reserved_in_trust_cutoff, c.marital_status, c.language, c.dependents, c.household, c.method_first_contact, c.preferred_contact, c.config_fee, c.first_appt, c.first_kept_appt, c.first_resched_appt, c.program_months, c.first_deposit_date, c.last_deposit_date, c.last_deposit_amount," + _
                            " c.payout_total_debt, c.payout_months_to_payout, c.payout_total_fees, c.payout_total_interest, c.payout_total_payments, c.payout_deposit_amount, c.payout_monthly_fee, c.payout_cushion_amount, c.payout_termination_date," + _
                            " c.created_by, c.date_created, c.client_guid, c.initial_program, c.current_program, LEFT(st.mailingcode, 2) AS state_name " + _
                            "FROM clients c WITH (NOLOCK) " + _
                            "LEFT OUTER JOIN addresses a on c.addressid = a.address " + _
                            "LEFT OUTER JOIN states st WITH (NOLOCK) ON a.state = st.state " + _
                            "WHERE c.client = @client"
                        cmd.CommandType = System.Data.CommandType.Text
                        cmd.Parameters.Add("@client", System.Data.SqlDbType.Int).Value = rpt.Parameters("ParameterClient").Value
                        cmd.CommandTimeout = 0

                        Using da As New System.Data.SqlClient.SqlDataAdapter(cmd)
                            da.Fill(ds, "clients")
                        End Using
                    End Using

                    '-- If there is no state then use the client's home address as the state code
                    If ds.Tables("clients").Rows.Count > 0 Then
                        If rpt.Parameters("ParameterState").Value Is System.DBNull.Value OrElse String.IsNullOrEmpty(Convert.ToString(rpt.Parameters("ParameterState").Value)) Then
                            Dim NewState As String
                            Dim obj As Object = ds.Tables("clients").Rows(0)("state_name")
                            If obj IsNot Nothing AndAlso obj IsNot System.DBNull.Value Then
                                NewState = System.Convert.ToString(obj, System.Globalization.CultureInfo.InvariantCulture).Trim
                                If NewState <> String.Empty Then
                                    rpt.Parameters("ParameterState").Value = NewState
                                End If
                            End If
                        End If

                        '-- Use the client's language if one is given
                        If rpt.Parameters("ParameterLanguage").Value Is Nothing OrElse rpt.Parameters("ParameterLanguage").Value Is System.DBNull.Value OrElse Convert.ToInt32(rpt.Parameters("ParameterLanguage").Value) <= 0 Then
                            Dim NewLanguage As Int32
                            Dim obj As Object = ds.Tables("clients").Rows(0)("language")
                            If obj IsNot Nothing AndAlso obj IsNot System.DBNull.Value Then
                                NewLanguage = System.Convert.ToInt32(obj, System.Globalization.CultureInfo.InvariantCulture)
                                If NewLanguage > 0 Then
                                    rpt.Parameters("ParameterLanguage").Value = NewLanguage
                                End If
                            End If
                        End If
                    End If

                    '-- Do not allow the state to be defined from this point forward or we will trap out on the select statement.
                    If rpt.Parameters("ParameterState").Value Is System.DBNull.Value Then rpt.Parameters("ParameterState").Value = String.Empty

                    '-- Convert the state name key to a legal string
                    Dim StateName As String = System.Convert.ToString(rpt.Parameters("ParameterState").Value).ToUpper().Trim

                    '-- Find the language ID from the value
                    LanguageName = "en-US"
                    Dim key As System.Nullable(Of Int32) = DebtPlus.Utils.Nulls.v_Int32(rpt.Parameters("ParameterLanguage").Value)
                    If key.HasValue Then
                        For Each languageItem As DebtPlus.LINQ.AttributeType In DebtPlus.LINQ.Cache.AttributeType.getLanguageList()
                            If languageItem.Id = key.Value Then
                                LanguageName = languageItem.LanguageID
                                Exit For
                            End If
                        Next
                    End If

                    '-- Find the client deposit information
                    Using cmd As New System.Data.SqlClient.SqlCommand()
                        cmd.Connection = cn
                        cmd.CommandText = "SELECT deposit_date, DATEPART(day,deposit_date) as due_day_text,sum(deposit_amount) as deposit_amount FROM client_deposits WHERE client = @client GROUP BY deposit_date ORDER BY 1"
                        cmd.CommandType = System.Data.CommandType.Text
                        cmd.Parameters.Add("@client", System.Data.SqlDbType.Int).Value = rpt.Parameters("ParameterClient").Value
                        cmd.CommandTimeout = 0

                        Using da As New System.Data.SqlClient.SqlDataAdapter(cmd)
                            da.Fill(ds, "client_deposits")
                        End Using
                    End Using

                    '-- Find the debt information
                    Using cmd As New System.Data.SqlClient.SqlCommand()
                        cmd.Connection = cn
                        cmd.CommandText = "rpt_PrintClient_CreditorDebt"
                        cmd.CommandType = System.Data.CommandType.StoredProcedure
                        cmd.Parameters.Add("@client", System.Data.SqlDbType.Int).Value = rpt.Parameters("ParameterClient").Value
                        cmd.CommandTimeout = 0

                        Using da As New System.Data.SqlClient.SqlDataAdapter(cmd)
                            da.Fill(ds, "debts")
                        End Using
                    End Using

                    '-- Find the client names
                    Using cmd As New System.Data.SqlClient.SqlCommand()
                        cmd.Connection = cn
                        cmd.CommandText = "SELECT relation, dbo.format_normal_name(default, pn.first, pn.middle, pn.last, pn.suffix) as client_name FROM people p WITH (NOLOCK) LEFT OUTER JOIN names pn WITH (NOLOCK) ON p.nameid = pn.name WHERE p.client = @client ORDER BY relation"
                        cmd.CommandType = System.Data.CommandType.Text
                        cmd.Parameters.Add("@client", System.Data.SqlDbType.Int).Value = rpt.Parameters("ParameterClient").Value
                        cmd.CommandTimeout = 0

                        Using da As New System.Data.SqlClient.SqlDataAdapter(cmd)
                            da.Fill(ds, "people")
                        End Using
                    End Using

                    '--fill the client payout details
                    Using cmd As New System.Data.SqlClient.SqlCommand()
                        cmd.Connection = cn
                        cmd.CommandText = "SELECT payout_total_debt,payout_months_to_payout,payout_total_fees,payout_deposit_amount,payout_termination_date FROM [clients] WITH (NOLOCK) WHERE client = @client"
                        cmd.CommandType = System.Data.CommandType.Text
                        cmd.Parameters.Add("@client", System.Data.SqlDbType.Int).Value = rpt.Parameters("ParameterClient").Value
                        cmd.CommandTimeout = 0

                        Using da As New System.Data.SqlClient.SqlDataAdapter(cmd)
                            da.Fill(ds, "clients")
                        End Using
                    End Using

                    '-- Try to locate the item in the collection that matches our search term
                    Dim revInfo As RevisionInfo = Nothing
                    For Each item As RevisionInfo In colRevisions
                        If String.Compare(item.StateName, StateName, True) = 0 AndAlso String.Compare(item.Language, LanguageName, True) = 0 Then
                            revInfo = item
                            Exit For
                        End If
                    Next

                    ' If the item is not found then try "English" as a language for this state
                    If revInfo Is Nothing Then
                        For Each item As RevisionInfo In colRevisions
                            If String.Compare(item.StateName, StateName, True) = 0 AndAlso String.Compare(item.Language, "en-US", True) = 0 Then
                                revInfo = item
                                Exit For
                            End If
                        Next

                        ' If still not found then try "state fees" and the proper language for the item
                        If revInfo Is Nothing Then
                            For Each item As RevisionInfo In colRevisions
                                If String.Compare(item.StateName, "fee states", True) = 0 AndAlso String.Compare(item.Language, LanguageName, True) = 0 Then
                                    revInfo = item
                                    Exit For
                                End If
                            Next

                            ' If still not found then try "state fees" and English
                            If revInfo Is Nothing Then
                                For Each item As RevisionInfo In colRevisions
                                    If String.Compare(item.StateName, "fee states", True) = 0 AndAlso String.Compare(item.Language, "en-US", True) = 0 Then
                                        revInfo = item
                                        Exit For
                                    End If
                                Next
                            End If
                        End If
                    End If

                    ' Finally if not found then just exit without doing anything
                    If revInfo Is Nothing Then
                        Return
                    End If

                    ' Find the corresponding state reference name
                    Dim strText As String = revInfo.StateName
                    For Each st As DebtPlus.LINQ.state In DebtPlus.LINQ.Cache.state.getList()
                        If String.Compare(st.MailingCode.PadLeft(2).Substring(0, 2), revInfo.StateName, True) = 0 Then
                            strText = st.Name
                            Exit For
                        End If
                    Next

                    ' Format the footer field with the state name
                    XrLabel_page_footer.Text = String.Format(revInfo.Footer, strText)

                    ' Load the header buffer text area
                    strText = LoadRTFText(revInfo, revInfo.Heading)
                    If Not String.IsNullOrWhiteSpace(strText) Then
                        LoadControl(XrRichText_debt_counseling_notice, strText)
                        GroupHeader_Header.Visible = True
                    Else
                        GroupHeader_Header.Visible = False
                        GroupHeader_Header.HeightF = 0.0!
                    End If

                    ' Load the main RTF text block. There should be something here.
                    strText = LoadRTFText(revInfo, revInfo.Text)
                    If Not String.IsNullOrWhiteSpace(strText) Then
                        LoadControl(XrRichText_notice_text, strText)
                    End If

                    ' Bind the schedules sub-report to the main report
                    Dim ScheduleSubReport As DevExpress.XtraReports.UI.XtraReport = LoadSchedule(revInfo, revInfo.Schedules)
                    If ScheduleSubReport IsNot Nothing Then
                        XrSubreportSchedules.ReportSource = ScheduleSubReport
                        XrSubreportSchedules.Visible = True
                        GroupHeader_Schedule.Visible = True
                    Else
                        GroupHeader_Schedule.Visible = False
                        GroupHeader_Schedule.HeightF = 0.0!
                    End If

                    ' Load the panel #1 text area
                    strText = LoadRTFText(revInfo, revInfo.Panel1)
                    If Not String.IsNullOrWhiteSpace(strText) Then
                        GroupHeader_Pannel1.Visible = True
                        Dim currentDate As String = System.DateTime.Now.ToShortDateString()
                        Dim cancellationDate As String = System.DateTime.Now.AddDays(14)
                        strText = strText.Replace("#CurrentDate#", currentDate).Replace("#CancellationDate#", cancellationDate)
                        LoadControl(XrRichText_Pannel1, strText)
                    Else
                        GroupHeader_Pannel1.Visible = False
                        GroupHeader_Pannel1.HeightF = 0.0!
                    End If

                    ' Load the panel #2 text area
                    strText = LoadRTFText(revInfo, revInfo.Panel2)
                    If strText IsNot Nothing Then
                        GroupHeader_Pannel2.Visible = True
                        Dim currentDate As String = System.DateTime.Now.ToShortDateString()
                        Dim cancellationDate As String = System.DateTime.Now.AddDays(14)
                        strText = strText.Replace("#CurrentDate#", currentDate).Replace("#CancellationDate#", cancellationDate)
                        LoadControl(XrRichText_Pannel2, strText)
                    Else
                        GroupHeader_Pannel2.Visible = False
                        GroupHeader_Pannel2.HeightF = 0.0!
                    End If

                    ' Load the panel #3 text area
                    strText = LoadRTFText(revInfo, revInfo.Panel3)
                    If strText IsNot Nothing Then
                        GroupHeader_Pannel3.Visible = True
                        Dim currentDate As String = System.DateTime.Now.ToShortDateString()
                        Dim cancellationDate As String = System.DateTime.Now.AddDays(14)
                        strText = strText.Replace("#CurrentDate#", currentDate).Replace("#CancellationDate#", cancellationDate)
                        LoadControl(XrRichText_Pannel3, strText)
                    Else
                        GroupHeader_Pannel3.Visible = False
                        GroupHeader_Pannel3.HeightF = 0.0!
                    End If

                    ' Load the panel #4 text area
                    strText = LoadRTFText(revInfo, revInfo.Panel4)
                    If strText IsNot Nothing Then
                        GroupHeader_Pannel4.Visible = True
                        Dim currentDate As String = System.DateTime.Now.ToShortDateString()
                        Dim cancellationDate As String = System.DateTime.Now.AddDays(14)
                        strText = strText.Replace("#CurrentDate#", currentDate).Replace("#CancellationDate#", cancellationDate)
                        LoadControl(XrRichText_Pannel4, strText)
                    Else
                        GroupHeader_Pannel4.Visible = False
                        GroupHeader_Pannel4.HeightF = 0.0!
                    End If

                    ' Format the revision information with the revision date
                    XrLabel_revision.Text = String.Format(revInfo.RevisionString, revInfo.RevisionDate)

                    ' Generate the data record list for the report
                    Record = New DebtPlus.Reports.Forms.Authorization.RecordDefinition(ds, LanguageName)
                    Dim lst As New System.Collections.Generic.List(Of DebtPlus.Reports.Forms.Authorization.RecordDefinition)
                    lst.Add(Record)
                    rpt.DataSource = lst
                End Using

            Catch ex As System.Exception
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex)
            End Try
        End Sub

        ''' <summary>
        ''' Read the indicated source file and return the string contents
        ''' </summary>
        Private Function LoadRTFText(revInfo As RevisionInfo, fileName As String) As String

            ' If there is no file then silently return nothing
            If String.IsNullOrWhiteSpace(fileName) Then
                Return Nothing
            End If

            Try
                ' Translate the filename to a specific entry.
                Dim fName As String = String.Format(fileName, revInfo.BaseDirectory, revInfo.StateName, revInfo.Language)

                ' Open the source file and read its contents
                Using fs As New System.IO.FileStream(fName, System.IO.FileMode.Open, System.IO.FileAccess.Read, System.IO.FileShare.Read)
                    Using sr As New System.IO.StreamReader(fs)
                        Return sr.ReadToEnd()
                    End Using
                End Using

            Catch ex As System.IO.FileNotFoundException
                ' Do nothing about the error condition.

            Catch ex As System.IO.DirectoryNotFoundException
                ' Do nothing about the error condition.
            End Try

            ' Return the empty string to indicate that there is no data.
            Return Nothing
        End Function

        ''' <summary>
        ''' Load the indicated RTF editing control with the text. We support RTF, HTML, and TXT formats
        ''' </summary>
        Private Sub LoadControl(ctl As DevExpress.XtraReports.UI.XRRichText, strText As String)

            ' Ignore empty text buffers
            If String.IsNullOrWhiteSpace(strText) Then
                Return
            End If

            ' Look for RTF text formats
            If DebtPlus.Utils.Format.Strings.IsRTF(strText) Then
                Try
                    ctl.Rtf = strText
                    Return
                Catch ex As Exception
                    ' Ignore the error conditions that may be thrown for loading invalid RTF text
                End Try
            End If

            ' Look for HTML text format
            If DebtPlus.Utils.Format.Strings.IsHTML(strText) Then
                Try
                    ctl.Html = strText
                    Return
                Catch ex As Exception
                    ' Ignore the error conditions that may be thrown for loading invalid RTF text
                End Try
            End If

            ' Finally, load the text component. There should be no error here about bad format.
            ctl.Text = strText
        End Sub

        ''' <summary>
        ''' Load the schedules sub-report if one is present in the directory
        ''' </summary>
        Private Function LoadSchedule(revInfo As RevisionInfo, fileName As String) As DevExpress.XtraReports.UI.XtraReport

            ' If there is no file name then silently return nothing
            If String.IsNullOrWhiteSpace(fileName) Then
                Return Nothing
            End If

            ' Translate the filename to a specific entry.
            Dim fName As String = String.Format(fileName, revInfo.BaseDirectory, revInfo.StateName, revInfo.Language)

            Try
                '-- Try to load the schedules sub-report if possible
                If System.IO.File.Exists(fName) Then
                    Using fs As New System.IO.FileStream(fName, System.IO.FileMode.Open, System.IO.FileAccess.Read, System.IO.FileShare.Read)

                        '-- Load the report
                        Dim SubRpt As New DebtPlus.Reports.Template.BaseXtraReportClass
                        SubRpt.LoadLayout(fs)
                        SubRpt.ScriptReferences = DebtPlus.Utils.AssemblyTools.GetAllSiblingAssemblies()
                        Return SubRpt
                    End Using
                End If

            Catch ex As System.Exception
                ' Ignore the error conditions
            End Try

            Return Nothing
        End Function

        Private Class RevisionInfo
            Public Sub New()
                Footer = "for use in {0} only"
                RevisionString = "revised {0:M/yy}"
                Text = "{0}/{2}/{1}.rtf"
                Heading = "{0}/{2}/HeaderText.rtf"
            End Sub

            Public Property Language As String
            Public Property BaseDirectory As String
            Public Property RevisionString As String
            Public Property StateName As String
            Public Property Text As String
            Public Property Heading As String
            Public Property Footer As String
            Public Property Schedules As String
            Public Property Panel1 As String
            Public Property Panel2 As String
            Public Property Panel3 As String
            Public Property Panel4 As String

            Public Property RevisionDate As DateTime
        End Class

        Private Shared Function getRevisionInfo(ByVal NewBaseDir As String) As System.Collections.Generic.List(Of RevisionInfo)
            Dim XMLName As String = System.IO.Path.Combine(NewBaseDir, "Revisions.xml")
            Dim answer As New System.Collections.Generic.List(Of RevisionInfo)

            Try
                If System.IO.File.Exists(XMLName) Then
                    Using streamSource As New System.IO.FileStream(XMLName, System.IO.FileMode.Open, System.IO.FileAccess.Read, System.IO.FileShare.Read)
                        Dim doc As New System.Xml.XmlDocument
                        doc.Load(streamSource)

                        '-- Process the document
                        Dim Item As System.Xml.XmlNode = doc.FirstChild.NextSibling.FirstChild
                        Do While Item IsNot Nothing
                            If String.Compare(Item.Name, "revision", True) = 0 AndAlso Item.HasChildNodes Then
                                Dim ChildItem As System.Xml.XmlNode = Item.FirstChild
                                Dim RevInfo As New RevisionInfo() With {.BaseDirectory = NewBaseDir}

                                '-- Look for the node with the item values
                                Do While ChildItem IsNot Nothing
                                    If String.Compare(ChildItem.Name, "state", True) = 0 Then
                                        If (Not String.IsNullOrEmpty(ChildItem.InnerText)) Then RevInfo.StateName = ChildItem.InnerText
                                    ElseIf String.Compare(ChildItem.Name, "language", True) = 0 Then
                                        If (Not String.IsNullOrEmpty(ChildItem.InnerText)) Then RevInfo.Language = ChildItem.InnerText
                                    ElseIf String.Compare(ChildItem.Name, "date", True) = 0 Then
                                        If (Not String.IsNullOrEmpty(ChildItem.InnerText)) Then RevInfo.RevisionDate = System.Xml.XmlConvert.ToDateTime(ChildItem.InnerText, Xml.XmlDateTimeSerializationMode.Local)
                                    ElseIf String.Compare(ChildItem.Name, "revised", True) = 0 Then
                                        If (Not String.IsNullOrEmpty(ChildItem.InnerText)) Then RevInfo.RevisionString = ChildItem.InnerText
                                    ElseIf String.Compare(ChildItem.Name, "text", True) = 0 Then
                                        If (Not String.IsNullOrEmpty(ChildItem.InnerText)) Then RevInfo.Text = ChildItem.InnerText
                                    ElseIf String.Compare(ChildItem.Name, "heading", True) = 0 Then
                                        If (Not String.IsNullOrEmpty(ChildItem.InnerText)) Then RevInfo.Heading = ChildItem.InnerText
                                    ElseIf String.Compare(ChildItem.Name, "footer", True) = 0 Then
                                        If (Not String.IsNullOrEmpty(ChildItem.InnerText)) Then RevInfo.Footer = ChildItem.InnerText
                                    ElseIf String.Compare(ChildItem.Name, "panel1", True) = 0 Then
                                        If (Not String.IsNullOrEmpty(ChildItem.InnerText)) Then RevInfo.Panel1 = ChildItem.InnerText
                                    ElseIf String.Compare(ChildItem.Name, "panel2", True) = 0 Then
                                        If (Not String.IsNullOrEmpty(ChildItem.InnerText)) Then RevInfo.Panel2 = ChildItem.InnerText
                                    ElseIf String.Compare(ChildItem.Name, "panel3", True) = 0 Then
                                        If (Not String.IsNullOrEmpty(ChildItem.InnerText)) Then RevInfo.Panel3 = ChildItem.InnerText
                                    ElseIf String.Compare(ChildItem.Name, "panel4", True) = 0 Then
                                        If (Not String.IsNullOrEmpty(ChildItem.InnerText)) Then RevInfo.Panel4 = ChildItem.InnerText
                                    ElseIf String.Compare(ChildItem.Name, "schedule", True) = 0 Then
                                        If (Not String.IsNullOrEmpty(ChildItem.InnerText)) Then RevInfo.Schedules = ChildItem.InnerText
                                    ElseIf String.Compare(ChildItem.Name, "basedirectory", True) = 0 Then
                                        If (Not String.IsNullOrEmpty(ChildItem.InnerText)) Then RevInfo.BaseDirectory = ChildItem.InnerText
                                    End If
                                    ChildItem = ChildItem.NextSibling
                                Loop

                                '-- Add the item to the resulting list
                                answer.Add(RevInfo)
                            End If

                            '-- On to the next item
                            Item = Item.NextSibling
                        Loop
                    End Using
                End If

            Catch ex As System.Xml.XmlException
                ' Ignore these error conditions.
            End Try

            ' Return the collection of items from the XML document
            Return answer
        End Function
    End Class
End Namespace
