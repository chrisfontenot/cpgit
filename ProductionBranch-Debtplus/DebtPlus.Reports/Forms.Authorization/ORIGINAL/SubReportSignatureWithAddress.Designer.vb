Namespace Forms.Authorization
    <Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
    Partial Class SubReportSignatureWithAddress
        Inherits DevExpress.XtraReports.UI.XtraReport

        'XtraReport overrides dispose to clean up the component list.
        <System.Diagnostics.DebuggerNonUserCode()> _
        Protected Overrides Sub Dispose(ByVal disposing As Boolean)
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
            MyBase.Dispose(disposing)
        End Sub

        'Required by the Designer
        Private components As System.ComponentModel.IContainer = Nothing

        'NOTE: The following procedure is required by the Designer
        'It can be modified using the Designer.  
        'Do not modify it using the code editor.
        <System.Diagnostics.DebuggerStepThrough()> _
        Private Sub InitializeComponent()
            Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(SubReportSignatureWithAddress))
            Me.XrLabel3 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel1 = New DevExpress.XtraReports.UI.XRLabel()
            Me.Detail = New DevExpress.XtraReports.UI.DetailBand()
            Me.XrLabel_Applicant_Signature = New DevExpress.XtraReports.UI.XRLabel()
            Me.TopMarginBand1 = New DevExpress.XtraReports.UI.TopMarginBand()
            Me.BottomMarginBand1 = New DevExpress.XtraReports.UI.BottomMarginBand()
            Me.ReportFooter = New DevExpress.XtraReports.UI.ReportFooterBand()
            Me.XrRichText_CounselorSignature = New DevExpress.XtraReports.UI.XRRichText()
            Me.GroupFooter1 = New DevExpress.XtraReports.UI.GroupFooterBand()
            Me.XrLabelPhone = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel4 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabelAddressLine1 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabelPhoneNumber = New DevExpress.XtraReports.UI.XRLabel()
            Me.GroupHeader1 = New DevExpress.XtraReports.UI.GroupHeaderBand()
            CType(Me.XrRichText_CounselorSignature, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
            '
            'XrLabel3
            '
            Me.XrLabel3.Borders = DevExpress.XtraPrinting.BorderSide.Top
            Me.XrLabel3.BorderWidth = 2
            Me.XrLabel3.Font = New System.Drawing.Font("Calibri", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.XrLabel3.LocationFloat = New DevExpress.Utils.PointFloat(386.1249!, 63.04169!)
            Me.XrLabel3.Name = "XrLabel3"
            Me.XrLabel3.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel3.SizeF = New System.Drawing.SizeF(153.4584!, 23.25!)
            Me.XrLabel3.StylePriority.UseBorders = False
            Me.XrLabel3.StylePriority.UseBorderWidth = False
            Me.XrLabel3.StylePriority.UseFont = False
            Me.XrLabel3.Text = "Date"
            '
            'XrLabel1
            '
            Me.XrLabel1.Borders = DevExpress.XtraPrinting.BorderSide.Top
            Me.XrLabel1.BorderWidth = 2
            Me.XrLabel1.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "client_name")})
            Me.XrLabel1.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 63.04169!)
            Me.XrLabel1.Name = "XrLabel1"
            Me.XrLabel1.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel1.SizeF = New System.Drawing.SizeF(347.9167!, 23.25!)
            Me.XrLabel1.StylePriority.UseBorders = False
            Me.XrLabel1.StylePriority.UseBorderWidth = False
            Me.XrLabel1.Text = "XrLabel1"
            '
            'Detail
            '
            Me.Detail.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel_Applicant_Signature, Me.XrLabel1, Me.XrLabel3})
            Me.Detail.HeightF = 129.0!
            Me.Detail.Name = "Detail"
            '
            'XrLabel_Applicant_Signature
            '
            Me.XrLabel_Applicant_Signature.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
            Me.XrLabel_Applicant_Signature.Name = "XrLabel_Applicant_Signature"
            Me.XrLabel_Applicant_Signature.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_Applicant_Signature.Scripts.OnBeforePrint = "XrLabel_Applicant_Signature_BeforePrint"
            Me.XrLabel_Applicant_Signature.SizeF = New System.Drawing.SizeF(302.0833!, 23.0!)
            Me.XrLabel_Applicant_Signature.Text = "Applicant signature"
            '
            'TopMarginBand1
            '
            Me.TopMarginBand1.HeightF = 25.0!
            Me.TopMarginBand1.Name = "TopMarginBand1"
            '
            'BottomMarginBand1
            '
            Me.BottomMarginBand1.HeightF = 25.0!
            Me.BottomMarginBand1.Name = "BottomMarginBand1"
            '
            'ReportFooter
            '
            Me.ReportFooter.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrRichText_CounselorSignature})
            Me.ReportFooter.HeightF = 101.125!
            Me.ReportFooter.Name = "ReportFooter"
            '
            'XrRichText_CounselorSignature
            '
            Me.XrRichText_CounselorSignature.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 10.00001!)
            Me.XrRichText_CounselorSignature.Name = "XrRichText_CounselorSignature"
            Me.XrRichText_CounselorSignature.Scripts.OnBeforePrint = "XrRichText_CounselorSignature_BeforePrint"
            Me.XrRichText_CounselorSignature.SerializableRtfString = resources.GetString("XrRichText_CounselorSignature.SerializableRtfString")
            Me.XrRichText_CounselorSignature.SizeF = New System.Drawing.SizeF(539.5833!, 91.12498!)
            '
            'GroupFooter1
            '
            Me.GroupFooter1.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabelPhone, Me.XrLabel4, Me.XrLabelAddressLine1, Me.XrLabelPhoneNumber})
            Me.GroupFooter1.HeightF = 79.16667!
            Me.GroupFooter1.Name = "GroupFooter1"
            '
            'XrLabelPhone
            '
            Me.XrLabelPhone.Font = New System.Drawing.Font("Calibri", 12.0!)
            Me.XrLabelPhone.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
            Me.XrLabelPhone.Name = "XrLabelPhone"
            Me.XrLabelPhone.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabelPhone.SizeF = New System.Drawing.SizeF(50.33334!, 17.00001!)
            Me.XrLabelPhone.StylePriority.UseFont = False
            Me.XrLabelPhone.Text = "Phone"
            '
            'XrLabel4
            '
            Me.XrLabel4.Font = New System.Drawing.Font("Calibri", 12.0!)
            Me.XrLabel4.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 29.54165!)
            Me.XrLabel4.Name = "XrLabel4"
            Me.XrLabel4.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel4.SizeF = New System.Drawing.SizeF(63.54169!, 17.00001!)
            Me.XrLabel4.StylePriority.UseFont = False
            Me.XrLabel4.Text = "Address"
            '
            'XrLabelAddressLine1
            '
            Me.XrLabelAddressLine1.Font = New System.Drawing.Font("Times New Roman", 9.75!)
            Me.XrLabelAddressLine1.LocationFloat = New DevExpress.Utils.PointFloat(83.33337!, 29.54165!)
            Me.XrLabelAddressLine1.Multiline = True
            Me.XrLabelAddressLine1.Name = "XrLabelAddressLine1"
            Me.XrLabelAddressLine1.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabelAddressLine1.SizeF = New System.Drawing.SizeF(389.5833!, 49.62502!)
            Me.XrLabelAddressLine1.StylePriority.UseFont = False
            '
            'XrLabelPhoneNumber
            '
            Me.XrLabelPhoneNumber.Font = New System.Drawing.Font("Calibri", 12.0!)
            Me.XrLabelPhoneNumber.LocationFloat = New DevExpress.Utils.PointFloat(83.33336!, 0.0!)
            Me.XrLabelPhoneNumber.Name = "XrLabelPhoneNumber"
            Me.XrLabelPhoneNumber.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabelPhoneNumber.SizeF = New System.Drawing.SizeF(117.0!, 17.00001!)
            Me.XrLabelPhoneNumber.StylePriority.UseFont = False
            Me.XrLabelPhoneNumber.Text = "000-000-0000"
            '
            'GroupHeader1
            '
            Me.GroupHeader1.HeightF = 0.0!
            Me.GroupHeader1.Name = "GroupHeader1"
            '
            'SubReportSignatureWithAddress
            '
            Me.Bands.AddRange(New DevExpress.XtraReports.UI.Band() {Me.Detail, Me.TopMarginBand1, Me.BottomMarginBand1, Me.ReportFooter, Me.GroupFooter1, Me.GroupHeader1})
            Me.Font = New System.Drawing.Font("Calibri", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.Margins = New System.Drawing.Printing.Margins(25, 25, 25, 25)
            Me.ScriptLanguage = DevExpress.XtraReports.ScriptLanguage.VisualBasic
            Me.ScriptReferencesString = resources.GetString("$this.ScriptReferencesString")
            Me.Scripts.OnBeforePrint = "SubReportSignature_BeforePrint"
            Me.ScriptsSource = resources.GetString("$this.ScriptsSource")
            Me.Version = "11.2"
            CType(Me.XrRichText_CounselorSignature, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me, System.ComponentModel.ISupportInitialize).EndInit()

        End Sub

        Protected Friend WithEvents Detail As DevExpress.XtraReports.UI.DetailBand
        Protected Friend WithEvents XrLabel1 As DevExpress.XtraReports.UI.XRLabel
        Protected Friend WithEvents XrLabel3 As DevExpress.XtraReports.UI.XRLabel
        Protected Friend WithEvents TopMarginBand1 As DevExpress.XtraReports.UI.TopMarginBand
        Protected Friend WithEvents BottomMarginBand1 As DevExpress.XtraReports.UI.BottomMarginBand
        Protected Friend WithEvents ReportFooter As DevExpress.XtraReports.UI.ReportFooterBand
        Protected Friend WithEvents XrRichText_CounselorSignature As DevExpress.XtraReports.UI.XRRichText
        Protected Friend WithEvents XrLabel_Applicant_Signature As DevExpress.XtraReports.UI.XRLabel
        Protected Friend WithEvents GroupFooter1 As DevExpress.XtraReports.UI.GroupFooterBand
        Protected Friend WithEvents XrLabelPhone As DevExpress.XtraReports.UI.XRLabel
        Protected Friend WithEvents XrLabel4 As DevExpress.XtraReports.UI.XRLabel
        Protected Friend WithEvents XrLabelAddressLine1 As DevExpress.XtraReports.UI.XRLabel
        Protected Friend WithEvents XrLabelPhoneNumber As DevExpress.XtraReports.UI.XRLabel
        Protected Friend WithEvents GroupHeader1 As DevExpress.XtraReports.UI.GroupHeaderBand
    End Class
End Namespace
