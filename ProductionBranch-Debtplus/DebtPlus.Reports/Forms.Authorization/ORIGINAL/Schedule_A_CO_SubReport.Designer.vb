﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Public Class DMPDebtsSubReport
    Inherits DevExpress.XtraReports.UI.XtraReport

    'XtraReport overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Designer
    'It can be modified using the Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(DMPDebtsSubReport))
        Dim XrSummary1 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary()
        Dim XrSummary2 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary()
        Dim XrSummary3 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary()
        Me.XrTable9 = New DevExpress.XtraReports.UI.XRTable()
        Me.XrTableRow37 = New DevExpress.XtraReports.UI.XRTableRow()
        Me.XrTableCell98 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.XrTableCell101 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.XrTableCell102 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.XrTableCell103 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.XrTableRow34 = New DevExpress.XtraReports.UI.XRTableRow()
        Me.XrTableCell8 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.XrTableCell78 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.XrTableCell70 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.XrTableCell81 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.XrTableCell71 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.XrTableCell97 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.XrTableCell87 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.XrTableCell90 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.XrTableCell93 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.XrTable10 = New DevExpress.XtraReports.UI.XRTable()
        Me.XrTableRow35 = New DevExpress.XtraReports.UI.XRTableRow()
        Me.XrTableCell_creditor_name = New DevExpress.XtraReports.UI.XRTableCell()
        Me.XrTableCell_account_number = New DevExpress.XtraReports.UI.XRTableCell()
        Me.XrTableCel_balance = New DevExpress.XtraReports.UI.XRTableCell()
        Me.XrTableCell_dmp_rate = New DevExpress.XtraReports.UI.XRTableCell()
        Me.XrTableCell_disbursement_factor = New DevExpress.XtraReports.UI.XRTableCell()
        Me.XrTableCellnon_dmp_rate = New DevExpress.XtraReports.UI.XRTableCell()
        Me.XrTableCell_non_dmp_payment = New DevExpress.XtraReports.UI.XRTableCell()
        Me.XrTableCell_payout_date = New DevExpress.XtraReports.UI.XRTableCell()
        Me.XrTableCell_creditor = New DevExpress.XtraReports.UI.XRTableCell()
        Me.XrTableRow38 = New DevExpress.XtraReports.UI.XRTableRow()
        Me.XrTableCell_creditor_address = New DevExpress.XtraReports.UI.XRTableCell()
        Me.XrTableCell107 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.GroupFooter1 = New DevExpress.XtraReports.UI.GroupFooterBand()
        Me.XrPanel5 = New DevExpress.XtraReports.UI.XRPanel()
        Me.XrLabel16 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLine2 = New DevExpress.XtraReports.UI.XRLine()
        Me.XrLabel30 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLine1 = New DevExpress.XtraReports.UI.XRLine()
        Me.XrLabel29 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrRichText1 = New DevExpress.XtraReports.UI.XRRichText()
        Me.XrTable11 = New DevExpress.XtraReports.UI.XRTable()
        Me.XrTableRow36 = New DevExpress.XtraReports.UI.XRTableRow()
        Me.XrTableCell80 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.XrTableCell_total_balance = New DevExpress.XtraReports.UI.XRTableCell()
        Me.XrTableCell_total_disbursement_factor = New DevExpress.XtraReports.UI.XRTableCell()
        Me.XrTableCell_total_non_dmp_payment = New DevExpress.XtraReports.UI.XRTableCell()
        Me.XrTableCell95 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.GroupFooter2 = New DevExpress.XtraReports.UI.GroupFooterBand()
        Me.XrRichText2 = New DevExpress.XtraReports.UI.XRRichText()
        Me.label1 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel1 = New DevExpress.XtraReports.UI.XRLabel()
        Me.GroupHeader1 = New DevExpress.XtraReports.UI.GroupHeaderBand()
        Me.Detail = New DevExpress.XtraReports.UI.DetailBand()
        Me.ReportHeader = New DevExpress.XtraReports.UI.ReportHeaderBand()
        Me.TopMarginBand1 = New DevExpress.XtraReports.UI.TopMarginBand()
        Me.BottomMarginBand1 = New DevExpress.XtraReports.UI.BottomMarginBand()
        CType(Me.XrTable9, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.XrTable10, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.XrRichText1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.XrTable11, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.XrRichText2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
        '
        'XrTable9
        '
        Me.XrTable9.AnchorVertical = DevExpress.XtraReports.UI.VerticalAnchorStyles.Bottom
        Me.XrTable9.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
        Me.XrTable9.Name = "XrTable9"
        Me.XrTable9.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 2, 2, 100.0!)
        Me.XrTable9.Rows.AddRange(New DevExpress.XtraReports.UI.XRTableRow() {Me.XrTableRow37, Me.XrTableRow34})
        Me.XrTable9.SizeF = New System.Drawing.SizeF(743.7501!, 34.0!)
        Me.XrTable9.StylePriority.UsePadding = False
        '
        'XrTableRow37
        '
        Me.XrTableRow37.Borders = CType((((DevExpress.XtraPrinting.BorderSide.Left Or DevExpress.XtraPrinting.BorderSide.Top) _
            Or DevExpress.XtraPrinting.BorderSide.Right) _
            Or DevExpress.XtraPrinting.BorderSide.Bottom), DevExpress.XtraPrinting.BorderSide)
        Me.XrTableRow37.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.XrTableCell98, Me.XrTableCell101, Me.XrTableCell102, Me.XrTableCell103})
        Me.XrTableRow37.Font = New System.Drawing.Font("Calibri", 12.0!, System.Drawing.FontStyle.Bold)
        Me.XrTableRow37.Name = "XrTableRow37"
        Me.XrTableRow37.StylePriority.UseBorders = False
        Me.XrTableRow37.StylePriority.UseFont = False
        Me.XrTableRow37.StylePriority.UseTextAlignment = False
        Me.XrTableRow37.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
        Me.XrTableRow37.Weight = 1.0R
        '
        'XrTableCell98
        '
        Me.XrTableCell98.Borders = DevExpress.XtraPrinting.BorderSide.Bottom
        Me.XrTableCell98.CanGrow = False
        Me.XrTableCell98.Name = "XrTableCell98"
        Me.XrTableCell98.StylePriority.UseBorders = False
        Me.XrTableCell98.Weight = 1.5R
        '
        'XrTableCell101
        '
        Me.XrTableCell101.BackColor = System.Drawing.Color.Black
        Me.XrTableCell101.BorderColor = System.Drawing.Color.White
        Me.XrTableCell101.Borders = CType((DevExpress.XtraPrinting.BorderSide.Left Or DevExpress.XtraPrinting.BorderSide.Right), DevExpress.XtraPrinting.BorderSide)
        Me.XrTableCell101.BorderWidth = 5
        Me.XrTableCell101.CanGrow = False
        Me.XrTableCell101.ForeColor = System.Drawing.Color.White
        Me.XrTableCell101.Name = "XrTableCell101"
        Me.XrTableCell101.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 2, 2, 100.0!)
        Me.XrTableCell101.StylePriority.UseBackColor = False
        Me.XrTableCell101.StylePriority.UseBorderColor = False
        Me.XrTableCell101.StylePriority.UseBorders = False
        Me.XrTableCell101.StylePriority.UseBorderWidth = False
        Me.XrTableCell101.StylePriority.UseForeColor = False
        Me.XrTableCell101.StylePriority.UsePadding = False
        Me.XrTableCell101.Text = "PLAN"
        Me.XrTableCell101.Weight = 0.61175878630211611R
        '
        'XrTableCell102
        '
        Me.XrTableCell102.BackColor = System.Drawing.Color.Black
        Me.XrTableCell102.BorderColor = System.Drawing.Color.White
        Me.XrTableCell102.Borders = CType((DevExpress.XtraPrinting.BorderSide.Left Or DevExpress.XtraPrinting.BorderSide.Right), DevExpress.XtraPrinting.BorderSide)
        Me.XrTableCell102.BorderWidth = 5
        Me.XrTableCell102.CanGrow = False
        Me.XrTableCell102.ForeColor = System.Drawing.Color.White
        Me.XrTableCell102.Name = "XrTableCell102"
        Me.XrTableCell102.StylePriority.UseBackColor = False
        Me.XrTableCell102.StylePriority.UseBorderColor = False
        Me.XrTableCell102.StylePriority.UseBorders = False
        Me.XrTableCell102.StylePriority.UseBorderWidth = False
        Me.XrTableCell102.StylePriority.UseForeColor = False
        Me.XrTableCell102.Text = "SELF"
        Me.XrTableCell102.Weight = 0.61748744332011629R
        '
        'XrTableCell103
        '
        Me.XrTableCell103.Borders = DevExpress.XtraPrinting.BorderSide.Bottom
        Me.XrTableCell103.CanGrow = False
        Me.XrTableCell103.Name = "XrTableCell103"
        Me.XrTableCell103.StylePriority.UseBorders = False
        Me.XrTableCell103.Weight = 0.85869376331118485R
        '
        'XrTableRow34
        '
        Me.XrTableRow34.Borders = CType((((DevExpress.XtraPrinting.BorderSide.Left Or DevExpress.XtraPrinting.BorderSide.Top) _
            Or DevExpress.XtraPrinting.BorderSide.Right) _
            Or DevExpress.XtraPrinting.BorderSide.Bottom), DevExpress.XtraPrinting.BorderSide)
        Me.XrTableRow34.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.XrTableCell8, Me.XrTableCell78, Me.XrTableCell70, Me.XrTableCell81, Me.XrTableCell71, Me.XrTableCell97, Me.XrTableCell87, Me.XrTableCell90, Me.XrTableCell93})
        Me.XrTableRow34.Font = New System.Drawing.Font("Calibri", 12.0!, System.Drawing.FontStyle.Bold)
        Me.XrTableRow34.Name = "XrTableRow34"
        Me.XrTableRow34.StylePriority.UseBorders = False
        Me.XrTableRow34.StylePriority.UseFont = False
        Me.XrTableRow34.StylePriority.UseTextAlignment = False
        Me.XrTableRow34.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
        Me.XrTableRow34.Weight = 1.0R
        '
        'XrTableCell8
        '
        Me.XrTableCell8.Borders = CType(((DevExpress.XtraPrinting.BorderSide.Left Or DevExpress.XtraPrinting.BorderSide.Right) _
            Or DevExpress.XtraPrinting.BorderSide.Bottom), DevExpress.XtraPrinting.BorderSide)
        Me.XrTableCell8.CanGrow = False
        Me.XrTableCell8.Name = "XrTableCell8"
        Me.XrTableCell8.StylePriority.UseBorders = False
        Me.XrTableCell8.StylePriority.UseTextAlignment = False
        Me.XrTableCell8.Text = "Name"
        Me.XrTableCell8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        Me.XrTableCell8.Weight = 0.8944722827347481R
        '
        'XrTableCell78
        '
        Me.XrTableCell78.Borders = CType(((DevExpress.XtraPrinting.BorderSide.Left Or DevExpress.XtraPrinting.BorderSide.Right) _
            Or DevExpress.XtraPrinting.BorderSide.Bottom), DevExpress.XtraPrinting.BorderSide)
        Me.XrTableCell78.CanGrow = False
        Me.XrTableCell78.Name = "XrTableCell78"
        Me.XrTableCell78.StylePriority.UseBorders = False
        Me.XrTableCell78.StylePriority.UseTextAlignment = False
        Me.XrTableCell78.Text = "ACCT #"
        Me.XrTableCell78.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
        Me.XrTableCell78.Weight = 0.2814071265185496R
        '
        'XrTableCell70
        '
        Me.XrTableCell70.Borders = CType(((DevExpress.XtraPrinting.BorderSide.Left Or DevExpress.XtraPrinting.BorderSide.Right) _
            Or DevExpress.XtraPrinting.BorderSide.Bottom), DevExpress.XtraPrinting.BorderSide)
        Me.XrTableCell70.CanGrow = False
        Me.XrTableCell70.Name = "XrTableCell70"
        Me.XrTableCell70.StylePriority.UseBorders = False
        Me.XrTableCell70.Text = "Balance"
        Me.XrTableCell70.Weight = 0.32412059074670224R
        '
        'XrTableCell81
        '
        Me.XrTableCell81.CanGrow = False
        Me.XrTableCell81.Name = "XrTableCell81"
        Me.XrTableCell81.Text = "Rate"
        Me.XrTableCell81.Weight = 0.28371872791692843R
        '
        'XrTableCell71
        '
        Me.XrTableCell71.CanGrow = False
        Me.XrTableCell71.Name = "XrTableCell71"
        Me.XrTableCell71.Text = "Payment"
        Me.XrTableCell71.Weight = 0.32804005991873442R
        '
        'XrTableCell97
        '
        Me.XrTableCell97.CanGrow = False
        Me.XrTableCell97.Name = "XrTableCell97"
        Me.XrTableCell97.Text = "Rate"
        Me.XrTableCell97.Weight = 0.28944709202752039R
        '
        'XrTableCell87
        '
        Me.XrTableCell87.CanGrow = False
        Me.XrTableCell87.Name = "XrTableCell87"
        Me.XrTableCell87.Text = "Payment"
        Me.XrTableCell87.Weight = 0.32804034975904922R
        '
        'XrTableCell90
        '
        Me.XrTableCell90.Borders = CType(((DevExpress.XtraPrinting.BorderSide.Left Or DevExpress.XtraPrinting.BorderSide.Right) _
            Or DevExpress.XtraPrinting.BorderSide.Bottom), DevExpress.XtraPrinting.BorderSide)
        Me.XrTableCell90.CanGrow = False
        Me.XrTableCell90.Name = "XrTableCell90"
        Me.XrTableCell90.StylePriority.UseBorders = False
        Me.XrTableCell90.Text = "Payout"
        Me.XrTableCell90.Weight = 0.31839196056576835R
        '
        'XrTableCell93
        '
        Me.XrTableCell93.Borders = CType(((DevExpress.XtraPrinting.BorderSide.Left Or DevExpress.XtraPrinting.BorderSide.Right) _
            Or DevExpress.XtraPrinting.BorderSide.Bottom), DevExpress.XtraPrinting.BorderSide)
        Me.XrTableCell93.CanGrow = False
        Me.XrTableCell93.Name = "XrTableCell93"
        Me.XrTableCell93.StylePriority.UseBorders = False
        Me.XrTableCell93.StylePriority.UseTextAlignment = False
        Me.XrTableCell93.Text = "Creditor ID"
        Me.XrTableCell93.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        Me.XrTableCell93.Weight = 0.5403018027454165R
        '
        'XrTable10
        '
        Me.XrTable10.Borders = CType((((DevExpress.XtraPrinting.BorderSide.Left Or DevExpress.XtraPrinting.BorderSide.Top) _
            Or DevExpress.XtraPrinting.BorderSide.Right) _
            Or DevExpress.XtraPrinting.BorderSide.Bottom), DevExpress.XtraPrinting.BorderSide)
        Me.XrTable10.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
        Me.XrTable10.Name = "XrTable10"
        Me.XrTable10.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 2, 2, 100.0!)
        Me.XrTable10.Rows.AddRange(New DevExpress.XtraReports.UI.XRTableRow() {Me.XrTableRow35, Me.XrTableRow38})
        Me.XrTable10.SizeF = New System.Drawing.SizeF(743.7499!, 34.0!)
        Me.XrTable10.StylePriority.UseBorders = False
        Me.XrTable10.StylePriority.UsePadding = False
        '
        'XrTableRow35
        '
        Me.XrTableRow35.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.XrTableCell_creditor_name, Me.XrTableCell_account_number, Me.XrTableCel_balance, Me.XrTableCell_dmp_rate, Me.XrTableCell_disbursement_factor, Me.XrTableCellnon_dmp_rate, Me.XrTableCell_non_dmp_payment, Me.XrTableCell_payout_date, Me.XrTableCell_creditor})
        Me.XrTableRow35.Name = "XrTableRow35"
        Me.XrTableRow35.Weight = 1.0R
        '
        'XrTableCell_creditor_name
        '
        Me.XrTableCell_creditor_name.CanShrink = True
        Me.XrTableCell_creditor_name.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "creditor_name")})
        Me.XrTableCell_creditor_name.Name = "XrTableCell_creditor_name"
        Me.XrTableCell_creditor_name.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
        Me.XrTableCell_creditor_name.StylePriority.UsePadding = False
        Me.XrTableCell_creditor_name.StylePriority.UseTextAlignment = False
        Me.XrTableCell_creditor_name.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        Me.XrTableCell_creditor_name.Weight = 0.8944723918098616R
        Me.XrTableCell_creditor_name.WordWrap = False
        '
        'XrTableCell_account_number
        '
        Me.XrTableCell_account_number.CanGrow = False
        Me.XrTableCell_account_number.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "account_number")})
        Me.XrTableCell_account_number.Name = "XrTableCell_account_number"
        Me.XrTableCell_account_number.StylePriority.UseTextAlignment = False
        Me.XrTableCell_account_number.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
        Me.XrTableCell_account_number.Weight = 0.28140698063831676R
        Me.XrTableCell_account_number.WordWrap = False
        '
        'XrTableCel_balance
        '
        Me.XrTableCel_balance.CanGrow = False
        Me.XrTableCel_balance.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "balance", "{0:c}")})
        Me.XrTableCel_balance.Name = "XrTableCel_balance"
        Me.XrTableCel_balance.StylePriority.UseTextAlignment = False
        Me.XrTableCel_balance.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
        Me.XrTableCel_balance.Weight = 0.32412062755182158R
        Me.XrTableCel_balance.WordWrap = False
        '
        'XrTableCell_dmp_rate
        '
        Me.XrTableCell_dmp_rate.CanGrow = False
        Me.XrTableCell_dmp_rate.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "dmp_interest", "{0:p}")})
        Me.XrTableCell_dmp_rate.Name = "XrTableCell_dmp_rate"
        Me.XrTableCell_dmp_rate.StylePriority.UseTextAlignment = False
        Me.XrTableCell_dmp_rate.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
        Me.XrTableCell_dmp_rate.Weight = 0.28371872791692843R
        Me.XrTableCell_dmp_rate.WordWrap = False
        '
        'XrTableCell_disbursement_factor
        '
        Me.XrTableCell_disbursement_factor.CanGrow = False
        Me.XrTableCell_disbursement_factor.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "dmp_payment", "{0:c}")})
        Me.XrTableCell_disbursement_factor.Name = "XrTableCell_disbursement_factor"
        Me.XrTableCell_disbursement_factor.StylePriority.UseTextAlignment = False
        Me.XrTableCell_disbursement_factor.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
        Me.XrTableCell_disbursement_factor.Weight = 0.32804005991873431R
        Me.XrTableCell_disbursement_factor.WordWrap = False
        '
        'XrTableCellnon_dmp_rate
        '
        Me.XrTableCellnon_dmp_rate.CanGrow = False
        Me.XrTableCellnon_dmp_rate.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "non_dmp_interest", "{0:p}")})
        Me.XrTableCellnon_dmp_rate.Name = "XrTableCellnon_dmp_rate"
        Me.XrTableCellnon_dmp_rate.StylePriority.UseTextAlignment = False
        Me.XrTableCellnon_dmp_rate.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
        Me.XrTableCellnon_dmp_rate.Weight = 0.28944738646847523R
        Me.XrTableCellnon_dmp_rate.WordWrap = False
        '
        'XrTableCell_non_dmp_payment
        '
        Me.XrTableCell_non_dmp_payment.CanGrow = False
        Me.XrTableCell_non_dmp_payment.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "non_dmp_payment", "{0:c}")})
        Me.XrTableCell_non_dmp_payment.Name = "XrTableCell_non_dmp_payment"
        Me.XrTableCell_non_dmp_payment.StylePriority.UseTextAlignment = False
        Me.XrTableCell_non_dmp_payment.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
        Me.XrTableCell_non_dmp_payment.Weight = 0.3232161551624087R
        Me.XrTableCell_non_dmp_payment.WordWrap = False
        '
        'XrTableCell_payout_date
        '
        Me.XrTableCell_payout_date.CanGrow = False
        Me.XrTableCell_payout_date.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "dmp_payout", "{0:M/yy}")})
        Me.XrTableCell_payout_date.Name = "XrTableCell_payout_date"
        Me.XrTableCell_payout_date.StylePriority.UseTextAlignment = False
        Me.XrTableCell_payout_date.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
        Me.XrTableCell_payout_date.Weight = 0.3232161551624087R
        Me.XrTableCell_payout_date.WordWrap = False
        '
        'XrTableCell_creditor
        '
        Me.XrTableCell_creditor.CanGrow = False
        Me.XrTableCell_creditor.Name = "XrTableCell_creditor"
        Me.XrTableCell_creditor.Scripts.OnBeforePrint = "XrTableCell_creditor_BeforePrint"
        Me.XrTableCell_creditor.StylePriority.UseTextAlignment = False
        Me.XrTableCell_creditor.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        Me.XrTableCell_creditor.Weight = 0.5403009194225521R
        Me.XrTableCell_creditor.WordWrap = False
        '
        'XrTableRow38
        '
        Me.XrTableRow38.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.XrTableCell_creditor_address, Me.XrTableCell107})
        Me.XrTableRow38.Name = "XrTableRow38"
        Me.XrTableRow38.Weight = 1.0R
        '
        'XrTableCell_creditor_address
        '
        Me.XrTableCell_creditor_address.Multiline = True
        Me.XrTableCell_creditor_address.Name = "XrTableCell_creditor_address"
        Me.XrTableCell_creditor_address.Scripts.OnBeforePrint = "XrTableCell_creditor_address_BeforePrint"
        Me.XrTableCell_creditor_address.Weight = 1.1758793969849246R
        Me.XrTableCell_creditor_address.WordWrap = False
        '
        'XrTableCell107
        '
        Me.XrTableCell107.Name = "XrTableCell107"
        Me.XrTableCell107.Weight = 2.4120600070665827R
        Me.XrTableCell107.WordWrap = False
        '
        'GroupFooter1
        '
        Me.GroupFooter1.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrPanel5, Me.XrRichText1, Me.XrTable11})
        Me.GroupFooter1.GroupUnion = DevExpress.XtraReports.UI.GroupFooterUnion.WithLastDetail
        Me.GroupFooter1.HeightF = 448.0!
        Me.GroupFooter1.Name = "GroupFooter1"
        '
        'XrPanel5
        '
        Me.XrPanel5.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel16, Me.XrLine2, Me.XrLabel30, Me.XrLine1, Me.XrLabel29})
        Me.XrPanel5.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 364.4165!)
        Me.XrPanel5.Name = "XrPanel5"
        Me.XrPanel5.SizeF = New System.Drawing.SizeF(764.4584!, 75.0!)
        '
        'XrLabel16
        '
        Me.XrLabel16.Font = New System.Drawing.Font("Calibri", 12.0!, System.Drawing.FontStyle.Bold)
        Me.XrLabel16.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
        Me.XrLabel16.Name = "XrLabel16"
        Me.XrLabel16.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel16.SizeF = New System.Drawing.SizeF(740.0!, 25.3334!)
        Me.XrLabel16.StylePriority.UseFont = False
        Me.XrLabel16.Text = "I UNDERSTAND THAT THIS IS AN ESTIMATE AND IS NO WAY BINDING."
        '
        'XrLine2
        '
        Me.XrLine2.LocationFloat = New DevExpress.Utils.PointFloat(402.0!, 50.0!)
        Me.XrLine2.Name = "XrLine2"
        Me.XrLine2.SizeF = New System.Drawing.SizeF(317.0!, 8.0!)
        '
        'XrLabel30
        '
        Me.XrLabel30.Font = New System.Drawing.Font("Calibri", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.XrLabel30.LocationFloat = New DevExpress.Utils.PointFloat(402.0!, 58.00009!)
        Me.XrLabel30.Name = "XrLabel30"
        Me.XrLabel30.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel30.SizeF = New System.Drawing.SizeF(100.0!, 17.0!)
        Me.XrLabel30.StylePriority.UseFont = False
        Me.XrLabel30.Text = "Date"
        '
        'XrLine1
        '
        Me.XrLine1.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 50.0!)
        Me.XrLine1.Name = "XrLine1"
        Me.XrLine1.SizeF = New System.Drawing.SizeF(383.0!, 8.0!)
        '
        'XrLabel29
        '
        Me.XrLabel29.Font = New System.Drawing.Font("Calibri", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.XrLabel29.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 58.00009!)
        Me.XrLabel29.Name = "XrLabel29"
        Me.XrLabel29.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel29.SizeF = New System.Drawing.SizeF(100.0!, 17.0!)
        Me.XrLabel29.StylePriority.UseFont = False
        Me.XrLabel29.Text = "Signature"
        '
        'XrRichText1
        '
        Me.XrRichText1.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 39.33331!)
        Me.XrRichText1.Name = "XrRichText1"
        Me.XrRichText1.SerializableRtfString = resources.GetString("XrRichText1.SerializableRtfString")
        Me.XrRichText1.SizeF = New System.Drawing.SizeF(750.0!, 325.0832!)
        '
        'XrTable11
        '
        Me.XrTable11.Borders = CType((((DevExpress.XtraPrinting.BorderSide.Left Or DevExpress.XtraPrinting.BorderSide.Top) _
            Or DevExpress.XtraPrinting.BorderSide.Right) _
            Or DevExpress.XtraPrinting.BorderSide.Bottom), DevExpress.XtraPrinting.BorderSide)
        Me.XrTable11.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
        Me.XrTable11.Name = "XrTable11"
        Me.XrTable11.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 2, 2, 100.0!)
        Me.XrTable11.Rows.AddRange(New DevExpress.XtraReports.UI.XRTableRow() {Me.XrTableRow36})
        Me.XrTable11.SizeF = New System.Drawing.SizeF(743.75!, 17.0!)
        Me.XrTable11.StylePriority.UseBorders = False
        Me.XrTable11.StylePriority.UsePadding = False
        '
        'XrTableRow36
        '
        Me.XrTableRow36.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.XrTableCell80, Me.XrTableCell_total_balance, Me.XrTableCell_total_disbursement_factor, Me.XrTableCell_total_non_dmp_payment, Me.XrTableCell95})
        Me.XrTableRow36.Font = New System.Drawing.Font("Calibri", 12.0!, System.Drawing.FontStyle.Bold)
        Me.XrTableRow36.Name = "XrTableRow36"
        Me.XrTableRow36.StylePriority.UseFont = False
        Me.XrTableRow36.StylePriority.UseTextAlignment = False
        Me.XrTableRow36.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
        Me.XrTableRow36.Weight = 0.47999998276654476R
        '
        'XrTableCell80
        '
        Me.XrTableCell80.Name = "XrTableCell80"
        Me.XrTableCell80.StylePriority.UseTextAlignment = False
        Me.XrTableCell80.Text = "TOTALS"
        Me.XrTableCell80.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        Me.XrTableCell80.Weight = 0.8944723986141645R
        '
        'XrTableCell_total_balance
        '
        Me.XrTableCell_total_balance.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "balance")})
        Me.XrTableCell_total_balance.Name = "XrTableCell_total_balance"
        XrSummary1.FormatString = "{0:c}"
        XrSummary1.Running = DevExpress.XtraReports.UI.SummaryRunning.Group
        Me.XrTableCell_total_balance.Summary = XrSummary1
        Me.XrTableCell_total_balance.Weight = 0.60552760138583528R
        Me.XrTableCell_total_balance.WordWrap = False
        '
        'XrTableCell_total_disbursement_factor
        '
        Me.XrTableCell_total_disbursement_factor.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "dmp_payment")})
        Me.XrTableCell_total_disbursement_factor.Name = "XrTableCell_total_disbursement_factor"
        XrSummary2.FormatString = "{0:c}"
        XrSummary2.Running = DevExpress.XtraReports.UI.SummaryRunning.Group
        Me.XrTableCell_total_disbursement_factor.Summary = XrSummary2
        Me.XrTableCell_total_disbursement_factor.Weight = 0.61175864061518537R
        Me.XrTableCell_total_disbursement_factor.WordWrap = False
        '
        'XrTableCell_total_non_dmp_payment
        '
        Me.XrTableCell_total_non_dmp_payment.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "non_dmp_payment")})
        Me.XrTableCell_total_non_dmp_payment.Name = "XrTableCell_total_non_dmp_payment"
        XrSummary3.FormatString = "{0:c}"
        XrSummary3.Running = DevExpress.XtraReports.UI.SummaryRunning.Group
        Me.XrTableCell_total_non_dmp_payment.Summary = XrSummary3
        Me.XrTableCell_total_non_dmp_payment.Weight = 0.617487589007047R
        Me.XrTableCell_total_non_dmp_payment.WordWrap = False
        '
        'XrTableCell95
        '
        Me.XrTableCell95.Name = "XrTableCell95"
        Me.XrTableCell95.Weight = 0.8586934688702299R
        Me.XrTableCell95.WordWrap = False
        '
        'GroupFooter2
        '
        Me.GroupFooter2.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrRichText2, Me.label1})
        Me.GroupFooter2.HeightF = 426.8767!
        Me.GroupFooter2.Level = 1
        Me.GroupFooter2.Name = "GroupFooter2"
        Me.GroupFooter2.PageBreak = DevExpress.XtraReports.UI.PageBreak.BeforeBand
        '
        'XrRichText2
        '
        Me.XrRichText2.Borders = CType((((DevExpress.XtraPrinting.BorderSide.Left Or DevExpress.XtraPrinting.BorderSide.Top) _
            Or DevExpress.XtraPrinting.BorderSide.Right) _
            Or DevExpress.XtraPrinting.BorderSide.Bottom), DevExpress.XtraPrinting.BorderSide)
        Me.XrRichText2.BorderWidth = 2
        Me.XrRichText2.LocationFloat = New DevExpress.Utils.PointFloat(10.00001!, 42.49999!)
        Me.XrRichText2.Name = "XrRichText2"
        Me.XrRichText2.Padding = New DevExpress.XtraPrinting.PaddingInfo(10, 10, 10, 10, 100.0!)
        Me.XrRichText2.SerializableRtfString = resources.GetString("XrRichText2.SerializableRtfString")
        Me.XrRichText2.SizeF = New System.Drawing.SizeF(780.0!, 378.1267!)
        Me.XrRichText2.StylePriority.UseBorders = False
        Me.XrRichText2.StylePriority.UseBorderWidth = False
        Me.XrRichText2.StylePriority.UsePadding = False
        '
        'label1
        '
        Me.label1.Font = New System.Drawing.Font("Calibri", 12.0!, System.Drawing.FontStyle.Bold)
        Me.label1.LocationFloat = New DevExpress.Utils.PointFloat(10.0!, 10.0!)
        Me.label1.Name = "label1"
        Me.label1.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.label1.SizeF = New System.Drawing.SizeF(780.0!, 20.0!)
        Me.label1.StylePriority.UseFont = False
        Me.label1.StylePriority.UseTextAlignment = False
        Me.label1.Text = "COLORADO DISCLOSURE"
        Me.label1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
        '
        'XrLabel1
        '
        Me.XrLabel1.Font = New System.Drawing.Font("Calibri", 12.0!, System.Drawing.FontStyle.Bold)
        Me.XrLabel1.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
        Me.XrLabel1.Name = "XrLabel1"
        Me.XrLabel1.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel1.SizeF = New System.Drawing.SizeF(750.0!, 33.0!)
        Me.XrLabel1.StylePriority.UseFont = False
        Me.XrLabel1.StylePriority.UseTextAlignment = False
        Me.XrLabel1.Text = "CREDITOR LIST - SCHEDULE A"
        Me.XrLabel1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
        '
        'GroupHeader1
        '
        Me.GroupHeader1.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrTable9})
        Me.GroupHeader1.GroupUnion = DevExpress.XtraReports.UI.GroupUnion.WithFirstDetail
        Me.GroupHeader1.HeightF = 34.0!
        Me.GroupHeader1.Name = "GroupHeader1"
        Me.GroupHeader1.RepeatEveryPage = True
        '
        'Detail
        '
        Me.Detail.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrTable10})
        Me.Detail.HeightF = 34.0!
        Me.Detail.Name = "Detail"
        Me.Detail.StylePriority.UseTextAlignment = False
        Me.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'ReportHeader
        '
        Me.ReportHeader.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel1})
        Me.ReportHeader.HeightF = 40.0!
        Me.ReportHeader.Name = "ReportHeader"
        Me.ReportHeader.PageBreak = DevExpress.XtraReports.UI.PageBreak.BeforeBand
        '
        'TopMarginBand1
        '
        Me.TopMarginBand1.HeightF = 25.0!
        Me.TopMarginBand1.Name = "TopMarginBand1"
        '
        'BottomMarginBand1
        '
        Me.BottomMarginBand1.HeightF = 25.0!
        Me.BottomMarginBand1.Name = "BottomMarginBand1"
        '
        'DMPDebtsSubReport
        '
        Me.Bands.AddRange(New DevExpress.XtraReports.UI.Band() {Me.Detail, Me.ReportHeader, Me.GroupHeader1, Me.GroupFooter1, Me.GroupFooter2, Me.TopMarginBand1, Me.BottomMarginBand1})
        Me.Margins = New System.Drawing.Printing.Margins(25, 25, 25, 25)
        Me.ScriptLanguage = DevExpress.XtraReports.ScriptLanguage.VisualBasic
        Me.ScriptReferencesString = resources.GetString("$this.ScriptReferencesString")
        Me.Scripts.OnBeforePrint = "DMPDebtsSubReport_BeforePrint"
        Me.ScriptsSource = resources.GetString("$this.ScriptsSource")
        Me.Version = "11.2"
        CType(Me.XrTable9, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.XrTable10, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.XrRichText1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.XrTable11, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.XrRichText2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me, System.ComponentModel.ISupportInitialize).EndInit()

    End Sub
    Friend WithEvents XrTable9 As DevExpress.XtraReports.UI.XRTable
    Friend WithEvents XrTableRow37 As DevExpress.XtraReports.UI.XRTableRow
    Friend WithEvents XrTableCell98 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents XrTableCell101 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents XrTableCell102 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents XrTableCell103 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents XrTableRow34 As DevExpress.XtraReports.UI.XRTableRow
    Friend WithEvents XrTableCell8 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents XrTableCell78 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents XrTableCell70 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents XrTableCell81 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents XrTableCell71 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents XrTableCell97 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents XrTableCell87 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents XrTableCell90 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents XrTableCell93 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents XrTable10 As DevExpress.XtraReports.UI.XRTable
    Friend WithEvents XrTableRow35 As DevExpress.XtraReports.UI.XRTableRow
    Friend WithEvents XrTableCell_creditor_name As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents XrTableCell_account_number As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents XrTableCel_balance As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents XrTableCell_dmp_rate As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents XrTableCell_disbursement_factor As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents XrTableCellnon_dmp_rate As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents XrTableCell_non_dmp_payment As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents XrTableCell_payout_date As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents XrTableCell_creditor As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents XrTableRow38 As DevExpress.XtraReports.UI.XRTableRow
    Friend WithEvents XrTableCell_creditor_address As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents XrTableCell107 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents GroupFooter1 As DevExpress.XtraReports.UI.GroupFooterBand
    Friend WithEvents XrPanel5 As DevExpress.XtraReports.UI.XRPanel
    Friend WithEvents XrLabel16 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLine2 As DevExpress.XtraReports.UI.XRLine
    Friend WithEvents XrLabel30 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLine1 As DevExpress.XtraReports.UI.XRLine
    Friend WithEvents XrLabel29 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrRichText1 As DevExpress.XtraReports.UI.XRRichText
    Friend WithEvents XrTable11 As DevExpress.XtraReports.UI.XRTable
    Friend WithEvents XrTableRow36 As DevExpress.XtraReports.UI.XRTableRow
    Friend WithEvents XrTableCell80 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents XrTableCell_total_balance As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents XrTableCell_total_disbursement_factor As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents XrTableCell_total_non_dmp_payment As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents XrTableCell95 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents GroupFooter2 As DevExpress.XtraReports.UI.GroupFooterBand
    Friend WithEvents XrRichText2 As DevExpress.XtraReports.UI.XRRichText
    Friend WithEvents label1 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel1 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents GroupHeader1 As DevExpress.XtraReports.UI.GroupHeaderBand
    Friend WithEvents Detail As DevExpress.XtraReports.UI.DetailBand
    Friend WithEvents ReportHeader As DevExpress.XtraReports.UI.ReportHeaderBand
    Friend WithEvents TopMarginBand1 As DevExpress.XtraReports.UI.TopMarginBand
    Friend WithEvents BottomMarginBand1 As DevExpress.XtraReports.UI.BottomMarginBand
End Class
