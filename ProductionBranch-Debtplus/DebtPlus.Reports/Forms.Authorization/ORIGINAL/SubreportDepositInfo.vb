#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}
#End Region

Namespace Forms.Authorization
    Friend Class SubreportDepositInfo
        Public Sub New()
            MyBase.New()
            InitializeComponent()
            'AddHandler XrTableCell_client_id.BeforePrint, AddressOf XrTableCell_client_id_BeforePrint
            'AddHandler XrTableCell_payment.BeforePrint, AddressOf XrTableCell_payment_BeforePrint
        End Sub

        Private Sub XrTableCell_client_id_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs)
            With CType(sender, DevExpress.XtraReports.UI.XRTableCell)
                Dim rpt As DevExpress.XtraReports.UI.XtraReport = CType(.Report, DevExpress.XtraReports.UI.XtraReport)
                Dim MasterRpt As DevExpress.XtraReports.UI.XtraReport = CType(rpt.MasterReport, DevExpress.XtraReports.UI.XtraReport)
                .Text = DebtPlus.Utils.Format.Client.FormatClientID(MasterRpt.Parameters("ParameterClient").Value)
            End With
        End Sub

        Private Sub XrTableCell_payment_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs)
            With CType(sender, DevExpress.XtraReports.UI.XRTableCell)
                Dim rpt As DevExpress.XtraReports.UI.XtraReport = CType(.Report, DevExpress.XtraReports.UI.XtraReport)
                Dim MasterRpt As DevExpress.XtraReports.UI.XtraReport = CType(rpt.MasterReport, DevExpress.XtraReports.UI.XtraReport)
                Dim MasterDataSource As System.Collections.IList = CType(MasterRpt.DataSource, System.Collections.IList)
                If MasterDataSource IsNot Nothing AndAlso MasterDataSource.Count > 0 Then
                    Dim Record As DebtPlus.Reports.Forms.Authorization.RecordDefinition = CType(MasterDataSource.Item(0), DebtPlus.Reports.Forms.Authorization.RecordDefinition)
                    .Text = String.Format("{0:c}", Record.deposit_amount)
                End If
            End With
        End Sub

    End Class
End Namespace
