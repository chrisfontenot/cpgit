Namespace Forms.Authorization
    <Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
    Partial Public Class AuthorizationReport
        Inherits DebtPlus.Reports.Template.BaseXtraReportClass

        'XtraReport overrides dispose to clean up the component list.
        <System.Diagnostics.DebuggerNonUserCode()> _
        Protected Overrides Sub Dispose(ByVal disposing As Boolean)
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
            MyBase.Dispose(disposing)
        End Sub

        'Required by the Designer
        Private components As System.ComponentModel.IContainer = Nothing

        'NOTE: The following procedure is required by the Designer
        'It can be modified using the Designer.  
        'Do not modify it using the code editor.
        <System.Diagnostics.DebuggerStepThrough()> _
        Private Sub InitializeComponent()
            Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(AuthorizationReport))
            Me.XrRichText_Pannel2 = New DevExpress.XtraReports.UI.XRRichText()
            Me.XrRichText_Pannel1 = New DevExpress.XtraReports.UI.XRRichText()
            Me.XrSubreportSchedules = New DevExpress.XtraReports.UI.XRSubreport()
            Me.XrRichText_notice_text = New DevExpress.XtraReports.UI.XRRichText()
            Me.XrRichText_debt_counseling_notice = New DevExpress.XtraReports.UI.XRRichText()
            Me.XrPictureBox1 = New DevExpress.XtraReports.UI.XRPictureBox()
            Me.PageHeader = New DevExpress.XtraReports.UI.PageHeaderBand()
            Me.XrRichText1 = New DevExpress.XtraReports.UI.XRRichText()
            Me.PageFooter = New DevExpress.XtraReports.UI.PageFooterBand()
            Me.XrPageInfo1 = New DevExpress.XtraReports.UI.XRPageInfo()
            Me.XrLabel_page_footer = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_revision = New DevExpress.XtraReports.UI.XRLabel()
            Me.ParameterClient = New DevExpress.XtraReports.Parameters.Parameter()
            Me.ParameterState = New DevExpress.XtraReports.Parameters.Parameter()
            Me.ParameterLanguage = New DevExpress.XtraReports.Parameters.Parameter()
            Me.XrSubreport_depositinfo = New DevExpress.XtraReports.UI.XRSubreport()
            Me.SubreportDepositInfo1 = New DebtPlus.Reports.Forms.Authorization.SubreportDepositInfo()
            Me.XrSubreport_Signatures = New DevExpress.XtraReports.UI.XRSubreport()
            Me.SubReportSignatureWithAddress1 = New DebtPlus.Reports.Forms.Authorization.SubReportSignatureWithAddress()
            Me.GroupHeader_Schedule = New DevExpress.XtraReports.UI.GroupHeaderBand()
            Me.GroupHeader_Pannel1 = New DevExpress.XtraReports.UI.GroupHeaderBand()
            Me.GroupHeader_Pannel2 = New DevExpress.XtraReports.UI.GroupHeaderBand()
            Me.GroupHeader_Text = New DevExpress.XtraReports.UI.GroupHeaderBand()
            Me.GroupHeader_Header = New DevExpress.XtraReports.UI.GroupHeaderBand()
            Me.GroupHeader_Pannel4 = New DevExpress.XtraReports.UI.GroupHeaderBand()
            Me.XrRichText_Pannel4 = New DevExpress.XtraReports.UI.XRRichText()
            Me.GroupHeader_Pannel3 = New DevExpress.XtraReports.UI.GroupHeaderBand()
            Me.XrRichText_Pannel3 = New DevExpress.XtraReports.UI.XRRichText()
            CType(Me.XrRichText_Pannel2, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.XrRichText_Pannel1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.XrRichText_notice_text, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.XrRichText_debt_counseling_notice, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.XrRichText1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.SubreportDepositInfo1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.SubReportSignatureWithAddress1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.XrRichText_Pannel4, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.XrRichText_Pannel3, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
            '
            'Detail
            '
            Me.Detail.HeightF = 0.0!
            '
            'XrRichText_Pannel2
            '
            Me.XrRichText_Pannel2.BorderWidth = 0
            Me.XrRichText_Pannel2.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
            Me.XrRichText_Pannel2.Name = "XrRichText_Pannel2"
            Me.XrRichText_Pannel2.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
            Me.XrRichText_Pannel2.SizeF = New System.Drawing.SizeF(783.0!, 25.0!)
            Me.XrRichText_Pannel2.StylePriority.UseBorderWidth = False
            Me.XrRichText_Pannel2.StylePriority.UsePadding = False
            '
            'XrRichText_Pannel1
            '
            Me.XrRichText_Pannel1.BorderWidth = 0
            Me.XrRichText_Pannel1.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
            Me.XrRichText_Pannel1.Name = "XrRichText_Pannel1"
            Me.XrRichText_Pannel1.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
            Me.XrRichText_Pannel1.SizeF = New System.Drawing.SizeF(783.0!, 25.0!)
            '
            'XrSubreportSchedules
            '
            Me.XrSubreportSchedules.LocationFloat = New DevExpress.Utils.PointFloat(0.00009536743!, 0.0!)
            Me.XrSubreportSchedules.Name = "XrSubreportSchedules"
            Me.XrSubreportSchedules.SizeF = New System.Drawing.SizeF(799.0!, 25.0!)
            Me.XrSubreportSchedules.Visible = False
            '
            'XrRichText_notice_text
            '
            Me.XrRichText_notice_text.BorderWidth = 0
            Me.XrRichText_notice_text.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
            Me.XrRichText_notice_text.Name = "XrRichText_notice_text"
            Me.XrRichText_notice_text.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
            Me.XrRichText_notice_text.SerializableRtfString = resources.GetString("XrRichText_notice_text.SerializableRtfString")
            Me.XrRichText_notice_text.SizeF = New System.Drawing.SizeF(783.0!, 42.70833!)
            Me.XrRichText_notice_text.StylePriority.UseBorderWidth = False
            Me.XrRichText_notice_text.StylePriority.UsePadding = False
            '
            'XrRichText_debt_counseling_notice
            '
            Me.XrRichText_debt_counseling_notice.Borders = CType((((DevExpress.XtraPrinting.BorderSide.Left Or DevExpress.XtraPrinting.BorderSide.Top) _
                Or DevExpress.XtraPrinting.BorderSide.Right) _
                Or DevExpress.XtraPrinting.BorderSide.Bottom), DevExpress.XtraPrinting.BorderSide)
            Me.XrRichText_debt_counseling_notice.BorderWidth = 2
            Me.XrRichText_debt_counseling_notice.LocationFloat = New DevExpress.Utils.PointFloat(32.99991!, 10.00001!)
            Me.XrRichText_debt_counseling_notice.Name = "XrRichText_debt_counseling_notice"
            Me.XrRichText_debt_counseling_notice.Padding = New DevExpress.XtraPrinting.PaddingInfo(20, 20, 0, 0, 100.0!)
            Me.XrRichText_debt_counseling_notice.SerializableRtfString = resources.GetString("XrRichText_debt_counseling_notice.SerializableRtfString")
            Me.XrRichText_debt_counseling_notice.SizeF = New System.Drawing.SizeF(750.0!, 177.7917!)
            Me.XrRichText_debt_counseling_notice.StylePriority.UseBorders = False
            Me.XrRichText_debt_counseling_notice.StylePriority.UseBorderWidth = False
            Me.XrRichText_debt_counseling_notice.StylePriority.UsePadding = False
            '
            'XrPictureBox1
            '
            Me.XrPictureBox1.Image = CType(resources.GetObject("XrPictureBox1.Image"), System.Drawing.Image)
            Me.XrPictureBox1.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
            Me.XrPictureBox1.Name = "XrPictureBox1"
            Me.XrPictureBox1.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 2, 2, 100.0!)
            Me.XrPictureBox1.SizeF = New System.Drawing.SizeF(198.0!, 100.0!)
            Me.XrPictureBox1.StylePriority.UsePadding = False
            '
            'PageHeader
            '
            Me.PageHeader.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrRichText1, Me.XrPictureBox1})
            Me.PageHeader.HeightF = 112.5!
            Me.PageHeader.Name = "PageHeader"
            Me.PageHeader.Padding = New DevExpress.XtraPrinting.PaddingInfo(3, 3, 3, 3, 100.0!)
            Me.PageHeader.StylePriority.UsePadding = False
            '
            'XrRichText1
            '
            Me.XrRichText1.LocationFloat = New DevExpress.Utils.PointFloat(331.25!, 0.0!)
            Me.XrRichText1.Name = "XrRichText1"
            Me.XrRichText1.Padding = New DevExpress.XtraPrinting.PaddingInfo(3, 3, 3, 3, 100.0!)
            Me.XrRichText1.SerializableRtfString = resources.GetString("XrRichText1.SerializableRtfString")
            Me.XrRichText1.SizeF = New System.Drawing.SizeF(451.7499!, 112.5!)
            Me.XrRichText1.StylePriority.UsePadding = False
            '
            'PageFooter
            '
            Me.PageFooter.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrPageInfo1, Me.XrLabel_page_footer, Me.XrLabel_revision})
            Me.PageFooter.HeightF = 55.0!
            Me.PageFooter.Name = "PageFooter"
            '
            'XrPageInfo1
            '
            Me.XrPageInfo1.Font = New System.Drawing.Font("Calibri", 12.0!)
            Me.XrPageInfo1.Format = "Page {0:f0} of {1:f0}"
            Me.XrPageInfo1.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 28.83333!)
            Me.XrPageInfo1.Name = "XrPageInfo1"
            Me.XrPageInfo1.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrPageInfo1.SizeF = New System.Drawing.SizeF(100.0!, 21.16667!)
            Me.XrPageInfo1.StylePriority.UseFont = False
            Me.XrPageInfo1.StylePriority.UseTextAlignment = False
            Me.XrPageInfo1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft
            '
            'XrLabel_page_footer
            '
            Me.XrLabel_page_footer.Font = New System.Drawing.Font("Calibri", 12.0!)
            Me.XrLabel_page_footer.LocationFloat = New DevExpress.Utils.PointFloat(100.0!, 28.83333!)
            Me.XrLabel_page_footer.Name = "XrLabel_page_footer"
            Me.XrLabel_page_footer.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_page_footer.SizeF = New System.Drawing.SizeF(533.0!, 21.16667!)
            Me.XrLabel_page_footer.StylePriority.UseFont = False
            Me.XrLabel_page_footer.StylePriority.UseTextAlignment = False
            Me.XrLabel_page_footer.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomCenter
            '
            'XrLabel_revision
            '
            Me.XrLabel_revision.Font = New System.Drawing.Font("Calibri", 12.0!)
            Me.XrLabel_revision.LocationFloat = New DevExpress.Utils.PointFloat(633.0!, 28.83333!)
            Me.XrLabel_revision.Name = "XrLabel_revision"
            Me.XrLabel_revision.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_revision.SizeF = New System.Drawing.SizeF(167.0!, 21.16667!)
            Me.XrLabel_revision.StylePriority.UseFont = False
            Me.XrLabel_revision.StylePriority.UseTextAlignment = False
            Me.XrLabel_revision.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight
            '
            'ParameterClient
            '
            Me.ParameterClient.Description = "Client ID"
            Me.ParameterClient.Name = "ParameterClient"
            Me.ParameterClient.Type = GetType(Integer)
            Me.ParameterClient.Value = -1
            Me.ParameterClient.Visible = False
            '
            'ParameterState
            '
            Me.ParameterState.Description = "State ID"
            Me.ParameterState.Name = "ParameterState"
            Me.ParameterState.Value = ""
            Me.ParameterState.Visible = False
            '
            'ParameterLanguage
            '
            Me.ParameterLanguage.Description = "Language ID"
            Me.ParameterLanguage.Name = "ParameterLanguage"
            Me.ParameterLanguage.Type = GetType(Integer)
            Me.ParameterLanguage.Value = -1
            Me.ParameterLanguage.Visible = False
            '
            'XrSubreport_depositinfo
            '
            Me.XrSubreport_depositinfo.LocationFloat = New DevExpress.Utils.PointFloat(492.0!, 59.375!)
            Me.XrSubreport_depositinfo.Name = "XrSubreport_depositinfo"
            Me.XrSubreport_depositinfo.ReportSource = Me.SubreportDepositInfo1
            Me.XrSubreport_depositinfo.SizeF = New System.Drawing.SizeF(308.0!, 25.0!)
            '
            'XrSubreport_Signatures
            '
            Me.XrSubreport_Signatures.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 59.375!)
            Me.XrSubreport_Signatures.Name = "XrSubreport_Signatures"
            Me.XrSubreport_Signatures.ReportSource = Me.SubReportSignatureWithAddress1
            Me.XrSubreport_Signatures.SizeF = New System.Drawing.SizeF(488.5833!, 25.0!)
            '
            'GroupHeader_Schedule
            '
            Me.GroupHeader_Schedule.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrSubreportSchedules})
            Me.GroupHeader_Schedule.HeightF = 25.0!
            Me.GroupHeader_Schedule.Level = 4
            Me.GroupHeader_Schedule.Name = "GroupHeader_Schedule"
            Me.GroupHeader_Schedule.PageBreak = DevExpress.XtraReports.UI.PageBreak.BeforeBand
            Me.GroupHeader_Schedule.Visible = False
            '
            'GroupHeader_Pannel1
            '
            Me.GroupHeader_Pannel1.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrRichText_Pannel1})
            Me.GroupHeader_Pannel1.HeightF = 25.0!
            Me.GroupHeader_Pannel1.Level = 3
            Me.GroupHeader_Pannel1.Name = "GroupHeader_Pannel1"
            Me.GroupHeader_Pannel1.PageBreak = DevExpress.XtraReports.UI.PageBreak.BeforeBand
            Me.GroupHeader_Pannel1.Visible = False
            '
            'GroupHeader_Pannel2
            '
            Me.GroupHeader_Pannel2.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrRichText_Pannel2})
            Me.GroupHeader_Pannel2.HeightF = 25.0!
            Me.GroupHeader_Pannel2.Level = 2
            Me.GroupHeader_Pannel2.Name = "GroupHeader_Pannel2"
            Me.GroupHeader_Pannel2.PageBreak = DevExpress.XtraReports.UI.PageBreak.BeforeBand
            Me.GroupHeader_Pannel2.Visible = False
            '
            'GroupHeader_Text
            '
            Me.GroupHeader_Text.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrSubreport_depositinfo, Me.XrRichText_notice_text, Me.XrSubreport_Signatures})
            Me.GroupHeader_Text.HeightF = 84.375!
            Me.GroupHeader_Text.Level = 5
            Me.GroupHeader_Text.Name = "GroupHeader_Text"
            '
            'GroupHeader_Header
            '
            Me.GroupHeader_Header.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrRichText_debt_counseling_notice})
            Me.GroupHeader_Header.HeightF = 202.4584!
            Me.GroupHeader_Header.Level = 6
            Me.GroupHeader_Header.Name = "GroupHeader_Header"
            Me.GroupHeader_Header.Visible = False
            '
            'GroupHeader_Pannel4
            '
            Me.GroupHeader_Pannel4.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrRichText_Pannel4})
            Me.GroupHeader_Pannel4.HeightF = 25.0!
            Me.GroupHeader_Pannel4.Name = "GroupHeader_Pannel4"
            Me.GroupHeader_Pannel4.PageBreak = DevExpress.XtraReports.UI.PageBreak.BeforeBand
            Me.GroupHeader_Pannel4.Visible = False
            '
            'XrRichText_Pannel4
            '
            Me.XrRichText_Pannel4.BorderWidth = 0
            Me.XrRichText_Pannel4.LocationFloat = New DevExpress.Utils.PointFloat(0.0001220703!, 0.0!)
            Me.XrRichText_Pannel4.Name = "XrRichText_Pannel4"
            Me.XrRichText_Pannel4.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
            Me.XrRichText_Pannel4.SizeF = New System.Drawing.SizeF(783.0!, 25.0!)
            Me.XrRichText_Pannel4.StylePriority.UseBorderWidth = False
            Me.XrRichText_Pannel4.StylePriority.UsePadding = False
            '
            'GroupHeader_Pannel3
            '
            Me.GroupHeader_Pannel3.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrRichText_Pannel3})
            Me.GroupHeader_Pannel3.HeightF = 25.0!
            Me.GroupHeader_Pannel3.Level = 1
            Me.GroupHeader_Pannel3.Name = "GroupHeader_Pannel3"
            Me.GroupHeader_Pannel3.PageBreak = DevExpress.XtraReports.UI.PageBreak.BeforeBand
            Me.GroupHeader_Pannel3.Visible = False
            '
            'XrRichText_Pannel3
            '
            Me.XrRichText_Pannel3.BorderWidth = 0
            Me.XrRichText_Pannel3.LocationFloat = New DevExpress.Utils.PointFloat(0.00009536743!, 0.0!)
            Me.XrRichText_Pannel3.Name = "XrRichText_Pannel3"
            Me.XrRichText_Pannel3.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
            Me.XrRichText_Pannel3.SizeF = New System.Drawing.SizeF(783.0!, 25.0!)
            Me.XrRichText_Pannel3.StylePriority.UseBorderWidth = False
            Me.XrRichText_Pannel3.StylePriority.UsePadding = False
            '
            'AuthorizationReport
            '
            Me.Bands.AddRange(New DevExpress.XtraReports.UI.Band() {Me.TopMarginBand1, Me.Detail, Me.BottomMarginBand1, Me.PageHeader, Me.PageFooter, Me.GroupHeader_Schedule, Me.GroupHeader_Pannel1, Me.GroupHeader_Pannel2, Me.GroupHeader_Text, Me.GroupHeader_Header, Me.GroupHeader_Pannel4, Me.GroupHeader_Pannel3})
            Me.Font = New System.Drawing.Font("Calibri", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.Parameters.AddRange(New DevExpress.XtraReports.Parameters.Parameter() {Me.ParameterClient, Me.ParameterState, Me.ParameterLanguage})
            Me.ReportPrintOptions.DetailCount = 1
            Me.ReportPrintOptions.DetailCountAtDesignTime = 1
            Me.RequestParameters = False
            Me.ScriptLanguage = DevExpress.XtraReports.ScriptLanguage.VisualBasic
            Me.ScriptReferencesString = resources.GetString("$this.ScriptReferencesString")
            Me.Scripts.OnBeforePrint = "AuthorizationReport_BeforePrint"
            Me.ScriptsSource = resources.GetString("$this.ScriptsSource")
            Me.ShowPreviewMarginLines = False
            Me.Version = "11.2"
            Me.Controls.SetChildIndex(Me.GroupHeader_Pannel3, 0)
            Me.Controls.SetChildIndex(Me.GroupHeader_Pannel4, 0)
            Me.Controls.SetChildIndex(Me.GroupHeader_Header, 0)
            Me.Controls.SetChildIndex(Me.GroupHeader_Text, 0)
            Me.Controls.SetChildIndex(Me.GroupHeader_Pannel2, 0)
            Me.Controls.SetChildIndex(Me.GroupHeader_Pannel1, 0)
            Me.Controls.SetChildIndex(Me.GroupHeader_Schedule, 0)
            Me.Controls.SetChildIndex(Me.PageFooter, 0)
            Me.Controls.SetChildIndex(Me.PageHeader, 0)
            Me.Controls.SetChildIndex(Me.BottomMarginBand1, 0)
            Me.Controls.SetChildIndex(Me.Detail, 0)
            Me.Controls.SetChildIndex(Me.TopMarginBand1, 0)
            CType(Me.XrRichText_Pannel2, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.XrRichText_Pannel1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.XrRichText_notice_text, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.XrRichText_debt_counseling_notice, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.XrRichText1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.SubreportDepositInfo1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.SubReportSignatureWithAddress1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.XrRichText_Pannel4, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.XrRichText_Pannel3, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me, System.ComponentModel.ISupportInitialize).EndInit()

        End Sub

        Friend WithEvents XrPictureBox1 As DevExpress.XtraReports.UI.XRPictureBox
        Friend WithEvents XrRichText_debt_counseling_notice As DevExpress.XtraReports.UI.XRRichText
        Friend WithEvents XrRichText_notice_text As DevExpress.XtraReports.UI.XRRichText
        Friend WithEvents PageHeader As DevExpress.XtraReports.UI.PageHeaderBand
        Friend WithEvents XrSubreport_Signatures As DevExpress.XtraReports.UI.XRSubreport
        Friend WithEvents XrSubreport_depositinfo As DevExpress.XtraReports.UI.XRSubreport
        Friend WithEvents XrSubreportSchedules As DevExpress.XtraReports.UI.XRSubreport
        Friend WithEvents PageFooter As DevExpress.XtraReports.UI.PageFooterBand
        Friend WithEvents XrLabel_revision As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_page_footer As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrRichText_Pannel2 As DevExpress.XtraReports.UI.XRRichText
        Friend WithEvents XrRichText_Pannel1 As DevExpress.XtraReports.UI.XRRichText
        Friend WithEvents ParameterClient As DevExpress.XtraReports.Parameters.Parameter
        Friend WithEvents ParameterState As DevExpress.XtraReports.Parameters.Parameter
        Friend WithEvents XrPageInfo1 As DevExpress.XtraReports.UI.XRPageInfo
        Friend WithEvents ParameterLanguage As DevExpress.XtraReports.Parameters.Parameter
        Friend WithEvents XrRichText1 As DevExpress.XtraReports.UI.XRRichText
        Friend WithEvents GroupHeader_Schedule As DevExpress.XtraReports.UI.GroupHeaderBand
        Friend WithEvents GroupHeader_Pannel1 As DevExpress.XtraReports.UI.GroupHeaderBand
        Friend WithEvents GroupHeader_Pannel2 As DevExpress.XtraReports.UI.GroupHeaderBand
        Friend WithEvents GroupHeader_Text As DevExpress.XtraReports.UI.GroupHeaderBand
        Friend WithEvents GroupHeader_Header As DevExpress.XtraReports.UI.GroupHeaderBand
        Private WithEvents SubreportDepositInfo1 As DebtPlus.Reports.Forms.Authorization.SubreportDepositInfo
        Private WithEvents SubReportSignatureWithAddress1 As DebtPlus.Reports.Forms.Authorization.SubReportSignatureWithAddress
        Friend WithEvents GroupHeader_Pannel4 As DevExpress.XtraReports.UI.GroupHeaderBand
        Friend WithEvents GroupHeader_Pannel3 As DevExpress.XtraReports.UI.GroupHeaderBand
        Friend WithEvents XrRichText_Pannel4 As DevExpress.XtraReports.UI.XRRichText
        Friend WithEvents XrRichText_Pannel3 As DevExpress.XtraReports.UI.XRRichText
    End Class
End Namespace
