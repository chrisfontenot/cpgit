﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Public Class DMPDebtsSubReport
    Inherits DevExpress.XtraReports.UI.XtraReport

    'XtraReport overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Designer
    'It can be modified using the Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim XrSummary1 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary()
        Dim XrSummary2 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary()
        Dim XrSummary3 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(DMPDebtsSubReport))
        Me.XrTableRow35 = New DevExpress.XtraReports.UI.XRTableRow()
        Me.XrTableCell_creditor_name = New DevExpress.XtraReports.UI.XRTableCell()
        Me.XrTableCell_account_number = New DevExpress.XtraReports.UI.XRTableCell()
        Me.XrTableCel_balance = New DevExpress.XtraReports.UI.XRTableCell()
        Me.XrTableCell_dmp_rate = New DevExpress.XtraReports.UI.XRTableCell()
        Me.XrTableCell_disbursement_factor = New DevExpress.XtraReports.UI.XRTableCell()
        Me.XrTableCellnon_dmp_rate = New DevExpress.XtraReports.UI.XRTableCell()
        Me.XrTableCell_non_dmp_payment = New DevExpress.XtraReports.UI.XRTableCell()
        Me.XrTableCell_payout_date = New DevExpress.XtraReports.UI.XRTableCell()
        Me.XrTableCell_creditor = New DevExpress.XtraReports.UI.XRTableCell()
        Me.XrTableCell7 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.XrTableRow20 = New DevExpress.XtraReports.UI.XRTableRow()
        Me.XrTableCell42 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.XrTableCell43 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.XrTableCell34 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.XrTableCell62 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.XrTableCell8 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.XrTableCell93 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.XrTableRow28 = New DevExpress.XtraReports.UI.XRTableRow()
        Me.XrTableCell58 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.XrTableCell59 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.XrTableRow34 = New DevExpress.XtraReports.UI.XRTableRow()
        Me.XrTableCell78 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.XrTableCell70 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.XrTableCell81 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.XrTableCell71 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.XrTableCell97 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.XrTableCell87 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.XrTableCell90 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.XrTableCell52 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.XrTable9 = New DevExpress.XtraReports.UI.XRTable()
        Me.XrTableRow37 = New DevExpress.XtraReports.UI.XRTableRow()
        Me.XrTableCell98 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.XrTableCell101 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.XrTableCell102 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.XrTableCell103 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.XrLabel14 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrTableCell66 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.XrPanel1 = New DevExpress.XtraReports.UI.XRPanel()
        Me.XrTable5 = New DevExpress.XtraReports.UI.XRTable()
        Me.XrTableRow10 = New DevExpress.XtraReports.UI.XRTableRow()
        Me.XrTableCell22 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.XrTableCell24 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.XrTableRow11 = New DevExpress.XtraReports.UI.XRTableRow()
        Me.XrTableCell23 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.XrTableCell25 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.XrTableRow12 = New DevExpress.XtraReports.UI.XRTableRow()
        Me.XrTableCell26 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.XrTableCell27 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.XrTableRow13 = New DevExpress.XtraReports.UI.XRTableRow()
        Me.XrTableCell28 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.XrTableCell29 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.XrTableRow14 = New DevExpress.XtraReports.UI.XRTableRow()
        Me.XrTableCell30 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.XrTableCell31 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.XrTableRow15 = New DevExpress.XtraReports.UI.XRTableRow()
        Me.XrTableCell32 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.XrTableCell33 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.XrLabel12 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrTableRow16 = New DevExpress.XtraReports.UI.XRTableRow()
        Me.XrTableCell35 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.XrTableCell20 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.XrTableCell65 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.XrTableRow36 = New DevExpress.XtraReports.UI.XRTableRow()
        Me.XrTableCell80 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.XrTableCell_total_balance = New DevExpress.XtraReports.UI.XRTableCell()
        Me.XrTableCell_total_disbursement_factor = New DevExpress.XtraReports.UI.XRTableCell()
        Me.XrTableCell_total_non_dmp_payment = New DevExpress.XtraReports.UI.XRTableCell()
        Me.XrTableCell95 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.XrTable4 = New DevExpress.XtraReports.UI.XRTable()
        Me.XrTableRow5 = New DevExpress.XtraReports.UI.XRTableRow()
        Me.XrTableCell5 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.XrTableCell_marital_status = New DevExpress.XtraReports.UI.XRTableCell()
        Me.XrTableRow6 = New DevExpress.XtraReports.UI.XRTableRow()
        Me.XrTableCell9 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.XrTableRow7 = New DevExpress.XtraReports.UI.XRTableRow()
        Me.XrTableCell13 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.XrTableCell17 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.XrTableRow8 = New DevExpress.XtraReports.UI.XRTableRow()
        Me.XrTableCell18 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.XrTableCell19 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.XrTableRow9 = New DevExpress.XtraReports.UI.XRTableRow()
        Me.XrTableCell21 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.XrLabel29 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrTableCell56 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.XrLabel30 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrTableRow26 = New DevExpress.XtraReports.UI.XRTableRow()
        Me.XrTableCell54 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.XrTableCell55 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.XrTableCell61 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.XrTable11 = New DevExpress.XtraReports.UI.XRTable()
        Me.XrTableRow24 = New DevExpress.XtraReports.UI.XRTableRow()
        Me.XrTableCell50 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.XrTableCell51 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.XrTableCell49 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.XrTableCell45 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.GroupHeader1 = New DevExpress.XtraReports.UI.GroupHeaderBand()
        Me.XrTableCell69 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.XrTableCell40 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.XrTable7 = New DevExpress.XtraReports.UI.XRTable()
        Me.XrTableRow22 = New DevExpress.XtraReports.UI.XRTableRow()
        Me.XrTableCell46 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.XrTableCell47 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.XrTableRow23 = New DevExpress.XtraReports.UI.XRTableRow()
        Me.XrTableCell48 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.XrTableRow25 = New DevExpress.XtraReports.UI.XRTableRow()
        Me.XrTableCell53 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.XrTableRow27 = New DevExpress.XtraReports.UI.XRTableRow()
        Me.XrTableCell57 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.XrLabel13 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrPanel3 = New DevExpress.XtraReports.UI.XRPanel()
        Me.XrTableRow29 = New DevExpress.XtraReports.UI.XRTableRow()
        Me.XrTableCell60 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.XrTableCell63 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.XrTableCell64 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.XrTableRow33 = New DevExpress.XtraReports.UI.XRTableRow()
        Me.XrTableCell68 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.XrPanel2 = New DevExpress.XtraReports.UI.XRPanel()
        Me.XrTable6 = New DevExpress.XtraReports.UI.XRTable()
        Me.XrTableRow17 = New DevExpress.XtraReports.UI.XRTableRow()
        Me.XrTableCell36 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.XrTableCell37 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.XrTableRow18 = New DevExpress.XtraReports.UI.XRTableRow()
        Me.XrTableCell38 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.XrTableCell39 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.XrTableRow19 = New DevExpress.XtraReports.UI.XRTableRow()
        Me.XrTableCell41 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.XrTableRow21 = New DevExpress.XtraReports.UI.XRTableRow()
        Me.XrTableCell44 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.XrTable8 = New DevExpress.XtraReports.UI.XRTable()
        Me.XrTableRow30 = New DevExpress.XtraReports.UI.XRTableRow()
        Me.XrTableRow31 = New DevExpress.XtraReports.UI.XRTableRow()
        Me.XrTableRow32 = New DevExpress.XtraReports.UI.XRTableRow()
        Me.XrTableCell67 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.XrPanel5 = New DevExpress.XtraReports.UI.XRPanel()
        Me.XrLabel16 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLine2 = New DevExpress.XtraReports.UI.XRLine()
        Me.XrLine1 = New DevExpress.XtraReports.UI.XRLine()
        Me.XrTableRow38 = New DevExpress.XtraReports.UI.XRTableRow()
        Me.XrTableCell_creditor_address = New DevExpress.XtraReports.UI.XRTableCell()
        Me.XrTableCell107 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.GroupFooter1 = New DevExpress.XtraReports.UI.GroupFooterBand()
        Me.XrRichText1 = New DevExpress.XtraReports.UI.XRRichText()
        Me.XrLabel_Name = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel11 = New DevExpress.XtraReports.UI.XRLabel()
        Me.ReportFooter = New DevExpress.XtraReports.UI.ReportFooterBand()
        Me.XrPanel4 = New DevExpress.XtraReports.UI.XRPanel()
        Me.XrLabel15 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrTable10 = New DevExpress.XtraReports.UI.XRTable()
        Me.ReportHeader = New DevExpress.XtraReports.UI.ReportHeaderBand()
        Me.XrLabel1 = New DevExpress.XtraReports.UI.XRLabel()
        Me.Detail = New DevExpress.XtraReports.UI.DetailBand()
        Me.TopMarginBand1 = New DevExpress.XtraReports.UI.TopMarginBand()
        Me.BottomMarginBand1 = New DevExpress.XtraReports.UI.BottomMarginBand()
        CType(Me.XrTable9, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.XrTable5, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.XrTable4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.XrTable11, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.XrTable7, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.XrTable6, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.XrTable8, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.XrRichText1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.XrTable10, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
        '
        'XrTableRow35
        '
        Me.XrTableRow35.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.XrTableCell_creditor_name, Me.XrTableCell_account_number, Me.XrTableCel_balance, Me.XrTableCell_dmp_rate, Me.XrTableCell_disbursement_factor, Me.XrTableCellnon_dmp_rate, Me.XrTableCell_non_dmp_payment, Me.XrTableCell_payout_date, Me.XrTableCell_creditor})
        Me.XrTableRow35.Name = "XrTableRow35"
        Me.XrTableRow35.Weight = 1.0R
        '
        'XrTableCell_creditor_name
        '
        Me.XrTableCell_creditor_name.CanShrink = True
        Me.XrTableCell_creditor_name.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "creditor_name")})
        Me.XrTableCell_creditor_name.Name = "XrTableCell_creditor_name"
        Me.XrTableCell_creditor_name.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
        Me.XrTableCell_creditor_name.StylePriority.UsePadding = False
        Me.XrTableCell_creditor_name.StylePriority.UseTextAlignment = False
        Me.XrTableCell_creditor_name.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        Me.XrTableCell_creditor_name.Weight = 0.8944723918098616R
        Me.XrTableCell_creditor_name.WordWrap = False
        '
        'XrTableCell_account_number
        '
        Me.XrTableCell_account_number.CanGrow = False
        Me.XrTableCell_account_number.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "account_number")})
        Me.XrTableCell_account_number.Name = "XrTableCell_account_number"
        Me.XrTableCell_account_number.StylePriority.UseTextAlignment = False
        Me.XrTableCell_account_number.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
        Me.XrTableCell_account_number.Weight = 0.28140698063831676R
        Me.XrTableCell_account_number.WordWrap = False
        '
        'XrTableCel_balance
        '
        Me.XrTableCel_balance.CanGrow = False
        Me.XrTableCel_balance.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "balance", "{0:c}")})
        Me.XrTableCel_balance.Name = "XrTableCel_balance"
        Me.XrTableCel_balance.StylePriority.UseTextAlignment = False
        Me.XrTableCel_balance.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
        Me.XrTableCel_balance.Weight = 0.32412062755182158R
        Me.XrTableCel_balance.WordWrap = False
        '
        'XrTableCell_dmp_rate
        '
        Me.XrTableCell_dmp_rate.CanGrow = False
        Me.XrTableCell_dmp_rate.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "dmp_interest", "{0:p}")})
        Me.XrTableCell_dmp_rate.Name = "XrTableCell_dmp_rate"
        Me.XrTableCell_dmp_rate.StylePriority.UseTextAlignment = False
        Me.XrTableCell_dmp_rate.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
        Me.XrTableCell_dmp_rate.Weight = 0.28371872791692843R
        Me.XrTableCell_dmp_rate.WordWrap = False
        '
        'XrTableCell_disbursement_factor
        '
        Me.XrTableCell_disbursement_factor.CanGrow = False
        Me.XrTableCell_disbursement_factor.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "dmp_payment", "{0:c}")})
        Me.XrTableCell_disbursement_factor.Name = "XrTableCell_disbursement_factor"
        Me.XrTableCell_disbursement_factor.StylePriority.UseTextAlignment = False
        Me.XrTableCell_disbursement_factor.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
        Me.XrTableCell_disbursement_factor.Weight = 0.32804005991873431R
        Me.XrTableCell_disbursement_factor.WordWrap = False
        '
        'XrTableCellnon_dmp_rate
        '
        Me.XrTableCellnon_dmp_rate.CanGrow = False
        Me.XrTableCellnon_dmp_rate.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "non_dmp_interest", "{0:p}")})
        Me.XrTableCellnon_dmp_rate.Name = "XrTableCellnon_dmp_rate"
        Me.XrTableCellnon_dmp_rate.StylePriority.UseTextAlignment = False
        Me.XrTableCellnon_dmp_rate.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
        Me.XrTableCellnon_dmp_rate.Weight = 0.28944738646847523R
        Me.XrTableCellnon_dmp_rate.WordWrap = False
        '
        'XrTableCell_non_dmp_payment
        '
        Me.XrTableCell_non_dmp_payment.CanGrow = False
        Me.XrTableCell_non_dmp_payment.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "non_dmp_payment", "{0:c}")})
        Me.XrTableCell_non_dmp_payment.Name = "XrTableCell_non_dmp_payment"
        Me.XrTableCell_non_dmp_payment.StylePriority.UseTextAlignment = False
        Me.XrTableCell_non_dmp_payment.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
        Me.XrTableCell_non_dmp_payment.Weight = 0.3232161551624087R
        Me.XrTableCell_non_dmp_payment.WordWrap = False
        '
        'XrTableCell_payout_date
        '
        Me.XrTableCell_payout_date.CanGrow = False
        Me.XrTableCell_payout_date.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "dmp_payout", "{0:M/yy}")})
        Me.XrTableCell_payout_date.Name = "XrTableCell_payout_date"
        Me.XrTableCell_payout_date.StylePriority.UseTextAlignment = False
        Me.XrTableCell_payout_date.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
        Me.XrTableCell_payout_date.Weight = 0.3232161551624087R
        Me.XrTableCell_payout_date.WordWrap = False
        '
        'XrTableCell_creditor
        '
        Me.XrTableCell_creditor.CanGrow = False
        Me.XrTableCell_creditor.Name = "XrTableCell_creditor"
        Me.XrTableCell_creditor.Scripts.OnBeforePrint = "XrTableCell_creditor_BeforePrint"
        Me.XrTableCell_creditor.StylePriority.UseTextAlignment = False
        Me.XrTableCell_creditor.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        Me.XrTableCell_creditor.Weight = 0.5403009194225521R
        Me.XrTableCell_creditor.WordWrap = False
        '
        'XrTableCell7
        '
        Me.XrTableCell7.Name = "XrTableCell7"
        Me.XrTableCell7.Text = "Number of Dependents:"
        Me.XrTableCell7.Weight = 2.2934839662216904R
        '
        'XrTableRow20
        '
        Me.XrTableRow20.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.XrTableCell42, Me.XrTableCell43})
        Me.XrTableRow20.Name = "XrTableRow20"
        Me.XrTableRow20.Weight = 1.0R
        '
        'XrTableCell42
        '
        Me.XrTableCell42.Name = "XrTableCell42"
        Me.XrTableCell42.Weight = 5.4104168701171877R
        '
        'XrTableCell43
        '
        Me.XrTableCell43.Name = "XrTableCell43"
        Me.XrTableCell43.Weight = 1.5791662597656253R
        '
        'XrTableCell34
        '
        Me.XrTableCell34.Name = "XrTableCell34"
        Me.XrTableCell34.StylePriority.UseTextAlignment = False
        Me.XrTableCell34.Text = "Description"
        Me.XrTableCell34.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
        Me.XrTableCell34.Weight = 5.4104162597656247R
        '
        'XrTableCell62
        '
        Me.XrTableCell62.Name = "XrTableCell62"
        Me.XrTableCell62.Weight = 5.4104168701171877R
        '
        'XrTableCell8
        '
        Me.XrTableCell8.Borders = CType(((DevExpress.XtraPrinting.BorderSide.Left Or DevExpress.XtraPrinting.BorderSide.Right) _
            Or DevExpress.XtraPrinting.BorderSide.Bottom), DevExpress.XtraPrinting.BorderSide)
        Me.XrTableCell8.CanGrow = False
        Me.XrTableCell8.Name = "XrTableCell8"
        Me.XrTableCell8.StylePriority.UseBorders = False
        Me.XrTableCell8.StylePriority.UseTextAlignment = False
        Me.XrTableCell8.Text = "Name"
        Me.XrTableCell8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        Me.XrTableCell8.Weight = 0.8944722827347481R
        '
        'XrTableCell93
        '
        Me.XrTableCell93.Borders = CType(((DevExpress.XtraPrinting.BorderSide.Left Or DevExpress.XtraPrinting.BorderSide.Right) _
            Or DevExpress.XtraPrinting.BorderSide.Bottom), DevExpress.XtraPrinting.BorderSide)
        Me.XrTableCell93.CanGrow = False
        Me.XrTableCell93.Name = "XrTableCell93"
        Me.XrTableCell93.StylePriority.UseBorders = False
        Me.XrTableCell93.StylePriority.UseTextAlignment = False
        Me.XrTableCell93.Text = "Creditor ID"
        Me.XrTableCell93.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        Me.XrTableCell93.Weight = 0.5403018027454165R
        '
        'XrTableRow28
        '
        Me.XrTableRow28.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.XrTableCell58, Me.XrTableCell59})
        Me.XrTableRow28.Font = New System.Drawing.Font("Calibri", 12.0!, System.Drawing.FontStyle.Bold)
        Me.XrTableRow28.Name = "XrTableRow28"
        Me.XrTableRow28.StylePriority.UseFont = False
        Me.XrTableRow28.Weight = 1.0R
        '
        'XrTableCell58
        '
        Me.XrTableCell58.Name = "XrTableCell58"
        Me.XrTableCell58.StylePriority.UseTextAlignment = False
        Me.XrTableCell58.Text = "Description"
        Me.XrTableCell58.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
        Me.XrTableCell58.Weight = 5.4104168701171877R
        '
        'XrTableCell59
        '
        Me.XrTableCell59.Name = "XrTableCell59"
        Me.XrTableCell59.StylePriority.UseTextAlignment = False
        Me.XrTableCell59.Text = "Amount Per Pay Period"
        Me.XrTableCell59.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter
        Me.XrTableCell59.Weight = 1.5791662597656253R
        '
        'XrTableRow34
        '
        Me.XrTableRow34.Borders = CType((((DevExpress.XtraPrinting.BorderSide.Left Or DevExpress.XtraPrinting.BorderSide.Top) _
            Or DevExpress.XtraPrinting.BorderSide.Right) _
            Or DevExpress.XtraPrinting.BorderSide.Bottom), DevExpress.XtraPrinting.BorderSide)
        Me.XrTableRow34.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.XrTableCell8, Me.XrTableCell78, Me.XrTableCell70, Me.XrTableCell81, Me.XrTableCell71, Me.XrTableCell97, Me.XrTableCell87, Me.XrTableCell90, Me.XrTableCell93})
        Me.XrTableRow34.Font = New System.Drawing.Font("Calibri", 12.0!, System.Drawing.FontStyle.Bold)
        Me.XrTableRow34.Name = "XrTableRow34"
        Me.XrTableRow34.StylePriority.UseBorders = False
        Me.XrTableRow34.StylePriority.UseFont = False
        Me.XrTableRow34.StylePriority.UseTextAlignment = False
        Me.XrTableRow34.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
        Me.XrTableRow34.Weight = 1.0R
        '
        'XrTableCell78
        '
        Me.XrTableCell78.Borders = CType(((DevExpress.XtraPrinting.BorderSide.Left Or DevExpress.XtraPrinting.BorderSide.Right) _
            Or DevExpress.XtraPrinting.BorderSide.Bottom), DevExpress.XtraPrinting.BorderSide)
        Me.XrTableCell78.CanGrow = False
        Me.XrTableCell78.Name = "XrTableCell78"
        Me.XrTableCell78.StylePriority.UseBorders = False
        Me.XrTableCell78.StylePriority.UseTextAlignment = False
        Me.XrTableCell78.Text = "ACCT #"
        Me.XrTableCell78.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
        Me.XrTableCell78.Weight = 0.2814071265185496R
        '
        'XrTableCell70
        '
        Me.XrTableCell70.Borders = CType(((DevExpress.XtraPrinting.BorderSide.Left Or DevExpress.XtraPrinting.BorderSide.Right) _
            Or DevExpress.XtraPrinting.BorderSide.Bottom), DevExpress.XtraPrinting.BorderSide)
        Me.XrTableCell70.CanGrow = False
        Me.XrTableCell70.Name = "XrTableCell70"
        Me.XrTableCell70.StylePriority.UseBorders = False
        Me.XrTableCell70.Text = "Balance"
        Me.XrTableCell70.Weight = 0.32412059074670224R
        '
        'XrTableCell81
        '
        Me.XrTableCell81.CanGrow = False
        Me.XrTableCell81.Name = "XrTableCell81"
        Me.XrTableCell81.Text = "Rate"
        Me.XrTableCell81.Weight = 0.28371872791692843R
        '
        'XrTableCell71
        '
        Me.XrTableCell71.CanGrow = False
        Me.XrTableCell71.Name = "XrTableCell71"
        Me.XrTableCell71.Text = "Payment"
        Me.XrTableCell71.Weight = 0.32804005991873442R
        '
        'XrTableCell97
        '
        Me.XrTableCell97.CanGrow = False
        Me.XrTableCell97.Name = "XrTableCell97"
        Me.XrTableCell97.Text = "Rate"
        Me.XrTableCell97.Weight = 0.28944709202752039R
        '
        'XrTableCell87
        '
        Me.XrTableCell87.CanGrow = False
        Me.XrTableCell87.Name = "XrTableCell87"
        Me.XrTableCell87.Text = "Payment"
        Me.XrTableCell87.Weight = 0.32804034975904922R
        '
        'XrTableCell90
        '
        Me.XrTableCell90.Borders = CType(((DevExpress.XtraPrinting.BorderSide.Left Or DevExpress.XtraPrinting.BorderSide.Right) _
            Or DevExpress.XtraPrinting.BorderSide.Bottom), DevExpress.XtraPrinting.BorderSide)
        Me.XrTableCell90.CanGrow = False
        Me.XrTableCell90.Name = "XrTableCell90"
        Me.XrTableCell90.StylePriority.UseBorders = False
        Me.XrTableCell90.Text = "Payout"
        Me.XrTableCell90.Weight = 0.31839196056576835R
        '
        'XrTableCell52
        '
        Me.XrTableCell52.Name = "XrTableCell52"
        Me.XrTableCell52.Weight = 5.4104162597656247R
        '
        'XrTable9
        '
        Me.XrTable9.AnchorVertical = DevExpress.XtraReports.UI.VerticalAnchorStyles.Bottom
        Me.XrTable9.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
        Me.XrTable9.Name = "XrTable9"
        Me.XrTable9.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 2, 2, 100.0!)
        Me.XrTable9.Rows.AddRange(New DevExpress.XtraReports.UI.XRTableRow() {Me.XrTableRow37, Me.XrTableRow34})
        Me.XrTable9.SizeF = New System.Drawing.SizeF(743.7501!, 34.0!)
        Me.XrTable9.StylePriority.UsePadding = False
        '
        'XrTableRow37
        '
        Me.XrTableRow37.Borders = CType((((DevExpress.XtraPrinting.BorderSide.Left Or DevExpress.XtraPrinting.BorderSide.Top) _
            Or DevExpress.XtraPrinting.BorderSide.Right) _
            Or DevExpress.XtraPrinting.BorderSide.Bottom), DevExpress.XtraPrinting.BorderSide)
        Me.XrTableRow37.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.XrTableCell98, Me.XrTableCell101, Me.XrTableCell102, Me.XrTableCell103})
        Me.XrTableRow37.Font = New System.Drawing.Font("Calibri", 12.0!, System.Drawing.FontStyle.Bold)
        Me.XrTableRow37.Name = "XrTableRow37"
        Me.XrTableRow37.StylePriority.UseBorders = False
        Me.XrTableRow37.StylePriority.UseFont = False
        Me.XrTableRow37.StylePriority.UseTextAlignment = False
        Me.XrTableRow37.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
        Me.XrTableRow37.Weight = 1.0R
        '
        'XrTableCell98
        '
        Me.XrTableCell98.Borders = DevExpress.XtraPrinting.BorderSide.Bottom
        Me.XrTableCell98.CanGrow = False
        Me.XrTableCell98.Name = "XrTableCell98"
        Me.XrTableCell98.StylePriority.UseBorders = False
        Me.XrTableCell98.Weight = 1.5R
        '
        'XrTableCell101
        '
        Me.XrTableCell101.BackColor = System.Drawing.Color.Black
        Me.XrTableCell101.BorderColor = System.Drawing.Color.White
        Me.XrTableCell101.Borders = CType((DevExpress.XtraPrinting.BorderSide.Left Or DevExpress.XtraPrinting.BorderSide.Right), DevExpress.XtraPrinting.BorderSide)
        Me.XrTableCell101.BorderWidth = 5
        Me.XrTableCell101.CanGrow = False
        Me.XrTableCell101.ForeColor = System.Drawing.Color.White
        Me.XrTableCell101.Name = "XrTableCell101"
        Me.XrTableCell101.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 2, 2, 100.0!)
        Me.XrTableCell101.StylePriority.UseBackColor = False
        Me.XrTableCell101.StylePriority.UseBorderColor = False
        Me.XrTableCell101.StylePriority.UseBorders = False
        Me.XrTableCell101.StylePriority.UseBorderWidth = False
        Me.XrTableCell101.StylePriority.UseForeColor = False
        Me.XrTableCell101.StylePriority.UsePadding = False
        Me.XrTableCell101.Text = "PLAN"
        Me.XrTableCell101.Weight = 0.61175878630211611R
        '
        'XrTableCell102
        '
        Me.XrTableCell102.BackColor = System.Drawing.Color.Black
        Me.XrTableCell102.BorderColor = System.Drawing.Color.White
        Me.XrTableCell102.Borders = CType((DevExpress.XtraPrinting.BorderSide.Left Or DevExpress.XtraPrinting.BorderSide.Right), DevExpress.XtraPrinting.BorderSide)
        Me.XrTableCell102.BorderWidth = 5
        Me.XrTableCell102.CanGrow = False
        Me.XrTableCell102.ForeColor = System.Drawing.Color.White
        Me.XrTableCell102.Name = "XrTableCell102"
        Me.XrTableCell102.StylePriority.UseBackColor = False
        Me.XrTableCell102.StylePriority.UseBorderColor = False
        Me.XrTableCell102.StylePriority.UseBorders = False
        Me.XrTableCell102.StylePriority.UseBorderWidth = False
        Me.XrTableCell102.StylePriority.UseForeColor = False
        Me.XrTableCell102.Text = "SELF"
        Me.XrTableCell102.Weight = 0.61748744332011629R
        '
        'XrTableCell103
        '
        Me.XrTableCell103.Borders = DevExpress.XtraPrinting.BorderSide.Bottom
        Me.XrTableCell103.CanGrow = False
        Me.XrTableCell103.Name = "XrTableCell103"
        Me.XrTableCell103.StylePriority.UseBorders = False
        Me.XrTableCell103.Weight = 0.85869376331118485R
        '
        'XrLabel14
        '
        Me.XrLabel14.Font = New System.Drawing.Font("Calibri", 12.0!, System.Drawing.FontStyle.Bold)
        Me.XrLabel14.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
        Me.XrLabel14.Name = "XrLabel14"
        Me.XrLabel14.Padding = New DevExpress.XtraPrinting.PaddingInfo(5, 5, 5, 5, 100.0!)
        Me.XrLabel14.SizeF = New System.Drawing.SizeF(698.9583!, 23.00002!)
        Me.XrLabel14.StylePriority.UseFont = False
        Me.XrLabel14.StylePriority.UsePadding = False
        Me.XrLabel14.Text = "Judgments"
        '
        'XrTableCell66
        '
        Me.XrTableCell66.Name = "XrTableCell66"
        Me.XrTableCell66.Weight = 5.4104162597656247R
        '
        'XrPanel1
        '
        Me.XrPanel1.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrTable5, Me.XrLabel12})
        Me.XrPanel1.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 218.75!)
        Me.XrPanel1.Name = "XrPanel1"
        Me.XrPanel1.SizeF = New System.Drawing.SizeF(714.5833!, 194.7917!)
        '
        'XrTable5
        '
        Me.XrTable5.Borders = CType((((DevExpress.XtraPrinting.BorderSide.Left Or DevExpress.XtraPrinting.BorderSide.Top) _
            Or DevExpress.XtraPrinting.BorderSide.Right) _
            Or DevExpress.XtraPrinting.BorderSide.Bottom), DevExpress.XtraPrinting.BorderSide)
        Me.XrTable5.BorderWidth = 2
        Me.XrTable5.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 35.50002!)
        Me.XrTable5.Name = "XrTable5"
        Me.XrTable5.Rows.AddRange(New DevExpress.XtraReports.UI.XRTableRow() {Me.XrTableRow10, Me.XrTableRow11, Me.XrTableRow12, Me.XrTableRow13, Me.XrTableRow14, Me.XrTableRow15})
        Me.XrTable5.SizeF = New System.Drawing.SizeF(698.9583!, 150.0!)
        Me.XrTable5.StylePriority.UseBorders = False
        Me.XrTable5.StylePriority.UseBorderWidth = False
        '
        'XrTableRow10
        '
        Me.XrTableRow10.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.XrTableCell22, Me.XrTableCell24})
        Me.XrTableRow10.Font = New System.Drawing.Font("Calibri", 12.0!, System.Drawing.FontStyle.Bold)
        Me.XrTableRow10.Name = "XrTableRow10"
        Me.XrTableRow10.StylePriority.UseFont = False
        Me.XrTableRow10.Weight = 1.0R
        '
        'XrTableCell22
        '
        Me.XrTableCell22.Name = "XrTableCell22"
        Me.XrTableCell22.StylePriority.UseTextAlignment = False
        Me.XrTableCell22.Text = "Description"
        Me.XrTableCell22.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
        Me.XrTableCell22.Weight = 5.4104162597656247R
        '
        'XrTableCell24
        '
        Me.XrTableCell24.Name = "XrTableCell24"
        Me.XrTableCell24.StylePriority.UseTextAlignment = False
        Me.XrTableCell24.Text = "Amount Per Pay Period"
        Me.XrTableCell24.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter
        Me.XrTableCell24.Weight = 1.5791668701171877R
        '
        'XrTableRow11
        '
        Me.XrTableRow11.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.XrTableCell23, Me.XrTableCell25})
        Me.XrTableRow11.Name = "XrTableRow11"
        Me.XrTableRow11.Weight = 1.0R
        '
        'XrTableCell23
        '
        Me.XrTableCell23.Name = "XrTableCell23"
        Me.XrTableCell23.Weight = 5.4104162597656247R
        '
        'XrTableCell25
        '
        Me.XrTableCell25.Name = "XrTableCell25"
        Me.XrTableCell25.Weight = 1.5791668701171877R
        '
        'XrTableRow12
        '
        Me.XrTableRow12.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.XrTableCell26, Me.XrTableCell27})
        Me.XrTableRow12.Name = "XrTableRow12"
        Me.XrTableRow12.Weight = 1.0R
        '
        'XrTableCell26
        '
        Me.XrTableCell26.Name = "XrTableCell26"
        Me.XrTableCell26.Weight = 5.4104162597656247R
        '
        'XrTableCell27
        '
        Me.XrTableCell27.Name = "XrTableCell27"
        Me.XrTableCell27.Weight = 1.5791668701171877R
        '
        'XrTableRow13
        '
        Me.XrTableRow13.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.XrTableCell28, Me.XrTableCell29})
        Me.XrTableRow13.Name = "XrTableRow13"
        Me.XrTableRow13.Weight = 1.0R
        '
        'XrTableCell28
        '
        Me.XrTableCell28.Name = "XrTableCell28"
        Me.XrTableCell28.Weight = 5.4104162597656247R
        '
        'XrTableCell29
        '
        Me.XrTableCell29.Name = "XrTableCell29"
        Me.XrTableCell29.Weight = 1.5791668701171877R
        '
        'XrTableRow14
        '
        Me.XrTableRow14.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.XrTableCell30, Me.XrTableCell31})
        Me.XrTableRow14.Name = "XrTableRow14"
        Me.XrTableRow14.Weight = 1.0R
        '
        'XrTableCell30
        '
        Me.XrTableCell30.Name = "XrTableCell30"
        Me.XrTableCell30.Weight = 5.4104162597656247R
        '
        'XrTableCell31
        '
        Me.XrTableCell31.Name = "XrTableCell31"
        Me.XrTableCell31.Weight = 1.5791668701171877R
        '
        'XrTableRow15
        '
        Me.XrTableRow15.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.XrTableCell32, Me.XrTableCell33})
        Me.XrTableRow15.Name = "XrTableRow15"
        Me.XrTableRow15.Weight = 1.0R
        '
        'XrTableCell32
        '
        Me.XrTableCell32.Name = "XrTableCell32"
        Me.XrTableCell32.Weight = 5.4104162597656247R
        '
        'XrTableCell33
        '
        Me.XrTableCell33.Name = "XrTableCell33"
        Me.XrTableCell33.Weight = 1.5791668701171877R
        '
        'XrLabel12
        '
        Me.XrLabel12.Font = New System.Drawing.Font("Calibri", 12.0!, System.Drawing.FontStyle.Bold)
        Me.XrLabel12.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
        Me.XrLabel12.Name = "XrLabel12"
        Me.XrLabel12.Padding = New DevExpress.XtraPrinting.PaddingInfo(5, 5, 5, 5, 100.0!)
        Me.XrLabel12.SizeF = New System.Drawing.SizeF(698.9583!, 23.00002!)
        Me.XrLabel12.StylePriority.UseFont = False
        Me.XrLabel12.StylePriority.UsePadding = False
        Me.XrLabel12.Text = "Other Income"
        '
        'XrTableRow16
        '
        Me.XrTableRow16.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.XrTableCell34, Me.XrTableCell35})
        Me.XrTableRow16.Font = New System.Drawing.Font("Calibri", 12.0!, System.Drawing.FontStyle.Bold)
        Me.XrTableRow16.Name = "XrTableRow16"
        Me.XrTableRow16.StylePriority.UseFont = False
        Me.XrTableRow16.Weight = 1.0R
        '
        'XrTableCell35
        '
        Me.XrTableCell35.Name = "XrTableCell35"
        Me.XrTableCell35.StylePriority.UseTextAlignment = False
        Me.XrTableCell35.Text = "Amount Per Pay Period"
        Me.XrTableCell35.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter
        Me.XrTableCell35.Weight = 1.5791668701171877R
        '
        'XrTableCell20
        '
        Me.XrTableCell20.Name = "XrTableCell20"
        Me.XrTableCell20.Text = "Net Income per Pay Period:"
        Me.XrTableCell20.Weight = 2.2934839662216904R
        '
        'XrTableCell65
        '
        Me.XrTableCell65.Name = "XrTableCell65"
        Me.XrTableCell65.Weight = 1.5791662597656253R
        '
        'XrTableRow36
        '
        Me.XrTableRow36.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.XrTableCell80, Me.XrTableCell_total_balance, Me.XrTableCell_total_disbursement_factor, Me.XrTableCell_total_non_dmp_payment, Me.XrTableCell95})
        Me.XrTableRow36.Font = New System.Drawing.Font("Calibri", 12.0!, System.Drawing.FontStyle.Bold)
        Me.XrTableRow36.Name = "XrTableRow36"
        Me.XrTableRow36.StylePriority.UseFont = False
        Me.XrTableRow36.StylePriority.UseTextAlignment = False
        Me.XrTableRow36.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
        Me.XrTableRow36.Weight = 0.47999998276654476R
        '
        'XrTableCell80
        '
        Me.XrTableCell80.Name = "XrTableCell80"
        Me.XrTableCell80.StylePriority.UseTextAlignment = False
        Me.XrTableCell80.Text = "TOTALS"
        Me.XrTableCell80.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        Me.XrTableCell80.Weight = 0.8944723986141645R
        '
        'XrTableCell_total_balance
        '
        Me.XrTableCell_total_balance.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "balance")})
        Me.XrTableCell_total_balance.Name = "XrTableCell_total_balance"
        XrSummary1.FormatString = "{0:c}"
        XrSummary1.Running = DevExpress.XtraReports.UI.SummaryRunning.Group
        Me.XrTableCell_total_balance.Summary = XrSummary1
        Me.XrTableCell_total_balance.Weight = 0.60552760138583528R
        Me.XrTableCell_total_balance.WordWrap = False
        '
        'XrTableCell_total_disbursement_factor
        '
        Me.XrTableCell_total_disbursement_factor.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "dmp_payment")})
        Me.XrTableCell_total_disbursement_factor.Name = "XrTableCell_total_disbursement_factor"
        XrSummary2.FormatString = "{0:c}"
        XrSummary2.Running = DevExpress.XtraReports.UI.SummaryRunning.Group
        Me.XrTableCell_total_disbursement_factor.Summary = XrSummary2
        Me.XrTableCell_total_disbursement_factor.Weight = 0.61175864061518537R
        Me.XrTableCell_total_disbursement_factor.WordWrap = False
        '
        'XrTableCell_total_non_dmp_payment
        '
        Me.XrTableCell_total_non_dmp_payment.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "non_dmp_payment")})
        Me.XrTableCell_total_non_dmp_payment.Name = "XrTableCell_total_non_dmp_payment"
        XrSummary3.FormatString = "{0:c}"
        XrSummary3.Running = DevExpress.XtraReports.UI.SummaryRunning.Group
        Me.XrTableCell_total_non_dmp_payment.Summary = XrSummary3
        Me.XrTableCell_total_non_dmp_payment.Weight = 0.617487589007047R
        Me.XrTableCell_total_non_dmp_payment.WordWrap = False
        '
        'XrTableCell95
        '
        Me.XrTableCell95.Name = "XrTableCell95"
        Me.XrTableCell95.Weight = 0.8586934688702299R
        Me.XrTableCell95.WordWrap = False
        '
        'XrTable4
        '
        Me.XrTable4.Borders = CType((((DevExpress.XtraPrinting.BorderSide.Left Or DevExpress.XtraPrinting.BorderSide.Top) _
            Or DevExpress.XtraPrinting.BorderSide.Right) _
            Or DevExpress.XtraPrinting.BorderSide.Bottom), DevExpress.XtraPrinting.BorderSide)
        Me.XrTable4.Font = New System.Drawing.Font("Calibri", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.XrTable4.LocationFloat = New DevExpress.Utils.PointFloat(10.00001!, 78.125!)
        Me.XrTable4.Name = "XrTable4"
        Me.XrTable4.Rows.AddRange(New DevExpress.XtraReports.UI.XRTableRow() {Me.XrTableRow5, Me.XrTableRow6, Me.XrTableRow7, Me.XrTableRow8, Me.XrTableRow9})
        Me.XrTable4.SizeF = New System.Drawing.SizeF(580.6251!, 125.0!)
        Me.XrTable4.StylePriority.UseBorders = False
        Me.XrTable4.StylePriority.UseFont = False
        Me.XrTable4.StylePriority.UseTextAlignment = False
        Me.XrTable4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        '
        'XrTableRow5
        '
        Me.XrTableRow5.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.XrTableCell5, Me.XrTableCell_marital_status})
        Me.XrTableRow5.Name = "XrTableRow5"
        Me.XrTableRow5.Weight = 1.0R
        '
        'XrTableCell5
        '
        Me.XrTableCell5.Name = "XrTableCell5"
        Me.XrTableCell5.Text = "Marital Status:"
        Me.XrTableCell5.Weight = 2.2934839662216904R
        '
        'XrTableCell_marital_status
        '
        Me.XrTableCell_marital_status.Name = "XrTableCell_marital_status"
        Me.XrTableCell_marital_status.Scripts.OnBeforePrint = "XrTableCell_marital_status_BeforePrint"
        Me.XrTableCell_marital_status.StylePriority.UseTextAlignment = False
        Me.XrTableCell_marital_status.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
        Me.XrTableCell_marital_status.Weight = 0.70651603377830985R
        '
        'XrTableRow6
        '
        Me.XrTableRow6.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.XrTableCell7, Me.XrTableCell9})
        Me.XrTableRow6.Name = "XrTableRow6"
        Me.XrTableRow6.Weight = 1.0R
        '
        'XrTableCell9
        '
        Me.XrTableCell9.Name = "XrTableCell9"
        Me.XrTableCell9.StylePriority.UseTextAlignment = False
        Me.XrTableCell9.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
        Me.XrTableCell9.Weight = 0.70651603377830985R
        '
        'XrTableRow7
        '
        Me.XrTableRow7.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.XrTableCell13, Me.XrTableCell17})
        Me.XrTableRow7.Name = "XrTableRow7"
        Me.XrTableRow7.Weight = 1.0R
        '
        'XrTableCell13
        '
        Me.XrTableCell13.Name = "XrTableCell13"
        Me.XrTableCell13.Text = "Number of Exemptions Claimed on Federal Tax Returns:"
        Me.XrTableCell13.Weight = 2.2934841723441597R
        '
        'XrTableCell17
        '
        Me.XrTableCell17.Name = "XrTableCell17"
        Me.XrTableCell17.StylePriority.UseTextAlignment = False
        Me.XrTableCell17.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
        Me.XrTableCell17.Weight = 0.70651582765584031R
        '
        'XrTableRow8
        '
        Me.XrTableRow8.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.XrTableCell18, Me.XrTableCell19})
        Me.XrTableRow8.Name = "XrTableRow8"
        Me.XrTableRow8.Weight = 1.0R
        '
        'XrTableCell18
        '
        Me.XrTableCell18.Name = "XrTableCell18"
        Me.XrTableCell18.Text = "Gross Income per Pay Period:"
        Me.XrTableCell18.Weight = 2.2934841723441597R
        '
        'XrTableCell19
        '
        Me.XrTableCell19.Name = "XrTableCell19"
        Me.XrTableCell19.StylePriority.UseTextAlignment = False
        Me.XrTableCell19.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
        Me.XrTableCell19.Weight = 0.70651582765584031R
        '
        'XrTableRow9
        '
        Me.XrTableRow9.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.XrTableCell20, Me.XrTableCell21})
        Me.XrTableRow9.Name = "XrTableRow9"
        Me.XrTableRow9.Weight = 1.0R
        '
        'XrTableCell21
        '
        Me.XrTableCell21.Name = "XrTableCell21"
        Me.XrTableCell21.StylePriority.UseTextAlignment = False
        Me.XrTableCell21.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
        Me.XrTableCell21.Weight = 0.70651603377830985R
        '
        'XrLabel29
        '
        Me.XrLabel29.Font = New System.Drawing.Font("Calibri", 12.0!)
        Me.XrLabel29.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 58.00009!)
        Me.XrLabel29.Name = "XrLabel29"
        Me.XrLabel29.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel29.SizeF = New System.Drawing.SizeF(100.0!, 17.0!)
        Me.XrLabel29.StylePriority.UseFont = False
        Me.XrLabel29.Text = "Signature"
        '
        'XrTableCell56
        '
        Me.XrTableCell56.Name = "XrTableCell56"
        Me.XrTableCell56.Weight = 5.4104162597656247R
        '
        'XrLabel30
        '
        Me.XrLabel30.Font = New System.Drawing.Font("Calibri", 12.0!)
        Me.XrLabel30.LocationFloat = New DevExpress.Utils.PointFloat(402.0!, 58.00009!)
        Me.XrLabel30.Name = "XrLabel30"
        Me.XrLabel30.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel30.SizeF = New System.Drawing.SizeF(100.0!, 17.0!)
        Me.XrLabel30.StylePriority.UseFont = False
        Me.XrLabel30.Text = "Date"
        '
        'XrTableRow26
        '
        Me.XrTableRow26.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.XrTableCell54, Me.XrTableCell55})
        Me.XrTableRow26.Name = "XrTableRow26"
        Me.XrTableRow26.Weight = 1.0R
        '
        'XrTableCell54
        '
        Me.XrTableCell54.Name = "XrTableCell54"
        Me.XrTableCell54.Weight = 5.4104162597656247R
        '
        'XrTableCell55
        '
        Me.XrTableCell55.Name = "XrTableCell55"
        Me.XrTableCell55.Weight = 1.5791668701171877R
        '
        'XrTableCell61
        '
        Me.XrTableCell61.Name = "XrTableCell61"
        Me.XrTableCell61.Weight = 1.5791668701171877R
        '
        'XrTable11
        '
        Me.XrTable11.Borders = CType((((DevExpress.XtraPrinting.BorderSide.Left Or DevExpress.XtraPrinting.BorderSide.Top) _
            Or DevExpress.XtraPrinting.BorderSide.Right) _
            Or DevExpress.XtraPrinting.BorderSide.Bottom), DevExpress.XtraPrinting.BorderSide)
        Me.XrTable11.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
        Me.XrTable11.Name = "XrTable11"
        Me.XrTable11.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 2, 2, 100.0!)
        Me.XrTable11.Rows.AddRange(New DevExpress.XtraReports.UI.XRTableRow() {Me.XrTableRow36})
        Me.XrTable11.SizeF = New System.Drawing.SizeF(743.75!, 17.0!)
        Me.XrTable11.StylePriority.UseBorders = False
        Me.XrTable11.StylePriority.UsePadding = False
        '
        'XrTableRow24
        '
        Me.XrTableRow24.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.XrTableCell50, Me.XrTableCell51})
        Me.XrTableRow24.Name = "XrTableRow24"
        Me.XrTableRow24.Weight = 1.0R
        '
        'XrTableCell50
        '
        Me.XrTableCell50.Name = "XrTableCell50"
        Me.XrTableCell50.Weight = 5.4104162597656247R
        '
        'XrTableCell51
        '
        Me.XrTableCell51.Name = "XrTableCell51"
        Me.XrTableCell51.Weight = 1.5791668701171877R
        '
        'XrTableCell49
        '
        Me.XrTableCell49.Name = "XrTableCell49"
        Me.XrTableCell49.Weight = 1.5791662597656253R
        '
        'XrTableCell45
        '
        Me.XrTableCell45.Name = "XrTableCell45"
        Me.XrTableCell45.Weight = 1.5791668701171877R
        '
        'GroupHeader1
        '
        Me.GroupHeader1.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrTable9})
        Me.GroupHeader1.GroupUnion = DevExpress.XtraReports.UI.GroupUnion.WithFirstDetail
        Me.GroupHeader1.HeightF = 34.0!
        Me.GroupHeader1.Name = "GroupHeader1"
        Me.GroupHeader1.RepeatEveryPage = True
        '
        'XrTableCell69
        '
        Me.XrTableCell69.Name = "XrTableCell69"
        Me.XrTableCell69.Weight = 1.5791662597656253R
        '
        'XrTableCell40
        '
        Me.XrTableCell40.Name = "XrTableCell40"
        Me.XrTableCell40.Weight = 5.4104168701171877R
        '
        'XrTable7
        '
        Me.XrTable7.Borders = CType((((DevExpress.XtraPrinting.BorderSide.Left Or DevExpress.XtraPrinting.BorderSide.Top) _
            Or DevExpress.XtraPrinting.BorderSide.Right) _
            Or DevExpress.XtraPrinting.BorderSide.Bottom), DevExpress.XtraPrinting.BorderSide)
        Me.XrTable7.BorderWidth = 2
        Me.XrTable7.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 35.50002!)
        Me.XrTable7.Name = "XrTable7"
        Me.XrTable7.Rows.AddRange(New DevExpress.XtraReports.UI.XRTableRow() {Me.XrTableRow22, Me.XrTableRow23, Me.XrTableRow24, Me.XrTableRow25, Me.XrTableRow26, Me.XrTableRow27})
        Me.XrTable7.SizeF = New System.Drawing.SizeF(698.9583!, 150.0!)
        Me.XrTable7.StylePriority.UseBorders = False
        Me.XrTable7.StylePriority.UseBorderWidth = False
        '
        'XrTableRow22
        '
        Me.XrTableRow22.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.XrTableCell46, Me.XrTableCell47})
        Me.XrTableRow22.Font = New System.Drawing.Font("Calibri", 12.0!, System.Drawing.FontStyle.Bold)
        Me.XrTableRow22.Name = "XrTableRow22"
        Me.XrTableRow22.StylePriority.UseFont = False
        Me.XrTableRow22.Weight = 1.0R
        '
        'XrTableCell46
        '
        Me.XrTableCell46.Name = "XrTableCell46"
        Me.XrTableCell46.StylePriority.UseTextAlignment = False
        Me.XrTableCell46.Text = "Description"
        Me.XrTableCell46.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
        Me.XrTableCell46.Weight = 5.4104162597656247R
        '
        'XrTableCell47
        '
        Me.XrTableCell47.Name = "XrTableCell47"
        Me.XrTableCell47.StylePriority.UseTextAlignment = False
        Me.XrTableCell47.Text = "Amount Per Pay Period"
        Me.XrTableCell47.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter
        Me.XrTableCell47.Weight = 1.5791668701171877R
        '
        'XrTableRow23
        '
        Me.XrTableRow23.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.XrTableCell48, Me.XrTableCell49})
        Me.XrTableRow23.Name = "XrTableRow23"
        Me.XrTableRow23.Weight = 1.0R
        '
        'XrTableCell48
        '
        Me.XrTableCell48.Name = "XrTableCell48"
        Me.XrTableCell48.Weight = 5.4104168701171877R
        '
        'XrTableRow25
        '
        Me.XrTableRow25.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.XrTableCell52, Me.XrTableCell53})
        Me.XrTableRow25.Name = "XrTableRow25"
        Me.XrTableRow25.Weight = 1.0R
        '
        'XrTableCell53
        '
        Me.XrTableCell53.Name = "XrTableCell53"
        Me.XrTableCell53.Weight = 1.5791668701171877R
        '
        'XrTableRow27
        '
        Me.XrTableRow27.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.XrTableCell56, Me.XrTableCell57})
        Me.XrTableRow27.Name = "XrTableRow27"
        Me.XrTableRow27.Weight = 1.0R
        '
        'XrTableCell57
        '
        Me.XrTableCell57.Name = "XrTableCell57"
        Me.XrTableCell57.Weight = 1.5791668701171877R
        '
        'XrLabel13
        '
        Me.XrLabel13.Font = New System.Drawing.Font("Calibri", 12.0!, System.Drawing.FontStyle.Bold)
        Me.XrLabel13.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
        Me.XrLabel13.Name = "XrLabel13"
        Me.XrLabel13.Padding = New DevExpress.XtraPrinting.PaddingInfo(5, 5, 5, 5, 100.0!)
        Me.XrLabel13.SizeF = New System.Drawing.SizeF(698.9583!, 23.00002!)
        Me.XrLabel13.StylePriority.UseFont = False
        Me.XrLabel13.StylePriority.UsePadding = False
        Me.XrLabel13.Text = "Payroll Deductions"
        '
        'XrPanel3
        '
        Me.XrPanel3.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrTable7, Me.XrLabel14})
        Me.XrPanel3.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 635.4167!)
        Me.XrPanel3.Name = "XrPanel3"
        Me.XrPanel3.SizeF = New System.Drawing.SizeF(714.5833!, 194.7917!)
        '
        'XrTableRow29
        '
        Me.XrTableRow29.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.XrTableCell60, Me.XrTableCell61})
        Me.XrTableRow29.Name = "XrTableRow29"
        Me.XrTableRow29.Weight = 1.0R
        '
        'XrTableCell60
        '
        Me.XrTableCell60.Name = "XrTableCell60"
        Me.XrTableCell60.Weight = 5.4104162597656247R
        '
        'XrTableCell63
        '
        Me.XrTableCell63.Name = "XrTableCell63"
        Me.XrTableCell63.Weight = 1.5791662597656253R
        '
        'XrTableCell64
        '
        Me.XrTableCell64.Name = "XrTableCell64"
        Me.XrTableCell64.Weight = 5.4104168701171877R
        '
        'XrTableRow33
        '
        Me.XrTableRow33.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.XrTableCell68, Me.XrTableCell69})
        Me.XrTableRow33.Name = "XrTableRow33"
        Me.XrTableRow33.Weight = 1.0R
        '
        'XrTableCell68
        '
        Me.XrTableCell68.Name = "XrTableCell68"
        Me.XrTableCell68.Weight = 5.4104168701171877R
        '
        'XrPanel2
        '
        Me.XrPanel2.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrTable6, Me.XrLabel13})
        Me.XrPanel2.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 425.0!)
        Me.XrPanel2.Name = "XrPanel2"
        Me.XrPanel2.SizeF = New System.Drawing.SizeF(714.5833!, 194.7917!)
        '
        'XrTable6
        '
        Me.XrTable6.Borders = CType((((DevExpress.XtraPrinting.BorderSide.Left Or DevExpress.XtraPrinting.BorderSide.Top) _
            Or DevExpress.XtraPrinting.BorderSide.Right) _
            Or DevExpress.XtraPrinting.BorderSide.Bottom), DevExpress.XtraPrinting.BorderSide)
        Me.XrTable6.BorderWidth = 2
        Me.XrTable6.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 35.50002!)
        Me.XrTable6.Name = "XrTable6"
        Me.XrTable6.Rows.AddRange(New DevExpress.XtraReports.UI.XRTableRow() {Me.XrTableRow16, Me.XrTableRow17, Me.XrTableRow18, Me.XrTableRow19, Me.XrTableRow20, Me.XrTableRow21})
        Me.XrTable6.SizeF = New System.Drawing.SizeF(698.9583!, 150.0!)
        Me.XrTable6.StylePriority.UseBorders = False
        Me.XrTable6.StylePriority.UseBorderWidth = False
        '
        'XrTableRow17
        '
        Me.XrTableRow17.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.XrTableCell36, Me.XrTableCell37})
        Me.XrTableRow17.Name = "XrTableRow17"
        Me.XrTableRow17.Weight = 1.0R
        '
        'XrTableCell36
        '
        Me.XrTableCell36.Name = "XrTableCell36"
        Me.XrTableCell36.Weight = 5.4104168701171877R
        '
        'XrTableCell37
        '
        Me.XrTableCell37.Name = "XrTableCell37"
        Me.XrTableCell37.Weight = 1.5791662597656253R
        '
        'XrTableRow18
        '
        Me.XrTableRow18.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.XrTableCell38, Me.XrTableCell39})
        Me.XrTableRow18.Name = "XrTableRow18"
        Me.XrTableRow18.Weight = 1.0R
        '
        'XrTableCell38
        '
        Me.XrTableCell38.Name = "XrTableCell38"
        Me.XrTableCell38.Weight = 5.4104162597656247R
        '
        'XrTableCell39
        '
        Me.XrTableCell39.Name = "XrTableCell39"
        Me.XrTableCell39.Weight = 1.5791668701171877R
        '
        'XrTableRow19
        '
        Me.XrTableRow19.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.XrTableCell40, Me.XrTableCell41})
        Me.XrTableRow19.Name = "XrTableRow19"
        Me.XrTableRow19.Weight = 1.0R
        '
        'XrTableCell41
        '
        Me.XrTableCell41.Name = "XrTableCell41"
        Me.XrTableCell41.Weight = 1.5791662597656253R
        '
        'XrTableRow21
        '
        Me.XrTableRow21.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.XrTableCell44, Me.XrTableCell45})
        Me.XrTableRow21.Name = "XrTableRow21"
        Me.XrTableRow21.Weight = 1.0R
        '
        'XrTableCell44
        '
        Me.XrTableCell44.Name = "XrTableCell44"
        Me.XrTableCell44.Weight = 5.4104162597656247R
        '
        'XrTable8
        '
        Me.XrTable8.Borders = CType((((DevExpress.XtraPrinting.BorderSide.Left Or DevExpress.XtraPrinting.BorderSide.Top) _
            Or DevExpress.XtraPrinting.BorderSide.Right) _
            Or DevExpress.XtraPrinting.BorderSide.Bottom), DevExpress.XtraPrinting.BorderSide)
        Me.XrTable8.BorderWidth = 2
        Me.XrTable8.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 35.50002!)
        Me.XrTable8.Name = "XrTable8"
        Me.XrTable8.Rows.AddRange(New DevExpress.XtraReports.UI.XRTableRow() {Me.XrTableRow28, Me.XrTableRow29, Me.XrTableRow30, Me.XrTableRow31, Me.XrTableRow32, Me.XrTableRow33})
        Me.XrTable8.SizeF = New System.Drawing.SizeF(698.9583!, 150.0!)
        Me.XrTable8.StylePriority.UseBorders = False
        Me.XrTable8.StylePriority.UseBorderWidth = False
        '
        'XrTableRow30
        '
        Me.XrTableRow30.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.XrTableCell62, Me.XrTableCell63})
        Me.XrTableRow30.Name = "XrTableRow30"
        Me.XrTableRow30.Weight = 1.0R
        '
        'XrTableRow31
        '
        Me.XrTableRow31.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.XrTableCell64, Me.XrTableCell65})
        Me.XrTableRow31.Name = "XrTableRow31"
        Me.XrTableRow31.Weight = 1.0R
        '
        'XrTableRow32
        '
        Me.XrTableRow32.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.XrTableCell66, Me.XrTableCell67})
        Me.XrTableRow32.Name = "XrTableRow32"
        Me.XrTableRow32.Weight = 1.0R
        '
        'XrTableCell67
        '
        Me.XrTableCell67.Name = "XrTableCell67"
        Me.XrTableCell67.Weight = 1.5791668701171877R
        '
        'XrPanel5
        '
        Me.XrPanel5.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel16, Me.XrLine2, Me.XrLabel30, Me.XrLine1, Me.XrLabel29})
        Me.XrPanel5.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 364.4165!)
        Me.XrPanel5.Name = "XrPanel5"
        Me.XrPanel5.SizeF = New System.Drawing.SizeF(764.4584!, 75.0!)
        '
        'XrLabel16
        '
        Me.XrLabel16.Font = New System.Drawing.Font("Calibri", 12.0!, System.Drawing.FontStyle.Bold)
        Me.XrLabel16.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
        Me.XrLabel16.Name = "XrLabel16"
        Me.XrLabel16.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel16.SizeF = New System.Drawing.SizeF(740.0!, 25.3334!)
        Me.XrLabel16.StylePriority.UseFont = False
        Me.XrLabel16.Text = "I UNDERSTAND THAT THIS IS AN ESTIMATE AND IS NO WAY BINDING."
        '
        'XrLine2
        '
        Me.XrLine2.LocationFloat = New DevExpress.Utils.PointFloat(402.0!, 50.0!)
        Me.XrLine2.Name = "XrLine2"
        Me.XrLine2.SizeF = New System.Drawing.SizeF(317.0!, 8.0!)
        '
        'XrLine1
        '
        Me.XrLine1.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 50.0!)
        Me.XrLine1.Name = "XrLine1"
        Me.XrLine1.SizeF = New System.Drawing.SizeF(383.0!, 8.0!)
        '
        'XrTableRow38
        '
        Me.XrTableRow38.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.XrTableCell_creditor_address, Me.XrTableCell107})
        Me.XrTableRow38.Name = "XrTableRow38"
        Me.XrTableRow38.Weight = 1.0R
        '
        'XrTableCell_creditor_address
        '
        Me.XrTableCell_creditor_address.Multiline = True
        Me.XrTableCell_creditor_address.Name = "XrTableCell_creditor_address"
        Me.XrTableCell_creditor_address.Scripts.OnBeforePrint = "XrTableCell_creditor_address_BeforePrint"
        Me.XrTableCell_creditor_address.Weight = 1.1758793969849246R
        Me.XrTableCell_creditor_address.WordWrap = False
        '
        'XrTableCell107
        '
        Me.XrTableCell107.Name = "XrTableCell107"
        Me.XrTableCell107.Weight = 2.4120600070665827R
        Me.XrTableCell107.WordWrap = False
        '
        'GroupFooter1
        '
        Me.GroupFooter1.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrPanel5, Me.XrRichText1, Me.XrTable11})
        Me.GroupFooter1.GroupUnion = DevExpress.XtraReports.UI.GroupFooterUnion.WithLastDetail
        Me.GroupFooter1.HeightF = 448.0!
        Me.GroupFooter1.Name = "GroupFooter1"
        '
        'XrRichText1
        '
        Me.XrRichText1.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 39.33331!)
        Me.XrRichText1.Name = "XrRichText1"
        Me.XrRichText1.SerializableRtfString = resources.GetString("XrRichText1.SerializableRtfString")
        Me.XrRichText1.SizeF = New System.Drawing.SizeF(750.0!, 325.0832!)
        '
        'XrLabel_Name
        '
        Me.XrLabel_Name.Font = New System.Drawing.Font("Calibri", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.XrLabel_Name.LocationFloat = New DevExpress.Utils.PointFloat(10.00001!, 37.45829!)
        Me.XrLabel_Name.Multiline = True
        Me.XrLabel_Name.Name = "XrLabel_Name"
        Me.XrLabel_Name.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel_Name.Scripts.OnBeforePrint = "XrLabel_Name_BeforePrint"
        Me.XrLabel_Name.SizeF = New System.Drawing.SizeF(580.6251!, 23.00002!)
        Me.XrLabel_Name.StylePriority.UseFont = False
        '
        'XrLabel11
        '
        Me.XrLabel11.Font = New System.Drawing.Font("Calibri", 12.0!, System.Drawing.FontStyle.Bold)
        Me.XrLabel11.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
        Me.XrLabel11.Name = "XrLabel11"
        Me.XrLabel11.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel11.SizeF = New System.Drawing.SizeF(800.0!, 23.0!)
        Me.XrLabel11.StylePriority.UseFont = False
        Me.XrLabel11.StylePriority.UseTextAlignment = False
        Me.XrLabel11.Text = "SCHEDULE M"
        Me.XrLabel11.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
        '
        'ReportFooter
        '
        Me.ReportFooter.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrPanel4, Me.XrPanel3, Me.XrPanel2, Me.XrTable4, Me.XrPanel1, Me.XrLabel11, Me.XrLabel_Name})
        Me.ReportFooter.HeightF = 1037.5!
        Me.ReportFooter.Name = "ReportFooter"
        Me.ReportFooter.PageBreak = DevExpress.XtraReports.UI.PageBreak.BeforeBand
        '
        'XrPanel4
        '
        Me.XrPanel4.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrTable8, Me.XrLabel15})
        Me.XrPanel4.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 842.7083!)
        Me.XrPanel4.Name = "XrPanel4"
        Me.XrPanel4.SizeF = New System.Drawing.SizeF(714.5833!, 194.7917!)
        '
        'XrLabel15
        '
        Me.XrLabel15.Font = New System.Drawing.Font("Calibri", 12.0!, System.Drawing.FontStyle.Bold)
        Me.XrLabel15.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
        Me.XrLabel15.Name = "XrLabel15"
        Me.XrLabel15.Padding = New DevExpress.XtraPrinting.PaddingInfo(5, 5, 5, 5, 100.0!)
        Me.XrLabel15.SizeF = New System.Drawing.SizeF(698.9583!, 23.00002!)
        Me.XrLabel15.StylePriority.UseFont = False
        Me.XrLabel15.StylePriority.UsePadding = False
        Me.XrLabel15.Text = "Garnishments"
        '
        'XrTable10
        '
        Me.XrTable10.Borders = CType((((DevExpress.XtraPrinting.BorderSide.Left Or DevExpress.XtraPrinting.BorderSide.Top) _
            Or DevExpress.XtraPrinting.BorderSide.Right) _
            Or DevExpress.XtraPrinting.BorderSide.Bottom), DevExpress.XtraPrinting.BorderSide)
        Me.XrTable10.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
        Me.XrTable10.Name = "XrTable10"
        Me.XrTable10.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 2, 2, 100.0!)
        Me.XrTable10.Rows.AddRange(New DevExpress.XtraReports.UI.XRTableRow() {Me.XrTableRow35, Me.XrTableRow38})
        Me.XrTable10.SizeF = New System.Drawing.SizeF(743.7499!, 34.0!)
        Me.XrTable10.StylePriority.UseBorders = False
        Me.XrTable10.StylePriority.UsePadding = False
        '
        'ReportHeader
        '
        Me.ReportHeader.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel1})
        Me.ReportHeader.HeightF = 40.0!
        Me.ReportHeader.Name = "ReportHeader"
        Me.ReportHeader.PageBreak = DevExpress.XtraReports.UI.PageBreak.BeforeBand
        '
        'XrLabel1
        '
        Me.XrLabel1.Font = New System.Drawing.Font("Calibri", 12.0!, System.Drawing.FontStyle.Bold)
        Me.XrLabel1.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
        Me.XrLabel1.Name = "XrLabel1"
        Me.XrLabel1.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel1.SizeF = New System.Drawing.SizeF(750.0!, 33.0!)
        Me.XrLabel1.StylePriority.UseFont = False
        Me.XrLabel1.StylePriority.UseTextAlignment = False
        Me.XrLabel1.Text = "CREDITOR LIST - SCHEDULE A"
        Me.XrLabel1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
        '
        'Detail
        '
        Me.Detail.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrTable10})
        Me.Detail.HeightF = 34.0!
        Me.Detail.Name = "Detail"
        Me.Detail.StylePriority.UseTextAlignment = False
        Me.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'TopMarginBand1
        '
        Me.TopMarginBand1.HeightF = 25.0!
        Me.TopMarginBand1.Name = "TopMarginBand1"
        '
        'BottomMarginBand1
        '
        Me.BottomMarginBand1.HeightF = 25.0!
        Me.BottomMarginBand1.Name = "BottomMarginBand1"
        '
        'DMPDebtsSubReport
        '
        Me.Bands.AddRange(New DevExpress.XtraReports.UI.Band() {Me.Detail, Me.ReportHeader, Me.GroupHeader1, Me.GroupFooter1, Me.ReportFooter, Me.TopMarginBand1, Me.BottomMarginBand1})
        Me.Margins = New System.Drawing.Printing.Margins(25, 25, 25, 25)
        Me.ScriptLanguage = DevExpress.XtraReports.ScriptLanguage.VisualBasic
        Me.ScriptReferencesString = resources.GetString("$this.ScriptReferencesString")
        Me.Scripts.OnBeforePrint = "DMPDebtsSubReport_BeforePrint"
        Me.ScriptsSource = resources.GetString("$this.ScriptsSource")
        Me.Version = "11.2"
        CType(Me.XrTable9, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.XrTable5, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.XrTable4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.XrTable11, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.XrTable7, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.XrTable6, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.XrTable8, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.XrRichText1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.XrTable10, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me, System.ComponentModel.ISupportInitialize).EndInit()

    End Sub
    Friend WithEvents XrTableRow35 As DevExpress.XtraReports.UI.XRTableRow
    Friend WithEvents XrTableCell_creditor_name As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents XrTableCell_account_number As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents XrTableCel_balance As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents XrTableCell_dmp_rate As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents XrTableCell_disbursement_factor As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents XrTableCellnon_dmp_rate As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents XrTableCell_non_dmp_payment As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents XrTableCell_payout_date As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents XrTableCell_creditor As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents XrTableCell7 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents XrTableRow20 As DevExpress.XtraReports.UI.XRTableRow
    Friend WithEvents XrTableCell42 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents XrTableCell43 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents XrTableCell34 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents XrTableCell62 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents XrTableCell8 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents XrTableCell93 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents XrTableRow28 As DevExpress.XtraReports.UI.XRTableRow
    Friend WithEvents XrTableCell58 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents XrTableCell59 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents XrTableRow34 As DevExpress.XtraReports.UI.XRTableRow
    Friend WithEvents XrTableCell78 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents XrTableCell70 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents XrTableCell81 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents XrTableCell71 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents XrTableCell97 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents XrTableCell87 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents XrTableCell90 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents XrTableCell52 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents XrTable9 As DevExpress.XtraReports.UI.XRTable
    Friend WithEvents XrTableRow37 As DevExpress.XtraReports.UI.XRTableRow
    Friend WithEvents XrTableCell98 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents XrTableCell101 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents XrTableCell102 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents XrTableCell103 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents XrLabel14 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrTableCell66 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents XrPanel1 As DevExpress.XtraReports.UI.XRPanel
    Friend WithEvents XrTable5 As DevExpress.XtraReports.UI.XRTable
    Friend WithEvents XrTableRow10 As DevExpress.XtraReports.UI.XRTableRow
    Friend WithEvents XrTableCell22 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents XrTableCell24 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents XrTableRow11 As DevExpress.XtraReports.UI.XRTableRow
    Friend WithEvents XrTableCell23 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents XrTableCell25 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents XrTableRow12 As DevExpress.XtraReports.UI.XRTableRow
    Friend WithEvents XrTableCell26 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents XrTableCell27 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents XrTableRow13 As DevExpress.XtraReports.UI.XRTableRow
    Friend WithEvents XrTableCell28 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents XrTableCell29 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents XrTableRow14 As DevExpress.XtraReports.UI.XRTableRow
    Friend WithEvents XrTableCell30 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents XrTableCell31 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents XrTableRow15 As DevExpress.XtraReports.UI.XRTableRow
    Friend WithEvents XrTableCell32 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents XrTableCell33 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents XrLabel12 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrTableRow16 As DevExpress.XtraReports.UI.XRTableRow
    Friend WithEvents XrTableCell35 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents XrTableCell20 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents XrTableCell65 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents XrTableRow36 As DevExpress.XtraReports.UI.XRTableRow
    Friend WithEvents XrTableCell80 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents XrTableCell_total_balance As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents XrTableCell_total_disbursement_factor As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents XrTableCell_total_non_dmp_payment As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents XrTableCell95 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents XrTable4 As DevExpress.XtraReports.UI.XRTable
    Friend WithEvents XrTableRow5 As DevExpress.XtraReports.UI.XRTableRow
    Friend WithEvents XrTableCell5 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents XrTableCell_marital_status As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents XrTableRow6 As DevExpress.XtraReports.UI.XRTableRow
    Friend WithEvents XrTableCell9 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents XrTableRow7 As DevExpress.XtraReports.UI.XRTableRow
    Friend WithEvents XrTableCell13 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents XrTableCell17 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents XrTableRow8 As DevExpress.XtraReports.UI.XRTableRow
    Friend WithEvents XrTableCell18 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents XrTableCell19 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents XrTableRow9 As DevExpress.XtraReports.UI.XRTableRow
    Friend WithEvents XrTableCell21 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents XrLabel29 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrTableCell56 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents XrLabel30 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrTableRow26 As DevExpress.XtraReports.UI.XRTableRow
    Friend WithEvents XrTableCell54 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents XrTableCell55 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents XrTableCell61 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents XrTable11 As DevExpress.XtraReports.UI.XRTable
    Friend WithEvents XrTableRow24 As DevExpress.XtraReports.UI.XRTableRow
    Friend WithEvents XrTableCell50 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents XrTableCell51 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents XrTableCell49 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents XrTableCell45 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents GroupHeader1 As DevExpress.XtraReports.UI.GroupHeaderBand
    Friend WithEvents XrTableCell69 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents XrTableCell40 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents XrTable7 As DevExpress.XtraReports.UI.XRTable
    Friend WithEvents XrTableRow22 As DevExpress.XtraReports.UI.XRTableRow
    Friend WithEvents XrTableCell46 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents XrTableCell47 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents XrTableRow23 As DevExpress.XtraReports.UI.XRTableRow
    Friend WithEvents XrTableCell48 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents XrTableRow25 As DevExpress.XtraReports.UI.XRTableRow
    Friend WithEvents XrTableCell53 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents XrTableRow27 As DevExpress.XtraReports.UI.XRTableRow
    Friend WithEvents XrTableCell57 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents XrLabel13 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrPanel3 As DevExpress.XtraReports.UI.XRPanel
    Friend WithEvents XrTableRow29 As DevExpress.XtraReports.UI.XRTableRow
    Friend WithEvents XrTableCell60 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents XrTableCell63 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents XrTableCell64 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents XrTableRow33 As DevExpress.XtraReports.UI.XRTableRow
    Friend WithEvents XrTableCell68 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents XrPanel2 As DevExpress.XtraReports.UI.XRPanel
    Friend WithEvents XrTable6 As DevExpress.XtraReports.UI.XRTable
    Friend WithEvents XrTableRow17 As DevExpress.XtraReports.UI.XRTableRow
    Friend WithEvents XrTableCell36 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents XrTableCell37 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents XrTableRow18 As DevExpress.XtraReports.UI.XRTableRow
    Friend WithEvents XrTableCell38 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents XrTableCell39 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents XrTableRow19 As DevExpress.XtraReports.UI.XRTableRow
    Friend WithEvents XrTableCell41 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents XrTableRow21 As DevExpress.XtraReports.UI.XRTableRow
    Friend WithEvents XrTableCell44 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents XrTable8 As DevExpress.XtraReports.UI.XRTable
    Friend WithEvents XrTableRow30 As DevExpress.XtraReports.UI.XRTableRow
    Friend WithEvents XrTableRow31 As DevExpress.XtraReports.UI.XRTableRow
    Friend WithEvents XrTableRow32 As DevExpress.XtraReports.UI.XRTableRow
    Friend WithEvents XrTableCell67 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents XrPanel5 As DevExpress.XtraReports.UI.XRPanel
    Friend WithEvents XrLabel16 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLine2 As DevExpress.XtraReports.UI.XRLine
    Friend WithEvents XrLine1 As DevExpress.XtraReports.UI.XRLine
    Friend WithEvents XrTableRow38 As DevExpress.XtraReports.UI.XRTableRow
    Friend WithEvents XrTableCell_creditor_address As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents XrTableCell107 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents GroupFooter1 As DevExpress.XtraReports.UI.GroupFooterBand
    Friend WithEvents XrRichText1 As DevExpress.XtraReports.UI.XRRichText
    Friend WithEvents XrLabel_Name As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel11 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents ReportFooter As DevExpress.XtraReports.UI.ReportFooterBand
    Friend WithEvents XrPanel4 As DevExpress.XtraReports.UI.XRPanel
    Friend WithEvents XrLabel15 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrTable10 As DevExpress.XtraReports.UI.XRTable
    Friend WithEvents ReportHeader As DevExpress.XtraReports.UI.ReportHeaderBand
    Friend WithEvents XrLabel1 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents Detail As DevExpress.XtraReports.UI.DetailBand
    Friend WithEvents TopMarginBand1 As DevExpress.XtraReports.UI.TopMarginBand
    Friend WithEvents BottomMarginBand1 As DevExpress.XtraReports.UI.BottomMarginBand
End Class
