#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}
#End Region

Namespace Forms.Authorization
    Public Class SubReportSignatureWithAddress

        Public Sub New()
            MyBase.New()
            InitializeComponent()
            'AddHandler Me.XrLabel_Applicant_Signature.BeforePrint, AddressOf XrLabel_Applicant_Signature_BeforePrint
            'AddHandler Me.BeforePrint, AddressOf SubReportSignature_BeforePrint
        End Sub

        ''' <summary>
        ''' Handle the BeforePrint function to set the data source for the report
        ''' </summary>
        Private Sub SubReportSignature_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs)
            Dim rpt As DevExpress.XtraReports.UI.XtraReport = CType(sender, DevExpress.XtraReports.UI.XtraReport)
            Dim MasterRpt As DevExpress.XtraReports.UI.XtraReport = CType(rpt.MasterReport, DevExpress.XtraReports.UI.XtraReport)
            Dim MasterDataSource As System.Collections.IList = CType(MasterRpt.DataSource, System.Collections.IList)
            If MasterDataSource Is Nothing OrElse MasterDataSource.Count <= 0 Then
                Return
            End If

            Dim Record As DebtPlus.Reports.Forms.Authorization.RecordDefinition = CType(MasterDataSource.Item(0), DebtPlus.Reports.Forms.Authorization.RecordDefinition)
            Dim ds As System.Data.DataSet = Record.ds
            Dim tbl As System.Data.DataTable = ds.Tables("people")
            rpt.DataSource = tbl.DefaultView

            ' Client telephone number
            Dim Client As System.Int32 = Convert.ToInt32(MasterRpt.Parameters("ParameterClient").Value)
            Dim sb As New System.Text.StringBuilder()
            Using cn As System.Data.SqlClient.SqlConnection = New System.Data.SqlClient.SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
                cn.Open()

                Using cmd As New System.Data.SqlClient.SqlCommand()
                    cmd.Connection = cn
                    cmd.CommandText = "SELECT dbo.get_client_phone_numbers(@client)"
                    cmd.CommandType = System.Data.CommandType.Text
                    cmd.Parameters.Add("@client", System.Data.SqlDbType.Int).Value = Client
                    Dim objAnswer As Object = cmd.ExecuteScalar()
                    If objAnswer IsNot Nothing AndAlso objAnswer IsNot System.DBNull.Value Then
                        XrLabelPhoneNumber.Text = objAnswer.ToString()
                    End If
                End Using

                ' Client home address
                Using cmd As New System.Data.SqlClient.SqlCommand()
                    cmd.Connection = cn
                    cmd.CommandText = "SELECT addr1,addr2,addr3 FROM view_client_address WITH (NOLOCK) WHERE client = @client"
                    cmd.CommandType = System.Data.CommandType.Text
                    cmd.Parameters.Add("@client", System.Data.SqlDbType.Int).Value = Client

                    Using rd As System.Data.SqlClient.SqlDataReader = cmd.ExecuteReader(System.Data.CommandBehavior.CloseConnection Or System.Data.CommandBehavior.SingleRow)
                        If rd.Read() Then
                            For Each str As String In New System.String() {DebtPlus.Utils.Nulls.v_String(rd.GetValue(0)), DebtPlus.Utils.Nulls.v_String(rd.GetValue(1)), DebtPlus.Utils.Nulls.v_String(rd.GetValue(2))}
                                If Not String.IsNullOrEmpty(str) Then
                                    sb.Append(System.Environment.NewLine)
                                    sb.Append(str)
                                End If
                            Next

                            If sb.Length > 0 Then
                                sb.Remove(0, 2)
                            End If

                        End If
                    End Using
                End Using
            End Using

            XrLabelAddressLine1.Text = sb.ToString()
        End Sub

        ''' <summary>
        ''' Handle the formatting of the signature marker
        ''' </summary>
        Private Sub XrLabel_Applicant_Signature_BeforePrint(sender As Object, e As System.Drawing.Printing.PrintEventArgs)

            ' Find the label being used
            Dim lbl As DevExpress.XtraReports.UI.XRLabel = TryCast(sender, DevExpress.XtraReports.UI.XRLabel)
            If lbl Is Nothing Then
                Return
            End If

            ' Find the current report object
            Dim rpt As DevExpress.XtraReports.UI.XtraReport = TryCast(lbl.Report, DevExpress.XtraReports.UI.XtraReport)
            If rpt Is Nothing Then
                Return
            End If

            ' Find the current row
            Dim row As System.Data.DataRowView = TryCast(rpt.GetCurrentRow(), System.Data.DataRowView)
            If row Is Nothing Then
                Return
            End If

            ' Find the relation value
            Dim relation As System.Nullable(Of System.Int32) = DebtPlus.Utils.Nulls.v_Int32(row("relation"))
            If Not relation.HasValue Then
                Return
            End If

            ' From the relation, determine the label value
            lbl.Text = If(relation.Value = 1, "Applicant", "Co-Applicant") + " signature"
        End Sub
    End Class
End Namespace
