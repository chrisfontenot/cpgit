﻿#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}
#End Region

'-- We need to simulate the scripts so we need to do delayed binding
Option Strict Off

Namespace Forms.Authorization
    Public Class RecordDefinition
        Protected ReadOnly privateDS As System.Data.DataSet
        Protected ReadOnly privateLanguage As String

        Public Sub New(ByVal ds As System.Data.DataSet, ByVal Language As String)
            privateDS = ds
            privateLanguage = Language
        End Sub

        Public Overridable ReadOnly Property ds() As System.Data.DataSet
            Get
                Return privateDS
            End Get
        End Property

        Public Overridable Property setupfee() As Decimal
            Get
                Dim tbl As System.Data.DataTable = ds.Tables("debts")
                Dim Answer As Object = tbl.Compute("max(adjusted_orig_balance)", "[setup_creditor]=True")
                Return DebtPlus.Utils.Nulls.v_Decimal(Answer).GetValueOrDefault(0D)
            End Get
            Set(value As Decimal)
                ' Do not change the computed figure
            End Set
        End Property

        Public Overridable Property dep_amt_plus_setup() As Decimal
            Get
                Return setupfee + deposit_amount
            End Get
            Set(value As Decimal)
                ' do not change the property. It is only computed.
            End Set
        End Property

        Public Overridable Property [IN_debt_pmt]() As Decimal
            Get
                Return System.Math.Min(cred_disb * 60D, total_debt)
            End Get
            Set(ByVal value As Decimal)
                ' do not change the value
            End Set
        End Property

        Public Overridable Property [NE_end_date]() As String
            Get
                Return end_date ' This is the same as Iowa's end date (for now).
            End Get
            Set(ByVal value As String)
                '
            End Set
        End Property

        ''' <summary>
        ''' Return the last payout date from the list of debts. This is the "final" payout date.
        ''' </summary>
        Public Overridable Property [last_dmp_payout]() As Object
            Get
                Dim tbl As System.Data.DataTable = ds.Tables("debts")
                Dim Answer As Object = tbl.Compute("max(dmp_payout)", "[fee_creditor]=False AND [empty_creditor]=False AND [cushion_creditor]=False AND [setup_creditor]=False")
                Return Answer
            End Get
            Set(value As Object)
                '
            End Set
        End Property

        Public Overridable Property [last_dmp_payout_text]() As String
            Get
                Return FormatDate(last_dmp_payout)
            End Get
            Set(value As String)
                '
            End Set
        End Property

        Public Overridable Property [first_dmp_payout]() As Object
            Get
                Dim tbl As System.Data.DataTable = ds.Tables("debts")
                Dim Answer As Object = tbl.Compute("min(dmp_payout)", "[fee_creditor]=False AND [empty_creditor]=False AND [cushion_creditor]=False AND [setup_creditor]=False")
                Return Answer
            End Get
            Set(value As Object)
                '
            End Set
        End Property

        Public Overridable Property [first_dmp_payout_text]() As String
            Get
                Return FormatDate(first_dmp_payout)
            End Get
            Set(value As String)
                '
            End Set
        End Property

        ''' <summary>
        ''' Return the date three years from the starting date
        ''' </summary>
        Public Overridable Property [end_date]() As String
            Get
                Dim ItemDate As Object = due_date
                If ItemDate IsNot Nothing AndAlso ItemDate IsNot DBNull.Value Then
                    ItemDate = Convert.ToDateTime(ItemDate, System.Globalization.CultureInfo.InvariantCulture).AddYears(3)
                End If
                Return FormatDate(ItemDate)
            End Get
            Set(ByVal value As String)
                '
            End Set
        End Property

        ''' <summary>
        ''' Return the total amount of debt for the client
        ''' </summary>
        Public Overridable Property [total_debt]() As Decimal
            Get
                Dim tbl As System.Data.DataTable = ds.Tables("debts")
                Dim Answer As Object = tbl.Compute("sum(balance)", "([fee_creditor]=False OR [empty_creditor]=True) AND [cushion_creditor]=False AND [setup_creditor]=False")
                If Answer Is DBNull.Value Then Answer = 0D
                Return System.Convert.ToDecimal(Answer, System.Globalization.CultureInfo.InvariantCulture)
            End Get
            Set(ByVal value As Decimal)
                ' Do nothing to change the information
            End Set
        End Property

        ''' <summary>
        ''' Return the amount disbursed to the creditors each month, not including fees
        ''' </summary>
        Public Overridable Property [cred_disb]() As Decimal
            Get
                Dim tbl As System.Data.DataTable = ds.Tables("debts")
                Dim Answer As Object = tbl.Compute("sum(dmp_payment)", "([fee_creditor]=False OR [empty_creditor]=True) AND ([setup_creditor]=False)")
                If Answer Is System.DBNull.Value Then Answer = 0D
                Return System.Convert.ToDecimal(Answer, System.Globalization.CultureInfo.InvariantCulture)
            End Get
            Set(ByVal value As Decimal)
                ' Do nothing to change the information
            End Set
        End Property

        ''' <summary>
        ''' Return the Pro-Forma Percentage figure for the debt payout
        ''' </summary>
        Public Overridable Property proforma_percentage As Double
            Get
                Try
                    Return (Convert.ToDouble(payout_total_fees) / Convert.ToDouble(payout_total_payments)) * 100.0
                Catch ex As Exception
                    Return 0.0
                End Try
            End Get
            Set(value As Double)
                ' Ignore modifications
            End Set
        End Property

        ''' <summary>
        ''' Return the amount of fees each month
        ''' </summary>
        Public Overridable Property [fee_disb]() As Decimal
            Get
                Dim tbl As System.Data.DataTable = ds.Tables("debts")
                Dim Answer As Object = tbl.Compute("sum(dmp_payment)", "[fee_creditor]=True AND [empty_creditor]=False AND [cushion_creditor]=False AND [setup_creditor]=False")
                If Answer Is System.DBNull.Value Then Answer = 0D
                Return System.Convert.ToDecimal(Answer, System.Globalization.CultureInfo.InvariantCulture)
            End Get
            Set(ByVal value As Decimal)
                ' Do nothing to change the information
            End Set
        End Property

        ''' <summary>
        ''' Return the expected deposit amount each month
        ''' </summary>
        Public Overridable Property [deposit_amount]() As Decimal
            Get
                Dim Answer As Object = ds.Tables("client_deposits").Compute("sum(deposit_amount)", String.Empty)
                If Answer Is System.DBNull.Value Then Answer = 0D
                Return System.Convert.ToDecimal(Answer, System.Globalization.CultureInfo.InvariantCulture)
            End Get
            Set(ByVal value As Decimal)
                ' Do nothing to change the information
            End Set
        End Property

        ''' <summary>
        ''' Return the total debt at the time of payout
        ''' </summary>
        Public Overridable Property [simple_payout_total_debt]() As Decimal
            Get
                Dim tbl As System.Data.DataTable = ds.Tables("clients")
                If tbl IsNot Nothing AndAlso tbl.Rows.Count > 0 Then
                    Return DebtPlus.Utils.Nulls.DDec(tbl.Rows(0)("payout_total_debt"))
                End If
                Return 0D
            End Get
            Set(ByVal value As Decimal)
                ' Do nothing to change the information
            End Set
        End Property

        ''' <summary>
        ''' Return the total debt but not include any attributed to the cushion at the time of payout
        ''' </summary>
        Public Overridable Property [payout_total_debt]() As Decimal
            Get
                Return simple_payout_total_debt + payout_cushion_amount
            End Get
            Set(ByVal value As Decimal)
                ' Do nothing to change the information
            End Set
        End Property

        ''' <summary>
        ''' Return the number of months represented on the payout report
        ''' </summary>
        Public Overridable Property [payout_months_to_payout]() As Int32
            Get
                Dim tbl As System.Data.DataTable = ds.Tables("clients")
                If tbl IsNot Nothing AndAlso tbl.Rows.Count > 0 Then
                    Return DebtPlus.Utils.Nulls.DInt(tbl.Rows(0)("payout_months_to_payout"))
                End If
                Return 0
            End Get
            Set(ByVal value As Int32)
                ' Do nothing to change the information
            End Set
        End Property

        ''' <summary>
        ''' Return the total fees at the time of payout
        ''' </summary>
        Public Overridable Property [payout_total_fees]() As Decimal
            Get
                Dim tbl As System.Data.DataTable = ds.Tables("clients")
                If tbl IsNot Nothing AndAlso tbl.Rows.Count > 0 Then
                    Return DebtPlus.Utils.Nulls.DDec(tbl.Rows(0)("payout_total_fees"))
                End If
                Return 0D
            End Get
            Set(ByVal value As Decimal)
                ' Do nothing to change the information
            End Set
        End Property

        ''' <summary>
        ''' Return the total interest at the time of payout
        ''' </summary>
        Public Overridable Property [payout_total_interest]() As Decimal
            Get
                Dim tbl As System.Data.DataTable = ds.Tables("clients")
                If tbl IsNot Nothing AndAlso tbl.Rows.Count > 0 Then
                    Return DebtPlus.Utils.Nulls.DDec(tbl.Rows(0)("payout_total_interest"))
                End If
                Return 0D
            End Get
            Set(ByVal value As Decimal)
                ' Do nothing to change the information
            End Set
        End Property

        ''' <summary>
        ''' Return the total payments at the time of payout
        ''' </summary>
        Public Overridable Property [payout_total_payments]() As Decimal
            Get
                Dim tbl As System.Data.DataTable = ds.Tables("clients")
                If tbl IsNot Nothing AndAlso tbl.Rows.Count > 0 Then
                    Return DebtPlus.Utils.Nulls.DDec(tbl.Rows(0)("payout_total_payments"))
                End If
                Return 0D
            End Get
            Set(ByVal value As Decimal)
                ' Do nothing to change the information
            End Set
        End Property

        ''' <summary>
        ''' Return the deposit amount at the time of payout
        ''' </summary>
        Public Overridable Property [payout_deposit_amount]() As Decimal
            Get
                Dim tbl As System.Data.DataTable = ds.Tables("clients")
                If tbl IsNot Nothing AndAlso tbl.Rows.Count > 0 Then
                    Return DebtPlus.Utils.Nulls.DDec(tbl.Rows(0)("payout_deposit_amount"))
                End If
                Return 0D
            End Get
            Set(ByVal value As Decimal)
                ' Do nothing to change the information
            End Set
        End Property

        ''' <summary>
        ''' Return the monthly fee amount at the time of payout
        ''' </summary>
        Public Overridable Property [payout_monthly_fee]() As Decimal
            Get
                Dim tbl As System.Data.DataTable = ds.Tables("clients")
                If tbl IsNot Nothing AndAlso tbl.Rows.Count > 0 Then
                    Return DebtPlus.Utils.Nulls.DDec(tbl.Rows(0)("payout_monthly_fee"))
                End If
                Return 0D
            End Get
            Set(ByVal value As Decimal)
                ' Do nothing to change the information
            End Set
        End Property

        ''' <summary>
        ''' Return the monthly cushion amount at the time of payout
        ''' </summary>
        Public Overridable Property [payout_cushion_amount]() As Decimal
            Get
                Dim tbl As System.Data.DataTable = ds.Tables("clients")
                If tbl IsNot Nothing AndAlso tbl.Rows.Count > 0 Then
                    Return DebtPlus.Utils.Nulls.DDec(tbl.Rows(0)("payout_cushion_amount"))
                End If
                Return 0D
            End Get
            Set(ByVal value As Decimal)
                ' Do nothing to change the information
            End Set
        End Property

        ''' <summary>
        ''' Return the termination date at the time of payout
        ''' </summary>
        Public Overridable Property [payout_termination_date]() As String
            Get
                Dim Answer As String = String.Empty
                Dim tbl As System.Data.DataTable = ds.Tables("clients")
                If tbl IsNot Nothing AndAlso tbl.Rows.Count > 0 AndAlso tbl.Rows(0)("payout_termination_date") IsNot System.DBNull.Value Then
                    Answer = tbl.Rows(0)("payout_termination_date")
                End If
                Return Answer
            End Get
            Set(ByVal value As String)
                ' Do nothing to change the information
            End Set
        End Property

        Protected Function FormatDate(ByVal ItemDate As Object) As String
            Dim Answer As String = "_____________________________________"
            If ItemDate IsNot Nothing AndAlso ItemDate IsNot System.DBNull.Value Then
                Dim ConvertedDate As DateTime = System.Convert.ToDateTime(ItemDate, System.Globalization.CultureInfo.InvariantCulture).Date
                If ConvertedDate > Date.MinValue Then
                    Answer = String.Format("{0:MMMM, yyyy}", Convert.ToDateTime(ItemDate, System.Globalization.CultureInfo.InvariantCulture))
                End If
            End If
            Return Answer
        End Function

        ''' <summary>
        ''' Return the text string for the deposit date. If there is none, return a field to fill it in.
        ''' </summary>
        Public Overridable Property [due_date_text]() As String
            Get
                Return FormatDate(due_date)
            End Get
            Set(ByVal value As String)
                ' Do nothing to change the information
            End Set
        End Property

        ''' <summary>
        ''' Return the expected deposit date for the client
        ''' </summary>
        Public Overridable Property [due_date]() As Object
            Get
                Dim Answer As Object = System.DBNull.Value
                Dim tbl As System.Data.DataTable = ds.Tables("client_deposits")
                If tbl.Rows.Count > 0 Then
                    Answer = tbl.Rows(0)("deposit_date")
                End If
                Return Answer
            End Get
            Set(ByVal value As Object)
                ' Do nothing to change the information
            End Set
        End Property

        ''' <summary>
        ''' Return the text string for the deposit day. If there is none, return a field to fill it in.
        ''' </summary>
        Public Overridable Property [deposit_day_text]() As String
            Get
                Dim Answer As String = String.Empty
                Dim day As Integer = 1
                Dim tbl As System.Data.DataTable = ds.Tables("client_deposits")
                If tbl.Rows.Count > 0 Then
                    day = tbl.Rows(0)("due_day_text")
                    If privateLanguage = "ES-MX" Then
                        Answer = OrdinalSuffixSpanish(day)
                    Else
                        Answer = OrdinalSuffixEnglish(day)
                    End If
                End If
                Return Answer
            End Get
            Set(ByVal value As String)
                ' Do nothing to change the information
            End Set
        End Property

        Public Overridable Property [deposit_day_plus10]() As String
            Get
                Dim Answer As String = String.Empty
                Dim day As Integer = 0
                Dim dayPlusTen As Integer = 0
                Dim tbl As System.Data.DataTable = ds.Tables("client_deposits")
                If tbl.Rows.Count > 0 Then
                    day = tbl.Rows(0)("due_day_text")
                    If day > 0 AndAlso day < 21 Then
                        dayPlusTen = day + 10
                    ElseIf day > 20 AndAlso day < 26 Then
                        dayPlusTen = 30
                    ElseIf day > 25 AndAlso day < 32 Then
                        dayPlusTen = 9
                    End If
                    If privateLanguage = "ES-MX" Then
                        Answer = OrdinalSuffixSpanish(dayPlusTen)
                    Else
                        Answer = OrdinalSuffixEnglish(dayPlusTen)
                    End If
                End If
                Return Answer
            End Get
            Set(ByVal value As String)
                ' Do nothing to change the information
            End Set
        End Property

        Public Shared Function OrdinalSuffixEnglish(number As Integer) As [String]
            Dim suffix As String = [String].Empty

            Dim ones As Integer = number Mod 10
            Dim tens As Integer = CInt(Math.Floor(number / 10D)) Mod 10

            If tens = 1 Then
                suffix = "th"
            Else
                Select Case ones
                    Case 1
                        suffix = "st"
                        Exit Select

                    Case 2
                        suffix = "nd"
                        Exit Select

                    Case 3
                        suffix = "rd"
                        Exit Select

                    Case Else
                        suffix = "th"
                        Exit Select
                End Select
            End If
            Return [String].Format("{0}{1}", number, suffix)
        End Function

        Public Shared Function OrdinalSuffixSpanish(number As Integer) As [String]
            Dim suffix As String = "°"
            Return [String].Format("{0}{1}", number, suffix)
        End Function
    End Class
End Namespace
