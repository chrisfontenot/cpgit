#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}
#End Region

Option Compare Binary
Option Explicit On
Option Strict On

Imports DebtPlus.Utils.Format

Namespace Client.Labels

    Public MustInherit Class BasicLabelDefinition
        Inherits DebtPlus.Reports.Template.BaseXtraReportClass

        Public Sub New()
            MyBase.New()
            InitializeComponent()
            AddHandler XrBarCode.BeforePrint, AddressOf XrBarCode_BeforePrint
            AddHandler XrBarCode.PrintOnPage, AddressOf XrBarCode_PrintOnPage
            AddHandler XrPanel1.BeforePrint, AddressOf XrPanel1_BeforePrint
            ReconfigureLabels()
        End Sub

        Public Property MyParameter_Include_Barcode() As Boolean = False

        Private Sub XrBarCode_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs)

            With XrBarCode
                Dim TextString As String = DebtPlus.Utils.Format.Strings.DigitsOnly(DebtPlus.Utils.Nulls.DStr(GetCurrentColumnValue("zipcode")))

                If TextString <> String.Empty Then
                    If TextString.Length = 9 AndAlso TextString.EndsWith("0000") Then
                        TextString = TextString.Substring(0, 5)
                    End If
                    If TextString.StartsWith("00000") Then TextString = String.Empty

                    Select Case TextString.Length
                        Case 5, 9
                        Case Else
                            TextString = String.Empty
                    End Select
                End If

                If TextString = String.Empty Then
                    .Visible = False
                Else
                    .Visible = True
                    .Text = TextString
                End If
            End With
        End Sub

        Protected Overridable Sub ReconfigureLabels()
        End Sub

        Private Sub XrBarCode_PrintOnPage(ByVal sender As Object, ByVal e As DevExpress.XtraReports.UI.PrintOnPageEventArgs)
            If Not MyParameter_Include_Barcode Then
                e.Cancel = True
                Return
            End If
        End Sub

        Protected vue As DataView
        Protected Overridable Sub DefineDataSet(ByVal tbl As DataTable)
            vue = New DataView(tbl, String.Empty, "zipcode", DataViewRowState.CurrentRows)
            DataSource = vue
        End Sub

        Dim FirstTime As Boolean = True
        Private Sub XrPanel1_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs)
            If FirstTime Then
                FirstTime = False
                XrBarCode.Location = New Point(0, 0)
                If MyParameter_Include_Barcode Then
                    XrLabel_Text.Location = New Point(0, XrBarCode.Height)
                Else
                    XrLabel_Text.Location = XrBarCode.Location
                End If
                XrLabel_Text.Size = New Size(XrPanel1.Width, XrPanel1.Height - XrLabel_Text.Location.Y)
            End If
        End Sub
    End Class
End Namespace