Namespace Client.Labels.Forms
    <Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
    Partial Class NameReportParametersForm
        Inherits DebtPlus.Reports.Template.Forms.ReportParametersForm

        'Form overrides dispose to clean up the component list.
        <System.Diagnostics.DebuggerNonUserCode()> _
        Protected Overrides Sub Dispose(ByVal disposing As Boolean)
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
                If vueLanguages IsNot Nothing Then
                    vueLanguages.Dispose()
                End If
                ds.Dispose()
            End If
            MyBase.Dispose(disposing)
        End Sub

        'Required by the Windows Form Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Windows Form Designer
        'It can be modified using the Windows Form Designer.  
        'Do not modify it using the code editor.
        <System.Diagnostics.DebuggerStepThrough()> _
        Private Sub InitializeComponent()
            Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl
            Me.LookUpEdit_language = New DevExpress.XtraEditors.LookUpEdit
            Me.GroupControl1 = New DevExpress.XtraEditors.GroupControl
            Me.ClientName1 = New DevExpress.XtraEditors.TextEdit
            Me.CheckEdit2 = New DevExpress.XtraEditors.CheckEdit
            Me.CheckEdit1 = New DevExpress.XtraEditors.CheckEdit
            Me.GroupControl2 = New DevExpress.XtraEditors.GroupControl
            Me.ClientName2 = New DevExpress.XtraEditors.TextEdit
            Me.CheckEdit3 = New DevExpress.XtraEditors.CheckEdit
            Me.CheckEdit4 = New DevExpress.XtraEditors.CheckEdit
            CType(Me.DxErrorProvider1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LookUpEdit_language.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.GroupControl1.SuspendLayout()
            CType(Me.ClientName1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.CheckEdit2.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.CheckEdit1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.GroupControl2, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.GroupControl2.SuspendLayout()
            CType(Me.ClientName2.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.CheckEdit3.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.CheckEdit4.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.SuspendLayout()
            '
            'ButtonOK
            '
            Me.ButtonOK.Enabled = True
            Me.ButtonOK.TabIndex = 4
            '
            'ButtonCancel
            '
            Me.ButtonCancel.TabIndex = 5
            '
            'LabelControl1
            '
            Me.LabelControl1.Location = New System.Drawing.Point(15, 214)
            Me.LabelControl1.Name = "LabelControl1"
            Me.LabelControl1.Size = New System.Drawing.Size(47, 13)
            Me.LabelControl1.TabIndex = 2
            Me.LabelControl1.Text = "Language"
            '
            'LookUpEdit_language
            '
            Me.LookUpEdit_language.Location = New System.Drawing.Point(68, 211)
            Me.LookUpEdit_language.Name = "LookUpEdit_language"
            Me.LookUpEdit_language.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
            Me.LookUpEdit_language.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.LookUpEdit_language.Properties.Columns.AddRange(New DevExpress.XtraEditors.Controls.LookUpColumnInfo() {New DevExpress.XtraEditors.Controls.LookUpColumnInfo("oID", "ID", 20, DevExpress.Utils.FormatType.Numeric, "f0", False, DevExpress.Utils.HorzAlignment.[Default]), New DevExpress.XtraEditors.Controls.LookUpColumnInfo("Attribute", "Description", 20, DevExpress.Utils.FormatType.None, "", True, DevExpress.Utils.HorzAlignment.[Default], DevExpress.Data.ColumnSortOrder.Ascending)})
            Me.LookUpEdit_language.Properties.DisplayMember = "Attribute"
            Me.LookUpEdit_language.Properties.NullText = "All Languages"
            Me.LookUpEdit_language.Properties.ShowFooter = False
            Me.LookUpEdit_language.Properties.ShowHeader = False
            Me.LookUpEdit_language.Properties.ValueMember = "oID"
            Me.LookUpEdit_language.Size = New System.Drawing.Size(256, 20)
            Me.LookUpEdit_language.TabIndex = 3
            Me.LookUpEdit_language.Properties.SortColumnIndex = 1
            '
            'GroupControl1
            '
            Me.GroupControl1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                                             Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.GroupControl1.Controls.Add(Me.ClientName1)
            Me.GroupControl1.Controls.Add(Me.CheckEdit2)
            Me.GroupControl1.Controls.Add(Me.CheckEdit1)
            Me.GroupControl1.Location = New System.Drawing.Point(13, 13)
            Me.GroupControl1.Name = "GroupControl1"
            Me.GroupControl1.Size = New System.Drawing.Size(200, 93)
            Me.GroupControl1.TabIndex = 0
            Me.GroupControl1.Text = "Starting Name"
            '
            'ClientName1
            '
            Me.ClientName1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                                           Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.ClientName1.Enabled = False
            Me.ClientName1.Location = New System.Drawing.Point(39, 63)
            Me.ClientName1.Name = "ClientName1"
            Me.ClientName1.Size = New System.Drawing.Size(139, 20)
            Me.ClientName1.TabIndex = 2
            Me.ClientName1.ToolTip = "Enter the last name first as in ""Smith, John"""
            '
            'CheckEdit2
            '
            Me.CheckEdit2.Location = New System.Drawing.Point(8, 61)
            Me.CheckEdit2.Name = "CheckEdit2"
            Me.CheckEdit2.Properties.Caption = ""
            Me.CheckEdit2.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
            Me.CheckEdit2.Properties.NullStyle = DevExpress.XtraEditors.Controls.StyleIndeterminate.Unchecked
            Me.CheckEdit2.Properties.RadioGroupIndex = 0
            Me.CheckEdit2.Size = New System.Drawing.Size(24, 19)
            Me.CheckEdit2.TabIndex = 1
            Me.CheckEdit2.TabStop = False
            '
            'CheckEdit1
            '
            Me.CheckEdit1.EditValue = True
            Me.CheckEdit1.Location = New System.Drawing.Point(6, 26)
            Me.CheckEdit1.Name = "CheckEdit1"
            Me.CheckEdit1.Properties.Caption = "First Name"
            Me.CheckEdit1.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
            Me.CheckEdit1.Properties.NullStyle = DevExpress.XtraEditors.Controls.StyleIndeterminate.Unchecked
            Me.CheckEdit1.Properties.RadioGroupIndex = 0
            Me.CheckEdit1.Size = New System.Drawing.Size(75, 19)
            Me.CheckEdit1.TabIndex = 0
            '
            'GroupControl2
            '
            Me.GroupControl2.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                                             Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.GroupControl2.Controls.Add(Me.ClientName2)
            Me.GroupControl2.Controls.Add(Me.CheckEdit3)
            Me.GroupControl2.Controls.Add(Me.CheckEdit4)
            Me.GroupControl2.Location = New System.Drawing.Point(13, 112)
            Me.GroupControl2.Name = "GroupControl2"
            Me.GroupControl2.Size = New System.Drawing.Size(200, 93)
            Me.GroupControl2.TabIndex = 1
            Me.GroupControl2.Text = "Ending Name"
            '
            'ClientName2
            '
            Me.ClientName2.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                                           Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.ClientName2.Enabled = False
            Me.ClientName2.Location = New System.Drawing.Point(39, 63)
            Me.ClientName2.Name = "ClientName2"
            Me.ClientName2.Size = New System.Drawing.Size(139, 20)
            Me.ClientName2.TabIndex = 2
            Me.ClientName2.ToolTip = "Enter the last name first as in ""Smith, John"""
            '
            'CheckEdit3
            '
            Me.CheckEdit3.Location = New System.Drawing.Point(8, 61)
            Me.CheckEdit3.Name = "CheckEdit3"
            Me.CheckEdit3.Properties.Caption = ""
            Me.CheckEdit3.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
            Me.CheckEdit3.Properties.NullStyle = DevExpress.XtraEditors.Controls.StyleIndeterminate.Unchecked
            Me.CheckEdit3.Properties.RadioGroupIndex = 1
            Me.CheckEdit3.Size = New System.Drawing.Size(24, 19)
            Me.CheckEdit3.TabIndex = 1
            Me.CheckEdit3.TabStop = False
            '
            'CheckEdit4
            '
            Me.CheckEdit4.EditValue = True
            Me.CheckEdit4.Location = New System.Drawing.Point(6, 26)
            Me.CheckEdit4.Name = "CheckEdit4"
            Me.CheckEdit4.Properties.Caption = "Last Name"
            Me.CheckEdit4.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
            Me.CheckEdit4.Properties.NullStyle = DevExpress.XtraEditors.Controls.StyleIndeterminate.Unchecked
            Me.CheckEdit4.Properties.RadioGroupIndex = 1
            Me.CheckEdit4.Size = New System.Drawing.Size(75, 19)
            Me.CheckEdit4.TabIndex = 0
            '
            'NameReportParametersForm
            '
            Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
            Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
            Me.ClientSize = New System.Drawing.Size(336, 242)
            Me.Controls.Add(Me.GroupControl2)
            Me.Controls.Add(Me.GroupControl1)
            Me.Controls.Add(Me.LookUpEdit_language)
            Me.Controls.Add(Me.LabelControl1)
            Me.Name = "NameReportParametersForm"
            Me.Controls.SetChildIndex(Me.LabelControl1, 0)
            Me.Controls.SetChildIndex(Me.LookUpEdit_language, 0)
            Me.Controls.SetChildIndex(Me.GroupControl1, 0)
            Me.Controls.SetChildIndex(Me.ButtonOK, 0)
            Me.Controls.SetChildIndex(Me.ButtonCancel, 0)
            Me.Controls.SetChildIndex(Me.GroupControl2, 0)
            CType(Me.DxErrorProvider1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LookUpEdit_language.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).EndInit()
            Me.GroupControl1.ResumeLayout(False)
            CType(Me.ClientName1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.CheckEdit2.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.CheckEdit1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.GroupControl2, System.ComponentModel.ISupportInitialize).EndInit()
            Me.GroupControl2.ResumeLayout(False)
            CType(Me.ClientName2.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.CheckEdit3.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.CheckEdit4.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            Me.ResumeLayout(False)
            Me.PerformLayout()

        End Sub
        Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents LookUpEdit_language As DevExpress.XtraEditors.LookUpEdit
        Friend WithEvents GroupControl1 As DevExpress.XtraEditors.GroupControl
        Friend WithEvents CheckEdit2 As DevExpress.XtraEditors.CheckEdit
        Friend WithEvents CheckEdit1 As DevExpress.XtraEditors.CheckEdit
        Friend WithEvents GroupControl2 As DevExpress.XtraEditors.GroupControl
        Friend WithEvents CheckEdit3 As DevExpress.XtraEditors.CheckEdit
        Friend WithEvents CheckEdit4 As DevExpress.XtraEditors.CheckEdit
        Friend WithEvents ClientName1 As DevExpress.XtraEditors.TextEdit
        Friend WithEvents ClientName2 As DevExpress.XtraEditors.TextEdit
    End Class
End Namespace