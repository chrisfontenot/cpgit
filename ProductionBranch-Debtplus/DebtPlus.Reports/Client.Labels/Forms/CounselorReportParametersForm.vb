#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}
#End Region

Namespace Client.Labels.Forms
    Public Class CounselorReportParametersForm

        Dim ds As New System.Data.DataSet("ds")
        Dim vueLanguages As System.Data.DataView = Nothing
        Dim vueCounselors As System.Data.DataView = Nothing


        Public Sub New()
            MyBase.New()
            InitializeComponent()
            AddHandler Me.Load, AddressOf Form_Load
        End Sub

        Public ReadOnly Property Parameter_Language() As System.Int32
            Get
                If LookUpEdit_language.EditValue Is Nothing OrElse LookUpEdit_language.EditValue Is System.DBNull.Value Then
                    Return -1
                End If
                Return Convert.ToInt32(LookUpEdit_language.EditValue)
            End Get
        End Property

        Public ReadOnly Property Parameter_Counselor() As Object
            Get
                If LookUpEdit_Counselor.EditValue Is System.DBNull.Value Then
                    Return Nothing
                End If
                Return LookUpEdit_Counselor.EditValue
            End Get
        End Property

        Private Sub Form_Load(ByVal sender As Object, ByVal e As System.EventArgs)

            ' Load the language definitions
            Dim tbl As System.Data.DataTable = ds.Tables("AttributeTypes")
            If tbl Is Nothing Then
                Try
                    Using cmd As SqlClient.SqlCommand = New System.Data.SqlClient.SqlCommand()
                        With cmd
                            .Connection = New System.Data.SqlClient.SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
                            .CommandText = "SELECT [oID], [Attribute] FROM AttributeTypes WITH (NOLOCK) WHERE [grouping] = 'LANGUAGE'"
                            .CommandType = CommandType.Text
                            .CommandTimeout = 0
                        End With

                        Using da As New SqlClient.SqlDataAdapter(cmd)
                            da.Fill(ds, "AttributeTypes")
                            tbl = ds.Tables("AttributeTypes")
                        End Using
                    End Using

                Catch ex As SqlClient.SqlException
                    DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error reading AttributeTypes table")
                End Try
            End If

            With LookUpEdit_language
                vueLanguages = New System.Data.DataView(tbl, String.Empty, "Attribute", DataViewRowState.CurrentRows)
                .Properties.DataSource = vueLanguages
            End With

            ' Load the Counselor definitions
            tbl = ds.Tables("counselors")
            If tbl Is Nothing Then
                Try
                    Using cmd As SqlClient.SqlCommand = New System.Data.SqlClient.SqlCommand()
                        With cmd
                            .Connection = New System.Data.SqlClient.SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
                            .CommandText = "SELECT [Counselor], [name] FROM view_counselors WITH (NOLOCK) WHERE [ActiveFlag] <> 0"
                            .CommandType = CommandType.Text
                            .CommandTimeout = 0
                        End With

                        Using da As New SqlClient.SqlDataAdapter(cmd)
                            da.Fill(ds, "counselors")
                            tbl = ds.Tables("counselors")
                        End Using
                    End Using

                Catch ex As SqlClient.SqlException
                    DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error reading Counselors table")
                End Try
            End If

            With LookUpEdit_Counselor
                vueCounselors = New System.Data.DataView(tbl, String.Empty, "name", DataViewRowState.CurrentRows)
                .Properties.DataSource = vueCounselors
            End With
        End Sub
    End Class
End Namespace