#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}
#End Region

Namespace Client.Labels.Forms
    Public Class NameReportParametersForm

        Dim ds As New System.Data.DataSet("ds")
        Dim vueLanguages As System.Data.DataView = Nothing


        Public Sub New()
            MyBase.New()
            InitializeComponent()
            AddHandler Me.Load, AddressOf Form_Load
            AddHandler CheckEdit2.CheckedChanged, AddressOf CheckEdit2_CheckedChanged
            AddHandler CheckEdit3.CheckedChanged, AddressOf CheckEdit3_CheckedChanged
        End Sub

        Public ReadOnly Property Parameter_Language() As System.Int32
            Get
                If LookUpEdit_language.EditValue Is Nothing OrElse LookUpEdit_language.EditValue Is System.DBNull.Value Then
                    Return -1
                End If
                Return Convert.ToInt32(LookUpEdit_language.EditValue)
            End Get
        End Property

        Public Property Parameter_First() As Object
            Get
                If Not CheckEdit2.Checked Then
                    Return Nothing
                End If
                Return ClientName1.Text.Trim()
            End Get
            Set(ByVal value As Object)
                If value Is Nothing OrElse value Is System.DBNull.Value Then
                    CheckEdit2.Checked = True
                Else
                    CheckEdit2.Checked = False
                    ClientName1.Text = Convert.ToString(value).Trim()
                End If
            End Set
        End Property

        Public Property Parameter_Last() As Object
            Get
                If Not CheckEdit3.Checked Then
                    Return Nothing
                End If
                Return ClientName2.EditValue
            End Get
            Set(ByVal value As Object)
                If value Is Nothing OrElse value Is System.DBNull.Value Then
                    CheckEdit3.Checked = True
                Else
                    CheckEdit3.Checked = False
                    ClientName2.Text = Convert.ToString(value).Trim()
                End If
            End Set
        End Property

        Private Sub Form_Load(ByVal sender As Object, ByVal e As System.EventArgs)

            ' Load the language definitions
            Dim tbl As System.Data.DataTable = ds.Tables("AttributeTypes")
            If tbl Is Nothing Then
                Try
                    Using cmd As SqlClient.SqlCommand = New System.Data.SqlClient.SqlCommand()
                        With cmd
                            .Connection = New System.Data.SqlClient.SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
                            .CommandText = "SELECT [oID], [Attribute] FROM AttributeTypes WITH (NOLOCK) WHERE [grouping] = 'LANGUAGE'"
                            .CommandType = CommandType.Text
                        End With

                        Using da As New SqlClient.SqlDataAdapter(cmd)
                            da.Fill(ds, "AttributeTypes")
                            tbl = ds.Tables("AttributeTypes")
                        End Using
                    End Using

                Catch ex As SqlClient.SqlException
                    DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error reading AttributeTypes table")
                End Try
            End If

            With LookUpEdit_language
                vueLanguages = New System.Data.DataView(tbl, String.Empty, "Attribute", DataViewRowState.CurrentRows)
                .Properties.DataSource = vueLanguages
            End With
        End Sub

        Private Sub CheckEdit2_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
            ClientName1.Enabled = CheckEdit2.Checked
        End Sub

        Private Sub CheckEdit3_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
            ClientName2.Enabled = CheckEdit3.Checked
        End Sub
    End Class
End Namespace