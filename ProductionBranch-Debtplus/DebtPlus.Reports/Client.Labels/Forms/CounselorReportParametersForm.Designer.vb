Namespace Client.Labels.Forms
    <Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
    Partial Class CounselorReportParametersForm
        Inherits DebtPlus.Reports.Template.Forms.ReportParametersForm

        'Form overrides dispose to clean up the component list.
        <System.Diagnostics.DebuggerNonUserCode()> _
        Protected Overrides Sub Dispose(ByVal disposing As Boolean)
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
                If vueLanguages IsNot Nothing Then
                    vueLanguages.Dispose()
                End If
                If vueCounselors IsNot Nothing Then
                    vueCounselors.Dispose()
                End If
                ds.Dispose()
            End If
            MyBase.Dispose(disposing)
        End Sub

        'Required by the Windows Form Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Windows Form Designer
        'It can be modified using the Windows Form Designer.  
        'Do not modify it using the code editor.
        <System.Diagnostics.DebuggerStepThrough()> _
        Private Sub InitializeComponent()
            Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl
            Me.LookUpEdit_language = New DevExpress.XtraEditors.LookUpEdit
            Me.LabelControl2 = New DevExpress.XtraEditors.LabelControl
            Me.LookUpEdit_Counselor = New DevExpress.XtraEditors.LookUpEdit
            CType(Me.DxErrorProvider1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LookUpEdit_language.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LookUpEdit_Counselor.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.SuspendLayout()
            '
            'ButtonOK
            '
            Me.ButtonOK.Enabled = True
            Me.ButtonOK.TabIndex = 4
            '
            'ButtonCancel
            '
            Me.ButtonCancel.TabIndex = 5
            '
            'LabelControl1
            '
            Me.LabelControl1.Location = New System.Drawing.Point(14, 52)
            Me.LabelControl1.Name = "LabelControl1"
            Me.LabelControl1.Size = New System.Drawing.Size(47, 13)
            Me.LabelControl1.TabIndex = 2
            Me.LabelControl1.Text = "Language"
            '
            'LookUpEdit_language
            '
            Me.LookUpEdit_language.Location = New System.Drawing.Point(67, 49)
            Me.LookUpEdit_language.Name = "LookUpEdit_language"
            Me.LookUpEdit_language.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
            Me.LookUpEdit_language.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.LookUpEdit_language.Properties.Columns.AddRange(New DevExpress.XtraEditors.Controls.LookUpColumnInfo() {New DevExpress.XtraEditors.Controls.LookUpColumnInfo("oID", "ID", 20, DevExpress.Utils.FormatType.Numeric, "f0", False, DevExpress.Utils.HorzAlignment.[Default]), New DevExpress.XtraEditors.Controls.LookUpColumnInfo("Attribute", "Description", 20, DevExpress.Utils.FormatType.None, "", True, DevExpress.Utils.HorzAlignment.[Default], DevExpress.Data.ColumnSortOrder.Ascending)})
            Me.LookUpEdit_language.Properties.DisplayMember = "Attribute"
            Me.LookUpEdit_language.Properties.NullText = "All Languages"
            Me.LookUpEdit_language.Properties.ShowFooter = False
            Me.LookUpEdit_language.Properties.ShowHeader = False
            Me.LookUpEdit_language.Properties.ValueMember = "oID"
            Me.LookUpEdit_language.Size = New System.Drawing.Size(163, 20)
            Me.LookUpEdit_language.TabIndex = 3
            Me.LookUpEdit_language.Properties.SortColumnIndex = 1
            '
            'LabelControl2
            '
            Me.LabelControl2.Location = New System.Drawing.Point(14, 26)
            Me.LabelControl2.Name = "LabelControl2"
            Me.LabelControl2.Size = New System.Drawing.Size(48, 13)
            Me.LabelControl2.TabIndex = 0
            Me.LabelControl2.Text = "Counselor"
            '
            'LookUpEdit_Counselor
            '
            Me.LookUpEdit_Counselor.Location = New System.Drawing.Point(67, 23)
            Me.LookUpEdit_Counselor.Name = "LookUpEdit_Counselor"
            Me.LookUpEdit_Counselor.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
            Me.LookUpEdit_Counselor.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.LookUpEdit_Counselor.Properties.Columns.AddRange(New DevExpress.XtraEditors.Controls.LookUpColumnInfo() {New DevExpress.XtraEditors.Controls.LookUpColumnInfo("Counselor", "ID", 20, DevExpress.Utils.FormatType.Numeric, "f0", False, DevExpress.Utils.HorzAlignment.[Default]), New DevExpress.XtraEditors.Controls.LookUpColumnInfo("name", "Description", 20, DevExpress.Utils.FormatType.None, "", True, DevExpress.Utils.HorzAlignment.[Default], DevExpress.Data.ColumnSortOrder.Ascending)})
            Me.LookUpEdit_Counselor.Properties.DisplayMember = "name"
            Me.LookUpEdit_Counselor.Properties.NullText = "All Counselors"
            Me.LookUpEdit_Counselor.Properties.ShowFooter = False
            Me.LookUpEdit_Counselor.Properties.ShowHeader = False
            Me.LookUpEdit_Counselor.Properties.ValueMember = "Counselor"
            Me.LookUpEdit_Counselor.Size = New System.Drawing.Size(163, 20)
            Me.LookUpEdit_Counselor.TabIndex = 1
            Me.LookUpEdit_Counselor.Properties.SortColumnIndex = 1
            '
            'CounselorReportParametersForm
            '
            Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
            Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
            Me.ClientSize = New System.Drawing.Size(336, 162)
            Me.Controls.Add(Me.LabelControl2)
            Me.Controls.Add(Me.LookUpEdit_Counselor)
            Me.Controls.Add(Me.LabelControl1)
            Me.Controls.Add(Me.LookUpEdit_language)
            Me.Name = "CounselorReportParametersForm"
            Me.Controls.SetChildIndex(Me.LookUpEdit_language, 0)
            Me.Controls.SetChildIndex(Me.LabelControl1, 0)
            Me.Controls.SetChildIndex(Me.ButtonOK, 0)
            Me.Controls.SetChildIndex(Me.ButtonCancel, 0)
            Me.Controls.SetChildIndex(Me.LookUpEdit_Counselor, 0)
            Me.Controls.SetChildIndex(Me.LabelControl2, 0)
            CType(Me.DxErrorProvider1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LookUpEdit_language.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LookUpEdit_Counselor.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            Me.ResumeLayout(False)
            Me.PerformLayout()

        End Sub
        Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents LookUpEdit_language As DevExpress.XtraEditors.LookUpEdit
        Friend WithEvents LabelControl2 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents LookUpEdit_Counselor As DevExpress.XtraEditors.LookUpEdit
    End Class
End Namespace