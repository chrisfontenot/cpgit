#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}
#End Region

Namespace Client.Labels.Forms
    Public Class IndicatorReportParametersForm
        Implements IBindableComponent

        Dim ds As New System.Data.DataSet("ds")
        Dim vueLanguages As System.Data.DataView = Nothing
        Dim vueIndicators As System.Data.DataView = Nothing

        Public Sub New()
            MyBase.New()
            InitializeComponent()
            AddHandler Me.Load, AddressOf Form_Load
            AddHandler LookUpEdit_Indicator.EditValueChanging, AddressOf LookUpEdit_Indicator_EditValueChanging
        End Sub

        Public ReadOnly Property Parameter_Language() As System.Int32
            Get
                If LookUpEdit_language.EditValue Is Nothing OrElse LookUpEdit_language.EditValue Is System.DBNull.Value Then
                    Return -1
                End If
                Return Convert.ToInt32(LookUpEdit_language.EditValue)
            End Get
        End Property

        Private privateParameter_Indicator As Object = Nothing
        Public Property Parameter_Indicator() As Object
            Get
                Return privateParameter_Indicator
            End Get
            Set(ByVal Value As Object)
                privateParameter_Indicator = Value
            End Set
        End Property

        Public Property Parameter_ResetIndicator() As Boolean
            Get
                Return CheckEdit1.Checked
            End Get
            Set(ByVal value As Boolean)
                CheckEdit1.Checked = value
            End Set
        End Property

        Private Sub Form_Load(ByVal sender As Object, ByVal e As System.EventArgs)

            ' Load the language definitions
            Dim tbl As System.Data.DataTable = ds.Tables("AttributeTypes")
            If tbl Is Nothing Then
                Try
                    Using cmd As SqlClient.SqlCommand = New System.Data.SqlClient.SqlCommand()
                        With cmd
                            .Connection = New System.Data.SqlClient.SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
                            .CommandText = "SELECT [oID], [Attribute] FROM AttributeTypes WITH (NOLOCK) WHERE [grouping] = 'LANGUAGE'"
                            .CommandType = CommandType.Text
                        End With

                        Using da As New SqlClient.SqlDataAdapter(cmd)
                            da.Fill(ds, "AttributeTypes")
                            tbl = ds.Tables("AttributeTypes")
                        End Using
                    End Using

                Catch ex As SqlClient.SqlException
                    DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error reading AttributeTypes table")
                End Try
            End If

            With LookUpEdit_language
                vueLanguages = New System.Data.DataView(tbl, String.Empty, "Attribute", DataViewRowState.CurrentRows)
                .Properties.DataSource = vueLanguages
            End With

            ' Load the Indicator definitions
            tbl = ds.Tables("indicators")
            If tbl Is Nothing Then
                Try
                    Using cmd As SqlClient.SqlCommand = New System.Data.SqlClient.SqlCommand()
                        With cmd
                            .Connection = New System.Data.SqlClient.SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
                            .CommandText = "SELECT [indicator], [description] FROM Indicators WITH (NOLOCK)"
                            .CommandType = CommandType.Text
                            .CommandTimeout = 0
                        End With

                        Using da As New SqlClient.SqlDataAdapter(cmd)
                            da.Fill(ds, "indicators")
                            tbl = ds.Tables("indicators")
                        End Using
                    End Using

                Catch ex As SqlClient.SqlException
                    DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error reading Indicators table")
                End Try
            End If

            With LookUpEdit_Indicator
                vueIndicators = New System.Data.DataView(tbl, String.Empty, "description", DataViewRowState.CurrentRows)
                .Properties.DataSource = vueIndicators

                ' Use the current setting for the value if possible
                If Parameter_Indicator Is Nothing Then
                    If vueIndicators.Count > 0 Then
                        Parameter_Indicator = vueIndicators(0)("indicator")
                    End If
                End If

                ' Bind the control to the parameter setting
                .EditValue = Parameter_Indicator
                .DataBindings.Clear()
                .DataBindings.Add("EditValue", Me, "Parameter_Indicator")
            End With

            ' Disable the OK button until an indicator is choosen
            ButtonOK.Enabled = Parameter_Indicator IsNot Nothing AndAlso Parameter_Indicator IsNot System.DBNull.Value
        End Sub

        Private Sub LookUpEdit_Indicator_EditValueChanging(ByVal sender As Object, ByVal e As DevExpress.XtraEditors.Controls.ChangingEventArgs)
            ButtonOK.Enabled = e.NewValue IsNot Nothing AndAlso e.NewValue IsNot System.DBNull.Value
        End Sub
    End Class
End Namespace