Namespace Client.Labels.Forms
    <Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
    Partial Class DateReportParametersForm
        Inherits DebtPlus.Reports.Template.Forms.DateReportParametersForm

        'Form overrides dispose to clean up the component list.
        <System.Diagnostics.DebuggerNonUserCode()> _
        Protected Overrides Sub Dispose(ByVal disposing As Boolean)
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
                If vue IsNot Nothing Then
                    vue.Dispose()
                End If
                ds.Dispose()
            End If
            MyBase.Dispose(disposing)
        End Sub

        'Required by the Windows Form Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Windows Form Designer
        'It can be modified using the Windows Form Designer.  
        'Do not modify it using the code editor.
        <System.Diagnostics.DebuggerStepThrough()> _
        Private Sub InitializeComponent()
            Dim ComboboxItem2 As DebtPlus.Data.Controls.ComboboxItem = New DebtPlus.Data.Controls.ComboboxItem
            Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl
            Me.LookUpEdit_language = New DevExpress.XtraEditors.LookUpEdit
            CType(Me.XrGroup_param_08_1, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.XrGroup_param_08_1.SuspendLayout()
            CType(Me.XrCombo_param_08_1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.XrDate_param_08_2.Properties.VistaTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.XrDate_param_08_2.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.XrDate_param_08_1.Properties.VistaTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.XrDate_param_08_1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.DxErrorProvider1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LookUpEdit_language.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.SuspendLayout()
            '
            'ButtonOK
            '
            Me.ButtonOK.TabIndex = 3
            '
            'ButtonCancel
            '
            Me.ButtonCancel.TabIndex = 4
            '
            'LabelControl1
            '
            Me.LabelControl1.Location = New System.Drawing.Point(13, 136)
            Me.LabelControl1.Name = "LabelControl1"
            Me.LabelControl1.Size = New System.Drawing.Size(47, 13)
            Me.LabelControl1.TabIndex = 1
            Me.LabelControl1.Text = "Language"
            '
            'LookUpEdit_language
            '
            Me.LookUpEdit_language.Location = New System.Drawing.Point(66, 133)
            Me.LookUpEdit_language.Name = "LookUpEdit_language"
            Me.LookUpEdit_language.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
            Me.LookUpEdit_language.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.LookUpEdit_language.Properties.Columns.AddRange(New DevExpress.XtraEditors.Controls.LookUpColumnInfo() {New DevExpress.XtraEditors.Controls.LookUpColumnInfo("oID", "ID", 20, DevExpress.Utils.FormatType.Numeric, "f0", False, DevExpress.Utils.HorzAlignment.[Default]), New DevExpress.XtraEditors.Controls.LookUpColumnInfo("Attribute", "Description", 20, DevExpress.Utils.FormatType.None, "", True, DevExpress.Utils.HorzAlignment.[Default], DevExpress.Data.ColumnSortOrder.Ascending)})
            Me.LookUpEdit_language.Properties.DisplayMember = "Attribute"
            Me.LookUpEdit_language.Properties.NullText = "All Languages"
            Me.LookUpEdit_language.Properties.ShowFooter = False
            Me.LookUpEdit_language.Properties.ShowHeader = False
            Me.LookUpEdit_language.Properties.ValueMember = "oID"
            Me.LookUpEdit_language.Size = New System.Drawing.Size(258, 20)
            Me.LookUpEdit_language.TabIndex = 2
            Me.LookUpEdit_language.Properties.SortColumnIndex = 1
            '
            'DateReportParametersForm
            '
            Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
            Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
            Me.ClientSize = New System.Drawing.Size(336, 162)
            Me.Controls.Add(Me.LabelControl1)
            Me.Controls.Add(Me.LookUpEdit_language)
            Me.Name = "DateReportParametersForm"
            Me.Controls.SetChildIndex(Me.LookUpEdit_language, 0)
            Me.Controls.SetChildIndex(Me.LabelControl1, 0)
            Me.Controls.SetChildIndex(Me.ButtonOK, 0)
            Me.Controls.SetChildIndex(Me.ButtonCancel, 0)
            Me.Controls.SetChildIndex(Me.XrGroup_param_08_1, 0)
            CType(Me.XrGroup_param_08_1, System.ComponentModel.ISupportInitialize).EndInit()
            Me.XrGroup_param_08_1.ResumeLayout(False)
            Me.XrGroup_param_08_1.PerformLayout()
            CType(Me.XrCombo_param_08_1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.XrDate_param_08_2.Properties.VistaTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.XrDate_param_08_2.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.XrDate_param_08_1.Properties.VistaTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.XrDate_param_08_1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.DxErrorProvider1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LookUpEdit_language.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            Me.ResumeLayout(False)
            Me.PerformLayout()

        End Sub
        Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents LookUpEdit_language As DevExpress.XtraEditors.LookUpEdit
    End Class
End Namespace