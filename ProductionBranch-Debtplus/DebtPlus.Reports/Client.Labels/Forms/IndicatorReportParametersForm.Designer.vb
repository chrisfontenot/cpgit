Namespace Client.Labels.Forms
    <Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
    Partial Class IndicatorReportParametersForm
        Inherits DebtPlus.Reports.Template.Forms.ReportParametersForm

        'Form overrides dispose to clean up the component list.
        <System.Diagnostics.DebuggerNonUserCode()> _
        Protected Overrides Sub Dispose(ByVal disposing As Boolean)
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
                If vueLanguages IsNot Nothing Then
                    vueLanguages.Dispose()
                End If
                If vueIndicators IsNot Nothing Then
                    vueIndicators.Dispose()
                End If
                ds.Dispose()
            End If
            MyBase.Dispose(disposing)
        End Sub

        'Required by the Windows Form Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Windows Form Designer
        'It can be modified using the Windows Form Designer.  
        'Do not modify it using the code editor.
        <System.Diagnostics.DebuggerStepThrough()> _
        Private Sub InitializeComponent()
            Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl
            Me.LookUpEdit_language = New DevExpress.XtraEditors.LookUpEdit
            Me.LabelControl2 = New DevExpress.XtraEditors.LabelControl
            Me.LookUpEdit_Indicator = New DevExpress.XtraEditors.LookUpEdit
            Me.CheckEdit1 = New DevExpress.XtraEditors.CheckEdit
            CType(Me.DxErrorProvider1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LookUpEdit_language.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LookUpEdit_Indicator.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.CheckEdit1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.SuspendLayout()
            '
            'ButtonOK
            '
            Me.ButtonOK.TabIndex = 4
            '
            'ButtonCancel
            '
            Me.ButtonCancel.TabIndex = 5
            '
            'LabelControl1
            '
            Me.LabelControl1.Location = New System.Drawing.Point(14, 52)
            Me.LabelControl1.Name = "LabelControl1"
            Me.LabelControl1.Size = New System.Drawing.Size(47, 13)
            Me.LabelControl1.TabIndex = 2
            Me.LabelControl1.Text = "Language"
            '
            'LookUpEdit_language
            '
            Me.LookUpEdit_language.Location = New System.Drawing.Point(67, 49)
            Me.LookUpEdit_language.Name = "LookUpEdit_language"
            Me.LookUpEdit_language.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
            Me.LookUpEdit_language.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.LookUpEdit_language.Properties.Columns.AddRange(New DevExpress.XtraEditors.Controls.LookUpColumnInfo() {New DevExpress.XtraEditors.Controls.LookUpColumnInfo("oID", "ID", 20, DevExpress.Utils.FormatType.Numeric, "f0", False, DevExpress.Utils.HorzAlignment.[Default]), New DevExpress.XtraEditors.Controls.LookUpColumnInfo("Attribute", "Description", 20, DevExpress.Utils.FormatType.None, "", True, DevExpress.Utils.HorzAlignment.[Default], DevExpress.Data.ColumnSortOrder.Ascending)})
            Me.LookUpEdit_language.Properties.DisplayMember = "Attribute"
            Me.LookUpEdit_language.Properties.NullText = "All Languages"
            Me.LookUpEdit_language.Properties.ShowFooter = False
            Me.LookUpEdit_language.Properties.ShowHeader = False
            Me.LookUpEdit_language.Properties.ValueMember = "oID"
            Me.LookUpEdit_language.Size = New System.Drawing.Size(163, 20)
            Me.LookUpEdit_language.TabIndex = 3
            Me.LookUpEdit_language.Properties.SortColumnIndex = 1
            '
            'LabelControl2
            '
            Me.LabelControl2.Location = New System.Drawing.Point(14, 26)
            Me.LabelControl2.Name = "LabelControl2"
            Me.LabelControl2.Size = New System.Drawing.Size(43, 13)
            Me.LabelControl2.TabIndex = 0
            Me.LabelControl2.Text = "Indicator"
            '
            'LookUpEdit_Indicator
            '
            Me.LookUpEdit_Indicator.Location = New System.Drawing.Point(67, 23)
            Me.LookUpEdit_Indicator.Name = "LookUpEdit_Indicator"
            Me.LookUpEdit_Indicator.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[False]
            Me.LookUpEdit_Indicator.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.LookUpEdit_Indicator.Properties.Columns.AddRange(New DevExpress.XtraEditors.Controls.LookUpColumnInfo() {New DevExpress.XtraEditors.Controls.LookUpColumnInfo("indicator", "ID", 20, DevExpress.Utils.FormatType.Numeric, "f0", False, DevExpress.Utils.HorzAlignment.[Default]), New DevExpress.XtraEditors.Controls.LookUpColumnInfo("description", "Description", 20, DevExpress.Utils.FormatType.None, "", True, DevExpress.Utils.HorzAlignment.[Default], DevExpress.Data.ColumnSortOrder.Ascending)})
            Me.LookUpEdit_Indicator.Properties.DisplayMember = "description"
            Me.LookUpEdit_Indicator.Properties.NullText = "A choice is required here"
            Me.LookUpEdit_Indicator.Properties.ShowFooter = False
            Me.LookUpEdit_Indicator.Properties.ShowHeader = False
            Me.LookUpEdit_Indicator.Properties.ValueMember = "indicator"
            Me.LookUpEdit_Indicator.Size = New System.Drawing.Size(163, 20)
            Me.LookUpEdit_Indicator.TabIndex = 1
            Me.LookUpEdit_Indicator.Properties.SortColumnIndex = 1
            '
            'CheckEdit1
            '
            Me.CheckEdit1.EditValue = True
            Me.CheckEdit1.Location = New System.Drawing.Point(14, 131)
            Me.CheckEdit1.Name = "CheckEdit1"
            Me.CheckEdit1.Properties.Caption = "Set indicator for all printed labels"
            Me.CheckEdit1.Size = New System.Drawing.Size(186, 19)
            Me.CheckEdit1.TabIndex = 6
            '
            'IndicatorReportParametersForm
            '
            Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
            Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
            Me.ClientSize = New System.Drawing.Size(336, 162)
            Me.Controls.Add(Me.CheckEdit1)
            Me.Controls.Add(Me.LabelControl2)
            Me.Controls.Add(Me.LookUpEdit_Indicator)
            Me.Controls.Add(Me.LabelControl1)
            Me.Controls.Add(Me.LookUpEdit_language)
            Me.Name = "IndicatorReportParametersForm"
            Me.Controls.SetChildIndex(Me.LookUpEdit_language, 0)
            Me.Controls.SetChildIndex(Me.LabelControl1, 0)
            Me.Controls.SetChildIndex(Me.ButtonOK, 0)
            Me.Controls.SetChildIndex(Me.ButtonCancel, 0)
            Me.Controls.SetChildIndex(Me.LookUpEdit_Indicator, 0)
            Me.Controls.SetChildIndex(Me.LabelControl2, 0)
            Me.Controls.SetChildIndex(Me.CheckEdit1, 0)
            CType(Me.DxErrorProvider1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LookUpEdit_language.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LookUpEdit_Indicator.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.CheckEdit1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            Me.ResumeLayout(False)
            Me.PerformLayout()

        End Sub
        Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents LookUpEdit_language As DevExpress.XtraEditors.LookUpEdit
        Friend WithEvents LabelControl2 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents LookUpEdit_Indicator As DevExpress.XtraEditors.LookUpEdit
        Friend WithEvents CheckEdit1 As DevExpress.XtraEditors.CheckEdit
    End Class
End Namespace