#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}
#End Region

Option Compare Binary
Option Explicit On
Option Strict On

Imports DebtPlus.Reports.Client.Labels.Formatting

Namespace Client.Labels

    Public Class ClientLabelsReport
        Inherits BasicLabelDefinition

        Public Sub New()
            MyBase.New()
            InitializeComponent()
            AddHandler XrLabel_Text.BeforePrint, AddressOf XrLabel_Text_BeforePrint
            AddHandler BeforePrint, AddressOf Report_BeforePrint

            ' Define the storage information for the label options
            CurrentStg.Parameter_Include_ClientID = False
            CurrentStg.Parameter_Language = 0
            CurrentStg.Parameter_Set_Indicator = False
            CurrentStg.Parameter_First = Nothing
            CurrentStg.Parameter_Last = Nothing
            CurrentStg.parameter_Type = Selection_Type.ByClientID
            CurrentStg.Parameter_A = True
        End Sub

        Public Sub New(ByVal stg As Storage)
            MyClass.New()
            CurrentStg = stg
        End Sub

        Public Sub New(ByVal ReportType As Selection_Type)
            MyClass.New()
            CurrentStg.parameter_Type = ReportType
        End Sub

#Region "Storage"
        ' Types of the client labels
        Public Enum Selection_Type As System.Int32
            ByClientID = 1
            ByDMPDate = 2
            ByName = 3
            ByIndicator = 4
            ByCreationDate = 5
            ByCounselor = 6
            ByOffice = 7
        End Enum

        ' Storage for the parameters used in the definition
        Public Class Storage
            Public parameter_Type As Selection_Type
            Public Parameter_Include_ClientID As Boolean
            Public Parameter_Language As Int32
            Public Parameter_First As Object
            Public Parameter_Last As Object
            Public Parameter_Set_Indicator As Boolean
            Public Parameter_Include_Barcode As Boolean

            Public Parameter_A As Boolean
            Public Parameter_PND As Boolean
            Public Parameter_PRO As Boolean
            Public Parameter_RDY As Boolean
            Public Parameter_CRE As Boolean
            Public Parameter_EX As Boolean
            Public Parameter_I As Boolean
            Public Parameter_APT As Boolean
            Public Parameter_WKS As Boolean

            Public Function ActiveStatusList() As String
                Dim sb As New System.Text.StringBuilder
                If Parameter_CRE Then
                    sb.Append(",'CRE'")
                End If
                If Parameter_APT Then
                    sb.Append(",'APT'")
                End If
                If Parameter_WKS Then
                    sb.Append(",'WKS'")
                End If
                If Parameter_PND Then
                    sb.Append(",'PND'")
                End If
                If Parameter_RDY Then
                    sb.Append(",'RDY'")
                End If
                If Parameter_PRO Then
                    sb.Append(",'PRO'")
                End If
                If Parameter_A Then
                    sb.Append(",'A','AR'")
                End If
                If Parameter_EX Then
                    sb.Append(",'EX'")
                End If
                If Parameter_I Then
                    sb.Append(",'I'")
                End If

                If sb.Length > 0 Then
                    sb.Remove(0, 1)
                    sb.Insert(0, " AND v.active_status IN (")
                    sb.Append(")")
                End If
                Return sb.ToString()
            End Function
        End Class

        Protected CurrentStg As Storage = New Storage()

#End Region
#Region "Properties"
        Public Overridable Property Parameter_Type() As Selection_Type
            Get
                Return CurrentStg.parameter_Type
            End Get
            Set(ByVal value As Selection_Type)
                CurrentStg.parameter_Type = value
            End Set
        End Property

        Public Overridable Property Parameter_First() As Object
            Get
                Return CurrentStg.Parameter_First
            End Get
            Set(ByVal value As Object)
                CurrentStg.Parameter_First = value
            End Set
        End Property

        Public Overridable Property Parameter_Last() As Object
            Get
                Return CurrentStg.Parameter_Last
            End Get
            Set(ByVal value As Object)
                CurrentStg.Parameter_Last = value
            End Set
        End Property

        Public Overridable Property Parameter_SetIndicator() As Boolean
            Get
                Return CurrentStg.Parameter_Set_Indicator
            End Get
            Set(ByVal value As Boolean)
                CurrentStg.Parameter_Set_Indicator = value
            End Set
        End Property

        Public Property Parameter_Include_ClientID() As Boolean
            Get
                Return CurrentStg.Parameter_Include_ClientID
            End Get
            Set(ByVal Value As Boolean)
                CurrentStg.Parameter_Include_ClientID = Value
            End Set
        End Property

        Public Property Parameter_Language() As Int32
            Get
                Return CurrentStg.Parameter_Language
            End Get
            Set(ByVal Value As Int32)
                CurrentStg.Parameter_Language = Value
            End Set
        End Property

        Public Property Parameter_Include_Barcode() As Boolean
            Get
                Return CurrentStg.Parameter_Include_Barcode
            End Get
            Set(ByVal value As Boolean)
                MyBase.MyParameter_Include_Barcode = value
                CurrentStg.Parameter_Include_Barcode = value
            End Set
        End Property

        Public Property Parameter_A() As Boolean
            Get
                Return CurrentStg.Parameter_A
            End Get
            Set(ByVal value As Boolean)
                CurrentStg.Parameter_A = value
            End Set
        End Property

        Public Property Parameter_CRE() As Boolean
            Get
                Return CurrentStg.Parameter_CRE
            End Get
            Set(ByVal value As Boolean)
                CurrentStg.Parameter_CRE = value
            End Set
        End Property

        Public Property Parameter_PND() As Boolean
            Get
                Return CurrentStg.Parameter_PND
            End Get
            Set(ByVal value As Boolean)
                CurrentStg.Parameter_PND = value
            End Set
        End Property

        Public Property Parameter_PRO() As Boolean
            Get
                Return CurrentStg.Parameter_PRO
            End Get
            Set(ByVal value As Boolean)
                CurrentStg.Parameter_PRO = value
            End Set
        End Property

        Public Property Parameter_RDY() As Boolean
            Get
                Return CurrentStg.Parameter_RDY
            End Get
            Set(ByVal value As Boolean)
                CurrentStg.Parameter_RDY = value
            End Set
        End Property

        Public Property Parameter_EX() As Boolean
            Get
                Return CurrentStg.Parameter_EX
            End Get
            Set(ByVal value As Boolean)
                CurrentStg.Parameter_EX = value
            End Set
        End Property

        Public Property Parameter_I() As Boolean
            Get
                Return CurrentStg.Parameter_I
            End Get
            Set(ByVal value As Boolean)
                CurrentStg.Parameter_I = value
            End Set
        End Property

        Public Property Parameter_APT() As Boolean
            Get
                Return CurrentStg.Parameter_APT
            End Get
            Set(ByVal value As Boolean)
                CurrentStg.Parameter_APT = value
            End Set
        End Property

        Public Property Parameter_WKS() As Boolean
            Get
                Return CurrentStg.Parameter_WKS
            End Get
            Set(ByVal value As Boolean)
                CurrentStg.Parameter_WKS = value
            End Set
        End Property

#End Region

        ''' <summary>
        ''' Generate the name and address information
        ''' </summary>
        Private Sub XrLabel_Text_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs)

            ' Find the data row view. If there are no rows then do not generate a response
            Dim drv As DataRowView = CType(GetCurrentRow(), DataRowView)
            If drv Is Nothing Then Return

            Dim sb As New System.Text.StringBuilder

            ' Include the client id if desired
            If Parameter_Include_ClientID Then
                sb.Append(Environment.NewLine)
                sb.Append(String.Format("{0:0000000}", GetCurrentColumnValue("client")))
            End If

            ' Include the name and address
            For Each ItemString As String In New String() {"name", "addr1", "addr2", "addr3"}
                If drv(ItemString) IsNot DBNull.Value Then
                    Dim NameString As String = Convert.ToString(drv(ItemString)).Trim()
                    If NameString <> String.Empty Then
                        sb.Append(Environment.NewLine)
                        sb.Append(NameString)
                    End If
                End If
            Next ItemString

            If sb.Length > 0 Then sb.Remove(0, 2)
            XrLabel_Text.Text = sb.ToString()
        End Sub


        ''' <summary>
        ''' Set the report parameter values
        ''' </summary>
        Public Overrides Sub SetReportParameter(ByVal Parameter As String, ByVal Value As Object)
            Select Case Parameter
                Case "First", "Indicator", "Counselor", "Office"
                    Parameter_First = Value
                Case "Last"
                    Parameter_Last = Value
                Case "SetIndicator"
                    Parameter_SetIndicator = Convert.ToBoolean(Value)
                Case "Language"
                    Parameter_Language = Convert.ToInt32(Value)
                Case "Include_ClientID"
                    Parameter_Include_ClientID = Convert.ToBoolean(Value)
                Case "Include_Barcode"
                    Parameter_Include_Barcode = Convert.ToBoolean(Value)
                Case "Type"
                    Parameter_Type = CType(Value, Selection_Type)
                Case "CRE"
                    Parameter_CRE = Convert.ToBoolean(Value)
                Case "APT"
                    Parameter_APT = Convert.ToBoolean(Value)
                Case "WKS"
                    Parameter_WKS = Convert.ToBoolean(Value)
                Case "PND"
                    Parameter_PND = Convert.ToBoolean(Value)
                Case "RDY"
                    Parameter_RDY = Convert.ToBoolean(Value)
                Case "PRO"
                    Parameter_PRO = Convert.ToBoolean(Value)
                Case "A"
                    Parameter_A = Convert.ToBoolean(Value)
                Case "EX"
                    Parameter_EX = Convert.ToBoolean(Value)
                Case "I"
                    Parameter_I = Convert.ToBoolean(Value)

                Case Else
                    MyBase.SetReportParameter(Parameter, Value)
            End Select
        End Sub


        ''' <summary>
        ''' Determine the proper processing sub-type of the report
        ''' </summary>
        Private CurrentLabelProcessor As IClientLabelDefinition
        Private Function LabelProcessor() As IClientLabelDefinition
            If CurrentLabelProcessor Is Nothing Then
                Select Case CurrentStg.parameter_Type
                    Case Selection_Type.ByClientID
                        CurrentLabelProcessor = New ClientLabelsByClientReport(CurrentStg)
                    Case Selection_Type.ByCounselor
                        CurrentLabelProcessor = New ClientLabelsByCounselorReport(CurrentStg)
                    Case Selection_Type.ByCreationDate
                        CurrentLabelProcessor = New ClientLabelsByCreationDateReport(CurrentStg)
                    Case Selection_Type.ByDMPDate
                        CurrentLabelProcessor = New ClientLabelsByDMPDateReport(CurrentStg)
                    Case Selection_Type.ByIndicator
                        CurrentLabelProcessor = New ClientLabelsByIndicatorReport(CurrentStg)
                    Case Selection_Type.ByName
                        CurrentLabelProcessor = New ClientLabelsByNameReport(CurrentStg)
                    Case Selection_Type.ByOffice
                        CurrentLabelProcessor = New ClientLabelsByOfficeReport(CurrentStg)
                    Case Else
                        Throw New ArgumentOutOfRangeException("Parameter_Type")
                End Select
            End If
            Return CurrentLabelProcessor
        End Function


        ''' <summary>
        ''' Do we need parameters for the report?
        ''' </summary>
        Public Overrides Function NeedParameters() As Boolean
            Return LabelProcessor.NeedParameters()
        End Function


        ''' <summary>
        ''' Request the report parameters based upon the sub-type
        ''' </summary>
        Public Overrides Function RequestReportParameters() As DialogResult
            Return LabelProcessor.RequestReportParameters()
        End Function


        ''' <summary>
        ''' Process the REPORT BEFOREPRINT Event
        ''' </summary>
        Private Sub Report_BeforePrint(ByVal Sender As Object, ByVal e As EventArgs)
            Dim tbl As DataTable = LabelProcessor.BeforePrint()
            DefineDataSet(tbl)
        End Sub
    End Class


    ''' <summary>
    ''' Implemented by the various sub-types of this report
    ''' </summary>
    Friend Interface IClientLabelDefinition
        Function NeedParameters() As Boolean
        Function RequestReportParameters() As DialogResult
        Function BeforePrint() As DataTable
    End Interface
End Namespace
