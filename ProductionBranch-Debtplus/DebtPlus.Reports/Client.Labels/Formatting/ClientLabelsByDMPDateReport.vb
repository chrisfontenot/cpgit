#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}
#End Region

Option Compare Binary
Option Explicit On
Option Strict On

Imports DebtPlus.Reports.Client.Labels.Forms

Namespace Client.Labels.Formatting

    Public Class ClientLabelsByDMPDateReport
        Implements IClientLabelDefinition

        Private stg As DebtPlus.Reports.Client.Labels.ClientLabelsReport.Storage

        Public Sub New(ByVal stg As DebtPlus.Reports.Client.Labels.ClientLabelsReport.Storage)
            Me.stg = stg
        End Sub

        Public Function NeedParameters() As Boolean Implements IClientLabelDefinition.NeedParameters
            Return True
        End Function

        Public Function RequestReportParameters() As System.Windows.Forms.DialogResult Implements IClientLabelDefinition.RequestReportParameters
            Dim answer As System.Windows.Forms.DialogResult = DialogResult.OK

            If NeedParameters() Then
                Using frm As New DateReportParametersForm()
                    With frm
                        answer = .ShowDialog()
                        stg.Parameter_First = .Parameter_FromDate
                        stg.Parameter_Last = .Parameter_ToDate
                        stg.Parameter_Language = .Parameter_Language
                    End With
                End Using
            End If

            Return answer
        End Function

        Dim ds As New System.Data.DataSet("ds")
        Private Function BeforePrint() As System.Data.DataTable Implements IClientLabelDefinition.BeforePrint
            Dim tbl As System.Data.DataTable = ds.Tables("client_labels")

            If tbl Is Nothing Then
                Dim cn As SqlClient.SqlConnection = New System.Data.SqlClient.SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
                Dim txn As SqlClient.SqlTransaction = Nothing
                Dim sb As New System.Text.StringBuilder

                Try
                    cn.Open()
                    txn = cn.BeginTransaction(IsolationLevel.ReadUncommitted)

                    ' Find the unchecked indicators
                    Using cmd As SqlClient.SqlCommand = New System.Data.SqlClient.SqlCommand()
                        With cmd
                            .Connection = cn
                            .Transaction = txn
                            .CommandTimeout = 0

                            If stg.Parameter_First IsNot Nothing Then
                                sb.Append(" AND c.[dmp_status_date] >= @first")
                                .Parameters.Add("@first", SqlDbType.DateTime).Value = Convert.ToDateTime(stg.Parameter_First).Date
                            End If

                            If stg.Parameter_Last IsNot Nothing Then
                                sb.Append(" AND c.[dmp_status_date] < @last")
                                .Parameters.Add("@last", SqlDbType.DateTime).Value = Convert.ToDateTime(stg.Parameter_Last).Date.AddDays(1)
                            End If

                            If stg.Parameter_Language > 0 Then
                                sb.Append(" AND c.[language] = @language")
                                .Parameters.Add("@language", SqlDbType.Int).Value = stg.Parameter_Language
                            End If

                            ' We always select only active clients
                            sb.Append(stg.ActiveStatusList)

                            ' Ignore client 0
                            sb.Append(" AND v.[client] <> 0")

                            ' Change the first "AND" to "WHERE"
                            If sb.Length > 0 Then
                                sb.Remove(0, 5)
                                sb.Insert(0, " WHERE ")
                            End If

                            ' Retrieve the items
                            sb.Insert(0, "SELECT v.zipcode, v.client, v.name, v.addr1, v.addr2, v.addr3 FROM view_client_address v INNER JOIN clients c WITH (NOLOCK) ON v.client = c.client")

                            ' Supress intermediate results
                            sb.Insert(0, "SET NOCOUNT ON; ")

                            sb.Append("; ")
                            .CommandText = sb.ToString()
                            .CommandTimeout = 0
                        End With

                        Using da As New SqlClient.SqlDataAdapter(cmd)
                            da.Fill(ds, "client_labels")
                            tbl = ds.Tables("client_labels")
                        End Using
                    End Using

                    txn.Commit()
                    txn.Dispose()
                    txn = Nothing

                Finally
                    If txn IsNot Nothing Then
                        Try
                            txn.Rollback()
                        Catch exRollback As System.Exception
                        End Try
                        txn.Dispose()
                        txn = Nothing
                    End If

                    If cn IsNot Nothing Then cn.Dispose()
                End Try
            End If

            Return tbl
        End Function
    End Class
End Namespace