#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}
#End Region

Option Compare Binary
Option Explicit On
Option Strict On

Imports DebtPlus.Reports.Client.Labels.Forms

Namespace Client.Labels.Formatting

    Public Class ClientLabelsByIndicatorReport
        Implements IClientLabelDefinition

        Private stg As DebtPlus.Reports.Client.Labels.ClientLabelsReport.Storage
        Public Sub New(ByVal stg As DebtPlus.Reports.Client.Labels.ClientLabelsReport.Storage)
            Me.stg = stg
        End Sub

        Public Function NeedParameters() As Boolean Implements IClientLabelDefinition.NeedParameters
            Return (stg.Parameter_First Is Nothing) OrElse (stg.Parameter_First Is System.DBNull.Value)
        End Function

        Public Function RequestReportParameters() As System.Windows.Forms.DialogResult Implements IClientLabelDefinition.RequestReportParameters
            Dim answer As System.Windows.Forms.DialogResult = DialogResult.OK

            If NeedParameters() Then
                Using frm As New IndicatorReportParametersForm()
                    With frm
                        .Parameter_Indicator = stg.Parameter_First
                        .Parameter_ResetIndicator = stg.Parameter_Set_Indicator
                        answer = .ShowDialog()
                        stg.Parameter_First = .Parameter_Indicator
                        stg.Parameter_Language = .Parameter_Language
                        stg.Parameter_Set_Indicator = .Parameter_ResetIndicator
                    End With
                End Using
            End If

            Return answer
        End Function

        Dim ds As New System.Data.DataSet("ds")
        Private Function BeforePrint() As System.Data.DataTable Implements IClientLabelDefinition.BeforePrint
            Dim tbl As System.Data.DataTable = ds.Tables("client_labels")

            If tbl Is Nothing Then
                Dim cn As SqlClient.SqlConnection = New System.Data.SqlClient.SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
                Dim txn As SqlClient.SqlTransaction = Nothing

                Try
                    cn.Open()
                    txn = cn.BeginTransaction(IsolationLevel.ReadUncommitted)

                    ' Find the unchecked indicators
                    Using cmd As SqlClient.SqlCommand = New System.Data.SqlClient.SqlCommand()
                        With cmd
                            Dim sb As New System.Text.StringBuilder

                            .Connection = cn
                            .Transaction = txn
                            .CommandTimeout = 0

                            ' The indicator is the first parameter
                            .Parameters.Add("@indicator", SqlDbType.Int).Value = Convert.ToInt32(stg.Parameter_First)

                            ' Supress intermediate results
                            sb.Append("SET NOCOUNT ON; ")

                            ' Create the table of items
                            sb.Append("SELECT v.client INTO #clients FROM clients v WITH (NOLOCK) LEFT OUTER JOIN client_indicators i WITH (NOLOCK) ON v.client = i.client AND @indicator = i.indicator")
                            sb.Append(" WHERE v.client > 0")
                            sb.Append(stg.ActiveStatusList)
                            sb.Append(" AND v.client IS NULL")

                            If stg.Parameter_Language > 0 Then
                                sb.Append(" AND v.language=@language")
                                .Parameters.Add("@language", SqlDbType.Int).Value = stg.Parameter_Language
                            End If
                            sb.Append("; ")

                            ' Check the indicator if desired
                            If stg.Parameter_Set_Indicator Then
                                sb.Append("INSERT INTO client_indicators (client,indicator) SELECT client, @indicator FROM #clients; ")
                            End If

                            ' Retrieve the items
                            sb.Append("SELECT v.zipcode, v.client, v.name, v.addr1, v.addr2, v.addr3 FROM view_client_address v INNER JOIN #clients x on v.client = x.client; ")

                            ' Discard the working table
                            sb.Append("DROP TABLE #clients;")

                            ' Retrieve the command text
                            .CommandText = sb.ToString()
                            .CommandTimeout = 0
                        End With

                        Using da As New SqlClient.SqlDataAdapter(cmd)
                            da.Fill(ds, "clients")
                            tbl = ds.Tables("clients")
                        End Using
                    End Using

                    txn.Commit()
                    txn.Dispose()
                    txn = Nothing

                Finally
                    If txn IsNot Nothing Then
                        Try
                            txn.Rollback()
                        Catch exRollback As System.Exception
                        End Try
                        txn.Dispose()
                        txn = Nothing
                    End If

                    If cn IsNot Nothing Then cn.Dispose()
                End Try
            End If

            Return tbl
        End Function
    End Class
End Namespace