Namespace Client.Labels
    <Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
    Partial Class BasicLabelDefinition

        'XtraReport overrides dispose to clean up the component list.
        <System.Diagnostics.DebuggerNonUserCode()> _
        Protected Overrides Sub Dispose(ByVal disposing As Boolean)
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
                If vue IsNot Nothing Then
                    vue.Dispose()
                    vue = Nothing
                End If
            End If
            MyBase.Dispose(disposing)
        End Sub

        'Required by the Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Designer
        'It can be modified using the Designer.  
        'Do not modify it using the code editor.
        <System.Diagnostics.DebuggerStepThrough()> _
        Private Sub InitializeComponent()
            Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(BasicLabelDefinition))
            Dim PostNetGenerator1 As DevExpress.XtraPrinting.BarCode.PostNetGenerator = New DevExpress.XtraPrinting.BarCode.PostNetGenerator
            Me.XrPanel1 = New DevExpress.XtraReports.UI.XRPanel
            Me.XrBarCode = New DevExpress.XtraReports.UI.XRBarCode
            Me.XrLabel_Text = New DevExpress.XtraReports.UI.XRLabel
            CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
            '
            'Detail
            '
            Me.Detail.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrPanel1})
            Me.Detail.MultiColumn.ColumnSpacing = 13.0!
            Me.Detail.MultiColumn.ColumnWidth = 262.0!
            Me.Detail.MultiColumn.Layout = DevExpress.XtraPrinting.ColumnLayout.AcrossThenDown
            Me.Detail.MultiColumn.Mode = DevExpress.XtraReports.UI.MultiColumnMode.UseColumnWidth
            Me.Detail.Name = "Detail"
            Me.Detail.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
            Me.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
            '
            'XrPanel1
            '
            Me.XrPanel1.BorderColor = System.Drawing.Color.Transparent
            Me.XrPanel1.CanGrow = False
            Me.XrPanel1.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrBarCode, Me.XrLabel_Text})
            Me.XrPanel1.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
            Me.XrPanel1.Name = "XrPanel1"
            Me.XrPanel1.SizeF = New System.Drawing.SizeF(262.0!, 100.0!)
            '
            'XrBarCode
            '
            Me.XrBarCode.BackColor = System.Drawing.Color.White
            Me.XrBarCode.BorderColor = System.Drawing.Color.Black
            Me.XrBarCode.BorderWidth = 0
            Me.XrBarCode.ForeColor = System.Drawing.Color.Black
            Me.XrBarCode.KeepTogether = False
            Me.XrBarCode.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
            Me.XrBarCode.Name = "XrBarCode"
            Me.XrBarCode.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrBarCode.ShowText = False
            Me.XrBarCode.SizeF = New System.Drawing.SizeF(262.0!, 14.0!)
            Me.XrBarCode.StylePriority.UseBackColor = False
            Me.XrBarCode.StylePriority.UseBorderColor = False
            Me.XrBarCode.StylePriority.UseForeColor = False
            Me.XrBarCode.StylePriority.UsePadding = False
            Me.XrBarCode.Symbology = PostNetGenerator1
            Me.XrBarCode.Visible = False
            '
            'XrLabel_Text
            '
            Me.XrLabel_Text.BorderColor = System.Drawing.Color.Transparent
            Me.XrLabel_Text.CanShrink = True
            Me.XrLabel_Text.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 16.0!)
            Me.XrLabel_Text.Multiline = True
            Me.XrLabel_Text.Name = "XrLabel_Text"
            Me.XrLabel_Text.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 2, 2, 100.0!)
            Me.XrLabel_Text.SizeF = New System.Drawing.SizeF(262.0!, 84.0!)
            Me.XrLabel_Text.StylePriority.UsePadding = False
            '
            'BasicLabelDefinition
            '
            Me.Bands.AddRange(New DevExpress.XtraReports.UI.Band() {Me.Detail})
            Me.ReportPrintOptions.DetailCount = 30
            Me.Margins = New System.Drawing.Printing.Margins(38, 0, 50, 0)
            Me.PaperName = "Avery 5160 Addressing Labels"
            Me.Version = "10.2"
            CType(Me, System.ComponentModel.ISupportInitialize).EndInit()

        End Sub

        Public WithEvents XrPanel1 As DevExpress.XtraReports.UI.XRPanel
        Public WithEvents XrBarCode As DevExpress.XtraReports.UI.XRBarCode
        Public WithEvents XrLabel_Text As DevExpress.XtraReports.UI.XRLabel
    End Class
End Namespace