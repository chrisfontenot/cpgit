Namespace Disbursement.Variance.Deposit
    <Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
    Partial Class CounselorParametersForm

        'Form overrides dispose to clean up the component list.
        Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
            If disposing Then
                If components IsNot Nothing Then
                    components.Dispose()
                End If
            End If
            MyBase.Dispose(disposing)
        End Sub

        'Required by the Windows Form Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Windows Form Designer
        'It can be modified using the Windows Form Designer.  
        'Do not modify it using the code editor.
        Protected Friend WithEvents Label1 As DevExpress.XtraEditors.LabelControl
        Protected Friend WithEvents CounselorDropDown As DevExpress.XtraEditors.LookUpEdit
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
            Me.Label1 = New DevExpress.XtraEditors.LabelControl
            Me.CounselorDropDown = New DevExpress.XtraEditors.LookUpEdit
            CType(Me.DxErrorProvider1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.CounselorDropDown.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.SuspendLayout()
            '
            'ButtonOK
            '
            Me.ButtonOK.Location = New System.Drawing.Point(247, 16)
            Me.ButtonOK.TabIndex = 2
            '
            'ButtonCancel
            '
            Me.ButtonCancel.Location = New System.Drawing.Point(247, 48)
            Me.ButtonCancel.TabIndex = 3
            '
            'Label1
            '
            Me.Label1.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
            Me.Label1.Appearance.Options.UseTextOptions = True
            Me.Label1.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center
            Me.Label1.Location = New System.Drawing.Point(8, 35)
            Me.Label1.Name = "Label1"
            Me.Label1.Size = New System.Drawing.Size(52, 13)
            Me.Label1.TabIndex = 0
            Me.Label1.Text = "Counse&lor:"
            '
            'CounselorDropDown
            '
            Me.CounselorDropDown.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
                        Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.CounselorDropDown.Location = New System.Drawing.Point(66, 32)
            Me.CounselorDropDown.Name = "CounselorDropDown"
            Me.CounselorDropDown.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
            Me.CounselorDropDown.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.CounselorDropDown.Properties.Columns.AddRange(New DevExpress.XtraEditors.Controls.LookUpColumnInfo() {New DevExpress.XtraEditors.Controls.LookUpColumnInfo("item_key", "ID", 20, DevExpress.Utils.FormatType.Numeric, "f0", False, DevExpress.Utils.HorzAlignment.[Default], DevExpress.Data.ColumnSortOrder.None), New DevExpress.XtraEditors.Controls.LookUpColumnInfo("description", "Name", 20, DevExpress.Utils.FormatType.None, "", True, DevExpress.Utils.HorzAlignment.[Default], DevExpress.Data.ColumnSortOrder.None)})
            Me.CounselorDropDown.Properties.DisplayMember = "description"
            Me.CounselorDropDown.Properties.NullText = "[All Counselors]"
            Me.CounselorDropDown.Properties.ShowFooter = False
            Me.CounselorDropDown.Properties.ShowHeader = False
            Me.CounselorDropDown.Properties.ValueMember = "item_key"
            Me.CounselorDropDown.Size = New System.Drawing.Size(175, 20)
            Me.CounselorDropDown.TabIndex = 1
            Me.CounselorDropDown.ToolTip = "Choose the counselor to which you wish to limit the report"
            Me.CounselorDropDown.ToolTipController = Me.ToolTipController1
            '
            'DatedCounselorParametersForm
            '
            Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
            Me.ClientSize = New System.Drawing.Size(335, 157)
            Me.Controls.Add(Me.CounselorDropDown)
            Me.Controls.Add(Me.Label1)
            Me.Name = "DatedCounselorParametersForm"
            Me.Controls.SetChildIndex(Me.ButtonOK, 0)
            Me.Controls.SetChildIndex(Me.ButtonCancel, 0)
            Me.Controls.SetChildIndex(Me.Label1, 0)
            Me.Controls.SetChildIndex(Me.CounselorDropDown, 0)
            CType(Me.DxErrorProvider1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.CounselorDropDown.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            Me.ResumeLayout(False)
            Me.PerformLayout()

        End Sub
    End Class
End Namespace
