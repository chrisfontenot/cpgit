#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}

#End Region

Option Compare Binary
Option Explicit On
Option Strict On

Imports System.ComponentModel
Imports DebtPlus.Reports.Template

Imports System.Drawing.Printing
Imports System.Data.SqlClient

Namespace Disbursement.Variance.Deposit
    Public Class DisbursementVsDepositVarianceReport
        Inherits TemplateXtraReportClass

        Public Sub New()
            MyBase.New()
            InitializeComponent()
            AddHandler Me.BeforePrint, AddressOf TrustBalanceReportClass_BeforePrint
        End Sub

#Region " Component Designer generated code "

        'Component overrides dispose to clean up the component list.
        Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
            If disposing Then
                If components IsNot Nothing Then
                    components.Dispose()
                End If
            End If
            MyBase.Dispose(disposing)
        End Sub

        'Required by the Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Designer
        'It can be modified using the Designer.
        'Do not modify it using the code editor.
        Friend WithEvents XrLabel1 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel2 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel3 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel4 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel5 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrPanel1 As DevExpress.XtraReports.UI.XRPanel
        Friend WithEvents XrLabel14 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel15 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel25 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel26 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel27 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel30 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents Style_Detail_LText As DevExpress.XtraReports.UI.XRControlStyle
        Friend WithEvents Style_Total_Amount As DevExpress.XtraReports.UI.XRControlStyle
        Friend WithEvents Style_Heading_Amount As DevExpress.XtraReports.UI.XRControlStyle
        Friend WithEvents XrControlStyle4 As DevExpress.XtraReports.UI.XRControlStyle
        Friend WithEvents Style_Heading_Pannel As DevExpress.XtraReports.UI.XRControlStyle
        Friend WithEvents Style_Detail_Amount As DevExpress.XtraReports.UI.XRControlStyle
        Friend WithEvents Style_Heading_RText As DevExpress.XtraReports.UI.XRControlStyle
        Friend WithEvents Style_Total_LText As DevExpress.XtraReports.UI.XRControlStyle
        Friend WithEvents Style_Detail_RText As DevExpress.XtraReports.UI.XRControlStyle
        Friend WithEvents XrLabel_client As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_disbursement As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_deposit As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_debt As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_name As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_variance As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents GroupHeader1 As DevExpress.XtraReports.UI.GroupHeaderBand
        Friend WithEvents XrLabel6 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents GroupFooter1 As DevExpress.XtraReports.UI.GroupFooterBand
        Friend WithEvents XrLabel_total_disbursement As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_total_deposit As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_total_debt As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel13 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_total_variance As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents FormattingRule_TotalDebt As DevExpress.XtraReports.UI.FormattingRule
        Friend WithEvents FormattingRule_variance As DevExpress.XtraReports.UI.FormattingRule
        Friend WithEvents Style_Total_RText As DevExpress.XtraReports.UI.XRControlStyle

        <System.Diagnostics.DebuggerStepThrough()>
        Private Sub InitializeComponent()
            Dim XrSummary1 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary
            Dim XrSummary2 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary
            Dim XrSummary3 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary
            Dim XrSummary4 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary
            Me.Style_Detail_LText = New DevExpress.XtraReports.UI.XRControlStyle
            Me.Style_Total_Amount = New DevExpress.XtraReports.UI.XRControlStyle
            Me.Style_Heading_Amount = New DevExpress.XtraReports.UI.XRControlStyle
            Me.XrControlStyle4 = New DevExpress.XtraReports.UI.XRControlStyle
            Me.Style_Heading_Pannel = New DevExpress.XtraReports.UI.XRControlStyle
            Me.Style_Detail_Amount = New DevExpress.XtraReports.UI.XRControlStyle
            Me.Style_Heading_RText = New DevExpress.XtraReports.UI.XRControlStyle
            Me.Style_Total_LText = New DevExpress.XtraReports.UI.XRControlStyle
            Me.Style_Detail_RText = New DevExpress.XtraReports.UI.XRControlStyle
            Me.Style_Total_RText = New DevExpress.XtraReports.UI.XRControlStyle
            Me.XrLabel1 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel2 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel3 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel4 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel5 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrPanel1 = New DevExpress.XtraReports.UI.XRPanel
            Me.XrLabel30 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel27 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel26 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel25 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel14 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel15 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_variance = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_name = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_debt = New DevExpress.XtraReports.UI.XRLabel
            Me.FormattingRule_TotalDebt = New DevExpress.XtraReports.UI.FormattingRule
            Me.XrLabel_deposit = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_disbursement = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_client = New DevExpress.XtraReports.UI.XRLabel
            Me.GroupHeader1 = New DevExpress.XtraReports.UI.GroupHeaderBand
            Me.XrLabel6 = New DevExpress.XtraReports.UI.XRLabel
            Me.GroupFooter1 = New DevExpress.XtraReports.UI.GroupFooterBand
            Me.XrLabel_total_disbursement = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_total_deposit = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_total_debt = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel13 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_total_variance = New DevExpress.XtraReports.UI.XRLabel
            Me.FormattingRule_variance = New DevExpress.XtraReports.UI.FormattingRule
            CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
            '
            'Detail
            '
            Me.Detail.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel_client, Me.XrLabel_disbursement, Me.XrLabel_deposit, Me.XrLabel_debt, Me.XrLabel_name, Me.XrLabel_variance})
            Me.Detail.Height = 17
            '
            'PageHeader
            '
            Me.PageHeader.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrPanel1})
            Me.PageHeader.Height = 150
            '
            'Style_Detail_LText
            '
            Me.Style_Detail_LText.BackColor = System.Drawing.Color.Transparent
            Me.Style_Detail_LText.Name = "Style_Detail_LText"
            Me.Style_Detail_LText.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            '
            'Style_Total_Amount
            '
            Me.Style_Total_Amount.BackColor = System.Drawing.Color.Transparent
            Me.Style_Total_Amount.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.Style_Total_Amount.Name = "Style_Total_Amount"
            '
            'Style_Heading_Amount
            '
            Me.Style_Heading_Amount.BackColor = System.Drawing.Color.Transparent
            Me.Style_Heading_Amount.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.Style_Heading_Amount.ForeColor = System.Drawing.Color.White
            Me.Style_Heading_Amount.Name = "Style_Heading_Amount"
            Me.Style_Heading_Amount.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrControlStyle4
            '
            Me.XrControlStyle4.BackColor = System.Drawing.Color.Transparent
            Me.XrControlStyle4.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrControlStyle4.ForeColor = System.Drawing.Color.White
            Me.XrControlStyle4.Name = "XrControlStyle4"
            Me.XrControlStyle4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            '
            'Style_Heading_Pannel
            '
            Me.Style_Heading_Pannel.BackColor = System.Drawing.Color.Teal
            Me.Style_Heading_Pannel.BorderColor = System.Drawing.Color.Teal
            Me.Style_Heading_Pannel.Borders = CType((((DevExpress.XtraPrinting.BorderSide.Left Or DevExpress.XtraPrinting.BorderSide.Top) _
                                                      Or DevExpress.XtraPrinting.BorderSide.Right) _
                                                     Or DevExpress.XtraPrinting.BorderSide.Bottom),
                                                    DevExpress.XtraPrinting.BorderSide)
            Me.Style_Heading_Pannel.ForeColor = System.Drawing.Color.White
            Me.Style_Heading_Pannel.Name = "Style_Heading_Pannel"
            Me.Style_Heading_Pannel.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            '
            'Style_Detail_Amount
            '
            Me.Style_Detail_Amount.BackColor = System.Drawing.Color.Transparent
            Me.Style_Detail_Amount.Name = "Style_Detail_Amount"
            Me.Style_Detail_Amount.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'Style_Heading_RText
            '
            Me.Style_Heading_RText.BackColor = System.Drawing.Color.Transparent
            Me.Style_Heading_RText.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.Style_Heading_RText.ForeColor = System.Drawing.Color.White
            Me.Style_Heading_RText.Name = "Style_Heading_RText"
            Me.Style_Heading_RText.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'Style_Total_LText
            '
            Me.Style_Total_LText.BackColor = System.Drawing.Color.Transparent
            Me.Style_Total_LText.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.Style_Total_LText.Name = "Style_Total_LText"
            '
            'Style_Detail_RText
            '
            Me.Style_Detail_RText.BackColor = System.Drawing.Color.Transparent
            Me.Style_Detail_RText.Name = "Style_Detail_RText"
            Me.Style_Detail_RText.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'Style_Total_RText
            '
            Me.Style_Total_RText.BackColor = System.Drawing.Color.Transparent
            Me.Style_Total_RText.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.Style_Total_RText.Name = "Style_Total_RText"
            '
            'XrLabel1
            '
            Me.XrLabel1.Location = New System.Drawing.Point(0, 0)
            Me.XrLabel1.Name = "XrLabel1"
            Me.XrLabel1.Size = New System.Drawing.Size(100, 23)
            Me.XrLabel1.Text = "XrLabel1"
            '
            'XrLabel2
            '
            Me.XrLabel2.Location = New System.Drawing.Point(0, 0)
            Me.XrLabel2.Name = "XrLabel2"
            Me.XrLabel2.Size = New System.Drawing.Size(100, 23)
            Me.XrLabel2.Text = "XrLabel2"
            '
            'XrLabel3
            '
            Me.XrLabel3.Location = New System.Drawing.Point(0, 0)
            Me.XrLabel3.Name = "XrLabel3"
            Me.XrLabel3.Size = New System.Drawing.Size(100, 23)
            Me.XrLabel3.Text = "XrLabel3"
            '
            'XrLabel4
            '
            Me.XrLabel4.Location = New System.Drawing.Point(0, 0)
            Me.XrLabel4.Name = "XrLabel4"
            Me.XrLabel4.Size = New System.Drawing.Size(100, 23)
            Me.XrLabel4.Text = "XrLabel4"
            '
            'XrLabel5
            '
            Me.XrLabel5.Location = New System.Drawing.Point(0, 0)
            Me.XrLabel5.Name = "XrLabel5"
            Me.XrLabel5.Size = New System.Drawing.Size(100, 23)
            Me.XrLabel5.Text = "XrLabel5"
            '
            'XrPanel1
            '
            Me.XrPanel1.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel30, Me.XrLabel27, Me.XrLabel26, Me.XrLabel25, Me.XrLabel14, Me.XrLabel15})
            Me.XrPanel1.Location = New System.Drawing.Point(0, 125)
            Me.XrPanel1.Name = "XrPanel1"
            Me.XrPanel1.Size = New System.Drawing.Size(800, 17)
            Me.XrPanel1.StyleName = "Style_Heading_Pannel"
            '
            'XrLabel30
            '
            Me.XrLabel30.Location = New System.Drawing.Point(708, 0)
            Me.XrLabel30.Name = "XrLabel30"
            Me.XrLabel30.Size = New System.Drawing.Size(80, 17)
            Me.XrLabel30.StyleName = "Style_Heading_RText"
            Me.XrLabel30.Text = "VARIANCE"
            Me.XrLabel30.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel27
            '
            Me.XrLabel27.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel27.Location = New System.Drawing.Point(83, 0)
            Me.XrLabel27.Name = "XrLabel27"
            Me.XrLabel27.Size = New System.Drawing.Size(116, 17)
            Me.XrLabel27.StylePriority.UseFont = False
            Me.XrLabel27.Text = "NAME"
            Me.XrLabel27.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            '
            'XrLabel26
            '
            Me.XrLabel26.Location = New System.Drawing.Point(500, 0)
            Me.XrLabel26.Name = "XrLabel26"
            Me.XrLabel26.Size = New System.Drawing.Size(75, 17)
            Me.XrLabel26.StyleName = "Style_Heading_RText"
            Me.XrLabel26.Text = "DEPOSIT"
            Me.XrLabel26.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel25
            '
            Me.XrLabel25.Location = New System.Drawing.Point(608, 0)
            Me.XrLabel25.Name = "XrLabel25"
            Me.XrLabel25.Size = New System.Drawing.Size(73, 17)
            Me.XrLabel25.StyleName = "Style_Heading_RText"
            Me.XrLabel25.Text = "DISB"
            Me.XrLabel25.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel14
            '
            Me.XrLabel14.Location = New System.Drawing.Point(0, 0)
            Me.XrLabel14.Name = "XrLabel14"
            Me.XrLabel14.Size = New System.Drawing.Size(74, 17)
            Me.XrLabel14.StyleName = "Style_Heading_RText"
            Me.XrLabel14.Text = "CLIENT"
            Me.XrLabel14.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel15
            '
            Me.XrLabel15.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel15.Location = New System.Drawing.Point(383, 0)
            Me.XrLabel15.Name = "XrLabel15"
            Me.XrLabel15.Size = New System.Drawing.Size(100, 17)
            Me.XrLabel15.StylePriority.UseFont = False
            Me.XrLabel15.StylePriority.UseTextAlignment = False
            Me.XrLabel15.Text = "TOTAL DEBT"
            Me.XrLabel15.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel_variance
            '
            Me.XrLabel_variance.CanGrow = False
            Me.XrLabel_variance.Font = New System.Drawing.Font("Times New Roman", 9.75!)
            Me.XrLabel_variance.ForeColor = System.Drawing.SystemColors.ControlText
            Me.XrLabel_variance.FormattingRules.Add(Me.FormattingRule_variance)
            Me.XrLabel_variance.Location = New System.Drawing.Point(708, 0)
            Me.XrLabel_variance.Name = "XrLabel_variance"
            Me.XrLabel_variance.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_variance.Size = New System.Drawing.Size(80, 17)
            Me.XrLabel_variance.StyleName = "Style_Heading_RText"
            Me.XrLabel_variance.StylePriority.UseFont = False
            Me.XrLabel_variance.StylePriority.UseForeColor = False
            Me.XrLabel_variance.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel_name
            '
            Me.XrLabel_name.CanGrow = False
            Me.XrLabel_name.Font = New System.Drawing.Font("Times New Roman", 9.75!)
            Me.XrLabel_name.ForeColor = System.Drawing.SystemColors.ControlText
            Me.XrLabel_name.Location = New System.Drawing.Point(83, 0)
            Me.XrLabel_name.Name = "XrLabel_name"
            Me.XrLabel_name.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_name.Size = New System.Drawing.Size(292, 17)
            Me.XrLabel_name.StylePriority.UseFont = False
            Me.XrLabel_name.StylePriority.UseForeColor = False
            Me.XrLabel_name.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            '
            'XrLabel_debt
            '
            Me.XrLabel_debt.CanGrow = False
            Me.XrLabel_debt.Font = New System.Drawing.Font("Times New Roman", 9.75!)
            Me.XrLabel_debt.ForeColor = System.Drawing.SystemColors.ControlText
            Me.XrLabel_debt.FormattingRules.Add(Me.FormattingRule_TotalDebt)
            Me.XrLabel_debt.Location = New System.Drawing.Point(383, 0)
            Me.XrLabel_debt.Name = "XrLabel_debt"
            Me.XrLabel_debt.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_debt.Size = New System.Drawing.Size(100, 17)
            Me.XrLabel_debt.StylePriority.UseFont = False
            Me.XrLabel_debt.StylePriority.UseForeColor = False
            Me.XrLabel_debt.StylePriority.UseTextAlignment = False
            Me.XrLabel_debt.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'FormattingRule_TotalDebt
            '
            Me.FormattingRule_TotalDebt.Condition = "[debt] <= [deposit]"
            '
            '
            '
            Me.FormattingRule_TotalDebt.Formatting.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Int32), CType(CType(255, Byte), Int32), CType(CType(128, Byte), Int32))
            Me.FormattingRule_TotalDebt.Name = "FormattingRule_TotalDebt"
            '
            'XrLabel_deposit
            '
            Me.XrLabel_deposit.CanGrow = False
            Me.XrLabel_deposit.Font = New System.Drawing.Font("Times New Roman", 9.75!)
            Me.XrLabel_deposit.ForeColor = System.Drawing.SystemColors.ControlText
            Me.XrLabel_deposit.Location = New System.Drawing.Point(500, 0)
            Me.XrLabel_deposit.Name = "XrLabel_deposit"
            Me.XrLabel_deposit.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_deposit.Size = New System.Drawing.Size(75, 17)
            Me.XrLabel_deposit.StyleName = "Style_Heading_RText"
            Me.XrLabel_deposit.StylePriority.UseFont = False
            Me.XrLabel_deposit.StylePriority.UseForeColor = False
            Me.XrLabel_deposit.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel_disbursement
            '
            Me.XrLabel_disbursement.CanGrow = False
            Me.XrLabel_disbursement.Font = New System.Drawing.Font("Times New Roman", 9.75!)
            Me.XrLabel_disbursement.ForeColor = System.Drawing.SystemColors.ControlText
            Me.XrLabel_disbursement.Location = New System.Drawing.Point(608, 0)
            Me.XrLabel_disbursement.Name = "XrLabel_disbursement"
            Me.XrLabel_disbursement.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_disbursement.Size = New System.Drawing.Size(73, 17)
            Me.XrLabel_disbursement.StyleName = "Style_Heading_RText"
            Me.XrLabel_disbursement.StylePriority.UseFont = False
            Me.XrLabel_disbursement.StylePriority.UseForeColor = False
            Me.XrLabel_disbursement.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel_client
            '
            Me.XrLabel_client.CanGrow = False
            Me.XrLabel_client.Font = New System.Drawing.Font("Times New Roman", 9.75!)
            Me.XrLabel_client.ForeColor = System.Drawing.SystemColors.ControlText
            Me.XrLabel_client.Location = New System.Drawing.Point(0, 0)
            Me.XrLabel_client.Name = "XrLabel_client"
            Me.XrLabel_client.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_client.Size = New System.Drawing.Size(74, 17)
            Me.XrLabel_client.StyleName = "Style_Heading_RText"
            Me.XrLabel_client.StylePriority.UseFont = False
            Me.XrLabel_client.StylePriority.UseForeColor = False
            Me.XrLabel_client.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'GroupHeader1
            '
            Me.GroupHeader1.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel6})
            Me.GroupHeader1.GroupFields.AddRange(New DevExpress.XtraReports.UI.GroupField() {New DevExpress.XtraReports.UI.GroupField("disbursement_date", DevExpress.XtraReports.UI.XRColumnSortOrder.Ascending)})
            Me.GroupHeader1.Height = 40
            Me.GroupHeader1.Name = "GroupHeader1"
            '
            'XrLabel6
            '
            Me.XrLabel6.CanGrow = False
            Me.XrLabel6.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel6.ForeColor = System.Drawing.Color.Maroon
            Me.XrLabel6.Location = New System.Drawing.Point(0, 8)
            Me.XrLabel6.Name = "XrLabel6"
            Me.XrLabel6.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel6.Size = New System.Drawing.Size(800, 17)
            Me.XrLabel6.StylePriority.UseFont = False
            Me.XrLabel6.StylePriority.UseForeColor = False
            Me.XrLabel6.StylePriority.UseTextAlignment = False
            Me.XrLabel6.Text = "DISBURSEMENT CYCLE: [disbursement_date]"
            Me.XrLabel6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
            '
            'GroupFooter1
            '
            Me.GroupFooter1.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel_total_disbursement, Me.XrLabel_total_deposit, Me.XrLabel_total_debt, Me.XrLabel13, Me.XrLabel_total_variance})
            Me.GroupFooter1.GroupUnion = DevExpress.XtraReports.UI.GroupFooterUnion.WithLastDetail
            Me.GroupFooter1.Height = 24
            Me.GroupFooter1.Name = "GroupFooter1"
            '
            'XrLabel_total_disbursement
            '
            Me.XrLabel_total_disbursement.CanGrow = False
            Me.XrLabel_total_disbursement.ForeColor = System.Drawing.SystemColors.ControlText
            Me.XrLabel_total_disbursement.Location = New System.Drawing.Point(575, 0)
            Me.XrLabel_total_disbursement.Name = "XrLabel_total_disbursement"
            Me.XrLabel_total_disbursement.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_total_disbursement.Size = New System.Drawing.Size(106, 17)
            Me.XrLabel_total_disbursement.StyleName = "Style_Heading_RText"
            Me.XrLabel_total_disbursement.StylePriority.UseForeColor = False
            XrSummary1.FormatString = "{0:c}"
            XrSummary1.Running = DevExpress.XtraReports.UI.SummaryRunning.Group
            Me.XrLabel_total_disbursement.Summary = XrSummary1
            Me.XrLabel_total_disbursement.Text = "XrLabel_total_disbursement"
            Me.XrLabel_total_disbursement.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel_total_deposit
            '
            Me.XrLabel_total_deposit.CanGrow = False
            Me.XrLabel_total_deposit.ForeColor = System.Drawing.SystemColors.ControlText
            Me.XrLabel_total_deposit.Location = New System.Drawing.Point(483, 0)
            Me.XrLabel_total_deposit.Name = "XrLabel_total_deposit"
            Me.XrLabel_total_deposit.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_total_deposit.Size = New System.Drawing.Size(92, 17)
            Me.XrLabel_total_deposit.StyleName = "Style_Heading_RText"
            Me.XrLabel_total_deposit.StylePriority.UseForeColor = False
            XrSummary2.FormatString = "{0:c}"
            XrSummary2.Running = DevExpress.XtraReports.UI.SummaryRunning.Group
            Me.XrLabel_total_deposit.Summary = XrSummary2
            Me.XrLabel_total_deposit.Text = "XrLabel_total_deposit"
            Me.XrLabel_total_deposit.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel_total_debt
            '
            Me.XrLabel_total_debt.CanGrow = False
            Me.XrLabel_total_debt.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel_total_debt.ForeColor = System.Drawing.SystemColors.ControlText
            Me.XrLabel_total_debt.Location = New System.Drawing.Point(333, 0)
            Me.XrLabel_total_debt.Name = "XrLabel_total_debt"
            Me.XrLabel_total_debt.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_total_debt.Size = New System.Drawing.Size(150, 17)
            Me.XrLabel_total_debt.StylePriority.UseFont = False
            Me.XrLabel_total_debt.StylePriority.UseForeColor = False
            Me.XrLabel_total_debt.StylePriority.UseTextAlignment = False
            XrSummary3.FormatString = "{0:c}"
            XrSummary3.Running = DevExpress.XtraReports.UI.SummaryRunning.Group
            Me.XrLabel_total_debt.Summary = XrSummary3
            Me.XrLabel_total_debt.Text = "XrLabel_total_debt"
            Me.XrLabel_total_debt.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel13
            '
            Me.XrLabel13.CanGrow = False
            Me.XrLabel13.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel13.Location = New System.Drawing.Point(83, 0)
            Me.XrLabel13.Name = "XrLabel13"
            Me.XrLabel13.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel13.Size = New System.Drawing.Size(116, 17)
            Me.XrLabel13.StylePriority.UseFont = False
            Me.XrLabel13.Text = "TOTALS"
            Me.XrLabel13.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            '
            'XrLabel_total_variance
            '
            Me.XrLabel_total_variance.CanGrow = False
            Me.XrLabel_total_variance.ForeColor = System.Drawing.SystemColors.ControlText
            Me.XrLabel_total_variance.Location = New System.Drawing.Point(683, 0)
            Me.XrLabel_total_variance.Name = "XrLabel_total_variance"
            Me.XrLabel_total_variance.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_total_variance.Size = New System.Drawing.Size(105, 17)
            Me.XrLabel_total_variance.StyleName = "Style_Heading_RText"
            Me.XrLabel_total_variance.StylePriority.UseForeColor = False
            XrSummary4.FormatString = "{0:c}"
            XrSummary4.Running = DevExpress.XtraReports.UI.SummaryRunning.Group
            Me.XrLabel_total_variance.Summary = XrSummary4
            Me.XrLabel_total_variance.Text = "XrLabel_total_variance"
            Me.XrLabel_total_variance.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'FormattingRule_variance
            '
            Me.FormattingRule_variance.Condition = "[variance]<0"
            Me.FormattingRule_variance.Formatting.ForeColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Int32), CType(CType(0, Byte), Int32), CType(CType(0, Byte), Int32))
            Me.FormattingRule_variance.Name = "FormattingRule_variance"
            '
            'DisbursementVsDepositVarianceReport
            '
            Me.Bands.AddRange(New DevExpress.XtraReports.UI.Band() {Me.GroupHeader1, Me.GroupFooter1})
            Me.FormattingRuleSheet.AddRange(New DevExpress.XtraReports.UI.FormattingRule() {Me.FormattingRule_TotalDebt, Me.FormattingRule_variance})
            Me.StyleSheet.AddRange(New DevExpress.XtraReports.UI.XRControlStyle() {Me.Style_Detail_LText, Me.Style_Total_Amount, Me.Style_Heading_Amount, Me.XrControlStyle4, Me.Style_Heading_Pannel, Me.Style_Detail_Amount, Me.Style_Heading_RText, Me.Style_Total_LText, Me.Style_Detail_RText, Me.Style_Total_RText})
            Me.Version = "8.3"
            CType(Me, System.ComponentModel.ISupportInitialize).EndInit()
        End Sub

#End Region

        ''' <summary>
        ''' Report Title string
        ''' </summary>
        Public Overrides ReadOnly Property ReportTitle() As String
            Get
                Return "Deposit v.s. Disbursement"
            End Get
        End Property

        ''' <summary>
        ''' Report SubTitle string
        ''' </summary>
        Public Overrides ReadOnly Property ReportSubTitle() As String
            Get
                Dim Answer As String = String.Empty
                If ParameterCounselor Is DBNull.Value Then
                    Answer = " all counselors"
                Else
                    Answer = String.Format(" counselor #{0:f0}", Convert.ToInt32(ParameterCounselor))
                End If
                Return "This report is for" + Answer
            End Get
        End Property

        ''' <summary>
        ''' Counselor ID
        ''' </summary>
        Private privateCounselorSet As Boolean = False
        Private privateCounselor As Object = DBNull.Value

        Public Property ParameterCounselor() As Object
            Get
                Return privateCounselor
            End Get
            Set(ByVal value As Object)
                privateCounselorSet = True
                privateCounselor = value
            End Set
        End Property

        ''' <summary>
        ''' Set the report parameters
        ''' </summary>
        Public Overrides Sub SetReportParameter(ByVal Parameter As String, ByVal Value As Object)
            If Parameter = "Counselor" Then
                ParameterCounselor = Value
            Else
                MyBase.SetReportParameter(Parameter, Value)
            End If
        End Sub

        ''' <summary>
        ''' Do we need to ask for parameters?
        ''' </summary>
        Public Overrides Function NeedParameters() As Boolean
            Return MyBase.NeedParameters() OrElse Not privateCounselorSet
        End Function

        ''' <summary>
        ''' Ask for the needed parameters
        ''' </summary>
        Public Overrides Function RequestReportParameters() As DialogResult
            Dim answer As DialogResult = DialogResult.OK
            If NeedParameters() Then
                With New CounselorParametersForm(True)
                    answer = .ShowDialog()
                    ParameterCounselor = .Parameter_Counselor
                    .Dispose()
                End With
            End If
            Return answer
        End Function

        ''' <summary>
        ''' Bind the dataset to the report
        ''' </summary>
        Private Sub TrustBalanceReportClass_BeforePrint(ByVal sender As Object, ByVal e As PrintEventArgs)
            Dim Currency As String = "{0:c}"
            Dim ds As New DataSet("ds")

            Using cmd As SqlCommand = New System.Data.SqlClient.SqlCommand()
                With cmd
                    .Connection = New System.Data.SqlClient.SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
                    .CommandText = "rpt_Compare_Disbursement_To_Deposits"
                    .CommandType = CommandType.StoredProcedure
                    .Parameters.Add("@counselor", SqlDbType.Int).Value = ParameterCounselor
                    .CommandTimeout = 0
                End With

                Using da As New SqlDataAdapter(cmd)
                    da.FillLoadOption = LoadOption.OverwriteChanges
                    da.Fill(ds, "rpt_Compare_Disbursement_To_Deposits")
                End Using
            End Using

            Dim tbl As DataTable = ds.Tables("rpt_Compare_Disbursement_To_Deposits")
            With tbl
                .Columns.Add("variance", GetType(Decimal), "[deposit]-[disbursement]")
            End With
            Dim vue As DataView = tbl.DefaultView
            DataSource = vue

            With XrLabel_client
                AddHandler .BeforePrint, AddressOf XrLabel_client_BeforePrint
            End With

            With XrLabel_debt
                .DataBindings.Add("Text", vue, "debt", Currency)
            End With

            With XrLabel_deposit
                .DataBindings.Add("Text", vue, "deposit", Currency)
            End With

            With XrLabel_disbursement
                .DataBindings.Add("Text", vue, "disbursement", Currency)
            End With

            With XrLabel_name
                .DataBindings.Add("Text", vue, "name")
            End With

            With XrLabel_variance
                .DataBindings.Add("Text", vue, "variance", Currency)
            End With

            With XrLabel_total_debt
                .DataBindings.Add("Text", vue, "debt")
            End With

            With XrLabel_total_deposit
                .DataBindings.Add("Text", vue, "deposit")
            End With

            With XrLabel_total_disbursement
                .DataBindings.Add("Text", vue, "disbursement")
            End With

            With XrLabel_total_variance
                .DataBindings.Add("Text", vue, "variance")
            End With
        End Sub

        ''' <summary>
        ''' Foramt the client ID when needed
        ''' </summary>
        Private Sub XrLabel_client_BeforePrint(ByVal sender As Object, ByVal e As PrintEventArgs)
            XrLabel_client.Text = String.Format("{0:0000000}", GetCurrentColumnValue("client"))
        End Sub
    End Class
End Namespace
