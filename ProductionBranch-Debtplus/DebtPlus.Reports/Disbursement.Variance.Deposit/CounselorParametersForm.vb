#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}

#End Region


Imports System.Data.SqlClient
Imports DevExpress.Utils

Namespace Disbursement.Variance.Deposit
    Public Class CounselorParametersForm
        Inherits DebtPlus.Reports.Template.Forms.ReportParametersForm

        ' TRUE if "[All Counselors]" is in the list
        Private EnableAllCounselors As Boolean = True

        Sub New()
            MyClass.New(True)
        End Sub

        Public Sub New(ByVal EnableAllCounselors As Boolean)
            MyBase.New()
            InitializeComponent()
            AddHandler Load, AddressOf DateCounselorReportParametersForm_Load
            Me.EnableAllCounselors = EnableAllCounselors
        End Sub

        ''' <summary>
        ''' Return the counselor selected in the form
        ''' </summary>
        Public ReadOnly Property Parameter_Counselor() As Object
            Get
                With CounselorDropDown
                    If .EditValue IsNot Nothing AndAlso .EditValue IsNot DBNull.Value Then
                        If Convert.ToInt32(.EditValue) > 0 Then Return Convert.ToInt32(.EditValue)
                    End If

                    ' If all counselors is not permitted then return -1
                    If Not EnableAllCounselors Then Return -1
                    Return DBNull.Value
                End With
            End Get
        End Property


        ''' <summary>
        ''' Process the form load event
        ''' </summary>
        Private Sub DateCounselorReportParametersForm_Load(ByVal sender As Object, ByVal e As EventArgs)
            CounselorDropDown_Load()
            ButtonOK.Enabled = Not HasErrors()
        End Sub


        ''' <summary>
        ''' Load the list of counselors
        ''' </summary>
        Private Sub CounselorDropDown_Load()
            Dim ds As New DataSet("ds")

            Dim cmd As SqlCommand = New System.Data.SqlClient.SqlCommand()
            With cmd
                .Connection = New System.Data.SqlClient.SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
                .CommandText = "lst_counselors"
                .CommandType = CommandType.StoredProcedure
            End With

            Dim da As New SqlDataAdapter(cmd)
            da.Fill(ds, "counselors")
            Dim tbl As DataTable = ds.Tables(0)

            If EnableAllCounselors Then
                With tbl
                    Dim row As DataRow = tbl.NewRow
                    With row
                        .Item("item_key") = -1
                        .Item("description") = "[All Counselors]"
                        .Item("default") = False
                        .Item("ActiveFlag") = True
                    End With
                    .Rows.InsertAt(row, 0)
                End With
                CounselorDropDown.Properties.AllowNullInput = DefaultBoolean.True
            Else
                CounselorDropDown.Properties.AllowNullInput = DefaultBoolean.False
            End If

            With CounselorDropDown
                .Properties.DataSource = New DataView(tbl, String.Empty, "description, item_key", DataViewRowState.CurrentRows)
                If tbl.Columns.Contains("default") Then
                    Dim vue As New DataView(tbl, "[default]<>0", "item_key", DataViewRowState.CurrentRows)
                    If vue.Count > 0 Then
                        .EditValue = vue(0)("item_key")
                    End If
                End If

                AddHandler .EditValueChanging, AddressOf DebtPlus.Data.Validation.LookUpEdit_ActiveTest
                AddHandler .EditValueChanged, AddressOf FormChanged
            End With
        End Sub


        ''' <summary>
        ''' Look for errors in the counselor list
        ''' </summary>
        Protected Overrides Function HasErrors() As Boolean
            Return MyBase.HasErrors() OrElse (Not EnableAllCounselors AndAlso CounselorDropDown.EditValue Is DBNull.Value)
        End Function
    End Class
End Namespace
