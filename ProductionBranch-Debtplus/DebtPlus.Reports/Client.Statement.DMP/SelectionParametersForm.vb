﻿Imports DebtPlus.LINQ
Imports System.Linq
Imports System.Windows.Forms

Namespace Client.Statement.DMP
    Public Class SelectionParametersForm
        Inherits DebtPlus.Reports.Template.Forms.BaseBatchParametersForm

        Public Sub New()
            MyBase.New()
            InitializeComponent()
        End Sub

        Dim ds As New System.Data.DataSet("ds")
        Protected record As SelectionParameters = Nothing

        Public Sub New(record As SelectionParameters)
            MyClass.New()
            Me.record = record
            RegisterHandlers()
        End Sub

        Private Sub RegisterHandlers()
            AddHandler Load, AddressOf ClientBatchParametersForm_Load
        End Sub

        Private Sub UnRegisterHandlers()
            RemoveHandler Load, AddressOf ClientBatchParametersForm_Load
        End Sub

        ''' <summary>
        ''' Set the data source to the list of statement batches
        ''' </summary>
        Public Function SetDataSource() As Boolean

            Try
                Dim connectionString As String = DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString
                Using cn As New System.Data.SqlClient.SqlConnection(connectionString)
                    cn.Open()

                    Using cmd As New System.Data.SqlClient.SqlCommand()
                        cmd.Connection = cn
                        cmd.CommandType = CommandType.Text
                        cmd.CommandText = String.Format("SET ROWCOUNT 50; {0} ORDER BY b.[client_statement_batch] DESC", BaseClientStatementReport.Select_client_statement_batches)

                        Using da As New System.Data.SqlClient.SqlDataAdapter(cmd)
                            da.Fill(ds, "client_statement_batches")
                            Dim tbl As System.Data.DataTable = ds.Tables("client_statement_batches")

                            If tbl IsNot Nothing AndAlso tbl.Rows.Count > 0 Then
                                Dim vue As System.Data.DataView = tbl.DefaultView
                                If vue.Count > 0 Then
                                    XrLookup_param_09_1.Properties.DataSource = vue
                                    XrLookup_param_09_1.EditValue = DebtPlus.Utils.Nulls.v_Int32(tbl.Rows(0)("client_statement_batch"))
                                    XrText_param_09_1.EditValue = DebtPlus.Utils.Nulls.v_Int32(tbl.Rows(0)("client_statement_batch"))
                                    Return True
                                End If
                            End If
                        End Using
                    End Using
                End Using

            Catch ex As System.Data.SqlClient.SqlException
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex)
            End Try

            Return False
        End Function

        Private Sub ClientBatchParametersForm_Load(sender As Object, e As System.EventArgs)
            UnRegisterHandlers()
            Try

                ' Set the batch list
                SetDataSource()

                ' Set the combobox entries to the appropriate modes
                ComboBoxEdit_MultiPage.SelectedIndex = Convert.ToInt32(record.PageItems)
                ComboBoxEdit_ACH.SelectedIndex = Convert.ToInt32(record.AchItems)
                ButtonOK.Enabled = Not HasErrors()

            Catch ex As Exception
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex)

            Finally
                RegisterHandlers()
            End Try
        End Sub

        Protected Overrides Sub ButtonOK_Click(sender As Object, e As System.EventArgs)
            MyBase.ButtonOK_Click(sender, e)

            record.BatchID = getSelectedID()

            record.client_statement_batch = TryCast(XrLookup_param_09_1.GetSelectedDataRow(), System.Data.DataRowView)
            record.AchItems = DirectCast(ComboBoxEdit_ACH.SelectedIndex, SelectionParameters.AchEnum)
            record.PageItems = DirectCast(ComboBoxEdit_MultiPage.SelectedIndex, SelectionParameters.PageItemEnum)

            ' Force selection of the clients to just those wanting paper statements if so desired
            record.OnlyPaperStatements = CheckEdit1.Checked
        End Sub
    End Class
End Namespace
