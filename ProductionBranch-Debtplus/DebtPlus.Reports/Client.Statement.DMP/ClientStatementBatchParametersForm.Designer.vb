﻿Namespace Client.Statement.DMP
    Partial Class ClientStatementBatchParametersForm

        'Form overrides dispose to clean up the component list.
        Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
            If disposing Then
                If components IsNot Nothing Then
                    components.Dispose()
                End If
            End If
            MyBase.Dispose(disposing)
        End Sub

        'Required by the Windows Form Designer
        Private components As System.ComponentModel.IContainer

        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
            Me.XrGroup_param_09_1.SuspendLayout()
            CType(Me.XrRadio_param_09_1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.XrRadio_param_09_2.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.XrText_param_09_1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.XrLookup_param_09_1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.DxErrorProvider1, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.SuspendLayout()
            '
            'XrGroup_param_09_1
            '
            Me.XrGroup_param_09_1.TabIndex = 2
            Me.XrGroup_param_09_1.Text = "Statement Batch ID"
            '
            'XrRadio_param_09_1
            '
            '
            'XrRadio_param_09_2
            '
            '
            'XrLbl_param_09_1
            '
            Me.XrLbl_param_09_1.Appearance.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            '
            'XrText_param_09_1
            '
            Me.XrText_param_09_1.Properties.Appearance.Options.UseTextOptions = True
            Me.XrText_param_09_1.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.XrText_param_09_1.Properties.AppearanceDisabled.Options.UseTextOptions = True
            Me.XrText_param_09_1.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.XrText_param_09_1.Properties.DisplayFormat.FormatString = "f0"
            Me.XrText_param_09_1.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.XrText_param_09_1.Properties.EditFormat.FormatString = "f0"
            Me.XrText_param_09_1.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.XrText_param_09_1.Properties.Mask.BeepOnError = True
            Me.XrText_param_09_1.Properties.Mask.EditMask = "\d*"
            Me.XrText_param_09_1.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx
            '
            'XrLookup_param_09_1
            '
            Me.XrLookup_param_09_1.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[False]
            Me.XrLookup_param_09_1.Properties.Columns.AddRange(New DevExpress.XtraEditors.Controls.LookUpColumnInfo() {New DevExpress.XtraEditors.Controls.LookUpColumnInfo("client_statement_batch", "ID", 5, DevExpress.Utils.FormatType.Numeric, "f0", True, DevExpress.Utils.HorzAlignment.Far, DevExpress.Data.ColumnSortOrder.None, DevExpress.Utils.DefaultBoolean.[True]), New DevExpress.XtraEditors.Controls.LookUpColumnInfo("statement_date", "Statement Date", 20, DevExpress.Utils.FormatType.DateTime, "MM/dd/yyyy hh:mm tt", True, DevExpress.Utils.HorzAlignment.Near, DevExpress.Data.ColumnSortOrder.Descending, DevExpress.Utils.DefaultBoolean.[True]), New DevExpress.XtraEditors.Controls.LookUpColumnInfo("period_start", "Starting Date", 10, DevExpress.Utils.FormatType.Numeric, "d", True, DevExpress.Utils.HorzAlignment.Far, DevExpress.Data.ColumnSortOrder.None, DevExpress.Utils.DefaultBoolean.[True]), New DevExpress.XtraEditors.Controls.LookUpColumnInfo("period_end", "Ending Date", 10, DevExpress.Utils.FormatType.DateTime, "d", True, DevExpress.Utils.HorzAlignment.Far, DevExpress.Data.ColumnSortOrder.None, DevExpress.Utils.DefaultBoolean.[True])})
            Me.XrLookup_param_09_1.Properties.DisplayFormat.FormatString = "MM/dd/yyyy hh:mm tt"
            Me.XrLookup_param_09_1.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime
            Me.XrLookup_param_09_1.Properties.DisplayMember = "statement_date"
            Me.XrLookup_param_09_1.Properties.ValueMember = "client_statement_batch"
            '
            'DefaultLookAndFeel1
            '
            Me.DefaultLookAndFeel1.LookAndFeel.SkinName = "McSkin"
            '
            'ClientStatementBatchParametersForm
            '
            Me.ClientSize = New System.Drawing.Size(336, 133)
            Me.Name = "ClientStatementBatchParametersForm"
            Me.XrGroup_param_09_1.ResumeLayout(False)
            Me.XrGroup_param_09_1.PerformLayout()
            CType(Me.XrRadio_param_09_1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.XrRadio_param_09_2.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.XrText_param_09_1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.XrLookup_param_09_1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.DxErrorProvider1, System.ComponentModel.ISupportInitialize).EndInit()
            Me.ResumeLayout(False)

        End Sub
    End Class
End Namespace