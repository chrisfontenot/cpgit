﻿Namespace Client.Statement.DMP
    Public Class SelectionParameters
        Public Sub New()
        End Sub

        Public Enum PageItemEnum
            [AllStatements] = 0
            [SinglePageStatements] = 1
            [MultiPageStatements] = 2
        End Enum

        Public Enum AchEnum
            [AllStatements] = 0
            [ACHOnly] = 1
            [NonACHOnly] = 2
        End Enum

        '--------------------------------- Selection Information -----------------------------

        ''' <summary>
        ''' Desired statement batch
        ''' </summary>
        Public Property BatchID As System.Nullable(Of Int32) = Nothing

        ''' <summary>
        ''' Starting date range for the batch list
        ''' </summary>
        ''' <remarks>Value has no meaning without an ending date</remarks>
        Public Property BatchStartingDate As System.Nullable(Of DateTime) = Nothing

        ''' <summary>
        ''' TRUE if we want only paper statements in the selection list.
        ''' </summary>
        ''' <value></value>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Property OnlyPaperStatements As Boolean = False

        ''' <summary>
        ''' Ending date range for the batch list.
        ''' </summary>
        ''' <remarks>Value has no meaning without a starting date</remarks>
        Public Property BatchEndingDate As System.Nullable(Of DateTime) = Nothing

        ''' <summary>
        ''' Desired range of clients if they are single or multi-page statements
        ''' </summary>
        Public Property PageItems As PageItemEnum = PageItemEnum.AllStatements

        ''' <summary>
        ''' Desired range of clients if they are on ACH or not
        ''' </summary>
        Public Property AchItems As AchEnum = AchEnum.AllStatements

        ''' <summary>
        ''' Desired specific client ID. If specified, it overrides all selection criteria.
        ''' </summary>
        Public Property ClientID As System.Nullable(Of Int32) = Nothing

        '--------------------------------- Used in the report -----------------------------

        ''' <summary>
        ''' Current statement batch row
        ''' </summary>
        Public Property client_statement_batch As System.Data.DataRowView = Nothing

        ''' <summary>
        ''' Current client statement row
        ''' </summary>
        Public Property client_statement As System.Data.DataRowView = Nothing

        ''' <summary>
        ''' Current client ID from the client statement row
        ''' </summary>
        Public ReadOnly Property CurrentClientID As Int32
            Get
                Return Convert.ToInt32(client_statement("client"))
            End Get
        End Property

        ''' <summary>
        ''' Current statement batch ID from the statement row
        ''' </summary>
        Public ReadOnly Property CurrentBatchID As Int32
            Get
                Return Convert.ToInt32(client_statement_batch("client_statement_batch"))
            End Get
        End Property
    End Class
End Namespace
