﻿Namespace Client.Statement.DMP
    Public Class ClientStatementReport
        Inherits BaseClientStatementReport

        ''' <summary>
        ''' Initialize the new class instance
        ''' </summary>
        ''' <remarks></remarks>
        Public Sub New()
            MyBase.New()
        End Sub

        ''' <summary>
        ''' Override the Requirement for Parameters. Always ask for them.
        ''' </summary>
        Public Overrides Function NeedParameters() As Boolean
            Return True
        End Function

        ''' <summary>
        ''' Request the report parameters from the user.
        ''' </summary>
        Public Overrides Function RequestReportParameters() As System.Windows.Forms.DialogResult

            Dim FilterCriteria As New DebtPlus.Reports.Client.Statement.DMP.SelectionParameters()
            Using frm As New Global.DebtPlus.Reports.Client.Statement.DMP.SelectionParametersForm(FilterCriteria)
                If frm.ShowDialog() = DialogResult.OK Then
                    DefineDataSource(FilterCriteria)
                    Return DialogResult.OK
                End If
            End Using

            Return DialogResult.Cancel
        End Function
    End Class
End Namespace
