﻿Namespace Client.Statement.DMP
    Public Class BatchedClientStatementReport
        Inherits BaseClientStatementReport

        ''' <summary>
        ''' Initialize the new class instance
        ''' </summary>
        ''' <remarks></remarks>
        Public Sub New()
            MyBase.New()
        End Sub

        ''' <summary>
        ''' Override the Requirement for Parameters. Always ask for them.
        ''' </summary>
        Public Overrides Function NeedParameters() As Boolean
            Return True
        End Function

        ''' <summary>
        ''' Request the report parameters from the user.
        ''' </summary>
        Public Overrides Function RequestReportParameters() As System.Windows.Forms.DialogResult

            Using frm As New DebtPlus.Reports.Template.Forms.DatedClientParametersForm()
                If frm.ShowDialog() = DialogResult.OK Then
                    Dim FilterCriteria As New DebtPlus.Reports.Client.Statement.DMP.SelectionParameters()
                    FilterCriteria.ClientID = frm.Parameter_Client
                    FilterCriteria.BatchStartingDate = frm.Parameter_FromDate
                    FilterCriteria.BatchEndingDate = frm.Parameter_ToDate

                    DefineDataSource(FilterCriteria)
                    Return DialogResult.OK
                End If
            End Using

            Return DialogResult.Cancel
        End Function
    End Class
End Namespace
