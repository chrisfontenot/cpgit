﻿Namespace Client.Statement.DMP
    <Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
    Partial Public Class ClientTransactions
        Inherits DevExpress.XtraReports.UI.XtraReport

        'XtraReport overrides dispose to clean up the component list.
        <System.Diagnostics.DebuggerNonUserCode()> _
        Protected Overrides Sub Dispose(ByVal disposing As Boolean)
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
            MyBase.Dispose(disposing)
        End Sub

        'Required by the Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Designer
        'It can be modified using the Designer.  
        'Do not modify it using the code editor.
        <System.Diagnostics.DebuggerStepThrough()> _
        Private Sub InitializeComponent()
            Dim XrSummary1 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary()
            Dim XrSummary2 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary()
            Me.Detail = New DevExpress.XtraReports.UI.DetailBand()
            Me.XrTable3 = New DevExpress.XtraReports.UI.XRTable()
            Me.XrTableRow3 = New DevExpress.XtraReports.UI.XRTableRow()
            Me.XrTableCell_detail_type = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableCell_detail_date = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableCell_detail_amount = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableCell14 = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableCell9 = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableCell13 = New DevExpress.XtraReports.UI.XRTableCell()
            Me.TopMargin = New DevExpress.XtraReports.UI.TopMarginBand()
            Me.BottomMargin = New DevExpress.XtraReports.UI.BottomMarginBand()
            Me.ReportHeader = New DevExpress.XtraReports.UI.ReportHeaderBand()
            Me.XrTable2 = New DevExpress.XtraReports.UI.XRTable()
            Me.XrTableRow2 = New DevExpress.XtraReports.UI.XRTableRow()
            Me.XrTableCell_header_type = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableCell_header_date = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableCell_header_amount = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableCell11 = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableCell10 = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableCell6 = New DevExpress.XtraReports.UI.XRTableCell()
            Me.ReportFooter = New DevExpress.XtraReports.UI.ReportFooterBand()
            Me.XrTable1 = New DevExpress.XtraReports.UI.XRTable()
            Me.XrTableRow1 = New DevExpress.XtraReports.UI.XRTableRow()
            Me.XrTableCell_footer_type = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableCell_footer_date = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableCell_footer_amount = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableCell17 = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableCell3 = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableCell16 = New DevExpress.XtraReports.UI.XRTableCell()
            CType(Me.XrTable3, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.XrTable2, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.XrTable1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
            '
            'Detail
            '
            Me.Detail.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrTable3})
            Me.Detail.HeightF = 20.0!
            Me.Detail.Name = "Detail"
            Me.Detail.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
            Me.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
            '
            'XrTable3
            '
            Me.XrTable3.AnchorVertical = CType((DevExpress.XtraReports.UI.VerticalAnchorStyles.Top Or DevExpress.XtraReports.UI.VerticalAnchorStyles.Bottom), DevExpress.XtraReports.UI.VerticalAnchorStyles)
            Me.XrTable3.Borders = CType((((DevExpress.XtraPrinting.BorderSide.Left Or DevExpress.XtraPrinting.BorderSide.Top) _
                Or DevExpress.XtraPrinting.BorderSide.Right) _
                Or DevExpress.XtraPrinting.BorderSide.Bottom), DevExpress.XtraPrinting.BorderSide)
            Me.XrTable3.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
            Me.XrTable3.Name = "XrTable3"
            Me.XrTable3.Rows.AddRange(New DevExpress.XtraReports.UI.XRTableRow() {Me.XrTableRow3})
            Me.XrTable3.SizeF = New System.Drawing.SizeF(795.0!, 20.0!)
            Me.XrTable3.StylePriority.UseBorders = False
            '
            'XrTableRow3
            '
            Me.XrTableRow3.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.XrTableCell_detail_type, Me.XrTableCell_detail_date, Me.XrTableCell_detail_amount, Me.XrTableCell14, Me.XrTableCell9, Me.XrTableCell13})
            Me.XrTableRow3.Name = "XrTableRow3"
            Me.XrTableRow3.Weight = 1.0R
            '
            'XrTableCell_detail_type
            '
            Me.XrTableCell_detail_type.Borders = CType(((DevExpress.XtraPrinting.BorderSide.Left Or DevExpress.XtraPrinting.BorderSide.Right) _
                Or DevExpress.XtraPrinting.BorderSide.Bottom), DevExpress.XtraPrinting.BorderSide)
            Me.XrTableCell_detail_type.CanGrow = False
            Me.XrTableCell_detail_type.Name = "XrTableCell_detail_type"
            Me.XrTableCell_detail_type.Padding = New DevExpress.XtraPrinting.PaddingInfo(10, 0, 0, 0, 100.0!)
            Me.XrTableCell_detail_type.StylePriority.UseBorders = False
            Me.XrTableCell_detail_type.StylePriority.UsePadding = False
            Me.XrTableCell_detail_type.StylePriority.UseTextAlignment = False
            Me.XrTableCell_detail_type.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            Me.XrTableCell_detail_type.Weight = 0.83649050154775939R
            '
            'XrTableCell_detail_date
            '
            Me.XrTableCell_detail_date.Borders = CType((DevExpress.XtraPrinting.BorderSide.Right Or DevExpress.XtraPrinting.BorderSide.Bottom), DevExpress.XtraPrinting.BorderSide)
            Me.XrTableCell_detail_date.CanGrow = False
            Me.XrTableCell_detail_date.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "date_created", "{0:d}")})
            Me.XrTableCell_detail_date.Name = "XrTableCell_detail_date"
            Me.XrTableCell_detail_date.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 10, 0, 0, 100.0!)
            Me.XrTableCell_detail_date.StylePriority.UseBorders = False
            Me.XrTableCell_detail_date.StylePriority.UsePadding = False
            Me.XrTableCell_detail_date.StylePriority.UseTextAlignment = False
            Me.XrTableCell_detail_date.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrTableCell_detail_date.Weight = 0.4465283087964329R
            '
            'XrTableCell_detail_amount
            '
            Me.XrTableCell_detail_amount.Borders = CType((DevExpress.XtraPrinting.BorderSide.Right Or DevExpress.XtraPrinting.BorderSide.Bottom), DevExpress.XtraPrinting.BorderSide)
            Me.XrTableCell_detail_amount.CanGrow = False
            Me.XrTableCell_detail_amount.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "difference", "{0:c}")})
            Me.XrTableCell_detail_amount.Name = "XrTableCell_detail_amount"
            Me.XrTableCell_detail_amount.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 10, 0, 0, 100.0!)
            Me.XrTableCell_detail_amount.StylePriority.UseBorders = False
            Me.XrTableCell_detail_amount.StylePriority.UsePadding = False
            Me.XrTableCell_detail_amount.StylePriority.UseTextAlignment = False
            Me.XrTableCell_detail_amount.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrTableCell_detail_amount.Weight = 0.46384887695312493R
            '
            'XrTableCell14
            '
            Me.XrTableCell14.Borders = CType((DevExpress.XtraPrinting.BorderSide.Right Or DevExpress.XtraPrinting.BorderSide.Bottom), DevExpress.XtraPrinting.BorderSide)
            Me.XrTableCell14.CanGrow = False
            Me.XrTableCell14.Name = "XrTableCell14"
            Me.XrTableCell14.StylePriority.UseBorders = False
            Me.XrTableCell14.Weight = 0.40890548993956366R
            '
            'XrTableCell9
            '
            Me.XrTableCell9.Borders = CType((DevExpress.XtraPrinting.BorderSide.Right Or DevExpress.XtraPrinting.BorderSide.Bottom), DevExpress.XtraPrinting.BorderSide)
            Me.XrTableCell9.CanGrow = False
            Me.XrTableCell9.Name = "XrTableCell9"
            Me.XrTableCell9.StylePriority.UseBorders = False
            Me.XrTableCell9.Weight = 0.42909465285966986R
            '
            'XrTableCell13
            '
            Me.XrTableCell13.Borders = CType((DevExpress.XtraPrinting.BorderSide.Right Or DevExpress.XtraPrinting.BorderSide.Bottom), DevExpress.XtraPrinting.BorderSide)
            Me.XrTableCell13.CanGrow = False
            Me.XrTableCell13.Name = "XrTableCell13"
            Me.XrTableCell13.StylePriority.UseBorders = False
            Me.XrTableCell13.Weight = 0.41513216990344931R
            '
            'TopMargin
            '
            Me.TopMargin.HeightF = 25.0!
            Me.TopMargin.Name = "TopMargin"
            Me.TopMargin.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
            Me.TopMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
            '
            'BottomMargin
            '
            Me.BottomMargin.HeightF = 27.08333!
            Me.BottomMargin.Name = "BottomMargin"
            Me.BottomMargin.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
            Me.BottomMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
            '
            'ReportHeader
            '
            Me.ReportHeader.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrTable2})
            Me.ReportHeader.HeightF = 20.0!
            Me.ReportHeader.Name = "ReportHeader"
            '
            'XrTable2
            '
            Me.XrTable2.AnchorVertical = DevExpress.XtraReports.UI.VerticalAnchorStyles.Bottom
            Me.XrTable2.Borders = CType((((DevExpress.XtraPrinting.BorderSide.Left Or DevExpress.XtraPrinting.BorderSide.Top) _
                Or DevExpress.XtraPrinting.BorderSide.Right) _
                Or DevExpress.XtraPrinting.BorderSide.Bottom), DevExpress.XtraPrinting.BorderSide)
            Me.XrTable2.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
            Me.XrTable2.Name = "XrTable2"
            Me.XrTable2.Rows.AddRange(New DevExpress.XtraReports.UI.XRTableRow() {Me.XrTableRow2})
            Me.XrTable2.SizeF = New System.Drawing.SizeF(795.0!, 20.0!)
            Me.XrTable2.StylePriority.UseBorders = False
            '
            'XrTableRow2
            '
            Me.XrTableRow2.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.XrTableCell_header_type, Me.XrTableCell_header_date, Me.XrTableCell_header_amount, Me.XrTableCell11, Me.XrTableCell10, Me.XrTableCell6})
            Me.XrTableRow2.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrTableRow2.Name = "XrTableRow2"
            Me.XrTableRow2.StylePriority.UseFont = False
            Me.XrTableRow2.Weight = 1.0R
            '
            'XrTableCell_header_type
            '
            Me.XrTableCell_header_type.CanGrow = False
            Me.XrTableCell_header_type.Name = "XrTableCell_header_type"
            Me.XrTableCell_header_type.Padding = New DevExpress.XtraPrinting.PaddingInfo(10, 0, 0, 0, 100.0!)
            Me.XrTableCell_header_type.StylePriority.UsePadding = False
            Me.XrTableCell_header_type.StylePriority.UseTextAlignment = False
            Me.XrTableCell_header_type.Text = "TRANSACTION TYPE"
            Me.XrTableCell_header_type.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            Me.XrTableCell_header_type.Weight = 0.83649055912809545R
            '
            'XrTableCell_header_date
            '
            Me.XrTableCell_header_date.Borders = CType(((DevExpress.XtraPrinting.BorderSide.Top Or DevExpress.XtraPrinting.BorderSide.Right) _
                Or DevExpress.XtraPrinting.BorderSide.Bottom), DevExpress.XtraPrinting.BorderSide)
            Me.XrTableCell_header_date.CanGrow = False
            Me.XrTableCell_header_date.Name = "XrTableCell_header_date"
            Me.XrTableCell_header_date.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 10, 0, 0, 100.0!)
            Me.XrTableCell_header_date.StylePriority.UseBorders = False
            Me.XrTableCell_header_date.StylePriority.UsePadding = False
            Me.XrTableCell_header_date.StylePriority.UseTextAlignment = False
            Me.XrTableCell_header_date.Text = "DATE"
            Me.XrTableCell_header_date.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrTableCell_header_date.Weight = 0.4465283087964329R
            '
            'XrTableCell_header_amount
            '
            Me.XrTableCell_header_amount.Borders = CType(((DevExpress.XtraPrinting.BorderSide.Top Or DevExpress.XtraPrinting.BorderSide.Right) _
                Or DevExpress.XtraPrinting.BorderSide.Bottom), DevExpress.XtraPrinting.BorderSide)
            Me.XrTableCell_header_amount.CanGrow = False
            Me.XrTableCell_header_amount.Name = "XrTableCell_header_amount"
            Me.XrTableCell_header_amount.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 10, 0, 0, 100.0!)
            Me.XrTableCell_header_amount.StylePriority.UseBorders = False
            Me.XrTableCell_header_amount.StylePriority.UsePadding = False
            Me.XrTableCell_header_amount.StylePriority.UseTextAlignment = False
            Me.XrTableCell_header_amount.Text = "AMOUNT"
            Me.XrTableCell_header_amount.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrTableCell_header_amount.Weight = 0.46384904969413321R
            '
            'XrTableCell11
            '
            Me.XrTableCell11.Borders = CType(((DevExpress.XtraPrinting.BorderSide.Top Or DevExpress.XtraPrinting.BorderSide.Right) _
                Or DevExpress.XtraPrinting.BorderSide.Bottom), DevExpress.XtraPrinting.BorderSide)
            Me.XrTableCell11.CanGrow = False
            Me.XrTableCell11.Name = "XrTableCell11"
            Me.XrTableCell11.StylePriority.UseBorders = False
            Me.XrTableCell11.Weight = 0.40890566268057194R
            '
            'XrTableCell10
            '
            Me.XrTableCell10.Borders = CType(((DevExpress.XtraPrinting.BorderSide.Top Or DevExpress.XtraPrinting.BorderSide.Right) _
                Or DevExpress.XtraPrinting.BorderSide.Bottom), DevExpress.XtraPrinting.BorderSide)
            Me.XrTableCell10.CanGrow = False
            Me.XrTableCell10.Name = "XrTableCell10"
            Me.XrTableCell10.StylePriority.UseBorders = False
            Me.XrTableCell10.Weight = 0.42909433616782133R
            '
            'XrTableCell6
            '
            Me.XrTableCell6.Borders = CType(((DevExpress.XtraPrinting.BorderSide.Top Or DevExpress.XtraPrinting.BorderSide.Right) _
                Or DevExpress.XtraPrinting.BorderSide.Bottom), DevExpress.XtraPrinting.BorderSide)
            Me.XrTableCell6.CanGrow = False
            Me.XrTableCell6.Name = "XrTableCell6"
            Me.XrTableCell6.StylePriority.UseBorders = False
            Me.XrTableCell6.Weight = 0.41513208353294517R
            '
            'ReportFooter
            '
            Me.ReportFooter.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrTable1})
            Me.ReportFooter.HeightF = 20.0!
            Me.ReportFooter.Name = "ReportFooter"
            '
            'XrTable1
            '
            Me.XrTable1.Borders = CType((((DevExpress.XtraPrinting.BorderSide.Left Or DevExpress.XtraPrinting.BorderSide.Top) _
                Or DevExpress.XtraPrinting.BorderSide.Right) _
                Or DevExpress.XtraPrinting.BorderSide.Bottom), DevExpress.XtraPrinting.BorderSide)
            Me.XrTable1.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
            Me.XrTable1.Name = "XrTable1"
            Me.XrTable1.Rows.AddRange(New DevExpress.XtraReports.UI.XRTableRow() {Me.XrTableRow1})
            Me.XrTable1.SizeF = New System.Drawing.SizeF(795.0!, 20.0!)
            Me.XrTable1.StylePriority.UseBorders = False
            '
            'XrTableRow1
            '
            Me.XrTableRow1.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.XrTableCell_footer_type, Me.XrTableCell_footer_date, Me.XrTableCell_footer_amount, Me.XrTableCell17, Me.XrTableCell3, Me.XrTableCell16})
            Me.XrTableRow1.Name = "XrTableRow1"
            Me.XrTableRow1.Weight = 0.31506853713173355R
            '
            'XrTableCell_footer_type
            '
            Me.XrTableCell_footer_type.Borders = CType(((DevExpress.XtraPrinting.BorderSide.Left Or DevExpress.XtraPrinting.BorderSide.Right) _
                Or DevExpress.XtraPrinting.BorderSide.Bottom), DevExpress.XtraPrinting.BorderSide)
            Me.XrTableCell_footer_type.Name = "XrTableCell_footer_type"
            Me.XrTableCell_footer_type.Padding = New DevExpress.XtraPrinting.PaddingInfo(10, 0, 0, 0, 100.0!)
            Me.XrTableCell_footer_type.StylePriority.UseBorders = False
            Me.XrTableCell_footer_type.StylePriority.UsePadding = False
            Me.XrTableCell_footer_type.StylePriority.UseTextAlignment = False
            Me.XrTableCell_footer_type.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            Me.XrTableCell_footer_type.Weight = 0.83649055912809556R
            '
            'XrTableCell_footer_date
            '
            Me.XrTableCell_footer_date.Borders = CType((DevExpress.XtraPrinting.BorderSide.Right Or DevExpress.XtraPrinting.BorderSide.Bottom), DevExpress.XtraPrinting.BorderSide)
            Me.XrTableCell_footer_date.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "date_created")})
            Me.XrTableCell_footer_date.Name = "XrTableCell_footer_date"
            Me.XrTableCell_footer_date.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 10, 0, 0, 100.0!)
            Me.XrTableCell_footer_date.StylePriority.UseBorders = False
            Me.XrTableCell_footer_date.StylePriority.UsePadding = False
            Me.XrTableCell_footer_date.StylePriority.UseTextAlignment = False
            XrSummary1.FormatString = "{0:d}"
            XrSummary1.Func = DevExpress.XtraReports.UI.SummaryFunc.Max
            XrSummary1.Running = DevExpress.XtraReports.UI.SummaryRunning.Report
            Me.XrTableCell_footer_date.Summary = XrSummary1
            Me.XrTableCell_footer_date.Text = "XrTableCell_footer_date"
            Me.XrTableCell_footer_date.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrTableCell_footer_date.Weight = 0.44652842395710507R
            '
            'XrTableCell_footer_amount
            '
            Me.XrTableCell_footer_amount.Borders = CType((DevExpress.XtraPrinting.BorderSide.Right Or DevExpress.XtraPrinting.BorderSide.Bottom), DevExpress.XtraPrinting.BorderSide)
            Me.XrTableCell_footer_amount.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "difference")})
            Me.XrTableCell_footer_amount.Name = "XrTableCell_footer_amount"
            Me.XrTableCell_footer_amount.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 10, 0, 0, 100.0!)
            Me.XrTableCell_footer_amount.StylePriority.UseBorders = False
            Me.XrTableCell_footer_amount.StylePriority.UsePadding = False
            Me.XrTableCell_footer_amount.StylePriority.UseTextAlignment = False
            XrSummary2.FormatString = "{0:c}"
            XrSummary2.Running = DevExpress.XtraReports.UI.SummaryRunning.Report
            Me.XrTableCell_footer_amount.Summary = XrSummary2
            Me.XrTableCell_footer_amount.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrTableCell_footer_amount.Weight = 0.46384876179245282R
            '
            'XrTableCell17
            '
            Me.XrTableCell17.Borders = CType((DevExpress.XtraPrinting.BorderSide.Right Or DevExpress.XtraPrinting.BorderSide.Bottom), DevExpress.XtraPrinting.BorderSide)
            Me.XrTableCell17.Name = "XrTableCell17"
            Me.XrTableCell17.StylePriority.UseBorders = False
            Me.XrTableCell17.Weight = 0.40890548993956355R
            '
            'XrTableCell3
            '
            Me.XrTableCell3.Borders = CType((DevExpress.XtraPrinting.BorderSide.Right Or DevExpress.XtraPrinting.BorderSide.Bottom), DevExpress.XtraPrinting.BorderSide)
            Me.XrTableCell3.Name = "XrTableCell3"
            Me.XrTableCell3.StylePriority.UseBorders = False
            Me.XrTableCell3.Weight = 0.4290946528596698R
            '
            'XrTableCell16
            '
            Me.XrTableCell16.Borders = CType((DevExpress.XtraPrinting.BorderSide.Right Or DevExpress.XtraPrinting.BorderSide.Bottom), DevExpress.XtraPrinting.BorderSide)
            Me.XrTableCell16.Name = "XrTableCell16"
            Me.XrTableCell16.StylePriority.UseBorders = False
            Me.XrTableCell16.Weight = 0.4151321123231132R
            '
            'ClientTransactions
            '
            Me.Bands.AddRange(New DevExpress.XtraReports.UI.Band() {Me.Detail, Me.TopMargin, Me.BottomMargin, Me.ReportHeader, Me.ReportFooter})
            Me.Margins = New System.Drawing.Printing.Margins(25, 25, 25, 27)
            Me.ReportPrintOptions.DetailCountOnEmptyDataSource = 0
            Me.ReportPrintOptions.PrintOnEmptyDataSource = False
            Me.ScriptLanguage = DevExpress.XtraReports.ScriptLanguage.VisualBasic
            Me.Version = "11.2"
            CType(Me.XrTable3, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.XrTable2, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.XrTable1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me, System.ComponentModel.ISupportInitialize).EndInit()

        End Sub

        Friend WithEvents Detail As DevExpress.XtraReports.UI.DetailBand
        Friend WithEvents TopMargin As DevExpress.XtraReports.UI.TopMarginBand
        Friend WithEvents BottomMargin As DevExpress.XtraReports.UI.BottomMarginBand
        Friend WithEvents XrTable3 As DevExpress.XtraReports.UI.XRTable
        Friend WithEvents XrTableRow3 As DevExpress.XtraReports.UI.XRTableRow
        Friend WithEvents XrTableCell_detail_type As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents XrTableCell_detail_date As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents XrTableCell_detail_amount As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents XrTableCell14 As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents XrTableCell9 As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents XrTableCell13 As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents ReportHeader As DevExpress.XtraReports.UI.ReportHeaderBand
        Friend WithEvents XrTable2 As DevExpress.XtraReports.UI.XRTable
        Friend WithEvents XrTableRow2 As DevExpress.XtraReports.UI.XRTableRow
        Friend WithEvents XrTableCell_header_type As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents XrTableCell_header_date As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents XrTableCell_header_amount As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents XrTableCell11 As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents XrTableCell10 As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents XrTableCell6 As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents ReportFooter As DevExpress.XtraReports.UI.ReportFooterBand
        Friend WithEvents XrTable1 As DevExpress.XtraReports.UI.XRTable
        Friend WithEvents XrTableRow1 As DevExpress.XtraReports.UI.XRTableRow
        Friend WithEvents XrTableCell_footer_type As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents XrTableCell_footer_date As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents XrTableCell_footer_amount As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents XrTableCell17 As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents XrTableCell3 As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents XrTableCell16 As DevExpress.XtraReports.UI.XRTableCell
    End Class
End Namespace