﻿Namespace Client.Statement.DMP
    <Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
    Partial Public Class BaseClientStatementReport

        'XtraReport overrides dispose to clean up the component list.
        <System.Diagnostics.DebuggerNonUserCode()> _
        Protected Overrides Sub Dispose(ByVal disposing As Boolean)
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
            MyBase.Dispose(disposing)
        End Sub

        'Required by the Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Designer
        'It can be modified using the Designer.  
        'Do not modify it using the code editor.
        <System.Diagnostics.DebuggerStepThrough()> _
        Private Sub InitializeComponent()
            Me.XrSubreport_ClientStatementClientsSubReport = New DevExpress.XtraReports.UI.XRSubreport()
            Me.ClientStatementClientSubReport1 = New DebtPlus.Reports.Client.Statement.DMP.ClientStatementClientSubReport()
            CType(Me.ClientStatementClientSubReport1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
            '
            'Detail
            '
            Me.Detail.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrSubreport_ClientStatementClientsSubReport})
            Me.Detail.HeightF = 23.0!
            Me.Detail.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
            Me.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
            '
            'BottomMarginBand1
            '
            Me.BottomMarginBand1.HeightF = 26.99998!
            '
            'XrSubreport_ClientStatementClientsSubReport
            '
            Me.XrSubreport_ClientStatementClientsSubReport.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
            Me.XrSubreport_ClientStatementClientsSubReport.Name = "XrSubreport_ClientStatementClientsSubReport"
            Me.XrSubreport_ClientStatementClientsSubReport.ReportSource = Me.ClientStatementClientSubReport1
            Me.XrSubreport_ClientStatementClientsSubReport.SizeF = New System.Drawing.SizeF(799.9999!, 23.0!)
            '
            'BaseClientStatementReport
            '
            Me.Bands.AddRange(New DevExpress.XtraReports.UI.Band() {Me.TopMarginBand1, Me.Detail, Me.BottomMarginBand1})
            Me.Margins = New System.Drawing.Printing.Margins(25, 25, 25, 27)
            Me.ScriptLanguage = DevExpress.XtraReports.ScriptLanguage.VisualBasic
            Me.Version = "11.2"
            CType(Me.ClientStatementClientSubReport1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me, System.ComponentModel.ISupportInitialize).EndInit()

        End Sub

        Protected XrSubreport_ClientStatementClientsSubReport As DevExpress.XtraReports.UI.XRSubreport
        Private WithEvents ClientStatementClientSubReport1 As DebtPlus.Reports.Client.Statement.DMP.ClientStatementClientSubReport
    End Class
End Namespace
