﻿Namespace Client.Statement.DMP
    <Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
    Partial Public Class DebtTransactions
        Inherits DevExpress.XtraReports.UI.XtraReport

        'XtraReport overrides dispose to clean up the component list.
        <System.Diagnostics.DebuggerNonUserCode()> _
        Protected Overrides Sub Dispose(ByVal disposing As Boolean)
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
            MyBase.Dispose(disposing)
        End Sub

        'Required by the Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Designer
        'It can be modified using the Designer.  
        'Do not modify it using the code editor.
        <System.Diagnostics.DebuggerStepThrough()> _
        Private Sub InitializeComponent()
            Dim XrSummary1 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary()
            Dim XrSummary2 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary()
            Dim XrSummary3 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary()
            Dim XrSummary4 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary()
            Me.Detail = New DevExpress.XtraReports.UI.DetailBand()
            Me.XrLabel_detail_payments = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_detail_balance = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_detail_marker = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_detail_this_month = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_detail_creditor = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_detail_account_number = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_detail_orig_balance = New DevExpress.XtraReports.UI.XRLabel()
            Me.TopMargin = New DevExpress.XtraReports.UI.TopMarginBand()
            Me.BottomMargin = New DevExpress.XtraReports.UI.BottomMarginBand()
            Me.GroupHeader1 = New DevExpress.XtraReports.UI.GroupHeaderBand()
            Me.GroupFooter1 = New DevExpress.XtraReports.UI.GroupFooterBand()
            Me.XrTable3 = New DevExpress.XtraReports.UI.XRTable()
            Me.XrTableRow6 = New DevExpress.XtraReports.UI.XRTableRow()
            Me.XrTableCell_total_creditor = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableCell_total_account_number = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableCell_total_orig_balance = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableCell_total_this_month = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableCell_total_payments = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableCell_total_balance = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableCell31 = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableRow9 = New DevExpress.XtraReports.UI.XRTableRow()
            Me.XrTableCell29 = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTable4 = New DevExpress.XtraReports.UI.XRTable()
            Me.XrTableRow7 = New DevExpress.XtraReports.UI.XRTableRow()
            Me.XrTableCell14 = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableCell28 = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableCell15 = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableCell27 = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableCell16 = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableCell26 = New DevExpress.XtraReports.UI.XRTableCell()
            CType(Me.XrTable3, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.XrTable4, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
            '
            'Detail
            '
            Me.Detail.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel_detail_payments, Me.XrLabel_detail_balance, Me.XrLabel_detail_marker, Me.XrLabel_detail_this_month, Me.XrLabel_detail_creditor, Me.XrLabel_detail_account_number, Me.XrLabel_detail_orig_balance})
            Me.Detail.HeightF = 20.00001!
            Me.Detail.Name = "Detail"
            Me.Detail.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
            Me.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
            '
            'XrLabel_detail_payments
            '
            Me.XrLabel_detail_payments.Borders = CType((DevExpress.XtraPrinting.BorderSide.Right Or DevExpress.XtraPrinting.BorderSide.Bottom), DevExpress.XtraPrinting.BorderSide)
            Me.XrLabel_detail_payments.CanGrow = False
            Me.XrLabel_detail_payments.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "payments", "{0:c2}")})
            Me.XrLabel_detail_payments.LocationFloat = New DevExpress.Utils.PointFloat(571.278!, 0.0!)
            Me.XrLabel_detail_payments.Name = "XrLabel_detail_payments"
            Me.XrLabel_detail_payments.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_detail_payments.SizeF = New System.Drawing.SizeF(113.7088!, 20.0!)
            Me.XrLabel_detail_payments.StylePriority.UseBorders = False
            Me.XrLabel_detail_payments.StylePriority.UseTextAlignment = False
            Me.XrLabel_detail_payments.Text = "XrLabel3"
            Me.XrLabel_detail_payments.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel_detail_balance
            '
            Me.XrLabel_detail_balance.Borders = DevExpress.XtraPrinting.BorderSide.Bottom
            Me.XrLabel_detail_balance.CanGrow = False
            Me.XrLabel_detail_balance.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "balance", "{0:c2}")})
            Me.XrLabel_detail_balance.LocationFloat = New DevExpress.Utils.PointFloat(684.9869!, 0.0!)
            Me.XrLabel_detail_balance.Name = "XrLabel_detail_balance"
            Me.XrLabel_detail_balance.Padding = New DevExpress.XtraPrinting.PaddingInfo(5, 1, 5, 5, 100.0!)
            Me.XrLabel_detail_balance.SizeF = New System.Drawing.SizeF(99.99988!, 20.0!)
            Me.XrLabel_detail_balance.StylePriority.UseBorders = False
            Me.XrLabel_detail_balance.StylePriority.UsePadding = False
            Me.XrLabel_detail_balance.StylePriority.UseTextAlignment = False
            Me.XrLabel_detail_balance.Text = "XrLabel3"
            Me.XrLabel_detail_balance.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel_detail_marker
            '
            Me.XrLabel_detail_marker.Borders = CType((DevExpress.XtraPrinting.BorderSide.Right Or DevExpress.XtraPrinting.BorderSide.Bottom), DevExpress.XtraPrinting.BorderSide)
            Me.XrLabel_detail_marker.CanGrow = False
            Me.XrLabel_detail_marker.LocationFloat = New DevExpress.Utils.PointFloat(784.9868!, 0.0!)
            Me.XrLabel_detail_marker.Name = "XrLabel_detail_marker"
            Me.XrLabel_detail_marker.Padding = New DevExpress.XtraPrinting.PaddingInfo(1, 1, 5, 5, 100.0!)
            Me.XrLabel_detail_marker.SizeF = New System.Drawing.SizeF(10.01331!, 20.0!)
            Me.XrLabel_detail_marker.StylePriority.UseBorders = False
            Me.XrLabel_detail_marker.StylePriority.UsePadding = False
            Me.XrLabel_detail_marker.StylePriority.UseTextAlignment = False
            Me.XrLabel_detail_marker.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            '
            'XrLabel_detail_this_month
            '
            Me.XrLabel_detail_this_month.Borders = CType((DevExpress.XtraPrinting.BorderSide.Right Or DevExpress.XtraPrinting.BorderSide.Bottom), DevExpress.XtraPrinting.BorderSide)
            Me.XrLabel_detail_this_month.CanGrow = False
            Me.XrLabel_detail_this_month.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "this_month", "{0:c2}")})
            Me.XrLabel_detail_this_month.LocationFloat = New DevExpress.Utils.PointFloat(462.9167!, 0.0!)
            Me.XrLabel_detail_this_month.Name = "XrLabel_detail_this_month"
            Me.XrLabel_detail_this_month.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_detail_this_month.SizeF = New System.Drawing.SizeF(108.3612!, 20.0!)
            Me.XrLabel_detail_this_month.StylePriority.UseBorders = False
            Me.XrLabel_detail_this_month.StylePriority.UseTextAlignment = False
            Me.XrLabel_detail_this_month.Text = "XrLabel3"
            Me.XrLabel_detail_this_month.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel_detail_creditor
            '
            Me.XrLabel_detail_creditor.Borders = CType(((DevExpress.XtraPrinting.BorderSide.Left Or DevExpress.XtraPrinting.BorderSide.Right) _
                Or DevExpress.XtraPrinting.BorderSide.Bottom), DevExpress.XtraPrinting.BorderSide)
            Me.XrLabel_detail_creditor.CanGrow = False
            Me.XrLabel_detail_creditor.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "creditor_name")})
            Me.XrLabel_detail_creditor.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
            Me.XrLabel_detail_creditor.Name = "XrLabel_detail_creditor"
            Me.XrLabel_detail_creditor.Padding = New DevExpress.XtraPrinting.PaddingInfo(10, 5, 0, 0, 100.0!)
            Me.XrLabel_detail_creditor.SizeF = New System.Drawing.SizeF(221.6667!, 20.0!)
            Me.XrLabel_detail_creditor.StylePriority.UseBorders = False
            Me.XrLabel_detail_creditor.StylePriority.UsePadding = False
            Me.XrLabel_detail_creditor.StylePriority.UseTextAlignment = False
            Me.XrLabel_detail_creditor.Text = "XrLabel_detail_creditor"
            Me.XrLabel_detail_creditor.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            Me.XrLabel_detail_creditor.WordWrap = False
            '
            'XrLabel_detail_account_number
            '
            Me.XrLabel_detail_account_number.Borders = CType((DevExpress.XtraPrinting.BorderSide.Right Or DevExpress.XtraPrinting.BorderSide.Bottom), DevExpress.XtraPrinting.BorderSide)
            Me.XrLabel_detail_account_number.CanGrow = False
            Me.XrLabel_detail_account_number.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "account_number")})
            Me.XrLabel_detail_account_number.LocationFloat = New DevExpress.Utils.PointFloat(221.6667!, 0.0!)
            Me.XrLabel_detail_account_number.Name = "XrLabel_detail_account_number"
            Me.XrLabel_detail_account_number.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_detail_account_number.SizeF = New System.Drawing.SizeF(118.3333!, 20.0!)
            Me.XrLabel_detail_account_number.StylePriority.UseBorders = False
            Me.XrLabel_detail_account_number.StylePriority.UseTextAlignment = False
            Me.XrLabel_detail_account_number.Text = "XrLabel_detail_account_number"
            Me.XrLabel_detail_account_number.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
            '
            'XrLabel_detail_orig_balance
            '
            Me.XrLabel_detail_orig_balance.Borders = CType((DevExpress.XtraPrinting.BorderSide.Right Or DevExpress.XtraPrinting.BorderSide.Bottom), DevExpress.XtraPrinting.BorderSide)
            Me.XrLabel_detail_orig_balance.CanGrow = False
            Me.XrLabel_detail_orig_balance.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "original_balance", "{0:c2}")})
            Me.XrLabel_detail_orig_balance.LocationFloat = New DevExpress.Utils.PointFloat(340.0001!, 0.0!)
            Me.XrLabel_detail_orig_balance.Name = "XrLabel_detail_orig_balance"
            Me.XrLabel_detail_orig_balance.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_detail_orig_balance.SizeF = New System.Drawing.SizeF(122.9166!, 20.0!)
            Me.XrLabel_detail_orig_balance.StylePriority.UseBorders = False
            Me.XrLabel_detail_orig_balance.StylePriority.UseTextAlignment = False
            Me.XrLabel_detail_orig_balance.Text = "XrLabel_detail_orig_balance"
            Me.XrLabel_detail_orig_balance.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'TopMargin
            '
            Me.TopMargin.HeightF = 25.0!
            Me.TopMargin.Name = "TopMargin"
            Me.TopMargin.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
            Me.TopMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
            '
            'BottomMargin
            '
            Me.BottomMargin.HeightF = 25.29167!
            Me.BottomMargin.Name = "BottomMargin"
            Me.BottomMargin.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
            Me.BottomMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
            '
            'GroupHeader1
            '
            Me.GroupHeader1.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrTable4})
            Me.GroupHeader1.GroupUnion = DevExpress.XtraReports.UI.GroupUnion.WithFirstDetail
            Me.GroupHeader1.HeightF = 35.0!
            Me.GroupHeader1.Name = "GroupHeader1"
            Me.GroupHeader1.RepeatEveryPage = True
            '
            'GroupFooter1
            '
            Me.GroupFooter1.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrTable3})
            Me.GroupFooter1.GroupUnion = DevExpress.XtraReports.UI.GroupFooterUnion.WithLastDetail
            Me.GroupFooter1.HeightF = 40.0!
            Me.GroupFooter1.Name = "GroupFooter1"
            '
            'XrTable3
            '
            Me.XrTable3.Borders = CType((DevExpress.XtraPrinting.BorderSide.Right Or DevExpress.XtraPrinting.BorderSide.Bottom), DevExpress.XtraPrinting.BorderSide)
            Me.XrTable3.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
            Me.XrTable3.Name = "XrTable3"
            Me.XrTable3.Padding = New DevExpress.XtraPrinting.PaddingInfo(5, 5, 5, 5, 100.0!)
            Me.XrTable3.Rows.AddRange(New DevExpress.XtraReports.UI.XRTableRow() {Me.XrTableRow6, Me.XrTableRow9})
            Me.XrTable3.SizeF = New System.Drawing.SizeF(795.0!, 40.0!)
            Me.XrTable3.StylePriority.UseBorders = False
            Me.XrTable3.StylePriority.UsePadding = False
            '
            'XrTableRow6
            '
            Me.XrTableRow6.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.XrTableCell_total_creditor, Me.XrTableCell_total_account_number, Me.XrTableCell_total_orig_balance, Me.XrTableCell_total_this_month, Me.XrTableCell_total_payments, Me.XrTableCell_total_balance, Me.XrTableCell31})
            Me.XrTableRow6.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrTableRow6.Name = "XrTableRow6"
            Me.XrTableRow6.StylePriority.UseFont = False
            Me.XrTableRow6.Weight = 0.8398950251279157R
            '
            'XrTableCell_total_creditor
            '
            Me.XrTableCell_total_creditor.Borders = CType((DevExpress.XtraPrinting.BorderSide.Left Or DevExpress.XtraPrinting.BorderSide.Bottom), DevExpress.XtraPrinting.BorderSide)
            Me.XrTableCell_total_creditor.Name = "XrTableCell_total_creditor"
            Me.XrTableCell_total_creditor.Padding = New DevExpress.XtraPrinting.PaddingInfo(10, 5, 5, 5, 100.0!)
            Me.XrTableCell_total_creditor.StylePriority.UseBorders = False
            Me.XrTableCell_total_creditor.StylePriority.UsePadding = False
            Me.XrTableCell_total_creditor.StylePriority.UseTextAlignment = False
            Me.XrTableCell_total_creditor.Text = "TOTALS"
            Me.XrTableCell_total_creditor.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            Me.XrTableCell_total_creditor.Weight = 0.83647797655717349R
            '
            'XrTableCell_total_account_number
            '
            Me.XrTableCell_total_account_number.Name = "XrTableCell_total_account_number"
            Me.XrTableCell_total_account_number.StylePriority.UseTextAlignment = False
            Me.XrTableCell_total_account_number.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            Me.XrTableCell_total_account_number.Weight = 0.44654104998490862R
            '
            'XrTableCell_total_orig_balance
            '
            Me.XrTableCell_total_orig_balance.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "original_balance")})
            Me.XrTableCell_total_orig_balance.Name = "XrTableCell_total_orig_balance"
            Me.XrTableCell_total_orig_balance.StylePriority.UseTextAlignment = False
            XrSummary1.FormatString = "{0:c2}"
            XrSummary1.Running = DevExpress.XtraReports.UI.SummaryRunning.Report
            Me.XrTableCell_total_orig_balance.Summary = XrSummary1
            Me.XrTableCell_total_orig_balance.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrTableCell_total_orig_balance.Weight = 0.4638364916280483R
            '
            'XrTableCell_total_this_month
            '
            Me.XrTableCell_total_this_month.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "this_month")})
            Me.XrTableCell_total_this_month.Name = "XrTableCell_total_this_month"
            Me.XrTableCell_total_this_month.StylePriority.UseTextAlignment = False
            XrSummary2.FormatString = "{0:c2}"
            XrSummary2.Running = DevExpress.XtraReports.UI.SummaryRunning.Report
            Me.XrTableCell_total_this_month.Summary = XrSummary2
            Me.XrTableCell_total_this_month.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrTableCell_total_this_month.Weight = 0.40890993842895323R
            '
            'XrTableCell_total_payments
            '
            Me.XrTableCell_total_payments.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "payments")})
            Me.XrTableCell_total_payments.Name = "XrTableCell_total_payments"
            Me.XrTableCell_total_payments.StylePriority.UseTextAlignment = False
            XrSummary3.FormatString = "{0:c2}"
            XrSummary3.Running = DevExpress.XtraReports.UI.SummaryRunning.Report
            Me.XrTableCell_total_payments.Summary = XrSummary3
            Me.XrTableCell_total_payments.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrTableCell_total_payments.Weight = 0.42909052497766259R
            '
            'XrTableCell_total_balance
            '
            Me.XrTableCell_total_balance.Borders = DevExpress.XtraPrinting.BorderSide.Bottom
            Me.XrTableCell_total_balance.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "balance")})
            Me.XrTableCell_total_balance.Name = "XrTableCell_total_balance"
            Me.XrTableCell_total_balance.Padding = New DevExpress.XtraPrinting.PaddingInfo(5, 1, 5, 5, 100.0!)
            Me.XrTableCell_total_balance.StylePriority.UseBorders = False
            Me.XrTableCell_total_balance.StylePriority.UsePadding = False
            Me.XrTableCell_total_balance.StylePriority.UseTextAlignment = False
            XrSummary4.FormatString = "{0:c2}"
            XrSummary4.Running = DevExpress.XtraReports.UI.SummaryRunning.Report
            Me.XrTableCell_total_balance.Summary = XrSummary4
            Me.XrTableCell_total_balance.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrTableCell_total_balance.Weight = 0.37735832298550553R
            '
            'XrTableCell31
            '
            Me.XrTableCell31.Name = "XrTableCell31"
            Me.XrTableCell31.StylePriority.UseTextAlignment = False
            Me.XrTableCell31.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            Me.XrTableCell31.Weight = 0.037785695437748082R
            '
            'XrTableRow9
            '
            Me.XrTableRow9.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.XrTableCell29})
            Me.XrTableRow9.Name = "XrTableRow9"
            Me.XrTableRow9.Weight = 0.83989500111880355R
            '
            'XrTableCell29
            '
            Me.XrTableCell29.Borders = CType(((DevExpress.XtraPrinting.BorderSide.Left Or DevExpress.XtraPrinting.BorderSide.Right) _
                Or DevExpress.XtraPrinting.BorderSide.Bottom), DevExpress.XtraPrinting.BorderSide)
            Me.XrTableCell29.Multiline = True
            Me.XrTableCell29.Name = "XrTableCell29"
            Me.XrTableCell29.Padding = New DevExpress.XtraPrinting.PaddingInfo(5, 1, 5, 5, 100.0!)
            Me.XrTableCell29.StylePriority.UseBorders = False
            Me.XrTableCell29.StylePriority.UsePadding = False
            Me.XrTableCell29.StylePriority.UseTextAlignment = False
            Me.XrTableCell29.Text = "Estimated Interest Calculated *"
            Me.XrTableCell29.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrTableCell29.Weight = 2.9999999999999996R
            '
            'XrTable4
            '
            Me.XrTable4.AnchorVertical = DevExpress.XtraReports.UI.VerticalAnchorStyles.Bottom
            Me.XrTable4.Borders = CType(((DevExpress.XtraPrinting.BorderSide.Top Or DevExpress.XtraPrinting.BorderSide.Right) _
                Or DevExpress.XtraPrinting.BorderSide.Bottom), DevExpress.XtraPrinting.BorderSide)
            Me.XrTable4.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
            Me.XrTable4.Name = "XrTable4"
            Me.XrTable4.Padding = New DevExpress.XtraPrinting.PaddingInfo(5, 5, 5, 5, 100.0!)
            Me.XrTable4.Rows.AddRange(New DevExpress.XtraReports.UI.XRTableRow() {Me.XrTableRow7})
            Me.XrTable4.SizeF = New System.Drawing.SizeF(794.9999!, 35.0!)
            Me.XrTable4.StylePriority.UseBorders = False
            Me.XrTable4.StylePriority.UsePadding = False
            '
            'XrTableRow7
            '
            Me.XrTableRow7.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.XrTableCell14, Me.XrTableCell28, Me.XrTableCell15, Me.XrTableCell27, Me.XrTableCell16, Me.XrTableCell26})
            Me.XrTableRow7.Name = "XrTableRow7"
            Me.XrTableRow7.Weight = 0.7924523967067636R
            '
            'XrTableCell14
            '
            Me.XrTableCell14.Borders = CType((((DevExpress.XtraPrinting.BorderSide.Left Or DevExpress.XtraPrinting.BorderSide.Top) _
                Or DevExpress.XtraPrinting.BorderSide.Right) _
                Or DevExpress.XtraPrinting.BorderSide.Bottom), DevExpress.XtraPrinting.BorderSide)
            Me.XrTableCell14.CanGrow = False
            Me.XrTableCell14.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrTableCell14.Name = "XrTableCell14"
            Me.XrTableCell14.Padding = New DevExpress.XtraPrinting.PaddingInfo(10, 5, 5, 5, 100.0!)
            Me.XrTableCell14.StylePriority.UseBorders = False
            Me.XrTableCell14.StylePriority.UseFont = False
            Me.XrTableCell14.StylePriority.UsePadding = False
            Me.XrTableCell14.StylePriority.UseTextAlignment = False
            Me.XrTableCell14.Text = "CREDITOR"
            Me.XrTableCell14.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft
            Me.XrTableCell14.Weight = 0.83647822003045347R
            '
            'XrTableCell28
            '
            Me.XrTableCell28.CanGrow = False
            Me.XrTableCell28.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrTableCell28.Name = "XrTableCell28"
            Me.XrTableCell28.StylePriority.UseFont = False
            Me.XrTableCell28.StylePriority.UseTextAlignment = False
            Me.XrTableCell28.Text = "ACCOUNT"
            Me.XrTableCell28.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomCenter
            Me.XrTableCell28.Weight = 0.44654088599366737R
            '
            'XrTableCell15
            '
            Me.XrTableCell15.CanGrow = False
            Me.XrTableCell15.Font = New System.Drawing.Font("Times New Roman", 7.0!, System.Drawing.FontStyle.Bold)
            Me.XrTableCell15.Multiline = True
            Me.XrTableCell15.Name = "XrTableCell15"
            Me.XrTableCell15.StylePriority.UseFont = False
            Me.XrTableCell15.StylePriority.UseTextAlignment = False
            Me.XrTableCell15.Text = "ORIGINAL" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "BALANCE"
            Me.XrTableCell15.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight
            Me.XrTableCell15.Weight = 0.46383655554282932R
            '
            'XrTableCell27
            '
            Me.XrTableCell27.CanGrow = False
            Me.XrTableCell27.Font = New System.Drawing.Font("Times New Roman", 7.0!, System.Drawing.FontStyle.Bold)
            Me.XrTableCell27.Multiline = True
            Me.XrTableCell27.Name = "XrTableCell27"
            Me.XrTableCell27.StylePriority.UseFont = False
            Me.XrTableCell27.StylePriority.UseTextAlignment = False
            Me.XrTableCell27.Text = "PAYMENTS" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "THIS MONTH"
            Me.XrTableCell27.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight
            Me.XrTableCell27.Weight = 0.40890993418740879R
            '
            'XrTableCell16
            '
            Me.XrTableCell16.CanGrow = False
            Me.XrTableCell16.Font = New System.Drawing.Font("Times New Roman", 7.0!, System.Drawing.FontStyle.Bold)
            Me.XrTableCell16.Multiline = True
            Me.XrTableCell16.Name = "XrTableCell16"
            Me.XrTableCell16.StylePriority.UseFont = False
            Me.XrTableCell16.StylePriority.UseTextAlignment = False
            Me.XrTableCell16.Text = "PAYMENTS" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "TO DATE"
            Me.XrTableCell16.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight
            Me.XrTableCell16.Weight = 0.42909028505678365R
            '
            'XrTableCell26
            '
            Me.XrTableCell26.CanGrow = False
            Me.XrTableCell26.Font = New System.Drawing.Font("Times New Roman", 7.0!, System.Drawing.FontStyle.Bold)
            Me.XrTableCell26.Multiline = True
            Me.XrTableCell26.Name = "XrTableCell26"
            Me.XrTableCell26.StylePriority.UseFont = False
            Me.XrTableCell26.StylePriority.UseTextAlignment = False
            Me.XrTableCell26.Text = "ESTIMATED" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "BALANCE"
            Me.XrTableCell26.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight
            Me.XrTableCell26.Weight = 0.4151441191888573R
            '
            'DebtTransactions
            '
            Me.Bands.AddRange(New DevExpress.XtraReports.UI.Band() {Me.Detail, Me.TopMargin, Me.BottomMargin, Me.GroupHeader1, Me.GroupFooter1})
            Me.Margins = New System.Drawing.Printing.Margins(25, 25, 25, 25)
            Me.Version = "11.2"
            CType(Me.XrTable3, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.XrTable4, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me, System.ComponentModel.ISupportInitialize).EndInit()

        End Sub
        Friend WithEvents Detail As DevExpress.XtraReports.UI.DetailBand
        Friend WithEvents TopMargin As DevExpress.XtraReports.UI.TopMarginBand
        Friend WithEvents BottomMargin As DevExpress.XtraReports.UI.BottomMarginBand
        Friend WithEvents XrLabel_detail_payments As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_detail_balance As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_detail_marker As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_detail_this_month As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_detail_creditor As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_detail_account_number As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_detail_orig_balance As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents GroupHeader1 As DevExpress.XtraReports.UI.GroupHeaderBand
        Friend WithEvents GroupFooter1 As DevExpress.XtraReports.UI.GroupFooterBand
        Friend WithEvents XrTable3 As DevExpress.XtraReports.UI.XRTable
        Friend WithEvents XrTableRow6 As DevExpress.XtraReports.UI.XRTableRow
        Friend WithEvents XrTableCell_total_creditor As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents XrTableCell_total_account_number As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents XrTableCell_total_orig_balance As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents XrTableCell_total_this_month As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents XrTableCell_total_payments As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents XrTableCell_total_balance As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents XrTableCell31 As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents XrTableRow9 As DevExpress.XtraReports.UI.XRTableRow
        Friend WithEvents XrTableCell29 As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents XrTable4 As DevExpress.XtraReports.UI.XRTable
        Friend WithEvents XrTableRow7 As DevExpress.XtraReports.UI.XRTableRow
        Friend WithEvents XrTableCell14 As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents XrTableCell28 As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents XrTableCell15 As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents XrTableCell27 As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents XrTableCell16 As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents XrTableCell26 As DevExpress.XtraReports.UI.XRTableCell
    End Class
End Namespace
