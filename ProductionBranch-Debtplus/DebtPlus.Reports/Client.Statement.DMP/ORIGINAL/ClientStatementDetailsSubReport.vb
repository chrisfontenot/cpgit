﻿Namespace Client.Statement.DMP
    Public Class ClientStatementDetailsSubReport

        ''' <summary>
        ''' Initialize for a new period range
        ''' </summary>
        Public Sub New()
            MyBase.New()
            InitializeComponent()
        End Sub

        ''' <summary>
        ''' Select statement to access the client transaction data
        ''' </summary>
        Public Shared ReadOnly Property Select_registers_client As String
            Get
                Return "SELECT [client_register],[client],[tran_type],[credit_amt],[debit_amt],isnull([credit_amt],0)-isnull([debit_amt],0) as difference,[date_created] FROM [registers_client]"
            End Get
        End Property

        ''' <summary>
        ''' Select statement to access the detail information for the statement
        ''' </summary>
        Public Shared ReadOnly Property Select_client_statement_details As String
            Get
                Return "SELECT d.[client_statement_detail],d.[client_statement],d.[current_balance] as balance,isnull(d.[debits],0)-isnull(d.[credits],0) as this_month,isnull(d.[credits],0) as credits,d.[interest],d.[payments_to_date] as payments,d.[original_balance],d.[interest_rate],coalesce(cr.[creditor_name],cc.[creditor_name],'') AS [creditor_name],right('XXXX' + isnull(dbo.statement_account_number(cc.account_number),''),4) as account_number FROM [client_statement_details] d INNER JOIN [client_creditor] cc ON d.[client_creditor]=cc.[client_creditor] LEFT OUTER JOIN [creditors] cr ON cc.[creditor]=cr.[creditor]"
            End Get
        End Property

        ''' <summary>
        ''' Filter criteria for the current report
        ''' </summary>
        Private FilterCriteria As SelectionParameters = Nothing
        Private IsMultiPage As Boolean = False

        ''' <summary>
        ''' Dataset for the report information
        ''' </summary>
        Private ds As New System.Data.DataSet("ds")

        ''' <summary>
        ''' Process a change in the report's data row. We need to edit the items that vary depending upon the client_statement_clients row
        ''' </summary>
        Public Sub DefineDataSource(FilterCriteria As SelectionParameters)
            Me.FilterCriteria = FilterCriteria

            ' Ensure that the dataset is empty
            ds.Clear()

            ' Find the period dates. Correct the ending date to be the next day at midnight.
            Dim fromDate As DateTime = DebtPlus.Utils.Nulls.v_DateTime(FilterCriteria.client_statement_batch("period_start")).GetValueOrDefault().Date
            Dim toDate As DateTime = DebtPlus.Utils.Nulls.v_DateTime(FilterCriteria.client_statement_batch("period_end")).GetValueOrDefault().Date.AddDays(1)

            ' Supply the information from the statement batch row as needed
            Dim statementDate As DateTime = DebtPlus.Utils.Nulls.v_DateTime(FilterCriteria.client_statement_batch("statement_date")).GetValueOrDefault()
            XrLabel_statement_date_2.Text = String.Format("STATEMENT DATE: {0:d}", statementDate)
            XrTableCell_statement_date_1.Text = String.Format("{0:d}", statementDate)

            Dim note As String = If(DebtPlus.Utils.Nulls.v_String(FilterCriteria.client_statement_batch("note")), "")
            XrLabel_message.Text = note

            ' Format the fields that we can't bind since we are not the master report.
            ' (We are a sub-report that is defined in a different module so the bindings don't work.)
            XrTableCell_due_date.Text = DebtPlus.Utils.Nulls.v_String(FilterCriteria.client_statement("expected_deposit_date"))
            XrTableCell_due_amount.Text = DebtPlus.Utils.Nulls.v_String(FilterCriteria.client_statement("expected_deposit_amt"))
            XrTableCell_counselor_1.Text = DebtPlus.Utils.Nulls.v_String(FilterCriteria.client_statement("counselor_name"))
            XrLabel_counselor_2.Text = String.Format("COUNSELOR: {0}", DebtPlus.Utils.Nulls.v_String(FilterCriteria.client_statement("counselor_name")))

            ' Is the client statement a multi-page report or not?
            IsMultiPage = DebtPlus.Utils.Nulls.v_Int32(FilterCriteria.client_statement("IsMultiPage")) <> 0

            ' Enable or disable the final statement notice where needed
            XrLabel_FinalStatement.Visible = DebtPlus.Utils.Nulls.v_Int32(FilterCriteria.client_statement("isFinal")).GetValueOrDefault(0) <> 0

            ' The client ID is not a string.
            Dim stmtClient As System.Nullable(Of Int32) = DebtPlus.Utils.Nulls.v_Int32(FilterCriteria.client_statement("client"))
            XrTableCell_client_1.Text = If(stmtClient.HasValue, stmtClient.Value.ToString("0000000"), String.Empty)
            XrLabel_client_2.Text = String.Format("CLIENT ID: {0}", XrTableCell_client_1.Text)

            Using cn As New System.Data.SqlClient.SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
                cn.Open()

                ' Read the client address information
                XrLabel_client_address.Text = ReadClientAddress(cn, FilterCriteria)

                ' Retrieve the dataset for the client transactions
                Using cmd As New System.Data.SqlClient.SqlCommand()
                    cmd.Connection = cn
                    cmd.CommandType = CommandType.Text
                    cmd.CommandText = String.Format("{0} WHERE ([client] = @ClientID) AND ([date_created] >= @FromDate AND [date_created] < @ToDate) ORDER BY [date_created]", Select_registers_client)
                    cmd.Parameters.Add("@ClientID", SqlDbType.Int).Value = FilterCriteria.CurrentClientID
                    cmd.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = fromDate
                    cmd.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = toDate

                    Using da As New System.Data.SqlClient.SqlDataAdapter(cmd)
                        da.Fill(ds, "registers_client")
                        Dim tbl As System.Data.DataTable = ds.Tables("registers_client")
                        ClientTransactions1.DataSource = tbl.DefaultView
                    End Using
                End Using

                ' Retrieve the dataset for the current debt list
                Using cmd As New System.Data.SqlClient.SqlCommand()
                    cmd.Connection = cn
                    cmd.CommandText = String.Format("{0} WHERE d.[client_statement]=@clientStatement", Select_client_statement_details)
                    cmd.CommandType = CommandType.Text
                    cmd.Parameters.Add("@clientStatement", SqlDbType.UniqueIdentifier).Value = FilterCriteria.client_statement("client_statement")

                    Using da As New System.Data.SqlClient.SqlDataAdapter(cmd)
                        da.Fill(ds, "client_statement_details")
                        Dim tbl As System.Data.DataTable = ds.Tables("client_statement_details")
                        DebtTransactions1.DataSource = tbl.DefaultView
                    End Using
                End Using
            End Using

            ' Enable or disable the extra blank page on the footer as needed
            XrPageBreak1.Visible = IsMultiPage
            XrLabel_Blank.Visible = IsMultiPage
        End Sub

        Private Function ReadClientAddress(cn As System.Data.SqlClient.SqlConnection, FilterCriteria As SelectionParameters) As String
            Dim sb As New System.Text.StringBuilder()

            ' Retrieve the client address information
            Using cmd As New System.Data.SqlClient.SqlCommand()
                cmd.Connection = cn
                cmd.CommandType = CommandType.Text
                cmd.CommandText = "SELECT v.[name],v.[addr1],v.[addr2],v.[addr3] FROM [view_client_address] v WHERE v.[client] = @ClientID"
                cmd.Parameters.Add("@ClientID", SqlDbType.Int).Value = FilterCriteria.CurrentClientID

                Using rdr As System.Data.SqlClient.SqlDataReader = cmd.ExecuteReader(CommandBehavior.SingleRow)
                    If rdr.Read() Then
                        For FieldID As Int32 = 0 To 3
                            If Not rdr.IsDBNull(FieldID) Then
                                Dim strItem As String = rdr.GetString(FieldID)
                                If Not String.IsNullOrWhiteSpace(strItem) Then
                                    sb.Append(System.Environment.NewLine)
                                    sb.Append(strItem.Trim())
                                End If
                            End If
                        Next
                    End If
                    rdr.Close()
                End Using
            End Using

            If sb.Length > 0 Then
                sb.Remove(0, 2)
            End If

            Return sb.ToString()
        End Function
    End Class
End Namespace
