﻿Namespace Client.Statement.DMP
    Public Class ClientStatusReport
        Inherits ClientStatementDetailsSubReport
        Implements DebtPlus.Interfaces.Client.IClient
        Implements DebtPlus.Interfaces.Reports.IReports

        ''' <summary>
        ''' Handle the Dispose method
        ''' </summary>
        Protected Overrides Sub Dispose(disposing As Boolean)
            Try
                If disposing Then
                    If ds IsNot Nothing Then ds.Dispose()
                End If

                ds = Nothing
                selInfo = Nothing

            Finally
                MyBase.Dispose(disposing)
            End Try
        End Sub

        ''' <summary>
        ''' Initialize the new context
        ''' </summary>
        Public Sub New()
            MyBase.New()
        End Sub

        ' Selection information
        Private selInfo As New DebtPlus.Reports.Client.Statement.DMP.SelectionParameters()
        Private ds As New System.Data.DataSet("ds")

        ''' <summary>
        ''' Request the report parameters to run the report
        ''' </summary>
        Public Function RequestReportParameters() As System.Windows.Forms.DialogResult Implements Interfaces.Reports.IReports.RequestReportParameters

            ' Define the report selection criteria
            selInfo.ClientID = ClientId
            selInfo.PageItems = SelectionParameters.PageItemEnum.AllStatements
            selInfo.AchItems = SelectionParameters.AchEnum.AllStatements

            Try
                ' Attempt to locate the latest statement batch for the client in question.
                Dim sqlInfo As DebtPlus.LINQ.SQLInfoClass = DebtPlus.LINQ.SQLInfoClass.getDefault()
                Using cn As New System.Data.SqlClient.SqlConnection(sqlInfo.ConnectionString)
                    cn.Open()

                    ' Read the statement batch for this client.
                    Using cmd As New System.Data.SqlClient.SqlCommand()
                        cmd.Connection = cn
                        cmd.CommandType = CommandType.Text
                        cmd.CommandText = String.Format("SET ROWCOUNT 1; {0} INNER JOIN client_statement_clients c ON c.[client_statement_batch] = b.[client_statement_batch] WHERE c.[client] = @client ORDER BY b.[statement_date] DESC", DebtPlus.Reports.Client.Statement.DMP.BaseClientStatementReport.Select_client_statement_batches)
                        cmd.Parameters.Add("@client", SqlDbType.Int).Value = ClientId
                        Using da As New System.Data.SqlClient.SqlDataAdapter(cmd)
                            da.Fill(ds, "client_statement_batches")
                        End Using
                    End Using

                    Dim tbl As System.Data.DataTable = ds.Tables("client_statement_batches")
                    Dim vue As System.Data.DataView = tbl.DefaultView()
                    If vue.Count > 0 Then
                        selInfo.client_statement_batch = vue(0)
                        selInfo.BatchID = DebtPlus.Utils.Nulls.v_Int32(selInfo.client_statement_batch("client_statement_batch"))

                        ' Read the client statement from the batch.
                        Using cmd As New System.Data.SqlClient.SqlCommand()
                            cmd.Connection = cn
                            cmd.CommandType = CommandType.Text
                            cmd.CommandText = String.Format("{0} WHERE [client] = @client AND [client_statement_batch] = @client_statement_batch", DebtPlus.Reports.Client.Statement.DMP.ClientStatementClientSubReport.Select_client_statement_clients)
                            cmd.Parameters.Add("@client", SqlDbType.Int).Value = ClientId
                            cmd.Parameters.Add("@client_statement_batch", SqlDbType.Int).Value = selInfo.BatchID.Value
                            Using da As New System.Data.SqlClient.SqlDataAdapter(cmd)
                                da.Fill(ds, "client_statement_clients")
                            End Using
                        End Using

                        ' Pass the parameters to the report so that it will run
                        tbl = ds.Tables("client_statement_clients")
                        vue = tbl.DefaultView
                        If vue.Count > 0 Then
                            selInfo.client_statement = vue(0)
                            MyBase.DefineDataSource(selInfo)
                            Return DialogResult.OK
                        End If
                    End If
                End Using

                ' Complain that there are no client statements if we can not find any.
                DebtPlus.Data.Forms.MessageBox.Show("We are unable to locate a statement for the client", "Sorry, but there is no data", MessageBoxButtons.OK)

            Catch ex As System.Data.SqlClient.SqlException
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex)
            End Try

            ' On error or not found, cancel the report.
            Return DialogResult.Cancel
        End Function

        '' --------------------------------------------------------------------------------------------------------------
        '' --------------------------------------------------------------------------------------------------------------
        '' -----       Implement the DebtPlus.Interfaces.Client.IClient interface       ---------------------------------
        '' --------------------------------------------------------------------------------------------------------------
        '' --------------------------------------------------------------------------------------------------------------

        ''' <summary>
        ''' Client ID
        ''' </summary>
        Public Property ClientId As Integer Implements Interfaces.Client.IClient.ClientId

        '' --------------------------------------------------------------------------------------------------------------
        '' --------------------------------------------------------------------------------------------------------------
        '' -----       Implement the DebtPlus.Interfaces.Report.IReports interface       --------------------------------
        '' --------------------------------------------------------------------------------------------------------------
        '' --------------------------------------------------------------------------------------------------------------

        Public Property AllowParameterChangesByUser As Boolean Implements Interfaces.Reports.IReports.AllowParameterChangesByUser

        Public Sub CreatePdfDocument(path As String) Implements Interfaces.Reports.IReports.CreatePdfDocument
            Me.ExportOptions.Pdf.NeverEmbeddedFonts = DebtPlus.Data.Fonts.GetExcludeList()
            MyBase.ExportToPdf(path)
        End Sub

        Public Sub CreatePdfDocument(stream As System.IO.Stream) Implements Interfaces.Reports.IReports.CreatePdfDocument
            Me.ExportOptions.Pdf.NeverEmbeddedFonts = DebtPlus.Data.Fonts.GetExcludeList()
            MyBase.ExportToPdf(stream)
        End Sub

        Public Sub CreateRtfDocument(path As String) Implements Interfaces.Reports.IReports.CreateRtfDocument
            MyBase.ExportToRtf(path)
        End Sub

        Public Sub CreateRtfDocument(stream As System.IO.Stream) Implements Interfaces.Reports.IReports.CreateRtfDocument
            MyBase.ExportToRtf(stream)
        End Sub

        Public Function DisplayPageSetupDialog() As System.Windows.Forms.DialogResult Implements Interfaces.Reports.IReports.DisplayPageSetupDialog
            Return MyBase.ShowPageSetupDialog()
        End Function

        Public Sub DisplayPreviewDialog() Implements Interfaces.Reports.IReports.DisplayPreviewDialog
            MyBase.ShowPreviewDialog()
        End Sub

        Public Function ParametersDictionary() As System.Collections.Generic.Dictionary(Of String, String) Implements Interfaces.Reports.IReports.ParametersDictionary
            Return Parameters.AsQueryable().OfType(Of DevExpress.XtraReports.Parameters.Parameter)().ToDictionary(Function(p) p.Name, Function(p) p.Value.ToString())
        End Function

        Public Sub PrintDocument() Implements Interfaces.Reports.IReports.PrintDocument
            MyBase.Print()
        End Sub

        Public Sub PrintDocument(printerName As String) Implements Interfaces.Reports.IReports.PrintDocument
            MyBase.Print(printerName)
        End Sub

        Public ReadOnly Property ReportSubTitle As String Implements Interfaces.Reports.IReports.ReportSubTitle
            Get
                Return String.Empty
            End Get
        End Property

        Public ReadOnly Property ReportTitle As String Implements Interfaces.Reports.IReports.ReportTitle
            Get
                Return String.Empty
            End Get
        End Property

        Public Sub RunReport() Implements Interfaces.Reports.IReports.RunReport
            If RequestReportParameters() = DialogResult.OK Then
                DisplayPreviewDialog()
            End If
        End Sub

        Public Sub RunReportInSeparateThread() Implements Interfaces.Reports.IReports.RunReportInSeparateThread
            Dim thrd = New System.Threading.Thread(AddressOf RunReport)
            thrd.SetApartmentState(System.Threading.ApartmentState.STA)
            thrd.IsBackground = True
            thrd.Name = Guid.NewGuid().ToString()
            thrd.Start()
        End Sub

        Public Sub SetReportParameter(parameterName As String, value As Object) Implements Interfaces.Reports.IReports.SetReportParameter
        End Sub
    End Class
End Namespace

