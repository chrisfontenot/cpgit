﻿Namespace Client.Statement.DMP
    Public Class BaseClientStatementReport
        Inherits DebtPlus.Reports.Template.BaseXtraReportClass

        Public Sub New()
            MyBase.New()
            InitializeComponent()
            AddHandler XrSubreport_ClientStatementClientsSubReport.BeforePrint, AddressOf XrSubreport_ClientStatementClientsSubReport_BeforePrint
        End Sub

        ''' <summary>
        ''' Select statement to access the statement batch table
        ''' </summary>
        Public Shared ReadOnly Property Select_client_statement_batches As String
            Get
                Return "SELECT b.[client_statement_batch],b.[note],b.[statement_date],b.[period_start],b.[period_end] FROM [client_statement_batches] b"
            End Get
        End Property

        ''' <summary>
        ''' Dataset for the client statements
        ''' </summary>
        Protected ds As New System.Data.DataSet("ds")
        Protected filterCriteria As SelectionParameters = Nothing

        ''' <summary>
        ''' Load the sub-report information to retrieve just the selected clients from the report data
        ''' </summary>
        ''' <param name="FilterCritera">Criteria for the selection list of items.</param>
        ''' <remarks></remarks>
        Public Sub DefineDataSource(FilterCritera As SelectionParameters)
            Me.filterCriteria = FilterCritera

            ' Ensure that the dataset is empty
            ds.Clear()

            Dim connectionString As String = DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString
            Using cn As New System.Data.SqlClient.SqlConnection(connectionString)
                cn.Open()

                Dim sb As New System.Text.StringBuilder()
                If FilterCritera.BatchID.HasValue Then
                    sb.AppendFormat(" AND (b.[client_statement_batch]={0:f0})", FilterCritera.BatchID.Value)
                Else
                    If FilterCritera.BatchStartingDate.HasValue AndAlso FilterCritera.BatchEndingDate.HasValue Then
                        sb.AppendFormat(" AND ((b.[period_end] < '{0:MM/dd/yyyy}')", FilterCritera.BatchEndingDate.Value.Date.AddDays(1))
                        sb.AppendFormat(" AND (b.[period_start] >= '{0:MM/dd/yyyy}'))", FilterCritera.BatchStartingDate.Value.Date)
                    End If
                End If

                ' Correct the selection clause
                If sb.Length > 0 Then
                    sb.Remove(0, 5)
                    sb.Insert(0, " WHERE ")
                End If

                Using cmd As New System.Data.SqlClient.SqlCommand()
                    cmd.Connection = cn
                    cmd.CommandType = CommandType.Text
                    cmd.CommandText = String.Format("{0}{1}", Select_client_statement_batches, sb.ToString())

                    Using da As New System.Data.SqlClient.SqlDataAdapter(cmd)
                        da.Fill(ds, "client_statement_batches")
                        Dim tbl As System.Data.DataTable = ds.Tables("client_statement_batches")
                        DataSource = tbl.DefaultView
                    End Using
                End Using
            End Using
        End Sub

        Protected Sub XrSubreport_ClientStatementClientsSubReport_BeforePrint(sender As Object, e As System.Drawing.Printing.PrintEventArgs)
            filterCriteria.client_statement_batch = DirectCast(GetCurrentRow(), System.Data.DataRowView)
            ClientStatementClientSubReport1.DefineDataSource(filterCriteria)
        End Sub
    End Class
End Namespace
