﻿Namespace Client.Statement.DMP
    Public Class ClientTransactions

        Public Sub New()
            MyBase.New()
            InitializeComponent()

            AddHandler BeforePrint, AddressOf ClientTransactions_BeforePrint
            AddHandler XrTableCell_detail_type.BeforePrint, AddressOf XrTableCell_detail_type_BeforePrint
            AddHandler XrTableCell_footer_type.BeforePrint, AddressOf XrTableCell_footer_type_BeforePrint
        End Sub

        ' Map transaction types to a description string
        Private colTranTypes As System.Collections.Generic.Dictionary(Of String, String)

        ''' <summary>
        ''' Create the transaction type table when the report starts
        ''' </summary>
        Private Sub ClientTransactions_BeforePrint(sender As Object, e As System.Drawing.Printing.PrintEventArgs)
            CreateTranTypesTable()
        End Sub

        ''' <summary>
        ''' Translate the transaction type to a string
        ''' </summary>
        Private Sub XrTableCell_detail_type_BeforePrint(sender As Object, e As System.Drawing.Printing.PrintEventArgs)

            XrTableCell_detail_type.Text = String.Empty

            ' From the label, find the report
            Dim rpt As DevExpress.XtraReports.UI.XtraReport = TryCast(XrTableCell_detail_type.Report, DevExpress.XtraReports.UI.XtraReport)
            If rpt Is Nothing Then
                Return
            End If

            ' From the report, get the current row
            Dim row As System.Data.DataRowView = TryCast(rpt.GetCurrentRow(), System.Data.DataRowView)
            If row Is Nothing Then
                Return
            End If

            ' From the current row, get the field
            Dim field As String = DebtPlus.Utils.Nulls.v_String(row("tran_type"))
            If String.IsNullOrEmpty(field) Then
                Return
            End If

            XrTableCell_detail_type.Text = TranslateType(field.ToUpper().Trim())
        End Sub

        ''' <summary>
        ''' Translate the footer type to a string
        ''' </summary>
        Private Sub XrTableCell_footer_type_BeforePrint(sender As Object, e As System.Drawing.Printing.PrintEventArgs)
            XrTableCell_footer_type.Text = TranslateType("EB")
        End Sub

        ''' <summary>
        ''' Translate a type value to a string
        ''' </summary>
        Private Function TranslateType(ByVal field As String) As String

            If colTranTypes.ContainsKey(field) Then
                Return colTranTypes(field)
            End If

            Return field.ToUpper().Trim()
        End Function

        ''' <summary>
        ''' Build the type translation table
        ''' </summary>
        ''' <remarks></remarks>
        Private Sub CreateTranTypesTable()
            colTranTypes = New System.Collections.Generic.Dictionary(Of String, String)

            colTranTypes.Add("AD", "Payments Disbursed")
            colTranTypes.Add("MD", "Payments Disbursed")
            colTranTypes.Add("CM", "Payments Disbursed")
            colTranTypes.Add("BB", "Beginning Balance")
            colTranTypes.Add("EB", "Ending Balance")
            colTranTypes.Add("CD", "Corrected Deposit")
            colTranTypes.Add("CW", "Corrected Deposit")
            colTranTypes.Add("CR", "Client Refund")
            colTranTypes.Add("DM", "Deposit MISC")
            colTranTypes.Add("MF", "Deposit Reversal")
            colTranTypes.Add("DP", "Deposit")
            colTranTypes.Add("RF", "Creditor Return")
            colTranTypes.Add("RR", "Creditor Return")
            colTranTypes.Add("VD", "Payment Void")
            colTranTypes.Add("VR", "Payment Void")
        End Sub
    End Class
End Namespace