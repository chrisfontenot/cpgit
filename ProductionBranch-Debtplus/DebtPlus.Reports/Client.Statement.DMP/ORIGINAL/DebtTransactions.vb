﻿Namespace Client.Statement.DMP
    Public Class DebtTransactions
        Public Sub New()
            MyBase.New()
            InitializeComponent()
            AddHandler XrLabel_detail_marker.BeforePrint, AddressOf XrLabel_detail_marker_BeforePrint
        End Sub

        ''' <summary>
        ''' Format the marker character in the detail section.
        ''' </summary>
        Private Sub XrLabel_detail_marker_BeforePrint(sender As Object, e As System.Drawing.Printing.PrintEventArgs)

            ' From the sender, get the label
            Dim lbl As DevExpress.XtraReports.UI.XRLabel = TryCast(sender, DevExpress.XtraReports.UI.XRLabel)
            If lbl Is Nothing Then
                Return
            End If

            ' Find the current row for the item
            Dim row As System.Data.DataRowView = TryCast(lbl.Report.GetCurrentRow(), System.Data.DataRowView)
            If row Is Nothing Then
                Return
            End If

            ' Obtain the interest rate. If there is a rate then include the marker. Otherwise, make it blank.
            Dim interestRate As System.Nullable(Of Double) = DebtPlus.Utils.Nulls.v_Double(row("interest_rate"))
            lbl.Text = If(interestRate.GetValueOrDefault(0.0#) > 0.0#, "*", " ")
        End Sub
    End Class
End Namespace
