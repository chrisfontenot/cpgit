﻿Namespace Client.Statement.DMP
    <Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
    Partial Public Class ClientStatementDetailsSubReport
        Inherits DevExpress.XtraReports.UI.XtraReport

        'XtraReport overrides dispose to clean up the component list.
        <System.Diagnostics.DebuggerNonUserCode()> _
        Protected Overrides Sub Dispose(ByVal disposing As Boolean)
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
            MyBase.Dispose(disposing)
        End Sub

        'Required by the Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Designer
        'It can be modified using the Designer.  
        'Do not modify it using the code editor.
        <System.Diagnostics.DebuggerStepThrough()> _
        Private Sub InitializeComponent()
            Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(ClientStatementDetailsSubReport))
            Me.Detail = New DevExpress.XtraReports.UI.DetailBand()
            Me.XrSubreport_detail_transactions = New DevExpress.XtraReports.UI.XRSubreport()
            Me.DebtTransactions1 = New DebtPlus.Reports.Client.Statement.DMP.DebtTransactions()
            Me.XrSubreport_client_transactions = New DevExpress.XtraReports.UI.XRSubreport()
            Me.ClientTransactions1 = New DebtPlus.Reports.Client.Statement.DMP.ClientTransactions()
            Me.TopMargin = New DevExpress.XtraReports.UI.TopMarginBand()
            Me.BottomMargin = New DevExpress.XtraReports.UI.BottomMarginBand()
            Me.ReportHeader = New DevExpress.XtraReports.UI.ReportHeaderBand()
            Me.XrLabel_FinalStatement = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel6 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel5 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLine1 = New DevExpress.XtraReports.UI.XRLine()
            Me.XrLabel4 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrTable2 = New DevExpress.XtraReports.UI.XRTable()
            Me.XrTableRow4 = New DevExpress.XtraReports.UI.XRTableRow()
            Me.XrTableCell7 = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableCell_due_date = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableRow5 = New DevExpress.XtraReports.UI.XRTableRow()
            Me.XrTableCell9 = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableCell_due_amount = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTable1 = New DevExpress.XtraReports.UI.XRTable()
            Me.XrTableRow1 = New DevExpress.XtraReports.UI.XRTableRow()
            Me.XrTableCell1 = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableCell_statement_date_1 = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableRow2 = New DevExpress.XtraReports.UI.XRTableRow()
            Me.XrTableCell3 = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableCell_client_1 = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableRow3 = New DevExpress.XtraReports.UI.XRTableRow()
            Me.XrTableCell5 = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableCell_counselor_1 = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrLabel_message = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel2 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_client_address = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrPictureBox1 = New DevExpress.XtraReports.UI.XRPictureBox()
            Me.PageHeader = New DevExpress.XtraReports.UI.PageHeaderBand()
            Me.XrLabel1 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel10 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrRichText1 = New DevExpress.XtraReports.UI.XRRichText()
            Me.XrLabel_statement_date_2 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_counselor_2 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_client_2 = New DevExpress.XtraReports.UI.XRLabel()
            Me.GroupHeader_OnEveryPage = New DevExpress.XtraReports.UI.GroupHeaderBand()
            Me.ReportFooter = New DevExpress.XtraReports.UI.ReportFooterBand()
            Me.XrLabel_Blank = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrPageBreak1 = New DevExpress.XtraReports.UI.XRPageBreak()
            Me.XrRichText2 = New DevExpress.XtraReports.UI.XRRichText()
            CType(Me.DebtTransactions1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.ClientTransactions1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.XrTable2, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.XrTable1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.XrRichText1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.XrRichText2, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
            '
            'Detail
            '
            Me.Detail.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrSubreport_detail_transactions, Me.XrSubreport_client_transactions})
            Me.Detail.HeightF = 66.0!
            Me.Detail.Name = "Detail"
            Me.Detail.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
            Me.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
            '
            'XrSubreport_detail_transactions
            '
            Me.XrSubreport_detail_transactions.LocationFloat = New DevExpress.Utils.PointFloat(0.0001430511!, 43.0!)
            Me.XrSubreport_detail_transactions.Name = "XrSubreport_detail_transactions"
            Me.XrSubreport_detail_transactions.ReportSource = Me.DebtTransactions1
            Me.XrSubreport_detail_transactions.SizeF = New System.Drawing.SizeF(799.9999!, 23.0!)
            '
            'XrSubreport_client_transactions
            '
            Me.XrSubreport_client_transactions.LocationFloat = New DevExpress.Utils.PointFloat(0.00007947286!, 0.0!)
            Me.XrSubreport_client_transactions.Name = "XrSubreport_client_transactions"
            Me.XrSubreport_client_transactions.ReportSource = Me.ClientTransactions1
            Me.XrSubreport_client_transactions.SizeF = New System.Drawing.SizeF(799.9999!, 23.0!)
            '
            'TopMargin
            '
            Me.TopMargin.HeightF = 25.0!
            Me.TopMargin.Name = "TopMargin"
            Me.TopMargin.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
            Me.TopMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
            '
            'BottomMargin
            '
            Me.BottomMargin.HeightF = 25.0!
            Me.BottomMargin.Name = "BottomMargin"
            Me.BottomMargin.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
            Me.BottomMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
            '
            'ReportHeader
            '
            Me.ReportHeader.BorderWidth = 1
            Me.ReportHeader.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel_FinalStatement, Me.XrLabel6, Me.XrLabel5, Me.XrLine1, Me.XrLabel4, Me.XrTable2, Me.XrTable1, Me.XrLabel_message, Me.XrLabel2, Me.XrLabel_client_address, Me.XrPictureBox1})
            Me.ReportHeader.HeightF = 346.0!
            Me.ReportHeader.Name = "ReportHeader"
            Me.ReportHeader.StylePriority.UseBorderWidth = False
            '
            'XrLabel_FinalStatement
            '
            Me.XrLabel_FinalStatement.BorderColor = System.Drawing.Color.Teal
            Me.XrLabel_FinalStatement.Borders = CType((((DevExpress.XtraPrinting.BorderSide.Left Or DevExpress.XtraPrinting.BorderSide.Top) _
                        Or DevExpress.XtraPrinting.BorderSide.Right) _
                        Or DevExpress.XtraPrinting.BorderSide.Bottom), DevExpress.XtraPrinting.BorderSide)
            Me.XrLabel_FinalStatement.BorderWidth = 2
            Me.XrLabel_FinalStatement.Font = New System.Drawing.Font("Calibri", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.XrLabel_FinalStatement.ForeColor = System.Drawing.Color.Teal
            Me.XrLabel_FinalStatement.LocationFloat = New DevExpress.Utils.PointFloat(466.0!, 257.0!)
            Me.XrLabel_FinalStatement.Name = "XrLabel_FinalStatement"
            Me.XrLabel_FinalStatement.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 2, 2, 100.0!)
            Me.XrLabel_FinalStatement.SizeF = New System.Drawing.SizeF(323.9998!, 35.70831!)
            Me.XrLabel_FinalStatement.StylePriority.UseBorderColor = False
            Me.XrLabel_FinalStatement.StylePriority.UseBorders = False
            Me.XrLabel_FinalStatement.StylePriority.UseBorderWidth = False
            Me.XrLabel_FinalStatement.StylePriority.UseFont = False
            Me.XrLabel_FinalStatement.StylePriority.UseForeColor = False
            Me.XrLabel_FinalStatement.StylePriority.UsePadding = False
            Me.XrLabel_FinalStatement.StylePriority.UseTextAlignment = False
            Me.XrLabel_FinalStatement.Text = "THIS IS YOUR FINAL STATEMENT"
            Me.XrLabel_FinalStatement.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
            '
            'XrLabel6
            '
            Me.XrLabel6.BorderDashStyle = DevExpress.XtraPrinting.BorderDashStyle.DashDot
            Me.XrLabel6.Borders = DevExpress.XtraPrinting.BorderSide.Bottom
            Me.XrLabel6.Font = New System.Drawing.Font("Times New Roman", 9.0!, System.Drawing.FontStyle.Italic)
            Me.XrLabel6.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 292.7083!)
            Me.XrLabel6.Multiline = True
            Me.XrLabel6.Name = "XrLabel6"
            Me.XrLabel6.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel6.SizeF = New System.Drawing.SizeF(799.9999!, 31.33334!)
            Me.XrLabel6.StylePriority.UseBorderDashStyle = False
            Me.XrLabel6.StylePriority.UseBorders = False
            Me.XrLabel6.StylePriority.UseFont = False
            Me.XrLabel6.StylePriority.UseTextAlignment = False
            Me.XrLabel6.Text = "IF YOU DO NOT MAKE YOUR PAYMENT THROUGH AUTOMATIC DRAFT, PLEASE RETURN THIS TOP P" & _
                "ORTION WITH YOUR DEPOSIT"
            Me.XrLabel6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
            '
            'XrLabel5
            '
            Me.XrLabel5.Font = New System.Drawing.Font("Times New Roman", 6.0!)
            Me.XrLabel5.LocationFloat = New DevExpress.Utils.PointFloat(466.0!, 226.5!)
            Me.XrLabel5.Multiline = True
            Me.XrLabel5.Name = "XrLabel5"
            Me.XrLabel5.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel5.SizeF = New System.Drawing.SizeF(323.9998!, 30.5!)
            Me.XrLabel5.StylePriority.UseFont = False
            Me.XrLabel5.StylePriority.UseTextAlignment = False
            Me.XrLabel5.Text = "PLEASE MAKE CASHIER’S CHECK OR MONEY ORDER ONLY PAYABLE TO" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "MONEY MANAGEMENT"
            Me.XrLabel5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
            '
            'XrLine1
            '
            Me.XrLine1.LocationFloat = New DevExpress.Utils.PointFloat(623.5!, 206.625!)
            Me.XrLine1.Name = "XrLine1"
            Me.XrLine1.SizeF = New System.Drawing.SizeF(166.5!, 7.374969!)
            '
            'XrLabel4
            '
            Me.XrLabel4.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel4.LocationFloat = New DevExpress.Utils.PointFloat(466.0!, 191.0!)
            Me.XrLabel4.Name = "XrLabel4"
            Me.XrLabel4.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel4.SizeF = New System.Drawing.SizeF(157.5!, 23.0!)
            Me.XrLabel4.StylePriority.UseFont = False
            Me.XrLabel4.StylePriority.UseTextAlignment = False
            Me.XrLabel4.Text = "AMOUNT ENCLOSED $"
            Me.XrLabel4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight
            '
            'XrTable2
            '
            Me.XrTable2.Borders = CType((((DevExpress.XtraPrinting.BorderSide.Left Or DevExpress.XtraPrinting.BorderSide.Top) _
                        Or DevExpress.XtraPrinting.BorderSide.Right) _
                        Or DevExpress.XtraPrinting.BorderSide.Bottom), DevExpress.XtraPrinting.BorderSide)
            Me.XrTable2.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrTable2.LocationFloat = New DevExpress.Utils.PointFloat(466.0!, 133.0!)
            Me.XrTable2.Name = "XrTable2"
            Me.XrTable2.Padding = New DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 100.0!)
            Me.XrTable2.Rows.AddRange(New DevExpress.XtraReports.UI.XRTableRow() {Me.XrTableRow4, Me.XrTableRow5})
            Me.XrTable2.SizeF = New System.Drawing.SizeF(324.0!, 50.0!)
            Me.XrTable2.StylePriority.UseBorders = False
            Me.XrTable2.StylePriority.UseFont = False
            Me.XrTable2.StylePriority.UsePadding = False
            Me.XrTable2.StylePriority.UseTextAlignment = False
            Me.XrTable2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrTableRow4
            '
            Me.XrTableRow4.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.XrTableCell7, Me.XrTableCell_due_date})
            Me.XrTableRow4.Name = "XrTableRow4"
            Me.XrTableRow4.Weight = 1.0R
            '
            'XrTableCell7
            '
            Me.XrTableCell7.Name = "XrTableCell7"
            Me.XrTableCell7.Text = "DUE DATE:"
            Me.XrTableCell7.Weight = 1.4583331298828126R
            '
            'XrTableCell_due_date
            '
            Me.XrTableCell_due_date.Name = "XrTableCell_due_date"
            Me.XrTableCell_due_date.Weight = 1.5416668701171874R
            '
            'XrTableRow5
            '
            Me.XrTableRow5.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.XrTableCell9, Me.XrTableCell_due_amount})
            Me.XrTableRow5.Name = "XrTableRow5"
            Me.XrTableRow5.Weight = 1.0R
            '
            'XrTableCell9
            '
            Me.XrTableCell9.Name = "XrTableCell9"
            Me.XrTableCell9.Text = "PAYMENT AMOUNT:"
            Me.XrTableCell9.Weight = 1.4583331298828126R
            '
            'XrTableCell_due_amount
            '
            Me.XrTableCell_due_amount.Name = "XrTableCell_due_amount"
            Me.XrTableCell_due_amount.Weight = 1.5416668701171874R
            '
            'XrTable1
            '
            Me.XrTable1.LocationFloat = New DevExpress.Utils.PointFloat(466.0!, 0.0!)
            Me.XrTable1.Name = "XrTable1"
            Me.XrTable1.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrTable1.Rows.AddRange(New DevExpress.XtraReports.UI.XRTableRow() {Me.XrTableRow1, Me.XrTableRow2, Me.XrTableRow3})
            Me.XrTable1.SizeF = New System.Drawing.SizeF(325.0!, 45.0!)
            Me.XrTable1.StylePriority.UsePadding = False
            '
            'XrTableRow1
            '
            Me.XrTableRow1.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.XrTableCell1, Me.XrTableCell_statement_date_1})
            Me.XrTableRow1.Name = "XrTableRow1"
            Me.XrTableRow1.Weight = 0.6R
            '
            'XrTableCell1
            '
            Me.XrTableCell1.Name = "XrTableCell1"
            Me.XrTableCell1.Text = "STATEMENT DATE"
            Me.XrTableCell1.Weight = 1.2275649320162259R
            '
            'XrTableCell_statement_date_1
            '
            Me.XrTableCell_statement_date_1.Name = "XrTableCell_statement_date_1"
            Me.XrTableCell_statement_date_1.Padding = New DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 100.0!)
            Me.XrTableCell_statement_date_1.StylePriority.UsePadding = False
            Me.XrTableCell_statement_date_1.Text = "MM/DD/YYYY"
            Me.XrTableCell_statement_date_1.Weight = 1.7724350679837741R
            '
            'XrTableRow2
            '
            Me.XrTableRow2.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.XrTableCell3, Me.XrTableCell_client_1})
            Me.XrTableRow2.Name = "XrTableRow2"
            Me.XrTableRow2.Weight = 0.59999999999999987R
            '
            'XrTableCell3
            '
            Me.XrTableCell3.Name = "XrTableCell3"
            Me.XrTableCell3.Text = "CLIENT ID"
            Me.XrTableCell3.Weight = 1.2275643686147837R
            '
            'XrTableCell_client_1
            '
            Me.XrTableCell_client_1.Name = "XrTableCell_client_1"
            Me.XrTableCell_client_1.Padding = New DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 100.0!)
            Me.XrTableCell_client_1.StylePriority.UsePadding = False
            Me.XrTableCell_client_1.Weight = 1.7724356313852163R
            '
            'XrTableRow3
            '
            Me.XrTableRow3.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.XrTableCell5, Me.XrTableCell_counselor_1})
            Me.XrTableRow3.Name = "XrTableRow3"
            Me.XrTableRow3.Weight = 0.60000000000000009R
            '
            'XrTableCell5
            '
            Me.XrTableCell5.Name = "XrTableCell5"
            Me.XrTableCell5.Text = "COUNSELOR"
            Me.XrTableCell5.Weight = 1.2275644625150242R
            '
            'XrTableCell_counselor_1
            '
            Me.XrTableCell_counselor_1.Name = "XrTableCell_counselor_1"
            Me.XrTableCell_counselor_1.Padding = New DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 100.0!)
            Me.XrTableCell_counselor_1.StylePriority.UsePadding = False
            Me.XrTableCell_counselor_1.Weight = 1.7724355374849758R
            '
            'XrLabel_message
            '
            Me.XrLabel_message.LocationFloat = New DevExpress.Utils.PointFloat(466.0!, 58.0!)
            Me.XrLabel_message.Multiline = True
            Me.XrLabel_message.Name = "XrLabel_message"
            Me.XrLabel_message.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_message.SizeF = New System.Drawing.SizeF(325.0!, 58.0!)
            '
            'XrLabel2
            '
            Me.XrLabel2.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 86.12499!)
            Me.XrLabel2.Multiline = True
            Me.XrLabel2.Name = "XrLabel2"
            Me.XrLabel2.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel2.SizeF = New System.Drawing.SizeF(329.0!, 51.41666!)
            Me.XrLabel2.Text = "Money Management International" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "PO Box 2075, Sugar Land, TX 77478-2075" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "888-845-5" & _
                "669 Opt 2, Opt 0"
            '
            'XrLabel_client_address
            '
            Me.XrLabel_client_address.LocationFloat = New DevExpress.Utils.PointFloat(58.0!, 191.0!)
            Me.XrLabel_client_address.Multiline = True
            Me.XrLabel_client_address.Name = "XrLabel_client_address"
            Me.XrLabel_client_address.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_client_address.SizeF = New System.Drawing.SizeF(370.0!, 66.0!)
            '
            'XrPictureBox1
            '
            Me.XrPictureBox1.Image = CType(resources.GetObject("XrPictureBox1.Image"), System.Drawing.Image)
            Me.XrPictureBox1.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
            Me.XrPictureBox1.Name = "XrPictureBox1"
            Me.XrPictureBox1.SizeF = New System.Drawing.SizeF(389.0!, 75.0!)
            '
            'PageHeader
            '
            Me.PageHeader.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel1, Me.XrLabel10})
            Me.PageHeader.HeightF = 346.0!
            Me.PageHeader.Name = "PageHeader"
            Me.PageHeader.PrintOn = CType((DevExpress.XtraReports.UI.PrintOnPages.NotWithReportHeader Or DevExpress.XtraReports.UI.PrintOnPages.NotWithReportFooter), DevExpress.XtraReports.UI.PrintOnPages)
            '
            'XrLabel1
            '
            Me.XrLabel1.BorderDashStyle = DevExpress.XtraPrinting.BorderDashStyle.DashDot
            Me.XrLabel1.Borders = DevExpress.XtraPrinting.BorderSide.Bottom
            Me.XrLabel1.Font = New System.Drawing.Font("Times New Roman", 9.0!, System.Drawing.FontStyle.Italic)
            Me.XrLabel1.LocationFloat = New DevExpress.Utils.PointFloat(0.0001430511!, 292.7083!)
            Me.XrLabel1.Multiline = True
            Me.XrLabel1.Name = "XrLabel1"
            Me.XrLabel1.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel1.SizeF = New System.Drawing.SizeF(799.9999!, 31.33334!)
            Me.XrLabel1.StylePriority.UseBorderDashStyle = False
            Me.XrLabel1.StylePriority.UseBorders = False
            Me.XrLabel1.StylePriority.UseFont = False
            Me.XrLabel1.StylePriority.UseTextAlignment = False
            Me.XrLabel1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
            '
            'XrLabel10
            '
            Me.XrLabel10.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel10.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 162.5!)
            Me.XrLabel10.Multiline = True
            Me.XrLabel10.Name = "XrLabel10"
            Me.XrLabel10.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel10.SizeF = New System.Drawing.SizeF(799.9999!, 23.0!)
            Me.XrLabel10.StylePriority.UseFont = False
            Me.XrLabel10.StylePriority.UseTextAlignment = False
            Me.XrLabel10.Text = "SEE REVERSE SIDE FOR IMPORTANT ACCOUNT INFORMATION"
            Me.XrLabel10.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
            '
            'XrRichText1
            '
            Me.XrRichText1.Font = New System.Drawing.Font("Times New Roman", 7.0!, System.Drawing.FontStyle.Bold)
            Me.XrRichText1.LocationFloat = New DevExpress.Utils.PointFloat(0.00007947286!, 23.00002!)
            Me.XrRichText1.Name = "XrRichText1"
            Me.XrRichText1.SerializableRtfString = resources.GetString("XrRichText1.SerializableRtfString")
            Me.XrRichText1.SizeF = New System.Drawing.SizeF(794.9999!, 27.8333!)
            Me.XrRichText1.StylePriority.UseFont = False
            '
            'XrLabel_statement_date_2
            '
            Me.XrLabel_statement_date_2.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
            Me.XrLabel_statement_date_2.Name = "XrLabel_statement_date_2"
            Me.XrLabel_statement_date_2.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_statement_date_2.SizeF = New System.Drawing.SizeF(218.75!, 23.0!)
            Me.XrLabel_statement_date_2.Text = "STATEMENT DATE: MM/DD/YYYY"
            '
            'XrLabel_counselor_2
            '
            Me.XrLabel_counselor_2.LocationFloat = New DevExpress.Utils.PointFloat(218.75!, 0.0!)
            Me.XrLabel_counselor_2.Name = "XrLabel_counselor_2"
            Me.XrLabel_counselor_2.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_counselor_2.SizeF = New System.Drawing.SizeF(425.0415!, 23.0!)
            '
            'XrLabel_client_2
            '
            Me.XrLabel_client_2.LocationFloat = New DevExpress.Utils.PointFloat(643.7915!, 0.0!)
            Me.XrLabel_client_2.Name = "XrLabel_client_2"
            Me.XrLabel_client_2.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_client_2.SizeF = New System.Drawing.SizeF(151.2084!, 23.0!)
            Me.XrLabel_client_2.StylePriority.UseTextAlignment = False
            Me.XrLabel_client_2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
            '
            'GroupHeader_OnEveryPage
            '
            Me.GroupHeader_OnEveryPage.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrRichText1, Me.XrLabel_statement_date_2, Me.XrLabel_counselor_2, Me.XrLabel_client_2})
            Me.GroupHeader_OnEveryPage.HeightF = 50.83332!
            Me.GroupHeader_OnEveryPage.Name = "GroupHeader_OnEveryPage"
            Me.GroupHeader_OnEveryPage.RepeatEveryPage = True
            '
            'ReportFooter
            '
            Me.ReportFooter.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel_Blank, Me.XrPageBreak1, Me.XrRichText2})
            Me.ReportFooter.HeightF = 48.0!
            Me.ReportFooter.Name = "ReportFooter"
            Me.ReportFooter.PageBreak = DevExpress.XtraReports.UI.PageBreak.BeforeBand
            '
            'XrLabel_Blank
            '
            Me.XrLabel_Blank.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 24.99999!)
            Me.XrLabel_Blank.Name = "XrLabel_Blank"
            Me.XrLabel_Blank.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_Blank.SizeF = New System.Drawing.SizeF(799.9999!, 23.0!)
            Me.XrLabel_Blank.StylePriority.UseTextAlignment = False
            Me.XrLabel_Blank.Text = "THIS PAGE IS INTENTIALLY LEFT BLANK"
            Me.XrLabel_Blank.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
            '
            'XrPageBreak1
            '
            Me.XrPageBreak1.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 22.99999!)
            Me.XrPageBreak1.Name = "XrPageBreak1"
            '
            'XrRichText2
            '
            Me.XrRichText2.LocationFloat = New DevExpress.Utils.PointFloat(0.0001430511!, 0.0!)
            Me.XrRichText2.Name = "XrRichText2"
            Me.XrRichText2.SerializableRtfString = resources.GetString("XrRichText2.SerializableRtfString")
            Me.XrRichText2.SizeF = New System.Drawing.SizeF(799.9997!, 23.0!)
            '
            'ClientStatementDetailsSubReport
            '
            Me.Bands.AddRange(New DevExpress.XtraReports.UI.Band() {Me.Detail, Me.TopMargin, Me.BottomMargin, Me.ReportHeader, Me.PageHeader, Me.GroupHeader_OnEveryPage, Me.ReportFooter})
            Me.Margins = New System.Drawing.Printing.Margins(25, 25, 25, 25)
            Me.ScriptLanguage = DevExpress.XtraReports.ScriptLanguage.VisualBasic
            Me.Version = "11.2"
            Me.Watermark.Font = New System.Drawing.Font("Calibri", 48.0!, System.Drawing.FontStyle.Bold)
            Me.Watermark.ForeColor = System.Drawing.Color.SteelBlue
            Me.Watermark.Text = "FINAL STATEMENT"
            Me.Watermark.TextDirection = DevExpress.XtraPrinting.Drawing.DirectionMode.BackwardDiagonal
            Me.Watermark.TextTransparency = 64
            CType(Me.DebtTransactions1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.ClientTransactions1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.XrTable2, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.XrTable1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.XrRichText1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.XrRichText2, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me, System.ComponentModel.ISupportInitialize).EndInit()

        End Sub

        Friend WithEvents Detail As DevExpress.XtraReports.UI.DetailBand
        Friend WithEvents TopMargin As DevExpress.XtraReports.UI.TopMarginBand
        Friend WithEvents BottomMargin As DevExpress.XtraReports.UI.BottomMarginBand
        Friend WithEvents ReportHeader As DevExpress.XtraReports.UI.ReportHeaderBand
        Friend WithEvents XrLabel6 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel5 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLine1 As DevExpress.XtraReports.UI.XRLine
        Friend WithEvents XrLabel4 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrTable2 As DevExpress.XtraReports.UI.XRTable
        Friend WithEvents XrTableRow4 As DevExpress.XtraReports.UI.XRTableRow
        Friend WithEvents XrTableCell7 As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents XrTableCell_due_date As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents XrTableRow5 As DevExpress.XtraReports.UI.XRTableRow
        Friend WithEvents XrTableCell9 As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents XrTableCell_due_amount As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents XrTable1 As DevExpress.XtraReports.UI.XRTable
        Friend WithEvents XrTableRow1 As DevExpress.XtraReports.UI.XRTableRow
        Friend WithEvents XrTableCell1 As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents XrTableCell_statement_date_1 As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents XrTableRow2 As DevExpress.XtraReports.UI.XRTableRow
        Friend WithEvents XrTableCell3 As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents XrTableCell_client_1 As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents XrTableRow3 As DevExpress.XtraReports.UI.XRTableRow
        Friend WithEvents XrTableCell5 As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents XrTableCell_counselor_1 As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents XrLabel_message As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel2 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_client_address As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrPictureBox1 As DevExpress.XtraReports.UI.XRPictureBox
        Friend WithEvents PageHeader As DevExpress.XtraReports.UI.PageHeaderBand
        Friend WithEvents XrRichText1 As DevExpress.XtraReports.UI.XRRichText
        Friend WithEvents XrLabel_statement_date_2 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_counselor_2 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_client_2 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrSubreport_client_transactions As DevExpress.XtraReports.UI.XRSubreport
        Friend WithEvents XrLabel10 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents GroupHeader_OnEveryPage As DevExpress.XtraReports.UI.GroupHeaderBand
        Private WithEvents ClientTransactions1 As DebtPlus.Reports.Client.Statement.DMP.ClientTransactions
        Friend WithEvents XrSubreport_detail_transactions As DevExpress.XtraReports.UI.XRSubreport
        Private WithEvents DebtTransactions1 As DebtPlus.Reports.Client.Statement.DMP.DebtTransactions
        Friend WithEvents XrLabel1 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_FinalStatement As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents ReportFooter As DevExpress.XtraReports.UI.ReportFooterBand
        Friend WithEvents XrLabel_Blank As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrPageBreak1 As DevExpress.XtraReports.UI.XRPageBreak
        Friend WithEvents XrRichText2 As DevExpress.XtraReports.UI.XRRichText
    End Class
End Namespace
