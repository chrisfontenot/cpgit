﻿Namespace Client.Statement.DMP
    Public Class ClientStatementClientSubReport
        Public Sub New()
            MyBase.New()
            InitializeComponent()
            AddHandler XrSubreport_ClientStatementDetailsSubReport.BeforePrint, AddressOf XrSubreport_ClientStatementDetailsSubReport_BeforePrint
        End Sub

        ''' <summary>
        ''' Dataset for the client statements
        ''' </summary>
        Private ds As New System.Data.DataSet("ds")
        Private filterCriteria As SelectionParameters = Nothing

        ''' <summary>
        ''' Select statement to access the client information for the statements
        ''' </summary>
        Public Shared ReadOnly Property Select_client_statement_clients As String
            Get
                Return "SELECT cl.[client_statement],cl.[client_statement_batch],cl.[client],cl.[isMultiPage],cl.[isACH],cl.[isFinal],cl.[active_status],cl.[active_debts],cl.[last_deposit_date],cl.[last_deposit_amt],cl.[deposit_amt],cl.[refund_amt],cl.[disbursement_amt],cl.[held_in_trust],cl.[reserved_in_trust],cl.[counselor],cl.[office],cl.[expected_deposit_date],cl.[expected_deposit_amt],cl.[delivery_method],cl.[delivery_date],dbo.format_normal_name(default,coName.[first],default,coName.[last],default) AS [counselor_name] FROM [client_statement_clients] cl LEFT OUTER JOIN [counselors] co ON cl.[counselor]=co.[counselor] LEFT OUTER JOIN names coName ON co.[NameID] = coName.[Name]"
            End Get
        End Property

        ''' <summary>
        ''' Load the sub-report information to retrieve just the selected clients from the report data
        ''' </summary>
        ''' <param name="FilterCritera">Criteria for the selection list of items.</param>
        ''' <remarks></remarks>
        Public Sub DefineDataSource(FilterCritera As SelectionParameters)
            Me.filterCriteria = FilterCritera

            ' Ensure that the dataset is empty
            ds.Clear()

            Dim connectionString As String = DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString
            Using cn As New System.Data.SqlClient.SqlConnection(connectionString)
                cn.Open()

                Dim sb As New System.Text.StringBuilder()
                Dim selectionStatement As String = Select_client_statement_clients
                If FilterCritera.ClientID.HasValue Then
                    sb.AppendFormat(" AND (cl.[client]={0:f0})", FilterCritera.ClientID.Value)
                Else

                    ' Honor the electronic statement only criteria if desired
                    If FilterCritera.OnlyPaperStatements Then
                        selectionStatement += " INNER JOIN [clients] c ON cl.[client] = c.[client]"
                        sb.Append(" AND (c.[ElectronicStatements] <> 1)")
                    End If

                    If FilterCritera.AchItems = SelectionParameters.AchEnum.ACHOnly Then
                        sb.Append(" AND (cl.[isACH]<>0)")
                    ElseIf FilterCritera.AchItems = SelectionParameters.AchEnum.NonACHOnly Then
                        sb.Append(" AND (cl.[isACH]=0)")
                    End If

                    If FilterCritera.PageItems = SelectionParameters.PageItemEnum.MultiPageStatements Then
                        sb.Append(" AND (cl.[isMultiPage]<>0)")
                    ElseIf FilterCritera.PageItems = SelectionParameters.PageItemEnum.SinglePageStatements Then
                        sb.Append(" AND (cl.[IsMultiPage]=0)")
                    End If
                End If

                ' Complete the selection by including the appropriate statement batch criteria
                sb.AppendFormat(" AND (cl.[client_statement_batch]={0:f0})", FilterCritera.CurrentBatchID)
                sb.Remove(0, 5)
                sb.Insert(0, " WHERE ")

                Using cmd As New System.Data.SqlClient.SqlCommand()
                    cmd.Connection = cn
                    cmd.CommandType = CommandType.Text
                    cmd.CommandText = String.Format("{0}{1}", selectionStatement, sb.ToString())

                    Using da As New System.Data.SqlClient.SqlDataAdapter(cmd)
                        da.Fill(ds, "client_statement_clients")
                        Dim tbl As System.Data.DataTable = ds.Tables("client_statement_clients")
                        Me.DataSource = New System.Data.DataView(tbl, String.Empty, "[client]", DataViewRowState.CurrentRows)
                    End Using
                End Using
            End Using
        End Sub

        Private Sub XrSubreport_ClientStatementDetailsSubReport_BeforePrint(sender As Object, e As System.Drawing.Printing.PrintEventArgs)

            ' Define the current client statement row
            filterCriteria.client_statement = DirectCast(GetCurrentRow(), System.Data.DataRowView)

            ' And load the sub-report details for it.
            ClientStatementDetailsSubReport1.DefineDataSource(filterCriteria)
        End Sub
    End Class
End Namespace
