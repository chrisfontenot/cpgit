﻿Namespace Client.Statement.Monthly
    Public Class MonthlyClientStatementReport
        Inherits DebtPlus.Reports.Client.Statement.DMP.BaseClientStatementReport
        Implements DebtPlus.Interfaces.Client.IClient

        Public Property ClientId As Integer Implements DebtPlus.Interfaces.Client.IClient.ClientId
            Get
                Dim parm As DevExpress.XtraReports.Parameters.Parameter = FindParameter("ParameterClient")
                If parm Is Nothing Then Return -1
                Return Convert.ToInt32(parm.Value)
            End Get
            Set(value As Integer)
                SetParameter("ParameterClient", GetType(Int32), value, "Client ID", False)
            End Set
        End Property

        Public Sub New()
            MyBase.New()
            AddHandler BeforePrint, AddressOf MonthlyClientStatementReport_BeforePrint
        End Sub

        Public Property StatementBatchID As Integer
            Get
                Dim parm As DevExpress.XtraReports.Parameters.Parameter = FindParameter("ParameterBatchID")
                If parm Is Nothing Then Return -1
                Return Convert.ToInt32(parm.Value)
            End Get
            Set(value As Integer)
                SetParameter("ParameterBatchID", GetType(Int32), value, "Statement Batch ID", False)
            End Set
        End Property

        Public Overrides Sub SetReportParameter(ByVal Parameter As String, ByVal Value As Object)
            If Parameter = "Client" Then
                ClientId = Convert.ToInt32(Value)
            ElseIf Parameter = "Batch" Then
                StatementBatchID = Convert.ToInt32(Value)
            Else
                MyBase.SetReportParameter(Parameter, Value)
            End If
        End Sub

        Public Overrides Function NeedParameters() As Boolean
            Return MyBase.NeedParameters OrElse ClientId < 0 OrElse StatementBatchID < 1
        End Function

        Public Overrides Function RequestReportParameters() As System.Windows.Forms.DialogResult

            ' Set the filter criteria for the client
            filterCriteria = New DebtPlus.Reports.Client.Statement.DMP.SelectionParameters()
            filterCriteria.ClientID = ClientId

            Using frm As New DebtPlus.Reports.Client.Statement.DMP.ClientStatementBatchParametersForm(filterCriteria)
                If Not frm.SetDataSource() Then
                    DebtPlus.Data.Forms.MessageBox.Show("There are no statements for this client to be shown", "Sorry, no statements", MessageBoxButtons.OK)
                    Return DialogResult.Cancel
                End If

                Return frm.ShowDialog()
            End Using
        End Function

        Private Sub MonthlyClientStatementReport_BeforePrint(sender As Object, e As System.Drawing.Printing.PrintEventArgs)
            DefineDataSource(filterCriteria)
        End Sub
    End Class
End Namespace
