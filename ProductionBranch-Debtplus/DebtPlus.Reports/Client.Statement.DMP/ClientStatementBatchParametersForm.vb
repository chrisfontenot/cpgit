#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}
#End Region

Option Compare Binary
Option Explicit On
Option Strict On

Imports DebtPlus.LINQ
Imports System.Linq

Namespace Client.Statement.DMP
    Public Class ClientStatementBatchParametersForm
        Inherits DebtPlus.Reports.Template.Forms.BaseBatchParametersForm

        Public Sub New()
            MyBase.New()
            InitializeComponent()
        End Sub

        Protected ds As New System.Data.DataSet("ds")
        Protected record As SelectionParameters = Nothing

        Public Sub New(record As SelectionParameters)
            MyClass.New()
            Me.record = record
            RegisterHandlers()
        End Sub

        Private Sub RegisterHandlers()
        End Sub

        Private Sub UnRegisterHandlers()
        End Sub

        ''' <summary>
        ''' Set the data source to the list of statement batches
        ''' </summary>
        Public Function SetDataSource() As Boolean
            UnRegisterHandlers()
            Try
                Dim connectionString As String = DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString
                Using cn As New System.Data.SqlClient.SqlConnection(connectionString)
                    cn.Open()

                    Using cmd As New System.Data.SqlClient.SqlCommand()
                        cmd.Connection = cn
                        cmd.CommandType = CommandType.Text
                        cmd.CommandText = String.Format("SET ROWCOUNT 50; {0} INNER JOIN [client_statement_clients] c on b.[client_statement_batch] = c.[client_statement_batch] AND c.[client] = @clientID ORDER BY b.[client_statement_batch] DESC", BaseClientStatementReport.Select_client_statement_batches)
                        cmd.Parameters.Add("@clientID", SqlDbType.Int).Value = record.ClientID

                        Using da As New System.Data.SqlClient.SqlDataAdapter(cmd)
                            da.Fill(ds, "client_statement_batches")
                            Dim tbl As System.Data.DataTable = ds.Tables("client_statement_batches")

                            If tbl IsNot Nothing AndAlso tbl.Rows.Count > 0 Then
                                Dim vue As System.Data.DataView = tbl.DefaultView
                                If vue.Count > 0 Then
                                    XrLookup_param_09_1.Properties.DataSource = vue
                                    XrLookup_param_09_1.EditValue = DebtPlus.Utils.Nulls.v_Int32(tbl.Rows(0)("client_statement_batch"))
                                    XrText_param_09_1.EditValue = DebtPlus.Utils.Nulls.v_Int32(tbl.Rows(0)("client_statement_batch"))
                                    Return True
                                End If
                            End If
                        End Using
                    End Using
                End Using

            Catch ex As System.Data.SqlClient.SqlException
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex)

            Finally
                RegisterHandlers()
            End Try

            Return False
        End Function

        ''' <summary>
        ''' Handle the click event on the OK button
        ''' </summary>
        Protected Overrides Sub ButtonOK_Click(sender As Object, e As System.EventArgs)
            MyBase.ButtonOK_Click(sender, e)

            ' Update the pointer to the selected statement batch
            record.BatchID = getSelectedID()
            record.client_statement_batch = TryCast(XrLookup_param_09_1.GetSelectedDataRow(), System.Data.DataRowView)
        End Sub
    End Class
End Namespace