﻿Namespace Client.Statement.DMP
    <Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
    Partial Class SelectionParametersForm

        'Form overrides dispose to clean up the component list.
        <System.Diagnostics.DebuggerNonUserCode()> _
        Protected Overrides Sub Dispose(ByVal disposing As Boolean)
            Try
                If disposing Then
                    If components IsNot Nothing Then components.Dispose()
                End If
                components = Nothing
            Finally
                MyBase.Dispose(disposing)
            End Try
        End Sub

        'Required by the Windows Form Designer
        Private components As System.ComponentModel.IContainer = Nothing

        'NOTE: The following procedure is required by the Windows Form Designer
        'It can be modified using the Windows Form Designer.  
        'Do not modify it using the code editor.
        <System.Diagnostics.DebuggerStepThrough()> _
        Private Sub InitializeComponent()
            Me.ComboBoxEdit_ACH = New DevExpress.XtraEditors.ComboBoxEdit()
            Me.ComboBoxEdit_MultiPage = New DevExpress.XtraEditors.ComboBoxEdit()
            Me.CheckEdit1 = New DevExpress.XtraEditors.CheckEdit()
            Me.XrGroup_param_09_1.SuspendLayout()
            CType(Me.XrRadio_param_09_1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.XrRadio_param_09_2.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.XrText_param_09_1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.XrLookup_param_09_1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.DxErrorProvider1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.ComboBoxEdit_ACH.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.ComboBoxEdit_MultiPage.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.CheckEdit1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.SuspendLayout()
            '
            'XrRadio_param_09_1
            '
            '
            'XrRadio_param_09_2
            '
            '
            'XrLbl_param_09_1
            '
            Me.XrLbl_param_09_1.Appearance.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            '
            'XrText_param_09_1
            '
            Me.XrText_param_09_1.Properties.Appearance.Options.UseTextOptions = True
            Me.XrText_param_09_1.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.XrText_param_09_1.Properties.AppearanceDisabled.Options.UseTextOptions = True
            Me.XrText_param_09_1.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.XrText_param_09_1.Properties.DisplayFormat.FormatString = "f0"
            Me.XrText_param_09_1.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.XrText_param_09_1.Properties.EditFormat.FormatString = "f0"
            Me.XrText_param_09_1.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.XrText_param_09_1.Properties.Mask.BeepOnError = True
            Me.XrText_param_09_1.Properties.Mask.EditMask = "\d*"
            Me.XrText_param_09_1.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx
            Me.XrText_param_09_1.ToolTip = "Enter a specific statement batch if it is not included in the above list"
            '
            'XrLookup_param_09_1
            '
            Me.XrLookup_param_09_1.Properties.Columns.AddRange(New DevExpress.XtraEditors.Controls.LookUpColumnInfo() {New DevExpress.XtraEditors.Controls.LookUpColumnInfo("client_statement_batch", "ID", 5, DevExpress.Utils.FormatType.Numeric, "f0", True, DevExpress.Utils.HorzAlignment.Far, DevExpress.Data.ColumnSortOrder.None, DevExpress.Utils.DefaultBoolean.[True]), New DevExpress.XtraEditors.Controls.LookUpColumnInfo("statement_date", "Statement Date", 20, DevExpress.Utils.FormatType.DateTime, "MM/dd/yyyy", True, DevExpress.Utils.HorzAlignment.Far, DevExpress.Data.ColumnSortOrder.Descending, DevExpress.Utils.DefaultBoolean.[True]), New DevExpress.XtraEditors.Controls.LookUpColumnInfo("period_start", "Starting Date", 20, DevExpress.Utils.FormatType.DateTime, "MM/dd/yyyy", True, DevExpress.Utils.HorzAlignment.Far, DevExpress.Data.ColumnSortOrder.None, DevExpress.Utils.DefaultBoolean.[True]), New DevExpress.XtraEditors.Controls.LookUpColumnInfo("period_end", "Ending Date", 20, DevExpress.Utils.FormatType.DateTime, "MM/dd/yyyy", True, DevExpress.Utils.HorzAlignment.Far, DevExpress.Data.ColumnSortOrder.None, DevExpress.Utils.DefaultBoolean.[True])})
            Me.XrLookup_param_09_1.Properties.DisplayFormat.FormatString = "MM/dd/yyyy"
            Me.XrLookup_param_09_1.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime
            Me.XrLookup_param_09_1.Properties.DisplayMember = "statement_date"
            Me.XrLookup_param_09_1.Properties.ValueMember = "client_statement_batch"
            '
            'ButtonOK
            '
            Me.ButtonOK.TabIndex = 4
            '
            'ButtonCancel
            '
            Me.ButtonCancel.TabIndex = 5
            '
            'DefaultLookAndFeel1
            '
            Me.DefaultLookAndFeel1.LookAndFeel.SkinName = "McSkin"
            '
            'ComboBoxEdit_ACH
            '
            Me.ComboBoxEdit_ACH.EditValue = "Include ACH and NON-ACH clients"
            Me.ComboBoxEdit_ACH.Location = New System.Drawing.Point(40, 153)
            Me.ComboBoxEdit_ACH.Name = "ComboBoxEdit_ACH"
            Me.ComboBoxEdit_ACH.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[False]
            Me.ComboBoxEdit_ACH.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.ComboBoxEdit_ACH.Properties.Items.AddRange(New Object() {"Include ACH and NON-ACH clients", "Include only ACH clients", "Include only NON-ACH clients"})
            Me.ComboBoxEdit_ACH.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
            Me.ComboBoxEdit_ACH.Size = New System.Drawing.Size(211, 20)
            Me.ComboBoxEdit_ACH.TabIndex = 2
            '
            'ComboBoxEdit_MultiPage
            '
            Me.ComboBoxEdit_MultiPage.EditValue = "Include all statements"
            Me.ComboBoxEdit_MultiPage.Location = New System.Drawing.Point(40, 127)
            Me.ComboBoxEdit_MultiPage.Name = "ComboBoxEdit_MultiPage"
            Me.ComboBoxEdit_MultiPage.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[False]
            Me.ComboBoxEdit_MultiPage.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.ComboBoxEdit_MultiPage.Properties.Items.AddRange(New Object() {"Include all statements", "Include only single-page statements", "Include only multi-page statements"})
            Me.ComboBoxEdit_MultiPage.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
            Me.ComboBoxEdit_MultiPage.Size = New System.Drawing.Size(211, 20)
            Me.ComboBoxEdit_MultiPage.TabIndex = 1
            '
            'CheckEdit1
            '
            Me.CheckEdit1.EditValue = True
            Me.CheckEdit1.Location = New System.Drawing.Point(18, 179)
            Me.CheckEdit1.Name = "CheckEdit1"
            Me.CheckEdit1.Properties.Caption = "Suppress clients who have opted out of paper statements"
            Me.CheckEdit1.Size = New System.Drawing.Size(312, 20)
            Me.CheckEdit1.TabIndex = 3
            '
            'SelectionParametersForm
            '
            Me.ClientSize = New System.Drawing.Size(336, 203)
            Me.Controls.Add(Me.CheckEdit1)
            Me.Controls.Add(Me.ComboBoxEdit_ACH)
            Me.Controls.Add(Me.ComboBoxEdit_MultiPage)
            Me.Name = "SelectionParametersForm"
            Me.Text = "Statement Batch Selection"
            Me.Controls.SetChildIndex(Me.ButtonOK, 0)
            Me.Controls.SetChildIndex(Me.ButtonCancel, 0)
            Me.Controls.SetChildIndex(Me.XrGroup_param_09_1, 0)
            Me.Controls.SetChildIndex(Me.ComboBoxEdit_MultiPage, 0)
            Me.Controls.SetChildIndex(Me.ComboBoxEdit_ACH, 0)
            Me.Controls.SetChildIndex(Me.CheckEdit1, 0)
            Me.XrGroup_param_09_1.ResumeLayout(False)
            Me.XrGroup_param_09_1.PerformLayout()
            CType(Me.XrRadio_param_09_1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.XrRadio_param_09_2.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.XrText_param_09_1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.XrLookup_param_09_1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.DxErrorProvider1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.ComboBoxEdit_ACH.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.ComboBoxEdit_MultiPage.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.CheckEdit1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            Me.ResumeLayout(False)

        End Sub
        Friend WithEvents ComboBoxEdit_ACH As DevExpress.XtraEditors.ComboBoxEdit
        Friend WithEvents ComboBoxEdit_MultiPage As DevExpress.XtraEditors.ComboBoxEdit
        Friend WithEvents CheckEdit1 As DevExpress.XtraEditors.CheckEdit
    End Class
End Namespace
