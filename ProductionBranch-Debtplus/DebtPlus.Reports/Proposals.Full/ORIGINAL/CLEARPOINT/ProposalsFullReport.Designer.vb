Namespace Proposals.Full
    <Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
    Partial Class ProposalsFullReport
        Inherits DebtPlus.Reports.Template.BaseXtraReportClass

        'XtraReport overrides dispose to clean up the component list.
        <System.Diagnostics.DebuggerNonUserCode()> _
        Protected Overrides Sub Dispose(ByVal disposing As Boolean)
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
            MyBase.Dispose(disposing)
        End Sub

        'Required by the Designer
        Private components As System.ComponentModel.IContainer = Nothing

        'NOTE: The following procedure is required by the Designer
        'It can be modified using the Designer.  
        'Do not modify it using the code editor.
        <System.Diagnostics.DebuggerStepThrough()>
        Private Sub InitializeComponent()
            Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(ProposalsFullReport))
            Dim PostNetGenerator1 As DevExpress.XtraPrinting.BarCode.PostNetGenerator = New DevExpress.XtraPrinting.BarCode.PostNetGenerator()
            Me.XrControlStyle1 = New DevExpress.XtraReports.UI.XRControlStyle()
            Me.XrLabel_Creditor_L = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel1 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel3 = New DevExpress.XtraReports.UI.XRLabel()
            Me.PageHeader = New DevExpress.XtraReports.UI.PageHeaderBand()
            Me.XrLabel_print_date = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel4 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrPictureBox1 = New DevExpress.XtraReports.UI.XRPictureBox()
            Me.XrLabel2 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrSubreport_GeneralInfo = New DevExpress.XtraReports.UI.XRSubreport()
            Me.XtraReport_GeneralInfo1 = New DebtPlus.Reports.Proposals.Full.XtraReport_GeneralInfo()
            Me.XrPanel1 = New DevExpress.XtraReports.UI.XRPanel()
            Me.XrLine2 = New DevExpress.XtraReports.UI.XRLine()
            Me.XrLine1 = New DevExpress.XtraReports.UI.XRLine()
            Me.XrLabel6 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel9 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel5 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel11 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel10 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel8 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel7 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_message = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrPanel_CreditorAddress = New DevExpress.XtraReports.UI.XRPanel()
            Me.XrLabel_Creditor_Address = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrBarCode_PostalCode = New DevExpress.XtraReports.UI.XRBarCode()
            Me.XrLabel_ClientNameAddress = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrSubreport_Summary = New DevExpress.XtraReports.UI.XRSubreport()
            Me.XtraReport_Summary1 = New DebtPlus.Reports.Proposals.Full.XtraReport_Summary()
            Me.XrSubreport_Employer = New DevExpress.XtraReports.UI.XRSubreport()
            Me.XtraReport_Employer1 = New DebtPlus.Reports.Proposals.Full.XtraReport_Employer()
            Me.XrSubreport_AccountNumber = New DevExpress.XtraReports.UI.XRSubreport()
            Me.XtraReport_AccountNumber1 = New DebtPlus.Reports.Proposals.Full.XtraReport_AccountNumber()
            Me.XrSubreport_Secured = New DevExpress.XtraReports.UI.XRSubreport()
            Me.XtraReport_Secured1 = New DebtPlus.Reports.Proposals.Full.XtraReport_Secured()
            Me.XrSubreport_Budget = New DevExpress.XtraReports.UI.XRSubreport()
            Me.XtraReport_Budget1 = New DebtPlus.Reports.Proposals.Full.XtraReport_Budget()
            Me.XrSubreport_CreditorList = New DevExpress.XtraReports.UI.XRSubreport()
            Me.XtraReport_CreditorList1 = New DebtPlus.Reports.Proposals.Full.XtraReport_CreditorList()
            Me.GroupHeader_SecuredList = New DevExpress.XtraReports.UI.GroupHeaderBand()
            Me.GroupHeader_BudgetList = New DevExpress.XtraReports.UI.GroupHeaderBand()
            Me.GroupHeader_CreditorList = New DevExpress.XtraReports.UI.GroupHeaderBand()
            Me.GroupHeader_GeneralInfo = New DevExpress.XtraReports.UI.GroupHeaderBand()
            Me.GroupHeader_message = New DevExpress.XtraReports.UI.GroupHeaderBand()
            Me.GroupHeader6 = New DevExpress.XtraReports.UI.GroupHeaderBand()
            Me.XrLabel12 = New DevExpress.XtraReports.UI.XRLabel()
            CType(Me.XtraReport_GeneralInfo1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.XtraReport_Summary1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.XtraReport_Employer1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.XtraReport_AccountNumber1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.XtraReport_Secured1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.XtraReport_Budget1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.XtraReport_CreditorList1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
            '
            'Detail
            '
            Me.Detail.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel12})
            Me.Detail.HeightF = 2.0!
            Me.Detail.SnapLinePadding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
            '
            'XrControlStyle1
            '
            Me.XrControlStyle1.BackColor = System.Drawing.Color.Teal
            Me.XrControlStyle1.BorderColor = System.Drawing.Color.Teal
            Me.XrControlStyle1.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.XrControlStyle1.ForeColor = System.Drawing.Color.White
            Me.XrControlStyle1.Name = "XrControlStyle1"
            Me.XrControlStyle1.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
            Me.XrControlStyle1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            '
            'XrLabel_Creditor_L
            '
            Me.XrLabel_Creditor_L.LocationFloat = New DevExpress.Utils.PointFloat(8.0!, 158.0!)
            Me.XrLabel_Creditor_L.Multiline = True
            Me.XrLabel_Creditor_L.Name = "XrLabel_Creditor_L"
            Me.XrLabel_Creditor_L.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_Creditor_L.SizeF = New System.Drawing.SizeF(308.0!, 100.0!)
            Me.XrLabel_Creditor_L.Text = "XrLabel_Creditor_L"
            '
            'XrLabel1
            '
            Me.XrLabel1.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
            Me.XrLabel1.Name = "XrLabel1"
            Me.XrLabel1.SizeF = New System.Drawing.SizeF(100.0!, 23.0!)
            '
            'XrLabel3
            '
            Me.XrLabel3.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
            Me.XrLabel3.Name = "XrLabel3"
            Me.XrLabel3.SizeF = New System.Drawing.SizeF(100.0!, 23.0!)
            '
            'PageHeader
            '
            Me.PageHeader.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel_print_date, Me.XrLabel4, Me.XrPictureBox1, Me.XrLabel2})
            Me.PageHeader.HeightF = 182.0!
            Me.PageHeader.Name = "PageHeader"
            '
            'XrLabel_print_date
            '
            Me.XrLabel_print_date.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "print_date", "{0:MMMM d, yyyy}")})
            Me.XrLabel_print_date.LocationFloat = New DevExpress.Utils.PointFloat(17.0!, 117.0!)
            Me.XrLabel_print_date.Name = "XrLabel_print_date"
            Me.XrLabel_print_date.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_print_date.SizeF = New System.Drawing.SizeF(183.0!, 17.0!)
            Me.XrLabel_print_date.Text = "Feburary 30, 2009"
            '
            'XrLabel4
            '
            Me.XrLabel4.LocationFloat = New DevExpress.Utils.PointFloat(17.0!, 142.0!)
            Me.XrLabel4.Multiline = True
            Me.XrLabel4.Name = "XrLabel4"
            Me.XrLabel4.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel4.SizeF = New System.Drawing.SizeF(183.0!, 33.0!)
            Me.XrLabel4.Text = "NFCC Unit # 20003" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Federal Tax ID# 58-0942924"
            '
            'XrPictureBox1
            '
            Me.XrPictureBox1.Image = CType(resources.GetObject("XrPictureBox1.Image"), System.Drawing.Image)
            Me.XrPictureBox1.LocationFloat = New DevExpress.Utils.PointFloat(16.0!, 8.0!)
            Me.XrPictureBox1.Name = "XrPictureBox1"
            Me.XrPictureBox1.SizeF = New System.Drawing.SizeF(198.0!, 100.0!)
            '
            'XrLabel2
            '
            Me.XrLabel2.LocationFloat = New DevExpress.Utils.PointFloat(233.0!, 10.00001!)
            Me.XrLabel2.Multiline = True
            Me.XrLabel2.Name = "XrLabel2"
            Me.XrLabel2.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel2.SizeF = New System.Drawing.SizeF(381.5833!, 97.99998!)
            Me.XrLabel2.StylePriority.UseTextAlignment = False
            Me.XrLabel2.Text = "Consumer Credit Counseling Service of Greater Atlanta, Inc." & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "ClearPoint Credit Co" & _
        "unseling Solutions" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "8000 Franklin Farms Drive" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Richmond, VA 23229-5004" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Telephon" & _
        "e: 877-877-1995" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Fax: 855-895-1718" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
            Me.XrLabel2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            '
            'XrSubreport_GeneralInfo
            '
            Me.XrSubreport_GeneralInfo.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
            Me.XrSubreport_GeneralInfo.Name = "XrSubreport_GeneralInfo"
            Me.XrSubreport_GeneralInfo.ReportSource = Me.XtraReport_GeneralInfo1
            Me.XrSubreport_GeneralInfo.SizeF = New System.Drawing.SizeF(800.0!, 25.0!)
            '
            'XrPanel1
            '
            Me.XrPanel1.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLine2, Me.XrLine1, Me.XrLabel6, Me.XrLabel9, Me.XrLabel5, Me.XrLabel11, Me.XrLabel10, Me.XrLabel8, Me.XrLabel7})
            Me.XrPanel1.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 265.5417!)
            Me.XrPanel1.Name = "XrPanel1"
            Me.XrPanel1.SizeF = New System.Drawing.SizeF(800.0!, 234.375!)
            '
            'XrLine2
            '
            Me.XrLine2.LocationFloat = New DevExpress.Utils.PointFloat(233.0!, 163.5416!)
            Me.XrLine2.Name = "XrLine2"
            Me.XrLine2.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 10, 0, 100.0!)
            Me.XrLine2.SizeF = New System.Drawing.SizeF(167.0!, 17.0!)
            Me.XrLine2.StylePriority.UsePadding = False
            '
            'XrLine1
            '
            Me.XrLine1.LocationFloat = New DevExpress.Utils.PointFloat(233.0!, 138.5416!)
            Me.XrLine1.Name = "XrLine1"
            Me.XrLine1.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 10, 0, 100.0!)
            Me.XrLine1.SizeF = New System.Drawing.SizeF(167.0!, 17.0!)
            Me.XrLine1.StylePriority.UsePadding = False
            '
            'XrLabel6
            '
            Me.XrLabel6.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 188.5417!)
            Me.XrLabel6.Name = "XrLabel6"
            Me.XrLabel6.Padding = New DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 100.0!)
            Me.XrLabel6.SizeF = New System.Drawing.SizeF(767.0!, 17.0!)
            Me.XrLabel6.StylePriority.UsePadding = False
            Me.XrLabel6.Text = "3. Consider the abatement or reduction of interest (Fill in APR below)"
            '
            'XrLabel9
            '
            Me.XrLabel9.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 163.5417!)
            Me.XrLabel9.Name = "XrLabel9"
            Me.XrLabel9.Padding = New DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 100.0!)
            Me.XrLabel9.SizeF = New System.Drawing.SizeF(233.0!, 17.0!)
            Me.XrLabel9.StylePriority.UsePadding = False
            Me.XrLabel9.Text = "2. Discontinue collection activities"
            '
            'XrLabel5
            '
            Me.XrLabel5.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
            Me.XrLabel5.Name = "XrLabel5"
            Me.XrLabel5.Padding = New DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 100.0!)
            Me.XrLabel5.SizeF = New System.Drawing.SizeF(800.0!, 17.0!)
            Me.XrLabel5.StylePriority.UsePadding = False
            Me.XrLabel5.Text = "THE ABOVE CLIENT HAS GIVEN US PERMISSION TO DISCUSS HIS/HER ACCOUNTS WITH ALL CRE" & _
        "DITORS."
            '
            'XrLabel11
            '
            Me.XrLabel11.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 215.625!)
            Me.XrLabel11.Name = "XrLabel11"
            Me.XrLabel11.Padding = New DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 100.0!)
            Me.XrLabel11.SizeF = New System.Drawing.SizeF(767.0!, 17.0!)
            Me.XrLabel11.StylePriority.UsePadding = False
            Me.XrLabel11.Text = "4. Allow us to retain up to 15% of each payment as a tax deductable contribution " & _
        "while allowing the client full credit"
            '
            'XrLabel10
            '
            Me.XrLabel10.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel10.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 26.04166!)
            Me.XrLabel10.Name = "XrLabel10"
            Me.XrLabel10.Padding = New DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 100.0!)
            Me.XrLabel10.SizeF = New System.Drawing.SizeF(800.0!, 25.0!)
            Me.XrLabel10.StylePriority.UseFont = False
            Me.XrLabel10.StylePriority.UsePadding = False
            Me.XrLabel10.StylePriority.UseTextAlignment = False
            Me.XrLabel10.Text = "Please complete this portion below and return to the above address."
            Me.XrLabel10.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
            '
            'XrLabel8
            '
            Me.XrLabel8.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 138.5417!)
            Me.XrLabel8.Name = "XrLabel8"
            Me.XrLabel8.Padding = New DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 100.0!)
            Me.XrLabel8.SizeF = New System.Drawing.SizeF(233.0!, 17.0!)
            Me.XrLabel8.StylePriority.UsePadding = False
            Me.XrLabel8.Text = "1. Wave late charges and over limit fees."
            '
            'XrLabel7
            '
            Me.XrLabel7.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 63.54169!)
            Me.XrLabel7.Multiline = True
            Me.XrLabel7.Name = "XrLabel7"
            Me.XrLabel7.Padding = New DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 100.0!)
            Me.XrLabel7.SizeF = New System.Drawing.SizeF(800.0!, 64.58322!)
            Me.XrLabel7.StylePriority.UsePadding = False
            Me.XrLabel7.StylePriority.UseTextAlignment = False
            Me.XrLabel7.Text = resources.GetString("XrLabel7.Text")
            Me.XrLabel7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopJustify
            '
            'XrLabel_message
            '
            Me.XrLabel_message.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "message")})
            Me.XrLabel_message.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel_message.LocationFloat = New DevExpress.Utils.PointFloat(0.00002384186!, 0.0!)
            Me.XrLabel_message.Multiline = True
            Me.XrLabel_message.Name = "XrLabel_message"
            Me.XrLabel_message.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_message.SizeF = New System.Drawing.SizeF(800.0!, 25.0!)
            Me.XrLabel_message.StylePriority.UseFont = False
            '
            'XrPanel_CreditorAddress
            '
            Me.XrPanel_CreditorAddress.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel_Creditor_Address, Me.XrBarCode_PostalCode})
            Me.XrPanel_CreditorAddress.LocationFloat = New DevExpress.Utils.PointFloat(46.54!, 42.75!)
            Me.XrPanel_CreditorAddress.Name = "XrPanel_CreditorAddress"
            Me.XrPanel_CreditorAddress.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
            Me.XrPanel_CreditorAddress.Scripts.OnBeforePrint = "XrPanel_CreditorAddress_BeforePrint"
            Me.XrPanel_CreditorAddress.SizeF = New System.Drawing.SizeF(300.0!, 117.0!)
            '
            'XrLabel_Creditor_Address
            '
            Me.XrLabel_Creditor_Address.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 17.0!)
            Me.XrLabel_Creditor_Address.Multiline = True
            Me.XrLabel_Creditor_Address.Name = "XrLabel_Creditor_Address"
            Me.XrLabel_Creditor_Address.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_Creditor_Address.SizeF = New System.Drawing.SizeF(300.0!, 100.0!)
            Me.XrLabel_Creditor_Address.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
            '
            'XrBarCode_PostalCode
            '
            Me.XrBarCode_PostalCode.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
            Me.XrBarCode_PostalCode.Name = "XrBarCode_PostalCode"
            Me.XrBarCode_PostalCode.Padding = New DevExpress.XtraPrinting.PaddingInfo(10, 10, 0, 0, 100.0!)
            Me.XrBarCode_PostalCode.ShowText = False
            Me.XrBarCode_PostalCode.SizeF = New System.Drawing.SizeF(300.0!, 17.0!)
            Me.XrBarCode_PostalCode.Symbology = PostNetGenerator1
            Me.XrBarCode_PostalCode.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
            '
            'XrLabel_ClientNameAddress
            '
            Me.XrLabel_ClientNameAddress.LocationFloat = New DevExpress.Utils.PointFloat(500.0!, 35.08332!)
            Me.XrLabel_ClientNameAddress.Multiline = True
            Me.XrLabel_ClientNameAddress.Name = "XrLabel_ClientNameAddress"
            Me.XrLabel_ClientNameAddress.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_ClientNameAddress.Scripts.OnBeforePrint = "XrLabel_ClientNameAddress_BeforePrint"
            Me.XrLabel_ClientNameAddress.SizeF = New System.Drawing.SizeF(283.0!, 92.0!)
            '
            'XrSubreport_Summary
            '
            Me.XrSubreport_Summary.LocationFloat = New DevExpress.Utils.PointFloat(500.0!, 142.0!)
            Me.XrSubreport_Summary.Name = "XrSubreport_Summary"
            Me.XrSubreport_Summary.ReportSource = Me.XtraReport_Summary1
            Me.XrSubreport_Summary.SizeF = New System.Drawing.SizeF(300.0!, 110.0!)
            '
            'XrSubreport_Employer
            '
            Me.XrSubreport_Employer.LocationFloat = New DevExpress.Utils.PointFloat(7.999992!, 222.6875!)
            Me.XrSubreport_Employer.Name = "XrSubreport_Employer"
            Me.XrSubreport_Employer.ReportSource = Me.XtraReport_Employer1
            Me.XrSubreport_Employer.SizeF = New System.Drawing.SizeF(400.0!, 34.0!)
            '
            'XrSubreport_AccountNumber
            '
            Me.XrSubreport_AccountNumber.LocationFloat = New DevExpress.Utils.PointFloat(7.999992!, 179.8333!)
            Me.XrSubreport_AccountNumber.Name = "XrSubreport_AccountNumber"
            Me.XrSubreport_AccountNumber.ReportSource = Me.XtraReport_AccountNumber1
            Me.XrSubreport_AccountNumber.SizeF = New System.Drawing.SizeF(400.0!, 34.0!)
            '
            'XrSubreport_Secured
            '
            Me.XrSubreport_Secured.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
            Me.XrSubreport_Secured.Name = "XrSubreport_Secured"
            Me.XrSubreport_Secured.ReportSource = Me.XtraReport_Secured1
            Me.XrSubreport_Secured.SizeF = New System.Drawing.SizeF(800.0!, 25.0!)
            '
            'XrSubreport_Budget
            '
            Me.XrSubreport_Budget.LocationFloat = New DevExpress.Utils.PointFloat(0.00002384186!, 0.0!)
            Me.XrSubreport_Budget.Name = "XrSubreport_Budget"
            Me.XrSubreport_Budget.ReportSource = Me.XtraReport_Budget1
            Me.XrSubreport_Budget.SizeF = New System.Drawing.SizeF(800.0!, 25.0!)
            '
            'XrSubreport_CreditorList
            '
            Me.XrSubreport_CreditorList.LocationFloat = New DevExpress.Utils.PointFloat(0.00002384186!, 0.0!)
            Me.XrSubreport_CreditorList.Name = "XrSubreport_CreditorList"
            Me.XrSubreport_CreditorList.ReportSource = Me.XtraReport_CreditorList1
            Me.XrSubreport_CreditorList.SizeF = New System.Drawing.SizeF(800.0!, 25.0!)
            '
            'GroupHeader_SecuredList
            '
            Me.GroupHeader_SecuredList.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrSubreport_Secured})
            Me.GroupHeader_SecuredList.HeightF = 25.0!
            Me.GroupHeader_SecuredList.KeepTogether = True
            Me.GroupHeader_SecuredList.Name = "GroupHeader_SecuredList"
            '
            'GroupHeader_BudgetList
            '
            Me.GroupHeader_BudgetList.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrSubreport_Budget})
            Me.GroupHeader_BudgetList.HeightF = 25.0!
            Me.GroupHeader_BudgetList.KeepTogether = True
            Me.GroupHeader_BudgetList.Level = 1
            Me.GroupHeader_BudgetList.Name = "GroupHeader_BudgetList"
            '
            'GroupHeader_CreditorList
            '
            Me.GroupHeader_CreditorList.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrSubreport_CreditorList})
            Me.GroupHeader_CreditorList.HeightF = 25.0!
            Me.GroupHeader_CreditorList.KeepTogether = True
            Me.GroupHeader_CreditorList.Level = 2
            Me.GroupHeader_CreditorList.Name = "GroupHeader_CreditorList"
            '
            'GroupHeader_GeneralInfo
            '
            Me.GroupHeader_GeneralInfo.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrSubreport_GeneralInfo})
            Me.GroupHeader_GeneralInfo.HeightF = 25.0!
            Me.GroupHeader_GeneralInfo.KeepTogether = True
            Me.GroupHeader_GeneralInfo.Level = 3
            Me.GroupHeader_GeneralInfo.Name = "GroupHeader_GeneralInfo"
            '
            'GroupHeader_message
            '
            Me.GroupHeader_message.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel_message})
            Me.GroupHeader_message.HeightF = 25.0!
            Me.GroupHeader_message.KeepTogether = True
            Me.GroupHeader_message.Level = 4
            Me.GroupHeader_message.Name = "GroupHeader_message"
            Me.GroupHeader_message.Scripts.OnBeforePrint = "GroupHeader_message_BeforePrint"
            '
            'GroupHeader6
            '
            Me.GroupHeader6.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrPanel1, Me.XrSubreport_Summary, Me.XrSubreport_Employer, Me.XrSubreport_AccountNumber, Me.XrPanel_CreditorAddress, Me.XrLabel_ClientNameAddress})
            Me.GroupHeader6.HeightF = 510.3333!
            Me.GroupHeader6.KeepTogether = True
            Me.GroupHeader6.Level = 5
            Me.GroupHeader6.Name = "GroupHeader6"
            Me.GroupHeader6.PageBreak = DevExpress.XtraReports.UI.PageBreak.BeforeBand
            Me.GroupHeader6.GroupFields.Add(New DevExpress.XtraReports.UI.GroupField("client_creditor_proposal"))
            '
            'XrLabel12
            '
            Me.XrLabel12.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
            Me.XrLabel12.Name = "XrLabel12"
            Me.XrLabel12.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel12.SizeF = New System.Drawing.SizeF(100.0!, 2.0!)
            '
            'ProposalsFullReport
            '
            Me.Bands.AddRange(New DevExpress.XtraReports.UI.Band() {Me.TopMarginBand1, Me.Detail, Me.BottomMarginBand1, Me.PageHeader, Me.GroupHeader_SecuredList, Me.GroupHeader_BudgetList, Me.GroupHeader_CreditorList, Me.GroupHeader_GeneralInfo, Me.GroupHeader_message, Me.GroupHeader6})
            Me.RequestParameters = False
            Me.ScriptLanguage = DevExpress.XtraReports.ScriptLanguage.VisualBasic
            Me.ScriptReferencesString = resources.GetString("$this.ScriptReferencesString")
            Me.Scripts.OnBeforePrint = "ProposalsFullReport_BeforePrint"
            Me.ScriptsSource = resources.GetString("$this.ScriptsSource")
            Me.StyleSheet.AddRange(New DevExpress.XtraReports.UI.XRControlStyle() {Me.XrControlStyle1})
            Me.Version = "11.2"
            Me.Controls.SetChildIndex(Me.GroupHeader6, 0)
            Me.Controls.SetChildIndex(Me.GroupHeader_message, 0)
            Me.Controls.SetChildIndex(Me.GroupHeader_GeneralInfo, 0)
            Me.Controls.SetChildIndex(Me.GroupHeader_CreditorList, 0)
            Me.Controls.SetChildIndex(Me.GroupHeader_BudgetList, 0)
            Me.Controls.SetChildIndex(Me.GroupHeader_SecuredList, 0)
            Me.Controls.SetChildIndex(Me.PageHeader, 0)
            Me.Controls.SetChildIndex(Me.BottomMarginBand1, 0)
            Me.Controls.SetChildIndex(Me.Detail, 0)
            Me.Controls.SetChildIndex(Me.TopMarginBand1, 0)
            CType(Me.XtraReport_GeneralInfo1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.XtraReport_Summary1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.XtraReport_Employer1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.XtraReport_AccountNumber1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.XtraReport_Secured1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.XtraReport_Budget1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.XtraReport_CreditorList1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me, System.ComponentModel.ISupportInitialize).EndInit()

        End Sub

        Protected WithEvents XrLabel_Creditor_L As DevExpress.XtraReports.UI.XRLabel
        Protected WithEvents XrLabel3 As DevExpress.XtraReports.UI.XRLabel
        Protected WithEvents XrLabel1 As DevExpress.XtraReports.UI.XRLabel
        Protected WithEvents PageHeader As DevExpress.XtraReports.UI.PageHeaderBand
        Protected WithEvents XrLabel2 As DevExpress.XtraReports.UI.XRLabel
        Protected WithEvents XrLabel_ClientNameAddress As DevExpress.XtraReports.UI.XRLabel
        Protected WithEvents XrPanel_CreditorAddress As DevExpress.XtraReports.UI.XRPanel
        Protected WithEvents XrLabel_Creditor_Address As DevExpress.XtraReports.UI.XRLabel
        Protected WithEvents XrBarCode_PostalCode As DevExpress.XtraReports.UI.XRBarCode
        Protected WithEvents XrPictureBox1 As DevExpress.XtraReports.UI.XRPictureBox
        Protected WithEvents XrLabel4 As DevExpress.XtraReports.UI.XRLabel
        Protected WithEvents XrLabel_print_date As DevExpress.XtraReports.UI.XRLabel
        Protected WithEvents XrLabel_message As DevExpress.XtraReports.UI.XRLabel
        Protected WithEvents XrSubreport_Summary As DevExpress.XtraReports.UI.XRSubreport
        Protected WithEvents XrSubreport_Employer As DevExpress.XtraReports.UI.XRSubreport
        Protected WithEvents XrSubreport_AccountNumber As DevExpress.XtraReports.UI.XRSubreport
        Protected WithEvents XrSubreport_CreditorList As DevExpress.XtraReports.UI.XRSubreport
        Protected WithEvents XrSubreport_Budget As DevExpress.XtraReports.UI.XRSubreport
        Protected WithEvents XrSubreport_Secured As DevExpress.XtraReports.UI.XRSubreport
        Protected WithEvents XrSubreport_GeneralInfo As DevExpress.XtraReports.UI.XRSubreport
        Protected WithEvents XrPanel1 As DevExpress.XtraReports.UI.XRPanel
        Protected WithEvents XrLine2 As DevExpress.XtraReports.UI.XRLine
        Protected WithEvents XrLine1 As DevExpress.XtraReports.UI.XRLine
        Protected WithEvents XrLabel6 As DevExpress.XtraReports.UI.XRLabel
        Protected WithEvents XrLabel9 As DevExpress.XtraReports.UI.XRLabel
        Protected WithEvents XrLabel5 As DevExpress.XtraReports.UI.XRLabel
        Protected WithEvents XrLabel11 As DevExpress.XtraReports.UI.XRLabel
        Protected WithEvents XrLabel10 As DevExpress.XtraReports.UI.XRLabel
        Protected WithEvents XrLabel8 As DevExpress.XtraReports.UI.XRLabel
        Protected WithEvents XrLabel7 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrControlStyle1 As DevExpress.XtraReports.UI.XRControlStyle
        Private WithEvents XtraReport_GeneralInfo1 As DebtPlus.Reports.Proposals.Full.XtraReport_GeneralInfo
        Private WithEvents XtraReport_AccountNumber1 As DebtPlus.Reports.Proposals.Full.XtraReport_AccountNumber
        Private WithEvents XtraReport_Employer1 As DebtPlus.Reports.Proposals.Full.XtraReport_Employer
        Private WithEvents XtraReport_Summary1 As DebtPlus.Reports.Proposals.Full.XtraReport_Summary
        Private WithEvents XtraReport_CreditorList1 As DebtPlus.Reports.Proposals.Full.XtraReport_CreditorList
        Private WithEvents XtraReport_Budget1 As DebtPlus.Reports.Proposals.Full.XtraReport_Budget
        Private WithEvents XtraReport_Secured1 As DebtPlus.Reports.Proposals.Full.XtraReport_Secured
        Friend WithEvents GroupHeader_SecuredList As DevExpress.XtraReports.UI.GroupHeaderBand
        Friend WithEvents GroupHeader_BudgetList As DevExpress.XtraReports.UI.GroupHeaderBand
        Friend WithEvents GroupHeader_CreditorList As DevExpress.XtraReports.UI.GroupHeaderBand
        Friend WithEvents GroupHeader_GeneralInfo As DevExpress.XtraReports.UI.GroupHeaderBand
        Friend WithEvents GroupHeader_message As DevExpress.XtraReports.UI.GroupHeaderBand
        Friend WithEvents GroupHeader6 As DevExpress.XtraReports.UI.GroupHeaderBand
        Friend WithEvents XrLabel12 As DevExpress.XtraReports.UI.XRLabel
    End Class
End Namespace
