Namespace Proposals.Full
    <Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
    Partial Class XtraReport_GeneralInfo
        Inherits DevExpress.XtraReports.UI.XtraReport

        'XtraReport overrides dispose to clean up the component list.
        <System.Diagnostics.DebuggerNonUserCode()> _
        Protected Overrides Sub Dispose(ByVal disposing As Boolean)
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
            MyBase.Dispose(disposing)
        End Sub

        'Required by the Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Designer
        'It can be modified using the Designer.  
        'Do not modify it using the code editor.
        <System.Diagnostics.DebuggerStepThrough()> _
        Private Sub InitializeComponent()
            Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(XtraReport_GeneralInfo))
            Me.Detail = New DevExpress.XtraReports.UI.DetailBand()
            Me.XrLabel_client = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel15 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel14 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel13 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel12 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel11 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel10 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel9 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel8 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel7 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_message = New DevExpress.XtraReports.UI.XRLabel()
            Me.TopMarginBand1 = New DevExpress.XtraReports.UI.TopMarginBand()
            Me.BottomMarginBand1 = New DevExpress.XtraReports.UI.BottomMarginBand()
            CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
            '
            'Detail
            '
            Me.Detail.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel_client, Me.XrLabel15, Me.XrLabel14, Me.XrLabel13, Me.XrLabel12, Me.XrLabel11, Me.XrLabel10, Me.XrLabel9, Me.XrLabel8, Me.XrLabel7, Me.XrLabel_message})
            Me.Detail.HeightF = 294.0833!
            Me.Detail.Name = "Detail"
            Me.Detail.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
            Me.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
            '
            'XrLabel_client
            '
            Me.XrLabel_client.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel_client.LocationFloat = New DevExpress.Utils.PointFloat(575.0!, 102.0833!)
            Me.XrLabel_client.Name = "XrLabel_client"
            Me.XrLabel_client.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_client.Scripts.OnBeforePrint = "XrLabel_client_BeforePrint"
            Me.XrLabel_client.SizeF = New System.Drawing.SizeF(225.0!, 25.0!)
            Me.XrLabel_client.StylePriority.UseFont = False
            '
            'XrLabel15
            '
            Me.XrLabel15.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "counselor_name", "Counselor: {0}")})
            Me.XrLabel15.LocationFloat = New DevExpress.Utils.PointFloat(375.0!, 269.0833!)
            Me.XrLabel15.Name = "XrLabel15"
            Me.XrLabel15.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel15.SizeF = New System.Drawing.SizeF(425.0!, 25.0!)
            '
            'XrLabel14
            '
            Me.XrLabel14.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 269.0833!)
            Me.XrLabel14.Name = "XrLabel14"
            Me.XrLabel14.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel14.SizeF = New System.Drawing.SizeF(367.0!, 25.0!)
            Me.XrLabel14.Text = "Signature __________________________________________"
            '
            'XrLabel13
            '
            Me.XrLabel13.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 235.0833!)
            Me.XrLabel13.Name = "XrLabel13"
            Me.XrLabel13.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel13.SizeF = New System.Drawing.SizeF(800.0!, 25.0!)
            Me.XrLabel13.Text = "Name _____________________________________________   Amount of Last Payment _____" & _
        "___________________________"
            '
            'XrLabel12
            '
            Me.XrLabel12.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 202.0833!)
            Me.XrLabel12.Name = "XrLabel12"
            Me.XrLabel12.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel12.SizeF = New System.Drawing.SizeF(800.0!, 25.0!)
            Me.XrLabel12.Text = "Balance ____________________    Account Number: _________________________  Last P" & _
        "ayment Date _________________________"
            '
            'XrLabel11
            '
            Me.XrLabel11.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 169.0833!)
            Me.XrLabel11.Name = "XrLabel11"
            Me.XrLabel11.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel11.SizeF = New System.Drawing.SizeF(800.0!, 25.0!)
            Me.XrLabel11.Text = "We will stop finance and service charges: ____________ Yes   ____________ No    I" & _
        "f not then what is the APR rate? ____________"
            '
            'XrLabel10
            '
            Me.XrLabel10.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 135.0833!)
            Me.XrLabel10.Name = "XrLabel10"
            Me.XrLabel10.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel10.SizeF = New System.Drawing.SizeF(800.0!, 25.0!)
            Me.XrLabel10.Text = "If not then please explain ______________________________________________________" & _
        "_________________________________"
            Me.XrLabel10.WordWrap = False
            '
            'XrLabel9
            '
            Me.XrLabel9.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 102.0833!)
            Me.XrLabel9.Name = "XrLabel9"
            Me.XrLabel9.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel9.SizeF = New System.Drawing.SizeF(558.0!, 25.0!)
            Me.XrLabel9.Text = "We consent to the Debt Liquidation Proposal as stated above:    __________ Yes   " & _
        " __________ No"
            '
            'XrLabel8
            '
            Me.XrLabel8.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 69.08334!)
            Me.XrLabel8.Name = "XrLabel8"
            Me.XrLabel8.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel8.SizeF = New System.Drawing.SizeF(800.0!, 25.0!)
            Me.XrLabel8.Text = "Deduct _______ % from each payment or send billing statement at ________ %  Clien" & _
        "t will receive gross amount."
            '
            'XrLabel7
            '
            Me.XrLabel7.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 35.08332!)
            Me.XrLabel7.Name = "XrLabel7"
            Me.XrLabel7.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel7.SizeF = New System.Drawing.SizeF(800.0!, 25.0!)
            Me.XrLabel7.Text = "If a new creditor with this agency, please choose deduct amount or billing cycle " & _
        "Monthly, Quarterly, Semi-Annual or Annual."
            '
            'XrLabel_message
            '
            Me.XrLabel_message.Borders = DevExpress.XtraPrinting.BorderSide.Bottom
            Me.XrLabel_message.BorderWidth = 2
            Me.XrLabel_message.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "message")})
            Me.XrLabel_message.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel_message.ForeColor = System.Drawing.Color.Gray
            Me.XrLabel_message.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
            Me.XrLabel_message.Name = "XrLabel_message"
            Me.XrLabel_message.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_message.SizeF = New System.Drawing.SizeF(800.0!, 25.0!)
            Me.XrLabel_message.StylePriority.UseBorders = False
            Me.XrLabel_message.StylePriority.UseBorderWidth = False
            Me.XrLabel_message.StylePriority.UseFont = False
            Me.XrLabel_message.StylePriority.UseForeColor = False
            Me.XrLabel_message.StylePriority.UseTextAlignment = False
            Me.XrLabel_message.Text = "message"
            Me.XrLabel_message.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
            '
            'TopMarginBand1
            '
            Me.TopMarginBand1.HeightF = 0.0!
            Me.TopMarginBand1.Name = "TopMarginBand1"
            '
            'BottomMarginBand1
            '
            Me.BottomMarginBand1.HeightF = 0.0!
            Me.BottomMarginBand1.Name = "BottomMarginBand1"
            '
            'XtraReport_GeneralInfo
            '
            Me.Bands.AddRange(New DevExpress.XtraReports.UI.Band() {Me.Detail, Me.TopMarginBand1, Me.BottomMarginBand1})
            Me.Margins = New System.Drawing.Printing.Margins(25, 25, 0, 0)
            Me.ScriptLanguage = DevExpress.XtraReports.ScriptLanguage.VisualBasic
            Me.ScriptReferencesString = resources.GetString("$this.ScriptReferencesString")
            Me.Scripts.OnBeforePrint = "XtraReport_GeneralInfo_BeforePrint"
            Me.ScriptsSource = resources.GetString("$this.ScriptsSource")
            Me.Version = "11.2"
            CType(Me, System.ComponentModel.ISupportInitialize).EndInit()

        End Sub

        Friend WithEvents Detail As DevExpress.XtraReports.UI.DetailBand
        Friend WithEvents XrLabel7 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_message As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel14 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel13 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel12 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel11 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel10 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel9 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel8 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_client As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel15 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents TopMarginBand1 As DevExpress.XtraReports.UI.TopMarginBand
        Friend WithEvents BottomMarginBand1 As DevExpress.XtraReports.UI.BottomMarginBand
    End Class
End Namespace
