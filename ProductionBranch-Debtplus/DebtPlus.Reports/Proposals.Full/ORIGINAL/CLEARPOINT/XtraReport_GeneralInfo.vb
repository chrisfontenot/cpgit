#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}
#End Region

Namespace Proposals.Full
    Public Class XtraReport_GeneralInfo

        Public Sub New()
            MyBase.New()
            InitializeComponent()
            'AddHandler Me.BeforePrint, AddressOf XtraReport_GeneralInfo_BeforePrint
            'AddHandler XrLabel_client.BeforePrint, AddressOf XrLabel_client_BeforePrint
        End Sub

        Private Sub XtraReport_GeneralInfo_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs)
            Dim rpt As DevExpress.XtraReports.UI.XtraReport = CType(sender, DevExpress.XtraReports.UI.XtraReport)
            Dim MasterRpt As DevExpress.XtraReports.UI.XtraReportBase = rpt.MasterReport
            Dim client_creditor As System.Int32 = DebtPlus.Utils.Nulls.DInt(MasterRpt.GetCurrentColumnValue("client_creditor"))

            '-- Retrieve the tables from the system
            Using bc As New DebtPlus.LINQ.BusinessContext()
                rpt.DataSource = bc.rpt_Proposal_Contribution(client_creditor)
            End Using
        End Sub

        Private Sub XrLabel_client_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs)
            With CType(sender, DevExpress.XtraReports.UI.XRLabel)
                Dim rpt As DevExpress.XtraReports.UI.XtraReport = CType(.Report, DevExpress.XtraReports.UI.XtraReport)

                Dim client As Integer = DebtPlus.Utils.Nulls.DInt(rpt.GetCurrentColumnValue("client"))
                Dim creditor As String = DebtPlus.Utils.Nulls.DStr(rpt.GetCurrentColumnValue("creditor"))
                .Text = DebtPlus.Utils.Format.Client.FormatClientID(client) + "*" + creditor
            End With
        End Sub
    End Class
End Namespace
