#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}
#End Region

Namespace Proposals.Full
    Public Class XtraReport_Employer

        Public Sub New()
            MyBase.New()
            InitializeComponent()
            'AddHandler Me.BeforePrint, AddressOf XtraReport_Employer_BeforePrint
        End Sub

        Private Sub XtraReport_Employer_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs)
            Dim rpt As DevExpress.XtraReports.UI.XtraReport = CType(sender, DevExpress.XtraReports.UI.XtraReport)
            Dim MasterRpt As DevExpress.XtraReports.UI.XtraReport = CType(rpt.MasterReport, DevExpress.XtraReports.UI.XtraReport)
            Dim sqlInfo As DebtPlus.LINQ.SQLInfoClass = DebtPlus.LINQ.SQLInfoClass.getDefault()
            Dim client As System.Int32 = Convert.ToInt32(MasterRpt.GetCurrentColumnValue("client"), System.Globalization.CultureInfo.InvariantCulture)

            Dim rd As System.Data.SqlClient.SqlDataReader = Nothing
            Dim cn As New System.Data.SqlClient.SqlConnection(sqlInfo.ConnectionString)
            Try
                cn.Open()
                Using cmd As New System.Data.SqlClient.SqlCommand()
                    cmd.Connection = cn
                    cmd.CommandText = "rpt_proposal_employer"
                    cmd.CommandType = System.Data.CommandType.StoredProcedure
                    cmd.Parameters.Add("@client", System.Data.SqlDbType.Int).Value = client
                    rd = cmd.ExecuteReader(System.Data.CommandBehavior.CloseConnection Or System.Data.CommandBehavior.SingleRow)
                End Using

                If rd IsNot Nothing AndAlso rd.Read Then

                    ' Set the employer information
                    Dim ctl As DevExpress.XtraReports.UI.XRLabel = TryCast(rpt.FindControl("XrLabel_Employer", True), DevExpress.XtraReports.UI.XRLabel)
                    If ctl IsNot Nothing Then
                        Dim StrValue As String = String.Empty
                        If Not rd.IsDBNull(rd.GetOrdinal("employer")) Then
                            StrValue = rd.GetString(rd.GetOrdinal("employer")).Trim()
                        End If
                        ctl.Text = StrValue
                    End If

                    ' And the financial problem
                    ctl = TryCast(rpt.FindControl("XrLabel_FinancialProblem", True), DevExpress.XtraReports.UI.XRLabel)
                    If ctl IsNot Nothing Then
                        Dim StrValue As String = String.Empty
                        If Not rd.IsDBNull(rd.GetOrdinal("financial_problem")) Then
                            StrValue = rd.GetString(rd.GetOrdinal("financial_problem")).Trim()
                        End If
                        ctl.Text = StrValue
                    End If
                End If

            Finally
                If rd IsNot Nothing Then
                    rd.Dispose()
                End If
                If cn IsNot Nothing Then
                    cn.Dispose()
                End If
            End Try
        End Sub
    End Class
End Namespace
