Namespace Proposals.Full
    <Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
    Partial Class XtraReport_Budget
        Inherits DevExpress.XtraReports.UI.XtraReport

        'XtraReport overrides dispose to clean up the component list.
        <System.Diagnostics.DebuggerNonUserCode()> _
        Protected Overrides Sub Dispose(ByVal disposing As Boolean)
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
            MyBase.Dispose(disposing)
        End Sub

        'Required by the Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Designer
        'It can be modified using the Designer.  
        'Do not modify it using the code editor.
        <System.Diagnostics.DebuggerStepThrough()> _
        Private Sub InitializeComponent()
            Dim XrSummary1 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary()
            Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(XtraReport_Budget))
            Me.Detail = New DevExpress.XtraReports.UI.DetailBand()
            Me.XrLabel_amount = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_description = New DevExpress.XtraReports.UI.XRLabel()
            Me.PageHeader = New DevExpress.XtraReports.UI.PageHeaderBand()
            Me.XrLabel4 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel2 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel1 = New DevExpress.XtraReports.UI.XRLabel()
            Me.ReportFooter = New DevExpress.XtraReports.UI.ReportFooterBand()
            Me.XrLabel_total_amount = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel8 = New DevExpress.XtraReports.UI.XRLabel()
            Me.TopMarginBand1 = New DevExpress.XtraReports.UI.TopMarginBand()
            Me.BottomMarginBand1 = New DevExpress.XtraReports.UI.BottomMarginBand()
            CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
            '
            'Detail
            '
            Me.Detail.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel_amount, Me.XrLabel_description})
            Me.Detail.HeightF = 16.0!
            Me.Detail.Name = "Detail"
            Me.Detail.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
            Me.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
            '
            'XrLabel_amount
            '
            Me.XrLabel_amount.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "amount", "{0:c}")})
            Me.XrLabel_amount.Font = New System.Drawing.Font("Times New Roman", 9.75!)
            Me.XrLabel_amount.LocationFloat = New DevExpress.Utils.PointFloat(525.0!, 0.0!)
            Me.XrLabel_amount.Name = "XrLabel_amount"
            Me.XrLabel_amount.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_amount.SizeF = New System.Drawing.SizeF(72.0!, 16.0!)
            Me.XrLabel_amount.StylePriority.UseFont = False
            Me.XrLabel_amount.StylePriority.UseTextAlignment = False
            Me.XrLabel_amount.Text = "$0.00"
            Me.XrLabel_amount.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
            '
            'XrLabel_description
            '
            Me.XrLabel_description.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "description")})
            Me.XrLabel_description.Font = New System.Drawing.Font("Times New Roman", 9.75!)
            Me.XrLabel_description.LocationFloat = New DevExpress.Utils.PointFloat(25.0!, 0.0!)
            Me.XrLabel_description.Name = "XrLabel_description"
            Me.XrLabel_description.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_description.SizeF = New System.Drawing.SizeF(483.0!, 16.0!)
            Me.XrLabel_description.StylePriority.UseFont = False
            Me.XrLabel_description.Text = "description"
            '
            'PageHeader
            '
            Me.PageHeader.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel4, Me.XrLabel2, Me.XrLabel1})
            Me.PageHeader.HeightF = 65.66666!
            Me.PageHeader.Name = "PageHeader"
            Me.PageHeader.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
            Me.PageHeader.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
            '
            'XrLabel4
            '
            Me.XrLabel4.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel4.LocationFloat = New DevExpress.Utils.PointFloat(467.0!, 49.66666!)
            Me.XrLabel4.Name = "XrLabel4"
            Me.XrLabel4.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel4.SizeF = New System.Drawing.SizeF(130.0!, 16.0!)
            Me.XrLabel4.StylePriority.UseFont = False
            Me.XrLabel4.StylePriority.UseTextAlignment = False
            Me.XrLabel4.Text = "AMOUNT"
            Me.XrLabel4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
            '
            'XrLabel2
            '
            Me.XrLabel2.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel2.LocationFloat = New DevExpress.Utils.PointFloat(25.0!, 49.66666!)
            Me.XrLabel2.Name = "XrLabel2"
            Me.XrLabel2.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel2.SizeF = New System.Drawing.SizeF(216.0!, 16.0!)
            Me.XrLabel2.StylePriority.UseFont = False
            Me.XrLabel2.Text = "CATEGORY DESCRIPTION"
            '
            'XrLabel1
            '
            Me.XrLabel1.Font = New System.Drawing.Font("Times New Roman", 14.0!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Underline), System.Drawing.FontStyle))
            Me.XrLabel1.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 10.00001!)
            Me.XrLabel1.Name = "XrLabel1"
            Me.XrLabel1.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel1.SizeF = New System.Drawing.SizeF(600.0!, 25.0!)
            Me.XrLabel1.StylePriority.UseFont = False
            Me.XrLabel1.StylePriority.UseTextAlignment = False
            Me.XrLabel1.Text = "MONTHLY BUDGET INFORMATION"
            Me.XrLabel1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter
            '
            'ReportFooter
            '
            Me.ReportFooter.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel_total_amount, Me.XrLabel8})
            Me.ReportFooter.HeightF = 16.0!
            Me.ReportFooter.Name = "ReportFooter"
            '
            'XrLabel_total_amount
            '
            Me.XrLabel_total_amount.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "amount")})
            Me.XrLabel_total_amount.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel_total_amount.LocationFloat = New DevExpress.Utils.PointFloat(483.0!, 0.0!)
            Me.XrLabel_total_amount.Name = "XrLabel_total_amount"
            Me.XrLabel_total_amount.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_total_amount.SizeF = New System.Drawing.SizeF(114.0!, 16.0!)
            Me.XrLabel_total_amount.StylePriority.UseFont = False
            Me.XrLabel_total_amount.StylePriority.UseTextAlignment = False
            XrSummary1.FormatString = "{0:c}"
            XrSummary1.Running = DevExpress.XtraReports.UI.SummaryRunning.Report
            Me.XrLabel_total_amount.Summary = XrSummary1
            Me.XrLabel_total_amount.Text = "$0.00"
            Me.XrLabel_total_amount.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
            '
            'XrLabel8
            '
            Me.XrLabel8.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel8.LocationFloat = New DevExpress.Utils.PointFloat(25.0!, 0.0!)
            Me.XrLabel8.Name = "XrLabel8"
            Me.XrLabel8.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel8.SizeF = New System.Drawing.SizeF(216.0!, 16.0!)
            Me.XrLabel8.StylePriority.UseFont = False
            Me.XrLabel8.Text = "MONTHLY TOTAL"
            '
            'TopMarginBand1
            '
            Me.TopMarginBand1.HeightF = 0.0!
            Me.TopMarginBand1.Name = "TopMarginBand1"
            '
            'BottomMarginBand1
            '
            Me.BottomMarginBand1.HeightF = 0.0!
            Me.BottomMarginBand1.Name = "BottomMarginBand1"
            '
            'XtraReport_Budget
            '
            Me.Bands.AddRange(New DevExpress.XtraReports.UI.Band() {Me.Detail, Me.PageHeader, Me.ReportFooter, Me.TopMarginBand1, Me.BottomMarginBand1})
            Me.Margins = New System.Drawing.Printing.Margins(100, 100, 0, 0)
            Me.ScriptLanguage = DevExpress.XtraReports.ScriptLanguage.VisualBasic
            Me.ScriptReferencesString = resources.GetString("$this.ScriptReferencesString")
            Me.Scripts.OnBeforePrint = "XtraReport_Budget_BeforePrint"
            Me.ScriptsSource = resources.GetString("$this.ScriptsSource")
            Me.Version = "11.2"
            CType(Me, System.ComponentModel.ISupportInitialize).EndInit()

        End Sub

        Friend WithEvents Detail As DevExpress.XtraReports.UI.DetailBand
        Friend WithEvents PageHeader As DevExpress.XtraReports.UI.PageHeaderBand
        Friend WithEvents ReportFooter As DevExpress.XtraReports.UI.ReportFooterBand
        Friend WithEvents XrLabel4 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel2 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel1 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_amount As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_description As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_total_amount As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel8 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents TopMarginBand1 As DevExpress.XtraReports.UI.TopMarginBand
        Friend WithEvents BottomMarginBand1 As DevExpress.XtraReports.UI.BottomMarginBand
    End Class
End Namespace
