#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}
#End Region

Namespace Proposals.Full
    Public Class XtraReport_Summary

        Public Sub New()
            MyBase.New()
            InitializeComponent()
            'AddHandler Me.BeforePrint, AddressOf XtraReport_Summary_BeforePrint
        End Sub

        Private Sub XtraReport_Summary_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs)
            Dim rpt As DevExpress.XtraReports.UI.XtraReport = CType(sender, DevExpress.XtraReports.UI.XtraReport)
            Dim MasterRpt As DevExpress.XtraReports.UI.XtraReportBase = rpt.MasterReport
            Dim sqlInfo As DebtPlus.LINQ.SQLInfoClass = DebtPlus.LINQ.SQLInfoClass.getDefault()
            Dim client_creditor_proposal As System.Int32 = Convert.ToInt32(MasterRpt.GetCurrentColumnValue("client_creditor_proposal"), System.Globalization.CultureInfo.InvariantCulture)

            Dim rd As System.Data.SqlClient.SqlDataReader = Nothing
            Dim cn As New System.Data.SqlClient.SqlConnection(sqlInfo.ConnectionString)
            Try
                cn.Open()

                Using cmd As New System.Data.SqlClient.SqlCommand()
                    cmd.Connection = cn
                    cmd.CommandText = "rpt_proposal_summary"
                    cmd.CommandType = System.Data.CommandType.StoredProcedure
                    cmd.Parameters.Add("@client_creditor_proposal", System.Data.SqlDbType.Int).Value = client_creditor_proposal
                    rd = cmd.ExecuteReader(System.Data.CommandBehavior.CloseConnection Or System.Data.CommandBehavior.SingleRow)
                End Using

                If rd IsNot Nothing AndAlso rd.Read Then
                    Dim ctl As DevExpress.XtraReports.UI.XRTableCell

                    For Field As Integer = 0 To rd.FieldCount - 1
                        If Not rd.IsDBNull(Field) Then
                            Select Case rd.GetName(Field).ToLower
                                Case "balance_owed"
                                    ctl = TryCast(rpt.FindControl("XrTableCell_balance_owed", True), DevExpress.XtraReports.UI.XRTableCell)
                                    If ctl IsNot Nothing Then ctl.Text = String.Format("{0:c}", Convert.ToDecimal(rd.GetValue(Field), System.Globalization.CultureInfo.InvariantCulture))

                                Case "proposed_payment"
                                    ctl = TryCast(rpt.FindControl("XrTableCell_proposed_payment", True), DevExpress.XtraReports.UI.XRTableCell)
                                    If ctl IsNot Nothing Then ctl.Text = String.Format("{0:c}", Convert.ToDecimal(rd.GetValue(Field), System.Globalization.CultureInfo.InvariantCulture))

                                Case "start_date"
                                    ctl = TryCast(rpt.FindControl("XrTableCell_start_date", True), DevExpress.XtraReports.UI.XRTableCell)
                                    If ctl IsNot Nothing Then ctl.Text = String.Format("{0:d}", Convert.ToDateTime(rd.GetValue(Field), System.Globalization.CultureInfo.InvariantCulture))

                                Case "net_income"
                                    ctl = TryCast(rpt.FindControl("XrTableCell_net_income", True), DevExpress.XtraReports.UI.XRTableCell)
                                    If ctl IsNot Nothing Then ctl.Text = String.Format("{0:c}", Convert.ToDecimal(rd.GetValue(Field), System.Globalization.CultureInfo.InvariantCulture))

                                Case "living_expense"
                                    ctl = TryCast(rpt.FindControl("XrTableCell_living_expense", True), DevExpress.XtraReports.UI.XRTableCell)
                                    If ctl IsNot Nothing Then ctl.Text = String.Format("{0:c}", Convert.ToDecimal(rd.GetValue(Field), System.Globalization.CultureInfo.InvariantCulture))

                                Case "housing_expense"
                                    ctl = TryCast(rpt.FindControl("XrTableCell_housing_expense", True), DevExpress.XtraReports.UI.XRTableCell)
                                    If ctl IsNot Nothing Then ctl.Text = String.Format("{0:c}", Convert.ToDecimal(rd.GetValue(Field), System.Globalization.CultureInfo.InvariantCulture))

                                Case "people_in_household"
                                    ctl = TryCast(rpt.FindControl("XrTableCell_people_in_household", True), DevExpress.XtraReports.UI.XRTableCell)
                                    If ctl IsNot Nothing Then ctl.Text = String.Format("{0:f0}", Convert.ToInt32(rd.GetValue(Field), System.Globalization.CultureInfo.InvariantCulture))
                            End Select
                        End If
                    Next
                End If

            Finally
                If rd IsNot Nothing Then
                    rd.Dispose()
                End If
                If cn IsNot Nothing Then
                    cn.Dispose()
                End If
            End Try
        End Sub

    End Class
End Namespace
