#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}
#End Region

Namespace Proposals.Full
    Public Class XtraReport_Budget

        Public Sub New()
            MyBase.New()
            InitializeComponent()
            'AddHandler Me.BeforePrint, AddressOf XtraReport_Budget_BeforePrint
        End Sub

        Private Sub XtraReport_Budget_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs)
            Const TableName As String = "rpt_BudgetSummary"
            Dim rpt As DevExpress.XtraReports.UI.XtraReport = CType(sender, DevExpress.XtraReports.UI.XtraReport)
            Dim MasterRpt As DevExpress.XtraReports.UI.XtraReportBase = rpt.MasterReport
            Dim sqlInfo As DebtPlus.LINQ.SQLInfoClass = DebtPlus.LINQ.SQLInfoClass.getDefault()

            Dim client As System.Int32 = DebtPlus.Utils.Nulls.DInt(MasterRpt.GetCurrentColumnValue("client"))
            Dim proposal_budget_info As Boolean = DebtPlus.Utils.Nulls.DBool(MasterRpt.GetCurrentColumnValue("proposal_budget_info"))
            If proposal_budget_info AndAlso client >= 0 Then
                Dim ds As New System.Data.DataSet("ds")

                Using cmd As New System.Data.SqlClient.SqlCommand()
                    cmd.Connection = New System.Data.SqlClient.SqlConnection(sqlInfo.ConnectionString)
                    cmd.CommandType = System.Data.CommandType.StoredProcedure
                    cmd.CommandText = "rpt_BudgetSummary"
                    cmd.CommandTimeout = 0
                    cmd.Parameters.Add("@client", System.Data.SqlDbType.Int).Value = client

                    Using da As New System.Data.SqlClient.SqlDataAdapter(cmd)
                        da.Fill(ds, TableName)
                    End Using
                End Using

                Dim tbl As System.Data.DataTable = ds.Tables(TableName)
                If tbl.Rows.Count > 0 Then
                    rpt.DataSource = tbl.DefaultView
                    Return
                End If
            End If

            e.Cancel = True
        End Sub
    End Class
End Namespace