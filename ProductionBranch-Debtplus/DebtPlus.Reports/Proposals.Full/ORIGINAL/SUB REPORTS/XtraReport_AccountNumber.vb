#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}
#End Region

Namespace Proposals.Full
    Public Class XtraReport_AccountNumber

        Public Sub New()
            MyBase.New()
            InitializeComponent()
            'AddHandler Me.BeforePrint, AddressOf XtraReport_AccountNumber_BeforePrint
        End Sub

        Private Sub XtraReport_AccountNumber_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs)
            Dim rpt As DevExpress.XtraReports.UI.XtraReport = CType(sender, DevExpress.XtraReports.UI.XtraReport)
            Dim MasterRpt As DevExpress.XtraReports.UI.XtraReportBase = rpt.MasterReport
            Dim client_creditor As System.Int32 = Convert.ToInt32(MasterRpt.GetCurrentColumnValue("client_creditor"), System.Globalization.CultureInfo.InvariantCulture)

            Using bc As New DebtPlus.LINQ.BusinessContext()
                Dim col As System.Collections.Generic.List(Of DebtPlus.LINQ.rpt_Proposal_AccountNumberResult) = bc.rpt_Proposal_AccountNumber(client_creditor)
                If col IsNot Nothing AndAlso col.Count > 0 Then
                    Dim rec As DebtPlus.LINQ.rpt_Proposal_AccountNumberResult = col(0)

                    Dim ctl As DevExpress.XtraReports.UI.XRLabel = TryCast(rpt.FindControl("XrLabel_AccountNumber", True), DevExpress.XtraReports.UI.XRLabel)
                    If ctl IsNot Nothing Then
                        ctl.Text = rec.account_number
                    End If

                    ' Set the social security number
                    ctl = TryCast(rpt.FindControl("XrLabel_SocialSecurityNumber", True), DevExpress.XtraReports.UI.XRLabel)
                    If ctl IsNot Nothing Then
                        ctl.Text = DebtPlus.Utils.Format.SSN.FormatSSN(rec.ssn)
                    End If
                End If
            End Using
        End Sub
    End Class
End Namespace
