Namespace Proposals.Full
    <Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
    Partial Class XtraReport_CreditorList
        Inherits DevExpress.XtraReports.UI.XtraReport

        'XtraReport overrides dispose to clean up the component list.
        <System.Diagnostics.DebuggerNonUserCode()> _
        Protected Overrides Sub Dispose(ByVal disposing As Boolean)
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
            MyBase.Dispose(disposing)
        End Sub

        'Required by the Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Designer
        'It can be modified using the Designer.  
        'Do not modify it using the code editor.
        <System.Diagnostics.DebuggerStepThrough()> _
        Private Sub InitializeComponent()
            Dim XrSummary1 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary()
            Dim XrSummary2 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary()
            Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(XtraReport_CreditorList))
            Me.Detail = New DevExpress.XtraReports.UI.DetailBand()
            Me.XrLabel_marker = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_balance = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_disbursement_factor = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_creditor_name = New DevExpress.XtraReports.UI.XRLabel()
            Me.PageHeader = New DevExpress.XtraReports.UI.PageHeaderBand()
            Me.XrLabel4 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel3 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel2 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel1 = New DevExpress.XtraReports.UI.XRLabel()
            Me.ReportFooter = New DevExpress.XtraReports.UI.ReportFooterBand()
            Me.XrLabel_total_balance = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_total_disbursement_factor = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel8 = New DevExpress.XtraReports.UI.XRLabel()
            Me.TopMarginBand1 = New DevExpress.XtraReports.UI.TopMarginBand()
            Me.BottomMarginBand1 = New DevExpress.XtraReports.UI.BottomMarginBand()
            CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
            '
            'Detail
            '
            Me.Detail.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel_marker, Me.XrLabel_balance, Me.XrLabel_disbursement_factor, Me.XrLabel_creditor_name})
            Me.Detail.HeightF = 16.0!
            Me.Detail.Name = "Detail"
            Me.Detail.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
            Me.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
            '
            'XrLabel_marker
            '
            Me.XrLabel_marker.CanGrow = False
            Me.XrLabel_marker.Font = New System.Drawing.Font("Wingdings", 12.0!, System.Drawing.FontStyle.Bold)
            Me.XrLabel_marker.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
            Me.XrLabel_marker.Name = "XrLabel_marker"
            Me.XrLabel_marker.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_marker.Scripts.OnBeforePrint = "XrLabel_marker_BeforePrint"
            Me.XrLabel_marker.SizeF = New System.Drawing.SizeF(25.0!, 16.0!)
            Me.XrLabel_marker.StylePriority.UseFont = False
            '
            'XrLabel_balance
            '
            Me.XrLabel_balance.CanGrow = False
            Me.XrLabel_balance.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "balance", "{0:c}")})
            Me.XrLabel_balance.Font = New System.Drawing.Font("Times New Roman", 9.75!)
            Me.XrLabel_balance.LocationFloat = New DevExpress.Utils.PointFloat(525.0!, 0.0!)
            Me.XrLabel_balance.Name = "XrLabel_balance"
            Me.XrLabel_balance.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_balance.SizeF = New System.Drawing.SizeF(72.0!, 16.0!)
            Me.XrLabel_balance.StylePriority.UseFont = False
            Me.XrLabel_balance.StylePriority.UseTextAlignment = False
            Me.XrLabel_balance.Text = "$0.00"
            Me.XrLabel_balance.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
            '
            'XrLabel_disbursement_factor
            '
            Me.XrLabel_disbursement_factor.CanGrow = False
            Me.XrLabel_disbursement_factor.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "disbursement_factor", "{0:c}")})
            Me.XrLabel_disbursement_factor.Font = New System.Drawing.Font("Times New Roman", 9.75!)
            Me.XrLabel_disbursement_factor.LocationFloat = New DevExpress.Utils.PointFloat(375.0!, 0.0!)
            Me.XrLabel_disbursement_factor.Name = "XrLabel_disbursement_factor"
            Me.XrLabel_disbursement_factor.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_disbursement_factor.SizeF = New System.Drawing.SizeF(100.0!, 16.0!)
            Me.XrLabel_disbursement_factor.StylePriority.UseFont = False
            Me.XrLabel_disbursement_factor.StylePriority.UseTextAlignment = False
            Me.XrLabel_disbursement_factor.Text = "$0.00"
            Me.XrLabel_disbursement_factor.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
            '
            'XrLabel_creditor_name
            '
            Me.XrLabel_creditor_name.CanGrow = False
            Me.XrLabel_creditor_name.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "creditor_name")})
            Me.XrLabel_creditor_name.Font = New System.Drawing.Font("Times New Roman", 9.75!)
            Me.XrLabel_creditor_name.LocationFloat = New DevExpress.Utils.PointFloat(25.0!, 0.0!)
            Me.XrLabel_creditor_name.Name = "XrLabel_creditor_name"
            Me.XrLabel_creditor_name.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_creditor_name.SizeF = New System.Drawing.SizeF(342.0!, 16.0!)
            Me.XrLabel_creditor_name.StylePriority.UseFont = False
            Me.XrLabel_creditor_name.Text = "CREDITOR NAME"
            Me.XrLabel_creditor_name.WordWrap = False
            '
            'PageHeader
            '
            Me.PageHeader.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel4, Me.XrLabel3, Me.XrLabel2, Me.XrLabel1})
            Me.PageHeader.HeightF = 75.0!
            Me.PageHeader.Name = "PageHeader"
            Me.PageHeader.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
            Me.PageHeader.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
            '
            'XrLabel4
            '
            Me.XrLabel4.CanGrow = False
            Me.XrLabel4.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel4.LocationFloat = New DevExpress.Utils.PointFloat(527.0!, 58.0!)
            Me.XrLabel4.Name = "XrLabel4"
            Me.XrLabel4.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel4.SizeF = New System.Drawing.SizeF(72.0!, 16.0!)
            Me.XrLabel4.StylePriority.UseFont = False
            Me.XrLabel4.StylePriority.UseTextAlignment = False
            Me.XrLabel4.Text = "BALANCE"
            Me.XrLabel4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
            '
            'XrLabel3
            '
            Me.XrLabel3.CanGrow = False
            Me.XrLabel3.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel3.LocationFloat = New DevExpress.Utils.PointFloat(375.0!, 58.0!)
            Me.XrLabel3.Name = "XrLabel3"
            Me.XrLabel3.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel3.SizeF = New System.Drawing.SizeF(100.0!, 16.0!)
            Me.XrLabel3.StylePriority.UseFont = False
            Me.XrLabel3.StylePriority.UseTextAlignment = False
            Me.XrLabel3.Text = "PAYMENT"
            Me.XrLabel3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
            '
            'XrLabel2
            '
            Me.XrLabel2.CanGrow = False
            Me.XrLabel2.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel2.LocationFloat = New DevExpress.Utils.PointFloat(25.0!, 58.0!)
            Me.XrLabel2.Name = "XrLabel2"
            Me.XrLabel2.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel2.SizeF = New System.Drawing.SizeF(216.0!, 16.0!)
            Me.XrLabel2.StylePriority.UseFont = False
            Me.XrLabel2.Text = "CREDITOR TYPE OR NAME"
            '
            'XrLabel1
            '
            Me.XrLabel1.CanGrow = False
            Me.XrLabel1.Font = New System.Drawing.Font("Times New Roman", 14.0!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Underline), System.Drawing.FontStyle))
            Me.XrLabel1.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 17.0!)
            Me.XrLabel1.Name = "XrLabel1"
            Me.XrLabel1.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel1.SizeF = New System.Drawing.SizeF(600.0!, 25.0!)
            Me.XrLabel1.StylePriority.UseFont = False
            Me.XrLabel1.StylePriority.UseTextAlignment = False
            Me.XrLabel1.Text = "PROPOSED DEBT LIQUIDATION PLAN"
            Me.XrLabel1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter
            '
            'ReportFooter
            '
            Me.ReportFooter.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel_total_balance, Me.XrLabel_total_disbursement_factor, Me.XrLabel8})
            Me.ReportFooter.HeightF = 16.0!
            Me.ReportFooter.Name = "ReportFooter"
            '
            'XrLabel_total_balance
            '
            Me.XrLabel_total_balance.CanGrow = False
            Me.XrLabel_total_balance.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "balance")})
            Me.XrLabel_total_balance.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel_total_balance.LocationFloat = New DevExpress.Utils.PointFloat(483.0!, 0.0!)
            Me.XrLabel_total_balance.Name = "XrLabel_total_balance"
            Me.XrLabel_total_balance.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_total_balance.SizeF = New System.Drawing.SizeF(114.0!, 16.0!)
            Me.XrLabel_total_balance.StylePriority.UseFont = False
            Me.XrLabel_total_balance.StylePriority.UseTextAlignment = False
            XrSummary1.FormatString = "{0:c}"
            XrSummary1.Running = DevExpress.XtraReports.UI.SummaryRunning.Report
            Me.XrLabel_total_balance.Summary = XrSummary1
            Me.XrLabel_total_balance.Text = "$0.00"
            Me.XrLabel_total_balance.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
            '
            'XrLabel_total_disbursement_factor
            '
            Me.XrLabel_total_disbursement_factor.CanGrow = False
            Me.XrLabel_total_disbursement_factor.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "disbursement_factor")})
            Me.XrLabel_total_disbursement_factor.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel_total_disbursement_factor.LocationFloat = New DevExpress.Utils.PointFloat(317.0!, 0.0!)
            Me.XrLabel_total_disbursement_factor.Name = "XrLabel_total_disbursement_factor"
            Me.XrLabel_total_disbursement_factor.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_total_disbursement_factor.SizeF = New System.Drawing.SizeF(158.0!, 16.0!)
            Me.XrLabel_total_disbursement_factor.StylePriority.UseFont = False
            Me.XrLabel_total_disbursement_factor.StylePriority.UseTextAlignment = False
            XrSummary2.FormatString = "{0:c}"
            XrSummary2.Running = DevExpress.XtraReports.UI.SummaryRunning.Report
            Me.XrLabel_total_disbursement_factor.Summary = XrSummary2
            Me.XrLabel_total_disbursement_factor.Text = "$0.00"
            Me.XrLabel_total_disbursement_factor.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
            '
            'XrLabel8
            '
            Me.XrLabel8.CanGrow = False
            Me.XrLabel8.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel8.LocationFloat = New DevExpress.Utils.PointFloat(25.0!, 0.0!)
            Me.XrLabel8.Name = "XrLabel8"
            Me.XrLabel8.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel8.SizeF = New System.Drawing.SizeF(216.0!, 16.0!)
            Me.XrLabel8.StylePriority.UseFont = False
            Me.XrLabel8.Text = "TOTALS"
            '
            'TopMarginBand1
            '
            Me.TopMarginBand1.HeightF = 0.0!
            Me.TopMarginBand1.Name = "TopMarginBand1"
            '
            'BottomMarginBand1
            '
            Me.BottomMarginBand1.HeightF = 0.0!
            Me.BottomMarginBand1.Name = "BottomMarginBand1"
            '
            'XtraReport_CreditorList
            '
            Me.Bands.AddRange(New DevExpress.XtraReports.UI.Band() {Me.Detail, Me.PageHeader, Me.ReportFooter, Me.TopMarginBand1, Me.BottomMarginBand1})
            Me.Margins = New System.Drawing.Printing.Margins(25, 25, 0, 0)
            Me.ScriptLanguage = DevExpress.XtraReports.ScriptLanguage.VisualBasic
            Me.ScriptReferencesString = resources.GetString("$this.ScriptReferencesString")
            Me.Scripts.OnBeforePrint = "XtraReport_CreditorList_BeforePrint"
            Me.ScriptsSource = resources.GetString("$this.ScriptsSource")
            Me.Version = "11.2"
            CType(Me, System.ComponentModel.ISupportInitialize).EndInit()

        End Sub
        Friend WithEvents Detail As DevExpress.XtraReports.UI.DetailBand
        Friend WithEvents PageHeader As DevExpress.XtraReports.UI.PageHeaderBand
        Friend WithEvents XrLabel1 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel4 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel3 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel2 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_marker As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_balance As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_disbursement_factor As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_creditor_name As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents ReportFooter As DevExpress.XtraReports.UI.ReportFooterBand
        Friend WithEvents XrLabel_total_balance As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_total_disbursement_factor As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel8 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents TopMarginBand1 As DevExpress.XtraReports.UI.TopMarginBand
        Friend WithEvents BottomMarginBand1 As DevExpress.XtraReports.UI.BottomMarginBand
    End Class
End Namespace
