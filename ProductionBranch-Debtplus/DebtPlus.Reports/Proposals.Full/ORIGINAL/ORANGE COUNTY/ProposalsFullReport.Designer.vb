﻿Namespace Proposals.Full
    <Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
    Partial Public Class ProposalsFullReport
        Inherits DebtPlus.Reports.Template.BaseXtraReportClass

        'XtraReport overrides dispose to clean up the component list.
        <System.Diagnostics.DebuggerNonUserCode()> _
        Protected Overrides Sub Dispose(ByVal disposing As Boolean)
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
            MyBase.Dispose(disposing)
        End Sub

        'Required by the Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Designer
        'It can be modified using the Designer.  
        'Do not modify it using the code editor.
        <System.Diagnostics.DebuggerStepThrough()> _
        Private Sub InitializeComponent()
            Dim PostNetGenerator1 As DevExpress.XtraPrinting.BarCode.PostNetGenerator = New DevExpress.XtraPrinting.BarCode.PostNetGenerator()
            Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(ProposalsFullReport))
            Me.GroupFooter1 = New DevExpress.XtraReports.UI.GroupFooterBand()
            Me.XrSubreport_GeneralInfo = New DevExpress.XtraReports.UI.XRSubreport()
            Me.ParameterProposal = New DevExpress.XtraReports.Parameters.Parameter()
            Me.XrBarCode_PostalCode = New DevExpress.XtraReports.UI.XRBarCode()
            Me.ParameterBatch = New DevExpress.XtraReports.Parameters.Parameter()
            Me.PageHeader = New DevExpress.XtraReports.UI.PageHeaderBand()
            Me.XrRichText1 = New DevExpress.XtraReports.UI.XRRichText()
            Me.XrLabel_print_date = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel4 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel3 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrSubreport_Employer = New DevExpress.XtraReports.UI.XRSubreport()
            Me.XrControlStyle1 = New DevExpress.XtraReports.UI.XRControlStyle()
            Me.XrLabel_message = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrPanel_CreditorAddress = New DevExpress.XtraReports.UI.XRPanel()
            Me.XrLabel_Creditor_Address = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_Creditor_L = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel1 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrSubreport_AccountNumber = New DevExpress.XtraReports.UI.XRSubreport()
            Me.XrSubreport_CreditorList = New DevExpress.XtraReports.UI.XRSubreport()
            Me.XrSubreport_Secured = New DevExpress.XtraReports.UI.XRSubreport()
            Me.XrLabel_ClientNameAddress = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrSubreport_Summary = New DevExpress.XtraReports.UI.XRSubreport()
            Me.XrSubreport_Budget = New DevExpress.XtraReports.UI.XRSubreport()
            Me.GroupHeader1 = New DevExpress.XtraReports.UI.GroupHeaderBand()
            Me.XtraReport_CreditorList1 = New DebtPlus.Reports.Proposals.Full.XtraReport_CreditorList()
            Me.XtraReport_Budget1 = New DebtPlus.Reports.Proposals.Full.XtraReport_Budget()
            Me.XtraReport_Secured1 = New DebtPlus.Reports.Proposals.Full.XtraReport_Secured()
            Me.XtraReport_GeneralInfo1 = New DebtPlus.Reports.Proposals.Full.XtraReport_GeneralInfo()
            Me.XtraReport_Employer1 = New DebtPlus.Reports.Proposals.Full.XtraReport_Employer()
            Me.XtraReport_AccountNumber1 = New DebtPlus.Reports.Proposals.Full.XtraReport_AccountNumber()
            Me.XtraReport_Summary1 = New DebtPlus.Reports.Proposals.Full.XtraReport_Summary()
            CType(Me.XrRichText1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.XtraReport_CreditorList1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.XtraReport_Budget1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.XtraReport_Secured1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.XtraReport_GeneralInfo1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.XtraReport_Employer1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.XtraReport_AccountNumber1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.XtraReport_Summary1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
            '
            'Detail
            '
            Me.Detail.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrSubreport_Secured, Me.XrSubreport_Budget, Me.XrSubreport_CreditorList, Me.XrLabel_message})
            Me.Detail.HeightF = 137.5!
            '
            'GroupFooter1
            '
            Me.GroupFooter1.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrSubreport_GeneralInfo})
            Me.GroupFooter1.HeightF = 25.0!
            Me.GroupFooter1.KeepTogether = True
            Me.GroupFooter1.Name = "GroupFooter1"
            Me.GroupFooter1.PageBreak = DevExpress.XtraReports.UI.PageBreak.AfterBand
            Me.GroupFooter1.PrintAtBottom = True
            '
            'XrSubreport_GeneralInfo
            '
            Me.XrSubreport_GeneralInfo.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
            Me.XrSubreport_GeneralInfo.Name = "XrSubreport_GeneralInfo"
            Me.XrSubreport_GeneralInfo.ReportSource = Me.XtraReport_GeneralInfo1
            Me.XrSubreport_GeneralInfo.SizeF = New System.Drawing.SizeF(800.0!, 25.0!)
            '
            'ParameterProposal
            '
            Me.ParameterProposal.Description = "Proposal ID"
            Me.ParameterProposal.Name = "ParameterProposal"
            Me.ParameterProposal.Type = GetType(Integer)
            Me.ParameterProposal.Value = 0
            Me.ParameterProposal.Visible = False
            '
            'XrBarCode_PostalCode
            '
            Me.XrBarCode_PostalCode.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
            Me.XrBarCode_PostalCode.Name = "XrBarCode_PostalCode"
            Me.XrBarCode_PostalCode.Padding = New DevExpress.XtraPrinting.PaddingInfo(10, 10, 0, 0, 100.0!)
            Me.XrBarCode_PostalCode.ShowText = False
            Me.XrBarCode_PostalCode.SizeF = New System.Drawing.SizeF(300.0!, 16.0!)
            Me.XrBarCode_PostalCode.Symbology = PostNetGenerator1
            Me.XrBarCode_PostalCode.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
            '
            'ParameterBatch
            '
            Me.ParameterBatch.Description = "Proposal Batch ID"
            Me.ParameterBatch.Name = "ParameterBatch"
            Me.ParameterBatch.Type = GetType(Integer)
            Me.ParameterBatch.Value = 0
            '
            'PageHeader
            '
            Me.PageHeader.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrRichText1, Me.XrLabel_print_date, Me.XrLabel4})
            Me.PageHeader.HeightF = 169.5!
            Me.PageHeader.Name = "PageHeader"
            '
            'XrRichText1
            '
            Me.XrRichText1.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
            Me.XrRichText1.Name = "XrRichText1"
            Me.XrRichText1.SerializableRtfString = resources.GetString("XrRichText1.SerializableRtfString")
            Me.XrRichText1.SizeF = New System.Drawing.SizeF(800.0!, 98.0!)
            '
            'XrLabel_print_date
            '
            Me.XrLabel_print_date.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "print_date", "{0:MMMM d, yyyy}")})
            Me.XrLabel_print_date.LocationFloat = New DevExpress.Utils.PointFloat(22.20834!, 110.875!)
            Me.XrLabel_print_date.Name = "XrLabel_print_date"
            Me.XrLabel_print_date.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_print_date.SizeF = New System.Drawing.SizeF(183.0!, 17.0!)
            Me.XrLabel_print_date.Text = "Feburary 30, 2009"
            '
            'XrLabel4
            '
            Me.XrLabel4.LocationFloat = New DevExpress.Utils.PointFloat(22.20834!, 127.875!)
            Me.XrLabel4.Multiline = True
            Me.XrLabel4.Name = "XrLabel4"
            Me.XrLabel4.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel4.SizeF = New System.Drawing.SizeF(183.0!, 33.0!)
            Me.XrLabel4.Text = "NFCC Unit # 14014" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Federal Tax ID# 95-2426981"
            '
            'XrLabel3
            '
            Me.XrLabel3.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
            Me.XrLabel3.Name = "XrLabel3"
            Me.XrLabel3.SizeF = New System.Drawing.SizeF(100.0!, 23.0!)
            '
            'XrSubreport_Employer
            '
            Me.XrSubreport_Employer.LocationFloat = New DevExpress.Utils.PointFloat(10.0!, 331.7917!)
            Me.XrSubreport_Employer.Name = "XrSubreport_Employer"
            Me.XrSubreport_Employer.ReportSource = Me.XtraReport_Employer1
            Me.XrSubreport_Employer.SizeF = New System.Drawing.SizeF(400.0!, 34.0!)
            '
            'XrControlStyle1
            '
            Me.XrControlStyle1.BackColor = System.Drawing.Color.Teal
            Me.XrControlStyle1.BorderColor = System.Drawing.Color.Teal
            Me.XrControlStyle1.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.XrControlStyle1.ForeColor = System.Drawing.Color.White
            Me.XrControlStyle1.Name = "XrControlStyle1"
            Me.XrControlStyle1.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
            Me.XrControlStyle1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            '
            'XrLabel_message
            '
            Me.XrLabel_message.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "message")})
            Me.XrLabel_message.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel_message.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
            Me.XrLabel_message.Name = "XrLabel_message"
            Me.XrLabel_message.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_message.Scripts.OnPrintOnPage = "XrLabel_message_PrintOnPage"
            Me.XrLabel_message.SizeF = New System.Drawing.SizeF(800.0!, 25.0!)
            Me.XrLabel_message.StylePriority.UseFont = False
            '
            'XrPanel_CreditorAddress
            '
            Me.XrPanel_CreditorAddress.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel_Creditor_Address, Me.XrBarCode_PostalCode})
            Me.XrPanel_CreditorAddress.LocationFloat = New DevExpress.Utils.PointFloat(75.0!, 120.0!)
            Me.XrPanel_CreditorAddress.Name = "XrPanel_CreditorAddress"
            Me.XrPanel_CreditorAddress.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
            Me.XrPanel_CreditorAddress.Scripts.OnBeforePrint = "XrPanel_CreditorAddress_BeforePrint"
            Me.XrPanel_CreditorAddress.SizeF = New System.Drawing.SizeF(300.0!, 117.0!)
            '
            'XrLabel_Creditor_Address
            '
            Me.XrLabel_Creditor_Address.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 17.0!)
            Me.XrLabel_Creditor_Address.Multiline = True
            Me.XrLabel_Creditor_Address.Name = "XrLabel_Creditor_Address"
            Me.XrLabel_Creditor_Address.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_Creditor_Address.SizeF = New System.Drawing.SizeF(300.0!, 100.0!)
            Me.XrLabel_Creditor_Address.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
            '
            'XrLabel_Creditor_L
            '
            Me.XrLabel_Creditor_L.LocationFloat = New DevExpress.Utils.PointFloat(8.0!, 158.0!)
            Me.XrLabel_Creditor_L.Multiline = True
            Me.XrLabel_Creditor_L.Name = "XrLabel_Creditor_L"
            Me.XrLabel_Creditor_L.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_Creditor_L.SizeF = New System.Drawing.SizeF(308.0!, 100.0!)
            Me.XrLabel_Creditor_L.Text = "XrLabel_Creditor_L"
            '
            'XrLabel1
            '
            Me.XrLabel1.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
            Me.XrLabel1.Name = "XrLabel1"
            Me.XrLabel1.SizeF = New System.Drawing.SizeF(100.0!, 23.0!)
            '
            'XrSubreport_AccountNumber
            '
            Me.XrSubreport_AccountNumber.LocationFloat = New DevExpress.Utils.PointFloat(10.0!, 269.2917!)
            Me.XrSubreport_AccountNumber.Name = "XrSubreport_AccountNumber"
            Me.XrSubreport_AccountNumber.ReportSource = Me.XtraReport_AccountNumber1
            Me.XrSubreport_AccountNumber.SizeF = New System.Drawing.SizeF(400.0!, 34.0!)
            '
            'XrSubreport_CreditorList
            '
            Me.XrSubreport_CreditorList.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 37.5!)
            Me.XrSubreport_CreditorList.Name = "XrSubreport_CreditorList"
            Me.XrSubreport_CreditorList.ReportSource = Me.XtraReport_CreditorList1
            Me.XrSubreport_CreditorList.SizeF = New System.Drawing.SizeF(800.0!, 25.0!)
            '
            'XrSubreport_Secured
            '
            Me.XrSubreport_Secured.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 112.5!)
            Me.XrSubreport_Secured.Name = "XrSubreport_Secured"
            Me.XrSubreport_Secured.ReportSource = Me.XtraReport_Secured1
            Me.XrSubreport_Secured.SizeF = New System.Drawing.SizeF(800.0!, 25.0!)
            '
            'XrLabel_ClientNameAddress
            '
            Me.XrLabel_ClientNameAddress.LocationFloat = New DevExpress.Utils.PointFloat(500.0!, 120.0!)
            Me.XrLabel_ClientNameAddress.Multiline = True
            Me.XrLabel_ClientNameAddress.Name = "XrLabel_ClientNameAddress"
            Me.XrLabel_ClientNameAddress.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_ClientNameAddress.Scripts.OnBeforePrint = "XrLabel_ClientNameAddress_BeforePrint"
            Me.XrLabel_ClientNameAddress.SizeF = New System.Drawing.SizeF(283.0!, 92.0!)
            '
            'XrSubreport_Summary
            '
            Me.XrSubreport_Summary.LocationFloat = New DevExpress.Utils.PointFloat(500.0!, 269.2917!)
            Me.XrSubreport_Summary.Name = "XrSubreport_Summary"
            Me.XrSubreport_Summary.ReportSource = Me.XtraReport_Summary1
            Me.XrSubreport_Summary.SizeF = New System.Drawing.SizeF(300.0!, 110.0!)
            '
            'XrSubreport_Budget
            '
            Me.XrSubreport_Budget.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 75.0!)
            Me.XrSubreport_Budget.Name = "XrSubreport_Budget"
            Me.XrSubreport_Budget.ReportSource = Me.XtraReport_Budget1
            Me.XrSubreport_Budget.SizeF = New System.Drawing.SizeF(800.0!, 25.0!)
            '
            'GroupHeader1
            '
            Me.GroupHeader1.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrSubreport_Summary, Me.XrSubreport_Employer, Me.XrSubreport_AccountNumber, Me.XrLabel_ClientNameAddress, Me.XrPanel_CreditorAddress})
            Me.GroupHeader1.GroupFields.AddRange(New DevExpress.XtraReports.UI.GroupField() {New DevExpress.XtraReports.UI.GroupField("client_creditor_proposal", DevExpress.XtraReports.UI.XRColumnSortOrder.Ascending)})
            Me.GroupHeader1.HeightF = 379.2917!
            Me.GroupHeader1.Name = "GroupHeader1"
            '
            'ProposalsFullReport
            '
            Me.Bands.AddRange(New DevExpress.XtraReports.UI.Band() {Me.TopMarginBand1, Me.Detail, Me.BottomMarginBand1, Me.PageHeader, Me.GroupHeader1, Me.GroupFooter1})
            Me.Parameters.AddRange(New DevExpress.XtraReports.Parameters.Parameter() {Me.ParameterProposal, Me.ParameterBatch})
            Me.RequestParameters = False
            Me.ScriptLanguage = DevExpress.XtraReports.ScriptLanguage.VisualBasic
            Me.ScriptReferencesString = resources.GetString("$this.ScriptReferencesString")
            Me.Scripts.OnBeforePrint = "ProposalsFullReport_BeforePrint"
            Me.ScriptsSource = resources.GetString("$this.ScriptsSource")
            Me.StyleSheet.AddRange(New DevExpress.XtraReports.UI.XRControlStyle() {Me.XrControlStyle1})
            Me.Version = "11.2"
            Me.Controls.SetChildIndex(Me.GroupFooter1, 0)
            Me.Controls.SetChildIndex(Me.GroupHeader1, 0)
            Me.Controls.SetChildIndex(Me.PageHeader, 0)
            Me.Controls.SetChildIndex(Me.BottomMarginBand1, 0)
            Me.Controls.SetChildIndex(Me.Detail, 0)
            Me.Controls.SetChildIndex(Me.TopMarginBand1, 0)
            CType(Me.XrRichText1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.XtraReport_CreditorList1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.XtraReport_Budget1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.XtraReport_Secured1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.XtraReport_GeneralInfo1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.XtraReport_Employer1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.XtraReport_AccountNumber1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.XtraReport_Summary1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me, System.ComponentModel.ISupportInitialize).EndInit()

        End Sub
        Friend WithEvents GroupFooter1 As DevExpress.XtraReports.UI.GroupFooterBand
        Friend WithEvents XrSubreport_GeneralInfo As DevExpress.XtraReports.UI.XRSubreport
        Friend WithEvents XrSubreport_Secured As DevExpress.XtraReports.UI.XRSubreport
        Friend WithEvents XrSubreport_Budget As DevExpress.XtraReports.UI.XRSubreport
        Friend WithEvents XrSubreport_CreditorList As DevExpress.XtraReports.UI.XRSubreport
        Friend WithEvents XrLabel_message As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents ParameterProposal As DevExpress.XtraReports.Parameters.Parameter
        Friend WithEvents XrBarCode_PostalCode As DevExpress.XtraReports.UI.XRBarCode
        Friend WithEvents ParameterBatch As DevExpress.XtraReports.Parameters.Parameter
        Friend WithEvents PageHeader As DevExpress.XtraReports.UI.PageHeaderBand
        Friend WithEvents XrRichText1 As DevExpress.XtraReports.UI.XRRichText
        Friend WithEvents XrLabel_print_date As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel4 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel3 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrSubreport_Employer As DevExpress.XtraReports.UI.XRSubreport
        Friend WithEvents XrControlStyle1 As DevExpress.XtraReports.UI.XRControlStyle
        Friend WithEvents XrPanel_CreditorAddress As DevExpress.XtraReports.UI.XRPanel
        Friend WithEvents XrLabel_Creditor_Address As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_Creditor_L As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel1 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrSubreport_AccountNumber As DevExpress.XtraReports.UI.XRSubreport
        Friend WithEvents XrLabel_ClientNameAddress As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrSubreport_Summary As DevExpress.XtraReports.UI.XRSubreport
        Friend WithEvents GroupHeader1 As DevExpress.XtraReports.UI.GroupHeaderBand
        Private WithEvents XtraReport_Secured1 As DebtPlus.Reports.Proposals.Full.XtraReport_Secured
        Private WithEvents XtraReport_Budget1 As DebtPlus.Reports.Proposals.Full.XtraReport_Budget
        Private WithEvents XtraReport_CreditorList1 As DebtPlus.Reports.Proposals.Full.XtraReport_CreditorList
        Private WithEvents XtraReport_GeneralInfo1 As DebtPlus.Reports.Proposals.Full.XtraReport_GeneralInfo
        Private WithEvents XtraReport_Employer1 As DebtPlus.Reports.Proposals.Full.XtraReport_Employer
        Private WithEvents XtraReport_AccountNumber1 As DebtPlus.Reports.Proposals.Full.XtraReport_AccountNumber
        Private WithEvents XtraReport_Summary1 As DebtPlus.Reports.Proposals.Full.XtraReport_Summary
    End Class
End Namespace
