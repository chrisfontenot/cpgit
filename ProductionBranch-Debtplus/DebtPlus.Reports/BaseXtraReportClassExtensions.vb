﻿Imports System.Runtime.CompilerServices
Imports DebtPlus.Reports.Template
Imports System.IO
Imports System.Reflection
Imports DebtPlus.Utils
Imports DevExpress.XtraReports.UI

Public Module BaseXtraReportClassExtensions

    ' some common elements to all reports
    ' this is here, and not in BaseXtraReportClass because we dynamically load resources from the executing assembly.  We could
    ' get fancy and locate the right assembly at runtime, but this is a lot easier.

    <Extension()>
    Public Sub StandardSetup(ByVal report As BaseXtraReportClass)
        ' See if there is a report reference that we can use
        Dim fname As String = Path.Combine(DebtPlus.Configuration.Config.ReportsDirectory, report.ReportName)

        Try
            If File.Exists(fname) Then
                report.LoadLayout(fname)
                report.UsingDefaultLayout = False
            End If
        Catch ex As Exception
            DebtPlus.Svc.Common.ErrorHandling.HandleErrors(ex)
        End Try

        ' Load the standard report definition if we need a new item
        If report.UsingDefaultLayout Then
            Using ios = report.GetResourceStream(report.ReportName)
                If ios IsNot Nothing Then report.LoadLayout(ios)
            End Using
        End If

        ' Set the script references as needed
        report.ScriptReferences = DebtPlus.Utils.AssemblyTools.GetAllSiblingAssemblies()

        ' Update the subreports as well
        For Each bandPtr As Band In report.Bands
            For Each ctlPtr As XRControl In bandPtr.Controls
                Dim rpt As XRSubreport = TryCast(ctlPtr, XRSubreport)
                If rpt IsNot Nothing Then
                    Dim rptSrc As XtraReport = rpt.ReportSource
                    If rptSrc IsNot Nothing Then
                        rptSrc.ScriptReferences = report.ScriptReferences
                    End If
                End If
            Next
        Next
    End Sub

    <Extension()>
    Public Function GetResourceStream(ByVal report As BaseXtraReportClass, ByVal resourceName As String) As Stream
        Dim asm As Assembly = Assembly.GetExecutingAssembly
        'Dim list As String() = asm.GetManifestResourceNames
        Return asm.GetManifestResourceStream(String.Format("DebtPlus.Reports.{0}", resourceName))
    End Function

End Module