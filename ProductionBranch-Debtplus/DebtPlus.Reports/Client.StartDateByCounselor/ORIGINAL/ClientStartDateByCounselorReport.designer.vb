Namespace Client.StartDateByCounselor
    Partial Class ClientStartDateByCounselorReport

        'Component overrides dispose to clean up the component list.
        Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
            If disposing Then
                If components IsNot Nothing Then
                    components.Dispose()
                End If
            End If
            MyBase.Dispose(disposing)
        End Sub

        'Required by the Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Designer
        'It can be modified using the Designer.
        'Do not modify it using the code editor.
        Friend WithEvents GroupHeader1 As DevExpress.XtraReports.UI.GroupHeaderBand
        Friend WithEvents XrLabel_GroupName As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrPanel1 As DevExpress.XtraReports.UI.XRPanel
        Friend WithEvents XrLabel1 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel2 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel3 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel4 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel5 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel6 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel7 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrPanel2 As DevExpress.XtraReports.UI.XRPanel
        Friend WithEvents XrLabel_Office As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_Deposit As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_Last_Date As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_Balance As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_StartDate As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_Status As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_Client As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_Last_Amount As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_Group_Deposit As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents GroupFooter1 As DevExpress.XtraReports.UI.GroupFooterBand
        Friend WithEvents XrLabel_Group_Balance As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_Group_Clients As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_Group_Average As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents ReportFooter As DevExpress.XtraReports.UI.ReportFooterBand
        Friend WithEvents XrLabel_Total_Clients As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrSummary1 As DevExpress.XtraReports.UI.XRSummary
        Friend WithEvents XrSummary2 As DevExpress.XtraReports.UI.XRSummary
        Friend WithEvents XrSummary3 As DevExpress.XtraReports.UI.XRSummary
        Friend WithEvents XrSummary4 As DevExpress.XtraReports.UI.XRSummary
        Friend WithEvents XrSummary5 As DevExpress.XtraReports.UI.XRSummary
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
            Dim XrSummary1 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary()
            Dim XrSummary2 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary()
            Dim XrSummary3 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary()
            Dim XrSummary4 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary()
            Dim XrSummary5 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary()
            Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(ClientStartDateByCounselorReport))
            Me.GroupHeader1 = New DevExpress.XtraReports.UI.GroupHeaderBand()
            Me.XrPanel1 = New DevExpress.XtraReports.UI.XRPanel()
            Me.XrLabel7 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel6 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel5 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel4 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel3 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel2 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel1 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_GroupName = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrPanel2 = New DevExpress.XtraReports.UI.XRPanel()
            Me.XrLabel_Last_Amount = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_Office = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_Deposit = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_Last_Date = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_Balance = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_StartDate = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_Status = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_Client = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_Group_Deposit = New DevExpress.XtraReports.UI.XRLabel()
            Me.GroupFooter1 = New DevExpress.XtraReports.UI.GroupFooterBand()
            Me.XrLabel_Group_Average = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_Group_Clients = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_Group_Balance = New DevExpress.XtraReports.UI.XRLabel()
            Me.ReportFooter = New DevExpress.XtraReports.UI.ReportFooterBand()
            Me.XrLabel_Total_Clients = New DevExpress.XtraReports.UI.XRLabel()
            CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
            '
            'XrLabel_Title
            '
            Me.XrLabel_Title.SizeF = New System.Drawing.SizeF(725.0!, 42.0!)
            Me.XrLabel_Title.StylePriority.UseFont = False
            '
            'XrPageInfo_PageNumber
            '
            Me.XrPageInfo_PageNumber.LocationFloat = New DevExpress.Utils.PointFloat(917.0!, 8.0!)
            Me.XrPageInfo_PageNumber.StylePriority.UseTextAlignment = False
            '
            'XrPanel_AgencyAddress
            '
            Me.XrPanel_AgencyAddress.LocationFloat = New DevExpress.Utils.PointFloat(742.0!, 0.0!)
            '
            'XRLabel_Agency_Name
            '
            Me.XRLabel_Agency_Name.StylePriority.UseTextAlignment = False
            '
            'XrLine_Title
            '
            Me.XrLine_Title.SizeF = New System.Drawing.SizeF(725.0!, 8.0!)
            '
            'XrLabel_Agency_Address3
            '
            Me.XrLabel_Agency_Address3.StylePriority.UseFont = False
            Me.XrLabel_Agency_Address3.StylePriority.UseTextAlignment = False
            '
            'XrLabel_Agency_Address1
            '
            Me.XrLabel_Agency_Address1.StylePriority.UseFont = False
            Me.XrLabel_Agency_Address1.StylePriority.UseTextAlignment = False
            '
            'XrLabel_Agency_Phone
            '
            Me.XrLabel_Agency_Phone.StylePriority.UseFont = False
            Me.XrLabel_Agency_Phone.StylePriority.UseTextAlignment = False
            '
            'XrLabel_Agency_Address2
            '
            Me.XrLabel_Agency_Address2.StylePriority.UseFont = False
            Me.XrLabel_Agency_Address2.StylePriority.UseTextAlignment = False
            '
            'Detail
            '
            Me.Detail.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrPanel2})
            Me.Detail.HeightF = 16.0!
            '
            'GroupHeader1
            '
            Me.GroupHeader1.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrPanel1, Me.XrLabel_GroupName})
            Me.GroupHeader1.GroupFields.AddRange(New DevExpress.XtraReports.UI.GroupField() {New DevExpress.XtraReports.UI.GroupField("counselor_name", DevExpress.XtraReports.UI.XRColumnSortOrder.Ascending)})
            Me.GroupHeader1.HeightF = 79.0!
            Me.GroupHeader1.Name = "GroupHeader1"
            Me.GroupHeader1.RepeatEveryPage = True
            '
            'XrPanel1
            '
            Me.XrPanel1.BackColor = System.Drawing.Color.Teal
            Me.XrPanel1.BorderColor = System.Drawing.Color.Teal
            Me.XrPanel1.CanGrow = False
            Me.XrPanel1.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel7, Me.XrLabel6, Me.XrLabel5, Me.XrLabel4, Me.XrLabel3, Me.XrLabel2, Me.XrLabel1})
            Me.XrPanel1.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 58.0!)
            Me.XrPanel1.Name = "XrPanel1"
            Me.XrPanel1.SizeF = New System.Drawing.SizeF(1050.0!, 16.0!)
            '
            'XrLabel7
            '
            Me.XrLabel7.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel7.ForeColor = System.Drawing.Color.White
            Me.XrLabel7.LocationFloat = New DevExpress.Utils.PointFloat(808.0!, 0.0!)
            Me.XrLabel7.Name = "XrLabel7"
            Me.XrLabel7.SizeF = New System.Drawing.SizeF(233.0!, 15.0!)
            Me.XrLabel7.Text = "OFFICE"
            Me.XrLabel7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            '
            'XrLabel6
            '
            Me.XrLabel6.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel6.ForeColor = System.Drawing.Color.White
            Me.XrLabel6.LocationFloat = New DevExpress.Utils.PointFloat(666.0!, 0.0!)
            Me.XrLabel6.Name = "XrLabel6"
            Me.XrLabel6.SizeF = New System.Drawing.SizeF(125.0!, 15.0!)
            Me.XrLabel6.Text = "DEPOSIT AMT"
            Me.XrLabel6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel5
            '
            Me.XrLabel5.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel5.ForeColor = System.Drawing.Color.White
            Me.XrLabel5.LocationFloat = New DevExpress.Utils.PointFloat(520.0!, 0.0!)
            Me.XrLabel5.Name = "XrLabel5"
            Me.XrLabel5.SizeF = New System.Drawing.SizeF(141.0!, 15.0!)
            Me.XrLabel5.Text = "LAST DEPOSIT"
            Me.XrLabel5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
            '
            'XrLabel4
            '
            Me.XrLabel4.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel4.ForeColor = System.Drawing.Color.White
            Me.XrLabel4.LocationFloat = New DevExpress.Utils.PointFloat(444.0!, 0.0!)
            Me.XrLabel4.Name = "XrLabel4"
            Me.XrLabel4.SizeF = New System.Drawing.SizeF(72.0!, 15.0!)
            Me.XrLabel4.Text = "BALANCE"
            Me.XrLabel4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel3
            '
            Me.XrLabel3.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel3.ForeColor = System.Drawing.Color.White
            Me.XrLabel3.LocationFloat = New DevExpress.Utils.PointFloat(370.0!, 0.0!)
            Me.XrLabel3.Name = "XrLabel3"
            Me.XrLabel3.SizeF = New System.Drawing.SizeF(54.0!, 15.0!)
            Me.XrLabel3.Text = "START"
            Me.XrLabel3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel2
            '
            Me.XrLabel2.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel2.ForeColor = System.Drawing.Color.White
            Me.XrLabel2.LocationFloat = New DevExpress.Utils.PointFloat(325.0!, 0.0!)
            Me.XrLabel2.Name = "XrLabel2"
            Me.XrLabel2.SizeF = New System.Drawing.SizeF(31.0!, 15.0!)
            Me.XrLabel2.Text = "STS"
            Me.XrLabel2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            '
            'XrLabel1
            '
            Me.XrLabel1.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel1.ForeColor = System.Drawing.Color.White
            Me.XrLabel1.LocationFloat = New DevExpress.Utils.PointFloat(8.0!, 0.0!)
            Me.XrLabel1.Name = "XrLabel1"
            Me.XrLabel1.SizeF = New System.Drawing.SizeF(308.0!, 15.0!)
            Me.XrLabel1.Text = "CLIENT ID AND NAME"
            Me.XrLabel1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            '
            'XrLabel_GroupName
            '
            Me.XrLabel_GroupName.CanGrow = False
            Me.XrLabel_GroupName.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "counselor_name", "Counselor: {0}")})
            Me.XrLabel_GroupName.Font = New System.Drawing.Font("Times New Roman", 16.0!, System.Drawing.FontStyle.Bold)
            Me.XrLabel_GroupName.ForeColor = System.Drawing.Color.Maroon
            Me.XrLabel_GroupName.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 8.0!)
            Me.XrLabel_GroupName.Name = "XrLabel_GroupName"
            Me.XrLabel_GroupName.SizeF = New System.Drawing.SizeF(1050.0!, 34.0!)
            Me.XrLabel_GroupName.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            Me.XrLabel_GroupName.WordWrap = False
            '
            'XrPanel2
            '
            Me.XrPanel2.BorderColor = System.Drawing.Color.Transparent
            Me.XrPanel2.CanGrow = False
            Me.XrPanel2.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel_Last_Amount, Me.XrLabel_Office, Me.XrLabel_Deposit, Me.XrLabel_Last_Date, Me.XrLabel_Balance, Me.XrLabel_StartDate, Me.XrLabel_Status, Me.XrLabel_Client})
            Me.XrPanel2.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
            Me.XrPanel2.Name = "XrPanel2"
            Me.XrPanel2.SizeF = New System.Drawing.SizeF(1050.0!, 16.0!)
            '
            'XrLabel_Last_Amount
            '
            Me.XrLabel_Last_Amount.CanGrow = False
            Me.XrLabel_Last_Amount.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "last_deposit_amount", "{0:c}")})
            Me.XrLabel_Last_Amount.LocationFloat = New DevExpress.Utils.PointFloat(592.0!, 0.0!)
            Me.XrLabel_Last_Amount.Name = "XrLabel_Last_Amount"
            Me.XrLabel_Last_Amount.SizeF = New System.Drawing.SizeF(66.0!, 15.0!)
            Me.XrLabel_Last_Amount.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrLabel_Last_Amount.WordWrap = False
            '
            'XrLabel_Office
            '
            Me.XrLabel_Office.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "office_name")})
            Me.XrLabel_Office.LocationFloat = New DevExpress.Utils.PointFloat(808.0!, 0.0!)
            Me.XrLabel_Office.Name = "XrLabel_Office"
            Me.XrLabel_Office.SizeF = New System.Drawing.SizeF(233.0!, 15.0!)
            Me.XrLabel_Office.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            '
            'XrLabel_Deposit
            '
            Me.XrLabel_Deposit.CanGrow = False
            Me.XrLabel_Deposit.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "disbursement_factor", "{0:c}")})
            Me.XrLabel_Deposit.LocationFloat = New DevExpress.Utils.PointFloat(666.0!, 0.0!)
            Me.XrLabel_Deposit.Name = "XrLabel_Deposit"
            Me.XrLabel_Deposit.SizeF = New System.Drawing.SizeF(125.0!, 15.0!)
            Me.XrLabel_Deposit.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrLabel_Deposit.WordWrap = False
            '
            'XrLabel_Last_Date
            '
            Me.XrLabel_Last_Date.CanGrow = False
            Me.XrLabel_Last_Date.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "last_deposit_date", "{0:d}")})
            Me.XrLabel_Last_Date.LocationFloat = New DevExpress.Utils.PointFloat(517.0!, 0.0!)
            Me.XrLabel_Last_Date.Name = "XrLabel_Last_Date"
            Me.XrLabel_Last_Date.SizeF = New System.Drawing.SizeF(75.0!, 15.0!)
            Me.XrLabel_Last_Date.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrLabel_Last_Date.WordWrap = False
            '
            'XrLabel_Balance
            '
            Me.XrLabel_Balance.CanGrow = False
            Me.XrLabel_Balance.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "balance", "{0:c}")})
            Me.XrLabel_Balance.LocationFloat = New DevExpress.Utils.PointFloat(444.0!, 0.0!)
            Me.XrLabel_Balance.Name = "XrLabel_Balance"
            Me.XrLabel_Balance.SizeF = New System.Drawing.SizeF(72.0!, 15.0!)
            Me.XrLabel_Balance.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrLabel_Balance.WordWrap = False
            '
            'XrLabel_StartDate
            '
            Me.XrLabel_StartDate.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "start_date", "{0:d}")})
            Me.XrLabel_StartDate.LocationFloat = New DevExpress.Utils.PointFloat(370.0!, 0.0!)
            Me.XrLabel_StartDate.Name = "XrLabel_StartDate"
            Me.XrLabel_StartDate.SizeF = New System.Drawing.SizeF(54.0!, 15.0!)
            Me.XrLabel_StartDate.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel_Status
            '
            Me.XrLabel_Status.CanGrow = False
            Me.XrLabel_Status.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "status")})
            Me.XrLabel_Status.LocationFloat = New DevExpress.Utils.PointFloat(325.0!, 0.0!)
            Me.XrLabel_Status.Name = "XrLabel_Status"
            Me.XrLabel_Status.SizeF = New System.Drawing.SizeF(31.0!, 15.0!)
            Me.XrLabel_Status.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            Me.XrLabel_Status.WordWrap = False
            '
            'XrLabel_Client
            '
            Me.XrLabel_Client.CanGrow = False
            Me.XrLabel_Client.LocationFloat = New DevExpress.Utils.PointFloat(8.0!, 0.0!)
            Me.XrLabel_Client.Name = "XrLabel_Client"
            Me.XrLabel_Client.Scripts.OnBeforePrint = "XrLabel_Client_BeforePrint"
            Me.XrLabel_Client.SizeF = New System.Drawing.SizeF(308.0!, 15.0!)
            Me.XrLabel_Client.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            Me.XrLabel_Client.WordWrap = False
            '
            'XrLabel_Group_Deposit
            '
            Me.XrLabel_Group_Deposit.CanGrow = False
            Me.XrLabel_Group_Deposit.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "disbursement_factor")})
            Me.XrLabel_Group_Deposit.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel_Group_Deposit.LocationFloat = New DevExpress.Utils.PointFloat(641.0!, 8.0!)
            Me.XrLabel_Group_Deposit.Name = "XrLabel_Group_Deposit"
            Me.XrLabel_Group_Deposit.SizeF = New System.Drawing.SizeF(150.0!, 15.0!)
            XrSummary1.FormatString = "{0:c2}"
            XrSummary1.Running = DevExpress.XtraReports.UI.SummaryRunning.Group
            Me.XrLabel_Group_Deposit.Summary = XrSummary1
            Me.XrLabel_Group_Deposit.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'GroupFooter1
            '
            Me.GroupFooter1.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel_Group_Average, Me.XrLabel_Group_Clients, Me.XrLabel_Group_Balance, Me.XrLabel_Group_Deposit})
            Me.GroupFooter1.HeightF = 47.0!
            Me.GroupFooter1.Name = "GroupFooter1"
            '
            'XrLabel_Group_Average
            '
            Me.XrLabel_Group_Average.CanGrow = False
            Me.XrLabel_Group_Average.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "disbursement_factor")})
            Me.XrLabel_Group_Average.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel_Group_Average.LocationFloat = New DevExpress.Utils.PointFloat(808.0!, 8.0!)
            Me.XrLabel_Group_Average.Name = "XrLabel_Group_Average"
            Me.XrLabel_Group_Average.SizeF = New System.Drawing.SizeF(234.0!, 15.0!)
            XrSummary2.FormatString = "AVG Deposit: {0:c2}"
            XrSummary2.Func = DevExpress.XtraReports.UI.SummaryFunc.Avg
            XrSummary2.Running = DevExpress.XtraReports.UI.SummaryRunning.Group
            Me.XrLabel_Group_Average.Summary = XrSummary2
            Me.XrLabel_Group_Average.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            Me.XrLabel_Group_Average.WordWrap = False
            '
            'XrLabel_Group_Clients
            '
            Me.XrLabel_Group_Clients.CanGrow = False
            Me.XrLabel_Group_Clients.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "client")})
            Me.XrLabel_Group_Clients.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel_Group_Clients.LocationFloat = New DevExpress.Utils.PointFloat(8.0!, 8.0!)
            Me.XrLabel_Group_Clients.Name = "XrLabel_Group_Clients"
            Me.XrLabel_Group_Clients.SizeF = New System.Drawing.SizeF(150.0!, 17.0!)
            XrSummary3.FormatString = "{0:d} Client(s) listed"
            XrSummary3.Func = DevExpress.XtraReports.UI.SummaryFunc.Count
            XrSummary3.Running = DevExpress.XtraReports.UI.SummaryRunning.Group
            Me.XrLabel_Group_Clients.Summary = XrSummary3
            Me.XrLabel_Group_Clients.WordWrap = False
            '
            'XrLabel_Group_Balance
            '
            Me.XrLabel_Group_Balance.CanGrow = False
            Me.XrLabel_Group_Balance.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "balance")})
            Me.XrLabel_Group_Balance.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel_Group_Balance.LocationFloat = New DevExpress.Utils.PointFloat(366.0!, 8.0!)
            Me.XrLabel_Group_Balance.Name = "XrLabel_Group_Balance"
            Me.XrLabel_Group_Balance.SizeF = New System.Drawing.SizeF(150.0!, 15.0!)
            XrSummary4.FormatString = "{0:c2}"
            XrSummary4.Running = DevExpress.XtraReports.UI.SummaryRunning.Group
            Me.XrLabel_Group_Balance.Summary = XrSummary4
            Me.XrLabel_Group_Balance.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrLabel_Group_Balance.WordWrap = False
            '
            'ReportFooter
            '
            Me.ReportFooter.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel_Total_Clients})
            Me.ReportFooter.HeightF = 50.0!
            Me.ReportFooter.Name = "ReportFooter"
            '
            'XrLabel_Total_Clients
            '
            Me.XrLabel_Total_Clients.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "client")})
            Me.XrLabel_Total_Clients.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel_Total_Clients.LocationFloat = New DevExpress.Utils.PointFloat(8.0!, 17.0!)
            Me.XrLabel_Total_Clients.Name = "XrLabel_Total_Clients"
            Me.XrLabel_Total_Clients.SizeF = New System.Drawing.SizeF(150.0!, 17.0!)
            XrSummary5.FormatString = "{0:d} Total client(s) listed"
            XrSummary5.Func = DevExpress.XtraReports.UI.SummaryFunc.Count
            XrSummary5.Running = DevExpress.XtraReports.UI.SummaryRunning.Report
            Me.XrLabel_Total_Clients.Summary = XrSummary5
            Me.XrLabel_Total_Clients.Text = "0 Total Client(s) listed"
            '
            'ClientStartDateByCounselorReport
            '
            Me.Bands.AddRange(New DevExpress.XtraReports.UI.Band() {Me.Detail, Me.PageHeader, Me.PageFooter, Me.TopMarginBand1, Me.BottomMarginBand1, Me.GroupHeader1, Me.GroupFooter1, Me.ReportFooter})
            Me.Landscape = True
            Me.PageHeight = 850
            Me.PageWidth = 1100
            Me.ScriptLanguage = DevExpress.XtraReports.ScriptLanguage.VisualBasic
            Me.ScriptReferencesString = resources.GetString("$this.ScriptReferencesString")
            Me.Scripts.OnBeforePrint = "ClientStartDateByCounselorReport_BeforePrint"
            Me.ScriptsSource = resources.GetString("$this.ScriptsSource")
            Me.StyleSheet.AddRange(New DevExpress.XtraReports.UI.XRControlStyle() {Me.XrControlStyle_Header, Me.XrControlStyle_Totals, Me.XrControlStyle_HeaderPannel, Me.XrControlStyle_GroupHeader})
            Me.Version = "11.2"
            Me.Controls.SetChildIndex(Me.ReportFooter, 0)
            Me.Controls.SetChildIndex(Me.GroupFooter1, 0)
            Me.Controls.SetChildIndex(Me.GroupHeader1, 0)
            Me.Controls.SetChildIndex(Me.BottomMarginBand1, 0)
            Me.Controls.SetChildIndex(Me.TopMarginBand1, 0)
            Me.Controls.SetChildIndex(Me.PageFooter, 0)
            Me.Controls.SetChildIndex(Me.PageHeader, 0)
            Me.Controls.SetChildIndex(Me.Detail, 0)
            CType(Me, System.ComponentModel.ISupportInitialize).EndInit()

        End Sub
    End Class
End Namespace
