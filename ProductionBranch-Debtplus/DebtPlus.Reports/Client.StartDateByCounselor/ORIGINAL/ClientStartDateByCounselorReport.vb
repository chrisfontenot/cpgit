#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}
#End Region

Option Compare Binary
Option Explicit On 
Option Strict On

Imports DevExpress.XtraReports.UI

Namespace Client.StartDateByCounselor
    Public Class ClientStartDateByCounselorReport
        Inherits DebtPlus.Reports.Template.DatedTemplateXtraReportClass

        ''' <summary>
        ''' Create an instance of our report
        ''' </summary>
        Public Sub New()
            MyBase.New()
            Me.Parameters.Clear()
            InitializeComponent()
            AddHandler BeforePrint, AddressOf ClientStartDateByCounselorReport_BeforePrint
            AddHandler XrLabel_Client.BeforePrint, AddressOf XrLabel_Client_BeforePrint
        End Sub

        Public Overrides ReadOnly Property ReportTitle() As String
            Get
                Return "Client Start Dates by Counselor"
            End Get
        End Property

        Private ds As New System.Data.DataSet("ds")

        ''' <summary>
        ''' Read the data from the system and bind it to the report
        ''' </summary>
        Private Sub ClientStartDateByCounselorReport_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs)
            Const TableName As String = "rpt_ClientStartByCounselor"
            Dim rpt As DevExpress.XtraReports.UI.XtraReport = CType(sender, DevExpress.XtraReports.UI.XtraReport)
            Dim sqlInfo As DebtPlus.LINQ.SQLInfoClass = DebtPlus.LINQ.SQLInfoClass.getDefault()
            Dim cn As System.Data.SqlClient.SqlConnection = New System.Data.SqlClient.SqlConnection(sqlInfo.ConnectionString)

            cn.Open()
            Try
                Using cmd As New System.Data.SqlClient.SqlCommand()
                    With cmd
                        .Connection = cn
                        .CommandText = "rpt_ClientStartByCounselor"
                        .CommandType = System.Data.CommandType.StoredProcedure
                        System.Data.SqlClient.SqlCommandBuilder.DeriveParameters(cmd)
                        .CommandTimeout = 0
                        .Parameters(1).Value = rpt.Parameters("ParameterFromDate").Value
                        .Parameters(2).Value = rpt.Parameters("ParameterToDate").Value
                    End With

                    Using da As New System.Data.SqlClient.SqlDataAdapter(cmd)
                        da.Fill(ds, TableName)
                        rpt.DataSource = ds.Tables(TableName).DefaultView
                    End Using
                End Using

            Finally
                If cn IsNot Nothing Then
                    cn.Dispose()
                End If
            End Try
        End Sub

        ''' <summary>
        ''' Format the client ID and name
        ''' </summary>
        Private Sub XrLabel_Client_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs)
            Dim lbl As DevExpress.XtraReports.UI.XRLabel = CType(sender, DevExpress.XtraReports.UI.XRLabel)
            Dim rpt As DevExpress.XtraReports.UI.XtraReport = CType(lbl.Report, DevExpress.XtraReports.UI.XtraReport)
            Dim ClientID As String = String.Format("[{0}]", DebtPlus.Utils.Format.Client.FormatClientID(rpt.GetCurrentColumnValue("client")))

            Dim ClientName As String
            If rpt.GetCurrentColumnValue("name") IsNot Nothing AndAlso rpt.GetCurrentColumnValue("name") IsNot System.DBNull.Value Then
                ClientName = Convert.ToString(rpt.GetCurrentColumnValue("name"))
            Else
                ClientName = System.String.Empty
            End If

            With lbl
                If ClientName <> System.String.Empty Then
                    .Text = String.Format("{0} {1}", ClientID, ClientName)
                Else
                    .Text = ClientID
                End If
            End With
        End Sub
    End Class
End Namespace
