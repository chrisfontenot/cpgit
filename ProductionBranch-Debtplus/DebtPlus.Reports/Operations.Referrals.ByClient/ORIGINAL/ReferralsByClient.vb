#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}
#End Region

Option Compare Binary
Option Explicit On 
Option Strict On

Public Class ReferralsByClient

    ''' <summary>
    '''     Create an instance of our report
    ''' </summary>
    Public Sub New()
        MyBase.New()
        Parameters.Clear()
        InitializeComponent()
        'AddHandler BeforePrint, AddressOf Report_BeforePrint
        'AddHandler XrLabel_client.BeforePrint, AddressOf XrLabel_client_BeforePrint
    End Sub

    Public Property Parameter_Referral() As System.Int32
        Get
            Return Convert.ToInt32(Parameters("ParameterReferral").Value)
        End Get
        Set(ByVal value As System.Int32)
            Parameters("ParameterReferral").Value = value
        End Set
    End Property

    Public Overrides Sub SetReportParameter(ByVal Parameter As String, ByVal Value As Object)
        If Parameter = "Referral" Then
            Parameter_Referral = Convert.ToInt32(Value)
        Else
            MyBase.SetReportParameter(Parameter, Value)
        End If
    End Sub

    Public Overrides Function NeedParameters() As Boolean
        Return True
    End Function

    Public Overrides Function RequestReportParameters() As System.Windows.Forms.DialogResult
        Dim Answer As System.Windows.Forms.DialogResult
        Using frm As New ReportParametersForm()
            Answer = frm.ShowDialog
            Parameters("ParameterFromDate").Value = frm.Parameter_FromDate
            Parameters("ParameterToDate").Value = frm.Parameter_ToDate
            Parameters("ParameterReferral").Value = frm.Parameter_Referral
        End Using
        Return Answer
    End Function

    Public Overrides ReadOnly Property ReportTitle() As String
        Get
            Return "Referrals By Client"
        End Get
    End Property

    Dim ds As New System.Data.DataSet("ds")
    Private Sub Report_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs)
        Const TableName As String = "rpt_referred_by_by_client"
        Dim rpt As DevExpress.XtraReports.UI.XtraReport = CType(sender, DevExpress.XtraReports.UI.XtraReport)
        Dim sqlInfo As DebtPlus.LINQ.SQLInfoClass = DebtPlus.LINQ.SQLInfoClass.getDefault()
        Dim cn As System.Data.SqlClient.SqlConnection = New System.Data.SqlClient.SqlConnection(sqlInfo.ConnectionString)
        Try
            cn.Open()
            Using cmd As New System.Data.SqlClient.SqlCommand()
                With cmd
                    .Connection = cn
                    .CommandText = "rpt_referred_by_by_client"
                    .CommandType = System.Data.CommandType.StoredProcedure
                    System.Data.SqlClient.SqlCommandBuilder.DeriveParameters(cmd)
                    .CommandTimeout = 0

                    .Parameters(1).Value = rpt.Parameters("ParameterFromDate").Value
                    .Parameters(2).Value = rpt.Parameters("ParameterToDate").Value

                    '-- If a type is selected tehn use it as well.
                    Dim ItemKey As Int32 = Convert.ToInt32(rpt.Parameters("ParameterReferral").Value)
                    If ItemKey > 0 Then
                        .Parameters(3).Value = ItemKey
                    End If
                End With

                Using da As New System.Data.SqlClient.SqlDataAdapter(cmd)
                    da.Fill(ds, TableName)
                End Using
            End Using

            rpt.DataSource = ds.Tables(TableName).DefaultView

        Finally
            If cn IsNot Nothing Then
                If cn.State <> System.Data.ConnectionState.Closed Then cn.Close()
                cn.Dispose()
            End If
        End Try
    End Sub

    Private Sub XrLabel_client_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs) Handles XrLabel_client.BeforePrint
        With CType(sender, DevExpress.XtraReports.UI.XRLabel)
            Dim rpt As DevExpress.XtraReports.UI.XtraReport = CType(.Report, DevExpress.XtraReports.UI.XtraReport)
            .Text = DebtPlus.Utils.Format.Client.FormatClientID(rpt.GetCurrentColumnValue("client"))
        End With
    End Sub
End Class
