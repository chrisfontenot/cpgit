#Region "Copyright 2000-2010 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2010 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}
#End Region
<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class ReferralsByClient
    Inherits DebtPlus.Reports.Template.DatedTemplateXtraReportClass

    'Component overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If components IsNot Nothing Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Designer
    'It can be modified using the Designer.
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(ReferralsByClient))
        Me.XrPanel1 = New DevExpress.XtraReports.UI.XRPanel
        Me.XrLabel2 = New DevExpress.XtraReports.UI.XRLabel
        Me.XrLabel18 = New DevExpress.XtraReports.UI.XRLabel
        Me.XrLabel4 = New DevExpress.XtraReports.UI.XRLabel
        Me.XrLabel1 = New DevExpress.XtraReports.UI.XRLabel
        Me.XrLabel6 = New DevExpress.XtraReports.UI.XRLabel
        Me.XrLabel13 = New DevExpress.XtraReports.UI.XRLabel
        Me.XrLabel_client = New DevExpress.XtraReports.UI.XRLabel
        Me.XrLabel_referral_source = New DevExpress.XtraReports.UI.XRLabel
        Me.XrLabel_client_name = New DevExpress.XtraReports.UI.XRLabel
        Me.XrLabel_appt_date = New DevExpress.XtraReports.UI.XRLabel
        Me.ParameterReferral = New DevExpress.XtraReports.Parameters.Parameter
        Me.XrLabel_appt_time = New DevExpress.XtraReports.UI.XRLabel
        Me.XrLabel_appt_result = New DevExpress.XtraReports.UI.XRLabel
        CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
        '
        'Detail
        '
        Me.Detail.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel_appt_result, Me.XrLabel_appt_time, Me.XrLabel_client_name, Me.XrLabel_appt_date, Me.XrLabel_referral_source, Me.XrLabel_client})
        Me.Detail.HeightF = 15.0!
        '
        'PageHeader
        '
        Me.PageHeader.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrPanel1})
        Me.PageHeader.HeightF = 149.0!
        Me.PageHeader.Controls.SetChildIndex(Me.XrPanel1, 0)
        Me.PageHeader.Controls.SetChildIndex(Me.XrPanel_AgencyAddress, 0)
        Me.PageHeader.Controls.SetChildIndex(Me.XrLabel_Title, 0)
        Me.PageHeader.Controls.SetChildIndex(Me.XrLine_Title, 0)
        Me.PageHeader.Controls.SetChildIndex(Me.XrLabel_Subtitle, 0)
        Me.PageHeader.Controls.SetChildIndex(Me.XrPageInfo1, 0)
        '
        'XrLabel_Title
        '
        Me.XrLabel_Title.LocationFloat = New DevExpress.Utils.PointFloat(8.0!, 7.999992!)
        Me.XrLabel_Title.SizeF = New System.Drawing.SizeF(470.125!, 42.0!)
        Me.XrLabel_Title.StylePriority.UseFont = False
        '
        'XrLabel_Subtitle
        '
        Me.XrLabel_Subtitle.SizeF = New System.Drawing.SizeF(725.0!, 17.0!)
        '
        'XrPageInfo_PageNumber
        '
        Me.XrPageInfo_PageNumber.LocationFloat = New DevExpress.Utils.PointFloat(655.0!, 8.0!)
        Me.XrPageInfo_PageNumber.StylePriority.UseTextAlignment = False
        '
        'XrPanel_AgencyAddress
        '
        Me.XrPanel_AgencyAddress.LocationFloat = New DevExpress.Utils.PointFloat(492.0!, 0.0!)
        '
        'XRLabel_Agency_Name
        '
        Me.XRLabel_Agency_Name.StylePriority.UseTextAlignment = False
        '
        'XrLine_Title
        '
        Me.XrLine_Title.SizeF = New System.Drawing.SizeF(470.125!, 8.0!)
        '
        'XrLabel_Agency_Address3
        '
        Me.XrLabel_Agency_Address3.StylePriority.UseFont = False
        Me.XrLabel_Agency_Address3.StylePriority.UseTextAlignment = False
        '
        'XrLabel_Agency_Address1
        '
        Me.XrLabel_Agency_Address1.StylePriority.UseFont = False
        Me.XrLabel_Agency_Address1.StylePriority.UseTextAlignment = False
        '
        'XrLabel_Agency_Phone
        '
        Me.XrLabel_Agency_Phone.StylePriority.UseFont = False
        Me.XrLabel_Agency_Phone.StylePriority.UseTextAlignment = False
        '
        'XrLabel_Agency_Address2
        '
        Me.XrLabel_Agency_Address2.StylePriority.UseFont = False
        Me.XrLabel_Agency_Address2.StylePriority.UseTextAlignment = False
        '
        'XrPanel1
        '
        Me.XrPanel1.BackColor = System.Drawing.Color.Teal
        Me.XrPanel1.CanGrow = False
        Me.XrPanel1.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel2, Me.XrLabel18, Me.XrLabel4, Me.XrLabel1, Me.XrLabel6, Me.XrLabel13})
        Me.XrPanel1.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 125.0!)
        Me.XrPanel1.Name = "XrPanel1"
        Me.XrPanel1.SizeF = New System.Drawing.SizeF(798.0!, 18.0!)
        Me.XrPanel1.StylePriority.UseBackColor = False
        '
        'XrLabel2
        '
        Me.XrLabel2.CanGrow = False
        Me.XrLabel2.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
        Me.XrLabel2.ForeColor = System.Drawing.Color.White
        Me.XrLabel2.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 1.0!)
        Me.XrLabel2.Name = "XrLabel2"
        Me.XrLabel2.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel2.SizeF = New System.Drawing.SizeF(66.0!, 15.0!)
        Me.XrLabel2.StylePriority.UseFont = False
        Me.XrLabel2.StylePriority.UseForeColor = False
        Me.XrLabel2.StylePriority.UseTextAlignment = False
        Me.XrLabel2.Text = "DATE"
        Me.XrLabel2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
        '
        'XrLabel18
        '
        Me.XrLabel18.CanGrow = False
        Me.XrLabel18.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
        Me.XrLabel18.ForeColor = System.Drawing.Color.White
        Me.XrLabel18.LocationFloat = New DevExpress.Utils.PointFloat(75.0!, 1.0!)
        Me.XrLabel18.Name = "XrLabel18"
        Me.XrLabel18.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel18.SizeF = New System.Drawing.SizeF(58.0!, 15.0!)
        Me.XrLabel18.StylePriority.UseFont = False
        Me.XrLabel18.StylePriority.UseForeColor = False
        Me.XrLabel18.StylePriority.UseTextAlignment = False
        Me.XrLabel18.Text = "TIME"
        Me.XrLabel18.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
        '
        'XrLabel4
        '
        Me.XrLabel4.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
        Me.XrLabel4.ForeColor = System.Drawing.Color.White
        Me.XrLabel4.LocationFloat = New DevExpress.Utils.PointFloat(225.0!, 0.9999924!)
        Me.XrLabel4.Name = "XrLabel4"
        Me.XrLabel4.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 0, 0, 0, 100.0!)
        Me.XrLabel4.SizeF = New System.Drawing.SizeF(269.625!, 14.99999!)
        Me.XrLabel4.StylePriority.UseFont = False
        Me.XrLabel4.StylePriority.UseForeColor = False
        Me.XrLabel4.StylePriority.UsePadding = False
        Me.XrLabel4.StylePriority.UseTextAlignment = False
        Me.XrLabel4.Text = "NAME"
        Me.XrLabel4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        '
        'XrLabel1
        '
        Me.XrLabel1.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
        Me.XrLabel1.ForeColor = System.Drawing.Color.White
        Me.XrLabel1.LocationFloat = New DevExpress.Utils.PointFloat(150.0!, 1.0!)
        Me.XrLabel1.Name = "XrLabel1"
        Me.XrLabel1.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel1.SizeF = New System.Drawing.SizeF(66.0!, 15.0!)
        Me.XrLabel1.StylePriority.UseFont = False
        Me.XrLabel1.StylePriority.UseForeColor = False
        Me.XrLabel1.StylePriority.UseTextAlignment = False
        Me.XrLabel1.Text = "CLIENT"
        Me.XrLabel1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
        '
        'XrLabel6
        '
        Me.XrLabel6.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
        Me.XrLabel6.ForeColor = System.Drawing.Color.White
        Me.XrLabel6.LocationFloat = New DevExpress.Utils.PointFloat(707.4583!, 0.9999924!)
        Me.XrLabel6.Name = "XrLabel6"
        Me.XrLabel6.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel6.SizeF = New System.Drawing.SizeF(80.54169!, 14.99999!)
        Me.XrLabel6.StylePriority.UseFont = False
        Me.XrLabel6.StylePriority.UseForeColor = False
        Me.XrLabel6.StylePriority.UseTextAlignment = False
        Me.XrLabel6.Text = "RESULT"
        Me.XrLabel6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        '
        'XrLabel13
        '
        Me.XrLabel13.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
        Me.XrLabel13.ForeColor = System.Drawing.Color.White
        Me.XrLabel13.LocationFloat = New DevExpress.Utils.PointFloat(507.125!, 1.0!)
        Me.XrLabel13.Name = "XrLabel13"
        Me.XrLabel13.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel13.SizeF = New System.Drawing.SizeF(186.7917!, 14.99999!)
        Me.XrLabel13.StylePriority.UseFont = False
        Me.XrLabel13.StylePriority.UseForeColor = False
        Me.XrLabel13.StylePriority.UseTextAlignment = False
        Me.XrLabel13.Text = "REFFERAL"
        Me.XrLabel13.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        '
        'XrLabel_client
        '
        Me.XrLabel_client.Font = New System.Drawing.Font("Times New Roman", 9.75!)
        Me.XrLabel_client.ForeColor = System.Drawing.SystemColors.WindowText
        Me.XrLabel_client.LocationFloat = New DevExpress.Utils.PointFloat(133.0!, 0.0!)
        Me.XrLabel_client.Name = "XrLabel_client"
        Me.XrLabel_client.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel_client.Scripts.OnBeforePrint = "XrLabel_client_BeforePrint"
        Me.XrLabel_client.SizeF = New System.Drawing.SizeF(83.0!, 14.99999!)
        Me.XrLabel_client.StylePriority.UseFont = False
        Me.XrLabel_client.StylePriority.UseForeColor = False
        Me.XrLabel_client.StylePriority.UseTextAlignment = False
        Me.XrLabel_client.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
        '
        'XrLabel_referral_source
        '
        Me.XrLabel_referral_source.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "referral_source")})
        Me.XrLabel_referral_source.Font = New System.Drawing.Font("Times New Roman", 9.75!)
        Me.XrLabel_referral_source.ForeColor = System.Drawing.SystemColors.WindowText
        Me.XrLabel_referral_source.LocationFloat = New DevExpress.Utils.PointFloat(507.125!, 0.0!)
        Me.XrLabel_referral_source.Name = "XrLabel_referral_source"
        Me.XrLabel_referral_source.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel_referral_source.SizeF = New System.Drawing.SizeF(186.7917!, 14.99999!)
        Me.XrLabel_referral_source.StylePriority.UseFont = False
        Me.XrLabel_referral_source.StylePriority.UseForeColor = False
        Me.XrLabel_referral_source.StylePriority.UseTextAlignment = False
        Me.XrLabel_referral_source.Text = "1ST CONTACT & REFFERAL"
        Me.XrLabel_referral_source.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        Me.XrLabel_referral_source.WordWrap = False
        '
        'XrLabel_client_name
        '
        Me.XrLabel_client_name.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "client_name")})
        Me.XrLabel_client_name.Font = New System.Drawing.Font("Times New Roman", 9.75!)
        Me.XrLabel_client_name.ForeColor = System.Drawing.SystemColors.WindowText
        Me.XrLabel_client_name.LocationFloat = New DevExpress.Utils.PointFloat(225.0!, 0.0!)
        Me.XrLabel_client_name.Name = "XrLabel_client_name"
        Me.XrLabel_client_name.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel_client_name.SizeF = New System.Drawing.SizeF(269.625!, 14.99999!)
        Me.XrLabel_client_name.StylePriority.UseFont = False
        Me.XrLabel_client_name.StylePriority.UseForeColor = False
        Me.XrLabel_client_name.StylePriority.UseTextAlignment = False
        Me.XrLabel_client_name.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        '
        'XrLabel_appt_date
        '
        Me.XrLabel_appt_date.CanGrow = False
        Me.XrLabel_appt_date.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "appt_date", "{0:M/d/yy}")})
        Me.XrLabel_appt_date.Font = New System.Drawing.Font("Times New Roman", 9.75!)
        Me.XrLabel_appt_date.ForeColor = System.Drawing.SystemColors.WindowText
        Me.XrLabel_appt_date.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
        Me.XrLabel_appt_date.Name = "XrLabel_appt_date"
        Me.XrLabel_appt_date.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel_appt_date.SizeF = New System.Drawing.SizeF(66.0!, 14.99999!)
        Me.XrLabel_appt_date.StylePriority.UseFont = False
        Me.XrLabel_appt_date.StylePriority.UseForeColor = False
        Me.XrLabel_appt_date.StylePriority.UseTextAlignment = False
        Me.XrLabel_appt_date.Text = "TIME"
        Me.XrLabel_appt_date.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
        '
        'ParameterReferral
        '
        Me.ParameterReferral.Description = "Specific Referral Source"
        Me.ParameterReferral.Name = "ParameterReferral"
        Me.ParameterReferral.Type = GetType(Short)
        Me.ParameterReferral.Value = CType(-1, Short)
        Me.ParameterReferral.Visible = False
        '
        'XrLabel_appt_time
        '
        Me.XrLabel_appt_time.CanGrow = False
        Me.XrLabel_appt_time.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "appt_date", "{0:h:mm tt}")})
        Me.XrLabel_appt_time.Font = New System.Drawing.Font("Times New Roman", 9.75!)
        Me.XrLabel_appt_time.ForeColor = System.Drawing.SystemColors.WindowText
        Me.XrLabel_appt_time.LocationFloat = New DevExpress.Utils.PointFloat(67.00001!, 0.0!)
        Me.XrLabel_appt_time.Name = "XrLabel_appt_time"
        Me.XrLabel_appt_time.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel_appt_time.SizeF = New System.Drawing.SizeF(66.0!, 14.99999!)
        Me.XrLabel_appt_time.StylePriority.UseFont = False
        Me.XrLabel_appt_time.StylePriority.UseForeColor = False
        Me.XrLabel_appt_time.StylePriority.UseTextAlignment = False
        Me.XrLabel_appt_time.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
        '
        'XrLabel_appt_result
        '
        Me.XrLabel_appt_result.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "appt_result")})
        Me.XrLabel_appt_result.Font = New System.Drawing.Font("Times New Roman", 9.75!)
        Me.XrLabel_appt_result.ForeColor = System.Drawing.SystemColors.WindowText
        Me.XrLabel_appt_result.LocationFloat = New DevExpress.Utils.PointFloat(707.4583!, 0.0!)
        Me.XrLabel_appt_result.Name = "XrLabel_appt_result"
        Me.XrLabel_appt_result.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel_appt_result.SizeF = New System.Drawing.SizeF(80.54169!, 14.99999!)
        Me.XrLabel_appt_result.StylePriority.UseFont = False
        Me.XrLabel_appt_result.StylePriority.UseForeColor = False
        Me.XrLabel_appt_result.StylePriority.UseTextAlignment = False
        Me.XrLabel_appt_result.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        Me.XrLabel_appt_result.WordWrap = False
        '
        'ReferralsByClient
        '
        Me.Bands.AddRange(New DevExpress.XtraReports.UI.Band() {Me.Detail, Me.PageHeader, Me.PageFooter, Me.TopMarginBand1, Me.BottomMarginBand1})
        Me.DesignerOptions.ShowExportWarnings = False
        Me.DesignerOptions.ShowPrintingWarnings = False
        Me.Parameters.AddRange(New DevExpress.XtraReports.Parameters.Parameter() {Me.ParameterFromDate, Me.ParameterToDate, Me.ParameterFromDate, Me.ParameterToDate, Me.ParameterFromDate, Me.ParameterToDate, Me.ParameterFromDate, Me.ParameterToDate, Me.ParameterReferral})
        Me.ScriptLanguage = DevExpress.XtraReports.ScriptLanguage.VisualBasic
        Me.ScriptReferencesString = resources.GetString("$this.ScriptReferencesString")
        Me.Scripts.OnBeforePrint = "ReferralsByClient_BeforePrint"
        Me.ScriptsSource = resources.GetString("$this.ScriptsSource")
        Me.StyleSheet.AddRange(New DevExpress.XtraReports.UI.XRControlStyle() {Me.XrControlStyle_Header, Me.XrControlStyle_Totals, Me.XrControlStyle_HeaderPannel, Me.XrControlStyle_GroupHeader})
        Me.Version = "10.2"
        CType(Me, System.ComponentModel.ISupportInitialize).EndInit()

    End Sub

    Friend WithEvents XrPanel1 As DevExpress.XtraReports.UI.XRPanel
    Friend WithEvents XrLabel1 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel6 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel4 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel18 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel13 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel_appt_date As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel_client_name As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel_referral_source As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel_client As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents ParameterReferral As DevExpress.XtraReports.Parameters.Parameter
    Friend WithEvents XrLabel2 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel_appt_time As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel_appt_result As DevExpress.XtraReports.UI.XRLabel
End Class
