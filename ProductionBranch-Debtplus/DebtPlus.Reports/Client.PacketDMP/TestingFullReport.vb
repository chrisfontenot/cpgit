Imports DebtPlus.Interfaces.Client
Imports DebtPlus.Reports.Template

Imports DebtPlus.Utils
Imports DevExpress.XtraReports.UI
Imports DebtPlus.Reports.Template.Forms
Imports System.IO
Imports System.Reflection
Imports DevExpress.XtraReports.Parameters

Namespace Client.PacketDMP
    Public Class TestingFullReport
        Inherits BaseXtraReportClass
        Implements IClient

        ''' <summary>
        '''     Create an instance of our report
        ''' </summary>
        Public Sub New()
            MyBase.New()
            ScriptReferences = DebtPlus.Utils.AssemblyTools.GetAllSiblingAssemblies()
            InitializeComponent()

            AddHandler BeforePrint, AddressOf FullReport_BeforePrint
            AddHandler XrSubreport_ActionPlan.BeforePrint, AddressOf XrSubreport_ActionPlan_BeforePrint
            AddHandler XrSubreport_BudgetDetail.BeforePrint, AddressOf XrSubreport_BudgetDetail_BeforePrint
            AddHandler XrSubreport_BudgetOverview.BeforePrint, AddressOf XrSubreport_BudgetOverview_BeforePrint
            AddHandler XrSubreport_DebtAssumptions.BeforePrint, AddressOf XrSubreport_DebtAssumptions_BeforePrint
            AddHandler XrSubreport_DebtDetail.BeforePrint, AddressOf XrSubreport_DebtDetail_BeforePrint
            AddHandler XrSubreport_DebtOverview.BeforePrint, AddressOf XrSubreport_DebtOverview_BeforePrint

            CType(Me, IClient).ClientId = -1

            ' Copy the script references to the subreports as well
            For Each Band As Band In Bands
                For Each ctl As XRControl In Band.Controls
                    Dim subReport As XRSubreport = TryCast(ctl, XRSubreport)
                    If subReport IsNot Nothing Then
                        Dim rpt As XtraReport = subReport.ReportSource
                        If rpt IsNot Nothing Then
                            rpt.ScriptReferences = ScriptReferences
                        End If
                    End If
                Next
            Next
        End Sub

        Public Property Parameter_Client() As Int32 Implements IClient.ClientId
            Get
                Return Convert.ToInt32(Parameters("ParameterClient").Value)
            End Get
            Set(ByVal value As Int32)
                Parameters("ParameterClient").Value = value
            End Set
        End Property

        Public Overrides Function NeedParameters() As Boolean
            Return MyBase.NeedParameters() OrElse (Parameter_Client <= 0)
        End Function

        Public Overrides ReadOnly Property ReportTitle() As String
            Get
                Return "Client Packet"
            End Get
        End Property

        Public Overrides Sub SetReportParameter(ByVal Parameter As String, ByVal Value As Object)
            Select Case Parameter
                Case "Client"
                    Parameter_Client = Convert.ToInt32(Value)
                Case Else
                    MyBase.SetReportParameter(Parameter, Value)
            End Select
        End Sub

        Public Overrides Function RequestReportParameters() As DialogResult
            Dim answer As DialogResult = DialogResult.OK
            If NeedParameters() Then
                With New ClientParametersForm()
                    answer = .ShowDialog
                    Parameter_Client = .Parameter_Client
                    .Dispose()
                End With
            End If
            Return answer
        End Function

        Private Sub InitializeComponent()
            Const ReportName = "DebtPlus.Reports.Client.Packet.Testing.repx"

            ' See if there is a report reference that we can use
            Dim Fname As String = Path.Combine(DebtPlus.Configuration.Config.ReportsDirectory, ReportName)
            If Not File.Exists(Fname) Then
                Dim Path As String = Assembly.GetExecutingAssembly.Location
                Path = IO.Path.GetDirectoryName(Path)
                Fname = IO.Path.Combine(Path, ReportName)
            End If

            Try
                LoadLayout(Fname)

            Catch ex As ArgumentException
                Throw New ApplicationException(String.Format("The report {0} can not be found in the reports share location", ReportName), ex)
            End Try

            ' Ensure that the parameters are defaulted to their "missing" status
            Parameter_Client = -1

            ' Ensure that the DevExpress parameter dialog is not used on the report.
            RequestParameters = False
            For Each parm As Parameter In Parameters
                parm.Visible = False
            Next

            ' Set the script references to the current (running) values.
            ScriptReferences = DebtPlus.Utils.AssemblyTools.GetAllSiblingAssemblies()

            ' Copy the script references to the subreports as well
            For Each Band As Band In Bands
                For Each ctl As XRControl In Band.Controls
                    Dim subReport As XRSubreport = TryCast(ctl, XRSubreport)
                    If subReport IsNot Nothing Then
                        Dim rpt As XtraReport = subReport.ReportSource
                        If rpt IsNot Nothing Then
                            rpt.ScriptReferences = ScriptReferences
                        End If
                    End If
                Next
            Next
        End Sub
    End Class
End Namespace
