Namespace Client.PacketDMP
    <Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
    Partial Class CommonReportPortrait

        'XtraReport overrides dispose to clean up the component list.
        <System.Diagnostics.DebuggerNonUserCode()> _
        Protected Overrides Sub Dispose(ByVal disposing As Boolean)
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
            MyBase.Dispose(disposing)
        End Sub

        'Required by the Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Designer
        'It can be modified using the Designer.  
        'Do not modify it using the code editor.
        <System.Diagnostics.DebuggerStepThrough()> _
        Private Sub InitializeComponent()
            Me.PageFooter = New DevExpress.XtraReports.UI.PageFooterBand()
            Me.XrLabel_ClientID = New DevExpress.XtraReports.UI.XRLabel()
            Me.PageHeader = New DevExpress.XtraReports.UI.PageHeaderBand()
            Me.XrLabel_PageHeader = New DevExpress.XtraReports.UI.XRLabel()
            CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
            '
            'Detail
            '
            Me.Detail.HeightF = 26.04167!
            '
            'PageFooter
            '
            Me.PageFooter.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel_ClientID})
            Me.PageFooter.HeightF = 47.91668!
            Me.PageFooter.Name = "PageFooter"
            '
            'XrLabel_ClientID
            '
            Me.XrLabel_ClientID.Font = New System.Drawing.Font("Gill Sans MT", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.XrLabel_ClientID.ForeColor = System.Drawing.Color.DarkGray
            Me.XrLabel_ClientID.LocationFloat = New DevExpress.Utils.PointFloat(597.9167!, 24.91668!)
            Me.XrLabel_ClientID.Name = "XrLabel_ClientID"
            Me.XrLabel_ClientID.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_ClientID.SizeF = New System.Drawing.SizeF(202.0833!, 23.0!)
            Me.XrLabel_ClientID.StylePriority.UseFont = False
            Me.XrLabel_ClientID.StylePriority.UseForeColor = False
            Me.XrLabel_ClientID.StylePriority.UseTextAlignment = False
            Me.XrLabel_ClientID.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight
            '
            'PageHeader
            '
            Me.PageHeader.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel_PageHeader})
            Me.PageHeader.HeightF = 105.2083!
            Me.PageHeader.Name = "PageHeader"
            '
            'XrLabel_PageHeader
            '
            Me.XrLabel_PageHeader.BackColor = System.Drawing.Color.Empty
            Me.XrLabel_PageHeader.BorderColor = System.Drawing.Color.Teal
            Me.XrLabel_PageHeader.Borders = CType((((DevExpress.XtraPrinting.BorderSide.Left Or DevExpress.XtraPrinting.BorderSide.Top) _
                                                    Or DevExpress.XtraPrinting.BorderSide.Right) _
                                                   Or DevExpress.XtraPrinting.BorderSide.Bottom), DevExpress.XtraPrinting.BorderSide)
            Me.XrLabel_PageHeader.BorderWidth = 6
            Me.XrLabel_PageHeader.Font = New System.Drawing.Font("Gill Sans MT", 24.0!, System.Drawing.FontStyle.Bold)
            Me.XrLabel_PageHeader.ForeColor = System.Drawing.Color.Teal
            Me.XrLabel_PageHeader.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
            Me.XrLabel_PageHeader.Name = "XrLabel_PageHeader"
            Me.XrLabel_PageHeader.Padding = New DevExpress.XtraPrinting.PaddingInfo(50, 2, 0, 12, 100.0!)
            Me.XrLabel_PageHeader.SizeF = New System.Drawing.SizeF(800.0!, 96.875!)
            Me.XrLabel_PageHeader.StylePriority.UseBackColor = False
            Me.XrLabel_PageHeader.StylePriority.UseBorderColor = False
            Me.XrLabel_PageHeader.StylePriority.UseBorders = False
            Me.XrLabel_PageHeader.StylePriority.UseBorderWidth = False
            Me.XrLabel_PageHeader.StylePriority.UseFont = False
            Me.XrLabel_PageHeader.StylePriority.UseForeColor = False
            Me.XrLabel_PageHeader.StylePriority.UsePadding = False
            Me.XrLabel_PageHeader.StylePriority.UseTextAlignment = False
            Me.XrLabel_PageHeader.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft
            '
            'CommonReportPortrait
            '
            Me.Bands.AddRange(New DevExpress.XtraReports.UI.Band() {Me.Detail, Me.PageFooter, Me.PageHeader, Me.TopMarginBand1, Me.BottomMarginBand1})
            Me.Font = New System.Drawing.Font("Gill Sans MT", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.Version = "11.2"
            Me.Controls.SetChildIndex(Me.BottomMarginBand1, 0)
            Me.Controls.SetChildIndex(Me.TopMarginBand1, 0)
            Me.Controls.SetChildIndex(Me.PageHeader, 0)
            Me.Controls.SetChildIndex(Me.PageFooter, 0)
            Me.Controls.SetChildIndex(Me.Detail, 0)
            CType(Me, System.ComponentModel.ISupportInitialize).EndInit()

        End Sub
        Protected Friend WithEvents PageFooter As DevExpress.XtraReports.UI.PageFooterBand
        Protected Friend WithEvents PageHeader As DevExpress.XtraReports.UI.PageHeaderBand
        Protected Friend WithEvents XrLabel_PageHeader As DevExpress.XtraReports.UI.XRLabel
        Protected Friend WithEvents XrLabel_ClientID As DevExpress.XtraReports.UI.XRLabel
    End Class
End Namespace
