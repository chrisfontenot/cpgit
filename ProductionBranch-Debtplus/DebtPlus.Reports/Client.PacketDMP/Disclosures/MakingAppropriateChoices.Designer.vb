Namespace Client.PacketDMP
    <Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
    Partial Class MakingAppropriateChoices

        'XtraReport overrides dispose to clean up the component list.
        <System.Diagnostics.DebuggerNonUserCode()> _
        Protected Overrides Sub Dispose(ByVal disposing As Boolean)
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
            MyBase.Dispose(disposing)
        End Sub

        'Required by the Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Designer
        'It can be modified using the Designer.  
        'Do not modify it using the code editor.
        <System.Diagnostics.DebuggerStepThrough()> _
        Private Sub InitializeComponent()
            Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(MakingAppropriateChoices))
            Me.XrRichText1 = New DevExpress.XtraReports.UI.XRRichText()
            CType(Me.XrRichText1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
            '
            'XrLabel_PageHeader
            '
            Me.XrLabel_PageHeader.BackColor = System.Drawing.Color.Sienna
            Me.XrLabel_PageHeader.StylePriority.UseBackColor = False
            Me.XrLabel_PageHeader.StylePriority.UseFont = False
            Me.XrLabel_PageHeader.StylePriority.UseForeColor = False
            Me.XrLabel_PageHeader.StylePriority.UsePadding = False
            Me.XrLabel_PageHeader.StylePriority.UseTextAlignment = False
            Me.XrLabel_PageHeader.Text = "Making Appropriate Choices"
            '
            'XrLabel_ClientID
            '
            Me.XrLabel_ClientID.StylePriority.UseFont = False
            Me.XrLabel_ClientID.StylePriority.UseForeColor = False
            Me.XrLabel_ClientID.StylePriority.UseTextAlignment = False
            '
            'Detail
            '
            Me.Detail.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrRichText1})
            Me.Detail.HeightF = 23.0!
            '
            'XrRichText1
            '
            Me.XrRichText1.LocationFloat = New DevExpress.Utils.PointFloat(50.0!, 0.0!)
            Me.XrRichText1.Name = "XrRichText1"
            Me.XrRichText1.SizeF = New System.Drawing.SizeF(750.0!, 23.0!)
            '
            'MakingAppropriateChoices
            '
            Me.Bands.AddRange(New DevExpress.XtraReports.UI.Band() {Me.Detail, Me.PageFooter, Me.PageHeader, Me.TopMarginBand1, Me.BottomMarginBand1})
            Me.RequestParameters = False
            Me.ScriptLanguage = DevExpress.XtraReports.ScriptLanguage.VisualBasic
            Me.ScriptReferencesString = resources.GetString("$this.ScriptReferencesString")
            Me.TitleColor = System.Drawing.Color.Sienna
            Me.TitleString = "DMP Enrollment Checklist"
            Me.Version = "11.2"
            CType(Me.XrRichText1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me, System.ComponentModel.ISupportInitialize).EndInit()

        End Sub
        Friend WithEvents XrRichText1 As DevExpress.XtraReports.UI.XRRichText
    End Class
End Namespace
