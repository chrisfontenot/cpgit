﻿Imports System.Drawing.Printing
Imports System.Text.RegularExpressions
Imports System.IO

Namespace Client.PacketDMP
    Public Class Survey
        Inherits CommonReportPortrait

        Public Sub New()
            MyBase.New()
            InitializeComponent()
            AddHandler XrRichText1.BeforePrint, AddressOf XrRichText1_BeforePrint
        End Sub

        Private Sub XrRichText1_BeforePrint(sender As Object, e As PrintEventArgs)
            Const Fname As String = "Survey.rtf"
            Dim sb As String = "{\rtf1}"

            ' Try to find the text document.
            Dim FileName As String = FindDocument(Fname)
            If FileName <> String.Empty Then
                Using fs As StreamReader = File.OpenText(FileName)
                    sb = fs.ReadToEnd()
                End Using
            End If

            ' Replace the font with the standard font name but keep the formatting.
            With CType(sender, DevExpress.XtraReports.UI.XRRichText)
                Dim rpt As DevExpress.XtraReports.UI.XtraReport = CType(.Report, DevExpress.XtraReports.UI.XtraReport)
#If 0 Then
            ' Generate the regular expression to process the font references
            Dim MatchEval As New System.Text.RegularExpressions.MatchEvaluator(AddressOf FontNameReplaceEvaluator)
            Dim rxPattern As New System.Text.RegularExpressions.Regex("\{(?<leadin>\\f\d[^ ]* )(?<font>[^;]+)\;\}")
            sb = rxPattern.Replace(sb, MatchEval)
#End If
                .Rtf = sb
            End With
        End Sub

        Public Function FontNameReplaceEvaluator(ByVal m As Match) As String

            Dim item As String = m.Groups("font").Value
            Dim prefix As String = m.Groups("leadin").Value
            If String.Compare(item, "symbol", True) <> 0 Then
                Return String.Format("{{{0}Gill Sans MT;}}", prefix)
            Else
                Return m.Value
            End If
        End Function
    End Class
End Namespace