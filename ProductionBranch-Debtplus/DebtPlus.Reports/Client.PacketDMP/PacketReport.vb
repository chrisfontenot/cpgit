Imports DebtPlus.Interfaces.Client
Imports DebtPlus.Reports.Template
Imports System.Drawing.Printing
Imports DebtPlus.Utils
Imports DebtPlus.Reports.Template.Forms
Imports System.Data.SqlClient

Namespace Client.PacketDMP
    Public Class PacketReport
        Inherits BaseXtraReportClass
        Implements IClient


        ''' <summary>
        ''' Create an instance of our report
        ''' </summary>
        Public Sub New()
            MyBase.New()
            ScriptReferences = DebtPlus.Utils.AssemblyTools.GetAllSiblingAssemblies()
            InitializeComponent()

            AddHandler BeforePrint, AddressOf FullReport_BeforePrint
            AddHandler XrSubreport_BudgetOverview.BeforePrint, AddressOf XrSubreport_BudgetOverview_BeforePrint
            AddHandler XrSubreport_BudgetDetail.BeforePrint, AddressOf XrSubreport_BudgetDetail_BeforePrint
            AddHandler XrSubreport_ActionPlan.BeforePrint, AddressOf XrSubreport_ActionPlan_BeforePrint
            AddHandler XrSubreport_Surplus.BeforePrint, AddressOf XrSubreport_Surplus_BeforePrint
            AddHandler XrSubreport_DebtOverview.BeforePrint, AddressOf XrSubreport_DebtOverview_BeforePrint
            AddHandler XrSubreport_DebtDetail.BeforePrint, AddressOf XrSubreport_DebtDetail_BeforePrint
            AddHandler XrSubreport_DebtAssumptions.BeforePrint, AddressOf XrSubreport_DebtAssumptions_BeforePrint

            CType(Me, IClient).ClientId = -1

            ' Copy the script references to the subreports as well
            For Each Band As DevExpress.XtraReports.UI.Band In Bands
                For Each ctl As DevExpress.XtraReports.UI.XRControl In Band.Controls
                    Dim subReport As DevExpress.XtraReports.UI.XRSubreport = TryCast(ctl, DevExpress.XtraReports.UI.XRSubreport)
                    If subReport IsNot Nothing Then
                        Dim rpt As DevExpress.XtraReports.UI.XtraReport = subReport.ReportSource
                        If rpt IsNot Nothing Then
                            rpt.ScriptReferences = ScriptReferences
                        End If
                    End If
                Next
            Next
        End Sub

        Public Property Parameter_Client() As Int32 Implements IClient.ClientId
            Get
                Return Convert.ToInt32(Parameters("ParameterClient").Value)
            End Get
            Set(ByVal value As Int32)
                Parameters("ParameterClient").Value = value
            End Set
        End Property

        Public Overrides Function NeedParameters() As Boolean
            Return MyBase.NeedParameters() OrElse (Parameter_Client <= 0)
        End Function

        Public Overrides ReadOnly Property ReportTitle() As String
            Get
                Return "Client Packet"
            End Get
        End Property

        Public Overrides Sub SetReportParameter(ByVal Parameter As String, ByVal Value As Object)
            Select Case Parameter
                Case "Client"
                    Parameter_Client = Convert.ToInt32(Value)
                Case Else
                    MyBase.SetReportParameter(Parameter, Value)
            End Select
        End Sub

        Public Overrides Function RequestReportParameters() As DialogResult
            Dim answer As DialogResult = DialogResult.OK
            If NeedParameters() Then
                With New ClientParametersForm()
                    answer = .ShowDialog()
                    Parameter_Client = .Parameter_Client
                    .Dispose()
                End With
            End If
            Return answer
        End Function

        Friend ReadOnly ds As New DataSet("ds")

        Private Sub FullReport_BeforePrint(sender As Object, e As PrintEventArgs)
            Dim rpt As DevExpress.XtraReports.UI.XtraReport = CType(sender, DevExpress.XtraReports.UI.XtraReport)

            ' Generate the information for the appended reports
            Dim cn As System.Data.SqlClient.SqlConnection = New System.Data.SqlClient.SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
            Try
                cn.Open()

                ' Comparison informaiton
                Using cmd As New System.Data.SqlClient.SqlCommand()
                    With cmd
                        .Connection = cn
                        .CommandText = "rpt_PrintClient_CreditorDebt" ' "rpt_PrintClient_ComparisonByClient"
                        .CommandType = CommandType.StoredProcedure
                        .Parameters.Add("@Client", SqlDbType.Int).Value = rpt.Parameters("ParameterClient").Value
                    End With

                    Using da As New SqlDataAdapter(cmd)
                        da.Fill(ds, "rpt_PrintClient_CreditorDebt")
                        With ds.Tables("rpt_PrintClient_CreditorDebt")
                            .Columns.Add("combined_creditor_name", GetType(String), "[creditor_name] + ' #' + [account_number]")
                        End With
                    End Using
                End Using

                ' Budget information
                Using cmd As New System.Data.SqlClient.SqlCommand()
                    With cmd
                        .Connection = cn
                        .CommandText = "rpt_PrintClient_BudgetDetail_ByClient"
                        .CommandType = CommandType.StoredProcedure
                        .Parameters.Add("@Client", SqlDbType.Int).Value = rpt.Parameters("ParameterClient").Value
                    End With

                    Using da As New SqlDataAdapter(cmd)
                        da.Fill(ds, "BudgetDetails")
                        Dim tbl As DataTable = ds.Tables("BudgetDetails")
                        With tbl
                            .Columns.Add("difference", GetType(Decimal), "[client_amount]-[suggested_amount]")
                        End With
                    End Using
                End Using

                ' Suprplus/Deficit figures
                Using cmd As New System.Data.SqlClient.SqlCommand()
                    With cmd
                        .Connection = cn
                        .CommandText = "rpt_PrintClient_BottomLine"
                        .CommandType = CommandType.StoredProcedure
                        .Parameters.Add("@Client", SqlDbType.Int).Value = rpt.Parameters("ParameterClient").Value
                    End With

                    Using da As New SqlDataAdapter(cmd)
                        da.Fill(ds, "BottomLine")
                        Dim tbl As DataTable = ds.Tables("BottomLine")
                        With tbl.Columns
                            .Add("budget_expenses", GetType(Decimal), "[living_expenses] + [housing_expenses]")
                            .Add("total_expenses", GetType(Decimal), "[living_expenses] + [housing_expenses] + [other_expenses]")
                            .Add("total_assets", GetType(Decimal), "[payroll_1] + [payroll_2] + [assets]")
                            .Add("available_funds", GetType(Decimal), "[payroll_1] + [payroll_2] + [assets] - [living_expenses] - [housing_expenses] - [other_expenses] - [other_debt]")
                            .Add("grand_total", GetType(Decimal), "[payroll_1] + [payroll_2] + [assets] - [living_expenses] - [housing_expenses] - [other_expenses] - [other_debt] - [program_fees]")
                        End With
                    End Using
                End Using

                ' Action Plan
                Dim ActionPlan As Int32 = -1

                If ActionPlan <= 0 Then
                    Using cmd As New System.Data.SqlClient.SqlCommand()
                        With cmd
                            .Connection = cn
                            .CommandText = "SELECT dbo.map_client_to_action_plan(@client)"
                            .CommandType = CommandType.Text
                            .Parameters.Add("@client", SqlDbType.Int).Value = rpt.Parameters("ParameterClient").Value
                            Dim obj As Object = .ExecuteScalar
                            ActionPlan = DebtPlus.Utils.Nulls.DInt(obj)
                        End With
                    End Using
                End If

                If ActionPlan > 0 Then
                    Using cmd As New System.Data.SqlClient.SqlCommand()
                        With cmd
                            .Connection = cn
                            .CommandText = "SELECT [text] as 'Text' FROM [action_items_goals] WITH (NOLOCK) WHERE [action_plan] = @ActionPlan"
                            .CommandType = CommandType.Text
                            .CommandTimeout = 0
                            .Parameters.Add("@ActionPlan", SqlDbType.Int).Value = ActionPlan
                        End With

                        Using da As New SqlDataAdapter(cmd)
                            da.Fill(ds, "ActionPlanGoals")
                        End Using
                    End Using

                    Using cmd As New System.Data.SqlClient.SqlCommand()
                        With cmd
                            .Connection = cn
                            .CommandText = "rpt_PrintClient_ActionPlan_ByPlan"
                            .CommandType = CommandType.StoredProcedure
                            .CommandTimeout = 0
                            .Parameters.Add("@ActionPlan", SqlDbType.Int).Value = ActionPlan
                        End With

                        Using da As New SqlDataAdapter(cmd)
                            da.Fill(ds, "ActionPlanItems")
                        End Using
                    End Using
                End If

                ' Main report
                Using cmd As New System.Data.SqlClient.SqlCommand()
                    With cmd
                        .Connection = cn
                        .CommandText = "rpt_PrintClient"
                        .CommandType = CommandType.StoredProcedure
                        .Parameters.Add("@Client", SqlDbType.Int).Value = rpt.Parameters("ParameterClient").Value
                    End With

                    Using da As New SqlDataAdapter(cmd)
                        da.Fill(ds, "PrintClient")
                        Dim tbl As DataTable = ds.Tables("PrintClient")
                        With CType(sender, DevExpress.XtraReports.UI.XtraReport)
                            .DataSource = ds.Tables("PrintClient").DefaultView
                        End With
                    End Using
                End Using

            Finally
                If cn IsNot Nothing Then cn.Dispose()
            End Try
        End Sub

        Private Sub XrSubreport_BudgetOverview_BeforePrint(sender As Object, e As PrintEventArgs)
            With CType(sender, DevExpress.XtraReports.UI.XRSubreport)
                Dim tbl As DataTable = ds.Tables("BudgetDetails")
                If tbl IsNot Nothing Then
                    Using vue As New DataView(tbl, "[heading]=0", "heading_category", DataViewRowState.CurrentRows)
                        If vue.Count <= 0 Then
                            e.Cancel = True
                        End If
                    End Using
                End If
            End With
        End Sub

        Private Sub XrSubreport_BudgetDetail_BeforePrint(sender As Object, e As PrintEventArgs)
            With CType(sender, DevExpress.XtraReports.UI.XRSubreport)
                Dim rpt As DevExpress.XtraReports.UI.XtraReport = .ReportSource
                Dim tbl As DataTable = ds.Tables("BudgetDetails")

                If tbl IsNot Nothing Then
                    Dim vue As New DataView(tbl, "[heading]=0", "heading_category", DataViewRowState.CurrentRows)
                    If vue.Count > 0 Then
                        rpt.DataSource = vue
                        For Each calc As DevExpress.XtraReports.UI.CalculatedField In rpt.CalculatedFields
                            calc.Assign(calc.DataSource, calc.DataMember)
                        Next
                        Return
                    End If
                End If

                ' Cancel the printing if there is nothing to be printed
                e.Cancel = True
            End With
        End Sub

        Private Sub XrSubreport_DebtOverview_BeforePrint(sender As Object, e As PrintEventArgs)
            With CType(sender, DevExpress.XtraReports.UI.XRSubreport)
                Dim tbl As DataTable = ds.Tables("rpt_PrintClient_CreditorDebt")
                If tbl Is Nothing OrElse tbl.Rows.Count <= 0 Then
                    e.Cancel = True
                End If
            End With
        End Sub

        Private Sub XrSubreport_DebtAssumptions_BeforePrint(sender As Object, e As PrintEventArgs)
            With CType(sender, DevExpress.XtraReports.UI.XRSubreport)
                Dim rpt As DevExpress.XtraReports.UI.XtraReport = .ReportSource
                Dim tbl As DataTable = ds.Tables("rpt_PrintClient_CreditorDebt")

                If tbl IsNot Nothing Then
                    If tbl.Rows.Count > 0 Then
                        For Each calc As DevExpress.XtraReports.UI.CalculatedField In rpt.CalculatedFields
                            calc.Assign(calc.DataSource, calc.DataMember)
                        Next
                    End If
                Else
                    e.Cancel = True
                End If
            End With
        End Sub

        Private Sub XrSubreport_ActionPlan_BeforePrint(sender As Object, e As PrintEventArgs)
            With CType(sender, DevExpress.XtraReports.UI.XRSubreport)
                Dim rpt As DevExpress.XtraReports.UI.XtraReport = .ReportSource

                ' Determine if there is anything to print on the subreport
                Dim GoalsCount As Int32 = 0
                Dim ItemsCount As Int32 = 0
                If ds.Tables("ActionPlanGoals") IsNot Nothing Then
                    GoalsCount = ds.Tables("ActionPlanGoals").Rows.Count
                End If

                If ds.Tables("ActionPlanItems") IsNot Nothing Then
                    ItemsCount = ds.Tables("ActionPlanItems").Rows.Count
                End If

                If GoalsCount <= 0 AndAlso ItemsCount <= 0 Then
                    e.Cancel = True
                End If
            End With
        End Sub

        Private Sub XrSubreport_DebtDetail_BeforePrint(sender As Object, e As PrintEventArgs)
            With CType(sender, DevExpress.XtraReports.UI.XRSubreport)
                Dim rpt As DevExpress.XtraReports.UI.XtraReport = .ReportSource
                Dim tbl As DataTable = ds.Tables("rpt_PrintClient_CreditorDebt")

                If tbl IsNot Nothing AndAlso tbl.Rows.Count > 0 Then
                    rpt.DataSource = tbl.DefaultView
                    For Each calc As DevExpress.XtraReports.UI.CalculatedField In rpt.CalculatedFields
                        calc.Assign(rpt.DataSource, rpt.DataMember)
                    Next
                Else
                    e.Cancel = True
                End If
            End With
        End Sub

        Private Sub XrSubreport_Surplus_BeforePrint(ByVal sender As Object, ByVal e As PrintEventArgs)
            With CType(sender, DevExpress.XtraReports.UI.XRSubreport)
                Dim rpt As DevExpress.XtraReports.UI.XtraReport = .ReportSource
                Dim tbl As DataTable = ds.Tables("BottomLine")

                If tbl IsNot Nothing AndAlso tbl.Rows.Count > 0 Then
                    rpt.DataSource = tbl.DefaultView
                    For Each calc As DevExpress.XtraReports.UI.CalculatedField In rpt.CalculatedFields
                        calc.Assign(calc.DataSource, calc.DataMember)
                    Next
                Else
                    e.Cancel = True
                End If
            End With
        End Sub
    End Class
End Namespace
