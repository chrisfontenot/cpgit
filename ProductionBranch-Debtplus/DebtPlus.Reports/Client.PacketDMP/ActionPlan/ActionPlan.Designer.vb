Namespace Client.PacketDMP
    <Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
    Partial Class ActionPlan

        'XtraReport overrides dispose to clean up the component list.
        <System.Diagnostics.DebuggerNonUserCode()> _
        Protected Overrides Sub Dispose(ByVal disposing As Boolean)
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
            MyBase.Dispose(disposing)
        End Sub

        'Required by the Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Designer
        'It can be modified using the Designer.  
        'Do not modify it using the code editor.
        <System.Diagnostics.DebuggerStepThrough()> _
        Private Sub InitializeComponent()
            Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(ActionPlan))
            Me.XrControlStyle_PageHeaderBlock = New DevExpress.XtraReports.UI.XRControlStyle()
            Me.XrRichText_TextBuffer = New DevExpress.XtraReports.UI.XRRichText()
            Me.XrLabel1 = New DevExpress.XtraReports.UI.XRLabel()
            Me.GroupHeader1 = New DevExpress.XtraReports.UI.GroupHeaderBand()
            Me.GroupFooter1 = New DevExpress.XtraReports.UI.GroupFooterBand()
            Me.XrTable1 = New DevExpress.XtraReports.UI.XRTable()
            Me.XrTableRow1 = New DevExpress.XtraReports.UI.XRTableRow()
            Me.XrTableCell6 = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableCell1 = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableRow2 = New DevExpress.XtraReports.UI.XRTableRow()
            Me.XrTableCell2 = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableRow3 = New DevExpress.XtraReports.UI.XRTableRow()
            Me.XrTableCell3 = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableRow4 = New DevExpress.XtraReports.UI.XRTableRow()
            Me.XrTableCell4 = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableRow5 = New DevExpress.XtraReports.UI.XRTableRow()
            Me.XrTableCell5 = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrRichText_Concessions = New DevExpress.XtraReports.UI.XRRichText()
            Me.XrRichText_ActionItems = New DevExpress.XtraReports.UI.XRRichText()
            Me.XrLabel_GoalsHeader = New DevExpress.XtraReports.UI.XRLabel()
            CType(Me.XrRichText_TextBuffer, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.XrTable1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.XrRichText_Concessions, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.XrRichText_ActionItems, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
            '
            'XrLabel_PageHeader
            '
            Me.XrLabel_PageHeader.BorderColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Int32), CType(CType(173, Byte), Int32), CType(CType(59, Byte), Int32))
            Me.XrLabel_PageHeader.ForeColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Int32), CType(CType(173, Byte), Int32), CType(CType(59, Byte), Int32))
            Me.XrLabel_PageHeader.StylePriority.UseBackColor = False
            Me.XrLabel_PageHeader.StylePriority.UseBorderColor = False
            Me.XrLabel_PageHeader.StylePriority.UseBorders = False
            Me.XrLabel_PageHeader.StylePriority.UseBorderWidth = False
            Me.XrLabel_PageHeader.StylePriority.UseFont = False
            Me.XrLabel_PageHeader.StylePriority.UseForeColor = False
            Me.XrLabel_PageHeader.StylePriority.UsePadding = False
            Me.XrLabel_PageHeader.StylePriority.UseTextAlignment = False
            Me.XrLabel_PageHeader.Text = "Action Plan"
            '
            'XrLabel_ClientID
            '
            Me.XrLabel_ClientID.LocationFloat = New DevExpress.Utils.PointFloat(597.9167!, 22.83338!)
            Me.XrLabel_ClientID.StylePriority.UseFont = False
            Me.XrLabel_ClientID.StylePriority.UseForeColor = False
            Me.XrLabel_ClientID.StylePriority.UseTextAlignment = False
            '
            'Detail
            '
            Me.Detail.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel_GoalsHeader, Me.XrRichText_ActionItems, Me.XrRichText_TextBuffer, Me.XrRichText_Concessions})
            Me.Detail.HeightF = 118.2084!
            Me.Detail.MultiColumn.ColumnCount = 2
            Me.Detail.MultiColumn.ColumnSpacing = 20.0!
            Me.Detail.MultiColumn.Mode = DevExpress.XtraReports.UI.MultiColumnMode.UseColumnCount
            Me.Detail.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
            Me.Detail.PageBreak = DevExpress.XtraReports.UI.PageBreak.AfterBand
            Me.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
            '
            'XrControlStyle_PageHeaderBlock
            '
            Me.XrControlStyle_PageHeaderBlock.Font = New System.Drawing.Font("Gill Sans MT", 24.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.XrControlStyle_PageHeaderBlock.ForeColor = System.Drawing.Color.White
            Me.XrControlStyle_PageHeaderBlock.Name = "XrControlStyle_PageHeaderBlock"
            Me.XrControlStyle_PageHeaderBlock.Padding = New DevExpress.XtraPrinting.PaddingInfo(50, 2, 0, 12, 100.0!)
            Me.XrControlStyle_PageHeaderBlock.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft
            '
            'XrRichText_TextBuffer
            '
            Me.XrRichText_TextBuffer.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 23.0!)
            Me.XrRichText_TextBuffer.Name = "XrRichText_TextBuffer"
            Me.XrRichText_TextBuffer.SizeF = New System.Drawing.SizeF(388.5417!, 23.0!)
            '
            'XrLabel1
            '
            Me.XrLabel1.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
            Me.XrLabel1.Name = "XrLabel1"
            Me.XrLabel1.SizeF = New System.Drawing.SizeF(100.0!, 23.0!)
            '
            'GroupHeader1
            '
            Me.GroupHeader1.HeightF = 0.0!
            Me.GroupHeader1.Name = "GroupHeader1"
            '
            'GroupFooter1
            '
            Me.GroupFooter1.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrTable1})
            Me.GroupFooter1.HeightF = 238.125!
            Me.GroupFooter1.Name = "GroupFooter1"
            Me.GroupFooter1.PrintAtBottom = True
            '
            'XrTable1
            '
            Me.XrTable1.BorderColor = System.Drawing.Color.DarkGray
            Me.XrTable1.ForeColor = System.Drawing.Color.DarkGray
            Me.XrTable1.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 10.00001!)
            Me.XrTable1.Name = "XrTable1"
            Me.XrTable1.Rows.AddRange(New DevExpress.XtraReports.UI.XRTableRow() {Me.XrTableRow1, Me.XrTableRow2, Me.XrTableRow3, Me.XrTableRow4, Me.XrTableRow5})
            Me.XrTable1.SizeF = New System.Drawing.SizeF(300.0!, 228.125!)
            Me.XrTable1.StylePriority.UseBorderColor = False
            Me.XrTable1.StylePriority.UseForeColor = False
            '
            'XrTableRow1
            '
            Me.XrTableRow1.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.XrTableCell6, Me.XrTableCell1})
            Me.XrTableRow1.Name = "XrTableRow1"
            Me.XrTableRow1.Weight = 1.5318983079350079R
            '
            'XrTableCell6
            '
            Me.XrTableCell6.Font = New System.Drawing.Font("Wingdings", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(2, Byte))
            Me.XrTableCell6.ForeColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Int32), CType(CType(173, Byte), Int32), CType(CType(59, Byte), Int32))
            Me.XrTableCell6.Name = "XrTableCell6"
            Me.XrTableCell6.StylePriority.UseFont = False
            Me.XrTableCell6.StylePriority.UseForeColor = False
            Me.XrTableCell6.Text = "è"
            Me.XrTableCell6.Weight = 0.20833328247070315R
            '
            'XrTableCell1
            '
            Me.XrTableCell1.ForeColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Int32), CType(CType(173, Byte), Int32), CType(CType(59, Byte), Int32))
            Me.XrTableCell1.Name = "XrTableCell1"
            Me.XrTableCell1.StylePriority.UseForeColor = False
            Me.XrTableCell1.Text = "I HAVE READ AND UNDERSTOOD THE INFORMATION PRESENTED HERE"
            Me.XrTableCell1.Weight = 2.7916667175292966R
            '
            'XrTableRow2
            '
            Me.XrTableRow2.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.XrTableCell2})
            Me.XrTableRow2.Name = "XrTableRow2"
            Me.XrTableRow2.Weight = 0.8483003061618466R
            '
            'XrTableCell2
            '
            Me.XrTableCell2.BorderColor = System.Drawing.Color.DarkGray
            Me.XrTableCell2.Borders = DevExpress.XtraPrinting.BorderSide.Top
            Me.XrTableCell2.BorderWidth = 2
            Me.XrTableCell2.Name = "XrTableCell2"
            Me.XrTableCell2.StylePriority.UseBorderColor = False
            Me.XrTableCell2.StylePriority.UseBorders = False
            Me.XrTableCell2.StylePriority.UseBorderWidth = False
            Me.XrTableCell2.Text = "Signature"
            Me.XrTableCell2.Weight = 3.0R
            '
            'XrTableRow3
            '
            Me.XrTableRow3.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.XrTableCell3})
            Me.XrTableRow3.Name = "XrTableRow3"
            Me.XrTableRow3.Weight = 1.1832572668744799R
            '
            'XrTableCell3
            '
            Me.XrTableCell3.BorderColor = System.Drawing.Color.DarkGray
            Me.XrTableCell3.Borders = DevExpress.XtraPrinting.BorderSide.Top
            Me.XrTableCell3.BorderWidth = 2
            Me.XrTableCell3.Name = "XrTableCell3"
            Me.XrTableCell3.StylePriority.UseBorderColor = False
            Me.XrTableCell3.StylePriority.UseBorders = False
            Me.XrTableCell3.StylePriority.UseBorderWidth = False
            Me.XrTableCell3.Text = "Date"
            Me.XrTableCell3.Weight = 3.0R
            '
            'XrTableRow4
            '
            Me.XrTableRow4.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.XrTableCell4})
            Me.XrTableRow4.Name = "XrTableRow4"
            Me.XrTableRow4.Weight = 0.71827229760323585R
            '
            'XrTableCell4
            '
            Me.XrTableCell4.BorderColor = System.Drawing.Color.DarkGray
            Me.XrTableCell4.Borders = DevExpress.XtraPrinting.BorderSide.Top
            Me.XrTableCell4.BorderWidth = 2
            Me.XrTableCell4.Name = "XrTableCell4"
            Me.XrTableCell4.StylePriority.UseBorderColor = False
            Me.XrTableCell4.StylePriority.UseBorders = False
            Me.XrTableCell4.StylePriority.UseBorderWidth = False
            Me.XrTableCell4.Text = "Signature"
            Me.XrTableCell4.Weight = 3.0R
            '
            'XrTableRow5
            '
            Me.XrTableRow5.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.XrTableCell5})
            Me.XrTableRow5.Name = "XrTableRow5"
            Me.XrTableRow5.Weight = 0.46430395498197724R
            '
            'XrTableCell5
            '
            Me.XrTableCell5.BorderColor = System.Drawing.Color.DarkGray
            Me.XrTableCell5.Borders = DevExpress.XtraPrinting.BorderSide.Top
            Me.XrTableCell5.BorderWidth = 2
            Me.XrTableCell5.Name = "XrTableCell5"
            Me.XrTableCell5.StylePriority.UseBorderColor = False
            Me.XrTableCell5.StylePriority.UseBorders = False
            Me.XrTableCell5.StylePriority.UseBorderWidth = False
            Me.XrTableCell5.Text = "Date"
            Me.XrTableCell5.Weight = 3.0R
            '
            'XrRichText_Concessions
            '
            Me.XrRichText_Concessions.KeepTogether = True
            Me.XrRichText_Concessions.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 95.20839!)
            Me.XrRichText_Concessions.Name = "XrRichText_Concessions"
            Me.XrRichText_Concessions.SerializableRtfString = resources.GetString("XrRichText_Concessions.SerializableRtfString")
            Me.XrRichText_Concessions.SizeF = New System.Drawing.SizeF(388.5417!, 23.0!)
            '
            'XrRichText_ActionItems
            '
            Me.XrRichText_ActionItems.KeepTogether = True
            Me.XrRichText_ActionItems.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 59.10419!)
            Me.XrRichText_ActionItems.Name = "XrRichText_ActionItems"
            Me.XrRichText_ActionItems.SizeF = New System.Drawing.SizeF(388.5417!, 23.0!)
            '
            'XrLabel_GoalsHeader
            '
            Me.XrLabel_GoalsHeader.Font = New System.Drawing.Font("Gill Sans MT", 14.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Underline), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.XrLabel_GoalsHeader.ForeColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Int32), CType(CType(173, Byte), Int32), CType(CType(59, Byte), Int32))
            Me.XrLabel_GoalsHeader.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
            Me.XrLabel_GoalsHeader.Name = "XrLabel_GoalsHeader"
            Me.XrLabel_GoalsHeader.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_GoalsHeader.SizeF = New System.Drawing.SizeF(388.5417!, 23.0!)
            Me.XrLabel_GoalsHeader.StylePriority.UseFont = False
            Me.XrLabel_GoalsHeader.StylePriority.UseForeColor = False
            Me.XrLabel_GoalsHeader.Text = "Goals and Objectives"
            '
            'ActionPlan
            '
            Me.Bands.AddRange(New DevExpress.XtraReports.UI.Band() {Me.TopMarginBand1, Me.Detail, Me.BottomMarginBand1, Me.PageFooter, Me.PageHeader, Me.GroupHeader1, Me.GroupFooter1})
            Me.ScriptLanguage = DevExpress.XtraReports.ScriptLanguage.VisualBasic
            Me.ScriptReferencesString = resources.GetString("$this.ScriptReferencesString")
            Me.ScriptsSource = Nothing
            Me.StyleSheet.AddRange(New DevExpress.XtraReports.UI.XRControlStyle() {Me.XrControlStyle_PageHeaderBlock})
            Me.TitleColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Int32), CType(CType(173, Byte), Int32), CType(CType(59, Byte), Int32))
            Me.TitleString = "Action Plan"
            Me.Version = "11.2"
            Me.Controls.SetChildIndex(Me.GroupFooter1, 0)
            Me.Controls.SetChildIndex(Me.GroupHeader1, 0)
            Me.Controls.SetChildIndex(Me.PageHeader, 0)
            Me.Controls.SetChildIndex(Me.PageFooter, 0)
            Me.Controls.SetChildIndex(Me.BottomMarginBand1, 0)
            Me.Controls.SetChildIndex(Me.Detail, 0)
            Me.Controls.SetChildIndex(Me.TopMarginBand1, 0)
            CType(Me.XrRichText_TextBuffer, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.XrTable1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.XrRichText_Concessions, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.XrRichText_ActionItems, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me, System.ComponentModel.ISupportInitialize).EndInit()

        End Sub
        Protected Friend WithEvents XrLabel1 As DevExpress.XtraReports.UI.XRLabel
        Protected Friend WithEvents GroupHeader1 As DevExpress.XtraReports.UI.GroupHeaderBand
        Protected Friend WithEvents GroupFooter1 As DevExpress.XtraReports.UI.GroupFooterBand
        Protected Friend WithEvents XrTable1 As DevExpress.XtraReports.UI.XRTable
        Protected Friend WithEvents XrTableRow1 As DevExpress.XtraReports.UI.XRTableRow
        Protected Friend WithEvents XrTableCell6 As DevExpress.XtraReports.UI.XRTableCell
        Protected Friend WithEvents XrTableCell1 As DevExpress.XtraReports.UI.XRTableCell
        Protected Friend WithEvents XrTableRow2 As DevExpress.XtraReports.UI.XRTableRow
        Protected Friend WithEvents XrTableCell2 As DevExpress.XtraReports.UI.XRTableCell
        Protected Friend WithEvents XrTableRow3 As DevExpress.XtraReports.UI.XRTableRow
        Protected Friend WithEvents XrTableCell3 As DevExpress.XtraReports.UI.XRTableCell
        Protected Friend WithEvents XrTableRow4 As DevExpress.XtraReports.UI.XRTableRow
        Protected Friend WithEvents XrTableCell4 As DevExpress.XtraReports.UI.XRTableCell
        Protected Friend WithEvents XrTableRow5 As DevExpress.XtraReports.UI.XRTableRow
        Protected Friend WithEvents XrTableCell5 As DevExpress.XtraReports.UI.XRTableCell
        Protected Friend WithEvents XrRichText_TextBuffer As DevExpress.XtraReports.UI.XRRichText
        Protected Friend WithEvents XrRichText_ActionItems As DevExpress.XtraReports.UI.XRRichText
        Protected Friend WithEvents XrLabel_GoalsHeader As DevExpress.XtraReports.UI.XRLabel
        Protected Friend WithEvents XrRichText_Concessions As DevExpress.XtraReports.UI.XRRichText
        Friend WithEvents XrControlStyle_PageHeaderBlock As DevExpress.XtraReports.UI.XRControlStyle
    End Class
End Namespace
