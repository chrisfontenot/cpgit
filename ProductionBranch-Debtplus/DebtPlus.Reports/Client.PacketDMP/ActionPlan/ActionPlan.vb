﻿Imports System.Drawing.Printing
Imports System.Text.RegularExpressions
Imports System.Text
Imports System.IO

Namespace Client.PacketDMP

    Public Class ActionPlan
        Inherits CommonReportPortrait

        Public Sub New()
            MyBase.New()
            InitializeComponent()
            AddHandler XrRichText_TextBuffer.BeforePrint, AddressOf XrRichText_TextBuffer_BeforePrint
            AddHandler XrRichText_ActionItems.BeforePrint, AddressOf XrRichText_ActionItems_BeforePrint
            AddHandler XrLabel_GoalsHeader.BeforePrint, AddressOf XrLabel_GoalsHeader_BeforePrint
            AddHandler XrRichText_Concessions.BeforePrint, AddressOf XrRichText_Concessions_BeforePrint
        End Sub

        Private Sub XrRichText_TextBuffer_BeforePrint(sender As Object, e As PrintEventArgs)
            Dim ctl As DevExpress.XtraReports.UI.XRRichText = CType(sender, DevExpress.XtraReports.UI.XRRichText)
            Dim rpt As DevExpress.XtraReports.UI.XtraReport = CType(ctl.Report, DevExpress.XtraReports.UI.XtraReport)
            Dim MasterRpt As DevExpress.XtraReports.UI.XtraReport = rpt.MasterReport
            Dim ds As DataSet = CType(MasterRpt.DataSource, DataView).Table.DataSet
            Dim tbl As DataTable = ds.Tables("ActionPlanGoals")

            ' Find the text for the action plan goal
            Dim txt As String = String.Empty
            If tbl.Rows.Count > 0 Then
                Dim row As DataRow = tbl.Rows(0)
                txt = DebtPlus.Utils.Nulls.DStr(row("Text")).Trim()
            End If

            ' Insert the formatted text into the control. Action plan goals are always RTF if they are anything.
            If txt.Length > 0 Then

                ' Generate the regular expression to process the font references
                Dim MatchEval As New MatchEvaluator(AddressOf FontNameReplaceEvaluator)
                Dim rxPattern As New Regex("\{(?<leadin>\\f\d[^ ]* )(?<font>[^;]+)\;\}")
                txt = rxPattern.Replace(txt, MatchEval)

                ctl.Rtf = txt
            Else
                ctl.Text = String.Empty
                e.Cancel = True
            End If
        End Sub

        Public Function FontNameReplaceEvaluator(ByVal m As Match) As String

            Dim item As String = m.Groups("font").Value
            Dim prefix As String = m.Groups("leadin").Value
            If String.Compare(item, "symbol", True) <> 0 Then
                Return String.Format("{{{0}Gill Sans MT;}}", prefix)
            Else
                Return m.Value
            End If
        End Function

        Private Sub XrRichText_ActionItems_BeforePrint(sender As Object, e As PrintEventArgs)
            Dim ctl As DevExpress.XtraReports.UI.XRRichText = CType(sender, DevExpress.XtraReports.UI.XRRichText)
            Dim rpt As DevExpress.XtraReports.UI.XtraReport = CType(ctl.Report, DevExpress.XtraReports.UI.XtraReport)
            Dim MasterRpt As DevExpress.XtraReports.UI.XtraReport = CType(rpt.MasterReport, DevExpress.XtraReports.UI.XtraReport)
            Dim ds As DataSet = CType(MasterRpt.DataSource, DataView).Table.DataSet

            Dim tbl As DataTable = ds.Tables("ActionPlanItems")
            Dim txt As String = FindActionItemsHTML(tbl)

            ' Insert the HTML text for the action item list
            If txt.Length > 0 Then

                ' Set the HTML text into the control
                ctl.Html = String.Format("<html><head></head><body>{0}</body></html>", txt)
            Else
                ctl.Text = String.Empty
                e.Cancel = True
            End If
        End Sub

        Private Function FindActionItemsHTML(ByRef tbl As DataTable)
            Dim sb As New StringBuilder

            ' Look for the items to be done
            Using vue As New DataView(tbl, "[item_group]=1 AND [checked]<>False", "description", DataViewRowState.CurrentRows)
                sb.Append(FindActionItemGroup(vue, "Things To Do"))
            End Using

            ' Look for the items to increase income
            Using vue As New DataView(tbl, "[item_group]=2 AND [checked]<>False", "description", DataViewRowState.CurrentRows)
                sb.Append(FindActionItemGroup(vue, "Items To Increase Income"))
            End Using

            ' Look for the items to reduce expenses
            Using vue As New DataView(tbl, "[item_group]=3 AND [checked]<>False", "description", DataViewRowState.CurrentRows)
                sb.Append(FindActionItemGroup(vue, "Items To Reduce Expenses"))
            End Using

            ' Look for the items needed on a DMP
            Using vue As New DataView(tbl, "[item_group]=4 AND [checked]<>False", "description", DataViewRowState.CurrentRows)
                sb.Append(FindActionItemGroup(vue, "Items Needed for a DMP plan"))
            End Using

            Return sb.ToString()
        End Function

        Private Function FindActionItemGroup(ByRef vue As DataView, ByVal Heading As String) As String
            Dim sb As New StringBuilder
            Dim ItemNumber As Int32 = 0

            sb.AppendFormat("<table border=0 cellpadding=0 cellspacing=0>")

            sb.AppendFormat("<tr>")
            Dim ColorItem As Color = XrLabel_PageHeader.BorderColor
            Dim ColorText As String = String.Format("#FF{0:x2}{1:x2}{2:x2}", ColorItem.R, ColorItem.G, ColorItem.B)
            sb.AppendFormat("<td width=464 colspan=3 style='width:348pt;padding:.75pt .75pt .75pt .75pt;font-family:Gill Sans MT;font-weight:bold;font-size:16pt;color:{1}'><u>{0}</u></td>", Heading, ColorText)
            sb.AppendFormat("</tr>")

            For Each drv As DataRowView In vue
                Dim Description As String = DebtPlus.Utils.Nulls.DStr(drv("description")).Trim()
                If Description.Length > 0 Then
                    ItemNumber += 1
                    sb.Append("<tr>")

                    sb.AppendFormat("<td width=36 style='width:27pt;padding:.75pt .75pt .75pt .75pt;text-align:right;font-size:10pt;color:{1}'>{0:f0})</td>", ItemNumber, ColorText)
                    sb.AppendFormat("<td style='width:5pt;padding:.75pt .75pt .75pt .75pt'>{0}</td>", "&nbsp;")
                    sb.AppendFormat("<td width=500 style='padding:.75pt .75pt .75pt .75pt;font-size:10pt'>{0}</td>", EncodeHtml(Description))

                    sb.Append("</tr>")
                End If
            Next

            sb.Append("</table>")

            ' If there are no items then just return the empty string so that we don't generate a heading with a blank set of options
            If ItemNumber = 0 Then
                Return String.Empty
            End If

            Return sb.ToString()
        End Function

        Private Function EncodeHtml(ByVal InputString As String) As String
            Dim MatchEval As New MatchEvaluator(AddressOf HTMLReplaceEvaluator)
            Dim rxPattern As New Regex("(\&)|(\<)|(\>)|(\"")")
            Return rxPattern.Replace(InputString, MatchEval)
        End Function

        Public Function HTMLReplaceEvaluator(ByVal m As Match) As String
            Dim ItemMatch As String = m.Value
            Select Case ItemMatch
                Case "&"
                    Return "&amp;"
                Case """"
                    Return "&quot;"
                Case "<"
                    Return "&lt;"
                Case ">"
                    Return "&gt;"
                Case Else
                    Return ItemMatch
            End Select
        End Function

        Private Sub XrLabel_GoalsHeader_BeforePrint(sender As Object, e As PrintEventArgs)
            Dim ctl As DevExpress.XtraReports.UI.XRLabel = CType(sender, DevExpress.XtraReports.UI.XRLabel)
            Dim rpt As DevExpress.XtraReports.UI.XtraReport = CType(ctl.Report, DevExpress.XtraReports.UI.XtraReport)
            Dim MasterRpt As DevExpress.XtraReports.UI.XtraReport = rpt.MasterReport
            Dim ds As DataSet = CType(MasterRpt.DataSource, DataView).Table.DataSet
            Dim tbl As DataTable = ds.Tables("ActionPlanGoals")

            ' Find the text for the action plan goal
            Dim txt As String = String.Empty
            If tbl.Rows.Count > 0 Then
                Dim row As DataRow = tbl.Rows(0)
                txt = DebtPlus.Utils.Nulls.DStr(row("Text")).Trim()
            End If

            ' Disable the label if there is no text for the section.
            If txt.Length <= 0 Then
                e.Cancel = True
            End If
        End Sub

        Private Sub XrRichText_Concessions_BeforePrint(sender As Object, e As PrintEventArgs)
            Dim xrt As DevExpress.XtraReports.UI.XRRichText = CType(sender, DevExpress.XtraReports.UI.XRRichText)
            If xrt Is Nothing Then Return

            Dim asm As System.Reflection.Assembly = System.Reflection.Assembly.GetExecutingAssembly()
            Using ios = asm.GetManifestResourceStream("DebtPlus.Reports.Client.PacketDMP.ActionPlan." + "Creditor Cooperation and Concessions.rtf")
                Using fs As New StreamReader(ios)
                    xrt.Rtf = fs.ReadToEnd
                End Using
            End Using
        End Sub
    End Class
End Namespace