Namespace Client.PacketDMP
    <Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
    Partial Class PacketReport

        'XtraReport overrides dispose to clean up the component list.
        <System.Diagnostics.DebuggerNonUserCode()> _
        Protected Overrides Sub Dispose(ByVal disposing As Boolean)
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
            MyBase.Dispose(disposing)
        End Sub

        'Required by the Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Designer
        'It can be modified using the Designer.  
        'Do not modify it using the code editor.
        <System.Diagnostics.DebuggerStepThrough()> _
        Private Sub InitializeComponent()
            Me.ParameterClient = New DevExpress.XtraReports.Parameters.Parameter()
            Me.XrPageBreak1 = New DevExpress.XtraReports.UI.XRPageBreak()
            Me.XrPageBreak2 = New DevExpress.XtraReports.UI.XRPageBreak()
            Me.XrPageBreak3 = New DevExpress.XtraReports.UI.XRPageBreak()
            Me.XrPageBreak4 = New DevExpress.XtraReports.UI.XRPageBreak()
            Me.XrPageBreak5 = New DevExpress.XtraReports.UI.XRPageBreak()
            Me.XrSubreport_ActionPlan = New DevExpress.XtraReports.UI.XRSubreport()
            Me.ActionPlan1 = New ActionPlan()
            Me.XrSubreport_Surplus = New DevExpress.XtraReports.UI.XRSubreport()
            Me.SurplusDeficit1 = New SurplusDeficit()
            Me.XrSubreport_DebtOverview = New DevExpress.XtraReports.UI.XRSubreport()
            Me.DMPdebtOverview1 = New DMPdebtOverview()
            Me.XrSubreport_BudgetOverview = New DevExpress.XtraReports.UI.XRSubreport()
            Me.BudgetOverview1 = New BudgetOverview()
            Me.XrSubreport_BudgetDetail = New DevExpress.XtraReports.UI.XRSubreport()
            Me.BudgetDetail1 = New BudgetDetail()
            Me.XrSubreport_DebtDetail = New DevExpress.XtraReports.UI.XRSubreport()
            Me.DmpDebtDetail1 = New DMPdebtDetail()
            Me.XrPageBreak6 = New DevExpress.XtraReports.UI.XRPageBreak()
            Me.XrSubreport_DebtAssumptions = New DevExpress.XtraReports.UI.XRSubreport()
            Me.DMPdebtAssumptions1 = New DMPdebtAssumptions()
            CType(Me.ActionPlan1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.SurplusDeficit1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.DMPdebtOverview1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.BudgetOverview1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.BudgetDetail1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.DmpDebtDetail1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.DMPdebtAssumptions1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
            '
            'Detail
            '
            Me.Detail.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrSubreport_DebtAssumptions, Me.XrPageBreak6, Me.XrPageBreak5, Me.XrPageBreak4, Me.XrPageBreak3, Me.XrSubreport_ActionPlan, Me.XrSubreport_Surplus, Me.XrSubreport_DebtOverview, Me.XrPageBreak2, Me.XrPageBreak1, Me.XrSubreport_BudgetOverview, Me.XrSubreport_BudgetDetail, Me.XrSubreport_DebtDetail})
            Me.Detail.HeightF = 173.0!
            Me.Detail.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
            Me.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
            '
            'ParameterClient
            '
            Me.ParameterClient.Name = "ParameterClient"
            Me.ParameterClient.Type = GetType(Int32)
            Me.ParameterClient.Visible = False
            '
            'XrPageBreak1
            '
            Me.XrPageBreak1.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 22.99999!)
            Me.XrPageBreak1.Name = "XrPageBreak1"
            '
            'XrPageBreak2
            '
            Me.XrPageBreak2.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 47.99998!)
            Me.XrPageBreak2.Name = "XrPageBreak2"
            '
            'XrPageBreak3
            '
            Me.XrPageBreak3.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 72.99998!)
            Me.XrPageBreak3.Name = "XrPageBreak3"
            '
            'XrPageBreak4
            '
            Me.XrPageBreak4.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 97.99998!)
            Me.XrPageBreak4.Name = "XrPageBreak4"
            '
            'XrPageBreak5
            '
            Me.XrPageBreak5.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 123.0!)
            Me.XrPageBreak5.Name = "XrPageBreak5"
            '
            'XrSubreport_ActionPlan
            '
            Me.XrSubreport_ActionPlan.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 49.99998!)
            Me.XrSubreport_ActionPlan.Name = "XrSubreport_ActionPlan"
            Me.XrSubreport_ActionPlan.ReportSource = Me.ActionPlan1
            Me.XrSubreport_ActionPlan.Scripts.OnBeforePrint = "XrSubreport_ActionPlan_BeforePrint"
            Me.XrSubreport_ActionPlan.SizeF = New System.Drawing.SizeF(800.0!, 23.0!)
            '
            'XrSubreport_Surplus
            '
            Me.XrSubreport_Surplus.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 74.99998!)
            Me.XrSubreport_Surplus.Name = "XrSubreport_Surplus"
            Me.XrSubreport_Surplus.ReportSource = Me.SurplusDeficit1
            Me.XrSubreport_Surplus.Scripts.OnBeforePrint = "XrSubreport_Surplus_BeforePrint"
            Me.XrSubreport_Surplus.SizeF = New System.Drawing.SizeF(800.0!, 23.0!)
            '
            'XrSubreport_DebtOverview
            '
            Me.XrSubreport_DebtOverview.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 99.99998!)
            Me.XrSubreport_DebtOverview.Name = "XrSubreport_DebtOverview"
            Me.XrSubreport_DebtOverview.ReportSource = Me.DMPdebtOverview1
            Me.XrSubreport_DebtOverview.Scripts.OnBeforePrint = "XrSubreport_DebtOverview_BeforePrint"
            Me.XrSubreport_DebtOverview.SizeF = New System.Drawing.SizeF(799.9999!, 23.0!)
            '
            'XrSubreport_BudgetOverview
            '
            Me.XrSubreport_BudgetOverview.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
            Me.XrSubreport_BudgetOverview.Name = "XrSubreport_BudgetOverview"
            Me.XrSubreport_BudgetOverview.ReportSource = Me.BudgetOverview1
            Me.XrSubreport_BudgetOverview.Scripts.OnBeforePrint = "XrSubreport_BudgetOverview_BeforePrint"
            Me.XrSubreport_BudgetOverview.SizeF = New System.Drawing.SizeF(800.0!, 23.0!)
            '
            'XrSubreport_BudgetDetail
            '
            Me.XrSubreport_BudgetDetail.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 25.0!)
            Me.XrSubreport_BudgetDetail.Name = "XrSubreport_BudgetDetail"
            Me.XrSubreport_BudgetDetail.ReportSource = Me.BudgetDetail1
            Me.XrSubreport_BudgetDetail.Scripts.OnBeforePrint = "XrSubreport_BudgetDetail_BeforePrint"
            Me.XrSubreport_BudgetDetail.SizeF = New System.Drawing.SizeF(800.0!, 23.0!)
            '
            'XrSubreport_DebtDetail
            '
            Me.XrSubreport_DebtDetail.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 125.0!)
            Me.XrSubreport_DebtDetail.Name = "XrSubreport_DebtDetail"
            Me.XrSubreport_DebtDetail.ReportSource = Me.DmpDebtDetail1
            Me.XrSubreport_DebtDetail.SizeF = New System.Drawing.SizeF(800.0!, 23.0!)
            '
            'XrPageBreak6
            '
            Me.XrPageBreak6.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 148.0!)
            Me.XrPageBreak6.Name = "XrPageBreak6"
            '
            'XrSubreport_DebtAssumptions
            '
            Me.XrSubreport_DebtAssumptions.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 150.0!)
            Me.XrSubreport_DebtAssumptions.Name = "XrSubreport_DebtAssumptions"
            Me.XrSubreport_DebtAssumptions.ReportSource = Me.DMPdebtAssumptions1
            Me.XrSubreport_DebtAssumptions.Scripts.OnBeforePrint = "XrSubreport_DebtAssumptions_BeforePrint"
            Me.XrSubreport_DebtAssumptions.SizeF = New System.Drawing.SizeF(799.9999!, 23.0!)
            '
            'PacketReport
            '
            Me.Bands.AddRange(New DevExpress.XtraReports.UI.Band() {Me.TopMarginBand1, Me.Detail, Me.BottomMarginBand1})
            Me.Font = New System.Drawing.Font("Gill Sans MT", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.Parameters.AddRange(New DevExpress.XtraReports.Parameters.Parameter() {Me.ParameterClient})
            Me.RequestParameters = False
            Me.ScriptLanguage = DevExpress.XtraReports.ScriptLanguage.VisualBasic
            Me.Version = "11.2"
            CType(Me.ActionPlan1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.SurplusDeficit1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.DMPdebtOverview1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.BudgetOverview1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.BudgetDetail1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.DmpDebtDetail1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.DMPdebtAssumptions1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me, System.ComponentModel.ISupportInitialize).EndInit()

        End Sub
        Friend WithEvents XrSubreport_DebtDetail As DevExpress.XtraReports.UI.XRSubreport
        Private WithEvents DmpDebtDetail1 As DMPdebtDetail
        Friend WithEvents ParameterClient As DevExpress.XtraReports.Parameters.Parameter
        Friend WithEvents XrPageBreak2 As DevExpress.XtraReports.UI.XRPageBreak
        Friend WithEvents XrPageBreak1 As DevExpress.XtraReports.UI.XRPageBreak
        Friend WithEvents XrSubreport_BudgetOverview As DevExpress.XtraReports.UI.XRSubreport
        Private WithEvents BudgetOverview1 As BudgetOverview
        Friend WithEvents XrSubreport_BudgetDetail As DevExpress.XtraReports.UI.XRSubreport
        Private WithEvents BudgetDetail1 As BudgetDetail
        Friend WithEvents XrPageBreak5 As DevExpress.XtraReports.UI.XRPageBreak
        Friend WithEvents XrPageBreak4 As DevExpress.XtraReports.UI.XRPageBreak
        Friend WithEvents XrPageBreak3 As DevExpress.XtraReports.UI.XRPageBreak
        Friend WithEvents XrSubreport_ActionPlan As DevExpress.XtraReports.UI.XRSubreport
        Private WithEvents ActionPlan1 As ActionPlan
        Friend WithEvents XrSubreport_Surplus As DevExpress.XtraReports.UI.XRSubreport
        Private WithEvents SurplusDeficit1 As SurplusDeficit
        Friend WithEvents XrSubreport_DebtOverview As DevExpress.XtraReports.UI.XRSubreport
        Private WithEvents DMPdebtOverview1 As DMPdebtOverview
        Friend WithEvents XrPageBreak6 As DevExpress.XtraReports.UI.XRPageBreak
        Friend WithEvents XrSubreport_DebtAssumptions As DevExpress.XtraReports.UI.XRSubreport
        Private WithEvents DMPdebtAssumptions1 As DMPdebtAssumptions
    End Class
End Namespace
