Namespace Client.PacketDMP

    <Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
    Partial Class DMPdebtAssumptions

        'XtraReport overrides dispose to clean up the component list.
        <System.Diagnostics.DebuggerNonUserCode()> _
        Protected Overrides Sub Dispose(ByVal disposing As Boolean)
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
            MyBase.Dispose(disposing)
        End Sub

        'Required by the Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Designer
        'It can be modified using the Designer.  
        'Do not modify it using the code editor.
        <System.Diagnostics.DebuggerStepThrough()> _
        Private Sub InitializeComponent()
            Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(DMPdebtAssumptions))
            Me.XrRichText1 = New DevExpress.XtraReports.UI.XRRichText()
            Me.XrLabel1 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel2 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrTable1 = New DevExpress.XtraReports.UI.XRTable()
            Me.XrTableRow1 = New DevExpress.XtraReports.UI.XRTableRow()
            Me.XrTableCell1 = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableCell_TotalBalance = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableRow10 = New DevExpress.XtraReports.UI.XRTableRow()
            Me.XrTableCell15 = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableCell16 = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableRow2 = New DevExpress.XtraReports.UI.XRTableRow()
            Me.XrTableCell2 = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableCell_TotalPayment = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableRow3 = New DevExpress.XtraReports.UI.XRTableRow()
            Me.XrTableCell5 = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableCell_TotalDMPPayment = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableRow4 = New DevExpress.XtraReports.UI.XRTableRow()
            Me.XrTableCell7 = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableCell_Difference = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTable2 = New DevExpress.XtraReports.UI.XRTable()
            Me.XrTableRow5 = New DevExpress.XtraReports.UI.XRTableRow()
            Me.XrTableCell9 = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableCell10 = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableRow6 = New DevExpress.XtraReports.UI.XRTableRow()
            Me.XrTableCell11 = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableRow7 = New DevExpress.XtraReports.UI.XRTableRow()
            Me.XrTableCell12 = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableRow8 = New DevExpress.XtraReports.UI.XRTableRow()
            Me.XrTableCell13 = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableRow9 = New DevExpress.XtraReports.UI.XRTableRow()
            Me.XrTableCell14 = New DevExpress.XtraReports.UI.XRTableCell()
            CType(Me.XrRichText1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.XrTable1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.XrTable2, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
            '
            'XrLabel_PageHeader
            '
            Me.XrLabel_PageHeader.BorderColor = System.Drawing.Color.Purple
            Me.XrLabel_PageHeader.ForeColor = System.Drawing.Color.Purple
            Me.XrLabel_PageHeader.StylePriority.UseBackColor = False
            Me.XrLabel_PageHeader.StylePriority.UseBorderColor = False
            Me.XrLabel_PageHeader.StylePriority.UseBorders = False
            Me.XrLabel_PageHeader.StylePriority.UseBorderWidth = False
            Me.XrLabel_PageHeader.StylePriority.UseFont = False
            Me.XrLabel_PageHeader.StylePriority.UseForeColor = False
            Me.XrLabel_PageHeader.StylePriority.UsePadding = False
            Me.XrLabel_PageHeader.StylePriority.UseTextAlignment = False
            Me.XrLabel_PageHeader.Text = "Totals & Assumptions"
            '
            'XrLabel_ClientID
            '
            Me.XrLabel_ClientID.StylePriority.UseFont = False
            Me.XrLabel_ClientID.StylePriority.UseForeColor = False
            Me.XrLabel_ClientID.StylePriority.UseTextAlignment = False
            '
            'Detail
            '
            Me.Detail.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrTable2, Me.XrTable1, Me.XrLabel2, Me.XrLabel1, Me.XrRichText1})
            Me.Detail.HeightF = 594.7917!
            Me.Detail.MultiColumn.ColumnSpacing = 20.0!
            Me.Detail.MultiColumn.Mode = DevExpress.XtraReports.UI.MultiColumnMode.UseColumnCount
            '
            'XrRichText1
            '
            Me.XrRichText1.LocationFloat = New DevExpress.Utils.PointFloat(51.04167!, 47.87502!)
            Me.XrRichText1.Name = "XrRichText1"
            Me.XrRichText1.SerializableRtfString = resources.GetString("XrRichText1.SerializableRtfString")
            Me.XrRichText1.SizeF = New System.Drawing.SizeF(347.9167!, 536.9167!)
            '
            'XrLabel1
            '
            Me.XrLabel1.BorderColor = System.Drawing.Color.Purple
            Me.XrLabel1.Borders = DevExpress.XtraPrinting.BorderSide.Bottom
            Me.XrLabel1.Font = New System.Drawing.Font("Gill Sans MT", 14.0!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.XrLabel1.ForeColor = System.Drawing.Color.Purple
            Me.XrLabel1.LocationFloat = New DevExpress.Utils.PointFloat(51.04167!, 0.0!)
            Me.XrLabel1.Name = "XrLabel1"
            Me.XrLabel1.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel1.SizeF = New System.Drawing.SizeF(347.9167!, 33.29166!)
            Me.XrLabel1.StylePriority.UseBorderColor = False
            Me.XrLabel1.StylePriority.UseBorders = False
            Me.XrLabel1.StylePriority.UseFont = False
            Me.XrLabel1.StylePriority.UseForeColor = False
            Me.XrLabel1.Text = "Assumptions"
            '
            'XrLabel2
            '
            Me.XrLabel2.BorderColor = System.Drawing.Color.Purple
            Me.XrLabel2.Borders = DevExpress.XtraPrinting.BorderSide.Bottom
            Me.XrLabel2.Font = New System.Drawing.Font("Gill Sans MT", 14.0!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.XrLabel2.ForeColor = System.Drawing.Color.Purple
            Me.XrLabel2.LocationFloat = New DevExpress.Utils.PointFloat(427.0834!, 0.0!)
            Me.XrLabel2.Name = "XrLabel2"
            Me.XrLabel2.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel2.SizeF = New System.Drawing.SizeF(299.9999!, 33.29166!)
            Me.XrLabel2.StylePriority.UseBorderColor = False
            Me.XrLabel2.StylePriority.UseBorders = False
            Me.XrLabel2.StylePriority.UseFont = False
            Me.XrLabel2.StylePriority.UseForeColor = False
            Me.XrLabel2.Text = "Totals"
            '
            'XrTable1
            '
            Me.XrTable1.Font = New System.Drawing.Font("Gill Sans MT", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.XrTable1.LocationFloat = New DevExpress.Utils.PointFloat(427.0834!, 47.87502!)
            Me.XrTable1.Name = "XrTable1"
            Me.XrTable1.Rows.AddRange(New DevExpress.XtraReports.UI.XRTableRow() {Me.XrTableRow1, Me.XrTableRow10, Me.XrTableRow2, Me.XrTableRow3, Me.XrTableRow4})
            Me.XrTable1.SizeF = New System.Drawing.SizeF(299.9999!, 112.5!)
            Me.XrTable1.StylePriority.UseFont = False
            Me.XrTable1.StylePriority.UseTextAlignment = False
            Me.XrTable1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            '
            'XrTableRow1
            '
            Me.XrTableRow1.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.XrTableCell1, Me.XrTableCell_TotalBalance})
            Me.XrTableRow1.Name = "XrTableRow1"
            Me.XrTableRow1.StylePriority.UseTextAlignment = False
            Me.XrTableRow1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            Me.XrTableRow1.Weight = 1.0R
            '
            'XrTableCell1
            '
            Me.XrTableCell1.Name = "XrTableCell1"
            Me.XrTableCell1.Text = "Total Balance"
            Me.XrTableCell1.Weight = 1.8804600500687645R
            '
            'XrTableCell_TotalBalance
            '
            Me.XrTableCell_TotalBalance.Name = "XrTableCell_TotalBalance"
            Me.XrTableCell_TotalBalance.Scripts.OnBeforePrint = "XrTableCell_TotalBalance_BeforePrint"
            Me.XrTableCell_TotalBalance.StylePriority.UseTextAlignment = False
            Me.XrTableCell_TotalBalance.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrTableCell_TotalBalance.Weight = 1.3591243005171729R
            '
            'XrTableRow10
            '
            Me.XrTableRow10.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.XrTableCell15, Me.XrTableCell16})
            Me.XrTableRow10.Name = "XrTableRow10"
            Me.XrTableRow10.Weight = 0.5R
            '
            'XrTableCell15
            '
            Me.XrTableCell15.Name = "XrTableCell15"
            Me.XrTableCell15.Weight = 1.880460709164022R
            '
            'XrTableCell16
            '
            Me.XrTableCell16.Name = "XrTableCell16"
            Me.XrTableCell16.Weight = 1.3591236414219154R
            '
            'XrTableRow2
            '
            Me.XrTableRow2.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.XrTableCell2, Me.XrTableCell_TotalPayment})
            Me.XrTableRow2.Name = "XrTableRow2"
            Me.XrTableRow2.Weight = 1.0R
            '
            'XrTableCell2
            '
            Me.XrTableCell2.Name = "XrTableCell2"
            Me.XrTableCell2.Text = "Current Payment"
            Me.XrTableCell2.Weight = 1.8804600500687645R
            '
            'XrTableCell_TotalPayment
            '
            Me.XrTableCell_TotalPayment.Name = "XrTableCell_TotalPayment"
            Me.XrTableCell_TotalPayment.Scripts.OnBeforePrint = "XrTableCell_TotalPayment_BeforePrint"
            Me.XrTableCell_TotalPayment.StylePriority.UseTextAlignment = False
            Me.XrTableCell_TotalPayment.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrTableCell_TotalPayment.Weight = 1.3591243005171729R
            '
            'XrTableRow3
            '
            Me.XrTableRow3.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.XrTableCell5, Me.XrTableCell_TotalDMPPayment})
            Me.XrTableRow3.Name = "XrTableRow3"
            Me.XrTableRow3.Weight = 0.9166665649414063R
            '
            'XrTableCell5
            '
            Me.XrTableCell5.Name = "XrTableCell5"
            Me.XrTableCell5.Text = "DMP Payment"
            Me.XrTableCell5.Weight = 1.880460709164022R
            '
            'XrTableCell_TotalDMPPayment
            '
            Me.XrTableCell_TotalDMPPayment.Name = "XrTableCell_TotalDMPPayment"
            Me.XrTableCell_TotalDMPPayment.Scripts.OnBeforePrint = "XrTableCell_TotalDMPPayment_BeforePrint"
            Me.XrTableCell_TotalDMPPayment.StylePriority.UseTextAlignment = False
            Me.XrTableCell_TotalDMPPayment.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrTableCell_TotalDMPPayment.Weight = 1.3591236414219154R
            '
            'XrTableRow4
            '
            Me.XrTableRow4.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.XrTableCell7, Me.XrTableCell_Difference})
            Me.XrTableRow4.Name = "XrTableRow4"
            Me.XrTableRow4.Weight = 1.083333740234375R
            '
            'XrTableCell7
            '
            Me.XrTableCell7.Font = New System.Drawing.Font("Gill Sans MT", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.XrTableCell7.Name = "XrTableCell7"
            Me.XrTableCell7.StylePriority.UseFont = False
            Me.XrTableCell7.Text = "Monthly Difference"
            Me.XrTableCell7.Weight = 1.880460709164022R
            '
            'XrTableCell_Difference
            '
            Me.XrTableCell_Difference.Borders = DevExpress.XtraPrinting.BorderSide.Top
            Me.XrTableCell_Difference.Font = New System.Drawing.Font("Gill Sans MT", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.XrTableCell_Difference.Name = "XrTableCell_Difference"
            Me.XrTableCell_Difference.Scripts.OnBeforePrint = "XrTableCell_Difference_BeforePrint"
            Me.XrTableCell_Difference.StylePriority.UseBorders = False
            Me.XrTableCell_Difference.StylePriority.UseFont = False
            Me.XrTableCell_Difference.StylePriority.UseTextAlignment = False
            Me.XrTableCell_Difference.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrTableCell_Difference.Weight = 1.3591236414219154R
            '
            'XrTable2
            '
            Me.XrTable2.BorderColor = System.Drawing.Color.DarkGray
            Me.XrTable2.ForeColor = System.Drawing.Color.DarkGray
            Me.XrTable2.LocationFloat = New DevExpress.Utils.PointFloat(427.0834!, 356.6667!)
            Me.XrTable2.Name = "XrTable2"
            Me.XrTable2.Rows.AddRange(New DevExpress.XtraReports.UI.XRTableRow() {Me.XrTableRow5, Me.XrTableRow6, Me.XrTableRow7, Me.XrTableRow8, Me.XrTableRow9})
            Me.XrTable2.SizeF = New System.Drawing.SizeF(300.0!, 228.125!)
            Me.XrTable2.StylePriority.UseBorderColor = False
            Me.XrTable2.StylePriority.UseForeColor = False
            '
            'XrTableRow5
            '
            Me.XrTableRow5.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.XrTableCell9, Me.XrTableCell10})
            Me.XrTableRow5.Name = "XrTableRow5"
            Me.XrTableRow5.Weight = 1.5318983079350079R
            '
            'XrTableCell9
            '
            Me.XrTableCell9.Font = New System.Drawing.Font("Wingdings", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(2, Byte))
            Me.XrTableCell9.ForeColor = System.Drawing.Color.Purple
            Me.XrTableCell9.Name = "XrTableCell9"
            Me.XrTableCell9.StylePriority.UseFont = False
            Me.XrTableCell9.StylePriority.UseForeColor = False
            Me.XrTableCell9.Text = "è"
            Me.XrTableCell9.Weight = 0.20833328247070315R
            '
            'XrTableCell10
            '
            Me.XrTableCell10.ForeColor = System.Drawing.Color.Purple
            Me.XrTableCell10.Name = "XrTableCell10"
            Me.XrTableCell10.StylePriority.UseForeColor = False
            Me.XrTableCell10.Text = "I UNDERSTAND THAT THIS IS AN ESTIMATE AND IS IN NO WAY BINDING."
            Me.XrTableCell10.Weight = 2.7916667175292966R
            '
            'XrTableRow6
            '
            Me.XrTableRow6.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.XrTableCell11})
            Me.XrTableRow6.Name = "XrTableRow6"
            Me.XrTableRow6.Weight = 0.8483003061618466R
            '
            'XrTableCell11
            '
            Me.XrTableCell11.BorderColor = System.Drawing.Color.DarkGray
            Me.XrTableCell11.Borders = DevExpress.XtraPrinting.BorderSide.Top
            Me.XrTableCell11.BorderWidth = 2
            Me.XrTableCell11.Name = "XrTableCell11"
            Me.XrTableCell11.StylePriority.UseBorderColor = False
            Me.XrTableCell11.StylePriority.UseBorders = False
            Me.XrTableCell11.StylePriority.UseBorderWidth = False
            Me.XrTableCell11.Text = "Signature"
            Me.XrTableCell11.Weight = 3.0R
            '
            'XrTableRow7
            '
            Me.XrTableRow7.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.XrTableCell12})
            Me.XrTableRow7.Name = "XrTableRow7"
            Me.XrTableRow7.Weight = 1.1832572668744799R
            '
            'XrTableCell12
            '
            Me.XrTableCell12.BorderColor = System.Drawing.Color.DarkGray
            Me.XrTableCell12.Borders = DevExpress.XtraPrinting.BorderSide.Top
            Me.XrTableCell12.BorderWidth = 2
            Me.XrTableCell12.Name = "XrTableCell12"
            Me.XrTableCell12.StylePriority.UseBorderColor = False
            Me.XrTableCell12.StylePriority.UseBorders = False
            Me.XrTableCell12.StylePriority.UseBorderWidth = False
            Me.XrTableCell12.Text = "Date"
            Me.XrTableCell12.Weight = 3.0R
            '
            'XrTableRow8
            '
            Me.XrTableRow8.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.XrTableCell13})
            Me.XrTableRow8.Name = "XrTableRow8"
            Me.XrTableRow8.Weight = 0.71827229760323585R
            '
            'XrTableCell13
            '
            Me.XrTableCell13.BorderColor = System.Drawing.Color.DarkGray
            Me.XrTableCell13.Borders = DevExpress.XtraPrinting.BorderSide.Top
            Me.XrTableCell13.BorderWidth = 2
            Me.XrTableCell13.Name = "XrTableCell13"
            Me.XrTableCell13.StylePriority.UseBorderColor = False
            Me.XrTableCell13.StylePriority.UseBorders = False
            Me.XrTableCell13.StylePriority.UseBorderWidth = False
            Me.XrTableCell13.Text = "Signature"
            Me.XrTableCell13.Weight = 3.0R
            '
            'XrTableRow9
            '
            Me.XrTableRow9.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.XrTableCell14})
            Me.XrTableRow9.Name = "XrTableRow9"
            Me.XrTableRow9.Weight = 0.46430395498197724R
            '
            'XrTableCell14
            '
            Me.XrTableCell14.BorderColor = System.Drawing.Color.DarkGray
            Me.XrTableCell14.Borders = DevExpress.XtraPrinting.BorderSide.Top
            Me.XrTableCell14.BorderWidth = 2
            Me.XrTableCell14.Name = "XrTableCell14"
            Me.XrTableCell14.StylePriority.UseBorderColor = False
            Me.XrTableCell14.StylePriority.UseBorders = False
            Me.XrTableCell14.StylePriority.UseBorderWidth = False
            Me.XrTableCell14.Text = "Date"
            Me.XrTableCell14.Weight = 3.0R
            '
            'DMPdebtAssumptions
            '
            Me.Bands.AddRange(New DevExpress.XtraReports.UI.Band() {Me.Detail, Me.PageFooter, Me.PageHeader, Me.TopMarginBand1, Me.BottomMarginBand1})
            Me.ScriptLanguage = DevExpress.XtraReports.ScriptLanguage.VisualBasic
            Me.ScriptReferencesString = resources.GetString("$this.ScriptReferencesString")
            Me.Version = "11.2"
            CType(Me.XrRichText1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.XrTable1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.XrTable2, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me, System.ComponentModel.ISupportInitialize).EndInit()

        End Sub
        Friend WithEvents XrLabel1 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrRichText1 As DevExpress.XtraReports.UI.XRRichText
        Friend WithEvents XrTable1 As DevExpress.XtraReports.UI.XRTable
        Friend WithEvents XrTableRow1 As DevExpress.XtraReports.UI.XRTableRow
        Friend WithEvents XrTableCell1 As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents XrTableCell_TotalBalance As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents XrTableRow2 As DevExpress.XtraReports.UI.XRTableRow
        Friend WithEvents XrTableCell2 As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents XrTableCell_TotalPayment As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents XrTableRow3 As DevExpress.XtraReports.UI.XRTableRow
        Friend WithEvents XrTableCell5 As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents XrTableCell_TotalDMPPayment As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents XrTableRow4 As DevExpress.XtraReports.UI.XRTableRow
        Friend WithEvents XrTableCell7 As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents XrTableCell_Difference As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents XrLabel2 As DevExpress.XtraReports.UI.XRLabel
        Protected Friend WithEvents XrTable2 As DevExpress.XtraReports.UI.XRTable
        Protected Friend WithEvents XrTableRow5 As DevExpress.XtraReports.UI.XRTableRow
        Protected Friend WithEvents XrTableCell9 As DevExpress.XtraReports.UI.XRTableCell
        Protected Friend WithEvents XrTableCell10 As DevExpress.XtraReports.UI.XRTableCell
        Protected Friend WithEvents XrTableRow6 As DevExpress.XtraReports.UI.XRTableRow
        Protected Friend WithEvents XrTableCell11 As DevExpress.XtraReports.UI.XRTableCell
        Protected Friend WithEvents XrTableRow7 As DevExpress.XtraReports.UI.XRTableRow
        Protected Friend WithEvents XrTableCell12 As DevExpress.XtraReports.UI.XRTableCell
        Protected Friend WithEvents XrTableRow8 As DevExpress.XtraReports.UI.XRTableRow
        Protected Friend WithEvents XrTableCell13 As DevExpress.XtraReports.UI.XRTableCell
        Protected Friend WithEvents XrTableRow9 As DevExpress.XtraReports.UI.XRTableRow
        Protected Friend WithEvents XrTableCell14 As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents XrTableRow10 As DevExpress.XtraReports.UI.XRTableRow
        Friend WithEvents XrTableCell15 As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents XrTableCell16 As DevExpress.XtraReports.UI.XRTableCell
    End Class
End Namespace
