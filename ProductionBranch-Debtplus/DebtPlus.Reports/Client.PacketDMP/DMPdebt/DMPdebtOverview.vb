﻿Imports System.Drawing.Printing
Imports DevExpress.XtraCharts

Namespace Client.PacketDMP
    Public Class DMPdebtOverview
        Inherits CommonReportPortrait

        Public Sub New()
            MyBase.New()
            InitializeComponent()
            AddHandler XrChart1.BeforePrint, AddressOf XrChart1_BeforePrint
        End Sub

        Private Sub XrChart1_BeforePrint(sender As Object, e As PrintEventArgs)
            Dim chart As DevExpress.XtraReports.UI.XRChart = CType(sender, DevExpress.XtraReports.UI.XRChart)
            Dim rpt As DevExpress.XtraReports.UI.XtraReport = CType(chart.Report, DevExpress.XtraReports.UI.XtraReport)
            Dim MasterRpt As DevExpress.XtraReports.UI.XtraReport = rpt.MasterReport
            Dim ds As DataSet = CType(MasterRpt.DataSource, DataView).Table.DataSet
            Dim tbl As DataTable = ds.Tables("rpt_PrintClient_CreditorDebt")

            If tbl IsNot Nothing Then
                Dim vue As DataView = tbl.DefaultView
                If vue.Count > 0 Then
                    With chart

                        ' Calculate the monthly totals used in the chart
                        Dim sum_non_dmp_payment As Decimal = DebtPlus.Utils.Nulls.DDec(tbl.Compute("sum(non_dmp_payment)", String.Empty))
                        Dim sum_dmp_payment As Decimal = DebtPlus.Utils.Nulls.DDec(tbl.Compute("sum(dmp_payment)", String.Empty))

                        ' Generate the label for the chart
                        Dim Difference As Decimal = sum_non_dmp_payment - sum_dmp_payment

                        Dim DifferenceString As String
                        If Difference < 0D Then
                            DifferenceString = String.Format("{0:$#,##0} less", 0D - Difference)
                        Else
                            DifferenceString = String.Format("{0:$#,##0} more", Difference)
                        End If
                        .Titles(0).Text = String.Format("<size=18><i><b>{0}</b></i></size> per month<br>available to pay off your<br>credit balances", DifferenceString)

                        ' Generate series #1
                        With .Series(0)
                            .DataSource = vue
                            .ArgumentScaleType = ScaleType.Qualitative
                            .ArgumentDataMember = "combined_creditor_name"
                            .ValueScaleType = ScaleType.Numerical
                            .ValueDataMembers.AddRange(New String() {"non_dmp_payment"})
                        End With

                        ' Generate series #2
                        With .Series(1)
                            .DataSource = vue
                            .ArgumentScaleType = ScaleType.Qualitative
                            .ArgumentDataMember = "combined_creditor_name"
                            .ValueScaleType = ScaleType.Numerical
                            .ValueDataMembers.AddRange(New String() {"dmp_payment"})
                        End With
                    End With
                End If
            End If
        End Sub
    End Class
End Namespace