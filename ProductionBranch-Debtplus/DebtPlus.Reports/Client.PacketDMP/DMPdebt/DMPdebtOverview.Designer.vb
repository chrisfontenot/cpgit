Namespace Client.PacketDMP

    <Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
    Partial Class DMPdebtOverview

        'XtraReport overrides dispose to clean up the component list.
        <System.Diagnostics.DebuggerNonUserCode()> _
        Protected Overrides Sub Dispose(ByVal disposing As Boolean)
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
            MyBase.Dispose(disposing)
        End Sub

        'Required by the Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Designer
        'It can be modified using the Designer.  
        'Do not modify it using the code editor.
        <System.Diagnostics.DebuggerStepThrough()> _
        Private Sub InitializeComponent()
            Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(DMPdebtOverview))
            Dim XyDiagram1 As DevExpress.XtraCharts.XYDiagram = New DevExpress.XtraCharts.XYDiagram()
            Dim Series1 As DevExpress.XtraCharts.Series = New DevExpress.XtraCharts.Series()
            Dim StackedBarSeriesLabel1 As DevExpress.XtraCharts.StackedBarSeriesLabel = New DevExpress.XtraCharts.StackedBarSeriesLabel()
            Dim PointOptions1 As DevExpress.XtraCharts.PointOptions = New DevExpress.XtraCharts.PointOptions()
            Dim SideBySideStackedBarSeriesView1 As DevExpress.XtraCharts.SideBySideStackedBarSeriesView = New DevExpress.XtraCharts.SideBySideStackedBarSeriesView()
            Dim HatchFillOptions1 As DevExpress.XtraCharts.HatchFillOptions = New DevExpress.XtraCharts.HatchFillOptions()
            Dim Series2 As DevExpress.XtraCharts.Series = New DevExpress.XtraCharts.Series()
            Dim StackedBarSeriesLabel2 As DevExpress.XtraCharts.StackedBarSeriesLabel = New DevExpress.XtraCharts.StackedBarSeriesLabel()
            Dim PointOptions2 As DevExpress.XtraCharts.PointOptions = New DevExpress.XtraCharts.PointOptions()
            Dim SideBySideStackedBarSeriesView2 As DevExpress.XtraCharts.SideBySideStackedBarSeriesView = New DevExpress.XtraCharts.SideBySideStackedBarSeriesView()
            Dim HatchFillOptions2 As DevExpress.XtraCharts.HatchFillOptions = New DevExpress.XtraCharts.HatchFillOptions()
            Dim StackedBarSeriesLabel3 As DevExpress.XtraCharts.StackedBarSeriesLabel = New DevExpress.XtraCharts.StackedBarSeriesLabel()
            Dim SideBySideStackedBarSeriesView3 As DevExpress.XtraCharts.SideBySideStackedBarSeriesView = New DevExpress.XtraCharts.SideBySideStackedBarSeriesView()
            Dim ChartTitle1 As DevExpress.XtraCharts.ChartTitle = New DevExpress.XtraCharts.ChartTitle()
            Dim ShapeArrow1 As DevExpress.XtraPrinting.Shape.ShapeArrow = New DevExpress.XtraPrinting.Shape.ShapeArrow()
            Me.XrChart1 = New DevExpress.XtraReports.UI.XRChart()
            Me.GroupFooter1 = New DevExpress.XtraReports.UI.GroupFooterBand()
            Me.XrLabel1 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrShape1 = New DevExpress.XtraReports.UI.XRShape()
            CType(Me.XrChart1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(XyDiagram1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Series1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(StackedBarSeriesLabel1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(SideBySideStackedBarSeriesView1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Series2, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(StackedBarSeriesLabel2, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(SideBySideStackedBarSeriesView2, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(StackedBarSeriesLabel3, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(SideBySideStackedBarSeriesView3, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
            '
            'XrLabel_PageHeader
            '
            Me.XrLabel_PageHeader.BorderColor = System.Drawing.Color.Purple
            Me.XrLabel_PageHeader.ForeColor = System.Drawing.Color.Purple
            Me.XrLabel_PageHeader.StylePriority.UseBackColor = False
            Me.XrLabel_PageHeader.StylePriority.UseBorderColor = False
            Me.XrLabel_PageHeader.StylePriority.UseBorders = False
            Me.XrLabel_PageHeader.StylePriority.UseBorderWidth = False
            Me.XrLabel_PageHeader.StylePriority.UseFont = False
            Me.XrLabel_PageHeader.StylePriority.UseForeColor = False
            Me.XrLabel_PageHeader.StylePriority.UsePadding = False
            Me.XrLabel_PageHeader.StylePriority.UseTextAlignment = False
            Me.XrLabel_PageHeader.Text = "Debt Overview"
            '
            'XrLabel_ClientID
            '
            Me.XrLabel_ClientID.StylePriority.UseFont = False
            Me.XrLabel_ClientID.StylePriority.UseForeColor = False
            Me.XrLabel_ClientID.StylePriority.UseTextAlignment = False
            '
            'Detail
            '
            Me.Detail.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrChart1})
            Me.Detail.HeightF = 739.5833!
            '
            'XrChart1
            '
            Me.XrChart1.BackColor = System.Drawing.Color.White
            Me.XrChart1.BackImage.Image = CType(resources.GetObject("XrChart1.BackImage.Image"), System.Drawing.Image)
            Me.XrChart1.BackImage.Stretch = True
            Me.XrChart1.BorderColor = System.Drawing.Color.Black
            Me.XrChart1.Borders = DevExpress.XtraPrinting.BorderSide.None
            XyDiagram1.AxisX.Label.Angle = 30
            XyDiagram1.AxisX.Range.SideMarginsEnabled = True
            XyDiagram1.AxisX.VisibleInPanesSerializable = "-1"
            XyDiagram1.AxisY.GridLines.Visible = False
            XyDiagram1.AxisY.GridSpacing = 2.0R
            XyDiagram1.AxisY.GridSpacingAuto = False
            XyDiagram1.AxisY.Label.Visible = False
            XyDiagram1.AxisY.Range.SideMarginsEnabled = True
            XyDiagram1.AxisY.Tickmarks.MinorVisible = False
            XyDiagram1.AxisY.Tickmarks.Visible = False
            XyDiagram1.AxisY.Visible = False
            XyDiagram1.AxisY.VisibleInPanesSerializable = "-1"
            XyDiagram1.DefaultPane.BackColor = System.Drawing.Color.Transparent
            XyDiagram1.DefaultPane.BorderVisible = False
            Me.XrChart1.Diagram = XyDiagram1
            Me.XrChart1.Legend.AlignmentHorizontal = DevExpress.XtraCharts.LegendAlignmentHorizontal.Left
            Me.XrChart1.Legend.AlignmentVertical = DevExpress.XtraCharts.LegendAlignmentVertical.TopOutside
            Me.XrChart1.Legend.BackColor = System.Drawing.Color.Transparent
            Me.XrChart1.Legend.Border.Visible = False
            Me.XrChart1.Legend.EquallySpacedItems = False
            Me.XrChart1.Legend.Font = New System.Drawing.Font("Gill Sans MT", 8.0!)
            Me.XrChart1.Legend.HorizontalIndent = 0
            Me.XrChart1.Legend.Padding.Bottom = 0
            Me.XrChart1.Legend.Padding.Left = 0
            Me.XrChart1.Legend.Padding.Right = 0
            Me.XrChart1.Legend.Padding.Top = 0
            Me.XrChart1.Legend.TextOffset = 0
            Me.XrChart1.Legend.VerticalIndent = 0
            Me.XrChart1.LocationFloat = New DevExpress.Utils.PointFloat(20.00001!, 0.0!)
            Me.XrChart1.Name = "XrChart1"
            Me.XrChart1.PaletteName = "Metro"
            PointOptions1.ValueNumericOptions.Format = DevExpress.XtraCharts.NumericFormat.Currency
            PointOptions1.ValueNumericOptions.Precision = 0
            StackedBarSeriesLabel1.PointOptions = PointOptions1
            StackedBarSeriesLabel1.ShowForZeroValues = True
            StackedBarSeriesLabel1.TextColor = System.Drawing.Color.Black
            Series1.Label = StackedBarSeriesLabel1
            Series1.Name = "SELF Payment"
            SideBySideStackedBarSeriesView1.Color = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Int32), CType(CType(0, Byte), Int32), CType(CType(0, Byte), Int32))
            SideBySideStackedBarSeriesView1.FillStyle.FillMode = DevExpress.XtraCharts.FillMode.Hatch
            HatchFillOptions1.Color2 = System.Drawing.Color.White
            HatchFillOptions1.HatchStyle = System.Drawing.Drawing2D.HatchStyle.DarkUpwardDiagonal
            SideBySideStackedBarSeriesView1.FillStyle.Options = HatchFillOptions1
            SideBySideStackedBarSeriesView1.StackedGroupSerializable = "Group 1"
            Series1.View = SideBySideStackedBarSeriesView1
            PointOptions2.ValueNumericOptions.Format = DevExpress.XtraCharts.NumericFormat.Currency
            PointOptions2.ValueNumericOptions.Precision = 0
            StackedBarSeriesLabel2.PointOptions = PointOptions2
            StackedBarSeriesLabel2.TextColor = System.Drawing.Color.Black
            Series2.Label = StackedBarSeriesLabel2
            Series2.Name = "DMP Payment"
            SideBySideStackedBarSeriesView2.Color = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Int32), CType(CType(0, Byte), Int32), CType(CType(255, Byte), Int32))
            SideBySideStackedBarSeriesView2.FillStyle.FillMode = DevExpress.XtraCharts.FillMode.Hatch
            HatchFillOptions2.Color2 = System.Drawing.Color.White
            HatchFillOptions2.HatchStyle = System.Drawing.Drawing2D.HatchStyle.DarkDownwardDiagonal
            SideBySideStackedBarSeriesView2.FillStyle.Options = HatchFillOptions2
            SideBySideStackedBarSeriesView2.StackedGroupSerializable = "Group 2"
            Series2.View = SideBySideStackedBarSeriesView2
            Me.XrChart1.SeriesSerializable = New DevExpress.XtraCharts.Series() {Series1, Series2}
            Me.XrChart1.SeriesTemplate.Label = StackedBarSeriesLabel3
            Me.XrChart1.SeriesTemplate.View = SideBySideStackedBarSeriesView3
            Me.XrChart1.SizeF = New System.Drawing.SizeF(753.125!, 739.5833!)
            ChartTitle1.Alignment = System.Drawing.StringAlignment.Far
            ChartTitle1.Indent = 0
            ChartTitle1.MaxLineCount = 3
            ChartTitle1.Text = "<size=18><i><b>$139 more</b></i></size> per month<br>available to pay off your<br" & _
                               ">credit balances"
            ChartTitle1.WordWrap = True
            Me.XrChart1.Titles.AddRange(New DevExpress.XtraCharts.ChartTitle() {ChartTitle1})
            '
            'GroupFooter1
            '
            Me.GroupFooter1.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel1, Me.XrShape1})
            Me.GroupFooter1.HeightF = 33.0!
            Me.GroupFooter1.Name = "GroupFooter1"
            Me.GroupFooter1.PrintAtBottom = True
            Me.GroupFooter1.RepeatEveryPage = True
            '
            'XrLabel1
            '
            Me.XrLabel1.Font = New System.Drawing.Font("Segoe Print", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.XrLabel1.ForeColor = System.Drawing.Color.Purple
            Me.XrLabel1.LocationFloat = New DevExpress.Utils.PointFloat(249.9983!, 0.0!)
            Me.XrLabel1.Name = "XrLabel1"
            Me.XrLabel1.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 10, 0, 0, 100.0!)
            Me.XrLabel1.SizeF = New System.Drawing.SizeF(501.0417!, 32.99999!)
            Me.XrLabel1.StylePriority.UseFont = False
            Me.XrLabel1.StylePriority.UseForeColor = False
            Me.XrLabel1.StylePriority.UsePadding = False
            Me.XrLabel1.StylePriority.UseTextAlignment = False
            Me.XrLabel1.Text = "See totals & assumptions for more information"
            Me.XrLabel1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrShape1
            '
            Me.XrShape1.Angle = 270
            Me.XrShape1.ForeColor = System.Drawing.Color.Purple
            Me.XrShape1.LocationFloat = New DevExpress.Utils.PointFloat(751.04!, 4.999994!)
            Me.XrShape1.Name = "XrShape1"
            Me.XrShape1.Shape = ShapeArrow1
            Me.XrShape1.SizeF = New System.Drawing.SizeF(48.96!, 23.0!)
            Me.XrShape1.StylePriority.UseForeColor = False
            '
            'DMPdebtOverview
            '
            Me.Bands.AddRange(New DevExpress.XtraReports.UI.Band() {Me.TopMarginBand1, Me.Detail, Me.BottomMarginBand1, Me.PageFooter, Me.PageHeader, Me.GroupFooter1})
            Me.RequestParameters = False
            Me.ScriptLanguage = DevExpress.XtraReports.ScriptLanguage.VisualBasic
            Me.ScriptReferencesString = resources.GetString("$this.ScriptReferencesString")
            Me.Version = "11.2"
            Me.Controls.SetChildIndex(Me.GroupFooter1, 0)
            Me.Controls.SetChildIndex(Me.PageHeader, 0)
            Me.Controls.SetChildIndex(Me.PageFooter, 0)
            Me.Controls.SetChildIndex(Me.BottomMarginBand1, 0)
            Me.Controls.SetChildIndex(Me.Detail, 0)
            Me.Controls.SetChildIndex(Me.TopMarginBand1, 0)
            CType(XyDiagram1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(StackedBarSeriesLabel1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(SideBySideStackedBarSeriesView1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Series1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(StackedBarSeriesLabel2, System.ComponentModel.ISupportInitialize).EndInit()
            CType(SideBySideStackedBarSeriesView2, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Series2, System.ComponentModel.ISupportInitialize).EndInit()
            CType(StackedBarSeriesLabel3, System.ComponentModel.ISupportInitialize).EndInit()
            CType(SideBySideStackedBarSeriesView3, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.XrChart1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me, System.ComponentModel.ISupportInitialize).EndInit()

        End Sub
        Protected Friend WithEvents XrChart1 As DevExpress.XtraReports.UI.XRChart
        Protected Friend WithEvents GroupFooter1 As DevExpress.XtraReports.UI.GroupFooterBand
        Protected Friend WithEvents XrShape1 As DevExpress.XtraReports.UI.XRShape
        Friend WithEvents XrLabel1 As DevExpress.XtraReports.UI.XRLabel
    End Class
End Namespace
