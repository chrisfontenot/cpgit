Namespace Client.PacketDMP

    <Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
    Partial Class DMPdebtDetail

        'XtraReport overrides dispose to clean up the component list.
        <System.Diagnostics.DebuggerNonUserCode()> _
        Protected Overrides Sub Dispose(ByVal disposing As Boolean)
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
            MyBase.Dispose(disposing)
        End Sub

        'Required by the Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Designer
        'It can be modified using the Designer.  
        'Do not modify it using the code editor.
        <System.Diagnostics.DebuggerStepThrough()> _
        Private Sub InitializeComponent()
            Dim XrSummary1 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary()
            Dim XrSummary2 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary()
            Dim XrSummary3 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary()
            Dim ShapeArrow1 As DevExpress.XtraPrinting.Shape.ShapeArrow = New DevExpress.XtraPrinting.Shape.ShapeArrow()
            Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(DMPdebtDetail))
            Me.GroupFooter1 = New DevExpress.XtraReports.UI.GroupFooterBand()
            Me.XrLabel_groupsum_non_dmp_payment = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel19 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_groupsum_dmp_payment = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_groupsum_balance = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel1 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrShape1 = New DevExpress.XtraReports.UI.XRShape()
            Me.XrLabel_creditor = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_balance = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_non_dmp_interest = New DevExpress.XtraReports.UI.XRLabel()
            Me.GroupHeader1 = New DevExpress.XtraReports.UI.GroupHeaderBand()
            Me.XrLabel2 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel11 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel12 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel13 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel8 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel9 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel10 = New DevExpress.XtraReports.UI.XRLabel()
            Me.GroupFooter2 = New DevExpress.XtraReports.UI.GroupFooterBand()
            Me.XrCrossBandLine1 = New DevExpress.XtraReports.UI.XRCrossBandLine()
            Me.XrCrossBandLine2 = New DevExpress.XtraReports.UI.XRCrossBandLine()
            Me.XrCrossBandLine3 = New DevExpress.XtraReports.UI.XRCrossBandLine()
            Me.XrCrossBandLine4 = New DevExpress.XtraReports.UI.XRCrossBandLine()
            Me.XrCrossBandLine5 = New DevExpress.XtraReports.UI.XRCrossBandLine()
            Me.XrLabel_dmp_interest = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_non_dmp_payment = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_dmp_payment = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrCrossBandLine6 = New DevExpress.XtraReports.UI.XRCrossBandLine()
            Me.XrLabel_dmp_payout = New DevExpress.XtraReports.UI.XRLabel()
            CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
            '
            'PageHeader
            '
            Me.PageHeader.HeightF = 107.2916!
            '
            'XrLabel_PageHeader
            '
            Me.XrLabel_PageHeader.BorderColor = System.Drawing.Color.Purple
            Me.XrLabel_PageHeader.ForeColor = System.Drawing.Color.Purple
            Me.XrLabel_PageHeader.StylePriority.UseBackColor = False
            Me.XrLabel_PageHeader.StylePriority.UseBorderColor = False
            Me.XrLabel_PageHeader.StylePriority.UseBorders = False
            Me.XrLabel_PageHeader.StylePriority.UseBorderWidth = False
            Me.XrLabel_PageHeader.StylePriority.UseFont = False
            Me.XrLabel_PageHeader.StylePriority.UseForeColor = False
            Me.XrLabel_PageHeader.StylePriority.UsePadding = False
            Me.XrLabel_PageHeader.StylePriority.UseTextAlignment = False
            Me.XrLabel_PageHeader.Text = "Debt Details"
            '
            'XrLabel_ClientID
            '
            Me.XrLabel_ClientID.StylePriority.UseFont = False
            Me.XrLabel_ClientID.StylePriority.UseForeColor = False
            Me.XrLabel_ClientID.StylePriority.UseTextAlignment = False
            '
            'Detail
            '
            Me.Detail.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel_dmp_payout, Me.XrLabel_dmp_payment, Me.XrLabel_non_dmp_payment, Me.XrLabel_dmp_interest, Me.XrLabel_non_dmp_interest, Me.XrLabel_balance, Me.XrLabel_creditor})
            Me.Detail.HeightF = 17.0!
            '
            'GroupFooter1
            '
            Me.GroupFooter1.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel_groupsum_non_dmp_payment, Me.XrLabel19, Me.XrLabel_groupsum_dmp_payment, Me.XrLabel_groupsum_balance})
            Me.GroupFooter1.Font = New System.Drawing.Font("Gill Sans MT", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.GroupFooter1.ForeColor = System.Drawing.Color.Goldenrod
            Me.GroupFooter1.HeightF = 38.0!
            Me.GroupFooter1.Name = "GroupFooter1"
            Me.GroupFooter1.StylePriority.UseFont = False
            Me.GroupFooter1.StylePriority.UseForeColor = False
            '
            'XrLabel_groupsum_non_dmp_payment
            '
            Me.XrLabel_groupsum_non_dmp_payment.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "non_dmp_payment")})
            Me.XrLabel_groupsum_non_dmp_payment.LocationFloat = New DevExpress.Utils.PointFloat(368.8333!, 13.00001!)
            Me.XrLabel_groupsum_non_dmp_payment.Name = "XrLabel_groupsum_non_dmp_payment"
            Me.XrLabel_groupsum_non_dmp_payment.Padding = New DevExpress.XtraPrinting.PaddingInfo(4, 4, 0, 0, 100.0!)
            Me.XrLabel_groupsum_non_dmp_payment.SizeF = New System.Drawing.SizeF(152.1667!, 23.0!)
            Me.XrLabel_groupsum_non_dmp_payment.StylePriority.UsePadding = False
            Me.XrLabel_groupsum_non_dmp_payment.StylePriority.UseTextAlignment = False
            XrSummary1.FormatString = "{0:$#,##0}"
            XrSummary1.Running = DevExpress.XtraReports.UI.SummaryRunning.Group
            Me.XrLabel_groupsum_non_dmp_payment.Summary = XrSummary1
            Me.XrLabel_groupsum_non_dmp_payment.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel19
            '
            Me.XrLabel19.LocationFloat = New DevExpress.Utils.PointFloat(0.00006357829!, 13.0!)
            Me.XrLabel19.Name = "XrLabel19"
            Me.XrLabel19.Padding = New DevExpress.XtraPrinting.PaddingInfo(4, 4, 0, 0, 100.0!)
            Me.XrLabel19.SizeF = New System.Drawing.SizeF(205.2083!, 23.0!)
            Me.XrLabel19.StylePriority.UsePadding = False
            Me.XrLabel19.StylePriority.UseTextAlignment = False
            Me.XrLabel19.Text = "Totals"
            Me.XrLabel19.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            '
            'XrLabel_groupsum_dmp_payment
            '
            Me.XrLabel_groupsum_dmp_payment.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "dmp_payment")})
            Me.XrLabel_groupsum_dmp_payment.LocationFloat = New DevExpress.Utils.PointFloat(533.2085!, 13.00001!)
            Me.XrLabel_groupsum_dmp_payment.Name = "XrLabel_groupsum_dmp_payment"
            Me.XrLabel_groupsum_dmp_payment.Padding = New DevExpress.XtraPrinting.PaddingInfo(4, 4, 0, 0, 100.0!)
            Me.XrLabel_groupsum_dmp_payment.SizeF = New System.Drawing.SizeF(176.6666!, 23.0!)
            Me.XrLabel_groupsum_dmp_payment.StylePriority.UsePadding = False
            Me.XrLabel_groupsum_dmp_payment.StylePriority.UseTextAlignment = False
            XrSummary2.FormatString = "{0:$#,##0}"
            XrSummary2.Running = DevExpress.XtraReports.UI.SummaryRunning.Group
            Me.XrLabel_groupsum_dmp_payment.Summary = XrSummary2
            Me.XrLabel_groupsum_dmp_payment.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel_groupsum_balance
            '
            Me.XrLabel_groupsum_balance.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "balance")})
            Me.XrLabel_groupsum_balance.LocationFloat = New DevExpress.Utils.PointFloat(223.875!, 13.00001!)
            Me.XrLabel_groupsum_balance.Name = "XrLabel_groupsum_balance"
            Me.XrLabel_groupsum_balance.Padding = New DevExpress.XtraPrinting.PaddingInfo(4, 4, 0, 0, 100.0!)
            Me.XrLabel_groupsum_balance.SizeF = New System.Drawing.SizeF(111.5417!, 23.0!)
            Me.XrLabel_groupsum_balance.StylePriority.UsePadding = False
            Me.XrLabel_groupsum_balance.StylePriority.UseTextAlignment = False
            XrSummary3.FormatString = "{0:$#,##0}"
            XrSummary3.Running = DevExpress.XtraReports.UI.SummaryRunning.Group
            Me.XrLabel_groupsum_balance.Summary = XrSummary3
            Me.XrLabel_groupsum_balance.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel1
            '
            Me.XrLabel1.Font = New System.Drawing.Font("Segoe Print", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.XrLabel1.ForeColor = System.Drawing.Color.Purple
            Me.XrLabel1.LocationFloat = New DevExpress.Utils.PointFloat(250.0!, 0.0!)
            Me.XrLabel1.Name = "XrLabel1"
            Me.XrLabel1.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 10, 0, 0, 100.0!)
            Me.XrLabel1.SizeF = New System.Drawing.SizeF(501.0417!, 32.99999!)
            Me.XrLabel1.StylePriority.UseFont = False
            Me.XrLabel1.StylePriority.UseForeColor = False
            Me.XrLabel1.StylePriority.UsePadding = False
            Me.XrLabel1.StylePriority.UseTextAlignment = False
            Me.XrLabel1.Text = "See totals & assumptions for more information"
            Me.XrLabel1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrShape1
            '
            Me.XrShape1.Angle = 270
            Me.XrShape1.ForeColor = System.Drawing.Color.Purple
            Me.XrShape1.LocationFloat = New DevExpress.Utils.PointFloat(751.0417!, 4.999994!)
            Me.XrShape1.Name = "XrShape1"
            Me.XrShape1.Shape = ShapeArrow1
            Me.XrShape1.SizeF = New System.Drawing.SizeF(48.95831!, 23.0!)
            Me.XrShape1.StylePriority.UseForeColor = False
            '
            'XrLabel_creditor
            '
            Me.XrLabel_creditor.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "combined_creditor_name")})
            Me.XrLabel_creditor.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
            Me.XrLabel_creditor.Name = "XrLabel_creditor"
            Me.XrLabel_creditor.Padding = New DevExpress.XtraPrinting.PaddingInfo(4, 4, 0, 0, 100.0!)
            Me.XrLabel_creditor.SizeF = New System.Drawing.SizeF(221.875!, 16.99987!)
            Me.XrLabel_creditor.StylePriority.UsePadding = False
            Me.XrLabel_creditor.StylePriority.UseTextAlignment = False
            Me.XrLabel_creditor.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            '
            'XrLabel_balance
            '
            Me.XrLabel_balance.AnchorVertical = CType((DevExpress.XtraReports.UI.VerticalAnchorStyles.Top Or DevExpress.XtraReports.UI.VerticalAnchorStyles.Bottom), DevExpress.XtraReports.UI.VerticalAnchorStyles)
            Me.XrLabel_balance.CanGrow = False
            Me.XrLabel_balance.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "balance", "{0:$#,##0}")})
            Me.XrLabel_balance.LocationFloat = New DevExpress.Utils.PointFloat(235.4167!, 0.0!)
            Me.XrLabel_balance.Name = "XrLabel_balance"
            Me.XrLabel_balance.Padding = New DevExpress.XtraPrinting.PaddingInfo(4, 4, 0, 0, 100.0!)
            Me.XrLabel_balance.SizeF = New System.Drawing.SizeF(100.0!, 15.99987!)
            Me.XrLabel_balance.StylePriority.UsePadding = False
            Me.XrLabel_balance.StylePriority.UseTextAlignment = False
            Me.XrLabel_balance.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel_non_dmp_interest
            '
            Me.XrLabel_non_dmp_interest.AnchorVertical = CType((DevExpress.XtraReports.UI.VerticalAnchorStyles.Top Or DevExpress.XtraReports.UI.VerticalAnchorStyles.Bottom), DevExpress.XtraReports.UI.VerticalAnchorStyles)
            Me.XrLabel_non_dmp_interest.CanGrow = False
            Me.XrLabel_non_dmp_interest.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "non_dmp_interest", "{0:#0.000%;-#0.000%;\-\-   }")})
            Me.XrLabel_non_dmp_interest.LocationFloat = New DevExpress.Utils.PointFloat(346.0!, 0.0!)
            Me.XrLabel_non_dmp_interest.Name = "XrLabel_non_dmp_interest"
            Me.XrLabel_non_dmp_interest.Padding = New DevExpress.XtraPrinting.PaddingInfo(4, 4, 0, 0, 100.0!)
            Me.XrLabel_non_dmp_interest.SizeF = New System.Drawing.SizeF(64.0!, 15.99987!)
            Me.XrLabel_non_dmp_interest.StylePriority.UsePadding = False
            Me.XrLabel_non_dmp_interest.StylePriority.UseTextAlignment = False
            Me.XrLabel_non_dmp_interest.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'GroupHeader1
            '
            Me.GroupHeader1.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel2, Me.XrLabel11, Me.XrLabel12, Me.XrLabel13, Me.XrLabel8, Me.XrLabel9, Me.XrLabel10})
            Me.GroupHeader1.ForeColor = System.Drawing.Color.Purple
            Me.GroupHeader1.GroupUnion = DevExpress.XtraReports.UI.GroupUnion.WithFirstDetail
            Me.GroupHeader1.HeightF = 31.25!
            Me.GroupHeader1.Name = "GroupHeader1"
            Me.GroupHeader1.RepeatEveryPage = True
            Me.GroupHeader1.StylePriority.UseForeColor = False
            '
            'XrLabel2
            '
            Me.XrLabel2.LocationFloat = New DevExpress.Utils.PointFloat(732.2084!, 0.0!)
            Me.XrLabel2.Name = "XrLabel2"
            Me.XrLabel2.Padding = New DevExpress.XtraPrinting.PaddingInfo(4, 4, 0, 0, 100.0!)
            Me.XrLabel2.SizeF = New System.Drawing.SizeF(67.79163!, 23.0!)
            Me.XrLabel2.StylePriority.UsePadding = False
            Me.XrLabel2.StylePriority.UseTextAlignment = False
            Me.XrLabel2.Text = "Payout"
            Me.XrLabel2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel11
            '
            Me.XrLabel11.LocationFloat = New DevExpress.Utils.PointFloat(534.0!, 0.0!)
            Me.XrLabel11.Name = "XrLabel11"
            Me.XrLabel11.Padding = New DevExpress.XtraPrinting.PaddingInfo(4, 4, 0, 0, 100.0!)
            Me.XrLabel11.SizeF = New System.Drawing.SizeF(64.91986!, 23.0!)
            Me.XrLabel11.StylePriority.UsePadding = False
            Me.XrLabel11.StylePriority.UseTextAlignment = False
            Me.XrLabel11.Text = "DMP %"
            Me.XrLabel11.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel12
            '
            Me.XrLabel12.LocationFloat = New DevExpress.Utils.PointFloat(422.0!, 1.041667!)
            Me.XrLabel12.Name = "XrLabel12"
            Me.XrLabel12.Padding = New DevExpress.XtraPrinting.PaddingInfo(4, 4, 0, 0, 100.0!)
            Me.XrLabel12.SizeF = New System.Drawing.SizeF(99.0!, 23.0!)
            Me.XrLabel12.StylePriority.UsePadding = False
            Me.XrLabel12.StylePriority.UseTextAlignment = False
            Me.XrLabel12.Text = "SELF Payment"
            Me.XrLabel12.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel13
            '
            Me.XrLabel13.LocationFloat = New DevExpress.Utils.PointFloat(0.00006357829!, 0.0!)
            Me.XrLabel13.Name = "XrLabel13"
            Me.XrLabel13.Padding = New DevExpress.XtraPrinting.PaddingInfo(4, 4, 0, 0, 100.0!)
            Me.XrLabel13.SizeF = New System.Drawing.SizeF(205.2083!, 23.0!)
            Me.XrLabel13.StylePriority.UsePadding = False
            Me.XrLabel13.StylePriority.UseTextAlignment = False
            Me.XrLabel13.Text = "Creditor"
            Me.XrLabel13.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            '
            'XrLabel8
            '
            Me.XrLabel8.LocationFloat = New DevExpress.Utils.PointFloat(610.0!, 0.0!)
            Me.XrLabel8.Name = "XrLabel8"
            Me.XrLabel8.Padding = New DevExpress.XtraPrinting.PaddingInfo(4, 4, 0, 0, 100.0!)
            Me.XrLabel8.SizeF = New System.Drawing.SizeF(99.87509!, 23.0!)
            Me.XrLabel8.StylePriority.UsePadding = False
            Me.XrLabel8.StylePriority.UseTextAlignment = False
            Me.XrLabel8.Text = "DMP Payment"
            Me.XrLabel8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel9
            '
            Me.XrLabel9.LocationFloat = New DevExpress.Utils.PointFloat(235.4167!, 0.0!)
            Me.XrLabel9.Name = "XrLabel9"
            Me.XrLabel9.Padding = New DevExpress.XtraPrinting.PaddingInfo(4, 4, 0, 0, 100.0!)
            Me.XrLabel9.SizeF = New System.Drawing.SizeF(100.0!, 23.0!)
            Me.XrLabel9.StylePriority.UsePadding = False
            Me.XrLabel9.StylePriority.UseTextAlignment = False
            Me.XrLabel9.Text = "Balance"
            Me.XrLabel9.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel10
            '
            Me.XrLabel10.LocationFloat = New DevExpress.Utils.PointFloat(346.0!, 0.0!)
            Me.XrLabel10.Name = "XrLabel10"
            Me.XrLabel10.Padding = New DevExpress.XtraPrinting.PaddingInfo(4, 4, 0, 0, 100.0!)
            Me.XrLabel10.SizeF = New System.Drawing.SizeF(64.0!, 23.0!)
            Me.XrLabel10.StylePriority.UsePadding = False
            Me.XrLabel10.StylePriority.UseTextAlignment = False
            Me.XrLabel10.Text = "SELF %"
            Me.XrLabel10.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'GroupFooter2
            '
            Me.GroupFooter2.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrShape1, Me.XrLabel1})
            Me.GroupFooter2.HeightF = 33.00001!
            Me.GroupFooter2.Level = 1
            Me.GroupFooter2.Name = "GroupFooter2"
            Me.GroupFooter2.PrintAtBottom = True
            Me.GroupFooter2.RepeatEveryPage = True
            '
            'XrCrossBandLine1
            '
            Me.XrCrossBandLine1.EndBand = Me.Detail
            Me.XrCrossBandLine1.EndPointFloat = New DevExpress.Utils.PointFloat(416.0!, 15.75001!)
            Me.XrCrossBandLine1.ForeColor = System.Drawing.Color.Purple
            Me.XrCrossBandLine1.LocationFloat = New DevExpress.Utils.PointFloat(416.0!, 0.0!)
            Me.XrCrossBandLine1.Name = "XrCrossBandLine1"
            Me.XrCrossBandLine1.StartBand = Me.GroupHeader1
            Me.XrCrossBandLine1.StartPointFloat = New DevExpress.Utils.PointFloat(416.0!, 0.0!)
            Me.XrCrossBandLine1.WidthF = 2.000031!
            '
            'XrCrossBandLine2
            '
            Me.XrCrossBandLine2.EndBand = Me.GroupFooter1
            Me.XrCrossBandLine2.EndPointFloat = New DevExpress.Utils.PointFloat(528.0!, 36.50001!)
            Me.XrCrossBandLine2.ForeColor = System.Drawing.Color.Purple
            Me.XrCrossBandLine2.LocationFloat = New DevExpress.Utils.PointFloat(528.0!, 0.0!)
            Me.XrCrossBandLine2.Name = "XrCrossBandLine2"
            Me.XrCrossBandLine2.StartBand = Me.GroupHeader1
            Me.XrCrossBandLine2.StartPointFloat = New DevExpress.Utils.PointFloat(528.0!, 0.0!)
            Me.XrCrossBandLine2.WidthF = 2.0!
            '
            'XrCrossBandLine3
            '
            Me.XrCrossBandLine3.EndBand = Me.Detail
            Me.XrCrossBandLine3.EndPointFloat = New DevExpress.Utils.PointFloat(604.0!, 16.79165!)
            Me.XrCrossBandLine3.ForeColor = System.Drawing.Color.Purple
            Me.XrCrossBandLine3.LocationFloat = New DevExpress.Utils.PointFloat(604.0!, 0.0!)
            Me.XrCrossBandLine3.Name = "XrCrossBandLine3"
            Me.XrCrossBandLine3.StartBand = Me.GroupHeader1
            Me.XrCrossBandLine3.StartPointFloat = New DevExpress.Utils.PointFloat(604.0!, 0.0!)
            Me.XrCrossBandLine3.WidthF = 2.0!
            '
            'XrCrossBandLine4
            '
            Me.XrCrossBandLine4.EndBand = Me.GroupFooter1
            Me.XrCrossBandLine4.EndPointFloat = New DevExpress.Utils.PointFloat(340.0!, 36.5!)
            Me.XrCrossBandLine4.ForeColor = System.Drawing.Color.Purple
            Me.XrCrossBandLine4.LocationFloat = New DevExpress.Utils.PointFloat(340.0!, 0.0!)
            Me.XrCrossBandLine4.Name = "XrCrossBandLine4"
            Me.XrCrossBandLine4.StartBand = Me.GroupHeader1
            Me.XrCrossBandLine4.StartPointFloat = New DevExpress.Utils.PointFloat(340.0!, 0.0!)
            Me.XrCrossBandLine4.WidthF = 2.0!
            '
            'XrCrossBandLine5
            '
            Me.XrCrossBandLine5.EndBand = Me.GroupFooter1
            Me.XrCrossBandLine5.EndPointFloat = New DevExpress.Utils.PointFloat(221.875!, 36.5!)
            Me.XrCrossBandLine5.ForeColor = System.Drawing.Color.Purple
            Me.XrCrossBandLine5.LocationFloat = New DevExpress.Utils.PointFloat(221.875!, 0.0!)
            Me.XrCrossBandLine5.Name = "XrCrossBandLine5"
            Me.XrCrossBandLine5.StartBand = Me.GroupHeader1
            Me.XrCrossBandLine5.StartPointFloat = New DevExpress.Utils.PointFloat(221.875!, 0.0!)
            Me.XrCrossBandLine5.WidthF = 2.0!
            '
            'XrLabel_dmp_interest
            '
            Me.XrLabel_dmp_interest.AnchorVertical = CType((DevExpress.XtraReports.UI.VerticalAnchorStyles.Top Or DevExpress.XtraReports.UI.VerticalAnchorStyles.Bottom), DevExpress.XtraReports.UI.VerticalAnchorStyles)
            Me.XrLabel_dmp_interest.CanGrow = False
            Me.XrLabel_dmp_interest.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "dmp_interest", "{0:#0.000%;-#0.000%;\-\-   }")})
            Me.XrLabel_dmp_interest.LocationFloat = New DevExpress.Utils.PointFloat(534.0!, 0.00003178914!)
            Me.XrLabel_dmp_interest.Name = "XrLabel_dmp_interest"
            Me.XrLabel_dmp_interest.Padding = New DevExpress.XtraPrinting.PaddingInfo(4, 4, 0, 0, 100.0!)
            Me.XrLabel_dmp_interest.SizeF = New System.Drawing.SizeF(64.91986!, 15.99987!)
            Me.XrLabel_dmp_interest.StylePriority.UsePadding = False
            Me.XrLabel_dmp_interest.StylePriority.UseTextAlignment = False
            Me.XrLabel_dmp_interest.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel_non_dmp_payment
            '
            Me.XrLabel_non_dmp_payment.AnchorVertical = CType((DevExpress.XtraReports.UI.VerticalAnchorStyles.Top Or DevExpress.XtraReports.UI.VerticalAnchorStyles.Bottom), DevExpress.XtraReports.UI.VerticalAnchorStyles)
            Me.XrLabel_non_dmp_payment.CanGrow = False
            Me.XrLabel_non_dmp_payment.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "non_dmp_payment", "{0:$#,##0}")})
            Me.XrLabel_non_dmp_payment.LocationFloat = New DevExpress.Utils.PointFloat(422.0!, 0.0!)
            Me.XrLabel_non_dmp_payment.Name = "XrLabel_non_dmp_payment"
            Me.XrLabel_non_dmp_payment.Padding = New DevExpress.XtraPrinting.PaddingInfo(4, 4, 0, 0, 100.0!)
            Me.XrLabel_non_dmp_payment.SizeF = New System.Drawing.SizeF(99.0!, 15.99987!)
            Me.XrLabel_non_dmp_payment.StylePriority.UsePadding = False
            Me.XrLabel_non_dmp_payment.StylePriority.UseTextAlignment = False
            Me.XrLabel_non_dmp_payment.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel_dmp_payment
            '
            Me.XrLabel_dmp_payment.AnchorVertical = CType((DevExpress.XtraReports.UI.VerticalAnchorStyles.Top Or DevExpress.XtraReports.UI.VerticalAnchorStyles.Bottom), DevExpress.XtraReports.UI.VerticalAnchorStyles)
            Me.XrLabel_dmp_payment.CanGrow = False
            Me.XrLabel_dmp_payment.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "dmp_payment", "{0:$#,##0}")})
            Me.XrLabel_dmp_payment.LocationFloat = New DevExpress.Utils.PointFloat(610.0!, 0.00003178914!)
            Me.XrLabel_dmp_payment.Name = "XrLabel_dmp_payment"
            Me.XrLabel_dmp_payment.Padding = New DevExpress.XtraPrinting.PaddingInfo(4, 4, 0, 0, 100.0!)
            Me.XrLabel_dmp_payment.SizeF = New System.Drawing.SizeF(99.87509!, 15.99987!)
            Me.XrLabel_dmp_payment.StylePriority.UsePadding = False
            Me.XrLabel_dmp_payment.StylePriority.UseTextAlignment = False
            Me.XrLabel_dmp_payment.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrCrossBandLine6
            '
            Me.XrCrossBandLine6.EndBand = Me.GroupFooter1
            Me.XrCrossBandLine6.EndPointFloat = New DevExpress.Utils.PointFloat(716.0!, 37.5!)
            Me.XrCrossBandLine6.ForeColor = System.Drawing.Color.Purple
            Me.XrCrossBandLine6.LocationFloat = New DevExpress.Utils.PointFloat(716.0!, 1.041667!)
            Me.XrCrossBandLine6.Name = "XrCrossBandLine6"
            Me.XrCrossBandLine6.StartBand = Me.GroupHeader1
            Me.XrCrossBandLine6.StartPointFloat = New DevExpress.Utils.PointFloat(716.0!, 1.041667!)
            Me.XrCrossBandLine6.WidthF = 2.0!
            '
            'XrLabel_dmp_payout
            '
            Me.XrLabel_dmp_payout.AnchorVertical = CType((DevExpress.XtraReports.UI.VerticalAnchorStyles.Top Or DevExpress.XtraReports.UI.VerticalAnchorStyles.Bottom), DevExpress.XtraReports.UI.VerticalAnchorStyles)
            Me.XrLabel_dmp_payout.CanGrow = False
            Me.XrLabel_dmp_payout.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "dmp_payout", "{0:M/yyyy}")})
            Me.XrLabel_dmp_payout.LocationFloat = New DevExpress.Utils.PointFloat(718.6667!, 0.0!)
            Me.XrLabel_dmp_payout.Name = "XrLabel_dmp_payout"
            Me.XrLabel_dmp_payout.Padding = New DevExpress.XtraPrinting.PaddingInfo(4, 4, 0, 0, 100.0!)
            Me.XrLabel_dmp_payout.SizeF = New System.Drawing.SizeF(81.33!, 15.99987!)
            Me.XrLabel_dmp_payout.StylePriority.UsePadding = False
            Me.XrLabel_dmp_payout.StylePriority.UseTextAlignment = False
            Me.XrLabel_dmp_payout.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'DMPdebtDetail
            '
            Me.Bands.AddRange(New DevExpress.XtraReports.UI.Band() {Me.TopMarginBand1, Me.Detail, Me.BottomMarginBand1, Me.PageFooter, Me.PageHeader, Me.GroupFooter1, Me.GroupHeader1, Me.GroupFooter2})
            Me.CrossBandControls.AddRange(New DevExpress.XtraReports.UI.XRCrossBandControl() {Me.XrCrossBandLine6, Me.XrCrossBandLine5, Me.XrCrossBandLine4, Me.XrCrossBandLine3, Me.XrCrossBandLine2, Me.XrCrossBandLine1})
            Me.DesignerOptions.ShowExportWarnings = False
            Me.DesignerOptions.ShowPrintingWarnings = False
            Me.Font = New System.Drawing.Font("Gill Sans MT", 9.75!)
            Me.ReportPrintOptions.DetailCountOnEmptyDataSource = 0
            Me.ReportPrintOptions.PrintOnEmptyDataSource = False
            Me.RequestParameters = False
            Me.ScriptLanguage = DevExpress.XtraReports.ScriptLanguage.VisualBasic
            Me.ScriptReferencesString = resources.GetString("$this.ScriptReferencesString")
            Me.ShowPrintMarginsWarning = False
            Me.ShowPrintStatusDialog = False
            Me.Version = "11.2"
            Me.Controls.SetChildIndex(Me.GroupFooter2, 0)
            Me.Controls.SetChildIndex(Me.GroupHeader1, 0)
            Me.Controls.SetChildIndex(Me.GroupFooter1, 0)
            Me.Controls.SetChildIndex(Me.PageHeader, 0)
            Me.Controls.SetChildIndex(Me.PageFooter, 0)
            Me.Controls.SetChildIndex(Me.BottomMarginBand1, 0)
            Me.Controls.SetChildIndex(Me.Detail, 0)
            Me.Controls.SetChildIndex(Me.TopMarginBand1, 0)
            CType(Me, System.ComponentModel.ISupportInitialize).EndInit()

        End Sub

        Friend WithEvents GroupFooter1 As DevExpress.XtraReports.UI.GroupFooterBand
        Friend WithEvents XrLabel1 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_non_dmp_interest As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_balance As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_creditor As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_groupsum_non_dmp_payment As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel19 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_groupsum_dmp_payment As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_groupsum_balance As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrShape1 As DevExpress.XtraReports.UI.XRShape
        Friend WithEvents GroupHeader1 As DevExpress.XtraReports.UI.GroupHeaderBand
        Friend WithEvents XrLabel11 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel12 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel13 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel8 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel9 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel10 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents GroupFooter2 As DevExpress.XtraReports.UI.GroupFooterBand
        Friend WithEvents XrCrossBandLine1 As DevExpress.XtraReports.UI.XRCrossBandLine
        Friend WithEvents XrCrossBandLine2 As DevExpress.XtraReports.UI.XRCrossBandLine
        Friend WithEvents XrCrossBandLine3 As DevExpress.XtraReports.UI.XRCrossBandLine
        Friend WithEvents XrCrossBandLine4 As DevExpress.XtraReports.UI.XRCrossBandLine
        Friend WithEvents XrCrossBandLine5 As DevExpress.XtraReports.UI.XRCrossBandLine
        Friend WithEvents XrLabel_dmp_payment As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_non_dmp_payment As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_dmp_interest As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_dmp_payout As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel2 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrCrossBandLine6 As DevExpress.XtraReports.UI.XRCrossBandLine
    End Class
End Namespace
