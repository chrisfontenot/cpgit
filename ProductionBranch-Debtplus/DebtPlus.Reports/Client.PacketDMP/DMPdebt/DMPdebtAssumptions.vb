﻿Imports System.Drawing.Printing

Namespace Client.PacketDMP
    Public Class DMPdebtAssumptions
        Inherits CommonReportPortrait

        Public Sub New()
            MyBase.New()
            InitializeComponent()

            AddHandler XrTableCell_Difference.BeforePrint, AddressOf XrTableCell_Difference_BeforePrint
            AddHandler XrTableCell_TotalBalance.BeforePrint, AddressOf XrTableCell_TotalBalance_BeforePrint
            AddHandler XrTableCell_TotalDMPPayment.BeforePrint, AddressOf XrTableCell_TotalDMPPayment_BeforePrint
            AddHandler XrTableCell_TotalPayment.BeforePrint, AddressOf XrTableCell_TotalPayment_BeforePrint
        End Sub

        Private Sub XrTableCell_TotalBalance_BeforePrint(sender As Object, e As PrintEventArgs)
            With CType(sender, DevExpress.XtraReports.UI.XRLabel)
                .Text = String.Format("{0:$#,##0;-$#,##0;$0}", Find_balance(sender))
            End With
        End Sub

        Private Sub XrTableCell_TotalDMPPayment_BeforePrint(sender As Object, e As PrintEventArgs)
            With CType(sender, DevExpress.XtraReports.UI.XRLabel)
                .Text = String.Format("{0:$#,##0;-$#,##0;$0}", Find_dmp_payment(sender))
            End With
        End Sub

        Private Sub XrTableCell_TotalPayment_BeforePrint(sender As Object, e As PrintEventArgs)
            With CType(sender, DevExpress.XtraReports.UI.XRLabel)
                .Text = String.Format("{0:$#,##0;-$#,##0;$0}", Find_non_dmp_payment(sender))
            End With
        End Sub

        Private Sub XrTableCell_Difference_BeforePrint(sender As Object, e As PrintEventArgs)
            With CType(sender, DevExpress.XtraReports.UI.XRLabel)
                .Text = String.Format("{0:$#,##0;-$#,##0;$0}", find_difference(sender))
            End With
        End Sub

        Private Function Find_balance(ByVal Sender As Object) As Decimal
            With CType(Sender, DevExpress.XtraReports.UI.XRLabel)
                Dim rpt As DevExpress.XtraReports.UI.XtraReport = CType(.Report, DevExpress.XtraReports.UI.XtraReport)
                Dim MasterRpt As DevExpress.XtraReports.UI.XtraReport = CType(rpt.MasterReport, DevExpress.XtraReports.UI.XtraReport)
                Dim ds As DataSet = CType(MasterRpt.DataSource, DataView).Table.DataSet
                Dim tbl As DataTable = ds.Tables("rpt_PrintClient_CreditorDebt")
                Dim sum_balance As Decimal = DebtPlus.Utils.Nulls.DDec(tbl.Compute("sum(balance)", String.Empty))
                Return sum_balance
            End With
        End Function

        Private Function Find_non_dmp_payment(ByVal Sender As Object) As Decimal
            With CType(Sender, DevExpress.XtraReports.UI.XRLabel)
                Dim rpt As DevExpress.XtraReports.UI.XtraReport = CType(.Report, DevExpress.XtraReports.UI.XtraReport)
                Dim MasterRpt As DevExpress.XtraReports.UI.XtraReport = CType(rpt.MasterReport, DevExpress.XtraReports.UI.XtraReport)
                Dim ds As DataSet = CType(MasterRpt.DataSource, DataView).Table.DataSet
                Dim tbl As DataTable = ds.Tables("rpt_PrintClient_CreditorDebt")
                Dim sum_balance As Decimal = DebtPlus.Utils.Nulls.DDec(tbl.Compute("sum(non_dmp_payment)", String.Empty))
                Return sum_balance
            End With
        End Function

        Private Function Find_dmp_payment(ByVal Sender As Object) As Decimal
            With CType(Sender, DevExpress.XtraReports.UI.XRLabel)
                Dim rpt As DevExpress.XtraReports.UI.XtraReport = CType(.Report, DevExpress.XtraReports.UI.XtraReport)
                Dim MasterRpt As DevExpress.XtraReports.UI.XtraReport = CType(rpt.MasterReport, DevExpress.XtraReports.UI.XtraReport)
                Dim ds As DataSet = CType(MasterRpt.DataSource, DataView).Table.DataSet
                Dim tbl As DataTable = ds.Tables("rpt_PrintClient_CreditorDebt")
                Dim sum_balance As Decimal = DebtPlus.Utils.Nulls.DDec(tbl.Compute("sum(dmp_payment)", String.Empty))
                Return sum_balance
            End With
        End Function

        Private Function find_difference(ByVal sender As Object) As Decimal
            Return Find_non_dmp_payment(sender) - Find_dmp_payment(sender)
        End Function
    End Class
End Namespace