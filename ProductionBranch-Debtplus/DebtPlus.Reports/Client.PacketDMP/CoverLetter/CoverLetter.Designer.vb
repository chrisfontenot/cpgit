Namespace Client.PacketDMP
    <Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
    Partial Class CoverLetter

        'XtraReport overrides dispose to clean up the component list.
        <System.Diagnostics.DebuggerNonUserCode()> _
        Protected Overrides Sub Dispose(ByVal disposing As Boolean)
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
            MyBase.Dispose(disposing)
        End Sub

        'Required by the Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Designer
        'It can be modified using the Designer.  
        'Do not modify it using the code editor.
        <System.Diagnostics.DebuggerStepThrough()> _
        Private Sub InitializeComponent()
            Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(CoverLetter))
            Me.XrRichText1 = New DevExpress.XtraReports.UI.XRRichText()
            Me.ReportHeader = New DevExpress.XtraReports.UI.ReportHeaderBand()
            Me.XrPictureBox1 = New DevExpress.XtraReports.UI.XRPictureBox()
            Me.ReportFooter = New DevExpress.XtraReports.UI.ReportFooterBand()
            Me.XrPictureBox5 = New DevExpress.XtraReports.UI.XRPictureBox()
            Me.XrPictureBox4 = New DevExpress.XtraReports.UI.XRPictureBox()
            Me.XrPictureBox3 = New DevExpress.XtraReports.UI.XRPictureBox()
            Me.XrPictureBox2 = New DevExpress.XtraReports.UI.XRPictureBox()
            Me.XrLabel1 = New DevExpress.XtraReports.UI.XRLabel()
            Me.SalutationName = New DevExpress.XtraReports.UI.CalculatedField()
            Me.FinancialSpecialistName = New DevExpress.XtraReports.UI.CalculatedField()
            Me.Today = New DevExpress.XtraReports.UI.CalculatedField()
            CType(Me.XrRichText1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
            '
            'Detail
            '
            Me.Detail.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrRichText1})
            Me.Detail.HeightF = 35.0!
            Me.Detail.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
            Me.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
            '
            'XrRichText1
            '
            Me.XrRichText1.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
            Me.XrRichText1.Name = "XrRichText1"
            Me.XrRichText1.Padding = New DevExpress.XtraPrinting.PaddingInfo(5, 5, 5, 5, 100.0!)
            Me.XrRichText1.SizeF = New System.Drawing.SizeF(800.0!, 35.0!)
            Me.XrRichText1.StylePriority.UsePadding = False
            '
            'ReportHeader
            '
            Me.ReportHeader.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrPictureBox1})
            Me.ReportHeader.HeightF = 114.5833!
            Me.ReportHeader.Name = "ReportHeader"
            '
            'XrPictureBox1
            '
            Me.XrPictureBox1.Image = CType(resources.GetObject("XrPictureBox1.Image"), System.Drawing.Image)
            Me.XrPictureBox1.LocationFloat = New DevExpress.Utils.PointFloat(275.0!, 0.0!)
            Me.XrPictureBox1.Name = "XrPictureBox1"
            Me.XrPictureBox1.SizeF = New System.Drawing.SizeF(194.0!, 100.0!)
            Me.XrPictureBox1.Sizing = DevExpress.XtraPrinting.ImageSizeMode.ZoomImage
            '
            'ReportFooter
            '
            Me.ReportFooter.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrPictureBox5, Me.XrPictureBox4, Me.XrPictureBox3, Me.XrPictureBox2, Me.XrLabel1})
            Me.ReportFooter.HeightF = 74.00001!
            Me.ReportFooter.Name = "ReportFooter"
            Me.ReportFooter.PrintAtBottom = True
            '
            'XrPictureBox5
            '
            Me.XrPictureBox5.Image = CType(resources.GetObject("XrPictureBox5.Image"), System.Drawing.Image)
            Me.XrPictureBox5.LocationFloat = New DevExpress.Utils.PointFloat(510.0!, 0.0!)
            Me.XrPictureBox5.Name = "XrPictureBox5"
            Me.XrPictureBox5.SizeF = New System.Drawing.SizeF(79.0!, 74.0!)
            Me.XrPictureBox5.Sizing = DevExpress.XtraPrinting.ImageSizeMode.ZoomImage
            '
            'XrPictureBox4
            '
            Me.XrPictureBox4.Image = CType(resources.GetObject("XrPictureBox4.Image"), System.Drawing.Image)
            Me.XrPictureBox4.LocationFloat = New DevExpress.Utils.PointFloat(612.0!, 0.0!)
            Me.XrPictureBox4.Name = "XrPictureBox4"
            Me.XrPictureBox4.SizeF = New System.Drawing.SizeF(75.0!, 74.0!)
            Me.XrPictureBox4.Sizing = DevExpress.XtraPrinting.ImageSizeMode.ZoomImage
            '
            'XrPictureBox3
            '
            Me.XrPictureBox3.Image = CType(resources.GetObject("XrPictureBox3.Image"), System.Drawing.Image)
            Me.XrPictureBox3.LocationFloat = New DevExpress.Utils.PointFloat(175.0!, 0.0!)
            Me.XrPictureBox3.Name = "XrPictureBox3"
            Me.XrPictureBox3.SizeF = New System.Drawing.SizeF(46.0!, 74.0!)
            Me.XrPictureBox3.Sizing = DevExpress.XtraPrinting.ImageSizeMode.ZoomImage
            '
            'XrPictureBox2
            '
            Me.XrPictureBox2.Image = CType(resources.GetObject("XrPictureBox2.Image"), System.Drawing.Image)
            Me.XrPictureBox2.LocationFloat = New DevExpress.Utils.PointFloat(77.0!, 0.0!)
            Me.XrPictureBox2.Name = "XrPictureBox2"
            Me.XrPictureBox2.SizeF = New System.Drawing.SizeF(46.0!, 74.0!)
            Me.XrPictureBox2.Sizing = DevExpress.XtraPrinting.ImageSizeMode.ZoomImage
            '
            'XrLabel1
            '
            Me.XrLabel1.Font = New System.Drawing.Font("Arial", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.XrLabel1.LocationFloat = New DevExpress.Utils.PointFloat(275.0!, 0.0!)
            Me.XrLabel1.Multiline = True
            Me.XrLabel1.Name = "XrLabel1"
            Me.XrLabel1.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel1.SizeF = New System.Drawing.SizeF(204.1667!, 73.99998!)
            Me.XrLabel1.StylePriority.UseFont = False
            Me.XrLabel1.StylePriority.UseTextAlignment = False
            Me.XrLabel1.Text = "www.ClearPointCCS.org" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "1-877-877-1995"
            Me.XrLabel1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
            '
            'SalutationName
            '
            Me.SalutationName.FieldType = DevExpress.XtraReports.UI.FieldType.[String]
            Me.SalutationName.Name = "SalutationName"
            '
            'FinancialSpecialistName
            '
            Me.FinancialSpecialistName.FieldType = DevExpress.XtraReports.UI.FieldType.[String]
            Me.FinancialSpecialistName.Name = "FinancialSpecialistName"
            '
            'Today
            '
            Me.Today.FieldType = DevExpress.XtraReports.UI.FieldType.DateTime
            Me.Today.Name = "Today"
            '
            'CoverLetter
            '
            Me.Bands.AddRange(New DevExpress.XtraReports.UI.Band() {Me.TopMarginBand1, Me.Detail, Me.BottomMarginBand1, Me.ReportHeader, Me.ReportFooter})
            Me.CalculatedFields.AddRange(New DevExpress.XtraReports.UI.CalculatedField() {Me.SalutationName, Me.FinancialSpecialistName, Me.Today})
            Me.ReportPrintOptions.DetailCount = 1
            Me.RequestParameters = False
            Me.ScriptLanguage = DevExpress.XtraReports.ScriptLanguage.VisualBasic
            Me.ScriptReferencesString = resources.GetString("$this.ScriptReferencesString")
            Me.Version = "11.2"
            Me.Controls.SetChildIndex(Me.ReportFooter, 0)
            Me.Controls.SetChildIndex(Me.ReportHeader, 0)
            Me.Controls.SetChildIndex(Me.BottomMarginBand1, 0)
            Me.Controls.SetChildIndex(Me.Detail, 0)
            Me.Controls.SetChildIndex(Me.TopMarginBand1, 0)
            CType(Me.XrRichText1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me, System.ComponentModel.ISupportInitialize).EndInit()

        End Sub
        Friend WithEvents XrRichText1 As DevExpress.XtraReports.UI.XRRichText
        Friend WithEvents ReportHeader As DevExpress.XtraReports.UI.ReportHeaderBand
        Friend WithEvents XrPictureBox1 As DevExpress.XtraReports.UI.XRPictureBox
        Friend WithEvents ReportFooter As DevExpress.XtraReports.UI.ReportFooterBand
        Friend WithEvents XrLabel1 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrPictureBox5 As DevExpress.XtraReports.UI.XRPictureBox
        Friend WithEvents XrPictureBox4 As DevExpress.XtraReports.UI.XRPictureBox
        Friend WithEvents XrPictureBox3 As DevExpress.XtraReports.UI.XRPictureBox
        Friend WithEvents XrPictureBox2 As DevExpress.XtraReports.UI.XRPictureBox
        Friend WithEvents SalutationName As DevExpress.XtraReports.UI.CalculatedField
        Friend WithEvents FinancialSpecialistName As DevExpress.XtraReports.UI.CalculatedField
        Friend WithEvents Today As DevExpress.XtraReports.UI.CalculatedField
    End Class
End Namespace
