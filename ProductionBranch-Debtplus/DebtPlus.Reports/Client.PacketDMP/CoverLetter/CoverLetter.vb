#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}

#End Region

Imports DebtPlus.Reports.Template
Imports System.Drawing.Printing
Imports DebtPlus.Utils
Imports System.IO
Imports System.Reflection
Imports System.Globalization
Imports System.Data.SqlClient

Namespace Client.PacketDMP
    Public Class CoverLetter
        Inherits BaseXtraReportClass

        Public Sub New()
            MyBase.New()
            InitializeComponent()
            ScriptReferences = DebtPlus.Utils.AssemblyTools.GetAllSiblingAssemblies()
            AddHandler BeforePrint, AddressOf CoverLetter_BeforePrint
            AddHandler SalutationName.GetValue, AddressOf SalutationName_GetValue
            AddHandler FinancialSpecialistName.GetValue, AddressOf FinancialSpecialistName_GetValue
            AddHandler Today.GetValue, AddressOf Today_GetValue
        End Sub

        Private Sub CoverLetter_BeforePrint(ByVal sender As Object, ByVal e As PrintEventArgs)
            Const RootNameSpace As String = "DebtPlus.Reports.Client.Packet.DMP"
            Const Fname As String = "DMP Cover Letter Text.rtf"
            Dim sb As String = String.Empty

            ' Try to find the text document.
            Dim FileName As String = CommonReportPortrait.FindDocument(Fname)
            If FileName <> String.Empty Then
                Using fs As StreamReader = File.OpenText(FileName)
                    sb = fs.ReadToEnd()
                End Using
            End If

            ' If there is no letter text then try the copy in the dll assembly
            If sb.Length < 1 Then
                Dim _assembly As Assembly = Assembly.GetExecutingAssembly
                Using ios As Stream = _assembly.GetManifestResourceStream(String.Format("{0}.{1}", RootNameSpace, Fname))
                    Using fs As New StreamReader(ios)
                        sb = fs.ReadToEnd
                    End Using
                End Using
            End If

            ' Replace the font with the standard font name but keep the formatting.
            With XrRichText1
#If 0 Then
            ' Generate the regular expression to process the font references
            Dim MatchEval As New System.Text.RegularExpressions.MatchEvaluator(AddressOf FontNameReplaceEvaluator)
            Dim rxPattern As New System.Text.RegularExpressions.Regex("\{(?<leadin>\\f\d[^ ]* )(?<font>[^;]+)\;\}")
            sb = rxPattern.Replace(sb, MatchEval)
#End If
                .Rtf = sb
            End With

            Dim LocalRpt As DevExpress.XtraReports.UI.XtraReport = CType(sender, DevExpress.XtraReports.UI.XtraReport)
            Dim MasterRpt As DevExpress.XtraReports.UI.XtraReport = LocalRpt.MasterReport
            Dim Client As Int32 = Convert.ToInt32(MasterRpt.Parameters("ParameterClient").Value, CultureInfo.InvariantCulture)
            Dim ds As DataSet = CType(MasterRpt.DataSource, DataView).Table.DataSet

            Using cmd As New System.Data.SqlClient.SqlCommand()
                With cmd
                    .Connection = New System.Data.SqlClient.SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
                    .CommandText = "SELECT [client],[active_status],[client_status],[start_date],[zipcode],[name],[coapplicant],[last_name_first],[addr1],[addr2],[addr3],[salutation],[phone],[app_work_phone],[app_cell_phone],[coapp_work_phone],[coapp_cell_phone],[language],[city],[state],[date_created],[ssn_1],[ssn_2],[office],[counselor],[counselor_name],[work_ph],[work_ext],[message_ph],[mail_error_date],[LastName],[FirstName],[EmailAddress] FROM view_client_address WHERE [client]=@Client"
                    .CommandType = CommandType.Text
                    .Parameters.Add("@Client", SqlDbType.Int).Value = Client
                End With

                Using da As New SqlDataAdapter(cmd)
                    da.Fill(ds, "view_client_address")
                End Using
            End Using

            ' Set the report source
            With LocalRpt
                .DataSource = ds.Tables("view_client_address").DefaultView

                ' Set the data source for any calculated fields
                For Each calcField As DevExpress.XtraReports.UI.CalculatedField In LocalRpt.CalculatedFields
                    With calcField
                        .Assign(LocalRpt.DataSource, LocalRpt.DataMember)
                    End With
                Next
            End With
        End Sub

        Private Sub SalutationName_GetValue(sender As Object, e As DevExpress.XtraReports.UI.GetValueEventArgs)
            With CType(sender, DevExpress.XtraReports.UI.CalculatedField)
                'Dim rpt As XtraReport = e.Report
                Dim row As DataRowView = CType(e.Row, DataRowView)
                Dim Text As String = DebtPlus.Utils.Nulls.DStr(row("salutation")).Trim()
                e.Value = CommonReportPortrait.ToNormalCase(Text)
            End With
        End Sub

        Private Sub FinancialSpecialistName_GetValue(sender As Object, e As DevExpress.XtraReports.UI.GetValueEventArgs)
            With CType(sender, DevExpress.XtraReports.UI.CalculatedField)
                'Dim rpt As XtraReport = e.Report
                Dim row As DataRowView = CType(e.Row, DataRowView)
                Dim Text As String = DebtPlus.Utils.Nulls.DStr(row("counselor_name")).Trim()
                e.Value = CommonReportPortrait.ToNormalCase(Text)
            End With
        End Sub

        Private Sub Today_GetValue(sender As Object, e As DevExpress.XtraReports.UI.GetValueEventArgs)
            e.Value = DateTime.Now
        End Sub
    End Class
End Namespace