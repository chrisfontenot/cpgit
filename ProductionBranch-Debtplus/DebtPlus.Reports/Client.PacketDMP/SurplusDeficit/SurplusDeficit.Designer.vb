Namespace Client.PacketDMP
    <Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
    Partial Class SurplusDeficit

        'XtraReport overrides dispose to clean up the component list.
        <System.Diagnostics.DebuggerNonUserCode()> _
        Protected Overrides Sub Dispose(ByVal disposing As Boolean)
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
            MyBase.Dispose(disposing)
        End Sub

        'Required by the Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Designer
        'It can be modified using the Designer.  
        'Do not modify it using the code editor.
        <System.Diagnostics.DebuggerStepThrough()> _
        Private Sub InitializeComponent()
            Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(SurplusDeficit))
            Me.XrControlStyle_PageHeaderBlock = New DevExpress.XtraReports.UI.XRControlStyle()
            Me.XrTable3 = New DevExpress.XtraReports.UI.XRTable()
            Me.XrTableRow11 = New DevExpress.XtraReports.UI.XRTableRow()
            Me.XrTableCell21 = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableCell22 = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableRow12 = New DevExpress.XtraReports.UI.XRTableRow()
            Me.XrTableCell23 = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableCell_available_funds = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableRow13 = New DevExpress.XtraReports.UI.XRTableRow()
            Me.XrTableCell25 = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableCell_program_fees = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableRow15 = New DevExpress.XtraReports.UI.XRTableRow()
            Me.XrTableCell29 = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableCell_grand_total = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTable2 = New DevExpress.XtraReports.UI.XRTable()
            Me.XrTableRow6 = New DevExpress.XtraReports.UI.XRTableRow()
            Me.XrTableCell11 = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableCell12 = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableRow7 = New DevExpress.XtraReports.UI.XRTableRow()
            Me.XrTableCell13 = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableCell_total_budget_expenses = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableRow8 = New DevExpress.XtraReports.UI.XRTableRow()
            Me.XrTableCell15 = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableCell_other_debt = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableRow10 = New DevExpress.XtraReports.UI.XRTableRow()
            Me.XrTableCell19 = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableCell_subtotal_expenses = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTable1 = New DevExpress.XtraReports.UI.XRTable()
            Me.XrTableRow1 = New DevExpress.XtraReports.UI.XRTableRow()
            Me.XrTableCell3 = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableCell1 = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableRow2 = New DevExpress.XtraReports.UI.XRTableRow()
            Me.XrTableCell2 = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableCell_payroll_1 = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableRow3 = New DevExpress.XtraReports.UI.XRTableRow()
            Me.XrTableCell5 = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableCell_payroll_2 = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableRow4 = New DevExpress.XtraReports.UI.XRTableRow()
            Me.XrTableCell7 = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableCell_assets = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableRow5 = New DevExpress.XtraReports.UI.XRTableRow()
            Me.XrTableCell9 = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableCell_total_assets = New DevExpress.XtraReports.UI.XRTableCell()
            CType(Me.XrTable3, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.XrTable2, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.XrTable1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
            '
            'PageHeader
            '
            Me.PageHeader.HeightF = 107.375!
            '
            'XrLabel_PageHeader
            '
            Me.XrLabel_PageHeader.BorderColor = System.Drawing.Color.FromArgb(CType(CType(17, Byte), Int32), CType(CType(107, Byte), Int32), CType(CType(82, Byte), Int32))
            Me.XrLabel_PageHeader.ForeColor = System.Drawing.Color.FromArgb(CType(CType(17, Byte), Int32), CType(CType(107, Byte), Int32), CType(CType(82, Byte), Int32))
            Me.XrLabel_PageHeader.LocationFloat = New DevExpress.Utils.PointFloat(0.00001589457!, 0.0!)
            Me.XrLabel_PageHeader.StyleName = "XrControlStyle_PageHeaderBlock"
            Me.XrLabel_PageHeader.StylePriority.UseBackColor = False
            Me.XrLabel_PageHeader.StylePriority.UseBorderColor = False
            Me.XrLabel_PageHeader.StylePriority.UseBorders = False
            Me.XrLabel_PageHeader.StylePriority.UseBorderWidth = False
            Me.XrLabel_PageHeader.StylePriority.UseFont = False
            Me.XrLabel_PageHeader.StylePriority.UseForeColor = False
            Me.XrLabel_PageHeader.StylePriority.UsePadding = False
            Me.XrLabel_PageHeader.StylePriority.UseTextAlignment = False
            Me.XrLabel_PageHeader.Text = "Monthly Surplus/Deficit"
            '
            'XrLabel_ClientID
            '
            Me.XrLabel_ClientID.StylePriority.UseFont = False
            Me.XrLabel_ClientID.StylePriority.UseForeColor = False
            Me.XrLabel_ClientID.StylePriority.UseTextAlignment = False
            '
            'Detail
            '
            Me.Detail.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrTable3, Me.XrTable2, Me.XrTable1})
            Me.Detail.HeightF = 398.5416!
            Me.Detail.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
            Me.Detail.PageBreak = DevExpress.XtraReports.UI.PageBreak.AfterBand
            Me.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
            '
            'XrControlStyle_PageHeaderBlock
            '
            Me.XrControlStyle_PageHeaderBlock.Name = "XrControlStyle_PageHeaderBlock"
            Me.XrControlStyle_PageHeaderBlock.Padding = New DevExpress.XtraPrinting.PaddingInfo(50, 2, 0, 12, 100.0!)
            Me.XrControlStyle_PageHeaderBlock.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft
            '
            'XrTable3
            '
            Me.XrTable3.LocationFloat = New DevExpress.Utils.PointFloat(53.125!, 285.0!)
            Me.XrTable3.Name = "XrTable3"
            Me.XrTable3.Rows.AddRange(New DevExpress.XtraReports.UI.XRTableRow() {Me.XrTableRow11, Me.XrTableRow12, Me.XrTableRow13, Me.XrTableRow15})
            Me.XrTable3.SizeF = New System.Drawing.SizeF(647.9167!, 100.0!)
            Me.XrTable3.StylePriority.UseTextAlignment = False
            Me.XrTable3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            '
            'XrTableRow11
            '
            Me.XrTableRow11.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.XrTableCell21, Me.XrTableCell22})
            Me.XrTableRow11.Name = "XrTableRow11"
            Me.XrTableRow11.Weight = 1.0R
            '
            'XrTableCell21
            '
            Me.XrTableCell21.BorderColor = System.Drawing.Color.FromArgb(CType(CType(17, Byte), Int32), CType(CType(107, Byte), Int32), CType(CType(82, Byte), Int32))
            Me.XrTableCell21.Borders = DevExpress.XtraPrinting.BorderSide.Bottom
            Me.XrTableCell21.Font = New System.Drawing.Font("Gill Sans MT", 14.0!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.XrTableCell21.Name = "XrTableCell21"
            Me.XrTableCell21.Padding = New DevExpress.XtraPrinting.PaddingInfo(5, 0, 0, 0, 100.0!)
            Me.XrTableCell21.StylePriority.UseBorderColor = False
            Me.XrTableCell21.StylePriority.UseBorders = False
            Me.XrTableCell21.StylePriority.UseFont = False
            Me.XrTableCell21.StylePriority.UsePadding = False
            Me.XrTableCell21.Text = "Surplus/Deficit"
            Me.XrTableCell21.Weight = 3.8700420637143886R
            '
            'XrTableCell22
            '
            Me.XrTableCell22.Name = "XrTableCell22"
            Me.XrTableCell22.Weight = 3.3591248064027996R
            '
            'XrTableRow12
            '
            Me.XrTableRow12.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.XrTableCell23, Me.XrTableCell_available_funds})
            Me.XrTableRow12.Name = "XrTableRow12"
            Me.XrTableRow12.Weight = 1.0R
            '
            'XrTableCell23
            '
            Me.XrTableCell23.Name = "XrTableCell23"
            Me.XrTableCell23.Padding = New DevExpress.XtraPrinting.PaddingInfo(5, 0, 0, 0, 100.0!)
            Me.XrTableCell23.StylePriority.UsePadding = False
            Me.XrTableCell23.Text = "Subtotal Surplus"
            Me.XrTableCell23.Weight = 5.4791668701171883R
            '
            'XrTableCell_available_funds
            '
            Me.XrTableCell_available_funds.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "available_funds", "{0:$#,##0}")})
            Me.XrTableCell_available_funds.Name = "XrTableCell_available_funds"
            Me.XrTableCell_available_funds.StylePriority.UseTextAlignment = False
            Me.XrTableCell_available_funds.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrTableCell_available_funds.Weight = 1.75R
            '
            'XrTableRow13
            '
            Me.XrTableRow13.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.XrTableCell25, Me.XrTableCell_program_fees})
            Me.XrTableRow13.Name = "XrTableRow13"
            Me.XrTableRow13.Weight = 1.0R
            '
            'XrTableCell25
            '
            Me.XrTableCell25.Name = "XrTableCell25"
            Me.XrTableCell25.Padding = New DevExpress.XtraPrinting.PaddingInfo(5, 0, 0, 0, 100.0!)
            Me.XrTableCell25.StylePriority.UsePadding = False
            Me.XrTableCell25.Text = "Proposed Debt Management Program"
            Me.XrTableCell25.Weight = 5.4791668701171883R
            '
            'XrTableCell_program_fees
            '
            Me.XrTableCell_program_fees.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "program_fees", "{0:$#,##0}")})
            Me.XrTableCell_program_fees.Name = "XrTableCell_program_fees"
            Me.XrTableCell_program_fees.StylePriority.UseTextAlignment = False
            Me.XrTableCell_program_fees.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrTableCell_program_fees.Weight = 1.75R
            '
            'XrTableRow15
            '
            Me.XrTableRow15.BorderColor = System.Drawing.Color.DarkGray
            Me.XrTableRow15.Borders = DevExpress.XtraPrinting.BorderSide.Top
            Me.XrTableRow15.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.XrTableCell29, Me.XrTableCell_grand_total})
            Me.XrTableRow15.Name = "XrTableRow15"
            Me.XrTableRow15.StylePriority.UseBorderColor = False
            Me.XrTableRow15.StylePriority.UseBorders = False
            Me.XrTableRow15.Weight = 1.0R
            '
            'XrTableCell29
            '
            Me.XrTableCell29.Font = New System.Drawing.Font("Gill Sans MT", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.XrTableCell29.ForeColor = System.Drawing.Color.Goldenrod
            Me.XrTableCell29.Name = "XrTableCell29"
            Me.XrTableCell29.Padding = New DevExpress.XtraPrinting.PaddingInfo(5, 0, 0, 0, 100.0!)
            Me.XrTableCell29.StylePriority.UseFont = False
            Me.XrTableCell29.StylePriority.UseForeColor = False
            Me.XrTableCell29.StylePriority.UsePadding = False
            Me.XrTableCell29.Text = "Total Monthly Surplus"
            Me.XrTableCell29.Weight = 5.4791668701171883R
            '
            'XrTableCell_grand_total
            '
            Me.XrTableCell_grand_total.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "grand_total", "{0:$#,##0}")})
            Me.XrTableCell_grand_total.Font = New System.Drawing.Font("Gill Sans MT", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.XrTableCell_grand_total.ForeColor = System.Drawing.Color.Goldenrod
            Me.XrTableCell_grand_total.Name = "XrTableCell_grand_total"
            Me.XrTableCell_grand_total.StylePriority.UseFont = False
            Me.XrTableCell_grand_total.StylePriority.UseForeColor = False
            Me.XrTableCell_grand_total.StylePriority.UseTextAlignment = False
            Me.XrTableCell_grand_total.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrTableCell_grand_total.Weight = 1.75R
            '
            'XrTable2
            '
            Me.XrTable2.LocationFloat = New DevExpress.Utils.PointFloat(53.125!, 160.0!)
            Me.XrTable2.Name = "XrTable2"
            Me.XrTable2.Rows.AddRange(New DevExpress.XtraReports.UI.XRTableRow() {Me.XrTableRow6, Me.XrTableRow7, Me.XrTableRow8, Me.XrTableRow10})
            Me.XrTable2.SizeF = New System.Drawing.SizeF(647.9167!, 100.0!)
            Me.XrTable2.StylePriority.UseTextAlignment = False
            Me.XrTable2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            '
            'XrTableRow6
            '
            Me.XrTableRow6.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.XrTableCell11, Me.XrTableCell12})
            Me.XrTableRow6.Name = "XrTableRow6"
            Me.XrTableRow6.Weight = 1.0R
            '
            'XrTableCell11
            '
            Me.XrTableCell11.BorderColor = System.Drawing.Color.FromArgb(CType(CType(17, Byte), Int32), CType(CType(107, Byte), Int32), CType(CType(82, Byte), Int32))
            Me.XrTableCell11.Borders = DevExpress.XtraPrinting.BorderSide.Bottom
            Me.XrTableCell11.Font = New System.Drawing.Font("Gill Sans MT", 14.0!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.XrTableCell11.Name = "XrTableCell11"
            Me.XrTableCell11.Padding = New DevExpress.XtraPrinting.PaddingInfo(5, 0, 0, 0, 100.0!)
            Me.XrTableCell11.StylePriority.UseBorderColor = False
            Me.XrTableCell11.StylePriority.UseBorders = False
            Me.XrTableCell11.StylePriority.UseFont = False
            Me.XrTableCell11.StylePriority.UsePadding = False
            Me.XrTableCell11.Text = "Expenses"
            Me.XrTableCell11.Weight = 3.8700424042159827R
            '
            'XrTableCell12
            '
            Me.XrTableCell12.Name = "XrTableCell12"
            Me.XrTableCell12.Weight = 3.3591244659012056R
            '
            'XrTableRow7
            '
            Me.XrTableRow7.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.XrTableCell13, Me.XrTableCell_total_budget_expenses})
            Me.XrTableRow7.Name = "XrTableRow7"
            Me.XrTableRow7.Weight = 1.0R
            '
            'XrTableCell13
            '
            Me.XrTableCell13.Name = "XrTableCell13"
            Me.XrTableCell13.Padding = New DevExpress.XtraPrinting.PaddingInfo(5, 0, 0, 0, 100.0!)
            Me.XrTableCell13.StylePriority.UsePadding = False
            Me.XrTableCell13.Text = "Budgeted Expenses"
            Me.XrTableCell13.Weight = 5.4791668701171883R
            '
            'XrTableCell_total_budget_expenses
            '
            Me.XrTableCell_total_budget_expenses.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "budget_expenses", "{0:$#,##0}")})
            Me.XrTableCell_total_budget_expenses.Name = "XrTableCell_total_budget_expenses"
            Me.XrTableCell_total_budget_expenses.StylePriority.UseTextAlignment = False
            Me.XrTableCell_total_budget_expenses.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrTableCell_total_budget_expenses.Weight = 1.75R
            '
            'XrTableRow8
            '
            Me.XrTableRow8.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.XrTableCell15, Me.XrTableCell_other_debt})
            Me.XrTableRow8.Name = "XrTableRow8"
            Me.XrTableRow8.Weight = 1.0R
            '
            'XrTableCell15
            '
            Me.XrTableCell15.Name = "XrTableCell15"
            Me.XrTableCell15.Padding = New DevExpress.XtraPrinting.PaddingInfo(5, 0, 0, 0, 100.0!)
            Me.XrTableCell15.StylePriority.UsePadding = False
            Me.XrTableCell15.Text = "Other Non-DMP Payments"
            Me.XrTableCell15.Weight = 5.4791668701171883R
            '
            'XrTableCell_other_debt
            '
            Me.XrTableCell_other_debt.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "other_debt", "{0:$#,##0}")})
            Me.XrTableCell_other_debt.Name = "XrTableCell_other_debt"
            Me.XrTableCell_other_debt.StylePriority.UseTextAlignment = False
            Me.XrTableCell_other_debt.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrTableCell_other_debt.Weight = 1.75R
            '
            'XrTableRow10
            '
            Me.XrTableRow10.BorderColor = System.Drawing.Color.DarkGray
            Me.XrTableRow10.Borders = DevExpress.XtraPrinting.BorderSide.Top
            Me.XrTableRow10.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.XrTableCell19, Me.XrTableCell_subtotal_expenses})
            Me.XrTableRow10.Name = "XrTableRow10"
            Me.XrTableRow10.StylePriority.UseBorderColor = False
            Me.XrTableRow10.StylePriority.UseBorders = False
            Me.XrTableRow10.Weight = 1.0R
            '
            'XrTableCell19
            '
            Me.XrTableCell19.Font = New System.Drawing.Font("Gill Sans MT", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.XrTableCell19.Name = "XrTableCell19"
            Me.XrTableCell19.Padding = New DevExpress.XtraPrinting.PaddingInfo(5, 0, 0, 0, 100.0!)
            Me.XrTableCell19.StylePriority.UseFont = False
            Me.XrTableCell19.StylePriority.UsePadding = False
            Me.XrTableCell19.Text = "Total Budgeted Expenses"
            Me.XrTableCell19.Weight = 5.4791668701171883R
            '
            'XrTableCell_subtotal_expenses
            '
            Me.XrTableCell_subtotal_expenses.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "total_expenses", "{0:$#,##0}")})
            Me.XrTableCell_subtotal_expenses.Font = New System.Drawing.Font("Gill Sans MT", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.XrTableCell_subtotal_expenses.Name = "XrTableCell_subtotal_expenses"
            Me.XrTableCell_subtotal_expenses.StylePriority.UseFont = False
            Me.XrTableCell_subtotal_expenses.StylePriority.UseTextAlignment = False
            Me.XrTableCell_subtotal_expenses.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrTableCell_subtotal_expenses.Weight = 1.75R
            '
            'XrTable1
            '
            Me.XrTable1.LocationFloat = New DevExpress.Utils.PointFloat(53.125!, 10.00001!)
            Me.XrTable1.Name = "XrTable1"
            Me.XrTable1.Rows.AddRange(New DevExpress.XtraReports.UI.XRTableRow() {Me.XrTableRow1, Me.XrTableRow2, Me.XrTableRow3, Me.XrTableRow4, Me.XrTableRow5})
            Me.XrTable1.SizeF = New System.Drawing.SizeF(647.9167!, 125.0!)
            Me.XrTable1.StylePriority.UseTextAlignment = False
            Me.XrTable1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            '
            'XrTableRow1
            '
            Me.XrTableRow1.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.XrTableCell3, Me.XrTableCell1})
            Me.XrTableRow1.Name = "XrTableRow1"
            Me.XrTableRow1.Weight = 1.0R
            '
            'XrTableCell3
            '
            Me.XrTableCell3.BorderColor = System.Drawing.Color.FromArgb(CType(CType(17, Byte), Int32), CType(CType(107, Byte), Int32), CType(CType(82, Byte), Int32))
            Me.XrTableCell3.Borders = DevExpress.XtraPrinting.BorderSide.Bottom
            Me.XrTableCell3.Font = New System.Drawing.Font("Gill Sans MT", 14.0!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.XrTableCell3.Name = "XrTableCell3"
            Me.XrTableCell3.Padding = New DevExpress.XtraPrinting.PaddingInfo(5, 0, 0, 0, 100.0!)
            Me.XrTableCell3.StylePriority.UseBorderColor = False
            Me.XrTableCell3.StylePriority.UseBorders = False
            Me.XrTableCell3.StylePriority.UseFont = False
            Me.XrTableCell3.StylePriority.UsePadding = False
            Me.XrTableCell3.Text = "Incomes"
            Me.XrTableCell3.Weight = 3.8700427447175767R
            '
            'XrTableCell1
            '
            Me.XrTableCell1.Name = "XrTableCell1"
            Me.XrTableCell1.Weight = 3.3591241253996116R
            '
            'XrTableRow2
            '
            Me.XrTableRow2.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.XrTableCell2, Me.XrTableCell_payroll_1})
            Me.XrTableRow2.Name = "XrTableRow2"
            Me.XrTableRow2.Weight = 1.0R
            '
            'XrTableCell2
            '
            Me.XrTableCell2.Name = "XrTableCell2"
            Me.XrTableCell2.Padding = New DevExpress.XtraPrinting.PaddingInfo(5, 0, 0, 0, 100.0!)
            Me.XrTableCell2.StylePriority.UsePadding = False
            Me.XrTableCell2.Text = "Applicant Employment Net Monthly Income"
            Me.XrTableCell2.Weight = 5.4791668701171883R
            '
            'XrTableCell_payroll_1
            '
            Me.XrTableCell_payroll_1.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "payroll_1", "{0:$#,##0}")})
            Me.XrTableCell_payroll_1.Name = "XrTableCell_payroll_1"
            Me.XrTableCell_payroll_1.StylePriority.UseTextAlignment = False
            Me.XrTableCell_payroll_1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrTableCell_payroll_1.Weight = 1.75R
            '
            'XrTableRow3
            '
            Me.XrTableRow3.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.XrTableCell5, Me.XrTableCell_payroll_2})
            Me.XrTableRow3.Name = "XrTableRow3"
            Me.XrTableRow3.Weight = 1.0R
            '
            'XrTableCell5
            '
            Me.XrTableCell5.Name = "XrTableCell5"
            Me.XrTableCell5.Padding = New DevExpress.XtraPrinting.PaddingInfo(5, 0, 0, 0, 100.0!)
            Me.XrTableCell5.StylePriority.UsePadding = False
            Me.XrTableCell5.Text = "Co-Applicant Employment Net Monthly Income"
            Me.XrTableCell5.Weight = 5.4791668701171883R
            '
            'XrTableCell_payroll_2
            '
            Me.XrTableCell_payroll_2.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "payroll_2", "{0:$#,##0}")})
            Me.XrTableCell_payroll_2.Name = "XrTableCell_payroll_2"
            Me.XrTableCell_payroll_2.StylePriority.UseTextAlignment = False
            Me.XrTableCell_payroll_2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrTableCell_payroll_2.Weight = 1.75R
            '
            'XrTableRow4
            '
            Me.XrTableRow4.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.XrTableCell7, Me.XrTableCell_assets})
            Me.XrTableRow4.Name = "XrTableRow4"
            Me.XrTableRow4.Weight = 1.0R
            '
            'XrTableCell7
            '
            Me.XrTableCell7.Name = "XrTableCell7"
            Me.XrTableCell7.Padding = New DevExpress.XtraPrinting.PaddingInfo(5, 0, 0, 0, 100.0!)
            Me.XrTableCell7.StylePriority.UsePadding = False
            Me.XrTableCell7.Text = "Other Monthly Income"
            Me.XrTableCell7.Weight = 5.4791668701171883R
            '
            'XrTableCell_assets
            '
            Me.XrTableCell_assets.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "assets", "{0:$#,##0}")})
            Me.XrTableCell_assets.Name = "XrTableCell_assets"
            Me.XrTableCell_assets.StylePriority.UseTextAlignment = False
            Me.XrTableCell_assets.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrTableCell_assets.Weight = 1.75R
            '
            'XrTableRow5
            '
            Me.XrTableRow5.BorderColor = System.Drawing.Color.DarkGray
            Me.XrTableRow5.Borders = DevExpress.XtraPrinting.BorderSide.Top
            Me.XrTableRow5.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.XrTableCell9, Me.XrTableCell_total_assets})
            Me.XrTableRow5.Name = "XrTableRow5"
            Me.XrTableRow5.StylePriority.UseBorderColor = False
            Me.XrTableRow5.StylePriority.UseBorders = False
            Me.XrTableRow5.Weight = 1.0R
            '
            'XrTableCell9
            '
            Me.XrTableCell9.BorderColor = System.Drawing.Color.DarkGray
            Me.XrTableCell9.Borders = DevExpress.XtraPrinting.BorderSide.Top
            Me.XrTableCell9.Font = New System.Drawing.Font("Gill Sans MT", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.XrTableCell9.Name = "XrTableCell9"
            Me.XrTableCell9.Padding = New DevExpress.XtraPrinting.PaddingInfo(5, 0, 0, 0, 100.0!)
            Me.XrTableCell9.StylePriority.UseBorderColor = False
            Me.XrTableCell9.StylePriority.UseBorders = False
            Me.XrTableCell9.StylePriority.UseFont = False
            Me.XrTableCell9.StylePriority.UsePadding = False
            Me.XrTableCell9.Text = "Total Monthly Income"
            Me.XrTableCell9.Weight = 5.4791668701171883R
            '
            'XrTableCell_total_assets
            '
            Me.XrTableCell_total_assets.BorderColor = System.Drawing.Color.DarkGray
            Me.XrTableCell_total_assets.Borders = DevExpress.XtraPrinting.BorderSide.Top
            Me.XrTableCell_total_assets.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "total_assets", "{0:$#,##0}")})
            Me.XrTableCell_total_assets.Font = New System.Drawing.Font("Gill Sans MT", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.XrTableCell_total_assets.Name = "XrTableCell_total_assets"
            Me.XrTableCell_total_assets.StylePriority.UseBorderColor = False
            Me.XrTableCell_total_assets.StylePriority.UseBorders = False
            Me.XrTableCell_total_assets.StylePriority.UseFont = False
            Me.XrTableCell_total_assets.StylePriority.UseTextAlignment = False
            Me.XrTableCell_total_assets.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrTableCell_total_assets.Weight = 1.75R
            '
            'SurplusDeficit
            '
            Me.Bands.AddRange(New DevExpress.XtraReports.UI.Band() {Me.Detail, Me.PageFooter, Me.PageHeader, Me.TopMarginBand1, Me.BottomMarginBand1})
            Me.ScriptLanguage = DevExpress.XtraReports.ScriptLanguage.VisualBasic
            Me.ScriptReferencesString = resources.GetString("$this.ScriptReferencesString")
            Me.StyleSheet.AddRange(New DevExpress.XtraReports.UI.XRControlStyle() {Me.XrControlStyle_PageHeaderBlock})
            Me.TitleColor = System.Drawing.Color.Brown
            Me.TitleString = "Monthly Surplus/Deficit"
            Me.Version = "11.2"
            CType(Me.XrTable3, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.XrTable2, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.XrTable1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me, System.ComponentModel.ISupportInitialize).EndInit()

        End Sub
        Friend WithEvents XrControlStyle_PageHeaderBlock As DevExpress.XtraReports.UI.XRControlStyle
        Friend WithEvents XrTable3 As DevExpress.XtraReports.UI.XRTable
        Friend WithEvents XrTableRow11 As DevExpress.XtraReports.UI.XRTableRow
        Friend WithEvents XrTableCell21 As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents XrTableCell22 As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents XrTableRow12 As DevExpress.XtraReports.UI.XRTableRow
        Friend WithEvents XrTableCell23 As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents XrTableCell_available_funds As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents XrTableRow13 As DevExpress.XtraReports.UI.XRTableRow
        Friend WithEvents XrTableCell25 As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents XrTableCell_program_fees As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents XrTableRow15 As DevExpress.XtraReports.UI.XRTableRow
        Friend WithEvents XrTableCell29 As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents XrTableCell_grand_total As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents XrTable2 As DevExpress.XtraReports.UI.XRTable
        Friend WithEvents XrTableRow6 As DevExpress.XtraReports.UI.XRTableRow
        Friend WithEvents XrTableCell11 As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents XrTableCell12 As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents XrTableRow7 As DevExpress.XtraReports.UI.XRTableRow
        Friend WithEvents XrTableCell13 As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents XrTableCell_total_budget_expenses As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents XrTableRow8 As DevExpress.XtraReports.UI.XRTableRow
        Friend WithEvents XrTableCell15 As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents XrTableCell_other_debt As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents XrTableRow10 As DevExpress.XtraReports.UI.XRTableRow
        Friend WithEvents XrTableCell19 As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents XrTableCell_subtotal_expenses As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents XrTable1 As DevExpress.XtraReports.UI.XRTable
        Friend WithEvents XrTableRow1 As DevExpress.XtraReports.UI.XRTableRow
        Friend WithEvents XrTableCell3 As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents XrTableCell1 As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents XrTableRow2 As DevExpress.XtraReports.UI.XRTableRow
        Friend WithEvents XrTableCell2 As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents XrTableCell_payroll_1 As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents XrTableRow3 As DevExpress.XtraReports.UI.XRTableRow
        Friend WithEvents XrTableCell5 As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents XrTableCell_payroll_2 As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents XrTableRow4 As DevExpress.XtraReports.UI.XRTableRow
        Friend WithEvents XrTableCell7 As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents XrTableCell_assets As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents XrTableRow5 As DevExpress.XtraReports.UI.XRTableRow
        Friend WithEvents XrTableCell9 As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents XrTableCell_total_assets As DevExpress.XtraReports.UI.XRTableCell
    End Class
End Namespace
