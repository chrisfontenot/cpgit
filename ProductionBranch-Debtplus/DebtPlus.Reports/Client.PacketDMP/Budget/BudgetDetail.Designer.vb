Imports DebtPlus.Reports.Client.PacketDMP

Namespace Client.PacketDMP

    <Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
    Partial Class BudgetDetail

        'XtraReport overrides dispose to clean up the component list.
        <System.Diagnostics.DebuggerNonUserCode()> _
        Protected Overrides Sub Dispose(ByVal disposing As Boolean)
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
            MyBase.Dispose(disposing)
        End Sub

        'Required by the Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Designer
        'It can be modified using the Designer.  
        'Do not modify it using the code editor.
        <System.Diagnostics.DebuggerStepThrough()> _
        Private Sub InitializeComponent()
            Dim XrSummary1 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary()
            Dim XrSummary2 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary()
            Dim XrSummary3 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary()
            Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(BudgetDetail))
            Me.XrControlStyle_PageHeaderBlock = New DevExpress.XtraReports.UI.XRControlStyle()
            Me.XrLabel_difference = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_description = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_client_amount = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_suggested_amount = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel2 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel9 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel8 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel7 = New DevExpress.XtraReports.UI.XRLabel()
            Me.ReportFooter = New DevExpress.XtraReports.UI.ReportFooterBand()
            Me.XrLabel14 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_total_client_amount = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_total_suggested_amount = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_total_difference = New DevExpress.XtraReports.UI.XRLabel()
            Me.GroupHeader1 = New DevExpress.XtraReports.UI.GroupHeaderBand()
            Me.XrLabel_heading_description = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrCrossBandLine1 = New DevExpress.XtraReports.UI.XRCrossBandLine()
            Me.XrCrossBandLine2 = New DevExpress.XtraReports.UI.XRCrossBandLine()
            Me.XrCrossBandLine3 = New DevExpress.XtraReports.UI.XRCrossBandLine()
            Me.XrCrossBandLine4 = New DevExpress.XtraReports.UI.XRCrossBandLine()
            Me.XrLabel1 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel3 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel4 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel5 = New DevExpress.XtraReports.UI.XRLabel()
            Me.r_client = New DevExpress.XtraReports.UI.CalculatedField()
            Me.r_suggested = New DevExpress.XtraReports.UI.CalculatedField()
            Me.r_difference = New DevExpress.XtraReports.UI.CalculatedField()
            CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
            '
            'PageHeader
            '
            Me.PageHeader.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel5, Me.XrLabel4, Me.XrLabel3, Me.XrLabel1})
            Me.PageHeader.HeightF = 136.75!
            Me.PageHeader.Controls.SetChildIndex(Me.XrLabel1, 0)
            Me.PageHeader.Controls.SetChildIndex(Me.XrLabel3, 0)
            Me.PageHeader.Controls.SetChildIndex(Me.XrLabel4, 0)
            Me.PageHeader.Controls.SetChildIndex(Me.XrLabel5, 0)
            Me.PageHeader.Controls.SetChildIndex(Me.XrLabel_PageHeader, 0)
            '
            'XrLabel_PageHeader
            '
            Me.XrLabel_PageHeader.StylePriority.UseBackColor = False
            Me.XrLabel_PageHeader.StylePriority.UseBorderColor = False
            Me.XrLabel_PageHeader.StylePriority.UseBorders = False
            Me.XrLabel_PageHeader.StylePriority.UseBorderWidth = False
            Me.XrLabel_PageHeader.StylePriority.UseFont = False
            Me.XrLabel_PageHeader.StylePriority.UseForeColor = False
            Me.XrLabel_PageHeader.StylePriority.UsePadding = False
            Me.XrLabel_PageHeader.StylePriority.UseTextAlignment = False
            Me.XrLabel_PageHeader.Text = "Budget Detail"
            '
            'XrLabel_ClientID
            '
            Me.XrLabel_ClientID.StylePriority.UseFont = False
            Me.XrLabel_ClientID.StylePriority.UseForeColor = False
            Me.XrLabel_ClientID.StylePriority.UseTextAlignment = False
            '
            'Detail
            '
            Me.Detail.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel_difference, Me.XrLabel_description, Me.XrLabel_client_amount, Me.XrLabel_suggested_amount})
            Me.Detail.HeightF = 14.66666!
            '
            'XrControlStyle_PageHeaderBlock
            '
            Me.XrControlStyle_PageHeaderBlock.Font = New System.Drawing.Font("Gill Sans MT", 24.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.XrControlStyle_PageHeaderBlock.ForeColor = System.Drawing.Color.White
            Me.XrControlStyle_PageHeaderBlock.Name = "XrControlStyle_PageHeaderBlock"
            Me.XrControlStyle_PageHeaderBlock.Padding = New DevExpress.XtraPrinting.PaddingInfo(50, 2, 0, 12, 100.0!)
            Me.XrControlStyle_PageHeaderBlock.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft
            '
            'XrLabel_difference
            '
            Me.XrLabel_difference.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "r_difference", "{0:$#,##0;-$#,##0; }")})
            Me.XrLabel_difference.LocationFloat = New DevExpress.Utils.PointFloat(676.0416!, 0.0!)
            Me.XrLabel_difference.Name = "XrLabel_difference"
            Me.XrLabel_difference.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_difference.SizeF = New System.Drawing.SizeF(99.99997!, 14.66666!)
            Me.XrLabel_difference.StylePriority.UseTextAlignment = False
            Me.XrLabel_difference.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel_description
            '
            Me.XrLabel_description.LocationFloat = New DevExpress.Utils.PointFloat(75.0!, 0.0!)
            Me.XrLabel_description.Name = "XrLabel_description"
            Me.XrLabel_description.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_description.Scripts.OnBeforePrint = "XrLabel_description_BeforePrint"
            Me.XrLabel_description.SizeF = New System.Drawing.SizeF(320.8333!, 13.95833!)
            Me.XrLabel_description.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            '
            'XrLabel_client_amount
            '
            Me.XrLabel_client_amount.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "r_client", "{0:$#,##0;-$#,##0; }")})
            Me.XrLabel_client_amount.LocationFloat = New DevExpress.Utils.PointFloat(422.5695!, 0.0!)
            Me.XrLabel_client_amount.Name = "XrLabel_client_amount"
            Me.XrLabel_client_amount.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_client_amount.SizeF = New System.Drawing.SizeF(100.0!, 14.66666!)
            Me.XrLabel_client_amount.StylePriority.UseTextAlignment = False
            Me.XrLabel_client_amount.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel_suggested_amount
            '
            Me.XrLabel_suggested_amount.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "r_suggested", "{0:$#,##0;-$#,##0; }")})
            Me.XrLabel_suggested_amount.LocationFloat = New DevExpress.Utils.PointFloat(549.3055!, 0.0!)
            Me.XrLabel_suggested_amount.Name = "XrLabel_suggested_amount"
            Me.XrLabel_suggested_amount.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_suggested_amount.SizeF = New System.Drawing.SizeF(99.99997!, 14.66666!)
            Me.XrLabel_suggested_amount.StylePriority.UseTextAlignment = False
            Me.XrLabel_suggested_amount.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel2
            '
            Me.XrLabel2.LocationFloat = New DevExpress.Utils.PointFloat(53.125!, 187.5!)
            Me.XrLabel2.Name = "XrLabel2"
            Me.XrLabel2.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel2.SizeF = New System.Drawing.SizeF(248.9583!, 23.0!)
            Me.XrLabel2.Text = "CATEGORY"
            '
            'XrLabel9
            '
            Me.XrLabel9.LocationFloat = New DevExpress.Utils.PointFloat(422.5695!, 190.625!)
            Me.XrLabel9.Name = "XrLabel9"
            Me.XrLabel9.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel9.SizeF = New System.Drawing.SizeF(100.0!, 22.99999!)
            Me.XrLabel9.StylePriority.UseTextAlignment = False
            Me.XrLabel9.Text = "CURRENT"
            Me.XrLabel9.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel8
            '
            Me.XrLabel8.LocationFloat = New DevExpress.Utils.PointFloat(549.3056!, 190.625!)
            Me.XrLabel8.Name = "XrLabel8"
            Me.XrLabel8.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel8.SizeF = New System.Drawing.SizeF(100.0!, 22.99999!)
            Me.XrLabel8.StylePriority.UseTextAlignment = False
            Me.XrLabel8.Text = "SUGGESTED"
            Me.XrLabel8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel7
            '
            Me.XrLabel7.LocationFloat = New DevExpress.Utils.PointFloat(676.0417!, 190.625!)
            Me.XrLabel7.Name = "XrLabel7"
            Me.XrLabel7.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel7.SizeF = New System.Drawing.SizeF(100.0!, 22.99999!)
            Me.XrLabel7.StylePriority.UseTextAlignment = False
            Me.XrLabel7.Text = "DIFFERENCE"
            Me.XrLabel7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'ReportFooter
            '
            Me.ReportFooter.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel14, Me.XrLabel_total_client_amount, Me.XrLabel_total_suggested_amount, Me.XrLabel_total_difference})
            Me.ReportFooter.Font = New System.Drawing.Font("Gill Sans MT", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.ReportFooter.ForeColor = System.Drawing.Color.Goldenrod
            Me.ReportFooter.HeightF = 33.25001!
            Me.ReportFooter.Name = "ReportFooter"
            Me.ReportFooter.StylePriority.UseFont = False
            Me.ReportFooter.StylePriority.UseForeColor = False
            Me.ReportFooter.StylePriority.UseTextAlignment = False
            Me.ReportFooter.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            '
            'XrLabel14
            '
            Me.XrLabel14.Font = New System.Drawing.Font("Gill Sans MT", 9.75!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.XrLabel14.LocationFloat = New DevExpress.Utils.PointFloat(53.125!, 10.00001!)
            Me.XrLabel14.Name = "XrLabel14"
            Me.XrLabel14.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel14.SizeF = New System.Drawing.SizeF(342.7083!, 23.25!)
            Me.XrLabel14.StylePriority.UseFont = False
            Me.XrLabel14.Text = "Monthly Total"
            '
            'XrLabel_total_client_amount
            '
            Me.XrLabel_total_client_amount.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "r_client")})
            Me.XrLabel_total_client_amount.LocationFloat = New DevExpress.Utils.PointFloat(422.5695!, 10.00001!)
            Me.XrLabel_total_client_amount.Name = "XrLabel_total_client_amount"
            Me.XrLabel_total_client_amount.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_total_client_amount.SizeF = New System.Drawing.SizeF(100.0!, 23.25!)
            Me.XrLabel_total_client_amount.StylePriority.UseTextAlignment = False
            XrSummary1.FormatString = "{0:$#,##0;-$#,##0; }"
            XrSummary1.Running = DevExpress.XtraReports.UI.SummaryRunning.Report
            Me.XrLabel_total_client_amount.Summary = XrSummary1
            Me.XrLabel_total_client_amount.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel_total_suggested_amount
            '
            Me.XrLabel_total_suggested_amount.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "r_suggested")})
            Me.XrLabel_total_suggested_amount.LocationFloat = New DevExpress.Utils.PointFloat(549.3056!, 10.00001!)
            Me.XrLabel_total_suggested_amount.Name = "XrLabel_total_suggested_amount"
            Me.XrLabel_total_suggested_amount.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_total_suggested_amount.SizeF = New System.Drawing.SizeF(99.99994!, 23.25!)
            Me.XrLabel_total_suggested_amount.StylePriority.UseTextAlignment = False
            XrSummary2.FormatString = "{0:$#,##0;-$#,##0; }"
            XrSummary2.Running = DevExpress.XtraReports.UI.SummaryRunning.Report
            Me.XrLabel_total_suggested_amount.Summary = XrSummary2
            Me.XrLabel_total_suggested_amount.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel_total_difference
            '
            Me.XrLabel_total_difference.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "r_difference")})
            Me.XrLabel_total_difference.LocationFloat = New DevExpress.Utils.PointFloat(676.0417!, 10.00001!)
            Me.XrLabel_total_difference.Name = "XrLabel_total_difference"
            Me.XrLabel_total_difference.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_total_difference.SizeF = New System.Drawing.SizeF(99.99994!, 23.25!)
            Me.XrLabel_total_difference.StylePriority.UseTextAlignment = False
            XrSummary3.FormatString = "{0:$#,##0;-$#,##0; }"
            XrSummary3.Running = DevExpress.XtraReports.UI.SummaryRunning.Report
            Me.XrLabel_total_difference.Summary = XrSummary3
            Me.XrLabel_total_difference.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'GroupHeader1
            '
            Me.GroupHeader1.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel_heading_description})
            Me.GroupHeader1.Font = New System.Drawing.Font("Gill Sans MT", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.GroupHeader1.GroupFields.AddRange(New DevExpress.XtraReports.UI.GroupField() {New DevExpress.XtraReports.UI.GroupField("heading_category", DevExpress.XtraReports.UI.XRColumnSortOrder.Ascending)})
            Me.GroupHeader1.GroupUnion = DevExpress.XtraReports.UI.GroupUnion.WithFirstDetail
            Me.GroupHeader1.HeightF = 36.87503!
            Me.GroupHeader1.Name = "GroupHeader1"
            Me.GroupHeader1.RepeatEveryPage = True
            Me.GroupHeader1.StylePriority.UseFont = False
            Me.GroupHeader1.StylePriority.UseTextAlignment = False
            Me.GroupHeader1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            '
            'XrLabel_heading_description
            '
            Me.XrLabel_heading_description.Font = New System.Drawing.Font("Gill Sans MT", 9.75!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.XrLabel_heading_description.LocationFloat = New DevExpress.Utils.PointFloat(53.125!, 13.87503!)
            Me.XrLabel_heading_description.Name = "XrLabel_heading_description"
            Me.XrLabel_heading_description.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_heading_description.Scripts.OnBeforePrint = "XrLabel_heading_description_BeforePrint"
            Me.XrLabel_heading_description.SizeF = New System.Drawing.SizeF(342.7083!, 23.0!)
            Me.XrLabel_heading_description.StylePriority.UseFont = False
            Me.XrLabel_heading_description.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            '
            'XrCrossBandLine1
            '
            Me.XrCrossBandLine1.EndBand = Me.ReportFooter
            Me.XrCrossBandLine1.EndPointFloat = New DevExpress.Utils.PointFloat(413.5417!, 30.37167!)
            Me.XrCrossBandLine1.ForeColor = System.Drawing.Color.Teal
            Me.XrCrossBandLine1.LocationFloat = New DevExpress.Utils.PointFloat(413.5417!, 108.0!)
            Me.XrCrossBandLine1.Name = "XrCrossBandLine1"
            Me.XrCrossBandLine1.StartBand = Me.PageHeader
            Me.XrCrossBandLine1.StartPointFloat = New DevExpress.Utils.PointFloat(413.5417!, 108.0!)
            Me.XrCrossBandLine1.WidthF = 2.0!
            '
            'XrCrossBandLine2
            '
            Me.XrCrossBandLine2.EndBand = Me.ReportFooter
            Me.XrCrossBandLine2.EndPointFloat = New DevExpress.Utils.PointFloat(534.9167!, 30.37504!)
            Me.XrCrossBandLine2.ForeColor = System.Drawing.Color.Teal
            Me.XrCrossBandLine2.LocationFloat = New DevExpress.Utils.PointFloat(534.9167!, 108.0!)
            Me.XrCrossBandLine2.Name = "XrCrossBandLine2"
            Me.XrCrossBandLine2.StartBand = Me.PageHeader
            Me.XrCrossBandLine2.StartPointFloat = New DevExpress.Utils.PointFloat(534.9167!, 108.0!)
            Me.XrCrossBandLine2.WidthF = 2.0!
            '
            'XrCrossBandLine3
            '
            Me.XrCrossBandLine3.EndBand = Me.ReportFooter
            Me.XrCrossBandLine3.EndPointFloat = New DevExpress.Utils.PointFloat(662.5!, 30.37504!)
            Me.XrCrossBandLine3.ForeColor = System.Drawing.Color.Teal
            Me.XrCrossBandLine3.LocationFloat = New DevExpress.Utils.PointFloat(662.5!, 108.0!)
            Me.XrCrossBandLine3.Name = "XrCrossBandLine3"
            Me.XrCrossBandLine3.StartBand = Me.PageHeader
            Me.XrCrossBandLine3.StartPointFloat = New DevExpress.Utils.PointFloat(662.5!, 108.0!)
            Me.XrCrossBandLine3.WidthF = 2.0!
            '
            'XrCrossBandLine4
            '
            Me.XrCrossBandLine4.EndBand = Me.ReportFooter
            Me.XrCrossBandLine4.EndPointFloat = New DevExpress.Utils.PointFloat(787.5!, 30.37504!)
            Me.XrCrossBandLine4.ForeColor = System.Drawing.Color.Teal
            Me.XrCrossBandLine4.LocationFloat = New DevExpress.Utils.PointFloat(787.5!, 108.0!)
            Me.XrCrossBandLine4.Name = "XrCrossBandLine4"
            Me.XrCrossBandLine4.StartBand = Me.PageHeader
            Me.XrCrossBandLine4.StartPointFloat = New DevExpress.Utils.PointFloat(787.5!, 108.0!)
            Me.XrCrossBandLine4.WidthF = 2.0!
            '
            'XrLabel1
            '
            Me.XrLabel1.Font = New System.Drawing.Font("Gill Sans MT", 10.0!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.XrLabel1.ForeColor = System.Drawing.Color.Teal
            Me.XrLabel1.LocationFloat = New DevExpress.Utils.PointFloat(676.0416!, 111.7917!)
            Me.XrLabel1.Name = "XrLabel1"
            Me.XrLabel1.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel1.SizeF = New System.Drawing.SizeF(99.99997!, 14.66666!)
            Me.XrLabel1.StylePriority.UseFont = False
            Me.XrLabel1.StylePriority.UseForeColor = False
            Me.XrLabel1.StylePriority.UseTextAlignment = False
            Me.XrLabel1.Text = "DIFFERENCE"
            Me.XrLabel1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel3
            '
            Me.XrLabel3.Font = New System.Drawing.Font("Gill Sans MT", 10.0!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.XrLabel3.ForeColor = System.Drawing.Color.Teal
            Me.XrLabel3.LocationFloat = New DevExpress.Utils.PointFloat(549.3054!, 111.7917!)
            Me.XrLabel3.Name = "XrLabel3"
            Me.XrLabel3.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel3.SizeF = New System.Drawing.SizeF(99.99997!, 14.66666!)
            Me.XrLabel3.StylePriority.UseFont = False
            Me.XrLabel3.StylePriority.UseForeColor = False
            Me.XrLabel3.StylePriority.UseTextAlignment = False
            Me.XrLabel3.Text = "SUGGESTED"
            Me.XrLabel3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel4
            '
            Me.XrLabel4.Font = New System.Drawing.Font("Gill Sans MT", 10.0!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.XrLabel4.ForeColor = System.Drawing.Color.Teal
            Me.XrLabel4.LocationFloat = New DevExpress.Utils.PointFloat(422.5695!, 111.7917!)
            Me.XrLabel4.Name = "XrLabel4"
            Me.XrLabel4.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel4.SizeF = New System.Drawing.SizeF(100.0!, 14.66666!)
            Me.XrLabel4.StylePriority.UseFont = False
            Me.XrLabel4.StylePriority.UseForeColor = False
            Me.XrLabel4.StylePriority.UseTextAlignment = False
            Me.XrLabel4.Text = "CURRENT"
            Me.XrLabel4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel5
            '
            Me.XrLabel5.Font = New System.Drawing.Font("Gill Sans MT", 10.0!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.XrLabel5.ForeColor = System.Drawing.Color.Teal
            Me.XrLabel5.LocationFloat = New DevExpress.Utils.PointFloat(53.125!, 111.7917!)
            Me.XrLabel5.Name = "XrLabel5"
            Me.XrLabel5.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel5.SizeF = New System.Drawing.SizeF(342.7083!, 14.66666!)
            Me.XrLabel5.StylePriority.UseFont = False
            Me.XrLabel5.StylePriority.UseForeColor = False
            Me.XrLabel5.StylePriority.UseTextAlignment = False
            Me.XrLabel5.Text = "CATEGORY"
            Me.XrLabel5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            '
            'r_client
            '
            Me.r_client.Expression = "Round([client_amount],0)"
            Me.r_client.FieldType = DevExpress.XtraReports.UI.FieldType.[Decimal]
            Me.r_client.Name = "r_client"
            '
            'r_suggested
            '
            Me.r_suggested.Expression = "Round([suggested_amount],0)"
            Me.r_suggested.FieldType = DevExpress.XtraReports.UI.FieldType.[Decimal]
            Me.r_suggested.Name = "r_suggested"
            '
            'r_difference
            '
            Me.r_difference.Expression = "Round([client_amount],0)-Round([suggested_amount],0)"
            Me.r_difference.FieldType = DevExpress.XtraReports.UI.FieldType.[Decimal]
            Me.r_difference.Name = "r_difference"
            '
            'BudgetDetail
            '
            Me.Bands.AddRange(New DevExpress.XtraReports.UI.Band() {Me.ReportFooter, Me.GroupHeader1, Me.TopMarginBand1, Me.Detail, Me.BottomMarginBand1, Me.PageFooter, Me.PageHeader})
            Me.CalculatedFields.AddRange(New DevExpress.XtraReports.UI.CalculatedField() {Me.r_client, Me.r_suggested, Me.r_difference})
            Me.CrossBandControls.AddRange(New DevExpress.XtraReports.UI.XRCrossBandControl() {Me.XrCrossBandLine4, Me.XrCrossBandLine3, Me.XrCrossBandLine2, Me.XrCrossBandLine1})
            Me.ScriptLanguage = DevExpress.XtraReports.ScriptLanguage.VisualBasic
            Me.ScriptReferencesString = resources.GetString("$this.ScriptReferencesString")
            Me.ScriptsSource = Nothing
            Me.StyleSheet.AddRange(New DevExpress.XtraReports.UI.XRControlStyle() {Me.XrControlStyle_PageHeaderBlock})
            Me.TitleString = "Budget Detail"
            Me.Version = "11.2"
            Me.Controls.SetChildIndex(Me.PageHeader, 0)
            Me.Controls.SetChildIndex(Me.PageFooter, 0)
            Me.Controls.SetChildIndex(Me.BottomMarginBand1, 0)
            Me.Controls.SetChildIndex(Me.Detail, 0)
            Me.Controls.SetChildIndex(Me.TopMarginBand1, 0)
            Me.Controls.SetChildIndex(Me.GroupHeader1, 0)
            Me.Controls.SetChildIndex(Me.ReportFooter, 0)
            CType(Me, System.ComponentModel.ISupportInitialize).EndInit()

        End Sub
        Friend WithEvents XrLabel_difference As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_description As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_client_amount As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_suggested_amount As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel2 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel9 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel8 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel7 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents ReportFooter As DevExpress.XtraReports.UI.ReportFooterBand
        Friend WithEvents XrLabel14 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_total_client_amount As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_total_suggested_amount As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_total_difference As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents GroupHeader1 As DevExpress.XtraReports.UI.GroupHeaderBand
        Friend WithEvents XrLabel_heading_description As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrCrossBandLine1 As DevExpress.XtraReports.UI.XRCrossBandLine
        Friend WithEvents XrCrossBandLine2 As DevExpress.XtraReports.UI.XRCrossBandLine
        Friend WithEvents XrCrossBandLine3 As DevExpress.XtraReports.UI.XRCrossBandLine
        Friend WithEvents XrCrossBandLine4 As DevExpress.XtraReports.UI.XRCrossBandLine
        Friend WithEvents XrControlStyle_PageHeaderBlock As DevExpress.XtraReports.UI.XRControlStyle
        Friend WithEvents XrLabel5 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel4 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel3 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel1 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents r_client As DevExpress.XtraReports.UI.CalculatedField
        Friend WithEvents r_suggested As DevExpress.XtraReports.UI.CalculatedField
        Friend WithEvents r_difference As DevExpress.XtraReports.UI.CalculatedField
    End Class
End Namespace
