﻿Imports System.Drawing.Printing
Imports DevExpress.XtraCharts
Imports System.Drawing.Drawing2D

Namespace Client.PacketDMP
    Public Class BudgetOverview
        Inherits CommonReportPortrait

        Public Sub New()
            MyBase.New()
            InitializeComponent()
            AddHandler XrChart_Client_Amount.CustomDrawSeriesPoint, AddressOf XrChart_Client_Amount_CustomDrawSeriesPoint
            AddHandler XrChart_Sugested.CustomDrawSeriesPoint, AddressOf XrChart_Client_Amount_CustomDrawSeriesPoint
            AddHandler XrChart_Client_Amount.BeforePrint, AddressOf XrChart_Client_Amount_BeforePrint
            AddHandler XrChart_Sugested.BeforePrint, AddressOf XrChart_Sugested_BeforePrint
        End Sub

        Private Sub XrChart_Client_Amount_BeforePrint(sender As Object, e As PrintEventArgs)
            Dim Chart As DevExpress.XtraReports.UI.XRChart = CType(sender, DevExpress.XtraReports.UI.XRChart)

            Dim rpt As DevExpress.XtraReports.UI.XtraReport = CType(Chart.Report, DevExpress.XtraReports.UI.XtraReport)
            Dim MasterRpt As DevExpress.XtraReports.UI.XtraReport = CType(rpt.MasterReport, DevExpress.XtraReports.UI.XtraReport)
            Dim ds As DataSet = CType(MasterRpt.DataSource, DataView).Table.DataSet
            Dim tbl As DataTable = ds.Tables("BudgetDetails")

            If tbl IsNot Nothing Then
                Using vue As New DataView(tbl, "[heading]=0 AND [client_amount]>0", "heading_category", DataViewRowState.CurrentRows)

                    ' Find the total of the suggested amount
                    Dim GrandTotal_clientAmount As Decimal = 0D
                    For Each drv As DataRowView In vue
                        GrandTotal_clientAmount += Decimal.Round(DebtPlus.Utils.Nulls.DDec(drv("client_amount")), 0)
                    Next

                    ' Compute the series information
                    Chart.Series(0).Points.Clear()

                    Dim rowID As Int32 = 0
                    Dim Sequence As Int32 = 0
                    Do While rowID < vue.Count
                        Dim categoryID As String = Convert.ToString(vue(rowID)("heading_description"))
                        Dim categoryNumber As Int32 = Convert.ToInt32(vue(rowID)("heading_category"))
                        Dim SubTotal_clientAmount As Decimal = 0D

                        ' Compute the percentages as a percent of the total figure
                        Do While rowID < vue.Count AndAlso Convert.ToInt32(vue(rowID)("heading_category")) = categoryNumber
                            SubTotal_clientAmount += Decimal.Round(DebtPlus.Utils.Nulls.DDec(vue(rowID)("client_amount")), 0)
                            rowID += 1
                        Loop

                        ' Set the client amount chart information
                        Dim pct As Double = Math.Round(Convert.ToDouble(SubTotal_clientAmount) / Convert.ToDouble(GrandTotal_clientAmount), 2)
                        If pct > 0.0# Then
                            Dim pt As New SeriesPoint(ToNormalCase(categoryID), pct)
                            Sequence += 1
                            pt.Tag = Sequence
                            Chart.Series(0).Points.Add(pt)
                        End If
                    Loop
                End Using
            End If
        End Sub

        Private Sub XrChart_Sugested_BeforePrint(sender As Object, e As PrintEventArgs)
            Dim Chart As DevExpress.XtraReports.UI.XRChart = CType(sender, DevExpress.XtraReports.UI.XRChart)
            Dim rpt As DevExpress.XtraReports.UI.XtraReport = CType(Chart.Report, DevExpress.XtraReports.UI.XtraReport)
            Dim MasterRpt As DevExpress.XtraReports.UI.XtraReport = CType(rpt.MasterReport, DevExpress.XtraReports.UI.XtraReport)
            Dim ds As DataSet = CType(MasterRpt.DataSource, DataView).Table.DataSet
            Dim tbl As DataTable = ds.Tables("BudgetDetails")

            If tbl IsNot Nothing Then
                Using vue As New DataView(tbl, "[heading]=0 AND [suggested_amount]>0", "heading_category", DataViewRowState.CurrentRows)

                    ' Find the total of the suggested amount
                    Dim GrandTotal_SuggestedAmount As Decimal = 0D
                    For Each drv As DataRowView In vue
                        GrandTotal_SuggestedAmount += Decimal.Round(DebtPlus.Utils.Nulls.DDec(drv("suggested_amount")), 0)
                    Next

                    ' Compute the series information
                    Chart.Series(0).Points.Clear()

                    Dim rowID As Int32 = 0
                    Dim Sequence As Int32 = 0
                    Do While rowID < vue.Count
                        Dim categoryID As String = Convert.ToString(vue(rowID)("heading_description"))
                        Dim categoryNumber As Int32 = Convert.ToInt32(vue(rowID)("heading_category"))
                        Dim SubTotal_SuggestedAmount As Decimal = 0D

                        ' Compute the percentages as a percent of the total figure
                        Do While rowID < vue.Count AndAlso Convert.ToInt32(vue(rowID)("heading_category")) = categoryNumber
                            SubTotal_SuggestedAmount += Decimal.Round(DebtPlus.Utils.Nulls.DDec(vue(rowID)("suggested_amount")), 0)
                            rowID += 1
                        Loop

                        ' Set the suggested chart information
                        Dim pct As Double = Math.Round(Convert.ToDouble(SubTotal_SuggestedAmount) / Convert.ToDouble(GrandTotal_SuggestedAmount), 2)
                        If pct > 0.0# Then
                            Dim pt As New SeriesPoint(ToNormalCase(categoryID), pct)
                            Sequence += 1
                            pt.Tag = Sequence
                            Chart.Series(0).Points.Add(pt)
                        End If
                    Loop
                End Using
            End If
        End Sub

        Private Sub XrChart_Client_Amount_CustomDrawSeriesPoint(sender As Object, e As CustomDrawSeriesPointEventArgs)
            Dim opts As PieDrawOptions = CType(e.SeriesDrawOptions, PieDrawOptions)
            If opts.FillStyle.FillMode = DevExpress.XtraCharts.FillMode.Hatch Then
                Dim pt As SeriesPoint = e.SeriesPoint
                Dim Sequence As Int32 = Convert.ToInt32(pt.Tag) Mod 5

                Dim hop As HatchFillOptions = CType(opts.FillStyle.Options, HatchFillOptions)
                Select Case Sequence
                    Case 0
                        hop.HatchStyle = HatchStyle.ZigZag

                    Case 1
                        hop.HatchStyle = HatchStyle.WideUpwardDiagonal

                    Case 2
                        hop.HatchStyle = HatchStyle.Cross

                    Case 3
                        hop.HatchStyle = HatchStyle.WideDownwardDiagonal

                    Case Else
                        hop.HatchStyle = HatchStyle.OutlinedDiamond

                End Select
            End If
        End Sub
    End Class
End Namespace