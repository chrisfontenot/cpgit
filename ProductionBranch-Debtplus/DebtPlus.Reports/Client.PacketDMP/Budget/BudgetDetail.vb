﻿Imports System.Drawing.Printing

Namespace Client.PacketDMP
    Public Class BudgetDetail
        Inherits CommonReportPortrait

        Public Sub New()
            MyBase.New()
            InitializeComponent()
            AddHandler XrLabel_heading_description.BeforePrint, AddressOf XrLabel_heading_description_BeforePrint
            AddHandler XrLabel_description.BeforePrint, AddressOf XrLabel_description_BeforePrint
        End Sub

        Private Sub XrLabel_heading_description_BeforePrint(sender As Object, e As PrintEventArgs)
            With CType(sender, DevExpress.XtraReports.UI.XRLabel)
                Dim rpt As DevExpress.XtraReports.UI.XtraReport = CType(.Report, DevExpress.XtraReports.UI.XtraReport)
                Dim fld As String = DebtPlus.Utils.Nulls.DStr(rpt.GetCurrentColumnValue("heading_description"))
                .Text = ToNormalCase(fld)
            End With
        End Sub

        Private Sub XrLabel_description_BeforePrint(sender As Object, e As PrintEventArgs)
            With CType(sender, DevExpress.XtraReports.UI.XRLabel)
                Dim rpt As DevExpress.XtraReports.UI.XtraReport = CType(.Report, DevExpress.XtraReports.UI.XtraReport)
                Dim fld As String = DebtPlus.Utils.Nulls.DStr(rpt.GetCurrentColumnValue("description"))
                .Text = ToNormalCase(fld)
            End With
        End Sub
    End Class
End Namespace