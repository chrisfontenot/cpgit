Namespace Client.PacketDMP

    <Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
    Partial Class BudgetOverview

        'XtraReport overrides dispose to clean up the component list.
        <System.Diagnostics.DebuggerNonUserCode()> _
        Protected Overrides Sub Dispose(ByVal disposing As Boolean)
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
            MyBase.Dispose(disposing)
        End Sub

        'Required by the Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Designer
        'It can be modified using the Designer.  
        'Do not modify it using the code editor.
        <System.Diagnostics.DebuggerStepThrough()> _
        Private Sub InitializeComponent()
            Dim Series1 As DevExpress.XtraCharts.Series = New DevExpress.XtraCharts.Series()
            Dim DoughnutSeriesLabel1 As DevExpress.XtraCharts.DoughnutSeriesLabel = New DevExpress.XtraCharts.DoughnutSeriesLabel()
            Dim PiePointOptions1 As DevExpress.XtraCharts.PiePointOptions = New DevExpress.XtraCharts.PiePointOptions()
            Dim DoughnutSeriesView1 As DevExpress.XtraCharts.DoughnutSeriesView = New DevExpress.XtraCharts.DoughnutSeriesView()
            Dim HatchFillOptions1 As DevExpress.XtraCharts.HatchFillOptions = New DevExpress.XtraCharts.HatchFillOptions()
            Dim DoughnutSeriesLabel2 As DevExpress.XtraCharts.DoughnutSeriesLabel = New DevExpress.XtraCharts.DoughnutSeriesLabel()
            Dim DoughnutSeriesView2 As DevExpress.XtraCharts.DoughnutSeriesView = New DevExpress.XtraCharts.DoughnutSeriesView()
            Dim ChartTitle1 As DevExpress.XtraCharts.ChartTitle = New DevExpress.XtraCharts.ChartTitle()
            Dim ChartTitle2 As DevExpress.XtraCharts.ChartTitle = New DevExpress.XtraCharts.ChartTitle()
            Dim SimpleDiagram1 As DevExpress.XtraCharts.SimpleDiagram = New DevExpress.XtraCharts.SimpleDiagram()
            Dim Series2 As DevExpress.XtraCharts.Series = New DevExpress.XtraCharts.Series()
            Dim DoughnutSeriesLabel3 As DevExpress.XtraCharts.DoughnutSeriesLabel = New DevExpress.XtraCharts.DoughnutSeriesLabel()
            Dim PiePointOptions2 As DevExpress.XtraCharts.PiePointOptions = New DevExpress.XtraCharts.PiePointOptions()
            Dim DoughnutSeriesView3 As DevExpress.XtraCharts.DoughnutSeriesView = New DevExpress.XtraCharts.DoughnutSeriesView()
            Dim HatchFillOptions2 As DevExpress.XtraCharts.HatchFillOptions = New DevExpress.XtraCharts.HatchFillOptions()
            Dim DoughnutSeriesLabel4 As DevExpress.XtraCharts.DoughnutSeriesLabel = New DevExpress.XtraCharts.DoughnutSeriesLabel()
            Dim DoughnutSeriesView4 As DevExpress.XtraCharts.DoughnutSeriesView = New DevExpress.XtraCharts.DoughnutSeriesView()
            Dim ChartTitle3 As DevExpress.XtraCharts.ChartTitle = New DevExpress.XtraCharts.ChartTitle()
            Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(BudgetOverview))
            Me.XrChart_Sugested = New DevExpress.XtraReports.UI.XRChart()
            Me.XrChart_Client_Amount = New DevExpress.XtraReports.UI.XRChart()
            Me.XrControlStyle_PageHeaderBlock = New DevExpress.XtraReports.UI.XRControlStyle()
            CType(Me.XrChart_Sugested, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Series1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(DoughnutSeriesLabel1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(DoughnutSeriesView1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(DoughnutSeriesLabel2, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(DoughnutSeriesView2, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.XrChart_Client_Amount, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(SimpleDiagram1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Series2, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(DoughnutSeriesLabel3, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(DoughnutSeriesView3, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(DoughnutSeriesLabel4, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(DoughnutSeriesView4, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
            '
            'XrLabel_PageHeader
            '
            Me.XrLabel_PageHeader.StyleName = "XrControlStyle_PageHeaderBlock"
            Me.XrLabel_PageHeader.StylePriority.UseBackColor = False
            Me.XrLabel_PageHeader.StylePriority.UseFont = False
            Me.XrLabel_PageHeader.StylePriority.UseForeColor = False
            Me.XrLabel_PageHeader.StylePriority.UsePadding = False
            Me.XrLabel_PageHeader.StylePriority.UseTextAlignment = False
            Me.XrLabel_PageHeader.Text = "Budget Overview"
            '
            'XrLabel_ClientID
            '
            Me.XrLabel_ClientID.StylePriority.UseFont = False
            Me.XrLabel_ClientID.StylePriority.UseForeColor = False
            Me.XrLabel_ClientID.StylePriority.UseTextAlignment = False
            '
            'Detail
            '
            Me.Detail.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrChart_Sugested, Me.XrChart_Client_Amount})
            Me.Detail.HeightF = 812.75!
            Me.Detail.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
            Me.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
            '
            'XrChart_Sugested
            '
            Me.XrChart_Sugested.BackColor = System.Drawing.Color.Transparent
            Me.XrChart_Sugested.BorderColor = System.Drawing.Color.Black
            Me.XrChart_Sugested.Borders = DevExpress.XtraPrinting.BorderSide.None
            Me.XrChart_Sugested.Legend.Visible = False
            Me.XrChart_Sugested.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
            Me.XrChart_Sugested.Name = "XrChart_Sugested"
            Me.XrChart_Sugested.PaletteName = "Metro"
            Me.XrChart_Sugested.Scripts.OnBeforePrint = "XrChart_Sugested_BeforePrint"
            DoughnutSeriesLabel1.Border.Visible = False
            DoughnutSeriesLabel1.Font = New System.Drawing.Font("Gill Sans MT", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            DoughnutSeriesLabel1.LineColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Int32), CType(CType(0, Byte), Int32), CType(CType(0, Byte), Int32))
            DoughnutSeriesLabel1.LineStyle.DashStyle = DevExpress.XtraCharts.DashStyle.Dash
            DoughnutSeriesLabel1.LineVisible = True
            PiePointOptions1.Pattern = "{A} {V}"
            PiePointOptions1.PointView = DevExpress.XtraCharts.PointView.ArgumentAndValues
            PiePointOptions1.ValueNumericOptions.Format = DevExpress.XtraCharts.NumericFormat.Percent
            PiePointOptions1.ValueNumericOptions.Precision = 0
            DoughnutSeriesLabel1.PointOptions = PiePointOptions1
            DoughnutSeriesLabel1.Position = DevExpress.XtraCharts.PieSeriesLabelPosition.TwoColumns
            DoughnutSeriesLabel1.ResolveOverlappingMode = DevExpress.XtraCharts.ResolveOverlappingMode.[Default]
            DoughnutSeriesLabel1.TextColor = System.Drawing.Color.Black
            Series1.Label = DoughnutSeriesLabel1
            Series1.Name = "Series 1"
            DoughnutSeriesView1.Border.Color = System.Drawing.Color.White
            DoughnutSeriesView1.Border.Thickness = 2
            DoughnutSeriesView1.FillStyle.FillMode = DevExpress.XtraCharts.FillMode.Hatch
            HatchFillOptions1.Color2 = System.Drawing.Color.White
            DoughnutSeriesView1.FillStyle.Options = HatchFillOptions1
            DoughnutSeriesView1.HoleRadiusPercent = 45
            DoughnutSeriesView1.Rotation = 10
            DoughnutSeriesView1.RuntimeExploding = False
            Series1.View = DoughnutSeriesView1
            Me.XrChart_Sugested.SeriesSerializable = New DevExpress.XtraCharts.Series() {Series1}
            DoughnutSeriesLabel2.LineVisible = True
            Me.XrChart_Sugested.SeriesTemplate.Label = DoughnutSeriesLabel2
            DoughnutSeriesView2.RuntimeExploding = False
            Me.XrChart_Sugested.SeriesTemplate.View = DoughnutSeriesView2
            Me.XrChart_Sugested.SizeF = New System.Drawing.SizeF(798.0!, 397.0!)
            Me.XrChart_Sugested.StylePriority.UseBackColor = False
            ChartTitle1.Alignment = System.Drawing.StringAlignment.Near
            ChartTitle1.Font = New System.Drawing.Font("Gill Sans MT", 18.0!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Underline), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            ChartTitle1.Indent = 0
            ChartTitle1.Text = "Suggested Budget Allocation"
            ChartTitle1.TextColor = System.Drawing.Color.Teal
            ChartTitle2.Alignment = System.Drawing.StringAlignment.Near
            ChartTitle2.Font = New System.Drawing.Font("Gill Sans MT", 7.75!)
            ChartTitle2.Indent = 0
            ChartTitle2.MaxLineCount = 2
            ChartTitle2.Text = "Based on your initial" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "counseling session."
            ChartTitle2.TextColor = System.Drawing.Color.Goldenrod
            Me.XrChart_Sugested.Titles.AddRange(New DevExpress.XtraCharts.ChartTitle() {ChartTitle1, ChartTitle2})
            '
            'XrChart_Client_Amount
            '
            Me.XrChart_Client_Amount.BackColor = System.Drawing.Color.Transparent
            Me.XrChart_Client_Amount.BorderColor = System.Drawing.Color.Black
            Me.XrChart_Client_Amount.Borders = DevExpress.XtraPrinting.BorderSide.None
            SimpleDiagram1.Margins.Bottom = 2
            SimpleDiagram1.Margins.Left = 2
            SimpleDiagram1.Margins.Right = 2
            SimpleDiagram1.Margins.Top = 2
            Me.XrChart_Client_Amount.Diagram = SimpleDiagram1
            Me.XrChart_Client_Amount.FillStyle.FillMode = DevExpress.XtraCharts.FillMode.Hatch
            Me.XrChart_Client_Amount.Legend.Visible = False
            Me.XrChart_Client_Amount.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 397.0!)
            Me.XrChart_Client_Amount.Name = "XrChart_Client_Amount"
            Me.XrChart_Client_Amount.PaletteName = "Metro"
            Me.XrChart_Client_Amount.Scripts.OnBeforePrint = "XrChart_Client_Amount_BeforePrint"
            DoughnutSeriesLabel3.Border.Visible = False
            DoughnutSeriesLabel3.Font = New System.Drawing.Font("Gill Sans MT", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            DoughnutSeriesLabel3.LineColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Int32), CType(CType(0, Byte), Int32), CType(CType(0, Byte), Int32))
            DoughnutSeriesLabel3.LineStyle.DashStyle = DevExpress.XtraCharts.DashStyle.Dash
            DoughnutSeriesLabel3.LineVisible = True
            PiePointOptions2.Pattern = "{A} {V}"
            PiePointOptions2.PointView = DevExpress.XtraCharts.PointView.ArgumentAndValues
            PiePointOptions2.ValueNumericOptions.Format = DevExpress.XtraCharts.NumericFormat.Percent
            PiePointOptions2.ValueNumericOptions.Precision = 0
            DoughnutSeriesLabel3.PointOptions = PiePointOptions2
            DoughnutSeriesLabel3.Position = DevExpress.XtraCharts.PieSeriesLabelPosition.TwoColumns
            DoughnutSeriesLabel3.ResolveOverlappingMode = DevExpress.XtraCharts.ResolveOverlappingMode.[Default]
            DoughnutSeriesLabel3.TextColor = System.Drawing.Color.Black
            Series2.Label = DoughnutSeriesLabel3
            Series2.Name = "Series 2"
            DoughnutSeriesView3.Border.Color = System.Drawing.Color.White
            DoughnutSeriesView3.Border.Thickness = 2
            DoughnutSeriesView3.FillStyle.FillMode = DevExpress.XtraCharts.FillMode.Hatch
            HatchFillOptions2.Color2 = System.Drawing.Color.White
            DoughnutSeriesView3.FillStyle.Options = HatchFillOptions2
            DoughnutSeriesView3.HoleRadiusPercent = 45
            DoughnutSeriesView3.Rotation = 10
            DoughnutSeriesView3.RuntimeExploding = False
            Series2.View = DoughnutSeriesView3
            Me.XrChart_Client_Amount.SeriesSerializable = New DevExpress.XtraCharts.Series() {Series2}
            DoughnutSeriesLabel4.LineVisible = True
            Me.XrChart_Client_Amount.SeriesTemplate.Label = DoughnutSeriesLabel4
            DoughnutSeriesView4.RuntimeExploding = False
            Me.XrChart_Client_Amount.SeriesTemplate.View = DoughnutSeriesView4
            Me.XrChart_Client_Amount.SizeF = New System.Drawing.SizeF(798.0!, 397.0!)
            Me.XrChart_Client_Amount.StylePriority.UseBackColor = False
            ChartTitle3.Alignment = System.Drawing.StringAlignment.Near
            ChartTitle3.Font = New System.Drawing.Font("Gill Sans MT", 18.0!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Underline), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            ChartTitle3.Indent = 0
            ChartTitle3.Text = "Current Budget Allocation"
            ChartTitle3.TextColor = System.Drawing.Color.Teal
            Me.XrChart_Client_Amount.Titles.AddRange(New DevExpress.XtraCharts.ChartTitle() {ChartTitle3})
            '
            'XrControlStyle_PageHeaderBlock
            '
            Me.XrControlStyle_PageHeaderBlock.Font = New System.Drawing.Font("Gill Sans MT", 24.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.XrControlStyle_PageHeaderBlock.ForeColor = System.Drawing.Color.White
            Me.XrControlStyle_PageHeaderBlock.Name = "XrControlStyle_PageHeaderBlock"
            Me.XrControlStyle_PageHeaderBlock.Padding = New DevExpress.XtraPrinting.PaddingInfo(50, 2, 0, 12, 100.0!)
            Me.XrControlStyle_PageHeaderBlock.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft
            '
            'BudgetOverview
            '
            Me.Bands.AddRange(New DevExpress.XtraReports.UI.Band() {Me.Detail, Me.PageFooter, Me.PageHeader, Me.TopMarginBand1, Me.BottomMarginBand1})
            Me.ScriptLanguage = DevExpress.XtraReports.ScriptLanguage.VisualBasic
            Me.ScriptReferencesString = resources.GetString("$this.ScriptReferencesString")
            Me.StyleSheet.AddRange(New DevExpress.XtraReports.UI.XRControlStyle() {Me.XrControlStyle_PageHeaderBlock})
            Me.TitleString = "Budget Overview"
            Me.Version = "11.2"
            CType(DoughnutSeriesLabel1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(DoughnutSeriesView1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Series1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(DoughnutSeriesLabel2, System.ComponentModel.ISupportInitialize).EndInit()
            CType(DoughnutSeriesView2, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.XrChart_Sugested, System.ComponentModel.ISupportInitialize).EndInit()
            CType(SimpleDiagram1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(DoughnutSeriesLabel3, System.ComponentModel.ISupportInitialize).EndInit()
            CType(DoughnutSeriesView3, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Series2, System.ComponentModel.ISupportInitialize).EndInit()
            CType(DoughnutSeriesLabel4, System.ComponentModel.ISupportInitialize).EndInit()
            CType(DoughnutSeriesView4, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.XrChart_Client_Amount, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me, System.ComponentModel.ISupportInitialize).EndInit()

        End Sub
        Friend WithEvents XrChart_Client_Amount As DevExpress.XtraReports.UI.XRChart
        Friend WithEvents XrChart_Sugested As DevExpress.XtraReports.UI.XRChart
        Friend WithEvents XrControlStyle_PageHeaderBlock As DevExpress.XtraReports.UI.XRControlStyle
    End Class
End Namespace
