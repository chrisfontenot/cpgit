Imports DebtPlus.Interfaces.Client
Imports System.Drawing.Printing
Imports System.IO
Imports System.Text
Imports DebtPlus.Utils

Namespace Client.PacketDMP
    Public Class CommonReportPortrait
        Inherits DebtPlus.Reports.Template.BaseXtraReportClass

        Public Sub New()
            MyBase.New()
            InitializeComponent()
            AddHandler Me.BeforePrint, AddressOf CommonReport_BeforePrint
        End Sub

        Const Language As String = "en-US"

        Public Shared Function FindDocument(ByVal DocumentName As String) As String
            Dim PathName As String = DebtPlus.Configuration.Config.ReportsDirectory()
            If PathName <> String.Empty Then

                ' Try the packets folder in the files first
                Dim TestPath As String = Path.Combine(PathName, String.Format("ClientPackets{0}{1}{0}DMP", Path.DirectorySeparatorChar, Language))
                PathName = Path.Combine(TestPath, DocumentName)
                If System.IO.File.Exists(PathName) Then
                    Return PathName
                End If

                ' Try it without the language specifier
                TestPath = Path.Combine(PathName, String.Format("ClientPackets{0}DMP", Path.DirectorySeparatorChar))
                PathName = Path.Combine(TestPath, DocumentName)
                If System.IO.File.Exists(PathName) Then
                    Return PathName
                End If

                ' Try it without the DMP modifier but a language
                TestPath = Path.Combine(PathName, String.Format("ClientPackets{0}{1}", Path.DirectorySeparatorChar, Language))
                PathName = Path.Combine(TestPath, DocumentName)
                If System.IO.File.Exists(PathName) Then
                    Return PathName
                End If

                ' Try it without any modifiers
                TestPath = Path.Combine(PathName, "ClientPackets")
                PathName = Path.Combine(TestPath, DocumentName)
                If System.IO.File.Exists(PathName) Then
                    Return PathName
                End If
            End If

            ' Give up and return an empty string
            Return String.Empty
        End Function

        Public ReadOnly Property Client As Int32
            Get
                If Not DesignMode Then
                    Dim rpt As IClient = TryCast(MasterReport, IClient)
                    If rpt IsNot Nothing Then
                        Return rpt.ClientId
                    End If
                End If

                Return 0
            End Get
        End Property

        Public Property TitleString As String = String.Empty
        Public Property TitleColor As Color = Color.FromArgb(51, 214, 153)

        Public Shared Function ToNormalCase(ByVal inputString As String) As String
            Dim sb As New StringBuilder(inputString)

            ' Replace the characters with the proper case
            Dim shouldBeUpper As Boolean = True
            For indxChr As Int32 = 0 To sb.Length - 1
                Dim chrValue As Char = sb.Chars(indxChr)
                If Char.IsDigit(chrValue) Then
                    shouldBeUpper = False
                ElseIf Char.IsLetter(chrValue) Then
                    If shouldBeUpper Then
                        If Char.IsLower(chrValue) Then sb.Chars(indxChr) = Char.ToUpperInvariant(chrValue)
                    Else
                        If Not Char.IsLower(chrValue) Then sb.Chars(indxChr) = Char.ToLowerInvariant(chrValue)
                    End If
                    shouldBeUpper = False
                Else
                    shouldBeUpper = True
                End If
            Next

            Return sb.ToString()
        End Function

        Private Sub CommonReport_BeforePrint(sender As Object, e As PrintEventArgs)
            XrLabel_ClientID.Text = String.Format("{0:0000000}", Client)
        End Sub
    End Class
End Namespace
