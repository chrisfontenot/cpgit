Namespace Operations.Disbursements.BySic
    <Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
    Partial Class DisbursementsBySicReport
        Inherits DebtPlus.Reports.Template.DatedTemplateXtraReportClass

        'XtraReport overrides dispose to clean up the component list.
        <System.Diagnostics.DebuggerNonUserCode()> _
        Protected Overrides Sub Dispose(ByVal disposing As Boolean)
            If disposing Then
                ds.Dispose()
                If components IsNot Nothing Then
                    components.Dispose()
                End If
            End If
            MyBase.Dispose(disposing)
        End Sub

        'Required by the Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Designer
        'It can be modified using the Designer.  
        'Do not modify it using the code editor.
        <System.Diagnostics.DebuggerStepThrough()> _
        Private Sub InitializeComponent()
            Dim XrSummary1 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary
            Dim XrSummary2 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary
            Dim XrSummary3 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary
            Dim XrSummary6 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary
            Dim XrSummary5 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary
            Dim XrSummary4 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary
            Me.XrPanel1 = New DevExpress.XtraReports.UI.XRPanel
            Me.XrLabel4 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel7 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel3 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel1 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel2 = New DevExpress.XtraReports.UI.XRLabel
            Me.ReportFooter = New DevExpress.XtraReports.UI.ReportFooterBand
            Me.XrLabel_total_debt = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_total_active_count = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_total_disbursement = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_creditor = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_active_count = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_disbursement = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_creditor_name = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_debt = New DevExpress.XtraReports.UI.XRLabel
            Me.GroupHeader1 = New DevExpress.XtraReports.UI.GroupHeaderBand
            Me.GroupFooter1 = New DevExpress.XtraReports.UI.GroupFooterBand
            Me.XrLabel_group_disbursement = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_group_active_count = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_group_debt = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel9 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel10 = New DevExpress.XtraReports.UI.XRLabel
            CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
            '
            'Detail
            '
            Me.Detail.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel_creditor_name, Me.XrLabel_debt, Me.XrLabel_disbursement, Me.XrLabel_creditor, Me.XrLabel_active_count})
            Me.Detail.HeightF = 15.00002!
            '
            'PageHeader
            '
            Me.PageHeader.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrPanel1})
            Me.PageHeader.HeightF = 141.0!
            Me.PageHeader.Controls.SetChildIndex(Me.XrPanel1, 0)
            Me.PageHeader.Controls.SetChildIndex(Me.XrPanel_AgencyAddress, 0)
            Me.PageHeader.Controls.SetChildIndex(Me.XrLabel_Title, 0)
            Me.PageHeader.Controls.SetChildIndex(Me.XrLine_Title, 0)
            Me.PageHeader.Controls.SetChildIndex(Me.XrLabel_Subtitle, 0)
            Me.PageHeader.Controls.SetChildIndex(Me.XrPageInfo1, 0)
            '
            'XrLabel_Title
            '
            Me.XrLabel_Title.StylePriority.UseFont = False
            '
            'XrPageInfo_PageNumber
            '
            Me.XrPageInfo_PageNumber.StylePriority.UseTextAlignment = False
            '
            'XRLabel_Agency_Name
            '
            Me.XRLabel_Agency_Name.StylePriority.UseTextAlignment = False
            '
            'XrLabel_Agency_Address3
            '
            Me.XrLabel_Agency_Address3.StylePriority.UseFont = False
            Me.XrLabel_Agency_Address3.StylePriority.UseTextAlignment = False
            '
            'XrLabel_Agency_Address1
            '
            Me.XrLabel_Agency_Address1.StylePriority.UseFont = False
            Me.XrLabel_Agency_Address1.StylePriority.UseTextAlignment = False
            '
            'XrLabel_Agency_Phone
            '
            Me.XrLabel_Agency_Phone.StylePriority.UseFont = False
            Me.XrLabel_Agency_Phone.StylePriority.UseTextAlignment = False
            '
            'XrLabel_Agency_Address2
            '
            Me.XrLabel_Agency_Address2.StylePriority.UseFont = False
            Me.XrLabel_Agency_Address2.StylePriority.UseTextAlignment = False
            '
            'XrPanel1
            '
            Me.XrPanel1.BackColor = System.Drawing.Color.Teal
            Me.XrPanel1.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel4, Me.XrLabel7, Me.XrLabel3, Me.XrLabel1, Me.XrLabel2})
            Me.XrPanel1.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 117.0!)
            Me.XrPanel1.Name = "XrPanel1"
            Me.XrPanel1.SizeF = New System.Drawing.SizeF(800.0!, 17.0!)
            Me.XrPanel1.StyleName = "XrControlStyle_HeaderPannel"
            '
            'XrLabel4
            '
            Me.XrLabel4.LocationFloat = New DevExpress.Utils.PointFloat(108.0!, 1.0!)
            Me.XrLabel4.Name = "XrLabel4"
            Me.XrLabel4.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
            Me.XrLabel4.SizeF = New System.Drawing.SizeF(283.7083!, 14.99999!)
            Me.XrLabel4.StylePriority.UsePadding = False
            Me.XrLabel4.StylePriority.UseTextAlignment = False
            Me.XrLabel4.Text = "CREDITOR ID AND NAME"
            Me.XrLabel4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            Me.XrLabel4.WordWrap = False
            '
            'XrLabel7
            '
            Me.XrLabel7.LocationFloat = New DevExpress.Utils.PointFloat(391.7083!, 0.9999593!)
            Me.XrLabel7.Name = "XrLabel7"
            Me.XrLabel7.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
            Me.XrLabel7.SizeF = New System.Drawing.SizeF(128.9167!, 15.00003!)
            Me.XrLabel7.StylePriority.UsePadding = False
            Me.XrLabel7.StylePriority.UseTextAlignment = False
            Me.XrLabel7.Text = "DISBURSEMENT"
            Me.XrLabel7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrLabel7.WordWrap = False
            '
            'XrLabel3
            '
            Me.XrLabel3.LocationFloat = New DevExpress.Utils.PointFloat(689.9999!, 0.9999911!)
            Me.XrLabel3.Name = "XrLabel3"
            Me.XrLabel3.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
            Me.XrLabel3.SizeF = New System.Drawing.SizeF(100.0!, 15.0!)
            Me.XrLabel3.StylePriority.UsePadding = False
            Me.XrLabel3.StylePriority.UseTextAlignment = False
            Me.XrLabel3.Text = "TOTAL DEBT"
            Me.XrLabel3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrLabel3.WordWrap = False
            '
            'XrLabel1
            '
            Me.XrLabel1.LocationFloat = New DevExpress.Utils.PointFloat(8.0!, 1.0!)
            Me.XrLabel1.Name = "XrLabel1"
            Me.XrLabel1.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
            Me.XrLabel1.SizeF = New System.Drawing.SizeF(75.0!, 15.0!)
            Me.XrLabel1.StylePriority.UsePadding = False
            Me.XrLabel1.StylePriority.UseTextAlignment = False
            Me.XrLabel1.Text = "S.I.C."
            Me.XrLabel1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            Me.XrLabel1.WordWrap = False
            '
            'XrLabel2
            '
            Me.XrLabel2.LocationFloat = New DevExpress.Utils.PointFloat(520.625!, 0.9999911!)
            Me.XrLabel2.Name = "XrLabel2"
            Me.XrLabel2.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
            Me.XrLabel2.SizeF = New System.Drawing.SizeF(111.0!, 15.0!)
            Me.XrLabel2.StylePriority.UsePadding = False
            Me.XrLabel2.StylePriority.UseTextAlignment = False
            Me.XrLabel2.Text = "ACTIVE COUNT"
            Me.XrLabel2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrLabel2.WordWrap = False
            '
            'ReportFooter
            '
            Me.ReportFooter.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel9, Me.XrLabel_total_debt, Me.XrLabel_total_active_count, Me.XrLabel_total_disbursement})
            Me.ReportFooter.HeightF = 35.0!
            Me.ReportFooter.Name = "ReportFooter"
            Me.ReportFooter.StyleName = "XrControlStyle_Totals"
            '
            'XrLabel_total_debt
            '
            Me.XrLabel_total_debt.LocationFloat = New DevExpress.Utils.PointFloat(631.6252!, 10.00001!)
            Me.XrLabel_total_debt.Name = "XrLabel_total_debt"
            Me.XrLabel_total_debt.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
            Me.XrLabel_total_debt.SizeF = New System.Drawing.SizeF(158.3748!, 14.99999!)
            Me.XrLabel_total_debt.StylePriority.UsePadding = False
            Me.XrLabel_total_debt.StylePriority.UseTextAlignment = False
            XrSummary1.FormatString = "{0:c}"
            XrSummary1.Running = DevExpress.XtraReports.UI.SummaryRunning.Report
            Me.XrLabel_total_debt.Summary = XrSummary1
            Me.XrLabel_total_debt.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrLabel_total_debt.WordWrap = False
            '
            'XrLabel_total_active_count
            '
            Me.XrLabel_total_active_count.LocationFloat = New DevExpress.Utils.PointFloat(520.6251!, 10.00001!)
            Me.XrLabel_total_active_count.Name = "XrLabel_total_active_count"
            Me.XrLabel_total_active_count.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
            Me.XrLabel_total_active_count.SizeF = New System.Drawing.SizeF(111.0!, 15.0!)
            Me.XrLabel_total_active_count.StylePriority.UsePadding = False
            Me.XrLabel_total_active_count.StylePriority.UseTextAlignment = False
            XrSummary2.FormatString = "{0:n0}"
            XrSummary2.Running = DevExpress.XtraReports.UI.SummaryRunning.Report
            Me.XrLabel_total_active_count.Summary = XrSummary2
            Me.XrLabel_total_active_count.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrLabel_total_active_count.WordWrap = False
            '
            'XrLabel_total_disbursement
            '
            Me.XrLabel_total_disbursement.LocationFloat = New DevExpress.Utils.PointFloat(356.7084!, 10.00001!)
            Me.XrLabel_total_disbursement.Name = "XrLabel_total_disbursement"
            Me.XrLabel_total_disbursement.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
            Me.XrLabel_total_disbursement.SizeF = New System.Drawing.SizeF(163.9167!, 15.00002!)
            Me.XrLabel_total_disbursement.StylePriority.UsePadding = False
            Me.XrLabel_total_disbursement.StylePriority.UseTextAlignment = False
            XrSummary3.FormatString = "{0:c}"
            XrSummary3.Running = DevExpress.XtraReports.UI.SummaryRunning.Report
            Me.XrLabel_total_disbursement.Summary = XrSummary3
            Me.XrLabel_total_disbursement.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrLabel_total_disbursement.WordWrap = False
            '
            'XrLabel_creditor
            '
            Me.XrLabel_creditor.LocationFloat = New DevExpress.Utils.PointFloat(8.000009!, 0.0!)
            Me.XrLabel_creditor.Name = "XrLabel_creditor"
            Me.XrLabel_creditor.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
            Me.XrLabel_creditor.SizeF = New System.Drawing.SizeF(75.0!, 15.0!)
            Me.XrLabel_creditor.StylePriority.UsePadding = False
            Me.XrLabel_creditor.StylePriority.UseTextAlignment = False
            Me.XrLabel_creditor.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            Me.XrLabel_creditor.WordWrap = False
            '
            'XrLabel_active_count
            '
            Me.XrLabel_active_count.LocationFloat = New DevExpress.Utils.PointFloat(536.25!, 0.0!)
            Me.XrLabel_active_count.Name = "XrLabel_active_count"
            Me.XrLabel_active_count.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
            Me.XrLabel_active_count.SizeF = New System.Drawing.SizeF(95.375!, 15.0!)
            Me.XrLabel_active_count.StylePriority.UsePadding = False
            Me.XrLabel_active_count.StylePriority.UseTextAlignment = False
            Me.XrLabel_active_count.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrLabel_active_count.WordWrap = False
            '
            'XrLabel_disbursement
            '
            Me.XrLabel_disbursement.LocationFloat = New DevExpress.Utils.PointFloat(391.7083!, 0.0!)
            Me.XrLabel_disbursement.Name = "XrLabel_disbursement"
            Me.XrLabel_disbursement.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
            Me.XrLabel_disbursement.SizeF = New System.Drawing.SizeF(128.9167!, 15.00001!)
            Me.XrLabel_disbursement.StylePriority.UsePadding = False
            Me.XrLabel_disbursement.StylePriority.UseTextAlignment = False
            Me.XrLabel_disbursement.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrLabel_disbursement.WordWrap = False
            '
            'XrLabel_creditor_name
            '
            Me.XrLabel_creditor_name.LocationFloat = New DevExpress.Utils.PointFloat(108.0!, 0.00003178914!)
            Me.XrLabel_creditor_name.Name = "XrLabel_creditor_name"
            Me.XrLabel_creditor_name.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
            Me.XrLabel_creditor_name.SizeF = New System.Drawing.SizeF(283.7083!, 14.99999!)
            Me.XrLabel_creditor_name.StylePriority.UsePadding = False
            Me.XrLabel_creditor_name.StylePriority.UseTextAlignment = False
            Me.XrLabel_creditor_name.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            Me.XrLabel_creditor_name.WordWrap = False
            '
            'XrLabel_debt
            '
            Me.XrLabel_debt.LocationFloat = New DevExpress.Utils.PointFloat(631.625!, 0.0!)
            Me.XrLabel_debt.Name = "XrLabel_debt"
            Me.XrLabel_debt.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
            Me.XrLabel_debt.SizeF = New System.Drawing.SizeF(158.3749!, 14.99999!)
            Me.XrLabel_debt.StylePriority.UsePadding = False
            Me.XrLabel_debt.StylePriority.UseTextAlignment = False
            Me.XrLabel_debt.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrLabel_debt.WordWrap = False
            '
            'GroupHeader1
            '
            Me.GroupHeader1.HeightF = 0.0!
            Me.GroupHeader1.Name = "GroupHeader1"
            '
            'GroupFooter1
            '
            Me.GroupFooter1.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel10, Me.XrLabel_group_debt, Me.XrLabel_group_active_count, Me.XrLabel_group_disbursement})
            Me.GroupFooter1.GroupUnion = DevExpress.XtraReports.UI.GroupFooterUnion.WithLastDetail
            Me.GroupFooter1.HeightF = 40.0!
            Me.GroupFooter1.Name = "GroupFooter1"
            Me.GroupFooter1.StyleName = "XrControlStyle_Totals"
            '
            'XrLabel_group_disbursement
            '
            Me.XrLabel_group_disbursement.LocationFloat = New DevExpress.Utils.PointFloat(356.7083!, 9.999974!)
            Me.XrLabel_group_disbursement.Name = "XrLabel_group_disbursement"
            Me.XrLabel_group_disbursement.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
            Me.XrLabel_group_disbursement.SizeF = New System.Drawing.SizeF(163.9167!, 15.00002!)
            Me.XrLabel_group_disbursement.StylePriority.UsePadding = False
            Me.XrLabel_group_disbursement.StylePriority.UseTextAlignment = False
            XrSummary6.FormatString = "{0:c}"
            XrSummary6.Running = DevExpress.XtraReports.UI.SummaryRunning.Group
            Me.XrLabel_group_disbursement.Summary = XrSummary6
            Me.XrLabel_group_disbursement.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrLabel_group_disbursement.WordWrap = False
            '
            'XrLabel_group_active_count
            '
            Me.XrLabel_group_active_count.LocationFloat = New DevExpress.Utils.PointFloat(520.625!, 10.00001!)
            Me.XrLabel_group_active_count.Name = "XrLabel_group_active_count"
            Me.XrLabel_group_active_count.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
            Me.XrLabel_group_active_count.SizeF = New System.Drawing.SizeF(111.0!, 15.0!)
            Me.XrLabel_group_active_count.StylePriority.UsePadding = False
            Me.XrLabel_group_active_count.StylePriority.UseTextAlignment = False
            XrSummary5.FormatString = "{0:n0}"
            XrSummary5.Running = DevExpress.XtraReports.UI.SummaryRunning.Group
            Me.XrLabel_group_active_count.Summary = XrSummary5
            Me.XrLabel_group_active_count.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrLabel_group_active_count.WordWrap = False
            '
            'XrLabel_group_debt
            '
            Me.XrLabel_group_debt.LocationFloat = New DevExpress.Utils.PointFloat(631.6252!, 10.00001!)
            Me.XrLabel_group_debt.Name = "XrLabel_group_debt"
            Me.XrLabel_group_debt.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
            Me.XrLabel_group_debt.SizeF = New System.Drawing.SizeF(158.3748!, 14.99999!)
            Me.XrLabel_group_debt.StylePriority.UsePadding = False
            Me.XrLabel_group_debt.StylePriority.UseTextAlignment = False
            XrSummary4.FormatString = "{0:c}"
            XrSummary4.Running = DevExpress.XtraReports.UI.SummaryRunning.Group
            Me.XrLabel_group_debt.Summary = XrSummary4
            Me.XrLabel_group_debt.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrLabel_group_debt.WordWrap = False
            '
            'XrLabel9
            '
            Me.XrLabel9.LocationFloat = New DevExpress.Utils.PointFloat(7.999992!, 10.00001!)
            Me.XrLabel9.Name = "XrLabel9"
            Me.XrLabel9.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96.0!)
            Me.XrLabel9.SizeF = New System.Drawing.SizeF(100.0!, 15.0!)
            Me.XrLabel9.StylePriority.UseTextAlignment = False
            Me.XrLabel9.Text = "TOTALS"
            Me.XrLabel9.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
            '
            'XrLabel10
            '
            Me.XrLabel10.LocationFloat = New DevExpress.Utils.PointFloat(7.999992!, 10.00001!)
            Me.XrLabel10.Name = "XrLabel10"
            Me.XrLabel10.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel10.SizeF = New System.Drawing.SizeF(146.875!, 15.0!)
            Me.XrLabel10.StylePriority.UseTextAlignment = False
            Me.XrLabel10.Text = "SUB-TOTALS"
            Me.XrLabel10.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
            '
            'DisbursementsBySicReport
            '
            Me.Bands.AddRange(New DevExpress.XtraReports.UI.Band() {Me.Detail, Me.PageHeader, Me.PageFooter, Me.ReportFooter, Me.GroupHeader1, Me.GroupFooter1})
            Me.Version = "10.1"
            Me.Controls.SetChildIndex(Me.GroupFooter1, 0)
            Me.Controls.SetChildIndex(Me.GroupHeader1, 0)
            Me.Controls.SetChildIndex(Me.ReportFooter, 0)
            Me.Controls.SetChildIndex(Me.PageFooter, 0)
            Me.Controls.SetChildIndex(Me.PageHeader, 0)
            Me.Controls.SetChildIndex(Me.Detail, 0)
            CType(Me, System.ComponentModel.ISupportInitialize).EndInit()

        End Sub
        Friend WithEvents XrPanel1 As DevExpress.XtraReports.UI.XRPanel
        Friend WithEvents XrLabel3 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel1 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel7 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel2 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel4 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents ReportFooter As DevExpress.XtraReports.UI.ReportFooterBand
        Friend WithEvents XrLabel_creditor_name As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_debt As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_disbursement As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_creditor As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_active_count As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_total_debt As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_total_active_count As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_total_disbursement As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel9 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents GroupHeader1 As DevExpress.XtraReports.UI.GroupHeaderBand
        Friend WithEvents GroupFooter1 As DevExpress.XtraReports.UI.GroupFooterBand
        Friend WithEvents XrLabel10 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_group_debt As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_group_active_count As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_group_disbursement As DevExpress.XtraReports.UI.XRLabel
    End Class
End Namespace