#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}

#End Region

Imports System.ComponentModel

Imports System.Data.SqlClient
Imports DevExpress.XtraReports.UI

Namespace Operations.Disbursements.BySic
    Public Class DisbursementsBySicReport

        Public Sub New()
            MyBase.New()
            InitializeComponent()
            AddHandler Me.BeforePrint, AddressOf Report_BeforePrint
        End Sub

        ''' <summary>
        ''' Return the report title information
        ''' </summary>
        Public Overrides ReadOnly Property ReportTitle() As String
            Get
                Return "Disbursement By SIC Report"
            End Get
        End Property


        ''' <summary>
        ''' Refresh the dataset with the new information
        ''' </summary>
        Protected ds As New DataSet("ds")

        Private Sub Report_BeforePrint(ByVal sender As Object, ByVal e As EventArgs)
            Dim tbl As DataTable = ds.Tables("rpt_disbursements_by_sic")

            If tbl Is Nothing Then
                Dim cn As SqlConnection = New System.Data.SqlClient.SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
                Try
                    cn.Open()
                    Using cmd As SqlCommand = New System.Data.SqlClient.SqlCommand()
                        With cmd
                            .Connection = cn
                            .CommandText = "rpt_disbursements_by_sic"
                            .CommandType = CommandType.StoredProcedure

                            SqlCommandBuilder.DeriveParameters(cmd)
                            .Parameters(1).Value = Parameter_FromDate
                            .Parameters(2).Value = Parameter_ToDate
                            .CommandTimeout = 0
                        End With

                        Using da As New SqlDataAdapter(cmd)
                            da.Fill(ds, "rpt_disbursements_by_sic")
                            tbl = ds.Tables("rpt_disbursements_by_sic")
                        End Using
                    End Using

                Catch ex As Exception
                    DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Database error")

                Finally
                    If cn IsNot Nothing Then cn.Dispose()
                End Try
            End If

            If tbl IsNot Nothing Then
                Dim vue As DataView = New DataView(tbl, String.Empty, "creditor, creditor_name", DataViewRowState.CurrentRows)
                DataSource = vue

                GroupHeader1.GroupFields.Add(New GroupField("creditor", XRColumnSortOrder.Ascending))

                With XrLabel_creditor
                    .DataBindings.Add("Text", vue, "creditor")
                End With

                With XrLabel_creditor_name
                    .DataBindings.Add("Text", vue, "creditor_name")
                End With

                With XrLabel_disbursement
                    .DataBindings.Add("Text", vue, "disbursement", "{0:c}")
                End With

                With XrLabel_total_disbursement
                    .DataBindings.Add("Text", vue, "disbursement")
                End With

                With XrLabel_group_disbursement
                    .DataBindings.Add("Text", vue, "disbursement")
                End With

                With XrLabel_active_count
                    .DataBindings.Add("Text", vue, "active_count", "{0:n0}")
                End With

                With XrLabel_total_active_count
                    .DataBindings.Add("Text", vue, "active_count")
                End With

                With XrLabel_group_active_count
                    .DataBindings.Add("Text", vue, "active_count")
                End With

                With XrLabel_debt
                    .DataBindings.Add("Text", vue, "debt", "{0:c}")
                End With

                With XrLabel_total_debt
                    .DataBindings.Add("Text", vue, "debt")
                End With

                With XrLabel_group_debt
                    .DataBindings.Add("Text", vue, "debt")
                End With
            End If
        End Sub
    End Class
End Namespace
