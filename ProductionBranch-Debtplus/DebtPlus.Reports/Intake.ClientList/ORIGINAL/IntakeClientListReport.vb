#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}
#End Region

Option Compare Binary
Option Explicit On 
Option Strict On

Namespace Intake.ClientList
    Public Class IntakeClientListReport

        ''' <summary>
        ''' Create an instance of our report
        ''' </summary>
        Public Sub New()
            MyBase.New()
            InitializeComponent()
            AddHandler BeforePrint, AddressOf IntakeClientListReport_BeforePrint
        End Sub

        ''' <summary>
        ''' Report Title
        ''' </summary>
        Public Overrides ReadOnly Property ReportTitle() As String
            Get
                Return "Intake Clients"
            End Get
        End Property

        ''' <summary>
        ''' Load the report on the first instance
        ''' </summary>
        Dim ds As New System.Data.DataSet("ds")

        Private Sub IntakeClientListReport_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs)
            Const TableName As String = "view_intake_client_list"
            Dim rpt As DevExpress.XtraReports.UI.XtraReport = CType(sender, DevExpress.XtraReports.UI.XtraReport)
            Dim sqlInfo As DebtPlus.LINQ.SQLInfoClass = DebtPlus.LINQ.SQLInfoClass.getDefault()

            Using cmd As New System.Data.SqlClient.SqlCommand()
                With cmd
                    .Connection = New System.Data.SqlClient.SqlConnection(sqlInfo.ConnectionString)
                    .CommandText = "SELECT * FROM view_intake_clients WHERE date_created >= @FromDate AND date_created < @ToDate ORDER BY [date_created]"
                    .CommandType = System.Data.CommandType.Text
                    .Parameters.Add("@FromDate", System.Data.SqlDbType.DateTime).Value = CType(rpt.Parameters("ParameterFromDate").Value, System.DateTime).Date
                    .Parameters.Add("@ToDate", System.Data.SqlDbType.DateTime).Value = CType(rpt.Parameters("ParameterToDate").Value, System.DateTime).AddDays(1).Date
                    .CommandTimeout = 0
                End With
                Using da As New System.Data.SqlClient.SqlDataAdapter(cmd)
                    da.Fill(ds, TableName)
                End Using
            End Using

            rpt.DataSource = ds.Tables(TableName).DefaultView
        End Sub
    End Class
End Namespace
