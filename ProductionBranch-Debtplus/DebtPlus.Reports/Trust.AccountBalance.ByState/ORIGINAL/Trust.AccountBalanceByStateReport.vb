#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}
#End Region

Option Compare Binary
Option Explicit On 
Option Strict On

Namespace Trust.AccountBalance.ByState
    Public Class TrustAccountBalanceByStateReport
        Inherits DebtPlus.Reports.Template.TemplateXtraReportClass

        Public Overrides ReadOnly Property ReportTitle() As String
            Get
                Return "Trust Balances by State"
            End Get
        End Property

        ''' <summary>
        ''' Create an instance of our report
        ''' </summary>
        Public Sub New()
            MyBase.New()
            InitializeComponent()

            AddHandler BeforePrint, AddressOf Report_BeforePrint
            AddHandler GroupFooter1.BeforePrint, AddressOf GroupFooter1_BeforePrint
            AddHandler XrLabel_state.PreviewClick, AddressOf Field_PreviewClick
            AddHandler XrLabel_balance.PreviewClick, AddressOf Field_PreviewClick
            AddHandler XrLabel_reserved.PreviewClick, AddressOf Field_PreviewClick

            '-- Enable the select expert
            ReportFilter.IsEnabled = True
        End Sub

#Region " Component Designer generated code "

        'Component overrides dispose to clean up the component list.
        Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
            If disposing Then
                If components IsNot Nothing Then
                    components.Dispose()
                End If
            End If
            MyBase.Dispose(disposing)
        End Sub

        'Required by the Designer
        Private components As System.ComponentModel.IContainer
        Friend WithEvents XrLabel_state As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_balance As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_reserved As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrPanel1 As DevExpress.XtraReports.UI.XRPanel
        Friend WithEvents XrLabel4 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel3 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel1 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_subtotal_text As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_subtotal_balance As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_subtotal_reserved As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLine1 As DevExpress.XtraReports.UI.XRLine
        Friend WithEvents XrLabel_total_reserved As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_total_balance As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_total_text As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents GroupHeader1 As DevExpress.XtraReports.UI.GroupHeaderBand
        Friend WithEvents GroupFooter1 As DevExpress.XtraReports.UI.GroupFooterBand

        'NOTE: The following procedure is required by the Designer
        'It can be modified using the Designer.
        'Do not modify it using the code editor.
        Friend WithEvents ReportFooter As DevExpress.XtraReports.UI.ReportFooterBand
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
            Dim XrSummary3 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary()
            Dim XrSummary4 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary()
            Dim XrSummary5 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary()
            Dim XrSummary6 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary()
            Dim XrSummary2 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary()
            Dim XrSummary1 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary()
            Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(TrustAccountBalanceByStateReport))
            Me.ReportFooter = New DevExpress.XtraReports.UI.ReportFooterBand()
            Me.XrLine1 = New DevExpress.XtraReports.UI.XRLine()
            Me.XrLabel_total_reserved = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_total_balance = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_total_text = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_subtotal_text = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_subtotal_balance = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_subtotal_reserved = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrPanel1 = New DevExpress.XtraReports.UI.XRPanel()
            Me.XrLabel4 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel3 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel1 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_reserved = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_balance = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_state = New DevExpress.XtraReports.UI.XRLabel()
            Me.GroupHeader1 = New DevExpress.XtraReports.UI.GroupHeaderBand()
            Me.GroupFooter1 = New DevExpress.XtraReports.UI.GroupFooterBand()
            CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
            '
            'PageHeader
            '
            Me.PageHeader.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrPanel1})
            Me.PageHeader.HeightF = 170.0!
            Me.PageHeader.Controls.SetChildIndex(Me.XrPanel_AgencyAddress, 0)
            Me.PageHeader.Controls.SetChildIndex(Me.XrLabel_Title, 0)
            Me.PageHeader.Controls.SetChildIndex(Me.XrLine_Title, 0)
            Me.PageHeader.Controls.SetChildIndex(Me.XrLabel_Subtitle, 0)
            Me.PageHeader.Controls.SetChildIndex(Me.XrPageInfo1, 0)
            Me.PageHeader.Controls.SetChildIndex(Me.XrPanel1, 0)
            '
            'XrLabel_Title
            '
            Me.XrLabel_Title.StylePriority.UseFont = False
            '
            'XrPageInfo_PageNumber
            '
            Me.XrPageInfo_PageNumber.StylePriority.UseTextAlignment = False
            '
            'XRLabel_Agency_Name
            '
            Me.XRLabel_Agency_Name.StylePriority.UseTextAlignment = False
            '
            'XrLabel_Agency_Address3
            '
            Me.XrLabel_Agency_Address3.StylePriority.UseFont = False
            Me.XrLabel_Agency_Address3.StylePriority.UseTextAlignment = False
            '
            'XrLabel_Agency_Address1
            '
            Me.XrLabel_Agency_Address1.StylePriority.UseFont = False
            Me.XrLabel_Agency_Address1.StylePriority.UseTextAlignment = False
            '
            'XrLabel_Agency_Phone
            '
            Me.XrLabel_Agency_Phone.StylePriority.UseFont = False
            Me.XrLabel_Agency_Phone.StylePriority.UseTextAlignment = False
            '
            'XrLabel_Agency_Address2
            '
            Me.XrLabel_Agency_Address2.StylePriority.UseFont = False
            Me.XrLabel_Agency_Address2.StylePriority.UseTextAlignment = False
            '
            'Detail
            '
            Me.Detail.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel_state, Me.XrLabel_balance, Me.XrLabel_reserved})
            Me.Detail.HeightF = 15.0!
            '
            'ReportFooter
            '
            Me.ReportFooter.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLine1, Me.XrLabel_total_reserved, Me.XrLabel_total_balance, Me.XrLabel_total_text})
            Me.ReportFooter.HeightF = 25.00001!
            Me.ReportFooter.Name = "ReportFooter"
            '
            'XrLine1
            '
            Me.XrLine1.LocationFloat = New DevExpress.Utils.PointFloat(35.00001!, 1.666672!)
            Me.XrLine1.Name = "XrLine1"
            Me.XrLine1.SizeF = New System.Drawing.SizeF(464.5833!, 8.333328!)
            '
            'XrLabel_total_reserved
            '
            Me.XrLabel_total_reserved.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "reserved")})
            Me.XrLabel_total_reserved.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel_total_reserved.LocationFloat = New DevExpress.Utils.PointFloat(375.0!, 10.00001!)
            Me.XrLabel_total_reserved.Name = "XrLabel_total_reserved"
            Me.XrLabel_total_reserved.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_total_reserved.SizeF = New System.Drawing.SizeF(125.0!, 15.0!)
            Me.XrLabel_total_reserved.StylePriority.UseFont = False
            Me.XrLabel_total_reserved.StylePriority.UseTextAlignment = False
            XrSummary3.FormatString = "{0:c}"
            XrSummary3.Running = DevExpress.XtraReports.UI.SummaryRunning.Report
            Me.XrLabel_total_reserved.Summary = XrSummary3
            Me.XrLabel_total_reserved.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
            '
            'XrLabel_total_balance
            '
            Me.XrLabel_total_balance.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "balance")})
            Me.XrLabel_total_balance.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel_total_balance.LocationFloat = New DevExpress.Utils.PointFloat(224.5001!, 10.00001!)
            Me.XrLabel_total_balance.Name = "XrLabel_total_balance"
            Me.XrLabel_total_balance.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_total_balance.SizeF = New System.Drawing.SizeF(125.8333!, 14.99999!)
            Me.XrLabel_total_balance.StylePriority.UseFont = False
            Me.XrLabel_total_balance.StylePriority.UseTextAlignment = False
            XrSummary4.FormatString = "{0:c}"
            XrSummary4.Running = DevExpress.XtraReports.UI.SummaryRunning.Report
            Me.XrLabel_total_balance.Summary = XrSummary4
            Me.XrLabel_total_balance.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
            '
            'XrLabel_total_text
            '
            Me.XrLabel_total_text.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel_total_text.LocationFloat = New DevExpress.Utils.PointFloat(35.0!, 10.0!)
            Me.XrLabel_total_text.Name = "XrLabel_total_text"
            Me.XrLabel_total_text.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_total_text.SizeF = New System.Drawing.SizeF(91.0!, 15.0!)
            Me.XrLabel_total_text.StylePriority.UseFont = False
            Me.XrLabel_total_text.StylePriority.UseTextAlignment = False
            Me.XrLabel_total_text.Text = "TOTALS"
            Me.XrLabel_total_text.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
            '
            'XrLabel_subtotal_text
            '
            Me.XrLabel_subtotal_text.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel_subtotal_text.LocationFloat = New DevExpress.Utils.PointFloat(35.00001!, 10.00001!)
            Me.XrLabel_subtotal_text.Name = "XrLabel_subtotal_text"
            Me.XrLabel_subtotal_text.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_subtotal_text.SizeF = New System.Drawing.SizeF(91.00002!, 15.0!)
            Me.XrLabel_subtotal_text.StylePriority.UseFont = False
            Me.XrLabel_subtotal_text.StylePriority.UseTextAlignment = False
            Me.XrLabel_subtotal_text.Text = "SUB-TOTAL"
            Me.XrLabel_subtotal_text.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
            '
            'XrLabel_subtotal_balance
            '
            Me.XrLabel_subtotal_balance.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "balance")})
            Me.XrLabel_subtotal_balance.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel_subtotal_balance.LocationFloat = New DevExpress.Utils.PointFloat(224.5001!, 10.00001!)
            Me.XrLabel_subtotal_balance.Name = "XrLabel_subtotal_balance"
            Me.XrLabel_subtotal_balance.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_subtotal_balance.SizeF = New System.Drawing.SizeF(125.8333!, 15.0!)
            Me.XrLabel_subtotal_balance.StylePriority.UseFont = False
            Me.XrLabel_subtotal_balance.StylePriority.UseTextAlignment = False
            XrSummary5.FormatString = "{0:c}"
            XrSummary5.Running = DevExpress.XtraReports.UI.SummaryRunning.Group
            Me.XrLabel_subtotal_balance.Summary = XrSummary5
            Me.XrLabel_subtotal_balance.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
            '
            'XrLabel_subtotal_reserved
            '
            Me.XrLabel_subtotal_reserved.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "reserved")})
            Me.XrLabel_subtotal_reserved.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel_subtotal_reserved.LocationFloat = New DevExpress.Utils.PointFloat(375.0!, 10.00001!)
            Me.XrLabel_subtotal_reserved.Name = "XrLabel_subtotal_reserved"
            Me.XrLabel_subtotal_reserved.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_subtotal_reserved.SizeF = New System.Drawing.SizeF(125.0!, 15.0!)
            Me.XrLabel_subtotal_reserved.StylePriority.UseFont = False
            Me.XrLabel_subtotal_reserved.StylePriority.UseTextAlignment = False
            XrSummary6.FormatString = "{0:c}"
            XrSummary6.Running = DevExpress.XtraReports.UI.SummaryRunning.Group
            Me.XrLabel_subtotal_reserved.Summary = XrSummary6
            Me.XrLabel_subtotal_reserved.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
            '
            'XrPanel1
            '
            Me.XrPanel1.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel4, Me.XrLabel3, Me.XrLabel1})
            Me.XrPanel1.LocationFloat = New DevExpress.Utils.PointFloat(10.00001!, 141.6667!)
            Me.XrPanel1.Name = "XrPanel1"
            Me.XrPanel1.SizeF = New System.Drawing.SizeF(750.0!, 17.0!)
            Me.XrPanel1.StyleName = "XrControlStyle_HeaderPannel"
            '
            'XrLabel4
            '
            Me.XrLabel4.LocationFloat = New DevExpress.Utils.PointFloat(364.5833!, 0.0!)
            Me.XrLabel4.Name = "XrLabel4"
            Me.XrLabel4.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel4.SizeF = New System.Drawing.SizeF(125.0!, 15.0!)
            Me.XrLabel4.StylePriority.UseTextAlignment = False
            Me.XrLabel4.Text = "RESERVED"
            Me.XrLabel4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
            '
            'XrLabel3
            '
            Me.XrLabel3.LocationFloat = New DevExpress.Utils.PointFloat(243.0833!, 1.000023!)
            Me.XrLabel3.Name = "XrLabel3"
            Me.XrLabel3.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel3.SizeF = New System.Drawing.SizeF(97.25002!, 15.0!)
            Me.XrLabel3.StylePriority.UseTextAlignment = False
            Me.XrLabel3.Text = "BALANCE"
            Me.XrLabel3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
            '
            'XrLabel1
            '
            Me.XrLabel1.LocationFloat = New DevExpress.Utils.PointFloat(25.0!, 1.0!)
            Me.XrLabel1.Name = "XrLabel1"
            Me.XrLabel1.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel1.SizeF = New System.Drawing.SizeF(91.0!, 15.0!)
            Me.XrLabel1.StylePriority.UseTextAlignment = False
            Me.XrLabel1.Text = "STATE"
            Me.XrLabel1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
            '
            'XrLabel_reserved
            '
            Me.XrLabel_reserved.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "reserved", "{0:c}"), New DevExpress.XtraReports.UI.XRBinding("Tag", Nothing, "state")})
            Me.XrLabel_reserved.LocationFloat = New DevExpress.Utils.PointFloat(374.5833!, 0.0!)
            Me.XrLabel_reserved.Name = "XrLabel_reserved"
            Me.XrLabel_reserved.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_reserved.Scripts.OnPreviewClick = "Field_PreviewClick"
            Me.XrLabel_reserved.SizeF = New System.Drawing.SizeF(125.0!, 15.0!)
            Me.XrLabel_reserved.StylePriority.UseTextAlignment = False
            XrSummary2.FormatString = "{0:c}"
            Me.XrLabel_reserved.Summary = XrSummary2
            Me.XrLabel_reserved.Text = "$0.00"
            Me.XrLabel_reserved.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
            '
            'XrLabel_balance
            '
            Me.XrLabel_balance.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "balance", "{0:c}"), New DevExpress.XtraReports.UI.XRBinding("Tag", Nothing, "state")})
            Me.XrLabel_balance.LocationFloat = New DevExpress.Utils.PointFloat(224.5001!, 0.0!)
            Me.XrLabel_balance.Name = "XrLabel_balance"
            Me.XrLabel_balance.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_balance.Scripts.OnPreviewClick = "Field_PreviewClick"
            Me.XrLabel_balance.SizeF = New System.Drawing.SizeF(125.8332!, 15.0!)
            Me.XrLabel_balance.StylePriority.UseTextAlignment = False
            XrSummary1.FormatString = "{0:c}"
            Me.XrLabel_balance.Summary = XrSummary1
            Me.XrLabel_balance.Text = "$0.00"
            Me.XrLabel_balance.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
            '
            'XrLabel_state
            '
            Me.XrLabel_state.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "state"), New DevExpress.XtraReports.UI.XRBinding("Tag", Nothing, "state")})
            Me.XrLabel_state.LocationFloat = New DevExpress.Utils.PointFloat(35.00001!, 0.0!)
            Me.XrLabel_state.Name = "XrLabel_state"
            Me.XrLabel_state.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_state.Scripts.OnPreviewClick = "Field_PreviewClick"
            Me.XrLabel_state.SizeF = New System.Drawing.SizeF(91.0!, 15.0!)
            Me.XrLabel_state.StylePriority.UseTextAlignment = False
            Me.XrLabel_state.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
            '
            'GroupHeader1
            '
            Me.GroupHeader1.GroupFields.AddRange(New DevExpress.XtraReports.UI.GroupField() {New DevExpress.XtraReports.UI.GroupField("state_group", DevExpress.XtraReports.UI.XRColumnSortOrder.Ascending)})
            Me.GroupHeader1.HeightF = 0.0!
            Me.GroupHeader1.Name = "GroupHeader1"
            '
            'GroupFooter1
            '
            Me.GroupFooter1.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel_subtotal_text, Me.XrLabel_subtotal_balance, Me.XrLabel_subtotal_reserved})
            Me.GroupFooter1.HeightF = 36.45833!
            Me.GroupFooter1.Name = "GroupFooter1"
            Me.GroupFooter1.Scripts.OnBeforePrint = "GroupFooter1_BeforePrint"
            '
            'TrustAccountBalanceByStateReport
            '
            Me.Bands.AddRange(New DevExpress.XtraReports.UI.Band() {Me.Detail, Me.PageHeader, Me.PageFooter, Me.ReportFooter, Me.TopMarginBand1, Me.BottomMarginBand1, Me.GroupHeader1, Me.GroupFooter1})
            Me.RequestParameters = False
            Me.ScriptLanguage = DevExpress.XtraReports.ScriptLanguage.VisualBasic
            Me.ScriptReferencesString = resources.GetString("$this.ScriptReferencesString")
            Me.Scripts.OnBeforePrint = "TrustAccountBalanceByStateReport_BeforePrint"
            Me.ScriptsSource = resources.GetString("$this.ScriptsSource")
            Me.StyleSheet.AddRange(New DevExpress.XtraReports.UI.XRControlStyle() {Me.XrControlStyle_Header, Me.XrControlStyle_Totals, Me.XrControlStyle_HeaderPannel, Me.XrControlStyle_GroupHeader})
            Me.Version = "11.2"
            Me.Controls.SetChildIndex(Me.GroupFooter1, 0)
            Me.Controls.SetChildIndex(Me.GroupHeader1, 0)
            Me.Controls.SetChildIndex(Me.BottomMarginBand1, 0)
            Me.Controls.SetChildIndex(Me.TopMarginBand1, 0)
            Me.Controls.SetChildIndex(Me.ReportFooter, 0)
            Me.Controls.SetChildIndex(Me.PageFooter, 0)
            Me.Controls.SetChildIndex(Me.PageHeader, 0)
            Me.Controls.SetChildIndex(Me.Detail, 0)
            CType(Me, System.ComponentModel.ISupportInitialize).EndInit()

        End Sub

#End Region

        Dim ds As New System.Data.DataSet("ds")

        Private Sub Report_BeforePrint(ByVal sender As Object, ByVal e As System.EventArgs)
            Const TableName As String = "view_held_in_trust"
            Dim rpt As DevExpress.XtraReports.UI.XtraReport = CType(sender, DevExpress.XtraReports.UI.XtraReport)
            Dim sqlInfo As DebtPlus.LINQ.SQLInfoClass = DebtPlus.LINQ.SQLInfoClass.getDefault()
            Dim tbl As System.Data.DataTable = ds.Tables(TableName)

            If tbl Is Nothing Then
                Dim StateCode As String = "**"

                Try
                    Using cn As System.Data.SqlClient.SqlConnection = New System.Data.SqlClient.SqlConnection(sqlInfo.ConnectionString)
                        cn.Open()

                        '-- Find the current state code for the agency
                        Using cmd As New System.Data.SqlClient.SqlCommand()
                            cmd.Connection = cn
                            cmd.CommandText = "SELECT TOP 1 mailingcode from states st inner join addresses a on st.state = a.state inner join config cnf on a.address = cnf.addressid ORDER BY cnf.company_id"
                            cmd.CommandType = System.Data.CommandType.Text
                            Dim obj As Object = cmd.ExecuteScalar()
                            If obj IsNot Nothing AndAlso obj IsNot System.DBNull.Value Then StateCode = Convert.ToString(obj)
                        End Using

                        '-- Retrieve the trust balances for the clients
                        Using cmd As New System.Data.SqlClient.SqlCommand()
                            cmd.Connection = cn
                            cmd.CommandText = "SELECT [state], sum([held_in_trust]) as balance, sum([reserved_in_trust]) as reserved, case when [state]=@config_state then 2 else 1 end as state_group FROM view_held_in_trust WITH (NOLOCK) WHERE ([held_in_trust] <> 0) OR ([state]=@config_state) GROUP BY [state]"
                            cmd.CommandType = System.Data.CommandType.Text
                            cmd.Parameters.Add("@config_state", System.Data.SqlDbType.VarChar, 10).Value = StateCode
                            cmd.CommandTimeout = 0

                            Using da As New System.Data.SqlClient.SqlDataAdapter(cmd)
                                da.Fill(ds, TableName)
                                tbl = ds.Tables(TableName)
                            End Using
                        End Using
                    End Using

                Catch ex As System.Data.SqlClient.SqlException
                    Using gdr As New DebtPlus.Repository.GetDataResult()
                        gdr.HandleException(ex)
                        DebtPlus.UI.Common.ErrorHandling.HandleErrors(gdr, "Error reading report data")
                    End Using
                End Try
            End If

            If tbl IsNot Nothing Then
                rpt.DataSource = New System.Data.DataView(tbl, CType(rpt, DebtPlus.Interfaces.Reports.IReportFilter).ReportFilter.ViewFilter, "[state_group], [state]", System.Data.DataViewRowState.CurrentRows)
            End If
        End Sub

        Private Sub Field_PreviewClick(ByVal sender As Object, ByVal e As DevExpress.XtraReports.UI.PreviewMouseEventArgs)
            Dim StateOBJ As Object = e.Brick.Value
            Dim state As String = DebtPlus.Utils.Nulls.DStr(StateOBJ)
            Dim thrd As New System.Threading.Thread(New System.Threading.ParameterizedThreadStart(AddressOf SubReportPrint))
            With thrd
                .IsBackground = True
                .SetApartmentState(Threading.ApartmentState.STA)
                .Name = state
                .Start(state)
            End With
        End Sub

        Private Sub SubReportPrint(ByVal obj As Object)
            Dim State As String = Convert.ToString(obj)

            Dim rpt As New DebtPlus.Reports.Trust.AccountBalance.TrustAccountBalanceReport()
            rpt.Parameters("ParameterState").Value = State
            Using frm As New DebtPlus.Reports.Template.PrintPreviewForm(rpt)
                frm.ShowDialog()
            End Using
        End Sub

        Private Sub GroupFooter1_BeforePrint(sender As Object, e As System.Drawing.Printing.PrintEventArgs)

            '-- Cancel the printing of the group footer (since there is only one detail row) if this is the offset state
            With CType(sender, DevExpress.XtraReports.UI.GroupFooterBand)
                Dim rpt As DevExpress.XtraReports.UI.XtraReport = CType(.Report, DevExpress.XtraReports.UI.XtraReport)
                Dim state_group As Int32 = Convert.ToInt32(rpt.GetCurrentColumnValue("state_group"))
                e.Cancel = (state_group = 2)
            End With

        End Sub
    End Class
End Namespace
