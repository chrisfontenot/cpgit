#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}
#End Region

Namespace Operations.MonthEnd.Summary
    Friend Class SubReport_OPERATOR_Detail

        Protected Friend Overrides Sub SetSubreportParameters(ByVal vue As System.Data.DataView)
            MyBase.SetSubreportParameters(vue)

            With XrLabel_amount
                .DataBindings.Add("Text", vue, "gross", "{0:c}")
            End With

            With XrLabel_client
                AddHandler .BeforePrint, AddressOf FormatClient
            End With

            With XrLabel_date_created
                .DataBindings.Add("Text", vue, "item_date", "{0:d}")
            End With

            With XrLabel_counselor
                .DataBindings.Add("Text", vue, "counselor")
            End With
        End Sub

        Protected Overrides Sub FieldPreviewClick(ByVal sender As Object, ByVal e As DevExpress.XtraReports.UI.PreviewMouseEventArgs)
        End Sub
    End Class
End Namespace