Namespace Operations.MonthEnd.Summary

    <Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
    Partial Class SubReport_DEPOSITS_Detail
        Inherits SubReport_DEPOSITS

        'XtraReport overrides dispose to clean up the component list.
        <System.Diagnostics.DebuggerNonUserCode()> _
        Protected Overrides Sub Dispose(ByVal disposing As Boolean)
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
            MyBase.Dispose(disposing)
        End Sub

        'Required by the Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Designer
        'It can be modified using the Designer.  
        'Do not modify it using the code editor.
        <System.Diagnostics.DebuggerStepThrough()> _
        Private Sub InitializeComponent()
            Me.XrLabel_type = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_amount = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_client = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_date = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_reference = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel10 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel11 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel12 = New DevExpress.XtraReports.UI.XRLabel
            CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
            '
            'Detail
            '
            Me.Detail.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel_reference, Me.XrLabel_date, Me.XrLabel_client, Me.XrLabel_amount, Me.XrLabel_type})
            Me.Detail.Height = 17
            '
            'XrLabel1
            '
            Me.XrLabel1.StylePriority.UseFont = False
            Me.XrLabel1.StylePriority.UseForeColor = False
            Me.XrLabel1.StylePriority.UseTextAlignment = False
            '
            'XrPanel1
            '
            Me.XrPanel1.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel12, Me.XrLabel11, Me.XrLabel10})
            Me.XrPanel1.StylePriority.UseBackColor = False
            '
            'XrLabel7
            '
            Me.XrLabel7.StylePriority.UseTextAlignment = False
            '
            'GroupFooter1
            '
            Me.GroupFooter1.Visible = False
            '
            'ReportFooter
            '
            Me.ReportFooter.Height = 34
            Me.ReportFooter.StylePriority.UseFont = False
            '
            'XrLabel_group_amount
            '
            Me.XrLabel_group_amount.StylePriority.UseTextAlignment = False
            '
            'XrLabel_report_amount
            '
            Me.XrLabel_report_amount.StylePriority.UseTextAlignment = False
            '
            'XrLabel_type
            '
            Me.XrLabel_type.Location = New System.Drawing.Point(0, 0)
            Me.XrLabel_type.Name = "XrLabel_type"
            Me.XrLabel_type.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_type.Size = New System.Drawing.Size(300, 17)
            Me.XrLabel_type.StylePriority.UseTextAlignment = False
            Me.XrLabel_type.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            '
            'XrLabel_amount
            '
            Me.XrLabel_amount.Location = New System.Drawing.Point(300, 0)
            Me.XrLabel_amount.Name = "XrLabel_amount"
            Me.XrLabel_amount.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_amount.Size = New System.Drawing.Size(100, 17)
            Me.XrLabel_amount.StylePriority.UseTextAlignment = False
            Me.XrLabel_amount.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel_client
            '
            Me.XrLabel_client.Location = New System.Drawing.Point(400, 0)
            Me.XrLabel_client.Name = "XrLabel_client"
            Me.XrLabel_client.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_client.Size = New System.Drawing.Size(100, 17)
            Me.XrLabel_client.StylePriority.UseTextAlignment = False
            Me.XrLabel_client.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel_date
            '
            Me.XrLabel_date.Location = New System.Drawing.Point(500, 0)
            Me.XrLabel_date.Name = "XrLabel_date"
            Me.XrLabel_date.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_date.Size = New System.Drawing.Size(92, 17)
            Me.XrLabel_date.StylePriority.UseTextAlignment = False
            Me.XrLabel_date.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel_reference
            '
            Me.XrLabel_reference.Location = New System.Drawing.Point(600, 0)
            Me.XrLabel_reference.Name = "XrLabel_reference"
            Me.XrLabel_reference.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_reference.Size = New System.Drawing.Size(200, 17)
            Me.XrLabel_reference.StylePriority.UseTextAlignment = False
            Me.XrLabel_reference.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            '
            'XrLabel10
            '
            Me.XrLabel10.Location = New System.Drawing.Point(600, 0)
            Me.XrLabel10.Name = "XrLabel10"
            Me.XrLabel10.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel10.Size = New System.Drawing.Size(100, 17)
            Me.XrLabel10.StylePriority.UseTextAlignment = False
            Me.XrLabel10.Text = "REFERENCE"
            Me.XrLabel10.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            '
            'XrLabel11
            '
            Me.XrLabel11.Location = New System.Drawing.Point(500, 0)
            Me.XrLabel11.Name = "XrLabel11"
            Me.XrLabel11.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel11.Size = New System.Drawing.Size(92, 17)
            Me.XrLabel11.StylePriority.UseTextAlignment = False
            Me.XrLabel11.Text = "DATE"
            Me.XrLabel11.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel12
            '
            Me.XrLabel12.Location = New System.Drawing.Point(400, 0)
            Me.XrLabel12.Name = "XrLabel12"
            Me.XrLabel12.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel12.Size = New System.Drawing.Size(100, 17)
            Me.XrLabel12.StylePriority.UseTextAlignment = False
            Me.XrLabel12.Text = "CLIENT"
            Me.XrLabel12.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'SubReport_DEPOSITS_Detail
            '
            Me.Version = "8.2"
            CType(Me, System.ComponentModel.ISupportInitialize).EndInit()

        End Sub
        Friend WithEvents XrLabel_reference As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_date As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_client As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_amount As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_type As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel12 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel11 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel10 As DevExpress.XtraReports.UI.XRLabel
    End Class
End Namespace