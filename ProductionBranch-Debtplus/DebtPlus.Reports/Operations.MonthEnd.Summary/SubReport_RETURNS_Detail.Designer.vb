Namespace Operations.MonthEnd.Summary

    <Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
    Partial Class SubReport_RETURNS_Detail
        Inherits Subreport_RETURNS

        'XtraReport overrides dispose to clean up the component list.
        <System.Diagnostics.DebuggerNonUserCode()> _
        Protected Overrides Sub Dispose(ByVal disposing As Boolean)
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
            MyBase.Dispose(disposing)
        End Sub

        'Required by the Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Designer
        'It can be modified using the Designer.  
        'Do not modify it using the code editor.
        <System.Diagnostics.DebuggerStepThrough()> _
        Private Sub InitializeComponent()
            Me.XrLabel2 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel3 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel5 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_type = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_amount = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_item_date = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_creditor = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_reference = New DevExpress.XtraReports.UI.XRLabel
            CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
            '
            'Detail
            '
            Me.Detail.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel_reference, Me.XrLabel_creditor, Me.XrLabel_item_date, Me.XrLabel_amount, Me.XrLabel_type})
            Me.Detail.Height = 17
            '
            'GroupFooter1
            '
            Me.GroupFooter1.Visible = False
            '
            'ReportFooter
            '
            Me.ReportFooter.Height = 34
            Me.ReportFooter.StylePriority.UseFont = False
            '
            'XrLabel1
            '
            Me.XrLabel1.StylePriority.UseFont = False
            Me.XrLabel1.StylePriority.UseForeColor = False
            Me.XrLabel1.StylePriority.UseTextAlignment = False
            '
            'XrPanel1
            '
            Me.XrPanel1.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel5, Me.XrLabel3, Me.XrLabel2})
            Me.XrPanel1.StylePriority.UseBackColor = False
            '
            'XrLabel7
            '
            Me.XrLabel7.StylePriority.UseTextAlignment = False
            '
            'XrLabel_group_amount
            '
            Me.XrLabel_group_amount.StylePriority.UseTextAlignment = False
            '
            'XrLabel_report_amount
            '
            Me.XrLabel_report_amount.StylePriority.UseTextAlignment = False
            '
            'XrLabel2
            '
            Me.XrLabel2.Location = New System.Drawing.Point(400, 0)
            Me.XrLabel2.Name = "XrLabel2"
            Me.XrLabel2.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel2.Size = New System.Drawing.Size(92, 15)
            Me.XrLabel2.StylePriority.UseTextAlignment = False
            Me.XrLabel2.Text = "DATE"
            Me.XrLabel2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel3
            '
            Me.XrLabel3.Location = New System.Drawing.Point(500, 0)
            Me.XrLabel3.Name = "XrLabel3"
            Me.XrLabel3.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel3.Size = New System.Drawing.Size(100, 17)
            Me.XrLabel3.Text = "SOURCE"
            '
            'XrLabel5
            '
            Me.XrLabel5.Location = New System.Drawing.Point(600, 0)
            Me.XrLabel5.Name = "XrLabel5"
            Me.XrLabel5.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel5.Size = New System.Drawing.Size(100, 15)
            Me.XrLabel5.Text = "REF"
            '
            'XrLabel_type
            '
            Me.XrLabel_type.Location = New System.Drawing.Point(0, 0)
            Me.XrLabel_type.Name = "XrLabel_type"
            Me.XrLabel_type.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_type.Size = New System.Drawing.Size(300, 15)
            '
            'XrLabel_amount
            '
            Me.XrLabel_amount.Location = New System.Drawing.Point(300, 0)
            Me.XrLabel_amount.Name = "XrLabel_amount"
            Me.XrLabel_amount.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_amount.Size = New System.Drawing.Size(100, 17)
            Me.XrLabel_amount.StylePriority.UseTextAlignment = False
            Me.XrLabel_amount.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel_item_date
            '
            Me.XrLabel_item_date.Location = New System.Drawing.Point(400, 0)
            Me.XrLabel_item_date.Name = "XrLabel_item_date"
            Me.XrLabel_item_date.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_item_date.Size = New System.Drawing.Size(92, 17)
            Me.XrLabel_item_date.StylePriority.UseTextAlignment = False
            Me.XrLabel_item_date.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel_creditor
            '
            Me.XrLabel_creditor.Location = New System.Drawing.Point(500, 0)
            Me.XrLabel_creditor.Name = "XrLabel_creditor"
            Me.XrLabel_creditor.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_creditor.Size = New System.Drawing.Size(100, 17)
            Me.XrLabel_creditor.StylePriority.UseTextAlignment = False
            Me.XrLabel_creditor.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            '
            'XrLabel_reference
            '
            Me.XrLabel_reference.Location = New System.Drawing.Point(600, 0)
            Me.XrLabel_reference.Name = "XrLabel_reference"
            Me.XrLabel_reference.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_reference.Size = New System.Drawing.Size(200, 17)
            Me.XrLabel_reference.StylePriority.UseTextAlignment = False
            Me.XrLabel_reference.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            '
            'SubReport_RETURNS_Detail
            '
            Me.Version = "8.2"
            CType(Me, System.ComponentModel.ISupportInitialize).EndInit()

        End Sub
        Friend WithEvents XrLabel_type As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel5 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel3 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel2 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_reference As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_creditor As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_item_date As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_amount As DevExpress.XtraReports.UI.XRLabel
    End Class
End Namespace