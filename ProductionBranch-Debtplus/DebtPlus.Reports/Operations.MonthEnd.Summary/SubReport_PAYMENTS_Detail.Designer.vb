Namespace Operations.MonthEnd.Summary

    <Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
    Partial Class SubReport_PAYMENTS_Detail
        Inherits SubReport_PAYMENTS

        'XtraReport overrides dispose to clean up the component list.
        <System.Diagnostics.DebuggerNonUserCode()> _
        Protected Overrides Sub Dispose(ByVal disposing As Boolean)
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
            MyBase.Dispose(disposing)
        End Sub

        'Required by the Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Designer
        'It can be modified using the Designer.  
        'Do not modify it using the code editor.
        <System.Diagnostics.DebuggerStepThrough()> _
        Private Sub InitializeComponent()
            Me.XrLabel_type = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_gross = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_deducted = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_billed = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_created_date = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel2 = New DevExpress.XtraReports.UI.XRLabel
            CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
            '
            'Detail
            '
            Me.Detail.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel_created_date, Me.XrLabel_billed, Me.XrLabel_deducted, Me.XrLabel_gross, Me.XrLabel_type})
            Me.Detail.Height = 17
            '
            'XrPanel1
            '
            Me.XrPanel1.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel2})
            Me.XrPanel1.StylePriority.UseBackColor = False
            '
            'GroupFooter1
            '
            Me.GroupFooter1.Visible = False
            '
            'XrLabel1
            '
            Me.XrLabel1.StylePriority.UseFont = False
            Me.XrLabel1.StylePriority.UseForeColor = False
            Me.XrLabel1.StylePriority.UseTextAlignment = False
            '
            'ReportFooter
            '
            Me.ReportFooter.StylePriority.UseFont = False
            '
            'XrLabel9
            '
            Me.XrLabel9.StylePriority.UseTextAlignment = False
            '
            'XrLabel8
            '
            Me.XrLabel8.StylePriority.UseTextAlignment = False
            '
            'XrLabel7
            '
            Me.XrLabel7.StylePriority.UseTextAlignment = False
            '
            'XrLabel_group_billed
            '
            Me.XrLabel_group_billed.StylePriority.UseTextAlignment = False
            '
            'XrLabel_group_deducted
            '
            Me.XrLabel_group_deducted.StylePriority.UseTextAlignment = False
            '
            'XrLabel_group_gross
            '
            Me.XrLabel_group_gross.StylePriority.UseTextAlignment = False
            '
            'XrLabel_total_gross
            '
            Me.XrLabel_total_gross.StylePriority.UseTextAlignment = False
            '
            'XrLabel_total_deducted
            '
            Me.XrLabel_total_deducted.StylePriority.UseTextAlignment = False
            '
            'XrLabel_total_billed
            '
            Me.XrLabel_total_billed.StylePriority.UseTextAlignment = False
            '
            'XrLabel_type
            '
            Me.XrLabel_type.Location = New System.Drawing.Point(0, 0)
            Me.XrLabel_type.Name = "XrLabel_type"
            Me.XrLabel_type.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_type.Size = New System.Drawing.Size(300, 17)
            '
            'XrLabel_gross
            '
            Me.XrLabel_gross.Location = New System.Drawing.Point(300, 0)
            Me.XrLabel_gross.Name = "XrLabel_gross"
            Me.XrLabel_gross.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_gross.Size = New System.Drawing.Size(100, 17)
            Me.XrLabel_gross.StylePriority.UseTextAlignment = False
            Me.XrLabel_gross.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel_deducted
            '
            Me.XrLabel_deducted.Location = New System.Drawing.Point(400, 0)
            Me.XrLabel_deducted.Name = "XrLabel_deducted"
            Me.XrLabel_deducted.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_deducted.Size = New System.Drawing.Size(100, 17)
            Me.XrLabel_deducted.StylePriority.UseTextAlignment = False
            Me.XrLabel_deducted.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel_billed
            '
            Me.XrLabel_billed.Location = New System.Drawing.Point(500, 0)
            Me.XrLabel_billed.Name = "XrLabel_billed"
            Me.XrLabel_billed.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_billed.Size = New System.Drawing.Size(100, 17)
            Me.XrLabel_billed.StylePriority.UseTextAlignment = False
            Me.XrLabel_billed.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel_created_date
            '
            Me.XrLabel_created_date.Location = New System.Drawing.Point(600, 0)
            Me.XrLabel_created_date.Name = "XrLabel_created_date"
            Me.XrLabel_created_date.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_created_date.Size = New System.Drawing.Size(100, 17)
            Me.XrLabel_created_date.StylePriority.UseTextAlignment = False
            Me.XrLabel_created_date.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel2
            '
            Me.XrLabel2.Location = New System.Drawing.Point(600, 0)
            Me.XrLabel2.Name = "XrLabel2"
            Me.XrLabel2.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel2.Size = New System.Drawing.Size(100, 17)
            Me.XrLabel2.StylePriority.UseTextAlignment = False
            Me.XrLabel2.Text = "DATE"
            Me.XrLabel2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'SubReport_PAYMENTS_Detail
            '
            Me.Version = "8.2"
            CType(Me, System.ComponentModel.ISupportInitialize).EndInit()

        End Sub
        Friend WithEvents XrLabel_created_date As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_billed As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_deducted As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_gross As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_type As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel2 As DevExpress.XtraReports.UI.XRLabel
    End Class
End Namespace