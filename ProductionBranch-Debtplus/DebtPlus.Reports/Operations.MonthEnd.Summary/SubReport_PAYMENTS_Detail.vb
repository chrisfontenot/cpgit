#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}
#End Region

Namespace Operations.MonthEnd.Summary
    Friend Class SubReport_PAYMENTS_Detail

        Protected Friend Overrides Sub SetSubreportParameters(ByVal vue As System.Data.DataView)
            MyBase.SetSubreportParameters(vue)

            With XrLabel_type
                .DataBindings.Add("Text", vue, "type")
            End With

            With XrLabel_gross
                .DataBindings.Add("Text", vue, "gross", "{0:c}")
            End With

            With XrLabel_deducted
                .DataBindings.Add("Text", vue, "deducted", "{0:c}")
            End With

            With XrLabel_billed
                .DataBindings.Add("Text", vue, "billed", "{0:c}")
            End With

            With XrLabel_created_date
                .DataBindings.Add("Text", vue, "created_date", "{0:d}")
            End With

        End Sub
    End Class
End Namespace