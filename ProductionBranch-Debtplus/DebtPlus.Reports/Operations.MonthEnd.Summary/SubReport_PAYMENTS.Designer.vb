Namespace Operations.MonthEnd.Summary
    <Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
    Partial Class SubReport_PAYMENTS
        Inherits TemplateSubReport

        'XtraReport overrides dispose to clean up the component list.
        <System.Diagnostics.DebuggerNonUserCode()> _
        Protected Overrides Sub Dispose(ByVal disposing As Boolean)
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
            MyBase.Dispose(disposing)
        End Sub

        'Required by the Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Designer
        'It can be modified using the Designer.  
        'Do not modify it using the code editor.
        <System.Diagnostics.DebuggerStepThrough()> _
        Private Sub InitializeComponent()
            Dim XrSummary1 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary
            Dim XrSummary2 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary
            Dim XrSummary3 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary
            Dim XrSummary4 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary
            Dim XrSummary5 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary
            Dim XrSummary6 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary
            Me.Detail = New DevExpress.XtraReports.UI.DetailBand
            Me.PageHeader = New DevExpress.XtraReports.UI.PageHeaderBand
            Me.XrLabel1 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrPanel1 = New DevExpress.XtraReports.UI.XRPanel
            Me.XrLabel9 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel8 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel7 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel6 = New DevExpress.XtraReports.UI.XRLabel
            Me.GroupFooter1 = New DevExpress.XtraReports.UI.GroupFooterBand
            Me.XrLabel_group_billed = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_group_deducted = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_group_gross = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_group_type = New DevExpress.XtraReports.UI.XRLabel
            Me.XrControlStyle1 = New DevExpress.XtraReports.UI.XRControlStyle
            Me.ReportFooter = New DevExpress.XtraReports.UI.ReportFooterBand
            Me.XrLabel_total_gross = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_total_deducted = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_total_billed = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLine1 = New DevExpress.XtraReports.UI.XRLine
            Me.XrLabel3 = New DevExpress.XtraReports.UI.XRLabel
            Me.GroupHeader1 = New DevExpress.XtraReports.UI.GroupHeaderBand
            CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
            '
            'Detail
            '
            Me.Detail.Height = 0
            Me.Detail.Name = "Detail"
            Me.Detail.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
            Me.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
            '
            'PageHeader
            '
            Me.PageHeader.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel1, Me.XrPanel1})
            Me.PageHeader.Height = 85
            Me.PageHeader.Name = "PageHeader"
            Me.PageHeader.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
            Me.PageHeader.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
            '
            'XrLabel1
            '
            Me.XrLabel1.Font = New System.Drawing.Font("Times New Roman", 12.0!, System.Drawing.FontStyle.Bold)
            Me.XrLabel1.ForeColor = System.Drawing.Color.Teal
            Me.XrLabel1.Location = New System.Drawing.Point(0, 17)
            Me.XrLabel1.Name = "XrLabel1"
            Me.XrLabel1.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel1.Size = New System.Drawing.Size(800, 25)
            Me.XrLabel1.StylePriority.UseFont = False
            Me.XrLabel1.StylePriority.UseForeColor = False
            Me.XrLabel1.StylePriority.UseTextAlignment = False
            Me.XrLabel1.Text = "DISBURSEMENTS TO CREDITORS AND CLIENT REFUNDS"
            Me.XrLabel1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            '
            'XrPanel1
            '
            Me.XrPanel1.BackColor = System.Drawing.Color.Teal
            Me.XrPanel1.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel9, Me.XrLabel8, Me.XrLabel7, Me.XrLabel6})
            Me.XrPanel1.Location = New System.Drawing.Point(0, 58)
            Me.XrPanel1.Name = "XrPanel1"
            Me.XrPanel1.Size = New System.Drawing.Size(800, 17)
            Me.XrPanel1.StyleName = "XrControlStyle1"
            Me.XrPanel1.StylePriority.UseBackColor = False
            '
            'XrLabel9
            '
            Me.XrLabel9.Location = New System.Drawing.Point(500, 0)
            Me.XrLabel9.Name = "XrLabel9"
            Me.XrLabel9.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel9.Size = New System.Drawing.Size(100, 17)
            Me.XrLabel9.StylePriority.UseTextAlignment = False
            Me.XrLabel9.Text = "BILLED"
            Me.XrLabel9.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel8
            '
            Me.XrLabel8.Location = New System.Drawing.Point(400, 0)
            Me.XrLabel8.Name = "XrLabel8"
            Me.XrLabel8.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel8.Size = New System.Drawing.Size(100, 17)
            Me.XrLabel8.StylePriority.UseTextAlignment = False
            Me.XrLabel8.Text = "DEDUCTED"
            Me.XrLabel8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel7
            '
            Me.XrLabel7.Location = New System.Drawing.Point(300, 0)
            Me.XrLabel7.Name = "XrLabel7"
            Me.XrLabel7.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel7.Size = New System.Drawing.Size(100, 17)
            Me.XrLabel7.StylePriority.UseTextAlignment = False
            Me.XrLabel7.Text = "GROSS"
            Me.XrLabel7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel6
            '
            Me.XrLabel6.Location = New System.Drawing.Point(0, 0)
            Me.XrLabel6.Name = "XrLabel6"
            Me.XrLabel6.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel6.Size = New System.Drawing.Size(142, 17)
            Me.XrLabel6.Text = "TYPE"
            '
            'GroupFooter1
            '
            Me.GroupFooter1.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel_group_billed, Me.XrLabel_group_deducted, Me.XrLabel_group_gross, Me.XrLabel_group_type})
            Me.GroupFooter1.Height = 17
            Me.GroupFooter1.Name = "GroupFooter1"
            '
            'XrLabel_group_billed
            '
            Me.XrLabel_group_billed.Location = New System.Drawing.Point(500, 0)
            Me.XrLabel_group_billed.Name = "XrLabel_group_billed"
            Me.XrLabel_group_billed.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_group_billed.Size = New System.Drawing.Size(100, 17)
            Me.XrLabel_group_billed.StylePriority.UseTextAlignment = False
            XrSummary1.FormatString = "{0:c}"
            XrSummary1.Running = DevExpress.XtraReports.UI.SummaryRunning.Group
            Me.XrLabel_group_billed.Summary = XrSummary1
            Me.XrLabel_group_billed.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel_group_deducted
            '
            Me.XrLabel_group_deducted.Location = New System.Drawing.Point(400, 0)
            Me.XrLabel_group_deducted.Name = "XrLabel_group_deducted"
            Me.XrLabel_group_deducted.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_group_deducted.Size = New System.Drawing.Size(100, 17)
            Me.XrLabel_group_deducted.StylePriority.UseTextAlignment = False
            XrSummary2.FormatString = "{0:c}"
            XrSummary2.Running = DevExpress.XtraReports.UI.SummaryRunning.Group
            Me.XrLabel_group_deducted.Summary = XrSummary2
            Me.XrLabel_group_deducted.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel_group_gross
            '
            Me.XrLabel_group_gross.Location = New System.Drawing.Point(300, 0)
            Me.XrLabel_group_gross.Name = "XrLabel_group_gross"
            Me.XrLabel_group_gross.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_group_gross.Size = New System.Drawing.Size(100, 17)
            Me.XrLabel_group_gross.StylePriority.UseTextAlignment = False
            XrSummary3.FormatString = "{0:c}"
            XrSummary3.Running = DevExpress.XtraReports.UI.SummaryRunning.Group
            Me.XrLabel_group_gross.Summary = XrSummary3
            Me.XrLabel_group_gross.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel_group_type
            '
            Me.XrLabel_group_type.Location = New System.Drawing.Point(0, 0)
            Me.XrLabel_group_type.Name = "XrLabel_group_type"
            Me.XrLabel_group_type.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_group_type.Size = New System.Drawing.Size(292, 17)
            '
            'XrControlStyle1
            '
            Me.XrControlStyle1.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.XrControlStyle1.ForeColor = System.Drawing.Color.White
            Me.XrControlStyle1.Name = "XrControlStyle1"
            Me.XrControlStyle1.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
            Me.XrControlStyle1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            '
            'ReportFooter
            '
            Me.ReportFooter.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel_total_gross, Me.XrLabel_total_deducted, Me.XrLabel_total_billed, Me.XrLine1, Me.XrLabel3})
            Me.ReportFooter.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.ReportFooter.Name = "ReportFooter"
            Me.ReportFooter.StylePriority.UseFont = False
            '
            'XrLabel_total_gross
            '
            Me.XrLabel_total_gross.Location = New System.Drawing.Point(300, 17)
            Me.XrLabel_total_gross.Name = "XrLabel_total_gross"
            Me.XrLabel_total_gross.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_total_gross.Size = New System.Drawing.Size(100, 17)
            Me.XrLabel_total_gross.StylePriority.UseTextAlignment = False
            XrSummary4.FormatString = "{0:c}"
            XrSummary4.Running = DevExpress.XtraReports.UI.SummaryRunning.Report
            Me.XrLabel_total_gross.Summary = XrSummary4
            Me.XrLabel_total_gross.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel_total_deducted
            '
            Me.XrLabel_total_deducted.Location = New System.Drawing.Point(400, 17)
            Me.XrLabel_total_deducted.Name = "XrLabel_total_deducted"
            Me.XrLabel_total_deducted.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_total_deducted.Size = New System.Drawing.Size(100, 17)
            Me.XrLabel_total_deducted.StylePriority.UseTextAlignment = False
            XrSummary5.FormatString = "{0:c}"
            XrSummary5.Running = DevExpress.XtraReports.UI.SummaryRunning.Report
            Me.XrLabel_total_deducted.Summary = XrSummary5
            Me.XrLabel_total_deducted.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel_total_billed
            '
            Me.XrLabel_total_billed.Location = New System.Drawing.Point(500, 17)
            Me.XrLabel_total_billed.Name = "XrLabel_total_billed"
            Me.XrLabel_total_billed.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_total_billed.Size = New System.Drawing.Size(100, 17)
            Me.XrLabel_total_billed.StylePriority.UseTextAlignment = False
            XrSummary6.FormatString = "{0:c}"
            XrSummary6.Running = DevExpress.XtraReports.UI.SummaryRunning.Report
            Me.XrLabel_total_billed.Summary = XrSummary6
            Me.XrLabel_total_billed.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLine1
            '
            Me.XrLine1.Location = New System.Drawing.Point(0, 0)
            Me.XrLine1.Name = "XrLine1"
            Me.XrLine1.Size = New System.Drawing.Size(800, 8)
            '
            'XrLabel3
            '
            Me.XrLabel3.Location = New System.Drawing.Point(0, 17)
            Me.XrLabel3.Name = "XrLabel3"
            Me.XrLabel3.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel3.Size = New System.Drawing.Size(142, 17)
            Me.XrLabel3.Text = "TOTALS"
            '
            'GroupHeader1
            '
            Me.GroupHeader1.GroupFields.AddRange(New DevExpress.XtraReports.UI.GroupField() {New DevExpress.XtraReports.UI.GroupField("tran_type", DevExpress.XtraReports.UI.XRColumnSortOrder.Ascending)})
            Me.GroupHeader1.Height = 0
            Me.GroupHeader1.Name = "GroupHeader1"
            '
            'SubReport_PAYMENTS
            '
            Me.Bands.AddRange(New DevExpress.XtraReports.UI.Band() {Me.Detail, Me.PageHeader, Me.GroupFooter1, Me.ReportFooter, Me.GroupHeader1})
            Me.Margins = New System.Drawing.Printing.Margins(25, 25, 25, 25)
            Me.StyleSheet.AddRange(New DevExpress.XtraReports.UI.XRControlStyle() {Me.XrControlStyle1})
            Me.Version = "8.2"
            CType(Me, System.ComponentModel.ISupportInitialize).EndInit()

        End Sub
        Protected Friend WithEvents Detail As DevExpress.XtraReports.UI.DetailBand
        Protected Friend WithEvents PageHeader As DevExpress.XtraReports.UI.PageHeaderBand
        Protected Friend WithEvents XrPanel1 As DevExpress.XtraReports.UI.XRPanel
        Protected Friend WithEvents GroupFooter1 As DevExpress.XtraReports.UI.GroupFooterBand
        Protected Friend WithEvents XrControlStyle1 As DevExpress.XtraReports.UI.XRControlStyle
        Protected Friend WithEvents XrLabel1 As DevExpress.XtraReports.UI.XRLabel
        Protected Friend WithEvents XrLabel_group_type As DevExpress.XtraReports.UI.XRLabel
        Protected Friend WithEvents ReportFooter As DevExpress.XtraReports.UI.ReportFooterBand
        Protected Friend WithEvents XrLine1 As DevExpress.XtraReports.UI.XRLine
        Protected Friend WithEvents XrLabel3 As DevExpress.XtraReports.UI.XRLabel
        Protected Friend WithEvents GroupHeader1 As DevExpress.XtraReports.UI.GroupHeaderBand
        Protected Friend WithEvents XrLabel9 As DevExpress.XtraReports.UI.XRLabel
        Protected Friend WithEvents XrLabel8 As DevExpress.XtraReports.UI.XRLabel
        Protected Friend WithEvents XrLabel7 As DevExpress.XtraReports.UI.XRLabel
        Protected Friend WithEvents XrLabel6 As DevExpress.XtraReports.UI.XRLabel
        Protected Friend WithEvents XrLabel_group_billed As DevExpress.XtraReports.UI.XRLabel
        Protected Friend WithEvents XrLabel_group_deducted As DevExpress.XtraReports.UI.XRLabel
        Protected Friend WithEvents XrLabel_group_gross As DevExpress.XtraReports.UI.XRLabel
        Protected Friend WithEvents XrLabel_total_gross As DevExpress.XtraReports.UI.XRLabel
        Protected Friend WithEvents XrLabel_total_deducted As DevExpress.XtraReports.UI.XRLabel
        Protected Friend WithEvents XrLabel_total_billed As DevExpress.XtraReports.UI.XRLabel
    End Class
End Namespace