Namespace Operations.MonthEnd.Summary
    <Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
    Partial Class Subreport_RETURNS
        Inherits TemplateSubReport

        'XtraReport overrides dispose to clean up the component list.
        <System.Diagnostics.DebuggerNonUserCode()> _
        Protected Overrides Sub Dispose(ByVal disposing As Boolean)
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
            MyBase.Dispose(disposing)
        End Sub

        'Required by the Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Designer
        'It can be modified using the Designer.  
        'Do not modify it using the code editor.
        <System.Diagnostics.DebuggerStepThrough()> _
        Private Sub InitializeComponent()
            Dim XrSummary1 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary
            Dim XrSummary2 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary
            Me.Detail = New DevExpress.XtraReports.UI.DetailBand
            Me.PageHeader = New DevExpress.XtraReports.UI.PageHeaderBand
            Me.GroupHeader1 = New DevExpress.XtraReports.UI.GroupHeaderBand
            Me.GroupFooter1 = New DevExpress.XtraReports.UI.GroupFooterBand
            Me.ReportFooter = New DevExpress.XtraReports.UI.ReportFooterBand
            Me.XrPanel1 = New DevExpress.XtraReports.UI.XRPanel
            Me.XrLabel7 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel6 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel1 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrControlStyle1 = New DevExpress.XtraReports.UI.XRControlStyle
            Me.XrLabel_group_amount = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_group_type = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel4 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_report_amount = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLine1 = New DevExpress.XtraReports.UI.XRLine
            CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
            '
            'Detail
            '
            Me.Detail.Height = 0
            Me.Detail.Name = "Detail"
            Me.Detail.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
            Me.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
            '
            'PageHeader
            '
            Me.PageHeader.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel1, Me.XrPanel1})
            Me.PageHeader.Height = 81
            Me.PageHeader.Name = "PageHeader"
            Me.PageHeader.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
            Me.PageHeader.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
            '
            'GroupHeader1
            '
            Me.GroupHeader1.GroupFields.AddRange(New DevExpress.XtraReports.UI.GroupField() {New DevExpress.XtraReports.UI.GroupField("tran_type", DevExpress.XtraReports.UI.XRColumnSortOrder.Ascending)})
            Me.GroupHeader1.Height = 0
            Me.GroupHeader1.Name = "GroupHeader1"
            '
            'GroupFooter1
            '
            Me.GroupFooter1.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel_group_type, Me.XrLabel_group_amount})
            Me.GroupFooter1.Height = 17
            Me.GroupFooter1.Name = "GroupFooter1"
            '
            'ReportFooter
            '
            Me.ReportFooter.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLine1, Me.XrLabel_report_amount, Me.XrLabel4})
            Me.ReportFooter.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.ReportFooter.Name = "ReportFooter"
            Me.ReportFooter.StylePriority.UseFont = False
            '
            'XrPanel1
            '
            Me.XrPanel1.BackColor = System.Drawing.Color.Teal
            Me.XrPanel1.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel7, Me.XrLabel6})
            Me.XrPanel1.Location = New System.Drawing.Point(0, 58)
            Me.XrPanel1.Name = "XrPanel1"
            Me.XrPanel1.Size = New System.Drawing.Size(800, 17)
            Me.XrPanel1.StyleName = "XrControlStyle1"
            Me.XrPanel1.StylePriority.UseBackColor = False
            '
            'XrLabel7
            '
            Me.XrLabel7.Location = New System.Drawing.Point(300, 0)
            Me.XrLabel7.Name = "XrLabel7"
            Me.XrLabel7.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel7.Size = New System.Drawing.Size(100, 15)
            Me.XrLabel7.StylePriority.UseTextAlignment = False
            Me.XrLabel7.Text = "AMOUNT"
            Me.XrLabel7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel6
            '
            Me.XrLabel6.Location = New System.Drawing.Point(0, 0)
            Me.XrLabel6.Name = "XrLabel6"
            Me.XrLabel6.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel6.Size = New System.Drawing.Size(300, 15)
            Me.XrLabel6.Text = "DESCRIPTION"
            '
            'XrLabel1
            '
            Me.XrLabel1.Font = New System.Drawing.Font("Times New Roman", 12.0!, System.Drawing.FontStyle.Bold)
            Me.XrLabel1.ForeColor = System.Drawing.Color.Teal
            Me.XrLabel1.Location = New System.Drawing.Point(0, 17)
            Me.XrLabel1.Name = "XrLabel1"
            Me.XrLabel1.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel1.Size = New System.Drawing.Size(800, 25)
            Me.XrLabel1.StylePriority.UseFont = False
            Me.XrLabel1.StylePriority.UseForeColor = False
            Me.XrLabel1.StylePriority.UseTextAlignment = False
            Me.XrLabel1.Text = "CREDITOR REFUNDS, VOIDS, REJECTED PAYMENTS"
            Me.XrLabel1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            '
            'XrControlStyle1
            '
            Me.XrControlStyle1.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.XrControlStyle1.ForeColor = System.Drawing.Color.White
            Me.XrControlStyle1.Name = "XrControlStyle1"
            Me.XrControlStyle1.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
            '
            'XrLabel_group_amount
            '
            Me.XrLabel_group_amount.Location = New System.Drawing.Point(300, 0)
            Me.XrLabel_group_amount.Name = "XrLabel_group_amount"
            Me.XrLabel_group_amount.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_group_amount.Size = New System.Drawing.Size(100, 17)
            Me.XrLabel_group_amount.StylePriority.UseTextAlignment = False
            XrSummary1.FormatString = "{0:c}"
            XrSummary1.Running = DevExpress.XtraReports.UI.SummaryRunning.Group
            Me.XrLabel_group_amount.Summary = XrSummary1
            Me.XrLabel_group_amount.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel_group_type
            '
            Me.XrLabel_group_type.Location = New System.Drawing.Point(0, 0)
            Me.XrLabel_group_type.Name = "XrLabel_group_type"
            Me.XrLabel_group_type.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_group_type.Size = New System.Drawing.Size(300, 17)
            '
            'XrLabel4
            '
            Me.XrLabel4.Location = New System.Drawing.Point(0, 17)
            Me.XrLabel4.Name = "XrLabel4"
            Me.XrLabel4.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel4.Size = New System.Drawing.Size(290, 17)
            Me.XrLabel4.Text = "TOTALS"
            '
            'XrLabel_report_amount
            '
            Me.XrLabel_report_amount.Location = New System.Drawing.Point(300, 17)
            Me.XrLabel_report_amount.Name = "XrLabel_report_amount"
            Me.XrLabel_report_amount.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_report_amount.Size = New System.Drawing.Size(100, 17)
            Me.XrLabel_report_amount.StylePriority.UseTextAlignment = False
            XrSummary2.FormatString = "{0:c}"
            XrSummary2.Running = DevExpress.XtraReports.UI.SummaryRunning.Report
            Me.XrLabel_report_amount.Summary = XrSummary2
            Me.XrLabel_report_amount.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLine1
            '
            Me.XrLine1.Location = New System.Drawing.Point(0, 0)
            Me.XrLine1.Name = "XrLine1"
            Me.XrLine1.Size = New System.Drawing.Size(800, 17)
            '
            'Subreport_RETURNS
            '
            Me.Bands.AddRange(New DevExpress.XtraReports.UI.Band() {Me.Detail, Me.PageHeader, Me.GroupHeader1, Me.GroupFooter1, Me.ReportFooter})
            Me.Margins = New System.Drawing.Printing.Margins(25, 25, 25, 25)
            Me.StyleSheet.AddRange(New DevExpress.XtraReports.UI.XRControlStyle() {Me.XrControlStyle1})
            Me.Version = "8.2"
            CType(Me, System.ComponentModel.ISupportInitialize).EndInit()

        End Sub
        Protected Friend WithEvents Detail As DevExpress.XtraReports.UI.DetailBand
        Protected Friend WithEvents PageHeader As DevExpress.XtraReports.UI.PageHeaderBand
        Protected Friend WithEvents GroupHeader1 As DevExpress.XtraReports.UI.GroupHeaderBand
        Protected Friend WithEvents GroupFooter1 As DevExpress.XtraReports.UI.GroupFooterBand
        Protected Friend WithEvents ReportFooter As DevExpress.XtraReports.UI.ReportFooterBand
        Protected Friend WithEvents XrLabel1 As DevExpress.XtraReports.UI.XRLabel
        Protected Friend WithEvents XrPanel1 As DevExpress.XtraReports.UI.XRPanel
        Protected Friend WithEvents XrLabel7 As DevExpress.XtraReports.UI.XRLabel
        Protected Friend WithEvents XrLabel6 As DevExpress.XtraReports.UI.XRLabel
        Protected Friend WithEvents XrControlStyle1 As DevExpress.XtraReports.UI.XRControlStyle
        Protected Friend WithEvents XrLabel_group_type As DevExpress.XtraReports.UI.XRLabel
        Protected Friend WithEvents XrLabel_group_amount As DevExpress.XtraReports.UI.XRLabel
        Protected Friend WithEvents XrLine1 As DevExpress.XtraReports.UI.XRLine
        Protected Friend WithEvents XrLabel_report_amount As DevExpress.XtraReports.UI.XRLabel
        Protected Friend WithEvents XrLabel4 As DevExpress.XtraReports.UI.XRLabel
    End Class
End Namespace