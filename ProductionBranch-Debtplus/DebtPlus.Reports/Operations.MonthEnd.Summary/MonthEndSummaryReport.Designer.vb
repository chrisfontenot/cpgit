Namespace Operations.MonthEnd.Summary

    <Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
    Partial Class MonthEndSummaryReport
        Inherits TemplateReport

        'XtraReport overrides dispose to clean up the component list.
        <System.Diagnostics.DebuggerNonUserCode()> _
        Protected Overrides Sub Dispose(ByVal disposing As Boolean)
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
            MyBase.Dispose(disposing)
        End Sub

        'Required by the Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Designer
        'It can be modified using the Designer.  
        'Do not modify it using the code editor.
        <System.Diagnostics.DebuggerStepThrough()> _
        Private Sub InitializeComponent()
            Me.XrSubreport_PAYMENTS = New DevExpress.XtraReports.UI.XRSubreport
            Me.XrSubreport_RETURNS = New DevExpress.XtraReports.UI.XRSubreport
            Me.XrSubreport_OPERATOR = New DevExpress.XtraReports.UI.XRSubreport
            Me.XrSubreport_DEPOSITS = New DevExpress.XtraReports.UI.XRSubreport
            Me.SubReport_PAYMENTS1 = New DebtPlus.Reports.Operations.MonthEnd.Summary.SubReport_PAYMENTS
            Me.Subreport_RETURNS1 = New DebtPlus.Reports.Operations.MonthEnd.Summary.Subreport_RETURNS
            Me.SubReport_OPERATOR1 = New DebtPlus.Reports.Operations.MonthEnd.Summary.SubReport_OPERATOR
            Me.SubReport_DEPOSITS1 = New DebtPlus.Reports.Operations.MonthEnd.Summary.SubReport_DEPOSITS
            CType(Me.SubReport_PAYMENTS1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.Subreport_RETURNS1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.SubReport_OPERATOR1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.SubReport_DEPOSITS1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
            '
            'Detail
            '
            Me.Detail.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrSubreport_DEPOSITS, Me.XrSubreport_OPERATOR, Me.XrSubreport_RETURNS, Me.XrSubreport_PAYMENTS})
            Me.Detail.Height = 100
            '
            'XrSubreport_PAYMENTS
            '
            Me.XrSubreport_PAYMENTS.Location = New System.Drawing.Point(0, 0)
            Me.XrSubreport_PAYMENTS.Name = "XrSubreport_PAYMENTS"
            Me.XrSubreport_PAYMENTS.ReportSource = Me.SubReport_PAYMENTS1
            Me.XrSubreport_PAYMENTS.Size = New System.Drawing.Size(800, 25)
            '
            'XrSubreport_RETURNS
            '
            Me.XrSubreport_RETURNS.Location = New System.Drawing.Point(0, 25)
            Me.XrSubreport_RETURNS.Name = "XrSubreport_RETURNS"
            Me.XrSubreport_RETURNS.ReportSource = Me.Subreport_RETURNS1
            Me.XrSubreport_RETURNS.Size = New System.Drawing.Size(800, 25)
            '
            'XrSubreport_OPERATOR
            '
            Me.XrSubreport_OPERATOR.Location = New System.Drawing.Point(0, 50)
            Me.XrSubreport_OPERATOR.Name = "XrSubreport_OPERATOR"
            Me.XrSubreport_OPERATOR.ReportSource = Me.SubReport_OPERATOR1
            Me.XrSubreport_OPERATOR.Size = New System.Drawing.Size(800, 25)
            '
            'XrSubreport_DEPOSITS
            '
            Me.XrSubreport_DEPOSITS.Location = New System.Drawing.Point(0, 75)
            Me.XrSubreport_DEPOSITS.Name = "XrSubreport_DEPOSITS"
            Me.XrSubreport_DEPOSITS.ReportSource = Me.SubReport_DEPOSITS1
            Me.XrSubreport_DEPOSITS.Size = New System.Drawing.Size(800, 25)
            '
            'MonthEndSummaryReport
            '
            Me.Version = "8.2"
            CType(Me.SubReport_PAYMENTS1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.Subreport_RETURNS1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.SubReport_OPERATOR1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.SubReport_DEPOSITS1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me, System.ComponentModel.ISupportInitialize).EndInit()

        End Sub
        Friend WithEvents XrSubreport_DEPOSITS As DevExpress.XtraReports.UI.XRSubreport
        Private WithEvents SubReport_DEPOSITS1 As DebtPlus.Reports.Operations.MonthEnd.Summary.SubReport_DEPOSITS
        Friend WithEvents XrSubreport_OPERATOR As DevExpress.XtraReports.UI.XRSubreport
        Private WithEvents SubReport_OPERATOR1 As DebtPlus.Reports.Operations.MonthEnd.Summary.SubReport_OPERATOR
        Friend WithEvents XrSubreport_RETURNS As DevExpress.XtraReports.UI.XRSubreport
        Private WithEvents Subreport_RETURNS1 As DebtPlus.Reports.Operations.MonthEnd.Summary.Subreport_RETURNS
        Friend WithEvents XrSubreport_PAYMENTS As DevExpress.XtraReports.UI.XRSubreport
        Private WithEvents SubReport_PAYMENTS1 As DebtPlus.Reports.Operations.MonthEnd.Summary.SubReport_PAYMENTS
    End Class
End Namespace