#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}
#End Region

Namespace Operations.MonthEnd.Summary
    Friend Class SubReport_DEPOSITS

        Protected Friend Overrides Sub SetSubreportParameters(ByVal vue As System.Data.DataView)
            MyBase.SetSubreportParameters(vue)

            ' Group summary
            With XrLabel_group_type
                .DataBindings.Add("Text", vue, "type")
                .DataBindings.Add("Tag", vue, "tran_type")
                AddHandler .PreviewClick, AddressOf FieldPreviewClick
            End With

            With XrLabel_group_amount
                .DataBindings.Add("Text", vue, "amount")
                .DataBindings.Add("Tag", vue, "tran_type")
                AddHandler .PreviewClick, AddressOf FieldPreviewClick
            End With

            ' Report summary
            With XrLabel_report_amount
                .DataBindings.Add("Text", vue, "amount")
            End With
        End Sub

        Private WithEvents DetailSubReport As SubReport_DEPOSITS_Detail = Nothing
        Dim key As String = String.Empty
        Protected Overrides Sub FieldPreviewClick(ByVal sender As Object, ByVal e As DevExpress.XtraReports.UI.PreviewMouseEventArgs)

            ' Find the sub-key for the row clicked
            key = Convert.ToString(e.Brick.Value)

            With New DetailReport()
                .ReportSubTitle = SavedSubitlteString
                DetailSubReport = New SubReport_DEPOSITS_Detail

                With .XrSubreport_Details
                    AddHandler .BeforePrint, AddressOf DetailsBeforePrint
                    .ReportSource = DetailSubReport
                End With

                .CreateDocument()
                .DisplayPreviewDialog()
                .Dispose()
            End With
        End Sub

        Private Sub DetailsBeforePrint(ByVal Sender As Object, ByVal e As System.EventArgs)
            Dim vue As System.Data.DataView = CType(Me.DataSource, System.Data.DataView)

            Dim DetailView As New System.Data.DataView(vue.Table, String.Format("[tran_type]='{0}'", key), vue.Sort, vue.RowStateFilter)
            DetailSubReport.SetSubreportParameters(DetailView)
        End Sub
    End Class
End Namespace