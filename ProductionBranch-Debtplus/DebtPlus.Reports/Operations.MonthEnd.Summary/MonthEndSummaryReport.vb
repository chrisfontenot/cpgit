#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}

#End Region

Imports System.ComponentModel

Imports System.Drawing.Printing
Imports System.Data.SqlClient

Namespace Operations.MonthEnd.Summary
    Public Class MonthEndSummaryReport

        Public Sub New()
            MyBase.New()
            InitializeComponent()
            AddHandler XrSubreport_PAYMENTS.BeforePrint, AddressOf XrSubreport_PAYMENTS_BeforePrint
            AddHandler XrSubreport_RETURNS.BeforePrint, AddressOf XrSubReport_RETURNS_BeforePrint
            AddHandler XrSubreport_OPERATOR.BeforePrint, AddressOf XrSubReport_OPERATOR_BeforePrint
            AddHandler XrSubreport_DEPOSITS.BeforePrint, AddressOf XrSubReport_DEPOSITS_BeforePrint

            ' Set the parameter values accordingly
            Parameter_FromDate = DebtPlus.Reports.Operations.MonthEnd.Summary.Storage.Parameter_FromDate
            Parameter_ToDate = DebtPlus.Reports.Operations.MonthEnd.Summary.Storage.Parameter_ToDate
        End Sub

        Private Sub XrSubreport_PAYMENTS_BeforePrint(ByVal sender As Object, ByVal e As PrintEventArgs)
            Dim tbl As DataTable = ds.Tables("rpt_MonthEndSummary_Payments")
            If tbl Is Nothing Then
                Dim cmd As SqlCommand = New System.Data.SqlClient.SqlCommand()
                With cmd
                    .Connection = New System.Data.SqlClient.SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
                    .CommandText = "rpt_MonthEndSummary_Payments"
                    .CommandType = CommandType.StoredProcedure
                    .Parameters.Add("@FromDate", SqlDbType.DateTime).Value = Parameter_FromDate
                    .Parameters.Add("@ToDate", SqlDbType.DateTime).Value = Parameter_ToDate
                    .CommandTimeout = 0
                End With

                Dim da As New SqlDataAdapter(cmd)
                da.Fill(ds, "rpt_MonthEndSummary_Payments")
                tbl = ds.Tables("rpt_MonthEndSummary_Payments")
            End If
            Dim vue As New DataView(tbl, String.Empty, "tran_type", DataViewRowState.CurrentRows)

            If vue.Count > 0 Then
                SubReport_PAYMENTS1.SetSubreportParameters(vue)
            Else
                e.Cancel = True
            End If
        End Sub

        Private Sub XrSubReport_RETURNS_BeforePrint(ByVal sender As Object, ByVal e As PrintEventArgs)
            Dim tbl As DataTable = ds.Tables("rpt_MonthEndSummary_Returns")
            If tbl Is Nothing Then
                Dim cmd As SqlCommand = New System.Data.SqlClient.SqlCommand()
                With cmd
                    .Connection = New System.Data.SqlClient.SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
                    .CommandText = "rpt_MonthEndSummary_Returns"
                    .CommandType = CommandType.StoredProcedure
                    .Parameters.Add("@FromDate", SqlDbType.DateTime).Value = Parameter_FromDate
                    .Parameters.Add("@ToDate", SqlDbType.DateTime).Value = Parameter_ToDate
                    .CommandTimeout = 0
                End With

                Dim da As New SqlDataAdapter(cmd)
                da.Fill(ds, "rpt_MonthEndSummary_Returns")
                tbl = ds.Tables("rpt_MonthEndSummary_Returns")
            End If
            Dim vue As New DataView(tbl, String.Empty, "tran_type", DataViewRowState.CurrentRows)

            If vue.Count > 0 Then
                Subreport_RETURNS1.SetSubreportParameters(vue)
            Else
                e.Cancel = True
            End If
        End Sub

        Private Sub XrSubReport_OPERATOR_BeforePrint(ByVal sender As Object, ByVal e As PrintEventArgs)
            Dim tbl As DataTable = ds.Tables("rpt_MonthEndSummary_Operator")
            If tbl Is Nothing Then
                Dim cmd As SqlCommand = New System.Data.SqlClient.SqlCommand()
                With cmd
                    .Connection = New System.Data.SqlClient.SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
                    .CommandText = "rpt_MonthEndSummary_Operator"
                    .CommandType = CommandType.StoredProcedure
                    .Parameters.Add("@FromDate", SqlDbType.DateTime).Value = Parameter_FromDate
                    .Parameters.Add("@ToDate", SqlDbType.DateTime).Value = Parameter_ToDate
                    .CommandTimeout = 0
                End With

                Dim da As New SqlDataAdapter(cmd)
                da.Fill(ds, "rpt_MonthEndSummary_Operator")
                tbl = ds.Tables("rpt_MonthEndSummary_Operator")
            End If
            Dim vue As New DataView(tbl, String.Empty, "counselor", DataViewRowState.CurrentRows)

            If vue.Count > 0 Then
                SubReport_OPERATOR1.SetSubreportParameters(vue)
            Else
                e.Cancel = True
            End If
        End Sub

        Private Sub XrSubReport_DEPOSITS_BeforePrint(ByVal sender As Object, ByVal e As PrintEventArgs)
            Dim tbl As DataTable = ds.Tables("rpt_MonthEndSummary_Deposits")
            If tbl Is Nothing Then
                Dim cmd As SqlCommand = New System.Data.SqlClient.SqlCommand()
                With cmd
                    .Connection = New System.Data.SqlClient.SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
                    .CommandText = "rpt_MonthEndSummary_Deposits"
                    .CommandType = CommandType.StoredProcedure
                    .Parameters.Add("@FromDate", SqlDbType.DateTime).Value = Parameter_FromDate
                    .Parameters.Add("@ToDate", SqlDbType.DateTime).Value = Parameter_ToDate
                    .CommandTimeout = 0
                End With

                Dim da As New SqlDataAdapter(cmd)
                da.Fill(ds, "rpt_MonthEndSummary_Deposits")
                tbl = ds.Tables("rpt_MonthEndSummary_Deposits")
            End If
            Dim vue As New DataView(tbl, String.Empty, "tran_type", DataViewRowState.CurrentRows)

            If vue.Count > 0 Then
                SubReport_DEPOSITS1.SetSubreportParameters(vue)
            Else
                e.Cancel = True
            End If
        End Sub

        ''' <summary>
        ''' Hook into the logic where the parameters are defined to snag a copy
        ''' </summary>
        Public Overrides Function RequestReportParameters() As DialogResult
            Return DialogResult.OK
        End Function
    End Class
End Namespace
