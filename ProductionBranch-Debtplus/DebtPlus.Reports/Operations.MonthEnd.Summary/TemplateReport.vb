#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}
#End Region

Namespace Operations.MonthEnd.Summary

    Public Class TemplateReport

        Public Sub New()
            MyBase.New()
            InitializeComponent()
        End Sub

        Public Overrides ReadOnly Property ReportTitle() As String
            Get
                Return "Monthly Summary"
            End Get
        End Property

        Public Overrides ReadOnly Property ReportSubtitle() As String
            Get
                Return String.Format("This report covers dates from {0:d} through {1:d}", Storage.Parameter_FromDate, Storage.Parameter_ToDate)
            End Get
        End Property
    End Class

    Friend Module Storage
        Friend ds As New System.Data.DataSet("ds")

        Friend ReadOnly Property Parameter_FromDate As DateTime
            Get
                Dim CurrentDate As DateTime = DateTime.Now.Date.AddDays(-15)
                Return New DateTime(CurrentDate.Year, CurrentDate.Month, 1)
            End Get
        End Property

        Friend ReadOnly Property Parameter_ToDate As DateTime
            Get
                Return Parameter_FromDate.AddMonths(1).AddDays(-1)
            End Get
        End Property
    End Module
End Namespace