Namespace Operations.MonthEnd.Summary

    <Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
    Partial Class DetailReport
        Inherits TemplateReport

        'XtraReport overrides dispose to clean up the component list.
        <System.Diagnostics.DebuggerNonUserCode()> _
        Protected Overrides Sub Dispose(ByVal disposing As Boolean)
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
            MyBase.Dispose(disposing)
        End Sub

        'Required by the Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Designer
        'It can be modified using the Designer.  
        'Do not modify it using the code editor.
        <System.Diagnostics.DebuggerStepThrough()> _
        Private Sub InitializeComponent()
            Me.XrSubreport_Details = New DevExpress.XtraReports.UI.XRSubreport
            CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
            '
            'Detail
            '
            Me.Detail.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrSubreport_Details})
            Me.Detail.Height = 25
            '
            'XrSubreport_Details
            '
            Me.XrSubreport_Details.Location = New System.Drawing.Point(0, 0)
            Me.XrSubreport_Details.Name = "XrSubreport_Details"
            Me.XrSubreport_Details.Size = New System.Drawing.Size(800, 25)
            '
            'DetailReport
            '
            Me.Version = "8.2"
            CType(Me, System.ComponentModel.ISupportInitialize).EndInit()

        End Sub
        Protected Friend WithEvents XrSubreport_Details As DevExpress.XtraReports.UI.XRSubreport
    End Class
End Namespace
