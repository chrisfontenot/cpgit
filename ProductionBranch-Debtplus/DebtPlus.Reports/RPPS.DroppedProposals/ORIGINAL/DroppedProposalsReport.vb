#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}
#End Region

Namespace RPPS.DroppedProposals
    Public Class DroppedProposalsReport

        ''' <summary>
        ''' Create an instance of our report
        ''' </summary>
        Public Sub New()
            MyBase.New()
            InitializeComponent()
        End Sub

        Public Overrides ReadOnly Property ReportSubTitle() As String
            Get
                Return "This report shows items that can not be sent at this time"
            End Get
        End Property

        Public Shared Function CreateReport(ByVal sqlInfo As DebtPlus.LINQ.SQLInfoClass) As DebtPlus.Interfaces.Reports.IReports
            Dim rpt As DroppedProposalsReport = Nothing
            Dim tbl As System.Data.DataTable = CreateTable(sqlInfo)
            If tbl IsNot Nothing AndAlso tbl.Rows.Count > 0 Then
                rpt = New DroppedProposalsReport()
                rpt.DataSource = tbl.DefaultView
            End If

            Return rpt
        End Function

        Private Shared Function CreateTable(ByVal sqlInfo As DebtPlus.LINQ.SQLInfoClass) As System.Data.DataTable
            Dim ds As New System.Data.DataSet("ds")
            Dim tbl As System.Data.DataTable = Nothing

            Dim cmd As SqlClient.SqlCommand = New System.Data.SqlClient.SqlCommand()
            With cmd
                .Connection = New System.Data.SqlClient.SqlConnection(SqlInfo.ConnectionString)
                .CommandText = "rpt_invalid_proposals"
                .CommandType = CommandType.StoredProcedure
                .CommandTimeout = 0
            End With

            Try
                Dim da As New SqlClient.SqlDataAdapter(cmd)
                da.Fill(ds, "rpt_invalid_proposals")
                tbl = ds.Tables("rpt_invalid_proposals")

            Catch ex As SqlClient.SqlException
                Using gdr As New DebtPlus.Repository.GetDataResult()
                    gdr.HandleException(ex)
                    DebtPlus.UI.Common.ErrorHandling.HandleErrors(gdr, "Error reading from rpt_invalid_proposals")
                End Using
            End Try

            Return tbl
        End Function

        ''' <summary>
        ''' Title of the report
        ''' </summary>
        Public Overrides ReadOnly Property ReportTitle() As String
            Get
                Return "Invalid Proposals"
            End Get
        End Property
    End Class
End Namespace
