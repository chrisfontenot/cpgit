﻿Namespace Custom.Letter.Followup.Month6
    Friend Class SendEnglish
        Protected tbl As DataTable

        Public Sub New(ByVal tbl As DataTable)
            Me.tbl = tbl
        End Sub

        Public Overridable Sub SendMessages()

            Dim templateRecord As DebtPlus.LINQ.email_template = Nothing
            Using bc As New DebtPlus.LINQ.BusinessContext()

                ' Find the English language ID
                Dim languageRecord As DebtPlus.LINQ.AttributeType = DebtPlus.LINQ.Cache.AttributeType.getLanguageList().Find(Function(s) String.Compare(s.LanguageID, "en-US", True) = 0)
                If languageRecord Is Nothing Then
                    Return
                End If

                ' Get the template for the English record
                templateRecord = bc.email_templates.Where(Function(s) s.language = languageRecord.Id AndAlso s.description = "6 Month Letter").FirstOrDefault()
                If templateRecord Is Nothing Then
                    Return
                End If

                ' Process the English records from the table list.
                Using vue As New System.Data.DataView(tbl, "([language] not like 'es-%') AND (isnull(client_email,'') <> '')", "client_email", DataViewRowState.CurrentRows)
                    For Each drv As System.Data.DataRowView In vue
                        Dim queueRecord As DebtPlus.LINQ.email_queue = DebtPlus.LINQ.Factory.Manufacture_email_queue()
                        queueRecord.email_address = DebtPlus.Utils.Nulls.v_String(drv("client_email"))
                        queueRecord.email_name = DebtPlus.Utils.Nulls.v_String(drv("client_email"))
                        queueRecord.template = templateRecord.Id
                        queueRecord.substitutions = generateSubstitutions(drv)

                        ' Insert the new record when completed
                        bc.email_queues.InsertOnSubmit(queueRecord)
                    Next
                End Using

                Try
                    ' Submit the changes to record the transactions
                    bc.SubmitChanges()
                Catch ex As System.Data.SqlClient.SqlException
                    DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error submitting changes to English email queue")
                End Try
            End Using
        End Sub

        Protected Function generateSubstitutions(ByVal drv As System.Data.DataRowView) As System.Xml.Linq.XElement
            Dim sb As New System.Text.StringBuilder()

            sb.Append("<substitutions>")
            sb.AppendFormat("<substitution><field>client</field><value>{0:0000000}</value></substitution>", DebtPlus.Utils.Nulls.v_Int32(drv("client")).GetValueOrDefault())
            sb.AppendFormat("<substitution><field>client_name</field><value>{0}</value></substitution>", toXML(DebtPlus.Utils.Nulls.v_String(drv("client_name"))))
            sb.Append("</substitutions>")

            Return System.Xml.Linq.XElement.Parse(sb.ToString())
        End Function

        Protected Function toXML(ByVal input As String) As String

            ' An empty string
            If input Is Nothing Then
                Return String.Empty
            End If

            ' Make the substitutions that are common in the strings.
            input = input.Replace("&", "&amp;")
            input = input.Replace("<", "&lt;")
            input = input.Replace(">", "&gt;")

            Return input
        End Function
    End Class
End Namespace
