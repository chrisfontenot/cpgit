﻿Namespace Custom.Letter.Followup.Month6
    Friend Class SendSpanish
        Inherits SendEnglish

        Public Sub New(ByVal tbl As DataTable)
            MyBase.New(tbl)
        End Sub

        Public Overrides Sub SendMessages()
            Dim templateRecord As DebtPlus.LINQ.email_template = Nothing
            Using bc As New DebtPlus.LINQ.BusinessContext()

                ' Find the English language ID
                Dim languageRecord As DebtPlus.LINQ.AttributeType = DebtPlus.LINQ.Cache.AttributeType.getLanguageList().Find(Function(s) String.Compare(s.LanguageID, "es-MX", True) = 0)
                If languageRecord Is Nothing Then
                    Return
                End If

                ' Get the template for the English record
                templateRecord = bc.email_templates.Where(Function(s) s.language = languageRecord.Id AndAlso s.description = "6 Month Letter").FirstOrDefault()
                If templateRecord Is Nothing Then
                    Return
                End If

                ' Process the English records from the table list.
                Using vue As New System.Data.DataView(tbl, "([language] like 'es-%') AND (isnull(client_email,'') <> '')", "client_email", DataViewRowState.CurrentRows)
                    For Each drv As System.Data.DataRowView In vue
                        Dim queueRecord As DebtPlus.LINQ.email_queue = DebtPlus.LINQ.Factory.Manufacture_email_queue()
                        queueRecord.email_address = DebtPlus.Utils.Nulls.v_String(drv("client_email"))
                        queueRecord.email_name = DebtPlus.Utils.Nulls.v_String(drv("client_email"))
                        queueRecord.template = templateRecord.Id
                        queueRecord.substitutions = generateSubstitutions(drv)

                        ' Insert the new record when completed
                        bc.email_queues.InsertOnSubmit(queueRecord)
                    Next
                End Using

                Try
                    ' Submit the changes to record the transactions
                    bc.SubmitChanges()
                Catch ex As System.Data.SqlClient.SqlException
                    DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error submitting changes to Spanish email queue")
                End Try
            End Using
        End Sub
    End Class
End Namespace