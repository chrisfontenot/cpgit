Namespace Custom.Letter.Followup.Month6
    <Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
    Partial Public Class FollowupLetterReport
        Inherits DebtPlus.Reports.Template.BaseXtraReportClass

        'XtraReport overrides dispose to clean up the component list.
        <System.Diagnostics.DebuggerNonUserCode()> _
        Protected Overrides Sub Dispose(ByVal disposing As Boolean)
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
            MyBase.Dispose(disposing)
        End Sub

        'Required by the Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Designer
        'It can be modified using the Designer.  
        'Do not modify it using the code editor.
        <System.Diagnostics.DebuggerStepThrough()> _
        Private Sub InitializeComponent()
            Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FollowupLetterReport))
            Me.XrRichText_ES = New DevExpress.XtraReports.UI.XRRichText()
            Me.XrTable1 = New DevExpress.XtraReports.UI.XRTable()
            Me.XrTableRow1 = New DevExpress.XtraReports.UI.XRTableRow()
            Me.XrTableCell1 = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableCell_office_name = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableRow2 = New DevExpress.XtraReports.UI.XRTableRow()
            Me.XrTableCell2 = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableCell_office_address = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableRow5 = New DevExpress.XtraReports.UI.XRTableRow()
            Me.XrTableCell9 = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableCell_office_phone = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableRow3 = New DevExpress.XtraReports.UI.XRTableRow()
            Me.XrTableCell5 = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableCell6 = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrPageInfo1 = New DevExpress.XtraReports.UI.XRPageInfo()
            Me.XrTableRow4 = New DevExpress.XtraReports.UI.XRTableRow()
            Me.XrTableCell7 = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableCell_client = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrLabel_client_name_and_address = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrRichText_EN = New DevExpress.XtraReports.UI.XRRichText()
            Me.XrPictureBox5 = New DevExpress.XtraReports.UI.XRPictureBox()
            Me.XrLabel2 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrPictureBox4 = New DevExpress.XtraReports.UI.XRPictureBox()
            Me.XrPictureBox3 = New DevExpress.XtraReports.UI.XRPictureBox()
            Me.XrPictureBox2 = New DevExpress.XtraReports.UI.XRPictureBox()
            Me.XrPictureBox1 = New DevExpress.XtraReports.UI.XRPictureBox()
            Me.client_name_and_address = New DevExpress.XtraReports.UI.CalculatedField()
            Me.PageFooter = New DevExpress.XtraReports.UI.PageFooterBand()
            CType(Me.XrRichText_ES, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.XrTable1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.XrRichText_EN, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
            '
            'Detail
            '
            Me.Detail.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrTable1, Me.XrLabel_client_name_and_address, Me.XrRichText_EN, Me.XrPictureBox5, Me.XrRichText_ES})
            Me.Detail.HeightF = 900.0!
            Me.Detail.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
            Me.Detail.PageBreak = DevExpress.XtraReports.UI.PageBreak.AfterBand
            Me.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
            '
            'XrRichText_ES
            '
            Me.XrRichText_ES.LocationFloat = New DevExpress.Utils.PointFloat(10.0!, 296.0!)
            Me.XrRichText_ES.Name = "XrRichText_ES"
            Me.XrRichText_ES.Scripts.OnBeforePrint = "XrRichText_ES_BeforePrint"
            Me.XrRichText_ES.SerializableRtfString = resources.GetString("XrRichText_ES.SerializableRtfString")
            Me.XrRichText_ES.SizeF = New System.Drawing.SizeF(780.0!, 594.0!)
            '
            'XrTable1
            '
            Me.XrTable1.LocationFloat = New DevExpress.Utils.PointFloat(398.9583!, 10.00001!)
            Me.XrTable1.Name = "XrTable1"
            Me.XrTable1.Rows.AddRange(New DevExpress.XtraReports.UI.XRTableRow() {Me.XrTableRow1, Me.XrTableRow2, Me.XrTableRow5, Me.XrTableRow3, Me.XrTableRow4})
            Me.XrTable1.SizeF = New System.Drawing.SizeF(350.0!, 98.95834!)
            '
            'XrTableRow1
            '
            Me.XrTableRow1.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.XrTableCell1, Me.XrTableCell_office_name})
            Me.XrTableRow1.Name = "XrTableRow1"
            Me.XrTableRow1.Weight = 1.0R
            '
            'XrTableCell1
            '
            Me.XrTableCell1.Name = "XrTableCell1"
            Me.XrTableCell1.Text = "Office"
            Me.XrTableCell1.Weight = 0.88392850525525724R
            '
            'XrTableCell_office_name
            '
            Me.XrTableCell_office_name.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "office_name")})
            Me.XrTableCell_office_name.Name = "XrTableCell_office_name"
            Me.XrTableCell_office_name.Weight = 2.1160714947447428R
            '
            'XrTableRow2
            '
            Me.XrTableRow2.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.XrTableCell2, Me.XrTableCell_office_address})
            Me.XrTableRow2.Name = "XrTableRow2"
            Me.XrTableRow2.Weight = 1.0R
            '
            'XrTableCell2
            '
            Me.XrTableCell2.Name = "XrTableCell2"
            Me.XrTableCell2.Text = "Address"
            Me.XrTableCell2.Weight = 0.88392850525525724R
            '
            'XrTableCell_office_address
            '
            Me.XrTableCell_office_address.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "office_address")})
            Me.XrTableCell_office_address.Multiline = True
            Me.XrTableCell_office_address.Name = "XrTableCell_office_address"
            Me.XrTableCell_office_address.Weight = 2.1160714947447428R
            '
            'XrTableRow5
            '
            Me.XrTableRow5.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.XrTableCell9, Me.XrTableCell_office_phone})
            Me.XrTableRow5.Name = "XrTableRow5"
            Me.XrTableRow5.Weight = 1.0R
            '
            'XrTableCell9
            '
            Me.XrTableCell9.Name = "XrTableCell9"
            Me.XrTableCell9.Text = "Phone"
            Me.XrTableCell9.Weight = 0.88392850525525724R
            '
            'XrTableCell_office_phone
            '
            Me.XrTableCell_office_phone.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "office_phone")})
            Me.XrTableCell_office_phone.Name = "XrTableCell_office_phone"
            Me.XrTableCell_office_phone.Weight = 2.1160714947447428R
            '
            'XrTableRow3
            '
            Me.XrTableRow3.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.XrTableCell5, Me.XrTableCell6})
            Me.XrTableRow3.Name = "XrTableRow3"
            Me.XrTableRow3.Weight = 1.0R
            '
            'XrTableCell5
            '
            Me.XrTableCell5.Name = "XrTableCell5"
            Me.XrTableCell5.Weight = 0.88392850525525724R
            '
            'XrTableCell6
            '
            Me.XrTableCell6.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrPageInfo1})
            Me.XrTableCell6.Name = "XrTableCell6"
            Me.XrTableCell6.Weight = 2.1160714947447428R
            '
            'XrPageInfo1
            '
            Me.XrPageInfo1.Format = "{0:D}"
            Me.XrPageInfo1.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.4166574!)
            Me.XrPageInfo1.Name = "XrPageInfo1"
            Me.XrPageInfo1.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrPageInfo1.PageInfo = DevExpress.XtraPrinting.PageInfo.DateTime
            Me.XrPageInfo1.SizeF = New System.Drawing.SizeF(240.0!, 17.79168!)
            '
            'XrTableRow4
            '
            Me.XrTableRow4.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.XrTableCell7, Me.XrTableCell_client})
            Me.XrTableRow4.Name = "XrTableRow4"
            Me.XrTableRow4.Weight = 1.0R
            '
            'XrTableCell7
            '
            Me.XrTableCell7.Name = "XrTableCell7"
            Me.XrTableCell7.Weight = 0.88392850525525724R
            '
            'XrTableCell_client
            '
            Me.XrTableCell_client.Name = "XrTableCell_client"
            Me.XrTableCell_client.Scripts.OnBeforePrint = "XrTableCell_client_BeforePrint"
            Me.XrTableCell_client.Weight = 2.1160714947447428R
            '
            'XrLabel_client_name_and_address
            '
            Me.XrLabel_client_name_and_address.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "client_name_and_address")})
            Me.XrLabel_client_name_and_address.LocationFloat = New DevExpress.Utils.PointFloat(10.00001!, 174.25!)
            Me.XrLabel_client_name_and_address.Multiline = True
            Me.XrLabel_client_name_and_address.Name = "XrLabel_client_name_and_address"
            Me.XrLabel_client_name_and_address.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_client_name_and_address.SizeF = New System.Drawing.SizeF(388.5417!, 62.58334!)
            '
            'XrRichText_EN
            '
            Me.XrRichText_EN.LocationFloat = New DevExpress.Utils.PointFloat(10.0!, 296.0!)
            Me.XrRichText_EN.Name = "XrRichText_EN"
            Me.XrRichText_EN.Scripts.OnBeforePrint = "XrRichText_EN_BeforePrint"
            Me.XrRichText_EN.SerializableRtfString = resources.GetString("XrRichText_EN.SerializableRtfString")
            Me.XrRichText_EN.SizeF = New System.Drawing.SizeF(780.0!, 594.0!)
            '
            'XrPictureBox5
            '
            Me.XrPictureBox5.Image = CType(resources.GetObject("XrPictureBox5.Image"), System.Drawing.Image)
            Me.XrPictureBox5.LocationFloat = New DevExpress.Utils.PointFloat(66.0!, 8.0!)
            Me.XrPictureBox5.Name = "XrPictureBox5"
            Me.XrPictureBox5.SizeF = New System.Drawing.SizeF(154.0!, 97.0!)
            Me.XrPictureBox5.Sizing = DevExpress.XtraPrinting.ImageSizeMode.ZoomImage
            '
            'XrLabel2
            '
            Me.XrLabel2.Font = New System.Drawing.Font("Garamond", 14.0!, System.Drawing.FontStyle.Bold)
            Me.XrLabel2.LocationFloat = New DevExpress.Utils.PointFloat(135.0833!, 4.0!)
            Me.XrLabel2.Name = "XrLabel2"
            Me.XrLabel2.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel2.SizeF = New System.Drawing.SizeF(466.0!, 66.0!)
            Me.XrLabel2.StylePriority.UseFont = False
            Me.XrLabel2.StylePriority.UseTextAlignment = False
            Me.XrLabel2.Text = "www.ClearPointCCS.org"
            Me.XrLabel2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
            '
            'XrPictureBox4
            '
            Me.XrPictureBox4.Image = CType(resources.GetObject("XrPictureBox4.Image"), System.Drawing.Image)
            Me.XrPictureBox4.LocationFloat = New DevExpress.Utils.PointFloat(601.0833!, 8.0!)
            Me.XrPictureBox4.Name = "XrPictureBox4"
            Me.XrPictureBox4.SizeF = New System.Drawing.SizeF(75.0!, 58.0!)
            Me.XrPictureBox4.Sizing = DevExpress.XtraPrinting.ImageSizeMode.ZoomImage
            '
            'XrPictureBox3
            '
            Me.XrPictureBox3.Image = CType(resources.GetObject("XrPictureBox3.Image"), System.Drawing.Image)
            Me.XrPictureBox3.LocationFloat = New DevExpress.Utils.PointFloat(693.0833!, 0.0!)
            Me.XrPictureBox3.Name = "XrPictureBox3"
            Me.XrPictureBox3.SizeF = New System.Drawing.SizeF(68.0!, 78.0!)
            Me.XrPictureBox3.Sizing = DevExpress.XtraPrinting.ImageSizeMode.ZoomImage
            '
            'XrPictureBox2
            '
            Me.XrPictureBox2.Image = CType(resources.GetObject("XrPictureBox2.Image"), System.Drawing.Image)
            Me.XrPictureBox2.LocationFloat = New DevExpress.Utils.PointFloat(10.00001!, 0.0!)
            Me.XrPictureBox2.Name = "XrPictureBox2"
            Me.XrPictureBox2.SizeF = New System.Drawing.SizeF(45.0!, 74.0!)
            Me.XrPictureBox2.Sizing = DevExpress.XtraPrinting.ImageSizeMode.ZoomImage
            '
            'XrPictureBox1
            '
            Me.XrPictureBox1.Image = CType(resources.GetObject("XrPictureBox1.Image"), System.Drawing.Image)
            Me.XrPictureBox1.LocationFloat = New DevExpress.Utils.PointFloat(66.0!, 0.0!)
            Me.XrPictureBox1.Name = "XrPictureBox1"
            Me.XrPictureBox1.SizeF = New System.Drawing.SizeF(58.0!, 78.0!)
            Me.XrPictureBox1.Sizing = DevExpress.XtraPrinting.ImageSizeMode.ZoomImage
            '
            'client_name_and_address
            '
            Me.client_name_and_address.FieldType = DevExpress.XtraReports.UI.FieldType.[String]
            Me.client_name_and_address.Name = "client_name_and_address"
            Me.client_name_and_address.Scripts.OnGetValue = "client_name_and_address_GetValue"
            '
            'PageFooter
            '
            Me.PageFooter.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrPictureBox2, Me.XrPictureBox1, Me.XrLabel2, Me.XrPictureBox4, Me.XrPictureBox3})
            Me.PageFooter.HeightF = 84.0!
            Me.PageFooter.Name = "PageFooter"
            '
            'FollowupLetterReport
            '
            Me.Bands.AddRange(New DevExpress.XtraReports.UI.Band() {Me.TopMarginBand1, Me.Detail, Me.BottomMarginBand1, Me.PageFooter})
            Me.CalculatedFields.AddRange(New DevExpress.XtraReports.UI.CalculatedField() {Me.client_name_and_address})
            Me.ReportPrintOptions.DetailCountOnEmptyDataSource = 0
            Me.ScriptLanguage = DevExpress.XtraReports.ScriptLanguage.VisualBasic
            Me.ScriptReferencesString = resources.GetString("$this.ScriptReferencesString")
            Me.ScriptsSource = resources.GetString("$this.ScriptsSource")
            Me.Version = "11.2"
            Me.Controls.SetChildIndex(Me.PageFooter, 0)
            Me.Controls.SetChildIndex(Me.BottomMarginBand1, 0)
            Me.Controls.SetChildIndex(Me.Detail, 0)
            Me.Controls.SetChildIndex(Me.TopMarginBand1, 0)
            CType(Me.XrRichText_ES, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.XrTable1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.XrRichText_EN, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me, System.ComponentModel.ISupportInitialize).EndInit()

        End Sub

        Friend WithEvents XrPictureBox1 As DevExpress.XtraReports.UI.XRPictureBox
        Friend WithEvents XrPictureBox5 As DevExpress.XtraReports.UI.XRPictureBox
        Friend WithEvents XrPictureBox4 As DevExpress.XtraReports.UI.XRPictureBox
        Friend WithEvents XrPictureBox3 As DevExpress.XtraReports.UI.XRPictureBox
        Friend WithEvents XrPictureBox2 As DevExpress.XtraReports.UI.XRPictureBox
        Friend WithEvents XrLabel_client_name_and_address As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrRichText_EN As DevExpress.XtraReports.UI.XRRichText
        Friend WithEvents client_name_and_address As DevExpress.XtraReports.UI.CalculatedField
        Friend WithEvents XrTable1 As DevExpress.XtraReports.UI.XRTable
        Friend WithEvents XrTableRow1 As DevExpress.XtraReports.UI.XRTableRow
        Friend WithEvents XrTableCell1 As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents XrTableCell_office_name As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents XrTableRow2 As DevExpress.XtraReports.UI.XRTableRow
        Friend WithEvents XrTableCell2 As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents XrTableCell_office_address As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents XrTableRow3 As DevExpress.XtraReports.UI.XRTableRow
        Friend WithEvents XrTableCell5 As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents XrPageInfo1 As DevExpress.XtraReports.UI.XRPageInfo
        Friend WithEvents XrTableCell6 As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents XrTableRow4 As DevExpress.XtraReports.UI.XRTableRow
        Friend WithEvents XrTableCell7 As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents XrTableCell_client As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents XrTableRow5 As DevExpress.XtraReports.UI.XRTableRow
        Friend WithEvents XrTableCell9 As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents XrTableCell_office_phone As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents XrLabel2 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents PageFooter As DevExpress.XtraReports.UI.PageFooterBand
        Friend WithEvents XrRichText_ES As DevExpress.XtraReports.UI.XRRichText
    End Class
End Namespace
