#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}
#End Region

Namespace Custom.Letter.Followup.Month6
    Public Class FollowupLetterReport

        Public Sub New()
            MyBase.New()
            InitializeComponent()
            AddHandler Me.BeforePrint, AddressOf FollowupLetterReport_BeforePrint
            'AddHandler client_name_and_address.GetValue, AddressOf client_name_and_address_GetValue
            'AddHandler XrTableCell_client.BeforePrint, AddressOf XrTableCell_client_BeforePrint
            'AddHandler XrRichText_EN.BeforePrint, AddressOf XrRichText_EN_BeforePrint
            'AddHandler XrRichText_ES.BeforePrint, AddressOf XrRichText_ES_BeforePrint
        End Sub

        Private Sub client_name_and_address_GetValue(ByVal sender As Object, ByVal e As DevExpress.XtraReports.UI.GetValueEventArgs)
            Dim rpt As DevExpress.XtraReports.UI.XtraReport = CType(e.Report, DevExpress.XtraReports.UI.XtraReport)
            Dim ClientName As String = DebtPlus.Utils.Nulls.DStr(rpt.GetCurrentColumnValue("client_name")).Trim
            Dim ClientAddress As String = DebtPlus.Utils.Nulls.DStr(rpt.GetCurrentColumnValue("client_address")).Trim
            If ClientName <> String.Empty AndAlso ClientAddress <> String.Empty Then
                e.Value = ClientName + Environment.NewLine + ClientAddress
            Else
                e.Value = ClientName + ClientAddress
            End If
        End Sub

        Private Sub FollowupLetterReport_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs)
            Dim ds As New System.Data.DataSet("ds")
            Dim rpt As DevExpress.XtraReports.UI.XtraReport = CType(sender, DevExpress.XtraReports.UI.XtraReport)
            Dim sqlInfo As DebtPlus.LINQ.SQLInfoClass = DebtPlus.LINQ.SQLInfoClass.getDefault()

            '-- Load the list of clients for the letter
            Using cmd As New System.Data.SqlClient.SqlCommand()
                With cmd
                    .Connection = New System.Data.SqlClient.SqlConnection(sqlInfo.ConnectionString)
                    .CommandText = "rpt_custom_6_month_letter"
                    .CommandType = System.Data.CommandType.StoredProcedure
                    .CommandTimeout = 0
                End With

                Using da As New System.Data.SqlClient.SqlDataAdapter(cmd)
                    da.Fill(ds, "rpt_custom_6_month_letter")
                End Using
            End Using

            Dim tbl As System.Data.DataTable = ds.Tables("rpt_custom_6_month_letter")

            '-- Create a thread to process the English messages. It must be a foreground thread so that it does not die when we do.
            Dim EnglishClass As New SendEnglish(tbl)
            Dim englishThrd As New System.Threading.Thread(New System.Threading.ThreadStart(AddressOf EnglishClass.SendMessages))
            englishThrd.SetApartmentState(Threading.ApartmentState.STA)
            englishThrd.IsBackground = False
            englishThrd.Start()

            '-- Create a thread to process the Spanish messages. It must be a foreground thread so that it does not die when we do.
            Dim SpanishClass As New SendSpanish(tbl)
            Dim spanishThrd As New System.Threading.Thread(New System.Threading.ThreadStart(AddressOf SpanishClass.SendMessages))
            spanishThrd.SetApartmentState(Threading.ApartmentState.STA)
            spanishThrd.IsBackground = False
            spanishThrd.Start()

            '-- Bind the report to the non-email items
            Dim vue As New System.Data.DataView(tbl, "[client_email]=''", "postalcode, client", DataViewRowState.CurrentRows)
            If vue.Count > 0 Then
                rpt.DataSource = vue
            Else
                e.Cancel = True
            End If
        End Sub

        Private Sub XrTableCell_client_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs)
            With CType(sender, DevExpress.XtraReports.UI.XRLabel)
                Dim rpt As DevExpress.XtraReports.UI.XtraReport = CType(.Report, DevExpress.XtraReports.UI.XtraReport)
                .Text = DebtPlus.Utils.Format.Client.FormatClientID(rpt.GetCurrentColumnValue("client"))
            End With
        End Sub

        Private Sub XrRichText_EN_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs)
            With CType(sender, DevExpress.XtraReports.UI.XRRichText)
                Dim rpt As DevExpress.XtraReports.UI.XtraReport = CType(.Report, DevExpress.XtraReports.UI.XtraReport)

                '-- Since this is the default, suppress any other valid language item
                Dim Language As String = DebtPlus.Utils.Nulls.DStr(rpt.GetCurrentColumnValue("language")).Trim.ToUpper
                e.Cancel = Language.StartsWith("ES")
            End With
        End Sub

        Private Sub XrRichText_ES_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs)
            With CType(sender, DevExpress.XtraReports.UI.XRRichText)
                Dim rpt As DevExpress.XtraReports.UI.XtraReport = CType(.Report, DevExpress.XtraReports.UI.XtraReport)

                '-- Include only if SPANISH
                Dim Language As String = DebtPlus.Utils.Nulls.DStr(rpt.GetCurrentColumnValue("language")).Trim.ToUpper
                e.Cancel = Not Language.StartsWith("ES")
            End With
        End Sub
    End Class
End Namespace
