#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}

#End Region

Option Compare Binary
Option Explicit On
Option Strict On

Imports System.ComponentModel
Imports DebtPlus.Reports.Template

Imports System.Drawing.Printing
Imports System.IO
Imports System.Reflection
Imports DebtPlus.Utils
Imports DevExpress.XtraReports.UI
Imports System.Data.SqlClient
Imports System.Threading
'Imports DebtPlus.Reports.Custom.Letter.Followup.Month6.Email.English
'Imports DebtPlus.Reports.Custom.Letter.Followup.Month6.Email.Spanish

Namespace Custom.Letter.Followup.Month6
    Public Class FollowupLetterReport
        Inherits BaseXtraReportClass

        Public Sub New()
            MyBase.New()

            Const ReportName As String = "DebtPlus.Reports.Custom.Letter.Followup.Month6.repx"

            ' See if there is a report reference that we can use
            Dim Fname As String = Path.Combine(DebtPlus.Configuration.Config.ReportsDirectory, ReportName)
            Dim UseDefault As Boolean = True
            Try
                If (Not DebtPlus.Configuration.Config.LocalReportsOnly) AndAlso System.IO.File.Exists(Fname) Then
                    LoadLayout(Fname)
                    UseDefault = False
                End If
            Catch ex As Exception
            End Try

            ' Load the standard report definition if we need a new item
            If UseDefault Then
                Dim asm As Assembly = Assembly.GetExecutingAssembly                                           ' changed
                Using ios As Stream = asm.GetManifestResourceStream(String.Format("DebtPlus.Reports.{0}", ReportName)) ' changed
                    If ios IsNot Nothing Then LoadLayout(ios) '                                                                       changed
                End Using
            End If

            ' Set the script references as needed
            ScriptReferences = DebtPlus.Utils.AssemblyTools.GetAllSiblingAssemblies()

            ' We can not properly script this procedure so do it here.
            AddHandler BeforePrint, AddressOf Report_BeforePrint
        End Sub

        Dim ds As New DataSet("ds")

        Private Sub Report_BeforePrint(ByVal sender As Object, ByVal e As PrintEventArgs)
            Dim rpt As XtraReport = CType(sender, XtraReport)

            ' Load the list of clients for the letter
            Using cmd As SqlCommand = New System.Data.SqlClient.SqlCommand()
                With cmd
                    .Connection = New System.Data.SqlClient.SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
                    .CommandText = "rpt_custom_6_month_letter"
                    .CommandType = CommandType.StoredProcedure
                    .CommandTimeout = 0
                End With

                Using da As New SqlDataAdapter(cmd)
                    da.Fill(ds, "rpt_custom_6_month_letter")
                End Using
            End Using

            Dim tbl As DataTable = ds.Tables("rpt_custom_6_month_letter")

#If 1 Then
            ' Create a thread to process the English messages. It must be a foreground thread so that it does not die when we do.
            Dim EnglishClass As New SendEnglish(tbl)
            With EnglishClass
                Dim thrd As New Thread(New ThreadStart(AddressOf .SendMessages))
                thrd.SetApartmentState(ApartmentState.STA)
                thrd.Start()

            End With

            ' Create a thread to process the Spanish messages. It must be a foreground thread so that it does not die when we do.
            Dim SpanishClass As New SendSpanish(tbl)
            With SpanishClass
                Dim thrd As New Thread(New ThreadStart(AddressOf .SendMessages))
                thrd.SetApartmentState(ApartmentState.STA)
                thrd.Start()

            End With

            ' Bind the report to the non-email items
            Dim vue As New DataView(tbl, "[client_email]=''", "postalcode, client", DataViewRowState.CurrentRows)
#Else
            Dim vue As New DataView(tbl, String.Empty, "postalcode, client", DataViewRowState.CurrentRows)
#End If
            If vue.Count > 0 Then
                rpt.DataSource = vue
            Else
                e.Cancel = True
            End If
        End Sub
    End Class
End Namespace