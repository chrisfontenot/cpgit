<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class ACHStopsReport
    Inherits DebtPlus.Reports.Template.DatedTemplateXtraReportClass

    'XtraReport overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Designer
    'It can be modified using the Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(ACHStopsReport))
        Me.XrPanel1 = New DevExpress.XtraReports.UI.XRPanel
        Me.XrLabel5 = New DevExpress.XtraReports.UI.XRLabel
        Me.XrLabel4 = New DevExpress.XtraReports.UI.XRLabel
        Me.XrLabel3 = New DevExpress.XtraReports.UI.XRLabel
        Me.XrLabel1 = New DevExpress.XtraReports.UI.XRLabel
        Me.XrLabel2 = New DevExpress.XtraReports.UI.XRLabel
        Me.XrLabel_counselor_name = New DevExpress.XtraReports.UI.XRLabel
        Me.XrLabel_name = New DevExpress.XtraReports.UI.XRLabel
        Me.XrLabel_note = New DevExpress.XtraReports.UI.XRLabel
        Me.XrLabel_date_created = New DevExpress.XtraReports.UI.XRLabel
        Me.XrLabel_client = New DevExpress.XtraReports.UI.XRLabel
        Me.GroupHeader1 = New DevExpress.XtraReports.UI.GroupHeaderBand
        Me.GroupFooter1 = New DevExpress.XtraReports.UI.GroupFooterBand
        Me.XrLabel6 = New DevExpress.XtraReports.UI.XRLabel
        CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
        '
        'Detail
        '
        Me.Detail.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel_client, Me.XrLabel_date_created, Me.XrLabel_name, Me.XrLabel_counselor_name, Me.XrLabel_note})
        Me.Detail.HeightF = 15.0!
        '
        'PageHeader
        '
        Me.PageHeader.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrPanel1})
        Me.PageHeader.HeightF = 140.0!
        Me.PageHeader.Controls.SetChildIndex(Me.XrPanel_AgencyAddress, 0)
        Me.PageHeader.Controls.SetChildIndex(Me.XrLabel_Title, 0)
        Me.PageHeader.Controls.SetChildIndex(Me.XrLine_Title, 0)
        Me.PageHeader.Controls.SetChildIndex(Me.XrLabel_Subtitle, 0)
        Me.PageHeader.Controls.SetChildIndex(Me.XrPageInfo1, 0)
        Me.PageHeader.Controls.SetChildIndex(Me.XrPanel1, 0)
        '
        'XrLabel_Title
        '
        Me.XrLabel_Title.StylePriority.UseFont = False
        '
        'XrPageInfo_PageNumber
        '
        Me.XrPageInfo_PageNumber.StylePriority.UseTextAlignment = False
        '
        'XRLabel_Agency_Name
        '
        Me.XRLabel_Agency_Name.StylePriority.UseTextAlignment = False
        '
        'XrLabel_Agency_Address3
        '
        Me.XrLabel_Agency_Address3.StylePriority.UseFont = False
        Me.XrLabel_Agency_Address3.StylePriority.UseTextAlignment = False
        '
        'XrLabel_Agency_Address1
        '
        Me.XrLabel_Agency_Address1.StylePriority.UseFont = False
        Me.XrLabel_Agency_Address1.StylePriority.UseTextAlignment = False
        '
        'XrLabel_Agency_Phone
        '
        Me.XrLabel_Agency_Phone.StylePriority.UseFont = False
        Me.XrLabel_Agency_Phone.StylePriority.UseTextAlignment = False
        '
        'XrLabel_Agency_Address2
        '
        Me.XrLabel_Agency_Address2.StylePriority.UseFont = False
        Me.XrLabel_Agency_Address2.StylePriority.UseTextAlignment = False
        '
        'XrPanel1
        '
        Me.XrPanel1.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel5, Me.XrLabel4, Me.XrLabel3, Me.XrLabel1, Me.XrLabel2})
        Me.XrPanel1.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 113.0!)
        Me.XrPanel1.Name = "XrPanel1"
        Me.XrPanel1.SizeF = New System.Drawing.SizeF(800.0!, 17.0!)
        Me.XrPanel1.StyleName = "XrControlStyle_HeaderPannel"
        '
        'XrLabel5
        '
        Me.XrLabel5.LocationFloat = New DevExpress.Utils.PointFloat(367.5!, 1.0!)
        Me.XrLabel5.Name = "XrLabel5"
        Me.XrLabel5.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
        Me.XrLabel5.SizeF = New System.Drawing.SizeF(81.0!, 14.99998!)
        Me.XrLabel5.StylePriority.UsePadding = False
        Me.XrLabel5.StylePriority.UseTextAlignment = False
        Me.XrLabel5.Text = "NOTE"
        Me.XrLabel5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        '
        'XrLabel4
        '
        Me.XrLabel4.LocationFloat = New DevExpress.Utils.PointFloat(608.0!, 1.0!)
        Me.XrLabel4.Name = "XrLabel4"
        Me.XrLabel4.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
        Me.XrLabel4.SizeF = New System.Drawing.SizeF(122.75!, 14.99998!)
        Me.XrLabel4.StylePriority.UsePadding = False
        Me.XrLabel4.StylePriority.UseTextAlignment = False
        Me.XrLabel4.Text = "COUNSELOR"
        Me.XrLabel4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        '
        'XrLabel3
        '
        Me.XrLabel3.LocationFloat = New DevExpress.Utils.PointFloat(271.0!, 1.0!)
        Me.XrLabel3.Name = "XrLabel3"
        Me.XrLabel3.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
        Me.XrLabel3.SizeF = New System.Drawing.SizeF(81.0!, 15.0!)
        Me.XrLabel3.StylePriority.UsePadding = False
        Me.XrLabel3.StylePriority.UseTextAlignment = False
        Me.XrLabel3.Text = "DATE"
        Me.XrLabel3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
        '
        'XrLabel1
        '
        Me.XrLabel1.LocationFloat = New DevExpress.Utils.PointFloat(11.5!, 1.0!)
        Me.XrLabel1.Name = "XrLabel1"
        Me.XrLabel1.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
        Me.XrLabel1.SizeF = New System.Drawing.SizeF(66.0!, 15.0!)
        Me.XrLabel1.StylePriority.UsePadding = False
        Me.XrLabel1.StylePriority.UseTextAlignment = False
        Me.XrLabel1.Text = "CLIENT"
        Me.XrLabel1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
        '
        'XrLabel2
        '
        Me.XrLabel2.LocationFloat = New DevExpress.Utils.PointFloat(86.5!, 1.0!)
        Me.XrLabel2.Name = "XrLabel2"
        Me.XrLabel2.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
        Me.XrLabel2.SizeF = New System.Drawing.SizeF(174.0!, 15.0!)
        Me.XrLabel2.StylePriority.UsePadding = False
        Me.XrLabel2.StylePriority.UseTextAlignment = False
        Me.XrLabel2.Text = "NAME"
        Me.XrLabel2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        '
        'XrLabel_counselor_name
        '
        Me.XrLabel_counselor_name.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "counselor_name")})
        Me.XrLabel_counselor_name.LocationFloat = New DevExpress.Utils.PointFloat(608.0!, 0.0!)
        Me.XrLabel_counselor_name.Name = "XrLabel_counselor_name"
        Me.XrLabel_counselor_name.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel_counselor_name.SizeF = New System.Drawing.SizeF(174.0!, 15.0!)
        Me.XrLabel_counselor_name.StylePriority.UseTextAlignment = False
        Me.XrLabel_counselor_name.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        '
        'XrLabel_name
        '
        Me.XrLabel_name.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "client_name")})
        Me.XrLabel_name.LocationFloat = New DevExpress.Utils.PointFloat(81.5!, 0.0!)
        Me.XrLabel_name.Multiline = True
        Me.XrLabel_name.Name = "XrLabel_name"
        Me.XrLabel_name.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel_name.Scripts.OnBeforePrint = "XrLabel_name_BeforePrint"
        Me.XrLabel_name.SizeF = New System.Drawing.SizeF(174.0!, 15.0!)
        Me.XrLabel_name.StylePriority.UseTextAlignment = False
        Me.XrLabel_name.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        '
        'XrLabel_note
        '
        Me.XrLabel_note.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "note")})
        Me.XrLabel_note.LocationFloat = New DevExpress.Utils.PointFloat(367.5!, 0.0!)
        Me.XrLabel_note.Multiline = True
        Me.XrLabel_note.Name = "XrLabel_note"
        Me.XrLabel_note.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel_note.SizeF = New System.Drawing.SizeF(225.0!, 15.0!)
        Me.XrLabel_note.StylePriority.UseTextAlignment = False
        Me.XrLabel_note.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        '
        'XrLabel_date_created
        '
        Me.XrLabel_date_created.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "date_created", "{0:d}")})
        Me.XrLabel_date_created.LocationFloat = New DevExpress.Utils.PointFloat(271.0!, 0.0!)
        Me.XrLabel_date_created.Name = "XrLabel_date_created"
        Me.XrLabel_date_created.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel_date_created.SizeF = New System.Drawing.SizeF(81.0!, 15.0!)
        Me.XrLabel_date_created.StylePriority.UseTextAlignment = False
        Me.XrLabel_date_created.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
        Me.XrLabel_date_created.WordWrap = False
        '
        'XrLabel_client
        '
        Me.XrLabel_client.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
        Me.XrLabel_client.Name = "XrLabel_client"
        Me.XrLabel_client.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel_client.Scripts.OnBeforePrint = "XrLabel_client_BeforePrint"
        Me.XrLabel_client.SizeF = New System.Drawing.SizeF(66.0!, 15.0!)
        Me.XrLabel_client.StylePriority.UseTextAlignment = False
        Me.XrLabel_client.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
        Me.XrLabel_client.WordWrap = False
        '
        'GroupHeader1
        '
        Me.GroupHeader1.GroupFields.AddRange(New DevExpress.XtraReports.UI.GroupField() {New DevExpress.XtraReports.UI.GroupField("client", DevExpress.XtraReports.UI.XRColumnSortOrder.Ascending)})
        Me.GroupHeader1.HeightF = 0.0!
        Me.GroupHeader1.Name = "GroupHeader1"
        '
        'GroupFooter1
        '
        Me.GroupFooter1.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel6})
        Me.GroupFooter1.HeightF = 15.0!
        Me.GroupFooter1.Name = "GroupFooter1"
        '
        'XrLabel6
        '
        Me.XrLabel6.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
        Me.XrLabel6.Name = "XrLabel6"
        Me.XrLabel6.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel6.SizeF = New System.Drawing.SizeF(100.0!, 15.0!)
        '
        'ACHStopsReport
        '
        Me.Bands.AddRange(New DevExpress.XtraReports.UI.Band() {Me.Detail, Me.PageHeader, Me.PageFooter, Me.TopMarginBand1, Me.BottomMarginBand1, Me.GroupHeader1, Me.GroupFooter1})
        Me.ScriptLanguage = DevExpress.XtraReports.ScriptLanguage.VisualBasic
        Me.ScriptReferencesString = resources.GetString("$this.ScriptReferencesString")
        Me.Scripts.OnBeforePrint = "ACHStopsReport_BeforePrint"
        Me.ScriptsSource = resources.GetString("$this.ScriptsSource")
        Me.StyleSheet.AddRange(New DevExpress.XtraReports.UI.XRControlStyle() {Me.XrControlStyle_Header, Me.XrControlStyle_Totals, Me.XrControlStyle_HeaderPannel, Me.XrControlStyle_GroupHeader})
        Me.Version = "10.1"
        Me.Controls.SetChildIndex(Me.GroupFooter1, 0)
        Me.Controls.SetChildIndex(Me.GroupHeader1, 0)
        Me.Controls.SetChildIndex(Me.BottomMarginBand1, 0)
        Me.Controls.SetChildIndex(Me.TopMarginBand1, 0)
        Me.Controls.SetChildIndex(Me.PageFooter, 0)
        Me.Controls.SetChildIndex(Me.PageHeader, 0)
        Me.Controls.SetChildIndex(Me.Detail, 0)
        CType(Me, System.ComponentModel.ISupportInitialize).EndInit()

    End Sub
    Friend WithEvents XrPanel1 As DevExpress.XtraReports.UI.XRPanel
    Friend WithEvents XrLabel5 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel4 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel3 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel1 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel2 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel_client As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel_date_created As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel_name As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel_counselor_name As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel_note As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents GroupHeader1 As DevExpress.XtraReports.UI.GroupHeaderBand
    Friend WithEvents GroupFooter1 As DevExpress.XtraReports.UI.GroupFooterBand
    Friend WithEvents XrLabel6 As DevExpress.XtraReports.UI.XRLabel
End Class
