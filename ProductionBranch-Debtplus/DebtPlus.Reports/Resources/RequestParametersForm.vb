#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}
#End Region

Option Compare Binary
Option Explicit On 
Option Strict On

Imports DebtPlus.UI.Client.Widgets.Controls

Namespace Resources
    Public Class RequestParametersForm
        Inherits Template.Forms.ReportParametersForm
        Private ResourceReport As ResourceReportClass = Nothing

        Public Sub New(ByVal Report As ResourceReportClass)
            MyBase.New()
            InitializeComponent()
            AddHandler Me.Load, AddressOf RequestParametersForm_Load
            AddHandler TextEdit_zipcode.Enter, AddressOf TextBox_Enter
            AddHandler TextEdit_zipcode.Validating, AddressOf TextEdit_zipcode_Validating
            Me.ResourceReport = Report
        End Sub

        Public ReadOnly Property Office() As System.Int32
            Get
                Return Convert.ToInt32(LookUpEdit_Office.EditValue)
            End Get
        End Property

        Public ReadOnly Property TypeList() As System.Collections.ArrayList
            Get
                Dim capacity As System.Int32 = System.Math.Max(CheckedListBoxControl_ResourceType.Items.Count, 1)
                Dim Result As New System.Collections.ArrayList(capacity)

                ' Find the list of types that are checked
                For ItemNumber As System.Int32 = 0 To CheckedListBoxControl_ResourceType.Items.Count - 1
                    Dim CurrentItem As DevExpress.XtraEditors.Controls.CheckedListBoxItem = Me.CheckedListBoxControl_ResourceType.Items(ItemNumber)

                    ' If the item is checked then add it to the list of values to be returned
                    If CurrentItem.CheckState = CheckState.Checked Then
                        Result.Add(Convert.ToInt32(CType(CurrentItem.Value, DebtPlus.Data.Controls.ComboboxItem).value))
                    End If
                Next ItemNumber

                ' The result is the array list
                Return Result
            End Get
        End Property

#Region " Windows Form Designer generated code "

        'Form overrides dispose to clean up the component list.
        Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
            If disposing Then
                If components IsNot Nothing Then
                    components.Dispose()
                End If
            End If
            MyBase.Dispose(disposing)
        End Sub

        'Required by the Windows Form Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Windows Form Designer
        'It can be modified using the Windows Form Designer.  
        'Do not modify it using the code editor.
        Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
        Friend WithEvents LookUpEdit_Office As DevExpress.XtraEditors.LookUpEdit
        Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
        Friend WithEvents CheckedListBoxControl_ResourceType As DevExpress.XtraEditors.CheckedListBoxControl
        Friend WithEvents TextEdit_zipcode As DevExpress.XtraEditors.TextEdit
        Friend WithEvents RadioButton_Client As System.Windows.Forms.RadioButton
        Friend WithEvents RadioButton_Office As System.Windows.Forms.RadioButton
        Friend WithEvents ButtonEdit_Client As DebtPlus.UI.Client.Widgets.Controls.ClientID
        Friend WithEvents RadioButton_PostalCode As System.Windows.Forms.RadioButton
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
            Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(RequestParametersForm))
            Dim SerializableAppearanceObject1 As DevExpress.Utils.SerializableAppearanceObject = New DevExpress.Utils.SerializableAppearanceObject
            Me.GroupBox1 = New System.Windows.Forms.GroupBox
            Me.ButtonEdit_Client = New DebtPlus.UI.Client.Widgets.Controls.ClientID
            Me.LookUpEdit_Office = New DevExpress.XtraEditors.LookUpEdit
            Me.RadioButton_Client = New System.Windows.Forms.RadioButton
            Me.RadioButton_Office = New System.Windows.Forms.RadioButton
            Me.RadioButton_PostalCode = New System.Windows.Forms.RadioButton
            Me.TextEdit_zipcode = New DevExpress.XtraEditors.TextEdit
            Me.GroupBox2 = New System.Windows.Forms.GroupBox
            Me.CheckedListBoxControl_ResourceType = New DevExpress.XtraEditors.CheckedListBoxControl
            CType(Me.DxErrorProvider1, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.GroupBox1.SuspendLayout()
            CType(Me.ButtonEdit_Client.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LookUpEdit_Office.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.TextEdit_zipcode.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.GroupBox2.SuspendLayout()
            CType(Me.CheckedListBoxControl_ResourceType, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.SuspendLayout()
            '
            'ButtonOK
            '
            Me.ButtonOK.Location = New System.Drawing.Point(408, 146)
            Me.ButtonOK.TabIndex = 2
            '
            'ButtonCancel
            '
            Me.ButtonCancel.Location = New System.Drawing.Point(408, 198)
            Me.ButtonCancel.TabIndex = 3
            '
            'GroupBox1
            '
            Me.GroupBox1.Controls.Add(Me.ButtonEdit_Client)
            Me.GroupBox1.Controls.Add(Me.LookUpEdit_Office)
            Me.GroupBox1.Controls.Add(Me.RadioButton_Client)
            Me.GroupBox1.Controls.Add(Me.RadioButton_Office)
            Me.GroupBox1.Controls.Add(Me.RadioButton_PostalCode)
            Me.GroupBox1.Controls.Add(Me.TextEdit_zipcode)
            Me.GroupBox1.Location = New System.Drawing.Point(280, 9)
            Me.GroupBox1.Name = "GroupBox1"
            Me.GroupBox1.Size = New System.Drawing.Size(208, 112)
            Me.GroupBox1.TabIndex = 1
            Me.GroupBox1.TabStop = False
            Me.GroupBox1.Text = "  Choose the location for the client "
            '
            'ButtonEdit_Client
            '
            Me.ButtonEdit_Client.Location = New System.Drawing.Point(80, 25)
            Me.ButtonEdit_Client.Name = "ButtonEdit_Client"
            Me.ButtonEdit_Client.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[False]
            Me.ButtonEdit_Client.Properties.Appearance.Options.UseTextOptions = True
            Me.ButtonEdit_Client.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.ButtonEdit_Client.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "", -1, True, True, False, DevExpress.XtraEditors.ImageLocation.MiddleCenter, CType(resources.GetObject("ButtonEdit_Client.Properties.Buttons"), System.Drawing.Image), New DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), SerializableAppearanceObject1, "Click here to search for a client ID", "search", Nothing, True)})
            Me.ButtonEdit_Client.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
            Me.ButtonEdit_Client.Properties.DisplayFormat.FormatString = "0000000"
            Me.ButtonEdit_Client.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom
            Me.ButtonEdit_Client.Properties.EditFormat.FormatString = "f0"
            Me.ButtonEdit_Client.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.ButtonEdit_Client.Properties.Mask.BeepOnError = True
            Me.ButtonEdit_Client.Properties.Mask.EditMask = "\d*"
            Me.ButtonEdit_Client.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx
            Me.ButtonEdit_Client.Properties.ValidateOnEnterKey = True
            Me.ButtonEdit_Client.Size = New System.Drawing.Size(96, 20)
            Me.ButtonEdit_Client.TabIndex = 1
            '
            'LookUpEdit_Office
            '
            Me.LookUpEdit_Office.Location = New System.Drawing.Point(80, 52)
            Me.LookUpEdit_Office.Name = "LookUpEdit_Office"
            Me.LookUpEdit_Office.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.LookUpEdit_Office.Properties.NullText = ""
            Me.LookUpEdit_Office.Properties.PopupWidth = 450
            Me.LookUpEdit_Office.Size = New System.Drawing.Size(96, 20)
            Me.LookUpEdit_Office.TabIndex = 3
            Me.LookUpEdit_Office.ToolTip = "Choose the office for the report information"
            Me.LookUpEdit_Office.ToolTipController = Me.ToolTipController1
            '
            'RadioButton_Client
            '
            Me.RadioButton_Client.AllowDrop = True
            Me.RadioButton_Client.Location = New System.Drawing.Point(8, 26)
            Me.RadioButton_Client.Name = "RadioButton_Client"
            Me.RadioButton_Client.Size = New System.Drawing.Size(66, 17)
            Me.RadioButton_Client.TabIndex = 0
            Me.RadioButton_Client.Tag = "client"
            Me.RadioButton_Client.Text = "Client ID"
            '
            'RadioButton_Office
            '
            Me.RadioButton_Office.AllowDrop = True
            Me.RadioButton_Office.Location = New System.Drawing.Point(8, 52)
            Me.RadioButton_Office.Name = "RadioButton_Office"
            Me.RadioButton_Office.Size = New System.Drawing.Size(66, 17)
            Me.RadioButton_Office.TabIndex = 2
            Me.RadioButton_Office.Tag = "office"
            Me.RadioButton_Office.Text = "Office"
            '
            'RadioButton_PostalCode
            '
            Me.RadioButton_PostalCode.AllowDrop = True
            Me.RadioButton_PostalCode.Location = New System.Drawing.Point(8, 78)
            Me.RadioButton_PostalCode.Name = "RadioButton_PostalCode"
            Me.RadioButton_PostalCode.Size = New System.Drawing.Size(66, 17)
            Me.RadioButton_PostalCode.TabIndex = 4
            Me.RadioButton_PostalCode.Tag = "postalcode"
            Me.RadioButton_PostalCode.Text = "ZipCode"
            '
            'TextEdit_zipcode
            '
            Me.TextEdit_zipcode.EditValue = ""
            Me.TextEdit_zipcode.Location = New System.Drawing.Point(80, 78)
            Me.TextEdit_zipcode.Name = "TextEdit_zipcode"
            Me.TextEdit_zipcode.Properties.Appearance.Options.UseTextOptions = True
            Me.TextEdit_zipcode.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.TextEdit_zipcode.Properties.AppearanceDisabled.Options.UseTextOptions = True
            Me.TextEdit_zipcode.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.TextEdit_zipcode.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
            Me.TextEdit_zipcode.Properties.DisplayFormat.FormatString = "f0"
            Me.TextEdit_zipcode.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.TextEdit_zipcode.Properties.EditFormat.FormatString = "f0"
            Me.TextEdit_zipcode.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.TextEdit_zipcode.Properties.Mask.EditMask = "\d{5}(-\d{4})?"
            Me.TextEdit_zipcode.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx
            Me.TextEdit_zipcode.Properties.MaxLength = 10
            Me.TextEdit_zipcode.Properties.ValidateOnEnterKey = True
            Me.TextEdit_zipcode.Size = New System.Drawing.Size(96, 20)
            Me.TextEdit_zipcode.TabIndex = 5
            Me.TextEdit_zipcode.ToolTip = "Enter the postalcode for the client's location. This is used to find the office f" & _
                "ield."
            Me.TextEdit_zipcode.ToolTipController = Me.ToolTipController1
            '
            'GroupBox2
            '
            Me.GroupBox2.Controls.Add(Me.CheckedListBoxControl_ResourceType)
            Me.GroupBox2.Location = New System.Drawing.Point(8, 9)
            Me.GroupBox2.Name = "GroupBox2"
            Me.GroupBox2.Size = New System.Drawing.Size(264, 241)
            Me.GroupBox2.TabIndex = 0
            Me.GroupBox2.TabStop = False
            Me.GroupBox2.Text = " Resources"
            '
            'CheckedListBoxControl_ResourceType
            '
            Me.CheckedListBoxControl_ResourceType.ItemHeight = 16
            Me.CheckedListBoxControl_ResourceType.Location = New System.Drawing.Point(8, 17)
            Me.CheckedListBoxControl_ResourceType.Name = "CheckedListBoxControl_ResourceType"
            Me.CheckedListBoxControl_ResourceType.Size = New System.Drawing.Size(248, 216)
            Me.CheckedListBoxControl_ResourceType.TabIndex = 0
            Me.CheckedListBoxControl_ResourceType.ToolTip = "Choose the types of resources that you wish to include in the report"
            Me.CheckedListBoxControl_ResourceType.ToolTipController = Me.ToolTipController1
            '
            'RequestParametersForm
            '
            Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
            Me.ClientSize = New System.Drawing.Size(498, 258)
            Me.Controls.Add(Me.GroupBox2)
            Me.Controls.Add(Me.GroupBox1)
            Me.LookAndFeel.UseDefaultLookAndFeel = True
            Me.Name = "RequestParametersForm"
            Me.Text = "Resource Guide"
            Me.TopMost = False
            Me.Controls.SetChildIndex(Me.ButtonOK, 0)
            Me.Controls.SetChildIndex(Me.ButtonCancel, 0)
            Me.Controls.SetChildIndex(Me.GroupBox1, 0)
            Me.Controls.SetChildIndex(Me.GroupBox2, 0)
            CType(Me.DxErrorProvider1, System.ComponentModel.ISupportInitialize).EndInit()
            Me.GroupBox1.ResumeLayout(False)
            CType(Me.ButtonEdit_Client.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LookUpEdit_Office.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.TextEdit_zipcode.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            Me.GroupBox2.ResumeLayout(False)
            CType(Me.CheckedListBoxControl_ResourceType, System.ComponentModel.ISupportInitialize).EndInit()
            Me.ResumeLayout(False)

        End Sub

#End Region

        Private Sub RequestParametersForm_Load(ByVal sender As System.Object, ByVal e As System.EventArgs)

            Try
                ' Load the office table
                LoadOffices()
                LoadTypes()

                ' Clear the button settings
                RadioButton_Office.Checked = True
                RadioButton_Client.Checked = False
                RadioButton_PostalCode.Checked = False

                ' Define the parameters from the information
                If ResourceReport.Parameter_office >= 1 Then
                    SetOffice(ResourceReport.Parameter_office)

                    ' Look at the client
                ElseIf ResourceReport.Parameter_Client >= 0 Then
                    ButtonEdit_Client.Text = String.Format("{0:0000000}", ResourceReport.Parameter_Client)
                    SetClient(ResourceReport.Parameter_Client)
                    RadioButton_Office.Checked = False
                    RadioButton_Client.Checked = True

                    ' Finally look at the postalcode
                ElseIf ResourceReport.Parameter_PostalCode <> System.String.Empty Then
                    TextEdit_zipcode.Text = ResourceReport.Parameter_PostalCode
                    SetPostalcode(ResourceReport.Parameter_PostalCode)
                    RadioButton_Office.Checked = False
                    RadioButton_PostalCode.Checked = True
                End If

                ' Enable or disable the input fields
                LookUpEdit_Office.Enabled = RadioButton_Office.Checked
                ButtonEdit_Client.Enabled = RadioButton_Client.Checked
                TextEdit_zipcode.Enabled = RadioButton_PostalCode.Checked

                ' Add the handlers for the update functions
                AddHandler RadioButton_Client.CheckedChanged, AddressOf RadioButton_CheckedChanged
                AddHandler RadioButton_Office.CheckedChanged, AddressOf RadioButton_CheckedChanged
                AddHandler RadioButton_PostalCode.CheckedChanged, AddressOf RadioButton_CheckedChanged

                AddHandler ButtonEdit_Client.Validated, AddressOf ButtonEdit_Client_Validated
                AddHandler LookUpEdit_Office.EditValueChanging, AddressOf DebtPlus.Data.Validation.LookUpEdit_ActiveTest

                AddHandler LookUpEdit_Office.EditValueChanged, AddressOf FormChanged
                AddHandler ButtonEdit_Client.EditValueChanged, AddressOf FormChanged
                AddHandler TextEdit_zipcode.EditValueChanged, AddressOf FormChanged
                AddHandler TextEdit_zipcode.Validating, AddressOf TextEdit_zipcode_Validating
                AddHandler RadioButton_Client.CheckedChanged, AddressOf FormChanged
                AddHandler RadioButton_Office.CheckedChanged, AddressOf FormChanged
                AddHandler RadioButton_PostalCode.CheckedChanged, AddressOf FormChanged
                AddHandler CheckedListBoxControl_ResourceType.ItemCheck, AddressOf FormChanged

                ' Hot track the mouse. It helps.
                CheckedListBoxControl_ResourceType.HotTrackItems = True

                ' Enable or disable the OK button
                ButtonOK.Enabled = Not HasErrors()

            Catch ex As Exception
                DebtPlus.Data.Forms.MessageBox.Show(ex.ToString(), "Error Loading Report", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1)
                DialogResult = System.Windows.Forms.DialogResult.Cancel
            End Try

        End Sub

        Private Sub ButtonEdit_Client_Validated(ByVal sender As Object, ByVal e As System.EventArgs)
            Dim ctl As DebtPlus.UI.Client.Widgets.Controls.ClientID = CType(sender, DebtPlus.UI.Client.Widgets.Controls.ClientID)
            Dim ErrorMessage As String
            If ctl.EditValue.GetValueOrDefault(-1) < 0 Then
                ErrorMessage = "The client number is not valid."
            Else
                ErrorMessage = String.Empty
                SetClient(ctl.EditValue.Value)
            End If

            ' Define the error message text
            DxErrorProvider1.SetError(ctl, ErrorMessage)
        End Sub

        Private Sub SetClient(ByVal Client As System.Int32)
            ' Try to find the postalcode from the client's address field

            Dim Answer As Object
            Dim cn As SqlClient.SqlConnection = New System.Data.SqlClient.SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)

            Try
                cn.Open()

                ' Select the disbursement information from the database
                Using cmd As New SqlClient.SqlCommand()
                    With cmd
                        .Connection = cn
                        .CommandTimeout = DebtPlus.LINQ.SQLInfoClass.getDefault().CommandTimeout
                        .CommandText = "SELECT ca.[postalcode] FROM clients c WITH (NOLOCK) INNER JOIN addresses ca ON c.[AddressID] = ca.[Address] WHERE c.[client]=@client"
                        .Parameters.Add(New SqlClient.SqlParameter("@client", SqlDbType.Int)).Value = Client
                        Answer = .ExecuteScalar
                    End With
                End Using

            Finally
                If cn IsNot Nothing Then cn.Dispose()
            End Try

            If Answer IsNot System.DBNull.Value Then
                Dim Postalcode As String = Convert.ToString(Answer).Trim().Replace("-", "").PadLeft(5, "0"c).Substring(0, 5)
                If Postalcode <> "00000" Then
                    TextEdit_zipcode.Text = Postalcode
                    SetPostalcode(Postalcode)
                End If
            End If
        End Sub

        Private Sub SetPostalcode(ByVal Postalcode As String)
            Dim Answer As System.Int32 = -1

            Dim cn As New SqlClient.SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)

            Try
                cn.Open()

                ' Select the disbursement information from the database
                Using cmd As New SqlClient.SqlCommand()
                    With cmd
                        .Connection = cn
                        .CommandTimeout = DebtPlus.LINQ.SQLInfoClass.getDefault().CommandTimeout
                        .CommandText = "xpr_Find_Office_ByZipcode"
                        .CommandType = CommandType.StoredProcedure
                        .Parameters.Add("@RETURN_VALUE", SqlDbType.Int).Direction = ParameterDirection.ReturnValue
                        .Parameters.Add("@postalcode", SqlDbType.VarChar, 80).Value = Postalcode
                        .ExecuteNonQuery()
                        Answer = Convert.ToInt32(.Parameters(0).Value)
                    End With
                End Using

            Finally
                If cn IsNot Nothing Then cn.Dispose()
            End Try

            If Answer >= 1 Then SetOffice(Answer)
        End Sub

        Private Sub SetOffice(ByVal Office As System.Int32)
            LookUpEdit_Office.EditValue = Office
        End Sub

        Private Sub LoadOffices()
            Dim ds As New System.Data.DataSet

            ' Retrieve the database connection logic
            Dim cn As New SqlClient.SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)

            ' Select the disbursement information from the database
            Using cmd As New SqlClient.SqlCommand()
                With cmd
                    .Connection = cn
                    .CommandText = "SELECT o.[office] as 'Office', o.[name] as 'Name', isnull(m.[description],'') as 'Type', o.ActiveFlag FROM [offices] o WITH (NOLOCK) LEFT OUTER JOIN [OfficeTypes] m WITH (NOLOCK) ON o.[type] = m.[oID] ORDER BY o.[name], o.[office]"
                    .CommandTimeout = 0
                End With

                ' Retrieve the disbursement table from the system
                Using da As New SqlClient.SqlDataAdapter(cmd)
                    da.Fill(ds, "offices")
                End Using
            End Using
            Dim tbl As System.Data.DataTable = ds.Tables(0)

            ' Define the values for the lookup control
            With LookUpEdit_Office
                With .Properties
                    .DataSource = tbl.DefaultView
                    .ShowFooter = False
                    .PopulateColumns()

                    With .Columns("Office")
                        .Alignment = DevExpress.Utils.HorzAlignment.Far
                        .Width = 20
                    End With

                    With .Columns("Name")
                        .Width = 90
                    End With

                    ' The display is the note, the value is the ID
                    .DisplayMember = "Name"
                    .ValueMember = "Office"
                    .BestFit()

                    .PopupWidth = 400
                End With
            End With
        End Sub

        Private Sub LoadTypes()

            ' Retrieve the database connection logic
            Dim cn As New SqlClient.SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
            Dim rd As SqlClient.SqlDataReader = Nothing

            Try
                cn.Open()

                ' Select the disbursement information from the database
                Using cmd As New SqlClient.SqlCommand()
                    With cmd
                        .Connection = cn
                        .CommandText = "SELECT resource_type, description FROM resource_types WITH (NOLOCK) WHERE description IS NOT NULL ORDER BY description"
                        .CommandTimeout = 0
                        rd = .ExecuteReader(CommandBehavior.CloseConnection Or CommandBehavior.SingleResult)
                    End With
                End Using

                ' Populate the items for the checkbox
                With CheckedListBoxControl_ResourceType
                    With .Items
                        .Clear()
                        Do While rd.Read
                            Dim resource_type As System.Int32 = rd.GetInt32(0)
                            Dim description As String = rd.GetString(1).Trim()
                            .Add(New DevExpress.XtraEditors.Controls.CheckedListBoxItem(New DebtPlus.Data.Controls.ComboboxItem(description, resource_type), False))
                        Loop
                    End With
                End With

            Finally
                If rd IsNot Nothing Then rd.Dispose()
                If cn IsNot Nothing Then cn.Dispose()
            End Try

            If CheckedListBoxControl_ResourceType.Items.Count = 0 Then
                DebtPlus.Data.Forms.MessageBox.Show("There are no resource types defined in the system." & Chr(13) & Chr(10) & Chr(13) & Chr(10) & "You will not be able to run the report without having some resource types and resources.", "Sorry, but you are missing the data", MessageBoxButtons.OK, MessageBoxIcon.Information)
            End If
        End Sub

        Private Shadows Function HasErrors() As Boolean
            Dim Answer As Boolean = True

            Do
                ' If there is an error then we can't accept the form until it is cleared
                If DxErrorProvider1.GetError(TextEdit_zipcode) <> System.String.Empty OrElse DxErrorProvider1.GetError(ButtonEdit_Client) <> System.String.Empty Then Exit Do

                ' If there is no office then do not accept the update
                If LookUpEdit_Office.EditValue Is Nothing Then Exit Do

                ' The zipcode can not have an error condition
                If TextEdit_zipcode.ErrorText <> String.Empty Then Exit Do

                ' If there are no types selected then do not accept the update
                If TypeList.Count = 0 Then Exit Do

                ' Success
                Answer = False
                Exit Do
            Loop

            Return Answer
        End Function

        Private Sub RadioButton_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)

            ' Enable or disable the lookup options
            LookUpEdit_Office.Enabled = RadioButton_Office.Checked
            ButtonEdit_Client.Enabled = RadioButton_Client.Checked
            TextEdit_zipcode.Enabled = RadioButton_PostalCode.Checked

            ' Go to the next control
            With CType(sender, System.Windows.Forms.RadioButton)
                .SelectNextControl(CType(sender, System.Windows.Forms.RadioButton), True, False, True, False)
            End With
        End Sub

        Private Sub TextBox_Enter(ByVal sender As System.Object, ByVal e As System.EventArgs)
            With CType(sender, DevExpress.XtraEditors.TextEdit)
                If .Text.Length > 0 Then
                    .Select(0, .Text.Length)
                End If
            End With
        End Sub

        Private Sub TextEdit_zipcode_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs)
            Dim ErrorStatus As Boolean = True
            If TextEdit_zipcode.EditValue IsNot Nothing AndAlso TextEdit_zipcode.EditValue IsNot System.DBNull.Value Then
                Dim ZipCode As String = DebtPlus.Utils.Nulls.DStr(TextEdit_zipcode.EditValue)
                If ZipCode <> String.Empty Then
                    Dim Postalcode As String = ZipCode.Trim().Replace("-", "").PadLeft(5, "0"c).Substring(0, 5)
                    If Postalcode <> "00000" Then
                        TextEdit_zipcode.Text = Postalcode
                        SetPostalcode(Postalcode)
                        ErrorStatus = False
                    End If
                End If
            End If

            If ErrorStatus Then
                TextEdit_zipcode.ErrorText = "Invalid zipcode"
            Else
                TextEdit_zipcode.ErrorText = String.Empty
            End If

            ButtonOK.Enabled = Not HasErrors()
        End Sub
    End Class
End Namespace