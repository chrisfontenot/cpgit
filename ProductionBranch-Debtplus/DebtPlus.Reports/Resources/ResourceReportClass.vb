#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}
#End Region

Imports System.IO
Imports System.Reflection
Imports DebtPlus.Interfaces.Client
Imports DebtPlus.Utils

Namespace Resources
    Public Class ResourceReportClass
        Inherits Template.TemplateXtraReportClass
        Implements IClient

        ''' <summary>
        '''     Create an instance of our report
        ''' </summary>
        Public Sub New()
            MyBase.New()
            InitializeComponent()

            ' Reset the parameters so that they represent the default settings of "not supplied"
            Parameter_Client = -1
            Parameter_office = -1
            Parameter_PostalCode = String.Empty
            Parameter_ResourceTypes = Nothing
        End Sub

        ''' <summary>
        '''     Client parameter for the report
        ''' </summary>
        ''' <remarks>
        ''' Implements IClient.ClientID
        ''' </remarks>
        Public Property ClientID As Int32 Implements IClient.ClientId
            Get
                Return Parameter_Client
            End Get
            Set(value As Int32)
                Parameter_Client = value
            End Set
        End Property

        ''' <summary>
        '''     Client parameter for the report
        ''' </summary>
        Public Property Parameter_Client() As System.Int32
            Get
                Dim parm As DevExpress.XtraReports.Parameters.Parameter = FindParameter("ParameterClient")
                Return If(parm Is Nothing, -1, Convert.ToInt32(parm.Value))
            End Get
            Set(ByVal Value As System.Int32)
                SetParameter("ParameterClient", GetType(Int32), Value, "Client ID", False)
            End Set
        End Property

        ''' <summary>
        '''     ZIPcode parameter for the report
        ''' </summary>
        Public Property Parameter_PostalCode() As String
            Get
                Dim parm As DevExpress.XtraReports.Parameters.Parameter = FindParameter("ParameterPostalCode")
                Return If(parm Is Nothing, String.Empty, Convert.ToString(parm.Value, System.Globalization.CultureInfo.InvariantCulture))
            End Get
            Set(ByVal Value As String)
                SetParameter("ParameterPostalCode", GetType(String), Value, "Client PostalCode", False)
            End Set
        End Property

        ''' <summary>
        '''     Office parameter for the report
        ''' </summary>
        Public Property Parameter_office() As System.Int32
            Get
                Dim parm As DevExpress.XtraReports.Parameters.Parameter = FindParameter("ParameterOffice")
                Return If(parm Is Nothing, -1, Convert.ToString(parm.Value, System.Globalization.CultureInfo.InvariantCulture))
            End Get
            Set(ByVal Value As System.Int32)
                SetParameter("ParameterOffice", GetType(Int32), Value, "Office ID", False)
            End Set
        End Property

        ''' <summary>
        '''     Resouce Types parameter for the report
        ''' </summary>
        Public Property Parameter_ResourceTypes() As System.Collections.ArrayList
            Get
                Dim parm As DevExpress.XtraReports.Parameters.Parameter = FindParameter("ParameterResourceTypes")
                Dim strValue As String = If(parm Is Nothing, String.Empty, Convert.ToString(parm.Value, System.Globalization.CultureInfo.InvariantCulture))
                If strValue <> String.Empty Then
                    Dim col As New System.Collections.ArrayList()
                    Dim StrValues() As String = strValue.Split(","c)
                    For Each Str As String In StrValues
                        Str = Str.Trim()
                        If Str <> String.Empty Then
                            col.Add(Str)
                        End If
                    Next
                    Return col
                End If
                Return Nothing
            End Get

            Set(ByVal Value As System.Collections.ArrayList)
                Dim strValue As String = String.Empty
                If Value IsNot Nothing Then
                    Dim sb As New System.Text.StringBuilder
                    For Each item As System.Int32 In Value
                        sb.AppendFormat(",{0:f0}", item)
                    Next
                    If sb.Length > 0 Then sb.Remove(0, 1)
                    strValue = sb.ToString()
                End If
                SetParameter("ParameterResourceTypes", GetType(String), strValue, "Resource IDs", False)
            End Set
        End Property

        ''' <summary>
        '''     Define the report parameters from other applications
        ''' </summary>
        Public Overrides Sub SetReportParameter(ByVal Parameter As String, ByVal Value As Object)
            Select Case Parameter
                Case "Client"
                    Parameter_Client = Value
                Case "Office"
                    Parameter_office = Value
                Case "PostalCode"
                    Parameter_PostalCode = Value
                Case "ResourceTypes"
                    Parameter_ResourceTypes = Value
                Case Else
                    MyBase.SetReportParameter(Parameter, Value)
            End Select
        End Sub

        ''' <summary>
        '''     Title associated with the report
        ''' </summary>
        Public Overrides ReadOnly Property ReportTitle() As String
            Get
                Return "Resource Information"
            End Get
        End Property

        ''' <summary>
        '''     Do we need to ask for the report parameters?
        ''' </summary>
        Public Overrides Function NeedParameters() As Boolean
            Return Parameter_ResourceTypes Is Nothing OrElse Parameter_office <= 0
        End Function

        ''' <summary>
        '''     Request the parameters for the report
        ''' </summary>
        Public Overrides Function RequestReportParameters() As System.Windows.Forms.DialogResult
            Dim answer As System.Windows.Forms.DialogResult = DialogResult.OK
            If NeedParameters() Then
                Using dialogForm As New RequestParametersForm(Me)
                    With dialogForm
                        answer = .ShowDialog()
                        Parameter_office = .Office
                        Parameter_ResourceTypes = .TypeList
                    End With
                End Using
            End If
            Return answer
        End Function

        ''' <summary>
        '''     Initialize the report component
        ''' </summary>
        Private Sub InitializeComponent()
            ' Name of the report file
            Const ReportName As String = "DebtPlus.Reports.Resources.repx"
            Const AssemblyName As String = "DebtPlus.Reports"

            ' See if there is a report reference that we can use
            Dim UseDefault As Boolean = True
            Try
                Dim Fname As String = System.IO.Path.Combine(DebtPlus.Configuration.Config.ReportsDirectory, ReportName)
                If (Not DebtPlus.Configuration.Config.LocalReportsOnly) AndAlso System.IO.File.Exists(Fname) Then
                    LoadLayout(Fname)
                    UseDefault = False
                End If

                ' Load the standard report definition if we need a new item
                If UseDefault Then
                    Dim asm As Assembly = Assembly.GetExecutingAssembly()
                    Using ios As Stream = asm.GetManifestResourceStream(String.Format("{0}.{1}", AssemblyName, ReportName))
                        If ios Is Nothing Then
                            Throw New ArgumentNullException("Report not defined")
                        End If

                        LoadLayout(ios) ' changed
                    End Using
                End If

                ' Set the script references as needed
                ScriptReferences = DebtPlus.Utils.AssemblyTools.GetAllSiblingAssemblies()

                ' Update the subreports as well
                For Each BandPtr As DevExpress.XtraReports.UI.Band In Bands
                    For Each CtlPtr As DevExpress.XtraReports.UI.XRControl In BandPtr.Controls
                        Dim Rpt As DevExpress.XtraReports.UI.XRSubreport = TryCast(CtlPtr, DevExpress.XtraReports.UI.XRSubreport)
                        If Rpt IsNot Nothing Then
                            Dim RptSrc As DevExpress.XtraReports.UI.XtraReport = TryCast(Rpt.ReportSource, DevExpress.XtraReports.UI.XtraReport)
                            If RptSrc IsNot Nothing Then
                                RptSrc.ScriptReferences = ScriptReferences
                            End If
                        End If
                    Next
                Next

            Catch ex As System.Exception
                Using gdr As New Repository.GetDataResult()
                    gdr.HandleException(ex)
                    DebtPlus.UI.Common.ErrorHandling.HandleErrors(gdr)
                End Using
            End Try
        End Sub
    End Class
End Namespace
