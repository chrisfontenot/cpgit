#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}
#End Region

Imports System.IO
Imports System.Reflection
Imports DebtPlus.Interfaces.Client
Imports DebtPlus.Utils

Namespace Resources
    Public Class ResourceReportClass
        Inherits Template.TemplateXtraReportClass
        Implements IClient

        ''' <summary>
        ''' Create an instance of our report
        ''' </summary>
        Public Sub New()
            MyBase.New()
            InitializeComponent()

            ' Reset the parameters so that they represent the default settings of "not supplied"
            Parameter_Client = -1
            Parameter_office = -1
            Parameter_PostalCode = String.Empty
            Parameter_ResourceTypes = Nothing

            ' Register the event handlers
            AddHandler BeforePrint, AddressOf ResourceReportClass_BeforePrint
            AddHandler XrLabel_Detail.BeforePrint, AddressOf XrLabel_Detail_BeforePrint
            AddHandler XrLabel_Description.BeforePrint, AddressOf XrLabel_Description_BeforePrint
        End Sub

        ''' <summary>
        ''' Client parameter for the report
        ''' </summary>
        ''' <remarks>
        ''' Implements IClient.ClientID
        ''' </remarks>
        Public Property ClientID As Int32 Implements IClient.ClientId
            Get
                Return Parameter_Client
            End Get
            Set(value As Int32)
                Parameter_Client = value
            End Set
        End Property

        ''' <summary>
        ''' Client parameter for the report
        ''' </summary>
        Public Property Parameter_Client() As System.Int32
            Get
                Dim parm As DevExpress.XtraReports.Parameters.Parameter = FindParameter("ParameterClient")
                Return If(parm Is Nothing, -1, Convert.ToInt32(parm.Value))
            End Get
            Set(ByVal Value As System.Int32)
                SetParameter("ParameterClient", GetType(Int32), Value, "Client ID", False)
            End Set
        End Property

        ''' <summary>
        ''' ZIPcode parameter for the report
        ''' </summary>
        Public Property Parameter_PostalCode() As String
            Get
                Dim parm As DevExpress.XtraReports.Parameters.Parameter = FindParameter("ParameterPostalCode")
                Return If(parm Is Nothing, String.Empty, Convert.ToString(parm.Value, System.Globalization.CultureInfo.InvariantCulture))
            End Get
            Set(ByVal Value As String)
                SetParameter("ParameterPostalCode", GetType(String), Value, "Client PostalCode", False)
            End Set
        End Property

        ''' <summary>
        ''' Office parameter for the report
        ''' </summary>
        Public Property Parameter_office() As System.Int32
            Get
                Dim parm As DevExpress.XtraReports.Parameters.Parameter = FindParameter("ParameterOffice")
                Return If(parm Is Nothing, -1, Convert.ToString(parm.Value, System.Globalization.CultureInfo.InvariantCulture))
            End Get
            Set(ByVal Value As System.Int32)
                SetParameter("ParameterOffice", GetType(Int32), Value, "Office ID", False)
            End Set
        End Property

        ''' <summary>
        ''' Resouce Types parameter for the report
        ''' </summary>
        Public Property Parameter_ResourceTypes() As System.Collections.ArrayList
            Get
                Dim parm As DevExpress.XtraReports.Parameters.Parameter = FindParameter("ParameterResourceTypes")
                Dim strValue As String = If(parm Is Nothing, String.Empty, Convert.ToString(parm.Value, System.Globalization.CultureInfo.InvariantCulture))
                If strValue <> String.Empty Then
                    Dim col As New System.Collections.ArrayList()
                    Dim StrValues() As String = strValue.Split(","c)
                    For Each Str As String In StrValues
                        Str = Str.Trim()
                        If Str <> String.Empty Then
                            col.Add(Str)
                        End If
                    Next
                    Return col
                End If
                Return Nothing
            End Get

            Set(ByVal Value As System.Collections.ArrayList)
                Dim strValue As String = String.Empty
                If Value IsNot Nothing Then
                    Dim sb As New System.Text.StringBuilder
                    For Each item As System.Int32 In Value
                        sb.AppendFormat(",{0:f0}", item)
                    Next
                    If sb.Length > 0 Then sb.Remove(0, 1)
                    strValue = sb.ToString()
                End If
                SetParameter("ParameterResourceTypes", GetType(String), strValue, "Resource IDs", False)
            End Set
        End Property

        ''' <summary>
        ''' Define the report parameters from other applications
        ''' </summary>
        Public Overrides Sub SetReportParameter(ByVal Parameter As String, ByVal Value As Object)
            Select Case Parameter
                Case "Client"
                    Parameter_Client = Value
                Case "Office"
                    Parameter_office = Value
                Case "PostalCode"
                    Parameter_PostalCode = Value
                Case "ResourceTypes"
                    Parameter_ResourceTypes = Value
                Case Else
                    MyBase.SetReportParameter(Parameter, Value)
            End Select
        End Sub

        ''' <summary>
        ''' Do we need to ask for the report parameters?
        ''' </summary>
        Public Overrides Function NeedParameters() As Boolean
            Return Parameter_ResourceTypes Is Nothing OrElse Parameter_office <= 0
        End Function

        ''' <summary>
        ''' Request the parameters for the report
        ''' </summary>
        Public Overrides Function RequestReportParameters() As System.Windows.Forms.DialogResult
            Dim answer As System.Windows.Forms.DialogResult = DialogResult.OK
            If NeedParameters() Then
                Using dialogForm As New RequestParametersForm(Me)
                    With dialogForm
                        answer = .ShowDialog()
                        Parameter_office = .Office
                        Parameter_ResourceTypes = .TypeList
                    End With
                End Using
            End If
            Return answer
        End Function

        ''' <summary>
        ''' Title associated with the report
        ''' </summary>
        Public Overrides ReadOnly Property ReportTitle() As String
            Get
                Return "Resource Information"
            End Get
        End Property

#Region " Component Designer generated code "

        'Component overrides dispose to clean up the component list.
        Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
            If disposing Then
                If components IsNot Nothing Then
                    components.Dispose()
                End If
            End If
            MyBase.Dispose(disposing)
        End Sub

        'Required by the Component Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Component Designer
        'It can be modified using the Component Designer.
        'Do not modify it using the code editor.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
            Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(ResourceReportClass))
            Me.XrControlStyle1 = New DevExpress.XtraReports.UI.XRControlStyle()
            Me.XrControlStyle2 = New DevExpress.XtraReports.UI.XRControlStyle()
            Me.GroupHeader1 = New DevExpress.XtraReports.UI.GroupHeaderBand()
            Me.XrLabel_Description = New DevExpress.XtraReports.UI.XRLabel()
            Me.GroupFooter1 = New DevExpress.XtraReports.UI.GroupFooterBand()
            Me.ParameterClient = New DevExpress.XtraReports.Parameters.Parameter()
            Me.ParameterOffice = New DevExpress.XtraReports.Parameters.Parameter()
            Me.ParameterResourceTypes = New DevExpress.XtraReports.Parameters.Parameter()
            Me.ParameterPostalCode = New DevExpress.XtraReports.Parameters.Parameter()
            Me.XrPanel1 = New DevExpress.XtraReports.UI.XRPanel()
            Me.XrLabel_Detail = New DevExpress.XtraReports.UI.XRRichText()
            Me.XrLabel_Name = New DevExpress.XtraReports.UI.XRLabel()
            CType(Me.XrLabel_Detail, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
            '
            'XrLabel_Title
            '
            Me.XrLabel_Title.StylePriority.UseFont = False
            '
            'XrPageInfo_PageNumber
            '
            Me.XrPageInfo_PageNumber.StylePriority.UseTextAlignment = False
            '
            'XRLabel_Agency_Name
            '
            Me.XRLabel_Agency_Name.StylePriority.UseTextAlignment = False
            '
            'XrLabel_Agency_Address3
            '
            Me.XrLabel_Agency_Address3.StylePriority.UseFont = False
            Me.XrLabel_Agency_Address3.StylePriority.UseTextAlignment = False
            '
            'XrLabel_Agency_Address1
            '
            Me.XrLabel_Agency_Address1.StylePriority.UseFont = False
            Me.XrLabel_Agency_Address1.StylePriority.UseTextAlignment = False
            '
            'XrLabel_Agency_Phone
            '
            Me.XrLabel_Agency_Phone.StylePriority.UseFont = False
            Me.XrLabel_Agency_Phone.StylePriority.UseTextAlignment = False
            '
            'XrLabel_Agency_Address2
            '
            Me.XrLabel_Agency_Address2.StylePriority.UseFont = False
            Me.XrLabel_Agency_Address2.StylePriority.UseTextAlignment = False
            '
            'Detail
            '
            Me.Detail.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrPanel1})
            Me.Detail.HeightF = 43.2083!
            Me.Detail.MultiColumn.ColumnCount = 2
            Me.Detail.MultiColumn.ColumnSpacing = 5.0!
            Me.Detail.MultiColumn.ColumnWidth = 372.0!
            Me.Detail.MultiColumn.Layout = DevExpress.XtraPrinting.ColumnLayout.AcrossThenDown
            Me.Detail.MultiColumn.Mode = DevExpress.XtraReports.UI.MultiColumnMode.UseColumnWidth
            '
            'XrControlStyle1
            '
            Me.XrControlStyle1.BackColor = System.Drawing.Color.Transparent
            Me.XrControlStyle1.ForeColor = System.Drawing.Color.Black
            Me.XrControlStyle1.Name = "XrControlStyle1"
            '
            'XrControlStyle2
            '
            Me.XrControlStyle2.BackColor = System.Drawing.Color.Transparent
            Me.XrControlStyle2.Font = New System.Drawing.Font("Times New Roman", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.XrControlStyle2.Name = "XrControlStyle2"
            '
            'GroupHeader1
            '
            Me.GroupHeader1.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel_Description})
            Me.GroupHeader1.GroupFields.AddRange(New DevExpress.XtraReports.UI.GroupField() {New DevExpress.XtraReports.UI.GroupField("description", DevExpress.XtraReports.UI.XRColumnSortOrder.Ascending)})
            Me.GroupHeader1.GroupUnion = DevExpress.XtraReports.UI.GroupUnion.WithFirstDetail
            Me.GroupHeader1.HeightF = 55.0!
            Me.GroupHeader1.Name = "GroupHeader1"
            Me.GroupHeader1.RepeatEveryPage = True
            '
            'XrLabel_Description
            '
            Me.XrLabel_Description.BackColor = System.Drawing.Color.Teal
            Me.XrLabel_Description.BorderColor = System.Drawing.Color.Teal
            Me.XrLabel_Description.CanGrow = False
            Me.XrLabel_Description.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "description")})
            Me.XrLabel_Description.Font = New System.Drawing.Font("Comic Sans MS", 16.0!)
            Me.XrLabel_Description.ForeColor = System.Drawing.Color.White
            Me.XrLabel_Description.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 8.0!)
            Me.XrLabel_Description.Name = "XrLabel_Description"
            Me.XrLabel_Description.Scripts.OnBeforePrint = "XrLabel_Description_BeforePrint"
            Me.XrLabel_Description.SizeF = New System.Drawing.SizeF(750.0!, 42.0!)
            Me.XrLabel_Description.Text = "Group Header"
            Me.XrLabel_Description.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
            Me.XrLabel_Description.WordWrap = False
            '
            'GroupFooter1
            '
            Me.GroupFooter1.HeightF = 17.0!
            Me.GroupFooter1.Name = "GroupFooter1"
            '
            'ParameterClient
            '
            Me.ParameterClient.Description = "Client"
            Me.ParameterClient.Name = "ParameterClient"
            Me.ParameterClient.Type = GetType(Integer)
            Me.ParameterClient.Value = -1
            Me.ParameterClient.Visible = False
            '
            'ParameterOffice
            '
            Me.ParameterOffice.Description = "Office"
            Me.ParameterOffice.Name = "ParameterOffice"
            Me.ParameterOffice.Type = GetType(Integer)
            Me.ParameterOffice.Value = 0
            Me.ParameterOffice.Visible = False
            '
            'ParameterResourceTypes
            '
            Me.ParameterResourceTypes.Description = "Type(s)"
            Me.ParameterResourceTypes.Name = "ParameterResourceTypes"
            Me.ParameterResourceTypes.Value = ""
            Me.ParameterResourceTypes.Visible = False
            '
            'ParameterPostalCode
            '
            Me.ParameterPostalCode.Description = "ZIP Code"
            Me.ParameterPostalCode.Name = "ParameterPostalCode"
            Me.ParameterPostalCode.Value = ""
            Me.ParameterPostalCode.Visible = False
            '
            'XrPanel1
            '
            Me.XrPanel1.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel_Detail, Me.XrLabel_Name})
            Me.XrPanel1.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
            Me.XrPanel1.Name = "XrPanel1"
            Me.XrPanel1.SizeF = New System.Drawing.SizeF(367.0!, 43.2083!)
            '
            'XrLabel_Detail
            '
            Me.XrLabel_Detail.LocationFloat = New DevExpress.Utils.PointFloat(25.62497!, 25.0!)
            Me.XrLabel_Detail.Name = "XrLabel_Detail"
            Me.XrLabel_Detail.Scripts.OnBeforePrint = "XrLabel_Detail_BeforePrint"
            Me.XrLabel_Detail.SerializableRtfString = resources.GetString("XrLabel_Detail.SerializableRtfString")
            Me.XrLabel_Detail.SizeF = New System.Drawing.SizeF(341.375!, 18.20831!)
            '
            'XrLabel_Name
            '
            Me.XrLabel_Name.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "name")})
            Me.XrLabel_Name.Font = New System.Drawing.Font("Times New Roman", 12.0!, System.Drawing.FontStyle.Bold)
            Me.XrLabel_Name.ForeColor = System.Drawing.Color.Black
            Me.XrLabel_Name.KeepTogether = True
            Me.XrLabel_Name.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
            Me.XrLabel_Name.Name = "XrLabel_Name"
            Me.XrLabel_Name.SizeF = New System.Drawing.SizeF(367.0!, 25.0!)
            Me.XrLabel_Name.Text = "Name"
            '
            'ResourceReportClass
            '
            Me.Bands.AddRange(New DevExpress.XtraReports.UI.Band() {Me.Detail, Me.PageHeader, Me.PageFooter, Me.TopMarginBand1, Me.BottomMarginBand1, Me.GroupHeader1, Me.GroupFooter1})
            Me.Margins = New System.Drawing.Printing.Margins(50, 50, 25, 25)
            Me.Parameters.AddRange(New DevExpress.XtraReports.Parameters.Parameter() {Me.ParameterClient, Me.ParameterOffice, Me.ParameterResourceTypes, Me.ParameterPostalCode})
            Me.RequestParameters = False
            Me.ScriptLanguage = DevExpress.XtraReports.ScriptLanguage.VisualBasic
            Me.ScriptReferencesString = resources.GetString("$this.ScriptReferencesString")
            Me.Scripts.OnBeforePrint = "ResourceReportClass_BeforePrint"
            Me.ScriptsSource = resources.GetString("$this.ScriptsSource")
            Me.ShowPrintMarginsWarning = False
            Me.ShowPrintStatusDialog = False
            Me.StyleSheet.AddRange(New DevExpress.XtraReports.UI.XRControlStyle() {Me.XrControlStyle_Header, Me.XrControlStyle_Totals, Me.XrControlStyle_HeaderPannel, Me.XrControlStyle_GroupHeader, Me.XrControlStyle1, Me.XrControlStyle2})
            Me.Version = "11.2"
            Me.Controls.SetChildIndex(Me.GroupFooter1, 0)
            Me.Controls.SetChildIndex(Me.GroupHeader1, 0)
            Me.Controls.SetChildIndex(Me.BottomMarginBand1, 0)
            Me.Controls.SetChildIndex(Me.TopMarginBand1, 0)
            Me.Controls.SetChildIndex(Me.PageFooter, 0)
            Me.Controls.SetChildIndex(Me.PageHeader, 0)
            Me.Controls.SetChildIndex(Me.Detail, 0)
            CType(Me.XrLabel_Detail, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me, System.ComponentModel.ISupportInitialize).EndInit()

        End Sub
        Friend WithEvents GroupHeader1 As DevExpress.XtraReports.UI.GroupHeaderBand
        Friend WithEvents XrLabel_Description As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents GroupFooter1 As DevExpress.XtraReports.UI.GroupFooterBand
        Friend WithEvents XrControlStyle1 As DevExpress.XtraReports.UI.XRControlStyle
        Friend WithEvents XrControlStyle2 As DevExpress.XtraReports.UI.XRControlStyle
        Friend WithEvents ParameterClient As DevExpress.XtraReports.Parameters.Parameter
        Friend WithEvents ParameterOffice As DevExpress.XtraReports.Parameters.Parameter
        Friend WithEvents ParameterResourceTypes As DevExpress.XtraReports.Parameters.Parameter
        Friend WithEvents ParameterPostalCode As DevExpress.XtraReports.Parameters.Parameter
        Friend WithEvents XrPanel1 As DevExpress.XtraReports.UI.XRPanel
        Friend WithEvents XrLabel_Name As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_Detail As DevExpress.XtraReports.UI.XRRichText
#End Region

        ' *************** MOVED TO SCRIPTS **********************************
        Private ReadOnly ds As New System.Data.DataSet("ds")

        Private Sub ResourceReportClass_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs)
            Dim rpt As DevExpress.XtraReports.UI.XtraReport = CType(sender, DevExpress.XtraReports.UI.XtraReport)
            Dim sqlInfo As DebtPlus.LINQ.SQLInfoClass = DebtPlus.LINQ.SQLInfoClass.getDefault()
            Const TableName As String = "rpt_ResourceGuide"

            ds.Clear()

            Dim cn As New System.Data.SqlClient.SqlConnection(sqlInfo.ConnectionString)
            Try
                cn.Open()
                Using cmd As New System.Data.SqlClient.SqlCommand()
                    cmd.Connection = cn
                    cmd.CommandText = "rpt_ResourceGuide"
                    cmd.CommandType = System.Data.CommandType.StoredProcedure
                    cmd.Parameters.Add("@office", System.Data.SqlDbType.Int).Value = rpt.Parameters("ParameterOffice").Value
                    cmd.Parameters.Add("@types", System.Data.SqlDbType.VarChar, 256).Value = rpt.Parameters("ParameterResourceTypes").Value
                    cmd.CommandTimeout = 0

                    Using da As New System.Data.SqlClient.SqlDataAdapter(cmd)
                        da.Fill(ds, TableName)
                        rpt.DataSource = New System.Data.DataView(ds.Tables(TableName), String.Empty, "description,name", System.Data.DataViewRowState.CurrentRows)
                    End Using
                End Using

            Catch ex As System.Data.SqlClient.SqlException
                Using gdr As New DebtPlus.Repository.GetDataResult()
                    gdr.HandleException(ex)
                    DebtPlus.UI.Common.ErrorHandling.HandleErrors(gdr, "Error reading report information")
                End Using

            Finally
                cn.Dispose()
            End Try
        End Sub

        Private Sub XrLabel_Detail_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs)
            With CType(sender, DevExpress.XtraReports.UI.XRRichText)
                Dim rpt As DevExpress.XtraReports.UI.XtraReport = CType(.Report, DevExpress.XtraReports.UI.XtraReport)
                Dim sb As New System.Text.StringBuilder

                '-- Include the address information
                For Each key As String In New String() {"address1", "address2", "address3", "telephone", "telephone2", "fax", "email", "www"}
                    Dim Value As String = DebtPlus.Utils.Nulls.DStr(rpt.GetCurrentColumnValue(key)).Trim
                    If Value <> String.Empty Then
                        Value = Value.Replace("\", "\\")

                        Select Case key
                            Case "telephone", "telephone2"
                                Value = String.Format("\b Telephone: \b0 {0}", Value)
                            Case "fax"
                                Value = String.Format("\b FAX: \b0 {0}", Value)
                            Case "email"
                                Value = String.Format("\b E-Mail: \b0 {0}", Value)
                            Case "www"
                                Value = String.Format("\b URL: \b0 {0}", Value)
                            Case Else
                        End Select
                        sb.Append("\par ")
                        sb.Append(Value)
                    End If
                Next

                If sb.Length > 0 Then sb.Remove(0, 5)
                sb.Insert(0, "{\rtf1 ")
                sb.Append("\par }")
                .Rtf = sb.ToString
            End With
        End Sub

        Private Sub XrLabel_Description_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs)
            With CType(sender, DevExpress.XtraReports.UI.XRLabel)
                Dim rpt As DevExpress.XtraReports.UI.XtraReport = CType(.Report, DevExpress.XtraReports.UI.XtraReport)
                Dim Value As String = Convert.ToString(rpt.GetCurrentColumnValue("description"))

                Dim LastValue As String = String.Empty
                Dim Match As Boolean = Value.CompareTo(.Tag) = 0
                .Tag = Value
                If Match Then Value += " (Cont.)"
                .Text = Value
            End With
        End Sub
    End Class
End Namespace
