#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}

#End Region

Option Compare Binary
Option Explicit On
Option Strict On

Imports System.ComponentModel
Imports DebtPlus.Reports.Template

Imports System.Drawing.Printing
Imports System.Data.SqlClient

Namespace Housing.LoanInformation
    Public Class HousingLoanInformationReport
        Inherits TemplateXtraReportClass


        ''' <summary>
        ''' Create an instance of our report
        ''' </summary>
        Public Sub New()
            MyBase.New()
            InitializeComponent()
            AddHandler BeforePrint, AddressOf Report_BeforePrint
        End Sub

#Region " Component Designer generated code "

        'Component overrides dispose to clean up the component list.
        Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
            If disposing Then
                If components IsNot Nothing Then
                    components.Dispose()
                End If
            End If
            MyBase.Dispose(disposing)
        End Sub

        Friend WithEvents XrPanel1 As DevExpress.XtraReports.UI.XRPanel
        Friend WithEvents XrControlStyle1 As DevExpress.XtraReports.UI.XRControlStyle
        Friend WithEvents XrControlStyle2 As DevExpress.XtraReports.UI.XRControlStyle
        Friend WithEvents XrLabel_hud_id As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_loan_position As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_loan_type As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_name As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_active_status As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_amount_owed As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_mos_past_due As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_lender_name As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_lender_loan_number As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel9 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel8 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel7 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel6 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel5 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel4 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel3 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel2 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel1 As DevExpress.XtraReports.UI.XRLabel

        'Required by the Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Designer
        'It can be modified using the Designer.
        'Do not modify it using the code editor.
        <System.Diagnostics.DebuggerStepThrough()>
        Private Sub InitializeComponent()
            Me.XrPanel1 = New DevExpress.XtraReports.UI.XRPanel
            Me.XrLabel9 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel8 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel7 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel6 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel5 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel4 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel3 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel2 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel1 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrControlStyle1 = New DevExpress.XtraReports.UI.XRControlStyle
            Me.XrControlStyle2 = New DevExpress.XtraReports.UI.XRControlStyle
            Me.XrLabel_hud_id = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_lender_loan_number = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_lender_name = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_mos_past_due = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_amount_owed = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_active_status = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_name = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_loan_type = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_loan_position = New DevExpress.XtraReports.UI.XRLabel
            CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
            '
            'Detail
            '
            Me.Detail.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel_loan_position, Me.XrLabel_loan_type, Me.XrLabel_name, Me.XrLabel_active_status, Me.XrLabel_amount_owed, Me.XrLabel_mos_past_due, Me.XrLabel_lender_name, Me.XrLabel_lender_loan_number, Me.XrLabel_hud_id})
            Me.Detail.HeightF = 15.0!
            Me.Detail.StyleName = "XrControlStyle2"
            '
            'PageHeader
            '
            Me.PageHeader.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrPanel1})
            Me.PageHeader.HeightF = 143.0!
            Me.PageHeader.Controls.SetChildIndex(Me.XrPanel1, 0)
            Me.PageHeader.Controls.SetChildIndex(Me.XrPanel_AgencyAddress, 0)
            Me.PageHeader.Controls.SetChildIndex(Me.XrLabel_Title, 0)
            Me.PageHeader.Controls.SetChildIndex(Me.XrLine_Title, 0)
            Me.PageHeader.Controls.SetChildIndex(Me.XrLabel_Subtitle, 0)
            Me.PageHeader.Controls.SetChildIndex(Me.XrPageInfo1, 0)
            '
            'XrLabel_Title
            '
            Me.XrLabel_Title.StylePriority.UseFont = False
            '
            'XrPageInfo_PageNumber
            '
            Me.XrPageInfo_PageNumber.LocationFloat = New DevExpress.Utils.PointFloat(917.0!, 8.0!)
            Me.XrPageInfo_PageNumber.StylePriority.UseTextAlignment = False
            '
            'XrPanel_AgencyAddress
            '
            Me.XrPanel_AgencyAddress.LocationFloat = New DevExpress.Utils.PointFloat(742.0!, 0.0!)
            '
            'XRLabel_Agency_Name
            '
            Me.XRLabel_Agency_Name.StylePriority.UseTextAlignment = False
            '
            'XrLabel_Agency_Address3
            '
            Me.XrLabel_Agency_Address3.StylePriority.UseFont = False
            Me.XrLabel_Agency_Address3.StylePriority.UseTextAlignment = False
            '
            'XrLabel_Agency_Address1
            '
            Me.XrLabel_Agency_Address1.StylePriority.UseFont = False
            Me.XrLabel_Agency_Address1.StylePriority.UseTextAlignment = False
            '
            'XrLabel_Agency_Phone
            '
            Me.XrLabel_Agency_Phone.StylePriority.UseFont = False
            Me.XrLabel_Agency_Phone.StylePriority.UseTextAlignment = False
            '
            'XrLabel_Agency_Address2
            '
            Me.XrLabel_Agency_Address2.StylePriority.UseFont = False
            Me.XrLabel_Agency_Address2.StylePriority.UseTextAlignment = False
            '
            'XrPanel1
            '
            Me.XrPanel1.BackColor = System.Drawing.Color.Teal
            Me.XrPanel1.BorderColor = System.Drawing.Color.Teal
            Me.XrPanel1.CanGrow = False
            Me.XrPanel1.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel9, Me.XrLabel8, Me.XrLabel7, Me.XrLabel6, Me.XrLabel5, Me.XrLabel4, Me.XrLabel3, Me.XrLabel2, Me.XrLabel1})
            Me.XrPanel1.KeepTogether = False
            Me.XrPanel1.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 117.0!)
            Me.XrPanel1.Name = "XrPanel1"
            Me.XrPanel1.SizeF = New System.Drawing.SizeF(1050.0!, 18.0!)
            Me.XrPanel1.StyleName = "XrControlStyle1"
            Me.XrPanel1.StylePriority.UseBackColor = False
            Me.XrPanel1.StylePriority.UseBorderColor = False
            '
            'XrLabel9
            '
            Me.XrLabel9.LocationFloat = New DevExpress.Utils.PointFloat(725.0!, 0.0!)
            Me.XrLabel9.Name = "XrLabel9"
            Me.XrLabel9.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel9.SizeF = New System.Drawing.SizeF(133.0!, 15.0!)
            Me.XrLabel9.Text = "LOAN NUMBER"
            '
            'XrLabel8
            '
            Me.XrLabel8.LocationFloat = New DevExpress.Utils.PointFloat(341.0!, 0.0!)
            Me.XrLabel8.Name = "XrLabel8"
            Me.XrLabel8.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel8.SizeF = New System.Drawing.SizeF(100.0!, 15.0!)
            Me.XrLabel8.Text = "POSITION"
            '
            'XrLabel7
            '
            Me.XrLabel7.LocationFloat = New DevExpress.Utils.PointFloat(450.0!, 0.0!)
            Me.XrLabel7.Name = "XrLabel7"
            Me.XrLabel7.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel7.SizeF = New System.Drawing.SizeF(100.0!, 15.0!)
            Me.XrLabel7.Text = "TYPE"
            '
            'XrLabel6
            '
            Me.XrLabel6.LocationFloat = New DevExpress.Utils.PointFloat(91.33333!, 0.000007629395!)
            Me.XrLabel6.Name = "XrLabel6"
            Me.XrLabel6.Padding = New DevExpress.XtraPrinting.PaddingInfo(5, 2, 0, 0, 100.0!)
            Me.XrLabel6.SizeF = New System.Drawing.SizeF(199.6667!, 14.99999!)
            Me.XrLabel6.StylePriority.UsePadding = False
            Me.XrLabel6.Text = "NAME"
            '
            'XrLabel5
            '
            Me.XrLabel5.LocationFloat = New DevExpress.Utils.PointFloat(300.0!, 0.9999911!)
            Me.XrLabel5.Name = "XrLabel5"
            Me.XrLabel5.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel5.SizeF = New System.Drawing.SizeF(33.0!, 15.0!)
            Me.XrLabel5.StylePriority.UseTextAlignment = False
            Me.XrLabel5.Text = "ST"
            Me.XrLabel5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
            '
            'XrLabel4
            '
            Me.XrLabel4.LocationFloat = New DevExpress.Utils.PointFloat(866.0!, 0.0!)
            Me.XrLabel4.Name = "XrLabel4"
            Me.XrLabel4.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel4.SizeF = New System.Drawing.SizeF(91.0!, 15.0!)
            Me.XrLabel4.StylePriority.UseTextAlignment = False
            Me.XrLabel4.Text = "OWED"
            Me.XrLabel4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel3
            '
            Me.XrLabel3.LocationFloat = New DevExpress.Utils.PointFloat(962.0!, 0.0!)
            Me.XrLabel3.Name = "XrLabel3"
            Me.XrLabel3.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel3.SizeF = New System.Drawing.SizeF(79.0!, 15.0!)
            Me.XrLabel3.StylePriority.UseTextAlignment = False
            Me.XrLabel3.Text = "PAST DUE"
            Me.XrLabel3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel2
            '
            Me.XrLabel2.LocationFloat = New DevExpress.Utils.PointFloat(558.0!, 0.0!)
            Me.XrLabel2.Name = "XrLabel2"
            Me.XrLabel2.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel2.SizeF = New System.Drawing.SizeF(158.0!, 15.0!)
            Me.XrLabel2.Text = "LENDER"
            '
            'XrLabel1
            '
            Me.XrLabel1.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.000007629395!)
            Me.XrLabel1.Name = "XrLabel1"
            Me.XrLabel1.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 5, 0, 0, 100.0!)
            Me.XrLabel1.SizeF = New System.Drawing.SizeF(91.33333!, 14.99999!)
            Me.XrLabel1.StylePriority.UsePadding = False
            Me.XrLabel1.StylePriority.UseTextAlignment = False
            Me.XrLabel1.Text = "ID"
            Me.XrLabel1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrControlStyle1
            '
            Me.XrControlStyle1.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.XrControlStyle1.ForeColor = System.Drawing.Color.White
            Me.XrControlStyle1.Name = "XrControlStyle1"
            Me.XrControlStyle1.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
            Me.XrControlStyle1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            '
            'XrControlStyle2
            '
            Me.XrControlStyle2.Name = "XrControlStyle2"
            Me.XrControlStyle2.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
            Me.XrControlStyle2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            '
            'XrLabel_hud_id
            '
            Me.XrLabel_hud_id.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "hud_id")})
            Me.XrLabel_hud_id.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
            Me.XrLabel_hud_id.Name = "XrLabel_hud_id"
            Me.XrLabel_hud_id.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 5, 0, 0, 100.0!)
            Me.XrLabel_hud_id.SizeF = New System.Drawing.SizeF(91.33333!, 14.99999!)
            Me.XrLabel_hud_id.StylePriority.UsePadding = False
            Me.XrLabel_hud_id.StylePriority.UseTextAlignment = False
            Me.XrLabel_hud_id.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel_lender_loan_number
            '
            Me.XrLabel_lender_loan_number.CanGrow = False
            Me.XrLabel_lender_loan_number.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "lender_loan_number")})
            Me.XrLabel_lender_loan_number.LocationFloat = New DevExpress.Utils.PointFloat(725.0!, 0.0!)
            Me.XrLabel_lender_loan_number.Name = "XrLabel_lender_loan_number"
            Me.XrLabel_lender_loan_number.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_lender_loan_number.SizeF = New System.Drawing.SizeF(133.0!, 15.0!)
            Me.XrLabel_lender_loan_number.WordWrap = False
            '
            'XrLabel_lender_name
            '
            Me.XrLabel_lender_name.CanGrow = False
            Me.XrLabel_lender_name.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "lender_name")})
            Me.XrLabel_lender_name.LocationFloat = New DevExpress.Utils.PointFloat(558.0!, 0.0!)
            Me.XrLabel_lender_name.Name = "XrLabel_lender_name"
            Me.XrLabel_lender_name.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_lender_name.SizeF = New System.Drawing.SizeF(158.0!, 15.0!)
            Me.XrLabel_lender_name.WordWrap = False
            '
            'XrLabel_mos_past_due
            '
            Me.XrLabel_mos_past_due.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "mos_past_due")})
            Me.XrLabel_mos_past_due.LocationFloat = New DevExpress.Utils.PointFloat(962.0!, 0.0!)
            Me.XrLabel_mos_past_due.Name = "XrLabel_mos_past_due"
            Me.XrLabel_mos_past_due.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_mos_past_due.SizeF = New System.Drawing.SizeF(79.0!, 15.0!)
            Me.XrLabel_mos_past_due.StylePriority.UseTextAlignment = False
            Me.XrLabel_mos_past_due.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrLabel_mos_past_due.WordWrap = False
            '
            'XrLabel_amount_owed
            '
            Me.XrLabel_amount_owed.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "amount_owed", "{0:c}")})
            Me.XrLabel_amount_owed.LocationFloat = New DevExpress.Utils.PointFloat(866.0!, 0.0!)
            Me.XrLabel_amount_owed.Name = "XrLabel_amount_owed"
            Me.XrLabel_amount_owed.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_amount_owed.SizeF = New System.Drawing.SizeF(91.0!, 15.0!)
            Me.XrLabel_amount_owed.StylePriority.UseTextAlignment = False
            Me.XrLabel_amount_owed.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrLabel_amount_owed.WordWrap = False
            '
            'XrLabel_active_status
            '
            Me.XrLabel_active_status.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "active_status")})
            Me.XrLabel_active_status.LocationFloat = New DevExpress.Utils.PointFloat(300.0!, 0.0!)
            Me.XrLabel_active_status.Name = "XrLabel_active_status"
            Me.XrLabel_active_status.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_active_status.SizeF = New System.Drawing.SizeF(33.0!, 15.0!)
            Me.XrLabel_active_status.StylePriority.UseTextAlignment = False
            Me.XrLabel_active_status.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
            Me.XrLabel_active_status.WordWrap = False
            '
            'XrLabel_name
            '
            Me.XrLabel_name.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "name")})
            Me.XrLabel_name.CanGrow = False
            Me.XrLabel_name.LocationFloat = New DevExpress.Utils.PointFloat(91.33333!, 0.0!)
            Me.XrLabel_name.Name = "XrLabel_name"
            Me.XrLabel_name.Padding = New DevExpress.XtraPrinting.PaddingInfo(5, 2, 0, 0, 100.0!)
            Me.XrLabel_name.SizeF = New System.Drawing.SizeF(199.6667!, 15.0!)
            Me.XrLabel_name.StylePriority.UsePadding = False
            Me.XrLabel_name.WordWrap = False
            '
            'XrLabel_loan_type
            '
            Me.XrLabel_loan_type.CanGrow = False
            Me.XrLabel_loan_type.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "loan_type")})
            Me.XrLabel_loan_type.LocationFloat = New DevExpress.Utils.PointFloat(450.0!, 0.0!)
            Me.XrLabel_loan_type.Name = "XrLabel_loan_type"
            Me.XrLabel_loan_type.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_loan_type.SizeF = New System.Drawing.SizeF(100.0!, 15.0!)
            Me.XrLabel_loan_type.WordWrap = False
            '
            'XrLabel_loan_position
            '
            Me.XrLabel_loan_position.CanGrow = False
            Me.XrLabel_loan_position.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "loan_position")})
            Me.XrLabel_loan_position.LocationFloat = New DevExpress.Utils.PointFloat(341.0!, 0.0!)
            Me.XrLabel_loan_position.Name = "XrLabel_loan_position"
            Me.XrLabel_loan_position.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_loan_position.SizeF = New System.Drawing.SizeF(100.0!, 15.0!)
            Me.XrLabel_loan_position.WordWrap = False
            '
            'HousingLoanInformationReport
            '
            Me.Bands.AddRange(New DevExpress.XtraReports.UI.Band() {Me.Detail, Me.PageHeader, Me.PageFooter, Me.TopMarginBand1, Me.BottomMarginBand1})
            Me.Landscape = True
            Me.PageHeight = 850
            Me.PageWidth = 1100
            Me.StyleSheet.AddRange(New DevExpress.XtraReports.UI.XRControlStyle() {Me.XrControlStyle_Header, Me.XrControlStyle_Totals, Me.XrControlStyle_HeaderPannel, Me.XrControlStyle_GroupHeader, Me.XrControlStyle1, Me.XrControlStyle2})
            Me.Version = "10.2"
            CType(Me, System.ComponentModel.ISupportInitialize).EndInit()
        End Sub

#End Region

        Public Overrides ReadOnly Property ReportTitle() As String
            Get
                Return "Housing Information"
            End Get
        End Property

        Dim ds As New DataSet("ds")

        Private Sub Report_BeforePrint(ByVal sender As Object, ByVal e As PrintEventArgs)

            ' Read the transactions
            Dim cn As SqlConnection = New System.Data.SqlClient.SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
            Dim current_cursor As Cursor = Cursor.Current
            Try
                Cursor.Current = Cursors.WaitCursor
                cn.Open()
                Using cmd As SqlCommand = New System.Data.SqlClient.SqlCommand()
                    With cmd
                        .Connection = cn
                        .CommandText = "rpt_housing_loan_info"
                        .CommandType = CommandType.StoredProcedure
                        .CommandTimeout = 0
                    End With

                    Using da As New SqlDataAdapter(cmd)
                        da.Fill(ds, "records")
                    End Using
                End Using

                DataSource = ds.Tables(0).DefaultView

            Catch ex As Exception
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error reading transactions")

            Finally
                If cn IsNot Nothing Then cn.Dispose()
                Cursor.Current = current_cursor
            End Try
        End Sub
    End Class
End Namespace
