﻿Namespace Statistics.Counselor
    <Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
    Partial Class StatisticsCounselorReport
        Inherits DebtPlus.Reports.Template.TemplateXtraReportClass

        'XtraReport overrides dispose to clean up the component list.
        <System.Diagnostics.DebuggerNonUserCode()> _
        Protected Overrides Sub Dispose(ByVal disposing As Boolean)
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
            MyBase.Dispose(disposing)
        End Sub

        'Required by the Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Designer
        'It can be modified using the Designer.  
        'Do not modify it using the code editor.
        <System.Diagnostics.DebuggerStepThrough()> _
        Private Sub InitializeComponent()
            Dim XrSummary1 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary()
            Dim XrSummary2 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary()
            Dim XrSummary3 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary()
            Dim XrSummary4 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary()
            Dim XrSummary5 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary()
            Dim XrSummary6 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary()
            Dim XrSummary7 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary()
            Dim XrSummary8 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary()
            Dim XrSummary9 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary()
            Dim XrSummary10 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary()
            Dim XrSummary11 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary()
            Dim XrSummary12 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary()
            Dim XrSummary13 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary()
            Dim XrSummary14 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary()
            Dim XrSummary15 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary()
            Dim XrSummary16 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary()
            Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(StatisticsCounselorReport))
            Me.XrLabel1 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel2 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel3 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel4 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel5 = New DevExpress.XtraReports.UI.XRLabel()
            Me.GroupHeader1 = New DevExpress.XtraReports.UI.GroupHeaderBand()
            Me.XrLabel7 = New DevExpress.XtraReports.UI.XRLabel()
            Me.GroupFooter1 = New DevExpress.XtraReports.UI.GroupFooterBand()
            Me.XrLabel_g1_disbursement_actual = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_g1_disbursement_expected = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_g1_clients_disbursed = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_g1_fees_paf = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_g1_clients_active = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_g1_counselor_name = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_g1_clients_dropped = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_g1_debt_new_dmp = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_g1_clients_new_dmp = New DevExpress.XtraReports.UI.XRLabel()
            Me.GroupHeader2 = New DevExpress.XtraReports.UI.GroupHeaderBand()
            Me.XrLabel6 = New DevExpress.XtraReports.UI.XRLabel()
            Me.GroupFooter2 = New DevExpress.XtraReports.UI.GroupFooterBand()
            Me.XrLabel_g2_disbursement_actual = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_g2_disbursement_expected = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_g2_clients_disbursed = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_g2_fees_paf = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_g2_clients_active = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_g2_counselor_name = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_g2_clients_dropped = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_g2_debt_new_dmp = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_g2_clients_new_dmp = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_counselor_name = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_clients_new_dmp = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_clients_dropped = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_fees_paf = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_pct_disb = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_disbursement_actual = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_clients_disbursed = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_disbursement_expected = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_clients_active = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_debt_new_dmp = New DevExpress.XtraReports.UI.XRLabel()
            Me.GroupHeader3 = New DevExpress.XtraReports.UI.GroupHeaderBand()
            Me.XrLabel17 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrPanel1 = New DevExpress.XtraReports.UI.XRPanel()
            Me.XrLabel8 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel31 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel30 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel29 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel28 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel27 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel26 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel25 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel24 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel15 = New DevExpress.XtraReports.UI.XRLabel()
            CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
            '
            'PageHeader
            '
            Me.PageHeader.HeightF = 111.125!
            '
            'XrLabel_Title
            '
            Me.XrLabel_Title.StylePriority.UseFont = False
            '
            'XrLabel_Subtitle
            '
            Me.XrLabel_Subtitle.LocationFloat = New DevExpress.Utils.PointFloat(7.999992!, 85.00001!)
            Me.XrLabel_Subtitle.SizeF = New System.Drawing.SizeF(706.0!, 17.00001!)
            '
            'XrPageInfo_PageNumber
            '
            Me.XrPageInfo_PageNumber.LocationFloat = New DevExpress.Utils.PointFloat(918.5834!, 7.999992!)
            Me.XrPageInfo_PageNumber.SizeF = New System.Drawing.SizeF(113.4167!, 17.00001!)
            Me.XrPageInfo_PageNumber.StylePriority.UseTextAlignment = False
            '
            'XrPanel_AgencyAddress
            '
            Me.XrPanel_AgencyAddress.LocationFloat = New DevExpress.Utils.PointFloat(732.0!, 0.0!)
            '
            'XRLabel_Agency_Name
            '
            Me.XRLabel_Agency_Name.StylePriority.UseTextAlignment = False
            '
            'XrLabel_Agency_Address3
            '
            Me.XrLabel_Agency_Address3.StylePriority.UseFont = False
            Me.XrLabel_Agency_Address3.StylePriority.UseTextAlignment = False
            '
            'XrLabel_Agency_Address1
            '
            Me.XrLabel_Agency_Address1.StylePriority.UseFont = False
            Me.XrLabel_Agency_Address1.StylePriority.UseTextAlignment = False
            '
            'XrLabel_Agency_Phone
            '
            Me.XrLabel_Agency_Phone.StylePriority.UseFont = False
            Me.XrLabel_Agency_Phone.StylePriority.UseTextAlignment = False
            '
            'XrLabel_Agency_Address2
            '
            Me.XrLabel_Agency_Address2.StylePriority.UseFont = False
            Me.XrLabel_Agency_Address2.StylePriority.UseTextAlignment = False
            '
            'Detail
            '
            Me.Detail.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel_clients_disbursed, Me.XrLabel_disbursement_actual, Me.XrLabel_disbursement_expected, Me.XrLabel_debt_new_dmp, Me.XrLabel_clients_active, Me.XrLabel_clients_new_dmp, Me.XrLabel_counselor_name, Me.XrLabel_clients_dropped, Me.XrLabel_pct_disb, Me.XrLabel_fees_paf})
            Me.Detail.HeightF = 15.0!
            '
            'XrLabel1
            '
            Me.XrLabel1.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
            Me.XrLabel1.Name = "XrLabel1"
            Me.XrLabel1.SizeF = New System.Drawing.SizeF(100.0!, 23.0!)
            Me.XrLabel1.Text = "XrLabel1"
            '
            'XrLabel2
            '
            Me.XrLabel2.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
            Me.XrLabel2.Name = "XrLabel2"
            Me.XrLabel2.SizeF = New System.Drawing.SizeF(100.0!, 23.0!)
            Me.XrLabel2.Text = "XrLabel2"
            '
            'XrLabel3
            '
            Me.XrLabel3.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
            Me.XrLabel3.Name = "XrLabel3"
            Me.XrLabel3.SizeF = New System.Drawing.SizeF(100.0!, 23.0!)
            Me.XrLabel3.Text = "XrLabel3"
            '
            'XrLabel4
            '
            Me.XrLabel4.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
            Me.XrLabel4.Name = "XrLabel4"
            Me.XrLabel4.SizeF = New System.Drawing.SizeF(100.0!, 23.0!)
            Me.XrLabel4.Text = "XrLabel4"
            '
            'XrLabel5
            '
            Me.XrLabel5.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
            Me.XrLabel5.Name = "XrLabel5"
            Me.XrLabel5.SizeF = New System.Drawing.SizeF(100.0!, 23.0!)
            Me.XrLabel5.Text = "XrLabel5"
            '
            'GroupHeader1
            '
            Me.GroupHeader1.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel7})
            Me.GroupHeader1.GroupFields.AddRange(New DevExpress.XtraReports.UI.GroupField() {New DevExpress.XtraReports.UI.GroupField("office_name", DevExpress.XtraReports.UI.XRColumnSortOrder.Ascending)})
            Me.GroupHeader1.GroupUnion = DevExpress.XtraReports.UI.GroupUnion.WithFirstDetail
            Me.GroupHeader1.HeightF = 42.70833!
            Me.GroupHeader1.Level = 1
            Me.GroupHeader1.Name = "GroupHeader1"
            '
            'XrLabel7
            '
            Me.XrLabel7.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "office_name")})
            Me.XrLabel7.Font = New System.Drawing.Font("Times New Roman", 14.0!, System.Drawing.FontStyle.Bold)
            Me.XrLabel7.ForeColor = System.Drawing.Color.Blue
            Me.XrLabel7.LocationFloat = New DevExpress.Utils.PointFloat(48.95833!, 10.00001!)
            Me.XrLabel7.Name = "XrLabel7"
            Me.XrLabel7.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel7.SizeF = New System.Drawing.SizeF(744.0416!, 22.99998!)
            Me.XrLabel7.StylePriority.UseFont = False
            Me.XrLabel7.StylePriority.UseForeColor = False
            Me.XrLabel7.Text = "XrLabel7"
            '
            'GroupFooter1
            '
            Me.GroupFooter1.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel_g1_disbursement_actual, Me.XrLabel_g1_disbursement_expected, Me.XrLabel_g1_clients_disbursed, Me.XrLabel_g1_fees_paf, Me.XrLabel_g1_clients_active, Me.XrLabel_g1_counselor_name, Me.XrLabel_g1_clients_dropped, Me.XrLabel_g1_debt_new_dmp, Me.XrLabel_g1_clients_new_dmp})
            Me.GroupFooter1.HeightF = 22.0!
            Me.GroupFooter1.Level = 1
            Me.GroupFooter1.Name = "GroupFooter1"
            '
            'XrLabel_g1_disbursement_actual
            '
            Me.XrLabel_g1_disbursement_actual.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "disbursement_actual")})
            Me.XrLabel_g1_disbursement_actual.LocationFloat = New DevExpress.Utils.PointFloat(500.0!, 0.0!)
            Me.XrLabel_g1_disbursement_actual.Name = "XrLabel_g1_disbursement_actual"
            Me.XrLabel_g1_disbursement_actual.SizeF = New System.Drawing.SizeF(100.0!, 15.0!)
            Me.XrLabel_g1_disbursement_actual.StylePriority.UseTextAlignment = False
            XrSummary1.FormatString = "{0:c}"
            XrSummary1.Running = DevExpress.XtraReports.UI.SummaryRunning.Group
            Me.XrLabel_g1_disbursement_actual.Summary = XrSummary1
            Me.XrLabel_g1_disbursement_actual.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel_g1_disbursement_expected
            '
            Me.XrLabel_g1_disbursement_expected.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "disbursement_expected")})
            Me.XrLabel_g1_disbursement_expected.LocationFloat = New DevExpress.Utils.PointFloat(391.0!, 0.0!)
            Me.XrLabel_g1_disbursement_expected.Name = "XrLabel_g1_disbursement_expected"
            Me.XrLabel_g1_disbursement_expected.SizeF = New System.Drawing.SizeF(100.0!, 15.0!)
            Me.XrLabel_g1_disbursement_expected.StylePriority.UseTextAlignment = False
            XrSummary2.FormatString = "{0:c}"
            XrSummary2.Running = DevExpress.XtraReports.UI.SummaryRunning.Group
            Me.XrLabel_g1_disbursement_expected.Summary = XrSummary2
            Me.XrLabel_g1_disbursement_expected.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel_g1_clients_disbursed
            '
            Me.XrLabel_g1_clients_disbursed.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "clients_disbursed")})
            Me.XrLabel_g1_clients_disbursed.LocationFloat = New DevExpress.Utils.PointFloat(325.0!, 0.0!)
            Me.XrLabel_g1_clients_disbursed.Name = "XrLabel_g1_clients_disbursed"
            Me.XrLabel_g1_clients_disbursed.SizeF = New System.Drawing.SizeF(50.0!, 15.0!)
            Me.XrLabel_g1_clients_disbursed.StylePriority.UseTextAlignment = False
            XrSummary3.FormatString = "{0:f0}"
            XrSummary3.Running = DevExpress.XtraReports.UI.SummaryRunning.Group
            Me.XrLabel_g1_clients_disbursed.Summary = XrSummary3
            Me.XrLabel_g1_clients_disbursed.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel_g1_fees_paf
            '
            Me.XrLabel_g1_fees_paf.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "fees_paf")})
            Me.XrLabel_g1_fees_paf.LocationFloat = New DevExpress.Utils.PointFloat(666.0!, 0.0!)
            Me.XrLabel_g1_fees_paf.Name = "XrLabel_g1_fees_paf"
            Me.XrLabel_g1_fees_paf.SizeF = New System.Drawing.SizeF(75.0!, 15.0!)
            Me.XrLabel_g1_fees_paf.StylePriority.UseTextAlignment = False
            XrSummary4.FormatString = "{0:c}"
            XrSummary4.Running = DevExpress.XtraReports.UI.SummaryRunning.Group
            Me.XrLabel_g1_fees_paf.Summary = XrSummary4
            Me.XrLabel_g1_fees_paf.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel_g1_clients_active
            '
            Me.XrLabel_g1_clients_active.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "clients_active")})
            Me.XrLabel_g1_clients_active.LocationFloat = New DevExpress.Utils.PointFloat(236.0!, 0.0!)
            Me.XrLabel_g1_clients_active.Name = "XrLabel_g1_clients_active"
            Me.XrLabel_g1_clients_active.SizeF = New System.Drawing.SizeF(72.0!, 15.0!)
            Me.XrLabel_g1_clients_active.StylePriority.UseTextAlignment = False
            XrSummary5.FormatString = "{0:f0}"
            XrSummary5.Running = DevExpress.XtraReports.UI.SummaryRunning.Group
            Me.XrLabel_g1_clients_active.Summary = XrSummary5
            Me.XrLabel_g1_clients_active.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel_g1_counselor_name
            '
            Me.XrLabel_g1_counselor_name.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "office_name", "{0} TOTALS")})
            Me.XrLabel_g1_counselor_name.LocationFloat = New DevExpress.Utils.PointFloat(8.0!, 0.0!)
            Me.XrLabel_g1_counselor_name.Name = "XrLabel_g1_counselor_name"
            Me.XrLabel_g1_counselor_name.SizeF = New System.Drawing.SizeF(217.0!, 14.99999!)
            Me.XrLabel_g1_counselor_name.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            '
            'XrLabel_g1_clients_dropped
            '
            Me.XrLabel_g1_clients_dropped.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "clients_dropped")})
            Me.XrLabel_g1_clients_dropped.LocationFloat = New DevExpress.Utils.PointFloat(754.0!, 0.0!)
            Me.XrLabel_g1_clients_dropped.Name = "XrLabel_g1_clients_dropped"
            Me.XrLabel_g1_clients_dropped.SizeF = New System.Drawing.SizeF(45.0!, 15.0!)
            Me.XrLabel_g1_clients_dropped.StylePriority.UseTextAlignment = False
            XrSummary6.FormatString = "{0:f0}"
            XrSummary6.Running = DevExpress.XtraReports.UI.SummaryRunning.Group
            Me.XrLabel_g1_clients_dropped.Summary = XrSummary6
            Me.XrLabel_g1_clients_dropped.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel_g1_debt_new_dmp
            '
            Me.XrLabel_g1_debt_new_dmp.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "debt_new_dmp")})
            Me.XrLabel_g1_debt_new_dmp.LocationFloat = New DevExpress.Utils.PointFloat(864.0!, 0.0!)
            Me.XrLabel_g1_debt_new_dmp.Name = "XrLabel_g1_debt_new_dmp"
            Me.XrLabel_g1_debt_new_dmp.SizeF = New System.Drawing.SizeF(79.0!, 15.0!)
            Me.XrLabel_g1_debt_new_dmp.StylePriority.UseTextAlignment = False
            XrSummary7.FormatString = "{0:c}"
            XrSummary7.Running = DevExpress.XtraReports.UI.SummaryRunning.Group
            Me.XrLabel_g1_debt_new_dmp.Summary = XrSummary7
            Me.XrLabel_g1_debt_new_dmp.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel_g1_clients_new_dmp
            '
            Me.XrLabel_g1_clients_new_dmp.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "clients_new_dmp")})
            Me.XrLabel_g1_clients_new_dmp.LocationFloat = New DevExpress.Utils.PointFloat(804.0!, 0.0!)
            Me.XrLabel_g1_clients_new_dmp.Name = "XrLabel_g1_clients_new_dmp"
            Me.XrLabel_g1_clients_new_dmp.SizeF = New System.Drawing.SizeF(45.0!, 15.0!)
            Me.XrLabel_g1_clients_new_dmp.StylePriority.UseTextAlignment = False
            XrSummary8.FormatString = "{0:f0}"
            XrSummary8.Running = DevExpress.XtraReports.UI.SummaryRunning.Group
            Me.XrLabel_g1_clients_new_dmp.Summary = XrSummary8
            Me.XrLabel_g1_clients_new_dmp.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'GroupHeader2
            '
            Me.GroupHeader2.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel6})
            Me.GroupHeader2.GroupFields.AddRange(New DevExpress.XtraReports.UI.GroupField() {New DevExpress.XtraReports.UI.GroupField("period_start", DevExpress.XtraReports.UI.XRColumnSortOrder.Ascending)})
            Me.GroupHeader2.HeightF = 40.0!
            Me.GroupHeader2.Level = 2
            Me.GroupHeader2.Name = "GroupHeader2"
            '
            'XrLabel6
            '
            Me.XrLabel6.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "period_start", "{0:MMMM, yyyy}")})
            Me.XrLabel6.Font = New System.Drawing.Font("Times New Roman", 14.0!, System.Drawing.FontStyle.Bold)
            Me.XrLabel6.ForeColor = System.Drawing.Color.Red
            Me.XrLabel6.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 7.000017!)
            Me.XrLabel6.Name = "XrLabel6"
            Me.XrLabel6.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel6.SizeF = New System.Drawing.SizeF(793.0!, 22.99998!)
            Me.XrLabel6.StylePriority.UseFont = False
            Me.XrLabel6.StylePriority.UseForeColor = False
            Me.XrLabel6.Text = "XrLabel6"
            '
            'GroupFooter2
            '
            Me.GroupFooter2.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel_g2_disbursement_actual, Me.XrLabel_g2_disbursement_expected, Me.XrLabel_g2_clients_disbursed, Me.XrLabel_g2_fees_paf, Me.XrLabel_g2_clients_active, Me.XrLabel_g2_counselor_name, Me.XrLabel_g2_clients_dropped, Me.XrLabel_g2_debt_new_dmp, Me.XrLabel_g2_clients_new_dmp})
            Me.GroupFooter2.HeightF = 23.0!
            Me.GroupFooter2.Level = 2
            Me.GroupFooter2.Name = "GroupFooter2"
            Me.GroupFooter2.PageBreak = DevExpress.XtraReports.UI.PageBreak.AfterBand
            '
            'XrLabel_g2_disbursement_actual
            '
            Me.XrLabel_g2_disbursement_actual.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "disbursement_actual")})
            Me.XrLabel_g2_disbursement_actual.LocationFloat = New DevExpress.Utils.PointFloat(500.0!, 0.0!)
            Me.XrLabel_g2_disbursement_actual.Name = "XrLabel_g2_disbursement_actual"
            Me.XrLabel_g2_disbursement_actual.SizeF = New System.Drawing.SizeF(100.0!, 15.0!)
            Me.XrLabel_g2_disbursement_actual.StylePriority.UseTextAlignment = False
            XrSummary9.FormatString = "{0:c}"
            XrSummary9.Running = DevExpress.XtraReports.UI.SummaryRunning.Group
            Me.XrLabel_g2_disbursement_actual.Summary = XrSummary9
            Me.XrLabel_g2_disbursement_actual.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel_g2_disbursement_expected
            '
            Me.XrLabel_g2_disbursement_expected.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "disbursement_expected")})
            Me.XrLabel_g2_disbursement_expected.LocationFloat = New DevExpress.Utils.PointFloat(391.0!, 0.0!)
            Me.XrLabel_g2_disbursement_expected.Name = "XrLabel_g2_disbursement_expected"
            Me.XrLabel_g2_disbursement_expected.SizeF = New System.Drawing.SizeF(100.0!, 15.0!)
            Me.XrLabel_g2_disbursement_expected.StylePriority.UseTextAlignment = False
            XrSummary10.FormatString = "{0:c}"
            XrSummary10.Running = DevExpress.XtraReports.UI.SummaryRunning.Group
            Me.XrLabel_g2_disbursement_expected.Summary = XrSummary10
            Me.XrLabel_g2_disbursement_expected.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel_g2_clients_disbursed
            '
            Me.XrLabel_g2_clients_disbursed.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "clients_disbursed")})
            Me.XrLabel_g2_clients_disbursed.LocationFloat = New DevExpress.Utils.PointFloat(325.0!, 0.0!)
            Me.XrLabel_g2_clients_disbursed.Name = "XrLabel_g2_clients_disbursed"
            Me.XrLabel_g2_clients_disbursed.SizeF = New System.Drawing.SizeF(50.0!, 15.0!)
            Me.XrLabel_g2_clients_disbursed.StylePriority.UseTextAlignment = False
            XrSummary11.FormatString = "{0:f0}"
            XrSummary11.Running = DevExpress.XtraReports.UI.SummaryRunning.Group
            Me.XrLabel_g2_clients_disbursed.Summary = XrSummary11
            Me.XrLabel_g2_clients_disbursed.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel_g2_fees_paf
            '
            Me.XrLabel_g2_fees_paf.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "fees_paf")})
            Me.XrLabel_g2_fees_paf.LocationFloat = New DevExpress.Utils.PointFloat(666.0!, 0.0!)
            Me.XrLabel_g2_fees_paf.Name = "XrLabel_g2_fees_paf"
            Me.XrLabel_g2_fees_paf.SizeF = New System.Drawing.SizeF(75.0!, 15.0!)
            Me.XrLabel_g2_fees_paf.StylePriority.UseTextAlignment = False
            XrSummary12.FormatString = "{0:c}"
            XrSummary12.Running = DevExpress.XtraReports.UI.SummaryRunning.Group
            Me.XrLabel_g2_fees_paf.Summary = XrSummary12
            Me.XrLabel_g2_fees_paf.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel_g2_clients_active
            '
            Me.XrLabel_g2_clients_active.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "clients_active")})
            Me.XrLabel_g2_clients_active.LocationFloat = New DevExpress.Utils.PointFloat(236.0!, 0.0!)
            Me.XrLabel_g2_clients_active.Name = "XrLabel_g2_clients_active"
            Me.XrLabel_g2_clients_active.SizeF = New System.Drawing.SizeF(72.0!, 15.0!)
            Me.XrLabel_g2_clients_active.StylePriority.UseTextAlignment = False
            XrSummary13.FormatString = "{0:f0}"
            XrSummary13.Running = DevExpress.XtraReports.UI.SummaryRunning.Group
            Me.XrLabel_g2_clients_active.Summary = XrSummary13
            Me.XrLabel_g2_clients_active.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel_g2_counselor_name
            '
            Me.XrLabel_g2_counselor_name.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "period_start", "{0:MMM/yyyy TOTALS}")})
            Me.XrLabel_g2_counselor_name.LocationFloat = New DevExpress.Utils.PointFloat(8.0!, 0.0!)
            Me.XrLabel_g2_counselor_name.Name = "XrLabel_g2_counselor_name"
            Me.XrLabel_g2_counselor_name.SizeF = New System.Drawing.SizeF(217.0!, 14.99999!)
            Me.XrLabel_g2_counselor_name.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            '
            'XrLabel_g2_clients_dropped
            '
            Me.XrLabel_g2_clients_dropped.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "clients_dropped")})
            Me.XrLabel_g2_clients_dropped.LocationFloat = New DevExpress.Utils.PointFloat(754.0!, 0.0!)
            Me.XrLabel_g2_clients_dropped.Name = "XrLabel_g2_clients_dropped"
            Me.XrLabel_g2_clients_dropped.SizeF = New System.Drawing.SizeF(45.0!, 15.0!)
            Me.XrLabel_g2_clients_dropped.StylePriority.UseTextAlignment = False
            XrSummary14.FormatString = "{0:f0}"
            XrSummary14.Running = DevExpress.XtraReports.UI.SummaryRunning.Group
            Me.XrLabel_g2_clients_dropped.Summary = XrSummary14
            Me.XrLabel_g2_clients_dropped.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel_g2_debt_new_dmp
            '
            Me.XrLabel_g2_debt_new_dmp.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "debt_new_dmp")})
            Me.XrLabel_g2_debt_new_dmp.LocationFloat = New DevExpress.Utils.PointFloat(864.0!, 0.0!)
            Me.XrLabel_g2_debt_new_dmp.Name = "XrLabel_g2_debt_new_dmp"
            Me.XrLabel_g2_debt_new_dmp.SizeF = New System.Drawing.SizeF(79.0!, 15.0!)
            Me.XrLabel_g2_debt_new_dmp.StylePriority.UseTextAlignment = False
            XrSummary15.FormatString = "{0:c}"
            XrSummary15.Running = DevExpress.XtraReports.UI.SummaryRunning.Group
            Me.XrLabel_g2_debt_new_dmp.Summary = XrSummary15
            Me.XrLabel_g2_debt_new_dmp.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel_g2_clients_new_dmp
            '
            Me.XrLabel_g2_clients_new_dmp.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "clients_new_dmp")})
            Me.XrLabel_g2_clients_new_dmp.LocationFloat = New DevExpress.Utils.PointFloat(804.0!, 0.0!)
            Me.XrLabel_g2_clients_new_dmp.Name = "XrLabel_g2_clients_new_dmp"
            Me.XrLabel_g2_clients_new_dmp.SizeF = New System.Drawing.SizeF(45.0!, 15.0!)
            Me.XrLabel_g2_clients_new_dmp.StylePriority.UseTextAlignment = False
            XrSummary16.FormatString = "{0:f0}"
            XrSummary16.Running = DevExpress.XtraReports.UI.SummaryRunning.Group
            Me.XrLabel_g2_clients_new_dmp.Summary = XrSummary16
            Me.XrLabel_g2_clients_new_dmp.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel_counselor_name
            '
            Me.XrLabel_counselor_name.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "counselor_name")})
            Me.XrLabel_counselor_name.LocationFloat = New DevExpress.Utils.PointFloat(8.0!, 0.0!)
            Me.XrLabel_counselor_name.Name = "XrLabel_counselor_name"
            Me.XrLabel_counselor_name.SizeF = New System.Drawing.SizeF(191.0!, 15.0!)
            Me.XrLabel_counselor_name.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            '
            'XrLabel_clients_new_dmp
            '
            Me.XrLabel_clients_new_dmp.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "clients_new_dmp", "{0:f0}")})
            Me.XrLabel_clients_new_dmp.LocationFloat = New DevExpress.Utils.PointFloat(804.0!, 0.0!)
            Me.XrLabel_clients_new_dmp.Name = "XrLabel_clients_new_dmp"
            Me.XrLabel_clients_new_dmp.SizeF = New System.Drawing.SizeF(45.0!, 15.0!)
            Me.XrLabel_clients_new_dmp.StylePriority.UseTextAlignment = False
            Me.XrLabel_clients_new_dmp.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel_clients_dropped
            '
            Me.XrLabel_clients_dropped.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "clients_dropped", "{0:f0}")})
            Me.XrLabel_clients_dropped.LocationFloat = New DevExpress.Utils.PointFloat(754.0!, 0.0!)
            Me.XrLabel_clients_dropped.Name = "XrLabel_clients_dropped"
            Me.XrLabel_clients_dropped.SizeF = New System.Drawing.SizeF(45.0!, 15.0!)
            Me.XrLabel_clients_dropped.StylePriority.UseTextAlignment = False
            Me.XrLabel_clients_dropped.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel_fees_paf
            '
            Me.XrLabel_fees_paf.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "fees_paf", "{0:c}")})
            Me.XrLabel_fees_paf.LocationFloat = New DevExpress.Utils.PointFloat(666.0!, 0.0!)
            Me.XrLabel_fees_paf.Name = "XrLabel_fees_paf"
            Me.XrLabel_fees_paf.SizeF = New System.Drawing.SizeF(75.0!, 15.0!)
            Me.XrLabel_fees_paf.StylePriority.UseTextAlignment = False
            Me.XrLabel_fees_paf.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel_pct_disb
            '
            Me.XrLabel_pct_disb.LocationFloat = New DevExpress.Utils.PointFloat(600.0!, 0.0!)
            Me.XrLabel_pct_disb.Name = "XrLabel_pct_disb"
            Me.XrLabel_pct_disb.Scripts.OnBeforePrint = "XrLabel_pct_disb_BeforePrint"
            Me.XrLabel_pct_disb.SizeF = New System.Drawing.SizeF(58.0!, 15.0!)
            Me.XrLabel_pct_disb.StylePriority.UseTextAlignment = False
            Me.XrLabel_pct_disb.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel_disbursement_actual
            '
            Me.XrLabel_disbursement_actual.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "disbursement_actual", "{0:c}")})
            Me.XrLabel_disbursement_actual.LocationFloat = New DevExpress.Utils.PointFloat(500.0!, 0.0!)
            Me.XrLabel_disbursement_actual.Name = "XrLabel_disbursement_actual"
            Me.XrLabel_disbursement_actual.SizeF = New System.Drawing.SizeF(100.0!, 15.0!)
            Me.XrLabel_disbursement_actual.StylePriority.UseTextAlignment = False
            Me.XrLabel_disbursement_actual.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel_clients_disbursed
            '
            Me.XrLabel_clients_disbursed.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "clients_disbursed", "{0:f0}")})
            Me.XrLabel_clients_disbursed.LocationFloat = New DevExpress.Utils.PointFloat(325.0!, 0.0!)
            Me.XrLabel_clients_disbursed.Name = "XrLabel_clients_disbursed"
            Me.XrLabel_clients_disbursed.SizeF = New System.Drawing.SizeF(50.0!, 15.0!)
            Me.XrLabel_clients_disbursed.StylePriority.UseTextAlignment = False
            Me.XrLabel_clients_disbursed.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel_disbursement_expected
            '
            Me.XrLabel_disbursement_expected.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "disbursement_expected", "{0:c}")})
            Me.XrLabel_disbursement_expected.LocationFloat = New DevExpress.Utils.PointFloat(391.0!, 0.0!)
            Me.XrLabel_disbursement_expected.Name = "XrLabel_disbursement_expected"
            Me.XrLabel_disbursement_expected.SizeF = New System.Drawing.SizeF(100.0!, 15.0!)
            Me.XrLabel_disbursement_expected.StylePriority.UseTextAlignment = False
            Me.XrLabel_disbursement_expected.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel_clients_active
            '
            Me.XrLabel_clients_active.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "clients_active", "{0:f0}")})
            Me.XrLabel_clients_active.LocationFloat = New DevExpress.Utils.PointFloat(236.0!, 0.0!)
            Me.XrLabel_clients_active.Name = "XrLabel_clients_active"
            Me.XrLabel_clients_active.SizeF = New System.Drawing.SizeF(72.0!, 15.0!)
            Me.XrLabel_clients_active.StylePriority.UseTextAlignment = False
            Me.XrLabel_clients_active.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel_debt_new_dmp
            '
            Me.XrLabel_debt_new_dmp.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "debt_new_dmp", "{0:c}")})
            Me.XrLabel_debt_new_dmp.LocationFloat = New DevExpress.Utils.PointFloat(864.0!, 0.0!)
            Me.XrLabel_debt_new_dmp.Name = "XrLabel_debt_new_dmp"
            Me.XrLabel_debt_new_dmp.SizeF = New System.Drawing.SizeF(79.0!, 15.0!)
            Me.XrLabel_debt_new_dmp.StylePriority.UseTextAlignment = False
            Me.XrLabel_debt_new_dmp.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'GroupHeader3
            '
            Me.GroupHeader3.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel17, Me.XrPanel1})
            Me.GroupHeader3.HeightF = 45.83333!
            Me.GroupHeader3.KeepTogether = True
            Me.GroupHeader3.Name = "GroupHeader3"
            Me.GroupHeader3.RepeatEveryPage = True
            '
            'XrLabel17
            '
            Me.XrLabel17.BackColor = System.Drawing.Color.Teal
            Me.XrLabel17.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel17.ForeColor = System.Drawing.Color.White
            Me.XrLabel17.LocationFloat = New DevExpress.Utils.PointFloat(754.0!, 6.0!)
            Me.XrLabel17.Name = "XrLabel17"
            Me.XrLabel17.SizeF = New System.Drawing.SizeF(199.4167!, 17.0!)
            Me.XrLabel17.StylePriority.UseBackColor = False
            Me.XrLabel17.StylePriority.UseFont = False
            Me.XrLabel17.StylePriority.UseForeColor = False
            Me.XrLabel17.StylePriority.UseTextAlignment = False
            Me.XrLabel17.Text = "CLIENTS"
            Me.XrLabel17.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
            '
            'XrPanel1
            '
            Me.XrPanel1.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel8, Me.XrLabel31, Me.XrLabel30, Me.XrLabel29, Me.XrLabel28, Me.XrLabel27, Me.XrLabel26, Me.XrLabel25, Me.XrLabel24, Me.XrLabel15})
            Me.XrPanel1.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 23.0!)
            Me.XrPanel1.Name = "XrPanel1"
            Me.XrPanel1.SizeF = New System.Drawing.SizeF(953.4166!, 17.00001!)
            Me.XrPanel1.StyleName = "XrControlStyle_HeaderPannel"
            '
            'XrLabel8
            '
            Me.XrLabel8.LocationFloat = New DevExpress.Utils.PointFloat(804.0!, 1.0!)
            Me.XrLabel8.Name = "XrLabel8"
            Me.XrLabel8.SizeF = New System.Drawing.SizeF(45.0!, 15.0!)
            Me.XrLabel8.Text = "NEW"
            Me.XrLabel8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel31
            '
            Me.XrLabel31.LocationFloat = New DevExpress.Utils.PointFloat(666.0!, 1.0!)
            Me.XrLabel31.Name = "XrLabel31"
            Me.XrLabel31.SizeF = New System.Drawing.SizeF(75.0!, 15.0!)
            Me.XrLabel31.Text = "$MON FEE"
            Me.XrLabel31.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel30
            '
            Me.XrLabel30.LocationFloat = New DevExpress.Utils.PointFloat(500.0!, 1.0!)
            Me.XrLabel30.Name = "XrLabel30"
            Me.XrLabel30.SizeF = New System.Drawing.SizeF(100.0!, 15.0!)
            Me.XrLabel30.Text = "$DISB ADJUST"
            Me.XrLabel30.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel29
            '
            Me.XrLabel29.LocationFloat = New DevExpress.Utils.PointFloat(754.0!, 1.0!)
            Me.XrLabel29.Name = "XrLabel29"
            Me.XrLabel29.SizeF = New System.Drawing.SizeF(45.0!, 15.0!)
            Me.XrLabel29.Text = "DROP"
            Me.XrLabel29.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel28
            '
            Me.XrLabel28.LocationFloat = New DevExpress.Utils.PointFloat(600.0!, 1.0!)
            Me.XrLabel28.Name = "XrLabel28"
            Me.XrLabel28.SizeF = New System.Drawing.SizeF(58.0!, 15.0!)
            Me.XrLabel28.Text = "% DISB"
            Me.XrLabel28.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel27
            '
            Me.XrLabel27.LocationFloat = New DevExpress.Utils.PointFloat(8.0!, 1.0!)
            Me.XrLabel27.Name = "XrLabel27"
            Me.XrLabel27.SizeF = New System.Drawing.SizeF(191.0!, 15.0!)
            Me.XrLabel27.Text = "COUNSELOR"
            Me.XrLabel27.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            '
            'XrLabel26
            '
            Me.XrLabel26.LocationFloat = New DevExpress.Utils.PointFloat(325.0!, 1.0!)
            Me.XrLabel26.Name = "XrLabel26"
            Me.XrLabel26.SizeF = New System.Drawing.SizeF(50.0!, 15.0!)
            Me.XrLabel26.Text = "# DISB"
            Me.XrLabel26.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel25
            '
            Me.XrLabel25.LocationFloat = New DevExpress.Utils.PointFloat(391.0!, 1.0!)
            Me.XrLabel25.Name = "XrLabel25"
            Me.XrLabel25.SizeF = New System.Drawing.SizeF(100.0!, 15.0!)
            Me.XrLabel25.Text = "$ SCHEDULED"
            Me.XrLabel25.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel24
            '
            Me.XrLabel24.LocationFloat = New DevExpress.Utils.PointFloat(864.0!, 0.9999924!)
            Me.XrLabel24.Name = "XrLabel24"
            Me.XrLabel24.SizeF = New System.Drawing.SizeF(79.0!, 15.0!)
            Me.XrLabel24.Text = "NEW DEBT"
            Me.XrLabel24.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel15
            '
            Me.XrLabel15.LocationFloat = New DevExpress.Utils.PointFloat(236.0!, 1.0!)
            Me.XrLabel15.Name = "XrLabel15"
            Me.XrLabel15.SizeF = New System.Drawing.SizeF(72.0!, 15.0!)
            Me.XrLabel15.Text = "# ACTIVE"
            '
            'StatisticsCounselorReport
            '
            Me.Bands.AddRange(New DevExpress.XtraReports.UI.Band() {Me.Detail, Me.PageHeader, Me.PageFooter, Me.TopMarginBand1, Me.BottomMarginBand1, Me.GroupFooter1, Me.GroupFooter2, Me.GroupHeader1, Me.GroupHeader2, Me.GroupHeader3})
            Me.Landscape = True
            Me.PageHeight = 850
            Me.PageWidth = 1100
            Me.ScriptLanguage = DevExpress.XtraReports.ScriptLanguage.VisualBasic
            Me.ScriptReferencesString = resources.GetString("$this.ScriptReferencesString")
            Me.Scripts.OnBeforePrint = "StatisticsCounselorReport_BeforePrint"
            Me.ScriptsSource = resources.GetString("$this.ScriptsSource")
            Me.StyleSheet.AddRange(New DevExpress.XtraReports.UI.XRControlStyle() {Me.XrControlStyle_Header, Me.XrControlStyle_Totals, Me.XrControlStyle_HeaderPannel, Me.XrControlStyle_GroupHeader})
            Me.Version = "11.2"
            Me.Controls.SetChildIndex(Me.GroupHeader3, 0)
            Me.Controls.SetChildIndex(Me.GroupHeader2, 0)
            Me.Controls.SetChildIndex(Me.GroupHeader1, 0)
            Me.Controls.SetChildIndex(Me.GroupFooter2, 0)
            Me.Controls.SetChildIndex(Me.GroupFooter1, 0)
            Me.Controls.SetChildIndex(Me.BottomMarginBand1, 0)
            Me.Controls.SetChildIndex(Me.TopMarginBand1, 0)
            Me.Controls.SetChildIndex(Me.PageFooter, 0)
            Me.Controls.SetChildIndex(Me.PageHeader, 0)
            Me.Controls.SetChildIndex(Me.Detail, 0)
            CType(Me, System.ComponentModel.ISupportInitialize).EndInit()

        End Sub

        Friend WithEvents GroupHeader1 As DevExpress.XtraReports.UI.GroupHeaderBand
        Friend WithEvents GroupFooter1 As DevExpress.XtraReports.UI.GroupFooterBand
        Friend WithEvents GroupHeader2 As DevExpress.XtraReports.UI.GroupHeaderBand
        Friend WithEvents GroupFooter2 As DevExpress.XtraReports.UI.GroupFooterBand
        Friend WithEvents XrLabel7 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel6 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_clients_disbursed As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_disbursement_actual As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_disbursement_expected As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_debt_new_dmp As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_clients_active As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_clients_new_dmp As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_counselor_name As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_clients_dropped As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_pct_disb As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_fees_paf As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_g1_disbursement_actual As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_g1_disbursement_expected As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_g1_clients_disbursed As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_g1_fees_paf As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_g1_clients_active As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_g1_counselor_name As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_g1_clients_dropped As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_g1_debt_new_dmp As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_g1_clients_new_dmp As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_g2_disbursement_actual As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_g2_disbursement_expected As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_g2_clients_disbursed As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_g2_fees_paf As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_g2_clients_active As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_g2_counselor_name As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_g2_clients_dropped As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_g2_debt_new_dmp As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_g2_clients_new_dmp As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel1 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel2 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel3 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel4 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel5 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents GroupHeader3 As DevExpress.XtraReports.UI.GroupHeaderBand
        Friend WithEvents XrLabel17 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrPanel1 As DevExpress.XtraReports.UI.XRPanel
        Friend WithEvents XrLabel8 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel31 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel30 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel29 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel28 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel27 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel26 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel25 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel24 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel15 As DevExpress.XtraReports.UI.XRLabel
    End Class
End Namespace
