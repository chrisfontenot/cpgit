#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}
#End Region

Option Compare Binary
Option Explicit On
Option Strict On

Imports DevExpress.XtraReports.UI

Namespace Statistics.Counselor
    Public Class StatisticsCounselorReport

        ''' <summary>
        ''' Create an instance of our report
        ''' </summary>
        Public Sub New()
            MyBase.New()
            InitializeComponent()

            AddHandler BeforePrint, AddressOf StatisticsCounselorReport_BeforePrint
            AddHandler XrLabel_pct_disb.BeforePrint, AddressOf XrLabel_pct_disb_BeforePrint
        End Sub

        Dim ds As New System.Data.DataSet("ds")

        ''' <summary>
        ''' Bind the dataset to the report
        ''' </summary>
        Private Sub StatisticsCounselorReport_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs)
            Dim rpt As DevExpress.XtraReports.UI.XtraReport = CType(sender, DevExpress.XtraReports.UI.XtraReport)
            Dim sqlInfo As DebtPlus.LINQ.SQLInfoClass = DebtPlus.LINQ.SQLInfoClass.getDefault()

            Using cmd As New System.Data.SqlClient.SqlCommand()
                With cmd
                    .Connection = New System.Data.SqlClient.SqlConnection(sqlInfo.ConnectionString)
                    .CommandTimeout = 0
                    .CommandType = System.Data.CommandType.StoredProcedure
                    .CommandText = "rpt_statistics_counselors"
                End With

                Using da As New System.Data.SqlClient.SqlDataAdapter(cmd)
                    da.Fill(ds, "rpt_statistics_counselors")
                    Dim tbl As System.Data.DataTable = ds.Tables("rpt_statistics_counselors")
                    rpt.DataSource = New System.Data.DataView(tbl, String.Empty, "period_start, office_name", System.Data.DataViewRowState.CurrentRows)
                End Using
            End Using
        End Sub

        Private Sub XrLabel_pct_disb_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs) Handles XrLabel_pct_disb.BeforePrint
            Dim Answer As Double = 0.0#
            With CType(sender, DevExpress.XtraReports.UI.XRLabel)
                Dim rpt As DevExpress.XtraReports.UI.XtraReport = CType(.Report, DevExpress.XtraReports.UI.XtraReport)
                .Text = String.Empty
                Dim obj As Object = rpt.GetCurrentColumnValue("disbursement_expected")
                If obj IsNot Nothing AndAlso obj IsNot System.DBNull.Value Then
                    Dim disbursement_expected As Decimal = Convert.ToDecimal(obj)

                    obj = rpt.GetCurrentColumnValue("disbursement_actual")
                    If obj IsNot Nothing AndAlso obj IsNot System.DBNull.Value Then
                        Dim disbursement_actual As Decimal = Convert.ToDecimal(obj)

                        Try
                            Answer = Convert.ToDouble(disbursement_actual) / Convert.ToDouble(disbursement_expected)
                            If Not Double.IsInfinity(Answer) AndAlso Not Double.IsNaN(Answer) AndAlso Not Double.IsNegativeInfinity(Answer) AndAlso Not Double.IsPositiveInfinity(Answer) Then
                                .Text = String.Format("{0:p}", Answer)
                            End If

                        Catch ex As Exception
                        End Try
                    End If
                End If
            End With
        End Sub


        ''' <summary>
        ''' Report Title string
        ''' </summary>
        Public Overrides ReadOnly Property ReportTitle() As String
            Get
                Return "Counselor Statistics"
            End Get
        End Property
    End Class
End Namespace
