#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}

#End Region

Option Compare Binary
Option Explicit On
Option Strict On

Imports DebtPlus.Reports.Template

Imports System.Drawing.Printing
Imports DevExpress.XtraReports.UI
Imports System.Data.SqlClient
Imports System.Threading

Namespace Custom.Appointments.CounseledByDistrict.RM

    Public Class AppointmentReport
        Inherits DatedTemplateXtraReportClass

        Public Sub New()
            MyBase.New()
            InitializeComponent()
            AddHandler BeforePrint, AddressOf Report_BeforePrint
            AddHandler XrLabel_group_count.PreviewClick, AddressOf XrLabel_PreviewClick
            AddHandler XrLabel_group_contact_type.PreviewClick, AddressOf XrLabel_PreviewClick
            AddHandler CalculatedField_Method.GetValue, AddressOf CalculatedField_Method_GetValue
        End Sub

        Public Overrides ReadOnly Property ReportTitle() As String
            Get
                Return "Count of Counseling Sessions CO"
            End Get
        End Property

        Private ds As New DataSet("ds")

        Private Sub Report_BeforePrint(ByVal sender As Object, ByVal e As PrintEventArgs)
            Const TableName As String = "report_custom_Appointments_By_District"

            Dim tbl As DataTable
            Dim rpt As XtraReport = CType(sender, XtraReport)
            Dim cn As SqlConnection = New System.Data.SqlClient.SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
            Try
                cn.Open()
                Using cmd As SqlCommand = New System.Data.SqlClient.SqlCommand()
                    With cmd
                        .Connection = cn
                        .CommandText = "SELECT [client],[active_status],[status],[result],[start_time],[name],[description],[appt_name],[client_appointment],[district],[contact_type],[counselor],[counselor_name],[client_name] FROM report_custom_Appointments_By_District WHERE start_time >= @from_date AND start_time < @to_date AND status in ('K','W') AND appt_name in ('RM - Face to Face', 'RM - Phone', 'RM - Phone Immediate')"
                        .CommandType = CommandType.Text
                        .Parameters.Add("@from_date", SqlDbType.DateTime).Value = Convert.ToDateTime(rpt.Parameters("ParameterFromDate").Value).Date
                        .Parameters.Add("@to_date", SqlDbType.DateTime).Value = Convert.ToDateTime(rpt.Parameters("ParameterToDate").Value).Date.AddDays(1)
                        .CommandTimeout = 0
                    End With

                    Using da As New SqlDataAdapter(cmd)
                        da.Fill(ds, TableName)
                        tbl = ds.Tables(TableName)
                        With tbl
                            .PrimaryKey = New DataColumn() {.Columns("client_appointment")}
                        End With
                    End Using
                End Using

                If tbl IsNot Nothing Then
                    rpt.DataSource = New DataView(tbl, String.Empty, "district", DataViewRowState.CurrentRows)
                    For Each calc As CalculatedField In rpt.CalculatedFields
                        calc.Assign(rpt.DataSource, rpt.DataMember)
                    Next
                End If

            Catch ex As SqlException
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error reading report information")

            Finally
                If cn IsNot Nothing Then cn.Dispose()
            End Try
        End Sub

        Private Sub XrLabel_PreviewClick(ByVal sender As Object, ByVal e As PreviewMouseEventArgs)
            Dim client_appointment As Int32 = DebtPlus.Utils.Nulls.DInt(e.Brick.Value)
            If client_appointment > 0 Then
                Dim tbl As DataTable = CType(CType(sender, XRControl).Report.DataSource, DataView).Table
                Dim row As DataRow = tbl.Rows.Find(client_appointment)
                If row IsNot Nothing Then
                    Dim district As Int32 = DebtPlus.Utils.Nulls.DInt(row("district"))
                    Dim contact_type As String = DebtPlus.Utils.Nulls.DStr(row("contact_type"))
                    If district > 0 AndAlso contact_type <> String.Empty Then
                        Dim NewVue As New DataView(tbl, String.Format("[district]={0:f0} AND [contact_type]='{1}'", district, contact_type), "name, start_time, client", DataViewRowState.CurrentRows)
                        If NewVue.Count <= 0 Then
                            NewVue.Dispose()
                        Else
                            Dim thrd As New Thread(New ParameterizedThreadStart(AddressOf PreviewHandler))
                            With thrd
                                .SetApartmentState(ApartmentState.STA)
                                .Name = "Detail Report"
                                .IsBackground = True
                                .Start(New Object() {NewVue, String.Format("{0} FOR district '{1}' and type '{2}'", ReportSubTitle, NewVue(0)("description"), TranslateType(contact_type))})
                            End With
                        End If
                    End If
                End If
            End If
        End Sub

        Private Sub PreviewHandler(ByVal obj As Object)
            Dim objList() As Object = CType(obj, Object())
            Dim Subtitle As String = CType(objList(1), String)
            Using NewVue As DataView = CType(objList(0), DataView)
                Using rpt As New DetailReport
                    rpt.SetSubtitle(Subtitle)
                    rpt.DataSource = NewVue
                    rpt.CreateDocument()
                    rpt.DisplayPreviewDialog()
                End Using
            End Using
        End Sub

        Private Sub CalculatedField_Method_GetValue(ByVal sender As Object, ByVal e As GetValueEventArgs)

            With CType(sender, CalculatedField)
                Dim row As DataRowView = CType(e.Row, DataRowView)
                Dim contact_type As String = DebtPlus.Utils.Nulls.DStr(row("contact_type"))
                e.Value = TranslateType(contact_type)
            End With
        End Sub

        Private Shared Function TranslateType(ByVal contact_type As String) As String
            Dim Answer As String = String.Empty

            Select Case contact_type
                Case "F"
                    Answer = "Face to Face"
                Case "P"
                    Answer = "Phone"
                Case "M"
                    Answer = "Mail"
                Case "I"
                    Answer = "Internet"
                Case "E"
                    Answer = "E-Mail"
                Case "X"
                    Answer = "Other"
                Case Else
                    Answer = "Unkown " + contact_type
            End Select

            Return Answer
        End Function
    End Class
End Namespace
