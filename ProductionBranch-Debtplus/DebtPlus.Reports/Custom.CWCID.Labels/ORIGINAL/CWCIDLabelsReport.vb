#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}
#End Region

Option Compare Binary
Option Explicit On 
Option Strict On

Namespace Custom.CWCID.Labels
    Public Class CWCIDLabelsReport

        ''' <summary>
        ''' Create an instance of our report
        ''' </summary>
        Public Sub New()
            MyBase.New()
            InitializeComponent()
            AddHandler BeforePrint, AddressOf CWCIDLabelsReport_BeforePrint
            AddHandler XrLabel_Text.BeforePrint, AddressOf XrLabel_LabelText_BeforePrint

            ReportFilter.IsEnabled = True
        End Sub

        Public Property Parameter_FromDate() As DateTime
            Get
                Return CType(Parameters("ParameterFromDate").Value, DateTime)
            End Get
            Set(ByVal value As DateTime)
                Parameters("ParameterFromDate").Value = value
            End Set
        End Property

        Public Property Parameter_ToDate() As DateTime
            Get
                Return CType(Parameters("ParameterToDate").Value, DateTime)
            End Get
            Set(ByVal value As DateTime)
                Parameters("ParameterToDate").Value = value
            End Set
        End Property

        Public Overrides Function NeedParameters() As Boolean
            Return True
        End Function

        Public Overrides Function RequestReportParameters() As System.Windows.Forms.DialogResult
            Dim Answer As System.Windows.Forms.DialogResult
            Using frm As New DebtPlus.Reports.Template.Forms.DateReportParametersForm()
                Answer = frm.ShowDialog
                Parameter_FromDate = frm.Parameter_FromDate
                Parameter_ToDate = frm.Parameter_ToDate
            End Using
            Return Answer
        End Function

        Private ds As New System.Data.DataSet("ds")
        Private Sub CWCIDLabelsReport_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs)
            Const TableName As String = "rpt_custom_CWCID"
            Dim rpt As DevExpress.XtraReports.UI.XtraReport = CType(sender, DevExpress.XtraReports.UI.XtraReport)

            Dim tbl As System.Data.DataTable = ds.Tables(TableName)
            If tbl Is Nothing Then
                Dim sqlInfo As DebtPlus.LINQ.SQLInfoClass = DebtPlus.LINQ.SQLInfoClass.getDefault()
                Dim cn As System.Data.SqlClient.SqlConnection = New System.Data.SqlClient.SqlConnection(sqlInfo.ConnectionString)
                Try
                    cn.Open()
                    Using cmd As New System.Data.SqlClient.SqlCommand()
                        With cmd
                            .Connection = cn
                            .CommandText = "rpt_custom_CWCID"
                            .CommandType = System.Data.CommandType.StoredProcedure
                            System.Data.SqlClient.SqlCommandBuilder.DeriveParameters(cmd)

                            .Parameters(1).Value = rpt.Parameters("ParameterFromDate").Value
                            .Parameters(2).Value = rpt.Parameters("ParameterToDate").Value
                            .CommandTimeout = 0
                        End With

                        Using da As New System.Data.SqlClient.SqlDataAdapter(cmd)
                            da.Fill(ds, TableName)
                            tbl = ds.Tables(TableName)
                        End Using
                    End Using

                Catch ex As System.Data.SqlClient.SqlException
                    Dim gdr As New DebtPlus.Repository.GetDataResult() : gdr.HandleException(ex) : DebtPlus.UI.Common.ErrorHandling.HandleErrors(gdr, "Error reading report information")

                Finally
                    If cn IsNot Nothing Then
                        If cn.State <> System.Data.ConnectionState.Closed Then cn.Close()
                        cn.Dispose()
                    End If
                End Try
            End If

            If tbl IsNot Nothing Then
                rpt.DataSource = New System.Data.DataView(tbl, CType(rpt, DebtPlus.Interfaces.Reports.IReportFilter).ReportFilter.ViewFilter, String.Empty, System.Data.DataViewRowState.CurrentRows)
                For Each calc As DevExpress.XtraReports.UI.CalculatedField In rpt.CalculatedFields
                    calc.Assign(rpt.DataSource, rpt.DataMember)
                Next
            End If
        End Sub

        Private Sub XrLabel_LabelText_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs)
            With CType(sender, DevExpress.XtraReports.UI.XRLabel)
                Dim rpt As DevExpress.XtraReports.UI.XtraReport = CType(.Report, DevExpress.XtraReports.UI.XtraReport)
                Dim row As System.Data.DataRow = CType(rpt.GetCurrentRow(), System.Data.DataRowView).Row
                Dim sb As New System.Text.StringBuilder
                For Each Fld As String In New String() {"name", "addr_1", "addr_2", "addr_3"}
                    Dim obj As Object = row(Fld)
                    If obj IsNot Nothing AndAlso obj IsNot System.DBNull.Value Then
                        Dim strValue As String = Convert.ToString(obj, System.Globalization.CultureInfo.InvariantCulture).Trim
                        If strValue <> String.Empty Then
                            sb.Append(System.Environment.NewLine)
                            sb.Append(strValue)
                        End If
                    End If
                Next
                If sb.Length > 0 Then
                    sb.Remove(0, 2)
                End If
                .Text = sb.ToString
            End With
        End Sub
    End Class
End Namespace
