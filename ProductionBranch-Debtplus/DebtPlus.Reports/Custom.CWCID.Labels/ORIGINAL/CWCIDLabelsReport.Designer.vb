﻿Namespace Custom.CWCID.Labels
    <Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
    Partial Class CWCIDLabelsReport
        Inherits DebtPlus.Reports.Template.BaseXtraReportClass

        'XtraReport overrides dispose to clean up the component list.
        <System.Diagnostics.DebuggerNonUserCode()> _
        Protected Overrides Sub Dispose(ByVal disposing As Boolean)
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
            MyBase.Dispose(disposing)
        End Sub

        'Required by the Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Designer
        'It can be modified using the Designer.  
        'Do not modify it using the code editor.
        <System.Diagnostics.DebuggerStepThrough()> _
        Private Sub InitializeComponent()
            Dim PostNetGenerator1 As DevExpress.XtraPrinting.BarCode.PostNetGenerator = New DevExpress.XtraPrinting.BarCode.PostNetGenerator()
            Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(CWCIDLabelsReport))
            Me.XrPanel1 = New DevExpress.XtraReports.UI.XRPanel()
            Me.XrBarCode = New DevExpress.XtraReports.UI.XRBarCode()
            Me.XrLabel_Text = New DevExpress.XtraReports.UI.XRLabel()
            Me.ParameterFromDate = New DevExpress.XtraReports.Parameters.Parameter()
            Me.ParameterToDate = New DevExpress.XtraReports.Parameters.Parameter()
            CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
            '
            'Detail
            '
            Me.Detail.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrPanel1})
            Me.Detail.MultiColumn.ColumnSpacing = 13.0!
            Me.Detail.MultiColumn.ColumnWidth = 262.0!
            Me.Detail.MultiColumn.Layout = DevExpress.XtraPrinting.ColumnLayout.AcrossThenDown
            Me.Detail.MultiColumn.Mode = DevExpress.XtraReports.UI.MultiColumnMode.UseColumnWidth
            Me.Detail.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
            Me.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
            '
            'XrPanel1
            '
            Me.XrPanel1.BorderColor = System.Drawing.Color.Transparent
            Me.XrPanel1.CanGrow = False
            Me.XrPanel1.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrBarCode, Me.XrLabel_Text})
            Me.XrPanel1.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
            Me.XrPanel1.Name = "XrPanel1"
            Me.XrPanel1.SizeF = New System.Drawing.SizeF(262.0!, 100.0!)
            '
            'XrBarCode
            '
            Me.XrBarCode.BackColor = System.Drawing.Color.White
            Me.XrBarCode.BorderColor = System.Drawing.Color.Black
            Me.XrBarCode.BorderWidth = 0
            Me.XrBarCode.ForeColor = System.Drawing.Color.Black
            Me.XrBarCode.KeepTogether = False
            Me.XrBarCode.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
            Me.XrBarCode.Name = "XrBarCode"
            Me.XrBarCode.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrBarCode.ShowText = False
            Me.XrBarCode.SizeF = New System.Drawing.SizeF(262.0!, 14.0!)
            Me.XrBarCode.StylePriority.UseBackColor = False
            Me.XrBarCode.StylePriority.UseBorderColor = False
            Me.XrBarCode.StylePriority.UseForeColor = False
            Me.XrBarCode.StylePriority.UsePadding = False
            Me.XrBarCode.Symbology = PostNetGenerator1
            Me.XrBarCode.Visible = False
            '
            'XrLabel_Text
            '
            Me.XrLabel_Text.BorderColor = System.Drawing.Color.Transparent
            Me.XrLabel_Text.CanShrink = True
            Me.XrLabel_Text.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 16.0!)
            Me.XrLabel_Text.Multiline = True
            Me.XrLabel_Text.Name = "XrLabel_Text"
            Me.XrLabel_Text.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 2, 2, 100.0!)
            Me.XrLabel_Text.Scripts.OnBeforePrint = "XrLabel_Text_BeforePrint"
            Me.XrLabel_Text.SizeF = New System.Drawing.SizeF(262.0!, 84.0!)
            Me.XrLabel_Text.StylePriority.UsePadding = False
            '
            'ParameterFromDate
            '
            Me.ParameterFromDate.Description = "Starting Date"
            Me.ParameterFromDate.Name = "ParameterFromDate"
            Me.ParameterFromDate.Type = GetType(Date)
            Me.ParameterFromDate.Value = New Date(CType(0, Long))
            Me.ParameterFromDate.Visible = False
            '
            'ParameterToDate
            '
            Me.ParameterToDate.Description = "Ending Date"
            Me.ParameterToDate.Name = "ParameterToDate"
            Me.ParameterToDate.Type = GetType(Date)
            Me.ParameterToDate.Value = New Date(CType(0, Long))
            Me.ParameterToDate.Visible = False
            '
            'CWCIDLabelsReport
            '
            Me.Bands.AddRange(New DevExpress.XtraReports.UI.Band() {Me.TopMarginBand1, Me.Detail, Me.BottomMarginBand1})
            Me.Margins = New System.Drawing.Printing.Margins(38, 0, 25, 25)
            Me.PaperName = "Avery 5160 Addressing Labels"
            Me.Parameters.AddRange(New DevExpress.XtraReports.Parameters.Parameter() {Me.ParameterFromDate, Me.ParameterToDate})
            Me.ReportPrintOptions.DetailCount = 30
            Me.RequestParameters = False
            Me.ScriptLanguage = DevExpress.XtraReports.ScriptLanguage.VisualBasic
            Me.ScriptReferencesString = resources.GetString("$this.ScriptReferencesString")
            Me.Scripts.OnBeforePrint = "CWCIDLabelsReport_BeforePrint"
            Me.ScriptsSource = resources.GetString("$this.ScriptsSource")
            Me.ShowPrintMarginsWarning = False
            Me.ShowPrintStatusDialog = False
            Me.Version = "11.2"
            CType(Me, System.ComponentModel.ISupportInitialize).EndInit()

        End Sub

        Protected Friend WithEvents XrPanel1 As DevExpress.XtraReports.UI.XRPanel
        Protected Friend WithEvents XrBarCode As DevExpress.XtraReports.UI.XRBarCode
        Protected Friend WithEvents XrLabel_Text As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents ParameterFromDate As DevExpress.XtraReports.Parameters.Parameter
        Friend WithEvents ParameterToDate As DevExpress.XtraReports.Parameters.Parameter
    End Class
End Namespace
