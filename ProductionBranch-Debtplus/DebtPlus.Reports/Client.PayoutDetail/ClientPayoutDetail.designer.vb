﻿Namespace Client.PayoutDetail
    <Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
    Partial Class ClientPayoutDetail
        Inherits DebtPlus.Reports.Template.TemplateXtraReportClass

        <System.Diagnostics.DebuggerNonUserCode()> _
        Protected Overrides Sub Dispose(ByVal disposing As Boolean)
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
            MyBase.Dispose(disposing)
        End Sub

        Private components As System.ComponentModel.IContainer
        <System.Diagnostics.DebuggerStepThrough()> _
        Private Sub InitializeComponent()
            Dim XrSummary1 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary()
            Dim XrSummary2 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary()
            Dim XrSummary3 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary()
            Dim XrSummary4 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary()
            Dim XrSummary5 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary()
            Dim XrSummary6 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary()
            Dim XrSummary7 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary()
            Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(ClientPayoutDetail))
            Dim PostNetGenerator1 As DevExpress.XtraPrinting.BarCode.PostNetGenerator = New DevExpress.XtraPrinting.BarCode.PostNetGenerator()
            Me.XrLabel_date = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_creditor_name = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_payment = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_interest = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_balance = New DevExpress.XtraReports.UI.XRLabel()
            Me.GroupHeader1 = New DevExpress.XtraReports.UI.GroupHeaderBand()
            Me.GroupFooter1 = New DevExpress.XtraReports.UI.GroupFooterBand()
            Me.XrLabel_total_fees = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel5 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_total_balance = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_total_interest = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_total_payment = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_account_number = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrPanel1 = New DevExpress.XtraReports.UI.XRPanel()
            Me.XrLabel10 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel8 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel7 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel6 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel4 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel3 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel1 = New DevExpress.XtraReports.UI.XRLabel()
            Me.ReportFooter = New DevExpress.XtraReports.UI.ReportFooterBand()
            Me.XrLabel_grand_total_payments = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel15 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_grand_total_fees = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel13 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_grand_total_interest = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel11 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLine1 = New DevExpress.XtraReports.UI.XRLine()
            Me.XrLabel_grand_total_balance = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_number_of_months = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel9 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel2 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrRichText1 = New DevExpress.XtraReports.UI.XRRichText()
            Me.ds = New System.Data.DataSet()
            Me.XrLabel_fees = New DevExpress.XtraReports.UI.XRLabel()
            Me.GroupHeader2 = New DevExpress.XtraReports.UI.GroupHeaderBand()
            Me.GroupHeader3 = New DevExpress.XtraReports.UI.GroupHeaderBand()
            Me.XrPanel_ClientAddress = New DevExpress.XtraReports.UI.XRPanel()
            Me.XrBarCode_PostalCode = New DevExpress.XtraReports.UI.XRBarCode()
            Me.XrLabel_Address = New DevExpress.XtraReports.UI.XRLabel()
            Me.CalculatedField_Last4 = New DevExpress.XtraReports.UI.CalculatedField()
            CType(Me.XrRichText1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.ds, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
            '
            'Detail
            '
            Me.Detail.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel_fees, Me.XrLabel_account_number, Me.XrLabel_balance, Me.XrLabel_interest, Me.XrLabel_payment, Me.XrLabel_creditor_name, Me.XrLabel_date})
            Me.Detail.HeightF = 15.0!
            '
            'PageHeader
            '
            Me.PageHeader.HeightF = 124.0!
            '
            'XrLabel_Title
            '
            Me.XrLabel_Title.StylePriority.UseFont = False
            '
            'XrPageInfo_PageNumber
            '
            Me.XrPageInfo_PageNumber.StylePriority.UseTextAlignment = False
            '
            'XRLabel_Agency_Name
            '
            Me.XRLabel_Agency_Name.StylePriority.UseTextAlignment = False
            '
            'XrLabel_Agency_Address3
            '
            Me.XrLabel_Agency_Address3.StylePriority.UseFont = False
            Me.XrLabel_Agency_Address3.StylePriority.UseTextAlignment = False
            '
            'XrLabel_Agency_Address1
            '
            Me.XrLabel_Agency_Address1.StylePriority.UseFont = False
            Me.XrLabel_Agency_Address1.StylePriority.UseTextAlignment = False
            '
            'XrLabel_Agency_Phone
            '
            Me.XrLabel_Agency_Phone.StylePriority.UseFont = False
            Me.XrLabel_Agency_Phone.StylePriority.UseTextAlignment = False
            '
            'XrLabel_Agency_Address2
            '
            Me.XrLabel_Agency_Address2.StylePriority.UseFont = False
            Me.XrLabel_Agency_Address2.StylePriority.UseTextAlignment = False
            '
            'XrLabel_date
            '
            Me.XrLabel_date.CanGrow = False
            Me.XrLabel_date.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "date", "{0:d}")})
            Me.XrLabel_date.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
            Me.XrLabel_date.Name = "XrLabel_date"
            Me.XrLabel_date.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_date.ProcessDuplicates = DevExpress.XtraReports.UI.ValueSuppressType.Suppress
            Me.XrLabel_date.SizeF = New System.Drawing.SizeF(75.0!, 15.0!)
            Me.XrLabel_date.StylePriority.UseTextAlignment = False
            Me.XrLabel_date.Text = "XrLabel_date"
            Me.XrLabel_date.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel_creditor_name
            '
            Me.XrLabel_creditor_name.KeepTogether = True
            Me.XrLabel_creditor_name.LocationFloat = New DevExpress.Utils.PointFloat(83.0!, 0.0!)
            Me.XrLabel_creditor_name.Name = "XrLabel_creditor_name"
            Me.XrLabel_creditor_name.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_creditor_name.SizeF = New System.Drawing.SizeF(242.0!, 15.0!)
            Me.XrLabel_creditor_name.StylePriority.UseTextAlignment = False
            Me.XrLabel_creditor_name.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            Me.XrLabel_creditor_name.WordWrap = False
            '
            'XrLabel_payment
            '
            Me.XrLabel_payment.CanGrow = False
            Me.XrLabel_payment.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "payment", "{0:c}")})
            Me.XrLabel_payment.LocationFloat = New DevExpress.Utils.PointFloat(483.0!, 0.0!)
            Me.XrLabel_payment.Name = "XrLabel_payment"
            Me.XrLabel_payment.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_payment.SizeF = New System.Drawing.SizeF(67.0!, 15.0!)
            Me.XrLabel_payment.StylePriority.UseTextAlignment = False
            Me.XrLabel_payment.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel_interest
            '
            Me.XrLabel_interest.CanGrow = False
            Me.XrLabel_interest.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "interest", "{0:c}")})
            Me.XrLabel_interest.LocationFloat = New DevExpress.Utils.PointFloat(625.0!, 0.0!)
            Me.XrLabel_interest.Name = "XrLabel_interest"
            Me.XrLabel_interest.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_interest.SizeF = New System.Drawing.SizeF(67.0!, 15.0!)
            Me.XrLabel_interest.StylePriority.UseTextAlignment = False
            Me.XrLabel_interest.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel_balance
            '
            Me.XrLabel_balance.CanGrow = False
            Me.XrLabel_balance.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "balance", "{0:c}")})
            Me.XrLabel_balance.LocationFloat = New DevExpress.Utils.PointFloat(700.0!, 0.0!)
            Me.XrLabel_balance.Name = "XrLabel_balance"
            Me.XrLabel_balance.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_balance.SizeF = New System.Drawing.SizeF(100.0!, 15.0!)
            Me.XrLabel_balance.StylePriority.UseTextAlignment = False
            Me.XrLabel_balance.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'GroupHeader1
            '
            Me.GroupHeader1.GroupFields.AddRange(New DevExpress.XtraReports.UI.GroupField() {New DevExpress.XtraReports.UI.GroupField("date", DevExpress.XtraReports.UI.XRColumnSortOrder.Ascending)})
            Me.GroupHeader1.HeightF = 0.0!
            Me.GroupHeader1.Name = "GroupHeader1"
            '
            'GroupFooter1
            '
            Me.GroupFooter1.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel_total_fees, Me.XrLabel5, Me.XrLabel_total_balance, Me.XrLabel_total_interest, Me.XrLabel_total_payment})
            Me.GroupFooter1.GroupUnion = DevExpress.XtraReports.UI.GroupFooterUnion.WithLastDetail
            Me.GroupFooter1.HeightF = 33.0!
            Me.GroupFooter1.Name = "GroupFooter1"
            '
            'XrLabel_total_fees
            '
            Me.XrLabel_total_fees.CanGrow = False
            Me.XrLabel_total_fees.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "fees")})
            Me.XrLabel_total_fees.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel_total_fees.LocationFloat = New DevExpress.Utils.PointFloat(550.0!, 0.0!)
            Me.XrLabel_total_fees.Name = "XrLabel_total_fees"
            Me.XrLabel_total_fees.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_total_fees.SizeF = New System.Drawing.SizeF(67.0!, 15.0!)
            Me.XrLabel_total_fees.StylePriority.UseFont = False
            Me.XrLabel_total_fees.StylePriority.UseTextAlignment = False
            XrSummary1.FormatString = "{0:c}"
            XrSummary1.IgnoreNullValues = True
            XrSummary1.Running = DevExpress.XtraReports.UI.SummaryRunning.Group
            Me.XrLabel_total_fees.Summary = XrSummary1
            Me.XrLabel_total_fees.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrLabel_total_fees.WordWrap = False
            '
            'XrLabel5
            '
            Me.XrLabel5.CanGrow = False
            Me.XrLabel5.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel5.LocationFloat = New DevExpress.Utils.PointFloat(400.0!, 0.0!)
            Me.XrLabel5.Name = "XrLabel5"
            Me.XrLabel5.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel5.SizeF = New System.Drawing.SizeF(75.0!, 15.0!)
            Me.XrLabel5.StylePriority.UseFont = False
            Me.XrLabel5.StylePriority.UseTextAlignment = False
            Me.XrLabel5.Text = "TOTAL"
            Me.XrLabel5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel_total_balance
            '
            Me.XrLabel_total_balance.CanGrow = False
            Me.XrLabel_total_balance.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "balance")})
            Me.XrLabel_total_balance.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel_total_balance.LocationFloat = New DevExpress.Utils.PointFloat(700.0!, 0.0!)
            Me.XrLabel_total_balance.Name = "XrLabel_total_balance"
            Me.XrLabel_total_balance.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_total_balance.SizeF = New System.Drawing.SizeF(100.0!, 15.0!)
            Me.XrLabel_total_balance.StylePriority.UseFont = False
            Me.XrLabel_total_balance.StylePriority.UseTextAlignment = False
            XrSummary2.FormatString = "{0:c}"
            XrSummary2.IgnoreNullValues = True
            XrSummary2.Running = DevExpress.XtraReports.UI.SummaryRunning.Group
            Me.XrLabel_total_balance.Summary = XrSummary2
            Me.XrLabel_total_balance.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel_total_interest
            '
            Me.XrLabel_total_interest.CanGrow = False
            Me.XrLabel_total_interest.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "interest")})
            Me.XrLabel_total_interest.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel_total_interest.LocationFloat = New DevExpress.Utils.PointFloat(625.0!, 0.0!)
            Me.XrLabel_total_interest.Name = "XrLabel_total_interest"
            Me.XrLabel_total_interest.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_total_interest.SizeF = New System.Drawing.SizeF(67.0!, 15.0!)
            Me.XrLabel_total_interest.StylePriority.UseFont = False
            Me.XrLabel_total_interest.StylePriority.UseTextAlignment = False
            XrSummary3.FormatString = "{0:c}"
            XrSummary3.IgnoreNullValues = True
            XrSummary3.Running = DevExpress.XtraReports.UI.SummaryRunning.Group
            Me.XrLabel_total_interest.Summary = XrSummary3
            Me.XrLabel_total_interest.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel_total_payment
            '
            Me.XrLabel_total_payment.CanGrow = False
            Me.XrLabel_total_payment.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "payment")})
            Me.XrLabel_total_payment.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel_total_payment.LocationFloat = New DevExpress.Utils.PointFloat(483.0!, 0.0!)
            Me.XrLabel_total_payment.Name = "XrLabel_total_payment"
            Me.XrLabel_total_payment.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_total_payment.SizeF = New System.Drawing.SizeF(67.0!, 15.0!)
            Me.XrLabel_total_payment.StylePriority.UseFont = False
            Me.XrLabel_total_payment.StylePriority.UseTextAlignment = False
            XrSummary4.FormatString = "{0:c}"
            XrSummary4.IgnoreNullValues = True
            XrSummary4.Running = DevExpress.XtraReports.UI.SummaryRunning.Group
            Me.XrLabel_total_payment.Summary = XrSummary4
            Me.XrLabel_total_payment.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel_account_number
            '
            Me.XrLabel_account_number.CanGrow = False
            Me.XrLabel_account_number.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "CalculatedField_Last4")})
            Me.XrLabel_account_number.LocationFloat = New DevExpress.Utils.PointFloat(333.0!, 0.0!)
            Me.XrLabel_account_number.Name = "XrLabel_account_number"
            Me.XrLabel_account_number.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_account_number.SizeF = New System.Drawing.SizeF(142.0!, 15.0!)
            Me.XrLabel_account_number.StylePriority.UseTextAlignment = False
            Me.XrLabel_account_number.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            Me.XrLabel_account_number.WordWrap = False
            '
            'XrPanel1
            '
            Me.XrPanel1.BackColor = System.Drawing.Color.Teal
            Me.XrPanel1.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel10, Me.XrLabel8, Me.XrLabel7, Me.XrLabel6, Me.XrLabel4, Me.XrLabel3, Me.XrLabel1})
            Me.XrPanel1.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 8.0!)
            Me.XrPanel1.Name = "XrPanel1"
            Me.XrPanel1.SizeF = New System.Drawing.SizeF(800.0!, 18.0!)
            Me.XrPanel1.StylePriority.UseBackColor = False
            '
            'XrLabel10
            '
            Me.XrLabel10.BackColor = System.Drawing.Color.Transparent
            Me.XrLabel10.BorderColor = System.Drawing.Color.Transparent
            Me.XrLabel10.CanGrow = False
            Me.XrLabel10.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel10.ForeColor = System.Drawing.Color.White
            Me.XrLabel10.LocationFloat = New DevExpress.Utils.PointFloat(575.0!, 0.0!)
            Me.XrLabel10.Name = "XrLabel10"
            Me.XrLabel10.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel10.SizeF = New System.Drawing.SizeF(42.0!, 15.0!)
            Me.XrLabel10.StylePriority.UseBackColor = False
            Me.XrLabel10.StylePriority.UseBorderColor = False
            Me.XrLabel10.StylePriority.UseFont = False
            Me.XrLabel10.StylePriority.UseForeColor = False
            Me.XrLabel10.StylePriority.UseTextAlignment = False
            Me.XrLabel10.Text = "FEE"
            Me.XrLabel10.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel8
            '
            Me.XrLabel8.BackColor = System.Drawing.Color.Transparent
            Me.XrLabel8.BorderColor = System.Drawing.Color.Transparent
            Me.XrLabel8.CanGrow = False
            Me.XrLabel8.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel8.ForeColor = System.Drawing.Color.White
            Me.XrLabel8.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
            Me.XrLabel8.Name = "XrLabel8"
            Me.XrLabel8.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel8.SizeF = New System.Drawing.SizeF(75.0!, 15.0!)
            Me.XrLabel8.StylePriority.UseBackColor = False
            Me.XrLabel8.StylePriority.UseBorderColor = False
            Me.XrLabel8.StylePriority.UseFont = False
            Me.XrLabel8.StylePriority.UseForeColor = False
            Me.XrLabel8.StylePriority.UseTextAlignment = False
            Me.XrLabel8.Text = "DATE"
            Me.XrLabel8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel7
            '
            Me.XrLabel7.BackColor = System.Drawing.Color.Transparent
            Me.XrLabel7.BorderColor = System.Drawing.Color.Transparent
            Me.XrLabel7.CanGrow = False
            Me.XrLabel7.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel7.ForeColor = System.Drawing.Color.White
            Me.XrLabel7.LocationFloat = New DevExpress.Utils.PointFloat(617.0!, 0.0!)
            Me.XrLabel7.Name = "XrLabel7"
            Me.XrLabel7.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel7.SizeF = New System.Drawing.SizeF(75.0!, 15.0!)
            Me.XrLabel7.StylePriority.UseBackColor = False
            Me.XrLabel7.StylePriority.UseBorderColor = False
            Me.XrLabel7.StylePriority.UseFont = False
            Me.XrLabel7.StylePriority.UseForeColor = False
            Me.XrLabel7.StylePriority.UseTextAlignment = False
            Me.XrLabel7.Text = "INTEREST"
            Me.XrLabel7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel6
            '
            Me.XrLabel6.BackColor = System.Drawing.Color.Transparent
            Me.XrLabel6.BorderColor = System.Drawing.Color.Transparent
            Me.XrLabel6.CanGrow = False
            Me.XrLabel6.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel6.ForeColor = System.Drawing.Color.White
            Me.XrLabel6.LocationFloat = New DevExpress.Utils.PointFloat(475.0!, 0.0!)
            Me.XrLabel6.Name = "XrLabel6"
            Me.XrLabel6.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel6.SizeF = New System.Drawing.SizeF(75.0!, 15.0!)
            Me.XrLabel6.StylePriority.UseBackColor = False
            Me.XrLabel6.StylePriority.UseBorderColor = False
            Me.XrLabel6.StylePriority.UseFont = False
            Me.XrLabel6.StylePriority.UseForeColor = False
            Me.XrLabel6.StylePriority.UseTextAlignment = False
            Me.XrLabel6.Text = "PAYMENT"
            Me.XrLabel6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel4
            '
            Me.XrLabel4.BackColor = System.Drawing.Color.Transparent
            Me.XrLabel4.BorderColor = System.Drawing.Color.Transparent
            Me.XrLabel4.CanGrow = False
            Me.XrLabel4.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel4.ForeColor = System.Drawing.Color.White
            Me.XrLabel4.LocationFloat = New DevExpress.Utils.PointFloat(333.0!, 0.0!)
            Me.XrLabel4.Name = "XrLabel4"
            Me.XrLabel4.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel4.SizeF = New System.Drawing.SizeF(142.0!, 15.0!)
            Me.XrLabel4.StylePriority.UseBackColor = False
            Me.XrLabel4.StylePriority.UseBorderColor = False
            Me.XrLabel4.StylePriority.UseFont = False
            Me.XrLabel4.StylePriority.UseForeColor = False
            Me.XrLabel4.StylePriority.UseTextAlignment = False
            Me.XrLabel4.Text = "ACCT"
            Me.XrLabel4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            '
            'XrLabel3
            '
            Me.XrLabel3.BackColor = System.Drawing.Color.Transparent
            Me.XrLabel3.BorderColor = System.Drawing.Color.Transparent
            Me.XrLabel3.CanGrow = False
            Me.XrLabel3.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel3.ForeColor = System.Drawing.Color.White
            Me.XrLabel3.LocationFloat = New DevExpress.Utils.PointFloat(83.0!, 0.0!)
            Me.XrLabel3.Name = "XrLabel3"
            Me.XrLabel3.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel3.SizeF = New System.Drawing.SizeF(183.0!, 15.0!)
            Me.XrLabel3.StylePriority.UseBackColor = False
            Me.XrLabel3.StylePriority.UseBorderColor = False
            Me.XrLabel3.StylePriority.UseFont = False
            Me.XrLabel3.StylePriority.UseForeColor = False
            Me.XrLabel3.StylePriority.UseTextAlignment = False
            Me.XrLabel3.Text = "CREDITOR"
            Me.XrLabel3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            '
            'XrLabel1
            '
            Me.XrLabel1.BackColor = System.Drawing.Color.Transparent
            Me.XrLabel1.BorderColor = System.Drawing.Color.Transparent
            Me.XrLabel1.CanGrow = False
            Me.XrLabel1.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel1.ForeColor = System.Drawing.Color.White
            Me.XrLabel1.LocationFloat = New DevExpress.Utils.PointFloat(700.0!, 0.0!)
            Me.XrLabel1.Name = "XrLabel1"
            Me.XrLabel1.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel1.SizeF = New System.Drawing.SizeF(100.0!, 15.0!)
            Me.XrLabel1.StylePriority.UseBackColor = False
            Me.XrLabel1.StylePriority.UseBorderColor = False
            Me.XrLabel1.StylePriority.UseFont = False
            Me.XrLabel1.StylePriority.UseForeColor = False
            Me.XrLabel1.StylePriority.UseTextAlignment = False
            Me.XrLabel1.Text = "BALANCE"
            Me.XrLabel1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'ReportFooter
            '
            Me.ReportFooter.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel_grand_total_payments, Me.XrLabel15, Me.XrLabel_grand_total_fees, Me.XrLabel13, Me.XrLabel_grand_total_interest, Me.XrLabel11, Me.XrLine1, Me.XrLabel_grand_total_balance, Me.XrLabel_number_of_months, Me.XrLabel9, Me.XrLabel2, Me.XrRichText1})
            Me.ReportFooter.HeightF = 117.0!
            Me.ReportFooter.Name = "ReportFooter"
            Me.ReportFooter.PrintAtBottom = True
            '
            'XrLabel_grand_total_payments
            '
            Me.XrLabel_grand_total_payments.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "payment")})
            Me.XrLabel_grand_total_payments.ForeColor = System.Drawing.Color.Teal
            Me.XrLabel_grand_total_payments.LocationFloat = New DevExpress.Utils.PointFloat(650.0!, 75.0!)
            Me.XrLabel_grand_total_payments.Name = "XrLabel_grand_total_payments"
            Me.XrLabel_grand_total_payments.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_grand_total_payments.SizeF = New System.Drawing.SizeF(150.0!, 16.0!)
            Me.XrLabel_grand_total_payments.StylePriority.UseForeColor = False
            Me.XrLabel_grand_total_payments.StylePriority.UseTextAlignment = False
            XrSummary5.FormatString = "{0:c}"
            XrSummary5.IgnoreNullValues = True
            XrSummary5.Running = DevExpress.XtraReports.UI.SummaryRunning.Report
            Me.XrLabel_grand_total_payments.Summary = XrSummary5
            Me.XrLabel_grand_total_payments.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel15
            '
            Me.XrLabel15.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel15.ForeColor = System.Drawing.Color.Teal
            Me.XrLabel15.LocationFloat = New DevExpress.Utils.PointFloat(475.0!, 75.0!)
            Me.XrLabel15.Name = "XrLabel15"
            Me.XrLabel15.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel15.SizeF = New System.Drawing.SizeF(167.0!, 16.0!)
            Me.XrLabel15.StylePriority.UseFont = False
            Me.XrLabel15.StylePriority.UseForeColor = False
            Me.XrLabel15.Text = "TOTAL PAYMENTS:"
            '
            'XrLabel_grand_total_fees
            '
            Me.XrLabel_grand_total_fees.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "fees")})
            Me.XrLabel_grand_total_fees.ForeColor = System.Drawing.Color.Teal
            Me.XrLabel_grand_total_fees.LocationFloat = New DevExpress.Utils.PointFloat(650.0!, 58.0!)
            Me.XrLabel_grand_total_fees.Name = "XrLabel_grand_total_fees"
            Me.XrLabel_grand_total_fees.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_grand_total_fees.SizeF = New System.Drawing.SizeF(150.0!, 16.0!)
            Me.XrLabel_grand_total_fees.StylePriority.UseForeColor = False
            Me.XrLabel_grand_total_fees.StylePriority.UseTextAlignment = False
            XrSummary6.FormatString = "{0:c}"
            XrSummary6.IgnoreNullValues = True
            XrSummary6.Running = DevExpress.XtraReports.UI.SummaryRunning.Report
            Me.XrLabel_grand_total_fees.Summary = XrSummary6
            Me.XrLabel_grand_total_fees.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel13
            '
            Me.XrLabel13.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel13.ForeColor = System.Drawing.Color.Teal
            Me.XrLabel13.LocationFloat = New DevExpress.Utils.PointFloat(475.0!, 58.0!)
            Me.XrLabel13.Name = "XrLabel13"
            Me.XrLabel13.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel13.SizeF = New System.Drawing.SizeF(167.0!, 16.0!)
            Me.XrLabel13.StylePriority.UseFont = False
            Me.XrLabel13.StylePriority.UseForeColor = False
            Me.XrLabel13.Text = "TOTAL FEES:"
            '
            'XrLabel_grand_total_interest
            '
            Me.XrLabel_grand_total_interest.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "interest")})
            Me.XrLabel_grand_total_interest.ForeColor = System.Drawing.Color.Teal
            Me.XrLabel_grand_total_interest.LocationFloat = New DevExpress.Utils.PointFloat(650.0!, 42.0!)
            Me.XrLabel_grand_total_interest.Name = "XrLabel_grand_total_interest"
            Me.XrLabel_grand_total_interest.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_grand_total_interest.SizeF = New System.Drawing.SizeF(150.0!, 16.0!)
            Me.XrLabel_grand_total_interest.StylePriority.UseForeColor = False
            Me.XrLabel_grand_total_interest.StylePriority.UseTextAlignment = False
            XrSummary7.FormatString = "{0:c}"
            XrSummary7.IgnoreNullValues = True
            XrSummary7.Running = DevExpress.XtraReports.UI.SummaryRunning.Report
            Me.XrLabel_grand_total_interest.Summary = XrSummary7
            Me.XrLabel_grand_total_interest.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel11
            '
            Me.XrLabel11.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel11.ForeColor = System.Drawing.Color.Teal
            Me.XrLabel11.LocationFloat = New DevExpress.Utils.PointFloat(475.0!, 42.0!)
            Me.XrLabel11.Name = "XrLabel11"
            Me.XrLabel11.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel11.SizeF = New System.Drawing.SizeF(167.0!, 16.0!)
            Me.XrLabel11.StylePriority.UseFont = False
            Me.XrLabel11.StylePriority.UseForeColor = False
            Me.XrLabel11.Text = "TOTAL INTEREST:"
            '
            'XrLine1
            '
            Me.XrLine1.ForeColor = System.Drawing.Color.Teal
            Me.XrLine1.LineWidth = 2
            Me.XrLine1.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 8.0!)
            Me.XrLine1.Name = "XrLine1"
            Me.XrLine1.SizeF = New System.Drawing.SizeF(800.0!, 9.0!)
            Me.XrLine1.StylePriority.UseForeColor = False
            '
            'XrLabel_grand_total_balance
            '
            Me.XrLabel_grand_total_balance.ForeColor = System.Drawing.Color.Teal
            Me.XrLabel_grand_total_balance.LocationFloat = New DevExpress.Utils.PointFloat(650.0!, 25.0!)
            Me.XrLabel_grand_total_balance.Name = "XrLabel_grand_total_balance"
            Me.XrLabel_grand_total_balance.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_grand_total_balance.SizeF = New System.Drawing.SizeF(150.0!, 16.0!)
            Me.XrLabel_grand_total_balance.StylePriority.UseForeColor = False
            Me.XrLabel_grand_total_balance.StylePriority.UseTextAlignment = False
            Me.XrLabel_grand_total_balance.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel_number_of_months
            '
            Me.XrLabel_number_of_months.ForeColor = System.Drawing.Color.Teal
            Me.XrLabel_number_of_months.LocationFloat = New DevExpress.Utils.PointFloat(650.0!, 92.0!)
            Me.XrLabel_number_of_months.Name = "XrLabel_number_of_months"
            Me.XrLabel_number_of_months.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_number_of_months.SizeF = New System.Drawing.SizeF(150.0!, 16.0!)
            Me.XrLabel_number_of_months.StylePriority.UseForeColor = False
            Me.XrLabel_number_of_months.StylePriority.UseTextAlignment = False
            Me.XrLabel_number_of_months.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel9
            '
            Me.XrLabel9.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel9.ForeColor = System.Drawing.Color.Teal
            Me.XrLabel9.LocationFloat = New DevExpress.Utils.PointFloat(475.0!, 25.0!)
            Me.XrLabel9.Name = "XrLabel9"
            Me.XrLabel9.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel9.SizeF = New System.Drawing.SizeF(167.0!, 16.0!)
            Me.XrLabel9.StylePriority.UseFont = False
            Me.XrLabel9.StylePriority.UseForeColor = False
            Me.XrLabel9.Text = "ORIGINAL BALANCE:"
            '
            'XrLabel2
            '
            Me.XrLabel2.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel2.ForeColor = System.Drawing.Color.Teal
            Me.XrLabel2.LocationFloat = New DevExpress.Utils.PointFloat(475.0!, 92.0!)
            Me.XrLabel2.Name = "XrLabel2"
            Me.XrLabel2.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel2.SizeF = New System.Drawing.SizeF(167.0!, 16.0!)
            Me.XrLabel2.StylePriority.UseFont = False
            Me.XrLabel2.StylePriority.UseForeColor = False
            Me.XrLabel2.Text = "NUMBER OF MONTHS:"
            '
            'XrRichText1
            '
            Me.XrRichText1.KeepTogether = True
            Me.XrRichText1.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 25.0!)
            Me.XrRichText1.Name = "XrRichText1"
            Me.XrRichText1.SerializableRtfString = resources.GetString("XrRichText1.SerializableRtfString")
            Me.XrRichText1.SizeF = New System.Drawing.SizeF(458.0!, 92.0!)
            '
            'ds
            '
            Me.ds.DataSetName = "dsDataSet"
            '
            'XrLabel_fees
            '
            Me.XrLabel_fees.CanGrow = False
            Me.XrLabel_fees.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "fees", "{0:c}")})
            Me.XrLabel_fees.LocationFloat = New DevExpress.Utils.PointFloat(550.0!, 0.0!)
            Me.XrLabel_fees.Name = "XrLabel_fees"
            Me.XrLabel_fees.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_fees.SizeF = New System.Drawing.SizeF(67.0!, 15.0!)
            Me.XrLabel_fees.StylePriority.UseTextAlignment = False
            Me.XrLabel_fees.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrLabel_fees.WordWrap = False
            '
            'GroupHeader2
            '
            Me.GroupHeader2.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrPanel1})
            Me.GroupHeader2.HeightF = 39.0!
            Me.GroupHeader2.Level = 1
            Me.GroupHeader2.Name = "GroupHeader2"
            Me.GroupHeader2.RepeatEveryPage = True
            '
            'GroupHeader3
            '
            Me.GroupHeader3.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrPanel_ClientAddress})
            Me.GroupHeader3.HeightF = 190.0!
            Me.GroupHeader3.Level = 2
            Me.GroupHeader3.Name = "GroupHeader3"
            '
            'XrPanel_ClientAddress
            '
            Me.XrPanel_ClientAddress.AnchorVertical = DevExpress.XtraReports.UI.VerticalAnchorStyles.Top
            Me.XrPanel_ClientAddress.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrBarCode_PostalCode, Me.XrLabel_Address})
            Me.XrPanel_ClientAddress.LocationFloat = New DevExpress.Utils.PointFloat(50.0!, 66.0!)
            Me.XrPanel_ClientAddress.Name = "XrPanel_ClientAddress"
            Me.XrPanel_ClientAddress.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
            Me.XrPanel_ClientAddress.SizeF = New System.Drawing.SizeF(300.0!, 100.0!)
            '
            'XrBarCode_PostalCode
            '
            Me.XrBarCode_PostalCode.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
            Me.XrBarCode_PostalCode.Name = "XrBarCode_PostalCode"
            Me.XrBarCode_PostalCode.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 10, 0, 4, 100.0!)
            Me.XrBarCode_PostalCode.ShowText = False
            Me.XrBarCode_PostalCode.SizeF = New System.Drawing.SizeF(300.0!, 17.0!)
            Me.XrBarCode_PostalCode.StylePriority.UsePadding = False
            Me.XrBarCode_PostalCode.Symbology = PostNetGenerator1
            Me.XrBarCode_PostalCode.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
            '
            'XrLabel_Address
            '
            Me.XrLabel_Address.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 17.0!)
            Me.XrLabel_Address.Multiline = True
            Me.XrLabel_Address.Name = "XrLabel_Address"
            Me.XrLabel_Address.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_Address.SizeF = New System.Drawing.SizeF(300.0!, 17.0!)
            Me.XrLabel_Address.Text = "XrLabel_Address"
            Me.XrLabel_Address.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
            Me.XrLabel_Address.WordWrap = False
            '
            'CalculatedField_Last4
            '
            Me.CalculatedField_Last4.Name = "CalculatedField_Last4"
            Me.CalculatedField_Last4.Scripts.OnGetValue = "CalculatedField_Last4_GetValue"
            '
            'ClientPayoutDetail
            '
            Me.Bands.AddRange(New DevExpress.XtraReports.UI.Band() {Me.Detail, Me.PageHeader, Me.PageFooter, Me.GroupHeader1, Me.GroupFooter1, Me.ReportFooter, Me.GroupHeader2, Me.GroupHeader3, Me.TopMarginBand1, Me.BottomMarginBand1})
            Me.CalculatedFields.AddRange(New DevExpress.XtraReports.UI.CalculatedField() {Me.CalculatedField_Last4})
            Me.ScriptLanguage = DevExpress.XtraReports.ScriptLanguage.VisualBasic
            Me.ScriptsSource = resources.GetString("$this.ScriptsSource")
            Me.StyleSheet.AddRange(New DevExpress.XtraReports.UI.XRControlStyle() {Me.XrControlStyle_Header, Me.XrControlStyle_Totals, Me.XrControlStyle_HeaderPannel, Me.XrControlStyle_GroupHeader})
            Me.Version = "11.2"
            Me.Controls.SetChildIndex(Me.BottomMarginBand1, 0)
            Me.Controls.SetChildIndex(Me.TopMarginBand1, 0)
            Me.Controls.SetChildIndex(Me.GroupHeader3, 0)
            Me.Controls.SetChildIndex(Me.GroupHeader2, 0)
            Me.Controls.SetChildIndex(Me.ReportFooter, 0)
            Me.Controls.SetChildIndex(Me.GroupFooter1, 0)
            Me.Controls.SetChildIndex(Me.GroupHeader1, 0)
            Me.Controls.SetChildIndex(Me.PageFooter, 0)
            Me.Controls.SetChildIndex(Me.PageHeader, 0)
            Me.Controls.SetChildIndex(Me.Detail, 0)
            CType(Me.XrRichText1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.ds, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me, System.ComponentModel.ISupportInitialize).EndInit()

        End Sub

        Protected Friend WithEvents XrLabel_balance As DevExpress.XtraReports.UI.XRLabel
        Protected Friend WithEvents XrLabel_interest As DevExpress.XtraReports.UI.XRLabel
        Protected Friend WithEvents XrLabel_payment As DevExpress.XtraReports.UI.XRLabel
        Protected Friend WithEvents XrLabel_creditor_name As DevExpress.XtraReports.UI.XRLabel
        Protected Friend WithEvents XrLabel_date As DevExpress.XtraReports.UI.XRLabel
        Protected Friend WithEvents XrLabel_account_number As DevExpress.XtraReports.UI.XRLabel
        Protected Friend WithEvents GroupHeader1 As DevExpress.XtraReports.UI.GroupHeaderBand
        Protected Friend WithEvents GroupFooter1 As DevExpress.XtraReports.UI.GroupFooterBand
        Protected Friend WithEvents XrLabel5 As DevExpress.XtraReports.UI.XRLabel
        Protected Friend WithEvents XrLabel_total_balance As DevExpress.XtraReports.UI.XRLabel
        Protected Friend WithEvents XrLabel_total_interest As DevExpress.XtraReports.UI.XRLabel
        Protected Friend WithEvents XrLabel_total_payment As DevExpress.XtraReports.UI.XRLabel
        Protected Friend WithEvents XrPanel1 As DevExpress.XtraReports.UI.XRPanel
        Protected Friend WithEvents XrLabel8 As DevExpress.XtraReports.UI.XRLabel
        Protected Friend WithEvents XrLabel7 As DevExpress.XtraReports.UI.XRLabel
        Protected Friend WithEvents XrLabel6 As DevExpress.XtraReports.UI.XRLabel
        Protected Friend WithEvents XrLabel4 As DevExpress.XtraReports.UI.XRLabel
        Protected Friend WithEvents XrLabel3 As DevExpress.XtraReports.UI.XRLabel
        Protected Friend WithEvents XrLabel1 As DevExpress.XtraReports.UI.XRLabel
        Protected Friend WithEvents ReportFooter As DevExpress.XtraReports.UI.ReportFooterBand
        Protected Friend WithEvents XrLabel2 As DevExpress.XtraReports.UI.XRLabel
        Protected Friend WithEvents XrLabel_grand_total_balance As DevExpress.XtraReports.UI.XRLabel
        Protected Friend WithEvents XrLabel_number_of_months As DevExpress.XtraReports.UI.XRLabel
        Protected Friend WithEvents XrLabel9 As DevExpress.XtraReports.UI.XRLabel
        Protected Friend WithEvents XrLine1 As DevExpress.XtraReports.UI.XRLine
        Protected Friend WithEvents ds As System.Data.DataSet
        Protected Friend WithEvents XrLabel_fees As DevExpress.XtraReports.UI.XRLabel
        Protected Friend WithEvents XrLabel10 As DevExpress.XtraReports.UI.XRLabel
        Protected Friend WithEvents XrLabel_total_fees As DevExpress.XtraReports.UI.XRLabel
        Protected Friend WithEvents XrLabel_grand_total_payments As DevExpress.XtraReports.UI.XRLabel
        Protected Friend WithEvents XrLabel15 As DevExpress.XtraReports.UI.XRLabel
        Protected Friend WithEvents XrLabel_grand_total_fees As DevExpress.XtraReports.UI.XRLabel
        Protected Friend WithEvents XrLabel13 As DevExpress.XtraReports.UI.XRLabel
        Protected Friend WithEvents XrLabel_grand_total_interest As DevExpress.XtraReports.UI.XRLabel
        Protected Friend WithEvents XrLabel11 As DevExpress.XtraReports.UI.XRLabel
        Protected Friend WithEvents GroupHeader2 As DevExpress.XtraReports.UI.GroupHeaderBand
        Protected Friend WithEvents GroupHeader3 As DevExpress.XtraReports.UI.GroupHeaderBand
        Protected Friend WithEvents XrPanel_ClientAddress As DevExpress.XtraReports.UI.XRPanel
        Protected Friend WithEvents XrBarCode_PostalCode As DevExpress.XtraReports.UI.XRBarCode
        Protected Friend WithEvents XrLabel_Address As DevExpress.XtraReports.UI.XRLabel
        Protected Friend WithEvents XrRichText1 As DevExpress.XtraReports.UI.XRRichText
        Friend WithEvents CalculatedField_Last4 As DevExpress.XtraReports.UI.CalculatedField
    End Class
End Namespace