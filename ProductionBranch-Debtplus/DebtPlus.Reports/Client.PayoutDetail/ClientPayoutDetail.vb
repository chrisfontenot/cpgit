#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}

#End Region

Option Compare Binary
Option Explicit On
Option Strict Off

Imports DebtPlus.Interfaces.Debt
Imports DebtPlus.Interfaces.Client
Imports DebtPlus.Svc.Debt
Imports DebtPlus.Events
Imports System.Drawing.Printing
Imports DebtPlus.Reports.Template.Forms
Imports DebtPlus.Reports.Client.PayoutDetail.Debts
Imports System.Threading
Imports System.Data.SqlClient
Imports System.Text
Imports DebtPlus.Utils.Format
Imports DebtPlus.LINQ
Imports System.Linq

Namespace Client.PayoutDetail
    Public Class ClientPayoutDetail
        Implements IClient

        Public Sub New()
            MyBase.New()
            InitializeComponent()
        End Sub

        ''' <summary>
        ''' Create a new instance of our class
        ''' </summary>
        Public Sub New(ByVal DebtList As IDebtRecordList)
            MyClass.New()
            PreviousClientDebtList = DebtList
            AddHandler Me.BeforePrint, AddressOf Report_BeforePrint
            AddHandler XrLabel_creditor_name.BeforePrint, AddressOf XrLabel_creditor_name_BeforePrint
            AddHandler XrPanel_ClientAddress.BeforePrint, AddressOf XrPanel_ClientAddress_BeforePrint
        End Sub

#Region "QueryValue"
        Public Event QueryValue As DebtPlus.Events.ParameterValueEventHandler

        Protected Overridable Sub onQueryValue(ByVal e As DebtPlus.Events.ParameterValueEventArgs)
            RaiseEvent QueryValue(Me, e)
        End Sub

        Protected Function RaiseQueryValue(ByVal name As String) As Object
            Dim e As New ParameterValueEventArgs(name)
            onQueryValue(e)
            Return e.Value
        End Function
#End Region

#Region " Parameters "

        Public Property ClientId() As Int32 = -1 Implements IClient.ClientId

        Public Overrides Sub SetReportParameter(ByVal Parameter As String, ByVal Value As Object)
            Select Case Parameter
                Case "Client"
                    ClientId = Convert.ToInt32(Value)
                Case Else
                    MyBase.SetReportParameter(Parameter, Value)
            End Select
        End Sub

        Public Overrides Function NeedParameters() As Boolean
            Return MyBase.NeedParameters() OrElse ClientId < 0
        End Function

        Public Overrides Function RequestReportParameters() As DialogResult
            Dim answer As DialogResult = DialogResult.OK
            If NeedParameters() Then
                Using frm As New ClientParametersForm
                    frm.Parameter_Client = ClientId
                    answer = frm.ShowDialog()
                    ClientId = frm.Parameter_Client
                End Using
            End If
            Return answer
        End Function

#End Region

        Private _totalDebt As System.Decimal

        Public Property TotalDebt As System.Decimal
            Get
                Return _totalDebt
            End Get
            Set(ByVal Value As System.Decimal)
                _totalDebt = Value
            End Set
        End Property

        Private _monthsToPayOut As System.Int32
        Public Property MonthsToPayOut As System.Int32
            Get
                Return _monthsToPayOut
            End Get
            Set(ByVal Value As System.Int32)
                _monthsToPayOut = Value
            End Set
        End Property

        Private _totaldeposit As System.Decimal
        Public Property Totaldeposit As System.Decimal
            Get
                Return _totaldeposit
            End Get
            Set(ByVal Value As System.Decimal)
                _totaldeposit = Value
            End Set
        End Property

        Private _totalfees As System.Decimal
        Public Property Totalfees As System.Decimal
            Get
                Return _totalfees
            End Get
            Set(ByVal Value As System.Decimal)
                _totalfees = Value
            End Set
        End Property


        Private _terminationDate As DateTime
        Public Property TerminationDate As DateTime
            Get
                Return _terminationDate
            End Get
            Set(ByVal Value As DateTime)
                _terminationDate = Value
            End Set
        End Property

        Private _startDate As DateTime
        Public Property StartDate As DateTime
            Get
                Return _startDate
            End Get
            Set(ByVal Value As DateTime)
                _startDate = Value
            End Set
        End Property

        Private ReadOnly PreviousClientDebtList As IDebtRecordList

        ''' <summary>
        ''' Report title line
        ''' </summary>
        Public Overrides ReadOnly Property ReportTitle() As String
            Get
                Return "Payout Schedule"
            End Get
        End Property


        ''' <summary>
        ''' Report subtitle line
        ''' </summary>
        Public Overrides ReadOnly Property ReportSubTitle() As String
            Get
                Dim Answer As String = String.Format("This report is for client {0}, deposit amount = {1:c}", String.Format("{0:0000000}", ClientId), ClientDepositAmount)
                Return Answer
            End Get
        End Property


        ''' <summary>
        ''' Refresh the data for the report
        ''' </summary>
        Protected ReportTbl As DataTable
        Protected vue As DataView

        Private Sub Report_BeforePrint(ByVal sender As Object, ByVal e As EventArgs)

            ' Bind the data to the corresponding view if needed
            If vue Is Nothing Then
                If ReportTbl Is Nothing Then

                    ' Read the client information so that we may update the payout data
                    Using bc As New BusinessContext()
                        Dim clientRecord As DebtPlus.LINQ.client = bc.clients.Where(Function(s) s.Id = ClientId).FirstOrDefault()

                        ' The client should exist. If it does not then we can't do the update but still allocate the storage for it so that we can do the payout
                        If clientRecord Is Nothing Then
                            clientRecord = New DebtPlus.LINQ.client()
                        End If

                        ' Generate the payout details. This will update the client record
                        ReportTbl = CreateTable(clientRecord)

                        ' Put the changes back into the database now.
                        bc.SubmitChanges()
                    End Using
                End If

                vue = New DataView(ReportTbl, String.Empty, "date, creditor, account_number, ID", DataViewRowState.CurrentRows)
            End If
            DataSource = vue
        End Sub


        ''' <summary>
        ''' Build the table with the data for the report
        ''' </summary>
        Protected result As DataTable = ResultTable()

        Protected Function CreateTable(ByRef clientRecord As DebtPlus.LINQ.client) As DataTable

            Dim NumberOfMonths As Int32 = 0
            Dim DebtsTable As PayoutDebtList = Nothing

            ' Clear the client payout information section
            clientRecord.payout_total_payments = 0D
            clientRecord.payout_total_interest = 0D
            clientRecord.payout_total_fees = 0D
            clientRecord.payout_total_debt = 0D
            clientRecord.payout_deposit_amount = 0D
            clientRecord.payout_cushion_amount = 0D
            clientRecord.payout_months_to_payout = 0
            clientRecord.payout_termination_date = Nothing

            ' Find the client deposit amount. If not available then stop here.
            Dim DepositTotal As Decimal = ClientDepositAmount()
            If DepositTotal > 0D Then
                clientRecord.payout_deposit_amount = DepositTotal

                ' Validate the arguments before starting
                If PreviousClientDebtList Is Nothing Then
                    Throw New ArgumentNullException("PreviousClientDebtList")
                End If

                ' Find the client's start date
                Dim start_date As DateTime = ClientStartDate()
                StartDate = start_date
                Dim CurrentDate As DateTime = DateTime.Now.Date

                ' Use the current date if the start date is in the past.
                If start_date = Date.MaxValue OrElse start_date < CurrentDate Then start_date = CurrentDate.AddMonths(1)

                ' Read a list of the debts for the client
                DebtsTable = New PayoutDebtList()
                DebtsTable.CloneDebtList(PreviousClientDebtList)

                ' Find and pay the setup fee. This is paid only once at the first month.
                Dim setupDebt As PayoutDebtRecord = DebtsTable.Cast(Of PayoutDebtRecord).Where(Function(s) s.DebtType = DebtTypeEnum.SetupFee).FirstOrDefault()
                If setupDebt IsNot Nothing Then
                    Dim setupAmount = setupDebt.adjusted_orig_balance
                    If setupAmount > 0D Then

                        ' Pay the debt the entire balance in the first month.
                        setupDebt.total_payments = setupAmount
                        setupDebt.expected_payout_date = start_date
                        setupDebt.IsPayoff = False
                        setupDebt.IsActive = False
                        setupDebt.debit_amt = 0D

                        ' Generate the detail row to pay the debt on the first month.
                        Dim row As DataRow = result.NewRow
                        result.Rows.Add(row)
                        row.BeginEdit()

                        row("payment") = setupAmount
                        row("fees") = setupAmount
                        row("balance") = 0D
                        row("date") = start_date
                        row("creditor") = setupDebt.Creditor
                        row("creditor_name") = setupDebt.display_creditor_name
                        row("account_number") = setupDebt.account_number

                        row.EndEdit()

                        ' Update the statistics correctly
                        clientRecord.payout_total_fees += setupAmount
                        clientRecord.payout_total_payments += setupAmount
                        clientRecord.payout_termination_date = start_date
                    End If
                End If

                ' Generate the report for at most 10 years. If it does not converge by then, it will probably never converge.
                For Months As Int32 = 0 To 119
                    CurrentDate = start_date.AddMonths(Months)

                    ' Copy the disbursement factor to the debts
                    Dim TotalCurrentBalance As Decimal = 0D

                    ' We need to pre-compute the disbursement balance on the debts before we go into the prorate function
                    ' or the fee will be $0.00 when it comes out.
                    For Each item As PayoutDebtRecord In DebtsTable

                        ' very special debts never have a disbursement
                        If item.DebtType = DebtTypeEnum.NonManaged OrElse item.DebtType = DebtTypeEnum.Fairshare OrElse item.DebtType = DebtTypeEnum.Secured Then
                            item.debit_amt = 0D
                            Continue For
                        End If

                        ' If the debt does not have a balance then ensure that the balance shown is $0.00
                        If item.ccl_zero_balance Then
                            item.total_interest = item.total_payments
                            item.orig_balance = 0D
                            item.orig_balance_adjustment = 0D
                        End If

                        ' If the items is marked as being always disburse then use the disbursement factor and don't check the balance
                        item.debit_amt = item.disbursement_factor
                        If item.ccl_always_disburse Then
                            item.expected_payout_date = CurrentDate
                            Continue For
                        End If

                        ' Process the payment cushion before we look at the balances. We need to leave the disbursement amount set
                        ' but clear the active flag for the next time.
                        If item.DebtType = DebtTypeEnum.PaymentCushion AndAlso item.IsActive AndAlso Not item.IsPayoff Then
                            clientRecord.payout_cushion_amount = item.current_balance
                            item.debit_amt = item.current_balance
                            item.expected_payout_date = CurrentDate
                            item.IsPayoff = True
                            Continue For
                        End If

                        ' If the record has been paid off then it is no longer active
                        If item.IsPayoff Then
                            item.IsPayoff = False
                            item.IsActive = False
                        End If

                        ' If the debt is no longer active then there is no disbursement
                        If Not item.IsActive Then
                            item.debit_amt = 0D
                            Continue For
                        End If

                        ' If the record may be prorated then we need to look at the balance
                        item.expected_payout_date = CurrentDate
                        TotalCurrentBalance += item.current_balance

                        If item.IsProratable Then
                            If item.current_balance <= 0D Then
                                item.debit_amt = 0D
                                item.IsActive = False
                            Else
                                ' Limit the debt payment to the balance
                                If item.debit_amt >= item.current_balance Then
                                    item.debit_amt = item.current_balance
                                    item.IsPayoff = True
                                    Continue For
                                End If
                            End If
                        End If

                        ' Finally, we come to the end for all standard debts. Limit the disbursement to the balance if possible.
                        If Not item.ccl_zero_balance AndAlso item.debit_amt > item.current_balance AndAlso item.current_balance > 0D Then
                            item.debit_amt = item.current_balance
                            item.IsPayoff = True
                        End If
                    Next

                    ' OK. We have made the pre-pass through the debts to adjust the initial stab at the disbursement
                    ' Now, update the figures on the first time only.
                    If Months = 0 Then
                        XrLabel_grand_total_balance.Text = String.Format("{0:c}", TotalCurrentBalance)
                        TotalDebt = TotalCurrentBalance
                        clientRecord.payout_total_debt = TotalCurrentBalance
                    End If

                    ' We always need to prorate the debts since the money is never correct
                    Using cls As New PayoutProrate(DebtsTable, False)
                        cls.Prorate(DepositTotal, True)
                    End Using

                    ' After possibly paying off debts in the prorate then it is time to re-evaluate the payments
                    ' based upon the fact that all of the debts may be paid. If this is the case then we just need
                    ' to re-compute the fee amount for the new value.
                    Dim AllDebtsInPayoff As Boolean = True
                    For Each item As PayoutDebtRecord In DebtsTable
                        If item.debit_amt > 0D AndAlso Not item.IsFee AndAlso Not item.ccl_zero_balance AndAlso item.debit_amt < item.current_balance Then
                            AllDebtsInPayoff = False
                            Exit For
                        End If
                    Next

                    ' If everything is paid off then calculate a new disbursement based upon the proper fee amount.
                    If AllDebtsInPayoff Then
                        Using cls As New PayoutProrate(DebtsTable, True)
                            cls.Prorate(DepositTotal, True)
                        End Using
                    End If

                    ' Generate the payment record for each of the debts in the list on this date
                    ' Do not pay the monthly fee figures first. Move them to the end of the list.
                    Dim creditorsPaid As Boolean = False
                    Dim debtsPaid As Boolean = False
                    For Each record As PayoutDebtRecord In DebtsTable

                        ' First, ignore records that have no debit amount. We don't pay those.
                        If record.debit_amt <= 0D Then
                            Continue For
                        End If

                        ' Ignore the monthly fee creditor for now. It will be handled below.
                        If record.DebtType = DebtTypeEnum.MonthlyFee Then
                            Continue For
                        End If

                        ' Indicate that we paid one or more creditors if needed. Do this only for external creditors and not agency fees.
                        If record.DebtType = DebtPlus.Interfaces.Debt.DebtTypeEnum.Normal Then
                            creditorsPaid = True
                        End If

                        ' Allocate a new row for the report
                        Dim row As DataRow = result.NewRow
                        result.Rows.Add(row)
                        row.BeginEdit()
                        debtsPaid = True

                        ' Pay the debt for this month. Do this first...
                        record.total_payments += record.debit_amt
                        row("payment") = record.debit_amt

                        ' Fill in the information for the row
                        row("date") = CurrentDate
                        row("creditor") = record.Creditor
                        row("creditor_name") = record.display_creditor_name
                        row("account_number") = record.account_number

                        ' Properly count the item as a fee or interest
                        If record.ccl_agency_account Then
                            row("fees") = record.debit_amt
                        Else
                            row("interest") = record.interest_amount
                        End If

                        ' Calculate the ending balance figure
                        If record.ccl_zero_balance Then
                            row("balance") = 0D
                        Else
                            record.total_interest += record.interest_amount
                            row("balance") = record.current_balance
                        End If

                        row.EndEdit()

                        ' Update the totals in the client payout section
                        clientRecord.payout_total_fees += DebtPlus.Utils.Nulls.DDec(row("fees"))
                        clientRecord.payout_total_interest += DebtPlus.Utils.Nulls.DDec(row("interest"))
                        clientRecord.payout_total_payments += DebtPlus.Utils.Nulls.DDec(row("payment"))
                    Next

                    ' If there were creditors that were paid then we need to asses the monthly fee
                    If creditorsPaid Then
                        NumberOfMonths += 1

                        Dim record As PayoutDebtRecord = DebtsTable.MonthlyFeeDebt()
                        If record IsNot Nothing Then

                            ' Allocate a new row for the report
                            Dim row As DataRow = result.NewRow
                            result.Rows.Add(row)
                            row.BeginEdit()
                            debtsPaid = True

                            ' Pay the debt for this month. Do this first...
                            record.total_payments += record.debit_amt
                            row("payment") = record.debit_amt

                            ' Fill in the information for the row
                            row("fees") = record.debit_amt
                            row("date") = CurrentDate
                            row("creditor") = record.Creditor
                            row("creditor_name") = record.display_creditor_name
                            row("account_number") = record.account_number

                            ' Calculate the ending balance figure
                            If record.ccl_zero_balance Then
                                row("balance") = 0D
                            Else
                                row("balance") = record.current_balance
                            End If

                            row.EndEdit()

                            ' Correct the information for the totals
                            clientRecord.payout_total_fees += DebtPlus.Utils.Nulls.DDec(row("fees"))
                            clientRecord.payout_total_payments += DebtPlus.Utils.Nulls.DDec(row("payment"))

                            ' If this is the first month (the one with the highest fee) then save the fee for the statistics.
                            If Months < 2 Then
                                clientRecord.payout_monthly_fee = DebtPlus.Utils.Nulls.DDec(row("payment"))
                            End If
                        End If
                    End If

                    ' If there are no payments for this month then stop processing.
                    If Not debtsPaid Then
                        Exit For
                    End If

                    ' Remember the current date as the ending date for the payout schedule
                    clientRecord.payout_termination_date = CurrentDate
                Next
            End If

            ' Set the number of months in the report
            XrLabel_number_of_months.Text = String.Format("{0:n0}", NumberOfMonths)
            clientRecord.payout_months_to_payout = NumberOfMonths

            ' Update the payout date for the debts
            If DebtsTable IsNot Nothing Then
                Dim thrd As Thread = Nothing
                If PreviousClientDebtList IsNot Nothing Then
                    thrd = New Thread(New ParameterizedThreadStart(AddressOf UpdateDebts))
                Else
                    thrd = New Thread(New ParameterizedThreadStart(AddressOf WriteDebts))
                End If

                With thrd
                    .IsBackground = False
                    .Name = "UpdateDebts"
                    .SetApartmentState(ApartmentState.STA)
                    .Start(DebtsTable)
                End With
            End If

            Return result
        End Function

        ''' <summary>
        ''' Update the debt's payout date if no previous list is given
        ''' </summary>
        Private Sub WriteDebts(ByVal obj As Object)
            Dim ThreadDebtsTable As PayoutDebtList = CType(obj, PayoutDebtList)
            Dim cn As SqlConnection = New System.Data.SqlClient.SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
            Try
                cn.Open()
                For Each Debt As PayoutDebtRecord In ThreadDebtsTable
                    Using cmd As New System.Data.SqlClient.SqlCommand()
                        With cmd
                            .Connection = cn
                            .CommandText = "UPDATE client_creditor SET [expected_payout_date]=@expected_payout_date WHERE [client_creditor]=@client_creditor"
                            .CommandType = CommandType.Text
                            .Parameters.Add("@expected_payout_date", SqlDbType.DateTime).Value = Debt.expected_payout_date
                            .Parameters.Add("@client_creditor", SqlDbType.Int).Value = Debt.client_creditor
                            .ExecuteNonQuery()
                        End With
                    End Using
                Next

            Catch ex As SqlException
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error updating client_creditor_record")

            Finally
                If cn IsNot Nothing Then cn.Dispose()
            End Try
        End Sub


        ''' <summary>
        ''' Update the caller's payout date if a previous list is given to us
        ''' </summary>
        Private Sub UpdateDebts(ByVal obj As Object)
            Dim threadDebtsTable As PayoutDebtList = CType(obj, PayoutDebtList)
            Dim previousList As BaseDebtList = TryCast(PreviousClientDebtList, BaseDebtList)
            If previousList IsNot Nothing Then
                For Each debt As PayoutDebtRecord In threadDebtsTable
                    Dim previousDebt As IDebtRecord = TryCast(previousList.Find(debt.client_creditor), IDebtRecord)
                    If previousDebt IsNot Nothing Then
                        previousDebt.expected_payout_date = debt.expected_payout_date
                    End If
                Next
            End If
        End Sub

        ''' <summary>
        ''' Create the table used in the report
        ''' </summary>
        Protected Function ResultTable() As DataTable

            ' Create the table for the resulting dataset
            Dim tbl As New DataTable("payout")
            With tbl
                Dim col As DataColumn = New DataColumn("ID", GetType(Int32))
                With col
                    .AutoIncrement = True
                    .AutoIncrementSeed = 1
                    .AutoIncrementStep = 1
                End With
                .Columns.Add(col)

                .Columns.Add("date", GetType(DateTime))
                .Columns.Add("creditor", GetType(String)).DefaultValue = String.Empty
                .Columns.Add("creditor_name", GetType(String)).DefaultValue = String.Empty
                .Columns.Add("account_number", GetType(String)).DefaultValue = String.Empty
                .Columns.Add("payment", GetType(Decimal)).DefaultValue = 0D
                .Columns.Add("interest", GetType(Decimal))
                .Columns.Add("fees", GetType(Decimal))
                .Columns.Add("balance", GetType(Decimal)).DefaultValue = 0D
            End With

            ' Put the table into a dataset
            If ds Is Nothing Then ds = New DataSet("ds")
            ds.Tables.Add(tbl)

            ' Return the resulting table
            Return tbl
        End Function


        Protected Function ClientDepositAmount() As Decimal

            ' Ask the creator for the proper value from their tables before going to the database for defaults
            Dim objAnswer As Object = RaiseQueryValue("client_deposit_amount")
            If objAnswer IsNot Nothing Then
                Return Convert.ToDecimal(objAnswer)
            End If

            Dim Answer As Decimal = 0D
            Dim cn As SqlConnection = New System.Data.SqlClient.SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
            Try
                cn.Open()
                Using cmd As New System.Data.SqlClient.SqlCommand()
                    cmd.Connection = cn
                    cmd.CommandText = "SELECT dbo.client_deposit_amount(@client)"
                    cmd.CommandType = CommandType.Text
                    cmd.Parameters.Add("@client", SqlDbType.Int).Value = ClientId
                    objAnswer = cmd.ExecuteScalar()
                    If objAnswer IsNot Nothing AndAlso objAnswer IsNot DBNull.Value Then Answer = Convert.ToDecimal(objAnswer)
                End Using

            Catch ex As SqlException
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error obtaining deposit totals")

            Finally
                If cn IsNot Nothing Then cn.Dispose()
            End Try

            Return Answer
        End Function



        ''' <summary>
        ''' Find the client's start date from the database
        ''' </summary>
        Protected Function ClientStartDate() As DateTime

            ' Ask the creator for the proper value from their tables before going to the database for defaults
            Dim objAnswer As Object = RaiseQueryValue("client_start_date")
            If objAnswer IsNot Nothing Then
                Return Convert.ToDateTime(objAnswer)
            End If

            Dim Answer As Date = Date.MaxValue
            Dim cn As SqlConnection = New System.Data.SqlClient.SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
            Try
                cn.Open()
                With New System.Data.SqlClient.SqlCommand()
                    .Connection = cn
                    .CommandText = "SELECT start_date FROM clients WITH (NOLOCK) WHERE client=@client"
                    .CommandType = CommandType.Text
                    .Parameters.Add("@client", SqlDbType.Int).Value = ClientId
                    objAnswer = .ExecuteScalar
                    If objAnswer IsNot Nothing AndAlso objAnswer IsNot DBNull.Value Then Answer = Convert.ToDateTime(objAnswer)
                End With

            Catch ex As SqlException
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error obtaining client start date")

            Finally
                If cn IsNot Nothing Then cn.Dispose()
            End Try

            Return Answer
        End Function


        ''' <summary>
        ''' When printing the creditor, include the ID as well as the name
        ''' </summary>
        Protected Sub XrLabel_creditor_name_BeforePrint(ByVal sender As Object, ByVal e As PrintEventArgs)
            Dim creditor_id As String = DebtPlus.Utils.Nulls.DStr(GetCurrentColumnValue("creditor"))
            Dim creditor_name As String = DebtPlus.Utils.Nulls.DStr(GetCurrentColumnValue("creditor_name"))

            If creditor_id <> String.Empty AndAlso creditor_name <> String.Empty Then
                XrLabel_creditor_name.Text = String.Format("{0} {1}", creditor_id, creditor_name)
            Else
                XrLabel_creditor_name.Text = creditor_id + creditor_name
            End If
        End Sub


        ''' <summary>
        ''' Obtain the client address
        ''' </summary>
        Private Sub XrPanel_ClientAddress_BeforePrint(ByVal sender As Object, ByVal e As PrintEventArgs)
            Dim row As DataRow = ClientAddressView(ClientId)
            If row IsNot Nothing Then

                ' Load the zipcode with the proper value
                Dim zipcode As String = DebtPlus.Utils.Format.Strings.DigitsOnly(DebtPlus.Utils.Nulls.DStr(row("zipcode")))
                XrBarCode_PostalCode.Text = zipcode

                ' Build the name and address information for the Client
                Dim sb As New StringBuilder
                For Each obj As Object In New Object() {row("name"), row("addr1"), row("addr2"), row("addr3")}
                    Dim str As String = DebtPlus.Utils.Nulls.DStr(obj).Trim()
                    If str <> String.Empty Then
                        sb.Append(Environment.NewLine)
                        sb.Append(str)
                    End If
                Next
                If sb.Length > 0 Then sb.Remove(0, 2)
                If sb.Length > 0 Then XrLabel_Address.Text = sb.ToString()
            End If
        End Sub


        ''' <summary>
        ''' Find the client address row pointer
        ''' </summary>
        Private Function ClientAddressView(ByVal ClientID As Int32) As DataRow
            Dim tbl As DataTable = ClientAddressTable()
            If tbl IsNot Nothing Then
                Dim row As DataRow = tbl.Rows.Find(ClientID)
                If row IsNot Nothing Then
                    Return row
                End If
            End If

            ReadClientAddress(ClientID)
            tbl = ClientAddressTable()
            Return tbl.Rows.Find(ClientID)
        End Function


        ''' <summary>
        ''' Read the client address row from the database
        ''' </summary>
        Private Sub ReadClientAddress(ByVal ClientID As Int32)
            Dim cn As SqlConnection = New System.Data.SqlClient.SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
            Dim Current_Cursor As Cursor = Cursor.Current
            Try
                Cursor.Current = Cursors.WaitCursor
                cn.Open()

                Using cmd As SqlCommand = New System.Data.SqlClient.SqlCommand()
                    With cmd
                        .Connection = cn
                        .CommandText = "SELECT [client],[name],[addr1],[addr2],[addr3],[zipcode] FROM view_client_address WHERE [client]=@client"
                        .CommandType = CommandType.Text
                        .Parameters.Add("@client", SqlDbType.Int).Value = ClientID
                        .CommandTimeout = 0
                    End With

                    Using da As New SqlDataAdapter(cmd)
                        da.MissingSchemaAction = MissingSchemaAction.Ignore
                        da.Fill(ds, "view_client_address")
                    End Using
                End Using

            Catch ex As SqlException
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Database Error")

            Finally
                If cn IsNot Nothing Then cn.Dispose()
                Cursor.Current = Current_Cursor
            End Try
        End Sub


        ''' <summary>
        ''' Read the schema for the addresses table
        ''' </summary>
        Private Function ClientAddressTable() As DataTable
            Dim tbl As DataTable = ds.Tables("view_client_address")

            If tbl Is Nothing Then
                Dim cn As SqlConnection = New System.Data.SqlClient.SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
                Dim Current_Cursor As Cursor = Cursor.Current
                Try
                    Cursor.Current = Cursors.WaitCursor
                    cn.Open()

                    Using cmd As SqlCommand = New System.Data.SqlClient.SqlCommand()
                        With cmd
                            .Connection = cn
                            .CommandText = "SELECT [client],[name],[addr1],[addr2],[addr3],[zipcode] FROM view_client_address"
                            .CommandType = CommandType.Text
                        End With

                        Using da As New SqlDataAdapter(cmd)
                            da.FillSchema(ds, SchemaType.Source, "view_client_address")
                        End Using
                    End Using
                    tbl = ds.Tables("view_client_address")

                Catch ex As SqlException
                    DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Database Error")

                Finally
                    If cn IsNot Nothing Then cn.Dispose()
                    Cursor.Current = Current_Cursor
                End Try
            End If

            Return tbl
        End Function
    End Class
End Namespace
