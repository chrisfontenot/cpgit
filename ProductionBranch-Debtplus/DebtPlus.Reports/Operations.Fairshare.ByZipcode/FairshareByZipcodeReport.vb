#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}

#End Region

Imports System.ComponentModel

Imports System.Drawing.Printing
Imports System.Data.SqlClient

Namespace Operations.Fairshare.ByZipcode
    Public Class FairshareByZipcodeReport
        Inherits DebtPlus.Reports.Template.DatedTemplateXtraReportClass

        Public Sub New()
            MyBase.New()
            InitializeComponent()
            AddHandler Me.BeforePrint, AddressOf Report_BeforePrint
            AddHandler XrLabel_city_footer.BeforePrint, AddressOf XrLabel_Footer_City_BeforePrint
            AddHandler XrLabel_postalcode_footer.BeforePrint, AddressOf XrLabel_Footer_PostalCode_BeforePrint
        End Sub

        Public Overrides ReadOnly Property ReportTitle() As String
            Get
                Return "Fairshare by Zipcode"
            End Get
        End Property

        Private Sub Report_BeforePrint(ByVal sender As Object, ByVal e As PrintEventArgs)

            ' Read the information from the dataset
            Dim ds As New DataSet("ds")
            Dim cn As SqlConnection = New System.Data.SqlClient.SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
            Try
                cn.Open()
                Using cmd As SqlCommand = New System.Data.SqlClient.SqlCommand()
                    With cmd
                        .Connection = cn
                        .CommandText = "rpt_client_by_zipcode"
                        .CommandType = CommandType.StoredProcedure
                        SqlCommandBuilder.DeriveParameters(cmd)

                        .Parameters(1).Value = Parameter_FromDate
                        .Parameters(2).Value = Parameter_ToDate
                        .CommandTimeout = 0
                    End With

                    Using da As New SqlDataAdapter(cmd)
                        da.Fill(ds, "client_fairshare")
                    End Using
                End Using

                ' Specify the dataset
                Dim tbl As DataTable = ds.Tables(0)

                Dim vue As New DataView(tbl, String.Empty, "city, postalcode", DataViewRowState.CurrentRows)
                DataSource = vue

                With XrLabel_fair_share
                    .DataBindings.Clear()
                    .DataBindings.Add("Text", vue, "fair_share", "{0:c}")
                End With

                With XrLabel_Group2_fair_share
                    .DataBindings.Clear()
                    .DataBindings.Add("Text", vue, "fair_share")
                End With

                With XrLabel_Group1_fair_share
                    .DataBindings.Clear()
                    .DataBindings.Add("Text", vue, "fair_share")
                End With

                With XrLabel_Report_total_fair_share
                    .DataBindings.Clear()
                    .DataBindings.Add("Text", vue, "fair_share")
                End With

                With XrLabel_Group1_Clients
                    .DataBindings.Clear()
                    .DataBindings.Add("Text", vue, "client")
                End With

                With XrLabel_city_header
                    .DataBindings.Clear()
                    .DataBindings.Add("Text", vue, "city")
                End With

                With XrLabel_postalcode_header
                    .DataBindings.Clear()
                    .DataBindings.Add("Text", vue, "postalcode")
                End With

            Catch ex As SqlException
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error reading data for report")

            Finally
                If cn IsNot Nothing Then cn.Dispose()
            End Try
        End Sub

        Private Sub XrLabel_Footer_City_BeforePrint(ByVal sender As Object, ByVal e As PrintEventArgs)
            XrLabel_city_footer.Text = "SUBTOTAL"
        End Sub

        Private Sub XrLabel_Footer_PostalCode_BeforePrint(ByVal sender As Object, ByVal e As PrintEventArgs)
            Dim postalcode As String = DebtPlus.Utils.Nulls.DStr(Me.GetCurrentColumnValue("postalcode"))
            If postalcode <> String.Empty Then
                XrLabel_postalcode_footer.Text = postalcode
            Else
                XrLabel_postalcode_footer.Text = "ZIPCODE TOTAL"
            End If
        End Sub
    End Class
End Namespace