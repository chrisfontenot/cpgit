Namespace Operations.Fairshare.ByZipcode
    <Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
    Partial Class FairshareByZipcodeReport

        'XtraReport overrides dispose to clean up the component list.
        <System.Diagnostics.DebuggerNonUserCode()> _
        Protected Overrides Sub Dispose(ByVal disposing As Boolean)
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
            MyBase.Dispose(disposing)
        End Sub

        'Required by the Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Designer
        'It can be modified using the Designer.  
        'Do not modify it using the code editor.
        <System.Diagnostics.DebuggerStepThrough()> _
        Private Sub InitializeComponent()
            Dim XrSummary1 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary
            Dim XrSummary2 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary
            Dim XrSummary3 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary
            Dim XrSummary4 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary
            Me.XrLabel_Name = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_fair_share = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel1_address = New DevExpress.XtraReports.UI.XRLabel
            Me.GroupHeader1 = New DevExpress.XtraReports.UI.GroupHeaderBand
            Me.XrLabel_postalcode_header = New DevExpress.XtraReports.UI.XRLabel
            Me.GroupHeader2 = New DevExpress.XtraReports.UI.GroupHeaderBand
            Me.XrLabel_city_header = New DevExpress.XtraReports.UI.XRLabel
            Me.GroupFooter1 = New DevExpress.XtraReports.UI.GroupFooterBand
            Me.XrLabel_Group1_Clients = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_postalcode_footer = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_Group1_fair_share = New DevExpress.XtraReports.UI.XRLabel
            Me.GroupFooter2 = New DevExpress.XtraReports.UI.GroupFooterBand
            Me.XrLabel_city_footer = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_Group2_fair_share = New DevExpress.XtraReports.UI.XRLabel
            Me.ReportFooter = New DevExpress.XtraReports.UI.ReportFooterBand
            Me.XrLabel8 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_Report_total_fair_share = New DevExpress.XtraReports.UI.XRLabel
            Me.XrPanel1 = New DevExpress.XtraReports.UI.XRPanel
            Me.XrLabel6 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel5 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel4 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel3 = New DevExpress.XtraReports.UI.XRLabel
            CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
            '
            'Detail
            '
            Me.Detail.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel_Name, Me.XrLabel_fair_share, Me.XrLabel1_address})
            Me.Detail.Height = 15
            Me.Detail.Visible = False
            '
            'PageHeader
            '
            Me.PageHeader.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrPanel1})
            Me.PageHeader.Height = 150
            '
            'XrLabel_Name
            '
            Me.XrLabel_Name.CanGrow = False
            Me.XrLabel_Name.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.XrLabel_Name.ForeColor = System.Drawing.Color.Black
            Me.XrLabel_Name.Location = New System.Drawing.Point(108, 0)
            Me.XrLabel_Name.Name = "XrLabel_Name"
            Me.XrLabel_Name.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_Name.Size = New System.Drawing.Size(292, 15)
            Me.XrLabel_Name.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            Me.XrLabel_Name.WordWrap = False
            '
            'XrLabel_fair_share
            '
            Me.XrLabel_fair_share.CanGrow = False
            Me.XrLabel_fair_share.Location = New System.Drawing.Point(595, 0)
            Me.XrLabel_fair_share.Name = "XrLabel_fair_share"
            Me.XrLabel_fair_share.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_fair_share.Size = New System.Drawing.Size(100, 15)
            Me.XrLabel_fair_share.StylePriority.UseTextAlignment = False
            Me.XrLabel_fair_share.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel1_address
            '
            Me.XrLabel1_address.CanGrow = False
            Me.XrLabel1_address.Location = New System.Drawing.Point(408, 0)
            Me.XrLabel1_address.Name = "XrLabel1_address"
            Me.XrLabel1_address.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel1_address.Size = New System.Drawing.Size(183, 15)
            Me.XrLabel1_address.StylePriority.UseTextAlignment = False
            Me.XrLabel1_address.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            '
            'GroupHeader1
            '
            Me.GroupHeader1.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel_postalcode_header})
            Me.GroupHeader1.GroupFields.AddRange(New DevExpress.XtraReports.UI.GroupField() {New DevExpress.XtraReports.UI.GroupField("postalcode", DevExpress.XtraReports.UI.XRColumnSortOrder.Ascending)})
            Me.GroupHeader1.Height = 15
            Me.GroupHeader1.Name = "GroupHeader1"
            Me.GroupHeader1.Visible = False
            '
            'XrLabel_postalcode_header
            '
            Me.XrLabel_postalcode_header.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel_postalcode_header.ForeColor = System.Drawing.Color.Maroon
            Me.XrLabel_postalcode_header.Location = New System.Drawing.Point(67, 0)
            Me.XrLabel_postalcode_header.Name = "XrLabel_postalcode_header"
            Me.XrLabel_postalcode_header.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_postalcode_header.Size = New System.Drawing.Size(333, 15)
            Me.XrLabel_postalcode_header.StylePriority.UseFont = False
            Me.XrLabel_postalcode_header.StylePriority.UseForeColor = False
            Me.XrLabel_postalcode_header.StylePriority.UseTextAlignment = False
            Me.XrLabel_postalcode_header.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            '
            'GroupHeader2
            '
            Me.GroupHeader2.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel_city_header})
            Me.GroupHeader2.GroupFields.AddRange(New DevExpress.XtraReports.UI.GroupField() {New DevExpress.XtraReports.UI.GroupField("city", DevExpress.XtraReports.UI.XRColumnSortOrder.Ascending)})
            Me.GroupHeader2.Height = 15
            Me.GroupHeader2.Level = 1
            Me.GroupHeader2.Name = "GroupHeader2"
            '
            'XrLabel_city_header
            '
            Me.XrLabel_city_header.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel_city_header.ForeColor = System.Drawing.Color.Maroon
            Me.XrLabel_city_header.Location = New System.Drawing.Point(1, 0)
            Me.XrLabel_city_header.Name = "XrLabel_city_header"
            Me.XrLabel_city_header.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_city_header.Size = New System.Drawing.Size(333, 15)
            Me.XrLabel_city_header.StylePriority.UseFont = False
            Me.XrLabel_city_header.StylePriority.UseForeColor = False
            Me.XrLabel_city_header.StylePriority.UseTextAlignment = False
            Me.XrLabel_city_header.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            '
            'GroupFooter1
            '
            Me.GroupFooter1.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel_Group1_Clients, Me.XrLabel_postalcode_footer, Me.XrLabel_Group1_fair_share})
            Me.GroupFooter1.Height = 15
            Me.GroupFooter1.Name = "GroupFooter1"
            '
            'XrLabel_Group1_Clients
            '
            Me.XrLabel_Group1_Clients.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.XrLabel_Group1_Clients.Location = New System.Drawing.Point(725, 0)
            Me.XrLabel_Group1_Clients.Name = "XrLabel_Group1_Clients"
            Me.XrLabel_Group1_Clients.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_Group1_Clients.Size = New System.Drawing.Size(50, 15)
            Me.XrLabel_Group1_Clients.StylePriority.UseFont = False
            Me.XrLabel_Group1_Clients.StylePriority.UseTextAlignment = False
            XrSummary1.FormatString = "{0:d}"
            XrSummary1.Func = DevExpress.XtraReports.UI.SummaryFunc.Count
            XrSummary1.Running = DevExpress.XtraReports.UI.SummaryRunning.Group
            Me.XrLabel_Group1_Clients.Summary = XrSummary1
            Me.XrLabel_Group1_Clients.Text = "0"
            Me.XrLabel_Group1_Clients.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel_postalcode_footer
            '
            Me.XrLabel_postalcode_footer.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.XrLabel_postalcode_footer.Location = New System.Drawing.Point(67, 0)
            Me.XrLabel_postalcode_footer.Name = "XrLabel_postalcode_footer"
            Me.XrLabel_postalcode_footer.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_postalcode_footer.Size = New System.Drawing.Size(333, 15)
            Me.XrLabel_postalcode_footer.StylePriority.UseFont = False
            Me.XrLabel_postalcode_footer.StylePriority.UseTextAlignment = False
            Me.XrLabel_postalcode_footer.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            '
            'XrLabel_Group1_fair_share
            '
            Me.XrLabel_Group1_fair_share.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.XrLabel_Group1_fair_share.Location = New System.Drawing.Point(592, 0)
            Me.XrLabel_Group1_fair_share.Name = "XrLabel_Group1_fair_share"
            Me.XrLabel_Group1_fair_share.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_Group1_fair_share.Size = New System.Drawing.Size(100, 15)
            Me.XrLabel_Group1_fair_share.StylePriority.UseFont = False
            Me.XrLabel_Group1_fair_share.StylePriority.UseTextAlignment = False
            XrSummary2.FormatString = "{0:c}"
            XrSummary2.Running = DevExpress.XtraReports.UI.SummaryRunning.Group
            Me.XrLabel_Group1_fair_share.Summary = XrSummary2
            Me.XrLabel_Group1_fair_share.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'GroupFooter2
            '
            Me.GroupFooter2.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel_city_footer, Me.XrLabel_Group2_fair_share})
            Me.GroupFooter2.Height = 32
            Me.GroupFooter2.Level = 1
            Me.GroupFooter2.Name = "GroupFooter2"
            '
            'XrLabel_city_footer
            '
            Me.XrLabel_city_footer.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel_city_footer.Location = New System.Drawing.Point(0, 0)
            Me.XrLabel_city_footer.Name = "XrLabel_city_footer"
            Me.XrLabel_city_footer.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_city_footer.Size = New System.Drawing.Size(333, 15)
            Me.XrLabel_city_footer.StylePriority.UseFont = False
            Me.XrLabel_city_footer.StylePriority.UseTextAlignment = False
            Me.XrLabel_city_footer.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            '
            'XrLabel_Group2_fair_share
            '
            Me.XrLabel_Group2_fair_share.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.XrLabel_Group2_fair_share.Location = New System.Drawing.Point(592, 0)
            Me.XrLabel_Group2_fair_share.Name = "XrLabel_Group2_fair_share"
            Me.XrLabel_Group2_fair_share.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_Group2_fair_share.Size = New System.Drawing.Size(100, 15)
            Me.XrLabel_Group2_fair_share.StylePriority.UseFont = False
            Me.XrLabel_Group2_fair_share.StylePriority.UseTextAlignment = False
            XrSummary3.FormatString = "{0:c}"
            XrSummary3.Running = DevExpress.XtraReports.UI.SummaryRunning.Group
            Me.XrLabel_Group2_fair_share.Summary = XrSummary3
            Me.XrLabel_Group2_fair_share.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'ReportFooter
            '
            Me.ReportFooter.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel8, Me.XrLabel_Report_total_fair_share})
            Me.ReportFooter.Height = 15
            Me.ReportFooter.Name = "ReportFooter"
            '
            'XrLabel8
            '
            Me.XrLabel8.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel8.Location = New System.Drawing.Point(0, 0)
            Me.XrLabel8.Name = "XrLabel8"
            Me.XrLabel8.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel8.Size = New System.Drawing.Size(333, 15)
            Me.XrLabel8.StylePriority.UseFont = False
            Me.XrLabel8.StylePriority.UseTextAlignment = False
            Me.XrLabel8.Text = "Report Totals"
            Me.XrLabel8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            '
            'XrLabel_Report_total_fair_share
            '
            Me.XrLabel_Report_total_fair_share.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel_Report_total_fair_share.Location = New System.Drawing.Point(595, 0)
            Me.XrLabel_Report_total_fair_share.Name = "XrLabel_Report_total_fair_share"
            Me.XrLabel_Report_total_fair_share.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_Report_total_fair_share.Size = New System.Drawing.Size(100, 15)
            Me.XrLabel_Report_total_fair_share.StylePriority.UseFont = False
            Me.XrLabel_Report_total_fair_share.StylePriority.UseTextAlignment = False
            XrSummary4.FormatString = "{0:c}"
            XrSummary4.Running = DevExpress.XtraReports.UI.SummaryRunning.Report
            Me.XrLabel_Report_total_fair_share.Summary = XrSummary4
            Me.XrLabel_Report_total_fair_share.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrPanel1
            '
            Me.XrPanel1.BackColor = System.Drawing.Color.Teal
            Me.XrPanel1.BorderColor = System.Drawing.Color.Teal
            Me.XrPanel1.CanGrow = False
            Me.XrPanel1.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel6, Me.XrLabel5, Me.XrLabel4, Me.XrLabel3})
            Me.XrPanel1.Location = New System.Drawing.Point(0, 125)
            Me.XrPanel1.Name = "XrPanel1"
            Me.XrPanel1.Size = New System.Drawing.Size(800, 17)
            '
            'XrLabel6
            '
            Me.XrLabel6.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel6.ForeColor = System.Drawing.Color.White
            Me.XrLabel6.Location = New System.Drawing.Point(67, 0)
            Me.XrLabel6.Name = "XrLabel6"
            Me.XrLabel6.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel6.Size = New System.Drawing.Size(33, 15)
            Me.XrLabel6.StylePriority.UseFont = False
            Me.XrLabel6.StylePriority.UseForeColor = False
            Me.XrLabel6.Text = "ZIP"
            '
            'XrLabel5
            '
            Me.XrLabel5.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel5.ForeColor = System.Drawing.Color.White
            Me.XrLabel5.Location = New System.Drawing.Point(0, 0)
            Me.XrLabel5.Name = "XrLabel5"
            Me.XrLabel5.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel5.Size = New System.Drawing.Size(42, 15)
            Me.XrLabel5.StylePriority.UseFont = False
            Me.XrLabel5.StylePriority.UseForeColor = False
            Me.XrLabel5.Text = "CITY"
            '
            'XrLabel4
            '
            Me.XrLabel4.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.XrLabel4.ForeColor = System.Drawing.Color.White
            Me.XrLabel4.Location = New System.Drawing.Point(700, 0)
            Me.XrLabel4.Name = "XrLabel4"
            Me.XrLabel4.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel4.Size = New System.Drawing.Size(75, 15)
            Me.XrLabel4.Text = "# CLIENTS"
            Me.XrLabel4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel3
            '
            Me.XrLabel3.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.XrLabel3.ForeColor = System.Drawing.Color.White
            Me.XrLabel3.Location = New System.Drawing.Point(608, 0)
            Me.XrLabel3.Name = "XrLabel3"
            Me.XrLabel3.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel3.Size = New System.Drawing.Size(87, 15)
            Me.XrLabel3.Text = "FAIRSHARE"
            Me.XrLabel3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'FairshareByZipcodeReport
            '
            Me.Bands.AddRange(New DevExpress.XtraReports.UI.Band() {Me.GroupHeader1, Me.GroupHeader2, Me.GroupFooter1, Me.GroupFooter2, Me.ReportFooter})
            Me.Version = "8.2"
            CType(Me, System.ComponentModel.ISupportInitialize).EndInit()

        End Sub
        Friend WithEvents XrLabel1_address As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_fair_share As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents GroupHeader1 As DevExpress.XtraReports.UI.GroupHeaderBand
        Friend WithEvents GroupHeader2 As DevExpress.XtraReports.UI.GroupHeaderBand
        Friend WithEvents GroupFooter1 As DevExpress.XtraReports.UI.GroupFooterBand
        Friend WithEvents XrLabel_Group1_fair_share As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents GroupFooter2 As DevExpress.XtraReports.UI.GroupFooterBand
        Friend WithEvents XrLabel_Group2_fair_share As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents ReportFooter As DevExpress.XtraReports.UI.ReportFooterBand
        Friend WithEvents XrLabel_Report_total_fair_share As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_postalcode_header As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_city_header As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_postalcode_footer As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_city_footer As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel8 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_Name As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_Group1_Clients As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrPanel1 As DevExpress.XtraReports.UI.XRPanel
        Friend WithEvents XrLabel6 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel5 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel4 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel3 As DevExpress.XtraReports.UI.XRLabel
    End Class
End Namespace
