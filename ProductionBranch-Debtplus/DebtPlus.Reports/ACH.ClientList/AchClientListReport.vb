#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}
#End Region

Option Compare Binary
Option Explicit On
Option Strict On

Imports DebtPlus.Reports.Template

Namespace ACH.ClientList

    Public Class AchClientListReport
        Inherits TemplateXtraReportClass

        ''' <summary>
        ''' Create an instance of our report
        ''' </summary>
        Public Sub New()
            MyBase.New()
            InitializeComponent()

            ReportFilter.IsEnabled = True

            AddHandler BeforePrint, AddressOf ACHBalancingReport_BeforePrint
            AddHandler XrLabel_client.BeforePrint, AddressOf XrLabel_Client_BeforePrint
        End Sub

        Public Overrides ReadOnly Property ReportTitle() As String
            Get
                Return "Clients on ACH"
            End Get
        End Property

#Region " Component Designer generated code "

        'Component overrides dispose to clean up the component list.
        Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
            If disposing Then
                If components IsNot Nothing Then
                    components.Dispose()
                End If
            End If
            MyBase.Dispose(disposing)
        End Sub

        'Required by the Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Designer
        'It can be modified using the Designer.
        'Do not modify it using the code editor.
        Friend WithEvents ReportFooter As DevExpress.XtraReports.UI.ReportFooterBand
        Friend WithEvents XrLabel_start_date As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_client_status As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_client_name As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_deposit_amount As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_account_type As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_aba As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_bank_number As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_error_date As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_prenote_date As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents GroupHeader1 As DevExpress.XtraReports.UI.GroupHeaderBand
        Friend WithEvents XrLabel_deposit_date As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrPanel1 As DevExpress.XtraReports.UI.XRPanel
        Friend WithEvents XrLabel4 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel10 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel9 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel8 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel7 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel6 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel5 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel3 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel2 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel1 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents GroupFooter1 As DevExpress.XtraReports.UI.GroupFooterBand
        Friend WithEvents XrLabel_Total As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_total_amount As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLine1 As DevExpress.XtraReports.UI.XRLine
        Friend WithEvents XrLabel_client As DevExpress.XtraReports.UI.XRLabel
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
            Dim XrSummary1 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary
            Dim XrSummary2 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary
            Me.ReportFooter = New DevExpress.XtraReports.UI.ReportFooterBand
            Me.XrLabel_client = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_prenote_date = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_error_date = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_bank_number = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_aba = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_account_type = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_deposit_amount = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_client_name = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_client_status = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_start_date = New DevExpress.XtraReports.UI.XRLabel
            Me.GroupHeader1 = New DevExpress.XtraReports.UI.GroupHeaderBand
            Me.XrLabel_deposit_date = New DevExpress.XtraReports.UI.XRLabel
            Me.XrPanel1 = New DevExpress.XtraReports.UI.XRPanel
            Me.XrLabel4 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel10 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel9 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel8 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel7 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel6 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel5 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel3 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel2 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel1 = New DevExpress.XtraReports.UI.XRLabel
            Me.GroupFooter1 = New DevExpress.XtraReports.UI.GroupFooterBand
            Me.XrLine1 = New DevExpress.XtraReports.UI.XRLine
            Me.XrLabel_total_amount = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_Total = New DevExpress.XtraReports.UI.XRLabel
            CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
            '
            'Detail
            '
            Me.Detail.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel_start_date, Me.XrLabel_client_status, Me.XrLabel_client_name, Me.XrLabel_deposit_amount, Me.XrLabel_account_type, Me.XrLabel_aba, Me.XrLabel_bank_number, Me.XrLabel_error_date, Me.XrLabel_prenote_date, Me.XrLabel_client})
            Me.Detail.HeightF = 15.0!
            '
            'PageHeader
            '
            Me.PageHeader.HeightF = 123.0!
            '
            'XrLabel_Title
            '
            Me.XrLabel_Title.StylePriority.UseFont = False
            '
            'XrPageInfo_PageNumber
            '
            Me.XrPageInfo_PageNumber.StylePriority.UseTextAlignment = False
            '
            'XRLabel_Agency_Name
            '
            Me.XRLabel_Agency_Name.StylePriority.UseTextAlignment = False
            '
            'XrLabel_Agency_Address3
            '
            Me.XrLabel_Agency_Address3.StylePriority.UseFont = False
            Me.XrLabel_Agency_Address3.StylePriority.UseTextAlignment = False
            '
            'XrLabel_Agency_Address1
            '
            Me.XrLabel_Agency_Address1.StylePriority.UseFont = False
            Me.XrLabel_Agency_Address1.StylePriority.UseTextAlignment = False
            '
            'XrLabel_Agency_Phone
            '
            Me.XrLabel_Agency_Phone.StylePriority.UseFont = False
            Me.XrLabel_Agency_Phone.StylePriority.UseTextAlignment = False
            '
            'XrLabel_Agency_Address2
            '
            Me.XrLabel_Agency_Address2.StylePriority.UseFont = False
            Me.XrLabel_Agency_Address2.StylePriority.UseTextAlignment = False
            '
            'ReportFooter
            '
            Me.ReportFooter.HeightF = 43.0!
            Me.ReportFooter.Name = "ReportFooter"
            Me.ReportFooter.StyleName = "XrControlStyle_Totals"
            '
            'XrLabel_client
            '
            Me.XrLabel_client.BorderColor = System.Drawing.Color.Transparent
            Me.XrLabel_client.Font = New System.Drawing.Font("Times New Roman", 9.75!)
            Me.XrLabel_client.ForeColor = System.Drawing.SystemColors.WindowText
            Me.XrLabel_client.LocationFloat = New DevExpress.Utils.PointFloat(0.9999911!, 0.0!)
            Me.XrLabel_client.Name = "XrLabel_client"
            Me.XrLabel_client.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
            Me.XrLabel_client.SizeF = New System.Drawing.SizeF(69.99997!, 14.99999!)
            Me.XrLabel_client.StylePriority.UseFont = False
            Me.XrLabel_client.StylePriority.UseForeColor = False
            Me.XrLabel_client.StylePriority.UsePadding = False
            Me.XrLabel_client.StylePriority.UseTextAlignment = False
            Me.XrLabel_client.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel_prenote_date
            '
            Me.XrLabel_prenote_date.BorderColor = System.Drawing.Color.Transparent
            Me.XrLabel_prenote_date.CanGrow = False
            Me.XrLabel_prenote_date.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "ach_prenote_date", "{0:d}")})
            Me.XrLabel_prenote_date.Font = New System.Drawing.Font("Times New Roman", 9.75!)
            Me.XrLabel_prenote_date.ForeColor = System.Drawing.SystemColors.WindowText
            Me.XrLabel_prenote_date.LocationFloat = New DevExpress.Utils.PointFloat(658.9999!, 0.0!)
            Me.XrLabel_prenote_date.Name = "XrLabel_prenote_date"
            Me.XrLabel_prenote_date.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
            Me.XrLabel_prenote_date.SizeF = New System.Drawing.SizeF(70.0!, 15.0!)
            Me.XrLabel_prenote_date.StylePriority.UseFont = False
            Me.XrLabel_prenote_date.StylePriority.UseForeColor = False
            Me.XrLabel_prenote_date.StylePriority.UsePadding = False
            Me.XrLabel_prenote_date.StylePriority.UseTextAlignment = False
            Me.XrLabel_prenote_date.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel_error_date
            '
            Me.XrLabel_error_date.BorderColor = System.Drawing.Color.Transparent
            Me.XrLabel_error_date.CanGrow = False
            Me.XrLabel_error_date.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "ach_error_date", "{0:d}")})
            Me.XrLabel_error_date.Font = New System.Drawing.Font("Times New Roman", 9.75!)
            Me.XrLabel_error_date.ForeColor = System.Drawing.SystemColors.WindowText
            Me.XrLabel_error_date.LocationFloat = New DevExpress.Utils.PointFloat(583.9999!, 0.0!)
            Me.XrLabel_error_date.Name = "XrLabel_error_date"
            Me.XrLabel_error_date.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
            Me.XrLabel_error_date.SizeF = New System.Drawing.SizeF(67.0!, 15.0!)
            Me.XrLabel_error_date.StylePriority.UseFont = False
            Me.XrLabel_error_date.StylePriority.UseForeColor = False
            Me.XrLabel_error_date.StylePriority.UsePadding = False
            Me.XrLabel_error_date.StylePriority.UseTextAlignment = False
            Me.XrLabel_error_date.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel_bank_number
            '
            Me.XrLabel_bank_number.BorderColor = System.Drawing.Color.Transparent
            Me.XrLabel_bank_number.CanGrow = False
            Me.XrLabel_bank_number.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "ach_bank_number")})
            Me.XrLabel_bank_number.Font = New System.Drawing.Font("Times New Roman", 9.75!)
            Me.XrLabel_bank_number.ForeColor = System.Drawing.SystemColors.WindowText
            Me.XrLabel_bank_number.LocationFloat = New DevExpress.Utils.PointFloat(437.0!, 0.0!)
            Me.XrLabel_bank_number.Name = "XrLabel_bank_number"
            Me.XrLabel_bank_number.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
            Me.XrLabel_bank_number.SizeF = New System.Drawing.SizeF(147.0!, 15.0!)
            Me.XrLabel_bank_number.StylePriority.UseFont = False
            Me.XrLabel_bank_number.StylePriority.UseForeColor = False
            Me.XrLabel_bank_number.StylePriority.UsePadding = False
            Me.XrLabel_bank_number.StylePriority.UseTextAlignment = False
            Me.XrLabel_bank_number.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            '
            'XrLabel_aba
            '
            Me.XrLabel_aba.BorderColor = System.Drawing.Color.Transparent
            Me.XrLabel_aba.CanGrow = False
            Me.XrLabel_aba.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "ach_routing_number")})
            Me.XrLabel_aba.Font = New System.Drawing.Font("Times New Roman", 9.75!)
            Me.XrLabel_aba.ForeColor = System.Drawing.SystemColors.WindowText
            Me.XrLabel_aba.LocationFloat = New DevExpress.Utils.PointFloat(361.9999!, 0.0!)
            Me.XrLabel_aba.Name = "XrLabel_aba"
            Me.XrLabel_aba.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
            Me.XrLabel_aba.SizeF = New System.Drawing.SizeF(75.0!, 15.0!)
            Me.XrLabel_aba.StylePriority.UseFont = False
            Me.XrLabel_aba.StylePriority.UseForeColor = False
            Me.XrLabel_aba.StylePriority.UsePadding = False
            Me.XrLabel_aba.StylePriority.UseTextAlignment = False
            Me.XrLabel_aba.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            Me.XrLabel_aba.WordWrap = False
            '
            'XrLabel_account_type
            '
            Me.XrLabel_account_type.BorderColor = System.Drawing.Color.Transparent
            Me.XrLabel_account_type.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "ach_account")})
            Me.XrLabel_account_type.Font = New System.Drawing.Font("Times New Roman", 9.75!)
            Me.XrLabel_account_type.ForeColor = System.Drawing.SystemColors.WindowText
            Me.XrLabel_account_type.LocationFloat = New DevExpress.Utils.PointFloat(344.9999!, 0.0!)
            Me.XrLabel_account_type.Name = "XrLabel_account_type"
            Me.XrLabel_account_type.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
            Me.XrLabel_account_type.SizeF = New System.Drawing.SizeF(14.0!, 15.0!)
            Me.XrLabel_account_type.StylePriority.UseFont = False
            Me.XrLabel_account_type.StylePriority.UseForeColor = False
            Me.XrLabel_account_type.StylePriority.UsePadding = False
            Me.XrLabel_account_type.StylePriority.UseTextAlignment = False
            Me.XrLabel_account_type.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
            '
            'XrLabel_deposit_amount
            '
            Me.XrLabel_deposit_amount.BorderColor = System.Drawing.Color.Transparent
            Me.XrLabel_deposit_amount.CanGrow = False
            Me.XrLabel_deposit_amount.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "deposit_amount", "{0:c}")})
            Me.XrLabel_deposit_amount.Font = New System.Drawing.Font("Times New Roman", 9.75!)
            Me.XrLabel_deposit_amount.ForeColor = System.Drawing.SystemColors.WindowText
            Me.XrLabel_deposit_amount.LocationFloat = New DevExpress.Utils.PointFloat(274.9999!, 0.0!)
            Me.XrLabel_deposit_amount.Name = "XrLabel_deposit_amount"
            Me.XrLabel_deposit_amount.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
            Me.XrLabel_deposit_amount.SizeF = New System.Drawing.SizeF(67.0!, 15.0!)
            Me.XrLabel_deposit_amount.StylePriority.UseFont = False
            Me.XrLabel_deposit_amount.StylePriority.UseForeColor = False
            Me.XrLabel_deposit_amount.StylePriority.UsePadding = False
            Me.XrLabel_deposit_amount.StylePriority.UseTextAlignment = False
            Me.XrLabel_deposit_amount.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel_client_name
            '
            Me.XrLabel_client_name.BorderColor = System.Drawing.Color.Transparent
            Me.XrLabel_client_name.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "name")})
            Me.XrLabel_client_name.Font = New System.Drawing.Font("Times New Roman", 9.75!)
            Me.XrLabel_client_name.ForeColor = System.Drawing.SystemColors.WindowText
            Me.XrLabel_client_name.LocationFloat = New DevExpress.Utils.PointFloat(102.9999!, 0.0!)
            Me.XrLabel_client_name.Name = "XrLabel_client_name"
            Me.XrLabel_client_name.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 0, 0, 0, 100.0!)
            Me.XrLabel_client_name.SizeF = New System.Drawing.SizeF(165.0!, 15.0!)
            Me.XrLabel_client_name.StylePriority.UseFont = False
            Me.XrLabel_client_name.StylePriority.UseForeColor = False
            Me.XrLabel_client_name.StylePriority.UsePadding = False
            Me.XrLabel_client_name.StylePriority.UseTextAlignment = False
            Me.XrLabel_client_name.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            '
            'XrLabel_client_status
            '
            Me.XrLabel_client_status.BorderColor = System.Drawing.Color.Transparent
            Me.XrLabel_client_status.CanGrow = False
            Me.XrLabel_client_status.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "active_status")})
            Me.XrLabel_client_status.Font = New System.Drawing.Font("Times New Roman", 9.75!)
            Me.XrLabel_client_status.ForeColor = System.Drawing.SystemColors.WindowText
            Me.XrLabel_client_status.LocationFloat = New DevExpress.Utils.PointFloat(74.49995!, 0.0!)
            Me.XrLabel_client_status.Name = "XrLabel_client_status"
            Me.XrLabel_client_status.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
            Me.XrLabel_client_status.SizeF = New System.Drawing.SizeF(25.0!, 15.0!)
            Me.XrLabel_client_status.StylePriority.UseFont = False
            Me.XrLabel_client_status.StylePriority.UseForeColor = False
            Me.XrLabel_client_status.StylePriority.UsePadding = False
            Me.XrLabel_client_status.StylePriority.UseTextAlignment = False
            Me.XrLabel_client_status.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
            '
            'XrLabel_start_date
            '
            Me.XrLabel_start_date.BorderColor = System.Drawing.Color.Transparent
            Me.XrLabel_start_date.CanGrow = False
            Me.XrLabel_start_date.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "ach_start_date", "{0:d}")})
            Me.XrLabel_start_date.Font = New System.Drawing.Font("Times New Roman", 9.75!)
            Me.XrLabel_start_date.ForeColor = System.Drawing.SystemColors.WindowText
            Me.XrLabel_start_date.LocationFloat = New DevExpress.Utils.PointFloat(733.9999!, 0.0!)
            Me.XrLabel_start_date.Name = "XrLabel_start_date"
            Me.XrLabel_start_date.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
            Me.XrLabel_start_date.SizeF = New System.Drawing.SizeF(66.0!, 15.0!)
            Me.XrLabel_start_date.StylePriority.UseFont = False
            Me.XrLabel_start_date.StylePriority.UseForeColor = False
            Me.XrLabel_start_date.StylePriority.UsePadding = False
            Me.XrLabel_start_date.StylePriority.UseTextAlignment = False
            Me.XrLabel_start_date.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'GroupHeader1
            '
            Me.GroupHeader1.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel_deposit_date, Me.XrPanel1})
            Me.GroupHeader1.GroupFields.AddRange(New DevExpress.XtraReports.UI.GroupField() {New DevExpress.XtraReports.UI.GroupField("deposit_date", DevExpress.XtraReports.UI.XRColumnSortOrder.Ascending)})
            Me.GroupHeader1.HeightF = 76.0!
            Me.GroupHeader1.Name = "GroupHeader1"
            Me.GroupHeader1.RepeatEveryPage = True
            Me.GroupHeader1.StyleName = "XrControlStyle_GroupHeader"
            '
            'XrLabel_deposit_date
            '
            Me.XrLabel_deposit_date.CanGrow = False
            Me.XrLabel_deposit_date.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "deposit_date", "Deposits scheduled for active clients on {0:d}")})
            Me.XrLabel_deposit_date.Font = New System.Drawing.Font("Times New Roman", 16.0!, System.Drawing.FontStyle.Bold)
            Me.XrLabel_deposit_date.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 8.0!)
            Me.XrLabel_deposit_date.Name = "XrLabel_deposit_date"
            Me.XrLabel_deposit_date.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_deposit_date.SizeF = New System.Drawing.SizeF(800.0!, 33.0!)
            Me.XrLabel_deposit_date.StylePriority.UseFont = False
            Me.XrLabel_deposit_date.StylePriority.UseTextAlignment = False
            Me.XrLabel_deposit_date.Text = "Deposits scheduled for active clients on [deposit_date]"
            Me.XrLabel_deposit_date.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            '
            'XrPanel1
            '
            Me.XrPanel1.BackColor = System.Drawing.Color.Teal
            Me.XrPanel1.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel4, Me.XrLabel10, Me.XrLabel9, Me.XrLabel8, Me.XrLabel7, Me.XrLabel6, Me.XrLabel5, Me.XrLabel3, Me.XrLabel2, Me.XrLabel1})
            Me.XrPanel1.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 50.0!)
            Me.XrPanel1.Name = "XrPanel1"
            Me.XrPanel1.SizeF = New System.Drawing.SizeF(800.0!, 18.0!)
            Me.XrPanel1.StyleName = "XrControlStyle_HeaderPannel"
            '
            'XrLabel4
            '
            Me.XrLabel4.BorderColor = System.Drawing.Color.Transparent
            Me.XrLabel4.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel4.ForeColor = System.Drawing.Color.White
            Me.XrLabel4.LocationFloat = New DevExpress.Utils.PointFloat(437.0!, 1.0!)
            Me.XrLabel4.Name = "XrLabel4"
            Me.XrLabel4.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
            Me.XrLabel4.SizeF = New System.Drawing.SizeF(75.0!, 15.0!)
            Me.XrLabel4.StylePriority.UsePadding = False
            Me.XrLabel4.StylePriority.UseTextAlignment = False
            Me.XrLabel4.Text = "ACCOUNT"
            Me.XrLabel4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            '
            'XrLabel10
            '
            Me.XrLabel10.BorderColor = System.Drawing.Color.Transparent
            Me.XrLabel10.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel10.ForeColor = System.Drawing.Color.White
            Me.XrLabel10.LocationFloat = New DevExpress.Utils.PointFloat(266.9999!, 1.0!)
            Me.XrLabel10.Name = "XrLabel10"
            Me.XrLabel10.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
            Me.XrLabel10.SizeF = New System.Drawing.SizeF(75.0!, 15.0!)
            Me.XrLabel10.StylePriority.UsePadding = False
            Me.XrLabel10.StylePriority.UseTextAlignment = False
            Me.XrLabel10.Text = "AMOUNT"
            Me.XrLabel10.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel9
            '
            Me.XrLabel9.BorderColor = System.Drawing.Color.Transparent
            Me.XrLabel9.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel9.ForeColor = System.Drawing.Color.White
            Me.XrLabel9.LocationFloat = New DevExpress.Utils.PointFloat(749.9999!, 1.0!)
            Me.XrLabel9.Name = "XrLabel9"
            Me.XrLabel9.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
            Me.XrLabel9.SizeF = New System.Drawing.SizeF(50.0!, 15.0!)
            Me.XrLabel9.StylePriority.UsePadding = False
            Me.XrLabel9.StylePriority.UseTextAlignment = False
            Me.XrLabel9.Text = "START"
            Me.XrLabel9.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel8
            '
            Me.XrLabel8.BorderColor = System.Drawing.Color.Transparent
            Me.XrLabel8.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel8.ForeColor = System.Drawing.Color.White
            Me.XrLabel8.LocationFloat = New DevExpress.Utils.PointFloat(658.9999!, 1.0!)
            Me.XrLabel8.Name = "XrLabel8"
            Me.XrLabel8.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
            Me.XrLabel8.SizeF = New System.Drawing.SizeF(70.0!, 15.0!)
            Me.XrLabel8.StylePriority.UsePadding = False
            Me.XrLabel8.StylePriority.UseTextAlignment = False
            Me.XrLabel8.Text = "PRENOTE"
            Me.XrLabel8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel7
            '
            Me.XrLabel7.BorderColor = System.Drawing.Color.Transparent
            Me.XrLabel7.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel7.ForeColor = System.Drawing.Color.White
            Me.XrLabel7.LocationFloat = New DevExpress.Utils.PointFloat(583.9999!, 1.0!)
            Me.XrLabel7.Name = "XrLabel7"
            Me.XrLabel7.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
            Me.XrLabel7.SizeF = New System.Drawing.SizeF(67.0!, 15.0!)
            Me.XrLabel7.StylePriority.UsePadding = False
            Me.XrLabel7.StylePriority.UseTextAlignment = False
            Me.XrLabel7.Text = "ERROR"
            Me.XrLabel7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel6
            '
            Me.XrLabel6.BorderColor = System.Drawing.Color.Transparent
            Me.XrLabel6.CanGrow = False
            Me.XrLabel6.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel6.ForeColor = System.Drawing.Color.White
            Me.XrLabel6.LocationFloat = New DevExpress.Utils.PointFloat(361.9999!, 1.0!)
            Me.XrLabel6.Name = "XrLabel6"
            Me.XrLabel6.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
            Me.XrLabel6.SizeF = New System.Drawing.SizeF(75.0!, 15.0!)
            Me.XrLabel6.StylePriority.UsePadding = False
            Me.XrLabel6.StylePriority.UseTextAlignment = False
            Me.XrLabel6.Text = "ROUTING"
            Me.XrLabel6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            Me.XrLabel6.WordWrap = False
            '
            'XrLabel5
            '
            Me.XrLabel5.BorderColor = System.Drawing.Color.Transparent
            Me.XrLabel5.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel5.ForeColor = System.Drawing.Color.White
            Me.XrLabel5.LocationFloat = New DevExpress.Utils.PointFloat(74.49995!, 1.0!)
            Me.XrLabel5.Name = "XrLabel5"
            Me.XrLabel5.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
            Me.XrLabel5.SizeF = New System.Drawing.SizeF(25.0!, 15.0!)
            Me.XrLabel5.StylePriority.UsePadding = False
            Me.XrLabel5.StylePriority.UseTextAlignment = False
            Me.XrLabel5.Text = "STS"
            Me.XrLabel5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
            '
            'XrLabel3
            '
            Me.XrLabel3.BorderColor = System.Drawing.Color.Transparent
            Me.XrLabel3.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel3.ForeColor = System.Drawing.Color.White
            Me.XrLabel3.LocationFloat = New DevExpress.Utils.PointFloat(344.9999!, 1.0!)
            Me.XrLabel3.Name = "XrLabel3"
            Me.XrLabel3.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel3.SizeF = New System.Drawing.SizeF(14.0!, 15.0!)
            Me.XrLabel3.StylePriority.UseTextAlignment = False
            Me.XrLabel3.Text = "T"
            Me.XrLabel3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
            '
            'XrLabel2
            '
            Me.XrLabel2.BorderColor = System.Drawing.Color.Transparent
            Me.XrLabel2.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel2.ForeColor = System.Drawing.Color.White
            Me.XrLabel2.LocationFloat = New DevExpress.Utils.PointFloat(102.9999!, 1.0!)
            Me.XrLabel2.Name = "XrLabel2"
            Me.XrLabel2.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
            Me.XrLabel2.SizeF = New System.Drawing.SizeF(149.0!, 15.0!)
            Me.XrLabel2.StylePriority.UsePadding = False
            Me.XrLabel2.StylePriority.UseTextAlignment = False
            Me.XrLabel2.Text = "NAME"
            Me.XrLabel2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            '
            'XrLabel1
            '
            Me.XrLabel1.BorderColor = System.Drawing.Color.Transparent
            Me.XrLabel1.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel1.ForeColor = System.Drawing.Color.White
            Me.XrLabel1.LocationFloat = New DevExpress.Utils.PointFloat(15.99996!, 1.0!)
            Me.XrLabel1.Name = "XrLabel1"
            Me.XrLabel1.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
            Me.XrLabel1.SizeF = New System.Drawing.SizeF(55.0!, 15.0!)
            Me.XrLabel1.StylePriority.UsePadding = False
            Me.XrLabel1.StylePriority.UseTextAlignment = False
            Me.XrLabel1.Text = "Client"
            Me.XrLabel1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'GroupFooter1
            '
            Me.GroupFooter1.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLine1, Me.XrLabel_total_amount, Me.XrLabel_Total})
            Me.GroupFooter1.HeightF = 59.0!
            Me.GroupFooter1.Name = "GroupFooter1"
            Me.GroupFooter1.PageBreak = DevExpress.XtraReports.UI.PageBreak.AfterBand
            Me.GroupFooter1.StyleName = "XrControlStyle_Totals"
            '
            'XrLine1
            '
            Me.XrLine1.ForeColor = System.Drawing.Color.Teal
            Me.XrLine1.LineWidth = 2
            Me.XrLine1.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 8.0!)
            Me.XrLine1.Name = "XrLine1"
            Me.XrLine1.SizeF = New System.Drawing.SizeF(800.0!, 9.0!)
            Me.XrLine1.StylePriority.UseForeColor = False
            '
            'XrLabel_total_amount
            '
            Me.XrLabel_total_amount.BorderColor = System.Drawing.Color.Transparent
            Me.XrLabel_total_amount.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "deposit_amount")})
            Me.XrLabel_total_amount.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel_total_amount.ForeColor = System.Drawing.Color.Teal
            Me.XrLabel_total_amount.LocationFloat = New DevExpress.Utils.PointFloat(233.0!, 25.0!)
            Me.XrLabel_total_amount.Name = "XrLabel_total_amount"
            Me.XrLabel_total_amount.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
            Me.XrLabel_total_amount.SizeF = New System.Drawing.SizeF(100.0!, 15.0!)
            Me.XrLabel_total_amount.StylePriority.UseFont = False
            Me.XrLabel_total_amount.StylePriority.UseForeColor = False
            Me.XrLabel_total_amount.StylePriority.UsePadding = False
            Me.XrLabel_total_amount.StylePriority.UseTextAlignment = False
            XrSummary1.FormatString = "{0:c}"
            XrSummary1.Running = DevExpress.XtraReports.UI.SummaryRunning.Group
            Me.XrLabel_total_amount.Summary = XrSummary1
            Me.XrLabel_total_amount.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel_Total
            '
            Me.XrLabel_Total.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "client")})
            Me.XrLabel_Total.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel_Total.ForeColor = System.Drawing.Color.Teal
            Me.XrLabel_Total.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 25.0!)
            Me.XrLabel_Total.Name = "XrLabel_Total"
            Me.XrLabel_Total.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
            Me.XrLabel_Total.SizeF = New System.Drawing.SizeF(225.0!, 15.0!)
            Me.XrLabel_Total.StylePriority.UseFont = False
            Me.XrLabel_Total.StylePriority.UseForeColor = False
            Me.XrLabel_Total.StylePriority.UsePadding = False
            XrSummary2.FormatString = "Totals for {0:d} client(s)"
            XrSummary2.Func = DevExpress.XtraReports.UI.SummaryFunc.DCount
            XrSummary2.Running = DevExpress.XtraReports.UI.SummaryRunning.Group
            Me.XrLabel_Total.Summary = XrSummary2
            '
            'ACHBalancingReport
            '
            Me.Bands.AddRange(New DevExpress.XtraReports.UI.Band() {Me.Detail, Me.PageHeader, Me.PageFooter, Me.ReportFooter, Me.GroupHeader1, Me.GroupFooter1, Me.TopMarginBand1, Me.BottomMarginBand1})
            Me.StyleSheet.AddRange(New DevExpress.XtraReports.UI.XRControlStyle() {Me.XrControlStyle_Header, Me.XrControlStyle_Totals, Me.XrControlStyle_HeaderPannel, Me.XrControlStyle_GroupHeader})
            Me.Version = "10.1"
            Me.Controls.SetChildIndex(Me.BottomMarginBand1, 0)
            Me.Controls.SetChildIndex(Me.TopMarginBand1, 0)
            Me.Controls.SetChildIndex(Me.GroupFooter1, 0)
            Me.Controls.SetChildIndex(Me.GroupHeader1, 0)
            Me.Controls.SetChildIndex(Me.ReportFooter, 0)
            Me.Controls.SetChildIndex(Me.PageFooter, 0)
            Me.Controls.SetChildIndex(Me.PageHeader, 0)
            Me.Controls.SetChildIndex(Me.Detail, 0)
            CType(Me, System.ComponentModel.ISupportInitialize).EndInit()

        End Sub

#End Region

        Dim ds As New System.Data.DataSet("ds")

        Private Sub ACHBalancingReport_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs)
            Const TableName As String = "view_clients_on_ach"
            Dim tbl As System.Data.DataTable = ds.Tables(TableName)
            If tbl Is Nothing Then
                Using cmd As New SqlClient.SqlCommand()
                    With cmd
                        .Connection = New System.Data.SqlClient.SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
                        .CommandText = "SELECT deposit_date, ach_active, ach_routing_number, active_status, name, deposit_amount, ach_bank_number, client, ach_account, ach_prenote_date, ach_start_date, ach_error_date FROM view_clients_on_ach WHERE active_status IN ('A','AR') ORDER BY deposit_date, client"
                        .CommandType = CommandType.Text
                        .CommandTimeout = 0
                    End With

                    Using da As New SqlClient.SqlDataAdapter(cmd)
                        da.Fill(ds, TableName)
                        tbl = ds.Tables(TableName)
                    End Using
                End Using
            End If

            DataSource = New System.Data.DataView(tbl, ReportFilter.ViewFilter, String.Empty, DataViewRowState.CurrentRows)
        End Sub

        Private Sub XrLabel_Client_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs)
            With CType(sender, DevExpress.XtraReports.UI.XRLabel)
                .Text = String.Format("{0:0000000}", GetCurrentColumnValue("client"))
            End With
        End Sub
    End Class
End Namespace
