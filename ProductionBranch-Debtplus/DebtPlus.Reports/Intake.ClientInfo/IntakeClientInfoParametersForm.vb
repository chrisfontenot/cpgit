#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}

#End Region

Option Compare Binary
Option Explicit On
Option Strict On

Imports DebtPlus.Reports.Template.Forms


Namespace Intake.ClientInfo
    Friend Class IntakeClientInfoParametersForm
        Inherits ReportParametersForm

        Public Sub New()
            MyBase.New()
            InitializeComponent()
            AddHandler Me.Load, AddressOf RequestParametersForm_Load
            AddHandler IDString.EditValueChanged, AddressOf IDString_EditValueChanged
        End Sub


        ''' <summary>
        ''' Method for selecting the client
        ''' </summary>
        Friend Property Parameter_Method() As Int32
            Get
                If Method_IntakeID.Checked Then
                    Return 0
                Else
                    Return 1
                End If
            End Get

            Set(ByVal Value As Int32)
                If Value = 0 Then
                    Method_IntakeID.Checked = True
                    Method_ClientID.Checked = False
                Else
                    Method_IntakeID.Checked = False
                    Method_ClientID.Checked = True
                End If
            End Set
        End Property


        ''' <summary>
        ''' Client ID value
        ''' </summary>
        Friend Property Parameter_ID() As String
            Get
                Return Convert.ToString(IDString.EditValue)
            End Get

            Set(ByVal Value As String)
                IDString.EditValue = Value
            End Set
        End Property

#Region " Windows Form Designer generated code "

        'Form overrides dispose to clean up the component list.
        Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
            If disposing Then
                If components IsNot Nothing Then
                    components.Dispose()
                End If
            End If
            MyBase.Dispose(disposing)
        End Sub

        'Required by the Windows Form Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Windows Form Designer
        'It can be modified using the Windows Form Designer.  
        'Do not modify it using the code editor.
        Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents Method_ClientID As DevExpress.XtraEditors.CheckEdit
        Friend WithEvents Method_IntakeID As DevExpress.XtraEditors.CheckEdit
        Friend WithEvents IDString As DevExpress.XtraEditors.TextEdit

        <System.Diagnostics.DebuggerStepThrough()>
        Private Sub InitializeComponent()
            Me.Method_ClientID = New DevExpress.XtraEditors.CheckEdit
            Me.Method_IntakeID = New DevExpress.XtraEditors.CheckEdit
            Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl
            Me.IDString = New DevExpress.XtraEditors.TextEdit

            CType(Me.Method_ClientID.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.Method_IntakeID.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.IDString.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.SuspendLayout()
            '
            'ButtonCancel
            '
            Me.ButtonCancel.Location = New System.Drawing.Point(248, 56)
            Me.ButtonCancel.Name = "ButtonCancel"
            Me.ButtonCancel.Size = New System.Drawing.Size(80, 28)
            Me.ButtonCancel.TabIndex = 4
            '
            'ButtonOK
            '
            Me.ButtonOK.Location = New System.Drawing.Point(248, 18)
            Me.ButtonOK.Name = "ButtonOK"
            Me.ButtonOK.Size = New System.Drawing.Size(80, 28)
            Me.ButtonOK.TabIndex = 3

            '
            'Method_ClientID
            '
            Me.Method_ClientID.Location = New System.Drawing.Point(56, 40)
            Me.Method_ClientID.Name = "Method_ClientID"
            '
            'Method_ClientID.Properties
            '
            Me.Method_ClientID.Properties.Caption = "Using Client ID"
            Me.Method_ClientID.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
            Me.Method_ClientID.Properties.RadioGroupIndex = 1
            Me.Method_ClientID.Size = New System.Drawing.Size(160, 19)
            Me.Method_ClientID.TabIndex = 5
            Me.Method_ClientID.TabStop = False
            '
            'Method_IntakeID
            '
            Me.Method_IntakeID.EditValue = True
            Me.Method_IntakeID.Location = New System.Drawing.Point(56, 64)
            Me.Method_IntakeID.Name = "Method_IntakeID"
            '
            'Method_IntakeID.Properties
            '
            Me.Method_IntakeID.Properties.Caption = "Using Temporary Intake ID"
            Me.Method_IntakeID.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
            Me.Method_IntakeID.Properties.RadioGroupIndex = 1
            Me.Method_IntakeID.Size = New System.Drawing.Size(160, 19)
            Me.Method_IntakeID.TabIndex = 6
            '
            'LabelControl1
            '
            Me.LabelControl1.Location = New System.Drawing.Point(56, 99)
            Me.LabelControl1.Name = "LabelControl1"
            Me.LabelControl1.Size = New System.Drawing.Size(56, 14)
            Me.LabelControl1.TabIndex = 7
            Me.LabelControl1.Text = "ID Value:"
            '
            'IDString
            '
            Me.IDString.EditValue = ""
            Me.IDString.Location = New System.Drawing.Point(112, 96)
            Me.IDString.Name = "IDString"
            '
            'IDString.Properties
            '
            Me.IDString.Properties.Appearance.Options.UseTextOptions = True
            Me.IDString.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.IDString.Properties.Mask.BeepOnError = True
            Me.IDString.Properties.Mask.EditMask = "f0"
            Me.IDString.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
            Me.IDString.Size = New System.Drawing.Size(96, 20)
            Me.IDString.TabIndex = 8
            Me.IDString.ToolTip = "Enter the value associated with the client"
            Me.IDString.ToolTipController = Me.ToolTipController1
            '
            'RequestParametersForm
            '
            Me.AutoScaleBaseSize = New System.Drawing.Size(5, 14)
            Me.ClientSize = New System.Drawing.Size(336, 163)
            Me.Controls.Add(Me.IDString)
            Me.Controls.Add(Me.LabelControl1)
            Me.Controls.Add(Me.Method_IntakeID)
            Me.Controls.Add(Me.Method_ClientID)
            Me.Name = "RequestParametersForm"
            Me.ToolTipController1.SetSuperTip(Me, Nothing)
            Me.Text = "Intake Client Report Parameters"
            Me.Controls.SetChildIndex(Me.ButtonOK, 0)
            Me.Controls.SetChildIndex(Me.ButtonCancel, 0)
            Me.Controls.SetChildIndex(Me.Method_ClientID, 0)
            Me.Controls.SetChildIndex(Me.Method_IntakeID, 0)
            Me.Controls.SetChildIndex(Me.LabelControl1, 0)
            Me.Controls.SetChildIndex(Me.IDString, 0)

            CType(Me.Method_ClientID.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.Method_IntakeID.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.IDString.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            Me.ResumeLayout(False)
        End Sub

#End Region

        Private Sub RequestParametersForm_Load(ByVal sender As Object, ByVal e As EventArgs)
            IDString_EditValueChanged(Nothing, EventArgs.Empty)
        End Sub

        Private Sub IDString_EditValueChanged(ByVal sender As Object, ByVal e As EventArgs)
            ButtonOK.Enabled = Convert.ToString(IDString.EditValue) <> String.Empty
        End Sub
    End Class
End Namespace
