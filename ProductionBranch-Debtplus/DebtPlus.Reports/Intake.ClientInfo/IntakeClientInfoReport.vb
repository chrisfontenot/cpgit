#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}

#End Region

Option Compare Binary
Option Explicit On
Option Strict On

Imports DebtPlus.Interfaces.Client
Imports DebtPlus.Reports.Template
Imports DebtPlus.Utils
Imports System.IO
Imports System.Reflection
Imports DevExpress.XtraReports.UI

Namespace Intake.ClientInfo
    Public Class IntakeClientInfoReport
        Inherits TemplateXtraReportClass
        Implements DebtPlus.Interfaces.Client.IClient

        Public Sub New()
            MyBase.New()
            Const ReportName As String = "DebtPlus.Reports.Intake.ClientInfo.repx"

            ' See if there is a report reference that we can use
            Dim Fname As String = Path.Combine(DebtPlus.Configuration.Config.ReportsDirectory, ReportName)
            Dim UseDefault As Boolean = True
            Try
                If (Not DebtPlus.Configuration.Config.LocalReportsOnly) AndAlso System.IO.File.Exists(Fname) Then
                    LoadLayout(Fname)
                    UseDefault = False
                End If
            Catch ex As Exception
            End Try

            ' Load the standard report definition if we need a new item
            If UseDefault Then
                Dim asm As Assembly = Assembly.GetExecutingAssembly                                           ' changed
                Using ios As Stream = asm.GetManifestResourceStream(String.Format("DebtPlus.Reports.{0}", ReportName)) ' changed
                    If ios IsNot Nothing Then LoadLayout(ios) '                                                                       changed
                End Using
            End If

            ' Set the script references as needed
            ScriptReferences = DebtPlus.Utils.AssemblyTools.GetAllSiblingAssemblies()

            ' Copy the script references to the subreports as well
            For Each Band As Band In Bands
                For Each ctl As XRControl In Band.Controls
                    Dim subReport As XRSubreport = TryCast(ctl, XRSubreport)
                    If subReport IsNot Nothing Then
                        Dim rpt As XtraReport = subReport.ReportSource
                        If rpt IsNot Nothing Then
                            rpt.ScriptReferences = ScriptReferences
                        End If
                    End If
                Next
            Next

            Parameters("ParameterMethod").Value = -1
        End Sub


        ''' <summary>
        '''     ID string associated with the client selection
        ''' </summary>
        Public Property Parameter_ID() As String
            Get
                Return CType(Parameters("ParameterClient").Value, String)
            End Get
            Set(ByVal Value As String)
                Parameters("ParameterID").Value = Value
                Parameter_Method = 0
            End Set
        End Property


        ''' <summary>
        '''     A simple property if we already have the client as a number
        ''' </summary>
        Public Property Parameter_IntakeClient() As Int32
            Get
                Return CType(Parameters("ParameterIntakeClient").Value, Int32)
            End Get

            Set(ByVal Value As Int32)
                Parameters("ParameterIntakeClient").Value = Value
                Parameter_Method = 1
            End Set
        End Property

        Public Property ClientID As Int32 Implements IClient.ClientId
            Get
                Return Parameter_Client
            End Get
            Set(value As Int32)
                Parameter_Client = value
            End Set
        End Property

        ''' <summary>
        '''     A simple property if we already have the client as a number
        ''' </summary>
        Public Property Parameter_Client() As Int32
            Get
                Return CType(Parameters("ParameterClient").Value, Int32)
            End Get

            Set(ByVal Value As Int32)
                Parameters("ParameterClient").Value = Value
                Parameter_Method = 2
            End Set
        End Property


        ''' <summary>
        '''     Method selecting the client id
        ''' </summary>
        Public Property Parameter_Method() As Int32
            Get
                Return CType(Parameters("ParameterMethod").Value, Int32)
            End Get
            Set(ByVal Value As Int32)
                Parameters("ParameterMethod").Value = Value
            End Set
        End Property


        ''' <summary>
        '''     Set a parameter value
        ''' </summary>
        Public Overrides Sub SetReportParameter(ByVal Parameter As String, ByVal Value As Object)
            Select Case Parameter
                Case "IntakeClient" : Parameter_IntakeClient = Convert.ToInt32(Value)
                Case "Client" : Parameter_Client = Convert.ToInt32(Value)
                Case "Method" : Parameter_Method = Convert.ToInt32(Value)
                Case "ID" : Parameter_ID = Convert.ToString(Value)
                Case Else : MyBase.SetReportParameter(Parameter, Value)
            End Select
        End Sub


        ''' <summary>
        '''     Title for the report
        ''' </summary>
        Public Overrides ReadOnly Property ReportTitle() As String
            Get
                Return "Intake Client"
            End Get
        End Property

        Public Overrides Function NeedParameters() As Boolean
            Return Parameter_Method < 0
        End Function


        ''' <summary>
        '''     Request the parameters from the user
        ''' </summary>
        Public Overrides Function RequestReportParameters() As DialogResult
            Dim answer As DialogResult = DialogResult.OK
            If NeedParameters() Then
                Using frm As New IntakeClientInfoParametersForm()
                    With frm
                        answer = .ShowDialog()
                        If .Parameter_Method = 1 Then
                            Parameter_IntakeClient = Convert.ToInt32(.Parameter_ID)
                        Else
                            Parameter_ID = .Parameter_ID
                        End If
                    End With
                End Using
            End If
            Return answer
        End Function
    End Class
End Namespace
