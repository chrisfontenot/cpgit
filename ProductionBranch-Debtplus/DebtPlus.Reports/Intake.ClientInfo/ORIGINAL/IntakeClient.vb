#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}
#End Region

Namespace Intake.ClientInfo
    Public Class IntakeClient

        Public Sub New()
            MyBase.New()
            InitializeComponent()
            AddHandler Me.BeforePrint, AddressOf IntakeClient_BeforePrint
        End Sub

        Private Sub IntakeClient_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs)
            Dim rpt As DevExpress.XtraReports.UI.XtraReport = CType(sender, DevExpress.XtraReports.UI.XtraReport)
            Dim MasterRpt As DevExpress.XtraReports.UI.XtraReport = CType(rpt.MasterReport, DevExpress.XtraReports.UI.XtraReport)
            Dim vue As System.Data.DataView = CType(MasterRpt.DataSource, System.Data.DataView)

            If vue.Count > 0 Then
                Dim drv As System.Data.DataRowView = vue(0)

                lbl_intake_id.Text = String.Format("{0}", drv("intake_id"))
                lbl_intake_client.Text = String.Format("{0:f0}", drv("intake_client"))
                lbl_status.Text = String.Format("{0}", drv("status"))
                lbl_bankruptcy.Text = String.Format("{0}", drv("bankruptcy"))
                lbl_people.Text = String.Format("{0:f0}", drv("people"))
                lbl_home_ph.Text = String.Format("{0}", drv("home_ph"))
                lbl_message_ph.Text = String.Format("{0}", drv("message_ph"))
                lbl_marital_status.Text = String.Format("{0}", drv("marital_status"))
                lbl_housing_type.Text = String.Format("{0}", drv("housing_type"))

                Dim sb As New System.Text.StringBuilder
                Dim AddressLine As String = DebtPlus.Utils.Nulls.DStr(drv("address1")).Trim
                If AddressLine <> String.Empty Then
                    sb.Append(Environment.NewLine)
                    sb.Append(AddressLine)
                End If

                AddressLine = DebtPlus.Utils.Nulls.DStr(drv("address2")).Trim
                If AddressLine <> String.Empty Then
                    sb.Append(Environment.NewLine)
                    sb.Append(AddressLine)
                End If

                AddressLine = DebtPlus.Utils.Nulls.DStr(drv("address3")).Trim
                If AddressLine <> String.Empty Then
                    sb.Append(Environment.NewLine)
                    sb.Append(AddressLine)
                End If

                If sb.Length > 0 Then sb.Remove(0, 2)
                lbl_home_address.Text = sb.ToString
            End If
        End Sub
    End Class
End Namespace
