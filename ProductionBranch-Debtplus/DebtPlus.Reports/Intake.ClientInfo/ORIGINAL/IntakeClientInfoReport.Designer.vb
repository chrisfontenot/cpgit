Namespace Intake.ClientInfo
    <Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
    Partial Class IntakeClientInfoReport
        Inherits DebtPlus.Reports.Template.TemplateXtraReportClass

        'XtraReport overrides dispose to clean up the component list.
        <System.Diagnostics.DebuggerNonUserCode()> _
        Protected Overrides Sub Dispose(ByVal disposing As Boolean)
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
            MyBase.Dispose(disposing)
        End Sub

        'Required by the Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Designer
        'It can be modified using the Designer.  
        'Do not modify it using the code editor.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
            Me.components = New System.ComponentModel.Container()
            Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(IntakeClientInfoReport))
            Me.XrLabel11 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel12 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel14 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel13 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel15 = New DevExpress.XtraReports.UI.XRLabel()
            Me.lbl_referred_by = New DevExpress.XtraReports.UI.XRLabel()
            Me.lbl_cause_fin_problem1 = New DevExpress.XtraReports.UI.XRLabel()
            Me.lbl_fed_tax_owed = New DevExpress.XtraReports.UI.XRLabel()
            Me.lbl_state_tax_owed = New DevExpress.XtraReports.UI.XRLabel()
            Me.lbl_local_tax_owed = New DevExpress.XtraReports.UI.XRLabel()
            Me.ParameterClient = New DevExpress.XtraReports.Parameters.Parameter()
            Me.ParameterMethod = New DevExpress.XtraReports.Parameters.Parameter()
            Me.ParameterID = New DevExpress.XtraReports.Parameters.Parameter()
            Me.ParameterIntakeClient = New DevExpress.XtraReports.Parameters.Parameter()
            Me.Subreport_Budget = New DevExpress.XtraReports.UI.XRSubreport()
            Me.IntakeBudget1 = New DebtPlus.Reports.Intake.ClientInfo.IntakeBudget(Me.components)
            Me.Subreport_BottomLine = New DevExpress.XtraReports.UI.XRSubreport()
            Me.BottomLine1 = New DebtPlus.Reports.Intake.ClientInfo.BottomLine(Me.components)
            Me.Subreport_unsecured = New DevExpress.XtraReports.UI.XRSubreport()
            Me.IntakeUnsecuredReport1 = New DebtPlus.Reports.Intake.ClientInfo.IntakeUnsecuredReport(Me.components)
            Me.Subreport_Secured = New DevExpress.XtraReports.UI.XRSubreport()
            Me.IntakeSecuredReport1 = New DebtPlus.Reports.Intake.ClientInfo.IntakeSecuredReport(Me.components)
            Me.Subreport_income = New DevExpress.XtraReports.UI.XRSubreport()
            Me.IntakeIncomeReport1 = New DebtPlus.Reports.Intake.ClientInfo.IntakeIncomeReport(Me.components)
            Me.Subreport_People = New DevExpress.XtraReports.UI.XRSubreport()
            Me.IntakePeopleReport2 = New DebtPlus.Reports.Intake.ClientInfo.IntakeApplicantReport(Me.components)
            Me.XrSubreport_ClientInfo = New DevExpress.XtraReports.UI.XRSubreport()
            Me.IntakeClient1 = New DebtPlus.Reports.Intake.ClientInfo.IntakeClient()
            CType(Me.IntakeBudget1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.BottomLine1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.IntakeUnsecuredReport1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.IntakeSecuredReport1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.IntakeIncomeReport1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.IntakePeopleReport2, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.IntakeClient1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
            '
            'XrLabel_Title
            '
            Me.XrLabel_Title.StylePriority.UseFont = False
            '
            'XrPageInfo_PageNumber
            '
            Me.XrPageInfo_PageNumber.StylePriority.UseTextAlignment = False
            '
            'XRLabel_Agency_Name
            '
            Me.XRLabel_Agency_Name.StylePriority.UseTextAlignment = False
            '
            'XrLabel_Agency_Address3
            '
            Me.XrLabel_Agency_Address3.StylePriority.UseFont = False
            Me.XrLabel_Agency_Address3.StylePriority.UseTextAlignment = False
            '
            'XrLabel_Agency_Address1
            '
            Me.XrLabel_Agency_Address1.StylePriority.UseFont = False
            Me.XrLabel_Agency_Address1.StylePriority.UseTextAlignment = False
            '
            'XrLabel_Agency_Phone
            '
            Me.XrLabel_Agency_Phone.StylePriority.UseFont = False
            Me.XrLabel_Agency_Phone.StylePriority.UseTextAlignment = False
            '
            'XrLabel_Agency_Address2
            '
            Me.XrLabel_Agency_Address2.StylePriority.UseFont = False
            Me.XrLabel_Agency_Address2.StylePriority.UseTextAlignment = False
            '
            'Detail
            '
            Me.Detail.Borders = CType((((DevExpress.XtraPrinting.BorderSide.Left Or DevExpress.XtraPrinting.BorderSide.Top) _
                Or DevExpress.XtraPrinting.BorderSide.Right) _
                Or DevExpress.XtraPrinting.BorderSide.Bottom), DevExpress.XtraPrinting.BorderSide)
            Me.Detail.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.Subreport_Budget, Me.Subreport_BottomLine, Me.Subreport_unsecured, Me.Subreport_Secured, Me.Subreport_income, Me.Subreport_People, Me.lbl_local_tax_owed, Me.lbl_state_tax_owed, Me.lbl_fed_tax_owed, Me.lbl_cause_fin_problem1, Me.lbl_referred_by, Me.XrLabel15, Me.XrLabel13, Me.XrLabel14, Me.XrLabel12, Me.XrLabel11, Me.XrSubreport_ClientInfo})
            Me.Detail.HeightF = 336.5416!
            '
            'XrLabel11
            '
            Me.XrLabel11.BorderWidth = 0
            Me.XrLabel11.CanGrow = False
            Me.XrLabel11.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel11.LocationFloat = New DevExpress.Utils.PointFloat(2.416674!, 50.33334!)
            Me.XrLabel11.Name = "XrLabel11"
            Me.XrLabel11.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel11.SizeF = New System.Drawing.SizeF(166.0!, 16.0!)
            Me.XrLabel11.Text = "Referral Source"
            '
            'XrLabel12
            '
            Me.XrLabel12.BorderWidth = 0
            Me.XrLabel12.CanGrow = False
            Me.XrLabel12.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel12.LocationFloat = New DevExpress.Utils.PointFloat(2.416674!, 66.33333!)
            Me.XrLabel12.Name = "XrLabel12"
            Me.XrLabel12.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel12.SizeF = New System.Drawing.SizeF(166.0!, 16.0!)
            Me.XrLabel12.Text = "Cause of Financial Problem"
            '
            'XrLabel14
            '
            Me.XrLabel14.BorderWidth = 0
            Me.XrLabel14.CanGrow = False
            Me.XrLabel14.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel14.LocationFloat = New DevExpress.Utils.PointFloat(2.416674!, 100.3333!)
            Me.XrLabel14.Multiline = True
            Me.XrLabel14.Name = "XrLabel14"
            Me.XrLabel14.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel14.SizeF = New System.Drawing.SizeF(275.0!, 16.0!)
            Me.XrLabel14.Text = "Federal Tax Owed and Months Delinquent" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
            '
            'XrLabel13
            '
            Me.XrLabel13.BorderWidth = 0
            Me.XrLabel13.CanGrow = False
            Me.XrLabel13.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel13.LocationFloat = New DevExpress.Utils.PointFloat(2.416674!, 116.3333!)
            Me.XrLabel13.Multiline = True
            Me.XrLabel13.Name = "XrLabel13"
            Me.XrLabel13.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel13.SizeF = New System.Drawing.SizeF(275.0!, 16.0!)
            Me.XrLabel13.Text = "State Tax Owed and Months Delinquent" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
            '
            'XrLabel15
            '
            Me.XrLabel15.BorderWidth = 0
            Me.XrLabel15.CanGrow = False
            Me.XrLabel15.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel15.LocationFloat = New DevExpress.Utils.PointFloat(2.416674!, 132.3333!)
            Me.XrLabel15.Multiline = True
            Me.XrLabel15.Name = "XrLabel15"
            Me.XrLabel15.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel15.SizeF = New System.Drawing.SizeF(275.0!, 16.0!)
            Me.XrLabel15.Text = "Local/City Tax Owed and Months Delinquent" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
            '
            'lbl_referred_by
            '
            Me.lbl_referred_by.BorderWidth = 0
            Me.lbl_referred_by.CanGrow = False
            Me.lbl_referred_by.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "referred_by")})
            Me.lbl_referred_by.LocationFloat = New DevExpress.Utils.PointFloat(200.0!, 50.33!)
            Me.lbl_referred_by.Name = "lbl_referred_by"
            Me.lbl_referred_by.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.lbl_referred_by.SizeF = New System.Drawing.SizeF(500.0!, 16.0!)
            '
            'lbl_cause_fin_problem1
            '
            Me.lbl_cause_fin_problem1.BorderWidth = 0
            Me.lbl_cause_fin_problem1.CanGrow = False
            Me.lbl_cause_fin_problem1.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "cause_fin_problem1")})
            Me.lbl_cause_fin_problem1.LocationFloat = New DevExpress.Utils.PointFloat(200.0!, 66.33!)
            Me.lbl_cause_fin_problem1.Name = "lbl_cause_fin_problem1"
            Me.lbl_cause_fin_problem1.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.lbl_cause_fin_problem1.SizeF = New System.Drawing.SizeF(500.0!, 16.0!)
            '
            'lbl_fed_tax_owed
            '
            Me.lbl_fed_tax_owed.BorderWidth = 0
            Me.lbl_fed_tax_owed.CanGrow = False
            Me.lbl_fed_tax_owed.LocationFloat = New DevExpress.Utils.PointFloat(285.4167!, 100.3333!)
            Me.lbl_fed_tax_owed.Name = "lbl_fed_tax_owed"
            Me.lbl_fed_tax_owed.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.lbl_fed_tax_owed.Scripts.OnBeforePrint = "lbl_fed_tax_owed_BeforePrint"
            Me.lbl_fed_tax_owed.SizeF = New System.Drawing.SizeF(233.0!, 16.0!)
            Me.lbl_fed_tax_owed.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'lbl_state_tax_owed
            '
            Me.lbl_state_tax_owed.BorderWidth = 0
            Me.lbl_state_tax_owed.CanGrow = False
            Me.lbl_state_tax_owed.LocationFloat = New DevExpress.Utils.PointFloat(285.4167!, 116.3333!)
            Me.lbl_state_tax_owed.Name = "lbl_state_tax_owed"
            Me.lbl_state_tax_owed.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.lbl_state_tax_owed.Scripts.OnBeforePrint = "lbl_state_tax_owed_BeforePrint"
            Me.lbl_state_tax_owed.SizeF = New System.Drawing.SizeF(233.0!, 16.0!)
            Me.lbl_state_tax_owed.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'lbl_local_tax_owed
            '
            Me.lbl_local_tax_owed.BorderWidth = 0
            Me.lbl_local_tax_owed.CanGrow = False
            Me.lbl_local_tax_owed.LocationFloat = New DevExpress.Utils.PointFloat(285.4167!, 133.3333!)
            Me.lbl_local_tax_owed.Name = "lbl_local_tax_owed"
            Me.lbl_local_tax_owed.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.lbl_local_tax_owed.Scripts.OnBeforePrint = "lbl_local_tax_owed_BeforePrint"
            Me.lbl_local_tax_owed.SizeF = New System.Drawing.SizeF(233.0!, 16.0!)
            Me.lbl_local_tax_owed.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'ParameterClient
            '
            Me.ParameterClient.Description = "Client ID"
            Me.ParameterClient.Name = "ParameterClient"
            Me.ParameterClient.Type = GetType(Integer)
            Me.ParameterClient.Value = -1
            Me.ParameterClient.Visible = False
            '
            'ParameterMethod
            '
            Me.ParameterMethod.Description = "Select ID?"
            Me.ParameterMethod.Name = "ParameterMethod"
            Me.ParameterMethod.Type = GetType(Boolean)
            Me.ParameterMethod.Value = False
            Me.ParameterMethod.Visible = False
            '
            'ParameterID
            '
            Me.ParameterID.Description = "Intake ID"
            Me.ParameterID.Name = "ParameterID"
            Me.ParameterID.Value = ""
            Me.ParameterID.Visible = False
            '
            'ParameterIntakeClient
            '
            Me.ParameterIntakeClient.Name = "ParameterIntakeClient"
            Me.ParameterIntakeClient.Type = GetType(Integer)
            Me.ParameterIntakeClient.Value = -1
            Me.ParameterIntakeClient.Visible = False
            '
            'Subreport_Budget
            '
            Me.Subreport_Budget.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 313.5417!)
            Me.Subreport_Budget.Name = "Subreport_Budget"
            Me.Subreport_Budget.ReportSource = Me.IntakeBudget1
            Me.Subreport_Budget.SizeF = New System.Drawing.SizeF(800.0!, 22.99994!)
            '
            'Subreport_BottomLine
            '
            Me.Subreport_BottomLine.LocationFloat = New DevExpress.Utils.PointFloat(401.4584!, 10.00001!)
            Me.Subreport_BottomLine.Name = "Subreport_BottomLine"
            Me.Subreport_BottomLine.ReportSource = Me.BottomLine1
            Me.Subreport_BottomLine.SizeF = New System.Drawing.SizeF(388.5417!, 25.0!)
            '
            'Subreport_unsecured
            '
            Me.Subreport_unsecured.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 276.0417!)
            Me.Subreport_unsecured.Name = "Subreport_unsecured"
            Me.Subreport_unsecured.ReportSource = Me.IntakeUnsecuredReport1
            Me.Subreport_unsecured.SizeF = New System.Drawing.SizeF(800.0!, 22.99994!)
            '
            'Subreport_Secured
            '
            Me.Subreport_Secured.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 238.875!)
            Me.Subreport_Secured.Name = "Subreport_Secured"
            Me.Subreport_Secured.ReportSource = Me.IntakeSecuredReport1
            Me.Subreport_Secured.SizeF = New System.Drawing.SizeF(800.0!, 23.00003!)
            '
            'Subreport_income
            '
            Me.Subreport_income.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 200.7084!)
            Me.Subreport_income.Name = "Subreport_income"
            Me.Subreport_income.ReportSource = Me.IntakeIncomeReport1
            Me.Subreport_income.SizeF = New System.Drawing.SizeF(800.0!, 23.00003!)
            '
            'Subreport_People
            '
            Me.Subreport_People.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 163.5417!)
            Me.Subreport_People.Name = "Subreport_People"
            Me.Subreport_People.ReportSource = Me.IntakePeopleReport2
            Me.Subreport_People.SizeF = New System.Drawing.SizeF(800.0!, 23.00002!)
            '
            'XrSubreport_ClientInfo
            '
            Me.XrSubreport_ClientInfo.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 10.00001!)
            Me.XrSubreport_ClientInfo.Name = "XrSubreport_ClientInfo"
            Me.XrSubreport_ClientInfo.ReportSource = Me.IntakeClient1
            Me.XrSubreport_ClientInfo.SizeF = New System.Drawing.SizeF(388.5417!, 25.0!)
            '
            'IntakeClientInfoReport
            '
            Me.Bands.AddRange(New DevExpress.XtraReports.UI.Band() {Me.TopMarginBand1, Me.Detail, Me.BottomMarginBand1, Me.PageHeader, Me.PageFooter})
            Me.Parameters.AddRange(New DevExpress.XtraReports.Parameters.Parameter() {Me.ParameterClient, Me.ParameterMethod, Me.ParameterID, Me.ParameterIntakeClient})
            Me.RequestParameters = False
            Me.ScriptLanguage = DevExpress.XtraReports.ScriptLanguage.VisualBasic
            Me.ScriptReferencesString = resources.GetString("$this.ScriptReferencesString")
            Me.Scripts.OnBeforePrint = "IntakeClientInfoReport_BeforePrint"
            Me.ScriptsSource = resources.GetString("$this.ScriptsSource")
            Me.StyleSheet.AddRange(New DevExpress.XtraReports.UI.XRControlStyle() {Me.XrControlStyle_Header, Me.XrControlStyle_Totals, Me.XrControlStyle_HeaderPannel, Me.XrControlStyle_GroupHeader})
            Me.Version = "11.2"
            CType(Me.IntakeBudget1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.BottomLine1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.IntakeUnsecuredReport1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.IntakeSecuredReport1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.IntakeIncomeReport1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.IntakePeopleReport2, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.IntakeClient1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me, System.ComponentModel.ISupportInitialize).EndInit()

        End Sub

        Friend WithEvents XrLabel11 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel12 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel14 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel13 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel15 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents lbl_cause_fin_problem1 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents lbl_referred_by As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents Subreport_People As DevExpress.XtraReports.UI.XRSubreport
        Friend WithEvents Subreport_income As DevExpress.XtraReports.UI.XRSubreport
        Friend WithEvents Subreport_Secured As DevExpress.XtraReports.UI.XRSubreport
        Friend WithEvents Subreport_unsecured As DevExpress.XtraReports.UI.XRSubreport
        Friend WithEvents Subreport_BottomLine As DevExpress.XtraReports.UI.XRSubreport
        Friend WithEvents XrSubreport_ClientInfo As DevExpress.XtraReports.UI.XRSubreport
        Friend WithEvents Subreport_Budget As DevExpress.XtraReports.UI.XRSubreport
        Private WithEvents IntakeClient1 As IntakeClient
        Friend WithEvents ParameterClient As DevExpress.XtraReports.Parameters.Parameter
        Friend WithEvents ParameterMethod As DevExpress.XtraReports.Parameters.Parameter
        Friend WithEvents ParameterID As DevExpress.XtraReports.Parameters.Parameter
        Friend WithEvents ParameterIntakeClient As DevExpress.XtraReports.Parameters.Parameter
        Private WithEvents IntakeSecuredReport1 As IntakeSecuredReport
        Private WithEvents IntakeBudget1 As IntakeBudget
        Private WithEvents IntakeUnsecuredReport1 As IntakeUnsecuredReport
        Private WithEvents IntakeIncomeReport1 As IntakeIncomeReport
        Private WithEvents IntakePeopleReport2 As IntakeApplicantReport
        Protected Friend WithEvents lbl_fed_tax_owed As DevExpress.XtraReports.UI.XRLabel
        Protected Friend WithEvents lbl_state_tax_owed As DevExpress.XtraReports.UI.XRLabel
        Protected Friend WithEvents lbl_local_tax_owed As DevExpress.XtraReports.UI.XRLabel
        Private WithEvents BottomLine1 As BottomLine
    End Class
End Namespace
