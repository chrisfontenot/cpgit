Namespace Intake.ClientInfo
    <Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
    Partial Class IntakeUnsecuredReport
        Inherits DevExpress.XtraReports.UI.XtraReport

        'Component overrides dispose to clean up the component list.
        Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
            If disposing Then
                If components IsNot Nothing Then
                    components.Dispose()
                End If
            End If
            MyBase.Dispose(disposing)
        End Sub

        'Required by the Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Designer
        'It can be modified using the Designer.
        'Do not modify it using the code editor.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
            Dim XrSummary1 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary()
            Dim XrSummary2 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary()
            Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(IntakeUnsecuredReport))
            Me.Detail = New DevExpress.XtraReports.UI.DetailBand()
            Me.XrTable2 = New DevExpress.XtraReports.UI.XRTable()
            Me.XrTableRow2 = New DevExpress.XtraReports.UI.XRTableRow()
            Me.lbl_creditor_name = New DevExpress.XtraReports.UI.XRTableCell()
            Me.lbl_account_number = New DevExpress.XtraReports.UI.XRTableCell()
            Me.lbl_non_dmp_interest = New DevExpress.XtraReports.UI.XRTableCell()
            Me.lbl_balance = New DevExpress.XtraReports.UI.XRTableCell()
            Me.lbl_non_dmp_payment = New DevExpress.XtraReports.UI.XRTableCell()
            Me.lbl_months_delinquent = New DevExpress.XtraReports.UI.XRTableCell()
            Me.PageHeader = New DevExpress.XtraReports.UI.PageHeaderBand()
            Me.XrPanel1 = New DevExpress.XtraReports.UI.XRPanel()
            Me.XrTable1 = New DevExpress.XtraReports.UI.XRTable()
            Me.XrTableRow1 = New DevExpress.XtraReports.UI.XRTableRow()
            Me.XrTableCell1 = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableCell10 = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableCell2 = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableCell13 = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableCell3 = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableCell16 = New DevExpress.XtraReports.UI.XRTableCell()
            Me.ReportFooter = New DevExpress.XtraReports.UI.ReportFooterBand()
            Me.XrTable3 = New DevExpress.XtraReports.UI.XRTable()
            Me.XrTableRow3 = New DevExpress.XtraReports.UI.XRTableRow()
            Me.XrTableCell12 = New DevExpress.XtraReports.UI.XRTableCell()
            Me.lbl_total_balance = New DevExpress.XtraReports.UI.XRTableCell()
            Me.lbl_total_non_dmp_payment = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableCell18 = New DevExpress.XtraReports.UI.XRTableCell()
            Me.ReportHeader = New DevExpress.XtraReports.UI.ReportHeaderBand()
            Me.XrLabel1 = New DevExpress.XtraReports.UI.XRLabel()
            Me.TopMarginBand1 = New DevExpress.XtraReports.UI.TopMarginBand()
            Me.BottomMarginBand1 = New DevExpress.XtraReports.UI.BottomMarginBand()
            CType(Me.XrTable2, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.XrTable1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.XrTable3, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
            '
            'Detail
            '
            Me.Detail.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrTable2})
            Me.Detail.HeightF = 17.0!
            Me.Detail.Name = "Detail"
            Me.Detail.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
            Me.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
            '
            'XrTable2
            '
            Me.XrTable2.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
            Me.XrTable2.Name = "XrTable2"
            Me.XrTable2.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 96.0!)
            Me.XrTable2.Rows.AddRange(New DevExpress.XtraReports.UI.XRTableRow() {Me.XrTableRow2})
            Me.XrTable2.SizeF = New System.Drawing.SizeF(700.0!, 17.0!)
            Me.XrTable2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
            '
            'XrTableRow2
            '
            Me.XrTableRow2.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.lbl_creditor_name, Me.lbl_account_number, Me.lbl_non_dmp_interest, Me.lbl_balance, Me.lbl_non_dmp_payment, Me.lbl_months_delinquent})
            Me.XrTableRow2.Name = "XrTableRow2"
            Me.XrTableRow2.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 96.0!)
            Me.XrTableRow2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
            Me.XrTableRow2.Weight = 1.0R
            '
            'lbl_creditor_name
            '
            Me.lbl_creditor_name.CanGrow = False
            Me.lbl_creditor_name.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "creditor_name")})
            Me.lbl_creditor_name.Name = "lbl_creditor_name"
            Me.lbl_creditor_name.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.lbl_creditor_name.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
            Me.lbl_creditor_name.Weight = 0.33R
            Me.lbl_creditor_name.WordWrap = False
            '
            'lbl_account_number
            '
            Me.lbl_account_number.CanGrow = False
            Me.lbl_account_number.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "account_number")})
            Me.lbl_account_number.Name = "lbl_account_number"
            Me.lbl_account_number.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.lbl_account_number.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
            Me.lbl_account_number.Weight = 0.20142857142857143R
            Me.lbl_account_number.WordWrap = False
            '
            'lbl_non_dmp_interest
            '
            Me.lbl_non_dmp_interest.CanGrow = False
            Me.lbl_non_dmp_interest.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "non_dmp_interest", "{0:p2}")})
            Me.lbl_non_dmp_interest.Name = "lbl_non_dmp_interest"
            Me.lbl_non_dmp_interest.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.lbl_non_dmp_interest.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.lbl_non_dmp_interest.Weight = 0.10714285714285714R
            Me.lbl_non_dmp_interest.WordWrap = False
            '
            'lbl_balance
            '
            Me.lbl_balance.CanGrow = False
            Me.lbl_balance.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "balance", "{0:c2}")})
            Me.lbl_balance.Name = "lbl_balance"
            Me.lbl_balance.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.lbl_balance.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.lbl_balance.Weight = 0.10714285714285714R
            Me.lbl_balance.WordWrap = False
            '
            'lbl_non_dmp_payment
            '
            Me.lbl_non_dmp_payment.CanGrow = False
            Me.lbl_non_dmp_payment.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "non_dmp_payment", "{0:c2}")})
            Me.lbl_non_dmp_payment.Name = "lbl_non_dmp_payment"
            Me.lbl_non_dmp_payment.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.lbl_non_dmp_payment.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.lbl_non_dmp_payment.Weight = 0.14714285714285713R
            Me.lbl_non_dmp_payment.WordWrap = False
            '
            'lbl_months_delinquent
            '
            Me.lbl_months_delinquent.CanGrow = False
            Me.lbl_months_delinquent.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "months_delinquent", "{0:f0}")})
            Me.lbl_months_delinquent.Name = "lbl_months_delinquent"
            Me.lbl_months_delinquent.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.lbl_months_delinquent.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.lbl_months_delinquent.Weight = 0.10714285714285714R
            Me.lbl_months_delinquent.WordWrap = False
            '
            'PageHeader
            '
            Me.PageHeader.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrPanel1})
            Me.PageHeader.HeightF = 21.0!
            Me.PageHeader.Name = "PageHeader"
            Me.PageHeader.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
            Me.PageHeader.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
            '
            'XrPanel1
            '
            Me.XrPanel1.BackColor = System.Drawing.Color.Teal
            Me.XrPanel1.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrTable1})
            Me.XrPanel1.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
            Me.XrPanel1.Name = "XrPanel1"
            Me.XrPanel1.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
            Me.XrPanel1.SizeF = New System.Drawing.SizeF(700.0!, 17.0!)
            '
            'XrTable1
            '
            Me.XrTable1.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrTable1.ForeColor = System.Drawing.Color.White
            Me.XrTable1.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
            Me.XrTable1.Name = "XrTable1"
            Me.XrTable1.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 96.0!)
            Me.XrTable1.Rows.AddRange(New DevExpress.XtraReports.UI.XRTableRow() {Me.XrTableRow1})
            Me.XrTable1.SizeF = New System.Drawing.SizeF(700.0!, 17.0!)
            Me.XrTable1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
            '
            'XrTableRow1
            '
            Me.XrTableRow1.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.XrTableCell1, Me.XrTableCell10, Me.XrTableCell2, Me.XrTableCell13, Me.XrTableCell3, Me.XrTableCell16})
            Me.XrTableRow1.Name = "XrTableRow1"
            Me.XrTableRow1.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 96.0!)
            Me.XrTableRow1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
            Me.XrTableRow1.Weight = 1.0R
            '
            'XrTableCell1
            '
            Me.XrTableCell1.Name = "XrTableCell1"
            Me.XrTableCell1.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrTableCell1.Text = "CREDITOR"
            Me.XrTableCell1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
            Me.XrTableCell1.Weight = 0.33R
            '
            'XrTableCell10
            '
            Me.XrTableCell10.Name = "XrTableCell10"
            Me.XrTableCell10.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrTableCell10.Text = "ACCOUNT"
            Me.XrTableCell10.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
            Me.XrTableCell10.Weight = 0.20142857142857143R
            '
            'XrTableCell2
            '
            Me.XrTableCell2.Name = "XrTableCell2"
            Me.XrTableCell2.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrTableCell2.Text = "INTEREST"
            Me.XrTableCell2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrTableCell2.Weight = 0.10714285714285714R
            '
            'XrTableCell13
            '
            Me.XrTableCell13.Name = "XrTableCell13"
            Me.XrTableCell13.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrTableCell13.Text = "BALANCE"
            Me.XrTableCell13.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrTableCell13.Weight = 0.10714285714285714R
            '
            'XrTableCell3
            '
            Me.XrTableCell3.Name = "XrTableCell3"
            Me.XrTableCell3.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrTableCell3.Text = "PAYMENT"
            Me.XrTableCell3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrTableCell3.Weight = 0.14714285714285713R
            '
            'XrTableCell16
            '
            Me.XrTableCell16.Name = "XrTableCell16"
            Me.XrTableCell16.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrTableCell16.Text = "PAST DUE"
            Me.XrTableCell16.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrTableCell16.Weight = 0.10714285714285714R
            '
            'ReportFooter
            '
            Me.ReportFooter.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrTable3})
            Me.ReportFooter.HeightF = 30.0!
            Me.ReportFooter.Name = "ReportFooter"
            Me.ReportFooter.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
            Me.ReportFooter.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
            '
            'XrTable3
            '
            Me.XrTable3.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 5.0!)
            Me.XrTable3.Name = "XrTable3"
            Me.XrTable3.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 96.0!)
            Me.XrTable3.Rows.AddRange(New DevExpress.XtraReports.UI.XRTableRow() {Me.XrTableRow3})
            Me.XrTable3.SizeF = New System.Drawing.SizeF(700.0!, 17.0!)
            Me.XrTable3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
            '
            'XrTableRow3
            '
            Me.XrTableRow3.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.XrTableCell12, Me.lbl_total_balance, Me.lbl_total_non_dmp_payment, Me.XrTableCell18})
            Me.XrTableRow3.Name = "XrTableRow3"
            Me.XrTableRow3.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 96.0!)
            Me.XrTableRow3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
            Me.XrTableRow3.Weight = 1.0R
            '
            'XrTableCell12
            '
            Me.XrTableCell12.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrTableCell12.ForeColor = System.Drawing.Color.Teal
            Me.XrTableCell12.Name = "XrTableCell12"
            Me.XrTableCell12.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrTableCell12.Text = "TOTALS"
            Me.XrTableCell12.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
            Me.XrTableCell12.Weight = 0.59571428571428575R
            '
            'lbl_total_balance
            '
            Me.lbl_total_balance.BorderColor = System.Drawing.Color.Teal
            Me.lbl_total_balance.Borders = DevExpress.XtraPrinting.BorderSide.Top
            Me.lbl_total_balance.BorderWidth = 2
            Me.lbl_total_balance.CanGrow = False
            Me.lbl_total_balance.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "balance")})
            Me.lbl_total_balance.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.lbl_total_balance.ForeColor = System.Drawing.Color.Teal
            Me.lbl_total_balance.Name = "lbl_total_balance"
            Me.lbl_total_balance.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            XrSummary1.FormatString = "{0:c}"
            XrSummary1.Running = DevExpress.XtraReports.UI.SummaryRunning.Report
            Me.lbl_total_balance.Summary = XrSummary1
            Me.lbl_total_balance.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.lbl_total_balance.Weight = 0.15R
            Me.lbl_total_balance.WordWrap = False
            '
            'lbl_total_non_dmp_payment
            '
            Me.lbl_total_non_dmp_payment.BorderColor = System.Drawing.Color.Teal
            Me.lbl_total_non_dmp_payment.Borders = DevExpress.XtraPrinting.BorderSide.Top
            Me.lbl_total_non_dmp_payment.BorderWidth = 2
            Me.lbl_total_non_dmp_payment.CanGrow = False
            Me.lbl_total_non_dmp_payment.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "non_dmp_payment")})
            Me.lbl_total_non_dmp_payment.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.lbl_total_non_dmp_payment.ForeColor = System.Drawing.Color.Teal
            Me.lbl_total_non_dmp_payment.Name = "lbl_total_non_dmp_payment"
            Me.lbl_total_non_dmp_payment.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            XrSummary2.FormatString = "{0:c}"
            XrSummary2.Running = DevExpress.XtraReports.UI.SummaryRunning.Report
            Me.lbl_total_non_dmp_payment.Summary = XrSummary2
            Me.lbl_total_non_dmp_payment.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.lbl_total_non_dmp_payment.Weight = 0.14714285714285713R
            Me.lbl_total_non_dmp_payment.WordWrap = False
            '
            'XrTableCell18
            '
            Me.XrTableCell18.Name = "XrTableCell18"
            Me.XrTableCell18.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrTableCell18.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrTableCell18.Weight = 0.10714285714285714R
            '
            'ReportHeader
            '
            Me.ReportHeader.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel1})
            Me.ReportHeader.HeightF = 44.0!
            Me.ReportHeader.Name = "ReportHeader"
            Me.ReportHeader.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
            Me.ReportHeader.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
            '
            'XrLabel1
            '
            Me.XrLabel1.Font = New System.Drawing.Font("Times New Roman", 14.0!, System.Drawing.FontStyle.Bold)
            Me.XrLabel1.ForeColor = System.Drawing.Color.Teal
            Me.XrLabel1.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 8.0!)
            Me.XrLabel1.Name = "XrLabel1"
            Me.XrLabel1.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel1.SizeF = New System.Drawing.SizeF(700.0!, 25.0!)
            Me.XrLabel1.Text = "UN-SECURED DEBT LIST"
            Me.XrLabel1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
            '
            'TopMarginBand1
            '
            Me.TopMarginBand1.HeightF = 25.0!
            Me.TopMarginBand1.Name = "TopMarginBand1"
            '
            'BottomMarginBand1
            '
            Me.BottomMarginBand1.HeightF = 25.0!
            Me.BottomMarginBand1.Name = "BottomMarginBand1"
            '
            'IntakeUnsecuredReport
            '
            Me.Bands.AddRange(New DevExpress.XtraReports.UI.Band() {Me.Detail, Me.PageHeader, Me.ReportFooter, Me.ReportHeader, Me.TopMarginBand1, Me.BottomMarginBand1})
            Me.Margins = New System.Drawing.Printing.Margins(25, 25, 25, 25)
            Me.ReportPrintOptions.PrintOnEmptyDataSource = False
            Me.ScriptLanguage = DevExpress.XtraReports.ScriptLanguage.VisualBasic
            Me.ScriptReferencesString = resources.GetString("$this.ScriptReferencesString")
            Me.Scripts.OnBeforePrint = "IntakeUnsecuredReport_BeforePrint"
            Me.ScriptsSource = resources.GetString("$this.ScriptsSource")
            Me.Version = "11.2"
            CType(Me.XrTable2, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.XrTable1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.XrTable3, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me, System.ComponentModel.ISupportInitialize).EndInit()

        End Sub
        Friend WithEvents Detail As DevExpress.XtraReports.UI.DetailBand
        Private WithEvents PageHeader As DevExpress.XtraReports.UI.PageHeaderBand
        Friend WithEvents ReportFooter As DevExpress.XtraReports.UI.ReportFooterBand
        Friend WithEvents XrTable2 As DevExpress.XtraReports.UI.XRTable
        Friend WithEvents XrTableRow2 As DevExpress.XtraReports.UI.XRTableRow
        Friend WithEvents XrTable3 As DevExpress.XtraReports.UI.XRTable
        Friend WithEvents XrTableRow3 As DevExpress.XtraReports.UI.XRTableRow
        Friend WithEvents XrTableCell12 As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents XrTableCell18 As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents XrPanel1 As DevExpress.XtraReports.UI.XRPanel
        Friend WithEvents XrTable1 As DevExpress.XtraReports.UI.XRTable
        Friend WithEvents XrTableRow1 As DevExpress.XtraReports.UI.XRTableRow
        Friend WithEvents XrTableCell1 As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents XrTableCell10 As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents XrTableCell2 As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents XrTableCell13 As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents XrTableCell3 As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents XrTableCell16 As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents ReportHeader As DevExpress.XtraReports.UI.ReportHeaderBand
        Friend WithEvents XrLabel1 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents lbl_creditor_name As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents lbl_account_number As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents lbl_balance As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents lbl_non_dmp_interest As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents lbl_non_dmp_payment As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents lbl_months_delinquent As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents lbl_total_non_dmp_payment As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents lbl_total_balance As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents TopMarginBand1 As DevExpress.XtraReports.UI.TopMarginBand
        Friend WithEvents BottomMarginBand1 As DevExpress.XtraReports.UI.BottomMarginBand
    End Class
End Namespace
