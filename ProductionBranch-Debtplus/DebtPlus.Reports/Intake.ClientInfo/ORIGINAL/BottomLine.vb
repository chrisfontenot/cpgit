#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}
#End Region

Namespace Intake.ClientInfo
    Public Class BottomLine

        Public Sub New(ByVal Container As System.ComponentModel.IContainer)
            MyClass.New()
            Container.Add(Me)
        End Sub

        Public Sub New()
            MyBase.New()
            InitializeComponent()
            AddHandler BeforePrint, AddressOf BottomLine_BeforePrint
        End Sub

        Private ds As New System.Data.DataSet("ds")

        Private Sub BottomLine_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs)
            Const TableName As String = "intake_bottom_line"
            Dim rpt As DevExpress.XtraReports.UI.XtraReport = CType(sender, DevExpress.XtraReports.UI.XtraReport)
            Dim MasterRpt As DevExpress.XtraReports.UI.XtraReport = CType(rpt.MasterReport, DevExpress.XtraReports.UI.XtraReport)
            Dim sqlInfo As DebtPlus.LINQ.SQLInfoClass = DebtPlus.LINQ.SQLInfoClass.getDefault()
            Dim IntakeClient As Int32 = CType(MasterRpt.Parameters("ParameterIntakeClient").Value, System.Int32)

            Dim current_cursor As System.Windows.Forms.Cursor = System.Windows.Forms.Cursor.Current
            Try
                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.WaitCursor
                Using cmd As New System.Data.SqlClient.SqlCommand()
                    With cmd
                        .Connection = New System.Data.SqlClient.SqlConnection(sqlInfo.ConnectionString)
                        .CommandText = "rpt_intake_bottom_line"
                        .CommandType = System.Data.CommandType.StoredProcedure
                        .Parameters.Add("@intake_client", System.Data.SqlDbType.Int).Value = IntakeClient
                        .CommandTimeout = 0
                    End With

                    Using da As New System.Data.SqlClient.SqlDataAdapter(cmd)
                        da.Fill(ds, TableName)
                    End Using
                End Using

                Dim vue As System.Data.DataView = ds.Tables(TableName).DefaultView

                '-- Bind the columns as needed
                If vue.Count > 0 Then
                    Dim drv As System.Data.DataRowView = vue(0)

                    Dim applicant_gross As Decimal = DebtPlus.Utils.Nulls.DDec(drv("applicant_gross"))
                    Dim applicant_net As Decimal = DebtPlus.Utils.Nulls.DDec(drv("applicant_net"))
                    Dim coapplicant_gross As Decimal = DebtPlus.Utils.Nulls.DDec(drv("coapplicant_gross"))
                    Dim coapplicant_net As Decimal = DebtPlus.Utils.Nulls.DDec(drv("coapplicant_net"))
                    Dim expenses As Decimal = DebtPlus.Utils.Nulls.DDec(drv("expenses"))
                    Dim other_income As Decimal = DebtPlus.Utils.Nulls.DDec(drv("other_income"))

                    With CType(rpt.FindControl("XrTable1", False), DevExpress.XtraReports.UI.XRTable)
                        .FindControl("XrTableCell_applicant_gross", False).Text = String.Format("{0:c}", applicant_gross)
                        .FindControl("XrTableCell_coapplicant_gross", False).Text = String.Format("{0:c}", coapplicant_gross)
                        .FindControl("XrTableCell_total_gross", False).Text = String.Format("{0:c}", applicant_gross + coapplicant_gross)
                        .FindControl("XrTableCell_applicant_net", False).Text = String.Format("{0:c}", applicant_net)
                        .FindControl("XrTableCell_coapplicant_net", False).Text = String.Format("{0:c}", coapplicant_net)
                        .FindControl("XrTableCell_other_income", False).Text = String.Format("{0:c}", other_income)
                        .FindControl("XrTableCell_total_net", False).Text = String.Format("{0:c}", applicant_net + coapplicant_net + other_income)
                        .FindControl("XrTableCell_expenses", False).Text = String.Format("{0:c}", expenses)
                        .FindControl("XrTableCell_available_income", False).Text = String.Format("{0:c}", applicant_net + coapplicant_net + other_income - expenses)
                    End With
                End If

            Catch ex As System.Data.SqlClient.SqlException
                Dim gdr As New DebtPlus.Repository.GetDataResult() : gdr.HandleException(ex) : DebtPlus.UI.Common.ErrorHandling.HandleErrors(gdr, "Error reading intake_bottom_line")

            Finally
                System.Windows.Forms.Cursor.Current = current_cursor
            End Try
        End Sub
    End Class
End Namespace
