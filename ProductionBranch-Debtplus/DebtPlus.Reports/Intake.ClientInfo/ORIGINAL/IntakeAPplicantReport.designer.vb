﻿Namespace Intake.ClientInfo
    <Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
    Partial Public Class IntakeApplicantReport
        Inherits DevExpress.XtraReports.UI.XtraReport

        'Component overrides dispose to clean up the component list.
        Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
            If disposing Then
                If components IsNot Nothing Then
                    components.Dispose()
                End If
            End If
            MyBase.Dispose(disposing)
        End Sub

        'Required by the Designer
        Private components As System.ComponentModel.IContainer
        Friend WithEvents Detail As DevExpress.XtraReports.UI.DetailBand

        'NOTE: The following procedure is required by the Designer
        'It can be modified using the Designer.
        'Do not modify it using the code editor.
        Friend WithEvents XrTable1 As DevExpress.XtraReports.UI.XRTable
        Friend WithEvents XrLabel1 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel2 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel3 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel4 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel5 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel6 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel7 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrTableRow1 As DevExpress.XtraReports.UI.XRTableRow
        Friend WithEvents lbl_birthdate As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents lbl_education As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents lbl_ssn As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents lbl_race As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents lbl_former As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents lbl_work_ph As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents lbl_work_fax As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents lbl_employer As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents lbl_job As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents lbl_name As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents lbl_email As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents lbl_header As DevExpress.XtraReports.UI.XRLabel
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
            Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(IntakeApplicantReport))
            Me.Detail = New DevExpress.XtraReports.UI.DetailBand()
            Me.lbl_email = New DevExpress.XtraReports.UI.XRLabel()
            Me.lbl_name = New DevExpress.XtraReports.UI.XRLabel()
            Me.lbl_job = New DevExpress.XtraReports.UI.XRLabel()
            Me.lbl_employer = New DevExpress.XtraReports.UI.XRLabel()
            Me.lbl_work_fax = New DevExpress.XtraReports.UI.XRLabel()
            Me.lbl_work_ph = New DevExpress.XtraReports.UI.XRLabel()
            Me.lbl_former = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel7 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel6 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel5 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel4 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel3 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel2 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel1 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrTable1 = New DevExpress.XtraReports.UI.XRTable()
            Me.XrTableRow1 = New DevExpress.XtraReports.UI.XRTableRow()
            Me.lbl_birthdate = New DevExpress.XtraReports.UI.XRTableCell()
            Me.lbl_race = New DevExpress.XtraReports.UI.XRTableCell()
            Me.lbl_education = New DevExpress.XtraReports.UI.XRTableCell()
            Me.lbl_ssn = New DevExpress.XtraReports.UI.XRTableCell()
            Me.lbl_header = New DevExpress.XtraReports.UI.XRLabel()
            Me.TopMarginBand1 = New DevExpress.XtraReports.UI.TopMarginBand()
            Me.BottomMarginBand1 = New DevExpress.XtraReports.UI.BottomMarginBand()
            Me.GroupHeader1 = New DevExpress.XtraReports.UI.GroupHeaderBand()
            CType(Me.XrTable1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
            '
            'Detail
            '
            Me.Detail.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.lbl_email, Me.lbl_name, Me.lbl_job, Me.lbl_employer, Me.lbl_work_fax, Me.lbl_work_ph, Me.lbl_former, Me.XrLabel7, Me.XrLabel6, Me.XrLabel5, Me.XrLabel4, Me.XrLabel3, Me.XrLabel2, Me.XrLabel1, Me.XrTable1})
            Me.Detail.HeightF = 177.0!
            Me.Detail.Name = "Detail"
            Me.Detail.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
            Me.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
            '
            'lbl_email
            '
            Me.lbl_email.CanGrow = False
            Me.lbl_email.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "email")})
            Me.lbl_email.LocationFloat = New DevExpress.Utils.PointFloat(108.0!, 84.0!)
            Me.lbl_email.Name = "lbl_email"
            Me.lbl_email.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.lbl_email.SizeF = New System.Drawing.SizeF(642.0!, 17.0!)
            Me.lbl_email.Text = "Email Address:"
            Me.lbl_email.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
            '
            'lbl_name
            '
            Me.lbl_name.CanGrow = False
            Me.lbl_name.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "name")})
            Me.lbl_name.LocationFloat = New DevExpress.Utils.PointFloat(108.0!, 50.0!)
            Me.lbl_name.Name = "lbl_name"
            Me.lbl_name.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.lbl_name.SizeF = New System.Drawing.SizeF(642.0!, 17.0!)
            Me.lbl_name.Text = "Name:"
            Me.lbl_name.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
            '
            'lbl_job
            '
            Me.lbl_job.CanGrow = False
            Me.lbl_job.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "job")})
            Me.lbl_job.LocationFloat = New DevExpress.Utils.PointFloat(108.0!, 101.0!)
            Me.lbl_job.Name = "lbl_job"
            Me.lbl_job.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.lbl_job.SizeF = New System.Drawing.SizeF(642.0!, 17.0!)
            Me.lbl_job.Text = "Job Description:"
            Me.lbl_job.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
            '
            'lbl_employer
            '
            Me.lbl_employer.LocationFloat = New DevExpress.Utils.PointFloat(108.0!, 118.0!)
            Me.lbl_employer.Multiline = True
            Me.lbl_employer.Name = "lbl_employer"
            Me.lbl_employer.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.lbl_employer.Scripts.OnBeforePrint = "lbl_employer_BeforePrint"
            Me.lbl_employer.SizeF = New System.Drawing.SizeF(642.0!, 17.0!)
            Me.lbl_employer.Text = "Employer:"
            Me.lbl_employer.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
            Me.lbl_employer.WordWrap = False
            '
            'lbl_work_fax
            '
            Me.lbl_work_fax.CanGrow = False
            Me.lbl_work_fax.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "work_fax")})
            Me.lbl_work_fax.LocationFloat = New DevExpress.Utils.PointFloat(108.0!, 152.0!)
            Me.lbl_work_fax.Name = "lbl_work_fax"
            Me.lbl_work_fax.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.lbl_work_fax.SizeF = New System.Drawing.SizeF(642.0!, 17.0!)
            Me.lbl_work_fax.Text = "Office FAX:"
            Me.lbl_work_fax.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
            '
            'lbl_work_ph
            '
            Me.lbl_work_ph.CanGrow = False
            Me.lbl_work_ph.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "work_ph")})
            Me.lbl_work_ph.LocationFloat = New DevExpress.Utils.PointFloat(108.0!, 135.0!)
            Me.lbl_work_ph.Name = "lbl_work_ph"
            Me.lbl_work_ph.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.lbl_work_ph.SizeF = New System.Drawing.SizeF(642.0!, 17.0!)
            Me.lbl_work_ph.Text = "Daytime Telephone:"
            Me.lbl_work_ph.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
            '
            'lbl_former
            '
            Me.lbl_former.CanGrow = False
            Me.lbl_former.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "former")})
            Me.lbl_former.LocationFloat = New DevExpress.Utils.PointFloat(108.0!, 67.0!)
            Me.lbl_former.Name = "lbl_former"
            Me.lbl_former.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.lbl_former.SizeF = New System.Drawing.SizeF(642.0!, 17.0!)
            Me.lbl_former.Text = "Former Name:"
            Me.lbl_former.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
            '
            'XrLabel7
            '
            Me.XrLabel7.CanGrow = False
            Me.XrLabel7.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 152.0!)
            Me.XrLabel7.Name = "XrLabel7"
            Me.XrLabel7.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel7.SizeF = New System.Drawing.SizeF(100.0!, 17.0!)
            Me.XrLabel7.Text = "Office FAX:"
            Me.XrLabel7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
            '
            'XrLabel6
            '
            Me.XrLabel6.CanGrow = False
            Me.XrLabel6.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 135.0!)
            Me.XrLabel6.Name = "XrLabel6"
            Me.XrLabel6.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel6.SizeF = New System.Drawing.SizeF(100.0!, 17.0!)
            Me.XrLabel6.Text = "Daytime Telephone:"
            Me.XrLabel6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
            '
            'XrLabel5
            '
            Me.XrLabel5.CanGrow = False
            Me.XrLabel5.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 118.0!)
            Me.XrLabel5.Name = "XrLabel5"
            Me.XrLabel5.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel5.SizeF = New System.Drawing.SizeF(100.0!, 17.0!)
            Me.XrLabel5.Text = "Employer:"
            Me.XrLabel5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
            '
            'XrLabel4
            '
            Me.XrLabel4.CanGrow = False
            Me.XrLabel4.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 101.0!)
            Me.XrLabel4.Name = "XrLabel4"
            Me.XrLabel4.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel4.SizeF = New System.Drawing.SizeF(100.0!, 17.0!)
            Me.XrLabel4.Text = "Job Description:"
            Me.XrLabel4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
            '
            'XrLabel3
            '
            Me.XrLabel3.CanGrow = False
            Me.XrLabel3.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 84.0!)
            Me.XrLabel3.Name = "XrLabel3"
            Me.XrLabel3.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel3.SizeF = New System.Drawing.SizeF(100.0!, 17.0!)
            Me.XrLabel3.Text = "Email Address:"
            Me.XrLabel3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
            '
            'XrLabel2
            '
            Me.XrLabel2.CanGrow = False
            Me.XrLabel2.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 67.0!)
            Me.XrLabel2.Name = "XrLabel2"
            Me.XrLabel2.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel2.SizeF = New System.Drawing.SizeF(100.0!, 17.0!)
            Me.XrLabel2.Text = "Former Name:"
            Me.XrLabel2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
            '
            'XrLabel1
            '
            Me.XrLabel1.CanGrow = False
            Me.XrLabel1.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 50.0!)
            Me.XrLabel1.Name = "XrLabel1"
            Me.XrLabel1.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel1.SizeF = New System.Drawing.SizeF(100.0!, 17.0!)
            Me.XrLabel1.Text = "Name:"
            Me.XrLabel1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
            '
            'XrTable1
            '
            Me.XrTable1.Borders = CType((((DevExpress.XtraPrinting.BorderSide.Left Or DevExpress.XtraPrinting.BorderSide.Top) _
                Or DevExpress.XtraPrinting.BorderSide.Right) _
                Or DevExpress.XtraPrinting.BorderSide.Bottom), DevExpress.XtraPrinting.BorderSide)
            Me.XrTable1.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
            Me.XrTable1.Name = "XrTable1"
            Me.XrTable1.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 96.0!)
            Me.XrTable1.Rows.AddRange(New DevExpress.XtraReports.UI.XRTableRow() {Me.XrTableRow1})
            Me.XrTable1.SizeF = New System.Drawing.SizeF(750.0!, 34.0!)
            Me.XrTable1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
            '
            'XrTableRow1
            '
            Me.XrTableRow1.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.lbl_birthdate, Me.lbl_race, Me.lbl_education, Me.lbl_ssn})
            Me.XrTableRow1.Name = "XrTableRow1"
            Me.XrTableRow1.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 96.0!)
            Me.XrTableRow1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
            Me.XrTableRow1.Weight = 1.0R
            '
            'lbl_birthdate
            '
            Me.lbl_birthdate.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "birthdate", "{0:d}")})
            Me.lbl_birthdate.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.lbl_birthdate.Name = "lbl_birthdate"
            Me.lbl_birthdate.Padding = New DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100.0!)
            Me.lbl_birthdate.Text = "Birthdate: {0:d}"
            Me.lbl_birthdate.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            Me.lbl_birthdate.Weight = 0.24933333333333332R
            '
            'lbl_race
            '
            Me.lbl_race.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "race")})
            Me.lbl_race.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.lbl_race.Name = "lbl_race"
            Me.lbl_race.Padding = New DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100.0!)
            Me.lbl_race.Text = "Ethnic Background: {0}"
            Me.lbl_race.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            Me.lbl_race.Weight = 0.25066666666666665R
            '
            'lbl_education
            '
            Me.lbl_education.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "education")})
            Me.lbl_education.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.lbl_education.Name = "lbl_education"
            Me.lbl_education.Padding = New DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100.0!)
            Me.lbl_education.Text = "Education Level: {0}"
            Me.lbl_education.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            Me.lbl_education.Weight = 0.24933333333333332R
            '
            'lbl_ssn
            '
            Me.lbl_ssn.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "ssn")})
            Me.lbl_ssn.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.lbl_ssn.Name = "lbl_ssn"
            Me.lbl_ssn.Padding = New DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100.0!)
            Me.lbl_ssn.Text = "SSN #:{0}"
            Me.lbl_ssn.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            Me.lbl_ssn.Weight = 0.25066666666666665R
            '
            'lbl_header
            '
            Me.lbl_header.Font = New System.Drawing.Font("Times New Roman", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.lbl_header.ForeColor = System.Drawing.Color.Teal
            Me.lbl_header.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 29.95834!)
            Me.lbl_header.Name = "lbl_header"
            Me.lbl_header.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.lbl_header.Scripts.OnBeforePrint = "lbl_header_BeforePrint"
            Me.lbl_header.SizeF = New System.Drawing.SizeF(750.0!, 34.0!)
            Me.lbl_header.Text = "Applicant Information"
            Me.lbl_header.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
            '
            'TopMarginBand1
            '
            Me.TopMarginBand1.HeightF = 0.0!
            Me.TopMarginBand1.Name = "TopMarginBand1"
            '
            'BottomMarginBand1
            '
            Me.BottomMarginBand1.HeightF = 0.0!
            Me.BottomMarginBand1.Name = "BottomMarginBand1"
            '
            'GroupHeader1
            '
            Me.GroupHeader1.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.lbl_header})
            Me.GroupHeader1.GroupFields.AddRange(New DevExpress.XtraReports.UI.GroupField() {New DevExpress.XtraReports.UI.GroupField("person", DevExpress.XtraReports.UI.XRColumnSortOrder.Ascending)})
            Me.GroupHeader1.HeightF = 73.95834!
            Me.GroupHeader1.Name = "GroupHeader1"
            '
            'IntakeApplicantReport
            '
            Me.Bands.AddRange(New DevExpress.XtraReports.UI.Band() {Me.Detail, Me.TopMarginBand1, Me.BottomMarginBand1, Me.GroupHeader1})
            Me.Margins = New System.Drawing.Printing.Margins(0, 0, 0, 0)
            Me.ReportPrintOptions.PrintOnEmptyDataSource = False
            Me.ScriptLanguage = DevExpress.XtraReports.ScriptLanguage.VisualBasic
            Me.ScriptReferencesString = resources.GetString("$this.ScriptReferencesString")
            Me.Scripts.OnBeforePrint = "IntakeApplicantReport_BeforePrint"
            Me.ScriptsSource = resources.GetString("$this.ScriptsSource")
            Me.Version = "11.2"
            CType(Me.XrTable1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me, System.ComponentModel.ISupportInitialize).EndInit()

        End Sub
        Friend WithEvents TopMarginBand1 As DevExpress.XtraReports.UI.TopMarginBand
        Friend WithEvents BottomMarginBand1 As DevExpress.XtraReports.UI.BottomMarginBand
        Friend WithEvents GroupHeader1 As DevExpress.XtraReports.UI.GroupHeaderBand
    End Class
End Namespace
