Namespace Intake.ClientInfo
    <Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
    Partial Public Class BottomLine
        Inherits DevExpress.XtraReports.UI.XtraReport

        'XtraReport overrides dispose to clean up the component list.
        <System.Diagnostics.DebuggerNonUserCode()> _
        Protected Overrides Sub Dispose(ByVal disposing As Boolean)
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
            MyBase.Dispose(disposing)
        End Sub

        'Required by the Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Designer
        'It can be modified using the Designer.  
        'Do not modify it using the code editor.
        <System.Diagnostics.DebuggerStepThrough()> _
        Private Sub InitializeComponent()
            Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(BottomLine))
            Me.Detail = New DevExpress.XtraReports.UI.DetailBand()
            Me.XrLabel1 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrTable1 = New DevExpress.XtraReports.UI.XRTable()
            Me.XrTableRow1 = New DevExpress.XtraReports.UI.XRTableRow()
            Me.XrTableCell1 = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableCell2 = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableCell3 = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableRow3 = New DevExpress.XtraReports.UI.XRTableRow()
            Me.XrTableCell7 = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableCell_applicant_net = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableCell_applicant_gross = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableRow2 = New DevExpress.XtraReports.UI.XRTableRow()
            Me.XrTableCell4 = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableCell_coapplicant_net = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableCell_coapplicant_gross = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableRow4 = New DevExpress.XtraReports.UI.XRTableRow()
            Me.XrTableCell10 = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableCell_other_income = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableCell11 = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableRow5 = New DevExpress.XtraReports.UI.XRTableRow()
            Me.XrTableCell13 = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableCell_total_net = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableCell_total_gross = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableRow6 = New DevExpress.XtraReports.UI.XRTableRow()
            Me.XrTableCell16 = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableCell_expenses = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableCell18 = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableRow7 = New DevExpress.XtraReports.UI.XRTableRow()
            Me.XrTableCell19 = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableCell_available_income = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableCell21 = New DevExpress.XtraReports.UI.XRTableCell()
            Me.TopMargin = New DevExpress.XtraReports.UI.TopMarginBand()
            Me.BottomMargin = New DevExpress.XtraReports.UI.BottomMarginBand()
            Me.ReportHeader = New DevExpress.XtraReports.UI.ReportHeaderBand()
            Me.XrLabel2 = New DevExpress.XtraReports.UI.XRLabel()
            CType(Me.XrTable1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
            '
            'Detail
            '
            Me.Detail.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel1, Me.XrTable1})
            Me.Detail.HeightF = 200.0!
            Me.Detail.Name = "Detail"
            Me.Detail.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
            Me.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
            '
            'XrLabel1
            '
            Me.XrLabel1.Font = New System.Drawing.Font("Times New Roman", 14.0!, System.Drawing.FontStyle.Bold)
            Me.XrLabel1.ForeColor = System.Drawing.Color.Teal
            Me.XrLabel1.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
            Me.XrLabel1.Name = "XrLabel1"
            Me.XrLabel1.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel1.SizeF = New System.Drawing.SizeF(350.0!, 25.0!)
            Me.XrLabel1.StylePriority.UseFont = False
            Me.XrLabel1.StylePriority.UseForeColor = False
            Me.XrLabel1.StylePriority.UseTextAlignment = False
            Me.XrLabel1.Text = "BOTTOM   LINE"
            Me.XrLabel1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
            '
            'XrTable1
            '
            Me.XrTable1.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 25.0!)
            Me.XrTable1.Name = "XrTable1"
            Me.XrTable1.Rows.AddRange(New DevExpress.XtraReports.UI.XRTableRow() {Me.XrTableRow1, Me.XrTableRow3, Me.XrTableRow2, Me.XrTableRow4, Me.XrTableRow5, Me.XrTableRow6, Me.XrTableRow7})
            Me.XrTable1.SizeF = New System.Drawing.SizeF(350.0!, 175.0!)
            '
            'XrTableRow1
            '
            Me.XrTableRow1.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.XrTableCell1, Me.XrTableCell2, Me.XrTableCell3})
            Me.XrTableRow1.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrTableRow1.ForeColor = System.Drawing.Color.Teal
            Me.XrTableRow1.Name = "XrTableRow1"
            Me.XrTableRow1.StylePriority.UseFont = False
            Me.XrTableRow1.StylePriority.UseForeColor = False
            Me.XrTableRow1.Weight = 1.0R
            '
            'XrTableCell1
            '
            Me.XrTableCell1.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrTableCell1.ForeColor = System.Drawing.Color.Teal
            Me.XrTableCell1.Name = "XrTableCell1"
            Me.XrTableCell1.StylePriority.UseFont = False
            Me.XrTableCell1.StylePriority.UseForeColor = False
            Me.XrTableCell1.StylePriority.UseTextAlignment = False
            Me.XrTableCell1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            Me.XrTableCell1.Weight = 1.3571428353445871R
            '
            'XrTableCell2
            '
            Me.XrTableCell2.Name = "XrTableCell2"
            Me.XrTableCell2.StylePriority.UseTextAlignment = False
            Me.XrTableCell2.Text = "NET"
            Me.XrTableCell2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrTableCell2.Weight = 0.81785720825195307R
            '
            'XrTableCell3
            '
            Me.XrTableCell3.Name = "XrTableCell3"
            Me.XrTableCell3.StylePriority.UseTextAlignment = False
            Me.XrTableCell3.Text = "GROSS"
            Me.XrTableCell3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrTableCell3.Weight = 0.8249999564034598R
            '
            'XrTableRow3
            '
            Me.XrTableRow3.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.XrTableCell7, Me.XrTableCell_applicant_net, Me.XrTableCell_applicant_gross})
            Me.XrTableRow3.Name = "XrTableRow3"
            Me.XrTableRow3.Weight = 0.6R
            '
            'XrTableCell7
            '
            Me.XrTableCell7.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrTableCell7.ForeColor = System.Drawing.Color.Teal
            Me.XrTableCell7.Name = "XrTableCell7"
            Me.XrTableCell7.StylePriority.UseFont = False
            Me.XrTableCell7.StylePriority.UseForeColor = False
            Me.XrTableCell7.StylePriority.UseTextAlignment = False
            Me.XrTableCell7.Text = "APPLICANT"
            Me.XrTableCell7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            Me.XrTableCell7.Weight = 1.3571428353445871R
            '
            'XrTableCell_applicant_net
            '
            Me.XrTableCell_applicant_net.Name = "XrTableCell_applicant_net"
            Me.XrTableCell_applicant_net.StylePriority.UseTextAlignment = False
            Me.XrTableCell_applicant_net.Text = "$0.00"
            Me.XrTableCell_applicant_net.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrTableCell_applicant_net.Weight = 0.81785720825195307R
            '
            'XrTableCell_applicant_gross
            '
            Me.XrTableCell_applicant_gross.Name = "XrTableCell_applicant_gross"
            Me.XrTableCell_applicant_gross.StylePriority.UseTextAlignment = False
            Me.XrTableCell_applicant_gross.Text = "$0.00"
            Me.XrTableCell_applicant_gross.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrTableCell_applicant_gross.Weight = 0.82499995640345991R
            '
            'XrTableRow2
            '
            Me.XrTableRow2.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.XrTableCell4, Me.XrTableCell_coapplicant_net, Me.XrTableCell_coapplicant_gross})
            Me.XrTableRow2.Name = "XrTableRow2"
            Me.XrTableRow2.Weight = 0.59999999999999987R
            '
            'XrTableCell4
            '
            Me.XrTableCell4.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrTableCell4.ForeColor = System.Drawing.Color.Teal
            Me.XrTableCell4.Name = "XrTableCell4"
            Me.XrTableCell4.StylePriority.UseFont = False
            Me.XrTableCell4.StylePriority.UseForeColor = False
            Me.XrTableCell4.StylePriority.UseTextAlignment = False
            Me.XrTableCell4.Text = "CO-APPLICANT"
            Me.XrTableCell4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            Me.XrTableCell4.Weight = 1.3571428353445871R
            '
            'XrTableCell_coapplicant_net
            '
            Me.XrTableCell_coapplicant_net.Name = "XrTableCell_coapplicant_net"
            Me.XrTableCell_coapplicant_net.StylePriority.UseTextAlignment = False
            Me.XrTableCell_coapplicant_net.Text = "$0.00"
            Me.XrTableCell_coapplicant_net.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrTableCell_coapplicant_net.Weight = 0.81785720825195307R
            '
            'XrTableCell_coapplicant_gross
            '
            Me.XrTableCell_coapplicant_gross.Name = "XrTableCell_coapplicant_gross"
            Me.XrTableCell_coapplicant_gross.StylePriority.UseTextAlignment = False
            Me.XrTableCell_coapplicant_gross.Text = "$0.00"
            Me.XrTableCell_coapplicant_gross.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrTableCell_coapplicant_gross.Weight = 0.8249999564034598R
            '
            'XrTableRow4
            '
            Me.XrTableRow4.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.XrTableCell10, Me.XrTableCell_other_income, Me.XrTableCell11})
            Me.XrTableRow4.Name = "XrTableRow4"
            Me.XrTableRow4.Weight = 0.60000000000000009R
            '
            'XrTableCell10
            '
            Me.XrTableCell10.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrTableCell10.ForeColor = System.Drawing.Color.Teal
            Me.XrTableCell10.Name = "XrTableCell10"
            Me.XrTableCell10.StylePriority.UseFont = False
            Me.XrTableCell10.StylePriority.UseForeColor = False
            Me.XrTableCell10.StylePriority.UseTextAlignment = False
            Me.XrTableCell10.Text = "OTHER INCOME"
            Me.XrTableCell10.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            Me.XrTableCell10.Weight = 1.3571428353445871R
            '
            'XrTableCell_other_income
            '
            Me.XrTableCell_other_income.Name = "XrTableCell_other_income"
            Me.XrTableCell_other_income.StylePriority.UseTextAlignment = False
            Me.XrTableCell_other_income.Text = "$0.00"
            Me.XrTableCell_other_income.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrTableCell_other_income.Weight = 0.82142858232770644R
            '
            'XrTableCell11
            '
            Me.XrTableCell11.Name = "XrTableCell11"
            Me.XrTableCell11.StylePriority.UseTextAlignment = False
            Me.XrTableCell11.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrTableCell11.Weight = 0.82142858232770644R
            '
            'XrTableRow5
            '
            Me.XrTableRow5.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.XrTableCell13, Me.XrTableCell_total_net, Me.XrTableCell_total_gross})
            Me.XrTableRow5.Name = "XrTableRow5"
            Me.XrTableRow5.Weight = 1.4000000000000001R
            '
            'XrTableCell13
            '
            Me.XrTableCell13.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrTableCell13.ForeColor = System.Drawing.Color.Teal
            Me.XrTableCell13.Name = "XrTableCell13"
            Me.XrTableCell13.StylePriority.UseFont = False
            Me.XrTableCell13.StylePriority.UseForeColor = False
            Me.XrTableCell13.StylePriority.UseTextAlignment = False
            Me.XrTableCell13.Text = "TOTAL INCOME"
            Me.XrTableCell13.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            Me.XrTableCell13.Weight = 1.3571427045549664R
            '
            'XrTableCell_total_net
            '
            Me.XrTableCell_total_net.Borders = DevExpress.XtraPrinting.BorderSide.Top
            Me.XrTableCell_total_net.BorderWidth = 2
            Me.XrTableCell_total_net.Name = "XrTableCell_total_net"
            Me.XrTableCell_total_net.StylePriority.UseBorders = False
            Me.XrTableCell_total_net.StylePriority.UseBorderWidth = False
            Me.XrTableCell_total_net.StylePriority.UseTextAlignment = False
            Me.XrTableCell_total_net.Text = "$0.00"
            Me.XrTableCell_total_net.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrTableCell_total_net.Weight = 0.81785733904157376R
            '
            'XrTableCell_total_gross
            '
            Me.XrTableCell_total_gross.Borders = DevExpress.XtraPrinting.BorderSide.Top
            Me.XrTableCell_total_gross.BorderWidth = 2
            Me.XrTableCell_total_gross.Name = "XrTableCell_total_gross"
            Me.XrTableCell_total_gross.StylePriority.UseBorders = False
            Me.XrTableCell_total_gross.StylePriority.UseBorderWidth = False
            Me.XrTableCell_total_gross.StylePriority.UseTextAlignment = False
            Me.XrTableCell_total_gross.Text = "$0.00"
            Me.XrTableCell_total_gross.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrTableCell_total_gross.Weight = 0.8249999564034598R
            '
            'XrTableRow6
            '
            Me.XrTableRow6.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.XrTableCell16, Me.XrTableCell_expenses, Me.XrTableCell18})
            Me.XrTableRow6.Name = "XrTableRow6"
            Me.XrTableRow6.Weight = 1.8R
            '
            'XrTableCell16
            '
            Me.XrTableCell16.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrTableCell16.ForeColor = System.Drawing.Color.Teal
            Me.XrTableCell16.Name = "XrTableCell16"
            Me.XrTableCell16.StylePriority.UseFont = False
            Me.XrTableCell16.StylePriority.UseForeColor = False
            Me.XrTableCell16.StylePriority.UseTextAlignment = False
            Me.XrTableCell16.Text = "LESS EXPENSES"
            Me.XrTableCell16.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            Me.XrTableCell16.Weight = 1.357142573765346R
            '
            'XrTableCell_expenses
            '
            Me.XrTableCell_expenses.Name = "XrTableCell_expenses"
            Me.XrTableCell_expenses.StylePriority.UseTextAlignment = False
            Me.XrTableCell_expenses.Text = "$0.00"
            Me.XrTableCell_expenses.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrTableCell_expenses.Weight = 0.81785746983119423R
            '
            'XrTableCell18
            '
            Me.XrTableCell18.Name = "XrTableCell18"
            Me.XrTableCell18.StylePriority.UseTextAlignment = False
            Me.XrTableCell18.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrTableCell18.Weight = 0.8249999564034598R
            '
            'XrTableRow7
            '
            Me.XrTableRow7.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.XrTableCell19, Me.XrTableCell_available_income, Me.XrTableCell21})
            Me.XrTableRow7.Name = "XrTableRow7"
            Me.XrTableRow7.Weight = 1.0R
            '
            'XrTableCell19
            '
            Me.XrTableCell19.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrTableCell19.ForeColor = System.Drawing.Color.Teal
            Me.XrTableCell19.Name = "XrTableCell19"
            Me.XrTableCell19.StylePriority.UseFont = False
            Me.XrTableCell19.StylePriority.UseForeColor = False
            Me.XrTableCell19.StylePriority.UseTextAlignment = False
            Me.XrTableCell19.Text = "AVAILABLE INCOME"
            Me.XrTableCell19.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            Me.XrTableCell19.Weight = 1.3571424429757255R
            '
            'XrTableCell_available_income
            '
            Me.XrTableCell_available_income.Borders = DevExpress.XtraPrinting.BorderSide.Top
            Me.XrTableCell_available_income.BorderWidth = 2
            Me.XrTableCell_available_income.Name = "XrTableCell_available_income"
            Me.XrTableCell_available_income.StylePriority.UseBorders = False
            Me.XrTableCell_available_income.StylePriority.UseBorderWidth = False
            Me.XrTableCell_available_income.StylePriority.UseTextAlignment = False
            Me.XrTableCell_available_income.Text = "$0.00"
            Me.XrTableCell_available_income.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrTableCell_available_income.Weight = 0.8178576006208147R
            '
            'XrTableCell21
            '
            Me.XrTableCell21.Name = "XrTableCell21"
            Me.XrTableCell21.StylePriority.UseTextAlignment = False
            Me.XrTableCell21.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrTableCell21.Weight = 0.8249999564034598R
            '
            'TopMargin
            '
            Me.TopMargin.HeightF = 0.0!
            Me.TopMargin.Name = "TopMargin"
            Me.TopMargin.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
            Me.TopMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
            '
            'BottomMargin
            '
            Me.BottomMargin.HeightF = 0.0!
            Me.BottomMargin.Name = "BottomMargin"
            Me.BottomMargin.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
            Me.BottomMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
            '
            'ReportHeader
            '
            Me.ReportHeader.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel2})
            Me.ReportHeader.HeightF = 51.04167!
            Me.ReportHeader.Name = "ReportHeader"
            '
            'XrLabel2
            '
            Me.XrLabel2.LocationFloat = New DevExpress.Utils.PointFloat(10.00001!, 10.00001!)
            Me.XrLabel2.Name = "XrLabel2"
            Me.XrLabel2.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel2.SizeF = New System.Drawing.SizeF(100.0!, 23.0!)
            '
            'BottomLine
            '
            Me.Bands.AddRange(New DevExpress.XtraReports.UI.Band() {Me.Detail, Me.TopMargin, Me.BottomMargin, Me.ReportHeader})
            Me.Margins = New System.Drawing.Printing.Margins(0, 0, 0, 0)
            Me.ScriptLanguage = DevExpress.XtraReports.ScriptLanguage.VisualBasic
            Me.ScriptReferencesString = resources.GetString("$this.ScriptReferencesString")
            Me.Scripts.OnBeforePrint = "BottomLine_BeforePrint"
            Me.ScriptsSource = resources.GetString("$this.ScriptsSource")
            Me.Version = "11.2"
            CType(Me.XrTable1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me, System.ComponentModel.ISupportInitialize).EndInit()

        End Sub
        Friend WithEvents Detail As DevExpress.XtraReports.UI.DetailBand
        Friend WithEvents TopMargin As DevExpress.XtraReports.UI.TopMarginBand
        Friend WithEvents BottomMargin As DevExpress.XtraReports.UI.BottomMarginBand
        Friend WithEvents XrLabel1 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrTable1 As DevExpress.XtraReports.UI.XRTable
        Friend WithEvents XrTableRow1 As DevExpress.XtraReports.UI.XRTableRow
        Friend WithEvents XrTableCell1 As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents XrTableCell2 As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents XrTableCell3 As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents XrTableRow3 As DevExpress.XtraReports.UI.XRTableRow
        Friend WithEvents XrTableCell7 As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents XrTableCell_applicant_net As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents XrTableCell_applicant_gross As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents XrTableRow2 As DevExpress.XtraReports.UI.XRTableRow
        Friend WithEvents XrTableCell4 As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents XrTableCell_coapplicant_net As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents XrTableCell_coapplicant_gross As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents XrTableRow4 As DevExpress.XtraReports.UI.XRTableRow
        Friend WithEvents XrTableCell10 As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents XrTableCell11 As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents XrTableRow5 As DevExpress.XtraReports.UI.XRTableRow
        Friend WithEvents XrTableCell13 As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents XrTableCell_total_net As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents XrTableCell_total_gross As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents XrTableRow6 As DevExpress.XtraReports.UI.XRTableRow
        Friend WithEvents XrTableCell16 As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents XrTableCell_expenses As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents XrTableCell18 As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents XrTableRow7 As DevExpress.XtraReports.UI.XRTableRow
        Friend WithEvents XrTableCell19 As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents XrTableCell_available_income As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents XrTableCell21 As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents XrTableCell_other_income As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents ReportHeader As DevExpress.XtraReports.UI.ReportHeaderBand
        Friend WithEvents XrLabel2 As DevExpress.XtraReports.UI.XRLabel
    End Class
End Namespace
