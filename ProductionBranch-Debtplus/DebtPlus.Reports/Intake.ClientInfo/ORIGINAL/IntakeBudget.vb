#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}
#End Region

Namespace Intake.ClientInfo
    Public Class IntakeBudget

        Public Sub New(ByVal Container As System.ComponentModel.IContainer)
            MyClass.New()
            Container.Add(Me)
        End Sub

        Public Sub New()
            MyBase.New()
            InitializeComponent()
            AddHandler BeforePrint, AddressOf IntakeBudget_BeforePrint
            AddHandler lbl_description.BeforePrint, AddressOf lbl_description_BeforePrint
        End Sub

        Private ReadOnly ds As New System.Data.DataSet("ds")
        Private Sub IntakeBudget_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs)
            Const TableName As String = "intake_budget"
            Dim rpt As DevExpress.XtraReports.UI.XtraReport = CType(sender, DevExpress.XtraReports.UI.XtraReport)
            Dim MasterRpt As DevExpress.XtraReports.UI.XtraReport = CType(rpt.MasterReport, DevExpress.XtraReports.UI.XtraReport)
            Dim sqlInfo As DebtPlus.LINQ.SQLInfoClass = DebtPlus.LINQ.SQLInfoClass.getDefault()
            Dim IntakeClient As Int32 = CType(MasterRpt.Parameters("ParameterIntakeClient").Value, System.Int32)

            Dim current_cursor As System.Windows.Forms.Cursor = System.Windows.Forms.Cursor.Current
            Try
                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.WaitCursor
                Using cmd As New System.Data.SqlClient.SqlCommand()
                    With cmd
                        .Connection = New System.Data.SqlClient.SqlConnection(sqlInfo.ConnectionString)
                        .CommandText = "rpt_intake_budget_list"
                        .CommandType = System.Data.CommandType.StoredProcedure
                        .Parameters.Add("@intake_client", System.Data.SqlDbType.Int).Value = IntakeClient
                        .CommandTimeout = 0
                    End With

                    Using da As New System.Data.SqlClient.SqlDataAdapter(cmd)
                        da.Fill(ds, TableName)
                    End Using
                End Using
                rpt.DataSource = ds.Tables(TableName).DefaultView

            Catch ex As System.Data.SqlClient.SqlException
                Dim gdr As New DebtPlus.Repository.GetDataResult() : gdr.HandleException(ex) : DebtPlus.UI.Common.ErrorHandling.HandleErrors(gdr, "Error reading intake_assets")

            Finally
                System.Windows.Forms.Cursor.Current = current_cursor
            End Try
        End Sub

        Private Sub lbl_description_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs)
            With CType(sender, DevExpress.XtraReports.UI.XRLabel)
                Dim rpt As DevExpress.XtraReports.UI.XtraReport = CType(.Report, DevExpress.XtraReports.UI.XtraReport)
                Dim isHeader As Boolean = False
                If rpt.GetCurrentColumnValue("heading") IsNot Nothing AndAlso rpt.GetCurrentColumnValue("heading") IsNot System.DBNull.Value Then
                    isHeader = Convert.ToBoolean(rpt.GetCurrentColumnValue("heading"))
                End If

                If isHeader Then
                    .Font = New System.Drawing.Font(.Font, FontStyle.Bold)
                    .ForeColor = Color.White
                    .BackColor = Color.Red
                Else
                    .Font = New System.Drawing.Font(.Font, FontStyle.Regular)
                    .ForeColor = Color.FromKnownColor(KnownColor.WindowText)
                    .BackColor = Color.Transparent
                End If
            End With
        End Sub
    End Class
End Namespace
