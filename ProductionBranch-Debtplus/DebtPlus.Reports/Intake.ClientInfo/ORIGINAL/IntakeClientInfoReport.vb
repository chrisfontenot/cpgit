#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}
#End Region

Option Compare Binary
Option Explicit On 
Option Strict On

Namespace Intake.ClientInfo
    Public Class IntakeClientInfoReport
        Implements DebtPlus.Interfaces.Client.IClient

        Public Sub New()
            MyBase.New()
            InitializeComponent()

            Parameter_Method = -1

            AddHandler BeforePrint, AddressOf IntakeClientInfoReport_BeforePrint
            AddHandler lbl_fed_tax_owed.BeforePrint, AddressOf lbl_fed_tax_owed_BeforePrint
            AddHandler lbl_local_tax_owed.BeforePrint, AddressOf lbl_local_tax_owed_BeforePrint
            AddHandler lbl_state_tax_owed.BeforePrint, AddressOf lbl_state_tax_owed_BeforePrint
        End Sub

        ''' <summary>
        ''' ID string associated with the client selection
        ''' </summary>
        Public Property Parameter_ID() As String
            Get
                Return CType(Parameters("ParameterClient").Value, System.String)
            End Get
            Set(ByVal Value As String)
                Parameters("ParameterID").Value = Value
                Parameter_Method = 0
            End Set
        End Property

        ''' <summary>
        ''' A simple property if we already have the client as a number
        ''' </summary>
        Public Property Parameter_IntakeClient() As System.Int32
            Get
                Return CType(Parameters("ParameterIntakeClient").Value, System.Int32)
            End Get

            Set(ByVal Value As System.Int32)
                Parameters("ParameterIntakeClient").Value = Value
                Parameter_Method = 1
            End Set
        End Property

        Public Property ClientID As Int32 Implements DebtPlus.Interfaces.Client.IClient.ClientId
            Get
                Return Parameter_Client
            End Get
            Set(value As Int32)
                Parameter_Client = value
            End Set
        End Property

        ''' <summary>
        ''' A simple property if we already have the client as a number
        ''' </summary>
        Public Property Parameter_Client() As System.Int32
            Get
                Return CType(Parameters("ParameterClient").Value, System.Int32)
            End Get

            Set(ByVal Value As System.Int32)
                Parameters("ParameterClient").Value = Value
                Parameter_Method = 2
            End Set
        End Property

        ''' <summary>
        ''' Metod selecting the client id
        ''' </summary>
        Public Property Parameter_Method() As System.Int32
            Get
                Return CType(Parameters("ParameterMethod").Value, System.Int32)
            End Get
            Set(ByVal Value As System.Int32)
                Parameters("ParameterMethod").Value = Value
            End Set
        End Property

        ''' <summary>
        ''' Set a parameter value
        ''' </summary>
        Public Overrides Sub SetReportParameter(ByVal Parameter As String, ByVal Value As Object)
            Select Case Parameter
                Case "IntakeClient" : Parameter_IntakeClient = Convert.ToInt32(Value)
                Case "Client" : Parameter_Client = Convert.ToInt32(Value)
                Case "Method" : Parameter_Method = Convert.ToInt32(Value)
                Case "ID" : Parameter_ID = Convert.ToString(Value)
                Case Else : MyBase.SetReportParameter(Parameter, Value)
            End Select
        End Sub

        ''' <summary>
        ''' Title for the report
        ''' </summary>
        Public Overrides ReadOnly Property ReportTitle() As String
            Get
                Return "Intake Client"
            End Get
        End Property

        Public Overrides Function NeedParameters() As Boolean
            Return Parameter_Method < 0
        End Function

        ''' <summary>
        ''' Request the parameters from the user
        ''' </summary>
        Public Overrides Function RequestReportParameters() As System.Windows.Forms.DialogResult
            Dim Answer As System.Windows.Forms.DialogResult = DialogResult.OK

            If NeedParameters() Then
                Using frm As New DebtPlus.Reports.Intake.ClientInfo.IntakeClientInfoParametersForm()
                    With frm
                        Answer = .ShowDialog
                        If .Parameter_Method = 1 Then
                            Parameter_IntakeClient = Convert.ToInt32(.Parameter_ID)
                        Else
                            Parameter_ID = .Parameter_ID
                        End If
                    End With
                End Using
            End If

            Return Answer
        End Function

        Private ReadOnly ds As New System.Data.DataSet("ds")


        ''' <summary>
        ''' Reload the data for the report
        ''' </summary>
        Private Sub IntakeClientInfoReport_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs)
            Dim rpt As DevExpress.XtraReports.UI.XtraReport = CType(sender, DevExpress.XtraReports.UI.XtraReport)
            Dim sqlInfo As DebtPlus.LINQ.SQLInfoClass = DebtPlus.LINQ.SQLInfoClass.getDefault()

            Const TableName As String = "intake_clients"
            Dim tbl As System.Data.DataTable = ds.Tables(TableName)
            If tbl IsNot Nothing Then tbl.Clear()

            Dim cn As System.Data.SqlClient.SqlConnection = New System.Data.SqlClient.SqlConnection(sqlInfo.ConnectionString)
            Dim current_cursor As System.Windows.Forms.Cursor = System.Windows.Forms.Cursor.Current
            Try
                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.WaitCursor
                Using cmd As New System.Data.SqlClient.SqlCommand()
                    With cmd
                        Dim CommandText As String = "SELECT TOP 1 * FROM view_intake_clients WITH (NOLOCK)"
                        Select Case CType(rpt.Parameters("ParameterMethod").Value, System.Int32)
                            Case 0
                                CommandText += " WHERE [intake_id]=@intake_id"
                                .Parameters.Add("@intake_id", System.Data.SqlDbType.Text, 80).Value = rpt.Parameters("ParameterID").Value
                            Case 1
                                CommandText += " WHERE [intake_client]=@intake_client"
                                .Parameters.Add("@intake_client", System.Data.SqlDbType.Int).Value = rpt.Parameters("ParameterIntakeClient").Value
                            Case 2
                                CommandText += " WHERE [client]=@client"
                                .Parameters.Add("@client", System.Data.SqlDbType.Int).Value = rpt.Parameters("ParameterClient").Value
                            Case Else
                        End Select

                        .Connection = cn
                        .CommandText = CommandText
                        .CommandTimeout = 0
                    End With

                    Using da As New System.Data.SqlClient.SqlDataAdapter(cmd)
                        da.Fill(ds, TableName)
                    End Using
                End Using

                Dim vue As System.Data.DataView = ds.Tables(TableName).DefaultView
                rpt.DataSource = vue

                '-- Find the intake client so that the other sub-reports will work properly
                If vue.Count > 0 Then
                    rpt.Parameters("ParameterIntakeClient").Value = vue(0)("intake_client")
                End If

            Catch ex As System.Data.SqlClient.SqlException
                Dim gdr As New DebtPlus.Repository.GetDataResult() : gdr.HandleException(ex) : DebtPlus.UI.Common.ErrorHandling.HandleErrors(gdr, "Error reading intake clients report")

            Finally
                System.Windows.Forms.Cursor.Current = current_cursor
            End Try
        End Sub


        ''' <summary>
        ''' Format the tax lines with the amount and period if indicated
        ''' </summary>
        Private Sub lbl_fed_tax_owed_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs)
            With CType(sender, DevExpress.XtraReports.UI.XRLabel)
                Dim rpt As DevExpress.XtraReports.UI.XtraReport = CType(.Report, DevExpress.XtraReports.UI.XtraReport)
                .Text = FormatTaxes(rpt.GetCurrentColumnValue("fed_tax_owed"), rpt.GetCurrentColumnValue("fed_tax_months"))
            End With
        End Sub

        Private Sub lbl_state_tax_owed_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs)
            With CType(sender, DevExpress.XtraReports.UI.XRLabel)
                Dim rpt As DevExpress.XtraReports.UI.XtraReport = CType(.Report, DevExpress.XtraReports.UI.XtraReport)
                .Text = FormatTaxes(rpt.GetCurrentColumnValue("state_tax_owed"), rpt.GetCurrentColumnValue("state_tax_months"))
            End With
        End Sub

        Private Sub lbl_local_tax_owed_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs)
            With CType(sender, DevExpress.XtraReports.UI.XRLabel)
                Dim rpt As DevExpress.XtraReports.UI.XtraReport = CType(.Report, DevExpress.XtraReports.UI.XtraReport)
                .Text = FormatTaxes(rpt.GetCurrentColumnValue("local_tax_owed"), rpt.GetCurrentColumnValue("local_tax_months"))
            End With
        End Sub

        Private Function FormatTaxes(ByVal Amount As Object, ByVal Months As Object) As String
            Dim Answer As New System.Text.StringBuilder

            If Amount Is Nothing OrElse Amount Is System.DBNull.Value Then Amount = 0D
            Answer.AppendFormat("{0:c}", Convert.ToDecimal(Amount))
            If Months IsNot Nothing AndAlso Months IsNot System.DBNull.Value Then
                If Convert.ToInt32(Months) > 0 Then Answer.AppendFormat(" for {0:n0} months", Convert.ToInt32(Months))
            End If

            Return Answer.ToString
        End Function
    End Class
End Namespace
