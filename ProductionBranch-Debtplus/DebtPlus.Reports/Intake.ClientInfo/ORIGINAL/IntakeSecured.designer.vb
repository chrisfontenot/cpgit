﻿Namespace Intake.ClientInfo
    <Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
    Partial Class IntakeSecuredReport
        Inherits DevExpress.XtraReports.UI.XtraReport

        'Component overrides dispose to clean up the component list.
        Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
            If disposing Then
                If components IsNot Nothing Then
                    components.Dispose()
                End If
            End If
            MyBase.Dispose(disposing)
        End Sub

        'Required by the Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Designer
        'It can be modified using the Designer.
        'Do not modify it using the code editor.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
            Dim XrSummary1 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary
            Dim XrSummary2 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary
            Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(IntakeSecuredReport))
            Me.Detail = New DevExpress.XtraReports.UI.DetailBand
            Me.XrTable2 = New DevExpress.XtraReports.UI.XRTable
            Me.XrTableRow2 = New DevExpress.XtraReports.UI.XRTableRow
            Me.lbl_type = New DevExpress.XtraReports.UI.XRTableCell
            Me.lbl_value = New DevExpress.XtraReports.UI.XRTableCell
            Me.lbl_loans = New DevExpress.XtraReports.UI.XRTableCell
            Me.PageHeader = New DevExpress.XtraReports.UI.PageHeaderBand
            Me.XrLabel1 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrPanel1 = New DevExpress.XtraReports.UI.XRPanel
            Me.XrTable1 = New DevExpress.XtraReports.UI.XRTable
            Me.XrTableRow1 = New DevExpress.XtraReports.UI.XRTableRow
            Me.XrTableCell1 = New DevExpress.XtraReports.UI.XRTableCell
            Me.XrTableCell2 = New DevExpress.XtraReports.UI.XRTableCell
            Me.XrTableCell3 = New DevExpress.XtraReports.UI.XRTableCell
            Me.ReportFooter = New DevExpress.XtraReports.UI.ReportFooterBand
            Me.XrTable3 = New DevExpress.XtraReports.UI.XRTable
            Me.XrTableRow3 = New DevExpress.XtraReports.UI.XRTableRow
            Me.XrTableCell7 = New DevExpress.XtraReports.UI.XRTableCell
            Me.lbl_total_value = New DevExpress.XtraReports.UI.XRTableCell
            Me.lbl_total_loans = New DevExpress.XtraReports.UI.XRTableCell
            Me.TopMarginBand1 = New DevExpress.XtraReports.UI.TopMarginBand
            Me.BottomMarginBand1 = New DevExpress.XtraReports.UI.BottomMarginBand
            CType(Me.XrTable2, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.XrTable1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.XrTable3, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
            '
            'Detail
            '
            Me.Detail.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrTable2})
            Me.Detail.HeightF = 17.0!
            Me.Detail.Name = "Detail"
            Me.Detail.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
            Me.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
            '
            'XrTable2
            '
            Me.XrTable2.LocationFloat = New DevExpress.Utils.PointFloat(8.0!, 0.0!)
            Me.XrTable2.Name = "XrTable2"
            Me.XrTable2.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
            Me.XrTable2.Rows.AddRange(New DevExpress.XtraReports.UI.XRTableRow() {Me.XrTableRow2})
            Me.XrTable2.Scripts.OnBeforePrint = "XrTable2_BeforePrint"
            Me.XrTable2.SizeF = New System.Drawing.SizeF(500.0!, 17.0!)
            Me.XrTable2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
            '
            'XrTableRow2
            '
            Me.XrTableRow2.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.lbl_type, Me.lbl_value, Me.lbl_loans})
            Me.XrTableRow2.Name = "XrTableRow2"
            Me.XrTableRow2.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
            Me.XrTableRow2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
            Me.XrTableRow2.Weight = 1
            '
            'lbl_type
            '
            Me.lbl_type.Name = "lbl_type"
            Me.lbl_type.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.lbl_type.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            Me.lbl_type.Weight = 0.334
            '
            'lbl_value
            '
            Me.lbl_value.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "value", "{0:c}")})
            Me.lbl_value.Name = "lbl_value"
            Me.lbl_value.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.lbl_value.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.lbl_value.Weight = 0.334
            '
            'lbl_loans
            '
            Me.lbl_loans.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "balance", "{0:c2}")})
            Me.lbl_loans.Name = "lbl_loans"
            Me.lbl_loans.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.lbl_loans.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.lbl_loans.Weight = 0.332
            '
            'PageHeader
            '
            Me.PageHeader.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel1, Me.XrPanel1})
            Me.PageHeader.HeightF = 59.0!
            Me.PageHeader.Name = "PageHeader"
            Me.PageHeader.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
            Me.PageHeader.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
            '
            'XrLabel1
            '
            Me.XrLabel1.Font = New System.Drawing.Font("Times New Roman", 14.0!, System.Drawing.FontStyle.Bold)
            Me.XrLabel1.ForeColor = System.Drawing.Color.Teal
            Me.XrLabel1.LocationFloat = New DevExpress.Utils.PointFloat(17.0!, 8.0!)
            Me.XrLabel1.Name = "XrLabel1"
            Me.XrLabel1.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel1.SizeF = New System.Drawing.SizeF(575.0!, 25.0!)
            Me.XrLabel1.Text = "SECURED INFORMATION"
            Me.XrLabel1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
            '
            'XrPanel1
            '
            Me.XrPanel1.BackColor = System.Drawing.Color.Teal
            Me.XrPanel1.BorderColor = System.Drawing.Color.Teal
            Me.XrPanel1.CanGrow = False
            Me.XrPanel1.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrTable1})
            Me.XrPanel1.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 42.0!)
            Me.XrPanel1.Name = "XrPanel1"
            Me.XrPanel1.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
            Me.XrPanel1.SizeF = New System.Drawing.SizeF(517.0!, 16.0!)
            '
            'XrTable1
            '
            Me.XrTable1.AnchorVertical = DevExpress.XtraReports.UI.VerticalAnchorStyles.Bottom
            Me.XrTable1.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrTable1.ForeColor = System.Drawing.Color.White
            Me.XrTable1.LocationFloat = New DevExpress.Utils.PointFloat(8.0!, 0.0!)
            Me.XrTable1.Name = "XrTable1"
            Me.XrTable1.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
            Me.XrTable1.Rows.AddRange(New DevExpress.XtraReports.UI.XRTableRow() {Me.XrTableRow1})
            Me.XrTable1.SizeF = New System.Drawing.SizeF(500.0!, 16.0!)
            Me.XrTable1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
            '
            'XrTableRow1
            '
            Me.XrTableRow1.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.XrTableCell1, Me.XrTableCell2, Me.XrTableCell3})
            Me.XrTableRow1.Name = "XrTableRow1"
            Me.XrTableRow1.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
            Me.XrTableRow1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
            Me.XrTableRow1.Weight = 1
            '
            'XrTableCell1
            '
            Me.XrTableCell1.CanGrow = False
            Me.XrTableCell1.Name = "XrTableCell1"
            Me.XrTableCell1.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrTableCell1.Text = "TYPE"
            Me.XrTableCell1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            Me.XrTableCell1.Weight = 0.334
            '
            'XrTableCell2
            '
            Me.XrTableCell2.CanGrow = False
            Me.XrTableCell2.Name = "XrTableCell2"
            Me.XrTableCell2.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrTableCell2.Text = "VALUE"
            Me.XrTableCell2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrTableCell2.Weight = 0.334
            '
            'XrTableCell3
            '
            Me.XrTableCell3.CanGrow = False
            Me.XrTableCell3.Name = "XrTableCell3"
            Me.XrTableCell3.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrTableCell3.Text = "LOANS"
            Me.XrTableCell3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrTableCell3.Weight = 0.332
            '
            'ReportFooter
            '
            Me.ReportFooter.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrTable3})
            Me.ReportFooter.HeightF = 30.0!
            Me.ReportFooter.Name = "ReportFooter"
            Me.ReportFooter.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
            Me.ReportFooter.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
            '
            'XrTable3
            '
            Me.XrTable3.AnchorVertical = DevExpress.XtraReports.UI.VerticalAnchorStyles.Top
            Me.XrTable3.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrTable3.LocationFloat = New DevExpress.Utils.PointFloat(8.0!, 0.0!)
            Me.XrTable3.Name = "XrTable3"
            Me.XrTable3.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
            Me.XrTable3.Rows.AddRange(New DevExpress.XtraReports.UI.XRTableRow() {Me.XrTableRow3})
            Me.XrTable3.SizeF = New System.Drawing.SizeF(500.0!, 25.0!)
            Me.XrTable3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
            '
            'XrTableRow3
            '
            Me.XrTableRow3.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.XrTableCell7, Me.lbl_total_value, Me.lbl_total_loans})
            Me.XrTableRow3.Name = "XrTableRow3"
            Me.XrTableRow3.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
            Me.XrTableRow3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
            Me.XrTableRow3.Weight = 1
            '
            'XrTableCell7
            '
            Me.XrTableCell7.ForeColor = System.Drawing.Color.Teal
            Me.XrTableCell7.Name = "XrTableCell7"
            Me.XrTableCell7.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrTableCell7.Text = "TOTALS"
            Me.XrTableCell7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            Me.XrTableCell7.Weight = 0.334
            '
            'lbl_total_value
            '
            Me.lbl_total_value.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "value")})
            Me.lbl_total_value.ForeColor = System.Drawing.Color.Teal
            Me.lbl_total_value.Name = "lbl_total_value"
            Me.lbl_total_value.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.lbl_total_value.Scripts.OnBeforePrint = "lbl_total_value_BeforePrint"
            XrSummary1.FormatString = "{0:c}"
            XrSummary1.Running = DevExpress.XtraReports.UI.SummaryRunning.Report
            Me.lbl_total_value.Summary = XrSummary1
            Me.lbl_total_value.Text = "lbl_total_value"
            Me.lbl_total_value.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.lbl_total_value.Weight = 0.334
            '
            'lbl_total_loans
            '
            Me.lbl_total_loans.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "balance")})
            Me.lbl_total_loans.ForeColor = System.Drawing.Color.Teal
            Me.lbl_total_loans.Name = "lbl_total_loans"
            Me.lbl_total_loans.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            XrSummary2.FormatString = "{0:c}"
            XrSummary2.Running = DevExpress.XtraReports.UI.SummaryRunning.Report
            Me.lbl_total_loans.Summary = XrSummary2
            Me.lbl_total_loans.Text = "lbl_total_loans"
            Me.lbl_total_loans.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.lbl_total_loans.Weight = 0.332
            '
            'TopMarginBand1
            '
            Me.TopMarginBand1.HeightF = 25.0!
            Me.TopMarginBand1.Name = "TopMarginBand1"
            '
            'BottomMarginBand1
            '
            Me.BottomMarginBand1.HeightF = 25.0!
            Me.BottomMarginBand1.Name = "BottomMarginBand1"
            '
            'IntakeSecuredReport
            '
            Me.Bands.AddRange(New DevExpress.XtraReports.UI.Band() {Me.Detail, Me.PageHeader, Me.ReportFooter, Me.TopMarginBand1, Me.BottomMarginBand1})
            Me.Margins = New System.Drawing.Printing.Margins(25, 25, 25, 25)
            Me.ReportPrintOptions.PrintOnEmptyDataSource = False
            Me.ScriptLanguage = DevExpress.XtraReports.ScriptLanguage.VisualBasic
            Me.ScriptReferencesString = resources.GetString("$this.ScriptReferencesString")
            Me.Scripts.OnBeforePrint = "IntakeSecuredReport_BeforePrint"
            Me.ScriptsSource = resources.GetString("$this.ScriptsSource")
            Me.Version = "11.1"
            CType(Me.XrTable2, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.XrTable1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.XrTable3, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me, System.ComponentModel.ISupportInitialize).EndInit()

        End Sub

        Friend WithEvents Detail As DevExpress.XtraReports.UI.DetailBand
        Private WithEvents PageHeader As DevExpress.XtraReports.UI.PageHeaderBand
        Friend WithEvents ReportFooter As DevExpress.XtraReports.UI.ReportFooterBand
        Friend WithEvents XrTable2 As DevExpress.XtraReports.UI.XRTable
        Friend WithEvents XrTableRow2 As DevExpress.XtraReports.UI.XRTableRow
        Friend WithEvents XrTable3 As DevExpress.XtraReports.UI.XRTable
        Friend WithEvents XrTableRow3 As DevExpress.XtraReports.UI.XRTableRow
        Friend WithEvents XrTableCell7 As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents lbl_type As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents lbl_value As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents lbl_loans As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents lbl_total_value As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents lbl_total_loans As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents XrPanel1 As DevExpress.XtraReports.UI.XRPanel
        Friend WithEvents XrTable1 As DevExpress.XtraReports.UI.XRTable
        Friend WithEvents XrTableRow1 As DevExpress.XtraReports.UI.XRTableRow
        Friend WithEvents XrTableCell1 As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents XrTableCell2 As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents XrTableCell3 As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents XrLabel1 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents TopMarginBand1 As DevExpress.XtraReports.UI.TopMarginBand
        Friend WithEvents BottomMarginBand1 As DevExpress.XtraReports.UI.BottomMarginBand
    End Class
End Namespace
