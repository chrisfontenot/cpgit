Namespace Intake.ClientInfo
    <Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
    Partial Public Class IntakeClient
        Inherits DevExpress.XtraReports.UI.XtraReport

        'XtraReport overrides dispose to clean up the component list.
        <System.Diagnostics.DebuggerNonUserCode()> _
        Protected Overrides Sub Dispose(ByVal disposing As Boolean)
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
            MyBase.Dispose(disposing)
        End Sub

        'Required by the Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Designer
        'It can be modified using the Designer.  
        'Do not modify it using the code editor.
        <System.Diagnostics.DebuggerStepThrough()> _
        Private Sub InitializeComponent()
            Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(IntakeClient))
            Me.Detail = New DevExpress.XtraReports.UI.DetailBand()
            Me.lbl_bankruptcy = New DevExpress.XtraReports.UI.XRLabel()
            Me.lbl_people = New DevExpress.XtraReports.UI.XRLabel()
            Me.lbl_status = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel1 = New DevExpress.XtraReports.UI.XRLabel()
            Me.lbl_intake_client = New DevExpress.XtraReports.UI.XRLabel()
            Me.lbl_housing_type = New DevExpress.XtraReports.UI.XRLabel()
            Me.lbl_marital_status = New DevExpress.XtraReports.UI.XRLabel()
            Me.lbl_message_ph = New DevExpress.XtraReports.UI.XRLabel()
            Me.lbl_home_address = New DevExpress.XtraReports.UI.XRLabel()
            Me.lbl_home_ph = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel4 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel5 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel3 = New DevExpress.XtraReports.UI.XRLabel()
            Me.lbl_intake_id = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel2 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel9 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel10 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel8 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel6 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel7 = New DevExpress.XtraReports.UI.XRLabel()
            Me.TopMargin = New DevExpress.XtraReports.UI.TopMarginBand()
            Me.BottomMargin = New DevExpress.XtraReports.UI.BottomMarginBand()
            CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
            '
            'Detail
            '
            Me.Detail.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.lbl_bankruptcy, Me.lbl_people, Me.lbl_status, Me.XrLabel1, Me.lbl_intake_client, Me.lbl_housing_type, Me.lbl_marital_status, Me.lbl_message_ph, Me.lbl_home_address, Me.lbl_home_ph, Me.XrLabel4, Me.XrLabel5, Me.XrLabel3, Me.lbl_intake_id, Me.XrLabel2, Me.XrLabel9, Me.XrLabel10, Me.XrLabel8, Me.XrLabel6, Me.XrLabel7})
            Me.Detail.HeightF = 211.125!
            Me.Detail.Name = "Detail"
            Me.Detail.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
            Me.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
            '
            'lbl_bankruptcy
            '
            Me.lbl_bankruptcy.BorderWidth = 0
            Me.lbl_bankruptcy.CanGrow = False
            Me.lbl_bankruptcy.LocationFloat = New DevExpress.Utils.PointFloat(200.0!, 67.00001!)
            Me.lbl_bankruptcy.Name = "lbl_bankruptcy"
            Me.lbl_bankruptcy.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.lbl_bankruptcy.SizeF = New System.Drawing.SizeF(233.0!, 16.0!)
            '
            'lbl_people
            '
            Me.lbl_people.BorderWidth = 0
            Me.lbl_people.CanGrow = False
            Me.lbl_people.LocationFloat = New DevExpress.Utils.PointFloat(200.0!, 97.50001!)
            Me.lbl_people.Name = "lbl_people"
            Me.lbl_people.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.lbl_people.SizeF = New System.Drawing.SizeF(233.0!, 16.0!)
            '
            'lbl_status
            '
            Me.lbl_status.BorderWidth = 0
            Me.lbl_status.CanGrow = False
            Me.lbl_status.LocationFloat = New DevExpress.Utils.PointFloat(200.0!, 51.00001!)
            Me.lbl_status.Name = "lbl_status"
            Me.lbl_status.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.lbl_status.SizeF = New System.Drawing.SizeF(233.0!, 16.0!)
            '
            'XrLabel1
            '
            Me.XrLabel1.BorderWidth = 0
            Me.XrLabel1.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel1.LocationFloat = New DevExpress.Utils.PointFloat(21.875!, 5.0!)
            Me.XrLabel1.Name = "XrLabel1"
            Me.XrLabel1.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel1.SizeF = New System.Drawing.SizeF(100.0!, 16.0!)
            Me.XrLabel1.Text = "Intake ID"
            '
            'lbl_intake_client
            '
            Me.lbl_intake_client.BorderWidth = 0
            Me.lbl_intake_client.CanGrow = False
            Me.lbl_intake_client.LocationFloat = New DevExpress.Utils.PointFloat(200.0!, 35.00001!)
            Me.lbl_intake_client.Name = "lbl_intake_client"
            Me.lbl_intake_client.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.lbl_intake_client.SizeF = New System.Drawing.SizeF(233.0!, 16.0!)
            '
            'lbl_housing_type
            '
            Me.lbl_housing_type.BorderWidth = 0
            Me.lbl_housing_type.CanGrow = False
            Me.lbl_housing_type.LocationFloat = New DevExpress.Utils.PointFloat(200.0!, 179.125!)
            Me.lbl_housing_type.Name = "lbl_housing_type"
            Me.lbl_housing_type.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.lbl_housing_type.SizeF = New System.Drawing.SizeF(233.0!, 16.0!)
            '
            'lbl_marital_status
            '
            Me.lbl_marital_status.BorderWidth = 0
            Me.lbl_marital_status.CanGrow = False
            Me.lbl_marital_status.LocationFloat = New DevExpress.Utils.PointFloat(200.0!, 195.125!)
            Me.lbl_marital_status.Name = "lbl_marital_status"
            Me.lbl_marital_status.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.lbl_marital_status.SizeF = New System.Drawing.SizeF(233.0!, 16.0!)
            '
            'lbl_message_ph
            '
            Me.lbl_message_ph.BorderWidth = 0
            Me.lbl_message_ph.CanGrow = False
            Me.lbl_message_ph.LocationFloat = New DevExpress.Utils.PointFloat(200.0!, 163.125!)
            Me.lbl_message_ph.Name = "lbl_message_ph"
            Me.lbl_message_ph.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.lbl_message_ph.SizeF = New System.Drawing.SizeF(233.0!, 16.0!)
            '
            'lbl_home_address
            '
            Me.lbl_home_address.BorderWidth = 0
            Me.lbl_home_address.LocationFloat = New DevExpress.Utils.PointFloat(200.0!, 113.5!)
            Me.lbl_home_address.Multiline = True
            Me.lbl_home_address.Name = "lbl_home_address"
            Me.lbl_home_address.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.lbl_home_address.SizeF = New System.Drawing.SizeF(233.0!, 16.0!)
            '
            'lbl_home_ph
            '
            Me.lbl_home_ph.BorderWidth = 0
            Me.lbl_home_ph.CanGrow = False
            Me.lbl_home_ph.LocationFloat = New DevExpress.Utils.PointFloat(200.0!, 129.5!)
            Me.lbl_home_ph.Name = "lbl_home_ph"
            Me.lbl_home_ph.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.lbl_home_ph.SizeF = New System.Drawing.SizeF(233.0!, 16.0!)
            '
            'XrLabel4
            '
            Me.XrLabel4.BorderWidth = 0
            Me.XrLabel4.CanGrow = False
            Me.XrLabel4.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel4.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 67.00001!)
            Me.XrLabel4.Name = "XrLabel4"
            Me.XrLabel4.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel4.SizeF = New System.Drawing.SizeF(166.0!, 16.0!)
            Me.XrLabel4.Text = "Bankruptcy Counseling"
            '
            'XrLabel5
            '
            Me.XrLabel5.BorderWidth = 0
            Me.XrLabel5.CanGrow = False
            Me.XrLabel5.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel5.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 97.50001!)
            Me.XrLabel5.Name = "XrLabel5"
            Me.XrLabel5.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel5.SizeF = New System.Drawing.SizeF(166.0!, 16.0!)
            Me.XrLabel5.Text = "People In Household"
            '
            'XrLabel3
            '
            Me.XrLabel3.BorderWidth = 0
            Me.XrLabel3.CanGrow = False
            Me.XrLabel3.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel3.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 51.00001!)
            Me.XrLabel3.Name = "XrLabel3"
            Me.XrLabel3.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel3.SizeF = New System.Drawing.SizeF(166.0!, 16.0!)
            Me.XrLabel3.Text = "Status"
            '
            'lbl_intake_id
            '
            Me.lbl_intake_id.Borders = CType((((DevExpress.XtraPrinting.BorderSide.Left Or DevExpress.XtraPrinting.BorderSide.Top) _
                Or DevExpress.XtraPrinting.BorderSide.Right) _
                Or DevExpress.XtraPrinting.BorderSide.Bottom), DevExpress.XtraPrinting.BorderSide)
            Me.lbl_intake_id.BorderWidth = 2
            Me.lbl_intake_id.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.lbl_intake_id.LocationFloat = New DevExpress.Utils.PointFloat(200.0!, 0.0!)
            Me.lbl_intake_id.Name = "lbl_intake_id"
            Me.lbl_intake_id.Padding = New DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100.0!)
            Me.lbl_intake_id.SizeF = New System.Drawing.SizeF(233.0!, 26.0!)
            Me.lbl_intake_id.StylePriority.UseBorders = False
            Me.lbl_intake_id.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
            '
            'XrLabel2
            '
            Me.XrLabel2.BorderWidth = 0
            Me.XrLabel2.CanGrow = False
            Me.XrLabel2.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel2.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 35.00001!)
            Me.XrLabel2.Name = "XrLabel2"
            Me.XrLabel2.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel2.SizeF = New System.Drawing.SizeF(166.0!, 16.0!)
            Me.XrLabel2.Text = "Intake Client ID"
            '
            'XrLabel9
            '
            Me.XrLabel9.BorderWidth = 0
            Me.XrLabel9.CanGrow = False
            Me.XrLabel9.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel9.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 179.125!)
            Me.XrLabel9.Name = "XrLabel9"
            Me.XrLabel9.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel9.SizeF = New System.Drawing.SizeF(166.0!, 16.0!)
            Me.XrLabel9.Text = "Housing Status"
            '
            'XrLabel10
            '
            Me.XrLabel10.BorderWidth = 0
            Me.XrLabel10.CanGrow = False
            Me.XrLabel10.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel10.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 195.125!)
            Me.XrLabel10.Name = "XrLabel10"
            Me.XrLabel10.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel10.SizeF = New System.Drawing.SizeF(166.0!, 16.0!)
            Me.XrLabel10.Text = "Marital Status"
            '
            'XrLabel8
            '
            Me.XrLabel8.BorderWidth = 0
            Me.XrLabel8.CanGrow = False
            Me.XrLabel8.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel8.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 163.125!)
            Me.XrLabel8.Name = "XrLabel8"
            Me.XrLabel8.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel8.SizeF = New System.Drawing.SizeF(175.0!, 16.0!)
            Me.XrLabel8.Text = "Message Telephone Number"
            '
            'XrLabel6
            '
            Me.XrLabel6.BorderWidth = 0
            Me.XrLabel6.CanGrow = False
            Me.XrLabel6.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel6.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 113.5!)
            Me.XrLabel6.Name = "XrLabel6"
            Me.XrLabel6.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel6.SizeF = New System.Drawing.SizeF(166.0!, 16.0!)
            Me.XrLabel6.Text = "Home Address"
            '
            'XrLabel7
            '
            Me.XrLabel7.BorderWidth = 0
            Me.XrLabel7.CanGrow = False
            Me.XrLabel7.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel7.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 129.5!)
            Me.XrLabel7.Name = "XrLabel7"
            Me.XrLabel7.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel7.SizeF = New System.Drawing.SizeF(166.0!, 16.0!)
            Me.XrLabel7.Text = "Home Telephone Number"
            '
            'TopMargin
            '
            Me.TopMargin.HeightF = 0.0!
            Me.TopMargin.Name = "TopMargin"
            Me.TopMargin.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
            Me.TopMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
            '
            'BottomMargin
            '
            Me.BottomMargin.HeightF = 0.0!
            Me.BottomMargin.Name = "BottomMargin"
            Me.BottomMargin.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
            Me.BottomMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
            '
            'IntakeClient
            '
            Me.Bands.AddRange(New DevExpress.XtraReports.UI.Band() {Me.Detail, Me.TopMargin, Me.BottomMargin})
            Me.Margins = New System.Drawing.Printing.Margins(0, 0, 0, 0)
            Me.ScriptLanguage = DevExpress.XtraReports.ScriptLanguage.VisualBasic
            Me.ScriptReferencesString = resources.GetString("$this.ScriptReferencesString")
            Me.Scripts.OnBeforePrint = "IntakeClient_BeforePrint"
            Me.ScriptsSource = resources.GetString("$this.ScriptsSource")
            Me.Version = "11.2"
            CType(Me, System.ComponentModel.ISupportInitialize).EndInit()

        End Sub
        Friend WithEvents Detail As DevExpress.XtraReports.UI.DetailBand
        Friend WithEvents TopMargin As DevExpress.XtraReports.UI.TopMarginBand
        Friend WithEvents BottomMargin As DevExpress.XtraReports.UI.BottomMarginBand
        Friend WithEvents lbl_bankruptcy As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents lbl_people As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents lbl_status As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel1 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents lbl_intake_client As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents lbl_housing_type As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents lbl_marital_status As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents lbl_message_ph As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents lbl_home_address As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents lbl_home_ph As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel4 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel5 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel3 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents lbl_intake_id As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel2 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel9 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel10 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel8 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel6 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel7 As DevExpress.XtraReports.UI.XRLabel
    End Class
End Namespace
