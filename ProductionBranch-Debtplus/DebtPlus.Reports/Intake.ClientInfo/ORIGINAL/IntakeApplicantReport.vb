#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}
#End Region

Namespace Intake.ClientInfo
    Public Class IntakeApplicantReport

        Public Sub New(ByVal Container As System.ComponentModel.IContainer)
            MyClass.New()
            Container.Add(Me)
        End Sub

        Public Sub New()
            MyBase.New()
            InitializeComponent()

            AddHandler BeforePrint, AddressOf IntakeApplicantReport_BeforePrint
            AddHandler lbl_employer.BeforePrint, AddressOf lbl_employer_BeforePrint
            AddHandler lbl_header.BeforePrint, AddressOf lbl_header_BeforePrint
        End Sub

        Private ds As New System.Data.DataSet("ds")

        Private Sub IntakeApplicantReport_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs)
            Dim rpt As DevExpress.XtraReports.UI.XtraReport = CType(sender, DevExpress.XtraReports.UI.XtraReport)
            Dim MasterRpt As DevExpress.XtraReports.UI.XtraReport = CType(rpt.MasterReport, DevExpress.XtraReports.UI.XtraReport)
            Dim sqlInfo As DebtPlus.LINQ.SQLInfoClass = DebtPlus.LINQ.SQLInfoClass.getDefault()
            Dim IntakeClient As Int32 = CType(MasterRpt.Parameters("ParameterIntakeClient").Value, System.Int32)
            Const TableName As String = "intake_people"

            Dim current_cursor As System.Windows.Forms.Cursor = System.Windows.Forms.Cursor.Current
            Try
                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.WaitCursor
                Using cmd As New System.Data.SqlClient.SqlCommand()
                    With cmd
                        .Connection = New System.Data.SqlClient.SqlConnection(sqlInfo.ConnectionString)
                        .CommandText = "SELECT * FROM view_intake_people WHERE intake_client=@intake_client"
                        .CommandType = System.Data.CommandType.Text
                        .Parameters.Add("@intake_client", System.Data.SqlDbType.Int).Value = IntakeClient
                        .CommandTimeout = 0
                    End With

                    Using da As New System.Data.SqlClient.SqlDataAdapter(cmd)
                        da.Fill(ds, TableName)
                    End Using
                End Using

                rpt.DataSource = New System.Data.DataView(ds.Tables(TableName), String.Empty, "person", System.Data.DataViewRowState.CurrentRows)

            Catch ex As System.Data.SqlClient.SqlException
                Dim gdr As New DebtPlus.Repository.GetDataResult() : gdr.HandleException(ex) : DebtPlus.UI.Common.ErrorHandling.HandleErrors(gdr, "Error reading intake_people")

            Finally
                System.Windows.Forms.Cursor.Current = current_cursor
            End Try
        End Sub

        Private Sub lbl_employer_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs)
            With CType(sender, DevExpress.XtraReports.UI.XRLabel)
                Dim rpt As DevExpress.XtraReports.UI.XtraReport = CType(.Report, DevExpress.XtraReports.UI.XtraReport)
                Dim sb As New System.Text.StringBuilder
                For Each Field As String In New String() {"employer_name", "employer_address1", "employer_address2", "employer_address3"}
                    Dim currentvalue As Object = rpt.GetCurrentColumnValue(Field)
                    If currentvalue IsNot Nothing AndAlso currentvalue IsNot System.DBNull.Value AndAlso Convert.ToString(currentvalue).Trim <> String.Empty Then
                        sb.Append(Environment.NewLine)
                        sb.Append(Convert.ToString(currentvalue))
                    End If
                Next
                If sb.Length > 0 Then sb.Remove(0, 2)
                .Text = sb.ToString
            End With
        End Sub

        Private Sub lbl_header_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs)
            With CType(sender, DevExpress.XtraReports.UI.XRLabel)
                Dim rpt As DevExpress.XtraReports.UI.XtraReport = CType(.Report, DevExpress.XtraReports.UI.XtraReport)
                Dim person As Int32 = 1
                If rpt.GetCurrentColumnValue("person") IsNot Nothing AndAlso rpt.GetCurrentColumnValue("person") IsNot System.DBNull.Value Then
                    person = Convert.ToInt32(rpt.GetCurrentColumnValue("person"))
                End If

                If person = 1 Then
                    .Text = "Applicant Information"
                Else
                    .Text = "Co-Applicant Information"
                End If
            End With
        End Sub
    End Class
End Namespace
