#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}
#End Region

Namespace Intake.ClientInfo
    Public Class IntakeSecuredReport
        Inherits DevExpress.XtraReports.UI.XtraReport

        Private ReadOnly ds As New System.Data.DataSet("ds")

        Public Sub New(ByVal Container As System.ComponentModel.IContainer)
            MyClass.New()
            Container.Add(Me)
        End Sub

        Public Sub New()
            MyBase.New()
            InitializeComponent()
            AddHandler BeforePrint, AddressOf Report_BeforePrint
            AddHandler XrTable2.BeforePrint, AddressOf XrTable2_BeforePrint
            AddHandler lbl_total_value.BeforePrint, AddressOf lbl_total_value_BeforePrint
        End Sub

        Private Sub Report_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs)
            Const TableName As String = "intake_hud_loans"
            Dim rpt As DevExpress.XtraReports.UI.XtraReport = CType(sender, DevExpress.XtraReports.UI.XtraReport)
            Dim MasterRpt As DevExpress.XtraReports.UI.XtraReport = CType(rpt.MasterReport, DevExpress.XtraReports.UI.XtraReport)
            Dim sqlInfo As DebtPlus.LINQ.SQLInfoClass = DebtPlus.LINQ.SQLInfoClass.getDefault()
            Dim IntakeClient As Int32 = CType(MasterRpt.Parameters("ParameterIntakeClient").Value, System.Int32)

            Dim current_cursor As System.Windows.Forms.Cursor = System.Windows.Forms.Cursor.Current
            Try
                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.WaitCursor
                Using cmd As New System.Data.SqlClient.SqlCommand()
                    With cmd
                        .Connection = New System.Data.SqlClient.SqlConnection(sqlInfo.ConnectionString)
                        .CommandText = "SELECT * FROM view_intake_hud_loans WHERE intake_client=@intake_client ORDER BY secured_property"
                        .CommandType = System.Data.CommandType.Text
                        .Parameters.Add("@intake_client", System.Data.SqlDbType.Int).Value = IntakeClient
                        .CommandTimeout = 0
                    End With

                    Using da As New System.Data.SqlClient.SqlDataAdapter(cmd)
                        da.Fill(ds, TableName)
                    End Using
                End Using
                rpt.DataSource = ds.Tables(TableName).DefaultView

            Catch ex As System.Data.SqlClient.SqlException
                Dim gdr As New DebtPlus.Repository.GetDataResult() : gdr.HandleException(ex) : DebtPlus.UI.Common.ErrorHandling.HandleErrors(gdr, "Error reading intake_assets")

            Finally
                System.Windows.Forms.Cursor.Current = current_cursor
            End Try
        End Sub

        Dim LastType As System.Int32 = -1
        Private TotalValue As Decimal = 0D
        Private Sub XrTable2_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs)
            With CType(sender, DevExpress.XtraReports.UI.XRTable)
                Dim rpt As DevExpress.XtraReports.UI.XtraReport = CType(.Report, DevExpress.XtraReports.UI.XtraReport)
                Dim CurrentType As System.Int32 = -1
                Dim CurItem As Object = rpt.GetCurrentColumnValue("secured_property")

                '-- Start with the cell blank
                .FindControl("lbl_type", True).Text = String.Empty
                .FindControl("lbl_value", True).Text = String.Empty

                '-- If this is a new group then fill in the cells
                If CurItem IsNot Nothing AndAlso CurItem IsNot System.DBNull.Value Then
                    If LastType < 0 OrElse Convert.ToInt32(CurItem) <> LastType Then
                        LastType = Convert.ToInt32(CurItem)

                        '-- Find the description for the item
                        Dim CurrentDescription As String = Convert.ToString(rpt.GetCurrentColumnValue("secured_type_description"))
                        If CurItem IsNot Nothing AndAlso CurItem IsNot System.DBNull.Value Then CurrentDescription = Convert.ToString(CurItem)
                        .FindControl("lbl_type", True).Text = CurrentDescription

                        '-- Find the value for the item
                        Dim CurrentValue As Decimal = 0D
                        CurItem = GetCurrentColumnValue("current_value")
                        If CurItem IsNot Nothing AndAlso CurItem IsNot System.DBNull.Value Then CurrentValue = Convert.ToDecimal(CurItem)
                        .FindControl("lbl_value", True).Text = String.Format("{0:c2}", CurrentValue)

                        '-- Update the totals for the bottom
                        TotalValue += CurrentValue
                    End If
                End If
            End With
        End Sub

        Private Sub lbl_total_value_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs)
            With CType(sender, DevExpress.XtraReports.UI.XRLabel)
                .Text = String.Format("{0:c2}", TotalValue)
            End With
        End Sub
    End Class
End Namespace
