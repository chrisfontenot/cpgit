#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}
#End Region

Option Compare Binary
Option Explicit On 
Option Strict On

Namespace Tables.ACH.Batches
    Public Class ACHBatchesReport
        Inherits DebtPlus.Reports.Template.TemplateXtraReportClass

        Protected DatePeriod As System.Int32 = 90

        Public Overrides ReadOnly Property ReportTitle() As String
            Get
                Return "ACH Batches"
            End Get
        End Property

        Public Overrides ReadOnly Property ReportSubTitle() As String
            Get
                Return String.Format("This report shows the batches created in the past {0:d} days.", DatePeriod)
            End Get
        End Property

        ''' <summary>
        ''' Create an instance of our report
        ''' </summary>
        Public Sub New()
            MyBase.new()
            InitializeComponent()
            AddHandler Me.BeforePrint, AddressOf Report_BeforePrint
        End Sub

#Region " Component Designer generated code "

        'Component overrides dispose to clean up the component list.
        Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
            If disposing Then
                If components IsNot Nothing Then
                    components.Dispose()
                End If
            End If
            MyBase.Dispose(disposing)
        End Sub

        'Required by the Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Designer
        'It can be modified using the Designer.
        'Do not modify it using the code editor.
        Friend WithEvents XrPanel1 As DevExpress.XtraReports.UI.XRPanel
        Friend WithEvents XrLabel3 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel2 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel1 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel7 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel6 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel5 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_created_by As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_pull_date As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_status As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_date_created As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_effective_date As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_ach_file As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_date_posted As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_settlement_date As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel4 As DevExpress.XtraReports.UI.XRLabel
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
            Me.XrPanel1 = New DevExpress.XtraReports.UI.XRPanel
            Me.XrLabel7 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel6 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel5 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel3 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel2 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel1 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel4 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_ach_file = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_date_posted = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_settlement_date = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_effective_date = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_pull_date = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_status = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_date_created = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_created_by = New DevExpress.XtraReports.UI.XRLabel
            CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
            '
            'Detail
            '
            Me.Detail.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel_created_by, Me.XrLabel_pull_date, Me.XrLabel_status, Me.XrLabel_date_created, Me.XrLabel_effective_date, Me.XrLabel_ach_file, Me.XrLabel_date_posted, Me.XrLabel_settlement_date})
            Me.Detail.HeightF = 15.0!
            '
            'PageHeader
            '
            Me.PageHeader.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrPanel1})
            Me.PageHeader.HeightF = 142.0!
            Me.PageHeader.Controls.SetChildIndex(Me.XrPanel1, 0)
            Me.PageHeader.Controls.SetChildIndex(Me.XrPanel_AgencyAddress, 0)
            Me.PageHeader.Controls.SetChildIndex(Me.XrLabel_Title, 0)
            Me.PageHeader.Controls.SetChildIndex(Me.XrLine_Title, 0)
            Me.PageHeader.Controls.SetChildIndex(Me.XrLabel_Subtitle, 0)
            Me.PageHeader.Controls.SetChildIndex(Me.XrPageInfo1, 0)
            '
            'XrLabel_Title
            '
            Me.XrLabel_Title.StylePriority.UseFont = False
            '
            'XrLabel_Subtitle
            '
            Me.XrLabel_Subtitle.LocationFloat = New DevExpress.Utils.PointFloat(6.000002!, 85.00001!)
            '
            'XrPageInfo_PageNumber
            '
            Me.XrPageInfo_PageNumber.StylePriority.UseTextAlignment = False
            '
            'XRLabel_Agency_Name
            '
            Me.XRLabel_Agency_Name.StylePriority.UseTextAlignment = False
            '
            'XrLabel_Agency_Address3
            '
            Me.XrLabel_Agency_Address3.StylePriority.UseFont = False
            Me.XrLabel_Agency_Address3.StylePriority.UseTextAlignment = False
            '
            'XrLabel_Agency_Address1
            '
            Me.XrLabel_Agency_Address1.StylePriority.UseFont = False
            Me.XrLabel_Agency_Address1.StylePriority.UseTextAlignment = False
            '
            'XrLabel_Agency_Phone
            '
            Me.XrLabel_Agency_Phone.StylePriority.UseFont = False
            Me.XrLabel_Agency_Phone.StylePriority.UseTextAlignment = False
            '
            'XrLabel_Agency_Address2
            '
            Me.XrLabel_Agency_Address2.StylePriority.UseFont = False
            Me.XrLabel_Agency_Address2.StylePriority.UseTextAlignment = False
            '
            'XrPanel1
            '
            Me.XrPanel1.BackColor = System.Drawing.Color.Teal
            Me.XrPanel1.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel7, Me.XrLabel6, Me.XrLabel5, Me.XrLabel3, Me.XrLabel2, Me.XrLabel1, Me.XrLabel4})
            Me.XrPanel1.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 117.0!)
            Me.XrPanel1.Name = "XrPanel1"
            Me.XrPanel1.SizeF = New System.Drawing.SizeF(800.0!, 17.0!)
            '
            'XrLabel7
            '
            Me.XrLabel7.BorderColor = System.Drawing.Color.Transparent
            Me.XrLabel7.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel7.ForeColor = System.Drawing.Color.White
            Me.XrLabel7.LocationFloat = New DevExpress.Utils.PointFloat(533.0!, 1.0!)
            Me.XrLabel7.Name = "XrLabel7"
            Me.XrLabel7.SizeF = New System.Drawing.SizeF(70.0!, 15.0!)
            Me.XrLabel7.StylePriority.UseTextAlignment = False
            Me.XrLabel7.Text = "CREATED"
            Me.XrLabel7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel6
            '
            Me.XrLabel6.BorderColor = System.Drawing.Color.Transparent
            Me.XrLabel6.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel6.ForeColor = System.Drawing.Color.White
            Me.XrLabel6.LocationFloat = New DevExpress.Utils.PointFloat(220.0!, 1.0!)
            Me.XrLabel6.Name = "XrLabel6"
            Me.XrLabel6.SizeF = New System.Drawing.SizeF(113.0!, 15.0!)
            Me.XrLabel6.StylePriority.UseTextAlignment = False
            Me.XrLabel6.Text = "EFFECTIVE"
            Me.XrLabel6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel5
            '
            Me.XrLabel5.BorderColor = System.Drawing.Color.Transparent
            Me.XrLabel5.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel5.ForeColor = System.Drawing.Color.White
            Me.XrLabel5.LocationFloat = New DevExpress.Utils.PointFloat(168.0!, 0.9999911!)
            Me.XrLabel5.Name = "XrLabel5"
            Me.XrLabel5.SizeF = New System.Drawing.SizeF(52.0!, 15.0!)
            Me.XrLabel5.StylePriority.UseTextAlignment = False
            Me.XrLabel5.Text = "PULL"
            Me.XrLabel5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel3
            '
            Me.XrLabel3.BorderColor = System.Drawing.Color.Transparent
            Me.XrLabel3.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel3.ForeColor = System.Drawing.Color.White
            Me.XrLabel3.LocationFloat = New DevExpress.Utils.PointFloat(341.0!, 1.0!)
            Me.XrLabel3.Name = "XrLabel3"
            Me.XrLabel3.SizeF = New System.Drawing.SizeF(100.0!, 15.0!)
            Me.XrLabel3.StylePriority.UseTextAlignment = False
            Me.XrLabel3.Text = "SETTLEMENT"
            Me.XrLabel3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel2
            '
            Me.XrLabel2.BorderColor = System.Drawing.Color.Transparent
            Me.XrLabel2.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel2.ForeColor = System.Drawing.Color.White
            Me.XrLabel2.LocationFloat = New DevExpress.Utils.PointFloat(75.0!, 1.0!)
            Me.XrLabel2.Name = "XrLabel2"
            Me.XrLabel2.SizeF = New System.Drawing.SizeF(66.0!, 15.0!)
            Me.XrLabel2.StylePriority.UseTextAlignment = False
            Me.XrLabel2.Text = "STATUS"
            Me.XrLabel2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
            '
            'XrLabel1
            '
            Me.XrLabel1.BorderColor = System.Drawing.Color.Transparent
            Me.XrLabel1.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel1.ForeColor = System.Drawing.Color.White
            Me.XrLabel1.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 1.0!)
            Me.XrLabel1.Name = "XrLabel1"
            Me.XrLabel1.SizeF = New System.Drawing.SizeF(60.0!, 15.0!)
            Me.XrLabel1.StylePriority.UseTextAlignment = False
            Me.XrLabel1.Text = "ID"
            Me.XrLabel1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel4
            '
            Me.XrLabel4.BorderColor = System.Drawing.Color.Transparent
            Me.XrLabel4.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel4.ForeColor = System.Drawing.Color.White
            Me.XrLabel4.LocationFloat = New DevExpress.Utils.PointFloat(458.0!, 1.0!)
            Me.XrLabel4.Name = "XrLabel4"
            Me.XrLabel4.SizeF = New System.Drawing.SizeF(66.0!, 15.0!)
            Me.XrLabel4.StylePriority.UseTextAlignment = False
            Me.XrLabel4.Text = "POSTED"
            Me.XrLabel4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel_ach_file
            '
            Me.XrLabel_ach_file.BorderColor = System.Drawing.Color.Transparent
            Me.XrLabel_ach_file.Font = New System.Drawing.Font("Times New Roman", 9.75!)
            Me.XrLabel_ach_file.ForeColor = System.Drawing.Color.Black
            Me.XrLabel_ach_file.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
            Me.XrLabel_ach_file.Name = "XrLabel_ach_file"
            Me.XrLabel_ach_file.SizeF = New System.Drawing.SizeF(60.0!, 15.0!)
            Me.XrLabel_ach_file.StylePriority.UseFont = False
            Me.XrLabel_ach_file.StylePriority.UseForeColor = False
            Me.XrLabel_ach_file.StylePriority.UseTextAlignment = False
            Me.XrLabel_ach_file.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel_date_posted
            '
            Me.XrLabel_date_posted.BorderColor = System.Drawing.Color.Transparent
            Me.XrLabel_date_posted.Font = New System.Drawing.Font("Times New Roman", 9.75!)
            Me.XrLabel_date_posted.ForeColor = System.Drawing.Color.Black
            Me.XrLabel_date_posted.LocationFloat = New DevExpress.Utils.PointFloat(458.0!, 0.0!)
            Me.XrLabel_date_posted.Name = "XrLabel_date_posted"
            Me.XrLabel_date_posted.SizeF = New System.Drawing.SizeF(66.0!, 15.0!)
            Me.XrLabel_date_posted.StylePriority.UseFont = False
            Me.XrLabel_date_posted.StylePriority.UseForeColor = False
            Me.XrLabel_date_posted.StylePriority.UseTextAlignment = False
            Me.XrLabel_date_posted.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel_settlement_date
            '
            Me.XrLabel_settlement_date.BorderColor = System.Drawing.Color.Transparent
            Me.XrLabel_settlement_date.Font = New System.Drawing.Font("Times New Roman", 9.75!)
            Me.XrLabel_settlement_date.ForeColor = System.Drawing.Color.Black
            Me.XrLabel_settlement_date.LocationFloat = New DevExpress.Utils.PointFloat(341.0!, 0.0!)
            Me.XrLabel_settlement_date.Name = "XrLabel_settlement_date"
            Me.XrLabel_settlement_date.SizeF = New System.Drawing.SizeF(100.0!, 15.0!)
            Me.XrLabel_settlement_date.StylePriority.UseFont = False
            Me.XrLabel_settlement_date.StylePriority.UseForeColor = False
            Me.XrLabel_settlement_date.StylePriority.UseTextAlignment = False
            Me.XrLabel_settlement_date.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel_effective_date
            '
            Me.XrLabel_effective_date.BorderColor = System.Drawing.Color.Transparent
            Me.XrLabel_effective_date.Font = New System.Drawing.Font("Times New Roman", 9.75!)
            Me.XrLabel_effective_date.ForeColor = System.Drawing.Color.Black
            Me.XrLabel_effective_date.LocationFloat = New DevExpress.Utils.PointFloat(247.0833!, 0.0!)
            Me.XrLabel_effective_date.Name = "XrLabel_effective_date"
            Me.XrLabel_effective_date.SizeF = New System.Drawing.SizeF(85.91666!, 15.0!)
            Me.XrLabel_effective_date.StylePriority.UseFont = False
            Me.XrLabel_effective_date.StylePriority.UseForeColor = False
            Me.XrLabel_effective_date.StylePriority.UseTextAlignment = False
            Me.XrLabel_effective_date.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel_pull_date
            '
            Me.XrLabel_pull_date.BorderColor = System.Drawing.Color.Transparent
            Me.XrLabel_pull_date.Font = New System.Drawing.Font("Times New Roman", 9.75!)
            Me.XrLabel_pull_date.ForeColor = System.Drawing.Color.Black
            Me.XrLabel_pull_date.LocationFloat = New DevExpress.Utils.PointFloat(147.0!, 0.0!)
            Me.XrLabel_pull_date.Name = "XrLabel_pull_date"
            Me.XrLabel_pull_date.SizeF = New System.Drawing.SizeF(73.00002!, 14.99999!)
            Me.XrLabel_pull_date.StylePriority.UseFont = False
            Me.XrLabel_pull_date.StylePriority.UseForeColor = False
            Me.XrLabel_pull_date.StylePriority.UseTextAlignment = False
            Me.XrLabel_pull_date.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel_status
            '
            Me.XrLabel_status.BorderColor = System.Drawing.Color.Transparent
            Me.XrLabel_status.Font = New System.Drawing.Font("Times New Roman", 9.75!)
            Me.XrLabel_status.ForeColor = System.Drawing.Color.Black
            Me.XrLabel_status.LocationFloat = New DevExpress.Utils.PointFloat(75.00002!, 0.0!)
            Me.XrLabel_status.Name = "XrLabel_status"
            Me.XrLabel_status.SizeF = New System.Drawing.SizeF(66.0!, 15.0!)
            Me.XrLabel_status.StylePriority.UseFont = False
            Me.XrLabel_status.StylePriority.UseForeColor = False
            Me.XrLabel_status.StylePriority.UseTextAlignment = False
            Me.XrLabel_status.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
            '
            'XrLabel_date_created
            '
            Me.XrLabel_date_created.BorderColor = System.Drawing.Color.Transparent
            Me.XrLabel_date_created.Font = New System.Drawing.Font("Times New Roman", 9.75!)
            Me.XrLabel_date_created.ForeColor = System.Drawing.Color.Black
            Me.XrLabel_date_created.LocationFloat = New DevExpress.Utils.PointFloat(533.0!, 0.0!)
            Me.XrLabel_date_created.Name = "XrLabel_date_created"
            Me.XrLabel_date_created.SizeF = New System.Drawing.SizeF(70.0!, 15.0!)
            Me.XrLabel_date_created.StylePriority.UseFont = False
            Me.XrLabel_date_created.StylePriority.UseForeColor = False
            Me.XrLabel_date_created.StylePriority.UseTextAlignment = False
            Me.XrLabel_date_created.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel_created_by
            '
            Me.XrLabel_created_by.BorderColor = System.Drawing.Color.Transparent
            Me.XrLabel_created_by.Font = New System.Drawing.Font("Times New Roman", 9.75!)
            Me.XrLabel_created_by.ForeColor = System.Drawing.Color.Black
            Me.XrLabel_created_by.LocationFloat = New DevExpress.Utils.PointFloat(608.0!, 0.0!)
            Me.XrLabel_created_by.Name = "XrLabel_created_by"
            Me.XrLabel_created_by.SizeF = New System.Drawing.SizeF(182.0!, 14.99999!)
            Me.XrLabel_created_by.StylePriority.UseFont = False
            Me.XrLabel_created_by.StylePriority.UseForeColor = False
            Me.XrLabel_created_by.StylePriority.UseTextAlignment = False
            Me.XrLabel_created_by.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            '
            'ACHBatchesReport
            '
            Me.Bands.AddRange(New DevExpress.XtraReports.UI.Band() {Me.Detail, Me.PageHeader, Me.PageFooter})
            Me.Version = "9.3"
            CType(Me, System.ComponentModel.ISupportInitialize).EndInit()

        End Sub

#End Region

        Dim ds As New System.Data.DataSet("ds")
        Private Sub Report_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs)
            Dim tbl As System.Data.DataTable = ds.Tables("view_ach_files")
            If tbl Is Nothing Then
                Using cmd As SqlClient.SqlCommand = New System.Data.SqlClient.SqlCommand()
                    With cmd
                        .CommandText = String.Format("SELECT [ach_file],[trust_register],[pull_date],[settlement_date],[effective_date],[filename],[date_closed],[date_posted],[posted_by],[date_created],[created_by],[message] FROM view_ach_files WHERE date_created >= convert(varchar(10), dateadd(d, -{0:f0}, getdate()), 101)", DatePeriod)
                        .CommandType = CommandType.Text
                        .Connection = New System.Data.SqlClient.SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
                        .CommandTimeout = 0
                    End With

                    Using da As New SqlClient.SqlDataAdapter(cmd)
                        da.Fill(ds, "view_ach_files")
                        tbl = ds.Tables("view_ach_files")
                    End Using
                End Using
            End If

            If tbl IsNot Nothing Then
                Dim vue As System.Data.DataView = ds.Tables(0).DefaultView
                DataSource = vue

                With XrLabel_created_by
                    .DataBindings.Clear()
                    .DataBindings.Add("Text", vue, "created_by")
                End With

                With XrLabel_date_created
                    .DataBindings.Clear()
                    .DataBindings.Add("Text", vue, "date_created", "{0:d}")
                End With

                With XrLabel_effective_date
                    .DataBindings.Clear()
                    .DataBindings.Add("Text", vue, "effective_date", "{0:d}")
                End With

                With XrLabel_date_posted
                    .DataBindings.Clear()
                    .DataBindings.Add("Text", vue, "date_posted", "{0:d}")
                End With

                With XrLabel_pull_date
                    .DataBindings.Clear()
                    .DataBindings.Add("Text", vue, "pull_date", "{0:d}")
                End With

                With XrLabel_settlement_date
                    .DataBindings.Clear()
                    .DataBindings.Add("Text", vue, "settlement_date")
                End With

                With XrLabel_status
                    AddHandler .BeforePrint, AddressOf XrLabel_status_BeforePrint
                End With

                With XrLabel_ach_file
                    .DataBindings.Clear()
                    .DataBindings.Add("Text", vue, "ach_file", "{0:f0}")
                End With
            End If
        End Sub

        Private Sub XrLabel_status_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs)
            Dim obj As Object = GetCurrentColumnValue("date_created")
            If obj Is Nothing OrElse obj Is System.DBNull.Value Then
                XrLabel_status.Text = "INVALID"
            Else
                obj = GetCurrentColumnValue("date_posted")
                If obj Is Nothing OrElse obj Is System.DBNull.Value Then
                    XrLabel_status.Text = "OPEN"
                Else
                    XrLabel_status.Text = "CLOSED"
                End If
            End If
        End Sub
    End Class

End Namespace
