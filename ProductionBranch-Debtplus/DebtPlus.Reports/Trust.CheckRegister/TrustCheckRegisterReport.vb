#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}
#End Region

Imports DebtPlus.Reports.Disbursement.DetailInformation

Namespace Trust.CheckRegister
    Public Class TrustCheckRegisterReport
        Inherits DisbursementDetailInformationReport

        Public Sub New()
            MyBase.New()
            InitializeComponent()

            ' Enable the select expert
            ReportFilter.IsEnabled = True
        End Sub

        Public Overrides ReadOnly Property ReportTitle() As String
            Get
                Return "Check Trust Register"
            End Get
        End Property

        Public Overrides ReadOnly Property ReportSubTitle() As String
            Get
                Return DatedReport_ReportSubtitle()
            End Get
        End Property

        Public Overrides Function NeedParameters() As Boolean
            Return DatedReport_NeedParameters()
        End Function

        Public Overrides Function RequestReportParameters() As System.Windows.Forms.DialogResult
            Return DatedReport_RequestParameters()
        End Function

        Protected ReadOnly ds As New System.Data.DataSet("ds")

        Protected Overrides Function DatabaseTable() As System.Data.DataView
            Const TableName As String = "records"
            Dim tbl As System.Data.DataTable = ds.Tables(TableName)
            If tbl Is Nothing Then

                Dim answer As New System.Text.StringBuilder
                answer.Append("select   convert(varchar(50),newid()) as id, ")
                answer.Append("         tr.trust_register, ")
                answer.Append("         tr.bank, tr.checknum, ")
                answer.Append("         rcc.creditor, ")
                answer.Append(" 	    cr.creditor_name, ")
                answer.Append(" 	    tr.date_created, ")
                answer.Append("         SUM(rcc.debit_amt) as gross, ")
                answer.Append("         SUM(case when rcc.creditor_type in ('D','N') then 0 else rcc.fairshare_amt end) as billed, ")
                answer.Append("	        SUM(case when rcc.creditor_type = 'D' then rcc.fairshare_amt else 0 end) as deducted, ")
                answer.Append("         convert(int,1) as source ")
                answer.Append("from     registers_client_creditor rcc ")
                answer.Append("inner join registers_trust tr on rcc.trust_register = tr.trust_register ")
                answer.Append("inner join creditors cr on rcc.creditor = cr.creditor ")
                answer.Append("where    rcc.tran_type in ('AD','MD','CM') ")
                answer.Append("and      tr.date_created >= @FromDate AND tr.date_created <= @ToDate ")
                answer.Append("group by tr.trust_register, tr.bank, tr.checknum, rcc.creditor, cr.creditor_name, tr.date_created ")

                answer.Append("        UNION ALL ")

                answer.Append("select  convert(varchar(50),newid()) as id, ")
                answer.Append("        tr.trust_register, ")
                answer.Append("        tr.bank, tr.checknum, ")
                answer.Append("        dbo.format_client_id(rc.client) as creditor, ")
                answer.Append("        v.name as creditor_name, ")
                answer.Append("        rc.date_created, ")
                answer.Append("        rc.debit_amt as gross, ")
                answer.Append("        convert(money,0) as billed, ")
                answer.Append("        convert(money,0) as deducted, ")
                answer.Append("        convert(int,2) as source ")
                answer.Append("from    registers_client rc ")
                answer.Append("inner join registers_trust tr on rc.trust_register = tr.trust_register ")
                answer.Append("inner join view_client_address v on rc.client = v.client ")
                answer.Append("where   rc.tran_type = 'CR' ")
                answer.Append("and     rc.date_created >= @FromDate AND rc.date_created <= @ToDate; ")

                Using cmd As SqlClient.SqlCommand = New System.Data.SqlClient.SqlCommand()
                    With cmd
                        .Connection = New System.Data.SqlClient.SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
                        .CommandText = answer.ToString()
                        .CommandType = CommandType.Text
                        .CommandTimeout = 0
                        .Parameters.Add("@FromDate", SqlDbType.DateTime).Value = Parameter_FromDate
                        .Parameters.Add("@ToDate", SqlDbType.DateTime).Value = Parameter_ToDate
                    End With

                    Using da As New SqlClient.SqlDataAdapter(cmd)
                        da.Fill(ds, "records")
                        tbl = ds.Tables("records")

                        With tbl
                            If Not .Columns.Contains("net") Then
                                With .Columns
                                    .Add("net", GetType(Decimal), "[gross]-[deducted]")
                                End With
                            End If
                            .PrimaryKey = New System.Data.DataColumn() {.Columns("id")}
                        End With
                    End Using
                End Using
            End If

            ' Apply the default view
            If tbl IsNot Nothing Then
                Return New System.Data.DataView(tbl, ReportFilter.ViewFilter, String.Empty, DataViewRowState.CurrentRows)
            End If

            Return Nothing
        End Function

        Protected Overrides Sub MyRunReport(ByVal obj As Object)
            Dim RecordsTable As System.Data.DataTable = CType(DataSource, System.Data.DataView).Table
            Dim row As System.Data.DataRow = RecordsTable.Rows.Find(obj)

            If Convert.ToInt32(row("source")) = 1 Then
                Dim iRep As New Trust.CheckRegister.DetailReport()
                Try
                    With iRep
                        .Parameter_TrustRegister = Convert.ToInt32(row("trust_register"))
                        .Parameter_Creditor = Convert.ToString(row("creditor"))
                        .Parameter_FromDate = Parameter_FromDate
                        .Parameter_ToDate = Parameter_ToDate

                        Using frm As New DebtPlus.Reports.Template.PrintPreviewForm(iRep)
                            .CreateDocument()
                            .PrintingSystem.Document.Name = ReportTitle
                            frm.ShowDialog()
                        End Using
                    End With

                Finally
                    iRep.Dispose()
                End Try
            End If
        End Sub

    End Class

End Namespace