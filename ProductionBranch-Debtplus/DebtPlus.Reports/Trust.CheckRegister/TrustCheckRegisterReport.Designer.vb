Namespace Trust.CheckRegister

    <Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
    Partial Class TrustCheckRegisterReport

        'XtraReport overrides dispose to clean up the component list.
        <System.Diagnostics.DebuggerNonUserCode()> _
        Protected Overrides Sub Dispose(ByVal disposing As Boolean)
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
            MyBase.Dispose(disposing)
        End Sub

        'Required by the Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Designer
        'It can be modified using the Designer.  
        'Do not modify it using the code editor.
        <System.Diagnostics.DebuggerStepThrough()> _
        Private Sub InitializeComponent()
            CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
            '
            'XrLabel_Date
            '
            Me.XrLabel_Date.StylePriority.UseTextAlignment = False
            Me.XrLabel_Date.WordWrap = False
            '
            'XrLabel_CheckNumber
            '
            Me.XrLabel_CheckNumber.StylePriority.UseTextAlignment = False
            Me.XrLabel_CheckNumber.WordWrap = False
            '
            'XrLabel_Creditor
            '
            Me.XrLabel_Creditor.StylePriority.UseTextAlignment = False
            Me.XrLabel_Creditor.WordWrap = False
            '
            'XrLabel_CreditorName
            '
            Me.XrLabel_CreditorName.StylePriority.UseTextAlignment = False
            '
            'XrLabel_Gross
            '
            Me.XrLabel_Gross.StylePriority.UseTextAlignment = False
            '
            'XrLabel_Deducted
            '
            Me.XrLabel_Deducted.StylePriority.UseTextAlignment = False
            '
            'XrLabel_Billed
            '
            Me.XrLabel_Billed.StylePriority.UseTextAlignment = False
            '
            'XrLabel_total_net
            '
            Me.XrLabel_total_net.StylePriority.UseFont = False
            '
            'XrLabel_total_deduct
            '
            Me.XrLabel_total_deduct.StylePriority.UseFont = False
            '
            'XrLabel_total_billed
            '
            Me.XrLabel_total_billed.StylePriority.UseFont = False
            '
            'XrLabel_total_gross
            '
            Me.XrLabel_total_gross.StylePriority.UseFont = False
            '
            'XrLabel11A
            '
            Me.XrLabel11A.StylePriority.UseFont = False
            '
            'XrLabel10A
            '
            Me.XrLabel10A.StylePriority.UseFont = False
            '
            'XrLabel9A
            '
            Me.XrLabel9A.StylePriority.UseFont = False
            '
            'XrLabel3A
            '
            Me.XrLabel3A.StylePriority.UseFont = False
            '
            'XrLabel_Title
            '
            Me.XrLabel_Title.StylePriority.UseFont = False
            '
            'XrPageInfo_PageNumber
            '
            Me.XrPageInfo_PageNumber.StylePriority.UseTextAlignment = False
            '
            'XRLabel_Agency_Name
            '
            Me.XRLabel_Agency_Name.StylePriority.UseTextAlignment = False
            '
            'XrLabel_Agency_Address3
            '
            Me.XrLabel_Agency_Address3.StylePriority.UseFont = False
            Me.XrLabel_Agency_Address3.StylePriority.UseTextAlignment = False
            '
            'XrLabel_Agency_Address1
            '
            Me.XrLabel_Agency_Address1.StylePriority.UseFont = False
            Me.XrLabel_Agency_Address1.StylePriority.UseTextAlignment = False
            '
            'XrLabel_Agency_Phone
            '
            Me.XrLabel_Agency_Phone.StylePriority.UseFont = False
            Me.XrLabel_Agency_Phone.StylePriority.UseTextAlignment = False
            '
            'XrLabel_Agency_Address2
            '
            Me.XrLabel_Agency_Address2.StylePriority.UseFont = False
            Me.XrLabel_Agency_Address2.StylePriority.UseTextAlignment = False
            '
            'TrustCheckRegisterReport
            '
            Me.Bands.AddRange(New DevExpress.XtraReports.UI.Band() {Me.Detail, Me.PageHeader, Me.PageFooter, Me.ReportFooter, Me.TopMarginBand1, Me.BottomMarginBand1})
            Me.StyleSheet.AddRange(New DevExpress.XtraReports.UI.XRControlStyle() {Me.XrControlStyle_Header, Me.XrControlStyle_Totals, Me.XrControlStyle_HeaderPannel, Me.XrControlStyle_GroupHeader})
            Me.Version = "10.1"
            CType(Me, System.ComponentModel.ISupportInitialize).EndInit()

        End Sub
    End Class

End Namespace