Namespace Trust.CheckRegister

    <Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
    Partial Class DetailReport

        'XtraReport overrides dispose to clean up the component list.
        <System.Diagnostics.DebuggerNonUserCode()> _
        Protected Overrides Sub Dispose(ByVal disposing As Boolean)
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
            MyBase.Dispose(disposing)
        End Sub

        'Required by the Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Designer
        'It can be modified using the Designer.  
        'Do not modify it using the code editor.
        <System.Diagnostics.DebuggerStepThrough()> _
        Private Sub InitializeComponent()
            Me.XrLabel_client = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_client_name = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel3 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel9 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_account_number = New DevExpress.XtraReports.UI.XRLabel
            CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
            '
            'XrLabel_Date
            '
            Me.XrLabel_Date.LocationFloat = New DevExpress.Utils.PointFloat(266.6667!, 0.0!)
            Me.XrLabel_Date.ProcessDuplicates = DevExpress.XtraReports.UI.ValueSuppressType.Suppress
            '
            'XrLabel_CheckNumber
            '
            Me.XrLabel_CheckNumber.LocationFloat = New DevExpress.Utils.PointFloat(333.6667!, 0.0!)
            Me.XrLabel_CheckNumber.ProcessDuplicates = DevExpress.XtraReports.UI.ValueSuppressType.Suppress
            '
            'XrLabel_Creditor
            '
            Me.XrLabel_Creditor.ProcessDuplicates = DevExpress.XtraReports.UI.ValueSuppressType.Suppress
            '
            'XrLabel_CreditorName
            '
            Me.XrLabel_CreditorName.ProcessDuplicates = DevExpress.XtraReports.UI.ValueSuppressType.Suppress
            Me.XrLabel_CreditorName.SizeF = New System.Drawing.SizeF(191.6667!, 15.0!)
            '
            'XrLabel_Gross
            '
            Me.XrLabel_Gross.LocationFloat = New DevExpress.Utils.PointFloat(693.75!, 0.0!)
            '
            'XrLabel_Deducted
            '
            Me.XrLabel_Deducted.LocationFloat = New DevExpress.Utils.PointFloat(760.75!, 0.0!)
            '
            'XrLabel_Billed
            '
            Me.XrLabel_Billed.LocationFloat = New DevExpress.Utils.PointFloat(827.75!, 0.0!)
            '
            'XrLabel_Net
            '
            Me.XrLabel_Net.LocationFloat = New DevExpress.Utils.PointFloat(1040.292!, 0.0!)
            Me.XrLabel_Net.SizeF = New System.Drawing.SizeF(9.708374!, 15.0!)
            Me.XrLabel_Net.Visible = False
            '
            'XrPanel1
            '
            Me.XrPanel1.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel9, Me.XrLabel3})
            Me.XrPanel1.SizeF = New System.Drawing.SizeF(1050.0!, 17.0!)
            Me.XrPanel1.Controls.SetChildIndex(Me.XrLabel3, 0)
            Me.XrPanel1.Controls.SetChildIndex(Me.XrLabel9, 0)
            Me.XrPanel1.Controls.SetChildIndex(Me.XrLabel1, 0)
            Me.XrPanel1.Controls.SetChildIndex(Me.XrLabel2, 0)
            Me.XrPanel1.Controls.SetChildIndex(Me.XrLabel4, 0)
            Me.XrPanel1.Controls.SetChildIndex(Me.XrLabel5, 0)
            Me.XrPanel1.Controls.SetChildIndex(Me.XrLabel6, 0)
            Me.XrPanel1.Controls.SetChildIndex(Me.XrLabel7, 0)
            Me.XrPanel1.Controls.SetChildIndex(Me.XrLabel8, 0)
            '
            'XrLabel1
            '
            Me.XrLabel1.LocationFloat = New DevExpress.Utils.PointFloat(266.0!, 1.0!)
            '
            'XrLabel2
            '
            Me.XrLabel2.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 1.0!)
            '
            'XrLabel4
            '
            Me.XrLabel4.LocationFloat = New DevExpress.Utils.PointFloat(333.0!, 1.0!)
            '
            'XrLabel5
            '
            Me.XrLabel5.LocationFloat = New DevExpress.Utils.PointFloat(827.0!, 1.0!)
            '
            'XrLabel6
            '
            Me.XrLabel6.LocationFloat = New DevExpress.Utils.PointFloat(904.0!, 1.0!)
            Me.XrLabel6.SizeF = New System.Drawing.SizeF(135.1249!, 15.0!)
            Me.XrLabel6.StylePriority.UseTextAlignment = False
            Me.XrLabel6.Text = "ACCOUNT"
            Me.XrLabel6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            '
            'XrLabel7
            '
            Me.XrLabel7.LocationFloat = New DevExpress.Utils.PointFloat(693.0!, 1.0!)
            '
            'XrLabel8
            '
            Me.XrLabel8.LocationFloat = New DevExpress.Utils.PointFloat(760.0!, 1.0!)
            '
            'XrLabel_total_net
            '
            Me.XrLabel_total_net.LocationFloat = New DevExpress.Utils.PointFloat(683.0!, 84.0!)
            Me.XrLabel_total_net.StylePriority.UseFont = False
            Me.XrLabel_total_net.Visible = False
            '
            'XrLabel_total_deduct
            '
            Me.XrLabel_total_deduct.LocationFloat = New DevExpress.Utils.PointFloat(683.0!, 50.0!)
            Me.XrLabel_total_deduct.StylePriority.UseFont = False
            '
            'XrLabel_total_billed
            '
            Me.XrLabel_total_billed.LocationFloat = New DevExpress.Utils.PointFloat(683.0!, 67.0!)
            Me.XrLabel_total_billed.StylePriority.UseFont = False
            '
            'XrLabel_total_gross
            '
            Me.XrLabel_total_gross.LocationFloat = New DevExpress.Utils.PointFloat(683.0!, 33.0!)
            Me.XrLabel_total_gross.StylePriority.UseFont = False
            '
            'XrLine1
            '
            Me.XrLine1.SizeF = New System.Drawing.SizeF(1050.0!, 8.0!)
            '
            'XrLabel11A
            '
            Me.XrLabel11A.LocationFloat = New DevExpress.Utils.PointFloat(525.0!, 84.0!)
            Me.XrLabel11A.StylePriority.UseFont = False
            Me.XrLabel11A.Visible = False
            '
            'XrLabel10A
            '
            Me.XrLabel10A.LocationFloat = New DevExpress.Utils.PointFloat(525.0!, 67.0!)
            Me.XrLabel10A.StylePriority.UseFont = False
            '
            'XrLabel9A
            '
            Me.XrLabel9A.LocationFloat = New DevExpress.Utils.PointFloat(525.0!, 50.0!)
            Me.XrLabel9A.StylePriority.UseFont = False
            '
            'XrLabel3A
            '
            Me.XrLabel3A.LocationFloat = New DevExpress.Utils.PointFloat(525.0!, 33.0!)
            Me.XrLabel3A.StylePriority.UseFont = False
            '
            'Detail
            '
            Me.Detail.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel_client_name, Me.XrLabel_client, Me.XrLabel_account_number})
            Me.Detail.Controls.SetChildIndex(Me.XrLabel_account_number, 0)
            Me.Detail.Controls.SetChildIndex(Me.XrLabel_client, 0)
            Me.Detail.Controls.SetChildIndex(Me.XrLabel_client_name, 0)
            Me.Detail.Controls.SetChildIndex(Me.XrLabel_Date, 0)
            Me.Detail.Controls.SetChildIndex(Me.XrLabel_CheckNumber, 0)
            Me.Detail.Controls.SetChildIndex(Me.XrLabel_Creditor, 0)
            Me.Detail.Controls.SetChildIndex(Me.XrLabel_CreditorName, 0)
            Me.Detail.Controls.SetChildIndex(Me.XrLabel_Gross, 0)
            Me.Detail.Controls.SetChildIndex(Me.XrLabel_Deducted, 0)
            Me.Detail.Controls.SetChildIndex(Me.XrLabel_Billed, 0)
            Me.Detail.Controls.SetChildIndex(Me.XrLabel_Net, 0)
            '
            'XrLabel_Title
            '
            Me.XrLabel_Title.StylePriority.UseFont = False
            '
            'XrPageInfo_PageNumber
            '
            Me.XrPageInfo_PageNumber.LocationFloat = New DevExpress.Utils.PointFloat(917.0!, 8.0!)
            Me.XrPageInfo_PageNumber.StylePriority.UseTextAlignment = False
            '
            'XrPanel_AgencyAddress
            '
            Me.XrPanel_AgencyAddress.LocationFloat = New DevExpress.Utils.PointFloat(742.0!, 0.0!)
            '
            'XRLabel_Agency_Name
            '
            Me.XRLabel_Agency_Name.StylePriority.UseTextAlignment = False
            '
            'XrLabel_Agency_Address3
            '
            Me.XrLabel_Agency_Address3.StylePriority.UseFont = False
            Me.XrLabel_Agency_Address3.StylePriority.UseTextAlignment = False
            '
            'XrLabel_Agency_Address1
            '
            Me.XrLabel_Agency_Address1.StylePriority.UseFont = False
            Me.XrLabel_Agency_Address1.StylePriority.UseTextAlignment = False
            '
            'XrLabel_Agency_Phone
            '
            Me.XrLabel_Agency_Phone.StylePriority.UseFont = False
            Me.XrLabel_Agency_Phone.StylePriority.UseTextAlignment = False
            '
            'XrLabel_Agency_Address2
            '
            Me.XrLabel_Agency_Address2.StylePriority.UseFont = False
            Me.XrLabel_Agency_Address2.StylePriority.UseTextAlignment = False
            '
            'XrLabel_client
            '
            Me.XrLabel_client.LocationFloat = New DevExpress.Utils.PointFloat(400.6667!, 0.0!)
            Me.XrLabel_client.Name = "XrLabel_client"
            Me.XrLabel_client.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_client.SizeF = New System.Drawing.SizeF(75.0!, 15.0!)
            Me.XrLabel_client.StylePriority.UseTextAlignment = False
            Me.XrLabel_client.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
            '
            'XrLabel_client_name
            '
            Me.XrLabel_client_name.LocationFloat = New DevExpress.Utils.PointFloat(485.75!, 0.0!)
            Me.XrLabel_client_name.Name = "XrLabel_client_name"
            Me.XrLabel_client_name.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_client_name.SizeF = New System.Drawing.SizeF(208.0!, 15.0!)
            '
            'XrLabel3
            '
            Me.XrLabel3.BorderColor = System.Drawing.Color.White
            Me.XrLabel3.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel3.ForeColor = System.Drawing.Color.White
            Me.XrLabel3.LocationFloat = New DevExpress.Utils.PointFloat(408.0!, 1.0!)
            Me.XrLabel3.Name = "XrLabel3"
            Me.XrLabel3.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel3.SizeF = New System.Drawing.SizeF(67.0!, 15.0!)
            Me.XrLabel3.StylePriority.UseBorderColor = False
            Me.XrLabel3.StylePriority.UseFont = False
            Me.XrLabel3.StylePriority.UseForeColor = False
            Me.XrLabel3.StylePriority.UseTextAlignment = False
            Me.XrLabel3.Text = "CLIENT"
            Me.XrLabel3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
            '
            'XrLabel9
            '
            Me.XrLabel9.BorderColor = System.Drawing.Color.White
            Me.XrLabel9.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel9.ForeColor = System.Drawing.Color.White
            Me.XrLabel9.LocationFloat = New DevExpress.Utils.PointFloat(485.0!, 1.0!)
            Me.XrLabel9.Name = "XrLabel9"
            Me.XrLabel9.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel9.SizeF = New System.Drawing.SizeF(67.0!, 15.0!)
            Me.XrLabel9.StylePriority.UseBorderColor = False
            Me.XrLabel9.StylePriority.UseFont = False
            Me.XrLabel9.StylePriority.UseForeColor = False
            Me.XrLabel9.StylePriority.UseTextAlignment = False
            Me.XrLabel9.Text = "NAME"
            Me.XrLabel9.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
            '
            'XrLabel_account_number
            '
            Me.XrLabel_account_number.LocationFloat = New DevExpress.Utils.PointFloat(904.0!, 0.0!)
            Me.XrLabel_account_number.Name = "XrLabel_account_number"
            Me.XrLabel_account_number.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96.0!)
            Me.XrLabel_account_number.SizeF = New System.Drawing.SizeF(135.1249!, 14.99999!)
            Me.XrLabel_account_number.WordWrap = False
            '
            Me.XrLabel_client.DataBindings.Add("Text", Nothing, "client", "{0:0000000}")
            Me.XrLabel_client_name.DataBindings.Add("Text", Nothing, "client_name")
            Me.XrLabel_account_number.DataBindings.Add("Text", Nothing, "account_number")
            '
            'DetailReport
            '
            Me.Bands.AddRange(New DevExpress.XtraReports.UI.Band() {Me.Detail, Me.PageHeader, Me.PageFooter, Me.ReportFooter})
            Me.Landscape = True
            Me.PageHeight = 850
            Me.PageWidth = 1100
            Me.Version = "9.3"
            CType(Me, System.ComponentModel.ISupportInitialize).EndInit()

        End Sub
        Friend WithEvents XrLabel9 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel3 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_client_name As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_client As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_account_number As DevExpress.XtraReports.UI.XRLabel
    End Class

End Namespace