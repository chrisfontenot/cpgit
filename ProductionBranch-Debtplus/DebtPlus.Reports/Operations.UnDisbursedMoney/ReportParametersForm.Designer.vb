﻿Namespace Operations.UnDisbursedMoney
    <Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
    Partial Class OperationsUnDisbursedMoneyParametersForm
        Inherits DebtPlus.Reports.Template.Forms.ReportParametersForm

        'Form overrides dispose to clean up the component list.
        <System.Diagnostics.DebuggerNonUserCode()> _
        Protected Overrides Sub Dispose(ByVal disposing As Boolean)
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
            MyBase.Dispose(disposing)
        End Sub

        'Required by the Windows Form Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Windows Form Designer
        'It can be modified using the Windows Form Designer.  
        'Do not modify it using the code editor.
        <System.Diagnostics.DebuggerStepThrough()> _
        Private Sub InitializeComponent()
            Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl
            Me.SpinEdit1 = New DevExpress.XtraEditors.SpinEdit
            Me.LabelControl2 = New DevExpress.XtraEditors.LabelControl
            Me.SpinEdit2 = New DevExpress.XtraEditors.SpinEdit
            Me.LabelControl3 = New DevExpress.XtraEditors.LabelControl
            CType(Me.DxErrorProvider1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.SpinEdit1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.SpinEdit2.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.SuspendLayout()
            '
            'ButtonOK
            '
            Me.ButtonOK.TabIndex = 5
            '
            'ButtonCancel
            '
            Me.ButtonCancel.CausesValidation = False
            Me.ButtonCancel.TabIndex = 6
            '
            'LabelControl1
            '
            Me.LabelControl1.Location = New System.Drawing.Point(13, 39)
            Me.LabelControl1.Name = "LabelControl1"
            Me.LabelControl1.Size = New System.Drawing.Size(63, 13)
            Me.LabelControl1.TabIndex = 0
            Me.LabelControl1.Text = "Use between"
            '
            'SpinEdit1
            '
            Me.SpinEdit1.EditValue = New Decimal(New Int32() {0, 0, 0, 0})
            Me.SpinEdit1.Location = New System.Drawing.Point(82, 36)
            Me.SpinEdit1.Name = "SpinEdit1"
            Me.SpinEdit1.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[False]
            Me.SpinEdit1.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton})
            Me.SpinEdit1.Properties.IsFloatValue = False
            Me.SpinEdit1.Properties.Mask.EditMask = "N00"
            Me.SpinEdit1.Size = New System.Drawing.Size(41, 20)
            Me.SpinEdit1.TabIndex = 1
            '
            'LabelControl2
            '
            Me.LabelControl2.Location = New System.Drawing.Point(130, 39)
            Me.LabelControl2.Name = "LabelControl2"
            Me.LabelControl2.Size = New System.Drawing.Size(18, 13)
            Me.LabelControl2.TabIndex = 2
            Me.LabelControl2.Text = "and"
            '
            'SpinEdit2
            '
            Me.SpinEdit2.EditValue = New Decimal(New Int32() {0, 0, 0, 0})
            Me.SpinEdit2.Location = New System.Drawing.Point(154, 36)
            Me.SpinEdit2.Name = "SpinEdit2"
            Me.SpinEdit2.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[False]
            Me.SpinEdit2.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton})
            Me.SpinEdit2.Properties.IsFloatValue = False
            Me.SpinEdit2.Properties.Mask.EditMask = "N00"
            Me.SpinEdit2.Size = New System.Drawing.Size(41, 20)
            Me.SpinEdit2.TabIndex = 3
            '
            'LabelControl3
            '
            Me.LabelControl3.Location = New System.Drawing.Point(201, 39)
            Me.LabelControl3.Name = "LabelControl3"
            Me.LabelControl3.Size = New System.Drawing.Size(23, 13)
            Me.LabelControl3.TabIndex = 4
            Me.LabelControl3.Text = "days"
            '
            'ReportParametersForm
            '
            Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
            Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
            Me.ClientSize = New System.Drawing.Size(336, 135)
            Me.Controls.Add(Me.LabelControl3)
            Me.Controls.Add(Me.SpinEdit2)
            Me.Controls.Add(Me.LabelControl2)
            Me.Controls.Add(Me.LabelControl1)
            Me.Controls.Add(Me.SpinEdit1)
            Me.Name = "ReportParametersForm"
            Me.Controls.SetChildIndex(Me.SpinEdit1, 0)
            Me.Controls.SetChildIndex(Me.LabelControl1, 0)
            Me.Controls.SetChildIndex(Me.LabelControl2, 0)
            Me.Controls.SetChildIndex(Me.ButtonOK, 0)
            Me.Controls.SetChildIndex(Me.ButtonCancel, 0)
            Me.Controls.SetChildIndex(Me.SpinEdit2, 0)
            Me.Controls.SetChildIndex(Me.LabelControl3, 0)
            CType(Me.DxErrorProvider1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.SpinEdit1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.SpinEdit2.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            Me.ResumeLayout(False)
            Me.PerformLayout()

        End Sub
        Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents SpinEdit1 As DevExpress.XtraEditors.SpinEdit
        Friend WithEvents LabelControl2 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents SpinEdit2 As DevExpress.XtraEditors.SpinEdit
        Friend WithEvents LabelControl3 As DevExpress.XtraEditors.LabelControl
    End Class
End Namespace