#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}

#End Region

Imports DevExpress.XtraEditors.Controls
Imports System.Globalization

Namespace Operations.UnDisbursedMoney
    Public Class OperationsUnDisbursedMoneyParametersForm

        Public Sub New()
            MyBase.New()
            InitializeComponent()
            AddHandler Me.Load, AddressOf ReportParametersForm_Load
            AddHandler SpinEdit2.EditValueChanging, AddressOf SpinEdit2_EditValueChanging
            AddHandler SpinEdit1.EditValueChanging, AddressOf SpinEdit1_EditValueChanging
        End Sub

        Public Property FromDay() As Int32
            Get
                Return Convert.ToInt32(SpinEdit1.EditValue)
            End Get
            Set(ByVal value As Int32)
                SpinEdit1.EditValue = value
            End Set
        End Property

        Public Property ToDay() As Int32
            Get
                Return Convert.ToInt32(SpinEdit2.EditValue)
            End Get
            Set(ByVal value As Int32)
                SpinEdit2.EditValue = value
            End Set
        End Property

        Private Sub SpinEdit1_EditValueChanging(ByVal sender As Object, ByVal e As ChangingEventArgs)
            ButtonOK.Enabled = False
            If e.NewValue Is Nothing OrElse e.NewValue Is DBNull.Value Then
                e.Cancel = True
            Else
                Dim NewValue As Int32 = Convert.ToInt32(e.NewValue, CultureInfo.InvariantCulture)
                If NewValue < 0 Then
                    e.Cancel = True
                Else
                    If NewValue > ToDay Then
                        SpinEdit2.EditValue = NewValue
                    End If
                    ButtonOK.Enabled = True
                End If
            End If
        End Sub

        Private Sub SpinEdit2_EditValueChanging(ByVal sender As Object, ByVal e As ChangingEventArgs)
            ButtonOK.Enabled = False
            If e.NewValue Is Nothing OrElse e.NewValue Is DBNull.Value Then
                e.Cancel = True
            Else
                Dim NewValue As Int32 = Convert.ToInt32(e.NewValue, CultureInfo.InvariantCulture)
                If NewValue < 0 Then
                    e.Cancel = True
                Else
                    If NewValue < FromDay Then
                        SpinEdit1.EditValue = NewValue
                    End If
                    ButtonOK.Enabled = True
                End If
            End If
        End Sub

        Private Sub ReportParametersForm_Load(ByVal sender As Object, ByVal e As EventArgs)
            ButtonOK.Enabled = True
        End Sub
    End Class
End Namespace