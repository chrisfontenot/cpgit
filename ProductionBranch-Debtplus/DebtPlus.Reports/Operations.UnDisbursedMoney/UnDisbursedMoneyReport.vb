#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}

#End Region

Imports System.ComponentModel
Imports DebtPlus.Reports.Template

Imports System.Drawing.Printing
Imports System.Data.SqlClient
Imports DevExpress.XtraReports.UI
Imports DebtPlus.Utils.Format

Namespace Operations.UnDisbursedMoney
    Public Class UnDisbursedMoneyReport
        Inherits TemplateXtraReportClass

        ''' <summary>
        ''' Return the report title information
        ''' </summary>
        Public Overrides ReadOnly Property ReportTitle() As String
            Get
                Return "UnDisbursed Money"
            End Get
        End Property


        ''' <summary>
        ''' Starting limit for day range
        ''' </summary>
        Private privateParameterFromDay As Int32 = 0

        Public Property Parameter_FromDay() As Int32
            Get
                Return privateParameterFromDay
            End Get
            Set(ByVal value As Int32)
                privateParameterFromDay = value
            End Set
        End Property


        ''' <summary>
        ''' Ending limit for day range
        ''' </summary>
        Private privateParameterToDay As Int32 = 0

        Public Property Parameter_ToDay() As Int32
            Get
                Return privateParameterToDay
            End Get
            Set(ByVal value As Int32)
                privateParameterToDay = value
            End Set
        End Property


        ''' <summary>
        ''' Return the report subtitle information
        ''' </summary>
        Public Overrides ReadOnly Property ReportSubtitle() As String
            Get
                Return String.Format("Report shows between {0:f0} and {1:f0} days", Parameter_FromDay, Parameter_ToDay)
            End Get
        End Property


        ''' <summary>
        ''' Create an instance of our report
        ''' </summary>
        Public Sub New()
            MyBase.New()
            InitializeComponent()

            ' Add handlers for events
            AddHandler XrLabel_cClient.BeforePrint, AddressOf XrLabel_cClient_BeforePrint
            AddHandler XrLabel_ClientName.BeforePrint, AddressOf XrLabel_ClientName_BeforePrint
            AddHandler BeforePrint, AddressOf Report_BeforePrint

            ' bind columns
            XrLabel_DisbFactor.DataBindings.Add("Text", Nothing, "DisbFactor", "{0:c}")
            XrLabel_ccTotalBalanceOwed.DataBindings.Add("Text", Nothing, "ccTotalBalanceOwed", "{0:c}")
            XrLabel_LeftOver.DataBindings.Add("Text", Nothing, "LeftOver", "{0:c}")
            XrLabel_ID.DataBindings.Add("Text", Nothing, "ID")
            XrLabel_rReason.DataBindings.Add("Text", Nothing, "rReason", "{0:f0}")
        End Sub

#Region " Component Designer generated code "

        'Component overrides dispose to clean up the component list.
        Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
            If disposing Then
                If components IsNot Nothing Then
                    components.Dispose()
                End If
                ds.Dispose()
            End If
            MyBase.Dispose(disposing)
        End Sub

        'Required by the Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Designer
        'It can be modified using the Designer.
        'Do not modify it using the code editor.
        Friend WithEvents XrPanel1 As DevExpress.XtraReports.UI.XRPanel
        Friend WithEvents XrLabel1 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents CLIENT As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel2 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel4 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel5 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel6 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_ClientName As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_DisbFactor As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_rReason As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_cClient As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_ccTotalBalanceOwed As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_LeftOver As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_ID As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel3 As DevExpress.XtraReports.UI.XRLabel

        <System.Diagnostics.DebuggerStepThrough()>
        Private Sub InitializeComponent()
            Me.XrPanel1 = New DevExpress.XtraReports.UI.XRPanel
            Me.XrLabel3 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel6 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel5 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel4 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel2 = New DevExpress.XtraReports.UI.XRLabel
            Me.CLIENT = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel1 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_ClientName = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_DisbFactor = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_rReason = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_cClient = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_ccTotalBalanceOwed = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_LeftOver = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_ID = New DevExpress.XtraReports.UI.XRLabel
            CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
            '
            'Detail
            '
            Me.Detail.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel_ID, Me.XrLabel_LeftOver, Me.XrLabel_ccTotalBalanceOwed, Me.XrLabel_cClient, Me.XrLabel_rReason, Me.XrLabel_DisbFactor, Me.XrLabel_ClientName})
            Me.Detail.HeightF = 15.0!
            '
            'PageHeader
            '
            Me.PageHeader.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrPanel1})
            Me.PageHeader.HeightF = 141.0!
            Me.PageHeader.Controls.SetChildIndex(Me.XrPanel1, 0)
            Me.PageHeader.Controls.SetChildIndex(Me.XrPanel_AgencyAddress, 0)
            Me.PageHeader.Controls.SetChildIndex(Me.XrLabel_Title, 0)
            Me.PageHeader.Controls.SetChildIndex(Me.XrLine_Title, 0)
            Me.PageHeader.Controls.SetChildIndex(Me.XrLabel_Subtitle, 0)
            Me.PageHeader.Controls.SetChildIndex(Me.XrPageInfo1, 0)
            '
            'XrLabel_Title
            '
            Me.XrLabel_Title.StylePriority.UseFont = False
            '
            'XrPageInfo_PageNumber
            '
            Me.XrPageInfo_PageNumber.StylePriority.UseTextAlignment = False
            '
            'XRLabel_Agency_Name
            '
            Me.XRLabel_Agency_Name.StylePriority.UseTextAlignment = False
            '
            'XrLabel_Agency_Address3
            '
            Me.XrLabel_Agency_Address3.StylePriority.UseFont = False
            Me.XrLabel_Agency_Address3.StylePriority.UseTextAlignment = False
            '
            'XrLabel_Agency_Address1
            '
            Me.XrLabel_Agency_Address1.StylePriority.UseFont = False
            Me.XrLabel_Agency_Address1.StylePriority.UseTextAlignment = False
            '
            'XrLabel_Agency_Phone
            '
            Me.XrLabel_Agency_Phone.StylePriority.UseFont = False
            Me.XrLabel_Agency_Phone.StylePriority.UseTextAlignment = False
            '
            'XrLabel_Agency_Address2
            '
            Me.XrLabel_Agency_Address2.StylePriority.UseFont = False
            Me.XrLabel_Agency_Address2.StylePriority.UseTextAlignment = False
            '
            'XrPanel1
            '
            Me.XrPanel1.BackColor = System.Drawing.Color.Teal
            Me.XrPanel1.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel3, Me.XrLabel6, Me.XrLabel5, Me.XrLabel4, Me.XrLabel2, Me.CLIENT, Me.XrLabel1})
            Me.XrPanel1.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 117.0!)
            Me.XrPanel1.Name = "XrPanel1"
            Me.XrPanel1.SizeF = New System.Drawing.SizeF(1002.083!, 17.00001!)
            Me.XrPanel1.StyleName = "XrControlStyle_HeaderPannel"
            '
            'XrLabel3
            '
            Me.XrLabel3.CanGrow = False
            Me.XrLabel3.LocationFloat = New DevExpress.Utils.PointFloat(855.1666!, 2.000014!)
            Me.XrLabel3.Name = "XrLabel3"
            Me.XrLabel3.SizeF = New System.Drawing.SizeF(66.95831!, 15.00001!)
            Me.XrLabel3.Text = "REASON"
            Me.XrLabel3.WordWrap = False
            '
            'XrLabel6
            '
            Me.XrLabel6.CanGrow = False
            Me.XrLabel6.LocationFloat = New DevExpress.Utils.PointFloat(934.0833!, 2.000014!)
            Me.XrLabel6.Name = "XrLabel6"
            Me.XrLabel6.SizeF = New System.Drawing.SizeF(58.0!, 15.0!)
            Me.XrLabel6.StylePriority.UseTextAlignment = False
            Me.XrLabel6.Text = "ID"
            Me.XrLabel6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            Me.XrLabel6.WordWrap = False
            '
            'XrLabel5
            '
            Me.XrLabel5.CanGrow = False
            Me.XrLabel5.LocationFloat = New DevExpress.Utils.PointFloat(753.1666!, 2.000046!)
            Me.XrLabel5.Name = "XrLabel5"
            Me.XrLabel5.SizeF = New System.Drawing.SizeF(102.0!, 15.0!)
            Me.XrLabel5.Text = "LEFT OVER"
            Me.XrLabel5.WordWrap = False
            '
            'XrLabel4
            '
            Me.XrLabel4.CanGrow = False
            Me.XrLabel4.LocationFloat = New DevExpress.Utils.PointFloat(608.0!, 2.000076!)
            Me.XrLabel4.Name = "XrLabel4"
            Me.XrLabel4.SizeF = New System.Drawing.SizeF(145.1666!, 15.00001!)
            Me.XrLabel4.Text = "TOT BALANCE OWED"
            Me.XrLabel4.WordWrap = False
            '
            'XrLabel2
            '
            Me.XrLabel2.CanGrow = False
            Me.XrLabel2.LocationFloat = New DevExpress.Utils.PointFloat(87.87502!, 2.000077!)
            Me.XrLabel2.Name = "XrLabel2"
            Me.XrLabel2.SizeF = New System.Drawing.SizeF(75.0!, 15.0!)
            Me.XrLabel2.StylePriority.UseTextAlignment = False
            Me.XrLabel2.Text = "NAME"
            Me.XrLabel2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            Me.XrLabel2.WordWrap = False
            '
            'CLIENT
            '
            Me.CLIENT.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.CLIENT.LocationFloat = New DevExpress.Utils.PointFloat(7.999992!, 0.9999924!)
            Me.CLIENT.Name = "CLIENT"
            Me.CLIENT.SizeF = New System.Drawing.SizeF(71.87502!, 15.0!)
            Me.CLIENT.Text = "CLIENT"
            '
            'XrLabel1
            '
            Me.XrLabel1.CanGrow = False
            Me.XrLabel1.LocationFloat = New DevExpress.Utils.PointFloat(475.7084!, 2.000077!)
            Me.XrLabel1.Name = "XrLabel1"
            Me.XrLabel1.SizeF = New System.Drawing.SizeF(118.7499!, 15.00001!)
            Me.XrLabel1.Text = "DEPOSITS RECVD"
            Me.XrLabel1.WordWrap = False
            '
            'XrLabel_ClientName
            '
            Me.XrLabel_ClientName.LocationFloat = New DevExpress.Utils.PointFloat(87.87502!, 0.0!)
            Me.XrLabel_ClientName.Name = "XrLabel_ClientName"
            Me.XrLabel_ClientName.SizeF = New System.Drawing.SizeF(373.5833!, 15.0!)
            '
            'XrLabel_DisbFactor
            '
            Me.XrLabel_DisbFactor.CanGrow = False
            Me.XrLabel_DisbFactor.LocationFloat = New DevExpress.Utils.PointFloat(608.0!, 0.0!)
            Me.XrLabel_DisbFactor.Name = "XrLabel_DisbFactor"
            Me.XrLabel_DisbFactor.SizeF = New System.Drawing.SizeF(145.1666!, 14.99999!)
            Me.XrLabel_DisbFactor.WordWrap = False
            '
            'XrLabel_rReason
            '
            Me.XrLabel_rReason.CanGrow = False
            Me.XrLabel_rReason.LocationFloat = New DevExpress.Utils.PointFloat(879.1249!, 0.0!)
            Me.XrLabel_rReason.Name = "XrLabel_rReason"
            Me.XrLabel_rReason.SizeF = New System.Drawing.SizeF(43.0!, 15.0!)
            Me.XrLabel_rReason.WordWrap = False
            '
            'XrLabel_cClient
            '
            Me.XrLabel_cClient.LocationFloat = New DevExpress.Utils.PointFloat(7.999992!, 0.0!)
            Me.XrLabel_cClient.Name = "XrLabel_cClient"
            Me.XrLabel_cClient.SizeF = New System.Drawing.SizeF(71.87502!, 14.99999!)
            '
            'XrLabel_ccTotalBalanceOwed
            '
            Me.XrLabel_ccTotalBalanceOwed.CanGrow = False
            Me.XrLabel_ccTotalBalanceOwed.LocationFloat = New DevExpress.Utils.PointFloat(475.7083!, 0.0!)
            Me.XrLabel_ccTotalBalanceOwed.Name = "XrLabel_ccTotalBalanceOwed"
            Me.XrLabel_ccTotalBalanceOwed.SizeF = New System.Drawing.SizeF(118.75!, 15.0!)
            Me.XrLabel_ccTotalBalanceOwed.WordWrap = False
            '
            'XrLabel_LeftOver
            '
            Me.XrLabel_LeftOver.CanGrow = False
            Me.XrLabel_LeftOver.LocationFloat = New DevExpress.Utils.PointFloat(777.1249!, 0.0!)
            Me.XrLabel_LeftOver.Name = "XrLabel_LeftOver"
            Me.XrLabel_LeftOver.SizeF = New System.Drawing.SizeF(78.04169!, 15.0!)
            Me.XrLabel_LeftOver.WordWrap = False
            '
            'XrLabel_ID
            '
            Me.XrLabel_ID.CanGrow = False
            Me.XrLabel_ID.LocationFloat = New DevExpress.Utils.PointFloat(934.0833!, 0.0!)
            Me.XrLabel_ID.Name = "XrLabel_ID"
            Me.XrLabel_ID.SizeF = New System.Drawing.SizeF(58.0!, 15.0!)
            Me.XrLabel_ID.WordWrap = False
            '
            'UnDisbursedMoneyReport
            '
            Me.Bands.AddRange(New DevExpress.XtraReports.UI.Band() {Me.Detail, Me.PageHeader, Me.PageFooter, Me.TopMarginBand1, Me.BottomMarginBand1})
            Me.Landscape = True
            Me.PageHeight = 850
            Me.PageWidth = 1100
            Me.StyleSheet.AddRange(New DevExpress.XtraReports.UI.XRControlStyle() {Me.XrControlStyle_Header, Me.XrControlStyle_Totals, Me.XrControlStyle_HeaderPannel, Me.XrControlStyle_GroupHeader})
            Me.Version = "10.1"
            CType(Me, System.ComponentModel.ISupportInitialize).EndInit()
        End Sub

#End Region


        ''' <summary>
        ''' Do we need to ask for parameters
        ''' </summary>
        Public Overrides Function NeedParameters() As Boolean
            Return MyBase.NeedParameters() OrElse (Parameter_FromDay < 0) OrElse (Parameter_ToDay < Parameter_FromDay) OrElse ((Parameter_FromDay = Parameter_ToDay) AndAlso Parameter_FromDay = 0)
        End Function


        ''' <summary>
        ''' If needed, request the report parameters
        ''' </summary>
        Public Overrides Function RequestReportParameters() As DialogResult
            Dim answer As DialogResult = DialogResult.OK
            If NeedParameters() Then
                Using frm As New OperationsUnDisbursedMoneyParametersForm
                    answer = frm.ShowDialog()
                    Parameter_FromDay = frm.FromDay
                    Parameter_ToDay = frm.ToDay
                End Using
            End If
            Return answer
        End Function

        Dim ds As New DataSet("ds")


        ''' <summary>
        ''' Refresh the dataset with the new information
        ''' </summary>
        Private Sub Report_BeforePrint(ByVal sender As Object, ByVal e As EventArgs)

            Dim tbl As DataTable = ds.Tables("spr_rpt_undisbursed_money")
            If tbl IsNot Nothing Then
                ds.Tables.Remove(tbl)
            End If

            Dim cn As SqlConnection = New System.Data.SqlClient.SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
            Try
                cn.Open()
                Using cmd As SqlCommand = New System.Data.SqlClient.SqlCommand()
                    With cmd
                        .Connection = cn
                        .CommandText = "spr_rpt_undisbursed_money"
                        .CommandType = CommandType.StoredProcedure

                        SqlCommandBuilder.DeriveParameters(cmd)
                        .Parameters(1).Value = Parameter_FromDay
                        .Parameters(2).Value = Parameter_ToDay

                        .CommandTimeout = 0

                        Using da As New SqlDataAdapter(cmd)
                            da.Fill(ds, "spr_rpt_undisbursed_money")
                            tbl = ds.Tables("spr_rpt_undisbursed_money")
                            With tbl
                                If Not .Columns.Contains("LeftOver") Then
                                    .Columns.Add("LeftOver", GetType(Decimal), "[ccBalanceOwed]-[cDepositsRecvd]")
                                End If
                            End With
                        End Using
                    End With
                End Using

                Dim vue As DataView = tbl.DefaultView
                DataSource = vue

            Catch ex As SqlException
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error reading report data")

            Finally
                If cn IsNot Nothing Then cn.Dispose()
            End Try
        End Sub

        Private Sub XrLabel_cClient_BeforePrint(ByVal sender As Object, ByVal e As PrintEventArgs)
            With CType(sender, XRLabel)
                .Text = String.Format("{0:0000000}", GetCurrentColumnValue("client"))
            End With
        End Sub

        Private Sub XrLabel_ClientName_BeforePrint(ByVal sender As Object, ByVal e As PrintEventArgs)
            With CType(sender, XRLabel)
                .Text = DebtPlus.LINQ.Name.FormatNormalName(Nothing, Me.GetCurrentColumnValue("pFirst"), Me.GetCurrentColumnValue("pMiddle"), Me.GetCurrentColumnValue("pLast"), Nothing)
            End With
        End Sub
    End Class
End Namespace
