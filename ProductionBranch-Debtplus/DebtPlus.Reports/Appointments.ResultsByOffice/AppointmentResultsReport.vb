#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}
#End Region

Option Compare Binary
Option Explicit On
Option Strict On

Imports DebtPlus.Reports.Template

Namespace Appointments.ResultsByOffice

    Public Class AppointmentResultsReport
        Inherits DatedTemplateXtraReportClass


        ''' <summary>
        ''' Create an instance of our report
        ''' </summary>
        Public Sub New()
            MyBase.New()
            InitializeComponent()

            AddHandler BeforePrint, AddressOf Report_BeforePrint
            AddHandler XrLabel_status.PreviewClick, AddressOf DetailReport
            AddHandler XrLabel_count.PreviewClick, AddressOf DetailReport
            AddHandler XrLabel_status.PreviewClick, AddressOf DetailReport
        End Sub

#Region " Component Designer generated code "

        'Component overrides dispose to clean up the component list.
        Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
            If disposing Then
                ds.Dispose()
                If components IsNot Nothing Then
                    components.Dispose()
                End If
            End If
            MyBase.Dispose(disposing)
        End Sub

        Friend WithEvents XrPanel1 As DevExpress.XtraReports.UI.XRPanel
        Friend WithEvents XrLabel6 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel9 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_count As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_office As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_status As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents GroupHeader1 As DevExpress.XtraReports.UI.GroupHeaderBand
        Friend WithEvents GroupFooter1 As DevExpress.XtraReports.UI.GroupFooterBand
        Friend WithEvents XrLabel_total_office As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_subtotal_count As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel3 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents GroupFooter2 As DevExpress.XtraReports.UI.GroupFooterBand
        Friend WithEvents GroupHeader2 As DevExpress.XtraReports.UI.GroupHeaderBand
        Friend WithEvents ReportFooter As DevExpress.XtraReports.UI.ReportFooterBand
        Friend WithEvents XrLabel_total_count As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel1 As DevExpress.XtraReports.UI.XRLabel

        'Required by the Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Designer
        'It can be modified using the Designer.
        'Do not modify it using the code editor.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
            Dim XrSummary1 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary
            Dim XrSummary2 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary
            Dim XrSummary3 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary
            Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(AppointmentResultsReport))
            Me.XrPanel1 = New DevExpress.XtraReports.UI.XRPanel
            Me.XrLabel3 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel6 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel9 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_status = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_count = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_office = New DevExpress.XtraReports.UI.XRLabel
            Me.GroupHeader1 = New DevExpress.XtraReports.UI.GroupHeaderBand
            Me.GroupFooter1 = New DevExpress.XtraReports.UI.GroupFooterBand
            Me.XrLabel_total_office = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_subtotal_count = New DevExpress.XtraReports.UI.XRLabel
            Me.GroupFooter2 = New DevExpress.XtraReports.UI.GroupFooterBand
            Me.GroupHeader2 = New DevExpress.XtraReports.UI.GroupHeaderBand
            Me.ReportFooter = New DevExpress.XtraReports.UI.ReportFooterBand
            Me.XrLabel_total_count = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel1 = New DevExpress.XtraReports.UI.XRLabel
            CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
            '
            'Detail
            '
            Me.Detail.HeightF = 0.0!
            '
            'PageHeader
            '
            Me.PageHeader.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrPanel1})
            Me.PageHeader.HeightF = 136.0!
            Me.PageHeader.Controls.SetChildIndex(Me.XrPanel1, 0)
            Me.PageHeader.Controls.SetChildIndex(Me.XrPanel_AgencyAddress, 0)
            Me.PageHeader.Controls.SetChildIndex(Me.XrLabel_Title, 0)
            Me.PageHeader.Controls.SetChildIndex(Me.XrLine_Title, 0)
            Me.PageHeader.Controls.SetChildIndex(Me.XrLabel_Subtitle, 0)
            Me.PageHeader.Controls.SetChildIndex(Me.XrPageInfo1, 0)
            '
            'XrLabel_Title
            '
            Me.XrLabel_Title.StylePriority.UseFont = False
            '
            'XrPageInfo_PageNumber
            '
            Me.XrPageInfo_PageNumber.StylePriority.UseTextAlignment = False
            '
            'XRLabel_Agency_Name
            '
            Me.XRLabel_Agency_Name.StylePriority.UseTextAlignment = False
            '
            'XrLabel_Agency_Address3
            '
            Me.XrLabel_Agency_Address3.StylePriority.UseFont = False
            Me.XrLabel_Agency_Address3.StylePriority.UseTextAlignment = False
            '
            'XrLabel_Agency_Address1
            '
            Me.XrLabel_Agency_Address1.StylePriority.UseFont = False
            Me.XrLabel_Agency_Address1.StylePriority.UseTextAlignment = False
            '
            'XrLabel_Agency_Phone
            '
            Me.XrLabel_Agency_Phone.StylePriority.UseFont = False
            Me.XrLabel_Agency_Phone.StylePriority.UseTextAlignment = False
            '
            'XrLabel_Agency_Address2
            '
            Me.XrLabel_Agency_Address2.StylePriority.UseFont = False
            Me.XrLabel_Agency_Address2.StylePriority.UseTextAlignment = False
            '
            'XrPanel1
            '
            Me.XrPanel1.BackColor = System.Drawing.Color.Teal
            Me.XrPanel1.BorderColor = System.Drawing.Color.Teal
            Me.XrPanel1.CanGrow = False
            Me.XrPanel1.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel3, Me.XrLabel6, Me.XrLabel9})
            Me.XrPanel1.KeepTogether = False
            Me.XrPanel1.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 113.9166!)
            Me.XrPanel1.Name = "XrPanel1"
            Me.XrPanel1.SizeF = New System.Drawing.SizeF(800.0!, 18.0!)
            Me.XrPanel1.StyleName = "XrControlStyle_HeaderPannel"
            Me.XrPanel1.StylePriority.UseBackColor = False
            Me.XrPanel1.StylePriority.UseBorderColor = False
            '
            'XrLabel3
            '
            Me.XrLabel3.BackColor = System.Drawing.Color.Transparent
            Me.XrLabel3.BorderColor = System.Drawing.Color.Transparent
            Me.XrLabel3.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel3.ForeColor = System.Drawing.Color.White
            Me.XrLabel3.LocationFloat = New DevExpress.Utils.PointFloat(75.0!, 1.0!)
            Me.XrLabel3.Name = "XrLabel3"
            Me.XrLabel3.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel3.SizeF = New System.Drawing.SizeF(450.0!, 14.99999!)
            Me.XrLabel3.StylePriority.UseFont = False
            Me.XrLabel3.StylePriority.UseForeColor = False
            Me.XrLabel3.Text = "OFFICE"
            Me.XrLabel3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            Me.XrLabel3.WordWrap = False
            '
            'XrLabel6
            '
            Me.XrLabel6.BackColor = System.Drawing.Color.Transparent
            Me.XrLabel6.BorderColor = System.Drawing.Color.Transparent
            Me.XrLabel6.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel6.ForeColor = System.Drawing.Color.White
            Me.XrLabel6.LocationFloat = New DevExpress.Utils.PointFloat(537.1667!, 3.000005!)
            Me.XrLabel6.Name = "XrLabel6"
            Me.XrLabel6.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel6.SizeF = New System.Drawing.SizeF(116.0!, 15.0!)
            Me.XrLabel6.StylePriority.UseBackColor = False
            Me.XrLabel6.StylePriority.UseBorderColor = False
            Me.XrLabel6.StylePriority.UseFont = False
            Me.XrLabel6.StylePriority.UseForeColor = False
            Me.XrLabel6.StylePriority.UseTextAlignment = False
            Me.XrLabel6.Text = "COUNT"
            Me.XrLabel6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel9
            '
            Me.XrLabel9.BackColor = System.Drawing.Color.Transparent
            Me.XrLabel9.BorderColor = System.Drawing.Color.Transparent
            Me.XrLabel9.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel9.ForeColor = System.Drawing.Color.White
            Me.XrLabel9.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
            Me.XrLabel9.Name = "XrLabel9"
            Me.XrLabel9.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel9.SizeF = New System.Drawing.SizeF(58.0!, 15.0!)
            Me.XrLabel9.StylePriority.UseBackColor = False
            Me.XrLabel9.StylePriority.UseBorderColor = False
            Me.XrLabel9.StylePriority.UseFont = False
            Me.XrLabel9.StylePriority.UseForeColor = False
            Me.XrLabel9.StylePriority.UseTextAlignment = False
            Me.XrLabel9.Text = "STATUS"
            Me.XrLabel9.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel_status
            '
            Me.XrLabel_status.BackColor = System.Drawing.Color.Transparent
            Me.XrLabel_status.BorderColor = System.Drawing.Color.Transparent
            Me.XrLabel_status.Font = New System.Drawing.Font("Times New Roman", 9.75!)
            Me.XrLabel_status.ForeColor = System.Drawing.Color.Black
            Me.XrLabel_status.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
            Me.XrLabel_status.Name = "XrLabel_status"
            Me.XrLabel_status.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_status.SizeF = New System.Drawing.SizeF(58.0!, 15.0!)
            Me.XrLabel_status.StylePriority.UseBackColor = False
            Me.XrLabel_status.StylePriority.UseBorderColor = False
            Me.XrLabel_status.StylePriority.UseFont = False
            Me.XrLabel_status.StylePriority.UseForeColor = False
            Me.XrLabel_status.StylePriority.UseTextAlignment = False
            Me.XrLabel_status.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            Me.XrLabel_status.WordWrap = False
            '
            'XrLabel_count
            '
            Me.XrLabel_count.BackColor = System.Drawing.Color.Transparent
            Me.XrLabel_count.BorderColor = System.Drawing.Color.Transparent
            Me.XrLabel_count.Font = New System.Drawing.Font("Times New Roman", 9.75!)
            Me.XrLabel_count.ForeColor = System.Drawing.Color.Black
            Me.XrLabel_count.LocationFloat = New DevExpress.Utils.PointFloat(537.1667!, 0.0!)
            Me.XrLabel_count.Name = "XrLabel_count"
            Me.XrLabel_count.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_count.SizeF = New System.Drawing.SizeF(116.0!, 15.0!)
            Me.XrLabel_count.StylePriority.UseBackColor = False
            Me.XrLabel_count.StylePriority.UseBorderColor = False
            Me.XrLabel_count.StylePriority.UseFont = False
            Me.XrLabel_count.StylePriority.UseForeColor = False
            Me.XrLabel_count.StylePriority.UseTextAlignment = False
            XrSummary1.FormatString = "{0:n0}"
            XrSummary1.Func = DevExpress.XtraReports.UI.SummaryFunc.Count
            XrSummary1.Running = DevExpress.XtraReports.UI.SummaryRunning.Group
            Me.XrLabel_count.Summary = XrSummary1
            Me.XrLabel_count.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrLabel_count.WordWrap = False
            '
            'XrLabel_office
            '
            Me.XrLabel_office.BackColor = System.Drawing.Color.Transparent
            Me.XrLabel_office.BorderColor = System.Drawing.Color.Transparent
            Me.XrLabel_office.Font = New System.Drawing.Font("Times New Roman", 9.75!)
            Me.XrLabel_office.ForeColor = System.Drawing.Color.Black
            Me.XrLabel_office.LocationFloat = New DevExpress.Utils.PointFloat(74.99998!, 0.0!)
            Me.XrLabel_office.Name = "XrLabel_office"
            Me.XrLabel_office.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_office.ProcessDuplicates = DevExpress.XtraReports.UI.ValueSuppressType.Suppress
            Me.XrLabel_office.SizeF = New System.Drawing.SizeF(450.0!, 14.99999!)
            Me.XrLabel_office.StylePriority.UseBackColor = False
            Me.XrLabel_office.StylePriority.UseBorderColor = False
            Me.XrLabel_office.StylePriority.UseFont = False
            Me.XrLabel_office.StylePriority.UseForeColor = False
            Me.XrLabel_office.StylePriority.UseTextAlignment = False
            Me.XrLabel_office.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            Me.XrLabel_office.WordWrap = False
            '
            'GroupHeader1
            '
            Me.GroupHeader1.HeightF = 0.0!
            Me.GroupHeader1.Name = "GroupHeader1"
            '
            'GroupFooter1
            '
            Me.GroupFooter1.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel_status, Me.XrLabel_office, Me.XrLabel_count})
            Me.GroupFooter1.HeightF = 15.0!
            Me.GroupFooter1.Name = "GroupFooter1"
            Me.GroupFooter1.StyleName = "XrControlStyle_Totals"
            '
            'XrLabel_total_office
            '
            Me.XrLabel_total_office.BackColor = System.Drawing.Color.Transparent
            Me.XrLabel_total_office.BorderColor = System.Drawing.Color.Transparent
            Me.XrLabel_total_office.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel_total_office.ForeColor = System.Drawing.Color.Black
            Me.XrLabel_total_office.LocationFloat = New DevExpress.Utils.PointFloat(7.999992!, 10.00001!)
            Me.XrLabel_total_office.Name = "XrLabel_total_office"
            Me.XrLabel_total_office.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_total_office.SizeF = New System.Drawing.SizeF(450.0!, 14.99999!)
            Me.XrLabel_total_office.StylePriority.UseBackColor = False
            Me.XrLabel_total_office.StylePriority.UseBorderColor = False
            Me.XrLabel_total_office.StylePriority.UseFont = False
            Me.XrLabel_total_office.StylePriority.UseForeColor = False
            Me.XrLabel_total_office.StylePriority.UseTextAlignment = False
            Me.XrLabel_total_office.Text = "SUB-TOTALS"
            Me.XrLabel_total_office.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            Me.XrLabel_total_office.WordWrap = False
            '
            'XrLabel_subtotal_count
            '
            Me.XrLabel_subtotal_count.BackColor = System.Drawing.Color.Transparent
            Me.XrLabel_subtotal_count.BorderColor = System.Drawing.Color.Transparent
            Me.XrLabel_subtotal_count.Font = New System.Drawing.Font("Times New Roman", 9.75!)
            Me.XrLabel_subtotal_count.ForeColor = System.Drawing.Color.Black
            Me.XrLabel_subtotal_count.LocationFloat = New DevExpress.Utils.PointFloat(537.1667!, 10.00001!)
            Me.XrLabel_subtotal_count.Name = "XrLabel_subtotal_count"
            Me.XrLabel_subtotal_count.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_subtotal_count.SizeF = New System.Drawing.SizeF(116.0!, 15.0!)
            Me.XrLabel_subtotal_count.StylePriority.UseBackColor = False
            Me.XrLabel_subtotal_count.StylePriority.UseBorderColor = False
            Me.XrLabel_subtotal_count.StylePriority.UseFont = False
            Me.XrLabel_subtotal_count.StylePriority.UseForeColor = False
            Me.XrLabel_subtotal_count.StylePriority.UseTextAlignment = False
            XrSummary2.FormatString = "{0:n0}"
            XrSummary2.Func = DevExpress.XtraReports.UI.SummaryFunc.Count
            XrSummary2.Running = DevExpress.XtraReports.UI.SummaryRunning.Group
            Me.XrLabel_subtotal_count.Summary = XrSummary2
            Me.XrLabel_subtotal_count.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrLabel_subtotal_count.WordWrap = False
            '
            'GroupFooter2
            '
            Me.GroupFooter2.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel_total_office, Me.XrLabel_subtotal_count})
            Me.GroupFooter2.HeightF = 35.0!
            Me.GroupFooter2.Level = 1
            Me.GroupFooter2.Name = "GroupFooter2"
            '
            'GroupHeader2
            '
            Me.GroupHeader2.HeightF = 0.0!
            Me.GroupHeader2.Level = 1
            Me.GroupHeader2.Name = "GroupHeader2"
            '
            'ReportFooter
            '
            Me.ReportFooter.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel_total_count, Me.XrLabel1})
            Me.ReportFooter.HeightF = 40.0!
            Me.ReportFooter.Name = "ReportFooter"
            '
            'XrLabel_total_count
            '
            Me.XrLabel_total_count.BackColor = System.Drawing.Color.Transparent
            Me.XrLabel_total_count.BorderColor = System.Drawing.Color.Transparent
            Me.XrLabel_total_count.Font = New System.Drawing.Font("Times New Roman", 9.75!)
            Me.XrLabel_total_count.ForeColor = System.Drawing.Color.Black
            Me.XrLabel_total_count.LocationFloat = New DevExpress.Utils.PointFloat(537.1667!, 9.999974!)
            Me.XrLabel_total_count.Name = "XrLabel_total_count"
            Me.XrLabel_total_count.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_total_count.SizeF = New System.Drawing.SizeF(116.0!, 15.0!)
            Me.XrLabel_total_count.StylePriority.UseBackColor = False
            Me.XrLabel_total_count.StylePriority.UseBorderColor = False
            Me.XrLabel_total_count.StylePriority.UseFont = False
            Me.XrLabel_total_count.StylePriority.UseForeColor = False
            Me.XrLabel_total_count.StylePriority.UseTextAlignment = False
            XrSummary3.FormatString = "{0:n0}"
            XrSummary3.Func = DevExpress.XtraReports.UI.SummaryFunc.Count
            XrSummary3.Running = DevExpress.XtraReports.UI.SummaryRunning.Report
            Me.XrLabel_total_count.Summary = XrSummary3
            Me.XrLabel_total_count.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrLabel_total_count.WordWrap = False
            '
            'XrLabel1
            '
            Me.XrLabel1.BackColor = System.Drawing.Color.Transparent
            Me.XrLabel1.BorderColor = System.Drawing.Color.Transparent
            Me.XrLabel1.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel1.ForeColor = System.Drawing.Color.Black
            Me.XrLabel1.LocationFloat = New DevExpress.Utils.PointFloat(10.00001!, 9.999974!)
            Me.XrLabel1.Name = "XrLabel1"
            Me.XrLabel1.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel1.SizeF = New System.Drawing.SizeF(450.0!, 14.99999!)
            Me.XrLabel1.StylePriority.UseBackColor = False
            Me.XrLabel1.StylePriority.UseBorderColor = False
            Me.XrLabel1.StylePriority.UseFont = False
            Me.XrLabel1.StylePriority.UseForeColor = False
            Me.XrLabel1.StylePriority.UseTextAlignment = False
            Me.XrLabel1.Text = "TOTALS"
            Me.XrLabel1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            Me.XrLabel1.WordWrap = False
            '
            Me.GroupHeader2.GroupFields.Add(New DevExpress.XtraReports.UI.GroupField("office", DevExpress.XtraReports.UI.XRColumnSortOrder.Ascending))
            Me.GroupHeader1.GroupFields.Add(New DevExpress.XtraReports.UI.GroupField("appt_results", DevExpress.XtraReports.UI.XRColumnSortOrder.Ascending))
            Me.XrLabel_office.DataBindings.Add("Text", Nothing, "office")
            Me.XrLabel_office.DataBindings.Add("Tag", Nothing, "Tag")
            Me.XrLabel_status.DataBindings.Add("Text", Nothing, "appt_results")
            Me.XrLabel_status.DataBindings.Add("Tag", Nothing, "Tag")
            Me.XrLabel_count.DataBindings.Add("Text", Nothing, "count", "{0:n0}")
            Me.XrLabel_count.DataBindings.Add("Tag", Nothing, "Tag")
            Me.XrLabel_subtotal_count.DataBindings.Add("Text", Nothing, "client")
            Me.XrLabel_total_count.DataBindings.Add("Text", Nothing, "client")
            '
            'AppointmentResultsReport
            '
            Me.Bands.AddRange(New DevExpress.XtraReports.UI.Band() {Me.Detail, Me.PageHeader, Me.PageFooter, Me.GroupHeader1, Me.GroupFooter1, Me.GroupFooter2, Me.GroupHeader2, Me.ReportFooter, Me.TopMarginBand1, Me.BottomMarginBand1})
            Me.ScriptLanguage = DevExpress.XtraReports.ScriptLanguage.VisualBasic
            Me.ScriptReferencesString = resources.GetString("$this.ScriptReferencesString")
            Me.StyleSheet.AddRange(New DevExpress.XtraReports.UI.XRControlStyle() {Me.XrControlStyle_Header, Me.XrControlStyle_Totals, Me.XrControlStyle_HeaderPannel, Me.XrControlStyle_GroupHeader})
            Me.Version = "10.1"
            Me.Controls.SetChildIndex(Me.BottomMarginBand1, 0)
            Me.Controls.SetChildIndex(Me.TopMarginBand1, 0)
            Me.Controls.SetChildIndex(Me.ReportFooter, 0)
            Me.Controls.SetChildIndex(Me.GroupHeader2, 0)
            Me.Controls.SetChildIndex(Me.GroupFooter2, 0)
            Me.Controls.SetChildIndex(Me.GroupFooter1, 0)
            Me.Controls.SetChildIndex(Me.GroupHeader1, 0)
            Me.Controls.SetChildIndex(Me.PageFooter, 0)
            Me.Controls.SetChildIndex(Me.PageHeader, 0)
            Me.Controls.SetChildIndex(Me.Detail, 0)
            CType(Me, System.ComponentModel.ISupportInitialize).EndInit()

        End Sub

#End Region

        Public Overrides ReadOnly Property ReportTitle() As String
            Get
                Return "Appointment Results By Office"
            End Get
        End Property

        Const TableName As String = "rpt_appt_results_by_office"
        Dim ds As New System.Data.DataSet("ds")
        Private Sub Report_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs)

            Dim tbl As System.Data.DataTable = ds.Tables(TableName)

            If tbl Is Nothing Then
                Dim current_cursor As System.Windows.Forms.Cursor = System.Windows.Forms.Cursor.Current
                Dim cn As SqlClient.SqlConnection = New System.Data.SqlClient.SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
                Try
                    System.Windows.Forms.Cursor.Current = Cursors.WaitCursor
                    cn.Open()
                    Using cmd As SqlClient.SqlCommand = New System.Data.SqlClient.SqlCommand()
                        With cmd
                            .Connection = cn
                            .CommandText = "rpt_appt_results_by_office"
                            .CommandType = CommandType.StoredProcedure

                            SqlClient.SqlCommandBuilder.DeriveParameters(cmd)
                            .Parameters(1).Value = Parameter_FromDate
                            .Parameters(2).Value = Parameter_ToDate
                            .CommandTimeout = 0
                        End With

                        Using da As New SqlClient.SqlDataAdapter(cmd)
                            da.Fill(ds, TableName)
                            tbl = ds.Tables(TableName)
                            tbl.Columns.Add("Tag", GetType(String), "[appt_results]+' '+[office]")
                        End Using
                    End Using

                Catch ex As Exception
                    DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error reading transactions")

                Finally
                    If cn IsNot Nothing Then cn.Dispose()

                    System.Windows.Forms.Cursor.Current = current_cursor
                End Try
            End If

            ' Bind the datasource to the items
            If tbl IsNot Nothing Then
                DataSource = New System.Data.DataView(tbl, String.Empty, "office, appt_results", DataViewRowState.CurrentRows)
            End If
        End Sub

        Private Class ReportThread
            Public Property ReportTitle As String
            Public Property ReportSubTitle As String
            Public Property MasterVue As DataView
            Public Property SubReportCriteria As String

            Public Sub RunThread()
                Using rpt As New ClientsSubReport(ReportTitle, ReportSubTitle)
                    AddHandler rpt.BeforePrint, AddressOf Report_BeforePrint
                    Using frm As New PrintPreviewForm(rpt)
                        frm.ShowDialog()
                    End Using
                End Using
            End Sub

            Private Sub Report_BeforePrint(ByVal Sender As Object, ByVal e As System.EventArgs)
                With CType(Sender, DevExpress.XtraReports.UI.XtraReport)

                    Dim CurrentCriteria As String = MasterVue.RowFilter
                    If CurrentCriteria <> String.Empty Then CurrentCriteria = String.Format(" AND ({0})", CurrentCriteria)
                    If SubReportCriteria <> String.Empty Then CurrentCriteria += String.Format(" AND ({0})", SubReportCriteria)

                    Dim FilterCriteria As String = CType(Sender, DebtPlus.Interfaces.Reports.IReportFilter).ReportFilter.ViewFilter
                    If FilterCriteria <> String.Empty Then CurrentCriteria += String.Format(" AND ({0})", FilterCriteria)
                    If CurrentCriteria <> String.Empty Then CurrentCriteria = CurrentCriteria.Substring(5)

                    ' Finally, set the data source
                    .DataSource = New System.Data.DataView(MasterVue.Table, CurrentCriteria, MasterVue.Sort + ", client", DataViewRowState.CurrentRows)
                End With
            End Sub
        End Class

        Private Sub DetailReport(ByVal sender As Object, ByVal e As DevExpress.XtraReports.UI.PreviewMouseEventArgs)
            Dim tag As String = DebtPlus.Utils.Nulls.DStr(e.Brick.Value)
            If tag <> String.Empty Then
                Dim NewClass As New ReportThread
                With NewClass
                    .MasterVue = CType(DataSource, DataView)
                    .SubReportCriteria = String.Format("Tag='{0}'", tag)

                    ' Split the tag into the component pieces
                    Dim office_name As String = String.Empty
                    Dim status As String = tag
                    Dim iPos As System.Int32 = tag.IndexOf(" "c)
                    If iPos > 0 Then
                        status = tag.Substring(0, iPos).Trim()
                        office_name = tag.Substring(iPos).Trim()
                    End If

                    .ReportTitle = ReportTitle
                    .ReportSubTitle = String.Format("{0} for status {1}", ReportSubTitle, status)

                    Dim thrd As New System.Threading.Thread(AddressOf .RunThread)
                    With thrd
                        .SetApartmentState(Threading.ApartmentState.STA)
                        .Name = "Subreport"
                        .IsBackground = True
                        .Start()
                    End With
                End With
            End If
        End Sub
    End Class
End Namespace
