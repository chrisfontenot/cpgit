#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}
#End Region
Imports DebtPlus.Reports.Template

Namespace Appointments.ResultsByOffice
    Friend Class ClientsSubReport
        Inherits TemplateXtraReportClass

        Public Sub New()
            MyBase.New()
            InitializeComponent()
            ReportFilter.IsEnabled = True
        End Sub

        Public Sub New(ByVal ReportTitle As String, ByVal ReportSubtitle As String)
            MyBase.New()
            InitializeComponent()
            privateReportSubtitle = ReportSubtitle
            privateReportTitle = ReportTitle
            ReportFilter.IsEnabled = True
        End Sub

        Private privateReportTitle As String = String.Empty
        Public Overrides ReadOnly Property ReportTitle() As String
            Get
                Return privateReportTitle
            End Get
        End Property

        Private privateReportSubtitle As String = String.Empty
        Public Overrides ReadOnly Property ReportSubTitle() As String
            Get
                Return privateReportSubtitle
            End Get
        End Property
    End Class
End Namespace