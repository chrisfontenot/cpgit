#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}
#End Region

Option Compare Binary
Option Explicit On 
Option Strict On

Imports DebtPlus.Reports
Imports DevExpress.XtraReports.UI

Namespace Custom.X0001.Zero
    Public Class X0001ZeroReport


        ''' <summary>
        ''' Create an instance of our report
        ''' </summary>
        Public Sub New()
            MyBase.New()
            InitializeComponent()
            AddHandler Me.BeforePrint, AddressOf X0001ZeroReport_BeforePrint
            AddHandler XrLabel_client.BeforePrint, AddressOf XrLabel_client_BeforePrint
        End Sub

        Public Overrides ReadOnly Property ReportTitle() As String
            Get
                Return "X0001 Disb Factor is Zero"
            End Get
        End Property

        Private ds As New System.Data.DataSet("ds")
        Private Sub X0001ZeroReport_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs)
            Const TableName As String = "rpt_custom_x0001_disb_fact_equal_0"

            Dim tbl As System.Data.DataTable
            Dim rpt As DevExpress.XtraReports.UI.XtraReport = CType(sender, DevExpress.XtraReports.UI.XtraReport)
            Dim sqlInfo As DebtPlus.LINQ.SQLInfoClass = DebtPlus.LINQ.SQLInfoClass.getDefault()
            Dim cn As System.Data.SqlClient.SqlConnection = New System.Data.SqlClient.SqlConnection(sqlInfo.ConnectionString)
            Try
                cn.Open()
                Using cmd As New System.Data.SqlClient.SqlCommand()
                    With cmd
                        .Connection = cn
                        .CommandText = "rpt_custom_x0001_disb_fact_equal_0"
                        .CommandType = System.Data.CommandType.StoredProcedure
                        .CommandTimeout = 0
                    End With

                    Using da As New System.Data.SqlClient.SqlDataAdapter(cmd)
                        da.Fill(ds, TableName)
                        tbl = ds.Tables(TableName)
                    End Using
                End Using

                If tbl IsNot Nothing Then
                    rpt.DataSource = tbl.DefaultView
                    For Each calc As DevExpress.XtraReports.UI.CalculatedField In rpt.CalculatedFields
                        calc.Assign(rpt.DataSource, rpt.DataMember)
                    Next
                End If

            Catch ex As System.Data.SqlClient.SqlException
                Dim gdr As New DebtPlus.Repository.GetDataResult() : gdr.HandleException(ex) : DebtPlus.UI.Common.ErrorHandling.HandleErrors(gdr, "Error reading report information")

            Finally
                If cn IsNot Nothing Then
                    If cn.State <> System.Data.ConnectionState.Closed Then cn.Close()
                    cn.Dispose()
                End If
            End Try
        End Sub

        Private Sub XrLabel_client_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs)
            With CType(sender, DevExpress.XtraReports.UI.XRLabel)
                Dim rpt As DevExpress.XtraReports.UI.XtraReport = CType(.Report, DevExpress.XtraReports.UI.XtraReport)
                .Text = DebtPlus.Utils.Format.Client.FormatClientID(rpt.GetCurrentColumnValue("client"))
            End With
        End Sub
    End Class
End Namespace
