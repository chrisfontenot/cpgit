#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}
#End Region

Namespace Proposals.OustandingRejects
    Public Class OutstandingRejectsReport
        Inherits Template.DatedTemplateXtraReportClass

        ''' <summary>
        ''' Create an instance of our report
        ''' </summary>
        Public Sub New()
            MyBase.New()
            InitializeComponent()
            AddHandler Me.BeforePrint, AddressOf Report_BeforePrint
            AddHandler XrPanel_detail.AfterPrint, AddressOf XrLabel_pannel_detail_AfterPrint
            AddHandler XrLabel_client.BeforePrint, AddressOf XrLabel_client_BeforePrint
            AddHandler GroupHeader1.BeforePrint, AddressOf GroupHeader1_BeforePrint
            AddHandler XrLabel_counselor.BeforePrint, AddressOf XrLabel_counselor_BeforePrint
            AddHandler XrLabel_cname.BeforePrint, AddressOf XrLabel_cname_BeforePrint
        End Sub

        Dim private_Parameter_Office As Object = System.DBNull.Value
        Public Property Parameter_Office() As Int32
            Get
                Dim parm As DevExpress.XtraReports.Parameters.Parameter = FindParameter("ParameterOffice")
                If parm Is Nothing Then Return -1
                Return Convert.ToInt32(parm.Value)
            End Get
            Set(ByVal value As Int32)
                SetParameter("ParameterOffice", GetType(Int32), value, "Office ID", False)
            End Set
        End Property

        Public Overrides ReadOnly Property ReportSubTitle() As String
            Get
                Dim Dates As String = MyBase.ReportSubtitle
                Dim offices As String
                If Parameter_Office < 0 Then
                    offices = "all offices"
                Else
                    offices = String.Format("office #{0:f0}", Convert.ToInt32(Parameter_Office))
                End If
                Return String.Format("{0} for {1}", Dates, offices)
            End Get
        End Property

        Public Overrides Sub SetReportParameter(ByVal Parameter As String, ByVal Value As Object)
            Select Case Parameter
                Case "Office"
                    Parameter_Office = Value
                Case Else
                    MyBase.SetReportParameter(Parameter, Value)
            End Select
        End Sub

        Public Overrides Function RequestReportParameters() As System.Windows.Forms.DialogResult
            Dim answer As System.Windows.Forms.DialogResult = DialogResult.OK
            If NeedParameters() Then
                Using frm As New DebtPlus.Reports.Template.Forms.DatedOfficeParametersForm(True)
                    answer = frm.ShowDialog()
                    Parameter_FromDate = frm.Parameter_FromDate
                    Parameter_ToDate = frm.Parameter_ToDate
                    Parameter_Office = If(Object.Equals(System.DBNull.Value, frm.Parameter_Office), -1, Convert.ToInt32(frm.Parameter_Office))
                End Using
            End If
            Return answer
        End Function

        Public Overrides ReadOnly Property ReportTitle() As String
            Get
                Return "Outstanding Proposal Rejected"
            End Get
        End Property

        Dim ds As New System.Data.DataSet("ds")

        Private Sub Report_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs)
            Dim tbl As System.Data.DataTable = ds.Tables("rpt_outstanding_proposal")

            If tbl Is Nothing Then
                Try
                    Using cn As SqlClient.SqlConnection = New System.Data.SqlClient.SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
                        cn.Open()
                        Using cmd As SqlClient.SqlCommand = New System.Data.SqlClient.SqlCommand()
                            cmd.Connection = cn
                            cmd.CommandText = "rpt_outstanding_proposal"
                            cmd.CommandType = CommandType.StoredProcedure
                            SqlClient.SqlCommandBuilder.DeriveParameters(cmd)
                            cmd.Parameters(1).Value = Parameter_FromDate
                            cmd.Parameters(2).Value = Parameter_ToDate
                            If Parameter_Office >= 0 Then
                                cmd.Parameters(3).Value = Parameter_Office
                            End If

                            cmd.CommandTimeout = 0

                            Using da As New SqlClient.SqlDataAdapter(cmd)
                                da.Fill(ds, "rpt_outstanding_proposal")
                                tbl = ds.Tables("rpt_outstanding_proposal")
                            End Using
                        End Using
                    End Using

                Catch ex As SqlClient.SqlException
                    Using gdr As New Repository.GetDataResult()
                        gdr.HandleException(ex)
                        DebtPlus.UI.Common.ErrorHandling.HandleErrors(gdr, "Error reading report information")
                    End Using
                End Try
            End If

            If tbl IsNot Nothing Then
                Dim vue As New System.Data.DataView(tbl, String.Empty, "client, creditor, crname, account", DataViewRowState.CurrentRows)
                DataSource = vue
            End If
        End Sub

        Private Sub XrLabel_pannel_detail_AfterPrint(ByVal sender As Object, ByVal e As System.EventArgs)
            ShouldPrintDetail = False
        End Sub

        Private Sub XrLabel_client_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs)
            If ShouldPrintDetail Then
                XrLabel_client.Text = String.Format("{0:0000000}", Convert.ToInt32(GetCurrentColumnValue("client")))
            Else
                XrLabel_client.Text = String.Empty
            End If
        End Sub

        Private ShouldPrintDetail As Boolean = True
        Private Sub GroupHeader1_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs)
            ShouldPrintDetail = True
        End Sub

        Private Sub XrLabel_counselor_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs)
            If ShouldPrintDetail Then
                XrLabel_counselor.Text = Convert.ToString(GetCurrentColumnValue("counselor"))
            Else
                XrLabel_counselor.Text = String.Empty
            End If
        End Sub

        Private Sub XrLabel_cname_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs)
            If ShouldPrintDetail Then
                XrLabel_cname.Text = Convert.ToString(GetCurrentColumnValue("cname"))
            Else
                XrLabel_cname.Text = String.Empty
            End If
        End Sub
    End Class
End Namespace
