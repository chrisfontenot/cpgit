﻿Option Compare Binary
Option Explicit On
Option Strict On

Imports DebtPlus.Reports
Imports DevExpress.XtraReports.UI

Namespace DailySummary.Transfers.CO
    <Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
    Partial Class DailySummaryTransfersCO
        Inherits DebtPlus.Reports.Template.DatedTemplateXtraReportClass

        'Component overrides dispose to clean up the component list.
        Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
            If disposing Then
                If components IsNot Nothing Then
                    components.Dispose()
                End If
            End If
            MyBase.Dispose(disposing)
        End Sub

        'Required by the Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Designer
        'It can be modified using the Designer.
        'Do not modify it using the code editor.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
            Dim XrSummary1 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary()
            Dim XrSummary2 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary()
            Dim XrSummary3 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary()
            Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(DailySummaryTransfersCO))
            Me.XrLabel_client = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_name = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_gross = New DevExpress.XtraReports.UI.XRLabel()
            Me.GroupHeader1 = New DevExpress.XtraReports.UI.GroupHeaderBand()
            Me.XrLabel7 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrPanel1 = New DevExpress.XtraReports.UI.XRPanel()
            Me.XrLabel5 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel6 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel4 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel3 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_item_date = New DevExpress.XtraReports.UI.XRLabel()
            Me.GroupFooter1 = New DevExpress.XtraReports.UI.GroupFooterBand()
            Me.XrLabel_group_gross = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel2 = New DevExpress.XtraReports.UI.XRLabel()
            Me.ReportFooter = New DevExpress.XtraReports.UI.ReportFooterBand()
            Me.XrLabel_total_gross = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel1 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_counselor = New DevExpress.XtraReports.UI.XRLabel()
            Me.GroupHeader2 = New DevExpress.XtraReports.UI.GroupHeaderBand()
            Me.GroupFooter2 = New DevExpress.XtraReports.UI.GroupFooterBand()
            Me.XrLabel_group2_gross = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel8 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_reason = New DevExpress.XtraReports.UI.XRLabel()
            Me.modified_item_date = New DevExpress.XtraReports.UI.CalculatedField()
            CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
            '
            'PageHeader
            '
            Me.PageHeader.HeightF = 119.0!
            '
            'XrLabel_Title
            '
            Me.XrLabel_Title.StylePriority.UseFont = False
            '
            'XrPageInfo_PageNumber
            '
            Me.XrPageInfo_PageNumber.StylePriority.UseTextAlignment = False
            '
            'XRLabel_Agency_Name
            '
            Me.XRLabel_Agency_Name.StylePriority.UseTextAlignment = False
            '
            'XrLabel_Agency_Address3
            '
            Me.XrLabel_Agency_Address3.StylePriority.UseFont = False
            Me.XrLabel_Agency_Address3.StylePriority.UseTextAlignment = False
            '
            'XrLabel_Agency_Address1
            '
            Me.XrLabel_Agency_Address1.StylePriority.UseFont = False
            Me.XrLabel_Agency_Address1.StylePriority.UseTextAlignment = False
            '
            'XrLabel_Agency_Phone
            '
            Me.XrLabel_Agency_Phone.StylePriority.UseFont = False
            Me.XrLabel_Agency_Phone.StylePriority.UseTextAlignment = False
            '
            'XrLabel_Agency_Address2
            '
            Me.XrLabel_Agency_Address2.StylePriority.UseFont = False
            Me.XrLabel_Agency_Address2.StylePriority.UseTextAlignment = False
            '
            'Detail
            '
            Me.Detail.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel_reason, Me.XrLabel_counselor, Me.XrLabel_gross, Me.XrLabel_name, Me.XrLabel_client})
            Me.Detail.HeightF = 15.0!
            '
            'XrLabel_client
            '
            Me.XrLabel_client.CanGrow = False
            Me.XrLabel_client.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "client", "{0:0000000}")})
            Me.XrLabel_client.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
            Me.XrLabel_client.Name = "XrLabel_client"
            Me.XrLabel_client.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_client.SizeF = New System.Drawing.SizeF(75.0!, 15.0!)
            Me.XrLabel_client.StylePriority.UseTextAlignment = False
            Me.XrLabel_client.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel_name
            '
            Me.XrLabel_name.CanGrow = False
            Me.XrLabel_name.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "name")})
            Me.XrLabel_name.LocationFloat = New DevExpress.Utils.PointFloat(83.0!, 0.0!)
            Me.XrLabel_name.Name = "XrLabel_name"
            Me.XrLabel_name.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_name.SizeF = New System.Drawing.SizeF(198.0!, 15.0!)
            Me.XrLabel_name.StylePriority.UseTextAlignment = False
            Me.XrLabel_name.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            Me.XrLabel_name.WordWrap = False
            '
            'XrLabel_gross
            '
            Me.XrLabel_gross.CanGrow = False
            Me.XrLabel_gross.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "gross", "{0:c}")})
            Me.XrLabel_gross.LocationFloat = New DevExpress.Utils.PointFloat(508.0!, 0.0!)
            Me.XrLabel_gross.Name = "XrLabel_gross"
            Me.XrLabel_gross.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_gross.SizeF = New System.Drawing.SizeF(75.0!, 15.0!)
            Me.XrLabel_gross.StylePriority.UseTextAlignment = False
            Me.XrLabel_gross.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'GroupHeader1
            '
            Me.GroupHeader1.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel7, Me.XrPanel1})
            Me.GroupHeader1.GroupFields.AddRange(New DevExpress.XtraReports.UI.GroupField() {New DevExpress.XtraReports.UI.GroupField("dest_account", DevExpress.XtraReports.UI.XRColumnSortOrder.Ascending)})
            Me.GroupHeader1.HeightF = 59.0!
            Me.GroupHeader1.Name = "GroupHeader1"
            '
            'XrLabel7
            '
            Me.XrLabel7.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel7.ForeColor = System.Drawing.Color.Teal
            Me.XrLabel7.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 8.0!)
            Me.XrLabel7.Name = "XrLabel7"
            Me.XrLabel7.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel7.SizeF = New System.Drawing.SizeF(800.0!, 17.0!)
            Me.XrLabel7.StylePriority.UseFont = False
            Me.XrLabel7.StylePriority.UseForeColor = False
            Me.XrLabel7.Text = "Transfers to account: [dest_account] [dest_description]"
            '
            'XrPanel1
            '
            Me.XrPanel1.BackColor = System.Drawing.Color.Teal
            Me.XrPanel1.BorderColor = System.Drawing.Color.Teal
            Me.XrPanel1.CanGrow = False
            Me.XrPanel1.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel5, Me.XrLabel6, Me.XrLabel4, Me.XrLabel3})
            Me.XrPanel1.KeepTogether = False
            Me.XrPanel1.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 33.0!)
            Me.XrPanel1.Name = "XrPanel1"
            Me.XrPanel1.SizeF = New System.Drawing.SizeF(800.0!, 18.0!)
            Me.XrPanel1.StylePriority.UseBackColor = False
            Me.XrPanel1.StylePriority.UseBorderColor = False
            '
            'XrLabel5
            '
            Me.XrLabel5.BackColor = System.Drawing.Color.Transparent
            Me.XrLabel5.BorderColor = System.Drawing.Color.Transparent
            Me.XrLabel5.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel5.ForeColor = System.Drawing.Color.White
            Me.XrLabel5.LocationFloat = New DevExpress.Utils.PointFloat(283.0!, 1.0!)
            Me.XrLabel5.Name = "XrLabel5"
            Me.XrLabel5.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel5.SizeF = New System.Drawing.SizeF(147.0!, 15.0!)
            Me.XrLabel5.StylePriority.UseBackColor = False
            Me.XrLabel5.StylePriority.UseBorderColor = False
            Me.XrLabel5.StylePriority.UseFont = False
            Me.XrLabel5.StylePriority.UseForeColor = False
            Me.XrLabel5.StylePriority.UseTextAlignment = False
            Me.XrLabel5.Text = "REASON"
            Me.XrLabel5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            '
            'XrLabel6
            '
            Me.XrLabel6.BackColor = System.Drawing.Color.Transparent
            Me.XrLabel6.BorderColor = System.Drawing.Color.Transparent
            Me.XrLabel6.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel6.ForeColor = System.Drawing.Color.White
            Me.XrLabel6.LocationFloat = New DevExpress.Utils.PointFloat(494.0!, 1.0!)
            Me.XrLabel6.Name = "XrLabel6"
            Me.XrLabel6.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel6.SizeF = New System.Drawing.SizeF(89.0!, 15.0!)
            Me.XrLabel6.StylePriority.UseBackColor = False
            Me.XrLabel6.StylePriority.UseBorderColor = False
            Me.XrLabel6.StylePriority.UseFont = False
            Me.XrLabel6.StylePriority.UseForeColor = False
            Me.XrLabel6.StylePriority.UseTextAlignment = False
            Me.XrLabel6.Text = "GROSS"
            Me.XrLabel6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel4
            '
            Me.XrLabel4.BackColor = System.Drawing.Color.Transparent
            Me.XrLabel4.BorderColor = System.Drawing.Color.Transparent
            Me.XrLabel4.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel4.ForeColor = System.Drawing.Color.White
            Me.XrLabel4.LocationFloat = New DevExpress.Utils.PointFloat(591.0!, 0.0!)
            Me.XrLabel4.Name = "XrLabel4"
            Me.XrLabel4.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel4.SizeF = New System.Drawing.SizeF(209.0!, 15.0!)
            Me.XrLabel4.StylePriority.UseBackColor = False
            Me.XrLabel4.StylePriority.UseBorderColor = False
            Me.XrLabel4.StylePriority.UseFont = False
            Me.XrLabel4.StylePriority.UseForeColor = False
            Me.XrLabel4.StylePriority.UseTextAlignment = False
            Me.XrLabel4.Text = "OPERATOR"
            Me.XrLabel4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            '
            'XrLabel3
            '
            Me.XrLabel3.BackColor = System.Drawing.Color.Transparent
            Me.XrLabel3.BorderColor = System.Drawing.Color.Transparent
            Me.XrLabel3.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel3.ForeColor = System.Drawing.Color.White
            Me.XrLabel3.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
            Me.XrLabel3.Name = "XrLabel3"
            Me.XrLabel3.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel3.SizeF = New System.Drawing.SizeF(175.0!, 15.0!)
            Me.XrLabel3.StylePriority.UseBackColor = False
            Me.XrLabel3.StylePriority.UseBorderColor = False
            Me.XrLabel3.StylePriority.UseFont = False
            Me.XrLabel3.StylePriority.UseForeColor = False
            Me.XrLabel3.StylePriority.UseTextAlignment = False
            Me.XrLabel3.Text = "CLIENT"
            Me.XrLabel3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            '
            'XrLabel_item_date
            '
            Me.XrLabel_item_date.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "item_date", "Transactions made on {0:d}")})
            Me.XrLabel_item_date.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel_item_date.ForeColor = System.Drawing.Color.Maroon
            Me.XrLabel_item_date.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 8.0!)
            Me.XrLabel_item_date.Name = "XrLabel_item_date"
            Me.XrLabel_item_date.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_item_date.SizeF = New System.Drawing.SizeF(800.0!, 25.0!)
            Me.XrLabel_item_date.StylePriority.UseFont = False
            Me.XrLabel_item_date.StylePriority.UseForeColor = False
            Me.XrLabel_item_date.StylePriority.UseTextAlignment = False
            Me.XrLabel_item_date.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
            '
            'GroupFooter1
            '
            Me.GroupFooter1.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel_group_gross, Me.XrLabel2})
            Me.GroupFooter1.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.GroupFooter1.HeightF = 24.0!
            Me.GroupFooter1.Name = "GroupFooter1"
            Me.GroupFooter1.StylePriority.UseFont = False
            Me.GroupFooter1.StylePriority.UseTextAlignment = False
            Me.GroupFooter1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel_group_gross
            '
            Me.XrLabel_group_gross.CanGrow = False
            Me.XrLabel_group_gross.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "gross")})
            Me.XrLabel_group_gross.LocationFloat = New DevExpress.Utils.PointFloat(450.0!, 8.0!)
            Me.XrLabel_group_gross.Name = "XrLabel_group_gross"
            Me.XrLabel_group_gross.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_group_gross.SizeF = New System.Drawing.SizeF(132.0!, 15.0!)
            Me.XrLabel_group_gross.StylePriority.UseTextAlignment = False
            XrSummary1.FormatString = "{0:c}"
            XrSummary1.Running = DevExpress.XtraReports.UI.SummaryRunning.Group
            Me.XrLabel_group_gross.Summary = XrSummary1
            Me.XrLabel_group_gross.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel2
            '
            Me.XrLabel2.CanGrow = False
            Me.XrLabel2.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 8.0!)
            Me.XrLabel2.Name = "XrLabel2"
            Me.XrLabel2.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel2.SizeF = New System.Drawing.SizeF(314.0!, 16.0!)
            Me.XrLabel2.StylePriority.UseTextAlignment = False
            Me.XrLabel2.Text = "ACCOUNT TOTAL"
            Me.XrLabel2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            '
            'ReportFooter
            '
            Me.ReportFooter.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel_total_gross, Me.XrLabel1})
            Me.ReportFooter.HeightF = 16.0!
            Me.ReportFooter.Name = "ReportFooter"
            '
            'XrLabel_total_gross
            '
            Me.XrLabel_total_gross.CanGrow = False
            Me.XrLabel_total_gross.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "gross")})
            Me.XrLabel_total_gross.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel_total_gross.LocationFloat = New DevExpress.Utils.PointFloat(450.0!, 0.0!)
            Me.XrLabel_total_gross.Name = "XrLabel_total_gross"
            Me.XrLabel_total_gross.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_total_gross.SizeF = New System.Drawing.SizeF(132.0!, 15.0!)
            Me.XrLabel_total_gross.StylePriority.UseFont = False
            Me.XrLabel_total_gross.StylePriority.UseTextAlignment = False
            XrSummary2.FormatString = "{0:c}"
            XrSummary2.Running = DevExpress.XtraReports.UI.SummaryRunning.Report
            Me.XrLabel_total_gross.Summary = XrSummary2
            Me.XrLabel_total_gross.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel1
            '
            Me.XrLabel1.CanGrow = False
            Me.XrLabel1.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel1.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
            Me.XrLabel1.Name = "XrLabel1"
            Me.XrLabel1.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel1.SizeF = New System.Drawing.SizeF(314.0!, 16.0!)
            Me.XrLabel1.StylePriority.UseFont = False
            Me.XrLabel1.StylePriority.UseTextAlignment = False
            Me.XrLabel1.Text = "GRAND TOTAL"
            Me.XrLabel1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            '
            'XrLabel_counselor
            '
            Me.XrLabel_counselor.CanGrow = False
            Me.XrLabel_counselor.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "counselor")})
            Me.XrLabel_counselor.LocationFloat = New DevExpress.Utils.PointFloat(591.0!, 0.0!)
            Me.XrLabel_counselor.Name = "XrLabel_counselor"
            Me.XrLabel_counselor.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_counselor.SizeF = New System.Drawing.SizeF(209.0!, 15.0!)
            Me.XrLabel_counselor.StylePriority.UseTextAlignment = False
            Me.XrLabel_counselor.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            Me.XrLabel_counselor.WordWrap = False
            '
            'GroupHeader2
            '
            Me.GroupHeader2.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel_item_date})
            Me.GroupHeader2.GroupFields.AddRange(New DevExpress.XtraReports.UI.GroupField() {New DevExpress.XtraReports.UI.GroupField("modified_item_date", DevExpress.XtraReports.UI.XRColumnSortOrder.Ascending)})
            Me.GroupHeader2.HeightF = 38.0!
            Me.GroupHeader2.Level = 1
            Me.GroupHeader2.Name = "GroupHeader2"
            '
            'GroupFooter2
            '
            Me.GroupFooter2.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel_group2_gross, Me.XrLabel8})
            Me.GroupFooter2.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.GroupFooter2.HeightF = 16.0!
            Me.GroupFooter2.Level = 1
            Me.GroupFooter2.Name = "GroupFooter2"
            Me.GroupFooter2.StylePriority.UseFont = False
            '
            'XrLabel_group2_gross
            '
            Me.XrLabel_group2_gross.CanGrow = False
            Me.XrLabel_group2_gross.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "gross")})
            Me.XrLabel_group2_gross.LocationFloat = New DevExpress.Utils.PointFloat(450.0!, 0.0!)
            Me.XrLabel_group2_gross.Name = "XrLabel_group2_gross"
            Me.XrLabel_group2_gross.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_group2_gross.SizeF = New System.Drawing.SizeF(132.0!, 15.0!)
            Me.XrLabel_group2_gross.StylePriority.UseTextAlignment = False
            XrSummary3.FormatString = "{0:c}"
            XrSummary3.Running = DevExpress.XtraReports.UI.SummaryRunning.Group
            Me.XrLabel_group2_gross.Summary = XrSummary3
            Me.XrLabel_group2_gross.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel8
            '
            Me.XrLabel8.CanGrow = False
            Me.XrLabel8.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
            Me.XrLabel8.Name = "XrLabel8"
            Me.XrLabel8.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel8.SizeF = New System.Drawing.SizeF(314.0!, 16.0!)
            Me.XrLabel8.StylePriority.UseTextAlignment = False
            Me.XrLabel8.Text = "DAILY TOTAL"
            Me.XrLabel8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            '
            'XrLabel_reason
            '
            Me.XrLabel_reason.CanGrow = False
            Me.XrLabel_reason.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "reason")})
            Me.XrLabel_reason.LocationFloat = New DevExpress.Utils.PointFloat(283.0!, 0.0!)
            Me.XrLabel_reason.Name = "XrLabel_reason"
            Me.XrLabel_reason.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_reason.SizeF = New System.Drawing.SizeF(216.0!, 15.0!)
            Me.XrLabel_reason.StylePriority.UseTextAlignment = False
            Me.XrLabel_reason.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            Me.XrLabel_reason.WordWrap = False
            '
            'modified_item_date
            '
            Me.modified_item_date.Expression = "GetDate([item_date])"
            Me.modified_item_date.Name = "modified_item_date"
            '
            'DailySummaryTransfersCO
            '
            Me.Bands.AddRange(New DevExpress.XtraReports.UI.Band() {Me.Detail, Me.PageHeader, Me.PageFooter, Me.TopMarginBand1, Me.BottomMarginBand1, Me.GroupHeader1, Me.GroupFooter1, Me.ReportFooter, Me.GroupHeader2, Me.GroupFooter2})
            Me.CalculatedFields.AddRange(New DevExpress.XtraReports.UI.CalculatedField() {Me.modified_item_date})
            Me.ScriptLanguage = DevExpress.XtraReports.ScriptLanguage.VisualBasic
            Me.ScriptReferencesString = resources.GetString("$this.ScriptReferencesString")
            Me.Scripts.OnBeforePrint = "DailySummaryTransfersCO_BeforePrint"
            Me.ScriptsSource = resources.GetString("$this.ScriptsSource")
            Me.StyleSheet.AddRange(New DevExpress.XtraReports.UI.XRControlStyle() {Me.XrControlStyle_Header, Me.XrControlStyle_Totals, Me.XrControlStyle_HeaderPannel, Me.XrControlStyle_GroupHeader})
            Me.Version = "11.2"
            Me.Controls.SetChildIndex(Me.GroupFooter2, 0)
            Me.Controls.SetChildIndex(Me.GroupHeader2, 0)
            Me.Controls.SetChildIndex(Me.ReportFooter, 0)
            Me.Controls.SetChildIndex(Me.GroupFooter1, 0)
            Me.Controls.SetChildIndex(Me.GroupHeader1, 0)
            Me.Controls.SetChildIndex(Me.BottomMarginBand1, 0)
            Me.Controls.SetChildIndex(Me.TopMarginBand1, 0)
            Me.Controls.SetChildIndex(Me.PageFooter, 0)
            Me.Controls.SetChildIndex(Me.PageHeader, 0)
            Me.Controls.SetChildIndex(Me.Detail, 0)
            CType(Me, System.ComponentModel.ISupportInitialize).EndInit()

        End Sub
        Friend WithEvents XrLabel_gross As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_name As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_client As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents GroupHeader1 As DevExpress.XtraReports.UI.GroupHeaderBand
        Friend WithEvents GroupFooter1 As DevExpress.XtraReports.UI.GroupFooterBand
        Friend WithEvents XrLabel2 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_item_date As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents ReportFooter As DevExpress.XtraReports.UI.ReportFooterBand
        Friend WithEvents XrLabel1 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_counselor As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_group_gross As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_total_gross As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrPanel1 As DevExpress.XtraReports.UI.XRPanel
        Friend WithEvents XrLabel6 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel4 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel3 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel7 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents GroupHeader2 As DevExpress.XtraReports.UI.GroupHeaderBand
        Friend WithEvents GroupFooter2 As DevExpress.XtraReports.UI.GroupFooterBand
        Friend WithEvents XrLabel_reason As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel5 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_group2_gross As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel8 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents modified_item_date As DevExpress.XtraReports.UI.CalculatedField

    End Class
End Namespace
