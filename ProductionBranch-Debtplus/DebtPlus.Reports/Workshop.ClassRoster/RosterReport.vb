#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}
#End Region

Option Compare Binary
Option Explicit On
Option Strict On

Imports System.Reflection
Imports System.IO
Imports DebtPlus.Utils

Namespace Workshop.ClassRoster
    Public Class RosterReport
        Inherits DebtPlus.Reports.Template.TemplateXtraReportClass

        Public Sub New()
            MyBase.new()

            ' Name of the report file
            Const ReportName As String = "DebtPlus.Reports.Workshop.ClassRoster.repx"

            ' See if there is a report reference that we can use
            Dim Fname As String = System.IO.Path.Combine(DebtPlus.Configuration.Config.ReportsDirectory, ReportName)
            Dim UseDefault As Boolean = True
            Try
                If (Not DebtPlus.Configuration.Config.LocalReportsOnly) AndAlso System.IO.File.Exists(Fname) Then
                    LoadLayout(Fname)
                    UseDefault = False
                End If

            Catch ex As System.Exception
                DebtPlus.Svc.Common.ErrorHandling.HandleErrors(ex)
            End Try

            ' Load the standard report definition if we need a new item
            If UseDefault Then
                Dim asm As Assembly = Assembly.GetExecutingAssembly                                                    ' changed
                Using ios As Stream = asm.GetManifestResourceStream(String.Format("DebtPlus.Reports.{0}", ReportName)) ' changed
                    If ios IsNot Nothing Then LoadLayout(ios) ' changed
                End Using
            End If

            ' Set the script references as needed
            ScriptReferences = DebtPlus.Utils.AssemblyTools.GetAllSiblingAssemblies()

            ' Update the subreports as well
            For Each BandPtr As DevExpress.XtraReports.UI.Band In Bands
                For Each CtlPtr As DevExpress.XtraReports.UI.XRControl In BandPtr.Controls
                    Dim Rpt As DevExpress.XtraReports.UI.XRSubreport = TryCast(CtlPtr, DevExpress.XtraReports.UI.XRSubreport)
                    If Rpt IsNot Nothing Then
                        Dim RptSrc As DevExpress.XtraReports.UI.XtraReport = TryCast(Rpt.ReportSource, DevExpress.XtraReports.UI.XtraReport)
                        If RptSrc IsNot Nothing Then
                            RptSrc.ScriptReferences = ScriptReferences
                        End If
                    End If
                Next
            Next

            SetParameter("ParameterWorkshop", GetType(Int32), -1, "Workshop", False)
            SetParameter("ParameterIncludeAddress", GetType(Boolean), False, "Include Address", False)
            SetParameter("ParameterIncludeEmail", GetType(Boolean), False, "Include Email", False)

            ReportFilter.IsEnabled = True
        End Sub

        Public Overrides ReadOnly Property ReportTitle() As String
            Get
                Return "Workshop Class Roster"
            End Get
        End Property

        Public Property Parameter_Workshop() As System.Int32
            Get
                Return Convert.ToInt32(Parameters("ParameterWorkshop").Value)
            End Get
            Set(ByVal Value As System.Int32)
                Parameters("ParameterWorkshop").Value = Value
            End Set
        End Property

        Public Property Parameter_IncludeAddress() As Boolean
            Get
                Return Convert.ToBoolean(Parameters("ParameterIncludeAddress").Value)
            End Get
            Set(ByVal Value As Boolean)
                Parameters("ParameterIncludeAddress").Value = Value
            End Set
        End Property

        Public Property Parameter_IncludeEmail() As Boolean
            Get
                Return Convert.ToBoolean(Parameters("ParameterIncludeEmail").Value)
            End Get
            Set(ByVal Value As Boolean)
                Parameters("ParameterIncludeEmail").Value = Value
            End Set
        End Property

        Public Overrides Sub SetReportParameter(ByVal Parameter As String, ByVal Value As Object)
            Select Case Parameter
                Case "Workshop"
                    Parameter_Workshop = Convert.ToInt32(Value)
                Case "IncludeAddress"
                    Parameter_IncludeAddress = Convert.ToBoolean(Value)
                Case "IncludeEmail"
                    Parameter_IncludeEmail = Convert.ToBoolean(Value)
                Case Else
                    MyBase.SetReportParameter(Parameter, Value)
            End Select
        End Sub

        Public Overrides Function RequestReportParameters() As System.Windows.Forms.DialogResult
            Dim answer As System.Windows.Forms.DialogResult
            Using frm As New ParametersForm
                answer = frm.ShowDialog()
                Parameter_Workshop = frm.Parameter_Workshop
                Parameter_IncludeEmail = frm.Parameter_IncludeEmail
                Parameter_IncludeAddress = frm.Parameter_IncludeAddress
            End Using
            Return answer
        End Function
    End Class
End Namespace
