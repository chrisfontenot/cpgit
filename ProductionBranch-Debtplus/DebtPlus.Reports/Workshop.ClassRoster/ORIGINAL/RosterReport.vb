#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}
#End Region

Option Compare Binary
Option Explicit On 
Option Strict On

Imports DevExpress.XtraReports.UI

Namespace Workshop.ClassRoster
    Public Class RosterReport

        ''' <summary>
        ''' Create an instance of our report
        ''' </summary>
        Public Sub New()
            MyBase.New()
            InitializeComponent()

            AddHandler BeforePrint, AddressOf Report_BeforePrint
            AddHandler XrLabel_Client.BeforePrint, AddressOf XrLabel_Client_BeforePrint
            AddHandler XrLabel_Address.BeforePrint, AddressOf XrLabel_Address_BeforePrint
            AddHandler XrTableCell_WorkshopID.BeforePrint, AddressOf XrTableCell_Workshop_WorkshopID_BeforePrint
            AddHandler XrTableCell_Workshop_Workshop.BeforePrint, AddressOf XrTableCell_Workshop_Workshop_BeforePrint
            AddHandler XrTableCell_Workshop_StartTime.BeforePrint, AddressOf XrTableCell_Workshop_StartTime_BeforePrint
            AddHandler XrTableCell_Workshop_Size.BeforePrint, AddressOf XrTableCell_Workshop_Size_BeforePrint
            AddHandler XrTableCell_Workshop_Duration.BeforePrint, AddressOf XrTableCell_Workshop_Duration_BeforePrint
            AddHandler XrTableCell_Workshop_Counselor.BeforePrint, AddressOf XrTableCell_Workshop_Counselor_BeforePrint
            AddHandler XrTableCell_Location_Directions.BeforePrint, AddressOf XrTableCell_Location_Directions_BeforePrint
            AddHandler XrTableCell_Location_Location.BeforePrint, AddressOf XrTableCell_Location_Location_BeforePrint
            AddHandler XrTableCell_Location_Type.BeforePrint, AddressOf XrTableCell_Location_Type_BeforePrint
            AddHandler XrTableCell_Guest_Name.BeforePrint, AddressOf XrTableCell_Guest_Name_BeforePrint
            AddHandler XrTableCell_Guest_Telephone.BeforePrint, AddressOf XrTableCell_Guest_Telephone_BeforePrint
            AddHandler XrTableCell_Guest_Email.BeforePrint, AddressOf XrTableCell_Guest_Email_BeforePrint

            Parameter_IncludeAddress = False
            Parameter_Workshop = -1
            Parameter_IncludeEmail = False

            ReportFilter.IsEnabled = True
        End Sub

        Public Overrides ReadOnly Property ReportTitle() As String
            Get
                Return "Workshop Class Roster"
            End Get
        End Property

        Public Property Parameter_Workshop() As System.Int32
            Get
                Dim parm As DevExpress.XtraReports.Parameters.Parameter = FindParameter("ParameterWorkshop")
                If parm Is Nothing Then Return -1
                Return Convert.ToInt32(parm.Value)
            End Get
            Set(ByVal Value As System.Int32)
                SetParameter("ParameterWorkshop", GetType(Int32), Value, "Workshop ID", False)
            End Set
        End Property

        Public Property Parameter_IncludeAddress() As Boolean
            Get
                Dim parm As DevExpress.XtraReports.Parameters.Parameter = FindParameter("ParameterIncludeAddress")
                If parm Is Nothing Then Return False
                Return Convert.ToBoolean(parm.Value)
            End Get
            Set(ByVal Value As Boolean)
                SetParameter("ParameterIncludeAddress", GetType(Boolean), Value, "Include Addresses?", False)
            End Set
        End Property

        Public Property Parameter_IncludeEmail() As Boolean
            Get
                Dim parm As DevExpress.XtraReports.Parameters.Parameter = FindParameter("ParameterIncludeEmail")
                If parm Is Nothing Then Return False
                Return Convert.ToBoolean(parm.Value)
            End Get
            Set(ByVal Value As Boolean)
                SetParameter("ParameterIncludeEmail", GetType(Boolean), Value, "Include Email?", False)
            End Set
        End Property

        Public Overrides Sub SetReportParameter(ByVal Parameter As String, ByVal Value As Object)
            Select Case Parameter
                Case "Workshop"
                    Parameter_Workshop = Convert.ToInt32(Value)
                Case "IncludeAddress"
                    Parameter_IncludeAddress = Convert.ToBoolean(Value)
                Case "IncludeEmail"
                    Parameter_IncludeEmail = Convert.ToBoolean(Value)
                Case Else
                    MyBase.SetReportParameter(Parameter, Value)
            End Select
        End Sub

        Public Overrides Function RequestReportParameters() As System.Windows.Forms.DialogResult
            Dim answer As System.Windows.Forms.DialogResult

            Using frm As New ParametersForm
                answer = frm.ShowDialog
                Parameter_Workshop = frm.Parameter_Workshop
                Parameter_IncludeEmail = frm.Parameter_IncludeEmail
                Parameter_IncludeAddress = frm.Parameter_IncludeAddress
            End Using

            Return answer
        End Function

        Dim ds As New System.Data.DataSet("ds")

        Protected Overridable Sub RosterReport_BeforePrint(ByVal sender As Object, ByVal e As System.EventArgs)
            Const TableName As String = "rpt_workshop_roster"
            Dim rpt As DevExpress.XtraReports.UI.XtraReport = CType(sender, DevExpress.XtraReports.UI.XtraReport)

            Dim tbl As System.Data.DataTable = ds.Tables(TableName)
            If tbl Is Nothing Then
                Dim sqlInfo As DebtPlus.LINQ.SQLInfoClass = DebtPlus.LINQ.SQLInfoClass.getDefault()
                Try
                    Using cn As System.Data.SqlClient.SqlConnection = New System.Data.SqlClient.SqlConnection(sqlInfo.ConnectionString)
                        cn.Open()
                        Dim Location As Int32 = -1

                        '-- Read the workshop detail information
                        Using cmd As New System.Data.SqlClient.SqlCommand()
                            cmd.Connection = cn
                            cmd.CommandText = "rpt_workshops"
                            cmd.CommandType = System.Data.CommandType.StoredProcedure
                            cmd.Parameters.Add("@workshop", System.Data.SqlDbType.Int).Value = rpt.Parameters("ParameterWorkshop").Value

                            Using da As New System.Data.SqlClient.SqlDataAdapter(cmd)
                                da.Fill(ds, "rpt_workshops")
                                Dim WorkshopTable As System.Data.DataTable = ds.Tables("rpt_workshops")
                                If WorkshopTable.PrimaryKey.GetUpperBound(0) < 0 Then
                                    WorkshopTable.PrimaryKey = New System.Data.DataColumn() {WorkshopTable.Columns("workshop")}

                                    If WorkshopTable.Rows.Count > 0 Then
                                        Location = DebtPlus.Utils.Nulls.DInt(WorkshopTable.Rows(0)("workshop_location"))
                                    End If
                                End If
                            End Using
                        End Using

                        '-- Read the workshop location information
                        If Location > 0 Then
                            Using cmd As New System.Data.SqlClient.SqlCommand()
                                cmd.Connection = cn
                                cmd.CommandText = "rpt_workshop_location"
                                cmd.CommandType = System.Data.CommandType.StoredProcedure
                                cmd.Parameters.Add("@location", System.Data.SqlDbType.Int).Value = Location

                                Using da As New System.Data.SqlClient.SqlDataAdapter(cmd)
                                    da.Fill(ds, "rpt_workshop_location")
                                End Using
                            End Using
                        End If

                        '-- Read the workshop roster
                        Using cmd As New System.Data.SqlClient.SqlCommand()
                            cmd.Connection = cn
                            cmd.CommandText = "rpt_workshop_roster"
                            cmd.CommandType = System.Data.CommandType.StoredProcedure
                            cmd.Parameters.Add("@workshop", System.Data.SqlDbType.Int).Value = rpt.Parameters("ParameterWorkshop").Value
                            cmd.CommandTimeout = 0

                            Using da As New System.Data.SqlClient.SqlDataAdapter(cmd)
                                da.Fill(ds, TableName)
                                tbl = ds.Tables(TableName)
                            End Using
                        End Using
                    End Using

                Catch ex As System.Data.SqlClient.SqlException
                    Using gdr As New DebtPlus.Repository.GetDataResult()
                        gdr.HandleException(ex)
                        DebtPlus.UI.Common.ErrorHandling.HandleErrors(gdr, "Error reading report information")
                    End Using
                End Try
            End If

            If tbl IsNot Nothing Then
                rpt.DataSource = New System.Data.DataView(tbl, CType(rpt, DebtPlus.Interfaces.Reports.IReportFilter).ReportFilter.ViewFilter, String.Empty, System.Data.DataViewRowState.CurrentRows)
            End If
        End Sub

        Private Shared Function WorkshopRow(ByVal rpt As DevExpress.XtraReports.UI.XtraReportBase) As System.Data.DataRow
            Dim src As System.Data.DataView = TryCast(CType(rpt, DevExpress.XtraReports.UI.XtraReport).DataSource, System.Data.DataView)
            If src IsNot Nothing Then
                Dim Workshop As Int32 = Convert.ToInt32(CType(rpt, DevExpress.XtraReports.UI.XtraReport).Parameters("ParameterWorkshop").Value)
                Dim ds As System.Data.DataSet = src.Table.DataSet
                Using tbl As System.Data.DataTable = ds.Tables("rpt_workshops")
                    If tbl IsNot Nothing Then
                        Return tbl.Rows(0)
                    End If
                End Using
            End If
            Return Nothing
        End Function

        Private Shared Function LocationRow(ByVal rpt As DevExpress.XtraReports.UI.XtraReportBase) As System.Data.DataRow
            Dim src As System.Data.DataView = TryCast(CType(rpt, DevExpress.XtraReports.UI.XtraReport).DataSource, System.Data.DataView)
            If src IsNot Nothing Then
                Dim ds As System.Data.DataSet = src.Table.DataSet
                Using tbl As System.Data.DataTable = ds.Tables("rpt_workshop_location")
                    If tbl IsNot Nothing Then
                        Return tbl.Rows(0)
                    End If
                End Using
            End If
            Return Nothing
        End Function

        Private Sub XrLabel_Client_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs)
            Dim lbl As DevExpress.XtraReports.UI.XRLabel = CType(sender, DevExpress.XtraReports.UI.XRLabel)
            lbl.Text = DebtPlus.Utils.Format.Client.FormatClientID(Convert.ToInt32(lbl.Report.GetCurrentColumnValue("client")))
        End Sub

        Private Sub XrLabel_Address_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs)
            Dim lbl As DevExpress.XtraReports.UI.XRLabel = CType(sender, DevExpress.XtraReports.UI.XRLabel)
            Dim rpt As DevExpress.XtraReports.UI.XtraReport = CType(lbl.Report, DevExpress.XtraReports.UI.XtraReport)
            Dim sb As New System.Text.StringBuilder()
            Dim strItem As String

            '-- Include the address
            If Convert.ToBoolean(rpt.Parameters("ParameterIncludeAddress").Value) Then
                For Each strKey As String In New String() {"addr_1", "addr_2", "addr_3"}
                    strItem = DebtPlus.Utils.Nulls.DStr(rpt.GetCurrentColumnValue(strKey)).Trim
                    If strItem <> System.String.Empty Then
                        sb.Append(strItem)
                        sb.Append(Environment.NewLine)
                    End If
                Next strKey
            End If

            '-- Include the email address
            If Convert.ToBoolean(rpt.Parameters("ParameterIncludeEmail").Value) Then
                Try
                    strItem = DebtPlus.Utils.Nulls.DStr(rpt.GetCurrentColumnValue("email")).Trim
                    If strItem <> System.String.Empty Then
                        sb.Append("Email: ")
                        sb.Append(strItem)
                        sb.Append(Environment.NewLine)
                    End If

                Catch ex As Exception
                    ' Ignore errors if email is not included in the result set
                End Try
            End If

            '-- Append a blank line
            sb.Append(" "c)

            '-- Update the text with the buffer
            lbl.Text = sb.ToString()
        End Sub

        Private Sub XrTableCell_Workshop_WorkshopID_BeforePrint(sender As Object, e As System.Drawing.Printing.PrintEventArgs)
            Dim cell As DevExpress.XtraReports.UI.XRTableCell = CType(sender, DevExpress.XtraReports.UI.XRTableCell)
            Dim row As System.Data.DataRow = WorkshopRow(cell.Report)
            If row IsNot Nothing Then
                cell.Text = String.Format("{0:0000000}", row("workshop"))
            End If
        End Sub

        Private Sub XrTableCell_Workshop_Workshop_BeforePrint(sender As Object, e As System.Drawing.Printing.PrintEventArgs)
            Dim cell As DevExpress.XtraReports.UI.XRTableCell = CType(sender, DevExpress.XtraReports.UI.XRTableCell)
            Dim row As System.Data.DataRow = WorkshopRow(cell.Report)
            If row IsNot Nothing Then
                cell.Text = DebtPlus.Utils.Nulls.DStr(row("workshop_description"))
            End If
        End Sub

        Private Sub XrTableCell_Workshop_StartTime_BeforePrint(sender As Object, e As System.Drawing.Printing.PrintEventArgs)
            Dim cell As DevExpress.XtraReports.UI.XRTableCell = CType(sender, DevExpress.XtraReports.UI.XRTableCell)
            Dim row As System.Data.DataRow = WorkshopRow(cell.Report)
            If row IsNot Nothing Then
                cell.Text = String.Format("{0:d} {0:T}", row("start_time"))
            End If
        End Sub

        Private Sub XrTableCell_Workshop_Size_BeforePrint(sender As Object, e As System.Drawing.Printing.PrintEventArgs)
            Dim cell As DevExpress.XtraReports.UI.XRTableCell = CType(sender, DevExpress.XtraReports.UI.XRTableCell)
            Dim row As System.Data.DataRow = WorkshopRow(cell.Report)
            If row IsNot Nothing Then
                cell.Text = String.Format("{0:n0}", row("seats_available"))
            End If
        End Sub

        Private Sub XrTableCell_Workshop_Duration_BeforePrint(sender As Object, e As System.Drawing.Printing.PrintEventArgs)
            Dim cell As DevExpress.XtraReports.UI.XRTableCell = CType(sender, DevExpress.XtraReports.UI.XRTableCell)
            Dim row As System.Data.DataRow = WorkshopRow(cell.Report)
            If row IsNot Nothing Then
                cell.Text = DebtPlus.Utils.Format.Time.HoursAndMinutes(row("workshop_duration"))
            End If
        End Sub

        Private Sub XrTableCell_Workshop_Counselor_BeforePrint(sender As Object, e As System.Drawing.Printing.PrintEventArgs)
            Dim cell As DevExpress.XtraReports.UI.XRTableCell = CType(sender, DevExpress.XtraReports.UI.XRTableCell)
            Dim row As System.Data.DataRow = WorkshopRow(cell.Report)
            If row IsNot Nothing Then
                cell.Text = DebtPlus.Utils.Nulls.DStr(row("counselor")).Trim
            End If
        End Sub

        Private Sub XrTableCell_Location_Directions_BeforePrint(sender As Object, e As System.Drawing.Printing.PrintEventArgs)
            Dim cell As DevExpress.XtraReports.UI.XRTableCell = CType(sender, DevExpress.XtraReports.UI.XRTableCell)
            Dim row As System.Data.DataRow = LocationRow(cell.Report)
            If row IsNot Nothing Then
                cell.Text = DebtPlus.Utils.Nulls.DStr(row("location_directions")).Trim
            End If
        End Sub

        Private Sub XrTableCell_Location_Location_BeforePrint(sender As Object, e As System.Drawing.Printing.PrintEventArgs)
            Dim cell As DevExpress.XtraReports.UI.XRTableCell = CType(sender, DevExpress.XtraReports.UI.XRTableCell)
            Dim row As System.Data.DataRow = LocationRow(cell.Report)
            If row IsNot Nothing Then
                Dim workshop_name As String = DebtPlus.Utils.Nulls.DStr(row("location_name"))
                Dim workshop_address1 As String = DebtPlus.Utils.Nulls.DStr(row("location_address1"))
                Dim workshop_address2 As String = DebtPlus.Utils.Nulls.DStr(row("location_address2"))
                Dim workshop_address3 As String = DebtPlus.Utils.Nulls.DStr(row("location_address3"))

                Dim Combined As New System.Text.StringBuilder
                For Each Str As String In New String() {workshop_name, workshop_address1, workshop_address2, workshop_address3}
                    Str = Str.Trim
                    If Str <> String.Empty Then
                        Combined.Append(System.Environment.NewLine)
                        Combined.Append(Str)
                    End If
                Next

                If Combined.Length > 0 Then Combined.Remove(0, 2)
                cell.Text = Combined.ToString()
            End If
        End Sub

        Private Sub XrTableCell_Location_Type_BeforePrint(sender As Object, e As System.Drawing.Printing.PrintEventArgs)
            Dim cell As DevExpress.XtraReports.UI.XRTableCell = CType(sender, DevExpress.XtraReports.UI.XRTableCell)
            Dim row As System.Data.DataRow = LocationRow(cell.Report)
            If row IsNot Nothing Then
                cell.Text = DebtPlus.Utils.Nulls.DStr(row("organization_type")).Trim
            End If
        End Sub

        Private Sub XrTableCell_Guest_Name_BeforePrint(sender As Object, e As System.Drawing.Printing.PrintEventArgs)
            Dim cell As DevExpress.XtraReports.UI.XRTableCell = CType(sender, DevExpress.XtraReports.UI.XRTableCell)
            Dim row As System.Data.DataRow = WorkshopRow(cell.Report)
            If row IsNot Nothing Then
                cell.Text = DebtPlus.Utils.Nulls.DStr(row("Guest_Name")).Trim
            End If
        End Sub

        Private Sub XrTableCell_Guest_Email_BeforePrint(sender As Object, e As System.Drawing.Printing.PrintEventArgs)
            Dim cell As DevExpress.XtraReports.UI.XRTableCell = CType(sender, DevExpress.XtraReports.UI.XRTableCell)
            Dim row As System.Data.DataRow = WorkshopRow(cell.Report)
            If row IsNot Nothing Then
                cell.Text = DebtPlus.Utils.Nulls.DStr(row("Guest_Email")).Trim
            End If
        End Sub

        Private Sub XrTableCell_Guest_Telephone_BeforePrint(sender As Object, e As System.Drawing.Printing.PrintEventArgs)
            Dim cell As DevExpress.XtraReports.UI.XRTableCell = CType(sender, DevExpress.XtraReports.UI.XRTableCell)
            Dim row As System.Data.DataRow = WorkshopRow(cell.Report)
            If row IsNot Nothing Then
                cell.Text = DebtPlus.Utils.Nulls.DStr(row("Guest_Telephone")).Trim
            End If
        End Sub
    End Class
End Namespace
