Namespace Workshop.ClassRoster
    <Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
    Partial Class RosterReport
        Inherits DebtPlus.Reports.Template.TemplateXtraReportClass

        'Component overrides dispose to clean up the component list.
        Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
            If disposing Then
                If components IsNot Nothing Then
                    components.Dispose()
                End If
            End If
            MyBase.Dispose(disposing)
        End Sub

        'Required by the Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Designer
        'It can be modified using the Designer.
        'Do not modify it using the code editor.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
            Dim XrSummary1 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary()
            Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(RosterReport))
            Me.XrLabel_Reference = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_Phone = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_ClientName = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_Status = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_Count = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_Client = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_Address = New DevExpress.XtraReports.UI.XRLabel()
            Me.ReportFooter = New DevExpress.XtraReports.UI.ReportFooterBand()
            Me.XrLabel_Total_Count = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel16 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLine1 = New DevExpress.XtraReports.UI.XRLine()
            Me.GroupHeader1 = New DevExpress.XtraReports.UI.GroupHeaderBand()
            Me.XrPanel1 = New DevExpress.XtraReports.UI.XRPanel()
            Me.XrLabel15 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel14 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel13 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel12 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel11 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel4 = New DevExpress.XtraReports.UI.XRLabel()
            Me.GroupHeader3 = New DevExpress.XtraReports.UI.GroupHeaderBand()
            Me.XrTable1 = New DevExpress.XtraReports.UI.XRTable()
            Me.XrTableRow1 = New DevExpress.XtraReports.UI.XRTableRow()
            Me.XrTableCell1 = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableCell_WorkshopID = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableCell4 = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableCell_Guest_Name = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableRow2 = New DevExpress.XtraReports.UI.XRTableRow()
            Me.XrTableCell5 = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableCell_Workshop_Workshop = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableCell7 = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableCell_Guest_Email = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableRow3 = New DevExpress.XtraReports.UI.XRTableRow()
            Me.XrTableCell9 = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableCell_Workshop_StartTime = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableCell11 = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableCell_Guest_Telephone = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableRow4 = New DevExpress.XtraReports.UI.XRTableRow()
            Me.XrTableCell13 = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableCell_Workshop_Duration = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableCell15 = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableCell16 = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableRow5 = New DevExpress.XtraReports.UI.XRTableRow()
            Me.XrTableCell17 = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableCell_Workshop_Size = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableCell19 = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableCell_Location_Type = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableRow6 = New DevExpress.XtraReports.UI.XRTableRow()
            Me.XrTableCell21 = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableCell_Workshop_Counselor = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableCell23 = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableCell_Location_Location = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableRow7 = New DevExpress.XtraReports.UI.XRTableRow()
            Me.XrTableCell27 = New DevExpress.XtraReports.UI.XRTableCell()
            Me.XrTableCell_Location_Directions = New DevExpress.XtraReports.UI.XRTableCell()
            Me.ParameterWorkshop = New DevExpress.XtraReports.Parameters.Parameter()
            Me.ParameterIncludeEmail = New DevExpress.XtraReports.Parameters.Parameter()
            Me.ParameterIncludeAddress = New DevExpress.XtraReports.Parameters.Parameter()
            CType(Me.XrTable1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
            '
            'PageHeader
            '
            Me.PageHeader.HeightF = 111.0834!
            '
            'XrLabel_Title
            '
            Me.XrLabel_Title.StylePriority.UseFont = False
            '
            'XrPageInfo_PageNumber
            '
            Me.XrPageInfo_PageNumber.StylePriority.UseTextAlignment = False
            '
            'XRLabel_Agency_Name
            '
            Me.XRLabel_Agency_Name.StylePriority.UseTextAlignment = False
            '
            'XrLabel_Agency_Address3
            '
            Me.XrLabel_Agency_Address3.StylePriority.UseFont = False
            Me.XrLabel_Agency_Address3.StylePriority.UseTextAlignment = False
            '
            'XrLabel_Agency_Address1
            '
            Me.XrLabel_Agency_Address1.StylePriority.UseFont = False
            Me.XrLabel_Agency_Address1.StylePriority.UseTextAlignment = False
            '
            'XrLabel_Agency_Phone
            '
            Me.XrLabel_Agency_Phone.StylePriority.UseFont = False
            Me.XrLabel_Agency_Phone.StylePriority.UseTextAlignment = False
            '
            'XrLabel_Agency_Address2
            '
            Me.XrLabel_Agency_Address2.StylePriority.UseFont = False
            Me.XrLabel_Agency_Address2.StylePriority.UseTextAlignment = False
            '
            'Detail
            '
            Me.Detail.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel_Address, Me.XrLabel_Client, Me.XrLabel_Count, Me.XrLabel_Status, Me.XrLabel_ClientName, Me.XrLabel_Phone, Me.XrLabel_Reference})
            Me.Detail.HeightF = 34.0!
            '
            'XrLabel_Reference
            '
            Me.XrLabel_Reference.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "client_referral")})
            Me.XrLabel_Reference.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.XrLabel_Reference.ForeColor = System.Drawing.Color.Black
            Me.XrLabel_Reference.LocationFloat = New DevExpress.Utils.PointFloat(608.0!, 0.0!)
            Me.XrLabel_Reference.Name = "XrLabel_Reference"
            Me.XrLabel_Reference.SizeF = New System.Drawing.SizeF(142.0!, 17.0!)
            Me.XrLabel_Reference.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            '
            'XrLabel_Phone
            '
            Me.XrLabel_Phone.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "home_ph")})
            Me.XrLabel_Phone.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.XrLabel_Phone.ForeColor = System.Drawing.Color.Black
            Me.XrLabel_Phone.LocationFloat = New DevExpress.Utils.PointFloat(433.0!, 0.0!)
            Me.XrLabel_Phone.Name = "XrLabel_Phone"
            Me.XrLabel_Phone.SizeF = New System.Drawing.SizeF(100.0!, 17.0!)
            Me.XrLabel_Phone.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            '
            'XrLabel_ClientName
            '
            Me.XrLabel_ClientName.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "client_name")})
            Me.XrLabel_ClientName.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.XrLabel_ClientName.ForeColor = System.Drawing.Color.Black
            Me.XrLabel_ClientName.LocationFloat = New DevExpress.Utils.PointFloat(116.0!, 0.0!)
            Me.XrLabel_ClientName.Name = "XrLabel_ClientName"
            Me.XrLabel_ClientName.SizeF = New System.Drawing.SizeF(308.0!, 17.0!)
            Me.XrLabel_ClientName.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            '
            'XrLabel_Status
            '
            Me.XrLabel_Status.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "active_status")})
            Me.XrLabel_Status.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.XrLabel_Status.ForeColor = System.Drawing.Color.Black
            Me.XrLabel_Status.LocationFloat = New DevExpress.Utils.PointFloat(72.0!, 0.0!)
            Me.XrLabel_Status.Name = "XrLabel_Status"
            Me.XrLabel_Status.SizeF = New System.Drawing.SizeF(37.0!, 17.0!)
            Me.XrLabel_Status.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
            '
            'XrLabel_Count
            '
            Me.XrLabel_Count.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "attending", "{0:f0}")})
            Me.XrLabel_Count.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.XrLabel_Count.ForeColor = System.Drawing.Color.Black
            Me.XrLabel_Count.LocationFloat = New DevExpress.Utils.PointFloat(541.0!, 0.0!)
            Me.XrLabel_Count.Name = "XrLabel_Count"
            Me.XrLabel_Count.SizeF = New System.Drawing.SizeF(58.0!, 17.0!)
            Me.XrLabel_Count.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel_Client
            '
            Me.XrLabel_Client.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.XrLabel_Client.ForeColor = System.Drawing.Color.Black
            Me.XrLabel_Client.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
            Me.XrLabel_Client.Name = "XrLabel_Client"
            Me.XrLabel_Client.Scripts.OnBeforePrint = "XrLabel_Client_BeforePrint"
            Me.XrLabel_Client.SizeF = New System.Drawing.SizeF(71.99999!, 17.00001!)
            Me.XrLabel_Client.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel_Address
            '
            Me.XrLabel_Address.LocationFloat = New DevExpress.Utils.PointFloat(140.0!, 17.0!)
            Me.XrLabel_Address.Multiline = True
            Me.XrLabel_Address.Name = "XrLabel_Address"
            Me.XrLabel_Address.Scripts.OnBeforePrint = "XrLabel_Address_BeforePrint"
            Me.XrLabel_Address.SizeF = New System.Drawing.SizeF(608.0!, 17.0!)
            Me.XrLabel_Address.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            '
            'ReportFooter
            '
            Me.ReportFooter.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel_Total_Count, Me.XrLabel16, Me.XrLine1})
            Me.ReportFooter.HeightF = 40.0!
            Me.ReportFooter.Name = "ReportFooter"
            Me.ReportFooter.PrintAtBottom = True
            '
            'XrLabel_Total_Count
            '
            Me.XrLabel_Total_Count.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "attending")})
            Me.XrLabel_Total_Count.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.XrLabel_Total_Count.LocationFloat = New DevExpress.Utils.PointFloat(499.0!, 17.0!)
            Me.XrLabel_Total_Count.Name = "XrLabel_Total_Count"
            Me.XrLabel_Total_Count.SizeF = New System.Drawing.SizeF(100.0!, 23.0!)
            XrSummary1.FormatString = "{0:f0}"
            XrSummary1.Running = DevExpress.XtraReports.UI.SummaryRunning.Report
            Me.XrLabel_Total_Count.Summary = XrSummary1
            Me.XrLabel_Total_Count.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel16
            '
            Me.XrLabel16.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.XrLabel16.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 17.0!)
            Me.XrLabel16.Name = "XrLabel16"
            Me.XrLabel16.SizeF = New System.Drawing.SizeF(283.0!, 23.0!)
            Me.XrLabel16.Text = "TOTAL ATTENDANCE:"
            Me.XrLabel16.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            '
            'XrLine1
            '
            Me.XrLine1.ForeColor = System.Drawing.Color.Teal
            Me.XrLine1.LineWidth = 2
            Me.XrLine1.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 8.0!)
            Me.XrLine1.Name = "XrLine1"
            Me.XrLine1.SizeF = New System.Drawing.SizeF(600.0!, 9.0!)
            '
            'GroupHeader1
            '
            Me.GroupHeader1.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrPanel1})
            Me.GroupHeader1.HeightF = 36.45833!
            Me.GroupHeader1.Name = "GroupHeader1"
            Me.GroupHeader1.RepeatEveryPage = True
            '
            'XrPanel1
            '
            Me.XrPanel1.BackColor = System.Drawing.Color.Teal
            Me.XrPanel1.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel15, Me.XrLabel14, Me.XrLabel13, Me.XrLabel12, Me.XrLabel11, Me.XrLabel4})
            Me.XrPanel1.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 10.0!)
            Me.XrPanel1.Name = "XrPanel1"
            Me.XrPanel1.SizeF = New System.Drawing.SizeF(750.0!, 17.0!)
            '
            'XrLabel15
            '
            Me.XrLabel15.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.XrLabel15.ForeColor = System.Drawing.Color.White
            Me.XrLabel15.LocationFloat = New DevExpress.Utils.PointFloat(608.0!, 0.0!)
            Me.XrLabel15.Name = "XrLabel15"
            Me.XrLabel15.SizeF = New System.Drawing.SizeF(123.0!, 17.0!)
            Me.XrLabel15.Text = "REFERENCE"
            Me.XrLabel15.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            '
            'XrLabel14
            '
            Me.XrLabel14.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.XrLabel14.ForeColor = System.Drawing.Color.White
            Me.XrLabel14.LocationFloat = New DevExpress.Utils.PointFloat(541.0!, 0.0!)
            Me.XrLabel14.Name = "XrLabel14"
            Me.XrLabel14.SizeF = New System.Drawing.SizeF(58.0!, 17.0!)
            Me.XrLabel14.Text = "COUNT"
            Me.XrLabel14.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel13
            '
            Me.XrLabel13.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.XrLabel13.ForeColor = System.Drawing.Color.White
            Me.XrLabel13.LocationFloat = New DevExpress.Utils.PointFloat(433.0!, 0.0!)
            Me.XrLabel13.Name = "XrLabel13"
            Me.XrLabel13.SizeF = New System.Drawing.SizeF(100.0!, 17.0!)
            Me.XrLabel13.Text = "PHONE"
            Me.XrLabel13.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            '
            'XrLabel12
            '
            Me.XrLabel12.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.XrLabel12.ForeColor = System.Drawing.Color.White
            Me.XrLabel12.LocationFloat = New DevExpress.Utils.PointFloat(116.0!, 0.0!)
            Me.XrLabel12.Name = "XrLabel12"
            Me.XrLabel12.SizeF = New System.Drawing.SizeF(308.0!, 17.0!)
            Me.XrLabel12.Text = "CLIENT NAME"
            Me.XrLabel12.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            '
            'XrLabel11
            '
            Me.XrLabel11.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.XrLabel11.ForeColor = System.Drawing.Color.White
            Me.XrLabel11.LocationFloat = New DevExpress.Utils.PointFloat(72.0!, 0.0!)
            Me.XrLabel11.Name = "XrLabel11"
            Me.XrLabel11.SizeF = New System.Drawing.SizeF(37.0!, 17.0!)
            Me.XrLabel11.Text = "STS"
            Me.XrLabel11.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
            '
            'XrLabel4
            '
            Me.XrLabel4.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.XrLabel4.ForeColor = System.Drawing.Color.White
            Me.XrLabel4.LocationFloat = New DevExpress.Utils.PointFloat(14.0!, 0.0!)
            Me.XrLabel4.Name = "XrLabel4"
            Me.XrLabel4.SizeF = New System.Drawing.SizeF(58.0!, 17.0!)
            Me.XrLabel4.Text = "CLIENT"
            Me.XrLabel4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'GroupHeader3
            '
            Me.GroupHeader3.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrTable1})
            Me.GroupHeader3.HeightF = 105.0!
            Me.GroupHeader3.Level = 1
            Me.GroupHeader3.Name = "GroupHeader3"
            '
            'XrTable1
            '
            Me.XrTable1.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
            Me.XrTable1.Name = "XrTable1"
            Me.XrTable1.Rows.AddRange(New DevExpress.XtraReports.UI.XRTableRow() {Me.XrTableRow1, Me.XrTableRow2, Me.XrTableRow3, Me.XrTableRow4, Me.XrTableRow5, Me.XrTableRow6, Me.XrTableRow7})
            Me.XrTable1.SizeF = New System.Drawing.SizeF(734.0!, 105.0!)
            '
            'XrTableRow1
            '
            Me.XrTableRow1.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.XrTableCell1, Me.XrTableCell_WorkshopID, Me.XrTableCell4, Me.XrTableCell_Guest_Name})
            Me.XrTableRow1.Name = "XrTableRow1"
            Me.XrTableRow1.Weight = 1.0R
            '
            'XrTableCell1
            '
            Me.XrTableCell1.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrTableCell1.Name = "XrTableCell1"
            Me.XrTableCell1.StylePriority.UseFont = False
            Me.XrTableCell1.Text = "WORKSHOP ID"
            Me.XrTableCell1.Weight = 1.2949999754695216R
            '
            'XrTableCell_WorkshopID
            '
            Me.XrTableCell_WorkshopID.Name = "XrTableCell_WorkshopID"
            Me.XrTableCell_WorkshopID.Scripts.OnBeforePrint = "XrTableCell_WorkshopID_BeforePrint"
            Me.XrTableCell_WorkshopID.Weight = 2.4450003297062595R
            '
            'XrTableCell4
            '
            Me.XrTableCell4.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrTableCell4.Name = "XrTableCell4"
            Me.XrTableCell4.StylePriority.UseFont = False
            Me.XrTableCell4.Text = "GUEST"
            Me.XrTableCell4.Weight = 1.0099996948242187R
            '
            'XrTableCell_Guest_Name
            '
            Me.XrTableCell_Guest_Name.Name = "XrTableCell_Guest_Name"
            Me.XrTableCell_Guest_Name.Scripts.OnBeforePrint = "XrTableCell_Guest_Name_BeforePrint"
            Me.XrTableCell_Guest_Name.Weight = 2.5899993896484377R
            '
            'XrTableRow2
            '
            Me.XrTableRow2.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.XrTableCell5, Me.XrTableCell_Workshop_Workshop, Me.XrTableCell7, Me.XrTableCell_Guest_Email})
            Me.XrTableRow2.Name = "XrTableRow2"
            Me.XrTableRow2.Weight = 1.0R
            '
            'XrTableCell5
            '
            Me.XrTableCell5.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrTableCell5.Name = "XrTableCell5"
            Me.XrTableCell5.StylePriority.UseFont = False
            Me.XrTableCell5.Text = "WORKSHOP"
            Me.XrTableCell5.Weight = 1.2949999754695216R
            '
            'XrTableCell_Workshop_Workshop
            '
            Me.XrTableCell_Workshop_Workshop.Name = "XrTableCell_Workshop_Workshop"
            Me.XrTableCell_Workshop_Workshop.Scripts.OnBeforePrint = "XrTableCell_Workshop_Workshop_BeforePrint"
            Me.XrTableCell_Workshop_Workshop.Weight = 2.4450003297062595R
            '
            'XrTableCell7
            '
            Me.XrTableCell7.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrTableCell7.Name = "XrTableCell7"
            Me.XrTableCell7.StylePriority.UseFont = False
            Me.XrTableCell7.Text = "GUEST EMAIL"
            Me.XrTableCell7.Weight = 1.0099996948242187R
            '
            'XrTableCell_Guest_Email
            '
            Me.XrTableCell_Guest_Email.Name = "XrTableCell_Guest_Email"
            Me.XrTableCell_Guest_Email.Scripts.OnBeforePrint = "XrTableCell_Guest_Email_BeforePrint"
            Me.XrTableCell_Guest_Email.Weight = 2.5899993896484377R
            '
            'XrTableRow3
            '
            Me.XrTableRow3.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.XrTableCell9, Me.XrTableCell_Workshop_StartTime, Me.XrTableCell11, Me.XrTableCell_Guest_Telephone})
            Me.XrTableRow3.Name = "XrTableRow3"
            Me.XrTableRow3.Weight = 1.0R
            '
            'XrTableCell9
            '
            Me.XrTableCell9.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrTableCell9.Name = "XrTableCell9"
            Me.XrTableCell9.StylePriority.UseFont = False
            Me.XrTableCell9.Text = "START TIME"
            Me.XrTableCell9.Weight = 1.2949999754695216R
            '
            'XrTableCell_Workshop_StartTime
            '
            Me.XrTableCell_Workshop_StartTime.Name = "XrTableCell_Workshop_StartTime"
            Me.XrTableCell_Workshop_StartTime.Scripts.OnBeforePrint = "XrTableCell_Workshop_StartTime_BeforePrint"
            Me.XrTableCell_Workshop_StartTime.Weight = 2.4450003297062595R
            '
            'XrTableCell11
            '
            Me.XrTableCell11.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrTableCell11.Name = "XrTableCell11"
            Me.XrTableCell11.StylePriority.UseFont = False
            Me.XrTableCell11.Text = "GUEST PHONE"
            Me.XrTableCell11.Weight = 1.0099996948242187R
            '
            'XrTableCell_Guest_Telephone
            '
            Me.XrTableCell_Guest_Telephone.Name = "XrTableCell_Guest_Telephone"
            Me.XrTableCell_Guest_Telephone.Scripts.OnBeforePrint = "XrTableCell_Guest_Telephone_BeforePrint"
            Me.XrTableCell_Guest_Telephone.Weight = 2.5899993896484377R
            '
            'XrTableRow4
            '
            Me.XrTableRow4.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.XrTableCell13, Me.XrTableCell_Workshop_Duration, Me.XrTableCell15, Me.XrTableCell16})
            Me.XrTableRow4.Name = "XrTableRow4"
            Me.XrTableRow4.Weight = 1.0R
            '
            'XrTableCell13
            '
            Me.XrTableCell13.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrTableCell13.Name = "XrTableCell13"
            Me.XrTableCell13.StylePriority.UseFont = False
            Me.XrTableCell13.Text = "DURATION"
            Me.XrTableCell13.Weight = 1.2949999754695216R
            '
            'XrTableCell_Workshop_Duration
            '
            Me.XrTableCell_Workshop_Duration.Name = "XrTableCell_Workshop_Duration"
            Me.XrTableCell_Workshop_Duration.Scripts.OnBeforePrint = "XrTableCell_Workshop_Duration_BeforePrint"
            Me.XrTableCell_Workshop_Duration.Weight = 2.4450003297062595R
            '
            'XrTableCell15
            '
            Me.XrTableCell15.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrTableCell15.Name = "XrTableCell15"
            Me.XrTableCell15.StylePriority.UseFont = False
            Me.XrTableCell15.Text = "AUDIANCE"
            Me.XrTableCell15.Weight = 1.0099996948242187R
            '
            'XrTableCell16
            '
            Me.XrTableCell16.Name = "XrTableCell16"
            Me.XrTableCell16.Scripts.OnBeforePrint = "XrTableCell_Location_Type_BeforePrint"
            Me.XrTableCell16.Weight = 2.5899993896484377R
            '
            'XrTableRow5
            '
            Me.XrTableRow5.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.XrTableCell17, Me.XrTableCell_Workshop_Size, Me.XrTableCell19, Me.XrTableCell_Location_Type})
            Me.XrTableRow5.Name = "XrTableRow5"
            Me.XrTableRow5.Weight = 1.0R
            '
            'XrTableCell17
            '
            Me.XrTableCell17.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrTableCell17.Name = "XrTableCell17"
            Me.XrTableCell17.StylePriority.UseFont = False
            Me.XrTableCell17.Text = "MAX CLASS SIZE"
            Me.XrTableCell17.Weight = 1.2949999754695216R
            '
            'XrTableCell_Workshop_Size
            '
            Me.XrTableCell_Workshop_Size.Name = "XrTableCell_Workshop_Size"
            Me.XrTableCell_Workshop_Size.Scripts.OnBeforePrint = "XrTableCell_Workshop_Size_BeforePrint"
            Me.XrTableCell_Workshop_Size.Weight = 2.4450003297062595R
            '
            'XrTableCell19
            '
            Me.XrTableCell19.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrTableCell19.Name = "XrTableCell19"
            Me.XrTableCell19.StylePriority.UseFont = False
            Me.XrTableCell19.Text = "TYPE"
            Me.XrTableCell19.Weight = 1.0099996948242187R
            '
            'XrTableCell_Location_Type
            '
            Me.XrTableCell_Location_Type.Name = "XrTableCell_Location_Type"
            Me.XrTableCell_Location_Type.Scripts.OnBeforePrint = "XrTableCell_Location_Type_BeforePrint"
            Me.XrTableCell_Location_Type.Weight = 2.5899993896484377R
            '
            'XrTableRow6
            '
            Me.XrTableRow6.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.XrTableCell21, Me.XrTableCell_Workshop_Counselor, Me.XrTableCell23, Me.XrTableCell_Location_Location})
            Me.XrTableRow6.Name = "XrTableRow6"
            Me.XrTableRow6.Weight = 1.0R
            '
            'XrTableCell21
            '
            Me.XrTableCell21.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrTableCell21.Name = "XrTableCell21"
            Me.XrTableCell21.StylePriority.UseFont = False
            Me.XrTableCell21.Text = "COORDINATOR"
            Me.XrTableCell21.Weight = 1.2949999754695216R
            '
            'XrTableCell_Workshop_Counselor
            '
            Me.XrTableCell_Workshop_Counselor.Name = "XrTableCell_Workshop_Counselor"
            Me.XrTableCell_Workshop_Counselor.Scripts.OnBeforePrint = "XrTableCell_Workshop_Counselor_BeforePrint"
            Me.XrTableCell_Workshop_Counselor.Weight = 2.4450003297062595R
            '
            'XrTableCell23
            '
            Me.XrTableCell23.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrTableCell23.Name = "XrTableCell23"
            Me.XrTableCell23.StylePriority.UseFont = False
            Me.XrTableCell23.Text = "LOCATION"
            Me.XrTableCell23.Weight = 1.0099996948242187R
            '
            'XrTableCell_Location_Location
            '
            Me.XrTableCell_Location_Location.Multiline = True
            Me.XrTableCell_Location_Location.Name = "XrTableCell_Location_Location"
            Me.XrTableCell_Location_Location.Scripts.OnBeforePrint = "XrTableCell_Location_Location_BeforePrint"
            Me.XrTableCell_Location_Location.Weight = 2.5899993896484377R
            '
            'XrTableRow7
            '
            Me.XrTableRow7.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.XrTableCell27, Me.XrTableCell_Location_Directions})
            Me.XrTableRow7.Name = "XrTableRow7"
            Me.XrTableRow7.Weight = 1.0R
            '
            'XrTableCell27
            '
            Me.XrTableCell27.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrTableCell27.Name = "XrTableCell27"
            Me.XrTableCell27.StylePriority.UseFont = False
            Me.XrTableCell27.Text = "DIRECTIONS"
            Me.XrTableCell27.Weight = 1.29499998212188R
            '
            'XrTableCell_Location_Directions
            '
            Me.XrTableCell_Location_Directions.Multiline = True
            Me.XrTableCell_Location_Directions.Name = "XrTableCell_Location_Directions"
            Me.XrTableCell_Location_Directions.Scripts.OnBeforePrint = "XrTableCell_Location_Directions_BeforePrint"
            Me.XrTableCell_Location_Directions.Weight = 6.0449994075265572R
            '
            'ParameterWorkshop
            '
            Me.ParameterWorkshop.Description = "Workshop ID"
            Me.ParameterWorkshop.Name = "ParameterWorkshop"
            Me.ParameterWorkshop.Type = GetType(Integer)
            Me.ParameterWorkshop.Value = -1
            Me.ParameterWorkshop.Visible = False
            '
            'ParameterIncludeEmail
            '
            Me.ParameterIncludeEmail.Description = "Include Email"
            Me.ParameterIncludeEmail.Name = "ParameterIncludeEmail"
            Me.ParameterIncludeEmail.Type = GetType(Boolean)
            Me.ParameterIncludeEmail.Visible = False
            '
            'ParameterIncludeAddress
            '
            Me.ParameterIncludeAddress.Description = "Include Address"
            Me.ParameterIncludeAddress.Name = "ParameterIncludeAddress"
            Me.ParameterIncludeAddress.Type = GetType(Boolean)
            Me.ParameterIncludeAddress.Visible = False
            '
            'RosterReport
            '
            Me.Bands.AddRange(New DevExpress.XtraReports.UI.Band() {Me.Detail, Me.PageHeader, Me.PageFooter, Me.ReportFooter, Me.TopMarginBand1, Me.BottomMarginBand1, Me.GroupHeader1, Me.GroupHeader3})
            Me.Parameters.AddRange(New DevExpress.XtraReports.Parameters.Parameter() {Me.ParameterWorkshop, Me.ParameterIncludeEmail, Me.ParameterIncludeAddress})
            Me.RequestParameters = False
            Me.ScriptLanguage = DevExpress.XtraReports.ScriptLanguage.VisualBasic
            Me.ScriptReferencesString = resources.GetString("$this.ScriptReferencesString")
            Me.Scripts.OnBeforePrint = "RosterReport_BeforePrint"
            Me.ScriptsSource = resources.GetString("$this.ScriptsSource")
            Me.StyleSheet.AddRange(New DevExpress.XtraReports.UI.XRControlStyle() {Me.XrControlStyle_Header, Me.XrControlStyle_Totals, Me.XrControlStyle_HeaderPannel, Me.XrControlStyle_GroupHeader})
            Me.Version = "11.2"
            Me.Controls.SetChildIndex(Me.GroupHeader3, 0)
            Me.Controls.SetChildIndex(Me.GroupHeader1, 0)
            Me.Controls.SetChildIndex(Me.BottomMarginBand1, 0)
            Me.Controls.SetChildIndex(Me.TopMarginBand1, 0)
            Me.Controls.SetChildIndex(Me.ReportFooter, 0)
            Me.Controls.SetChildIndex(Me.PageFooter, 0)
            Me.Controls.SetChildIndex(Me.PageHeader, 0)
            Me.Controls.SetChildIndex(Me.Detail, 0)
            CType(Me.XrTable1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me, System.ComponentModel.ISupportInitialize).EndInit()

        End Sub
        Friend WithEvents XrLabel_Reference As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_Phone As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_ClientName As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_Status As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_Count As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_Client As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_Address As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents ReportFooter As DevExpress.XtraReports.UI.ReportFooterBand
        Friend WithEvents XrLine1 As DevExpress.XtraReports.UI.XRLine
        Friend WithEvents XrLabel16 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_Total_Count As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents GroupHeader1 As DevExpress.XtraReports.UI.GroupHeaderBand
        Friend WithEvents XrPanel1 As DevExpress.XtraReports.UI.XRPanel
        Friend WithEvents XrLabel15 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel14 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel13 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel12 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel11 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel4 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents GroupHeader3 As DevExpress.XtraReports.UI.GroupHeaderBand
        Friend WithEvents ParameterWorkshop As DevExpress.XtraReports.Parameters.Parameter
        Friend WithEvents ParameterIncludeEmail As DevExpress.XtraReports.Parameters.Parameter
        Friend WithEvents ParameterIncludeAddress As DevExpress.XtraReports.Parameters.Parameter
        Friend WithEvents XrTable1 As DevExpress.XtraReports.UI.XRTable
        Friend WithEvents XrTableRow1 As DevExpress.XtraReports.UI.XRTableRow
        Friend WithEvents XrTableCell1 As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents XrTableCell_WorkshopID As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents XrTableCell4 As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents XrTableCell_Guest_Name As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents XrTableRow2 As DevExpress.XtraReports.UI.XRTableRow
        Friend WithEvents XrTableCell5 As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents XrTableCell_Workshop_Workshop As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents XrTableCell7 As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents XrTableCell_Guest_Email As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents XrTableRow3 As DevExpress.XtraReports.UI.XRTableRow
        Friend WithEvents XrTableCell9 As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents XrTableCell_Workshop_StartTime As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents XrTableCell11 As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents XrTableCell_Guest_Telephone As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents XrTableRow4 As DevExpress.XtraReports.UI.XRTableRow
        Friend WithEvents XrTableCell13 As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents XrTableCell_Workshop_Duration As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents XrTableCell15 As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents XrTableCell16 As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents XrTableRow5 As DevExpress.XtraReports.UI.XRTableRow
        Friend WithEvents XrTableCell17 As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents XrTableCell_Workshop_Size As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents XrTableCell19 As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents XrTableCell_Location_Type As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents XrTableRow6 As DevExpress.XtraReports.UI.XRTableRow
        Friend WithEvents XrTableCell21 As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents XrTableCell_Workshop_Counselor As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents XrTableCell23 As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents XrTableCell_Location_Location As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents XrTableRow7 As DevExpress.XtraReports.UI.XRTableRow
        Friend WithEvents XrTableCell27 As DevExpress.XtraReports.UI.XRTableCell
        Friend WithEvents XrTableCell_Location_Directions As DevExpress.XtraReports.UI.XRTableCell
    End Class
End Namespace
