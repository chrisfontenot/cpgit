#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}
#End Region

Namespace Operations.OperatingAccount
    Public Class OperatingAccountReport


        ''' <summary>
        ''' Create an instance of our report
        ''' </summary>
        Public Sub New()
            MyBase.New()
            InitializeComponent()
            AddHandler BeforePrint, AddressOf OperatingAccountReport_BeforePrint
            AddHandler XrLabel_creditor.BeforePrint, AddressOf XrLabel_creditor_BeforePrint
        End Sub


        ''' <summary>
        ''' Return the report title information
        ''' </summary>
        Public Overrides ReadOnly Property ReportTitle() As String
            Get
                Return "Operating Account"
            End Get
        End Property


        ''' <summary>
        ''' Refresh the dataset with the new information
        ''' </summary>
        Dim ds As New System.Data.DataSet("ds")
        Private Sub OperatingAccountReport_BeforePrint(ByVal sender As Object, ByVal e As System.EventArgs)
            Const TableName As String = "rpt_NonAR_ByLedger"
            Dim rpt As DevExpress.XtraReports.UI.XtraReport = CType(sender, DevExpress.XtraReports.UI.XtraReport)
            Dim sqlInfo As DebtPlus.LINQ.SQLInfoClass = DebtPlus.LINQ.SQLInfoClass.getDefault()

            '-- Read the data from the datasource
            Try
                Using cn As System.Data.SqlClient.SqlConnection = New System.Data.SqlClient.SqlConnection(sqlInfo.ConnectionString)
                    cn.Open()
                    Using cmd As New System.Data.SqlClient.SqlCommand()
                        cmd.Connection = cn
                        cmd.CommandText = "rpt_NonAR_ByLedger"
                        cmd.CommandType = System.Data.CommandType.StoredProcedure
                        System.Data.SqlClient.SqlCommandBuilder.DeriveParameters(cmd)

                        cmd.Parameters(1).Value = rpt.Parameters("ParameterFromDate").Value
                        cmd.Parameters(2).Value = rpt.Parameters("ParameterToDate").Value
                        cmd.CommandTimeout = 0

                        Using da As New System.Data.SqlClient.SqlDataAdapter(cmd)
                            da.Fill(ds, TableName)

                            '-- Support the calculated fields
                            rpt.DataSource = ds.Tables(TableName).DefaultView
                            For Each calc As DevExpress.XtraReports.UI.CalculatedField In rpt.CalculatedFields
                                calc.Assign(rpt.DataSource, rpt.DataMember)
                            Next
                        End Using
                    End Using
                End Using

            Catch ex As System.Data.SqlClient.SqlException
                Dim gdr As New DebtPlus.Repository.GetDataResult() : gdr.HandleException(ex) : DebtPlus.UI.Common.ErrorHandling.HandleErrors(gdr, "Error reading from rpt_NonAR_ByLedger")
            End Try
        End Sub

        Private Sub XrLabel_creditor_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs)
            With CType(sender, DevExpress.XtraReports.UI.XRLabel)
                Dim rpt As DevExpress.XtraReports.UI.XtraReport = CType(.Report, DevExpress.XtraReports.UI.XtraReport)
                Dim creditor As String = DebtPlus.Utils.Nulls.DStr(rpt.GetCurrentColumnValue("creditor"))
                Dim creditor_name As String = DebtPlus.Utils.Nulls.DStr(rpt.GetCurrentColumnValue("name"))
                .Text = String.Format("[{0}] {1}", creditor, creditor_name)
            End With
        End Sub
    End Class
End Namespace
