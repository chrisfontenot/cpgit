﻿Namespace Operations.Indicator.ClientList
    <Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
    Partial Class Parameters_Form
        Inherits DebtPlus.Reports.Template.Forms.DateReportParametersForm

        'Form overrides dispose to clean up the component list.
        <System.Diagnostics.DebuggerNonUserCode()> _
        Protected Overrides Sub Dispose(ByVal disposing As Boolean)
            Try
                If disposing AndAlso components IsNot Nothing Then
                    components.Dispose()
                End If
            Finally
                MyBase.Dispose(disposing)
            End Try
        End Sub

        'Required by the Windows Form Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Windows Form Designer
        'It can be modified using the Windows Form Designer.  
        'Do not modify it using the code editor.
        <System.Diagnostics.DebuggerStepThrough()> _
        Private Sub InitializeComponent()
            Dim ComboboxItem2 As DebtPlus.Data.Controls.ComboboxItem = New DebtPlus.Data.Controls.ComboboxItem
            Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl
            Me.LookUpEdit1 = New DevExpress.XtraEditors.LookUpEdit
            CType(Me.XrGroup_param_08_1, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.XrGroup_param_08_1.SuspendLayout()
            CType(Me.XrCombo_param_08_1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.XrDate_param_08_2.Properties.VistaTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.XrDate_param_08_2.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.XrDate_param_08_1.Properties.VistaTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.XrDate_param_08_1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.DxErrorProvider1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LookUpEdit1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.SuspendLayout()
            '
            'XrGroup_param_08_1
            '
            Me.ToolTipController1.SetSuperTip(Me.XrGroup_param_08_1, Nothing)
            '
            'XrCombo_param_08_1
            '
            ComboboxItem2.tag = Nothing
            ComboboxItem2.value = 12
            Me.XrCombo_param_08_1.EditValue = ComboboxItem2
            '
            'XrDate_param_08_2
            '
            Me.XrDate_param_08_2.EditValue = New Date(2008, 10, 1, 15, 9, 20, 458)
            Me.XrDate_param_08_2.Properties.VistaTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton})
            '
            'XrDate_param_08_1
            '
            Me.XrDate_param_08_1.EditValue = New Date(2008, 10, 1, 15, 9, 20, 458)
            Me.XrDate_param_08_1.Properties.VistaTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton})
            '
            'ButtonOK
            '
            Me.ButtonOK.TabIndex = 3
            '
            'ButtonCancel
            '
            Me.ButtonCancel.TabIndex = 4
            '
            'LabelControl1
            '
            Me.LabelControl1.Location = New System.Drawing.Point(24, 133)
            Me.LabelControl1.Name = "LabelControl1"
            Me.LabelControl1.Size = New System.Drawing.Size(43, 13)
            Me.LabelControl1.TabIndex = 1
            Me.LabelControl1.Text = "Indicator"
            '
            'LookUpEdit1
            '
            Me.LookUpEdit1.Location = New System.Drawing.Point(120, 130)
            Me.LookUpEdit1.Name = "LookUpEdit1"
            Me.LookUpEdit1.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
            Me.LookUpEdit1.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.LookUpEdit1.Properties.Columns.AddRange(New DevExpress.XtraEditors.Controls.LookUpColumnInfo() {New DevExpress.XtraEditors.Controls.LookUpColumnInfo("item_key", "ID", 20, DevExpress.Utils.FormatType.Numeric, "f0", False, DevExpress.Utils.HorzAlignment.[Default], DevExpress.Data.ColumnSortOrder.None), New DevExpress.XtraEditors.Controls.LookUpColumnInfo("description", "Description", 20, DevExpress.Utils.FormatType.None, "", True, DevExpress.Utils.HorzAlignment.[Default], DevExpress.Data.ColumnSortOrder.None)})
            Me.LookUpEdit1.Properties.DisplayMember = "description"
            Me.LookUpEdit1.Properties.NullText = "... All Indicators ..."
            Me.LookUpEdit1.Properties.ShowFooter = False
            Me.LookUpEdit1.Properties.ShowHeader = False
            Me.LookUpEdit1.Properties.ValueMember = "item_key"
            Me.LookUpEdit1.Size = New System.Drawing.Size(204, 20)
            Me.LookUpEdit1.TabIndex = 2
            Me.LookUpEdit1.Properties.SortColumnIndex = 1
            '
            'Parameters_Form
            '
            Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
            Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
            Me.ClientSize = New System.Drawing.Size(336, 162)
            Me.Controls.Add(Me.LookUpEdit1)
            Me.Controls.Add(Me.LabelControl1)
            Me.Name = "Parameters_Form"
            Me.ToolTipController1.SetSuperTip(Me, Nothing)
            Me.Controls.SetChildIndex(Me.LabelControl1, 0)
            Me.Controls.SetChildIndex(Me.LookUpEdit1, 0)
            Me.Controls.SetChildIndex(Me.ButtonOK, 0)
            Me.Controls.SetChildIndex(Me.ButtonCancel, 0)
            Me.Controls.SetChildIndex(Me.XrGroup_param_08_1, 0)
            CType(Me.XrGroup_param_08_1, System.ComponentModel.ISupportInitialize).EndInit()
            Me.XrGroup_param_08_1.ResumeLayout(False)
            Me.XrGroup_param_08_1.PerformLayout()
            CType(Me.XrCombo_param_08_1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.XrDate_param_08_2.Properties.VistaTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.XrDate_param_08_2.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.XrDate_param_08_1.Properties.VistaTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.XrDate_param_08_1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.DxErrorProvider1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LookUpEdit1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            Me.ResumeLayout(False)
            Me.PerformLayout()

        End Sub
        Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents LookUpEdit1 As DevExpress.XtraEditors.LookUpEdit
    End Class
End Namespace