#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}

#End Region

Imports System.ComponentModel

Imports System.Drawing.Printing
Imports System.Data.SqlClient
Imports DevExpress.XtraReports.UI

Namespace Operations.Indicator.ClientList
    Public Class IndicatorClientListReport

        Public Sub New()
            MyBase.New()
            InitializeComponent()
            AddHandler Me.BeforePrint, AddressOf IndicatorClientListReport_BeforePrint
            AddHandler XrLabel_client.BeforePrint, AddressOf XrLabel_client_BeforePrint
            AddHandler XrLabel_group_title.BeforePrint, AddressOf XrLabel_group_title_BeforePrint
        End Sub

        Private privateParameterIndicator As Object = DBNull.Value

        Public Property Parameter_Indicator() As Object
            Get
                Return privateParameterIndicator
            End Get
            Set(ByVal value As Object)
                privateParameterIndicator = value
            End Set
        End Property

        Public Overrides ReadOnly Property ReportTitle() As String
            Get
                Return "Indicator By Client"
            End Get
        End Property

        Public Overrides ReadOnly Property ReportSubTitle() As String
            Get
                Dim Answer As String = MyBase.ReportSubTitle
                If Parameter_Indicator IsNot Nothing AndAlso Parameter_Indicator IsNot DBNull.Value Then
                    Answer += String.Format(" for indicator #{0:f0}", Convert.ToInt32(Parameter_Indicator))
                End If
                Return Answer
            End Get
        End Property

        Public Overrides Sub SetReportParameter(ByVal Parameter As String, ByVal Value As Object)
            Select Case Parameter
                Case "Indicator"
                    Parameter_Indicator = Value
                Case Else
                    MyBase.SetReportParameter(Parameter, Value)
            End Select
        End Sub

        Public Overrides Function RequestReportParameters() As DialogResult
            Dim answer As DialogResult = DialogResult.OK
            If NeedParameters() Then
                With New Parameters_Form()
                    answer = .ShowDialog()
                    Parameter_FromDate = .Parameter_FromDate
                    Parameter_ToDate = .Parameter_ToDate
                    Parameter_Indicator = .Parameter_Indicator
                    .Dispose()
                End With
            End If
            Return answer
        End Function

        Dim ds As New DataSet("ds")

        Private Sub IndicatorClientListReport_BeforePrint(ByVal sender As Object, ByVal e As PrintEventArgs)

            Using cmd As SqlCommand = New System.Data.SqlClient.SqlCommand()
                With cmd
                    .Connection = New System.Data.SqlClient.SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
                    .CommandText = "rpt_client_indicators"
                    .CommandType = CommandType.StoredProcedure
                    .Parameters.Add("@indicator", SqlDbType.Int).Value = Parameter_Indicator
                    .Parameters.Add("@From_Date", SqlDbType.DateTime).Value = Parameter_FromDate
                    .Parameters.Add("@To_Date", SqlDbType.DateTime).Value = Parameter_ToDate
                    .CommandTimeout = 0
                End With

                Using da As New SqlDataAdapter(cmd)
                    da.Fill(ds, "indicators")
                End Using
            End Using

            Dim tbl As DataTable = ds.Tables(0)
            Dim vue As New DataView(tbl, String.Empty, "indicator, client", DataViewRowState.CurrentRows)

            ' Set the datasource
            DataSource = vue

            ' Bind the columns to the items
            With XrLabel_name
                .DataBindings.Clear()
                .DataBindings.Add("Text", vue, "name")
            End With

            With XrLabel_created_by
                .DataBindings.Clear()
                .DataBindings.Add("Text", vue, "created_by")
            End With

            With XrLabel_date_created
                .DataBindings.Clear()
                .DataBindings.Add("Text", vue, "date_created", "{0:d}")
            End With
        End Sub

        Private Sub XrLabel_client_BeforePrint(ByVal sender As Object, ByVal e As PrintEventArgs)
            With CType(sender, XRLabel)
                .Text = String.Format("{0:0000000}", Convert.ToInt32(GetCurrentColumnValue("client")))
            End With
        End Sub

        Private Sub XrLabel_group_title_BeforePrint(ByVal sender As Object, ByVal e As PrintEventArgs)
            With CType(sender, XRLabel)
                Dim rpt As XtraReport = CType(.Report, XtraReport)
                .Text = DebtPlus.Utils.Nulls.DStr(rpt.GetCurrentColumnValue("indicator")).ToUpper()
            End With
        End Sub
    End Class
End Namespace