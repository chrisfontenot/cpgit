#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}
#End Region

Namespace Operations.Indicator.ClientList
    Friend Class Parameters_Form

        Public Sub New()
            MyBase.New()
            InitializeComponent()
            AddHandler Me.Load, AddressOf Parameters_Form_Load
        End Sub

        Public ReadOnly Property Parameter_Indicator() As Object
            Get
                Return LookUpEdit1.EditValue
            End Get
        End Property

        Private ds As New System.Data.DataSet("ds")
        Private Sub Parameters_Form_Load(ByVal sender As Object, ByVal e As System.EventArgs)
            Dim cmd As SqlClient.SqlCommand = New System.Data.SqlClient.SqlCommand()
            With cmd
                .Connection = New System.Data.SqlClient.SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
                .CommandText = "lst_indicators"
                .CommandType = CommandType.StoredProcedure
                .CommandTimeout = 0
            End With

            Dim da As New SqlClient.SqlDataAdapter(cmd)
            da.Fill(ds, "indicators")
            With LookUpEdit1
                .EditValue = System.DBNull.Value
                .Properties.DataSource = ds.Tables(0).DefaultView
                AddHandler .EditValueChanging, AddressOf DebtPlus.Data.Validation.LookUpEdit_ActiveTest
            End With

            ButtonOK.Enabled = Not HasErrors()
        End Sub
    End Class
End Namespace