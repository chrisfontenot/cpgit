#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}
#End Region

Option Compare Binary
Option Explicit On
Option Strict On

Imports DevExpress.XtraReports

Namespace Appointments.Fhlb
    Public Class FhlbAppointmentsReport
        Inherits DebtPlus.Reports.Template.DatedTemplateXtraReportClass

        Public Sub New()
            MyBase.New()
            InitializeComponent()

            AddHandler Me.BeforePrint, AddressOf FhlbAppointmentsReport_BeforePrint

        End Sub
        Friend WithEvents XrLabel8 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel7 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel6 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel1 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel14 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel13 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel12 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel9 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_appt_date As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_counselor As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_office As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_client_email As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_client_city As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_client_name As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_appt_status As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_appt_type As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents GroupHeader1 As DevExpress.XtraReports.UI.GroupHeaderBand

        ''' <summary>
        ''' Title associated with the report
        ''' </summary>
        Public Overrides ReadOnly Property ReportTitle() As String
            Get
                Return "FHLB Appointments"
            End Get
        End Property

        ''' <summary>
        ''' Bind the dataset to the report
        ''' </summary>
        Dim ds As New System.Data.DataSet("ds")
        Private Sub FhlbAppointmentsReport_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs)
            Const TableName As String = "rpt_fhlb_appointments"
            Dim rpt As DevExpress.XtraReports.UI.XtraReport = CType(sender, DevExpress.XtraReports.UI.XtraReport)
            Dim sqlInfo As DebtPlus.LINQ.SQLInfoClass = DebtPlus.LINQ.SQLInfoClass.getDefault()
            Dim tbl As System.Data.DataTable = ds.Tables(TableName)

            If tbl Is Nothing Then
                Using cn As System.Data.SqlClient.SqlConnection = New System.Data.SqlClient.SqlConnection(sqlInfo.ConnectionString)
                    cn.Open()
                    Using cmd As New System.Data.SqlClient.SqlCommand()
                        cmd.Connection = cn
                        cmd.CommandText = "rpt_fhlb_appointments"
                        cmd.CommandType = System.Data.CommandType.StoredProcedure
                        System.Data.SqlClient.SqlCommandBuilder.DeriveParameters(cmd)

                        cmd.Parameters(1).Value = rpt.Parameters("ParameterFromDate").Value
                        cmd.Parameters(2).Value = rpt.Parameters("ParameterToDate").Value
                        cmd.CommandTimeout = 0

                        Using da As New System.Data.SqlClient.SqlDataAdapter(cmd)
                            da.Fill(ds, TableName)
                            tbl = ds.Tables(TableName)
                            rpt.DataSource = tbl
                            ''New System.Data.DataView(tbl, CType(rpt, DebtPlus.Interfaces.Reports.IReportFilter).ReportFilter.ViewFilter, String.Empty, System.Data.DataViewRowState.CurrentRows)
                        End Using
                    End Using
                End Using
            End If
        End Sub

#Region " Component Designer generated code "

        'Component overrides dispose to clean up the component list.
        Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
            If disposing Then
                If components IsNot Nothing Then
                    components.Dispose()
                End If
            End If
            MyBase.Dispose(disposing)
        End Sub

        'Required by the Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Designer
        'It can be modified using the Designer.
        'Do not modify it using the code editor.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
            Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FhlbAppointmentsReport))
            Me.XrPanel1 = New DevExpress.XtraReports.UI.XRPanel()
            Me.XrLabel14 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel13 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel12 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel9 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel8 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel7 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel6 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel1 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel4 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel5 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel3 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel2 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_referral = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_share_auth = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_client = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_fhlb = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_appt_date = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_appt_type = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_appt_status = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_client_name = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_client_city = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_client_email = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_office = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_counselor = New DevExpress.XtraReports.UI.XRLabel()
            Me.GroupHeader1 = New DevExpress.XtraReports.UI.GroupHeaderBand()
            CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
            '
            'XrLabel_Title
            '
            Me.XrLabel_Title.StylePriority.UseFont = False
            '
            'XrPageInfo_PageNumber
            '
            Me.XrPageInfo_PageNumber.LocationFloat = New DevExpress.Utils.PointFloat(917.0!, 9.999997!)
            Me.XrPageInfo_PageNumber.StylePriority.UseTextAlignment = False
            '
            'XrPanel_AgencyAddress
            '
            Me.XrPanel_AgencyAddress.LocationFloat = New DevExpress.Utils.PointFloat(741.9999!, 0.0!)
            Me.XrPanel_AgencyAddress.SizeF = New System.Drawing.SizeF(308.0001!, 85.00001!)
            '
            'XRLabel_Agency_Name
            '
            Me.XRLabel_Agency_Name.LocationFloat = New DevExpress.Utils.PointFloat(3.0!, 0.0!)
            Me.XRLabel_Agency_Name.StylePriority.UseTextAlignment = False
            '
            'XrLabel_Agency_Address3
            '
            Me.XrLabel_Agency_Address3.LocationFloat = New DevExpress.Utils.PointFloat(3.0!, 51.0!)
            Me.XrLabel_Agency_Address3.StylePriority.UseFont = False
            Me.XrLabel_Agency_Address3.StylePriority.UseTextAlignment = False
            '
            'XrLabel_Agency_Address1
            '
            Me.XrLabel_Agency_Address1.LocationFloat = New DevExpress.Utils.PointFloat(3.0!, 17.0!)
            Me.XrLabel_Agency_Address1.StylePriority.UseFont = False
            Me.XrLabel_Agency_Address1.StylePriority.UseTextAlignment = False
            '
            'XrLabel_Agency_Phone
            '
            Me.XrLabel_Agency_Phone.LocationFloat = New DevExpress.Utils.PointFloat(3.0!, 68.0!)
            Me.XrLabel_Agency_Phone.StylePriority.UseFont = False
            Me.XrLabel_Agency_Phone.StylePriority.UseTextAlignment = False
            '
            'XrLabel_Agency_Address2
            '
            Me.XrLabel_Agency_Address2.LocationFloat = New DevExpress.Utils.PointFloat(3.0!, 34.0!)
            Me.XrLabel_Agency_Address2.StylePriority.UseFont = False
            Me.XrLabel_Agency_Address2.StylePriority.UseTextAlignment = False
            '
            'Detail
            '
            Me.Detail.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel_counselor, Me.XrLabel_office, Me.XrLabel_client_email, Me.XrLabel_client_city, Me.XrLabel_client_name, Me.XrLabel_appt_status, Me.XrLabel_appt_type, Me.XrLabel_appt_date, Me.XrLabel_fhlb, Me.XrLabel_client, Me.XrLabel_share_auth, Me.XrLabel_referral})
            Me.Detail.HeightF = 22.20834!
            Me.Detail.KeepTogether = True
            '
            'XrPanel1
            '
            Me.XrPanel1.BackColor = System.Drawing.Color.Teal
            Me.XrPanel1.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel14, Me.XrLabel13, Me.XrLabel12, Me.XrLabel9, Me.XrLabel8, Me.XrLabel7, Me.XrLabel6, Me.XrLabel1, Me.XrLabel4, Me.XrLabel5, Me.XrLabel3, Me.XrLabel2})
            Me.XrPanel1.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
            Me.XrPanel1.Name = "XrPanel1"
            Me.XrPanel1.SizeF = New System.Drawing.SizeF(1050.0!, 51.67426!)
            '
            'XrLabel14
            '
            Me.XrLabel14.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.XrLabel14.ForeColor = System.Drawing.Color.White
            Me.XrLabel14.LocationFloat = New DevExpress.Utils.PointFloat(952.2949!, 0.0!)
            Me.XrLabel14.Name = "XrLabel14"
            Me.XrLabel14.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel14.SizeF = New System.Drawing.SizeF(97.70502!, 51.67427!)
            Me.XrLabel14.StylePriority.UseFont = False
            Me.XrLabel14.StylePriority.UsePadding = False
            Me.XrLabel14.StylePriority.UseTextAlignment = False
            Me.XrLabel14.Text = "Counselor"
            Me.XrLabel14.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
            '
            'XrLabel13
            '
            Me.XrLabel13.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.XrLabel13.ForeColor = System.Drawing.Color.White
            Me.XrLabel13.LocationFloat = New DevExpress.Utils.PointFloat(846.3821!, 0.0!)
            Me.XrLabel13.Name = "XrLabel13"
            Me.XrLabel13.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel13.SizeF = New System.Drawing.SizeF(105.9127!, 51.67426!)
            Me.XrLabel13.StylePriority.UseFont = False
            Me.XrLabel13.StylePriority.UsePadding = False
            Me.XrLabel13.StylePriority.UseTextAlignment = False
            Me.XrLabel13.Text = "Office"
            Me.XrLabel13.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            '
            'XrLabel12
            '
            Me.XrLabel12.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.XrLabel12.ForeColor = System.Drawing.Color.White
            Me.XrLabel12.LocationFloat = New DevExpress.Utils.PointFloat(727.9693!, 0.0!)
            Me.XrLabel12.Name = "XrLabel12"
            Me.XrLabel12.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel12.SizeF = New System.Drawing.SizeF(118.4128!, 51.67426!)
            Me.XrLabel12.StylePriority.UseFont = False
            Me.XrLabel12.StylePriority.UsePadding = False
            Me.XrLabel12.StylePriority.UseTextAlignment = False
            Me.XrLabel12.Text = "Client Email"
            Me.XrLabel12.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            '
            'XrLabel9
            '
            Me.XrLabel9.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.XrLabel9.ForeColor = System.Drawing.Color.White
            Me.XrLabel9.LocationFloat = New DevExpress.Utils.PointFloat(618.9316!, 0.0!)
            Me.XrLabel9.Name = "XrLabel9"
            Me.XrLabel9.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel9.SizeF = New System.Drawing.SizeF(109.0377!, 51.67426!)
            Me.XrLabel9.StylePriority.UseFont = False
            Me.XrLabel9.StylePriority.UsePadding = False
            Me.XrLabel9.StylePriority.UseTextAlignment = False
            Me.XrLabel9.Text = "Client City, State, Zip"
            Me.XrLabel9.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            '
            'XrLabel8
            '
            Me.XrLabel8.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.XrLabel8.ForeColor = System.Drawing.Color.White
            Me.XrLabel8.LocationFloat = New DevExpress.Utils.PointFloat(436.8105!, 0.0!)
            Me.XrLabel8.Name = "XrLabel8"
            Me.XrLabel8.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel8.SizeF = New System.Drawing.SizeF(54.49997!, 51.67426!)
            Me.XrLabel8.StylePriority.UseFont = False
            Me.XrLabel8.StylePriority.UsePadding = False
            Me.XrLabel8.StylePriority.UseTextAlignment = False
            Me.XrLabel8.Text = "Appt Status"
            Me.XrLabel8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            '
            'XrLabel7
            '
            Me.XrLabel7.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.XrLabel7.ForeColor = System.Drawing.Color.White
            Me.XrLabel7.LocationFloat = New DevExpress.Utils.PointFloat(380.2197!, 0.0!)
            Me.XrLabel7.Name = "XrLabel7"
            Me.XrLabel7.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel7.SizeF = New System.Drawing.SizeF(56.59082!, 51.67426!)
            Me.XrLabel7.StylePriority.UseFont = False
            Me.XrLabel7.StylePriority.UsePadding = False
            Me.XrLabel7.StylePriority.UseTextAlignment = False
            Me.XrLabel7.Text = "Appt Type"
            Me.XrLabel7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            '
            'XrLabel6
            '
            Me.XrLabel6.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.XrLabel6.ForeColor = System.Drawing.Color.White
            Me.XrLabel6.LocationFloat = New DevExpress.Utils.PointFloat(225.7197!, 0.0!)
            Me.XrLabel6.Name = "XrLabel6"
            Me.XrLabel6.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel6.SizeF = New System.Drawing.SizeF(68.46211!, 49.0909!)
            Me.XrLabel6.StylePriority.UseFont = False
            Me.XrLabel6.StylePriority.UsePadding = False
            Me.XrLabel6.StylePriority.UseTextAlignment = False
            Me.XrLabel6.Text = "Client ID"
            Me.XrLabel6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            '
            'XrLabel1
            '
            Me.XrLabel1.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.XrLabel1.ForeColor = System.Drawing.Color.White
            Me.XrLabel1.LocationFloat = New DevExpress.Utils.PointFloat(176.9545!, 0.0!)
            Me.XrLabel1.Name = "XrLabel1"
            Me.XrLabel1.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel1.SizeF = New System.Drawing.SizeF(48.76512!, 51.67426!)
            Me.XrLabel1.StylePriority.UseFont = False
            Me.XrLabel1.StylePriority.UsePadding = False
            Me.XrLabel1.StylePriority.UseTextAlignment = False
            Me.XrLabel1.Text = "Share Auth"
            Me.XrLabel1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            '
            'XrLabel4
            '
            Me.XrLabel4.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.XrLabel4.ForeColor = System.Drawing.Color.White
            Me.XrLabel4.LocationFloat = New DevExpress.Utils.PointFloat(294.1818!, 0.0!)
            Me.XrLabel4.Name = "XrLabel4"
            Me.XrLabel4.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel4.SizeF = New System.Drawing.SizeF(86.03793!, 51.67426!)
            Me.XrLabel4.StylePriority.UseFont = False
            Me.XrLabel4.StylePriority.UsePadding = False
            Me.XrLabel4.StylePriority.UseTextAlignment = False
            Me.XrLabel4.Text = "Appt Date"
            Me.XrLabel4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            '
            'XrLabel5
            '
            Me.XrLabel5.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.XrLabel5.ForeColor = System.Drawing.Color.White
            Me.XrLabel5.LocationFloat = New DevExpress.Utils.PointFloat(491.3105!, 0.0!)
            Me.XrLabel5.Name = "XrLabel5"
            Me.XrLabel5.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel5.SizeF = New System.Drawing.SizeF(127.6211!, 51.67426!)
            Me.XrLabel5.StylePriority.UseFont = False
            Me.XrLabel5.StylePriority.UsePadding = False
            Me.XrLabel5.StylePriority.UseTextAlignment = False
            Me.XrLabel5.Text = "Client Name"
            Me.XrLabel5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            '
            'XrLabel3
            '
            Me.XrLabel3.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.XrLabel3.ForeColor = System.Drawing.Color.White
            Me.XrLabel3.LocationFloat = New DevExpress.Utils.PointFloat(86.2803!, 0.0!)
            Me.XrLabel3.Name = "XrLabel3"
            Me.XrLabel3.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel3.SizeF = New System.Drawing.SizeF(90.67424!, 51.67426!)
            Me.XrLabel3.StylePriority.UseFont = False
            Me.XrLabel3.StylePriority.UsePadding = False
            Me.XrLabel3.StylePriority.UseTextAlignment = False
            Me.XrLabel3.Text = "Referral"
            Me.XrLabel3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            '
            'XrLabel2
            '
            Me.XrLabel2.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.XrLabel2.ForeColor = System.Drawing.Color.White
            Me.XrLabel2.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
            Me.XrLabel2.Name = "XrLabel2"
            Me.XrLabel2.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel2.SizeF = New System.Drawing.SizeF(1050.0!, 51.67426!)
            Me.XrLabel2.StylePriority.UseFont = False
            Me.XrLabel2.StylePriority.UsePadding = False
            Me.XrLabel2.StylePriority.UseTextAlignment = False
            Me.XrLabel2.Text = "FHLB#"
            Me.XrLabel2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            '
            'XrLabel_referral
            '
            Me.XrLabel_referral.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "referral")})
            Me.XrLabel_referral.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.XrLabel_referral.KeepTogether = True
            Me.XrLabel_referral.LocationFloat = New DevExpress.Utils.PointFloat(86.2803!, 0.0!)
            Me.XrLabel_referral.Name = "XrLabel_referral"
            Me.XrLabel_referral.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_referral.SizeF = New System.Drawing.SizeF(90.67424!, 17.0!)
            Me.XrLabel_referral.StylePriority.UseFont = False
            Me.XrLabel_referral.StylePriority.UsePadding = False
            Me.XrLabel_referral.StylePriority.UseTextAlignment = False
            Me.XrLabel_referral.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            '
            'XrLabel_share_auth
            '
            Me.XrLabel_share_auth.CanGrow = False
            Me.XrLabel_share_auth.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "share_auth")})
            Me.XrLabel_share_auth.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.XrLabel_share_auth.KeepTogether = True
            Me.XrLabel_share_auth.LocationFloat = New DevExpress.Utils.PointFloat(176.9546!, 0.0!)
            Me.XrLabel_share_auth.Name = "XrLabel_share_auth"
            Me.XrLabel_share_auth.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_share_auth.SizeF = New System.Drawing.SizeF(48.76508!, 17.0!)
            Me.XrLabel_share_auth.StylePriority.UseFont = False
            Me.XrLabel_share_auth.StylePriority.UsePadding = False
            Me.XrLabel_share_auth.StylePriority.UseTextAlignment = False
            Me.XrLabel_share_auth.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            Me.XrLabel_share_auth.WordWrap = False
            '
            'XrLabel_client
            '
            Me.XrLabel_client.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "client")})
            Me.XrLabel_client.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.XrLabel_client.KeepTogether = True
            Me.XrLabel_client.LocationFloat = New DevExpress.Utils.PointFloat(225.7197!, 0.0!)
            Me.XrLabel_client.Name = "XrLabel_client"
            Me.XrLabel_client.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_client.SizeF = New System.Drawing.SizeF(68.46214!, 17.0!)
            Me.XrLabel_client.StylePriority.UseFont = False
            Me.XrLabel_client.StylePriority.UsePadding = False
            Me.XrLabel_client.StylePriority.UseTextAlignment = False
            Me.XrLabel_client.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            '
            'XrLabel_fhlb
            '
            Me.XrLabel_fhlb.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "FHLB#")})
            Me.XrLabel_fhlb.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.XrLabel_fhlb.KeepTogether = True
            Me.XrLabel_fhlb.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
            Me.XrLabel_fhlb.Name = "XrLabel_fhlb"
            Me.XrLabel_fhlb.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_fhlb.SizeF = New System.Drawing.SizeF(86.2803!, 17.0!)
            Me.XrLabel_fhlb.StylePriority.UseFont = False
            Me.XrLabel_fhlb.StylePriority.UsePadding = False
            Me.XrLabel_fhlb.StylePriority.UseTextAlignment = False
            Me.XrLabel_fhlb.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            '
            'XrLabel_appt_date
            '
            Me.XrLabel_appt_date.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "start_time", "{0:MM/dd/yyyy HH:mm}")})
            Me.XrLabel_appt_date.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.XrLabel_appt_date.KeepTogether = True
            Me.XrLabel_appt_date.LocationFloat = New DevExpress.Utils.PointFloat(294.1818!, 0.0!)
            Me.XrLabel_appt_date.Name = "XrLabel_appt_date"
            Me.XrLabel_appt_date.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_appt_date.SizeF = New System.Drawing.SizeF(86.03793!, 17.0!)
            Me.XrLabel_appt_date.StylePriority.UseFont = False
            Me.XrLabel_appt_date.StylePriority.UsePadding = False
            Me.XrLabel_appt_date.StylePriority.UseTextAlignment = False
            Me.XrLabel_appt_date.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            '
            'XrLabel_appt_type
            '
            Me.XrLabel_appt_type.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "appt_type")})
            Me.XrLabel_appt_type.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.XrLabel_appt_type.KeepTogether = True
            Me.XrLabel_appt_type.LocationFloat = New DevExpress.Utils.PointFloat(380.2197!, 0.0!)
            Me.XrLabel_appt_type.Name = "XrLabel_appt_type"
            Me.XrLabel_appt_type.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_appt_type.SizeF = New System.Drawing.SizeF(56.59079!, 17.0!)
            Me.XrLabel_appt_type.StylePriority.UseFont = False
            Me.XrLabel_appt_type.StylePriority.UsePadding = False
            Me.XrLabel_appt_type.StylePriority.UseTextAlignment = False
            Me.XrLabel_appt_type.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            '
            'XrLabel_appt_status
            '
            Me.XrLabel_appt_status.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "appt_status")})
            Me.XrLabel_appt_status.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.XrLabel_appt_status.KeepTogether = True
            Me.XrLabel_appt_status.LocationFloat = New DevExpress.Utils.PointFloat(436.8105!, 0.0!)
            Me.XrLabel_appt_status.Name = "XrLabel_appt_status"
            Me.XrLabel_appt_status.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_appt_status.SizeF = New System.Drawing.SizeF(54.49994!, 17.0!)
            Me.XrLabel_appt_status.StylePriority.UseFont = False
            Me.XrLabel_appt_status.StylePriority.UsePadding = False
            Me.XrLabel_appt_status.StylePriority.UseTextAlignment = False
            Me.XrLabel_appt_status.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            Me.XrLabel_appt_status.WordWrap = False
            '
            'XrLabel_client_name
            '
            Me.XrLabel_client_name.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "clientname")})
            Me.XrLabel_client_name.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.XrLabel_client_name.KeepTogether = True
            Me.XrLabel_client_name.LocationFloat = New DevExpress.Utils.PointFloat(491.3105!, 0.0!)
            Me.XrLabel_client_name.Name = "XrLabel_client_name"
            Me.XrLabel_client_name.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_client_name.SizeF = New System.Drawing.SizeF(127.621!, 17.0!)
            Me.XrLabel_client_name.StylePriority.UseFont = False
            Me.XrLabel_client_name.StylePriority.UsePadding = False
            Me.XrLabel_client_name.StylePriority.UseTextAlignment = False
            Me.XrLabel_client_name.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            Me.XrLabel_client_name.WordWrap = False
            '
            'XrLabel_client_city
            '
            Me.XrLabel_client_city.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "city_state_zip")})
            Me.XrLabel_client_city.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.XrLabel_client_city.KeepTogether = True
            Me.XrLabel_client_city.LocationFloat = New DevExpress.Utils.PointFloat(618.9315!, 0.0!)
            Me.XrLabel_client_city.Name = "XrLabel_client_city"
            Me.XrLabel_client_city.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_client_city.SizeF = New System.Drawing.SizeF(109.0378!, 17.0!)
            Me.XrLabel_client_city.StylePriority.UseFont = False
            Me.XrLabel_client_city.StylePriority.UsePadding = False
            Me.XrLabel_client_city.StylePriority.UseTextAlignment = False
            Me.XrLabel_client_city.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            Me.XrLabel_client_city.WordWrap = False
            '
            'XrLabel_client_email
            '
            Me.XrLabel_client_email.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "clientemail")})
            Me.XrLabel_client_email.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.XrLabel_client_email.KeepTogether = True
            Me.XrLabel_client_email.LocationFloat = New DevExpress.Utils.PointFloat(727.9693!, 0.0!)
            Me.XrLabel_client_email.Name = "XrLabel_client_email"
            Me.XrLabel_client_email.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_client_email.SizeF = New System.Drawing.SizeF(118.4128!, 17.0!)
            Me.XrLabel_client_email.StylePriority.UseFont = False
            Me.XrLabel_client_email.StylePriority.UsePadding = False
            Me.XrLabel_client_email.StylePriority.UseTextAlignment = False
            Me.XrLabel_client_email.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            Me.XrLabel_client_email.WordWrap = False
            '
            'XrLabel_office
            '
            Me.XrLabel_office.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "officename")})
            Me.XrLabel_office.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.XrLabel_office.KeepTogether = True
            Me.XrLabel_office.LocationFloat = New DevExpress.Utils.PointFloat(846.3821!, 0.0!)
            Me.XrLabel_office.Name = "XrLabel_office"
            Me.XrLabel_office.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_office.SizeF = New System.Drawing.SizeF(105.9127!, 17.0!)
            Me.XrLabel_office.StylePriority.UseFont = False
            Me.XrLabel_office.StylePriority.UsePadding = False
            Me.XrLabel_office.StylePriority.UseTextAlignment = False
            Me.XrLabel_office.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            '
            'XrLabel_counselor
            '
            Me.XrLabel_counselor.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "counselorname")})
            Me.XrLabel_counselor.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.XrLabel_counselor.KeepTogether = True
            Me.XrLabel_counselor.LocationFloat = New DevExpress.Utils.PointFloat(952.2949!, 0.0!)
            Me.XrLabel_counselor.Name = "XrLabel_counselor"
            Me.XrLabel_counselor.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_counselor.SizeF = New System.Drawing.SizeF(97.70496!, 17.0!)
            Me.XrLabel_counselor.StylePriority.UseFont = False
            Me.XrLabel_counselor.StylePriority.UsePadding = False
            Me.XrLabel_counselor.StylePriority.UseTextAlignment = False
            Me.XrLabel_counselor.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            '
            'GroupHeader1
            '
            Me.GroupHeader1.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrPanel1})
            Me.GroupHeader1.HeightF = 55.20833!
            Me.GroupHeader1.Name = "GroupHeader1"
            '
            'FhlbAppointmentsReport
            '
            Me.Bands.AddRange(New DevExpress.XtraReports.UI.Band() {Me.Detail, Me.PageHeader, Me.PageFooter, Me.TopMarginBand1, Me.BottomMarginBand1, Me.GroupHeader1})
            Me.Landscape = True
            Me.PageHeight = 850
            Me.PageWidth = 1100
            Me.ScriptLanguage = DevExpress.XtraReports.ScriptLanguage.VisualBasic
            Me.ScriptReferencesString = resources.GetString("$this.ScriptReferencesString")
            Me.Scripts.OnBeforePrint = "FhlbAppointmentsReport_BeforePrint"
            Me.ScriptsSource = resources.GetString("$this.ScriptsSource")
            Me.StyleSheet.AddRange(New DevExpress.XtraReports.UI.XRControlStyle() {Me.XrControlStyle_Header, Me.XrControlStyle_Totals, Me.XrControlStyle_HeaderPannel, Me.XrControlStyle_GroupHeader})
            Me.Version = "11.2"
            Me.Controls.SetChildIndex(Me.GroupHeader1, 0)
            Me.Controls.SetChildIndex(Me.BottomMarginBand1, 0)
            Me.Controls.SetChildIndex(Me.TopMarginBand1, 0)
            Me.Controls.SetChildIndex(Me.PageFooter, 0)
            Me.Controls.SetChildIndex(Me.PageHeader, 0)
            Me.Controls.SetChildIndex(Me.Detail, 0)
            CType(Me, System.ComponentModel.ISupportInitialize).EndInit()

        End Sub
        Friend WithEvents XrPanel1 As DevExpress.XtraReports.UI.XRPanel
        Friend WithEvents XrLabel2 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel3 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel5 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_referral As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_share_auth As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_client As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_fhlb As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel4 As DevExpress.XtraReports.UI.XRLabel
#End Region
    End Class
End Namespace
