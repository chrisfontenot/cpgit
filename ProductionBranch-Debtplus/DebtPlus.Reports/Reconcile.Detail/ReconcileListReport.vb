#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}
#End Region

Option Compare Binary
Option Explicit On
Option Strict On

Imports DevExpress.XtraReports.UI

Namespace Reconcile.Detail
    Public Class CheckReconcileDetailListReport
        Inherits Template.TemplateXtraReportClass

        ''' <summary>
        ''' Create an instance of our report
        ''' </summary>
        Public Sub New()
            MyBase.new()
            InitializeComponent()
            AddHandler BeforePrint, AddressOf Report_BeforePrint
            AddHandler GroupHeader1.BeforePrint, AddressOf GroupHeader1_BeforePrint
        End Sub

        Public Overrides ReadOnly Property ReportTitle() As String
            Get
                Return "Reconciled Items"
            End Get
        End Property

        Private _Subtitle As String = System.String.Empty
        Public Overrides ReadOnly Property ReportSubTitle() As String
            Get
                Return _Subtitle
            End Get
        End Property

        Private _Parameter_BatchID As System.Int32 = -1
        Public Property Parameter_BatchID() As System.Int32
            Get
                Return _Parameter_BatchID
            End Get
            Set(ByVal Value As System.Int32)
                _Parameter_BatchID = Value
            End Set
        End Property

        Public Overrides Sub SetReportParameter(ByVal Parameter As String, ByVal Value As Object)
            Select Case Parameter
                Case "BatchID"
                    Parameter_BatchID = Convert.ToInt32(Value)
                Case Else
                    MyBase.SetReportParameter(Parameter, Value)
            End Select
        End Sub

        Public Overrides Function NeedParameters() As Boolean
            Return Parameter_BatchID <= 0
        End Function

        Public Overrides Function RequestReportParameters() As System.Windows.Forms.DialogResult
            Dim answer As System.Windows.Forms.DialogResult = DialogResult.OK
            If NeedParameters() Then
                Using frm As New RequestParametersForm()
                    answer = frm.ShowDialog()
                    If answer = DialogResult.OK Then Parameter_BatchID = frm.Parameter_BatchID
                End Using
            End If

            Return answer
        End Function

#Region " Component Designer generated code "

        'Component overrides dispose to clean up the component list.
        Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
            If disposing Then
                If components IsNot Nothing Then
                    components.Dispose()
                    ds.Dispose()
                End If
            End If
            MyBase.Dispose(disposing)
        End Sub

        'Required by the Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Designer
        'It can be modified using the Designer.
        'Do not modify it using the code editor.
        Friend WithEvents GroupHeader1 As DevExpress.XtraReports.UI.GroupHeaderBand
        Friend WithEvents GroupFooter1 As DevExpress.XtraReports.UI.GroupFooterBand
        Friend WithEvents XrPanel1 As DevExpress.XtraReports.UI.XRPanel
        Friend WithEvents XrLabel2 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel3 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_Cleared As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_Date_Created As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_Amount As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_Payee As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_Recon_Amount As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_Recon_Date As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_Recon_Message As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents ReportFooter As DevExpress.XtraReports.UI.ReportFooterBand
        Friend WithEvents XrLabel_Group_Cleared As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_Group_Recon_Amount As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLine1 As DevExpress.XtraReports.UI.XRLine
        Friend WithEvents XrLabel4 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel6 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel7 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel9 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel10 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel11 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel12 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel13 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_Total_Recon_Amount As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_Total_Cleared As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_checknum As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_Group_Amount As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_Total_Amount As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_Group_Header As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel1 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel5 As DevExpress.XtraReports.UI.XRLabel
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
            Dim XrSummary1 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary
            Dim XrSummary2 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary
            Dim XrSummary3 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary
            Dim XrSummary4 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary
            Dim XrSummary5 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary
            Dim XrSummary6 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary
            Me.GroupHeader1 = New DevExpress.XtraReports.UI.GroupHeaderBand
            Me.XrPanel1 = New DevExpress.XtraReports.UI.XRPanel
            Me.XrLabel13 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel12 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel11 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel10 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel9 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel7 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel4 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel6 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_Group_Header = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel2 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel3 = New DevExpress.XtraReports.UI.XRLabel
            Me.GroupFooter1 = New DevExpress.XtraReports.UI.GroupFooterBand
            Me.XrLabel_Group_Amount = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel1 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLine1 = New DevExpress.XtraReports.UI.XRLine
            Me.XrLabel_Group_Recon_Amount = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_Group_Cleared = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_Cleared = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_Date_Created = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_Amount = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_Payee = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_checknum = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_Recon_Amount = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_Recon_Date = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_Recon_Message = New DevExpress.XtraReports.UI.XRLabel
            Me.ReportFooter = New DevExpress.XtraReports.UI.ReportFooterBand
            Me.XrLabel_Total_Amount = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_Total_Cleared = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_Total_Recon_Amount = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel5 = New DevExpress.XtraReports.UI.XRLabel
            CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
            '
            'Detail
            '
            Me.Detail.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel_Recon_Message, Me.XrLabel_Recon_Date, Me.XrLabel_Recon_Amount, Me.XrLabel_checknum, Me.XrLabel_Payee, Me.XrLabel_Amount, Me.XrLabel_Date_Created, Me.XrLabel_Cleared})
            Me.Detail.HeightF = 15.0!
            '
            'PageHeader
            '
            Me.PageHeader.HeightF = 117.0!
            '
            'XrLabel_Title
            '
            Me.XrLabel_Title.StylePriority.UseFont = False
            '
            'XrPageInfo_PageNumber
            '
            Me.XrPageInfo_PageNumber.StylePriority.UseTextAlignment = False
            '
            'XRLabel_Agency_Name
            '
            Me.XRLabel_Agency_Name.StylePriority.UseTextAlignment = False
            '
            'XrLabel_Agency_Address3
            '
            Me.XrLabel_Agency_Address3.StylePriority.UseFont = False
            Me.XrLabel_Agency_Address3.StylePriority.UseTextAlignment = False
            '
            'XrLabel_Agency_Address1
            '
            Me.XrLabel_Agency_Address1.StylePriority.UseFont = False
            Me.XrLabel_Agency_Address1.StylePriority.UseTextAlignment = False
            '
            'XrLabel_Agency_Phone
            '
            Me.XrLabel_Agency_Phone.StylePriority.UseFont = False
            Me.XrLabel_Agency_Phone.StylePriority.UseTextAlignment = False
            '
            'XrLabel_Agency_Address2
            '
            Me.XrLabel_Agency_Address2.StylePriority.UseFont = False
            Me.XrLabel_Agency_Address2.StylePriority.UseTextAlignment = False
            '
            'GroupHeader1
            '
            Me.GroupHeader1.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrPanel1, Me.XrLabel_Group_Header, Me.XrLabel2, Me.XrLabel3})
            Me.GroupHeader1.Name = "GroupHeader1"
            Me.GroupHeader1.PageBreak = DevExpress.XtraReports.UI.PageBreak.BeforeBand
            Me.GroupHeader1.RepeatEveryPage = True
            '
            'XrPanel1
            '
            Me.XrPanel1.BackColor = System.Drawing.Color.Teal
            Me.XrPanel1.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel13, Me.XrLabel12, Me.XrLabel11, Me.XrLabel10, Me.XrLabel9, Me.XrLabel7, Me.XrLabel4, Me.XrLabel6})
            Me.XrPanel1.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 75.0!)
            Me.XrPanel1.Name = "XrPanel1"
            Me.XrPanel1.SizeF = New System.Drawing.SizeF(800.0!, 17.0!)
            '
            'XrLabel13
            '
            Me.XrLabel13.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel13.ForeColor = System.Drawing.Color.White
            Me.XrLabel13.LocationFloat = New DevExpress.Utils.PointFloat(675.0!, 0.0!)
            Me.XrLabel13.Name = "XrLabel13"
            Me.XrLabel13.SizeF = New System.Drawing.SizeF(125.0!, 15.0!)
            Me.XrLabel13.Text = "ERROR"
            Me.XrLabel13.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            '
            'XrLabel12
            '
            Me.XrLabel12.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel12.ForeColor = System.Drawing.Color.White
            Me.XrLabel12.LocationFloat = New DevExpress.Utils.PointFloat(600.0!, 0.0!)
            Me.XrLabel12.Name = "XrLabel12"
            Me.XrLabel12.SizeF = New System.Drawing.SizeF(66.0!, 15.0!)
            Me.XrLabel12.Text = "DATE"
            Me.XrLabel12.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel11
            '
            Me.XrLabel11.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel11.ForeColor = System.Drawing.Color.White
            Me.XrLabel11.LocationFloat = New DevExpress.Utils.PointFloat(519.0!, 0.0!)
            Me.XrLabel11.Name = "XrLabel11"
            Me.XrLabel11.SizeF = New System.Drawing.SizeF(72.0!, 15.0!)
            Me.XrLabel11.Text = "AMT"
            Me.XrLabel11.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel10
            '
            Me.XrLabel10.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel10.ForeColor = System.Drawing.Color.White
            Me.XrLabel10.LocationFloat = New DevExpress.Utils.PointFloat(433.0!, 0.0!)
            Me.XrLabel10.Name = "XrLabel10"
            Me.XrLabel10.SizeF = New System.Drawing.SizeF(83.0!, 15.0!)
            Me.XrLabel10.Text = "CHECK #"
            Me.XrLabel10.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel9
            '
            Me.XrLabel9.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel9.ForeColor = System.Drawing.Color.White
            Me.XrLabel9.LocationFloat = New DevExpress.Utils.PointFloat(183.0!, 0.0!)
            Me.XrLabel9.Name = "XrLabel9"
            Me.XrLabel9.SizeF = New System.Drawing.SizeF(250.0!, 15.0!)
            Me.XrLabel9.Text = "PAYEE"
            Me.XrLabel9.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            '
            'XrLabel7
            '
            Me.XrLabel7.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel7.ForeColor = System.Drawing.Color.White
            Me.XrLabel7.LocationFloat = New DevExpress.Utils.PointFloat(100.0!, 0.0!)
            Me.XrLabel7.Name = "XrLabel7"
            Me.XrLabel7.SizeF = New System.Drawing.SizeF(75.0!, 15.0!)
            Me.XrLabel7.Text = "AMT"
            Me.XrLabel7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel4
            '
            Me.XrLabel4.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel4.ForeColor = System.Drawing.Color.White
            Me.XrLabel4.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
            Me.XrLabel4.Name = "XrLabel4"
            Me.XrLabel4.SizeF = New System.Drawing.SizeF(33.0!, 16.0!)
            Me.XrLabel4.Text = "CLR"
            '
            'XrLabel6
            '
            Me.XrLabel6.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel6.ForeColor = System.Drawing.Color.White
            Me.XrLabel6.LocationFloat = New DevExpress.Utils.PointFloat(43.75!, 0.0!)
            Me.XrLabel6.Name = "XrLabel6"
            Me.XrLabel6.SizeF = New System.Drawing.SizeF(47.25!, 15.0!)
            Me.XrLabel6.Text = "DATE"
            Me.XrLabel6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel_Group_Header
            '
            Me.XrLabel_Group_Header.CanGrow = False
            Me.XrLabel_Group_Header.Font = New System.Drawing.Font("Times New Roman", 14.0!)
            Me.XrLabel_Group_Header.ForeColor = System.Drawing.Color.Maroon
            Me.XrLabel_Group_Header.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 8.0!)
            Me.XrLabel_Group_Header.Name = "XrLabel_Group_Header"
            Me.XrLabel_Group_Header.SizeF = New System.Drawing.SizeF(750.0!, 34.0!)
            Me.XrLabel_Group_Header.Text = "Transaction Type"
            Me.XrLabel_Group_Header.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            Me.XrLabel_Group_Header.WordWrap = False
            '
            'XrLabel2
            '
            Me.XrLabel2.BackColor = System.Drawing.Color.Teal
            Me.XrLabel2.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel2.ForeColor = System.Drawing.Color.White
            Me.XrLabel2.LocationFloat = New DevExpress.Utils.PointFloat(7.999992!, 57.99999!)
            Me.XrLabel2.Name = "XrLabel2"
            Me.XrLabel2.SizeF = New System.Drawing.SizeF(509.0!, 17.00001!)
            Me.XrLabel2.Text = "CHECK OR DEPOSIT ITEM AS WRITTEN"
            Me.XrLabel2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter
            Me.XrLabel2.WordWrap = False
            '
            'XrLabel3
            '
            Me.XrLabel3.BackColor = System.Drawing.Color.Teal
            Me.XrLabel3.CanGrow = False
            Me.XrLabel3.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel3.ForeColor = System.Drawing.Color.White
            Me.XrLabel3.LocationFloat = New DevExpress.Utils.PointFloat(525.0!, 57.99999!)
            Me.XrLabel3.Name = "XrLabel3"
            Me.XrLabel3.SizeF = New System.Drawing.SizeF(265.0!, 17.00001!)
            Me.XrLabel3.Text = "RECONCILED"
            Me.XrLabel3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter
            Me.XrLabel3.WordWrap = False
            '
            'GroupFooter1
            '
            Me.GroupFooter1.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel_Group_Amount, Me.XrLabel1, Me.XrLine1, Me.XrLabel_Group_Recon_Amount, Me.XrLabel_Group_Cleared})
            Me.GroupFooter1.HeightF = 54.99999!
            Me.GroupFooter1.Name = "GroupFooter1"
            '
            'XrLabel_Group_Amount
            '
            Me.XrLabel_Group_Amount.CanGrow = False
            Me.XrLabel_Group_Amount.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel_Group_Amount.LocationFloat = New DevExpress.Utils.PointFloat(83.0!, 25.0!)
            Me.XrLabel_Group_Amount.Name = "XrLabel_Group_Amount"
            Me.XrLabel_Group_Amount.SizeF = New System.Drawing.SizeF(92.0!, 17.0!)
            XrSummary1.FormatString = "{0:c2}"
            XrSummary1.Running = DevExpress.XtraReports.UI.SummaryRunning.Group
            Me.XrLabel_Group_Amount.Summary = XrSummary1
            Me.XrLabel_Group_Amount.Text = "$0.00"
            Me.XrLabel_Group_Amount.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrLabel_Group_Amount.WordWrap = False
            '
            'XrLabel1
            '
            Me.XrLabel1.CanGrow = False
            Me.XrLabel1.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel1.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 25.0!)
            Me.XrLabel1.Name = "XrLabel1"
            Me.XrLabel1.SizeF = New System.Drawing.SizeF(83.0!, 17.00001!)
            Me.XrLabel1.Text = "SUBTOTAL"
            Me.XrLabel1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            Me.XrLabel1.WordWrap = False
            '
            'XrLine1
            '
            Me.XrLine1.ForeColor = System.Drawing.Color.Teal
            Me.XrLine1.LineWidth = 2
            Me.XrLine1.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 8.0!)
            Me.XrLine1.Name = "XrLine1"
            Me.XrLine1.SizeF = New System.Drawing.SizeF(592.0!, 8.0!)
            '
            'XrLabel_Group_Recon_Amount
            '
            Me.XrLabel_Group_Recon_Amount.CanGrow = False
            Me.XrLabel_Group_Recon_Amount.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel_Group_Recon_Amount.LocationFloat = New DevExpress.Utils.PointFloat(408.0!, 25.0!)
            Me.XrLabel_Group_Recon_Amount.Name = "XrLabel_Group_Recon_Amount"
            Me.XrLabel_Group_Recon_Amount.SizeF = New System.Drawing.SizeF(183.0!, 17.0!)
            XrSummary2.FormatString = "{0:C2}"
            XrSummary2.Running = DevExpress.XtraReports.UI.SummaryRunning.Group
            Me.XrLabel_Group_Recon_Amount.Summary = XrSummary2
            Me.XrLabel_Group_Recon_Amount.Text = "$0.00"
            Me.XrLabel_Group_Recon_Amount.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrLabel_Group_Recon_Amount.WordWrap = False
            '
            'XrLabel_Group_Cleared
            '
            Me.XrLabel_Group_Cleared.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel_Group_Cleared.LocationFloat = New DevExpress.Utils.PointFloat(183.0!, 25.0!)
            Me.XrLabel_Group_Cleared.Name = "XrLabel_Group_Cleared"
            Me.XrLabel_Group_Cleared.SizeF = New System.Drawing.SizeF(217.0!, 17.0!)
            XrSummary3.FormatString = "{0:D} items"
            XrSummary3.Func = DevExpress.XtraReports.UI.SummaryFunc.Count
            XrSummary3.Running = DevExpress.XtraReports.UI.SummaryRunning.Group
            Me.XrLabel_Group_Cleared.Summary = XrSummary3
            Me.XrLabel_Group_Cleared.Text = "0 items"
            Me.XrLabel_Group_Cleared.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            '
            'XrLabel_Cleared
            '
            Me.XrLabel_Cleared.CanGrow = False
            Me.XrLabel_Cleared.LocationFloat = New DevExpress.Utils.PointFloat(6.0!, 0.0!)
            Me.XrLabel_Cleared.Name = "XrLabel_Cleared"
            Me.XrLabel_Cleared.SizeF = New System.Drawing.SizeF(10.0!, 15.0!)
            Me.XrLabel_Cleared.Text = "R"
            Me.XrLabel_Cleared.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            Me.XrLabel_Cleared.WordWrap = False
            '
            'XrLabel_Date_Created
            '
            Me.XrLabel_Date_Created.CanGrow = False
            Me.XrLabel_Date_Created.LocationFloat = New DevExpress.Utils.PointFloat(25.0!, 0.0!)
            Me.XrLabel_Date_Created.Name = "XrLabel_Date_Created"
            Me.XrLabel_Date_Created.SizeF = New System.Drawing.SizeF(66.0!, 15.0!)
            Me.XrLabel_Date_Created.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrLabel_Date_Created.WordWrap = False
            '
            'XrLabel_Amount
            '
            Me.XrLabel_Amount.CanGrow = False
            Me.XrLabel_Amount.LocationFloat = New DevExpress.Utils.PointFloat(100.0!, 0.0!)
            Me.XrLabel_Amount.Name = "XrLabel_Amount"
            Me.XrLabel_Amount.SizeF = New System.Drawing.SizeF(75.0!, 15.0!)
            Me.XrLabel_Amount.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrLabel_Amount.WordWrap = False
            '
            'XrLabel_Payee
            '
            Me.XrLabel_Payee.LocationFloat = New DevExpress.Utils.PointFloat(183.0!, 0.0!)
            Me.XrLabel_Payee.Name = "XrLabel_Payee"
            Me.XrLabel_Payee.SizeF = New System.Drawing.SizeF(250.0!, 15.0!)
            Me.XrLabel_Payee.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            Me.XrLabel_Payee.WordWrap = False
            '
            'XrLabel_checknum
            '
            Me.XrLabel_checknum.CanGrow = False
            Me.XrLabel_checknum.LocationFloat = New DevExpress.Utils.PointFloat(433.0!, 0.0!)
            Me.XrLabel_checknum.Name = "XrLabel_checknum"
            Me.XrLabel_checknum.SizeF = New System.Drawing.SizeF(83.0!, 15.0!)
            Me.XrLabel_checknum.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrLabel_checknum.WordWrap = False
            '
            'XrLabel_Recon_Amount
            '
            Me.XrLabel_Recon_Amount.CanGrow = False
            Me.XrLabel_Recon_Amount.LocationFloat = New DevExpress.Utils.PointFloat(519.0!, 0.0!)
            Me.XrLabel_Recon_Amount.Name = "XrLabel_Recon_Amount"
            Me.XrLabel_Recon_Amount.SizeF = New System.Drawing.SizeF(72.0!, 15.0!)
            Me.XrLabel_Recon_Amount.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrLabel_Recon_Amount.WordWrap = False
            '
            'XrLabel_Recon_Date
            '
            Me.XrLabel_Recon_Date.CanGrow = False
            Me.XrLabel_Recon_Date.LocationFloat = New DevExpress.Utils.PointFloat(600.0!, 0.0!)
            Me.XrLabel_Recon_Date.Name = "XrLabel_Recon_Date"
            Me.XrLabel_Recon_Date.SizeF = New System.Drawing.SizeF(66.0!, 15.0!)
            Me.XrLabel_Recon_Date.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrLabel_Recon_Date.WordWrap = False
            '
            'XrLabel_Recon_Message
            '
            Me.XrLabel_Recon_Message.CanGrow = False
            Me.XrLabel_Recon_Message.ForeColor = System.Drawing.Color.Red
            Me.XrLabel_Recon_Message.LocationFloat = New DevExpress.Utils.PointFloat(675.0!, 0.0!)
            Me.XrLabel_Recon_Message.Name = "XrLabel_Recon_Message"
            Me.XrLabel_Recon_Message.SizeF = New System.Drawing.SizeF(125.0!, 15.0!)
            Me.XrLabel_Recon_Message.Text = "XrLabel4"
            Me.XrLabel_Recon_Message.WordWrap = False
            '
            'ReportFooter
            '
            Me.ReportFooter.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel_Total_Amount, Me.XrLabel_Total_Cleared, Me.XrLabel_Total_Recon_Amount, Me.XrLabel5})
            Me.ReportFooter.HeightF = 35.0!
            Me.ReportFooter.Name = "ReportFooter"
            '
            'XrLabel_Total_Amount
            '
            Me.XrLabel_Total_Amount.CanGrow = False
            Me.XrLabel_Total_Amount.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel_Total_Amount.LocationFloat = New DevExpress.Utils.PointFloat(83.0!, 8.0!)
            Me.XrLabel_Total_Amount.Name = "XrLabel_Total_Amount"
            Me.XrLabel_Total_Amount.SizeF = New System.Drawing.SizeF(92.0!, 17.0!)
            XrSummary4.FormatString = "{0:C2}"
            XrSummary4.Running = DevExpress.XtraReports.UI.SummaryRunning.Report
            Me.XrLabel_Total_Amount.Summary = XrSummary4
            Me.XrLabel_Total_Amount.Text = "$0.00"
            Me.XrLabel_Total_Amount.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrLabel_Total_Amount.WordWrap = False
            '
            'XrLabel_Total_Cleared
            '
            Me.XrLabel_Total_Cleared.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel_Total_Cleared.LocationFloat = New DevExpress.Utils.PointFloat(183.0!, 8.0!)
            Me.XrLabel_Total_Cleared.Name = "XrLabel_Total_Cleared"
            Me.XrLabel_Total_Cleared.SizeF = New System.Drawing.SizeF(217.0!, 17.0!)
            XrSummary5.FormatString = "{0:D} items"
            XrSummary5.Func = DevExpress.XtraReports.UI.SummaryFunc.Count
            XrSummary5.Running = DevExpress.XtraReports.UI.SummaryRunning.Report
            Me.XrLabel_Total_Cleared.Summary = XrSummary5
            Me.XrLabel_Total_Cleared.Text = "0 items"
            Me.XrLabel_Total_Cleared.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            '
            'XrLabel_Total_Recon_Amount
            '
            Me.XrLabel_Total_Recon_Amount.CanGrow = False
            Me.XrLabel_Total_Recon_Amount.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel_Total_Recon_Amount.LocationFloat = New DevExpress.Utils.PointFloat(408.0!, 8.0!)
            Me.XrLabel_Total_Recon_Amount.Name = "XrLabel_Total_Recon_Amount"
            Me.XrLabel_Total_Recon_Amount.SizeF = New System.Drawing.SizeF(183.0!, 17.0!)
            XrSummary6.FormatString = "{0:c2}"
            XrSummary6.Running = DevExpress.XtraReports.UI.SummaryRunning.Report
            Me.XrLabel_Total_Recon_Amount.Summary = XrSummary6
            Me.XrLabel_Total_Recon_Amount.Text = "$0.00"
            Me.XrLabel_Total_Recon_Amount.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            Me.XrLabel_Total_Recon_Amount.WordWrap = False
            '
            'XrLabel5
            '
            Me.XrLabel5.CanGrow = False
            Me.XrLabel5.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel5.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 7.999992!)
            Me.XrLabel5.Name = "XrLabel5"
            Me.XrLabel5.SizeF = New System.Drawing.SizeF(83.0!, 17.00001!)
            Me.XrLabel5.Text = "TOTAL"
            Me.XrLabel5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            Me.XrLabel5.WordWrap = False
            '
            'CheckReconcileDetailListReport
            '
            Me.Bands.AddRange(New DevExpress.XtraReports.UI.Band() {Me.Detail, Me.PageHeader, Me.PageFooter, Me.TopMarginBand1, Me.BottomMarginBand1, Me.GroupHeader1, Me.GroupFooter1, Me.ReportFooter})
            Me.StyleSheet.AddRange(New DevExpress.XtraReports.UI.XRControlStyle() {Me.XrControlStyle_Header, Me.XrControlStyle_Totals, Me.XrControlStyle_HeaderPannel, Me.XrControlStyle_GroupHeader})
            Me.Version = "10.2"
            Me.Controls.SetChildIndex(Me.ReportFooter, 0)
            Me.Controls.SetChildIndex(Me.GroupFooter1, 0)
            Me.Controls.SetChildIndex(Me.GroupHeader1, 0)
            Me.Controls.SetChildIndex(Me.BottomMarginBand1, 0)
            Me.Controls.SetChildIndex(Me.TopMarginBand1, 0)
            Me.Controls.SetChildIndex(Me.PageFooter, 0)
            Me.Controls.SetChildIndex(Me.PageHeader, 0)
            Me.Controls.SetChildIndex(Me.Detail, 0)
            CType(Me, System.ComponentModel.ISupportInitialize).EndInit()

        End Sub

#End Region

        Dim ds As New System.Data.DataSet("ReconItems")
        Private Sub Report_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs)
            Dim tbl As System.Data.DataTable = ds.Tables("rpt_recon_items")

            If tbl Is Nothing Then
                Using cmd As SqlClient.SqlCommand = New System.Data.SqlClient.SqlCommand()
                    With cmd
                        .Connection = New System.Data.SqlClient.SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
                        .CommandText = "rpt_recon_items"
                        .CommandType = CommandType.StoredProcedure
                        .Parameters.Add("@recon_batch", SqlDbType.Int).Value = Parameter_BatchID
                        .CommandTimeout = 0
                    End With

                    Using da As New SqlClient.SqlDataAdapter(cmd)
                        da.Fill(ds, "rpt_recon_items")
                        tbl = ds.Tables("rpt_recon_items")
                    End Using
                End Using
            End If

            If tbl IsNot Nothing Then
                Dim vue As System.Data.DataView = tbl.DefaultView
                DataSource = vue

                ' Bind the items to the data fields
                XrLabel_Cleared.DataBindings.Add("Text", vue, "cleared")
                XrLabel_Group_Cleared.DataBindings.Add("Text", vue, "cleared")
                XrLabel_Total_Cleared.DataBindings.Add("Text", vue, "cleared")
                XrLabel_Date_Created.DataBindings.Add("Text", vue, "date_created", "{0:d}")
                XrLabel_Payee.DataBindings.Add("Text", vue, "payee")
                XrLabel_Amount.DataBindings.Add("Text", vue, "check_amount", "{0:c}")
                XrLabel_Group_Amount.DataBindings.Add("Text", vue, "check_amount", "{0:c}")
                XrLabel_Total_Amount.DataBindings.Add("Text", vue, "check_amount", "{0:c}")
                XrLabel_checknum.DataBindings.Add("Text", vue, "checknum")
                XrLabel_Recon_Amount.DataBindings.Add("Text", vue, "check_amount", "{0:c}")
                XrLabel_Group_Recon_Amount.DataBindings.Add("Text", vue, "check_amount", "{0:c}")
                XrLabel_Total_Recon_Amount.DataBindings.Add("Text", vue, "check_amount", "{0:c}")
                XrLabel_Recon_Date.DataBindings.Add("Text", vue, "reconciled_date", "{0:d}")
                XrLabel_Recon_Message.DataBindings.Add("Text", vue, "message")

                ' Group by the transaction type
                GroupHeader1.GroupFields.Add(New DevExpress.XtraReports.UI.GroupField("tran_type", XRColumnSortOrder.Ascending))
            End If
        End Sub

        Private Sub GroupHeader1_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs)
            Dim tran_type As String = Convert.ToString(Me.GetCurrentColumnValue("tran_type"))

            With XrLabel_Group_Header
                Select Case tran_type
                    Case "AD"
                        .Text = "Checks"
                    Case "RR"
                        .Text = "EFT Returns"
                    Case "RF"
                        .Text = "Creditor Refunds"
                    Case "DP"
                        .Text = "Deposits"
                    Case "BW"
                        .Text = "Bank Wires"
                    Case "SC"
                        .Text = "Service Charges"
                    Case "BI"
                        .Text = "Bank Interest"
                    Case "BE"
                        .Text = "Bank Errors"
                    Case Else
                        .Text = tran_type
                End Select
            End With
        End Sub
    End Class
End Namespace
