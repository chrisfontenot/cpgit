#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}
#End Region

Option Compare Binary
Option Explicit On 
Option Strict On

Namespace Reconcile.Detail
    Friend Class RequestParametersForm
        Inherits Template.Forms.ReportParametersForm
        Private _Parameter_BatchID As System.Int32 = 0

        Public Sub New()
            MyBase.New()
            InitializeComponent()
            AddHandler Load, AddressOf RequestParametersForm_Load
            AddHandler TextEdit_Parameter_BatchID.GotFocus, AddressOf TextEdit_Parameter_BatchID_GotFocus
        End Sub

        ''' <summary>
        ''' Batch ID
        ''' </summary>
        Friend Property Parameter_BatchID() As System.Int32
            Get
                Return _Parameter_BatchID
            End Get

            Set(ByVal Value As System.Int32)
                _Parameter_BatchID = Value
            End Set
        End Property

#Region " Windows Form Designer generated code "

        'Form overrides dispose to clean up the component list.
        Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
            If disposing Then
                If components IsNot Nothing Then
                    components.Dispose()
                End If
                ds.Dispose()
            End If
            MyBase.Dispose(disposing)
        End Sub

        'Required by the Windows Form Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Windows Form Designer
        'It can be modified using the Windows Form Designer.  
        'Do not modify it using the code editor.
        Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
        Friend WithEvents RadioButton_List As System.Windows.Forms.RadioButton
        Friend WithEvents RadioButton_Text As System.Windows.Forms.RadioButton
        Friend WithEvents Label1 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents TextEdit_Parameter_BatchID As DevExpress.XtraEditors.TextEdit
        Friend WithEvents LookUpEdit_Deposit As DevExpress.XtraEditors.LookUpEdit
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
            Me.GroupBox1 = New System.Windows.Forms.GroupBox
            Me.Label1 = New DevExpress.XtraEditors.LabelControl
            Me.RadioButton_Text = New System.Windows.Forms.RadioButton
            Me.RadioButton_List = New System.Windows.Forms.RadioButton
            Me.TextEdit_Parameter_BatchID = New DevExpress.XtraEditors.TextEdit
            Me.LookUpEdit_Deposit = New DevExpress.XtraEditors.LookUpEdit

            Me.GroupBox1.SuspendLayout()
            CType(Me.TextEdit_Parameter_BatchID.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LookUpEdit_Deposit.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.SuspendLayout()
            '
            'ButtonCancel
            '
            Me.ButtonCancel.Location = New System.Drawing.Point(248, 52)
            Me.ButtonCancel.Name = "ButtonCancel"
            Me.ButtonCancel.Size = New System.Drawing.Size(80, 26)
            Me.ButtonCancel.TabIndex = 4
            '
            'ButtonOK
            '
            Me.ButtonOK.Location = New System.Drawing.Point(248, 17)
            Me.ButtonOK.Name = "ButtonOK"
            Me.ButtonOK.Size = New System.Drawing.Size(80, 26)
            Me.ButtonOK.TabIndex = 3
            '
            'GroupBox1
            '
            Me.GroupBox1.Controls.Add(Me.Label1)
            Me.GroupBox1.Controls.Add(Me.RadioButton_Text)
            Me.GroupBox1.Controls.Add(Me.RadioButton_List)
            Me.GroupBox1.Controls.Add(Me.TextEdit_Parameter_BatchID)
            Me.GroupBox1.Controls.Add(Me.LookUpEdit_Deposit)
            Me.GroupBox1.Location = New System.Drawing.Point(8, 9)
            Me.GroupBox1.Name = "GroupBox1"
            Me.GroupBox1.Size = New System.Drawing.Size(232, 112)
            Me.GroupBox1.TabIndex = 0
            Me.GroupBox1.TabStop = False
            Me.GroupBox1.Text = " Recon Batch ID "
            '
            'Label1
            '
            Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.Label1.Location = New System.Drawing.Point(32, 54)
            Me.Label1.Name = "Label1"
            Me.Label1.Size = New System.Drawing.Size(152, 17)
            Me.Label1.TabIndex = 2
            Me.Label1.Text = "Or enter a specific ID"
            '
            'RadioButton_Text
            '
            Me.RadioButton_Text.AllowDrop = True
            Me.RadioButton_Text.Location = New System.Drawing.Point(8, 78)
            Me.RadioButton_Text.Name = "RadioButton_Text"
            Me.RadioButton_Text.Size = New System.Drawing.Size(16, 17)
            Me.RadioButton_Text.TabIndex = 3
            Me.RadioButton_Text.Tag = "text"
            Me.RadioButton_Text.TextAlign = System.Drawing.ContentAlignment.MiddleRight
            '
            'RadioButton_List
            '
            Me.RadioButton_List.Checked = True
            Me.RadioButton_List.Location = New System.Drawing.Point(8, 28)
            Me.RadioButton_List.Name = "RadioButton_List"
            Me.RadioButton_List.Size = New System.Drawing.Size(16, 17)
            Me.RadioButton_List.TabIndex = 0
            Me.RadioButton_List.TabStop = True
            Me.RadioButton_List.Tag = "list"
            '
            'TextEdit_Parameter_BatchID
            '
            Me.TextEdit_Parameter_BatchID.EditValue = ""
            Me.TextEdit_Parameter_BatchID.Location = New System.Drawing.Point(32, 78)
            Me.TextEdit_Parameter_BatchID.Name = "TextEdit_Parameter_BatchID"
            '
            'TextEdit_Parameter_BatchID.Properties
            '
            Me.TextEdit_Parameter_BatchID.Properties.Appearance.Options.UseTextOptions = True
            Me.TextEdit_Parameter_BatchID.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.TextEdit_Parameter_BatchID.Properties.AppearanceDisabled.Options.UseTextOptions = True
            Me.TextEdit_Parameter_BatchID.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.TextEdit_Parameter_BatchID.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
            Me.TextEdit_Parameter_BatchID.Properties.DisplayFormat.FormatString = "f0"
            Me.TextEdit_Parameter_BatchID.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.TextEdit_Parameter_BatchID.Properties.EditFormat.FormatString = "f0"
            Me.TextEdit_Parameter_BatchID.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.TextEdit_Parameter_BatchID.Properties.Enabled = False
            Me.TextEdit_Parameter_BatchID.Properties.ValidateOnEnterKey = True
            Me.TextEdit_Parameter_BatchID.Size = New System.Drawing.Size(64, 20)
            Me.TextEdit_Parameter_BatchID.TabIndex = 4
            Me.TextEdit_Parameter_BatchID.ToolTip = "Enter a specific disbursement if it is not included in the above list"
            Me.TextEdit_Parameter_BatchID.ToolTipController = Me.ToolTipController1
            '
            'LookUpEdit_Deposit
            '
            Me.LookUpEdit_Deposit.Location = New System.Drawing.Point(32, 26)
            Me.LookUpEdit_Deposit.Name = "LookUpEdit_Deposit"
            '
            'LookUpEdit_Deposit.Properties
            '
            Me.LookUpEdit_Deposit.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.LookUpEdit_Deposit.Properties.NullText = ""
            Me.LookUpEdit_Deposit.Properties.PopupWidth = 500
            Me.LookUpEdit_Deposit.Size = New System.Drawing.Size(184, 20)
            Me.LookUpEdit_Deposit.TabIndex = 1
            Me.LookUpEdit_Deposit.ToolTip = "Choose a disbursement from the list if desired"
            Me.LookUpEdit_Deposit.ToolTipController = Me.ToolTipController1
            '
            'RequestParametersForm
            '
            Me.AutoScaleBaseSize = New System.Drawing.Size(5, 14)
            Me.ClientSize = New System.Drawing.Size(336, 129)
            Me.Controls.Add(Me.GroupBox1)
            Me.Name = "RequestParametersForm"
            Me.Text = "Recon Batch Report Parameters"
            Me.Controls.SetChildIndex(Me.ButtonOK, 0)
            Me.Controls.SetChildIndex(Me.ButtonCancel, 0)
            Me.Controls.SetChildIndex(Me.GroupBox1, 0)

            Me.GroupBox1.ResumeLayout(False)
            CType(Me.TextEdit_Parameter_BatchID.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LookUpEdit_Deposit.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            Me.ResumeLayout(False)

        End Sub

#End Region

        Private Sub RequestParametersForm_Load(ByVal sender As System.Object, ByVal e As System.EventArgs)

            ' Load the deposit registers
            load_deposit_registers()

            ' Add the handling routines
            AddHandler LookUpEdit_Deposit.EditValueChanged, AddressOf LookUpEdit_Disbursement_EditValueChanged
            AddHandler TextEdit_Parameter_BatchID.EditValueChanged, AddressOf TextEdit_disbursement_register_EditValueChanged
            AddHandler TextEdit_Parameter_BatchID.KeyPress, AddressOf TextEdit_disbursement_register_KeyPress
            AddHandler RadioButton_List.CheckedChanged, AddressOf RadioButton_List_CheckedChanged
            AddHandler RadioButton_Text.CheckedChanged, AddressOf RadioButton_List_CheckedChanged

            ' Enable or disable the OK button
            EnableOK()
        End Sub

        Dim ds As New System.Data.DataSet("ds")

        Private Sub load_deposit_registers()

            ' Retrieve the disbursement table from the system
            Try

                ' Retrieve the database connection logic
                Const SelectStatement As String = "SELECT TOP 50 ids.recon_batch AS 'ID', ids.date_created AS 'Date', ids.created_by AS 'Creator', isnull(ids.note,'') as 'Note' FROM recon_batches ids WITH (NOLOCK) WHERE date_posted IS NULL ORDER BY 2 desc"
                Using cmd As SqlClient.SqlCommand = New System.Data.SqlClient.SqlCommand()
                    With cmd
                        .Connection = New System.Data.SqlClient.SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
                        .CommandText = SelectStatement
                        .CommandTimeout = 0
                    End With

                    Using da As New SqlClient.SqlDataAdapter(cmd)
                        da.Fill(ds, "recon_batches")
                    End Using
                End Using

                Dim tbl As System.Data.DataTable = ds.Tables("recon_batches")

                ' Define the values for the lookup control
                With LookUpEdit_Deposit
                    With .Properties
                        .DataSource = New System.Data.DataView(tbl, String.Empty, "Date", DataViewRowState.CurrentRows)
                        .ShowFooter = False
                        .PopulateColumns()

                        ' The date and time are the default values
                        With .Columns("Date")
                            .FormatString = "MM/dd/yyyy hh:mm tt"
                            .Width = 90
                        End With

                        ' Populate the ID column with the disbursement register, showing it as a number
                        With .Columns("ID")
                            .FormatString = "f0"
                            .Alignment = DevExpress.Utils.HorzAlignment.Far
                            .Width = 40
                        End With

                        ' The display is the note, the value is the ID
                        .DisplayMember = "Note"
                        .ValueMember = "ID"
                    End With

                    ' Set the value to the first item
                    If tbl.Rows.Count > 0 Then
                        Dim row As System.Data.DataRow = tbl.Rows(0)
                        .EditValue = row("ID")
                    End If

                    ' Populate the input value with the ID from the listbox
                    If .EditValue IsNot System.DBNull.Value Then
                        Parameter_BatchID = Convert.ToInt32(.EditValue)
                        TextEdit_Parameter_BatchID.Text = Parameter_BatchID.ToString()
                    End If
                End With

            Catch ex As SqlClient.SqlException
                DebtPlus.Data.Forms.MessageBox.Show(ex.ToString(), "Error retrieving batch list", MessageBoxButtons.OK, MessageBoxIcon.Error)

            End Try
        End Sub

        Private Sub TextEdit_Parameter_BatchID_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs)
            CType(sender, DevExpress.XtraEditors.TextEdit).SelectAll()
        End Sub

        Private Sub TextEdit_disbursement_register_EditValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
            Parameter_BatchID = Convert.ToInt32(TextEdit_Parameter_BatchID.EditValue)
            EnableOK()
        End Sub

        Private Sub TextEdit_disbursement_register_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs)
            If Not System.Char.IsControl(e.KeyChar) Then
                If Not System.Char.IsDigit(e.KeyChar) Then e.Handled = True
            End If
        End Sub

        Private Sub LookUpEdit_Disbursement_EditValueChanged(ByVal sender As Object, ByVal e As System.EventArgs)
            TextEdit_Parameter_BatchID.EditValue = LookUpEdit_Deposit.EditValue
            Parameter_BatchID = Convert.ToInt32(TextEdit_Parameter_BatchID.EditValue)
            EnableOK()
        End Sub

        Private Sub EnableOK()
            Try
                Dim NewValue As System.Int32 = CType(TextEdit_Parameter_BatchID.EditValue, Int32)
                If NewValue > 0 Then
                    ButtonOK.Enabled = True
                    Exit Sub
                End If
            Catch
            End Try

            ' The disbursement register is not valid. Disable the button.
            ButtonOK.Enabled = False
        End Sub

        Private Sub RadioButton_List_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)

            ' Enable or disable the controls based upon the radio buttons
            TextEdit_Parameter_BatchID.Enabled = RadioButton_Text.Checked
            LookUpEdit_Deposit.Enabled = RadioButton_List.Checked

            ' Syncrhonize the edit value with the selected item
            TextEdit_Parameter_BatchID.EditValue = LookUpEdit_Deposit.EditValue

            ' Go to the next control
            CType(sender, System.Windows.Forms.RadioButton).SelectNextControl(CType(sender, System.Windows.Forms.Control), True, True, False, True)
        End Sub
    End Class
End Namespace
