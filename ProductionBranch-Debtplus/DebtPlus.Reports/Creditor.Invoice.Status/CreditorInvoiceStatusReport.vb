#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}

#End Region

Imports System.ComponentModel
Imports DebtPlus.Reports.Template
Imports System.IO
Imports System.Reflection
Imports DevExpress.XtraReports.UI
Imports DebtPlus.Utils

Namespace Creditor.Invoice.Status
    Public Class CreditorInvoiceStatusReport
        Inherits DatedTemplateXtraReportClass

        Public Sub New()
            MyBase.New()
            InitializeComponent()
        End Sub

        Public Sub New(ByVal DateRange As DateRange)
            MyBase.New(DateRange)
            InitializeComponent()
        End Sub

        Public Sub InitializeComponent()
            Const ReportName As String = "DebtPlus.Reports.Creditor.Invoice.Status.repx"

            ' See if there is a report reference that we can use
            Dim Fname As String = Path.Combine(DebtPlus.Configuration.Config.ReportsDirectory, ReportName)
            Dim UseDefault As Boolean = True
            Try
                If (Not DebtPlus.Configuration.Config.LocalReportsOnly) AndAlso System.IO.File.Exists(Fname) Then
                    LoadLayout(Fname)
                    UseDefault = False
                End If

            Catch ex As Exception
                DebtPlus.Svc.Common.ErrorHandling.HandleErrors(ex)
            End Try

            ' Load the standard report definition if we need a new item
            If UseDefault Then
                Dim asm As Reflection.Assembly = Reflection.Assembly.GetExecutingAssembly
                Using ios As Stream = asm.GetManifestResourceStream(String.Format("DebtPlus.Reports.{0}", ReportName))
                    If ios IsNot Nothing Then
                        LoadLayout(ios)
                    End If
                End Using
            End If

            ' Set the script references as needed
            ScriptReferences = DebtPlus.Utils.AssemblyTools.GetAllSiblingAssemblies()                                                        ' changed   

            ' Update the subreports as well
            For Each BandPtr As Band In Bands
                For Each CtlPtr As XRControl In BandPtr.Controls
                    Dim Rpt As XRSubreport = TryCast(CtlPtr, XRSubreport)
                    If Rpt IsNot Nothing Then
                        Dim RptSrc As XtraReport = TryCast(Rpt.ReportSource, XtraReport)
                        If RptSrc IsNot Nothing Then
                            RptSrc.ScriptReferences = ScriptReferences
                        End If
                    End If
                Next
            Next

            ' Enable the select expert
            ReportFilter.IsEnabled = True

            ' Ensure that the parameters are clear
            Parameters("ParameterShowAll").Value = False
        End Sub


        ''' <summary>
        ''' Report Title
        ''' </summary>
        Public Overrides ReadOnly Property ReportTitle() As String
            Get
                Return "Fairshare Invoice Detail"
            End Get
        End Property

        Public Property Parameter_ShowAll() As Boolean
            Get
                Return DirectCast(Parameters("ParameterShowAll").Value, Boolean)
            End Get
            Set(ByVal value As Boolean)
                Parameters("ParameterShowAll").Value = value
            End Set
        End Property


        ''' <summary>
        ''' Common interface to set a parameter
        ''' </summary>
        Public Overrides Sub SetReportParameter(ByVal Parameter As String, ByVal Value As Object)
            Select Case Parameter
                Case "ShowAll"
                    Parameter_ShowAll = Convert.ToBoolean(Value)
                Case Else
                    MyBase.SetReportParameter(Parameter, Value)
            End Select
        End Sub


        ''' <summary>
        ''' Obtain the report parameters when they are not supplied
        ''' </summary>
        Public Overrides Function RequestReportParameters() As DialogResult
            Dim answer As DialogResult = DialogResult.OK
            If NeedParameters() Then
                Using frm As New CreditorInvoiceStatusParametersForm()
                    answer = frm.ShowDialog()
                    Parameters("ParameterFromDate").Value = frm.Parameter_FromDate
                    Parameters("ParameterToDate").Value = frm.Parameter_ToDate
                    Parameters("ParameterShowAll").Value = frm.Parameter_ShowAll
                End Using
            End If
            Return answer
        End Function
    End Class
End Namespace
