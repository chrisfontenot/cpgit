Namespace Creditor.Invoice.Status
    <Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
    Partial Class CreditorInvoiceStatusReport
        Inherits DebtPlus.Reports.Template.DatedTemplateXtraReportClass

        'XtraReport overrides dispose to clean up the component list.
        <System.Diagnostics.DebuggerNonUserCode()> _
        Protected Overrides Sub Dispose(ByVal disposing As Boolean)
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
            MyBase.Dispose(disposing)
        End Sub

        'Required by the Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Designer
        'It can be modified using the Designer.  
        'Do not modify it using the code editor.
        <System.Diagnostics.DebuggerStepThrough()> _
        Private Sub InitializeComponent()
            Dim XrSummary1 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary()
            Dim XrSummary2 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary()
            Dim XrSummary3 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary()
            Dim XrSummary4 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary()
            Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(CreditorInvoiceStatusReport))
            Me.XrLabel1 = New DevExpress.XtraReports.UI.XRLabel()
            Me.GroupFooter1 = New DevExpress.XtraReports.UI.GroupFooterBand()
            Me.XrLabel33 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_group_pmt_amount = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_group_adj_amount = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_group_balance = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_group_inv_amount = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLine2 = New DevExpress.XtraReports.UI.XRLine()
            Me.XrLabel_inv_date = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_adj_date = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_pmt_date = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_printed = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_invoice_register = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_inv_amount = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_pmt_amount = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_adj_amount = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_balance = New DevExpress.XtraReports.UI.XRLabel()
            Me.GroupHeader1 = New DevExpress.XtraReports.UI.GroupHeaderBand()
            Me.XrLabel10 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel3 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel4 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel5 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel6 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel7 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel8 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel16 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel17 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel18 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel19 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel2 = New DevExpress.XtraReports.UI.XRLabel()
            Me.ParameterShowAll = New DevExpress.XtraReports.Parameters.Parameter()
            CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
            '
            'PageHeader
            '
            Me.PageHeader.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel17, Me.XrLabel16, Me.XrLabel8, Me.XrLabel2, Me.XrLabel19, Me.XrLabel18, Me.XrLabel4, Me.XrLabel3, Me.XrLabel10, Me.XrLabel7, Me.XrLabel6, Me.XrLabel5})
            Me.PageHeader.HeightF = 174.0!
            Me.PageHeader.Controls.SetChildIndex(Me.XrPanel_AgencyAddress, 0)
            Me.PageHeader.Controls.SetChildIndex(Me.XrLabel_Title, 0)
            Me.PageHeader.Controls.SetChildIndex(Me.XrLine_Title, 0)
            Me.PageHeader.Controls.SetChildIndex(Me.XrLabel_Subtitle, 0)
            Me.PageHeader.Controls.SetChildIndex(Me.XrPageInfo1, 0)
            Me.PageHeader.Controls.SetChildIndex(Me.XrLabel5, 0)
            Me.PageHeader.Controls.SetChildIndex(Me.XrLabel6, 0)
            Me.PageHeader.Controls.SetChildIndex(Me.XrLabel7, 0)
            Me.PageHeader.Controls.SetChildIndex(Me.XrLabel10, 0)
            Me.PageHeader.Controls.SetChildIndex(Me.XrLabel3, 0)
            Me.PageHeader.Controls.SetChildIndex(Me.XrLabel4, 0)
            Me.PageHeader.Controls.SetChildIndex(Me.XrLabel18, 0)
            Me.PageHeader.Controls.SetChildIndex(Me.XrLabel19, 0)
            Me.PageHeader.Controls.SetChildIndex(Me.XrLabel2, 0)
            Me.PageHeader.Controls.SetChildIndex(Me.XrLabel8, 0)
            Me.PageHeader.Controls.SetChildIndex(Me.XrLabel16, 0)
            Me.PageHeader.Controls.SetChildIndex(Me.XrLabel17, 0)
            '
            'XrLabel_Title
            '
            Me.XrLabel_Title.StylePriority.UseFont = False
            '
            'XrPageInfo_PageNumber
            '
            Me.XrPageInfo_PageNumber.LocationFloat = New DevExpress.Utils.PointFloat(917.0!, 8.0!)
            Me.XrPageInfo_PageNumber.StylePriority.UseTextAlignment = False
            '
            'XrPanel_AgencyAddress
            '
            Me.XrPanel_AgencyAddress.LocationFloat = New DevExpress.Utils.PointFloat(742.0!, 0.0!)
            '
            'XRLabel_Agency_Name
            '
            Me.XRLabel_Agency_Name.StylePriority.UseTextAlignment = False
            '
            'XrLabel_Agency_Address3
            '
            Me.XrLabel_Agency_Address3.StylePriority.UseFont = False
            Me.XrLabel_Agency_Address3.StylePriority.UseTextAlignment = False
            '
            'XrLabel_Agency_Address1
            '
            Me.XrLabel_Agency_Address1.StylePriority.UseFont = False
            Me.XrLabel_Agency_Address1.StylePriority.UseTextAlignment = False
            '
            'XrLabel_Agency_Phone
            '
            Me.XrLabel_Agency_Phone.StylePriority.UseFont = False
            Me.XrLabel_Agency_Phone.StylePriority.UseTextAlignment = False
            '
            'XrLabel_Agency_Address2
            '
            Me.XrLabel_Agency_Address2.StylePriority.UseFont = False
            Me.XrLabel_Agency_Address2.StylePriority.UseTextAlignment = False
            '
            'Detail
            '
            Me.Detail.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel_pmt_amount, Me.XrLabel_pmt_date, Me.XrLabel_invoice_register, Me.XrLabel_printed, Me.XrLabel_adj_amount, Me.XrLabel_inv_date, Me.XrLabel_balance, Me.XrLabel_adj_date, Me.XrLabel_inv_amount})
            Me.Detail.HeightF = 15.0!
            '
            'XrLabel1
            '
            Me.XrLabel1.LocationFloat = New DevExpress.Utils.PointFloat(0.00002384186!, 10.00001!)
            Me.XrLabel1.Name = "XrLabel1"
            Me.XrLabel1.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel1.SizeF = New System.Drawing.SizeF(742.0!, 20.83333!)
            Me.XrLabel1.Text = "[[creditor]] [creditor_name]"
            '
            'GroupFooter1
            '
            Me.GroupFooter1.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel33, Me.XrLabel_group_pmt_amount, Me.XrLabel_group_adj_amount, Me.XrLabel_group_balance, Me.XrLabel_group_inv_amount, Me.XrLine2})
            Me.GroupFooter1.GroupUnion = DevExpress.XtraReports.UI.GroupFooterUnion.WithLastDetail
            Me.GroupFooter1.HeightF = 38.0!
            Me.GroupFooter1.KeepTogether = True
            Me.GroupFooter1.Name = "GroupFooter1"
            Me.GroupFooter1.StyleName = "XrControlStyle_Totals"
            '
            'XrLabel33
            '
            Me.XrLabel33.LocationFloat = New DevExpress.Utils.PointFloat(39.99999!, 12.5!)
            Me.XrLabel33.Name = "XrLabel33"
            Me.XrLabel33.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel33.SizeF = New System.Drawing.SizeF(96.12501!, 15.0!)
            Me.XrLabel33.StylePriority.UseTextAlignment = False
            Me.XrLabel33.Text = "SUBTOTAL"
            Me.XrLabel33.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            '
            'XrLabel_group_pmt_amount
            '
            Me.XrLabel_group_pmt_amount.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "pmt_amount")})
            Me.XrLabel_group_pmt_amount.LocationFloat = New DevExpress.Utils.PointFloat(534.7084!, 12.5!)
            Me.XrLabel_group_pmt_amount.Name = "XrLabel_group_pmt_amount"
            Me.XrLabel_group_pmt_amount.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_group_pmt_amount.SizeF = New System.Drawing.SizeF(107.2916!, 15.0!)
            Me.XrLabel_group_pmt_amount.StylePriority.UseTextAlignment = False
            XrSummary1.FormatString = "{0:c}"
            XrSummary1.Running = DevExpress.XtraReports.UI.SummaryRunning.Group
            Me.XrLabel_group_pmt_amount.Summary = XrSummary1
            Me.XrLabel_group_pmt_amount.Text = " "
            Me.XrLabel_group_pmt_amount.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel_group_adj_amount
            '
            Me.XrLabel_group_adj_amount.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "adj_amount")})
            Me.XrLabel_group_adj_amount.LocationFloat = New DevExpress.Utils.PointFloat(354.875!, 12.5!)
            Me.XrLabel_group_adj_amount.Name = "XrLabel_group_adj_amount"
            Me.XrLabel_group_adj_amount.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_group_adj_amount.SizeF = New System.Drawing.SizeF(103.125!, 15.0!)
            Me.XrLabel_group_adj_amount.StylePriority.UseTextAlignment = False
            XrSummary2.FormatString = "{0:c}"
            XrSummary2.Running = DevExpress.XtraReports.UI.SummaryRunning.Group
            Me.XrLabel_group_adj_amount.Summary = XrSummary2
            Me.XrLabel_group_adj_amount.Text = " "
            Me.XrLabel_group_adj_amount.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel_group_balance
            '
            Me.XrLabel_group_balance.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "balance")})
            Me.XrLabel_group_balance.LocationFloat = New DevExpress.Utils.PointFloat(726.375!, 12.5!)
            Me.XrLabel_group_balance.Name = "XrLabel_group_balance"
            Me.XrLabel_group_balance.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_group_balance.SizeF = New System.Drawing.SizeF(108.3333!, 15.0!)
            Me.XrLabel_group_balance.StylePriority.UseTextAlignment = False
            XrSummary3.FormatString = "{0:c}"
            XrSummary3.Running = DevExpress.XtraReports.UI.SummaryRunning.Group
            Me.XrLabel_group_balance.Summary = XrSummary3
            Me.XrLabel_group_balance.Text = " "
            Me.XrLabel_group_balance.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel_group_inv_amount
            '
            Me.XrLabel_group_inv_amount.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "inv_amount")})
            Me.XrLabel_group_inv_amount.LocationFloat = New DevExpress.Utils.PointFloat(168.0833!, 12.5!)
            Me.XrLabel_group_inv_amount.Name = "XrLabel_group_inv_amount"
            Me.XrLabel_group_inv_amount.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_group_inv_amount.SizeF = New System.Drawing.SizeF(97.91667!, 15.0!)
            Me.XrLabel_group_inv_amount.StylePriority.UseTextAlignment = False
            XrSummary4.FormatString = "{0:c}"
            XrSummary4.Running = DevExpress.XtraReports.UI.SummaryRunning.Group
            Me.XrLabel_group_inv_amount.Summary = XrSummary4
            Me.XrLabel_group_inv_amount.Text = " "
            Me.XrLabel_group_inv_amount.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLine2
            '
            Me.XrLine2.LocationFloat = New DevExpress.Utils.PointFloat(40.00001!, 0.0!)
            Me.XrLine2.Name = "XrLine2"
            Me.XrLine2.SizeF = New System.Drawing.SizeF(794.7083!, 12.5!)
            '
            'XrLabel_inv_date
            '
            Me.XrLabel_inv_date.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "inv_date", "{0:d}")})
            Me.XrLabel_inv_date.LocationFloat = New DevExpress.Utils.PointFloat(122.0!, 0.0!)
            Me.XrLabel_inv_date.Name = "XrLabel_inv_date"
            Me.XrLabel_inv_date.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_inv_date.SizeF = New System.Drawing.SizeF(60.0!, 15.0!)
            Me.XrLabel_inv_date.StylePriority.UseTextAlignment = False
            Me.XrLabel_inv_date.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel_adj_date
            '
            Me.XrLabel_adj_date.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "adj_date", "{0:d}")})
            Me.XrLabel_adj_date.LocationFloat = New DevExpress.Utils.PointFloat(308.0!, 0.0!)
            Me.XrLabel_adj_date.Name = "XrLabel_adj_date"
            Me.XrLabel_adj_date.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_adj_date.SizeF = New System.Drawing.SizeF(60.0!, 15.0!)
            Me.XrLabel_adj_date.StylePriority.UseTextAlignment = False
            Me.XrLabel_adj_date.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel_pmt_date
            '
            Me.XrLabel_pmt_date.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "pmt_date", "{0:d}")})
            Me.XrLabel_pmt_date.LocationFloat = New DevExpress.Utils.PointFloat(492.0!, 0.0!)
            Me.XrLabel_pmt_date.Name = "XrLabel_pmt_date"
            Me.XrLabel_pmt_date.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_pmt_date.SizeF = New System.Drawing.SizeF(60.0!, 15.0!)
            Me.XrLabel_pmt_date.StylePriority.UseTextAlignment = False
            Me.XrLabel_pmt_date.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel_printed
            '
            Me.XrLabel_printed.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "printed", "{0:d}")})
            Me.XrLabel_printed.LocationFloat = New DevExpress.Utils.PointFloat(667.0!, 0.0!)
            Me.XrLabel_printed.Name = "XrLabel_printed"
            Me.XrLabel_printed.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_printed.SizeF = New System.Drawing.SizeF(75.0!, 14.99999!)
            Me.XrLabel_printed.StylePriority.UseTextAlignment = False
            Me.XrLabel_printed.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel_invoice_register
            '
            Me.XrLabel_invoice_register.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "invoice_register", "{0:f0}")})
            Me.XrLabel_invoice_register.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
            Me.XrLabel_invoice_register.Name = "XrLabel_invoice_register"
            Me.XrLabel_invoice_register.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_invoice_register.SizeF = New System.Drawing.SizeF(108.0!, 14.99999!)
            Me.XrLabel_invoice_register.StylePriority.UseTextAlignment = False
            Me.XrLabel_invoice_register.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel_inv_amount
            '
            Me.XrLabel_inv_amount.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "inv_amount", "{0:c}")})
            Me.XrLabel_inv_amount.LocationFloat = New DevExpress.Utils.PointFloat(191.0!, 0.0!)
            Me.XrLabel_inv_amount.Name = "XrLabel_inv_amount"
            Me.XrLabel_inv_amount.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_inv_amount.SizeF = New System.Drawing.SizeF(75.0!, 15.0!)
            Me.XrLabel_inv_amount.StylePriority.UseTextAlignment = False
            Me.XrLabel_inv_amount.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel_pmt_amount
            '
            Me.XrLabel_pmt_amount.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "pmt_amount", "{0:c}")})
            Me.XrLabel_pmt_amount.LocationFloat = New DevExpress.Utils.PointFloat(567.0!, 0.0!)
            Me.XrLabel_pmt_amount.Name = "XrLabel_pmt_amount"
            Me.XrLabel_pmt_amount.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_pmt_amount.SizeF = New System.Drawing.SizeF(75.0!, 15.0!)
            Me.XrLabel_pmt_amount.StylePriority.UseTextAlignment = False
            Me.XrLabel_pmt_amount.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel_adj_amount
            '
            Me.XrLabel_adj_amount.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "adj_amount", "{0:c}")})
            Me.XrLabel_adj_amount.LocationFloat = New DevExpress.Utils.PointFloat(383.0!, 0.0!)
            Me.XrLabel_adj_amount.Name = "XrLabel_adj_amount"
            Me.XrLabel_adj_amount.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_adj_amount.SizeF = New System.Drawing.SizeF(75.0!, 15.0!)
            Me.XrLabel_adj_amount.StylePriority.UseTextAlignment = False
            Me.XrLabel_adj_amount.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel_balance
            '
            Me.XrLabel_balance.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "balance", "{0:c}")})
            Me.XrLabel_balance.LocationFloat = New DevExpress.Utils.PointFloat(759.7083!, 0.0!)
            Me.XrLabel_balance.Name = "XrLabel_balance"
            Me.XrLabel_balance.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_balance.SizeF = New System.Drawing.SizeF(75.0!, 15.0!)
            Me.XrLabel_balance.StylePriority.UseTextAlignment = False
            Me.XrLabel_balance.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'GroupHeader1
            '
            Me.GroupHeader1.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel1})
            Me.GroupHeader1.GroupFields.AddRange(New DevExpress.XtraReports.UI.GroupField() {New DevExpress.XtraReports.UI.GroupField("creditor", DevExpress.XtraReports.UI.XRColumnSortOrder.Ascending)})
            Me.GroupHeader1.HeightF = 44.0!
            Me.GroupHeader1.KeepTogether = True
            Me.GroupHeader1.Level = 1
            Me.GroupHeader1.Name = "GroupHeader1"
            Me.GroupHeader1.RepeatEveryPage = True
            Me.GroupHeader1.StyleName = "XrControlStyle_GroupHeader"
            '
            'XrLabel10
            '
            Me.XrLabel10.BorderColor = System.Drawing.Color.Black
            Me.XrLabel10.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel10.ForeColor = System.Drawing.Color.Black
            Me.XrLabel10.LocationFloat = New DevExpress.Utils.PointFloat(759.7083!, 131.875!)
            Me.XrLabel10.Name = "XrLabel10"
            Me.XrLabel10.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel10.SizeF = New System.Drawing.SizeF(75.0!, 15.0!)
            Me.XrLabel10.StylePriority.UseBorderColor = False
            Me.XrLabel10.StylePriority.UseFont = False
            Me.XrLabel10.StylePriority.UseForeColor = False
            Me.XrLabel10.StylePriority.UseTextAlignment = False
            Me.XrLabel10.Text = "BALANCE"
            Me.XrLabel10.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel3
            '
            Me.XrLabel3.BorderColor = System.Drawing.Color.Black
            Me.XrLabel3.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel3.ForeColor = System.Drawing.Color.Black
            Me.XrLabel3.LocationFloat = New DevExpress.Utils.PointFloat(122.0!, 146.875!)
            Me.XrLabel3.Name = "XrLabel3"
            Me.XrLabel3.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel3.SizeF = New System.Drawing.SizeF(60.0!, 15.0!)
            Me.XrLabel3.StylePriority.UseBorderColor = False
            Me.XrLabel3.StylePriority.UseFont = False
            Me.XrLabel3.StylePriority.UseForeColor = False
            Me.XrLabel3.StylePriority.UseTextAlignment = False
            Me.XrLabel3.Text = "DATE"
            Me.XrLabel3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel4
            '
            Me.XrLabel4.BorderColor = System.Drawing.Color.Black
            Me.XrLabel4.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel4.ForeColor = System.Drawing.Color.Black
            Me.XrLabel4.LocationFloat = New DevExpress.Utils.PointFloat(191.0!, 146.875!)
            Me.XrLabel4.Name = "XrLabel4"
            Me.XrLabel4.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel4.SizeF = New System.Drawing.SizeF(75.0!, 15.0!)
            Me.XrLabel4.StylePriority.UseBorderColor = False
            Me.XrLabel4.StylePriority.UseFont = False
            Me.XrLabel4.StylePriority.UseForeColor = False
            Me.XrLabel4.StylePriority.UseTextAlignment = False
            Me.XrLabel4.Text = "AMOUNT"
            Me.XrLabel4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel5
            '
            Me.XrLabel5.BorderColor = System.Drawing.Color.Black
            Me.XrLabel5.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel5.ForeColor = System.Drawing.Color.Black
            Me.XrLabel5.LocationFloat = New DevExpress.Utils.PointFloat(304.2917!, 146.875!)
            Me.XrLabel5.Name = "XrLabel5"
            Me.XrLabel5.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel5.SizeF = New System.Drawing.SizeF(60.0!, 15.0!)
            Me.XrLabel5.StylePriority.UseBorderColor = False
            Me.XrLabel5.StylePriority.UseFont = False
            Me.XrLabel5.StylePriority.UseForeColor = False
            Me.XrLabel5.StylePriority.UseTextAlignment = False
            Me.XrLabel5.Text = "DATE"
            Me.XrLabel5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel6
            '
            Me.XrLabel6.BorderColor = System.Drawing.Color.Black
            Me.XrLabel6.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel6.ForeColor = System.Drawing.Color.Black
            Me.XrLabel6.LocationFloat = New DevExpress.Utils.PointFloat(383.0!, 146.875!)
            Me.XrLabel6.Name = "XrLabel6"
            Me.XrLabel6.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel6.SizeF = New System.Drawing.SizeF(75.0!, 15.0!)
            Me.XrLabel6.StylePriority.UseBorderColor = False
            Me.XrLabel6.StylePriority.UseFont = False
            Me.XrLabel6.StylePriority.UseForeColor = False
            Me.XrLabel6.StylePriority.UseTextAlignment = False
            Me.XrLabel6.Text = "AMOUNT"
            Me.XrLabel6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel7
            '
            Me.XrLabel7.BorderColor = System.Drawing.Color.Black
            Me.XrLabel7.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel7.ForeColor = System.Drawing.Color.Black
            Me.XrLabel7.LocationFloat = New DevExpress.Utils.PointFloat(567.0!, 146.875!)
            Me.XrLabel7.Name = "XrLabel7"
            Me.XrLabel7.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel7.SizeF = New System.Drawing.SizeF(75.0!, 15.0!)
            Me.XrLabel7.StylePriority.UseBorderColor = False
            Me.XrLabel7.StylePriority.UseFont = False
            Me.XrLabel7.StylePriority.UseForeColor = False
            Me.XrLabel7.StylePriority.UseTextAlignment = False
            Me.XrLabel7.Text = "AMOUNT"
            Me.XrLabel7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel8
            '
            Me.XrLabel8.BorderColor = System.Drawing.Color.Black
            Me.XrLabel8.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel8.ForeColor = System.Drawing.Color.Black
            Me.XrLabel8.LocationFloat = New DevExpress.Utils.PointFloat(492.0!, 146.875!)
            Me.XrLabel8.Name = "XrLabel8"
            Me.XrLabel8.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel8.SizeF = New System.Drawing.SizeF(60.0!, 15.0!)
            Me.XrLabel8.StylePriority.UseBorderColor = False
            Me.XrLabel8.StylePriority.UseFont = False
            Me.XrLabel8.StylePriority.UseForeColor = False
            Me.XrLabel8.StylePriority.UseTextAlignment = False
            Me.XrLabel8.Text = "DATE"
            Me.XrLabel8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel16
            '
            Me.XrLabel16.BorderColor = System.Drawing.Color.Black
            Me.XrLabel16.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel16.ForeColor = System.Drawing.Color.Black
            Me.XrLabel16.LocationFloat = New DevExpress.Utils.PointFloat(667.0!, 131.875!)
            Me.XrLabel16.Name = "XrLabel16"
            Me.XrLabel16.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel16.SizeF = New System.Drawing.SizeF(75.0!, 14.99999!)
            Me.XrLabel16.StylePriority.UseBorderColor = False
            Me.XrLabel16.StylePriority.UseFont = False
            Me.XrLabel16.StylePriority.UseForeColor = False
            Me.XrLabel16.StylePriority.UseTextAlignment = False
            Me.XrLabel16.Text = "PRINTED"
            Me.XrLabel16.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel17
            '
            Me.XrLabel17.BorderColor = System.Drawing.Color.Black
            Me.XrLabel17.Borders = DevExpress.XtraPrinting.BorderSide.Bottom
            Me.XrLabel17.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel17.ForeColor = System.Drawing.Color.Black
            Me.XrLabel17.LocationFloat = New DevExpress.Utils.PointFloat(39.99996!, 131.875!)
            Me.XrLabel17.Name = "XrLabel17"
            Me.XrLabel17.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel17.SizeF = New System.Drawing.SizeF(226.0!, 14.99999!)
            Me.XrLabel17.StylePriority.UseBorderColor = False
            Me.XrLabel17.StylePriority.UseBorders = False
            Me.XrLabel17.StylePriority.UseFont = False
            Me.XrLabel17.StylePriority.UseForeColor = False
            Me.XrLabel17.StylePriority.UseTextAlignment = False
            Me.XrLabel17.Text = "NUMBER"
            Me.XrLabel17.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
            '
            'XrLabel18
            '
            Me.XrLabel18.BorderColor = System.Drawing.Color.Black
            Me.XrLabel18.Borders = DevExpress.XtraPrinting.BorderSide.Bottom
            Me.XrLabel18.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel18.ForeColor = System.Drawing.Color.Black
            Me.XrLabel18.LocationFloat = New DevExpress.Utils.PointFloat(308.0001!, 131.875!)
            Me.XrLabel18.Name = "XrLabel18"
            Me.XrLabel18.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel18.SizeF = New System.Drawing.SizeF(150.0!, 15.0!)
            Me.XrLabel18.StylePriority.UseBorderColor = False
            Me.XrLabel18.StylePriority.UseBorders = False
            Me.XrLabel18.StylePriority.UseFont = False
            Me.XrLabel18.StylePriority.UseForeColor = False
            Me.XrLabel18.StylePriority.UseTextAlignment = False
            Me.XrLabel18.Text = "ADJUSTMENTS"
            Me.XrLabel18.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
            '
            'XrLabel19
            '
            Me.XrLabel19.BorderColor = System.Drawing.Color.Black
            Me.XrLabel19.Borders = DevExpress.XtraPrinting.BorderSide.Bottom
            Me.XrLabel19.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel19.ForeColor = System.Drawing.Color.Black
            Me.XrLabel19.LocationFloat = New DevExpress.Utils.PointFloat(492.0!, 131.875!)
            Me.XrLabel19.Name = "XrLabel19"
            Me.XrLabel19.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel19.SizeF = New System.Drawing.SizeF(150.0!, 15.0!)
            Me.XrLabel19.StylePriority.UseBorderColor = False
            Me.XrLabel19.StylePriority.UseBorders = False
            Me.XrLabel19.StylePriority.UseFont = False
            Me.XrLabel19.StylePriority.UseForeColor = False
            Me.XrLabel19.StylePriority.UseTextAlignment = False
            Me.XrLabel19.Text = "PAYMENTS"
            Me.XrLabel19.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
            '
            'XrLabel2
            '
            Me.XrLabel2.BorderColor = System.Drawing.Color.Black
            Me.XrLabel2.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel2.ForeColor = System.Drawing.Color.Black
            Me.XrLabel2.LocationFloat = New DevExpress.Utils.PointFloat(39.99999!, 146.875!)
            Me.XrLabel2.Name = "XrLabel2"
            Me.XrLabel2.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel2.SizeF = New System.Drawing.SizeF(68.0!, 15.0!)
            Me.XrLabel2.StylePriority.UseBorderColor = False
            Me.XrLabel2.StylePriority.UseFont = False
            Me.XrLabel2.StylePriority.UseForeColor = False
            Me.XrLabel2.StylePriority.UseTextAlignment = False
            Me.XrLabel2.Text = "NUMBER"
            Me.XrLabel2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'ParameterShowAll
            '
            Me.ParameterShowAll.Name = "ParameterShowAll"
            Me.ParameterShowAll.Type = GetType(Boolean)
            Me.ParameterShowAll.Visible = False
            '
            'CreditorInvoiceStatusReport
            '
            Me.Bands.AddRange(New DevExpress.XtraReports.UI.Band() {Me.Detail, Me.PageHeader, Me.PageFooter, Me.GroupFooter1, Me.GroupHeader1, Me.TopMarginBand1, Me.BottomMarginBand1})
            Me.Landscape = True
            Me.PageHeight = 850
            Me.PageWidth = 1100
            Me.Parameters.AddRange(New DevExpress.XtraReports.Parameters.Parameter() {Me.ParameterShowAll})
            Me.ScriptLanguage = DevExpress.XtraReports.ScriptLanguage.VisualBasic
            Me.ScriptReferencesString = resources.GetString("$this.ScriptReferencesString")
            Me.Scripts.OnBeforePrint = "CreditorInvoiceStatusReport_BeforePrint"
            Me.ScriptsSource = resources.GetString("$this.ScriptsSource")
            Me.StyleSheet.AddRange(New DevExpress.XtraReports.UI.XRControlStyle() {Me.XrControlStyle_Header, Me.XrControlStyle_Totals, Me.XrControlStyle_HeaderPannel, Me.XrControlStyle_GroupHeader})
            Me.Version = "11.2"
            Me.Controls.SetChildIndex(Me.BottomMarginBand1, 0)
            Me.Controls.SetChildIndex(Me.TopMarginBand1, 0)
            Me.Controls.SetChildIndex(Me.GroupHeader1, 0)
            Me.Controls.SetChildIndex(Me.GroupFooter1, 0)
            Me.Controls.SetChildIndex(Me.PageFooter, 0)
            Me.Controls.SetChildIndex(Me.PageHeader, 0)
            Me.Controls.SetChildIndex(Me.Detail, 0)
            CType(Me, System.ComponentModel.ISupportInitialize).EndInit()

        End Sub

        Friend WithEvents XrLabel1 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents GroupFooter1 As DevExpress.XtraReports.UI.GroupFooterBand
        Friend WithEvents XrLabel_pmt_date As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_invoice_register As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_printed As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_inv_date As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_adj_date As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_pmt_amount As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_adj_amount As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_balance As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_inv_amount As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel33 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_group_pmt_amount As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_group_adj_amount As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_group_balance As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_group_inv_amount As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLine2 As DevExpress.XtraReports.UI.XRLine
        Friend WithEvents GroupHeader1 As DevExpress.XtraReports.UI.GroupHeaderBand
        Friend WithEvents XrLabel17 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel16 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel8 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel2 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel19 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel18 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel4 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel3 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel10 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel7 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel6 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel5 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents ParameterShowAll As DevExpress.XtraReports.Parameters.Parameter
    End Class
End Namespace
