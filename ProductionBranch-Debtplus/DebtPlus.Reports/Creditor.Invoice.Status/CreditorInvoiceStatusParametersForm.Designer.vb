Namespace Creditor.Invoice.Status
    <Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
    Partial Class CreditorInvoiceStatusParametersForm

        'Form overrides dispose to clean up the component list.
        <System.Diagnostics.DebuggerNonUserCode()> _
        Protected Overrides Sub Dispose(ByVal disposing As Boolean)
            Try
                If disposing AndAlso components IsNot Nothing Then
                    components.Dispose()
                End If
            Finally
                MyBase.Dispose(disposing)
            End Try
        End Sub

        'Required by the Windows Form Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Windows Form Designer
        'It can be modified using the Windows Form Designer.  
        'Do not modify it using the code editor.
        <System.Diagnostics.DebuggerStepThrough()> _
        Private Sub InitializeComponent()
            Dim ComboboxItem2 As DebtPlus.Data.Controls.ComboboxItem = New DebtPlus.Data.Controls.ComboboxItem
            Me.CheckEdit1 = New DevExpress.XtraEditors.CheckEdit
            CType(Me.XrGroup_param_08_1, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.XrGroup_param_08_1.SuspendLayout()
            CType(Me.XrCombo_param_08_1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.XrDate_param_08_2.Properties.VistaTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.XrDate_param_08_2.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.XrDate_param_08_1.Properties.VistaTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.XrDate_param_08_1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.DxErrorProvider1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.CheckEdit1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.SuspendLayout()
            '
            'XrCombo_param_08_1
            '
            ComboboxItem2.tag = Nothing
            ComboboxItem2.value = DebtPlus.Utils.DateRange.Today
            Me.XrCombo_param_08_1.EditValue = ComboboxItem2
            '
            'XrDate_param_08_2
            '
            Me.XrDate_param_08_2.Properties.VistaTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton})
            '
            'XrDate_param_08_1
            '
            Me.XrDate_param_08_1.Properties.VistaTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton})
            '
            'ButtonOK
            '
            Me.ButtonOK.TabIndex = 2
            '
            'ButtonCancel
            '
            Me.ButtonCancel.TabIndex = 3
            '
            'CheckEdit1
            '
            Me.CheckEdit1.Location = New System.Drawing.Point(6, 127)
            Me.CheckEdit1.Name = "CheckEdit1"
            Me.CheckEdit1.Properties.Caption = "Show All Items"
            Me.CheckEdit1.Size = New System.Drawing.Size(211, 19)
            Me.CheckEdit1.TabIndex = 1
            '
            'ParametersForm
            '
            Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
            Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
            Me.ClientSize = New System.Drawing.Size(336, 151)
            Me.Controls.Add(Me.CheckEdit1)
            Me.Name = "CreditorInvoiceStatusParametersForm"
            Me.Controls.SetChildIndex(Me.CheckEdit1, 0)
            Me.Controls.SetChildIndex(Me.ButtonOK, 0)
            Me.Controls.SetChildIndex(Me.ButtonCancel, 0)
            Me.Controls.SetChildIndex(Me.XrGroup_param_08_1, 0)
            CType(Me.XrGroup_param_08_1, System.ComponentModel.ISupportInitialize).EndInit()
            Me.XrGroup_param_08_1.ResumeLayout(False)
            Me.XrGroup_param_08_1.PerformLayout()
            CType(Me.XrCombo_param_08_1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.XrDate_param_08_2.Properties.VistaTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.XrDate_param_08_2.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.XrDate_param_08_1.Properties.VistaTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.XrDate_param_08_1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.DxErrorProvider1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.CheckEdit1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            Me.ResumeLayout(False)

        End Sub
        Friend WithEvents CheckEdit1 As DevExpress.XtraEditors.CheckEdit
    End Class
End Namespace
