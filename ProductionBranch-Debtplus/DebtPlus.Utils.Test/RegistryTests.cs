﻿using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace DebtPlus.Utils.Test
{
    [TestClass]
    public class RegistryTests
    {
        private string folder = "folder";
        private string key = "key";
        private string testKey = @"Software\DebtPlusRegistryTests";
        private string value = "value2";

        [TestInitialize]
        public void Initialize()
        {
            Registry.Hive = "HKEY_CURRENT_USER";
            Registry.RegistryBaseKey = testKey;
        }

        [TestMethod]
        public void SetRegistryBaseKey()
        {
            Assert.AreEqual(Registry.RegistryBaseKey, testKey);
        }

        [TestMethod]
        public void SetValue()
        {
            Assert.AreEqual(Registry.SetValue(folder, key, value), true);
        }

        [TestMethod]
        public void CreateRead()
        {
            //DebtPlus.Utils.Registry.SetValue(folder, key, value);
            Assert.AreEqual(Registry.GetValue(folder, key), value);
        }
    }
}