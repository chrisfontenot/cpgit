#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}

#End Region

Option Compare Binary
Option Explicit On
Option Strict On

Imports System.ComponentModel
Imports DevExpress.XtraEditors

Public Class EditTemplateForm
    Inherits DebtPlus.Data.Forms.DebtPlusForm

    Public ReadOnly _ctx As Context = Nothing
    Public drv As DataRowView = Nothing

    Public Sub New()
        MyBase.New()
        InitializeComponent()
    End Sub

    Public Sub New(ByVal ctx As Context)
        MyClass.New(ctx, CType(Nothing, DataRowView))
    End Sub

    Public Sub New(ByVal ctx As Context, ByVal drv As DataRowView)
        MyBase.New()
        If ctx Is Nothing Then Throw New Exception("Cannot create a form without a Context object.")

        _ctx = ctx

        InitializeComponent()
        RegisterCommonHandlers()
        MyClass.drv = drv
    End Sub

    Private Sub RegisterCommonHandlers()
        AddHandler SimpleButton_OK.Click, AddressOf SimpleButton_OK_Click
        AddHandler SimpleButton_Cancel.Click, AddressOf SimpleButton_Cancel_Click
    End Sub

    Private Sub UnRegisterCommonHandlers()
        RemoveHandler SimpleButton_OK.Click, AddressOf SimpleButton_OK_Click
        RemoveHandler SimpleButton_Cancel.Click, AddressOf SimpleButton_Cancel_Click
    End Sub

#Region " WinDows Form Designer generated code "

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If components IsNot Nothing Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the WinDows Form Designer
    Private components As IContainer

    'NOTE: The following procedure is required by the WinDows Form Designer
    'It can be modified using the WinDows Form Designer.  
    'Do not modify it using the code editor.
    Public WithEvents SimpleButton_OK As SimpleButton
    Public WithEvents SimpleButton_Cancel As SimpleButton

    <DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.SimpleButton_OK = New SimpleButton()
        Me.SimpleButton_Cancel = New SimpleButton()
        CType(Me.DxErrorProvider1, ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'SimpleButton_OK
        '
        Me.SimpleButton_OK.Anchor = CType((AnchorStyles.Top Or AnchorStyles.Right), AnchorStyles)
        Me.SimpleButton_OK.Enabled = False
        Me.SimpleButton_OK.Location = New Point(472, 43)
        Me.SimpleButton_OK.Name = "SimpleButton_OK"
        Me.SimpleButton_OK.Size = New Size(72, 26)
        Me.SimpleButton_OK.TabIndex = 18
        Me.SimpleButton_OK.Text = "&OK"
        Me.SimpleButton_OK.ToolTip = "Click here to commit the changes to the database"
        Me.SimpleButton_OK.ToolTipController = Me.ToolTipController1
        '
        'SimpleButton_Cancel
        '
        Me.SimpleButton_Cancel.Anchor = CType((AnchorStyles.Top Or AnchorStyles.Right), AnchorStyles)
        Me.SimpleButton_Cancel.Location = New Point(472, 86)
        Me.SimpleButton_Cancel.Name = "SimpleButton_Cancel"
        Me.SimpleButton_Cancel.Size = New Size(72, 26)
        Me.SimpleButton_Cancel.TabIndex = 19
        Me.SimpleButton_Cancel.Text = "&Cancel"
        Me.SimpleButton_Cancel.ToolTip = "Click here to cancel the dialog and return to the previous form."
        Me.SimpleButton_Cancel.ToolTipController = Me.ToolTipController1
        '
        'EditTemplateForm
        '
        Me.AcceptButton = Me.SimpleButton_OK
        Me.AutoScaleBaseSize = New Size(5, 14)
        Me.CancelButton = Me.SimpleButton_Cancel
        Me.ClientSize = New Size(568, 316)
        Me.Controls.Add(Me.SimpleButton_Cancel)
        Me.Controls.Add(Me.SimpleButton_OK)
        Me.FormBorderStyle = FormBorderStyle.FixedDialog
        Me.MaximizeBox = False
        Me.Name = "EditTemplateForm"
        Me.Text = "Edit Item"
        CType(Me.DxErrorProvider1, ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
    End Sub

#End Region

    ''' <summary>
    ''' Handle the click of the OK button. This routine would normally generate the OK dialog result
    ''' but it may be overriden to do anything else.
    ''' </summary>
    Protected Overridable Sub SimpleButton_OK_Click(sender As Object, e As EventArgs)
        DialogResult = DialogResult.OK
        UnRegisterCommonHandlers()
    End Sub

    ''' <summary>
    ''' Handle the click of the CANCEL button. This routine would normally generate a CANCEL event
    ''' but it may be overriden as needed to do anything else on the CANCEL operation.
    ''' </summary>
    Protected Overridable Sub SimpleButton_Cancel_Click(sender As Object, e As EventArgs)
        DialogResult = DialogResult.Cancel
        UnRegisterCommonHandlers()
    End Sub
End Class
