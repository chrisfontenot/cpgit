#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}
#End Region

Option Compare Binary
Option Explicit On
Option Strict On

Public MustInherit Class MainTemplateForm
    Inherits DebtPlus.Data.Forms.DebtPlusForm

    Protected _ctx As Context = Nothing

    Protected WithEvents RecordTable As DataTable

    Public Sub New()
        MyClass.New(New Context())
    End Sub

    Public Sub New(ByVal ctx As Context)
        MyBase.New()
        _ctx = ctx
        InitializeComponent()
        AddHandler GridView1.DoubleClick, AddressOf GridView1_DoubleClick
    End Sub

#Region " WinDows Form Designer generated code "

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If components IsNot Nothing Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the WinDows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the WinDows Form Designer
    'It can be modified using the WinDows Form Designer.  
    'Do not modify it using the code editor.
    Protected WithEvents SimpleButton_Cancel As DevExpress.XtraEditors.SimpleButton
    Protected WithEvents SimpleButton_Edit As DevExpress.XtraEditors.SimpleButton
    Protected WithEvents SimpleButton_New As DevExpress.XtraEditors.SimpleButton
    Protected WithEvents SimpleButton_SaveChanges As DevExpress.XtraEditors.SimpleButton
    Protected WithEvents PanelControl1 As DevExpress.XtraEditors.PanelControl
    Protected WithEvents GridControl1 As DevExpress.XtraGrid.GridControl
    Protected WithEvents GridView1 As DevExpress.XtraGrid.Views.Grid.GridView
    Protected WithEvents ContextMenu1 As ContextMenu
    Protected WithEvents ContextMenu1_Create As MenuItem
    Protected WithEvents ContextMenu1_Delete As MenuItem
    Protected WithEvents ContextMenu1_Edit As MenuItem
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.ContextMenu1 = New System.Windows.Forms.ContextMenu
        Me.ContextMenu1_Edit = New System.Windows.Forms.MenuItem
        Me.ContextMenu1_Create = New System.Windows.Forms.MenuItem
        Me.ContextMenu1_Delete = New System.Windows.Forms.MenuItem
        Me.SimpleButton_Cancel = New DevExpress.XtraEditors.SimpleButton
        Me.SimpleButton_Edit = New DevExpress.XtraEditors.SimpleButton
        Me.SimpleButton_New = New DevExpress.XtraEditors.SimpleButton
        Me.SimpleButton_SaveChanges = New DevExpress.XtraEditors.SimpleButton
        Me.PanelControl1 = New DevExpress.XtraEditors.PanelControl
        Me.GridControl1 = New DevExpress.XtraGrid.GridControl
        Me.GridView1 = New DevExpress.XtraGrid.Views.Grid.GridView
        CType(Me.DxErrorProvider1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PanelControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelControl1.SuspendLayout()
        CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'ContextMenu1
        '
        Me.ContextMenu1.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.ContextMenu1_Edit, Me.ContextMenu1_Create, Me.ContextMenu1_Delete})
        '
        'ContextMenu1_Edit
        '
        Me.ContextMenu1_Edit.DefaultItem = True
        Me.ContextMenu1_Edit.Index = 0
        Me.ContextMenu1_Edit.Text = "Edit..."
        '
        'ContextMenu1_Create
        '
        Me.ContextMenu1_Create.Index = 1
        Me.ContextMenu1_Create.Text = "Create..."
        '
        'ContextMenu1_Delete
        '
        Me.ContextMenu1_Delete.Index = 2
        Me.ContextMenu1_Delete.Text = "Delete..."
        '
        'SimpleButton_Cancel
        '
        Me.SimpleButton_Cancel.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.SimpleButton_Cancel.Location = New System.Drawing.Point(440, 80)
        Me.SimpleButton_Cancel.Name = "SimpleButton_Cancel"
        Me.SimpleButton_Cancel.Size = New System.Drawing.Size(80, 26)
        Me.SimpleButton_Cancel.TabIndex = 1
        Me.SimpleButton_Cancel.Text = "&Quit"
        Me.SimpleButton_Cancel.ToolTip = "Click here to cancel the form and discard all of your changes."
        Me.SimpleButton_Cancel.ToolTipController = Me.ToolTipController1
        '
        'SimpleButton_Edit
        '
        Me.SimpleButton_Edit.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.SimpleButton_Edit.Location = New System.Drawing.Point(440, 128)
        Me.SimpleButton_Edit.Name = "SimpleButton_Edit"
        Me.SimpleButton_Edit.Size = New System.Drawing.Size(80, 26)
        Me.SimpleButton_Edit.TabIndex = 2
        Me.SimpleButton_Edit.Text = "&Edit"
        Me.SimpleButton_Edit.ToolTip = "Click here to edit the current item."
        Me.SimpleButton_Edit.ToolTipController = Me.ToolTipController1
        '
        'SimpleButton_New
        '
        Me.SimpleButton_New.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.SimpleButton_New.Location = New System.Drawing.Point(440, 176)
        Me.SimpleButton_New.Name = "SimpleButton_New"
        Me.SimpleButton_New.Size = New System.Drawing.Size(80, 26)
        Me.SimpleButton_New.TabIndex = 3
        Me.SimpleButton_New.Text = "&New"
        Me.SimpleButton_New.ToolTip = "If you wish to add a new item, click here."
        Me.SimpleButton_New.ToolTipController = Me.ToolTipController1
        '
        'SimpleButton_SaveChanges
        '
        Me.SimpleButton_SaveChanges.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.SimpleButton_SaveChanges.Enabled = False
        Me.SimpleButton_SaveChanges.Location = New System.Drawing.Point(440, 32)
        Me.SimpleButton_SaveChanges.Name = "SimpleButton_SaveChanges"
        Me.SimpleButton_SaveChanges.Size = New System.Drawing.Size(80, 26)
        Me.SimpleButton_SaveChanges.TabIndex = 4
        Me.SimpleButton_SaveChanges.Text = "&Save"
        Me.SimpleButton_SaveChanges.ToolTip = "Click here to save your changes to the database. They are not saved until you cli" & _
            "ck here."
        Me.SimpleButton_SaveChanges.ToolTipController = Me.ToolTipController1
        '
        'PanelControl1
        '
        Me.PanelControl1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.PanelControl1.AutoSize = True
        Me.PanelControl1.Controls.Add(Me.GridControl1)
        Me.PanelControl1.Location = New System.Drawing.Point(2, 8)
        Me.PanelControl1.Name = "PanelControl1"
        Me.PanelControl1.Size = New System.Drawing.Size(432, 279)
        Me.PanelControl1.TabIndex = 5
        '
        'GridControl1
        '
        Me.GridControl1.ContextMenu = Me.ContextMenu1
        Me.GridControl1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.GridControl1.Location = New System.Drawing.Point(2, 2)
        Me.GridControl1.MainView = Me.GridView1
        Me.GridControl1.Name = "GridControl1"
        Me.GridControl1.Size = New System.Drawing.Size(428, 275)
        Me.GridControl1.TabIndex = 2
        Me.GridControl1.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridView1})
        '
        'GridView1
        '
        Me.GridView1.Appearance.ColumnFilterButton.BackColor = System.Drawing.Color.FromArgb(CType(CType(104, Byte), Int32), CType(CType(184, Byte), Int32), CType(CType(251, Byte), Int32))
        Me.GridView1.Appearance.ColumnFilterButton.BorderColor = System.Drawing.Color.FromArgb(CType(CType(104, Byte), Int32), CType(CType(184, Byte), Int32), CType(CType(251, Byte), Int32))
        Me.GridView1.Appearance.ColumnFilterButton.ForeColor = System.Drawing.Color.White
        Me.GridView1.Appearance.ColumnFilterButton.Options.UseBackColor = True
        Me.GridView1.Appearance.ColumnFilterButton.Options.UseBorderColor = True
        Me.GridView1.Appearance.ColumnFilterButton.Options.UseForeColor = True
        Me.GridView1.Appearance.ColumnFilterButtonActive.BackColor = System.Drawing.Color.FromArgb(CType(CType(170, Byte), Int32), CType(CType(216, Byte), Int32), CType(CType(254, Byte), Int32))
        Me.GridView1.Appearance.ColumnFilterButtonActive.BorderColor = System.Drawing.Color.FromArgb(CType(CType(170, Byte), Int32), CType(CType(216, Byte), Int32), CType(CType(254, Byte), Int32))
        Me.GridView1.Appearance.ColumnFilterButtonActive.ForeColor = System.Drawing.Color.Black
        Me.GridView1.Appearance.ColumnFilterButtonActive.Options.UseBackColor = True
        Me.GridView1.Appearance.ColumnFilterButtonActive.Options.UseBorderColor = True
        Me.GridView1.Appearance.ColumnFilterButtonActive.Options.UseForeColor = True
        Me.GridView1.Appearance.Empty.BackColor = System.Drawing.Color.FromArgb(CType(CType(236, Byte), Int32), CType(CType(246, Byte), Int32), CType(CType(255, Byte), Int32))
        Me.GridView1.Appearance.Empty.BackColor2 = System.Drawing.Color.White
        Me.GridView1.Appearance.Empty.Options.UseBackColor = True
        Me.GridView1.Appearance.EvenRow.BackColor = System.Drawing.Color.FromArgb(CType(CType(247, Byte), Int32), CType(CType(251, Byte), Int32), CType(CType(255, Byte), Int32))
        Me.GridView1.Appearance.EvenRow.BorderColor = System.Drawing.Color.FromArgb(CType(CType(247, Byte), Int32), CType(CType(251, Byte), Int32), CType(CType(255, Byte), Int32))
        Me.GridView1.Appearance.EvenRow.ForeColor = System.Drawing.Color.Black
        Me.GridView1.Appearance.EvenRow.Options.UseBackColor = True
        Me.GridView1.Appearance.EvenRow.Options.UseBorderColor = True
        Me.GridView1.Appearance.EvenRow.Options.UseForeColor = True
        Me.GridView1.Appearance.FilterCloseButton.BackColor = System.Drawing.Color.FromArgb(CType(CType(104, Byte), Int32), CType(CType(184, Byte), Int32), CType(CType(251, Byte), Int32))
        Me.GridView1.Appearance.FilterCloseButton.BorderColor = System.Drawing.Color.FromArgb(CType(CType(104, Byte), Int32), CType(CType(184, Byte), Int32), CType(CType(251, Byte), Int32))
        Me.GridView1.Appearance.FilterCloseButton.ForeColor = System.Drawing.Color.White
        Me.GridView1.Appearance.FilterCloseButton.Options.UseBackColor = True
        Me.GridView1.Appearance.FilterCloseButton.Options.UseBorderColor = True
        Me.GridView1.Appearance.FilterCloseButton.Options.UseForeColor = True
        Me.GridView1.Appearance.FilterPanel.BackColor = System.Drawing.Color.FromArgb(CType(CType(236, Byte), Int32), CType(CType(246, Byte), Int32), CType(CType(255, Byte), Int32))
        Me.GridView1.Appearance.FilterPanel.BackColor2 = System.Drawing.Color.White
        Me.GridView1.Appearance.FilterPanel.ForeColor = System.Drawing.Color.Black
        Me.GridView1.Appearance.FilterPanel.Options.UseBackColor = True
        Me.GridView1.Appearance.FilterPanel.Options.UseForeColor = True
        Me.GridView1.Appearance.FixedLine.BackColor = System.Drawing.Color.FromArgb(CType(CType(59, Byte), Int32), CType(CType(133, Byte), Int32), CType(CType(195, Byte), Int32))
        Me.GridView1.Appearance.FixedLine.Options.UseBackColor = True
        Me.GridView1.Appearance.FocusedCell.BackColor = System.Drawing.Color.White
        Me.GridView1.Appearance.FocusedCell.ForeColor = System.Drawing.Color.Black
        Me.GridView1.Appearance.FocusedCell.Options.UseBackColor = True
        Me.GridView1.Appearance.FocusedCell.Options.UseForeColor = True
        Me.GridView1.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb(CType(CType(38, Byte), Int32), CType(CType(109, Byte), Int32), CType(CType(189, Byte), Int32))
        Me.GridView1.Appearance.FocusedRow.BorderColor = System.Drawing.Color.FromArgb(CType(CType(59, Byte), Int32), CType(CType(139, Byte), Int32), CType(CType(206, Byte), Int32))
        Me.GridView1.Appearance.FocusedRow.ForeColor = System.Drawing.Color.White
        Me.GridView1.Appearance.FocusedRow.Options.UseBackColor = True
        Me.GridView1.Appearance.FocusedRow.Options.UseBorderColor = True
        Me.GridView1.Appearance.FocusedRow.Options.UseForeColor = True
        Me.GridView1.Appearance.FooterPanel.BackColor = System.Drawing.Color.FromArgb(CType(CType(104, Byte), Int32), CType(CType(184, Byte), Int32), CType(CType(251, Byte), Int32))
        Me.GridView1.Appearance.FooterPanel.BorderColor = System.Drawing.Color.FromArgb(CType(CType(104, Byte), Int32), CType(CType(184, Byte), Int32), CType(CType(251, Byte), Int32))
        Me.GridView1.Appearance.FooterPanel.ForeColor = System.Drawing.Color.Black
        Me.GridView1.Appearance.FooterPanel.Options.UseBackColor = True
        Me.GridView1.Appearance.FooterPanel.Options.UseBorderColor = True
        Me.GridView1.Appearance.FooterPanel.Options.UseForeColor = True
        Me.GridView1.Appearance.GroupButton.BackColor = System.Drawing.Color.FromArgb(CType(CType(104, Byte), Int32), CType(CType(184, Byte), Int32), CType(CType(251, Byte), Int32))
        Me.GridView1.Appearance.GroupButton.BorderColor = System.Drawing.Color.FromArgb(CType(CType(104, Byte), Int32), CType(CType(184, Byte), Int32), CType(CType(251, Byte), Int32))
        Me.GridView1.Appearance.GroupButton.Options.UseBackColor = True
        Me.GridView1.Appearance.GroupButton.Options.UseBorderColor = True
        Me.GridView1.Appearance.GroupFooter.BackColor = System.Drawing.Color.FromArgb(CType(CType(170, Byte), Int32), CType(CType(216, Byte), Int32), CType(CType(254, Byte), Int32))
        Me.GridView1.Appearance.GroupFooter.BorderColor = System.Drawing.Color.FromArgb(CType(CType(170, Byte), Int32), CType(CType(216, Byte), Int32), CType(CType(254, Byte), Int32))
        Me.GridView1.Appearance.GroupFooter.ForeColor = System.Drawing.Color.Black
        Me.GridView1.Appearance.GroupFooter.Options.UseBackColor = True
        Me.GridView1.Appearance.GroupFooter.Options.UseBorderColor = True
        Me.GridView1.Appearance.GroupFooter.Options.UseForeColor = True
        Me.GridView1.Appearance.GroupPanel.BackColor = System.Drawing.Color.FromArgb(CType(CType(236, Byte), Int32), CType(CType(246, Byte), Int32), CType(CType(255, Byte), Int32))
        Me.GridView1.Appearance.GroupPanel.BackColor2 = System.Drawing.Color.White
        Me.GridView1.Appearance.GroupPanel.ForeColor = System.Drawing.Color.Black
        Me.GridView1.Appearance.GroupPanel.Options.UseBackColor = True
        Me.GridView1.Appearance.GroupPanel.Options.UseForeColor = True
        Me.GridView1.Appearance.GroupRow.BackColor = System.Drawing.Color.FromArgb(CType(CType(170, Byte), Int32), CType(CType(216, Byte), Int32), CType(CType(254, Byte), Int32))
        Me.GridView1.Appearance.GroupRow.BorderColor = System.Drawing.Color.FromArgb(CType(CType(170, Byte), Int32), CType(CType(216, Byte), Int32), CType(CType(254, Byte), Int32))
        Me.GridView1.Appearance.GroupRow.ForeColor = System.Drawing.Color.Black
        Me.GridView1.Appearance.GroupRow.Options.UseBackColor = True
        Me.GridView1.Appearance.GroupRow.Options.UseBorderColor = True
        Me.GridView1.Appearance.GroupRow.Options.UseForeColor = True
        Me.GridView1.Appearance.HeaderPanel.BackColor = System.Drawing.Color.FromArgb(CType(CType(139, Byte), Int32), CType(CType(201, Byte), Int32), CType(CType(254, Byte), Int32))
        Me.GridView1.Appearance.HeaderPanel.BorderColor = System.Drawing.Color.FromArgb(CType(CType(139, Byte), Int32), CType(CType(201, Byte), Int32), CType(CType(254, Byte), Int32))
        Me.GridView1.Appearance.HeaderPanel.ForeColor = System.Drawing.Color.Black
        Me.GridView1.Appearance.HeaderPanel.Options.UseBackColor = True
        Me.GridView1.Appearance.HeaderPanel.Options.UseBorderColor = True
        Me.GridView1.Appearance.HeaderPanel.Options.UseForeColor = True
        Me.GridView1.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Int32), CType(CType(170, Byte), Int32), CType(CType(225, Byte), Int32))
        Me.GridView1.Appearance.HideSelectionRow.BorderColor = System.Drawing.Color.FromArgb(CType(CType(83, Byte), Int32), CType(CType(155, Byte), Int32), CType(CType(215, Byte), Int32))
        Me.GridView1.Appearance.HideSelectionRow.ForeColor = System.Drawing.Color.FromArgb(CType(CType(236, Byte), Int32), CType(CType(246, Byte), Int32), CType(CType(255, Byte), Int32))
        Me.GridView1.Appearance.HideSelectionRow.Options.UseBackColor = True
        Me.GridView1.Appearance.HideSelectionRow.Options.UseBorderColor = True
        Me.GridView1.Appearance.HideSelectionRow.Options.UseForeColor = True
        Me.GridView1.Appearance.HorzLine.BackColor = System.Drawing.Color.FromArgb(CType(CType(104, Byte), Int32), CType(CType(184, Byte), Int32), CType(CType(251, Byte), Int32))
        Me.GridView1.Appearance.HorzLine.Options.UseBackColor = True
        Me.GridView1.Appearance.OddRow.BackColor = System.Drawing.Color.FromArgb(CType(CType(236, Byte), Int32), CType(CType(246, Byte), Int32), CType(CType(255, Byte), Int32))
        Me.GridView1.Appearance.OddRow.BorderColor = System.Drawing.Color.FromArgb(CType(CType(236, Byte), Int32), CType(CType(246, Byte), Int32), CType(CType(255, Byte), Int32))
        Me.GridView1.Appearance.OddRow.ForeColor = System.Drawing.Color.Black
        Me.GridView1.Appearance.OddRow.Options.UseBackColor = True
        Me.GridView1.Appearance.OddRow.Options.UseBorderColor = True
        Me.GridView1.Appearance.OddRow.Options.UseForeColor = True
        Me.GridView1.Appearance.Preview.Font = New System.Drawing.Font("Verdana", 7.5!)
        Me.GridView1.Appearance.Preview.ForeColor = System.Drawing.Color.FromArgb(CType(CType(83, Byte), Int32), CType(CType(155, Byte), Int32), CType(CType(215, Byte), Int32))
        Me.GridView1.Appearance.Preview.Options.UseFont = True
        Me.GridView1.Appearance.Preview.Options.UseForeColor = True
        Me.GridView1.Appearance.Row.BackColor = System.Drawing.Color.FromArgb(CType(CType(247, Byte), Int32), CType(CType(251, Byte), Int32), CType(CType(255, Byte), Int32))
        Me.GridView1.Appearance.Row.ForeColor = System.Drawing.Color.Black
        Me.GridView1.Appearance.Row.Options.UseBackColor = True
        Me.GridView1.Appearance.Row.Options.UseForeColor = True
        Me.GridView1.Appearance.RowSeparator.BackColor = System.Drawing.Color.FromArgb(CType(CType(236, Byte), Int32), CType(CType(246, Byte), Int32), CType(CType(255, Byte), Int32))
        Me.GridView1.Appearance.RowSeparator.BackColor2 = System.Drawing.Color.White
        Me.GridView1.Appearance.RowSeparator.Options.UseBackColor = True
        Me.GridView1.Appearance.SelectedRow.BackColor = System.Drawing.Color.FromArgb(CType(CType(83, Byte), Int32), CType(CType(155, Byte), Int32), CType(CType(215, Byte), Int32))
        Me.GridView1.Appearance.SelectedRow.ForeColor = System.Drawing.Color.White
        Me.GridView1.Appearance.SelectedRow.Options.UseBackColor = True
        Me.GridView1.Appearance.SelectedRow.Options.UseForeColor = True
        Me.GridView1.Appearance.TopNewRow.BackColor = System.Drawing.Color.White
        Me.GridView1.Appearance.TopNewRow.Options.UseBackColor = True
        Me.GridView1.Appearance.VertLine.BackColor = System.Drawing.Color.FromArgb(CType(CType(104, Byte), Int32), CType(CType(184, Byte), Int32), CType(CType(251, Byte), Int32))
        Me.GridView1.Appearance.VertLine.Options.UseBackColor = True
        Me.GridView1.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus
        Me.GridView1.GridControl = Me.GridControl1
        Me.GridView1.Name = "GridView1"
        Me.GridView1.OptionsBehavior.Editable = False
        Me.GridView1.OptionsNavigation.AutoFocusNewRow = True
        Me.GridView1.OptionsSelection.InvertSelection = True
        Me.GridView1.OptionsView.RowAutoHeight = True
        Me.GridView1.OptionsView.ShowGroupPanel = False
        Me.GridView1.OptionsView.ShowIndicator = False
        Me.GridView1.ShowButtonMode = DevExpress.XtraGrid.Views.Base.ShowButtonModeEnum.ShowForFocusedRow
        '
        'MainTemplateForm
        '
        Me.AutoScaleDimensions = New SizeF(6.0!, 13.0!)
        Me.ClientSize = New Size(528, 294)
        Me.Controls.Add(Me.PanelControl1)
        Me.Controls.Add(Me.SimpleButton_SaveChanges)
        Me.Controls.Add(Me.SimpleButton_New)
        Me.Controls.Add(Me.SimpleButton_Edit)
        Me.Controls.Add(Me.SimpleButton_Cancel)
        Me.LookAndFeel.UseDefaultLookAndFeel = True
        Me.Name = "MainTemplateForm"
        Me.Text = "Master List"
        CType(Me.DxErrorProvider1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PanelControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelControl1.ResumeLayout(False)
        CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

#End Region

    ''' <summary>
    ''' Process the Click event on the Cancel button to close the form
    ''' </summary>
    Protected Overridable Sub SimpleButton_Cancel_Click(ByVal sender As Object, ByVal e As EventArgs) Handles SimpleButton_Cancel.Click
        Close()
    End Sub

    ''' <summary>
    ''' The focused row changed. Enable the Edit button as needed.
    ''' </summary>
    Private Sub GridView1_FocusedRowChanged(ByVal sender As Object, ByVal e As DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs)
        SimpleButton_Edit.Enabled = e.FocusedRowHandle >= 0
    End Sub

    ''' <summary>
    ''' Double click event on the list
    ''' </summary>
    Private Sub GridView1_DoubleClick(ByVal sender As Object, ByVal e As EventArgs)
        Dim gv As DevExpress.XtraGrid.Views.Grid.GridView = GridView1
        Dim ctl As DevExpress.XtraGrid.GridControl = gv.GridControl
        Dim hi As DevExpress.XtraGrid.Views.Grid.ViewInfo.GridHitInfo = gv.CalcHitInfo((ctl.PointToClient(MousePosition)))
        Dim rowHandle As Int32 = hi.RowHandle

        ' Find the row view in the dataview object for this row.
        Dim obj As Object = Nothing
        If rowHandle >= 0 Then
            gv.FocusedRowHandle = rowHandle
            obj = gv.GetRow(rowHandle)
        End If

        ' If the row is defined then edit the row.
        If obj IsNot Nothing AndAlso AllowEdit(obj) Then
            OnEdit(obj)
        End If
    End Sub

    ''' <summary>
    ''' Edit the current record
    ''' </summary>
    Private Sub SimpleButton_Edit_Click(ByVal sender As Object, ByVal e As EventArgs) Handles SimpleButton_Edit.Click
        Dim rowHandle As Int32 = GridView1.FocusedRowHandle

        ' Find the row that is being edited
        Dim obj As Object = GridView1.GetRow(rowHandle)
        If obj IsNot Nothing Then
            GridView1.FocusedRowHandle = rowHandle
            If AllowEdit(obj) Then
                OnEdit(obj)
            End If
        End If
    End Sub

    ''' <summary>
    ''' Edit -> menu being shown
    ''' </summary>
    Private Sub ContextMenu1_Popup(ByVal sender As Object, ByVal e As EventArgs) Handles ContextMenu1.Popup
        Dim gv As DevExpress.XtraGrid.Views.Grid.GridView = GridView1
        Dim ctl As DevExpress.XtraGrid.GridControl = gv.GridControl
        Dim hi As DevExpress.XtraGrid.Views.Grid.ViewInfo.GridHitInfo = gv.CalcHitInfo((ctl.PointToClient(MousePosition)))
        Dim rowHandle As Int32 = hi.RowHandle

        ' Find the row view in the dataview object for this row.
        Dim obj As Object = Nothing
        If rowHandle >= 0 Then
            gv.FocusedRowHandle = rowHandle
            obj = gv.GetRow(rowHandle)
        End If

        ' If the row is defined then edit the row.
        If obj IsNot Nothing AndAlso AllowEdit(obj) Then
            ContextMenu1_Edit.Enabled = True
            ContextMenu1_Delete.Enabled = True
        Else
            ContextMenu1_Edit.Enabled = False
            ContextMenu1_Delete.Enabled = False
        End If
    End Sub

    Protected Overridable Function AllowEdit(ByVal obj As Object) As Boolean
        Return True
    End Function

    ''' <summary>
    ''' Edit -> Create menu item clicked
    ''' </summary>
    Private Sub ContextMenu1_Create_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ContextMenu1_Create.Click, SimpleButton_New.Click
        OnCreate(GridControl1.DataSource)
    End Sub

    Protected Overridable Sub OnCreate(ByVal obj As Object)

        ' Find the view to the data
        Dim vue As DataView
        If TypeOf GridControl1.DataSource Is DataView Then
            vue = CType(obj, DataView)
        Else
            vue = CType(obj, DataTable).DefaultView
        End If

        ' Edit the new row
        EditRow(vue.AddNew)
    End Sub

    ''' <summary>
    ''' Edit -> Edit menu item clicked
    ''' </summary>
    Private Sub ContextMenu1_Edit_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ContextMenu1_Edit.Click
        Dim gv As DevExpress.XtraGrid.Views.Grid.GridView = GridView1
        Dim rowHandle As Int32 = GridView1.FocusedRowHandle
        Dim obj As Object = Nothing

        If rowHandle >= 0 Then
            gv.FocusedRowHandle = rowHandle
            obj = gv.GetRow(rowHandle)
        End If

        ' If the row is defined then edit the row.
        If obj IsNot Nothing AndAlso AllowEdit(obj) Then
            OnEdit(obj)
        End If
    End Sub

    ''' <summary>
    ''' Edit -> Delete menu item clicked
    ''' </summary>
    Private Sub ContextMenu1_Delete_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ContextMenu1_Delete.Click
        Dim gv As DevExpress.XtraGrid.Views.Grid.GridView = GridView1
        Dim rowHandle As Int32 = GridView1.FocusedRowHandle
        Dim obj As Object = Nothing

        If rowHandle >= 0 Then
            gv.FocusedRowHandle = rowHandle
            obj = gv.GetRow(rowHandle)
        End If

        If obj IsNot Nothing Then
            OnDelete(obj)
        End If
    End Sub

    Protected Overridable Sub OnDelete(ByVal obj As Object)
        Dim drv As DataRowView = TryCast(obj, DataRowView)

        ' If the row is defined then edit the row.
        If drv IsNot Nothing Then
            If DeleteRow(drv) = DialogResult.Yes Then
                drv.Delete()
                SimpleButton_SaveChanges.Enabled = True
            End If
        End If
    End Sub

    ''' <summary>
    ''' Handle the edit of the information on the form
    ''' </summary>
    Protected Overridable Function EditRow(ByVal drv As DataRowView) As DialogResult
        Return DialogResult.Cancel
    End Function

    Protected Overridable Sub OnEdit(ByVal obj As Object)
        Dim drv As DataRowView = TryCast(obj, DataRowView)
        If drv IsNot Nothing AndAlso EditRow(drv) = Windows.Forms.DialogResult.OK Then
            SimpleButton_SaveChanges.Enabled = True
        End If
    End Sub

    ''' <summary>
    ''' Handle the edit of the information on the form
    ''' </summary>
    Protected Overridable Sub OnDeleteRow(ByVal obj As Object)
        Dim drv As DataRowView = TryCast(obj, DataRowView)
        If drv IsNot Nothing AndAlso DeleteRow(drv) = Windows.Forms.DialogResult.OK Then
            drv.Delete()
            SimpleButton_SaveChanges.Enabled = True
        End If
    End Sub

    Protected Overridable Function DeleteRow(ByVal drv As DataRowView) As System.Windows.Forms.DialogResult
        Return DebtPlus.Data.Prompts.RequestConfirmation_Delete()
    End Function

    ''' <summary>
    ''' Save changes to the database
    ''' </summary>
    Private Sub SimpleButton_SaveChanges_Click(ByVal sender As Object, ByVal e As EventArgs) Handles SimpleButton_SaveChanges.Click
        CommitChanges()
        SimpleButton_SaveChanges.Enabled = False
    End Sub

    ''' <summary>
    ''' Commit the changes to the database
    ''' </summary>
    Protected Overridable Sub CommitChanges()
    End Sub

    ''' <summary>
    ''' The form is being closed. Ask if changes should be saved?
    ''' </summary>
    Protected Overridable Sub MainTemplateForm_Closing(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles Me.Closing

        ' The records are dirty if the "save" button is enabled.
        If SimpleButton_SaveChanges.Enabled Then

            ' Ask and perform the save if desired.
            Select Case DebtPlus.Data.Forms.MessageBox.Show("You have unsaved changes to the database. Do you wish to save them now?", "Are you sure?", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1)
                Case DialogResult.Yes
                    CommitChanges()

                Case DialogResult.No
                    ' don't bother

                Case DialogResult.Cancel
                    e.Cancel = True ' cancel the close event
            End Select
        End If
    End Sub

    ''' <summary>
    ''' The main table is being changed
    ''' </summary>
    Protected Overridable Sub RecordTable_ColumnChanging(ByVal sender As Object, ByVal e As DataColumnChangeEventArgs) Handles RecordTable.ColumnChanging
    End Sub

    Protected Overridable Sub RecordTable_RowChanging(ByVal sender As Object, ByVal e As DataRowChangeEventArgs) Handles RecordTable.RowChanging
    End Sub

    Protected Overridable Sub RecordTable_RowDeleting(ByVal sender As Object, ByVal e As DataRowChangeEventArgs) Handles RecordTable.RowDeleting
    End Sub
End Class

