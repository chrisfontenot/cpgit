﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using DebtPlus.Svc.OCS;
using System.IO;
using DebtPlus.OCS.Domain;

namespace DebtPlus.UI.Housing.OCS
{
    internal partial class LoadResultsReport : DebtPlus.Reports.Template.Forms.DateReportParametersForm
    {
        internal LoadResultsReport() : base(Utils.DateRange.Today)
        {
            InitializeComponent();
            RegisterHandlers();
        }

        protected override bool HasErrors()
        {
            // If there is an error in the base form then there is an error.
            if (base.HasErrors())
            {
                return true;
            }

            // The lookup control can not be null.
            return lookUpEdit1.EditValue == null;
        }

        private void RegisterHandlers()
        {
            this.Load += OCSLoadResults_Load;
            lookUpEdit1.EditValueChanged += lookUpEdit1_EditValueChanged;
        }

        private void UnRegisterHandlers()
        {
            this.Load -= OCSLoadResults_Load;
            lookUpEdit1.EditValueChanged -= lookUpEdit1_EditValueChanged;
        }

        private void lookUpEdit1_EditValueChanged(object sender, EventArgs e)
        {
            ButtonOK.Enabled = !HasErrors();
        }

        private void OCSLoadResults_Load(object sender, EventArgs e)
        {
            UnRegisterHandlers();
            try
            {
                // Load the list into the program types
                lookUpEdit1.Properties.DataSource = DebtPlus.OCS.InMemory.OCSProgramTypes.getList();
                lookUpEdit1.EditValue = DebtPlus.OCS.Domain.PROGRAM.FreddieMacEI;
                ButtonOK.Enabled = !HasErrors();
            }
            finally
            {
                RegisterHandlers();
            }
        }

        protected override void ButtonOK_Click(object sender, EventArgs e)
        {
            try
            {
                TopMost = false;
                Application.DoEvents();
                using (var reporter = new Reporter(new OCSDbMapper(new LINQ.BusinessContext())))
                {
                    reporter.GetLoadResultsExcel(Parameter_FromDate.Date, Parameter_ToDate.Date,(PROGRAM)lookUpEdit1.EditValue);
                }
                DebtPlus.Data.Forms.MessageBox.Show(Properties.Resources.ExcelSheetCreated, Properties.Resources.OperationCompleted);
                base.ButtonOK_Click(sender, e);
            }
            catch (Exception ex)
            {
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, Properties.Resources.ErrorCreatingExcel);
            }
        }
    }

    public class OCSLoadResults : DebtPlus.Interfaces.Desktop.IDesktopMainline
    {
        public void OldAppMain(string[] Arguments)
        {
            var thrd = new System.Threading.Thread(new System.Threading.ParameterizedThreadStart(Mainline_Thread))
                {
                    Name = "OCSLoadResults",
                    IsBackground = true
                };
            thrd.SetApartmentState(System.Threading.ApartmentState.STA);
            thrd.Start(Arguments);
        }

        private void Mainline_Thread(object obj)
        {
            using (var frm = new LoadResultsReport())
            {
                frm.ShowDialog();
            }
        }
    }
}
