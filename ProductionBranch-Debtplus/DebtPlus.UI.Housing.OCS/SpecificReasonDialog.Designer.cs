﻿namespace DebtPlus.UI.Housing.OCS
{
    partial class SpecificReasonDialog
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.promptVerbiage = new DevExpress.XtraEditors.LabelControl();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.specificReason = new DevExpress.XtraEditors.ComboBoxEdit();
            this.okBtn = new DevExpress.XtraEditors.SimpleButton();
            this.cancelBtn = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)(this.specificReason.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // promptVerbiage
            // 
            this.promptVerbiage.Location = new System.Drawing.Point(46, 29);
            this.promptVerbiage.Name = "promptVerbiage";
            this.promptVerbiage.Size = new System.Drawing.Size(193, 13);
            this.promptVerbiage.TabIndex = 0;
            this.promptVerbiage.Text = "Some verbiage to prompt the user here:";
            // 
            // labelControl2
            // 
            this.labelControl2.Location = new System.Drawing.Point(20, 78);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(40, 13);
            this.labelControl2.TabIndex = 1;
            this.labelControl2.Text = "Reason:";
            // 
            // specificReason
            // 
            this.specificReason.Location = new System.Drawing.Point(66, 75);
            this.specificReason.Name = "specificReason";
            this.specificReason.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.specificReason.Properties.Items.AddRange(new object[] {
            "Bankruptcy",
            "Loan Current",
            "Client unable to Verify ClearPoint/FMAC",
            "Consult with Spouse",
            "Divorce",
            "Deceased",
            "Indisposed/Medically/Legally",
            "Loan Mod",
            "Submitted Docs",
            "Repayment Plan",
            "Denied Help by Servicer",
            "Foreclosure",
            "Selling Home",
            "Short Sale",
            "Home Auctioned",
            "Working with a Third Party",
            "Client Disconnnected/Hung Up"});
            this.specificReason.Size = new System.Drawing.Size(214, 20);
            this.specificReason.TabIndex = 2;
            this.specificReason.SelectedIndexChanged += new System.EventHandler(this.specificReason_SelectedIndexChanged);
            // 
            // okBtn
            // 
            this.okBtn.Enabled = false;
            this.okBtn.Location = new System.Drawing.Point(46, 131);
            this.okBtn.Name = "okBtn";
            this.okBtn.Size = new System.Drawing.Size(75, 23);
            this.okBtn.TabIndex = 3;
            this.okBtn.Text = "OK";
            this.okBtn.Click += new System.EventHandler(this.okBtn_Click);
            // 
            // cancelBtn
            // 
            this.cancelBtn.Location = new System.Drawing.Point(174, 131);
            this.cancelBtn.Name = "cancelBtn";
            this.cancelBtn.Size = new System.Drawing.Size(75, 23);
            this.cancelBtn.TabIndex = 4;
            this.cancelBtn.Text = "Cancel";
            this.cancelBtn.Click += new System.EventHandler(this.cancelBtn_Click);
            // 
            // SpecificReasonDialog
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(313, 176);
            this.Controls.Add(this.cancelBtn);
            this.Controls.Add(this.okBtn);
            this.Controls.Add(this.specificReason);
            this.Controls.Add(this.labelControl2);
            this.Controls.Add(this.promptVerbiage);
            this.Name = "SpecificReasonDialog";
            this.Text = "Some Reason Specific Title";
            ((System.ComponentModel.ISupportInitialize)(this.specificReason.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.LabelControl promptVerbiage;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.ComboBoxEdit specificReason;
        private DevExpress.XtraEditors.SimpleButton okBtn;
        private DevExpress.XtraEditors.SimpleButton cancelBtn;
    }
}