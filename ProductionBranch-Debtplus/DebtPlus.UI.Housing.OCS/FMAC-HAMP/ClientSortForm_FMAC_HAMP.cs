﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DebtPlus.UI.Housing.OCS.FMAC_HAMP
{
    public class ClientSortForm_FMAC_HAMP : DebtPlus.UI.Housing.OCS.ClientSortLauncher
    {
        public ClientSortForm_FMAC_HAMP()
            : base()
        {
            program = DebtPlus.OCS.Domain.PROGRAM.FreddieMacHamp;
        }
    }
}
