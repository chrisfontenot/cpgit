﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DebtPlus.OCS.Domain;

namespace DebtPlus.UI.Housing.OCS.FMAC_HAMP
{
    public class LoadRecords_FMAC_HAMP : DebtPlus.UI.Housing.OCS.LoadRecords
    {
        public LoadRecords_FMAC_HAMP()
            : base()
        {
            program = PROGRAM.FreddieMacHamp;
            fieldValidationFunctions = FMACExcelMap.fieldValidationFuncs;
            fieldConvertActions = FMACExcelMap.fieldConvertActions;
        }
    }
}
