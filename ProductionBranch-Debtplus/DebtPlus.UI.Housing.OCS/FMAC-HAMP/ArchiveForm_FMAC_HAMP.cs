﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DebtPlus.UI.Housing.OCS.FMAC_HAMP
{
    public class ArchiveForm_FMAC_HAMP : DebtPlus.UI.Housing.OCS.ArchiveForm
    {
        public ArchiveForm_FMAC_HAMP()
            : base()
        {
            program = DebtPlus.OCS.Domain.PROGRAM.FreddieMacHamp;
        }

        private void InitializeComponent()
        {
            ((System.ComponentModel.ISupportInitialize)(this.DxErrorProvider1)).BeginInit();
            this.SuspendLayout();
            // 
            // ArchiveForm_FMAC_HAMP
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.ClientSize = new System.Drawing.Size(385, 286);
            this.Name = "ArchiveForm_FMAC_HAMP";
            this.Text = "Archive - Freddie Mac - EI";
            ((System.ComponentModel.ISupportInitialize)(this.DxErrorProvider1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }
    }
}
