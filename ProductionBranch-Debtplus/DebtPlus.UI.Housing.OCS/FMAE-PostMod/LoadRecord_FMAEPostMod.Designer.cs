﻿namespace DebtPlus.UI.Housing.OCS
{
    partial class LoadRecord_FMAEPostMod
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.uploadFileButton = new DevExpress.XtraEditors.SimpleButton();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.backgroundWorker1 = new System.ComponentModel.BackgroundWorker();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.fileName = new DevExpress.XtraEditors.TextEdit();
            this.textBatchName = new DevExpress.XtraEditors.TextEdit();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.checkAllRecords = new DevExpress.XtraEditors.CheckEdit();
            this.labelControl6 = new DevExpress.XtraEditors.LabelControl();
            this.checkDuplicateOnly = new DevExpress.XtraEditors.CheckEdit();
            this.okButton = new DevExpress.XtraEditors.SimpleButton();
            this.cancelButton = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)(this.DxErrorProvider1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fileName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBatchName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkAllRecords.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkDuplicateOnly.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // uploadFileButton
            // 
            this.uploadFileButton.Location = new System.Drawing.Point(174, 11);
            this.uploadFileButton.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.uploadFileButton.Name = "uploadFileButton";
            this.uploadFileButton.Size = new System.Drawing.Size(55, 25);
            this.uploadFileButton.TabIndex = 0;
            this.uploadFileButton.Text = "Browse";
            this.uploadFileButton.Click += new System.EventHandler(this.uploadFileButton_Click);
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.DefaultExt = "xlsx";
            this.openFileDialog1.Filter = "Excel Files|*.xlsx";
            // 
            // backgroundWorker1
            // 
            this.backgroundWorker1.DoWork += new System.ComponentModel.DoWorkEventHandler(this.backgroundWorker1_DoWork);
            this.backgroundWorker1.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.backgroundWorker1_RunWorkerCompleted);
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(17, 15);
            this.labelControl1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(119, 16);
            this.labelControl1.TabIndex = 1;
            this.labelControl1.Text = "Document To Upload";
            // 
            // fileName
            // 
            this.fileName.Enabled = false;
            this.fileName.Location = new System.Drawing.Point(234, 11);
            this.fileName.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.fileName.Name = "fileName";
            this.fileName.Size = new System.Drawing.Size(276, 22);
            this.fileName.TabIndex = 2;
            // 
            // textBatchName
            // 
            this.textBatchName.Location = new System.Drawing.Point(234, 43);
            this.textBatchName.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.textBatchName.Name = "textBatchName";
            this.textBatchName.Size = new System.Drawing.Size(276, 22);
            this.textBatchName.TabIndex = 3;
            this.textBatchName.TextChanged += new System.EventHandler(this.textBatchName_TextChanged);
            // 
            // labelControl2
            // 
            this.labelControl2.Location = new System.Drawing.Point(17, 47);
            this.labelControl2.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(92, 16);
            this.labelControl2.TabIndex = 4;
            this.labelControl2.Text = "Batch Name/No.";
            // 
            // labelControl4
            // 
            this.labelControl4.Location = new System.Drawing.Point(15, 84);
            this.labelControl4.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(131, 16);
            this.labelControl4.TabIndex = 7;
            this.labelControl4.Text = "Reminder calls to MTI?";
            // 
            // labelControl5
            // 
            this.labelControl5.Location = new System.Drawing.Point(15, 119);
            this.labelControl5.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl5.Name = "labelControl5";
            this.labelControl5.Size = new System.Drawing.Size(64, 16);
            this.labelControl5.TabIndex = 9;
            this.labelControl5.Text = "All Records";
            // 
            // checkAllRecords
            // 
            this.checkAllRecords.Enabled = false;
            this.checkAllRecords.Location = new System.Drawing.Point(128, 119);
            this.checkAllRecords.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.checkAllRecords.Name = "checkAllRecords";
            this.checkAllRecords.Properties.Caption = "";
            this.checkAllRecords.Size = new System.Drawing.Size(22, 21);
            this.checkAllRecords.TabIndex = 8;
            // 
            // labelControl6
            // 
            this.labelControl6.Location = new System.Drawing.Point(15, 154);
            this.labelControl6.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl6.Name = "labelControl6";
            this.labelControl6.Size = new System.Drawing.Size(81, 16);
            this.labelControl6.TabIndex = 11;
            this.labelControl6.Text = "Duplicate Only";
            // 
            // checkDuplicateOnly
            // 
            this.checkDuplicateOnly.Enabled = false;
            this.checkDuplicateOnly.Location = new System.Drawing.Point(128, 150);
            this.checkDuplicateOnly.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.checkDuplicateOnly.Name = "checkDuplicateOnly";
            this.checkDuplicateOnly.Properties.Caption = "";
            this.checkDuplicateOnly.Size = new System.Drawing.Size(22, 21);
            this.checkDuplicateOnly.TabIndex = 10;
            // 
            // okButton
            // 
            this.okButton.Enabled = false;
            this.okButton.Location = new System.Drawing.Point(149, 196);
            this.okButton.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.okButton.Name = "okButton";
            this.okButton.Size = new System.Drawing.Size(87, 28);
            this.okButton.TabIndex = 12;
            this.okButton.Text = "OK";
            this.okButton.Click += new System.EventHandler(this.okButton_Click);
            // 
            // cancelButton
            // 
            this.cancelButton.Location = new System.Drawing.Point(285, 196);
            this.cancelButton.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.cancelButton.Name = "cancelButton";
            this.cancelButton.Size = new System.Drawing.Size(87, 28);
            this.cancelButton.TabIndex = 13;
            this.cancelButton.Text = "Cancel";
            this.cancelButton.Click += new System.EventHandler(this.cancelButton_Click);
            // 
            // LoadRecord_FMAEPostMod
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(525, 236);
            this.Controls.Add(this.cancelButton);
            this.Controls.Add(this.okButton);
            this.Controls.Add(this.labelControl6);
            this.Controls.Add(this.checkDuplicateOnly);
            this.Controls.Add(this.labelControl5);
            this.Controls.Add(this.checkAllRecords);
            this.Controls.Add(this.labelControl4);
            this.Controls.Add(this.labelControl2);
            this.Controls.Add(this.textBatchName);
            this.Controls.Add(this.fileName);
            this.Controls.Add(this.labelControl1);
            this.Controls.Add(this.uploadFileButton);
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.MaximumSize = new System.Drawing.Size(468, 289);
            this.MinimumSize = new System.Drawing.Size(466, 200);
            this.Name = "LoadRecord_FMAEPostMod";
            this.Text = "OCS - Load - Fannie Mae - POSTMOD";
            this.Load += new System.EventHandler(this.LoadRecords_Load);
            ((System.ComponentModel.ISupportInitialize)(this.DxErrorProvider1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fileName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBatchName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkAllRecords.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkDuplicateOnly.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.SimpleButton uploadFileButton;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.ComponentModel.BackgroundWorker backgroundWorker1;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.TextEdit fileName;
        private DevExpress.XtraEditors.TextEdit textBatchName;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraEditors.LabelControl labelControl5;
        private DevExpress.XtraEditors.CheckEdit checkAllRecords;
        private DevExpress.XtraEditors.LabelControl labelControl6;
        private DevExpress.XtraEditors.CheckEdit checkDuplicateOnly;
        private DevExpress.XtraEditors.SimpleButton okButton;
        private DevExpress.XtraEditors.SimpleButton cancelButton;
    }
}