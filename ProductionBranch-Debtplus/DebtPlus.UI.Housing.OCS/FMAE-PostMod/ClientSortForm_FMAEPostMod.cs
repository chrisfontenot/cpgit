﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DebtPlus.Interfaces.Desktop;
using DebtPlus.UI.Housing.OCS;

namespace DebtPlus.UI.Housing.OCS.FMAE_PostMod
{
    public class ClientSortForm_FMAEPostMod : ClientSortLauncher
    {
        public ClientSortForm_FMAEPostMod() {
            program = DebtPlus.OCS.Domain.PROGRAM.FannieMaePostMod;
        }
    }
}
