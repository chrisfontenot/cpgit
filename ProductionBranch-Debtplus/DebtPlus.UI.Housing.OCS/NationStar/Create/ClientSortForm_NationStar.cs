﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DebtPlus.Interfaces.Desktop;
using DebtPlus.UI.Housing.OCS;

namespace DebtPlus.UI.Housing.OCS.NationStar
{
    public class ClientSortForm_NationStar : ClientSortLauncher
    {
        public ClientSortForm_NationStar() {
            program = DebtPlus.OCS.Domain.PROGRAM.NationStarSD1308;
        }
    }
}
