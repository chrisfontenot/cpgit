﻿namespace DebtPlus.UI.Housing.OCS.NationStar.Create
{
    partial class NewNationStarClient
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            try
            {
                if (disposing)
                {
                    if (components != null) components.Dispose();
                    if (bc != null) bc.Dispose();
                }
                components = null;
                bc = null;
            }
            finally
            {
                base.Dispose(disposing);
            }
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(NewNationStarClient));
            this.lblTitle = new DevExpress.XtraEditors.LabelControl();
            this.btnCancel = new DevExpress.XtraEditors.SimpleButton();
            this.btnCreateClient = new DevExpress.XtraEditors.SimpleButton();
            this.luReferral = new DevExpress.XtraEditors.LookUpEdit();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl6 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl7 = new DevExpress.XtraEditors.LabelControl();
            this.txtNationStarLoanNumber = new DevExpress.XtraEditors.TextEdit();
            this.luTrialModification = new DevExpress.XtraEditors.LookUpEdit();
            this.nameRecordControl_Applicant = new DebtPlus.Data.Controls.NameRecordControl();
            this.nameRecordControl_Coapplicant = new DebtPlus.Data.Controls.NameRecordControl();
            this.addressRecordControl = new DebtPlus.Data.Controls.AddressRecordControl();
            this.telephoneNumberRecordControl_Home = new DebtPlus.Data.Controls.TelephoneNumberRecordControl();
            this.telephoneNumberRecordControl_AppCell = new DebtPlus.Data.Controls.TelephoneNumberRecordControl();
            this.telephoneNumberRecordControl_CoappCell = new DebtPlus.Data.Controls.TelephoneNumberRecordControl();
            this.telephoneNumberRecordControl_Message = new DebtPlus.Data.Controls.TelephoneNumberRecordControl();
            this.telephoneNumberRecordControl_AppWork = new DebtPlus.Data.Controls.TelephoneNumberRecordControl();
            this.telephoneNumberRecordControl_CoappWork = new DebtPlus.Data.Controls.TelephoneNumberRecordControl();
            this.labelControl8 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl9 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl10 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl11 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl12 = new DevExpress.XtraEditors.LabelControl();
            this.barManager1 = new DevExpress.XtraBars.BarManager(this.components);
            this.bar2 = new DevExpress.XtraBars.Bar();
            this.barSubItem_File = new DevExpress.XtraBars.BarSubItem();
            this.exitFileItem = new DevExpress.XtraBars.BarButtonItem();
            this.barSubItem_Appointments = new DevExpress.XtraBars.BarSubItem();
            this.newAppointmentsItem = new DevExpress.XtraBars.BarButtonItem();
            this.listAppointmentsItem = new DevExpress.XtraBars.BarButtonItem();
            this.cancelAppointmentsItem = new DevExpress.XtraBars.BarButtonItem();
            this.confirmAppointmentsItem = new DevExpress.XtraBars.BarButtonItem();
            this.rescheduleAppointmentsItem = new DevExpress.XtraBars.BarButtonItem();
            this.barSubItem_Reports = new DevExpress.XtraBars.BarSubItem();
            this.barSubItem_Help = new DevExpress.XtraBars.BarSubItem();
            this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
            this.barSubItem1 = new DevExpress.XtraBars.BarSubItem();
            this.barStaticItem1 = new DevExpress.XtraBars.BarStaticItem();
            this.barSubItem2 = new DevExpress.XtraBars.BarSubItem();
            this.barSubItem3 = new DevExpress.XtraBars.BarSubItem();
            this.barSubItem4 = new DevExpress.XtraBars.BarSubItem();
            this.barStaticItem2 = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItem3 = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItem4 = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItem5 = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItem6 = new DevExpress.XtraBars.BarStaticItem();
            this.barSubItem5 = new DevExpress.XtraBars.BarSubItem();
            this.exitFileItem1 = new DevExpress.XtraBars.BarStaticItem();
            this.newAppointmentsItem1 = new DevExpress.XtraBars.BarStaticItem();
            this.listAppointmentsItem1 = new DevExpress.XtraBars.BarStaticItem();
            this.cancelAppointmentsItem1 = new DevExpress.XtraBars.BarStaticItem();
            this.confirmAppointmentsItem1 = new DevExpress.XtraBars.BarStaticItem();
            this.rescheduleAppointmentsItem1 = new DevExpress.XtraBars.BarStaticItem();
            ((System.ComponentModel.ISupportInitialize)(this.DxErrorProvider1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.luReferral.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNationStarLoanNumber.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.luTrialModification.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.addressRecordControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.telephoneNumberRecordControl_Home)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.telephoneNumberRecordControl_AppCell)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.telephoneNumberRecordControl_CoappCell)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.telephoneNumberRecordControl_Message)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.telephoneNumberRecordControl_AppWork)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.telephoneNumberRecordControl_CoappWork)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            this.SuspendLayout();
            // 
            // lblTitle
            // 
            this.lblTitle.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblTitle.Appearance.Font = new System.Drawing.Font("Comic Sans MS", 14F, System.Drawing.FontStyle.Bold);
            this.lblTitle.Appearance.ForeColor = System.Drawing.Color.RoyalBlue;
            this.lblTitle.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lblTitle.Location = new System.Drawing.Point(260, 29);
            this.lblTitle.Name = "lblTitle";
            this.lblTitle.Size = new System.Drawing.Size(396, 27);
            this.lblTitle.TabIndex = 1;
            this.lblTitle.Text = "NationStar - SD1308 - Create new client";
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(515, 418);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 14;
            this.btnCancel.Text = "Cancel";
            // 
            // btnCreateClient
            // 
            this.btnCreateClient.Location = new System.Drawing.Point(342, 418);
            this.btnCreateClient.Name = "btnCreateClient";
            this.btnCreateClient.Size = new System.Drawing.Size(75, 23);
            this.btnCreateClient.TabIndex = 13;
            this.btnCreateClient.Text = "Create Client";
            // 
            // luReferral
            // 
            this.luReferral.Location = new System.Drawing.Point(142, 79);
            this.luReferral.Name = "luReferral";
            this.luReferral.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.luReferral.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Id", "Id", 20, DevExpress.Utils.FormatType.None, "", false, DevExpress.Utils.HorzAlignment.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("description", "description")});
            this.luReferral.Properties.DisplayMember = "description";
            this.luReferral.Properties.DropDownRows = 2;
            this.luReferral.Properties.NullText = "";
            this.luReferral.Properties.ShowFooter = false;
            this.luReferral.Properties.ShowHeader = false;
            this.luReferral.Properties.ShowLines = false;
            this.luReferral.Properties.ValueMember = "Id";
            this.luReferral.Size = new System.Drawing.Size(434, 20);
            this.luReferral.TabIndex = 1;
            // 
            // labelControl3
            // 
            this.labelControl3.Location = new System.Drawing.Point(15, 83);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(39, 13);
            this.labelControl3.TabIndex = 6;
            this.labelControl3.Text = "Referral";
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(15, 151);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(80, 13);
            this.labelControl1.TabIndex = 10;
            this.labelControl1.Text = "Trial Modification";
            // 
            // labelControl2
            // 
            this.labelControl2.Location = new System.Drawing.Point(15, 117);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(88, 13);
            this.labelControl2.TabIndex = 11;
            this.labelControl2.Text = "NationStar Loan #";
            // 
            // labelControl4
            // 
            this.labelControl4.Location = new System.Drawing.Point(15, 185);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(47, 13);
            this.labelControl4.TabIndex = 12;
            this.labelControl4.Text = "Applicant ";
            // 
            // labelControl5
            // 
            this.labelControl5.Location = new System.Drawing.Point(15, 219);
            this.labelControl5.Name = "labelControl5";
            this.labelControl5.Size = new System.Drawing.Size(57, 13);
            this.labelControl5.TabIndex = 13;
            this.labelControl5.Text = "CoApplicant";
            // 
            // labelControl6
            // 
            this.labelControl6.Location = new System.Drawing.Point(15, 253);
            this.labelControl6.Name = "labelControl6";
            this.labelControl6.Size = new System.Drawing.Size(80, 13);
            this.labelControl6.TabIndex = 14;
            this.labelControl6.Text = "Home Telephone";
            // 
            // labelControl7
            // 
            this.labelControl7.Location = new System.Drawing.Point(15, 316);
            this.labelControl7.Name = "labelControl7";
            this.labelControl7.Size = new System.Drawing.Size(39, 13);
            this.labelControl7.TabIndex = 15;
            this.labelControl7.Text = "Address";
            // 
            // txtNationStarLoanNumber
            // 
            this.txtNationStarLoanNumber.Location = new System.Drawing.Point(142, 113);
            this.txtNationStarLoanNumber.Name = "txtNationStarLoanNumber";
            this.txtNationStarLoanNumber.Size = new System.Drawing.Size(122, 20);
            this.txtNationStarLoanNumber.TabIndex = 2;
            // 
            // luTrialModification
            // 
            this.luTrialModification.Location = new System.Drawing.Point(142, 147);
            this.luTrialModification.Name = "luTrialModification";
            this.luTrialModification.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.luTrialModification.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("description", "description"),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Id", "Id", 20, DevExpress.Utils.FormatType.None, "", false, DevExpress.Utils.HorzAlignment.Default)});
            this.luTrialModification.Properties.DisplayMember = "description";
            this.luTrialModification.Properties.DropDownRows = 4;
            this.luTrialModification.Properties.NullText = "";
            this.luTrialModification.Properties.ShowFooter = false;
            this.luTrialModification.Properties.ShowHeader = false;
            this.luTrialModification.Properties.ValueMember = "Id";
            this.luTrialModification.Size = new System.Drawing.Size(434, 20);
            this.luTrialModification.TabIndex = 3;
            // 
            // nameRecordControl_Applicant
            // 
            this.nameRecordControl_Applicant.Location = new System.Drawing.Point(142, 181);
            this.nameRecordControl_Applicant.Name = "nameRecordControl_Applicant";
            this.nameRecordControl_Applicant.Size = new System.Drawing.Size(773, 20);
            this.nameRecordControl_Applicant.TabIndex = 4;
            // 
            // nameRecordControl_Coapplicant
            // 
            this.nameRecordControl_Coapplicant.Location = new System.Drawing.Point(142, 215);
            this.nameRecordControl_Coapplicant.Name = "nameRecordControl_Coapplicant";
            this.nameRecordControl_Coapplicant.Size = new System.Drawing.Size(773, 20);
            this.nameRecordControl_Coapplicant.TabIndex = 5;
            // 
            // addressRecordControl
            // 
            this.addressRecordControl.Location = new System.Drawing.Point(142, 317);
            this.addressRecordControl.Name = "addressRecordControl";
            this.addressRecordControl.Size = new System.Drawing.Size(773, 74);
            this.addressRecordControl.TabIndex = 12;
            // 
            // telephoneNumberRecordControl_Home
            // 
            this.telephoneNumberRecordControl_Home.ErrorIcon = null;
            this.telephoneNumberRecordControl_Home.ErrorText = "";
            this.telephoneNumberRecordControl_Home.Location = new System.Drawing.Point(142, 249);
            this.telephoneNumberRecordControl_Home.Name = "telephoneNumberRecordControl_Home";
            this.telephoneNumberRecordControl_Home.Size = new System.Drawing.Size(150, 20);
            this.telephoneNumberRecordControl_Home.TabIndex = 6;
            // 
            // telephoneNumberRecordControl_AppCell
            // 
            this.telephoneNumberRecordControl_AppCell.ErrorIcon = null;
            this.telephoneNumberRecordControl_AppCell.ErrorText = "";
            this.telephoneNumberRecordControl_AppCell.Location = new System.Drawing.Point(453, 249);
            this.telephoneNumberRecordControl_AppCell.Name = "telephoneNumberRecordControl_AppCell";
            this.telephoneNumberRecordControl_AppCell.Size = new System.Drawing.Size(150, 20);
            this.telephoneNumberRecordControl_AppCell.TabIndex = 7;
            // 
            // telephoneNumberRecordControl_CoappCell
            // 
            this.telephoneNumberRecordControl_CoappCell.ErrorIcon = null;
            this.telephoneNumberRecordControl_CoappCell.ErrorText = "";
            this.telephoneNumberRecordControl_CoappCell.Location = new System.Drawing.Point(765, 249);
            this.telephoneNumberRecordControl_CoappCell.Name = "telephoneNumberRecordControl_CoappCell";
            this.telephoneNumberRecordControl_CoappCell.Size = new System.Drawing.Size(150, 20);
            this.telephoneNumberRecordControl_CoappCell.TabIndex = 8;
            // 
            // telephoneNumberRecordControl_Message
            // 
            this.telephoneNumberRecordControl_Message.ErrorIcon = null;
            this.telephoneNumberRecordControl_Message.ErrorText = "";
            this.telephoneNumberRecordControl_Message.Location = new System.Drawing.Point(142, 283);
            this.telephoneNumberRecordControl_Message.Name = "telephoneNumberRecordControl_Message";
            this.telephoneNumberRecordControl_Message.Size = new System.Drawing.Size(150, 20);
            this.telephoneNumberRecordControl_Message.TabIndex = 9;
            // 
            // telephoneNumberRecordControl_AppWork
            // 
            this.telephoneNumberRecordControl_AppWork.ErrorIcon = null;
            this.telephoneNumberRecordControl_AppWork.ErrorText = "";
            this.telephoneNumberRecordControl_AppWork.Location = new System.Drawing.Point(453, 283);
            this.telephoneNumberRecordControl_AppWork.Name = "telephoneNumberRecordControl_AppWork";
            this.telephoneNumberRecordControl_AppWork.Size = new System.Drawing.Size(150, 20);
            this.telephoneNumberRecordControl_AppWork.TabIndex = 10;
            // 
            // telephoneNumberRecordControl_CoappWork
            // 
            this.telephoneNumberRecordControl_CoappWork.ErrorIcon = null;
            this.telephoneNumberRecordControl_CoappWork.ErrorText = "";
            this.telephoneNumberRecordControl_CoappWork.Location = new System.Drawing.Point(765, 283);
            this.telephoneNumberRecordControl_CoappWork.Name = "telephoneNumberRecordControl_CoappWork";
            this.telephoneNumberRecordControl_CoappWork.Size = new System.Drawing.Size(150, 20);
            this.telephoneNumberRecordControl_CoappWork.TabIndex = 11;
            // 
            // labelControl8
            // 
            this.labelControl8.Location = new System.Drawing.Point(323, 253);
            this.labelControl8.Name = "labelControl8";
            this.labelControl8.Size = new System.Drawing.Size(64, 13);
            this.labelControl8.TabIndex = 28;
            this.labelControl8.Text = "Applicant Cell";
            // 
            // labelControl9
            // 
            this.labelControl9.Location = new System.Drawing.Point(634, 253);
            this.labelControl9.Name = "labelControl9";
            this.labelControl9.Size = new System.Drawing.Size(77, 13);
            this.labelControl9.TabIndex = 29;
            this.labelControl9.Text = "CoApplicant Cell";
            // 
            // labelControl10
            // 
            this.labelControl10.Location = new System.Drawing.Point(15, 287);
            this.labelControl10.Name = "labelControl10";
            this.labelControl10.Size = new System.Drawing.Size(95, 13);
            this.labelControl10.TabIndex = 30;
            this.labelControl10.Text = "Message Telephone";
            // 
            // labelControl11
            // 
            this.labelControl11.Location = new System.Drawing.Point(323, 287);
            this.labelControl11.Name = "labelControl11";
            this.labelControl11.Size = new System.Drawing.Size(105, 13);
            this.labelControl11.TabIndex = 31;
            this.labelControl11.Text = "Applicant Work Phone";
            // 
            // labelControl12
            // 
            this.labelControl12.Location = new System.Drawing.Point(634, 287);
            this.labelControl12.Name = "labelControl12";
            this.labelControl12.Size = new System.Drawing.Size(118, 13);
            this.labelControl12.TabIndex = 32;
            this.labelControl12.Text = "CoApplicant Work Phone";
            // 
            // barManager1
            // 
            this.barManager1.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar2});
            this.barManager1.DockControls.Add(this.barDockControlTop);
            this.barManager1.DockControls.Add(this.barDockControlBottom);
            this.barManager1.DockControls.Add(this.barDockControlLeft);
            this.barManager1.DockControls.Add(this.barDockControlRight);
            this.barManager1.Form = this;
            this.barManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.barSubItem1,
            this.barSubItem2,
            this.barSubItem3,
            this.barStaticItem1,
            this.barSubItem4,
            this.barSubItem5,
            this.barStaticItem2,
            this.barStaticItem3,
            this.barStaticItem4,
            this.barStaticItem5,
            this.barStaticItem6,
            this.barSubItem_File,
            this.barSubItem_Appointments,
            this.barSubItem_Reports,
            this.exitFileItem1,
            this.newAppointmentsItem1,
            this.listAppointmentsItem1,
            this.cancelAppointmentsItem1,
            this.confirmAppointmentsItem1,
            this.rescheduleAppointmentsItem1,
            this.barSubItem_Help,
            this.exitFileItem,
            this.newAppointmentsItem,
            this.listAppointmentsItem,
            this.cancelAppointmentsItem,
            this.confirmAppointmentsItem,
            this.rescheduleAppointmentsItem});
            this.barManager1.MainMenu = this.bar2;
            this.barManager1.MaxItemId = 27;
            // 
            // bar2
            // 
            this.bar2.BarName = "Main menu";
            this.bar2.DockCol = 0;
            this.bar2.DockRow = 0;
            this.bar2.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar2.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.barSubItem_File),
            new DevExpress.XtraBars.LinkPersistInfo(this.barSubItem_Appointments),
            new DevExpress.XtraBars.LinkPersistInfo(this.barSubItem_Reports),
            new DevExpress.XtraBars.LinkPersistInfo(this.barSubItem_Help)});
            this.bar2.OptionsBar.MultiLine = true;
            this.bar2.OptionsBar.UseWholeRow = true;
            this.bar2.Text = "Main menu";
            // 
            // barSubItem_File
            // 
            this.barSubItem_File.Caption = "&File";
            this.barSubItem_File.Id = 11;
            this.barSubItem_File.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.exitFileItem)});
            this.barSubItem_File.Name = "barSubItem_File";
            // 
            // exitFileItem
            // 
            this.exitFileItem.Caption = "&Exit";
            this.exitFileItem.Id = 21;
            this.exitFileItem.Name = "exitFileItem";
            this.exitFileItem.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.exitFileItem_ItemClick);
            // 
            // barSubItem_Appointments
            // 
            this.barSubItem_Appointments.Caption = "&Appointments";
            this.barSubItem_Appointments.Id = 12;
            this.barSubItem_Appointments.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.newAppointmentsItem),
            new DevExpress.XtraBars.LinkPersistInfo(this.listAppointmentsItem),
            new DevExpress.XtraBars.LinkPersistInfo(this.cancelAppointmentsItem),
            new DevExpress.XtraBars.LinkPersistInfo(this.confirmAppointmentsItem),
            new DevExpress.XtraBars.LinkPersistInfo(this.rescheduleAppointmentsItem)});
            this.barSubItem_Appointments.Name = "barSubItem_Appointments";
            // 
            // newAppointmentsItem
            // 
            this.newAppointmentsItem.Caption = "&New";
            this.newAppointmentsItem.Id = 22;
            this.newAppointmentsItem.Name = "newAppointmentsItem";
            this.newAppointmentsItem.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.newAppointmentsItem_ItemClick);
            // 
            // listAppointmentsItem
            // 
            this.listAppointmentsItem.Caption = "&List";
            this.listAppointmentsItem.Id = 23;
            this.listAppointmentsItem.Name = "listAppointmentsItem";
            this.listAppointmentsItem.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.listAppointmentsItem_ItemClick);
            // 
            // cancelAppointmentsItem
            // 
            this.cancelAppointmentsItem.Caption = "&Cancel";
            this.cancelAppointmentsItem.Id = 24;
            this.cancelAppointmentsItem.Name = "cancelAppointmentsItem";
            this.cancelAppointmentsItem.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.cancelAppointmentsItem_ItemClick);
            // 
            // confirmAppointmentsItem
            // 
            this.confirmAppointmentsItem.Caption = "C&onfirm";
            this.confirmAppointmentsItem.Id = 25;
            this.confirmAppointmentsItem.Name = "confirmAppointmentsItem";
            this.confirmAppointmentsItem.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.confirmAppointmentsItem_ItemClick);
            // 
            // rescheduleAppointmentsItem
            // 
            this.rescheduleAppointmentsItem.Caption = "R&eschedule";
            this.rescheduleAppointmentsItem.Id = 26;
            this.rescheduleAppointmentsItem.Name = "rescheduleAppointmentsItem";
            this.rescheduleAppointmentsItem.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.rescheduleAppointmentsItem_ItemClick);
            // 
            // barSubItem_Reports
            // 
            this.barSubItem_Reports.Caption = "&Reports";
            this.barSubItem_Reports.Id = 13;
            this.barSubItem_Reports.Name = "barSubItem_Reports";
            // 
            // barSubItem_Help
            // 
            this.barSubItem_Help.Caption = "&Help";
            this.barSubItem_Help.Id = 20;
            this.barSubItem_Help.Name = "barSubItem_Help";
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.CausesValidation = false;
            this.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControlTop.Location = new System.Drawing.Point(0, 0);
            this.barDockControlTop.Size = new System.Drawing.Size(927, 22);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.CausesValidation = false;
            this.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 453);
            this.barDockControlBottom.Size = new System.Drawing.Size(927, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.CausesValidation = false;
            this.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 22);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 431);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.CausesValidation = false;
            this.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControlRight.Location = new System.Drawing.Point(927, 22);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 431);
            // 
            // barSubItem1
            // 
            this.barSubItem1.Caption = "&File";
            this.barSubItem1.Id = 0;
            this.barSubItem1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItem1)});
            this.barSubItem1.Name = "barSubItem1";
            // 
            // barStaticItem1
            // 
            this.barStaticItem1.Caption = "Exit";
            this.barStaticItem1.Id = 3;
            this.barStaticItem1.Name = "barStaticItem1";
            this.barStaticItem1.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // barSubItem2
            // 
            this.barSubItem2.Caption = "Exit";
            this.barSubItem2.Id = 1;
            this.barSubItem2.Name = "barSubItem2";
            // 
            // barSubItem3
            // 
            this.barSubItem3.Caption = "Exit";
            this.barSubItem3.Id = 2;
            this.barSubItem3.Name = "barSubItem3";
            // 
            // barSubItem4
            // 
            this.barSubItem4.Caption = "&Appointments";
            this.barSubItem4.Id = 4;
            this.barSubItem4.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItem2),
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItem3),
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItem4),
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItem5),
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItem6)});
            this.barSubItem4.Name = "barSubItem4";
            // 
            // barStaticItem2
            // 
            this.barStaticItem2.Caption = "New";
            this.barStaticItem2.Id = 6;
            this.barStaticItem2.Name = "barStaticItem2";
            this.barStaticItem2.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // barStaticItem3
            // 
            this.barStaticItem3.Caption = "List";
            this.barStaticItem3.Id = 7;
            this.barStaticItem3.Name = "barStaticItem3";
            this.barStaticItem3.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // barStaticItem4
            // 
            this.barStaticItem4.Caption = "Cancel";
            this.barStaticItem4.Id = 8;
            this.barStaticItem4.Name = "barStaticItem4";
            this.barStaticItem4.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // barStaticItem5
            // 
            this.barStaticItem5.Caption = "Confirm";
            this.barStaticItem5.Id = 9;
            this.barStaticItem5.Name = "barStaticItem5";
            this.barStaticItem5.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // barStaticItem6
            // 
            this.barStaticItem6.Caption = "Reschedule";
            this.barStaticItem6.Id = 10;
            this.barStaticItem6.Name = "barStaticItem6";
            this.barStaticItem6.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // barSubItem5
            // 
            this.barSubItem5.Caption = "Help";
            this.barSubItem5.Id = 5;
            this.barSubItem5.Name = "barSubItem5";
            // 
            // exitFileItem1
            // 
            this.exitFileItem1.Caption = "Exit";
            this.exitFileItem1.Id = 14;
            this.exitFileItem1.Name = "exitFileItem1";
            this.exitFileItem1.TextAlignment = System.Drawing.StringAlignment.Near;
            this.exitFileItem1.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.exitFileItem_ItemClick);
            // 
            // newAppointmentsItem1
            // 
            this.newAppointmentsItem1.Caption = "New";
            this.newAppointmentsItem1.Id = 15;
            this.newAppointmentsItem1.Name = "newAppointmentsItem1";
            this.newAppointmentsItem1.TextAlignment = System.Drawing.StringAlignment.Near;
            this.newAppointmentsItem1.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.newAppointmentsItem_ItemClick);
            // 
            // listAppointmentsItem1
            // 
            this.listAppointmentsItem1.Caption = "List";
            this.listAppointmentsItem1.Id = 16;
            this.listAppointmentsItem1.Name = "listAppointmentsItem1";
            this.listAppointmentsItem1.TextAlignment = System.Drawing.StringAlignment.Near;
            this.listAppointmentsItem1.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.listAppointmentsItem_ItemClick);
            // 
            // cancelAppointmentsItem1
            // 
            this.cancelAppointmentsItem1.Caption = "Cancel";
            this.cancelAppointmentsItem1.Id = 17;
            this.cancelAppointmentsItem1.Name = "cancelAppointmentsItem1";
            this.cancelAppointmentsItem1.TextAlignment = System.Drawing.StringAlignment.Near;
            this.cancelAppointmentsItem1.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.cancelAppointmentsItem_ItemClick);
            // 
            // confirmAppointmentsItem1
            // 
            this.confirmAppointmentsItem1.Caption = "Confirm";
            this.confirmAppointmentsItem1.Id = 18;
            this.confirmAppointmentsItem1.Name = "confirmAppointmentsItem1";
            this.confirmAppointmentsItem1.TextAlignment = System.Drawing.StringAlignment.Near;
            this.confirmAppointmentsItem1.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.confirmAppointmentsItem_ItemClick);
            // 
            // rescheduleAppointmentsItem1
            // 
            this.rescheduleAppointmentsItem1.Caption = "Reschedule";
            this.rescheduleAppointmentsItem1.Id = 19;
            this.rescheduleAppointmentsItem1.Name = "rescheduleAppointmentsItem1";
            this.rescheduleAppointmentsItem1.TextAlignment = System.Drawing.StringAlignment.Near;
            this.rescheduleAppointmentsItem1.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.rescheduleAppointmentsItem_ItemClick);
            // 
            // NewNationStarClient
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(927, 453);
            this.Controls.Add(this.labelControl12);
            this.Controls.Add(this.labelControl11);
            this.Controls.Add(this.labelControl10);
            this.Controls.Add(this.labelControl9);
            this.Controls.Add(this.labelControl8);
            this.Controls.Add(this.telephoneNumberRecordControl_CoappWork);
            this.Controls.Add(this.telephoneNumberRecordControl_AppWork);
            this.Controls.Add(this.telephoneNumberRecordControl_Message);
            this.Controls.Add(this.telephoneNumberRecordControl_CoappCell);
            this.Controls.Add(this.telephoneNumberRecordControl_AppCell);
            this.Controls.Add(this.telephoneNumberRecordControl_Home);
            this.Controls.Add(this.addressRecordControl);
            this.Controls.Add(this.nameRecordControl_Coapplicant);
            this.Controls.Add(this.nameRecordControl_Applicant);
            this.Controls.Add(this.luTrialModification);
            this.Controls.Add(this.labelControl7);
            this.Controls.Add(this.labelControl6);
            this.Controls.Add(this.labelControl5);
            this.Controls.Add(this.labelControl4);
            this.Controls.Add(this.labelControl2);
            this.Controls.Add(this.labelControl1);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnCreateClient);
            this.Controls.Add(this.luReferral);
            this.Controls.Add(this.labelControl3);
            this.Controls.Add(this.lblTitle);
            this.Controls.Add(this.txtNationStarLoanNumber);
            this.Controls.Add(this.barDockControlLeft);
            this.Controls.Add(this.barDockControlRight);
            this.Controls.Add(this.barDockControlBottom);
            this.Controls.Add(this.barDockControlTop);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "NewNationStarClient";
            this.Text = "NationStar - SD1308 - OCS";
            ((System.ComponentModel.ISupportInitialize)(this.DxErrorProvider1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.luReferral.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNationStarLoanNumber.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.luTrialModification.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.addressRecordControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.telephoneNumberRecordControl_Home)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.telephoneNumberRecordControl_AppCell)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.telephoneNumberRecordControl_CoappCell)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.telephoneNumberRecordControl_Message)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.telephoneNumberRecordControl_AppWork)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.telephoneNumberRecordControl_CoappWork)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.LabelControl lblTitle;
        private DevExpress.XtraEditors.SimpleButton btnCancel;
        private DevExpress.XtraEditors.SimpleButton btnCreateClient;
        private DevExpress.XtraEditors.LookUpEdit luReferral;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraEditors.LabelControl labelControl5;
        private DevExpress.XtraEditors.LabelControl labelControl6;
        private DevExpress.XtraEditors.LabelControl labelControl7;
        private DevExpress.XtraEditors.TextEdit txtNationStarLoanNumber;
        private DevExpress.XtraEditors.LookUpEdit luTrialModification;
        private Data.Controls.NameRecordControl nameRecordControl_Applicant;
        private Data.Controls.NameRecordControl nameRecordControl_Coapplicant;
        private Data.Controls.AddressRecordControl addressRecordControl;
        private Data.Controls.TelephoneNumberRecordControl telephoneNumberRecordControl_Home;
        private Data.Controls.TelephoneNumberRecordControl telephoneNumberRecordControl_AppCell;
        private Data.Controls.TelephoneNumberRecordControl telephoneNumberRecordControl_CoappCell;
        private Data.Controls.TelephoneNumberRecordControl telephoneNumberRecordControl_Message;
        private Data.Controls.TelephoneNumberRecordControl telephoneNumberRecordControl_AppWork;
        private Data.Controls.TelephoneNumberRecordControl telephoneNumberRecordControl_CoappWork;
        private DevExpress.XtraEditors.LabelControl labelControl8;
        private DevExpress.XtraEditors.LabelControl labelControl9;
        private DevExpress.XtraEditors.LabelControl labelControl10;
        private DevExpress.XtraEditors.LabelControl labelControl11;
        private DevExpress.XtraEditors.LabelControl labelControl12;
        private DevExpress.XtraBars.BarManager barManager1;
        private DevExpress.XtraBars.Bar bar2;
        private DevExpress.XtraBars.BarDockControl barDockControlTop;
        private DevExpress.XtraBars.BarDockControl barDockControlBottom;
        private DevExpress.XtraBars.BarDockControl barDockControlLeft;
        private DevExpress.XtraBars.BarDockControl barDockControlRight;
        private DevExpress.XtraBars.BarSubItem barSubItem1;
        private DevExpress.XtraBars.BarSubItem barSubItem3;
        private DevExpress.XtraBars.BarSubItem barSubItem2;
        private DevExpress.XtraBars.BarStaticItem barStaticItem1;
        private DevExpress.XtraBars.BarSubItem barSubItem4;
        private DevExpress.XtraBars.BarSubItem barSubItem5;
        private DevExpress.XtraBars.BarStaticItem barStaticItem2;
        private DevExpress.XtraBars.BarStaticItem barStaticItem3;
        private DevExpress.XtraBars.BarStaticItem barStaticItem4;
        private DevExpress.XtraBars.BarStaticItem barStaticItem5;
        private DevExpress.XtraBars.BarStaticItem barStaticItem6;
        private DevExpress.XtraBars.BarSubItem barSubItem_File;
        private DevExpress.XtraBars.BarSubItem barSubItem_Appointments;
        private DevExpress.XtraBars.BarStaticItem exitFileItem1;
        private DevExpress.XtraBars.BarStaticItem newAppointmentsItem1;
        private DevExpress.XtraBars.BarStaticItem listAppointmentsItem1;
        private DevExpress.XtraBars.BarStaticItem cancelAppointmentsItem1;
        private DevExpress.XtraBars.BarStaticItem confirmAppointmentsItem1;
        private DevExpress.XtraBars.BarStaticItem rescheduleAppointmentsItem1;
        private DevExpress.XtraBars.BarSubItem barSubItem_Reports;
        private DevExpress.XtraBars.BarSubItem barSubItem_Help;
        private DevExpress.XtraBars.BarButtonItem exitFileItem;
        private DevExpress.XtraBars.BarButtonItem newAppointmentsItem;
        private DevExpress.XtraBars.BarButtonItem listAppointmentsItem;
        private DevExpress.XtraBars.BarButtonItem cancelAppointmentsItem;
        private DevExpress.XtraBars.BarButtonItem confirmAppointmentsItem;
        private DevExpress.XtraBars.BarButtonItem rescheduleAppointmentsItem;
    }
}