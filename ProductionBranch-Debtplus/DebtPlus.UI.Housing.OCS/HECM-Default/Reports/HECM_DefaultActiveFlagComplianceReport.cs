﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using DebtPlus.Svc.OCS;

namespace DebtPlus.UI.Housing.OCS.HECM_Default.Reports
{
    internal partial class HECM_DefaultActiveFlagComplianceReport : DebtPlus.Reports.Template.Forms.DateReportParametersForm
    {
        internal HECM_DefaultActiveFlagComplianceReport()
            : base(Utils.DateRange.Today)
        {
            InitializeComponent();
        }

        protected override void ButtonOK_Click(object sender, EventArgs e)
        {
            try
            {
                TopMost = false;
                Application.DoEvents();
                using (var reporter = new Reporter(new OCSDbMapper(new LINQ.BusinessContext())))
                {
                    //reporter.GetArchivedReportExcel(Parameter_FromDate.Date, Parameter_ToDate.Date);
                    reporter.GetNationStarActiveFlagComplianceReportExcel(Parameter_FromDate.Date, Parameter_ToDate.Date);
                }
                DebtPlus.Data.Forms.MessageBox.Show(Properties.Resources.ExcelSheetCreated, Properties.Resources.OperationCompleted);
                base.ButtonOK_Click(sender, e);
            }
            catch (Exception ex)
            {
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, Properties.Resources.ErrorCreatingExcel);
            }
        }
    }

    public class HECM_Default_ActiveFlagComplianceReport : DebtPlus.Interfaces.Desktop.IDesktopMainline
    {
        public void OldAppMain(string[] Arguments)
        {
            var thrd = new System.Threading.Thread(new System.Threading.ParameterizedThreadStart(Mainline_Thread))
            {
                Name = "HECM_DefaultActiveFlagComplianceReport",
                IsBackground = true
            };
            thrd.SetApartmentState(System.Threading.ApartmentState.STA);
            thrd.Start(Arguments);
        }

        private static void Mainline_Thread(object obj)
        {
            using (var frm = new HECM_DefaultActiveFlagComplianceReport())
            {
                frm.ShowDialog();
            }
        }
    }
}
