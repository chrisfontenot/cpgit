﻿namespace DebtPlus.UI.Housing.OCS
{
    partial class FileListingReport
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            DebtPlus.Data.Controls.ComboboxItem comboboxItem1 = new DebtPlus.Data.Controls.ComboboxItem();
            this.comboServicers = new DevExpress.XtraEditors.ComboBoxEdit();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            ((System.ComponentModel.ISupportInitialize)(this.XrGroup_param_08_1)).BeginInit();
            this.XrGroup_param_08_1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.XrCombo_param_08_1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.XrDate_param_08_2.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.XrDate_param_08_2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.XrDate_param_08_1.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.XrDate_param_08_1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DxErrorProvider1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboServicers.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // XrCombo_param_08_1
            // 
            comboboxItem1.tag = null;
            comboboxItem1.value = DebtPlus.Utils.DateRange.Today;
            this.XrCombo_param_08_1.EditValue = comboboxItem1;
            // 
            // XrDate_param_08_2
            // 
            this.XrDate_param_08_2.EditValue = new System.DateTime(2015, 8, 25, 0, 0, 0, 0);
            // 
            // XrDate_param_08_1
            // 
            this.XrDate_param_08_1.EditValue = new System.DateTime(2015, 8, 25, 0, 0, 0, 0);
            // 
            // comboServicers
            // 
            this.comboServicers.Location = new System.Drawing.Point(13, 146);
            this.comboServicers.Name = "comboServicers";
            this.comboServicers.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.comboServicers.Size = new System.Drawing.Size(211, 20);
            this.comboServicers.TabIndex = 82;
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(13, 126);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(36, 13);
            this.labelControl1.TabIndex = 83;
            this.labelControl1.Text = "Partner";
            // 
            // FileListingReport
            // 
            this.ClientSize = new System.Drawing.Size(336, 178);
            this.Controls.Add(this.labelControl1);
            this.Controls.Add(this.comboServicers);
            this.Name = "FileListingReport";
            this.Text = "File Listing Report";
            this.Controls.SetChildIndex(this.ButtonOK, 0);
            this.Controls.SetChildIndex(this.ButtonCancel, 0);
            this.Controls.SetChildIndex(this.XrGroup_param_08_1, 0);
            this.Controls.SetChildIndex(this.comboServicers, 0);
            this.Controls.SetChildIndex(this.labelControl1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.XrGroup_param_08_1)).EndInit();
            this.XrGroup_param_08_1.ResumeLayout(false);
            this.XrGroup_param_08_1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.XrCombo_param_08_1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.XrDate_param_08_2.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.XrDate_param_08_2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.XrDate_param_08_1.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.XrDate_param_08_1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DxErrorProvider1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboServicers.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }
        #endregion

        private DevExpress.XtraEditors.ComboBoxEdit comboServicers;
        private DevExpress.XtraEditors.LabelControl labelControl1;
    }
}