﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using DebtPlus.Svc.OCS;

namespace DebtPlus.UI.Housing.OCS
{
    internal partial class FileListingReport : DebtPlus.Reports.Template.Forms.DateReportParametersForm
    {
        internal FileListingReport()
            : base(Utils.DateRange.Today)
        {
            InitializeComponent();

            List<object> partners = new List<object>();

            using (var mapper = new OCSDbMapper(new LINQ.BusinessContext())) {
                List<string> fromDb = mapper.GetHECMPartners();
                fromDb.ForEach(partners.Add);
            }
            partners.Insert(0, "All");
            this.comboServicers.Properties.Items.AddRange(partners.ToArray());
            this.comboServicers.EditValue = "All";
        }

        protected override void ButtonOK_Click(object sender, EventArgs e)
        {
            try
            {
                TopMost = false;
                Application.DoEvents();
                using (var reporter = new Reporter(new OCSDbMapper(new LINQ.BusinessContext())))
                {
                    var servicerFilter = this.comboServicers.EditValue.ToString();
                    reporter.GetHECMFileListingReportExcel(Parameter_FromDate.Date, Parameter_ToDate.Date, servicerFilter);
                }
                DebtPlus.Data.Forms.MessageBox.Show(Properties.Resources.ExcelSheetCreated, Properties.Resources.OperationCompleted);
                base.ButtonOK_Click(sender, e);
            }
            catch (Exception ex)
            {
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, Properties.Resources.ErrorCreatingExcel);
            }
        }
    }

    public class HECMFileListingReport : DebtPlus.Interfaces.Desktop.IDesktopMainline
    {
        public void OldAppMain(string[] Arguments)
        {
            var thrd = new System.Threading.Thread(new System.Threading.ParameterizedThreadStart(Mainline_Thread))
            {
                Name = "HECMFileListingReportReport",
                IsBackground = true
            };
            thrd.SetApartmentState(System.Threading.ApartmentState.STA);
            thrd.Start(Arguments);
        }

        private static void Mainline_Thread(object obj)
        {
            using (var frm = new FileListingReport())
            {
                frm.ShowDialog();
            }
        }
    }
}
