﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DebtPlus.UI.Housing.OCS.HECM_Default.Create
{
    class SaveParams
    {
        public SaveParams() { }

        public int HecmPartner { get; set; }
        public int? referralCode { get; set; }
        public int? servicerID { get; set; }
        public string loanNumber { get; set; }
        public int? language { get; set; }
        public int? applicantName { get; set; }
        public int? coApplicantName { get; set; }
        public int? homePhone { get; set; }
        public int? applicantCell { get; set; }
        public int? coapplicantCell { get; set; }
        public int? messagePhone { get; set; }
        public int? applicantWorkPhone { get; set; }
        public int? coapplicantWorkPhone { get; set; }
        public int? address { get; set; }
        public int? methodOfFirstContact { get; set; }
        public string username { get; set; }
        public int? applicantEmail { get; set; }
        public int? coapplicantEmail { get; set; }
        public string InvestorLoanNumber { get; set; }
        public decimal? CorpAdvance { get; set; }
        public int? delinq_Type { get; set; }
        public int? SPOC_ID { get; set; }
    }
}
