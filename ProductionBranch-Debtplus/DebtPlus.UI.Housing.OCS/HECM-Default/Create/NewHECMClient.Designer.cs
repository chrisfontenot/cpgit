﻿namespace DebtPlus.UI.Housing.OCS.HECM_Default.Create
{
    partial class NewHECM_DefaultClient
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(NewHECM_DefaultClient));
            this.lblTitle = new DevExpress.XtraEditors.LabelControl();
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.lu_Referral = new DevExpress.XtraEditors.LookUpEdit();
            this.lu_Partner = new DevExpress.XtraEditors.LookUpEdit();
            this.lu_Servicer = new DevExpress.XtraEditors.LookUpEdit();
            this.nameRecordControl_SPOC = new DebtPlus.Data.Controls.NameRecordControl();
            this.lu_DelinqType = new DevExpress.XtraEditors.LookUpEdit();
            this.calcEdit_CorpAdvance = new DevExpress.XtraEditors.CalcEdit();
            this.barManager1 = new DevExpress.XtraBars.BarManager(this.components);
            this.bar2 = new DevExpress.XtraBars.Bar();
            this.barSubItem_File = new DevExpress.XtraBars.BarSubItem();
            this.exitFileItem = new DevExpress.XtraBars.BarButtonItem();
            this.barSubItem_Appointments = new DevExpress.XtraBars.BarSubItem();
            this.newAppointmentsItem = new DevExpress.XtraBars.BarButtonItem();
            this.listAppointmentsItem = new DevExpress.XtraBars.BarButtonItem();
            this.cancelAppointmentsItem = new DevExpress.XtraBars.BarButtonItem();
            this.confirmAppointmentsItem = new DevExpress.XtraBars.BarButtonItem();
            this.rescheduleAppointmentsItem = new DevExpress.XtraBars.BarButtonItem();
            this.barSubItem_Reports = new DevExpress.XtraBars.BarSubItem();
            this.barSubItem_Help = new DevExpress.XtraBars.BarSubItem();
            this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
            this.barSubItem1 = new DevExpress.XtraBars.BarSubItem();
            this.barStaticItem1 = new DevExpress.XtraBars.BarStaticItem();
            this.barSubItem2 = new DevExpress.XtraBars.BarSubItem();
            this.barSubItem3 = new DevExpress.XtraBars.BarSubItem();
            this.barSubItem4 = new DevExpress.XtraBars.BarSubItem();
            this.barStaticItem2 = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItem3 = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItem4 = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItem5 = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItem6 = new DevExpress.XtraBars.BarStaticItem();
            this.barSubItem5 = new DevExpress.XtraBars.BarSubItem();
            this.exitFileItem1 = new DevExpress.XtraBars.BarStaticItem();
            this.newAppointmentsItem1 = new DevExpress.XtraBars.BarStaticItem();
            this.listAppointmentsItem1 = new DevExpress.XtraBars.BarStaticItem();
            this.cancelAppointmentsItem1 = new DevExpress.XtraBars.BarStaticItem();
            this.confirmAppointmentsItem1 = new DevExpress.XtraBars.BarStaticItem();
            this.rescheduleAppointmentsItem1 = new DevExpress.XtraBars.BarStaticItem();
            this.addressRecordControl = new DebtPlus.Data.Controls.AddressRecordControl();
            this.btnCreateClient = new DevExpress.XtraEditors.SimpleButton();
            this.textEdit_InvestorLoan = new DevExpress.XtraEditors.TextEdit();
            this.btnCancel = new DevExpress.XtraEditors.SimpleButton();
            this.emailRecordControl_CoApplicant = new DebtPlus.Data.Controls.EmailRecordControl();
            this.emailRecordControl_Applicant = new DebtPlus.Data.Controls.EmailRecordControl();
            this.telephoneNumberRecordControl_AppWork = new DebtPlus.Data.Controls.TelephoneNumberRecordControl();
            this.telephoneNumberRecordControl_CoappWork = new DebtPlus.Data.Controls.TelephoneNumberRecordControl();
            this.telephoneNumberRecordControl_Message = new DebtPlus.Data.Controls.TelephoneNumberRecordControl();
            this.telephoneNumberRecordControl_CoappCell = new DebtPlus.Data.Controls.TelephoneNumberRecordControl();
            this.lu_Language = new DevExpress.XtraEditors.LookUpEdit();
            this.nameRecordControl_Applicant = new DebtPlus.Data.Controls.NameRecordControl();
            this.nameRecordControl_Coapplicant = new DebtPlus.Data.Controls.NameRecordControl();
            this.telephoneNumberRecordControl_AppCell = new DebtPlus.Data.Controls.TelephoneNumberRecordControl();
            this.txt_ServicerNumber = new DevExpress.XtraEditors.TextEdit();
            this.telephoneNumberRecordControl_Home = new DebtPlus.Data.Controls.TelephoneNumberRecordControl();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem6 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem7 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem8 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem9 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem10 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem11 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem12 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem13 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem14 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem15 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem2 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem17 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem19 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem20 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem21 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem22 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem23 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem3 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem4 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem5 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem6 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem_BottomLeft = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem_BottomRight = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem10 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem8 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem18 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem16 = new DevExpress.XtraLayout.LayoutControlItem();
            ((System.ComponentModel.ISupportInitialize)(this.DxErrorProvider1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lu_Referral.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lu_Partner.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lu_Servicer.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lu_DelinqType.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.calcEdit_CorpAdvance.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.addressRecordControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit_InvestorLoan.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emailRecordControl_CoApplicant.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emailRecordControl_Applicant.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.telephoneNumberRecordControl_AppWork)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.telephoneNumberRecordControl_CoappWork)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.telephoneNumberRecordControl_Message)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.telephoneNumberRecordControl_CoappCell)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lu_Language.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.telephoneNumberRecordControl_AppCell)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt_ServicerNumber.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.telephoneNumberRecordControl_Home)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem19)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem20)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem21)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem22)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem23)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem_BottomLeft)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem_BottomRight)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem16)).BeginInit();
            this.SuspendLayout();
            // 
            // DefaultLookAndFeel1
            // 
            this.DefaultLookAndFeel1.LookAndFeel.SkinName = "Caramel";
            // 
            // lblTitle
            // 
            this.lblTitle.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lblTitle.Appearance.Font = new System.Drawing.Font("Comic Sans MS", 14F, System.Drawing.FontStyle.Bold);
            this.lblTitle.Appearance.ForeColor = System.Drawing.Color.RoyalBlue;
            this.lblTitle.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lblTitle.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lblTitle.Location = new System.Drawing.Point(12, 12);
            this.lblTitle.Name = "lblTitle";
            this.lblTitle.Size = new System.Drawing.Size(904, 27);
            this.lblTitle.StyleController = this.layoutControl1;
            this.lblTitle.TabIndex = 0;
            this.lblTitle.Text = "HECM Default - Create new client";
            // 
            // layoutControl1
            // 
            this.layoutControl1.Controls.Add(this.lblTitle);
            this.layoutControl1.Controls.Add(this.lu_Referral);
            this.layoutControl1.Controls.Add(this.lu_Partner);
            this.layoutControl1.Controls.Add(this.lu_Servicer);
            this.layoutControl1.Controls.Add(this.nameRecordControl_SPOC);
            this.layoutControl1.Controls.Add(this.lu_DelinqType);
            this.layoutControl1.Controls.Add(this.calcEdit_CorpAdvance);
            this.layoutControl1.Controls.Add(this.addressRecordControl);
            this.layoutControl1.Controls.Add(this.btnCreateClient);
            this.layoutControl1.Controls.Add(this.textEdit_InvestorLoan);
            this.layoutControl1.Controls.Add(this.btnCancel);
            this.layoutControl1.Controls.Add(this.emailRecordControl_CoApplicant);
            this.layoutControl1.Controls.Add(this.emailRecordControl_Applicant);
            this.layoutControl1.Controls.Add(this.telephoneNumberRecordControl_AppWork);
            this.layoutControl1.Controls.Add(this.telephoneNumberRecordControl_CoappWork);
            this.layoutControl1.Controls.Add(this.telephoneNumberRecordControl_Message);
            this.layoutControl1.Controls.Add(this.telephoneNumberRecordControl_CoappCell);
            this.layoutControl1.Controls.Add(this.lu_Language);
            this.layoutControl1.Controls.Add(this.nameRecordControl_Applicant);
            this.layoutControl1.Controls.Add(this.nameRecordControl_Coapplicant);
            this.layoutControl1.Controls.Add(this.telephoneNumberRecordControl_AppCell);
            this.layoutControl1.Controls.Add(this.txt_ServicerNumber);
            this.layoutControl1.Controls.Add(this.telephoneNumberRecordControl_Home);
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl1.Location = new System.Drawing.Point(0, 24);
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(96, 281, 250, 350);
            this.layoutControl1.Root = this.layoutControlGroup1;
            this.layoutControl1.Size = new System.Drawing.Size(928, 474);
            this.layoutControl1.TabIndex = 46;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // lu_Referral
            // 
            this.lu_Referral.Location = new System.Drawing.Point(133, 63);
            this.lu_Referral.Name = "lu_Referral";
            this.lu_Referral.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lu_Referral.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Id", "Id", 20, DevExpress.Utils.FormatType.None, "", false, DevExpress.Utils.HorzAlignment.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("description", "description")});
            this.lu_Referral.Properties.DisplayMember = "description";
            this.lu_Referral.Properties.DropDownRows = 10;
            this.lu_Referral.Properties.NullText = "";
            this.lu_Referral.Properties.ShowFooter = false;
            this.lu_Referral.Properties.ShowHeader = false;
            this.lu_Referral.Properties.ShowLines = false;
            this.lu_Referral.Properties.ValueMember = "Id";
            this.lu_Referral.Size = new System.Drawing.Size(329, 20);
            this.lu_Referral.StyleController = this.layoutControl1;
            this.lu_Referral.TabIndex = 2;
            // 
            // lu_Partner
            // 
            this.lu_Partner.Location = new System.Drawing.Point(133, 87);
            this.lu_Partner.Name = "lu_Partner";
            this.lu_Partner.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lu_Partner.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Id", "Id", 20, DevExpress.Utils.FormatType.None, "", false, DevExpress.Utils.HorzAlignment.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("description", "description")});
            this.lu_Partner.Properties.DisplayMember = "description";
            this.lu_Partner.Properties.DropDownRows = 10;
            this.lu_Partner.Properties.NullText = "";
            this.lu_Partner.Properties.ShowFooter = false;
            this.lu_Partner.Properties.ShowHeader = false;
            this.lu_Partner.Properties.ShowLines = false;
            this.lu_Partner.Properties.ValueMember = "Id";
            this.lu_Partner.Size = new System.Drawing.Size(329, 20);
            this.lu_Partner.StyleController = this.layoutControl1;
            this.lu_Partner.TabIndex = 4;
            // 
            // lu_Servicer
            // 
            this.lu_Servicer.Location = new System.Drawing.Point(133, 111);
            this.lu_Servicer.Name = "lu_Servicer";
            this.lu_Servicer.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lu_Servicer.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Id", "Id", 20, DevExpress.Utils.FormatType.None, "", false, DevExpress.Utils.HorzAlignment.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("description", "description")});
            this.lu_Servicer.Properties.DisplayMember = "description";
            this.lu_Servicer.Properties.DropDownRows = 10;
            this.lu_Servicer.Properties.NullText = "";
            this.lu_Servicer.Properties.ShowFooter = false;
            this.lu_Servicer.Properties.ShowHeader = false;
            this.lu_Servicer.Properties.ShowLines = false;
            this.lu_Servicer.Properties.ValueMember = "Id";
            this.lu_Servicer.Size = new System.Drawing.Size(329, 20);
            this.lu_Servicer.StyleController = this.layoutControl1;
            this.lu_Servicer.TabIndex = 6;
            // 
            // nameRecordControl_SPOC
            // 
            this.nameRecordControl_SPOC.Location = new System.Drawing.Point(133, 135);
            this.nameRecordControl_SPOC.Name = "nameRecordControl_SPOC";
            this.nameRecordControl_SPOC.Size = new System.Drawing.Size(783, 20);
            this.nameRecordControl_SPOC.TabIndex = 8;
            // 
            // lu_DelinqType
            // 
            this.lu_DelinqType.Location = new System.Drawing.Point(800, 159);
            this.lu_DelinqType.Name = "lu_DelinqType";
            this.lu_DelinqType.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lu_DelinqType.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Id", "ID", 20, DevExpress.Utils.FormatType.Numeric, "f0", false, DevExpress.Utils.HorzAlignment.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("description", "Description", 20, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Default, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.lu_DelinqType.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.lu_DelinqType.Properties.DisplayMember = "description";
            this.lu_DelinqType.Properties.NullText = "";
            this.lu_DelinqType.Properties.ShowFooter = false;
            this.lu_DelinqType.Properties.ShowHeader = false;
            this.lu_DelinqType.Properties.ValueMember = "Id";
            this.lu_DelinqType.Size = new System.Drawing.Size(116, 20);
            this.lu_DelinqType.StyleController = this.layoutControl1;
            this.lu_DelinqType.TabIndex = 14;
            // 
            // calcEdit_CorpAdvance
            // 
            this.calcEdit_CorpAdvance.Location = new System.Drawing.Point(588, 159);
            this.calcEdit_CorpAdvance.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.calcEdit_CorpAdvance.MenuManager = this.barManager1;
            this.calcEdit_CorpAdvance.Name = "calcEdit_CorpAdvance";
            this.calcEdit_CorpAdvance.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.calcEdit_CorpAdvance.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.calcEdit_CorpAdvance.Properties.Mask.BeepOnError = true;
            this.calcEdit_CorpAdvance.Properties.Mask.EditMask = "c";
            this.calcEdit_CorpAdvance.Properties.Mask.IgnoreMaskBlank = false;
            this.calcEdit_CorpAdvance.Properties.Mask.SaveLiteral = false;
            this.calcEdit_CorpAdvance.Properties.Mask.ShowPlaceHolders = false;
            this.calcEdit_CorpAdvance.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.calcEdit_CorpAdvance.Properties.NullText = "$0.00";
            this.calcEdit_CorpAdvance.Size = new System.Drawing.Size(87, 20);
            this.calcEdit_CorpAdvance.StyleController = this.layoutControl1;
            this.calcEdit_CorpAdvance.TabIndex = 12;
            // 
            // barManager1
            // 
            this.barManager1.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar2});
            this.barManager1.DockControls.Add(this.barDockControlTop);
            this.barManager1.DockControls.Add(this.barDockControlBottom);
            this.barManager1.DockControls.Add(this.barDockControlLeft);
            this.barManager1.DockControls.Add(this.barDockControlRight);
            this.barManager1.Form = this;
            this.barManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.barSubItem1,
            this.barSubItem2,
            this.barSubItem3,
            this.barStaticItem1,
            this.barSubItem4,
            this.barSubItem5,
            this.barStaticItem2,
            this.barStaticItem3,
            this.barStaticItem4,
            this.barStaticItem5,
            this.barStaticItem6,
            this.barSubItem_File,
            this.barSubItem_Appointments,
            this.barSubItem_Reports,
            this.exitFileItem1,
            this.newAppointmentsItem1,
            this.listAppointmentsItem1,
            this.cancelAppointmentsItem1,
            this.confirmAppointmentsItem1,
            this.rescheduleAppointmentsItem1,
            this.barSubItem_Help,
            this.exitFileItem,
            this.newAppointmentsItem,
            this.listAppointmentsItem,
            this.cancelAppointmentsItem,
            this.confirmAppointmentsItem,
            this.rescheduleAppointmentsItem});
            this.barManager1.MainMenu = this.bar2;
            this.barManager1.MaxItemId = 27;
            // 
            // bar2
            // 
            this.bar2.BarName = "Main menu";
            this.bar2.DockCol = 0;
            this.bar2.DockRow = 0;
            this.bar2.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar2.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.None, false, this.barSubItem_File, false),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.None, false, this.barSubItem_Appointments, false),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.None, false, this.barSubItem_Reports, false),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.None, false, this.barSubItem_Help, false)});
            this.bar2.OptionsBar.MultiLine = true;
            this.bar2.OptionsBar.UseWholeRow = true;
            this.bar2.Text = "Main menu";
            // 
            // barSubItem_File
            // 
            this.barSubItem_File.Caption = "&File";
            this.barSubItem_File.Id = 11;
            this.barSubItem_File.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.exitFileItem)});
            this.barSubItem_File.Name = "barSubItem_File";
            // 
            // exitFileItem
            // 
            this.exitFileItem.Caption = "&Exit";
            this.exitFileItem.Id = 21;
            this.exitFileItem.Name = "exitFileItem";
            this.exitFileItem.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.exitFileItem_ItemClick);
            // 
            // barSubItem_Appointments
            // 
            this.barSubItem_Appointments.Caption = "&Appointments";
            this.barSubItem_Appointments.Id = 12;
            this.barSubItem_Appointments.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.newAppointmentsItem),
            new DevExpress.XtraBars.LinkPersistInfo(this.listAppointmentsItem),
            new DevExpress.XtraBars.LinkPersistInfo(this.cancelAppointmentsItem),
            new DevExpress.XtraBars.LinkPersistInfo(this.confirmAppointmentsItem),
            new DevExpress.XtraBars.LinkPersistInfo(this.rescheduleAppointmentsItem)});
            this.barSubItem_Appointments.Name = "barSubItem_Appointments";
            // 
            // newAppointmentsItem
            // 
            this.newAppointmentsItem.Caption = "&New";
            this.newAppointmentsItem.Id = 22;
            this.newAppointmentsItem.Name = "newAppointmentsItem";
            this.newAppointmentsItem.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.newAppointmentsItem_ItemClick);
            // 
            // listAppointmentsItem
            // 
            this.listAppointmentsItem.Caption = "&List";
            this.listAppointmentsItem.Id = 23;
            this.listAppointmentsItem.Name = "listAppointmentsItem";
            this.listAppointmentsItem.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.listAppointmentsItem_ItemClick);
            // 
            // cancelAppointmentsItem
            // 
            this.cancelAppointmentsItem.Caption = "&Cancel";
            this.cancelAppointmentsItem.Id = 24;
            this.cancelAppointmentsItem.Name = "cancelAppointmentsItem";
            this.cancelAppointmentsItem.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.cancelAppointmentsItem_ItemClick);
            // 
            // confirmAppointmentsItem
            // 
            this.confirmAppointmentsItem.Caption = "C&onfirm";
            this.confirmAppointmentsItem.Id = 25;
            this.confirmAppointmentsItem.Name = "confirmAppointmentsItem";
            this.confirmAppointmentsItem.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.confirmAppointmentsItem_ItemClick);
            // 
            // rescheduleAppointmentsItem
            // 
            this.rescheduleAppointmentsItem.Caption = "R&eschedule";
            this.rescheduleAppointmentsItem.Id = 26;
            this.rescheduleAppointmentsItem.Name = "rescheduleAppointmentsItem";
            this.rescheduleAppointmentsItem.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.rescheduleAppointmentsItem_ItemClick);
            // 
            // barSubItem_Reports
            // 
            this.barSubItem_Reports.Caption = "&Reports";
            this.barSubItem_Reports.Id = 13;
            this.barSubItem_Reports.Name = "barSubItem_Reports";
            // 
            // barSubItem_Help
            // 
            this.barSubItem_Help.Caption = "&Help";
            this.barSubItem_Help.Id = 20;
            this.barSubItem_Help.Name = "barSubItem_Help";
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.CausesValidation = false;
            this.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControlTop.Location = new System.Drawing.Point(0, 0);
            this.barDockControlTop.Size = new System.Drawing.Size(928, 24);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.CausesValidation = false;
            this.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 498);
            this.barDockControlBottom.Size = new System.Drawing.Size(928, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.CausesValidation = false;
            this.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 24);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 474);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.CausesValidation = false;
            this.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControlRight.Location = new System.Drawing.Point(928, 24);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 474);
            // 
            // barSubItem1
            // 
            this.barSubItem1.Caption = "&File";
            this.barSubItem1.Id = 0;
            this.barSubItem1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItem1)});
            this.barSubItem1.Name = "barSubItem1";
            // 
            // barStaticItem1
            // 
            this.barStaticItem1.Caption = "Exit";
            this.barStaticItem1.Id = 3;
            this.barStaticItem1.Name = "barStaticItem1";
            this.barStaticItem1.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // barSubItem2
            // 
            this.barSubItem2.Caption = "Exit";
            this.barSubItem2.Id = 1;
            this.barSubItem2.Name = "barSubItem2";
            // 
            // barSubItem3
            // 
            this.barSubItem3.Caption = "Exit";
            this.barSubItem3.Id = 2;
            this.barSubItem3.Name = "barSubItem3";
            // 
            // barSubItem4
            // 
            this.barSubItem4.Caption = "&Appointments";
            this.barSubItem4.Id = 4;
            this.barSubItem4.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItem2),
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItem3),
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItem4),
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItem5),
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItem6)});
            this.barSubItem4.Name = "barSubItem4";
            // 
            // barStaticItem2
            // 
            this.barStaticItem2.Caption = "New";
            this.barStaticItem2.Id = 6;
            this.barStaticItem2.Name = "barStaticItem2";
            this.barStaticItem2.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // barStaticItem3
            // 
            this.barStaticItem3.Caption = "List";
            this.barStaticItem3.Id = 7;
            this.barStaticItem3.Name = "barStaticItem3";
            this.barStaticItem3.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // barStaticItem4
            // 
            this.barStaticItem4.Caption = "Cancel";
            this.barStaticItem4.Id = 8;
            this.barStaticItem4.Name = "barStaticItem4";
            this.barStaticItem4.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // barStaticItem5
            // 
            this.barStaticItem5.Caption = "Confirm";
            this.barStaticItem5.Id = 9;
            this.barStaticItem5.Name = "barStaticItem5";
            this.barStaticItem5.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // barStaticItem6
            // 
            this.barStaticItem6.Caption = "Reschedule";
            this.barStaticItem6.Id = 10;
            this.barStaticItem6.Name = "barStaticItem6";
            this.barStaticItem6.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // barSubItem5
            // 
            this.barSubItem5.Caption = "Help";
            this.barSubItem5.Id = 5;
            this.barSubItem5.Name = "barSubItem5";
            // 
            // exitFileItem1
            // 
            this.exitFileItem1.Caption = "Exit";
            this.exitFileItem1.Id = 14;
            this.exitFileItem1.Name = "exitFileItem1";
            this.exitFileItem1.TextAlignment = System.Drawing.StringAlignment.Near;
            this.exitFileItem1.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.exitFileItem_ItemClick);
            // 
            // newAppointmentsItem1
            // 
            this.newAppointmentsItem1.Caption = "New";
            this.newAppointmentsItem1.Id = 15;
            this.newAppointmentsItem1.Name = "newAppointmentsItem1";
            this.newAppointmentsItem1.TextAlignment = System.Drawing.StringAlignment.Near;
            this.newAppointmentsItem1.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.newAppointmentsItem_ItemClick);
            // 
            // listAppointmentsItem1
            // 
            this.listAppointmentsItem1.Caption = "List";
            this.listAppointmentsItem1.Id = 16;
            this.listAppointmentsItem1.Name = "listAppointmentsItem1";
            this.listAppointmentsItem1.TextAlignment = System.Drawing.StringAlignment.Near;
            this.listAppointmentsItem1.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.listAppointmentsItem_ItemClick);
            // 
            // cancelAppointmentsItem1
            // 
            this.cancelAppointmentsItem1.Caption = "Cancel";
            this.cancelAppointmentsItem1.Id = 17;
            this.cancelAppointmentsItem1.Name = "cancelAppointmentsItem1";
            this.cancelAppointmentsItem1.TextAlignment = System.Drawing.StringAlignment.Near;
            this.cancelAppointmentsItem1.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.cancelAppointmentsItem_ItemClick);
            // 
            // confirmAppointmentsItem1
            // 
            this.confirmAppointmentsItem1.Caption = "Confirm";
            this.confirmAppointmentsItem1.Id = 18;
            this.confirmAppointmentsItem1.Name = "confirmAppointmentsItem1";
            this.confirmAppointmentsItem1.TextAlignment = System.Drawing.StringAlignment.Near;
            this.confirmAppointmentsItem1.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.confirmAppointmentsItem_ItemClick);
            // 
            // rescheduleAppointmentsItem1
            // 
            this.rescheduleAppointmentsItem1.Caption = "Reschedule";
            this.rescheduleAppointmentsItem1.Id = 19;
            this.rescheduleAppointmentsItem1.Name = "rescheduleAppointmentsItem1";
            this.rescheduleAppointmentsItem1.TextAlignment = System.Drawing.StringAlignment.Near;
            this.rescheduleAppointmentsItem1.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.rescheduleAppointmentsItem_ItemClick);
            // 
            // addressRecordControl
            // 
            this.addressRecordControl.Location = new System.Drawing.Point(133, 351);
            this.addressRecordControl.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.addressRecordControl.Name = "addressRecordControl";
            this.addressRecordControl.Size = new System.Drawing.Size(783, 72);
            this.addressRecordControl.TabIndex = 39;
            // 
            // btnCreateClient
            // 
            this.btnCreateClient.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.btnCreateClient.Location = new System.Drawing.Point(364, 440);
            this.btnCreateClient.Name = "btnCreateClient";
            this.btnCreateClient.Size = new System.Drawing.Size(81, 22);
            this.btnCreateClient.StyleController = this.layoutControl1;
            this.btnCreateClient.TabIndex = 40;
            this.btnCreateClient.Text = "Create Client";
            // 
            // textEdit_InvestorLoan
            // 
            this.textEdit_InvestorLoan.Location = new System.Drawing.Point(133, 159);
            this.textEdit_InvestorLoan.Name = "textEdit_InvestorLoan";
            this.textEdit_InvestorLoan.Properties.MaxLength = 50;
            this.textEdit_InvestorLoan.Size = new System.Drawing.Size(330, 20);
            this.textEdit_InvestorLoan.StyleController = this.layoutControl1;
            this.textEdit_InvestorLoan.TabIndex = 10;
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.btnCancel.Location = new System.Drawing.Point(477, 440);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(87, 22);
            this.btnCancel.StyleController = this.layoutControl1;
            this.btnCancel.TabIndex = 41;
            this.btnCancel.Text = "Cancel";
            // 
            // emailRecordControl_CoApplicant
            // 
            this.emailRecordControl_CoApplicant.ClickedColor = System.Drawing.Color.Maroon;
            this.emailRecordControl_CoApplicant.Location = new System.Drawing.Point(588, 327);
            this.emailRecordControl_CoApplicant.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.emailRecordControl_CoApplicant.MenuManager = this.barManager1;
            this.emailRecordControl_CoApplicant.Name = "emailRecordControl_CoApplicant";
            this.emailRecordControl_CoApplicant.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Underline);
            this.emailRecordControl_CoApplicant.Properties.Appearance.ForeColor = System.Drawing.Color.Blue;
            this.emailRecordControl_CoApplicant.Properties.Appearance.Options.UseFont = true;
            this.emailRecordControl_CoApplicant.Properties.Appearance.Options.UseForeColor = true;
            this.emailRecordControl_CoApplicant.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.emailRecordControl_CoApplicant.Size = new System.Drawing.Size(328, 20);
            this.emailRecordControl_CoApplicant.StyleController = this.layoutControl1;
            this.emailRecordControl_CoApplicant.TabIndex = 38;
            // 
            // emailRecordControl_Applicant
            // 
            this.emailRecordControl_Applicant.ClickedColor = System.Drawing.Color.Maroon;
            this.emailRecordControl_Applicant.Location = new System.Drawing.Point(133, 327);
            this.emailRecordControl_Applicant.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.emailRecordControl_Applicant.MenuManager = this.barManager1;
            this.emailRecordControl_Applicant.Name = "emailRecordControl_Applicant";
            this.emailRecordControl_Applicant.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Underline);
            this.emailRecordControl_Applicant.Properties.Appearance.ForeColor = System.Drawing.Color.Blue;
            this.emailRecordControl_Applicant.Properties.Appearance.Options.UseFont = true;
            this.emailRecordControl_Applicant.Properties.Appearance.Options.UseForeColor = true;
            this.emailRecordControl_Applicant.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.emailRecordControl_Applicant.Size = new System.Drawing.Size(330, 20);
            this.emailRecordControl_Applicant.StyleController = this.layoutControl1;
            this.emailRecordControl_Applicant.TabIndex = 36;
            // 
            // telephoneNumberRecordControl_AppWork
            // 
            this.telephoneNumberRecordControl_AppWork.ErrorIcon = null;
            this.telephoneNumberRecordControl_AppWork.ErrorText = "";
            this.telephoneNumberRecordControl_AppWork.Location = new System.Drawing.Point(440, 303);
            this.telephoneNumberRecordControl_AppWork.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.telephoneNumberRecordControl_AppWork.Name = "telephoneNumberRecordControl_AppWork";
            this.telephoneNumberRecordControl_AppWork.Size = new System.Drawing.Size(178, 20);
            this.telephoneNumberRecordControl_AppWork.TabIndex = 32;
            // 
            // telephoneNumberRecordControl_CoappWork
            // 
            this.telephoneNumberRecordControl_CoappWork.ErrorIcon = null;
            this.telephoneNumberRecordControl_CoappWork.ErrorText = "";
            this.telephoneNumberRecordControl_CoappWork.Location = new System.Drawing.Point(743, 303);
            this.telephoneNumberRecordControl_CoappWork.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.telephoneNumberRecordControl_CoappWork.Name = "telephoneNumberRecordControl_CoappWork";
            this.telephoneNumberRecordControl_CoappWork.Size = new System.Drawing.Size(173, 20);
            this.telephoneNumberRecordControl_CoappWork.TabIndex = 34;
            // 
            // telephoneNumberRecordControl_Message
            // 
            this.telephoneNumberRecordControl_Message.ErrorIcon = null;
            this.telephoneNumberRecordControl_Message.ErrorText = "";
            this.telephoneNumberRecordControl_Message.Location = new System.Drawing.Point(133, 303);
            this.telephoneNumberRecordControl_Message.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.telephoneNumberRecordControl_Message.Name = "telephoneNumberRecordControl_Message";
            this.telephoneNumberRecordControl_Message.Size = new System.Drawing.Size(182, 20);
            this.telephoneNumberRecordControl_Message.TabIndex = 30;
            // 
            // telephoneNumberRecordControl_CoappCell
            // 
            this.telephoneNumberRecordControl_CoappCell.ErrorIcon = null;
            this.telephoneNumberRecordControl_CoappCell.ErrorText = "";
            this.telephoneNumberRecordControl_CoappCell.Location = new System.Drawing.Point(743, 279);
            this.telephoneNumberRecordControl_CoappCell.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.telephoneNumberRecordControl_CoappCell.Name = "telephoneNumberRecordControl_CoappCell";
            this.telephoneNumberRecordControl_CoappCell.Size = new System.Drawing.Size(173, 20);
            this.telephoneNumberRecordControl_CoappCell.TabIndex = 28;
            // 
            // lu_Language
            // 
            this.lu_Language.Location = new System.Drawing.Point(133, 207);
            this.lu_Language.Name = "lu_Language";
            this.lu_Language.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lu_Language.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Id", "ID", 20, DevExpress.Utils.FormatType.Numeric, "f0", false, DevExpress.Utils.HorzAlignment.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Attribute", "Description", 20, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Default, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.lu_Language.Properties.DisplayMember = "Attribute";
            this.lu_Language.Properties.NullText = "";
            this.lu_Language.Properties.ShowFooter = false;
            this.lu_Language.Properties.ShowHeader = false;
            this.lu_Language.Properties.ValueMember = "Id";
            this.lu_Language.Size = new System.Drawing.Size(330, 20);
            this.lu_Language.StyleController = this.layoutControl1;
            this.lu_Language.TabIndex = 18;
            this.lu_Language.ToolTip = "Client\'s spoken language";
            // 
            // nameRecordControl_Applicant
            // 
            this.nameRecordControl_Applicant.Location = new System.Drawing.Point(133, 231);
            this.nameRecordControl_Applicant.Name = "nameRecordControl_Applicant";
            this.nameRecordControl_Applicant.Size = new System.Drawing.Size(783, 20);
            this.nameRecordControl_Applicant.TabIndex = 20;
            // 
            // nameRecordControl_Coapplicant
            // 
            this.nameRecordControl_Coapplicant.Location = new System.Drawing.Point(133, 255);
            this.nameRecordControl_Coapplicant.Name = "nameRecordControl_Coapplicant";
            this.nameRecordControl_Coapplicant.Size = new System.Drawing.Size(783, 20);
            this.nameRecordControl_Coapplicant.TabIndex = 22;
            // 
            // telephoneNumberRecordControl_AppCell
            // 
            this.telephoneNumberRecordControl_AppCell.ErrorIcon = null;
            this.telephoneNumberRecordControl_AppCell.ErrorText = "";
            this.telephoneNumberRecordControl_AppCell.Location = new System.Drawing.Point(440, 279);
            this.telephoneNumberRecordControl_AppCell.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.telephoneNumberRecordControl_AppCell.Name = "telephoneNumberRecordControl_AppCell";
            this.telephoneNumberRecordControl_AppCell.Size = new System.Drawing.Size(178, 20);
            this.telephoneNumberRecordControl_AppCell.TabIndex = 26;
            // 
            // txt_ServicerNumber
            // 
            this.txt_ServicerNumber.Location = new System.Drawing.Point(133, 183);
            this.txt_ServicerNumber.Name = "txt_ServicerNumber";
            this.txt_ServicerNumber.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.False;
            this.txt_ServicerNumber.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_ServicerNumber.Properties.MaxLength = 50;
            this.txt_ServicerNumber.Size = new System.Drawing.Size(330, 20);
            this.txt_ServicerNumber.StyleController = this.layoutControl1;
            this.txt_ServicerNumber.TabIndex = 16;
            this.txt_ServicerNumber.ToolTip = "Client\'s loan number";
            // 
            // telephoneNumberRecordControl_Home
            // 
            this.telephoneNumberRecordControl_Home.ErrorIcon = null;
            this.telephoneNumberRecordControl_Home.ErrorText = "";
            this.telephoneNumberRecordControl_Home.Location = new System.Drawing.Point(133, 279);
            this.telephoneNumberRecordControl_Home.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.telephoneNumberRecordControl_Home.Name = "telephoneNumberRecordControl_Home";
            this.telephoneNumberRecordControl_Home.Size = new System.Drawing.Size(182, 20);
            this.telephoneNumberRecordControl_Home.TabIndex = 24;
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "layoutControlGroup1";
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem1,
            this.layoutControlItem2,
            this.layoutControlItem3,
            this.layoutControlItem4,
            this.layoutControlItem5,
            this.layoutControlItem6,
            this.layoutControlItem7,
            this.layoutControlItem8,
            this.layoutControlItem9,
            this.layoutControlItem10,
            this.layoutControlItem11,
            this.layoutControlItem12,
            this.layoutControlItem13,
            this.layoutControlItem14,
            this.layoutControlItem15,
            this.emptySpaceItem1,
            this.emptySpaceItem2,
            this.layoutControlItem17,
            this.layoutControlItem19,
            this.layoutControlItem20,
            this.layoutControlItem21,
            this.layoutControlItem22,
            this.layoutControlItem23,
            this.emptySpaceItem3,
            this.emptySpaceItem4,
            this.emptySpaceItem5,
            this.emptySpaceItem6,
            this.emptySpaceItem_BottomLeft,
            this.emptySpaceItem_BottomRight,
            this.emptySpaceItem10,
            this.emptySpaceItem8,
            this.layoutControlItem18,
            this.layoutControlItem16});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "layoutControlGroup1";
            this.layoutControlGroup1.Size = new System.Drawing.Size(928, 474);
            this.layoutControlGroup1.Text = "layoutControlGroup1";
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem1.AppearanceItemCaption.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Top;
            this.layoutControlItem1.Control = this.addressRecordControl;
            this.layoutControlItem1.CustomizationFormText = "Address";
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 339);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(908, 76);
            this.layoutControlItem1.Text = "Address";
            this.layoutControlItem1.TextSize = new System.Drawing.Size(118, 13);
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.btnCreateClient;
            this.layoutControlItem2.CustomizationFormText = "layoutControlItem2";
            this.layoutControlItem2.Location = new System.Drawing.Point(352, 428);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(85, 26);
            this.layoutControlItem2.Text = "layoutControlItem2";
            this.layoutControlItem2.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem2.TextToControlDistance = 0;
            this.layoutControlItem2.TextVisible = false;
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.Control = this.btnCancel;
            this.layoutControlItem3.CustomizationFormText = "layoutControlItem3";
            this.layoutControlItem3.Location = new System.Drawing.Point(465, 428);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(91, 26);
            this.layoutControlItem3.Text = "layoutControlItem3";
            this.layoutControlItem3.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem3.TextToControlDistance = 0;
            this.layoutControlItem3.TextVisible = false;
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.Control = this.emailRecordControl_CoApplicant;
            this.layoutControlItem4.CustomizationFormText = "CoApplicant E-mail";
            this.layoutControlItem4.Location = new System.Drawing.Point(455, 315);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.Size = new System.Drawing.Size(453, 24);
            this.layoutControlItem4.Text = "CoApplicant E-mail";
            this.layoutControlItem4.TextSize = new System.Drawing.Size(118, 13);
            // 
            // layoutControlItem5
            // 
            this.layoutControlItem5.Control = this.emailRecordControl_Applicant;
            this.layoutControlItem5.CustomizationFormText = "Applicant E-mail";
            this.layoutControlItem5.Location = new System.Drawing.Point(0, 315);
            this.layoutControlItem5.Name = "layoutControlItem5";
            this.layoutControlItem5.Size = new System.Drawing.Size(455, 24);
            this.layoutControlItem5.Text = "Applicant E-mail";
            this.layoutControlItem5.TextSize = new System.Drawing.Size(118, 13);
            // 
            // layoutControlItem6
            // 
            this.layoutControlItem6.Control = this.telephoneNumberRecordControl_AppWork;
            this.layoutControlItem6.CustomizationFormText = "Applicant Work Phone";
            this.layoutControlItem6.Location = new System.Drawing.Point(307, 291);
            this.layoutControlItem6.Name = "layoutControlItem6";
            this.layoutControlItem6.Size = new System.Drawing.Size(303, 24);
            this.layoutControlItem6.Text = "Applicant Work Phone";
            this.layoutControlItem6.TextSize = new System.Drawing.Size(118, 13);
            // 
            // layoutControlItem7
            // 
            this.layoutControlItem7.Control = this.telephoneNumberRecordControl_CoappWork;
            this.layoutControlItem7.CustomizationFormText = "CoApplicant Work Phone";
            this.layoutControlItem7.Location = new System.Drawing.Point(610, 291);
            this.layoutControlItem7.Name = "layoutControlItem7";
            this.layoutControlItem7.Size = new System.Drawing.Size(298, 24);
            this.layoutControlItem7.Text = "CoApplicant Work Phone";
            this.layoutControlItem7.TextSize = new System.Drawing.Size(118, 13);
            // 
            // layoutControlItem8
            // 
            this.layoutControlItem8.Control = this.telephoneNumberRecordControl_Message;
            this.layoutControlItem8.CustomizationFormText = "Message Telephone";
            this.layoutControlItem8.Location = new System.Drawing.Point(0, 291);
            this.layoutControlItem8.Name = "layoutControlItem8";
            this.layoutControlItem8.Size = new System.Drawing.Size(307, 24);
            this.layoutControlItem8.Text = "Message Telephone";
            this.layoutControlItem8.TextSize = new System.Drawing.Size(118, 13);
            // 
            // layoutControlItem9
            // 
            this.layoutControlItem9.Control = this.telephoneNumberRecordControl_CoappCell;
            this.layoutControlItem9.CustomizationFormText = "CoApplicant Cell";
            this.layoutControlItem9.Location = new System.Drawing.Point(610, 267);
            this.layoutControlItem9.Name = "layoutControlItem9";
            this.layoutControlItem9.Size = new System.Drawing.Size(298, 24);
            this.layoutControlItem9.Text = "CoApplicant Cell";
            this.layoutControlItem9.TextSize = new System.Drawing.Size(118, 13);
            // 
            // layoutControlItem10
            // 
            this.layoutControlItem10.Control = this.telephoneNumberRecordControl_AppCell;
            this.layoutControlItem10.CustomizationFormText = "Applicant Cell";
            this.layoutControlItem10.Location = new System.Drawing.Point(307, 267);
            this.layoutControlItem10.Name = "layoutControlItem10";
            this.layoutControlItem10.Size = new System.Drawing.Size(303, 24);
            this.layoutControlItem10.Text = "Applicant Cell";
            this.layoutControlItem10.TextSize = new System.Drawing.Size(118, 13);
            // 
            // layoutControlItem11
            // 
            this.layoutControlItem11.Control = this.telephoneNumberRecordControl_Home;
            this.layoutControlItem11.CustomizationFormText = "Home Telephone";
            this.layoutControlItem11.Location = new System.Drawing.Point(0, 267);
            this.layoutControlItem11.Name = "layoutControlItem11";
            this.layoutControlItem11.Size = new System.Drawing.Size(307, 24);
            this.layoutControlItem11.Text = "Home Telephone";
            this.layoutControlItem11.TextSize = new System.Drawing.Size(118, 13);
            // 
            // layoutControlItem12
            // 
            this.layoutControlItem12.Control = this.nameRecordControl_Coapplicant;
            this.layoutControlItem12.CustomizationFormText = "CoApplicant";
            this.layoutControlItem12.Location = new System.Drawing.Point(0, 243);
            this.layoutControlItem12.Name = "layoutControlItem12";
            this.layoutControlItem12.Size = new System.Drawing.Size(908, 24);
            this.layoutControlItem12.Text = "CoApplicant";
            this.layoutControlItem12.TextSize = new System.Drawing.Size(118, 13);
            // 
            // layoutControlItem13
            // 
            this.layoutControlItem13.Control = this.nameRecordControl_Applicant;
            this.layoutControlItem13.CustomizationFormText = "Applicant ";
            this.layoutControlItem13.Location = new System.Drawing.Point(0, 219);
            this.layoutControlItem13.Name = "layoutControlItem13";
            this.layoutControlItem13.Size = new System.Drawing.Size(908, 24);
            this.layoutControlItem13.Text = "Applicant ";
            this.layoutControlItem13.TextSize = new System.Drawing.Size(118, 13);
            // 
            // layoutControlItem14
            // 
            this.layoutControlItem14.Control = this.lu_Language;
            this.layoutControlItem14.CustomizationFormText = "Language";
            this.layoutControlItem14.Location = new System.Drawing.Point(0, 195);
            this.layoutControlItem14.Name = "layoutControlItem14";
            this.layoutControlItem14.Size = new System.Drawing.Size(455, 24);
            this.layoutControlItem14.Text = "Language";
            this.layoutControlItem14.TextSize = new System.Drawing.Size(118, 13);
            // 
            // layoutControlItem15
            // 
            this.layoutControlItem15.Control = this.txt_ServicerNumber;
            this.layoutControlItem15.CustomizationFormText = "Servicer Loan #";
            this.layoutControlItem15.Location = new System.Drawing.Point(0, 171);
            this.layoutControlItem15.Name = "layoutControlItem15";
            this.layoutControlItem15.Size = new System.Drawing.Size(455, 24);
            this.layoutControlItem15.Text = "Servicer Loan #";
            this.layoutControlItem15.TextSize = new System.Drawing.Size(118, 13);
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.CustomizationFormText = "emptySpaceItem1";
            this.emptySpaceItem1.Location = new System.Drawing.Point(455, 195);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(453, 24);
            this.emptySpaceItem1.Text = "emptySpaceItem1";
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem2
            // 
            this.emptySpaceItem2.AllowHotTrack = false;
            this.emptySpaceItem2.CustomizationFormText = "emptySpaceItem2";
            this.emptySpaceItem2.Location = new System.Drawing.Point(455, 171);
            this.emptySpaceItem2.Name = "emptySpaceItem2";
            this.emptySpaceItem2.Size = new System.Drawing.Size(453, 24);
            this.emptySpaceItem2.Text = "emptySpaceItem2";
            this.emptySpaceItem2.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem17
            // 
            this.layoutControlItem17.Control = this.textEdit_InvestorLoan;
            this.layoutControlItem17.CustomizationFormText = "Investor Loan #";
            this.layoutControlItem17.Location = new System.Drawing.Point(0, 147);
            this.layoutControlItem17.Name = "layoutControlItem17";
            this.layoutControlItem17.Size = new System.Drawing.Size(455, 24);
            this.layoutControlItem17.Text = "Investor Loan #";
            this.layoutControlItem17.TextSize = new System.Drawing.Size(118, 13);
            // 
            // layoutControlItem19
            // 
            this.layoutControlItem19.Control = this.nameRecordControl_SPOC;
            this.layoutControlItem19.CustomizationFormText = "Servicer SPOC Name";
            this.layoutControlItem19.Location = new System.Drawing.Point(0, 123);
            this.layoutControlItem19.Name = "layoutControlItem19";
            this.layoutControlItem19.Size = new System.Drawing.Size(908, 24);
            this.layoutControlItem19.Text = "Servicer SPOC Name";
            this.layoutControlItem19.TextSize = new System.Drawing.Size(118, 13);
            // 
            // layoutControlItem20
            // 
            this.layoutControlItem20.Control = this.lu_Servicer;
            this.layoutControlItem20.CustomizationFormText = "Servicer";
            this.layoutControlItem20.Location = new System.Drawing.Point(0, 99);
            this.layoutControlItem20.Name = "layoutControlItem20";
            this.layoutControlItem20.Size = new System.Drawing.Size(454, 24);
            this.layoutControlItem20.Text = "Servicer";
            this.layoutControlItem20.TextSize = new System.Drawing.Size(118, 13);
            // 
            // layoutControlItem21
            // 
            this.layoutControlItem21.Control = this.lu_Partner;
            this.layoutControlItem21.CustomizationFormText = "Partner";
            this.layoutControlItem21.Location = new System.Drawing.Point(0, 75);
            this.layoutControlItem21.Name = "layoutControlItem21";
            this.layoutControlItem21.Size = new System.Drawing.Size(454, 24);
            this.layoutControlItem21.Text = "Partner";
            this.layoutControlItem21.TextSize = new System.Drawing.Size(118, 13);
            // 
            // layoutControlItem22
            // 
            this.layoutControlItem22.Control = this.lu_Referral;
            this.layoutControlItem22.CustomizationFormText = "Referral";
            this.layoutControlItem22.Location = new System.Drawing.Point(0, 51);
            this.layoutControlItem22.Name = "layoutControlItem22";
            this.layoutControlItem22.Size = new System.Drawing.Size(454, 24);
            this.layoutControlItem22.Text = "Referral";
            this.layoutControlItem22.TextSize = new System.Drawing.Size(118, 13);
            // 
            // layoutControlItem23
            // 
            this.layoutControlItem23.Control = this.lblTitle;
            this.layoutControlItem23.CustomizationFormText = "layoutControlItem23";
            this.layoutControlItem23.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem23.Name = "layoutControlItem23";
            this.layoutControlItem23.Size = new System.Drawing.Size(908, 31);
            this.layoutControlItem23.Text = "layoutControlItem23";
            this.layoutControlItem23.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem23.TextToControlDistance = 0;
            this.layoutControlItem23.TextVisible = false;
            // 
            // emptySpaceItem3
            // 
            this.emptySpaceItem3.AllowHotTrack = false;
            this.emptySpaceItem3.CustomizationFormText = "emptySpaceItem3";
            this.emptySpaceItem3.Location = new System.Drawing.Point(0, 31);
            this.emptySpaceItem3.MaxSize = new System.Drawing.Size(0, 20);
            this.emptySpaceItem3.MinSize = new System.Drawing.Size(10, 20);
            this.emptySpaceItem3.Name = "emptySpaceItem3";
            this.emptySpaceItem3.Size = new System.Drawing.Size(908, 20);
            this.emptySpaceItem3.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem3.Text = "emptySpaceItem3";
            this.emptySpaceItem3.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem4
            // 
            this.emptySpaceItem4.AllowHotTrack = false;
            this.emptySpaceItem4.CustomizationFormText = "emptySpaceItem4";
            this.emptySpaceItem4.Location = new System.Drawing.Point(454, 51);
            this.emptySpaceItem4.Name = "emptySpaceItem4";
            this.emptySpaceItem4.Size = new System.Drawing.Size(454, 24);
            this.emptySpaceItem4.Text = "emptySpaceItem4";
            this.emptySpaceItem4.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem5
            // 
            this.emptySpaceItem5.AllowHotTrack = false;
            this.emptySpaceItem5.CustomizationFormText = "emptySpaceItem5";
            this.emptySpaceItem5.Location = new System.Drawing.Point(454, 75);
            this.emptySpaceItem5.Name = "emptySpaceItem5";
            this.emptySpaceItem5.Size = new System.Drawing.Size(454, 24);
            this.emptySpaceItem5.Text = "emptySpaceItem5";
            this.emptySpaceItem5.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem6
            // 
            this.emptySpaceItem6.AllowHotTrack = false;
            this.emptySpaceItem6.CustomizationFormText = "emptySpaceItem6";
            this.emptySpaceItem6.Location = new System.Drawing.Point(454, 99);
            this.emptySpaceItem6.Name = "emptySpaceItem6";
            this.emptySpaceItem6.Size = new System.Drawing.Size(454, 24);
            this.emptySpaceItem6.Text = "emptySpaceItem6";
            this.emptySpaceItem6.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem_BottomLeft
            // 
            this.emptySpaceItem_BottomLeft.AllowHotTrack = false;
            this.emptySpaceItem_BottomLeft.CustomizationFormText = "emptySpaceItem_BottomLeft";
            this.emptySpaceItem_BottomLeft.Location = new System.Drawing.Point(0, 428);
            this.emptySpaceItem_BottomLeft.Name = "emptySpaceItem_BottomLeft";
            this.emptySpaceItem_BottomLeft.Size = new System.Drawing.Size(352, 26);
            this.emptySpaceItem_BottomLeft.Text = "emptySpaceItem_BottomLeft";
            this.emptySpaceItem_BottomLeft.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem_BottomRight
            // 
            this.emptySpaceItem_BottomRight.AllowHotTrack = false;
            this.emptySpaceItem_BottomRight.CustomizationFormText = "emptySpaceItem_BottomRight";
            this.emptySpaceItem_BottomRight.Location = new System.Drawing.Point(556, 428);
            this.emptySpaceItem_BottomRight.Name = "emptySpaceItem_BottomRight";
            this.emptySpaceItem_BottomRight.Size = new System.Drawing.Size(352, 26);
            this.emptySpaceItem_BottomRight.Text = "emptySpaceItem_BottomRight";
            this.emptySpaceItem_BottomRight.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem10
            // 
            this.emptySpaceItem10.AllowHotTrack = false;
            this.emptySpaceItem10.CustomizationFormText = "emptySpaceItem10";
            this.emptySpaceItem10.Location = new System.Drawing.Point(0, 415);
            this.emptySpaceItem10.Name = "emptySpaceItem10";
            this.emptySpaceItem10.Size = new System.Drawing.Size(908, 13);
            this.emptySpaceItem10.Text = "emptySpaceItem10";
            this.emptySpaceItem10.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem8
            // 
            this.emptySpaceItem8.AllowHotTrack = false;
            this.emptySpaceItem8.CustomizationFormText = "emptySpaceItem8";
            this.emptySpaceItem8.Location = new System.Drawing.Point(437, 428);
            this.emptySpaceItem8.MaxSize = new System.Drawing.Size(28, 0);
            this.emptySpaceItem8.MinSize = new System.Drawing.Size(28, 10);
            this.emptySpaceItem8.Name = "emptySpaceItem8";
            this.emptySpaceItem8.Size = new System.Drawing.Size(28, 26);
            this.emptySpaceItem8.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem8.Text = "emptySpaceItem8";
            this.emptySpaceItem8.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem18
            // 
            this.layoutControlItem18.Control = this.lu_DelinqType;
            this.layoutControlItem18.CustomizationFormText = "Delinquency Type";
            this.layoutControlItem18.Location = new System.Drawing.Point(667, 147);
            this.layoutControlItem18.Name = "layoutControlItem18";
            this.layoutControlItem18.Size = new System.Drawing.Size(241, 24);
            this.layoutControlItem18.Text = "Delinquency Type";
            this.layoutControlItem18.TextSize = new System.Drawing.Size(118, 13);
            // 
            // layoutControlItem16
            // 
            this.layoutControlItem16.Control = this.calcEdit_CorpAdvance;
            this.layoutControlItem16.CustomizationFormText = "Corporate Advance";
            this.layoutControlItem16.Location = new System.Drawing.Point(455, 147);
            this.layoutControlItem16.Name = "layoutControlItem16";
            this.layoutControlItem16.Size = new System.Drawing.Size(212, 24);
            this.layoutControlItem16.Text = "Corporate Advance";
            this.layoutControlItem16.TextSize = new System.Drawing.Size(118, 13);
            // 
            // NewHECM_DefaultClient
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(928, 498);
            this.Controls.Add(this.layoutControl1);
            this.Controls.Add(this.barDockControlLeft);
            this.Controls.Add(this.barDockControlRight);
            this.Controls.Add(this.barDockControlBottom);
            this.Controls.Add(this.barDockControlTop);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "NewHECM_DefaultClient";
            this.Text = "HECM Default";
            ((System.ComponentModel.ISupportInitialize)(this.DxErrorProvider1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.lu_Referral.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lu_Partner.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lu_Servicer.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lu_DelinqType.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.calcEdit_CorpAdvance.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.addressRecordControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit_InvestorLoan.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emailRecordControl_CoApplicant.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emailRecordControl_Applicant.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.telephoneNumberRecordControl_AppWork)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.telephoneNumberRecordControl_CoappWork)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.telephoneNumberRecordControl_Message)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.telephoneNumberRecordControl_CoappCell)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lu_Language.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.telephoneNumberRecordControl_AppCell)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt_ServicerNumber.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.telephoneNumberRecordControl_Home)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem19)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem20)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem21)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem22)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem23)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem_BottomLeft)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem_BottomRight)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem16)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.LabelControl lblTitle;
        private DevExpress.XtraEditors.SimpleButton btnCancel;
        private DevExpress.XtraEditors.SimpleButton btnCreateClient;
        private DevExpress.XtraEditors.LookUpEdit lu_Referral;
        private DevExpress.XtraEditors.TextEdit txt_ServicerNumber;
        private DevExpress.XtraEditors.LookUpEdit lu_Language;
        private Data.Controls.NameRecordControl nameRecordControl_Applicant;
        private Data.Controls.NameRecordControl nameRecordControl_Coapplicant;
        private Data.Controls.AddressRecordControl addressRecordControl;
        private Data.Controls.TelephoneNumberRecordControl telephoneNumberRecordControl_Home;
        private Data.Controls.TelephoneNumberRecordControl telephoneNumberRecordControl_AppCell;
        private Data.Controls.TelephoneNumberRecordControl telephoneNumberRecordControl_CoappCell;
        private Data.Controls.TelephoneNumberRecordControl telephoneNumberRecordControl_Message;
        private Data.Controls.TelephoneNumberRecordControl telephoneNumberRecordControl_AppWork;
        private Data.Controls.TelephoneNumberRecordControl telephoneNumberRecordControl_CoappWork;
        private DevExpress.XtraBars.BarManager barManager1;
        private DevExpress.XtraBars.Bar bar2;
        private DevExpress.XtraBars.BarDockControl barDockControlTop;
        private DevExpress.XtraBars.BarDockControl barDockControlBottom;
        private DevExpress.XtraBars.BarDockControl barDockControlLeft;
        private DevExpress.XtraBars.BarDockControl barDockControlRight;
        private DevExpress.XtraBars.BarSubItem barSubItem1;
        private DevExpress.XtraBars.BarSubItem barSubItem3;
        private DevExpress.XtraBars.BarSubItem barSubItem2;
        private DevExpress.XtraBars.BarStaticItem barStaticItem1;
        private DevExpress.XtraBars.BarSubItem barSubItem4;
        private DevExpress.XtraBars.BarSubItem barSubItem5;
        private DevExpress.XtraBars.BarStaticItem barStaticItem2;
        private DevExpress.XtraBars.BarStaticItem barStaticItem3;
        private DevExpress.XtraBars.BarStaticItem barStaticItem4;
        private DevExpress.XtraBars.BarStaticItem barStaticItem5;
        private DevExpress.XtraBars.BarStaticItem barStaticItem6;
        private DevExpress.XtraBars.BarSubItem barSubItem_File;
        private DevExpress.XtraBars.BarSubItem barSubItem_Appointments;
        private DevExpress.XtraBars.BarStaticItem exitFileItem1;
        private DevExpress.XtraBars.BarStaticItem newAppointmentsItem1;
        private DevExpress.XtraBars.BarStaticItem listAppointmentsItem1;
        private DevExpress.XtraBars.BarStaticItem cancelAppointmentsItem1;
        private DevExpress.XtraBars.BarStaticItem confirmAppointmentsItem1;
        private DevExpress.XtraBars.BarStaticItem rescheduleAppointmentsItem1;
        private DevExpress.XtraBars.BarSubItem barSubItem_Reports;
        private DevExpress.XtraBars.BarSubItem barSubItem_Help;
        private DevExpress.XtraBars.BarButtonItem exitFileItem;
        private DevExpress.XtraBars.BarButtonItem newAppointmentsItem;
        private DevExpress.XtraBars.BarButtonItem listAppointmentsItem;
        private DevExpress.XtraBars.BarButtonItem cancelAppointmentsItem;
        private DevExpress.XtraBars.BarButtonItem confirmAppointmentsItem;
        private DevExpress.XtraBars.BarButtonItem rescheduleAppointmentsItem;
        private DevExpress.XtraEditors.LookUpEdit lu_Servicer;
        private Data.Controls.EmailRecordControl emailRecordControl_CoApplicant;
        private Data.Controls.EmailRecordControl emailRecordControl_Applicant;
        private DevExpress.XtraEditors.CalcEdit calcEdit_CorpAdvance;
        private DevExpress.XtraEditors.LookUpEdit lu_DelinqType;
        private DevExpress.XtraEditors.TextEdit textEdit_InvestorLoan;
        private Data.Controls.NameRecordControl nameRecordControl_SPOC;
        private DevExpress.XtraEditors.LookUpEdit lu_Partner;
        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem5;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem6;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem7;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem8;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem9;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem10;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem11;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem12;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem13;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem14;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem15;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem17;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem19;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem20;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem21;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem22;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem23;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem3;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem4;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem5;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem6;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem_BottomLeft;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem_BottomRight;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem10;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem8;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem18;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem16;
    }
}