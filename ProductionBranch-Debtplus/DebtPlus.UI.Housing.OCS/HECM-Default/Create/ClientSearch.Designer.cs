﻿namespace DebtPlus.UI.Housing.OCS.HECM_Default.Create
{
    partial class ClientSearch
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ClientSearch));
            this.lblTitle = new DevExpress.XtraEditors.LabelControl();
            this.lblInstructions = new DevExpress.XtraEditors.LabelControl();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.homePhoneControl = new DebtPlus.Data.Controls.TelephoneNumberControl();
            this.luFirstContactMethod = new DevExpress.XtraEditors.LookUpEdit();
            this.chkHousingProblems = new DevExpress.XtraEditors.CheckEdit();
            this.btnOK = new DevExpress.XtraEditors.SimpleButton();
            this.btnCancel = new DevExpress.XtraEditors.SimpleButton();
            this.chkEdit_NoPhone = new DevExpress.XtraEditors.CheckEdit();
            ((System.ComponentModel.ISupportInitialize)(this.DxErrorProvider1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.homePhoneControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.luFirstContactMethod.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkHousingProblems.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkEdit_NoPhone.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // DefaultLookAndFeel1
            // 
            this.DefaultLookAndFeel1.LookAndFeel.SkinName = "McSkin";
            // 
            // lblTitle
            // 
            this.lblTitle.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.lblTitle.Appearance.Font = new System.Drawing.Font("Comic Sans MS", 14F, System.Drawing.FontStyle.Bold);
            this.lblTitle.Appearance.ForeColor = System.Drawing.Color.RoyalBlue;
            this.lblTitle.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lblTitle.Location = new System.Drawing.Point(63, 12);
            this.lblTitle.Name = "lblTitle";
            this.lblTitle.Size = new System.Drawing.Size(325, 27);
            this.lblTitle.TabIndex = 0;
            this.lblTitle.Text = "HECM Default - Create new client";
            // 
            // lblInstructions
            // 
            this.lblInstructions.AllowHtmlString = true;
            this.lblInstructions.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.lblInstructions.Location = new System.Drawing.Point(25, 56);
            this.lblInstructions.Name = "lblInstructions";
            this.lblInstructions.Size = new System.Drawing.Size(532, 52);
            this.lblInstructions.TabIndex = 1;
            this.lblInstructions.Text = resources.GetString("lblInstructions.Text");
            // 
            // labelControl2
            // 
            this.labelControl2.Location = new System.Drawing.Point(25, 133);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(100, 13);
            this.labelControl2.TabIndex = 2;
            this.labelControl2.Text = "Home Phone Number";
            // 
            // labelControl3
            // 
            this.labelControl3.Location = new System.Drawing.Point(25, 175);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(114, 13);
            this.labelControl3.TabIndex = 1;
            this.labelControl3.Text = "Method of First Contact";
            // 
            // homePhoneControl
            // 
            this.homePhoneControl.ErrorIcon = null;
            this.homePhoneControl.ErrorText = "";
            this.homePhoneControl.Location = new System.Drawing.Point(145, 133);
            this.homePhoneControl.Name = "homePhoneControl";
            this.homePhoneControl.Size = new System.Drawing.Size(412, 20);
            this.homePhoneControl.TabIndex = 1;
            // 
            // luFirstContactMethod
            // 
            this.luFirstContactMethod.Location = new System.Drawing.Point(145, 172);
            this.luFirstContactMethod.Name = "luFirstContactMethod";
            this.luFirstContactMethod.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.luFirstContactMethod.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Id", "Id", 20, DevExpress.Utils.FormatType.None, "", false, DevExpress.Utils.HorzAlignment.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("description", "description")});
            this.luFirstContactMethod.Properties.DisplayMember = "description";
            this.luFirstContactMethod.Properties.NullText = "";
            this.luFirstContactMethod.Properties.ShowFooter = false;
            this.luFirstContactMethod.Properties.ShowHeader = false;
            this.luFirstContactMethod.Properties.ShowLines = false;
            this.luFirstContactMethod.Properties.ValueMember = "Id";
            this.luFirstContactMethod.Size = new System.Drawing.Size(412, 20);
            this.luFirstContactMethod.TabIndex = 2;
            // 
            // chkHousingProblems
            // 
            this.chkHousingProblems.EditValue = true;
            this.chkHousingProblems.Location = new System.Drawing.Point(23, 219);
            this.chkHousingProblems.Name = "chkHousingProblems";
            this.chkHousingProblems.Properties.Caption = "Started with Housing Problems";
            this.chkHousingProblems.Size = new System.Drawing.Size(186, 20);
            this.chkHousingProblems.TabIndex = 3;
            // 
            // btnOK
            // 
            this.btnOK.Location = new System.Drawing.Point(156, 276);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(75, 23);
            this.btnOK.TabIndex = 4;
            this.btnOK.Text = "OK";
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(329, 276);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 5;
            this.btnCancel.Text = "Cancel";
            // 
            // chkEdit_NoPhone
            // 
            this.chkEdit_NoPhone.Location = new System.Drawing.Point(23, 245);
            this.chkEdit_NoPhone.Name = "chkEdit_NoPhone";
            this.chkEdit_NoPhone.Properties.Caption = "There is no telephone number. Just create the client.";
            this.chkEdit_NoPhone.Size = new System.Drawing.Size(293, 20);
            this.chkEdit_NoPhone.TabIndex = 7;
            // 
            // ClientSearch
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.ClientSize = new System.Drawing.Size(585, 323);
            this.Controls.Add(this.chkEdit_NoPhone);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnOK);
            this.Controls.Add(this.chkHousingProblems);
            this.Controls.Add(this.luFirstContactMethod);
            this.Controls.Add(this.homePhoneControl);
            this.Controls.Add(this.labelControl3);
            this.Controls.Add(this.labelControl2);
            this.Controls.Add(this.lblInstructions);
            this.Controls.Add(this.lblTitle);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "ClientSearch";
            this.Text = "HECM Default - Create a new client";
            ((System.ComponentModel.ISupportInitialize)(this.DxErrorProvider1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.homePhoneControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.luFirstContactMethod.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkHousingProblems.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkEdit_NoPhone.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.LabelControl lblTitle;
        private DevExpress.XtraEditors.LabelControl lblInstructions;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private Data.Controls.TelephoneNumberControl homePhoneControl;
        private DevExpress.XtraEditors.LookUpEdit luFirstContactMethod;
        private DevExpress.XtraEditors.CheckEdit chkHousingProblems;
        private DevExpress.XtraEditors.SimpleButton btnOK;
        private DevExpress.XtraEditors.SimpleButton btnCancel;
        private DevExpress.XtraEditors.CheckEdit chkEdit_NoPhone;
    }
}