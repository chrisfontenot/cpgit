﻿using System;
using System.Linq;
using DebtPlus.LINQ;
using DebtPlus.OCS.Domain;

namespace DebtPlus.UI.Housing.OCS.HECM_Default.Create
{
    public partial class NewHECM_DefaultClient : DebtPlus.Data.Forms.DebtPlusForm
    {
        public NewHECM_DefaultClient() : base()
        {
            InitializeComponent();
        }

        BusinessContext bc = new BusinessContext();
        SearchParams searchParams = null;
        string connectionString = null;
        const string HECM_DefaultReferralPrefix = "RVMDEF";
        bool isClientCreated = false;
        string sbNoteText = "";

        public NewHECM_DefaultClient(SearchParams searchData) : this()
        {
            searchParams = searchData;

            lblTitle.Text = "HECM Default - Create New Client";

            DebtPlus.LINQ.SQLInfoClass SqlInfo = DebtPlus.LINQ.SQLInfoClass.getDefault();
            connectionString = SqlInfo.ConnectionString;

            lu_Language.EditValue = DebtPlus.LINQ.Cache.AttributeType.getDefaultLanguage();

            RegisterHandlers();
        }

        private void RegisterHandlers()
        {
            Load                                               += NewHECMDefaultClient_Load;
            lu_Referral.EditValueChanged                       += FormChanged;
            txt_ServicerNumber.TextChanged                     += FormChanged;
            lu_Servicer.EditValueChanged                       += FormChanged;
            lu_Language.EditValueChanged                       += FormChanged;
            emailRecordControl_Applicant.TextChanged           += FormChanged;
            emailRecordControl_CoApplicant.TextChanged         += FormChanged;
            textEdit_InvestorLoan.TextChanged                  += FormChanged;
            calcEdit_CorpAdvance.TextChanged                   += FormChanged;
            lu_DelinqType.EditValueChanged                     += FormChanged;
            lu_Partner.EditValueChanged                        += FormChanged;
            telephoneNumberRecordControl_Home.TextChanged      += FormChanged;
            telephoneNumberRecordControl_AppCell.TextChanged   += FormChanged;
            telephoneNumberRecordControl_AppWork.TextChanged   += FormChanged;
            telephoneNumberRecordControl_CoappCell.TextChanged += FormChanged;
            telephoneNumberRecordControl_CoappWork.TextChanged += FormChanged;
            telephoneNumberRecordControl_Message.TextChanged   += FormChanged;
            nameRecordControl_Applicant.TextChanged            += FormChanged;
            nameRecordControl_Coapplicant.TextChanged          += FormChanged;
            addressRecordControl.TextChanged                   += FormChanged;
            nameRecordControl_SPOC.TextChanged                 += FormChanged;
            nameRecordControl_SPOC.NameChanged                 += nameRecordControl_SPOC_NameChanged;
            btnCreateClient.Click                              += btnCreateClient_Click;
            btnCancel.Click                                    += btnCancel_Click;
        }

        /// <summary>
        /// Handle the change in the SPOC name
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void nameRecordControl_SPOC_NameChanged(object sender, Events.NameChangeEventArgs e)
        {
            /*System.Diagnostics.Debug.Assert(e != null);
            System.Diagnostics.Debug.Assert(e.OldName != null);
            System.Diagnostics.Debug.Assert(e.NewName != null);*/

            if (e != null && e.OldName != null && e.NewName != null)
            {
                sbNoteText = string.Format("Changed name from '{0}' to '{1}'", e.OldName.ToString(), e.NewName.ToString());
            }
        }

        /// <summary>
        /// Remove the registration for the events
        /// </summary>
        private void UnRegisterHandlers()
        {
            Load                                               -= NewHECMDefaultClient_Load;
            lu_Referral.EditValueChanged                       -= FormChanged;
            txt_ServicerNumber.TextChanged                     -= FormChanged;
            lu_Servicer.EditValueChanged                       -= FormChanged;
            lu_Language.EditValueChanged                       -= FormChanged;
            emailRecordControl_Applicant.TextChanged           -= FormChanged;
            emailRecordControl_CoApplicant.TextChanged         -= FormChanged;
            textEdit_InvestorLoan.TextChanged                  -= FormChanged;
            calcEdit_CorpAdvance.TextChanged                   -= FormChanged;
            lu_DelinqType.EditValueChanged                     -= FormChanged;
            lu_Partner.EditValueChanged                        -= FormChanged;
            telephoneNumberRecordControl_Home.TextChanged      -= FormChanged;
            telephoneNumberRecordControl_AppCell.TextChanged   -= FormChanged;
            telephoneNumberRecordControl_AppWork.TextChanged   -= FormChanged;
            telephoneNumberRecordControl_CoappCell.TextChanged -= FormChanged;
            telephoneNumberRecordControl_CoappWork.TextChanged -= FormChanged;
            telephoneNumberRecordControl_Message.TextChanged   -= FormChanged;
            nameRecordControl_Applicant.TextChanged            -= FormChanged;
            nameRecordControl_Coapplicant.TextChanged          -= FormChanged;
            nameRecordControl_SPOC.TextChanged                 -= FormChanged;
            addressRecordControl.TextChanged                   -= FormChanged;
            nameRecordControl_SPOC.NameChanged                 -= nameRecordControl_SPOC_NameChanged;
            btnCreateClient.Click                              -= btnCreateClient_Click;
            btnCancel.Click                                    -= btnCancel_Click;
        }

        private void NewHECMDefaultClient_Load(Object sender, EventArgs e)
        {
            UnRegisterHandlers(); // Prevent event tripping when we are configuring the form.

            try
            {
                // Load the comboboxes
                LoadLookup();

                if (searchParams != null && searchParams.ClientFound == true)
                {
                    lblTitle.Text = "HECM Default - Update Client";
                    LoadClientData(searchParams.ClientID);
                }
                else
                {
                    // DP-976 : Default Referral to RVMDEF
                    var defaultReferralCode = DebtPlus.LINQ.Cache.referred_by.getHECMList().Where(r => r.description == HECM_DefaultReferralPrefix).FirstOrDefault();

                    if (defaultReferralCode != null)
                    {
                        lu_Referral.EditValue = defaultReferralCode.Id;
                    }

                    telephoneNumberRecordControl_Home.SetCurrentValues(searchParams.HomePhone);
                }

                // disable the create client button to begin with
                btnCreateClient.Enabled = !HasErrors();
            }
            finally
            {
                RegisterHandlers();
            }
        }

        private void LoadClientData(int clientID)
        {
            var result = bc.xpr_hecm_get_client(clientID);

            var clients = result.ToList();

            if (clients.Count > 0)
            {
                var client = clients[0];
                isClientCreated = true;

                btnCreateClient.Text = "Update Client";

                PopulateClientDetails(client);
            }
        }

        private void PopulateClientDetails(xpr_hecm_get_clientResult client)
        {
            if (client != null)
            {
                txt_ServicerNumber.Text                          = client.LoanNumber;
                lu_Language.EditValue                            = client.Language;
                lu_Referral.EditValue                            = client.ReferralCode;
                lu_Servicer.EditValue                            = client.ServicerID;
                lu_Partner.EditValue                             = client.Program;
                textEdit_InvestorLoan.Text                       = client.InvestorAccountNumber;
                calcEdit_CorpAdvance.EditValue                   = client.Corporate_Advance;
                lu_DelinqType.EditValue                          = DebtPlus.LINQ.Cache.HECM_Lookup.getDelinqencyType(client.Ins_Delinq_State, client.Tax_Dinq_State);
                lu_Partner.EditValue                             = client.HecmPartner;
                
                nameRecordControl_Applicant.EditValue            = client.ApplicantName;
                nameRecordControl_Coapplicant.EditValue          = client.CoapplicantName;
                nameRecordControl_SPOC.EditValue                 = client.SPOC_ID;
                
                emailRecordControl_Applicant.EditValue           = client.ApplicantEmail;
                emailRecordControl_CoApplicant.EditValue         = client.CoapplicantEmail;
                
                telephoneNumberRecordControl_Home.EditValue      = client.HomePhone;
                telephoneNumberRecordControl_Message.EditValue   = client.MessagePhone;
                telephoneNumberRecordControl_AppCell.EditValue   = client.ApplicantCell;
                telephoneNumberRecordControl_AppWork.EditValue   = client.ApplicantWorkPhone;
                telephoneNumberRecordControl_CoappCell.EditValue = client.CoapplicantCell;
                telephoneNumberRecordControl_CoappWork.EditValue = client.CoapplicantWorkPhone;

                addressRecordControl.EditValue                   = client.Address;
            }
            else
            {
                telephoneNumberRecordControl_Home.SetCurrentValues(searchParams.HomePhone);
            }
        }

        private void LoadLookup()
        {
            lu_Referral.Properties.DataSource   = DebtPlus.LINQ.Cache.referred_by.getHECMList();
            lu_Servicer.Properties.DataSource   = DebtPlus.LINQ.Cache.Housing_lender_servicer.getList();
            lu_Language.Properties.DataSource   = DebtPlus.LINQ.Cache.AttributeType.getLanguageList();
            lu_DelinqType.Properties.DataSource = DebtPlus.LINQ.Cache.HECM_Lookup.getDelinqencyType();
            lu_Partner.Properties.DataSource    = DebtPlus.OCS.Domain.OCSClient.getPartner();
        }

        protected void FormChanged(object sender, EventArgs e)
        {
            bool isEnabled = !HasErrors();
            btnCreateClient.Enabled = isEnabled;
        }

        private bool HasErrors()
        {
            // A referral type is required
            if (lu_Referral.EditValue == null)
            {
                return true;
            }

            // A partner is required
            if (lu_Partner.EditValue == null)
            {
                return true;
            }

            // A name is required
            var n = nameRecordControl_Applicant.GetCurrentValues();
            if (string.IsNullOrEmpty(n.First) || string.IsNullOrEmpty(n.Last))
            {
                return true;
            }

            // Test the telephone numbers
            var homePhone    = telephoneNumberRecordControl_Home.GetCurrentValues().SearchValue;
            var messagePhone = telephoneNumberRecordControl_Message.GetCurrentValues().SearchValue;
            var appCell      = telephoneNumberRecordControl_AppCell.GetCurrentValues().SearchValue;
            var appWork      = telephoneNumberRecordControl_AppWork.GetCurrentValues().SearchValue;
            var coappCell    = telephoneNumberRecordControl_CoappCell.GetCurrentValues().SearchValue;
            var coappWork    = telephoneNumberRecordControl_CoappWork.GetCurrentValues().SearchValue;

            // One or more of the telephone numbers is required
            if (string.IsNullOrWhiteSpace(homePhone) && string.IsNullOrWhiteSpace(messagePhone) && string.IsNullOrWhiteSpace(appCell) &&
                string.IsNullOrWhiteSpace(appWork) && string.IsNullOrWhiteSpace(coappCell) && string.IsNullOrWhiteSpace(coappWork))
            {
                return true;
            }

            // A sufficient mailing address is required.
            var a = addressRecordControl.GetCurrentValues();
            if (string.IsNullOrWhiteSpace(a.house)  ||
                string.IsNullOrWhiteSpace(a.street) ||
                string.IsNullOrWhiteSpace(a.city)   ||
                string.IsNullOrWhiteSpace(a.PostalCode))
            {
                return true;
            }

            return false;
        }

        protected void btnCreateClient_Click(Object sender, EventArgs e)
        {
            try
            {
                int clientID = CreateClient();
                GenerateSystemNote(clientID, sbNoteText);
                Close();
                ShowClientForm(clientID);
            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error creating client");
            }
        }

        private int CreateClient()
        {
            // Fetch current values
            int clientID             = 0;
            var clientInfo           = new SaveParams()
            {
                HecmPartner          = (int) (lu_Partner.EditValue),
                loanNumber           = txt_ServicerNumber.Text.Trim(),
                servicerID           = DebtPlus.Utils.Nulls.v_Int32(lu_Servicer.EditValue),
                language             = DebtPlus.Utils.Nulls.v_Int32(lu_Language.EditValue),
                referralCode         = DebtPlus.Utils.Nulls.v_Int32(lu_Referral.EditValue),
                homePhone            = telephoneNumberRecordControl_Home.EditValue,
                messagePhone         = telephoneNumberRecordControl_Message.EditValue,
                applicantCell        = telephoneNumberRecordControl_AppCell.EditValue,
                applicantWorkPhone   = telephoneNumberRecordControl_AppWork.EditValue,
                coapplicantCell      = telephoneNumberRecordControl_CoappCell.EditValue,
                coapplicantWorkPhone = telephoneNumberRecordControl_CoappWork.EditValue,
                InvestorLoanNumber   = textEdit_InvestorLoan.Text.Trim(),
                CorpAdvance          = DebtPlus.Utils.Nulls.DDec(calcEdit_CorpAdvance.EditValue),
                delinq_Type          = DebtPlus.Utils.Nulls.v_Int32(lu_DelinqType.EditValue),
                applicantEmail       = emailRecordControl_Applicant.EditValue,
                coapplicantEmail     = emailRecordControl_CoApplicant.EditValue,
                address              = addressRecordControl.EditValue,
                applicantName        = nameRecordControl_Applicant.EditValue,
                coApplicantName      = nameRecordControl_Coapplicant.EditValue,
                SPOC_ID              = nameRecordControl_SPOC.EditValue
            };

            clientID = SaveForm(clientInfo);
            return clientID;
        }

        private int SaveForm(SaveParams clientSaveParam)
        {
            int clientID = 0;
            if (!isClientCreated)
            {
                var username = DebtPlus.LINQ.BusinessContext.suser_sname();

                var result = bc.xpr_hecm_create_client((int)PROGRAM.HECMDefault,
                                                        clientSaveParam.referralCode,
                                                        clientSaveParam.servicerID,
                                                        clientSaveParam.loanNumber,
                                                        clientSaveParam.language,
                                                        clientSaveParam.applicantName,
                                                        clientSaveParam.coApplicantName,
                                                        clientSaveParam.homePhone,
                                                        clientSaveParam.applicantCell,
                                                        clientSaveParam.coapplicantCell,
                                                        clientSaveParam.messagePhone,
                                                        clientSaveParam.applicantWorkPhone,
                                                        clientSaveParam.coapplicantWorkPhone,
                                                        clientSaveParam.address,
                                                        searchParams.MethodOfFirstContact,
                                                        username,
                                                        clientSaveParam.applicantEmail,
                                                        clientSaveParam.coapplicantEmail,
                                                        clientSaveParam.InvestorLoanNumber,
                                                        clientSaveParam.CorpAdvance,
                                                        clientSaveParam.delinq_Type,
                                                        clientSaveParam.SPOC_ID,
                                                        clientSaveParam.HecmPartner);
                var clients = result.ToList();

                var client = clients[0];

                if (client != null && client.ClientID != null)
                {
                    clientID = (int)client.ClientID;
                }
            }
            else
            {
                var result = bc.xpr_hecm_update_client(searchParams.ClientID,
                                                        clientSaveParam.referralCode,
                                                        clientSaveParam.servicerID,
                                                        clientSaveParam.loanNumber,
                                                        clientSaveParam.language,
                                                        clientSaveParam.applicantName,
                                                        clientSaveParam.coApplicantName,
                                                        clientSaveParam.homePhone,
                                                        clientSaveParam.applicantCell,
                                                        clientSaveParam.coapplicantCell,
                                                        clientSaveParam.messagePhone,
                                                        clientSaveParam.applicantWorkPhone,
                                                        clientSaveParam.coapplicantWorkPhone,
                                                        clientSaveParam.address,
                                                        clientSaveParam.applicantEmail,
                                                        clientSaveParam.coapplicantEmail,
                                                        clientSaveParam.InvestorLoanNumber,
                                                        clientSaveParam.CorpAdvance,
                                                        clientSaveParam.delinq_Type,
                                                        clientSaveParam.SPOC_ID,
                                                        clientSaveParam.HecmPartner);
                clientID = searchParams.ClientID;
            }

            return clientID;
        }

        protected void btnCancel_Click(Object sender, EventArgs e)
        {
            CloseForm();
        }

        private void exitFileItem_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            CloseForm();
        }

        private void ShowClientForm(int clientID)
        {
            var t = new System.Threading.Thread(() =>
            {
                using (var editClass = new DebtPlus.UI.Client.Service.ClientUpdateClass())
                {
                    editClass.ShowEditDialog(clientID, true);
                }
            })
            {
                Name = "showClient",
                IsBackground = false
            };

            t.SetApartmentState(System.Threading.ApartmentState.STA);
            t.Start();
        }

        /// <summary>
        /// Process the CLICK event on the CreateAppointment menu item
        /// </summary>
        private void newAppointmentsItem_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            var t = new System.Threading.Thread(() =>
            {
                try
                {
                    // This is a modal dialog. Do not use "show". You need to use "showDialog".
                    using (var thrd_bc = new BusinessContext())
                    {
                        using (var frm = new DebtPlus.UI.Client.forms.Appointments.Appointments_Form_Book(thrd_bc, searchParams.ClientID))
                        {
                            frm.ShowDialog(this);
                        }

                        // Ensure that there is nothing left that is "pending" before it is disposed.
                        thrd_bc.SubmitChanges();
                    }
                }

                catch (System.Exception ex)
                {
                    // Handle the error conditions
                    DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex);
                }
            })
            {
                IsBackground = true,
                Name = "CreateAppointment"
            };

            t.SetApartmentState(System.Threading.ApartmentState.STA);
            t.Start();
        }

        /// <summary>
        /// Process the CLICK event on the ListAppointments menu item
        /// </summary>
        private void listAppointmentsItem_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            var rpt                         = new DebtPlus.Reports.Client.Appointments.ClientAppointmentsReport();
            rpt.ClientID                    = searchParams.ClientID;
            rpt.Parameter_PendingOnly       = true;
            rpt.AllowParameterChangesByUser = true;
            rpt.RunReportInSeparateThread();
        }

        /// <summary>
        /// Process the CLICK event on the CancelAppointment menu item
        /// </summary>
        private void cancelAppointmentsItem_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            var t = new System.Threading.Thread(() =>
            {
                try
                {
                    using (var thrd_bc = new BusinessContext())
                    {
                        using (var frm = new DebtPlus.UI.Client.forms.Appointments.Appointments_Form_Cancel(thrd_bc, searchParams.ClientID))
                        {
                            frm.ShowDialog(this);
                        }

                        // Ensure that there is nothing left that is "pending" before it is disposed.
                        thrd_bc.SubmitChanges();
                    }
                }

                catch (System.Exception ex)
                {
                    // Handle the error conditions
                    DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex);
                }
            })
            {
                IsBackground = true,
                Name = "CancelAppointment"
            };

            t.SetApartmentState(System.Threading.ApartmentState.STA);
            t.Start();
        }

        /// <summary>
        /// Process the CLICK event on the ConfirmAppointment menu item
        /// </summary>
        private void confirmAppointmentsItem_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            var t = new System.Threading.Thread(() =>
            {
                try
                {
                    using (var thrd_bc = new BusinessContext())
                    {
                        using (var frm = new DebtPlus.UI.Client.forms.Appointments.Appointments_Form_Confirm(thrd_bc, searchParams.ClientID))
                        {
                            frm.ShowDialog(this);
                        }

                        // Ensure that there is nothing left that is "pending" before it is disposed.
                        thrd_bc.SubmitChanges();
                    }
                }

                catch (System.Exception ex)
                {
                    // Handle the error conditions
                    DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex);
                }
            })
            {
                IsBackground = true,
                Name = "ConfirmAppointment"
            };

            t.SetApartmentState(System.Threading.ApartmentState.STA);
            t.Start();
        }

        /// <summary>
        /// Process the CLICK event on the RescheduleAppointment menu item
        /// </summary>
        private void rescheduleAppointmentsItem_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            var t = new System.Threading.Thread(() =>
            {
                try
                {
                    using (var thrd_bc = new BusinessContext())
                    {
                        using (var frm = new DebtPlus.UI.Client.forms.Appointments.Appointments_Form_Reschedule(thrd_bc, searchParams.ClientID))
                        {
                            frm.ShowDialog(this);
                        }

                        // Ensure that there is nothing left that is "pending" before it is disposed.
                        thrd_bc.SubmitChanges();
                    }
                }
                catch (System.Exception ex)
                {
                    // Handle the error conditions
                    DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex);
                }
            })
            {
                IsBackground = true,
                Name = "RescheduleAppointment"
            };

            t.SetApartmentState(System.Threading.ApartmentState.STA);
            t.Start();
        }

        private void CloseForm()
        {
            Close();
        }

        private void GenerateSystemNote()
        {
            throw new NotImplementedException();
        }

        private void GenerateSystemNote(System.Int32 clientID, string sbNoteText)
        {
            string subjectText = "Changed RM SPOC Name";

            if (clientID > 0 && ! string.IsNullOrWhiteSpace(sbNoteText))
            {
                var cn     = DebtPlus.LINQ.Factory.Manufacture_client_note(clientID, 3);
                cn.subject = subjectText;
                cn.note    = sbNoteText;

                bc.client_notes.InsertOnSubmit(cn);
                bc.SubmitChanges();
            }
        }
    }
}