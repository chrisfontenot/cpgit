﻿using System;
using System.Collections.Generic;
using System.Linq;
using DebtPlus.LINQ;
using DebtPlus.Svc.OCS;

namespace DebtPlus.UI.Housing.OCS.HECM_Default.Create
{
    public partial class ClientSearch : DebtPlus.Data.Forms.DebtPlusForm
    {
        public Int32 ClientId { get; set; }

        public ClientSearch()
        {
            InitializeComponent();
            ClientId = Int32.MinValue;
            RegisterHandlers();
        }

        private void RegisterHandlers()
        {
            Load                                  += ClientSearch_Load;
            homePhoneControl.TextChanged          += Form_Changed;
            luFirstContactMethod.EditValueChanged += Form_Changed;
            chkEdit_NoPhone.CheckedChanged        += Form_Changed;
            btnOK.Click                           += btnOK_Click;
            btnCancel.Click                       += btnCancel_Click;
        }

        /// <summary>
        /// Remove the registration for the events
        /// </summary>
        private void UnRegisterHandlers()
        {
            Load                                  -= ClientSearch_Load;
            homePhoneControl.TextChanged          -= Form_Changed;
            luFirstContactMethod.EditValueChanged -= Form_Changed;
            chkEdit_NoPhone.CheckedChanged        -= Form_Changed;
            btnOK.Click                           -= btnOK_Click;
            btnCancel.Click                       -= btnCancel_Click;
        }

        protected string HomePhone
        {
            get
            {
                var values = homePhoneControl.GetCurrentValues();
                return values.SearchValue;
            }
        }

        protected string FirstContactMethod
        {
            get
            {
                string answer = string.Empty;
                if (luFirstContactMethod.EditValue != null)
                {
                    answer = luFirstContactMethod.EditValue.ToString();
                }
                return answer;
            }
        }

        private void ClientSearch_Load(Object sender, EventArgs e)
        {
            // Prevent event tripping when we are configuring the form.
            UnRegisterHandlers();

            try
            {
                // Load the comboboxes
                luFirstContactMethod.Properties.DataSource = DebtPlus.LINQ.Cache.FirstContactType.getList();
                luFirstContactMethod.EditValue = DebtPlus.LINQ.Cache.FirstContactType.getDefault();

                btnOK.Enabled = !HasErrors();
            }
            finally
            {
                RegisterHandlers();
            }
        }

        protected void Form_Changed(Object sender, EventArgs e)
        {
            btnOK.Enabled = !HasErrors();
        }

        private bool HasErrors()
        {
            // A first contact method is required
            if (!DebtPlus.Utils.Nulls.v_Int32(luFirstContactMethod.EditValue).HasValue)
            {
                return true;
            }

            // If the person said there was no phone then we don't need a phone.
            if (! chkEdit_NoPhone.Checked && string.IsNullOrEmpty(HomePhone))
            {
                return true;
            }

            // All is good.
            return false;
        }

        protected void btnOK_Click(Object sender, EventArgs e)
        {
            // Pad the telephone number on the left with zeros if needed
            string tempNumber = DebtPlus.Utils.Format.Strings.DigitsOnly(HomePhone).PadRight(10, '0');
            tempNumber = tempNumber.Substring(tempNumber.Length - 10, 10);

            // Generate the search key for the lists
            string searchKey = tempNumber;

            SearchParams param = new SearchParams()
            {
                HomePhone            = homePhoneControl.GetCurrentValues(),
                HousingProblem       = chkHousingProblems.Checked,
                MethodOfFirstContact = DebtPlus.Utils.Nulls.v_Int32(luFirstContactMethod.EditValue)
            };

            if (chkEdit_NoPhone.Checked != true)
            {
                // Retrieve the results
                List<DebtPlus.LINQ.xpr_hecm_search_clientResult> clients = null;
                using (var bc = new BusinessContext())
                {
                    clients = bc.xpr_hecm_search_client(searchKey).ToList();
                }

                if (clients != null && clients.Count > 0)
                {
                    param.ClientFound = true;
                    param.ClientID = clients[0].clientID;
                }
            }

            // Close the search form first
            Close();

            // Display the client
            var newClient = new NewHECM_DefaultClient(param);
            newClient.Show();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            /* cancel and close the form */
            Close();
        }
    }
}