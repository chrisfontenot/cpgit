﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DebtPlus.Interfaces.Desktop;
using DebtPlus.UI.Housing.OCS;

namespace DebtPlus.UI.Housing.OCS.HECM_Default
{
    public class ClientSortForm_HECM_Default : ClientSortLauncher
    {
        public ClientSortForm_HECM_Default()
        {
            program = DebtPlus.OCS.Domain.PROGRAM.HECMDefault;
        }
    }
}
