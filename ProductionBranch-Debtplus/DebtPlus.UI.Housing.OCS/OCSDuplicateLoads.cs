﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using DebtPlus.Svc.OCS;
using System.IO;
using DebtPlus.OCS.Domain;

namespace DebtPlus.UI.Housing.OCS
{
    internal partial class DuplicateLoadsReport : DebtPlus.Reports.Template.Forms.DateReportParametersForm
    {
        internal DuplicateLoadsReport() : base(Utils.DateRange.Today)
        {
            InitializeComponent();
            RegisterHandlers();
        }

        protected override bool HasErrors()
        {
            // If there is an error in the base form then there is an error.
            if (base.HasErrors())
            {
                return true;
            }

            // The lookup control can not be null.
            return lookUpEdit1.EditValue == null;
        }

        private void RegisterHandlers()
        {
            Load += DuplicateLoadsReport_Load;
            lookUpEdit1.EditValueChanged += lookUpEdit1_EditValueChanged;
        }

        private void UnRegisterHandlers()
        {
            Load -= DuplicateLoadsReport_Load;
            lookUpEdit1.EditValueChanged -= lookUpEdit1_EditValueChanged;
        }

        private void lookUpEdit1_EditValueChanged(object sender, EventArgs e)
        {
            ButtonOK.Enabled = !HasErrors();
        }

        private void DuplicateLoadsReport_Load(object sender, EventArgs e)
        {
            UnRegisterHandlers();
            try
            {
                // Load the list into the program types
                lookUpEdit1.Properties.DataSource = DebtPlus.OCS.InMemory.OCSProgramTypes.getList();
                lookUpEdit1.EditValue = DebtPlus.OCS.Domain.PROGRAM.FreddieMacEI;
                ButtonOK.Enabled = !HasErrors();
            }
            finally
            {
                RegisterHandlers();
            }
        }

        protected override void ButtonOK_Click(object sender, EventArgs e)
        {
            try
            {
                TopMost = false;
                Application.DoEvents();
                using (var reporter = new Reporter(new OCSDbMapper(new LINQ.BusinessContext())))
                {
                    reporter.GetLoadDuplicatesExcel((PROGRAM)lookUpEdit1.EditValue, Parameter_FromDate.Date, Parameter_ToDate.Date, wasUploaded.Checked);
                }
                DebtPlus.Data.Forms.MessageBox.Show(Properties.Resources.ExcelSheetCreated, Properties.Resources.OperationCompleted);
                base.ButtonOK_Click(sender, e);
            }
            catch (Exception ex)
            {
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, Properties.Resources.ErrorCreatingExcel);
            }
        }
    }

    public class OCSDuplicateLoads : DebtPlus.Interfaces.Desktop.IDesktopMainline
    {
        public void OldAppMain(string[] Arguments)
        {
            var thrd = new System.Threading.Thread(new System.Threading.ParameterizedThreadStart(Mainline_Thread))
            {
                Name = "OCSDuplicateLoads",
                IsBackground = true
            };
            thrd.SetApartmentState(System.Threading.ApartmentState.STA);
            thrd.Start(Arguments);
        }

        private static void Mainline_Thread(object obj)
        {
            using (var frm = new DuplicateLoadsReport())
            {
                frm.ShowDialog();
            }
        }
    }
}
