﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DebtPlus.Interfaces.Desktop;
using DebtPlus.UI.Housing.OCS;

namespace DebtPlus.UI.Housing.OCS.PostModPilotCounseling
{
    public class ClientSortForm_PostModPilotCounseling : ClientSortLauncher
    {
        public ClientSortForm_PostModPilotCounseling()
        {
            program = DebtPlus.OCS.Domain.PROGRAM.PostModPilotCounseling;
        }
    }
}
