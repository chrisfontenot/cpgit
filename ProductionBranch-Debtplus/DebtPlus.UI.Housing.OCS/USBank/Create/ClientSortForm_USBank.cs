﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DebtPlus.Interfaces.Desktop;
using DebtPlus.UI.Housing.OCS;

namespace DebtPlus.UI.Housing.OCS.USBank
{
    public class ClientSortForm_USBank : ClientSortLauncher
    {
        public ClientSortForm_USBank()
        {
            program = DebtPlus.OCS.Domain.PROGRAM.USBankSD1308;
        }
    }
}
