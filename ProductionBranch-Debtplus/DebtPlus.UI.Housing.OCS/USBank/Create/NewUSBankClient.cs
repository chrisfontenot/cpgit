﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using System.Linq;
using DebtPlus.LINQ;
using DebtPlus.OCS.Domain;
using DebtPlus.Svc.OCS;

namespace DebtPlus.UI.Housing.OCS.USBank.Create
{
    public partial class NewUSBankClient : DebtPlus.Data.Forms.DebtPlusForm
    {
        public NewUSBankClient()
        {
            InitializeComponent();
        }

        SearchParams searchParams = null;
        string connectionString = null;
        const string USBankReferralPrefix = "USB1308";
        bool isClientCreated = false;
        BusinessContext bc = new BusinessContext();

        public NewUSBankClient(SearchParams searchData)
        {
            searchParams = searchData;
            InitializeComponent();

            lblTitle.Text = "US Bank - SD1308 - Create New Client";

            DebtPlus.LINQ.SQLInfoClass SqlInfo = DebtPlus.LINQ.SQLInfoClass.getDefault();
            connectionString = SqlInfo.ConnectionString;

            RegisterHandlers();
        }

        private void RegisterHandlers()
        {
            this.Load += new EventHandler(NewUSBankClient_Load);

            luReferral.TextChanged += FormChanged;
            txtUSBankLoanNumber.TextChanged += FormChanged;
            luTrialModification.TextChanged += FormChanged;

            telephoneNumberRecordControl_Home.TextChanged += FormChanged;
            telephoneNumberRecordControl_AppCell.TextChanged += FormChanged;
            telephoneNumberRecordControl_AppWork.TextChanged += FormChanged;
            telephoneNumberRecordControl_CoappCell.TextChanged += FormChanged;
            telephoneNumberRecordControl_CoappWork.TextChanged += FormChanged;
            telephoneNumberRecordControl_Message.TextChanged += FormChanged;

            nameRecordControl_Applicant.TextChanged += FormChanged;
            nameRecordControl_Coapplicant.TextChanged += FormChanged;

            addressRecordControl.TextChanged += FormChanged;

            btnCreateClient.Click += new EventHandler(btnCreateClient_Click);
            btnCancel.Click += new EventHandler(btnCancel_Click);
        }

        /// <summary>
        /// Remove the registration for the events
        /// </summary>
        private void UnRegisterHandlers()
        {
            this.Load -= new EventHandler(NewUSBankClient_Load);

            luReferral.TextChanged -= FormChanged;
            txtUSBankLoanNumber.TextChanged -= FormChanged;
            luTrialModification.TextChanged -= FormChanged;

            telephoneNumberRecordControl_Home.TextChanged -= FormChanged;
            telephoneNumberRecordControl_AppCell.TextChanged -= FormChanged;
            telephoneNumberRecordControl_AppWork.TextChanged -= FormChanged;
            telephoneNumberRecordControl_CoappCell.TextChanged -= FormChanged;
            telephoneNumberRecordControl_CoappWork.TextChanged -= FormChanged;
            telephoneNumberRecordControl_Message.TextChanged -= FormChanged;

            nameRecordControl_Applicant.TextChanged -= FormChanged;
            nameRecordControl_Coapplicant.TextChanged -= FormChanged;

            addressRecordControl.TextChanged -= FormChanged;

            btnCreateClient.Click -= new EventHandler(btnCreateClient_Click);
            btnCancel.Click -= new EventHandler(btnCancel_Click);
        }

        private void NewUSBankClient_Load(Object sender, EventArgs e)
        {
            UnRegisterHandlers();                    // Prevent evnet tripping when we are configuring the form.
            try
            {
                // Load the comboboxes
                LoadLookup();
                //EnableOK();

                if (searchParams != null && searchParams.ClientFound == true)
                {
                    lblTitle.Text = "US Bank - SD1308 - Update Client";
                    LoadClientData(searchParams.ClientID);
                }
                else
                {
                    // disable the create client button to begin with
                    btnCreateClient.Enabled = false;
                    PopulateSearchPhone();
                }
            }
            finally
            {
                RegisterHandlers();
            }
        }

        private void LoadClientData(int clientID)
        {
            var result = bc.xpr_nationstar_get_client(clientID);

            var clients = result.ToList();

            if (clients.Count > 0)
            {
                var client = clients[0];
                isClientCreated = true;

                btnCreateClient.Text = "Update Client";

                PopulateClientDetails(client);
            }
        }

        private void PopulateClientDetails(xpr_nationstar_get_clientResult client)
        {
            if (client != null)
            {
                txtUSBankLoanNumber.Text = client.LoanNumber;
                luTrialModification.EditValue = client.TrialModification;
                luReferral.EditValue = client.ReferralCode;

                nameRecordControl_Applicant.EditValue = client.ApplicantName;
                nameRecordControl_Coapplicant.EditValue = client.CoapplicantName;

                telephoneNumberRecordControl_Home.EditValue = client.HomePhone;
                telephoneNumberRecordControl_Message.EditValue = client.MessagePhone;
                telephoneNumberRecordControl_AppCell.EditValue = client.ApplicantCell;
                telephoneNumberRecordControl_AppWork.EditValue = client.ApplicantWorkPhone;
                telephoneNumberRecordControl_CoappCell.EditValue = client.CoapplicantCell;
                telephoneNumberRecordControl_CoappWork.EditValue = client.CoapplicantWorkPhone;

                addressRecordControl.EditValue = client.Address;
            }
            else
            {
                //telephoneNumberRecordControl_Home.EditValue = searchParams.HomePhone;
                PopulateSearchPhone();
            }
        }

        private void PopulateSearchPhone()
        {
            telephoneNumberRecordControl_Home.SetCurrentValues(searchParams.HomePhone);
        }

        private void LoadLookup()
        {
            var referrals = DebtPlus.LINQ.Cache.referred_by.getList();
            luReferral.Properties.DataSource = referrals.Where(r => r.description.StartsWith(USBankReferralPrefix)).ToList();

            List<TrialModificationType> trialModifications = new List<TrialModificationType>();
            trialModifications.Add(new TrialModificationType());
            trialModifications.AddRange(DebtPlus.LINQ.Cache.TrialModificationType.getList());
            luTrialModification.Properties.DataSource = trialModifications;
        }

        protected void FormChanged(object sender, EventArgs e)
        {
            bool isEnabled = !HasErrors();
            btnCreateClient.Enabled = isEnabled;
        }

        private bool HasErrors()
        {
            bool hasError = false;

            if (luReferral.EditValue == null)
            {
                hasError = true;
            }

            if (nameRecordControl_Applicant.Text_First.EditValue == null || string.IsNullOrWhiteSpace(nameRecordControl_Applicant.Text_First.EditValue.ToString()) ||
                nameRecordControl_Applicant.Text_Last.EditValue == null || string.IsNullOrWhiteSpace(nameRecordControl_Applicant.Text_Last.EditValue.ToString()))
            {
                hasError = true;
            }

            var homePhone = telephoneNumberRecordControl_Home.GetCurrentValues().SearchValue;
            var messagePhone = telephoneNumberRecordControl_Message.GetCurrentValues().SearchValue;
            var appCell = telephoneNumberRecordControl_AppCell.GetCurrentValues().SearchValue;
            var appWork = telephoneNumberRecordControl_AppWork.GetCurrentValues().SearchValue;
            var coappCell = telephoneNumberRecordControl_CoappCell.GetCurrentValues().SearchValue;
            var coappWork = telephoneNumberRecordControl_CoappWork.GetCurrentValues().SearchValue;

            if (string.IsNullOrWhiteSpace(homePhone) && string.IsNullOrWhiteSpace(messagePhone) && string.IsNullOrWhiteSpace(appCell) &&
                string.IsNullOrWhiteSpace(appWork) && string.IsNullOrWhiteSpace(coappCell) && string.IsNullOrWhiteSpace(coappWork))
            {
                hasError = true;
            }

            // DP-971 : Remove mandatory check on the House field of the Address
            //if (addressRecordControl.House.EditValue == null || string.IsNullOrWhiteSpace(addressRecordControl.House.EditValue.ToString()) ||
            if (addressRecordControl.Street.EditValue == null || string.IsNullOrWhiteSpace(addressRecordControl.Street.EditValue.ToString()) ||
                addressRecordControl.PostalCode.EditValue == null || string.IsNullOrWhiteSpace(addressRecordControl.PostalCode.EditValue.ToString()))
            {
                hasError = true;
            }

            return hasError;
        }

        protected void btnCreateClient_Click(Object sender, EventArgs e)
        {
            int clientID = CreateClient();
            ShowClientForm(clientID);
            CloseForm();
        }

        private int CreateClient()
        {
            // Fetch current values
            var loanNumber = txtUSBankLoanNumber.Text.Trim();
            var trialModification = DebtPlus.Utils.Nulls.v_Int32(luTrialModification.EditValue);
            var referralCode = DebtPlus.Utils.Nulls.v_Int32(luReferral.EditValue);
            var homePhone = telephoneNumberRecordControl_Home.EditValue;
            var messagePhone = telephoneNumberRecordControl_Message.EditValue;
            var applicantCell = telephoneNumberRecordControl_AppCell.EditValue;
            var applicantWorkPhone = telephoneNumberRecordControl_AppWork.EditValue;
            var coapplicantCell = telephoneNumberRecordControl_CoappCell.EditValue;
            var coapplicantWorkPhone = telephoneNumberRecordControl_CoappWork.EditValue;
            var address = addressRecordControl.EditValue;
            var applicantName = nameRecordControl_Applicant.EditValue;
            var coapplicantName = nameRecordControl_Coapplicant.EditValue;
            int clientID = 0;

            if (!isClientCreated)
            {
                var username = String.Format("{0}\\{1}", Environment.UserDomainName, Environment.UserName);

                var result = bc.xpr_nationstar_create_client((int)PROGRAM.USBankSD1308, referralCode, loanNumber, trialModification, applicantName, coapplicantName, homePhone, applicantCell, coapplicantCell, messagePhone, applicantWorkPhone, coapplicantWorkPhone, address, searchParams.MethodOfFirstContact, username);
                var clients = result.ToList();

                var client = clients[0];

                if (client != null && client.ClientID != null)
                {
                    clientID = (int)client.ClientID;
                }
            }
            else
            {
                var result = bc.xpr_nationstar_create_ocsclient_uploadrecord(searchParams.ClientID, (int)PROGRAM.USBankSD1308, loanNumber, trialModification, applicantName, coapplicantName, address, homePhone, messagePhone, applicantCell, applicantWorkPhone, coapplicantCell, coapplicantWorkPhone);
                clientID = searchParams.ClientID;
            }

            return clientID;
        }

        private void ShowClientForm(int clientID)
        {
            OCSClient foundClient = null;


            var t = new System.Threading.Thread(() =>
            {
                try
                {
                    using (var thrd_bc = new BusinessContext())
                    {
                        OCSDbMapper repository = new OCSDbMapper(thrd_bc);

                        var ocsRecord = thrd_bc.OCS_Clients.Where(ocs => ocs.ClientId == clientID).FirstOrDefault();

                        if (ocsRecord != null)
                        {
                            foundClient = repository.GetByOCSId(ocsRecord.Id);
                            using (var clientForm = new ClientForm(thrd_bc, repository, foundClient))
                            {
                                clientForm.ShowDialog();
                            }
                            // Ensure that there is nothing left that is "pending" before it is disposed.
                            thrd_bc.SubmitChanges();
                        }                        
                    }
                }

                catch (System.Exception ex)
                {
                    // Handle the error conditions
                    DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex);
                }
            });
            t.SetApartmentState(System.Threading.ApartmentState.STA);
            t.IsBackground = false;
            t.Start();
        }

        protected void btnCancel_Click(Object sender, EventArgs e)
        {
            CloseForm();
        }

        private void exitFileItem_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            CloseForm();
        }

        /// <summary>
        /// Process the CLICK event on the CreateAppointment menu item
        /// </summary>
        private void newAppointmentsItem_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            var t = new System.Threading.Thread(() =>
            {
                try
                {
                    // This is a modal dialog. Do not use "show". You need to use "showDialog".
                    using (var thrd_bc = new BusinessContext())
                    {
                        using (var frm = new DebtPlus.UI.Client.forms.Appointments.Appointments_Form_Book(thrd_bc, searchParams.ClientID))
                        {
                            frm.ShowDialog(this);
                        }

                        // Ensure that there is nothing left that is "pending" before it is disposed.
                        thrd_bc.SubmitChanges();
                    }
                }

                catch (System.Exception ex)
                {
                    // Handle the error conditions
                    DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex);
                }
            })
            {
                IsBackground = true,
                Name = "CreateAppointment"
            };

            t.SetApartmentState(System.Threading.ApartmentState.STA);
            t.Start();
        }

        /// <summary>
        /// Process the CLICK event on the ListAppointments menu item
        /// </summary>
        private void listAppointmentsItem_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            var rpt = new DebtPlus.Reports.Client.Appointments.ClientAppointmentsReport();
            rpt.ClientID = searchParams.ClientID;
            rpt.Parameter_PendingOnly = true;
            rpt.AllowParameterChangesByUser = true;
            rpt.RunReportInSeparateThread();
        }

        /// <summary>
        /// Process the CLICK event on the CancelAppointment menu item
        /// </summary>
        private void cancelAppointmentsItem_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            var t = new System.Threading.Thread(() =>
            {
                try
                {
                    using (var thrd_bc = new BusinessContext())
                    {
                        using (var frm = new DebtPlus.UI.Client.forms.Appointments.Appointments_Form_Cancel(thrd_bc, searchParams.ClientID))
                        {
                            frm.ShowDialog(this);
                        }

                        // Ensure that there is nothing left that is "pending" before it is disposed.
                        thrd_bc.SubmitChanges();
                    }
                }

                catch (System.Exception ex)
                {
                    // Handle the error conditions
                    DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex);
                }
            })
            {
                IsBackground = true,
                Name = "CancelAppointment"
            };

            t.SetApartmentState(System.Threading.ApartmentState.STA);
            t.Start();
        }

        /// <summary>
        /// Process the CLICK event on the ConfirmAppointment menu item
        /// </summary>
        private void confirmAppointmentsItem_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            var t = new System.Threading.Thread(() =>
            {
                try
                {
                    using (var thrd_bc = new BusinessContext())
                    {
                        using (var frm = new DebtPlus.UI.Client.forms.Appointments.Appointments_Form_Confirm(thrd_bc, searchParams.ClientID))
                        {
                            frm.ShowDialog(this);
                        }

                        // Ensure that there is nothing left that is "pending" before it is disposed.
                        thrd_bc.SubmitChanges();
                    }
                }

                catch (System.Exception ex)
                {
                    // Handle the error conditions
                    DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex);
                }
            })
            {
                IsBackground = true,
                Name = "ConfirmAppointment"
            };

            t.SetApartmentState(System.Threading.ApartmentState.STA);
            t.Start();
        }

        /// <summary>
        /// Process the CLICK event on the RescheduleAppointment menu item
        /// </summary>
        private void rescheduleAppointmentsItem_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            var t = new System.Threading.Thread(() =>
            {
                try
                {
                    using (var thrd_bc = new BusinessContext())
                    {
                        using (var frm = new DebtPlus.UI.Client.forms.Appointments.Appointments_Form_Reschedule(thrd_bc, searchParams.ClientID))
                        {
                            frm.ShowDialog(this);
                        }

                        // Ensure that there is nothing left that is "pending" before it is disposed.
                        thrd_bc.SubmitChanges();
                    }
                }
                catch (System.Exception ex)
                {
                    // Handle the error conditions
                    DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex);
                }
            })
            {
                IsBackground = true,
                Name = "RescheduleAppointment"
            };

            t.SetApartmentState(System.Threading.ApartmentState.STA);
            t.Start();
        }

        private void CloseForm()
        {
            this.Close();
        }
    }
}