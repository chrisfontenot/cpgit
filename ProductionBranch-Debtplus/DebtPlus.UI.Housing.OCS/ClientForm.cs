﻿using System;
using System.Collections.Generic;
using System.Data.SqlTypes;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Windows.Forms;
using DebtPlus.LINQ;
using DebtPlus.LINQ.BusinessLayer;
using DebtPlus.OCS.Domain;
using DebtPlus.Svc.OCS;
using DebtPlus.Svc.OCS.Phone;
using DebtPlus.UI.Client.Service;
using DebtPlus.UI.Desktop.CS.Client.Create;
using DevExpress.XtraEditors;
using DevExpress.XtraGrid.Views.Grid;

namespace DebtPlus.UI.Housing.OCS
{
    public partial class ClientForm : DevExpress.XtraEditors.XtraForm
    {
        BusinessContext bc = null;
        OCSClient loadedClient;
        LINQ.view_OCS_Account_Tab loadedClientAccount;
        OCSDbMapper repository;
        OCSSortArgs sortArgs;
        string specificReason = String.Empty;

        List<string> allowedClickToCall = new List<string> { 
            "brwilhite", "jnyman", "vnair", "varnai", "caroline"
        };

        public ClientForm() : base()
        {
            InitializeComponent();
        }

        private void SetupStorage(BusinessContext bc)
        {
            this.bc = bc;
            ResetControlValues();
            ResetProgramSpecificControlValues(null);
            bindClientAccountValues(null);

            if(allowedClickToCall.Contains(Environment.UserName.ToLower())){
                callBtn1.Visible = true;
                callBtn2.Visible = true;
                callBtn3.Visible = true;
                callBtn4.Visible = true;
                callBtn5.Visible = true;
            }
            
            userTimezoneComboBox.Items.Clear();
            userTimezoneComboBox.Items.Add("Atlantic");
            userTimezoneComboBox.Items.Add("Eastern");
            userTimezoneComboBox.Items.Add("Mountain");
            userTimezoneComboBox.Items.Add("Pacific");
            userTimezoneComboBox.Items.Add("Alaska");
            userTimezoneComboBox.Items.Add("Hawaii");

            userTimezoneComboBox.SelectedIndexChanged += new EventHandler(userTimezoneComboBox_SelectedIndexChanged);

            getSavedUserTimezone();
        }

        public ClientForm(BusinessContext bc, OCSClient cl)
            : this()
        {
            SetupStorage(bc);
            LoadClient(cl);
        }

        public ClientForm(BusinessContext bc, OCSDbMapper repo, OCSClient cl)
            : this(bc,repo,cl,null) { }

        public ClientForm(BusinessContext bc, OCSDbMapper repo, OCSClient cl, OCSSortArgs sort)
            : this()
        {
            SetupStorage(bc);
            repository = repo;
            LoadClient(cl);
            sortArgs = sort;
            if (isSortMode()) {
                forwardBtn.Visible = true;
                backBtn.Visible = true;
                bufferStatus.Visible = true;
                toEndBtn.Visible = true;
                toBeginBtn.Visible = true;

                forwardBtn.Enabled = false;
                backBtn.Enabled = false;
                toEndBtn.Enabled = false;
                toBeginBtn.Enabled = false;

                recordHistory.Add(cl);
                historyPointer = recordHistory.Count - 1;
            }
        }
                
        public ClientForm(BusinessContext bc, OCSDbMapper repo, int? clientId)
            : this()
        {
            SetupStorage(bc);
            repository = repo;
            if (clientId.HasValue) {
                OCSClient cl = repo.GetActiveClientById(clientId.Value);
                LoadClient(cl);
            }
        }

        private bool isSortMode() {
            return sortArgs != null;
        }

        #region User Timezone

        private string userTz = null;
        private void getSavedUserTimezone()
        {
            // Read the registry entry. If none are set then use "Eastern" to start.
            userTz = readTimezoneFromRegistry();
            if (string.IsNullOrEmpty(userTz))
            {
                userTz = "Eastern";
            }

            userTimezone.EditValue = userTz;
        }

        private void userTimezoneComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            var combo = (DevExpress.XtraEditors.ComboBoxEdit)sender;
            if (combo.EditValue != null)
            {
                userTz = combo.EditValue.ToString();
                writeTimezoneToRegistry(userTz);
                timeDifferential.Text = calculateTimeDiff(loadedClient);
            }
        }

        /// <summary>
        /// Return the timezone item from the registry
        /// </summary>
        /// <returns></returns>
        private string readTimezoneFromRegistry()
        {
            return readRegistry("Options", "UserTimezone");
        }

        /// <summary>
        /// Set the timezone item into the registry
        /// </summary>
        /// <param name="timezoneString"></param>
        /// <returns></returns>
        private bool writeTimezoneToRegistry(string timezoneString)
        {
            return writeRegistry("Options", "UserTimezone", timezoneString);
        }

        /// <summary>
        /// Return the string value from the registry entry. If the item is missing, NULL is returned.
        /// </summary>
        /// <param name="RegistryKey">Pointer to the string for the registry key entry</param>
        /// <returns></returns>
        private string readRegistry(string RegistryHive, string RegistryKey)
        {
            // Return the registry value corresponding to that key location
            Microsoft.Win32.RegistryKey RegKey = Microsoft.Win32.Registry.CurrentUser;
            try
            {
                // Open the "DebtPlus" key location in the CurrentUser tree
                RegKey = RegKey.OpenSubKey(System.IO.Path.Combine(DebtPlus.Configuration.Constants.InstallationRegistryKey, RegistryHive), false);
                if (RegKey != null)
                {
                    // Return the current string from the registry setting.
                    return Convert.ToString(RegKey.GetValue(RegistryKey));
                }
            }

            catch { }

            finally
            {
                if (RegKey != null)
                {
                    RegKey.Close();
                }
            }
            return null;
        }

        /// <summary>
        /// Write the string to the registry
        /// </summary>
        /// <param name="RegistryKey">Pointer to the string for the registry key entry</param>
        /// <returns></returns>
        private bool writeRegistry(string RegistryHive, string RegistryKey, string value)
        {
            // Return the registry value corresponding to that key location
            Microsoft.Win32.RegistryKey RegKey = Microsoft.Win32.Registry.CurrentUser;
            try
            {
                // Open the "DebtPlus" key location in the CurrentUser tree
                RegKey = RegKey.OpenSubKey(System.IO.Path.Combine(DebtPlus.Configuration.Constants.InstallationRegistryKey, RegistryHive), true);
                
                // Set the string value into the registry
                RegKey.SetValue(RegistryKey, value);
                return true;
            }

            catch { }

            finally
            {
                if (RegKey != null)
                {
                    RegKey.Close();
                }
            }
            return false;
        }
        
        private string calculateTimeDiff(DebtPlus.OCS.Domain.OCSClient cl)
        {
            if (cl == null || cl.Address == null || userTz == null || cl.Address.TimeZone == TIMEZONE.Indeterminate)
            {
                return "???";
            }

            TIMEZONE tz = (TIMEZONE)Enum.Parse(typeof(TIMEZONE), userTz);
            int addOffset = cl.Address.TimeZone.GetOffset();
            int userOffset = tz.GetOffset();
            var diff = addOffset - userOffset;

            var timeDiff = (diff > 0 ? "+" : diff == 0 ? "" : "") + diff.ToString() + " H";
            return timeDiff;
        }

        #endregion User Timezone

        #region Forward/Back

        int historyPointer = -1;
        List<OCSClient> recordHistory = new List<OCSClient>();
        int bufferMax = 20;

        private bool isHistoryAtCurrent() {
            return historyPointer == recordHistory.Count - 1;
        }

        private void setNavUIState() {

            if (historyPointer <= 0)
            {
                backBtn.Enabled = false;
                toBeginBtn.Enabled = false;
            }
            else 
            {
                backBtn.Enabled = true;
                toBeginBtn.Enabled = true;
            }

            if (isHistoryAtCurrent())
            {
                forwardBtn.Enabled = false;
                toEndBtn.Enabled = false;
            }
            else 
            {
                forwardBtn.Enabled = true;
                toEndBtn.Enabled = true;
            }

            bufferStatus.Text = String.Format("{0} of {1}", historyPointer + 1, recordHistory.Count);
        }

        private void trimBuffer() {
            if (recordHistory.Count > bufferMax) {
                recordHistory.RemoveAt(0);
            }   
        }

        private bool goForward() {

            var toReturn = true;

            //case we are at the end of history
            if (recordHistory.Count == 0 || historyPointer >= recordHistory.Count - 1)
            {
                OCSClient claimed = repository.claimNextSort(new SortProvider(repository), sortArgs);

                if (claimed != null)
                {
                    LoadClient(claimed, false);
                    recordHistory.Add(claimed);
                    trimBuffer();

                    historyPointer = recordHistory.Count - 1;
                }
                else
                {
                    toReturn = false;
                }
            }
            //case we are somewhere in the middle of history
            else 
            {
                historyPointer = historyPointer + 1;
                LoadClient(recordHistory[historyPointer],isHistoryAtCurrent() == false);
            }
            
            setNavUIState();

            return toReturn;
        }

        private void goEnd() {
            if (recordHistory.Count > 0) {
                LoadClient(recordHistory[recordHistory.Count - 1], false);
                historyPointer = recordHistory.Count - 1;
            }
            setNavUIState();
        }

        private void goBackward() {

            if (isHistoryAtCurrent()) {
                recordHistory[historyPointer] = extractClientFromUI();
            }

            if (historyPointer > 0 && recordHistory.Count > 0) {
                LoadClient(recordHistory[historyPointer - 1], true);
                historyPointer = historyPointer - 1;
            }

            setNavUIState();
        }

        private void goBeginning() {
            if (isHistoryAtCurrent())
            {
                recordHistory[historyPointer] = extractClientFromUI();
            }
            if (historyPointer > 0 && recordHistory.Count > 0)
            {
                LoadClient(recordHistory[0], true);
                historyPointer = 0;
            }

            setNavUIState();
        }

        private void forwardBtn_Click(object sender, EventArgs e)
        {
            if (isSortMode() && !goForward())
            {                
                MessageBox.Show("You have reached the end of the list");
            }
        }

        private void backBtn_Click(object sender, EventArgs e)
        {
            goBackward();
        }

        private void toBeginBtn_Click(object sender, EventArgs e)
        {
            goBeginning();
        }

        private void toEndBtn_Click(object sender, EventArgs e)
        {
            goEnd();
        }

        #endregion Forward/Back

        #region Loading/Hydrating

        private void LoadClient(OCSClient cl, bool readOnly = false)
        {
            loadedClient = cl;

            loadedClientAccount = null;
            ResetControlValues();
            ResetProgramSpecificControlValues(cl.Program);
            restoreToActiveItem.Enabled = false;
            
            if (!readOnly)
            {
                enableAllInputs();
                if (loadedClient.CurrentAttempt == null)
                {
                    loadedClient.StartContactAttempt();
                }
                bindAttemptValues(cl.CurrentAttempt);
                restoreToActiveItem.Enabled = loadedClient.Archive;//check for user permissions....
                bindClientValues(loadedClient);
                bindClientAccountValues(null);

            }else {
                bindClientValues(loadedClient);
                bindClientAccountValues(null);
                if (cl.CurrentAttempt != null) {
                    bindAttemptValues(cl.CurrentAttempt);
                }                
                disableAllInputs();
            }

            try
            {
                bool doNotDisturb = bc.clients.Where(c => c.Id == cl.ClientID).First().doNotDisturb;
                MaskPhoneNumbers(doNotDisturb);
            }
            catch(Exception ex)
            {
                string message = ex.Message;
            }
        }

        private void LoadClientAccount(int clientId)
        {
            loadedClientAccount = repository.GetClientAccountTab(clientId, loadedClient.ID.Value, loadedClient.Program);
            bindClientAccountValues(loadedClientAccount);
        }
        
        private void ResetControlValues()
        {
            p1FirstName.Text = "";
            p1MiddleName.Text = "";
            p1LastName.Text = "";
            p1Email.Text = "";
            p1SSN.Text = "";
            program.Text = "";

            p2FirstName.Text = "";
            p2MiddleName.Text = "";
            p2LastName.Text = "";
            p2Email.Text = "";
            p2SSN.Text = "";

            clientAddress1.Text = "";
            clientAddress2.Text = "";
            clientCity.Text = "";
            clientZipcode.Text = "";

            propertyAddress1.Text = "";
            propertyAddress2.Text = "";
            propertyCity.Text = "";
            propertyZipcode.Text = "";

            // As per new requirement, this value has to be retained so that used does not have to select this for every record
            // contactType.SelectedIndex = -1;

            LoadDropdown(timezone, DropdownConfig.GetTimezones());
            LoadDropdown(clientUSState, DropdownConfig.GetUSStateAbbreviations());
            LoadDropdown(propertyUSState, DropdownConfig.GetUSStateAbbreviations());
            LoadDropdown(preferredLanguage, new Dictionary<string, object> {
                {"English", Person.LANGUAGE.English},
                {"Spanish", Person.LANGUAGE.Spanish}
            });
            LoadDropdown(stage1, new Dictionary<string, object> { 
                {"Not Set", STAGE.NotSet}
            });
            LoadDropdown(stage2, new Dictionary<string, object> { 
                {"Not Set", STAGE.NotSet}
            });
            LoadDropdown(stage3, new Dictionary<string, object> { 
                {"Not Set", STAGE.NotSet}
            });
            LoadDropdown(stage4, new Dictionary<string, object> { 
                {"Not Set", STAGE.NotSet}
            });

            ph1Number.Text = "";
            ph2Number.Text = "";
            ph3Number.Text = "";
            ph4Number.Text = "";
            ph5Number.Text = "";

            LoadDropdown(p1Salutation, DropdownConfig.GetSalutations());
            p1Salutation.SelectedIndex = -1;
            LoadDropdown(p2Salutation, DropdownConfig.GetSalutations());
            p2Salutation.SelectedIndex = -1;
            LoadDropdown(p1Suffix, DropdownConfig.GetSuffixes());
            p1Suffix.SelectedIndex = -1;
            LoadDropdown(p2Suffix, DropdownConfig.GetSuffixes());
            p2Suffix.SelectedIndex = -1;

            var phoneTypes = DropdownConfig.GetPhoneTypes();
            LoadDropdown(ph1Type, phoneTypes);
            LoadDropdown(ph2Type, phoneTypes);
            LoadDropdown(ph3Type, phoneTypes);
            LoadDropdown(ph4Type, phoneTypes);
            LoadDropdown(ph5Type, phoneTypes);

            submitBtn.Enabled = false;
        }

        private void ResetProgramSpecificControlValues(PROGRAM? prog)
        {
            int selectedContactType = (contactType.SelectedIndex >= 0) ? contactType.SelectedIndex : -1;

            if (prog.HasValue == false) {
                statusCode.Properties.Items.Clear();
                contactType.Properties.Items.Clear();
                ph1Result.Properties.Items.Clear();
                ph2Result.Properties.Items.Clear();
                ph3Result.Properties.Items.Clear();
                ph4Result.Properties.Items.Clear();
                ph5Result.Properties.Items.Clear();
                ph1MTI.Properties.Items.Clear();
                ph2MTI.Properties.Items.Clear();
                ph3MTI.Properties.Items.Clear();
                ph4MTI.Properties.Items.Clear();
                ph5MTI.Properties.Items.Clear();

                return;
            }

            LoadDropdown(statusCode, ProgramConfigService.getStatusCodes(prog.Value).convertForDD());
            LoadDropdown(contactType, ProgramConfigService.getContactTypes(prog.Value).convertForDD());

            if (selectedContactType >= 0)
            {
                contactType.SelectedIndex = selectedContactType;
            }

            var unsortedResultCodes = ProgramConfigService.getResultCodes(prog.Value).convertForDD();
            Dictionary<string, object> resultCodes = new Dictionary<string, object>();
            foreach (var kvp in unsortedResultCodes.OrderBy(o => o.Key))
            {
                resultCodes.Add(kvp.Key, kvp.Value);
            }

            LoadDropdown(ph1Result, resultCodes);
            LoadDropdown(ph2Result, resultCodes);
            LoadDropdown(ph3Result, resultCodes);
            LoadDropdown(ph4Result, resultCodes);
            LoadDropdown(ph5Result, resultCodes);

            var mtiResults = ProgramConfigService.getMTIResults(prog.Value).convertForDD();
            LoadDropdown(ph1MTI, mtiResults);
            LoadDropdown(ph2MTI, mtiResults);
            LoadDropdown(ph3MTI, mtiResults);
            LoadDropdown(ph4MTI, mtiResults);
            LoadDropdown(ph5MTI, mtiResults);
        }

        private void enableAllInputs()
        {
            contactType.Enabled = true;
            preferredLanguage.Enabled = true;
            p1Salutation.Enabled = true;
            p2Salutation.Enabled = true;
            p1Suffix.Enabled = true;
            p2Suffix.Enabled = true;
            p1Email.Enabled = true;
            p1SSN.Enabled = true;
            p2Email.Enabled = true;
            p2SSN.Enabled = true;

            ph1Number.Enabled = true;
            ph1Result.Enabled = true;
            ph1Type.Enabled = true;

            ph2Number.Enabled = true;
            ph2Result.Enabled = true;
            ph2Type.Enabled = true;

            ph3Number.Enabled = true;
            ph3Result.Enabled = true;
            ph3Type.Enabled = true;

            ph4Number.Enabled = true;
            ph4Result.Enabled = true;
            ph4Type.Enabled = true;

            ph5Number.Enabled = true;
            ph5Result.Enabled = true;
            ph5Type.Enabled = true;

            submitBtn.Enabled = true;
        }
        
        private void disableAllInputs()
        {
            contactType.Enabled = false;
            preferredLanguage.Enabled = false;
            p1Salutation.Enabled = false;
            p2Salutation.Enabled = false;
            p1Suffix.Enabled = false;
            p2Suffix.Enabled = false;
            p1Email.Enabled = false;
            p1SSN.Enabled = false;
            p2Email.Enabled = false;
            p2SSN.Enabled = false;

            ph1Number.Enabled = false;
            ph1Result.Enabled = false;
            ph1Type.Enabled = false;

            ph2Number.Enabled = false;
            ph2Result.Enabled = false;
            ph2Type.Enabled = false;

            ph3Number.Enabled = false;
            ph3Result.Enabled = false;
            ph3Type.Enabled = false;

            ph4Number.Enabled = false;
            ph4Result.Enabled = false;
            ph4Type.Enabled = false;

            ph5Number.Enabled = false;
            ph5Result.Enabled = false;
            ph5Type.Enabled = false;
        }

        private void bindAttemptValues(ContactAttempt contactAttempt)
        {
            if (contactAttempt.Type.HasValue) {
                selectDropdown(contactType, value => (CONTACT_TYPE)value == contactAttempt.Type);
            }            

            if (contactAttempt.Outcomes.Count > 0) {
                bindOutcome(contactAttempt.Outcomes[0], ph1Number, ph1Type, ph1Result, ph1MTI);
            }
            if (contactAttempt.Outcomes.Count > 1)
            {
                bindOutcome(contactAttempt.Outcomes[1], ph2Number, ph2Type, ph2Result, ph2MTI);
            }
            if (contactAttempt.Outcomes.Count > 2)
            {
                bindOutcome(contactAttempt.Outcomes[2], ph3Number, ph3Type, ph3Result, ph3MTI);
            }
            if (contactAttempt.Outcomes.Count > 3)
            {
                bindOutcome(contactAttempt.Outcomes[3], ph4Number, ph4Type, ph4Result, ph4MTI);
            }
            if (contactAttempt.Outcomes.Count > 4)
            {
                bindOutcome(contactAttempt.Outcomes[4], ph5Number, ph5Type, ph5Result, ph5MTI);
            }
        }

        private void bindClientAccountValues(LINQ.view_OCS_Account_Tab _loadedClientAccount)
        {
            var acct = _loadedClientAccount;
            if (acct == null)
            {
                investorNo.Text = "";
                servicer.Text = "";
                servicerLoanNo.Text = "";
                servicerIDNo.Text = "";
                foreclosureDate.Text = "";
                pastDueDate.Text = "";
                daysPastDue.Text = "";
                monthsPastDue.Text = "";
                defaultReason.Text = "";
                trialMod.Text = "";
                loanModDate.Text = "";
                txt_TrialMod_Type.Text = "";
                pmtAppliedDate.Text = "";
                nextPmtDate.Text = "";
                dateFileReceived.Text = "";
                dateFileModified.Text = "";
                dateClosed.Text = "";
                duplicateFile.Checked = false;
                dt_ReferralDate.Text = "";
                agenyCase.Text = "";
                batchName.Text = "";
                firstRPContact.Text = "";
                firstCounselDate.Text = "";
                secondCounselDate.Text = "";
                lastChanceDate.Text = "";
                txtLastChanceFlag.Text = "";
                actualSaleDate.Text = "";
            }
            else {
                investorNo.Text = acct.InvestorNo;
                servicer.Text = acct.Servicer;
                servicerLoanNo.Text = acct.ServicerLoanNum;
                servicerIDNo.Text = acct.ServicerID.HasValue ? acct.ServicerID.Value.ToString() : "";
                foreclosureDate.Text = acct.ForeclosureDate.HasValue ? acct.ForeclosureDate.Value.ToShortDateString() : "";
                pastDueDate.Text = acct.PastDueDate.HasValue && acct.PastDueDate.Value != SqlDateTime.MinValue.Value ? acct.PastDueDate.Value.ToShortDateString() : "";
                daysPastDue.Text = acct.DaysPastDue.ToString();
                monthsPastDue.Text = acct.MonthsPastDue.HasValue ? acct.MonthsPastDue.Value.ToString() : "";
                defaultReason.Text = acct.DefaultReason.HasValue ? acct.DefaultReason.Value.ToString() : "";
                trialMod.Text = acct.TrialMod;
                loanModDate.Text = acct.LoanModDate.HasValue ? acct.LoanModDate.Value.ToShortDateString() : "";
                txt_TrialMod_Type.Text = acct.TrialMod_Type;
                pmtAppliedDate.Text = acct.LoanPmtApplied.HasValue ? acct.LoanPmtApplied.Value.ToShortDateString() : "";
                nextPmtDate.Text = acct.NextPmtDue.HasValue ? acct.NextPmtDue.Value.ToShortDateString() : "";
                dateFileReceived.Text = acct.DateFileReceived.HasValue ? acct.DateFileReceived.Value.ToShortDateString() : "";
                dateFileModified.Text = acct.DateFileModified.HasValue ? acct.DateFileModified.Value.ToShortDateString() : "";
                dateClosed.Text = acct.DateClosed.HasValue ? acct.DateClosed.Value.ToShortDateString() : "";
                duplicateFile.Checked = acct.DuplicateFile;
                dt_ReferralDate.Text = acct.Referral_Date.HasValue ? acct.Referral_Date.Value.ToShortDateString() : "";
                agenyCase.Text = acct.AgencyCase;
                batchName.Text = acct.BatchName;
                firstRPContact.Text = acct.FirstRPContactDate.HasValue && acct.FirstRPContactDate.Value != default(DateTime) ? acct.FirstRPContactDate.Value.ToShortDateString() : "";
                firstCounselDate.Text = acct.FirstCounselDate.HasValue && acct.FirstCounselDate.Value != default(DateTime) ? acct.FirstCounselDate.Value.ToShortDateString() : "";
                secondCounselDate.Text = acct.SecondCounselDate.HasValue && acct.SecondCounselDate.Value != default(DateTime) ? acct.SecondCounselDate.Value.ToShortDateString() : "";
                lastChanceDate.Text = acct.LastChanceDate.HasValue && acct.LastChanceDate.Value != default(DateTime) ? acct.LastChanceDate.Value.ToShortDateString() : "";
                txtLastChanceFlag.Text = acct.LastChanceFlag.HasValue ? acct.LastChanceFlag.ToString() : "";
                actualSaleDate.Text = acct.ActualSaleDate.HasValue && acct.ActualSaleDate.Value != SqlDateTime.MinValue.Value ? acct.ActualSaleDate.Value.ToShortDateString() : "";
            }
        }

        private class GridViewNote
        {
            public int Id { get; set; }
            public DateTime Date { get; set; }
            public string Type { get; set; }
            public string Counselor { get; set; }
            public string Subject { get; set; }
            public string IsArchived { get; set; }
        }

        List<GridViewNote> loadedNotes = null;
        private static Regex cleanDomain = new Regex(@"^\w*\\");

        private void bindNotes(int clientId) {

            List<GridViewNote> notes = new List<GridViewNote>();
        
            if (loadedNotes == null) {
                var linqNotes = repository.GetClientNotes(clientId);

                loadedNotes = (from ln in linqNotes.Try()
                orderby ln.date_created descending
                select new GridViewNote
                {
                    Id = ln.Id,
                    //Date = ln.date_created.ToString("g"),
                    Date = ln.date_created,
                    Type = ln.type < 1 || ln.type > 7 ? "" : ((DebtPlus.LINQ.InMemory.Notes.NoteTypes)ln.type).ToString(),
                    Counselor = cleanDomain.Replace(ln.created_by,String.Empty),
                    Subject = ln.subject,
                    IsArchived = "No"
                }).ToList();
            }

            notes.AddRange(loadedNotes);

            List<String> noteTypes = (new List<string> { 
                (showPermanentNotes.Checked ? "Permanent" : null),
                (showSystemNotes.Checked ? "System" : null),
                (showAlertNotes.Checked ? "Alert" : null),
                (showTemporaryNotes.Checked ? "Temporary" : null)
            }).Where(s => s != null).ToList();

            List<GridViewNote> archivedNotes = null;

            if (showArchivedNotes.Checked == true)
            {
                using (var ac = new ArchiveContext())
                {
                    var aNotes = ac.client_notes.Where(cn => cn.client_creditor == null && cn.client == clientId).ToList();

                    archivedNotes = aNotes.Select(cn => new GridViewNote()
                    {
                        Id = cn.Id,
                        //Date = cn.date_created.ToString("g"),
                        Date = cn.date_created,
                        Type = cn.type < 1 || cn.type > 7 ? "" : ((DebtPlus.LINQ.InMemory.Notes.NoteTypes)cn.type).ToString(),
                        Counselor = cleanDomain.Replace(cn.created_by, String.Empty),
                        Subject = cn.subject,
                        IsArchived = "Yes"
                    }).ToList();
                }

                notes.AddRange(archivedNotes);
            }

            notesGrid.DataSource = notes
                .Where(n => noteTypes.Contains(n.Type))
                .ToList();

            ((GridView)notesGrid.MainView).Columns["Id"].Visible = false;

            //20 is the minimum the designer will let us set the column width anyway...so lets just keep things simple and set it here
            ((GridView)notesGrid.MainView).Columns["Date"].Width = 25;
            ((GridView)notesGrid.MainView).Columns["Type"].Width = 20;
            ((GridView)notesGrid.MainView).Columns["Counselor"].Width = 20;
            ((GridView)notesGrid.MainView).Columns["IsArchived"].Width = 20;
            /* DP-1048 : Change the format of the date column to date time*/
            ((GridView)notesGrid.MainView).Columns["Date"].DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            ((GridView)notesGrid.MainView).Columns["Date"].DisplayFormat.FormatString = "MM/dd/yyyy hh:mm:ss tt";

            //make it non-editable
            foreach (var column in ((GridView)notesGrid.MainView).Columns)
            {
                ((DevExpress.XtraGrid.Columns.GridColumn)column).OptionsColumn.AllowEdit = false;
            }
        }

        private void bindClientValues(DebtPlus.OCS.Domain.OCSClient cl) {

            loadedNotes = null;
            bindNotes(cl.ClientID.Value);

            investorNo.Text = cl.InvestorLoanNo;
            servicer.Text = cl.Servicer;
            servicerLoanNo.Text = cl.LoanNo;
            program.Text = ProgramConfigService.getDescription(cl.Program);

            this.Text = "OCS - " + cl.ClientID.Value.ToString();

            queueName.Text = cl.ActiveFlag ? "Active" : "Inactive";      

            selectDropdown(timezone, value => (TIMEZONE)value == cl.Address.TimeZone);

            timeDifferential.Text = calculateTimeDiff(cl);
            
            contactAttemptCount.Text = cl.ContactAttemptCount.ToString();
            selectDropdown(preferredLanguage, value => (Person.LANGUAGE)value == cl.PreferredLanguage);
            selectDropdown(statusCode,value => (STATUS_CODE)value == cl.StatusCode);

            if(cl.Person1 != null){
                if (cl.Person1.Salutation.HasValue){
                    selectDropdown(p1Salutation, value => (Person.SALUTATION)value == cl.Person1.Salutation.Value);
                } 
                p1FirstName.Text = cl.Person1.FirstName;
                p1MiddleName.Text = cl.Person1.MiddleName;
                p1LastName.Text = cl.Person1.LastName;
                if(cl.Person1.Suffix.HasValue){
                    selectDropdown(p1Suffix, value => (Person.SUFFIX)value == cl.Person1.Suffix.Value);
                }
                p1Email.Text = cl.Person1.EmailAddress;
                p1SSN.Text = cl.Person1.SSN;

                var salutation = cl.Person1.Salutation.HasValue ? 
                    DropdownConfig.GetSalutations().Where(kvp => (Person.SALUTATION)kvp.Value == cl.Person1.Salutation.Value).First().Value :
                    String.Empty;

                var suffix = cl.Person1.Suffix.HasValue ?
                    ", " + DropdownConfig.GetSuffixes().Where(kvp => (Person.SUFFIX)kvp.Value == cl.Person1.Suffix.Value).First().Value :
                    String.Empty;

                clientNameItem.Caption = String.Format("{0} {1} {2} {3} {4}", salutation, cl.Person1.FirstName, cl.Person1.MiddleName, cl.Person1.LastName, suffix);
            }
            if (cl.Person2 != null)
            {
                if (cl.Person2.Salutation.HasValue) {
                    selectDropdown(p2Salutation, value => (Person.SALUTATION)value == cl.Person2.Salutation.Value);
                } 
                p2FirstName.Text = cl.Person2.FirstName;
                p2MiddleName.Text = cl.Person2.MiddleName;
                p2LastName.Text = cl.Person2.LastName;
                if (cl.Person2.Suffix.HasValue){
                    selectDropdown(p2Suffix, value => (Person.SUFFIX)value == cl.Person2.Suffix.Value);
                }
                p2Email.Text = cl.Person2.EmailAddress;
                p2SSN.Text = cl.Person2.SSN;
            }
            if (cl.Address != null) {
                clientAddress1.Text = cl.Address.street;
                clientAddress2.Text = cl.Address.address_line_2;
                clientCity.Text = cl.Address.city;
                if (cl.Address.StateProvince.HasValue) {
                    selectDropdown(clientUSState, value => (US_STATE)value == cl.Address.StateProvince);
                }                
                clientZipcode.Text = cl.Address.PostalCode;              
            }
            
            useHomeAddress.Checked = cl.UseHomeAddress;
            if (cl.PropertyAddress != null)
            {
                propertyAddress1.Text = cl.PropertyAddress.street;
                propertyAddress2.Text = cl.PropertyAddress.address_line_2;
                propertyCity.Text = cl.PropertyAddress.city;
                if (cl.PropertyAddress.StateProvince.HasValue)
                {
                    selectDropdown(propertyUSState, value => (US_STATE)value == cl.PropertyAddress.StateProvince);
                }
                propertyZipcode.Text = cl.PropertyAddress.PostalCode;
            }
            propertyAddress1.Enabled = !cl.UseHomeAddress;
            propertyAddress2.Enabled = !cl.UseHomeAddress;
            propertyCity.Enabled = !cl.UseHomeAddress;
            propertyUSState.Enabled = !cl.UseHomeAddress;
            propertyZipcode.Enabled = !cl.UseHomeAddress;

            //"stages" are not modeled in person yet.  need more info.
            selectDropdown(stage1, value => (STAGE)value == STAGE.NotSet);
            selectDropdown(stage2, value => (STAGE)value == STAGE.NotSet);
            selectDropdown(stage3, value => (STAGE)value == STAGE.NotSet);
            selectDropdown(stage4, value => (STAGE)value == STAGE.NotSet);

            debtPlusIdItem.EditValue = cl.ClientID.ToString();
        }
                 
        private void bindOutcome(ContactAttempt.CallOutcome outcome, Data.Controls.PhoneNumber phoneNumber, ComboBoxEdit phoneType, ComboBoxEdit resultCode, ComboBoxEdit mtiResult)
        {
            var phone = outcome.PhoneNumber;
            phoneNumber.Text = phone.SearchValue;
            resultCode.Enabled = true;

            if (phone.AutoAssignedType != PHONE_TYPE.None && !String.IsNullOrWhiteSpace(phone.Number)) {
                phoneNumber.Enabled = false;
            }

            if (phone.UserAssignedType == PHONE_TYPE.BadNumber)
            {
                outcome.Result = RESULT_CODE.PreviousBadNumber;
                //resultCode.Enabled = false;
            }
            else {
                selectDropdown(phoneType, value => (PHONE_TYPE)value == phone.UserAssignedType);
            }            

            if (outcome.Result.HasValue)
            {
                selectDropdown(resultCode, value => (RESULT_CODE)value == outcome.Result.Value);
            }
            else
            {
                resultCode.SelectedIndex = -1;
            }

            if (outcome.MTIResult.HasValue)
            {
                selectDropdown(mtiResult, value => (MTI_RESULT)value == outcome.MTIResult.Value);
            }
            else
            {
                mtiResult.SelectedIndex = -1;
            }
        }

        #endregion Loading/Hydrating

        #region Extracting/Validating/Saving

        private static Regex sanitizePhone = new Regex(@"\D");
        private ContactAttempt.CallOutcome ExtractOutcome(Data.Controls.PhoneNumber phoneNumber, ComboBoxEdit phoneType, ComboBoxEdit resultCode, ComboBoxEdit mtiResult) {
            
            var phoneText = sanitizePhone.Replace(phoneNumber.Text, "");
            if (phoneText == String.Empty) return null;

            var toReturn = new ContactAttempt.CallOutcome
            {
                PhoneNumber = new TelephoneNumber { 
                    SetNumber = phoneText
                }
            };

            toReturn.PhoneNumber.AutoAssignedType =
                loadedClient.PhoneNumbers.Exists(number => number.SearchValue == toReturn.PhoneNumber.SearchValue) ?
                loadedClient.PhoneNumbers.Where(number => number.SearchValue == toReturn.PhoneNumber.SearchValue).First().AutoAssignedType :
                PHONE_TYPE.None;

            if (phoneType.SelectedIndex != -1) toReturn.PhoneNumber.UserAssignedType = (PHONE_TYPE)GetValue(phoneType);
            if (resultCode.SelectedIndex != -1)
            {
                toReturn.Result = (RESULT_CODE)GetValue(resultCode);
            }
            else {
                toReturn.Result = RESULT_CODE.NotSet;
            }
            if (mtiResult.SelectedIndex != -1) toReturn.MTIResult = (MTI_RESULT)GetValue(mtiResult);

            if (toReturn.Result == RESULT_CODE.BadNumber || toReturn.Result == RESULT_CODE.PreviousBadNumber) {
                toReturn.PhoneNumber.UserAssignedType = PHONE_TYPE.BadNumber;
            }

            return toReturn;
        }

        private void extractCurrentAttemptFromUI(OCSClient toReturn) {

            if (loadedClient.CurrentAttempt != null)
            {
                toReturn.CurrentAttempt = new ContactAttempt();
                toReturn.CurrentAttempt.StartedAttempt(loadedClient.CurrentAttempt.Begin);
                toReturn.CurrentAttempt.EndedAttempt(loadedClient.CurrentAttempt.End);
                toReturn.CurrentAttempt.Outcomes = new List<ContactAttempt.CallOutcome>();
                if(contactType.SelectedIndex != -1) toReturn.CurrentAttempt.Type = (CONTACT_TYPE)GetValue(contactType);
                toReturn.CurrentAttempt.SpecificReason = loadedClient.CurrentAttempt.SpecificReason;

                var ph1 = ExtractOutcome(ph1Number, ph1Type, ph1Result, ph1MTI);
                var ph2 = ExtractOutcome(ph2Number, ph2Type, ph2Result, ph2MTI);
                var ph3 = ExtractOutcome(ph3Number, ph3Type, ph3Result, ph3MTI);
                var ph4 = ExtractOutcome(ph4Number, ph4Type, ph4Result, ph4MTI);
                var ph5 = ExtractOutcome(ph5Number, ph5Type, ph5Result, ph5MTI);
                if (ph1 != null)
                {
                    toReturn.CurrentAttempt.Outcomes.Add(ph1);
                    toReturn.PhoneNumbers.Add(ph1.PhoneNumber);
                }
                if (ph2 != null)
                {
                    toReturn.CurrentAttempt.Outcomes.Add(ph2);
                    toReturn.PhoneNumbers.Add(ph2.PhoneNumber);
                }
                if (ph3 != null)
                {
                    toReturn.CurrentAttempt.Outcomes.Add(ph3);
                    toReturn.PhoneNumbers.Add(ph3.PhoneNumber);
                }
                if (ph4 != null)
                {
                    toReturn.CurrentAttempt.Outcomes.Add(ph4);
                    toReturn.PhoneNumbers.Add(ph4.PhoneNumber);
                }
                if (ph5 != null)
                {
                    toReturn.CurrentAttempt.Outcomes.Add(ph5);
                    toReturn.PhoneNumbers.Add(ph5.PhoneNumber);
                }
            }        
        }

        private OCSClient extractClientFromUI() {
            var toReturn = new OCSClient();

            extractCurrentAttemptFromUI(toReturn);

            toReturn.ContactAttemptCount = Convert.ToInt32(contactAttemptCount.Text);
            toReturn.PreferredLanguage = (Person.LANGUAGE)GetValue(preferredLanguage);
            toReturn.StatusCode = (STATUS_CODE)GetValue(statusCode);
            toReturn.Program = loadedClient.Program;

            if (!String.IsNullOrWhiteSpace(servicer.Text))
            {
                toReturn.Servicer = servicer.Text;
            }

            if (!String.IsNullOrWhiteSpace(servicerLoanNo.Text)) {
                toReturn.LoanNo = servicerLoanNo.Text;
            }

            if ((new List<string> { clientAddress1.Text, clientAddress2.Text, clientCity.Text, (clientUSState.SelectedIndex != -1 ? "f" : ""), clientZipcode.Text })
                .Any(e => !String.IsNullOrWhiteSpace(e))) {
                    
                    toReturn.Address = new Address { 
                        street = clientAddress1.Text,
                        address_line_2 = clientAddress2.Text,
                        city = clientCity.Text,
                        StateProvince = (US_STATE)GetValue(clientUSState),
                        PostalCode = clientZipcode.Text,
                        GMTOffset = loadedClient.Address.GMTOffset,                        
                        TzDescriptor = loadedClient.Address.TzDescriptor
                    };
                    if (clientUSState.SelectedIndex != -1) {
                        toReturn.Address.StateProvince = (US_STATE)GetValue(clientUSState);
                    }
            }

            if ((new List<string> { p1FirstName.Text, p1MiddleName.Text, p1LastName.Text, p1Email.Text, p1SSN.Text })
                .Any(e => !String.IsNullOrWhiteSpace(e))) {

                Person.SUFFIX? suff = null;
                if(p1Suffix.SelectedIndex != -1) suff = (Person.SUFFIX)GetValue(p1Suffix);
                Person.SALUTATION? sal = null;
                if (p1Salutation.SelectedIndex != -1) sal = (Person.SALUTATION)GetValue(p1Salutation);

                toReturn.Person1 = new Person {
                    FirstName = p1FirstName.Text,
                    MiddleName = p1MiddleName.Text,
                    LastName = p1LastName.Text,
                    EmailAddress = p1Email.Text,
                    SSN = p1SSN.Text,
                    Suffix = suff,
                    Salutation = sal
                };
            }

            if ((new List<string> { p2FirstName.Text, p2MiddleName.Text, p2LastName.Text, p2Email.Text, p2SSN.Text }).Any(e => !String.IsNullOrWhiteSpace(e)))
            {
                Person.SUFFIX? suff = null;
                if (p2Suffix.SelectedIndex != -1) suff = (Person.SUFFIX)GetValue(p2Suffix);
                Person.SALUTATION? sal = null;
                if (p2Salutation.SelectedIndex != -1) sal = (Person.SALUTATION)GetValue(p2Salutation);

                toReturn.Person2 = new Person
                {
                    FirstName = p2FirstName.Text,
                    MiddleName = p2MiddleName.Text,
                    LastName = p2LastName.Text,
                    EmailAddress = p2Email.Text,
                    SSN = p2SSN.Text,
                    Suffix = suff,
                    Salutation = sal
                };
            }

            toReturn.ID = loadedClient.ID.Value;
            toReturn.ClientID = Convert.ToInt32(debtPlusIdItem.EditValue);

            //"stages" are not modeled in person yet.  need more info.
            selectDropdown(stage1, value => (STAGE)value == STAGE.NotSet);
            selectDropdown(stage2, value => (STAGE)value == STAGE.NotSet);
            selectDropdown(stage3, value => (STAGE)value == STAGE.NotSet);
            selectDropdown(stage4, value => (STAGE)value == STAGE.NotSet);
            
            toReturn.ActiveFlag = loadedClient.ActiveFlag;
            toReturn.InvestorLoanNo = loadedClient.InvestorLoanNo;
            toReturn.DueDate = loadedClient.DueDate;

            toReturn.Queue = loadedClient.Queue;

            toReturn.FirstCounselDate = loadedClient.FirstCounselDate;
            toReturn.SecondCounselDate = loadedClient.SecondCounselDate;
            toReturn.LastRpcDate = loadedClient.LastRpcDate;
            toReturn.PostModLastChanceDate = loadedClient.PostModLastChanceDate;
            toReturn.PostModLastChanceFlag = loadedClient.PostModLastChanceFlag;


            return extractAccountInfoFromUI(toReturn);            
        }

        private OCSClient extractAccountInfoFromUI(OCSClient client) {
            var toReturn = client;

            //PROPERTY ADDRESS RETRIEVAL
            if (loadedClientAccount != null)
            {
                toReturn.UseHomeAddress = useHomeAddress.Checked;
                if (!toReturn.UseHomeAddress)
                {
                    toReturn.PropertyAddress = new Address
                    {
                        street = propertyAddress1.Text,
                        address_line_2 = propertyAddress2.Text,
                        city = propertyCity.Text,
                        PostalCode = propertyZipcode.Text
                    };
                    if (propertyUSState.SelectedIndex != -1)
                    {
                        toReturn.PropertyAddress.StateProvince = (US_STATE)GetValue(propertyUSState);
                    }
                }
            }
            else
            {
                toReturn.UseHomeAddress = loadedClient.UseHomeAddress;
            }

            if (toReturn.PropertyAddress == null) {
                toReturn.PropertyAddress = loadedClient.PropertyAddress;
            }

            //INVESTOR NUMBER RETRIEVAL
            if (loadedClientAccount != null)
            {
                toReturn.InvestorLoanNo = investorNo.Text;
            }
            else {
                toReturn.InvestorLoanNo = loadedClient.InvestorLoanNo;
            }

            return toReturn;
        }

        private bool ValidateClient()
        {            
            StringBuilder errors = new StringBuilder();

            if (contactType.SelectedIndex == -1) {
                errors.Append("1. Call Type");
            }

            var errorString = errors.ToString();
            if (errorString != String.Empty) {
                errorString = "Please complete the following required fields and then resubmit:" + Environment.NewLine + Environment.NewLine + errorString;

                MessageBox.Show(errorString, "OCS - Freddie Mac - EI - Missing Required Info", MessageBoxButtons.OK);
                return false;
            }

            return true;
        }
        
        private bool CheckSpecificReason()
        {
            specificReason = String.Empty;

            ContactAttempt.CallOutcome found = null;

            var toCheck = new OCSClient();
            extractCurrentAttemptFromUI(toCheck);

            if (toCheck.CurrentAttempt != null)
            {
                found = toCheck.CurrentAttempt.Outcomes.Where(outcome => outcome.Result.HasValue && (outcome.Result.Value == RESULT_CODE.ClientNotInterested || outcome.Result.Value == RESULT_CODE.WorkingWithLender)).FirstOrDefault();
            }

            if (toCheck.CurrentAttempt != null && found != null)
            {
                var reasonDialog = new SpecificReasonDialog(found.Result.Value);

                reasonDialog.FormClosing += new FormClosingEventHandler(reasonDialog_FormClosing);

                reasonDialog.ShowDialog(this);

                reasonDialog.FormClosing -= reasonDialog_FormClosing;

                if (specificReason != String.Empty)
                {
                    //loadedClient.CurrentAttempt.SpecificReason = specificReason;
                    loadedClient.CurrentAttempt.SpecificReason =
                        String.Format("Reason for {0} = {1}", (found.Result.Value == RESULT_CODE.ClientNotInterested ? "NI" : "WL"), specificReason);

                    return true;
                }

                return false;
            }
            else
            {
                return true;
            }
        }

        private void reasonDialog_FormClosing(object sender, FormClosingEventArgs e)
        {
            specificReason = ((SpecificReasonDialog)sender).selectedReason;
        }

        private OCSClient SaveClient()
        {
            var newClient = extractClientFromUI();
            var oldClient = loadedClient;

            if (newClient.CurrentAttempt != null &&
                newClient.CurrentAttempt.Outcomes.Any(outcome => outcome.Result.Value == RESULT_CODE.Skip ||
                                                        outcome.Result.Value == RESULT_CODE.NotSet))
            {
                var msg =
@"Caution: You are about to skip 1 or more phone numbers.  This may prevent the contact attempts from advancing.
Do you wish to proceed and skip the telephone number(s)?";
                if (MessageBox.Show(msg, "OCS - Skip Phone Numbers", MessageBoxButtons.OKCancel, MessageBoxIcon.Warning) != System.Windows.Forms.DialogResult.OK)
                {
                    return null;
                }
                else {
                    newClient.CurrentAttempt.Outcomes.Where(outcome => outcome.Result.Value == RESULT_CODE.NotSet).ToList()
                        .ForEach(outcome => { outcome.Result = RESULT_CODE.Skip; });
                }
            }

            /* set the first and second counsel date */
            newClient.FirstCounselDate = oldClient.FirstCounselDate;
            newClient.SecondCounselDate = oldClient.SecondCounselDate;
            newClient.LastRpcDate = oldClient.LastRpcDate;
            newClient.PostModLastChanceDate = oldClient.PostModLastChanceDate;
            newClient.PostModLastChanceFlag = oldClient.PostModLastChanceFlag;

            newClient = repository.GetProgramSpecificInfo(newClient);

            if (newClient.PreferredLanguage != oldClient.PreferredLanguage)
            {
                MasterStateMachine.UpdateOnTrigger(newClient.Program, TRIGGER.LanguageChanged, oldClient.StatusCode, newClient);
            }
            else {
                MasterStateMachine.UpdateOnTrigger(newClient.Program, TRIGGER.NoTrigger, oldClient.StatusCode, newClient);
            }
            
            var result = oldClient.Validate(newClient);
            StringBuilder sb = new StringBuilder();
            foreach (var change in result.Warnings.Where(w => w.StartsWith("[Change]: ")))
            {
                sb.AppendLine(change);
            }
            
            if (oldClient.CurrentAttempt != null && !String.IsNullOrWhiteSpace(oldClient.CurrentAttempt.SpecificReason))
            {
                newClient.AddNote("SOME USER", oldClient.CurrentAttempt.SpecificReason, oldClient.CurrentAttempt.SpecificReason, (int)DebtPlus.LINQ.InMemory.Notes.NoteTypes.System);
            }

            newClient.AddNote("SOME USER", "Update", sb.ToString(), (int)DebtPlus.LINQ.InMemory.Notes.NoteTypes.System);

            repository.UpdateActiveClient(newClient);

            newClient.PhoneNumbers = oldClient.PhoneNumbers;
            
            return newClient;
        }

        private void submitBtn_Click(object sender, EventArgs e)
        {
            if (ValidateClient() &&
                CheckSpecificReason() &&
                MessageBox.Show("Submit this client info to the database?", "OCE - " + ProgramConfigService.getDescription(loadedClient.Program) + " - Confirm Submission", MessageBoxButtons.OKCancel) == System.Windows.Forms.DialogResult.OK)
            {
                submitBtn.Enabled = false;

                var saved = SaveClient();

                if (saved == null) {
                    submitBtn.Enabled = true;
                }

                if (isSortMode() && saved != null)
                {
                    if (isHistoryAtCurrent())
                    {
                        recordHistory[historyPointer] = saved;
                    }

                    if (!goForward()) {
                        LoadClient(saved, true);
                        MessageBox.Show("Record successfully saved. You have reached the end of the list");
                    }
                }
                else if (!isSortMode() && saved != null) {
                    this.Close();
                }
            }
        }

        #endregion Extracting/Validating/Saving

        #region Other Tabs/Actions

        private void newNoteBtn_Click(object sender, EventArgs e)
        {
            var newNote = new Notes.NoteClass.ClientNote();            
            newNote.Create(loadedClient.ClientID.Value);
        }
        
        private void propertyAddressIsSame_CheckedChanged(object sender, EventArgs e)
        {
            propertyAddress1.Enabled = !useHomeAddress.Checked;
            propertyAddress2.Enabled = !useHomeAddress.Checked;
            propertyCity.Enabled = !useHomeAddress.Checked;
            propertyUSState.Enabled = !useHomeAddress.Checked;
            propertyZipcode.Enabled = !useHomeAddress.Checked;
        }
        
        private void xtraTabControl1_SelectedPageChanging(object sender, DevExpress.XtraTab.TabPageChangingEventArgs e)
        {
            if (loadedClientAccount == null) {
                LoadClientAccount(loadedClient.ClientID.Value);
            }
        }

        private void toolStripMenuAdd_Click(object sender, EventArgs e)
        {
            var newNote = new Notes.NoteClass.ClientNote();

            if (newNote.Create(loadedClient.ClientID.Value)) {
                loadedNotes = null;
                bindNotes(loadedClient.ClientID.Value);
            }
        }

        private void toolStripMenuShow_Click(object sender, EventArgs e)
        {
            int[] selRows = ((GridView)notesGrid.MainView).GetSelectedRows();

            var note = (((GridView)notesGrid.MainView).GetRow(selRows[0]));
            
            //var newNote = new Notes.NoteClass.ClientNote();
            
            //newNote.Show(((GridViewNote)note).Id);
            ShowNote((GridViewNote)note);
        }

        private void gridView1_DoubleClick(object sender, EventArgs e)
        {
            int[] selRows = ((GridView)notesGrid.MainView).GetSelectedRows();

            var note = (((GridView)notesGrid.MainView).GetRow(selRows[0]));

            //var newNote = new Notes.NoteClass.ClientNote();

            //newNote.Show(((GridViewNote)note).Id);
            ShowNote((GridViewNote)note);
        }

        private void ShowNote(GridViewNote note)
        {
            using (var newNote = new Notes.NoteClass.ClientNote())
            {
                if (string.Compare(note.IsArchived, "yes", true) == 0)
                {
                    newNote.IsArchivedNote = true;
                }
                else
                {
                    newNote.IsArchivedNote = false;
                }
                newNote.Show(((GridViewNote)note).Id);
            }
        }
        
        private void barButtonItem1_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {

            var apptParams = new SearchEventArgs();
            apptParams.clientID = loadedClient.ClientID.Value;

            using (var form = new FormCreateAppointment(bc, apptParams, ProgramConfigService.getReferralSource(loadedClient.Program)))
            {
                form.ShowDialog();
                if (apptParams.clientAppointment != null)
                {
                    var counselorName = apptParams.clientAppointment.counselor.HasValue ? repository.GetCounselorName(apptParams.clientAppointment.counselor.Value) : String.Empty;
                    var body = String.Format(
@"A counselor appointment (#{0:f0}) for {1:MMM d yyyy h:mmtt} was scheduled.
The appointment is with counselor {2}.
The appointment is of type ""{3}"" and is at the {4} office.",
                        apptParams.clientAppointment.Id,
                        apptParams.clientAppointment.start_time,
                        counselorName,
                        (apptParams.clientAppointment.appt_type.HasValue ? repository.GetAppointmentType(apptParams.clientAppointment.appt_type.Value) : String.Empty),
                        (apptParams.clientAppointment.office.HasValue ? repository.GetOfficeName(apptParams.clientAppointment.office.Value) : String.Empty));

                    repository.AddNote(loadedClient.ClientID.Value,
                        String.Format("Scheduled {0:d} appointment with {1}", apptParams.clientAppointment.start_time, counselorName),
                        body);

                    loadedNotes = null;
                    bindNotes(loadedClient.ClientID.Value);
                }
            }

            // Ensure that there is nothing left as pending from the appointment operation
            try
            {
                bc.SubmitChanges();
            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex);
            }
        }

        private void barButtonItem5_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            DebtPlus.Reports.Client.Appointments.ClientAppointmentsReport rpt = new DebtPlus.Reports.Client.Appointments.ClientAppointmentsReport();
            rpt.ClientID = loadedClient.ClientID.Value;
            rpt.Parameter_PendingOnly = true;
            rpt.AllowParameterChangesByUser = true;
            rpt.RunReportInSeparateThread();
        }

        private void barButtonItem6_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            using (var form = new Client.forms.Appointments.Appointments_Form_Cancel(bc, loadedClient.ClientID.Value))
            {
                var result = form.ShowDialog();

                loadedNotes = null;
                bindNotes(loadedClient.ClientID.Value);
            }

            // Ensure that there is nothing left as pending from the appointment operation
            try
            {
                bc.SubmitChanges();
            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex);
            }
        }

        private void barButtonItem7_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            using (var form = new Client.forms.Appointments.Appointments_Form_Confirm(bc, loadedClient.ClientID.Value))
            {
                form.ShowDialog();

                loadedNotes = null;
                bindNotes(loadedClient.ClientID.Value);
            }

            // Ensure that there is nothing left as pending from the appointment operation
            try
            {
                bc.SubmitChanges();
            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex);
            }
        }

        private void barButtonItem8_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            using (var form = new Client.forms.Appointments.Appointments_Form_Reschedule(bc, loadedClient.ClientID.Value))
            {
                form.ShowDialog();

                loadedNotes = null;
                bindNotes(loadedClient.ClientID.Value);
            }

            // Ensure that there is nothing left as pending from the appointment operation
            try
            {
                bc.SubmitChanges();
            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex);
            }
        }

        private void barButtonItem3_ItemClick_1(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            //same as list appointment
            DebtPlus.Reports.Client.Appointments.ClientAppointmentsReport rpt = new DebtPlus.Reports.Client.Appointments.ClientAppointmentsReport();
            rpt.ClientID = loadedClient.ClientID.Value;
            rpt.Parameter_PendingOnly = false;
            rpt.AllowParameterChangesByUser = true;
            rpt.RunReportInSeparateThread();
        }
        
        private void barButtonItem3_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (MessageBox.Show("Caution: You are about to restore this record from the Archived File database to the Active database.  Restore this file to the Active Database?", "OCS - Freddie Mac - EI - Restore Archived File to Active Database", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == System.Windows.Forms.DialogResult.Yes)
            {
                if (repository.unarchiveAndActivate(loadedClient.ID.Value))
                {
                    loadedClient.Archive = false;
                    loadedClient.ActiveFlag = true;
                }
            }
        }
        
        private void barButtonItem2_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            //do nothing for now
        }

        private void barButtonItem4_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            this.Close();
        }

        private void barButtonItem9_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            DebtPlus.Reports.Client.Notes.ClientNotesReport rpt = new Reports.Client.Notes.ClientNotesReport();
            rpt.ClientId = loadedClient.ClientID.Value;
            rpt.AllowParameterChangesByUser = true;
            rpt.RunReportInSeparateThread();
        }

        private void showSystemNotes_CheckedChanged(object sender, EventArgs e)
        {
            bindNotes(loadedClient.ClientID.Value);
        }

        private void showPermanentNotes_CheckedChanged(object sender, EventArgs e)
        {
            bindNotes(loadedClient.ClientID.Value);
        }

        private void showAlertNotes_CheckedChanged(object sender, EventArgs e)
        {
            bindNotes(loadedClient.ClientID.Value);
        }

        private void showTemporaryNotes_CheckedChanged(object sender, EventArgs e)
        {
            bindNotes(loadedClient.ClientID.Value);
        }

        private void showArchivedNotes_CheckedChanged(object sender, EventArgs e)
        {
            bindNotes(loadedClient.ClientID.Value);
        }

        private void showDisbursementNotes_CheckedChanged(object sender, EventArgs e)
        {
            bindNotes(loadedClient.ClientID.Value);
        }

        #endregion  Other Tabs/Actions

        #region Helpers

        private void LoadDropdown(ComboBoxEdit combo, Dictionary<string, object> values)
        {
            DropdownHelper.LoadDropdown(combo, values);
        }

        private void selectDropdown(ComboBoxEdit combo, Func<object, bool> compareFunc)
        {
            DropdownHelper.selectDropdown(combo, compareFunc);
        }

        private object GetValue(ComboBoxEdit combo)
        {
            if (combo.SelectedIndex == -1)
            {
                return null;
            }
            var item = (DebtPlus.Data.Controls.ComboboxItem)combo.SelectedItem;
            return item.value;
        }

        #endregion Helpers

        #region Click To Call
        private void callBtn1_Click(object sender, EventArgs e)
        {
            call(ph1Number);
        }
        private void callBtn2_Click(object sender, EventArgs e)
        {
            call(ph2Number);
        }
        private void callBtn3_Click(object sender, EventArgs e)
        {
            call(ph3Number);
        }
        private void callBtn4_Click(object sender, EventArgs e)
        {
            call(ph4Number);
        }
        private void callBtn5_Click(object sender, EventArgs e)
        {
            call(ph5Number);
        }
        private void call(DebtPlus.Data.Controls.PhoneNumber phoneNumber) {
            var phoneText = sanitizePhone.Replace(phoneNumber.Text, "");
            OutboundCaller.call(phoneText);
        }
        #endregion Click To Call                               

        private void barButtonItem_Debtplus_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            var thread = new Thread(DisplayDebtPlusRecord);
            thread.Name = "LoadDebtPlus";
            thread.SetApartmentState(ApartmentState.STA);
            thread.IsBackground = true;
            thread.Start();
        }

        private void DisplayDebtPlusRecord()
        {
            using (var c = new ClientUpdateClass())
            {
                c.ShowEditDialog((int)loadedClient.ClientID, true);
            }
        }

        private void MaskPhoneNumbers(bool doNotDisturb)
        {
            if (doNotDisturb)
            {
                string blankPhone = "(___) ___-____";
                if (string.Compare(ph1Number.Text, blankPhone, true) != 0)
                {
                    ph1Number.MaskNumber();
                }
                if (string.Compare(ph2Number.Text, blankPhone, true) != 0)
                {
                    ph2Number.MaskNumber();
                }
                if (string.Compare(ph3Number.Text, blankPhone, true) != 0)
                {
                    ph3Number.MaskNumber();
                }
                if (string.Compare(ph4Number.Text, blankPhone, true) != 0)
                {
                    ph4Number.MaskNumber();
                }
                if (string.Compare(ph5Number.Text, blankPhone, true) != 0)
                {
                    ph5Number.MaskNumber();
                }
            }
        }
    }
}