﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DebtPlus.UI.Desktop.CS.Client.Create;
using DebtPlus.LINQ;

namespace DebtPlus.UI.Housing.OCS
{
    public class AppointmentWrapper
    {
        public LINQ.client_appointment appointment { get; set; }
    }

    public class FormCreateAppointment : DebtPlus.UI.Client.forms.Appointments.Appointments_Form_Book
    {
        protected DebtPlus.UI.Desktop.CS.Client.Create.SearchEventArgs mySearchParams;

        /// <summary>
        /// Initialize the new class instance
        /// </summary>
        public FormCreateAppointment()
            : base()
        {
        }

        /// <summary>
        /// Create the normal instance of the class
        /// </summary>
        public FormCreateAppointment(BusinessContext bc, SearchEventArgs searchParams)
            : base(bc, searchParams.clientID.Value)
        {
            mySearchParams = searchParams;   
        }

        /// <summary>
        /// Create the normal instance of the class
        /// </summary>
        public FormCreateAppointment(BusinessContext bc, SearchEventArgs searchParams,int referralCode)
            : base(bc, searchParams.clientID.Value,"",referralCode)
        {
            mySearchParams = searchParams;
        }
                
        /// <summary>
        /// Handle the completion of booking a face-to-face appointment
        /// </summary>
        protected override void Appointments_Book_Counseling_Control1_Completed(object sender, LINQ.client_appointment clientAppointment)
        {
            base.Appointments_Book_Counseling_Control1_Completed(sender, clientAppointment);
            mySearchParams.clientAppointment = clientAppointment;
        }

        /// <summary>
        /// Handle the completion of booking a workshop appointment
        /// </summary>
        protected override void Appointments_Control_Workshop_New1_Completed(object sender, LINQ.client_appointment clientAppointment)
        {
            base.Appointments_Control_Workshop_New1_Completed(sender, clientAppointment);
            mySearchParams.clientAppointment = clientAppointment;
        }
    }
}
