﻿namespace DebtPlus.UI.Housing.OCS.OneWest.Reports
{
    partial class OneWestClientCreationReport
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            DebtPlus.Data.Controls.ComboboxItem comboboxItem1 = new DebtPlus.Data.Controls.ComboboxItem();
            ((System.ComponentModel.ISupportInitialize)(this.XrGroup_param_08_1)).BeginInit();
            this.XrGroup_param_08_1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.XrCombo_param_08_1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.XrDate_param_08_2.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.XrDate_param_08_2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.XrDate_param_08_1.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.XrDate_param_08_1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DxErrorProvider1)).BeginInit();
            this.SuspendLayout();
            // 
            // XrCombo_param_08_1
            // 
            comboboxItem1.tag = null;
            comboboxItem1.value = DebtPlus.Utils.DateRange.Today;
            this.XrCombo_param_08_1.EditValue = comboboxItem1;
            // 
            // XrDate_param_08_2
            // 
            this.XrDate_param_08_2.EditValue = new System.DateTime(2015, 6, 16, 0, 0, 0, 0);
            // 
            // XrDate_param_08_1
            // 
            this.XrDate_param_08_1.EditValue = new System.DateTime(2015, 6, 16, 0, 0, 0, 0);
            // 
            // OneWestClientCreationReport
            // 
            this.ClientSize = new System.Drawing.Size(336, 137);
            this.Name = "OneWestClientCreationReport";
            this.Text = "OneWest Report";
            ((System.ComponentModel.ISupportInitialize)(this.XrGroup_param_08_1)).EndInit();
            this.XrGroup_param_08_1.ResumeLayout(false);
            this.XrGroup_param_08_1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.XrCombo_param_08_1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.XrDate_param_08_2.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.XrDate_param_08_2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.XrDate_param_08_1.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.XrDate_param_08_1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DxErrorProvider1)).EndInit();
            this.ResumeLayout(false);

        }
        #endregion
    }
}