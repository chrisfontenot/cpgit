﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DebtPlus.LINQ;

namespace DebtPlus.UI.Housing.OCS.OneWest.Create
{
    public class SearchParams
    {
        public TelephoneNumber HomePhone { get; set; }

        public int? MethodOfFirstContact { get; set; }

        public bool HousingProblem { get; set; }

        public bool ClientFound { get; set; }

        public int ClientID { get; set; }
    }
}
