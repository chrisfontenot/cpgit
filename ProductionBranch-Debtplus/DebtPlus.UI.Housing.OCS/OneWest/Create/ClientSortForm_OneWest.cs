﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DebtPlus.Interfaces.Desktop;
using DebtPlus.UI.Housing.OCS;

namespace DebtPlus.UI.Housing.OCS.OneWest
{
    public class ClientSortForm_OneWest : ClientSortLauncher
    {
        public ClientSortForm_OneWest()
        {
            program = DebtPlus.OCS.Domain.PROGRAM.OneWestSD1308;
        }
    }
}
