﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DebtPlus.Svc.OCS;
using DebtPlus.LINQ;
using System.Linq;

namespace DebtPlus.UI.Housing.OCS.OneWest.Create
{
    public partial class ClientSearch : DebtPlus.Data.Forms.DebtPlusForm
    {
        private SearchProvider provider;
        private OCSDbMapper repository;
        public Int32 ClientId { get; set; }

        public ClientSearch()
        {
            DebtPlus.LINQ.SQLInfoClass SqlInfo = DebtPlus.LINQ.SQLInfoClass.getDefault();
            var connectionString = SqlInfo.ConnectionString;
            DebtPlus.LINQ.BusinessContext bc = new LINQ.BusinessContext(connectionString);
            repository = new OCSDbMapper(bc);
            provider = new SearchProvider(repository);

            InitializeComponent();
            ClientId = Int32.MinValue;
            RegisterHandlers();
        }

        private void RegisterHandlers()
        {
            this.Load += new EventHandler(ClientSearch_Load);

            homePhoneControl.TextChanged += new EventHandler(Form_Changed);
            luFirstContactMethod.EditValueChanged += new EventHandler(Form_Changed);

            btnOK.Click += new EventHandler(btnOK_Click);
            btnCancel.Click += new EventHandler(btnCancel_Click);
        }

        /// <summary>
        /// Remove the registration for the events
        /// </summary>
        private void UnRegisterHandlers()
        {
            this.Load -= new EventHandler(ClientSearch_Load);

            homePhoneControl.TextChanged -= new EventHandler(Form_Changed);
            luFirstContactMethod.EditValueChanged -= new EventHandler(Form_Changed);

            btnOK.Click -= new EventHandler(btnOK_Click);
            btnCancel.Click -= new EventHandler(btnCancel_Click);
        }

        protected string HomePhone
        {
            get
            {
                string answer = string.Empty;
                try
                {
                    if (!homePhoneControl.GetCurrentValues().IsEmpty())
                    {
                        answer = homePhoneControl.GetCurrentValues().SearchValue;
                    }
                }
                catch
                {
                }
                return answer;
            }
        }

        protected string FirstContactMethod
        {
            get
            {
                string answer = string.Empty;
                if (luFirstContactMethod.EditValue != null)
                {
                    answer = luFirstContactMethod.EditValue.ToString();
                }
                return answer;
            }
        }

        private void LoadFirstContactMethod()
        {
            DebtPlus.LINQ.SQLInfoClass SqlInfo = DebtPlus.LINQ.SQLInfoClass.getDefault();
            var connectionString = SqlInfo.ConnectionString;
            using (DebtPlus.LINQ.BusinessContext bc = new LINQ.BusinessContext(connectionString))
            {
                luFirstContactMethod.Properties.DataSource = DebtPlus.LINQ.Cache.FirstContactType.getList();
            }
        }

        private void ClientSearch_Load(Object sender, EventArgs e)
        {
            UnRegisterHandlers();                    // Prevent evnet tripping when we are configuring the form.
            try
            {
                // Load the comboboxes
                LoadFirstContactMethod();
                EnableOK();
            }
            finally
            {
                RegisterHandlers();
            }
        }

        protected void Form_Changed(Object sender, EventArgs e)
        {
            EnableOK();
        }

        private void EnableOK()
        {
            string homePhone = HomePhone;
            string firstContactMethod = FirstContactMethod;

            bool dataEntered = true;
            if (string.IsNullOrWhiteSpace(homePhone) || homePhone.Length != 10)
            {
                dataEntered = false;
            }

            if(string.IsNullOrWhiteSpace(firstContactMethod)) 
            {
                dataEntered = false;
            }

            if (dataEntered)
            {
                btnOK.Enabled = true;
            }
            else
            {
                btnOK.Enabled = false;
            }
        }


        protected void btnOK_Click(Object sender, EventArgs e)
        {
            // Pad the telephone number on the left with zeros if needed
            string tempNumber = DebtPlus.Utils.Format.Strings.DigitsOnly(HomePhone).PadRight(10, '0');
            tempNumber = tempNumber.Substring(tempNumber.Length - 10, 10);

            // Generate the search key for the lists
            string searchKey = tempNumber;

            SearchParams param = new SearchParams()
            {
                HomePhone = homePhoneControl.GetCurrentValues(),
                HousingProblem = chkHousingProblems.Checked,
                MethodOfFirstContact = DebtPlus.Utils.Nulls.v_Int32(luFirstContactMethod.EditValue)
            };

            List<DebtPlus.LINQ.xpr_nationstar_search_clientResult> clients = null;
            // Retrieve the results
            using (var bc = new BusinessContext())
            {
                var result = bc.xpr_nationstar_search_client(searchKey);
                clients = result.ToList();
            }

            if (clients != null && clients.Count > 0)
            {
                param.ClientFound = true;
                param.ClientID = clients[0].clientID;
            }
            //else
            //{
                // Create Client
            var newClient = new NewOneWestClient(param);
            newClient.Show();
            this.Close();
            //}
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            /* cancel and close the form */
            this.Close();
        }


    }
}