﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using DebtPlus.Svc.OCS;
using DebtPlus.OCS.Domain;

namespace DebtPlus.UI.Housing.OCS
{
    public partial class ArchiveForm : DebtPlus.Data.Forms.DebtPlusForm
    {
        protected PROGRAM program { get; set; }

        public ArchiveForm()
        {
            InitializeComponent();
            pictureBox1.Image  = SystemIcons.Warning.ToBitmap();
            purgeDate.DateTime = DateTime.Now.AddMonths(-24);
            program            = PROGRAM.FreddieMacEI;

            RegisterHandlers();
        }

        private void RegisterHandlers()
        {
            purgeMonths.EditValueChanged  += purgeMonths_EditValueChanged;
            purgeMonths.EditValueChanging += purgeMonths_EditValueChanging;
            okBtn.Click                   += okBtn_Click;
            cancelBtn.Click               += cancelBtn_Click;
            Load                          += ArchiveForm_Load;
        }

        private void UnRegisterHandlers()
        {
            purgeMonths.EditValueChanged  -= purgeMonths_EditValueChanged;
            purgeMonths.EditValueChanging -= purgeMonths_EditValueChanging;
            okBtn.Click                   -= okBtn_Click;
            cancelBtn.Click               -= cancelBtn_Click;
            Load                          -= ArchiveForm_Load;
        }

        public ArchiveForm(OCSDbMapper repo) : this()
        {
            purgeDate.DateTime = DateTime.Now.AddMonths(-24);
        }

        private void cancelBtn_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void purgeMonths_EditValueChanged(object sender, EventArgs e)
        {
            purgeDate.DateTime = DateTime.Now.AddMonths(-1*(Convert.ToInt32(purgeMonths.Value)));
        }

        private void okBtn_Click(object sender, EventArgs e)
        {
            var confirmed = DebtPlus.Data.Forms.MessageBox.Show("Are you sure you wish to archive these records?", "OCS - Confirm Archive Database", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation);
            if (confirmed == System.Windows.Forms.DialogResult.Yes)
            {
                using (var bc = new DebtPlus.LINQ.BusinessContext())
                {
                    using (var repository = new OCSDbMapper(bc))
                    {
                        var numberArchived = repository.archiveOlderThan(program, purgeDate.DateTime.Date);
                        DebtPlus.Data.Forms.MessageBox.Show(string.Format("Program: Freddie Mac - EI\r\nRecords Archived: {0:n0}\r\nRecords Archived Prior To: {1:d}",
                                                                numberArchived,
                                                                purgeDate.DateTime),
                                                            "OCS Archive Results - Freddie Mac - EI");
                    }
                }
            }
        }

        private void purgeMonths_EditValueChanging(object sender, DevExpress.XtraEditors.Controls.ChangingEventArgs e)
        {
            decimal val = Convert.ToDecimal(e.NewValue);
            e.Cancel = val < purgeMonths.Properties.MinValue;
            if (e.Cancel)
            {
                purgeMonths.Value     = 0m;
                purgeMonths.EditValue = 0;
            }
        }

        private void ArchiveForm_Load(object sender, EventArgs e)
        {
            this.Text = "Archive - " + ProgramConfigService.getDescription(this.program);
        }
    }
}
