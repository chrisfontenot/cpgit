﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using DebtPlus.OCS.Domain;

namespace DebtPlus.UI.Housing.OCS
{
    public partial class SpecificReasonDialog : Form
    {
        public String selectedReason = String.Empty;

        public SpecificReasonDialog()
        {
            InitializeComponent();
        }

        public SpecificReasonDialog(RESULT_CODE resultCode) : this() {
            this.Text = String.Format("Result Code {0} - Select Reason",(resultCode == RESULT_CODE.ClientNotInterested ? "NI" : "WL"));
            promptVerbiage.Text = String.Format("You entered a result code of \"{0}\"." + Environment.NewLine + "Please select the reason why the client isn't interested.",
                (resultCode == RESULT_CODE.ClientNotInterested ? "NI - Not Interested" : "WL - Working With Lender"));
        }

        private void specificReason_SelectedIndexChanged(object sender, EventArgs e)
        {
            selectedReason = specificReason.SelectedText;
            okBtn.Enabled = true;
        }

        private void okBtn_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void cancelBtn_Click(object sender, EventArgs e)
        {
            selectedReason = String.Empty;
            this.Close();
        }
    }
}
