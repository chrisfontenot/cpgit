﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DebtPlus.OCS.Domain;
using DevExpress.XtraEditors;
using DebtPlus.LINQ;

namespace DebtPlus.UI.Housing.OCS
{
    public static class DropdownHelper
    {
        public static object GetValue(ComboBoxEdit combo)
        {
            if (combo.SelectedIndex == -1)
            {
                return null;
            }
            var item = (DebtPlus.Data.Controls.ComboboxItem)combo.SelectedItem;
            return item.value;
        }

        public static void LoadDropdown(ComboBoxEdit combo, Dictionary<string, object> values)
        {
            combo.Properties.Items.Clear();
            foreach (var kvp in values)
            {
                combo.Properties.Items.Add(new DebtPlus.Data.Controls.ComboboxItem(kvp.Key, kvp.Value, true));
            }
            combo.SelectedIndex = -1;
        }

        public static void selectDropdown(ComboBoxEdit combo, Func<object, bool> compareFunc)
        {
            for (int i = 0, l = combo.Properties.Items.Count; i < l; i++)
            {
                var item = (DebtPlus.Data.Controls.ComboboxItem)combo.Properties.Items[i];
                if (compareFunc(item.value))
                {
                    combo.SelectedIndex = i;
                    return;
                }
            }
            throw new ArgumentOutOfRangeException("Unable to find the correct combobox value using compare function.");
        }

        public static Dictionary<string, object> convertForDD<T>(this Dictionary<string, T> self)
        {
            var toReturn = new Dictionary<string, object>();

            foreach (var kvp in self)
            {
                toReturn.Add(kvp.Key, kvp.Value);
            }

            return toReturn;
        }    
    }
}
