﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DebtPlus.OCS.Domain;

namespace DebtPlus.UI.Housing.OCS.FMAC_720
{
    public class LoadRecords_FMAC720 : DebtPlus.UI.Housing.OCS.LoadRecords
    {
        public LoadRecords_FMAC720() : base() {
            program = PROGRAM.FreddieMac720;
            fieldValidationFunctions = FMACExcelMap.fieldValidationFuncs;
            fieldConvertActions = FMACExcelMap.fieldConvertActions;
        }
    }
}
