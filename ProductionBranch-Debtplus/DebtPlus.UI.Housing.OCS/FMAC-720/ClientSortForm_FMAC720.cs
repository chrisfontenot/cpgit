﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DebtPlus.UI.Housing.OCS.FMAC_720
{
    public class ClientSortForm_FMAC720 : DebtPlus.UI.Housing.OCS.ClientSortLauncher
    {
        public ClientSortForm_FMAC720() : base() {
            program = DebtPlus.OCS.Domain.PROGRAM.FreddieMac720;
        }
    }
}
