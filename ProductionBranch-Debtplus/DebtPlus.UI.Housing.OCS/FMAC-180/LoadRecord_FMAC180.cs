﻿using DebtPlus.OCS.Domain;

namespace DebtPlus.UI.Housing.OCS.FMAC_180
{
    public class LoadRecords_FMAC180 : DebtPlus.UI.Housing.OCS.LoadRecords
    {
        public LoadRecords_FMAC180() : base()
        {
            program = PROGRAM.FreddieMac180;
            fieldValidationFunctions = FMACExcelMap.fieldValidationFuncs;
            fieldConvertActions = FMACExcelMap.fieldConvertActions;            
        }
    }
}
