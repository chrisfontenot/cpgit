﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DebtPlus.UI.Housing.OCS.FMAC_180
{
    public class ArchiveForm_FMAC180 : DebtPlus.UI.Housing.OCS.ArchiveForm
    {
        public ArchiveForm_FMAC180() : base() {
            program = DebtPlus.OCS.Domain.PROGRAM.FreddieMac180;
        }
    }
}
