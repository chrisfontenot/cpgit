﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DebtPlus.Interfaces.Desktop;
using DebtPlus.UI.Housing.OCS;

namespace DebtPlus.UI.Housing.OCS.FMAC_180
{
    public class ClientSortForm_FMAC180 : ClientSortLauncher
    {
        public ClientSortForm_FMAC180() {
            program = DebtPlus.OCS.Domain.PROGRAM.FreddieMac180;            
        }
    }
}
