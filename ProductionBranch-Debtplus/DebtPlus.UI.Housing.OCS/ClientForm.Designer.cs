﻿namespace DebtPlus.UI.Housing.OCS
{
    partial class ClientForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            DevExpress.Utils.SuperToolTip superToolTip2 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipItem toolTipItem2 = new DevExpress.Utils.ToolTipItem();
            this.xtraTabControl1 = new DevExpress.XtraTab.XtraTabControl();
            this.clientTab = new DevExpress.XtraTab.XtraTabPage();
            this.panelControl9 = new DevExpress.XtraEditors.PanelControl();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.labelControl67 = new DevExpress.XtraEditors.LabelControl();
            this.program = new DevExpress.XtraEditors.TextEdit();
            this.timeDifferential = new DevExpress.XtraEditors.TextEdit();
            this.statusCode = new DevExpress.XtraEditors.ComboBoxEdit();
            this.timezone = new DevExpress.XtraEditors.ComboBoxEdit();
            this.queueName = new DevExpress.XtraEditors.TextEdit();
            this.preferredLanguage = new DevExpress.XtraEditors.ComboBoxEdit();
            this.contactAttemptCount = new DevExpress.XtraEditors.TextEdit();
            this.contactType = new DevExpress.XtraEditors.ComboBoxEdit();
            this.labelControl9 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl8 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl7 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl6 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.panelControl2 = new DevExpress.XtraEditors.PanelControl();
            this.aptNumber = new DevExpress.XtraEditors.TextEdit();
            this.aptSuite = new DevExpress.XtraEditors.ComboBoxEdit();
            this.streetSuffix = new DevExpress.XtraEditors.ComboBoxEdit();
            this.streetDirection = new DevExpress.XtraEditors.ComboBoxEdit();
            this.streetNumber = new DevExpress.XtraEditors.TextEdit();
            this.labelControl64 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl65 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl63 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl62 = new DevExpress.XtraEditors.LabelControl();
            this.p1Salutation = new DevExpress.XtraEditors.ComboBoxEdit();
            this.p2Salutation = new DevExpress.XtraEditors.ComboBoxEdit();
            this.p2SSN = new DevExpress.XtraEditors.TextEdit();
            this.p2Email = new DevExpress.XtraEditors.TextEdit();
            this.p1SSN = new DevExpress.XtraEditors.TextEdit();
            this.p1Email = new DevExpress.XtraEditors.TextEdit();
            this.clientZipcode = new DevExpress.XtraEditors.TextEdit();
            this.clientUSState = new DevExpress.XtraEditors.ComboBoxEdit();
            this.clientCity = new DevExpress.XtraEditors.TextEdit();
            this.clientAddress2 = new DevExpress.XtraEditors.TextEdit();
            this.clientAddress1 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl12 = new DevExpress.XtraEditors.LabelControl();
            this.p2Suffix = new DevExpress.XtraEditors.ComboBoxEdit();
            this.p2LastName = new DevExpress.XtraEditors.TextEdit();
            this.p2MiddleName = new DevExpress.XtraEditors.TextEdit();
            this.p2FirstName = new DevExpress.XtraEditors.TextEdit();
            this.p1Suffix = new DevExpress.XtraEditors.ComboBoxEdit();
            this.p1LastName = new DevExpress.XtraEditors.TextEdit();
            this.p1MiddleName = new DevExpress.XtraEditors.TextEdit();
            this.p1FirstName = new DevExpress.XtraEditors.TextEdit();
            this.labelControl11 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl10 = new DevExpress.XtraEditors.LabelControl();
            this.panelControl3 = new DevExpress.XtraEditors.PanelControl();
            this.callBtn4 = new DevExpress.XtraEditors.SimpleButton();
            this.callBtn3 = new DevExpress.XtraEditors.SimpleButton();
            this.callBtn2 = new DevExpress.XtraEditors.SimpleButton();
            this.callBtn5 = new DevExpress.XtraEditors.SimpleButton();
            this.callBtn1 = new DevExpress.XtraEditors.SimpleButton();
            this.ph5Number = new DebtPlus.Data.Controls.PhoneNumber();
            this.barManager1 = new DevExpress.XtraBars.BarManager(this.components);
            this.bar2 = new DevExpress.XtraBars.Bar();
            this.barSubItem_File = new DevExpress.XtraBars.BarSubItem();
            this.barButtonItem2 = new DevExpress.XtraBars.BarButtonItem();
            this.restoreToActiveItem = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem4 = new DevExpress.XtraBars.BarButtonItem();
            this.barSubItem_Appointments = new DevExpress.XtraBars.BarSubItem();
            this.barButtonItem1 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem5 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem6 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem7 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem8 = new DevExpress.XtraBars.BarButtonItem();
            this.barSubItem_Reports = new DevExpress.XtraBars.BarSubItem();
            this.barButtonItem3 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem9 = new DevExpress.XtraBars.BarButtonItem();
            this.barSubItem1 = new DevExpress.XtraBars.BarSubItem();
            this.userTimezone = new DevExpress.XtraBars.BarEditItem();
            this.userTimezoneComboBox = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.barSubItem_Debtplus = new DevExpress.XtraBars.BarSubItem();
            this.barButtonItem_Debtplus = new DevExpress.XtraBars.BarButtonItem();
            this.barSubItem_Help = new DevExpress.XtraBars.BarSubItem();
            this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
            this.barEditItem1 = new DevExpress.XtraBars.BarEditItem();
            this.repositoryItemPopupContainerEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemPopupContainerEdit();
            this.barEditItem2 = new DevExpress.XtraBars.BarEditItem();
            this.repositoryItemTextEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.barEditItem3 = new DevExpress.XtraBars.BarEditItem();
            this.repositoryItemPopupContainerEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemPopupContainerEdit();
            this.barButtonItem10 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem11 = new DevExpress.XtraBars.BarButtonItem();
            this.ph4Number = new DebtPlus.Data.Controls.PhoneNumber();
            this.ph3Number = new DebtPlus.Data.Controls.PhoneNumber();
            this.ph2Number = new DebtPlus.Data.Controls.PhoneNumber();
            this.ph1Number = new DebtPlus.Data.Controls.PhoneNumber();
            this.ph5MTI = new DevExpress.XtraEditors.ComboBoxEdit();
            this.labelControl30 = new DevExpress.XtraEditors.LabelControl();
            this.ph5Type = new DevExpress.XtraEditors.ComboBoxEdit();
            this.labelControl31 = new DevExpress.XtraEditors.LabelControl();
            this.ph5Result = new DevExpress.XtraEditors.ComboBoxEdit();
            this.labelControl32 = new DevExpress.XtraEditors.LabelControl();
            this.ph4MTI = new DevExpress.XtraEditors.ComboBoxEdit();
            this.labelControl27 = new DevExpress.XtraEditors.LabelControl();
            this.ph4Type = new DevExpress.XtraEditors.ComboBoxEdit();
            this.labelControl28 = new DevExpress.XtraEditors.LabelControl();
            this.ph4Result = new DevExpress.XtraEditors.ComboBoxEdit();
            this.labelControl29 = new DevExpress.XtraEditors.LabelControl();
            this.ph3MTI = new DevExpress.XtraEditors.ComboBoxEdit();
            this.labelControl24 = new DevExpress.XtraEditors.LabelControl();
            this.ph3Type = new DevExpress.XtraEditors.ComboBoxEdit();
            this.labelControl25 = new DevExpress.XtraEditors.LabelControl();
            this.ph3Result = new DevExpress.XtraEditors.ComboBoxEdit();
            this.labelControl26 = new DevExpress.XtraEditors.LabelControl();
            this.ph2MTI = new DevExpress.XtraEditors.ComboBoxEdit();
            this.labelControl21 = new DevExpress.XtraEditors.LabelControl();
            this.ph2Type = new DevExpress.XtraEditors.ComboBoxEdit();
            this.labelControl22 = new DevExpress.XtraEditors.LabelControl();
            this.ph2Result = new DevExpress.XtraEditors.ComboBoxEdit();
            this.labelControl23 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl20 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl19 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl18 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl17 = new DevExpress.XtraEditors.LabelControl();
            this.ph1MTI = new DevExpress.XtraEditors.ComboBoxEdit();
            this.labelControl16 = new DevExpress.XtraEditors.LabelControl();
            this.ph1Type = new DevExpress.XtraEditors.ComboBoxEdit();
            this.labelControl15 = new DevExpress.XtraEditors.LabelControl();
            this.ph1Result = new DevExpress.XtraEditors.ComboBoxEdit();
            this.labelControl14 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl13 = new DevExpress.XtraEditors.LabelControl();
            this.panelControl4 = new DevExpress.XtraEditors.PanelControl();
            this.stage4 = new DevExpress.XtraEditors.ComboBoxEdit();
            this.labelControl37 = new DevExpress.XtraEditors.LabelControl();
            this.stage3 = new DevExpress.XtraEditors.ComboBoxEdit();
            this.labelControl36 = new DevExpress.XtraEditors.LabelControl();
            this.stage2 = new DevExpress.XtraEditors.ComboBoxEdit();
            this.stage1 = new DevExpress.XtraEditors.ComboBoxEdit();
            this.labelControl35 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl34 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl33 = new DevExpress.XtraEditors.LabelControl();
            this.accountTab = new DevExpress.XtraTab.XtraTabPage();
            this.panelControl8 = new DevExpress.XtraEditors.PanelControl();
            this.panelControl7 = new DevExpress.XtraEditors.PanelControl();
            this.useHomeAddress = new DevExpress.XtraEditors.CheckEdit();
            this.labelControl59 = new DevExpress.XtraEditors.LabelControl();
            this.propertyZipcode = new DevExpress.XtraEditors.TextEdit();
            this.propertyUSState = new DevExpress.XtraEditors.ComboBoxEdit();
            this.propertyCity = new DevExpress.XtraEditors.TextEdit();
            this.propertyAddress2 = new DevExpress.XtraEditors.TextEdit();
            this.propertyAddress1 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl58 = new DevExpress.XtraEditors.LabelControl();
            this.panelControl5 = new DevExpress.XtraEditors.PanelControl();
            this.labelControl68 = new DevExpress.XtraEditors.LabelControl();
            this.actualSaleDate = new DevExpress.XtraEditors.DateEdit();
            this.servicerIDNo = new DevExpress.XtraEditors.TextEdit();
            this.labelControl66 = new DevExpress.XtraEditors.LabelControl();
            this.txt_TrialMod_Type = new DevExpress.XtraEditors.TextEdit();
            this.monthsPastDue = new DevExpress.XtraEditors.TextEdit();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl42 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl49 = new DevExpress.XtraEditors.LabelControl();
            this.investorNo = new DevExpress.XtraEditors.TextEdit();
            this.loanModDate = new DevExpress.XtraEditors.DateEdit();
            this.foreclosureDate = new DevExpress.XtraEditors.TextEdit();
            this.labelControl48 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl39 = new DevExpress.XtraEditors.LabelControl();
            this.daysPastDue = new DevExpress.XtraEditors.TextEdit();
            this.defaultReason = new DevExpress.XtraEditors.TextEdit();
            this.labelControl47 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl40 = new DevExpress.XtraEditors.LabelControl();
            this.servicerLoanNo = new DevExpress.XtraEditors.TextEdit();
            this.labelControl41 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl46 = new DevExpress.XtraEditors.LabelControl();
            this.pmtAppliedDate = new DevExpress.XtraEditors.DateEdit();
            this.nextPmtDate = new DevExpress.XtraEditors.DateEdit();
            this.servicer = new DevExpress.XtraEditors.TextEdit();
            this.labelControl45 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl43 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl44 = new DevExpress.XtraEditors.LabelControl();
            this.pastDueDate = new DevExpress.XtraEditors.DateEdit();
            this.trialMod = new DevExpress.XtraEditors.ComboBoxEdit();
            this.panelControl6 = new DevExpress.XtraEditors.PanelControl();
            this.txtLastChanceFlag = new DevExpress.XtraEditors.TextEdit();
            this.labelControl54 = new DevExpress.XtraEditors.LabelControl();
            this.lastChanceDate = new DevExpress.XtraEditors.DateEdit();
            this.labelControl38 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl69 = new DevExpress.XtraEditors.LabelControl();
            this.secondCounselDate = new DevExpress.XtraEditors.DateEdit();
            this.duplicateFile = new DevExpress.XtraEditors.CheckEdit();
            this.labelControl56 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl51 = new DevExpress.XtraEditors.LabelControl();
            this.firstRPContact = new DevExpress.XtraEditors.ComboBoxEdit();
            this.labelControl50 = new DevExpress.XtraEditors.LabelControl();
            this.dateFileReceived = new DevExpress.XtraEditors.DateEdit();
            this.agenyCase = new DevExpress.XtraEditors.TextEdit();
            this.labelControl52 = new DevExpress.XtraEditors.LabelControl();
            this.dateClosed = new DevExpress.XtraEditors.DateEdit();
            this.labelControl53 = new DevExpress.XtraEditors.LabelControl();
            this.batchName = new DevExpress.XtraEditors.TextEdit();
            this.labelControl55 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl57 = new DevExpress.XtraEditors.LabelControl();
            this.firstCounselDate = new DevExpress.XtraEditors.DateEdit();
            this.labelControl60 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl61 = new DevExpress.XtraEditors.LabelControl();
            this.dateFileModified = new DevExpress.XtraEditors.DateEdit();
            this.dt_ReferralDate = new DevExpress.XtraEditors.DateEdit();
            this.notesTab = new DevExpress.XtraTab.XtraTabPage();
            this.panelControl10 = new DevExpress.XtraEditors.PanelControl();
            this.showArchivedNotes = new DevExpress.XtraEditors.CheckEdit();
            this.showTemporaryNotes = new DevExpress.XtraEditors.CheckEdit();
            this.showAlertNotes = new DevExpress.XtraEditors.CheckEdit();
            this.showPermanentNotes = new DevExpress.XtraEditors.CheckEdit();
            this.showSystemNotes = new DevExpress.XtraEditors.CheckEdit();
            this.notesGrid = new DevExpress.XtraGrid.GridControl();
            this.notesContextMenu = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.submitBtn = new DevExpress.XtraEditors.SimpleButton();
            this.bar3 = new DevExpress.XtraBars.Bar();
            this.barManager2 = new DevExpress.XtraBars.BarManager(this.components);
            this.bar5 = new DevExpress.XtraBars.Bar();
            this.debtPlusIdItem = new DevExpress.XtraBars.BarEditItem();
            this.repositoryItemTextEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.clientNameItem = new DevExpress.XtraBars.BarStaticItem();
            this.barDockControl1 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl2 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl3 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl4 = new DevExpress.XtraBars.BarDockControl();
            this.forwardBtn = new DevExpress.XtraEditors.SimpleButton();
            this.backBtn = new DevExpress.XtraEditors.SimpleButton();
            this.bufferStatus = new DevExpress.XtraEditors.TextEdit();
            this.toBeginBtn = new DevExpress.XtraEditors.SimpleButton();
            this.toEndBtn = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl1)).BeginInit();
            this.xtraTabControl1.SuspendLayout();
            this.clientTab.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl9)).BeginInit();
            this.panelControl9.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.program.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.timeDifferential.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.statusCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.timezone.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.queueName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.preferredLanguage.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.contactAttemptCount.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.contactType.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).BeginInit();
            this.panelControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.aptNumber.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.aptSuite.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.streetSuffix.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.streetDirection.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.streetNumber.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.p1Salutation.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.p2Salutation.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.p2SSN.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.p2Email.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.p1SSN.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.p1Email.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.clientZipcode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.clientUSState.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.clientCity.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.clientAddress2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.clientAddress1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.p2Suffix.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.p2LastName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.p2MiddleName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.p2FirstName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.p1Suffix.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.p1LastName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.p1MiddleName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.p1FirstName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl3)).BeginInit();
            this.panelControl3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ph5Number.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.userTimezoneComboBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPopupContainerEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPopupContainerEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ph4Number.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ph3Number.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ph2Number.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ph1Number.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ph5MTI.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ph5Type.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ph5Result.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ph4MTI.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ph4Type.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ph4Result.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ph3MTI.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ph3Type.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ph3Result.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ph2MTI.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ph2Type.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ph2Result.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ph1MTI.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ph1Type.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ph1Result.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl4)).BeginInit();
            this.panelControl4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.stage4.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.stage3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.stage2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.stage1.Properties)).BeginInit();
            this.accountTab.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl8)).BeginInit();
            this.panelControl8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl7)).BeginInit();
            this.panelControl7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.useHomeAddress.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.propertyZipcode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.propertyUSState.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.propertyCity.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.propertyAddress2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.propertyAddress1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl5)).BeginInit();
            this.panelControl5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.actualSaleDate.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.actualSaleDate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.servicerIDNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt_TrialMod_Type.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.monthsPastDue.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.investorNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.loanModDate.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.loanModDate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.foreclosureDate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.daysPastDue.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.defaultReason.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.servicerLoanNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmtAppliedDate.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmtAppliedDate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nextPmtDate.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nextPmtDate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.servicer.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pastDueDate.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pastDueDate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trialMod.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl6)).BeginInit();
            this.panelControl6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtLastChanceFlag.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lastChanceDate.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lastChanceDate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.secondCounselDate.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.secondCounselDate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.duplicateFile.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.firstRPContact.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateFileReceived.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateFileReceived.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.agenyCase.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateClosed.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateClosed.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.batchName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.firstCounselDate.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.firstCounselDate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateFileModified.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateFileModified.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dt_ReferralDate.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dt_ReferralDate.Properties)).BeginInit();
            this.notesTab.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl10)).BeginInit();
            this.panelControl10.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.showArchivedNotes.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.showTemporaryNotes.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.showAlertNotes.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.showPermanentNotes.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.showSystemNotes.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.notesGrid)).BeginInit();
            this.notesContextMenu.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bufferStatus.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // xtraTabControl1
            // 
            this.xtraTabControl1.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.xtraTabControl1.Appearance.Options.UseBackColor = true;
            this.xtraTabControl1.Location = new System.Drawing.Point(12, 32);
            this.xtraTabControl1.Name = "xtraTabControl1";
            this.xtraTabControl1.SelectedTabPage = this.clientTab;
            this.xtraTabControl1.Size = new System.Drawing.Size(895, 581);
            this.xtraTabControl1.TabIndex = 0;
            this.xtraTabControl1.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.clientTab,
            this.accountTab,
            this.notesTab});
            this.xtraTabControl1.SelectedPageChanging += new DevExpress.XtraTab.TabPageChangingEventHandler(this.xtraTabControl1_SelectedPageChanging);
            // 
            // clientTab
            // 
            this.clientTab.Appearance.PageClient.BackColor = System.Drawing.Color.Turquoise;
            this.clientTab.Appearance.PageClient.Options.UseBackColor = true;
            this.clientTab.Controls.Add(this.panelControl9);
            this.clientTab.Name = "clientTab";
            this.clientTab.Size = new System.Drawing.Size(887, 551);
            this.clientTab.Text = "Client";
            // 
            // panelControl9
            // 
            this.panelControl9.Controls.Add(this.panelControl1);
            this.panelControl9.Controls.Add(this.panelControl2);
            this.panelControl9.Controls.Add(this.panelControl3);
            this.panelControl9.Controls.Add(this.panelControl4);
            this.panelControl9.Location = new System.Drawing.Point(-1, 0);
            this.panelControl9.Name = "panelControl9";
            this.panelControl9.Size = new System.Drawing.Size(890, 553);
            this.panelControl9.TabIndex = 5;
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.labelControl67);
            this.panelControl1.Controls.Add(this.program);
            this.panelControl1.Controls.Add(this.timeDifferential);
            this.panelControl1.Controls.Add(this.statusCode);
            this.panelControl1.Controls.Add(this.timezone);
            this.panelControl1.Controls.Add(this.queueName);
            this.panelControl1.Controls.Add(this.preferredLanguage);
            this.panelControl1.Controls.Add(this.contactAttemptCount);
            this.panelControl1.Controls.Add(this.contactType);
            this.panelControl1.Controls.Add(this.labelControl9);
            this.panelControl1.Controls.Add(this.labelControl8);
            this.panelControl1.Controls.Add(this.labelControl7);
            this.panelControl1.Controls.Add(this.labelControl6);
            this.panelControl1.Controls.Add(this.labelControl5);
            this.panelControl1.Controls.Add(this.labelControl4);
            this.panelControl1.Controls.Add(this.labelControl1);
            this.panelControl1.Location = new System.Drawing.Point(0, 0);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(890, 76);
            this.panelControl1.TabIndex = 0;
            // 
            // labelControl67
            // 
            this.labelControl67.Location = new System.Drawing.Point(547, 35);
            this.labelControl67.Name = "labelControl67";
            this.labelControl67.Size = new System.Drawing.Size(40, 13);
            this.labelControl67.TabIndex = 15;
            this.labelControl67.Text = "Program";
            // 
            // program
            // 
            this.program.EditValue = "";
            this.program.Location = new System.Drawing.Point(595, 32);
            this.program.Name = "program";
            this.program.Properties.ReadOnly = true;
            this.program.Size = new System.Drawing.Size(110, 20);
            this.program.TabIndex = 14;
            // 
            // timeDifferential
            // 
            this.timeDifferential.EditValue = "+3 Hr";
            this.timeDifferential.Location = new System.Drawing.Point(595, 5);
            this.timeDifferential.Name = "timeDifferential";
            this.timeDifferential.Properties.ReadOnly = true;
            this.timeDifferential.Size = new System.Drawing.Size(66, 20);
            this.timeDifferential.TabIndex = 13;
            // 
            // statusCode
            // 
            this.statusCode.Enabled = false;
            this.statusCode.Location = new System.Drawing.Point(441, 32);
            this.statusCode.Name = "statusCode";
            this.statusCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.statusCode.Properties.Items.AddRange(new object[] {
            "NC - No Contact",
            "CO - Counseled"});
            this.statusCode.Properties.ReadOnly = true;
            this.statusCode.Size = new System.Drawing.Size(100, 20);
            this.statusCode.TabIndex = 12;
            this.statusCode.ToolTip = "OCS_Client.StatusCode";
            // 
            // timezone
            // 
            this.timezone.Location = new System.Drawing.Point(441, 5);
            this.timezone.Name = "timezone";
            this.timezone.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.timezone.Properties.Items.AddRange(new object[] {
            "Eastern",
            "Central",
            "Mountain",
            "Pacific",
            "Alaska",
            "Hawaii"});
            this.timezone.Properties.ReadOnly = true;
            this.timezone.Size = new System.Drawing.Size(100, 20);
            this.timezone.TabIndex = 11;
            this.timezone.ToolTip = "ZipCodeSearches.TimeZone";
            // 
            // queueName
            // 
            this.queueName.Location = new System.Drawing.Point(270, 5);
            this.queueName.Name = "queueName";
            this.queueName.Properties.ReadOnly = true;
            this.queueName.Size = new System.Drawing.Size(100, 20);
            this.queueName.TabIndex = 10;
            this.queueName.ToolTip = "OCS_Client.ActiveFlag";
            // 
            // preferredLanguage
            // 
            this.preferredLanguage.EditValue = "";
            this.preferredLanguage.Location = new System.Drawing.Point(270, 32);
            this.preferredLanguage.Name = "preferredLanguage";
            this.preferredLanguage.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.preferredLanguage.Properties.Items.AddRange(new object[] {
            "English",
            "Spanish"});
            this.preferredLanguage.Size = new System.Drawing.Size(100, 20);
            this.preferredLanguage.TabIndex = 9;
            this.preferredLanguage.ToolTip = "client.language";
            // 
            // contactAttemptCount
            // 
            this.contactAttemptCount.EditValue = "4";
            this.contactAttemptCount.Location = new System.Drawing.Point(100, 32);
            this.contactAttemptCount.Name = "contactAttemptCount";
            this.contactAttemptCount.Properties.ReadOnly = true;
            this.contactAttemptCount.Size = new System.Drawing.Size(100, 20);
            this.contactAttemptCount.TabIndex = 8;
            this.contactAttemptCount.ToolTip = "OCS_Client.ContactAttempts";
            // 
            // contactType
            // 
            this.contactType.EditValue = "OB - Outbound";
            this.contactType.Location = new System.Drawing.Point(100, 5);
            this.contactType.Name = "contactType";
            this.contactType.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.contactType.Properties.Items.AddRange(new object[] {
            "OB - Outbound",
            "IB - Inbound"});
            this.contactType.Size = new System.Drawing.Size(100, 20);
            this.contactType.TabIndex = 7;
            this.contactType.ToolTip = "OCS_ContactAttempt.ContactType";
            // 
            // labelControl9
            // 
            this.labelControl9.Location = new System.Drawing.Point(547, 8);
            this.labelControl9.Name = "labelControl9";
            this.labelControl9.Size = new System.Drawing.Size(42, 13);
            this.labelControl9.TabIndex = 6;
            this.labelControl9.Text = "Time Diff";
            // 
            // labelControl8
            // 
            this.labelControl8.Location = new System.Drawing.Point(386, 35);
            this.labelControl8.Name = "labelControl8";
            this.labelControl8.Size = new System.Drawing.Size(55, 13);
            this.labelControl8.TabIndex = 5;
            this.labelControl8.Text = "OCS Status";
            // 
            // labelControl7
            // 
            this.labelControl7.Location = new System.Drawing.Point(386, 8);
            this.labelControl7.Name = "labelControl7";
            this.labelControl7.Size = new System.Drawing.Size(49, 13);
            this.labelControl7.TabIndex = 4;
            this.labelControl7.Text = "Time Zone";
            // 
            // labelControl6
            // 
            this.labelControl6.Location = new System.Drawing.Point(217, 35);
            this.labelControl6.Name = "labelControl6";
            this.labelControl6.Size = new System.Drawing.Size(47, 13);
            this.labelControl6.TabIndex = 3;
            this.labelControl6.Text = "Language";
            // 
            // labelControl5
            // 
            this.labelControl5.Location = new System.Drawing.Point(217, 8);
            this.labelControl5.Name = "labelControl5";
            this.labelControl5.Size = new System.Drawing.Size(53, 13);
            this.labelControl5.TabIndex = 2;
            this.labelControl5.Text = "Active Flag";
            // 
            // labelControl4
            // 
            this.labelControl4.Location = new System.Drawing.Point(5, 35);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(85, 13);
            this.labelControl4.TabIndex = 1;
            this.labelControl4.Text = "Contact Attempts";
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(5, 8);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(44, 13);
            this.labelControl1.TabIndex = 0;
            this.labelControl1.Text = "Call Type";
            // 
            // panelControl2
            // 
            this.panelControl2.Controls.Add(this.aptNumber);
            this.panelControl2.Controls.Add(this.aptSuite);
            this.panelControl2.Controls.Add(this.streetSuffix);
            this.panelControl2.Controls.Add(this.streetDirection);
            this.panelControl2.Controls.Add(this.streetNumber);
            this.panelControl2.Controls.Add(this.labelControl64);
            this.panelControl2.Controls.Add(this.labelControl65);
            this.panelControl2.Controls.Add(this.labelControl63);
            this.panelControl2.Controls.Add(this.labelControl62);
            this.panelControl2.Controls.Add(this.p1Salutation);
            this.panelControl2.Controls.Add(this.p2Salutation);
            this.panelControl2.Controls.Add(this.p2SSN);
            this.panelControl2.Controls.Add(this.p2Email);
            this.panelControl2.Controls.Add(this.p1SSN);
            this.panelControl2.Controls.Add(this.p1Email);
            this.panelControl2.Controls.Add(this.clientZipcode);
            this.panelControl2.Controls.Add(this.clientUSState);
            this.panelControl2.Controls.Add(this.clientCity);
            this.panelControl2.Controls.Add(this.clientAddress2);
            this.panelControl2.Controls.Add(this.clientAddress1);
            this.panelControl2.Controls.Add(this.labelControl12);
            this.panelControl2.Controls.Add(this.p2Suffix);
            this.panelControl2.Controls.Add(this.p2LastName);
            this.panelControl2.Controls.Add(this.p2MiddleName);
            this.panelControl2.Controls.Add(this.p2FirstName);
            this.panelControl2.Controls.Add(this.p1Suffix);
            this.panelControl2.Controls.Add(this.p1LastName);
            this.panelControl2.Controls.Add(this.p1MiddleName);
            this.panelControl2.Controls.Add(this.p1FirstName);
            this.panelControl2.Controls.Add(this.labelControl11);
            this.panelControl2.Controls.Add(this.labelControl10);
            this.panelControl2.Location = new System.Drawing.Point(1, 73);
            this.panelControl2.Name = "panelControl2";
            this.panelControl2.Size = new System.Drawing.Size(889, 192);
            this.panelControl2.TabIndex = 1;
            // 
            // aptNumber
            // 
            this.aptNumber.Location = new System.Drawing.Point(631, 61);
            this.aptNumber.Name = "aptNumber";
            this.aptNumber.Properties.ReadOnly = true;
            this.aptNumber.Size = new System.Drawing.Size(46, 20);
            this.aptNumber.TabIndex = 30;
            this.aptNumber.ToolTip = "Name.Middle";
            // 
            // aptSuite
            // 
            this.aptSuite.Location = new System.Drawing.Point(572, 61);
            this.aptSuite.Name = "aptSuite";
            this.aptSuite.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.aptSuite.Properties.Items.AddRange(new object[] {
            "Jr.",
            "Sr."});
            this.aptSuite.Properties.ReadOnly = true;
            this.aptSuite.Size = new System.Drawing.Size(53, 20);
            this.aptSuite.TabIndex = 29;
            this.aptSuite.ToolTip = "Name.Suffix";
            // 
            // streetSuffix
            // 
            this.streetSuffix.Location = new System.Drawing.Point(513, 61);
            this.streetSuffix.Name = "streetSuffix";
            this.streetSuffix.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.streetSuffix.Properties.Items.AddRange(new object[] {
            "Jr.",
            "Sr."});
            this.streetSuffix.Properties.ReadOnly = true;
            this.streetSuffix.Size = new System.Drawing.Size(53, 20);
            this.streetSuffix.TabIndex = 28;
            this.streetSuffix.ToolTip = "Name.Suffix";
            // 
            // streetDirection
            // 
            this.streetDirection.Location = new System.Drawing.Point(152, 61);
            this.streetDirection.Name = "streetDirection";
            this.streetDirection.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.streetDirection.Properties.Items.AddRange(new object[] {
            "Jr.",
            "Sr."});
            this.streetDirection.Properties.ReadOnly = true;
            this.streetDirection.Size = new System.Drawing.Size(47, 20);
            this.streetDirection.TabIndex = 27;
            this.streetDirection.ToolTip = "Name.Suffix";
            // 
            // streetNumber
            // 
            this.streetNumber.Location = new System.Drawing.Point(100, 61);
            this.streetNumber.Name = "streetNumber";
            this.streetNumber.Properties.ReadOnly = true;
            this.streetNumber.Size = new System.Drawing.Size(46, 20);
            this.streetNumber.TabIndex = 26;
            this.streetNumber.ToolTip = "Name.Middle";
            // 
            // labelControl64
            // 
            this.labelControl64.Location = new System.Drawing.Point(267, 168);
            this.labelControl64.Name = "labelControl64";
            this.labelControl64.Size = new System.Drawing.Size(79, 13);
            this.labelControl64.TabIndex = 25;
            this.labelControl64.Text = "CoApplicant SSN";
            // 
            // labelControl65
            // 
            this.labelControl65.Location = new System.Drawing.Point(5, 168);
            this.labelControl65.Name = "labelControl65";
            this.labelControl65.Size = new System.Drawing.Size(84, 13);
            this.labelControl65.TabIndex = 24;
            this.labelControl65.Text = "CoApplicant Email";
            // 
            // labelControl63
            // 
            this.labelControl63.Location = new System.Drawing.Point(267, 142);
            this.labelControl63.Name = "labelControl63";
            this.labelControl63.Size = new System.Drawing.Size(66, 13);
            this.labelControl63.TabIndex = 23;
            this.labelControl63.Text = "Applicant SSN";
            // 
            // labelControl62
            // 
            this.labelControl62.Location = new System.Drawing.Point(5, 142);
            this.labelControl62.Name = "labelControl62";
            this.labelControl62.Size = new System.Drawing.Size(71, 13);
            this.labelControl62.TabIndex = 22;
            this.labelControl62.Text = "Applicant Email";
            // 
            // p1Salutation
            // 
            this.p1Salutation.Location = new System.Drawing.Point(99, 6);
            this.p1Salutation.Name = "p1Salutation";
            this.p1Salutation.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.p1Salutation.Properties.Items.AddRange(new object[] {
            "Mr.",
            "Mrs."});
            this.p1Salutation.Size = new System.Drawing.Size(66, 20);
            this.p1Salutation.TabIndex = 21;
            this.p1Salutation.ToolTip = "Name.Prefix";
            // 
            // p2Salutation
            // 
            this.p2Salutation.Location = new System.Drawing.Point(99, 31);
            this.p2Salutation.Name = "p2Salutation";
            this.p2Salutation.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.p2Salutation.Properties.Items.AddRange(new object[] {
            "Mr.",
            "Mrs."});
            this.p2Salutation.Size = new System.Drawing.Size(66, 20);
            this.p2Salutation.TabIndex = 20;
            this.p2Salutation.ToolTip = "Name.Prefix";
            // 
            // p2SSN
            // 
            this.p2SSN.Location = new System.Drawing.Point(356, 165);
            this.p2SSN.Name = "p2SSN";
            this.p2SSN.Size = new System.Drawing.Size(70, 20);
            this.p2SSN.TabIndex = 19;
            this.p2SSN.ToolTip = "Name.SSN";
            // 
            // p2Email
            // 
            this.p2Email.Location = new System.Drawing.Point(99, 165);
            this.p2Email.Name = "p2Email";
            this.p2Email.Size = new System.Drawing.Size(144, 20);
            this.p2Email.TabIndex = 18;
            this.p2Email.ToolTip = "EmailAddresses.Address";
            // 
            // p1SSN
            // 
            this.p1SSN.Location = new System.Drawing.Point(356, 139);
            this.p1SSN.Name = "p1SSN";
            this.p1SSN.Size = new System.Drawing.Size(70, 20);
            this.p1SSN.TabIndex = 17;
            this.p1SSN.ToolTip = "Name.SSN";
            // 
            // p1Email
            // 
            this.p1Email.Location = new System.Drawing.Point(100, 139);
            this.p1Email.Name = "p1Email";
            this.p1Email.Size = new System.Drawing.Size(144, 20);
            this.p1Email.TabIndex = 16;
            this.p1Email.ToolTip = "EmailAddresses.Address";
            // 
            // clientZipcode
            // 
            this.clientZipcode.Location = new System.Drawing.Point(576, 113);
            this.clientZipcode.Name = "clientZipcode";
            this.clientZipcode.Properties.ReadOnly = true;
            this.clientZipcode.Size = new System.Drawing.Size(100, 20);
            this.clientZipcode.TabIndex = 15;
            this.clientZipcode.ToolTip = "addresses.PostalCode";
            // 
            // clientUSState
            // 
            this.clientUSState.Location = new System.Drawing.Point(470, 113);
            this.clientUSState.Name = "clientUSState";
            this.clientUSState.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.clientUSState.Properties.Items.AddRange(new object[] {
            "AL",
            "AK",
            "AZ"});
            this.clientUSState.Properties.ReadOnly = true;
            this.clientUSState.Size = new System.Drawing.Size(100, 20);
            this.clientUSState.TabIndex = 14;
            this.clientUSState.ToolTip = "addresses.state";
            // 
            // clientCity
            // 
            this.clientCity.Location = new System.Drawing.Point(100, 113);
            this.clientCity.Name = "clientCity";
            this.clientCity.Properties.ReadOnly = true;
            this.clientCity.Size = new System.Drawing.Size(364, 20);
            this.clientCity.TabIndex = 13;
            this.clientCity.ToolTip = "addresses.city";
            // 
            // clientAddress2
            // 
            this.clientAddress2.Location = new System.Drawing.Point(100, 87);
            this.clientAddress2.Name = "clientAddress2";
            this.clientAddress2.Properties.ReadOnly = true;
            this.clientAddress2.Size = new System.Drawing.Size(576, 20);
            this.clientAddress2.TabIndex = 12;
            // 
            // clientAddress1
            // 
            this.clientAddress1.Location = new System.Drawing.Point(205, 61);
            this.clientAddress1.Name = "clientAddress1";
            this.clientAddress1.Properties.ReadOnly = true;
            this.clientAddress1.Size = new System.Drawing.Size(302, 20);
            this.clientAddress1.TabIndex = 11;
            this.clientAddress1.ToolTip = "addresses.street";
            // 
            // labelControl12
            // 
            this.labelControl12.Location = new System.Drawing.Point(6, 64);
            this.labelControl12.Name = "labelControl12";
            this.labelControl12.Size = new System.Drawing.Size(39, 13);
            this.labelControl12.TabIndex = 10;
            this.labelControl12.Text = "Address";
            // 
            // p2Suffix
            // 
            this.p2Suffix.Location = new System.Drawing.Point(610, 31);
            this.p2Suffix.Name = "p2Suffix";
            this.p2Suffix.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.p2Suffix.Properties.Items.AddRange(new object[] {
            "Jr.",
            "Sr."});
            this.p2Suffix.Size = new System.Drawing.Size(66, 20);
            this.p2Suffix.TabIndex = 9;
            this.p2Suffix.ToolTip = "Name.Suffix";
            // 
            // p2LastName
            // 
            this.p2LastName.Location = new System.Drawing.Point(417, 31);
            this.p2LastName.Name = "p2LastName";
            this.p2LastName.Properties.ReadOnly = true;
            this.p2LastName.Size = new System.Drawing.Size(187, 20);
            this.p2LastName.TabIndex = 8;
            this.p2LastName.ToolTip = "Name.Last";
            // 
            // p2MiddleName
            // 
            this.p2MiddleName.Location = new System.Drawing.Point(345, 31);
            this.p2MiddleName.Name = "p2MiddleName";
            this.p2MiddleName.Properties.ReadOnly = true;
            this.p2MiddleName.Size = new System.Drawing.Size(66, 20);
            this.p2MiddleName.TabIndex = 7;
            this.p2MiddleName.ToolTip = "Name.Middle";
            // 
            // p2FirstName
            // 
            this.p2FirstName.Location = new System.Drawing.Point(177, 31);
            this.p2FirstName.Name = "p2FirstName";
            this.p2FirstName.Properties.ReadOnly = true;
            this.p2FirstName.Size = new System.Drawing.Size(162, 20);
            this.p2FirstName.TabIndex = 6;
            this.p2FirstName.ToolTip = "Name.First";
            // 
            // p1Suffix
            // 
            this.p1Suffix.Location = new System.Drawing.Point(610, 6);
            this.p1Suffix.Name = "p1Suffix";
            this.p1Suffix.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.p1Suffix.Properties.Items.AddRange(new object[] {
            "Jr.",
            "Sr."});
            this.p1Suffix.Size = new System.Drawing.Size(66, 20);
            this.p1Suffix.TabIndex = 5;
            this.p1Suffix.ToolTip = "Name.Suffix";
            // 
            // p1LastName
            // 
            this.p1LastName.Location = new System.Drawing.Point(417, 6);
            this.p1LastName.Name = "p1LastName";
            this.p1LastName.Properties.ReadOnly = true;
            this.p1LastName.Size = new System.Drawing.Size(187, 20);
            this.p1LastName.TabIndex = 4;
            this.p1LastName.ToolTip = "Name.Last";
            // 
            // p1MiddleName
            // 
            this.p1MiddleName.Location = new System.Drawing.Point(345, 6);
            this.p1MiddleName.Name = "p1MiddleName";
            this.p1MiddleName.Properties.ReadOnly = true;
            this.p1MiddleName.Size = new System.Drawing.Size(66, 20);
            this.p1MiddleName.TabIndex = 3;
            this.p1MiddleName.ToolTip = "Name.Middle";
            // 
            // p1FirstName
            // 
            this.p1FirstName.Location = new System.Drawing.Point(177, 6);
            this.p1FirstName.Name = "p1FirstName";
            this.p1FirstName.Properties.ReadOnly = true;
            this.p1FirstName.Size = new System.Drawing.Size(162, 20);
            this.p1FirstName.TabIndex = 2;
            this.p1FirstName.ToolTip = "Name.First";
            // 
            // labelControl11
            // 
            this.labelControl11.Location = new System.Drawing.Point(6, 34);
            this.labelControl11.Name = "labelControl11";
            this.labelControl11.Size = new System.Drawing.Size(57, 13);
            this.labelControl11.TabIndex = 1;
            this.labelControl11.Text = "CoApplicant";
            // 
            // labelControl10
            // 
            this.labelControl10.Location = new System.Drawing.Point(6, 9);
            this.labelControl10.Name = "labelControl10";
            this.labelControl10.Size = new System.Drawing.Size(44, 13);
            this.labelControl10.TabIndex = 0;
            this.labelControl10.Text = "Applicant";
            // 
            // panelControl3
            // 
            this.panelControl3.Controls.Add(this.callBtn4);
            this.panelControl3.Controls.Add(this.callBtn3);
            this.panelControl3.Controls.Add(this.callBtn2);
            this.panelControl3.Controls.Add(this.callBtn5);
            this.panelControl3.Controls.Add(this.callBtn1);
            this.panelControl3.Controls.Add(this.ph5Number);
            this.panelControl3.Controls.Add(this.ph4Number);
            this.panelControl3.Controls.Add(this.ph3Number);
            this.panelControl3.Controls.Add(this.ph2Number);
            this.panelControl3.Controls.Add(this.ph1Number);
            this.panelControl3.Controls.Add(this.ph5MTI);
            this.panelControl3.Controls.Add(this.labelControl30);
            this.panelControl3.Controls.Add(this.ph5Type);
            this.panelControl3.Controls.Add(this.labelControl31);
            this.panelControl3.Controls.Add(this.ph5Result);
            this.panelControl3.Controls.Add(this.labelControl32);
            this.panelControl3.Controls.Add(this.ph4MTI);
            this.panelControl3.Controls.Add(this.labelControl27);
            this.panelControl3.Controls.Add(this.ph4Type);
            this.panelControl3.Controls.Add(this.labelControl28);
            this.panelControl3.Controls.Add(this.ph4Result);
            this.panelControl3.Controls.Add(this.labelControl29);
            this.panelControl3.Controls.Add(this.ph3MTI);
            this.panelControl3.Controls.Add(this.labelControl24);
            this.panelControl3.Controls.Add(this.ph3Type);
            this.panelControl3.Controls.Add(this.labelControl25);
            this.panelControl3.Controls.Add(this.ph3Result);
            this.panelControl3.Controls.Add(this.labelControl26);
            this.panelControl3.Controls.Add(this.ph2MTI);
            this.panelControl3.Controls.Add(this.labelControl21);
            this.panelControl3.Controls.Add(this.ph2Type);
            this.panelControl3.Controls.Add(this.labelControl22);
            this.panelControl3.Controls.Add(this.ph2Result);
            this.panelControl3.Controls.Add(this.labelControl23);
            this.panelControl3.Controls.Add(this.labelControl20);
            this.panelControl3.Controls.Add(this.labelControl19);
            this.panelControl3.Controls.Add(this.labelControl18);
            this.panelControl3.Controls.Add(this.labelControl17);
            this.panelControl3.Controls.Add(this.ph1MTI);
            this.panelControl3.Controls.Add(this.labelControl16);
            this.panelControl3.Controls.Add(this.ph1Type);
            this.panelControl3.Controls.Add(this.labelControl15);
            this.panelControl3.Controls.Add(this.ph1Result);
            this.panelControl3.Controls.Add(this.labelControl14);
            this.panelControl3.Controls.Add(this.labelControl13);
            this.panelControl3.Location = new System.Drawing.Point(0, 264);
            this.panelControl3.Name = "panelControl3";
            this.panelControl3.Size = new System.Drawing.Size(890, 170);
            this.panelControl3.TabIndex = 2;
            // 
            // callBtn4
            // 
            this.callBtn4.Location = new System.Drawing.Point(760, 102);
            this.callBtn4.Name = "callBtn4";
            this.callBtn4.Size = new System.Drawing.Size(75, 23);
            this.callBtn4.TabIndex = 51;
            this.callBtn4.Text = "Call";
            this.callBtn4.Visible = false;
            this.callBtn4.Click += new System.EventHandler(this.callBtn4_Click);
            // 
            // callBtn3
            // 
            this.callBtn3.Location = new System.Drawing.Point(760, 68);
            this.callBtn3.Name = "callBtn3";
            this.callBtn3.Size = new System.Drawing.Size(75, 23);
            this.callBtn3.TabIndex = 50;
            this.callBtn3.Text = "Call";
            this.callBtn3.Visible = false;
            this.callBtn3.Click += new System.EventHandler(this.callBtn3_Click);
            // 
            // callBtn2
            // 
            this.callBtn2.Location = new System.Drawing.Point(760, 35);
            this.callBtn2.Name = "callBtn2";
            this.callBtn2.Size = new System.Drawing.Size(75, 23);
            this.callBtn2.TabIndex = 49;
            this.callBtn2.Text = "Call";
            this.callBtn2.Visible = false;
            this.callBtn2.Click += new System.EventHandler(this.callBtn2_Click);
            // 
            // callBtn5
            // 
            this.callBtn5.Location = new System.Drawing.Point(760, 135);
            this.callBtn5.Name = "callBtn5";
            this.callBtn5.Size = new System.Drawing.Size(75, 23);
            this.callBtn5.TabIndex = 48;
            this.callBtn5.Text = "Call";
            this.callBtn5.Visible = false;
            this.callBtn5.Click += new System.EventHandler(this.callBtn5_Click);
            // 
            // callBtn1
            // 
            this.callBtn1.Location = new System.Drawing.Point(760, 2);
            this.callBtn1.Name = "callBtn1";
            this.callBtn1.Size = new System.Drawing.Size(75, 23);
            this.callBtn1.TabIndex = 47;
            this.callBtn1.Text = "Call";
            this.callBtn1.Visible = false;
            this.callBtn1.Click += new System.EventHandler(this.callBtn1_Click);
            // 
            // ph5Number
            // 
            this.ph5Number.Location = new System.Drawing.Point(89, 137);
            this.ph5Number.MenuManager = this.barManager1;
            this.ph5Number.Name = "ph5Number";
            this.ph5Number.Properties.AppearanceDisabled.ForeColor = System.Drawing.Color.Black;
            this.ph5Number.Properties.AppearanceDisabled.Options.UseForeColor = true;
            this.ph5Number.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.ph5Number.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.ph5Number.Properties.Mask.AutoComplete = DevExpress.XtraEditors.Mask.AutoCompleteType.Strong;
            this.ph5Number.Properties.Mask.BeepOnError = true;
            this.ph5Number.Properties.Mask.EditMask = "(999) 000-0000";
            this.ph5Number.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Simple;
            this.ph5Number.Properties.Mask.SaveLiteral = false;
            this.ph5Number.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.ph5Number.Size = new System.Drawing.Size(100, 20);
            this.ph5Number.TabIndex = 46;
            this.ph5Number.ToolTip = "TelephoneNumbers.SearchValue";
            // 
            // barManager1
            // 
            this.barManager1.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar2});
            this.barManager1.DockControls.Add(this.barDockControlTop);
            this.barManager1.DockControls.Add(this.barDockControlBottom);
            this.barManager1.DockControls.Add(this.barDockControlLeft);
            this.barManager1.DockControls.Add(this.barDockControlRight);
            this.barManager1.Form = this;
            this.barManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.barSubItem_File,
            this.barSubItem_Appointments,
            this.barSubItem_Reports,
            this.barSubItem_Help,
            this.barButtonItem2,
            this.restoreToActiveItem,
            this.barButtonItem4,
            this.barButtonItem1,
            this.barButtonItem5,
            this.barButtonItem6,
            this.barButtonItem7,
            this.barButtonItem8,
            this.barButtonItem3,
            this.barButtonItem9,
            this.barSubItem1,
            this.userTimezone,
            this.barEditItem1,
            this.barEditItem2,
            this.barEditItem3,
            this.barSubItem_Debtplus,
            this.barButtonItem10,
            this.barButtonItem11,
            this.barButtonItem_Debtplus});
            this.barManager1.MainMenu = this.bar2;
            this.barManager1.MaxItemId = 36;
            this.barManager1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.userTimezoneComboBox,
            this.repositoryItemPopupContainerEdit1,
            this.repositoryItemTextEdit2,
            this.repositoryItemPopupContainerEdit2});
            // 
            // bar2
            // 
            this.bar2.BarName = "Main menu";
            this.bar2.DockCol = 0;
            this.bar2.DockRow = 0;
            this.bar2.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar2.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.barSubItem_File),
            new DevExpress.XtraBars.LinkPersistInfo(this.barSubItem_Appointments),
            new DevExpress.XtraBars.LinkPersistInfo(this.barSubItem_Reports),
            new DevExpress.XtraBars.LinkPersistInfo(this.barSubItem1),
            new DevExpress.XtraBars.LinkPersistInfo(this.barSubItem_Debtplus),
            new DevExpress.XtraBars.LinkPersistInfo(this.barSubItem_Help)});
            this.bar2.OptionsBar.MultiLine = true;
            this.bar2.OptionsBar.UseWholeRow = true;
            this.bar2.Text = "Main menu";
            // 
            // barSubItem_File
            // 
            this.barSubItem_File.Caption = "&File";
            this.barSubItem_File.Id = 1;
            this.barSubItem_File.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem2),
            new DevExpress.XtraBars.LinkPersistInfo(this.restoreToActiveItem),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem4)});
            this.barSubItem_File.Name = "barSubItem_File";
            // 
            // barButtonItem2
            // 
            this.barButtonItem2.Caption = "Print";
            this.barButtonItem2.Id = 17;
            this.barButtonItem2.Name = "barButtonItem2";
            this.barButtonItem2.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem2_ItemClick);
            // 
            // restoreToActiveItem
            // 
            this.restoreToActiveItem.Caption = "Restore to Active DB";
            this.restoreToActiveItem.Id = 18;
            this.restoreToActiveItem.Name = "restoreToActiveItem";
            this.restoreToActiveItem.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem3_ItemClick);
            // 
            // barButtonItem4
            // 
            this.barButtonItem4.Caption = "Exit";
            this.barButtonItem4.Id = 19;
            this.barButtonItem4.Name = "barButtonItem4";
            this.barButtonItem4.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem4_ItemClick);
            // 
            // barSubItem_Appointments
            // 
            this.barSubItem_Appointments.Caption = "&Appointments";
            this.barSubItem_Appointments.Id = 2;
            this.barSubItem_Appointments.ItemAppearance.Hovered.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.barSubItem_Appointments.ItemAppearance.Hovered.Options.UseBackColor = true;
            this.barSubItem_Appointments.ItemInMenuAppearance.Hovered.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.barSubItem_Appointments.ItemInMenuAppearance.Hovered.Options.UseBackColor = true;
            this.barSubItem_Appointments.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem1),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem5),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem6),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem7),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem8)});
            this.barSubItem_Appointments.Name = "barSubItem_Appointments";
            // 
            // barButtonItem1
            // 
            this.barButtonItem1.Caption = "New";
            this.barButtonItem1.Id = 20;
            this.barButtonItem1.Name = "barButtonItem1";
            this.barButtonItem1.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem1_ItemClick);
            // 
            // barButtonItem5
            // 
            this.barButtonItem5.Caption = "List";
            this.barButtonItem5.Id = 21;
            this.barButtonItem5.Name = "barButtonItem5";
            this.barButtonItem5.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem5_ItemClick);
            // 
            // barButtonItem6
            // 
            this.barButtonItem6.Caption = "Cancel";
            this.barButtonItem6.Id = 22;
            this.barButtonItem6.Name = "barButtonItem6";
            this.barButtonItem6.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem6_ItemClick);
            // 
            // barButtonItem7
            // 
            this.barButtonItem7.Caption = "Confirm";
            this.barButtonItem7.Id = 23;
            this.barButtonItem7.Name = "barButtonItem7";
            this.barButtonItem7.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem7_ItemClick);
            // 
            // barButtonItem8
            // 
            this.barButtonItem8.Caption = "Reschedule";
            this.barButtonItem8.Id = 24;
            this.barButtonItem8.Name = "barButtonItem8";
            this.barButtonItem8.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem8_ItemClick);
            // 
            // barSubItem_Reports
            // 
            this.barSubItem_Reports.Caption = "&Reports";
            this.barSubItem_Reports.Id = 4;
            this.barSubItem_Reports.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem3),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem9)});
            this.barSubItem_Reports.Name = "barSubItem_Reports";
            // 
            // barButtonItem3
            // 
            this.barButtonItem3.Caption = "Appointment History";
            this.barButtonItem3.Id = 25;
            this.barButtonItem3.Name = "barButtonItem3";
            this.barButtonItem3.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem3_ItemClick_1);
            // 
            // barButtonItem9
            // 
            this.barButtonItem9.Caption = "Notes";
            this.barButtonItem9.Id = 26;
            this.barButtonItem9.Name = "barButtonItem9";
            this.barButtonItem9.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem9_ItemClick);
            // 
            // barSubItem1
            // 
            this.barSubItem1.Caption = "&Settings";
            this.barSubItem1.Id = 27;
            this.barSubItem1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.userTimezone)});
            this.barSubItem1.Name = "barSubItem1";
            // 
            // userTimezone
            // 
            this.userTimezone.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Left;
            this.userTimezone.AllowRightClickInMenu = false;
            this.userTimezone.AutoFillWidth = true;
            this.userTimezone.Caption = "My Timezone";
            this.userTimezone.Edit = this.userTimezoneComboBox;
            this.userTimezone.Id = 28;
            this.userTimezone.Name = "userTimezone";
            this.userTimezone.Width = 75;
            // 
            // userTimezoneComboBox
            // 
            this.userTimezoneComboBox.AutoHeight = false;
            this.userTimezoneComboBox.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.userTimezoneComboBox.Items.AddRange(new object[] {
            "Atlantic",
            "Eastern"});
            this.userTimezoneComboBox.Name = "userTimezoneComboBox";
            // 
            // barSubItem_Debtplus
            // 
            this.barSubItem_Debtplus.Caption = "&DebtPlus";
            this.barSubItem_Debtplus.Id = 32;
            this.barSubItem_Debtplus.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem_Debtplus)});
            this.barSubItem_Debtplus.Name = "barSubItem_Debtplus";
            // 
            // barButtonItem_Debtplus
            // 
            this.barButtonItem_Debtplus.Caption = "&Launch Debtplus";
            this.barButtonItem_Debtplus.Id = 35;
            this.barButtonItem_Debtplus.Name = "barButtonItem_Debtplus";
            this.barButtonItem_Debtplus.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem_Debtplus_ItemClick);
            // 
            // barSubItem_Help
            // 
            this.barSubItem_Help.Caption = "&Help";
            this.barSubItem_Help.Id = 5;
            this.barSubItem_Help.Name = "barSubItem_Help";
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.CausesValidation = false;
            this.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControlTop.Location = new System.Drawing.Point(0, 0);
            this.barDockControlTop.Size = new System.Drawing.Size(930, 25);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.CausesValidation = false;
            this.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 663);
            this.barDockControlBottom.Size = new System.Drawing.Size(930, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.CausesValidation = false;
            this.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 25);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 638);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.CausesValidation = false;
            this.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControlRight.Location = new System.Drawing.Point(930, 25);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 638);
            // 
            // barEditItem1
            // 
            this.barEditItem1.Caption = "Click To Dial";
            this.barEditItem1.Edit = this.repositoryItemPopupContainerEdit1;
            this.barEditItem1.Id = 29;
            this.barEditItem1.Name = "barEditItem1";
            // 
            // repositoryItemPopupContainerEdit1
            // 
            this.repositoryItemPopupContainerEdit1.AutoHeight = false;
            this.repositoryItemPopupContainerEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemPopupContainerEdit1.Name = "repositoryItemPopupContainerEdit1";
            // 
            // barEditItem2
            // 
            this.barEditItem2.Caption = "barEditItem2";
            this.barEditItem2.Edit = this.repositoryItemTextEdit2;
            this.barEditItem2.Id = 30;
            this.barEditItem2.Name = "barEditItem2";
            // 
            // repositoryItemTextEdit2
            // 
            this.repositoryItemTextEdit2.AutoHeight = false;
            this.repositoryItemTextEdit2.Name = "repositoryItemTextEdit2";
            // 
            // barEditItem3
            // 
            this.barEditItem3.Caption = "barEditItem3";
            this.barEditItem3.Edit = this.repositoryItemPopupContainerEdit2;
            this.barEditItem3.Id = 31;
            this.barEditItem3.Name = "barEditItem3";
            // 
            // repositoryItemPopupContainerEdit2
            // 
            this.repositoryItemPopupContainerEdit2.AutoHeight = false;
            this.repositoryItemPopupContainerEdit2.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemPopupContainerEdit2.Name = "repositoryItemPopupContainerEdit2";
            // 
            // barButtonItem10
            // 
            this.barButtonItem10.Caption = "Launch DebtPlus";
            this.barButtonItem10.Id = 33;
            this.barButtonItem10.Name = "barButtonItem10";
            // 
            // barButtonItem11
            // 
            this.barButtonItem11.Caption = "&Launch DebtPlus";
            this.barButtonItem11.Id = 34;
            this.barButtonItem11.Name = "barButtonItem11";
            // 
            // ph4Number
            // 
            this.ph4Number.Location = new System.Drawing.Point(89, 104);
            this.ph4Number.MenuManager = this.barManager1;
            this.ph4Number.Name = "ph4Number";
            this.ph4Number.Properties.AppearanceDisabled.ForeColor = System.Drawing.Color.Black;
            this.ph4Number.Properties.AppearanceDisabled.Options.UseForeColor = true;
            this.ph4Number.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.ph4Number.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.ph4Number.Properties.Mask.AutoComplete = DevExpress.XtraEditors.Mask.AutoCompleteType.Strong;
            this.ph4Number.Properties.Mask.BeepOnError = true;
            this.ph4Number.Properties.Mask.EditMask = "(999) 000-0000";
            this.ph4Number.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Simple;
            this.ph4Number.Properties.Mask.SaveLiteral = false;
            this.ph4Number.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.ph4Number.Size = new System.Drawing.Size(100, 20);
            this.ph4Number.TabIndex = 45;
            this.ph4Number.ToolTip = "TelephoneNumbers.SearchValue";
            // 
            // ph3Number
            // 
            this.ph3Number.Location = new System.Drawing.Point(89, 70);
            this.ph3Number.MenuManager = this.barManager1;
            this.ph3Number.Name = "ph3Number";
            this.ph3Number.Properties.AppearanceDisabled.ForeColor = System.Drawing.Color.Black;
            this.ph3Number.Properties.AppearanceDisabled.Options.UseForeColor = true;
            this.ph3Number.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.ph3Number.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.ph3Number.Properties.Mask.AutoComplete = DevExpress.XtraEditors.Mask.AutoCompleteType.Strong;
            this.ph3Number.Properties.Mask.BeepOnError = true;
            this.ph3Number.Properties.Mask.EditMask = "(999) 000-0000";
            this.ph3Number.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Simple;
            this.ph3Number.Properties.Mask.SaveLiteral = false;
            this.ph3Number.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.ph3Number.Size = new System.Drawing.Size(100, 20);
            this.ph3Number.TabIndex = 44;
            this.ph3Number.ToolTip = "TelephoneNumbers.SearchValue";
            // 
            // ph2Number
            // 
            this.ph2Number.Location = new System.Drawing.Point(89, 37);
            this.ph2Number.MenuManager = this.barManager1;
            this.ph2Number.Name = "ph2Number";
            this.ph2Number.Properties.AppearanceDisabled.ForeColor = System.Drawing.Color.Black;
            this.ph2Number.Properties.AppearanceDisabled.Options.UseForeColor = true;
            this.ph2Number.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.ph2Number.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.ph2Number.Properties.Mask.AutoComplete = DevExpress.XtraEditors.Mask.AutoCompleteType.Strong;
            this.ph2Number.Properties.Mask.BeepOnError = true;
            this.ph2Number.Properties.Mask.EditMask = "(999) 000-0000";
            this.ph2Number.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Simple;
            this.ph2Number.Properties.Mask.SaveLiteral = false;
            this.ph2Number.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.ph2Number.Size = new System.Drawing.Size(100, 20);
            this.ph2Number.TabIndex = 43;
            this.ph2Number.ToolTip = "TelephoneNumbers.SearchValue";
            // 
            // ph1Number
            // 
            this.ph1Number.Location = new System.Drawing.Point(89, 5);
            this.ph1Number.MenuManager = this.barManager1;
            this.ph1Number.Name = "ph1Number";
            this.ph1Number.Properties.AppearanceDisabled.ForeColor = System.Drawing.Color.Black;
            this.ph1Number.Properties.AppearanceDisabled.Options.UseForeColor = true;
            this.ph1Number.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.ph1Number.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.ph1Number.Properties.Mask.AutoComplete = DevExpress.XtraEditors.Mask.AutoCompleteType.Strong;
            this.ph1Number.Properties.Mask.BeepOnError = true;
            this.ph1Number.Properties.Mask.EditMask = "(999) 000-0000";
            this.ph1Number.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Simple;
            this.ph1Number.Properties.Mask.SaveLiteral = false;
            this.ph1Number.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.ph1Number.Size = new System.Drawing.Size(100, 20);
            this.ph1Number.TabIndex = 42;
            this.ph1Number.ToolTip = "TelephoneNumbers.SearchValue";
            // 
            // ph5MTI
            // 
            this.ph5MTI.EnterMoveNextControl = true;
            this.ph5MTI.Location = new System.Drawing.Point(632, 137);
            this.ph5MTI.Name = "ph5MTI";
            this.ph5MTI.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.ph5MTI.Properties.ReadOnly = true;
            this.ph5MTI.Size = new System.Drawing.Size(100, 20);
            this.ph5MTI.TabIndex = 39;
            // 
            // labelControl30
            // 
            this.labelControl30.Location = new System.Drawing.Point(564, 140);
            this.labelControl30.Name = "labelControl30";
            this.labelControl30.Size = new System.Drawing.Size(51, 13);
            this.labelControl30.TabIndex = 38;
            this.labelControl30.Text = "MTI Result";
            // 
            // ph5Type
            // 
            this.ph5Type.Location = new System.Drawing.Point(441, 137);
            this.ph5Type.Name = "ph5Type";
            this.ph5Type.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.ph5Type.Properties.Items.AddRange(new object[] {
            "App - Cell",
            "CoApp - Work"});
            this.ph5Type.Size = new System.Drawing.Size(100, 20);
            this.ph5Type.TabIndex = 37;
            this.ph5Type.ToolTip = "TelephoneNumbers.user_selected_type";
            // 
            // labelControl31
            // 
            this.labelControl31.Location = new System.Drawing.Point(390, 140);
            this.labelControl31.Name = "labelControl31";
            this.labelControl31.Size = new System.Drawing.Size(43, 13);
            this.labelControl31.TabIndex = 36;
            this.labelControl31.Text = "Ph. Type";
            // 
            // ph5Result
            // 
            this.ph5Result.Location = new System.Drawing.Point(270, 137);
            this.ph5Result.Name = "ph5Result";
            this.ph5Result.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.ph5Result.Properties.Items.AddRange(new object[] {
            "LM - Left Message",
            "BN - Bad Number"});
            this.ph5Result.Size = new System.Drawing.Size(100, 20);
            this.ph5Result.TabIndex = 35;
            this.ph5Result.ToolTip = "OCS_CallOutcome.ResultCode";
            // 
            // labelControl32
            // 
            this.labelControl32.Location = new System.Drawing.Point(206, 140);
            this.labelControl32.Name = "labelControl32";
            this.labelControl32.Size = new System.Drawing.Size(50, 13);
            this.labelControl32.TabIndex = 34;
            this.labelControl32.Text = "Call Result";
            // 
            // ph4MTI
            // 
            this.ph4MTI.Location = new System.Drawing.Point(632, 104);
            this.ph4MTI.Name = "ph4MTI";
            this.ph4MTI.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.ph4MTI.Properties.ReadOnly = true;
            this.ph4MTI.Size = new System.Drawing.Size(100, 20);
            this.ph4MTI.TabIndex = 32;
            // 
            // labelControl27
            // 
            this.labelControl27.Location = new System.Drawing.Point(564, 107);
            this.labelControl27.Name = "labelControl27";
            this.labelControl27.Size = new System.Drawing.Size(51, 13);
            this.labelControl27.TabIndex = 31;
            this.labelControl27.Text = "MTI Result";
            // 
            // ph4Type
            // 
            this.ph4Type.Location = new System.Drawing.Point(441, 104);
            this.ph4Type.Name = "ph4Type";
            this.ph4Type.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.ph4Type.Properties.Items.AddRange(new object[] {
            "App - Cell",
            "CoApp - Work"});
            this.ph4Type.Size = new System.Drawing.Size(100, 20);
            this.ph4Type.TabIndex = 30;
            this.ph4Type.ToolTip = "TelephoneNumbers.user_selected_type";
            // 
            // labelControl28
            // 
            this.labelControl28.Location = new System.Drawing.Point(390, 107);
            this.labelControl28.Name = "labelControl28";
            this.labelControl28.Size = new System.Drawing.Size(43, 13);
            this.labelControl28.TabIndex = 29;
            this.labelControl28.Text = "Ph. Type";
            // 
            // ph4Result
            // 
            this.ph4Result.Location = new System.Drawing.Point(270, 104);
            this.ph4Result.Name = "ph4Result";
            this.ph4Result.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.ph4Result.Properties.Items.AddRange(new object[] {
            "LM - Left Message",
            "BN - Bad Number"});
            this.ph4Result.Size = new System.Drawing.Size(100, 20);
            this.ph4Result.TabIndex = 28;
            this.ph4Result.ToolTip = "OCS_CallOutcome.ResultCode";
            // 
            // labelControl29
            // 
            this.labelControl29.Location = new System.Drawing.Point(206, 107);
            this.labelControl29.Name = "labelControl29";
            this.labelControl29.Size = new System.Drawing.Size(50, 13);
            this.labelControl29.TabIndex = 27;
            this.labelControl29.Text = "Call Result";
            // 
            // ph3MTI
            // 
            this.ph3MTI.Location = new System.Drawing.Point(632, 70);
            this.ph3MTI.Name = "ph3MTI";
            this.ph3MTI.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.ph3MTI.Properties.ReadOnly = true;
            this.ph3MTI.Size = new System.Drawing.Size(100, 20);
            this.ph3MTI.TabIndex = 25;
            // 
            // labelControl24
            // 
            this.labelControl24.Location = new System.Drawing.Point(564, 73);
            this.labelControl24.Name = "labelControl24";
            this.labelControl24.Size = new System.Drawing.Size(51, 13);
            this.labelControl24.TabIndex = 24;
            this.labelControl24.Text = "MTI Result";
            // 
            // ph3Type
            // 
            this.ph3Type.Location = new System.Drawing.Point(441, 70);
            this.ph3Type.Name = "ph3Type";
            this.ph3Type.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.ph3Type.Properties.Items.AddRange(new object[] {
            "App - Cell",
            "CoApp - Work"});
            this.ph3Type.Size = new System.Drawing.Size(100, 20);
            this.ph3Type.TabIndex = 23;
            this.ph3Type.ToolTip = "TelephoneNumbers.user_selected_type";
            // 
            // labelControl25
            // 
            this.labelControl25.Location = new System.Drawing.Point(390, 73);
            this.labelControl25.Name = "labelControl25";
            this.labelControl25.Size = new System.Drawing.Size(43, 13);
            this.labelControl25.TabIndex = 22;
            this.labelControl25.Text = "Ph. Type";
            // 
            // ph3Result
            // 
            this.ph3Result.Location = new System.Drawing.Point(270, 70);
            this.ph3Result.Name = "ph3Result";
            this.ph3Result.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.ph3Result.Properties.Items.AddRange(new object[] {
            "LM - Left Message",
            "BN - Bad Number"});
            this.ph3Result.Size = new System.Drawing.Size(100, 20);
            this.ph3Result.TabIndex = 21;
            this.ph3Result.ToolTip = "OCS_CallOutcome.ResultCode";
            // 
            // labelControl26
            // 
            this.labelControl26.Location = new System.Drawing.Point(206, 73);
            this.labelControl26.Name = "labelControl26";
            this.labelControl26.Size = new System.Drawing.Size(50, 13);
            this.labelControl26.TabIndex = 20;
            this.labelControl26.Text = "Call Result";
            // 
            // ph2MTI
            // 
            this.ph2MTI.Location = new System.Drawing.Point(632, 37);
            this.ph2MTI.Name = "ph2MTI";
            this.ph2MTI.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.ph2MTI.Properties.ReadOnly = true;
            this.ph2MTI.Size = new System.Drawing.Size(100, 20);
            this.ph2MTI.TabIndex = 18;
            // 
            // labelControl21
            // 
            this.labelControl21.Location = new System.Drawing.Point(564, 40);
            this.labelControl21.Name = "labelControl21";
            this.labelControl21.Size = new System.Drawing.Size(51, 13);
            this.labelControl21.TabIndex = 17;
            this.labelControl21.Text = "MTI Result";
            // 
            // ph2Type
            // 
            this.ph2Type.Location = new System.Drawing.Point(441, 37);
            this.ph2Type.Name = "ph2Type";
            this.ph2Type.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.ph2Type.Properties.Items.AddRange(new object[] {
            "App - Cell",
            "CoApp - Work"});
            this.ph2Type.Size = new System.Drawing.Size(100, 20);
            this.ph2Type.TabIndex = 16;
            this.ph2Type.ToolTip = "TelephoneNumbers.user_selected_type";
            // 
            // labelControl22
            // 
            this.labelControl22.Location = new System.Drawing.Point(390, 40);
            this.labelControl22.Name = "labelControl22";
            this.labelControl22.Size = new System.Drawing.Size(43, 13);
            this.labelControl22.TabIndex = 15;
            this.labelControl22.Text = "Ph. Type";
            // 
            // ph2Result
            // 
            this.ph2Result.Location = new System.Drawing.Point(270, 37);
            this.ph2Result.Name = "ph2Result";
            this.ph2Result.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.ph2Result.Properties.Items.AddRange(new object[] {
            "LM - Left Message",
            "BN - Bad Number"});
            this.ph2Result.Size = new System.Drawing.Size(100, 20);
            this.ph2Result.TabIndex = 14;
            this.ph2Result.ToolTip = "OCS_CallOutcome.ResultCode";
            // 
            // labelControl23
            // 
            this.labelControl23.Location = new System.Drawing.Point(206, 40);
            this.labelControl23.Name = "labelControl23";
            this.labelControl23.Size = new System.Drawing.Size(50, 13);
            this.labelControl23.TabIndex = 13;
            this.labelControl23.Text = "Call Result";
            // 
            // labelControl20
            // 
            this.labelControl20.Location = new System.Drawing.Point(5, 140);
            this.labelControl20.Name = "labelControl20";
            this.labelControl20.Size = new System.Drawing.Size(47, 13);
            this.labelControl20.TabIndex = 11;
            this.labelControl20.Text = "Phone #5";
            // 
            // labelControl19
            // 
            this.labelControl19.Location = new System.Drawing.Point(5, 107);
            this.labelControl19.Name = "labelControl19";
            this.labelControl19.Size = new System.Drawing.Size(47, 13);
            this.labelControl19.TabIndex = 10;
            this.labelControl19.Text = "Phone #4";
            // 
            // labelControl18
            // 
            this.labelControl18.Location = new System.Drawing.Point(5, 73);
            this.labelControl18.Name = "labelControl18";
            this.labelControl18.Size = new System.Drawing.Size(47, 13);
            this.labelControl18.TabIndex = 9;
            this.labelControl18.Text = "Phone #3";
            // 
            // labelControl17
            // 
            this.labelControl17.Location = new System.Drawing.Point(5, 40);
            this.labelControl17.Name = "labelControl17";
            this.labelControl17.Size = new System.Drawing.Size(47, 13);
            this.labelControl17.TabIndex = 8;
            this.labelControl17.Text = "Phone #2";
            // 
            // ph1MTI
            // 
            this.ph1MTI.Location = new System.Drawing.Point(632, 5);
            this.ph1MTI.Name = "ph1MTI";
            this.ph1MTI.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.ph1MTI.Properties.ReadOnly = true;
            this.ph1MTI.Size = new System.Drawing.Size(100, 20);
            this.ph1MTI.TabIndex = 7;
            // 
            // labelControl16
            // 
            this.labelControl16.Location = new System.Drawing.Point(564, 8);
            this.labelControl16.Name = "labelControl16";
            this.labelControl16.Size = new System.Drawing.Size(51, 13);
            this.labelControl16.TabIndex = 6;
            this.labelControl16.Text = "MTI Result";
            // 
            // ph1Type
            // 
            this.ph1Type.Location = new System.Drawing.Point(441, 5);
            this.ph1Type.Name = "ph1Type";
            this.ph1Type.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.ph1Type.Properties.Items.AddRange(new object[] {
            "App - Cell",
            "CoApp - Work"});
            this.ph1Type.Size = new System.Drawing.Size(100, 20);
            this.ph1Type.TabIndex = 5;
            this.ph1Type.ToolTip = "TelephoneNumbers.user_selected_type";
            // 
            // labelControl15
            // 
            this.labelControl15.Location = new System.Drawing.Point(390, 8);
            this.labelControl15.Name = "labelControl15";
            this.labelControl15.Size = new System.Drawing.Size(43, 13);
            this.labelControl15.TabIndex = 4;
            this.labelControl15.Text = "Ph. Type";
            // 
            // ph1Result
            // 
            this.ph1Result.Location = new System.Drawing.Point(270, 5);
            this.ph1Result.Name = "ph1Result";
            this.ph1Result.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.ph1Result.Properties.Items.AddRange(new object[] {
            "LM - Left Message",
            "BN - Bad Number"});
            this.ph1Result.Size = new System.Drawing.Size(100, 20);
            this.ph1Result.TabIndex = 3;
            this.ph1Result.ToolTip = "OCS_CallOutcome.ResultCode";
            // 
            // labelControl14
            // 
            this.labelControl14.Location = new System.Drawing.Point(206, 8);
            this.labelControl14.Name = "labelControl14";
            this.labelControl14.Size = new System.Drawing.Size(50, 13);
            this.labelControl14.TabIndex = 2;
            this.labelControl14.Text = "Call Result";
            // 
            // labelControl13
            // 
            this.labelControl13.Location = new System.Drawing.Point(6, 8);
            this.labelControl13.Name = "labelControl13";
            this.labelControl13.Size = new System.Drawing.Size(47, 13);
            this.labelControl13.TabIndex = 0;
            this.labelControl13.Text = "Phone #1";
            // 
            // panelControl4
            // 
            this.panelControl4.Controls.Add(this.stage4);
            this.panelControl4.Controls.Add(this.labelControl37);
            this.panelControl4.Controls.Add(this.stage3);
            this.panelControl4.Controls.Add(this.labelControl36);
            this.panelControl4.Controls.Add(this.stage2);
            this.panelControl4.Controls.Add(this.stage1);
            this.panelControl4.Controls.Add(this.labelControl35);
            this.panelControl4.Controls.Add(this.labelControl34);
            this.panelControl4.Controls.Add(this.labelControl33);
            this.panelControl4.Location = new System.Drawing.Point(1, 433);
            this.panelControl4.Name = "panelControl4";
            this.panelControl4.Size = new System.Drawing.Size(886, 125);
            this.panelControl4.TabIndex = 3;
            // 
            // stage4
            // 
            this.stage4.Location = new System.Drawing.Point(456, 58);
            this.stage4.Name = "stage4";
            this.stage4.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.stage4.Properties.ReadOnly = true;
            this.stage4.Size = new System.Drawing.Size(204, 20);
            this.stage4.TabIndex = 8;
            // 
            // labelControl37
            // 
            this.labelControl37.Location = new System.Drawing.Point(389, 61);
            this.labelControl37.Name = "labelControl37";
            this.labelControl37.Size = new System.Drawing.Size(37, 13);
            this.labelControl37.TabIndex = 7;
            this.labelControl37.Text = "Stage 4";
            // 
            // stage3
            // 
            this.stage3.Location = new System.Drawing.Point(456, 30);
            this.stage3.Name = "stage3";
            this.stage3.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.stage3.Properties.ReadOnly = true;
            this.stage3.Size = new System.Drawing.Size(204, 20);
            this.stage3.TabIndex = 6;
            // 
            // labelControl36
            // 
            this.labelControl36.Location = new System.Drawing.Point(389, 33);
            this.labelControl36.Name = "labelControl36";
            this.labelControl36.Size = new System.Drawing.Size(37, 13);
            this.labelControl36.TabIndex = 5;
            this.labelControl36.Text = "Stage 3";
            // 
            // stage2
            // 
            this.stage2.Location = new System.Drawing.Point(72, 56);
            this.stage2.Name = "stage2";
            this.stage2.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.stage2.Properties.ReadOnly = true;
            this.stage2.Size = new System.Drawing.Size(204, 20);
            this.stage2.TabIndex = 4;
            // 
            // stage1
            // 
            this.stage1.Location = new System.Drawing.Point(72, 30);
            this.stage1.Name = "stage1";
            this.stage1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.stage1.Properties.ReadOnly = true;
            this.stage1.Size = new System.Drawing.Size(204, 20);
            this.stage1.TabIndex = 3;
            // 
            // labelControl35
            // 
            this.labelControl35.Location = new System.Drawing.Point(5, 61);
            this.labelControl35.Name = "labelControl35";
            this.labelControl35.Size = new System.Drawing.Size(37, 13);
            this.labelControl35.TabIndex = 2;
            this.labelControl35.Text = "Stage 2";
            // 
            // labelControl34
            // 
            this.labelControl34.Location = new System.Drawing.Point(5, 33);
            this.labelControl34.Name = "labelControl34";
            this.labelControl34.Size = new System.Drawing.Size(37, 13);
            this.labelControl34.TabIndex = 1;
            this.labelControl34.Text = "Stage 1";
            // 
            // labelControl33
            // 
            this.labelControl33.Location = new System.Drawing.Point(6, 5);
            this.labelControl33.Name = "labelControl33";
            this.labelControl33.Size = new System.Drawing.Size(87, 13);
            this.labelControl33.TabIndex = 0;
            this.labelControl33.Text = "Stages Completed";
            // 
            // accountTab
            // 
            this.accountTab.Appearance.PageClient.BackColor = System.Drawing.Color.Turquoise;
            this.accountTab.Appearance.PageClient.Options.UseBackColor = true;
            this.accountTab.Controls.Add(this.panelControl8);
            this.accountTab.Name = "accountTab";
            this.accountTab.Size = new System.Drawing.Size(887, 551);
            this.accountTab.Text = "Account";
            // 
            // panelControl8
            // 
            this.panelControl8.Controls.Add(this.panelControl7);
            this.panelControl8.Controls.Add(this.panelControl5);
            this.panelControl8.Controls.Add(this.panelControl6);
            this.panelControl8.Location = new System.Drawing.Point(-1, 0);
            this.panelControl8.Name = "panelControl8";
            this.panelControl8.Size = new System.Drawing.Size(890, 553);
            this.panelControl8.TabIndex = 5;
            // 
            // panelControl7
            // 
            this.panelControl7.Controls.Add(this.useHomeAddress);
            this.panelControl7.Controls.Add(this.labelControl59);
            this.panelControl7.Controls.Add(this.propertyZipcode);
            this.panelControl7.Controls.Add(this.propertyUSState);
            this.panelControl7.Controls.Add(this.propertyCity);
            this.panelControl7.Controls.Add(this.propertyAddress2);
            this.panelControl7.Controls.Add(this.propertyAddress1);
            this.panelControl7.Controls.Add(this.labelControl58);
            this.panelControl7.Location = new System.Drawing.Point(0, 271);
            this.panelControl7.Name = "panelControl7";
            this.panelControl7.Size = new System.Drawing.Size(890, 149);
            this.panelControl7.TabIndex = 26;
            // 
            // useHomeAddress
            // 
            this.useHomeAddress.EditValue = true;
            this.useHomeAddress.Location = new System.Drawing.Point(100, 27);
            this.useHomeAddress.Name = "useHomeAddress";
            this.useHomeAddress.Properties.Caption = "Same as Client Address";
            this.useHomeAddress.Size = new System.Drawing.Size(148, 20);
            this.useHomeAddress.TabIndex = 17;
            this.useHomeAddress.ToolTip = "Housing_properties.UseHomeAddress";
            this.useHomeAddress.CheckedChanged += new System.EventHandler(this.propertyAddressIsSame_CheckedChanged);
            // 
            // labelControl59
            // 
            this.labelControl59.Location = new System.Drawing.Point(5, 5);
            this.labelControl59.Name = "labelControl59";
            this.labelControl59.Size = new System.Drawing.Size(84, 13);
            this.labelControl59.TabIndex = 16;
            this.labelControl59.Text = "Property Address";
            // 
            // propertyZipcode
            // 
            this.propertyZipcode.EditValue = "";
            this.propertyZipcode.Location = new System.Drawing.Point(547, 113);
            this.propertyZipcode.Name = "propertyZipcode";
            this.propertyZipcode.Size = new System.Drawing.Size(100, 20);
            this.propertyZipcode.TabIndex = 15;
            this.propertyZipcode.ToolTip = "addresses.PostalCode";
            // 
            // propertyUSState
            // 
            this.propertyUSState.Location = new System.Drawing.Point(441, 113);
            this.propertyUSState.Name = "propertyUSState";
            this.propertyUSState.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.propertyUSState.Properties.Items.AddRange(new object[] {
            "AL",
            "AK",
            "AZ"});
            this.propertyUSState.Size = new System.Drawing.Size(100, 20);
            this.propertyUSState.TabIndex = 14;
            this.propertyUSState.ToolTip = "addresses.state";
            // 
            // propertyCity
            // 
            this.propertyCity.EditValue = "";
            this.propertyCity.Location = new System.Drawing.Point(100, 113);
            this.propertyCity.Name = "propertyCity";
            this.propertyCity.Size = new System.Drawing.Size(335, 20);
            this.propertyCity.TabIndex = 13;
            this.propertyCity.ToolTip = "addresses.city";
            // 
            // propertyAddress2
            // 
            this.propertyAddress2.Location = new System.Drawing.Point(100, 87);
            this.propertyAddress2.Name = "propertyAddress2";
            this.propertyAddress2.Size = new System.Drawing.Size(547, 20);
            this.propertyAddress2.TabIndex = 12;
            // 
            // propertyAddress1
            // 
            this.propertyAddress1.EditValue = "";
            this.propertyAddress1.Location = new System.Drawing.Point(101, 61);
            this.propertyAddress1.Name = "propertyAddress1";
            this.propertyAddress1.Size = new System.Drawing.Size(547, 20);
            this.propertyAddress1.TabIndex = 11;
            this.propertyAddress1.ToolTip = "addresses.street";
            // 
            // labelControl58
            // 
            this.labelControl58.Location = new System.Drawing.Point(6, 64);
            this.labelControl58.Name = "labelControl58";
            this.labelControl58.Size = new System.Drawing.Size(39, 13);
            this.labelControl58.TabIndex = 10;
            this.labelControl58.Text = "Address";
            // 
            // panelControl5
            // 
            this.panelControl5.Controls.Add(this.labelControl68);
            this.panelControl5.Controls.Add(this.actualSaleDate);
            this.panelControl5.Controls.Add(this.servicerIDNo);
            this.panelControl5.Controls.Add(this.labelControl66);
            this.panelControl5.Controls.Add(this.txt_TrialMod_Type);
            this.panelControl5.Controls.Add(this.monthsPastDue);
            this.panelControl5.Controls.Add(this.labelControl3);
            this.panelControl5.Controls.Add(this.labelControl42);
            this.panelControl5.Controls.Add(this.labelControl2);
            this.panelControl5.Controls.Add(this.labelControl49);
            this.panelControl5.Controls.Add(this.investorNo);
            this.panelControl5.Controls.Add(this.loanModDate);
            this.panelControl5.Controls.Add(this.foreclosureDate);
            this.panelControl5.Controls.Add(this.labelControl48);
            this.panelControl5.Controls.Add(this.labelControl39);
            this.panelControl5.Controls.Add(this.daysPastDue);
            this.panelControl5.Controls.Add(this.defaultReason);
            this.panelControl5.Controls.Add(this.labelControl47);
            this.panelControl5.Controls.Add(this.labelControl40);
            this.panelControl5.Controls.Add(this.servicerLoanNo);
            this.panelControl5.Controls.Add(this.labelControl41);
            this.panelControl5.Controls.Add(this.labelControl46);
            this.panelControl5.Controls.Add(this.pmtAppliedDate);
            this.panelControl5.Controls.Add(this.nextPmtDate);
            this.panelControl5.Controls.Add(this.servicer);
            this.panelControl5.Controls.Add(this.labelControl45);
            this.panelControl5.Controls.Add(this.labelControl43);
            this.panelControl5.Controls.Add(this.labelControl44);
            this.panelControl5.Controls.Add(this.pastDueDate);
            this.panelControl5.Controls.Add(this.trialMod);
            this.panelControl5.Location = new System.Drawing.Point(0, 0);
            this.panelControl5.Name = "panelControl5";
            this.panelControl5.Size = new System.Drawing.Size(890, 145);
            this.panelControl5.TabIndex = 24;
            // 
            // labelControl68
            // 
            this.labelControl68.Location = new System.Drawing.Point(677, 88);
            this.labelControl68.Name = "labelControl68";
            this.labelControl68.Size = new System.Drawing.Size(70, 13);
            this.labelControl68.TabIndex = 30;
            this.labelControl68.Text = "Trial Mod Type";
            // 
            // actualSaleDate
            // 
            this.actualSaleDate.EditValue = null;
            this.actualSaleDate.Location = new System.Drawing.Point(542, 116);
            this.actualSaleDate.Name = "actualSaleDate";
            this.actualSaleDate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.actualSaleDate.Properties.ReadOnly = true;
            this.actualSaleDate.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.actualSaleDate.Size = new System.Drawing.Size(126, 20);
            this.actualSaleDate.TabIndex = 29;
            this.actualSaleDate.ToolTip = "OCS_Client.actual_sale_date";
            // 
            // servicerIDNo
            // 
            this.servicerIDNo.EditValue = "";
            this.servicerIDNo.Location = new System.Drawing.Point(780, 11);
            this.servicerIDNo.Name = "servicerIDNo";
            this.servicerIDNo.Properties.ReadOnly = true;
            this.servicerIDNo.Size = new System.Drawing.Size(105, 20);
            this.servicerIDNo.TabIndex = 23;
            this.servicerIDNo.ToolTip = "Housing_lenders.ServicerID";
            // 
            // labelControl66
            // 
            this.labelControl66.Location = new System.Drawing.Point(452, 119);
            this.labelControl66.Name = "labelControl66";
            this.labelControl66.Size = new System.Drawing.Size(79, 13);
            this.labelControl66.TabIndex = 28;
            this.labelControl66.Text = "Actual Sale Date";
            // 
            // txt_TrialMod_Type
            // 
            this.txt_TrialMod_Type.EditValue = "";
            this.txt_TrialMod_Type.Location = new System.Drawing.Point(780, 83);
            this.txt_TrialMod_Type.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txt_TrialMod_Type.Name = "txt_TrialMod_Type";
            this.txt_TrialMod_Type.Properties.ReadOnly = true;
            this.txt_TrialMod_Type.Size = new System.Drawing.Size(105, 20);
            this.txt_TrialMod_Type.TabIndex = 27;
            this.txt_TrialMod_Type.ToolTip = "Housing_properties.HPF_foreclosureCaseID";
            // 
            // monthsPastDue
            // 
            this.monthsPastDue.EditValue = "";
            this.monthsPastDue.Location = new System.Drawing.Point(780, 46);
            this.monthsPastDue.Name = "monthsPastDue";
            this.monthsPastDue.Properties.ReadOnly = true;
            this.monthsPastDue.Size = new System.Drawing.Size(105, 20);
            this.monthsPastDue.TabIndex = 25;
            this.monthsPastDue.ToolTip = "calculated";
            // 
            // labelControl3
            // 
            this.labelControl3.Location = new System.Drawing.Point(677, 49);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(82, 13);
            this.labelControl3.TabIndex = 24;
            this.labelControl3.Text = "Months Past-Due";
            // 
            // labelControl42
            // 
            this.labelControl42.Location = new System.Drawing.Point(228, 14);
            this.labelControl42.Name = "labelControl42";
            this.labelControl42.Size = new System.Drawing.Size(39, 13);
            this.labelControl42.TabIndex = 8;
            this.labelControl42.Text = "Servicer";
            // 
            // labelControl2
            // 
            this.labelControl2.Location = new System.Drawing.Point(6, 14);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(75, 13);
            this.labelControl2.TabIndex = 0;
            this.labelControl2.Text = "Investor loan #";
            // 
            // labelControl49
            // 
            this.labelControl49.Location = new System.Drawing.Point(677, 14);
            this.labelControl49.Name = "labelControl49";
            this.labelControl49.Size = new System.Drawing.Size(64, 13);
            this.labelControl49.TabIndex = 22;
            this.labelControl49.Text = "Servicer ID #";
            // 
            // investorNo
            // 
            this.investorNo.EditValue = "";
            this.investorNo.Location = new System.Drawing.Point(102, 11);
            this.investorNo.Name = "investorNo";
            this.investorNo.Properties.ReadOnly = true;
            this.investorNo.Size = new System.Drawing.Size(120, 20);
            this.investorNo.TabIndex = 1;
            this.investorNo.ToolTip = "OCS_Client.fmac_loanno";
            // 
            // loanModDate
            // 
            this.loanModDate.EditValue = null;
            this.loanModDate.Location = new System.Drawing.Point(542, 81);
            this.loanModDate.Name = "loanModDate";
            this.loanModDate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.loanModDate.Properties.ReadOnly = true;
            this.loanModDate.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.loanModDate.Size = new System.Drawing.Size(126, 20);
            this.loanModDate.TabIndex = 21;
            // 
            // foreclosureDate
            // 
            this.foreclosureDate.EditValue = "";
            this.foreclosureDate.Location = new System.Drawing.Point(102, 46);
            this.foreclosureDate.Name = "foreclosureDate";
            this.foreclosureDate.Properties.ReadOnly = true;
            this.foreclosureDate.Size = new System.Drawing.Size(120, 20);
            this.foreclosureDate.TabIndex = 2;
            this.foreclosureDate.ToolTip = "Housing_properties.FcSaleDate";
            // 
            // labelControl48
            // 
            this.labelControl48.Location = new System.Drawing.Point(452, 84);
            this.labelControl48.Name = "labelControl48";
            this.labelControl48.Size = new System.Drawing.Size(72, 13);
            this.labelControl48.TabIndex = 20;
            this.labelControl48.Text = "Loan Mod Date";
            // 
            // labelControl39
            // 
            this.labelControl39.Location = new System.Drawing.Point(6, 49);
            this.labelControl39.Name = "labelControl39";
            this.labelControl39.Size = new System.Drawing.Size(82, 13);
            this.labelControl39.TabIndex = 3;
            this.labelControl39.Text = "Foreclosure Date";
            // 
            // daysPastDue
            // 
            this.daysPastDue.EditValue = "";
            this.daysPastDue.Location = new System.Drawing.Point(542, 46);
            this.daysPastDue.Name = "daysPastDue";
            this.daysPastDue.Properties.ReadOnly = true;
            this.daysPastDue.Size = new System.Drawing.Size(126, 20);
            this.daysPastDue.TabIndex = 19;
            this.daysPastDue.ToolTip = "calculated";
            // 
            // defaultReason
            // 
            this.defaultReason.EditValue = "";
            this.defaultReason.Location = new System.Drawing.Point(102, 81);
            this.defaultReason.Name = "defaultReason";
            this.defaultReason.Properties.ReadOnly = true;
            this.defaultReason.Size = new System.Drawing.Size(120, 20);
            this.defaultReason.TabIndex = 4;
            this.defaultReason.ToolTip = "client.cause_fin_problem1";
            // 
            // labelControl47
            // 
            this.labelControl47.Location = new System.Drawing.Point(452, 49);
            this.labelControl47.Name = "labelControl47";
            this.labelControl47.Size = new System.Drawing.Size(70, 13);
            this.labelControl47.TabIndex = 18;
            this.labelControl47.Text = "Days Past Due";
            // 
            // labelControl40
            // 
            this.labelControl40.Location = new System.Drawing.Point(6, 84);
            this.labelControl40.Name = "labelControl40";
            this.labelControl40.Size = new System.Drawing.Size(74, 13);
            this.labelControl40.TabIndex = 5;
            this.labelControl40.Text = "Default Reason";
            // 
            // servicerLoanNo
            // 
            this.servicerLoanNo.EditValue = "";
            this.servicerLoanNo.Location = new System.Drawing.Point(542, 11);
            this.servicerLoanNo.Name = "servicerLoanNo";
            this.servicerLoanNo.Properties.ReadOnly = true;
            this.servicerLoanNo.Size = new System.Drawing.Size(126, 20);
            this.servicerLoanNo.TabIndex = 17;
            this.servicerLoanNo.ToolTip = "Housing_lenders.ServicerLoanNum";
            // 
            // labelControl41
            // 
            this.labelControl41.Location = new System.Drawing.Point(6, 119);
            this.labelControl41.Name = "labelControl41";
            this.labelControl41.Size = new System.Drawing.Size(86, 13);
            this.labelControl41.TabIndex = 6;
            this.labelControl41.Text = "Loan Pmt. Applied";
            // 
            // labelControl46
            // 
            this.labelControl46.Location = new System.Drawing.Point(452, 14);
            this.labelControl46.Name = "labelControl46";
            this.labelControl46.Size = new System.Drawing.Size(76, 13);
            this.labelControl46.TabIndex = 16;
            this.labelControl46.Text = "Servicer Loan #";
            // 
            // pmtAppliedDate
            // 
            this.pmtAppliedDate.EditValue = null;
            this.pmtAppliedDate.Location = new System.Drawing.Point(102, 116);
            this.pmtAppliedDate.Name = "pmtAppliedDate";
            this.pmtAppliedDate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.pmtAppliedDate.Properties.ReadOnly = true;
            this.pmtAppliedDate.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.pmtAppliedDate.Size = new System.Drawing.Size(120, 20);
            this.pmtAppliedDate.TabIndex = 7;
            // 
            // nextPmtDate
            // 
            this.nextPmtDate.EditValue = null;
            this.nextPmtDate.Location = new System.Drawing.Point(324, 116);
            this.nextPmtDate.Name = "nextPmtDate";
            this.nextPmtDate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.nextPmtDate.Properties.ReadOnly = true;
            this.nextPmtDate.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.nextPmtDate.Size = new System.Drawing.Size(120, 20);
            this.nextPmtDate.TabIndex = 15;
            // 
            // servicer
            // 
            this.servicer.EditValue = "";
            this.servicer.Location = new System.Drawing.Point(324, 11);
            this.servicer.Name = "servicer";
            this.servicer.Properties.ReadOnly = true;
            this.servicer.Size = new System.Drawing.Size(120, 20);
            this.servicer.TabIndex = 9;
            this.servicer.ToolTip = "Housing_lenders.ServicerName";
            // 
            // labelControl45
            // 
            this.labelControl45.Location = new System.Drawing.Point(228, 119);
            this.labelControl45.Name = "labelControl45";
            this.labelControl45.Size = new System.Drawing.Size(70, 13);
            this.labelControl45.TabIndex = 14;
            this.labelControl45.Text = "Next Pmt. Due";
            // 
            // labelControl43
            // 
            this.labelControl43.Location = new System.Drawing.Point(228, 49);
            this.labelControl43.Name = "labelControl43";
            this.labelControl43.Size = new System.Drawing.Size(69, 13);
            this.labelControl43.TabIndex = 10;
            this.labelControl43.Text = "Past Due Date";
            // 
            // labelControl44
            // 
            this.labelControl44.Location = new System.Drawing.Point(228, 84);
            this.labelControl44.Name = "labelControl44";
            this.labelControl44.Size = new System.Drawing.Size(43, 13);
            this.labelControl44.TabIndex = 13;
            this.labelControl44.Text = "Trial Mod";
            // 
            // pastDueDate
            // 
            this.pastDueDate.EditValue = null;
            this.pastDueDate.Location = new System.Drawing.Point(324, 46);
            this.pastDueDate.Name = "pastDueDate";
            this.pastDueDate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.pastDueDate.Properties.ReadOnly = true;
            this.pastDueDate.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.pastDueDate.Size = new System.Drawing.Size(120, 20);
            this.pastDueDate.TabIndex = 11;
            this.pastDueDate.ToolTip = "OCS_Client.due_date";
            // 
            // trialMod
            // 
            this.trialMod.Location = new System.Drawing.Point(324, 81);
            this.trialMod.Name = "trialMod";
            this.trialMod.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.trialMod.Properties.ReadOnly = true;
            this.trialMod.Size = new System.Drawing.Size(120, 20);
            this.trialMod.TabIndex = 12;
            // 
            // panelControl6
            // 
            this.panelControl6.Controls.Add(this.txtLastChanceFlag);
            this.panelControl6.Controls.Add(this.labelControl54);
            this.panelControl6.Controls.Add(this.lastChanceDate);
            this.panelControl6.Controls.Add(this.labelControl38);
            this.panelControl6.Controls.Add(this.labelControl69);
            this.panelControl6.Controls.Add(this.secondCounselDate);
            this.panelControl6.Controls.Add(this.duplicateFile);
            this.panelControl6.Controls.Add(this.labelControl56);
            this.panelControl6.Controls.Add(this.labelControl51);
            this.panelControl6.Controls.Add(this.firstRPContact);
            this.panelControl6.Controls.Add(this.labelControl50);
            this.panelControl6.Controls.Add(this.dateFileReceived);
            this.panelControl6.Controls.Add(this.agenyCase);
            this.panelControl6.Controls.Add(this.labelControl52);
            this.panelControl6.Controls.Add(this.dateClosed);
            this.panelControl6.Controls.Add(this.labelControl53);
            this.panelControl6.Controls.Add(this.batchName);
            this.panelControl6.Controls.Add(this.labelControl55);
            this.panelControl6.Controls.Add(this.labelControl57);
            this.panelControl6.Controls.Add(this.firstCounselDate);
            this.panelControl6.Controls.Add(this.labelControl60);
            this.panelControl6.Controls.Add(this.labelControl61);
            this.panelControl6.Controls.Add(this.dateFileModified);
            this.panelControl6.Controls.Add(this.dt_ReferralDate);
            this.panelControl6.Location = new System.Drawing.Point(1, 142);
            this.panelControl6.Name = "panelControl6";
            this.panelControl6.Size = new System.Drawing.Size(889, 129);
            this.panelControl6.TabIndex = 25;
            // 
            // txtLastChanceFlag
            // 
            this.txtLastChanceFlag.EditValue = "";
            this.txtLastChanceFlag.Location = new System.Drawing.Point(779, 81);
            this.txtLastChanceFlag.Name = "txtLastChanceFlag";
            this.txtLastChanceFlag.Properties.ReadOnly = true;
            this.txtLastChanceFlag.Size = new System.Drawing.Size(105, 20);
            this.txtLastChanceFlag.TabIndex = 34;
            this.txtLastChanceFlag.ToolTip = "Last Chance Flag";
            // 
            // labelControl54
            // 
            this.labelControl54.Location = new System.Drawing.Point(677, 84);
            this.labelControl54.Name = "labelControl54";
            this.labelControl54.Size = new System.Drawing.Size(82, 13);
            this.labelControl54.TabIndex = 33;
            this.labelControl54.Text = "Last Chance Flag";
            // 
            // lastChanceDate
            // 
            this.lastChanceDate.EditValue = null;
            this.lastChanceDate.Location = new System.Drawing.Point(542, 81);
            this.lastChanceDate.Name = "lastChanceDate";
            this.lastChanceDate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lastChanceDate.Properties.ReadOnly = true;
            this.lastChanceDate.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.lastChanceDate.Size = new System.Drawing.Size(126, 20);
            this.lastChanceDate.TabIndex = 32;
            this.lastChanceDate.ToolTip = "Last Chance Date";
            // 
            // labelControl38
            // 
            this.labelControl38.Location = new System.Drawing.Point(451, 84);
            this.labelControl38.Name = "labelControl38";
            this.labelControl38.Size = new System.Drawing.Size(85, 13);
            this.labelControl38.TabIndex = 31;
            this.labelControl38.Text = "Last Chance Date";
            // 
            // labelControl69
            // 
            this.labelControl69.Location = new System.Drawing.Point(5, 51);
            this.labelControl69.Name = "labelControl69";
            this.labelControl69.Size = new System.Drawing.Size(65, 13);
            this.labelControl69.TabIndex = 30;
            this.labelControl69.Text = "Referral Date";
            // 
            // secondCounselDate
            // 
            this.secondCounselDate.EditValue = null;
            this.secondCounselDate.Location = new System.Drawing.Point(323, 81);
            this.secondCounselDate.Name = "secondCounselDate";
            this.secondCounselDate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.secondCounselDate.Properties.ReadOnly = true;
            this.secondCounselDate.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.secondCounselDate.Size = new System.Drawing.Size(120, 20);
            this.secondCounselDate.TabIndex = 29;
            this.secondCounselDate.ToolTip = "client_appointments.start_time";
            // 
            // duplicateFile
            // 
            this.duplicateFile.Location = new System.Drawing.Point(777, 12);
            this.duplicateFile.Name = "duplicateFile";
            this.duplicateFile.Properties.Caption = "";
            this.duplicateFile.Properties.ReadOnly = true;
            this.duplicateFile.Size = new System.Drawing.Size(75, 20);
            this.duplicateFile.TabIndex = 28;
            this.duplicateFile.ToolTip = "OCS_Client.IsDuplicate";
            // 
            // labelControl56
            // 
            this.labelControl56.Location = new System.Drawing.Point(677, 49);
            this.labelControl56.Name = "labelControl56";
            this.labelControl56.Size = new System.Drawing.Size(98, 13);
            this.labelControl56.TabIndex = 27;
            this.labelControl56.Text = "1st RP Contact Date";
            // 
            // labelControl51
            // 
            this.labelControl51.Location = new System.Drawing.Point(5, 14);
            this.labelControl51.Name = "labelControl51";
            this.labelControl51.Size = new System.Drawing.Size(71, 13);
            this.labelControl51.TabIndex = 26;
            this.labelControl51.Text = "Date File Rec\'d";
            // 
            // firstRPContact
            // 
            this.firstRPContact.Location = new System.Drawing.Point(779, 46);
            this.firstRPContact.Name = "firstRPContact";
            this.firstRPContact.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.firstRPContact.Properties.ReadOnly = true;
            this.firstRPContact.Size = new System.Drawing.Size(105, 20);
            this.firstRPContact.TabIndex = 26;
            this.firstRPContact.ToolTip = "OCS_ContactAttempt.Begin";
            // 
            // labelControl50
            // 
            this.labelControl50.Location = new System.Drawing.Point(227, 49);
            this.labelControl50.Name = "labelControl50";
            this.labelControl50.Size = new System.Drawing.Size(63, 13);
            this.labelControl50.TabIndex = 8;
            this.labelControl50.Text = "Agency Case";
            // 
            // dateFileReceived
            // 
            this.dateFileReceived.EditValue = null;
            this.dateFileReceived.Location = new System.Drawing.Point(101, 11);
            this.dateFileReceived.Name = "dateFileReceived";
            this.dateFileReceived.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateFileReceived.Properties.ReadOnly = true;
            this.dateFileReceived.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dateFileReceived.Size = new System.Drawing.Size(120, 20);
            this.dateFileReceived.TabIndex = 27;
            this.dateFileReceived.ToolTip = "OCS_UploadReport.UploadDate";
            // 
            // agenyCase
            // 
            this.agenyCase.EditValue = "";
            this.agenyCase.Location = new System.Drawing.Point(324, 46);
            this.agenyCase.Name = "agenyCase";
            this.agenyCase.Properties.ReadOnly = true;
            this.agenyCase.Size = new System.Drawing.Size(119, 20);
            this.agenyCase.TabIndex = 9;
            this.agenyCase.ToolTip = "Housing_lenders.CaseNumber";
            // 
            // labelControl52
            // 
            this.labelControl52.Location = new System.Drawing.Point(677, 14);
            this.labelControl52.Name = "labelControl52";
            this.labelControl52.Size = new System.Drawing.Size(63, 13);
            this.labelControl52.TabIndex = 22;
            this.labelControl52.Text = "Duplicate File";
            // 
            // dateClosed
            // 
            this.dateClosed.EditValue = null;
            this.dateClosed.Location = new System.Drawing.Point(542, 12);
            this.dateClosed.Name = "dateClosed";
            this.dateClosed.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateClosed.Properties.ReadOnly = true;
            this.dateClosed.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dateClosed.Size = new System.Drawing.Size(126, 20);
            this.dateClosed.TabIndex = 21;
            // 
            // labelControl53
            // 
            this.labelControl53.Location = new System.Drawing.Point(451, 14);
            this.labelControl53.Name = "labelControl53";
            this.labelControl53.Size = new System.Drawing.Size(58, 13);
            this.labelControl53.TabIndex = 20;
            this.labelControl53.Text = "Date Closed";
            // 
            // batchName
            // 
            this.batchName.EditValue = "";
            this.batchName.Location = new System.Drawing.Point(542, 46);
            this.batchName.Name = "batchName";
            this.batchName.Properties.ReadOnly = true;
            this.batchName.Size = new System.Drawing.Size(126, 20);
            this.batchName.TabIndex = 19;
            this.batchName.ToolTip = "OCS_UploadReport.BatchName";
            // 
            // labelControl55
            // 
            this.labelControl55.Location = new System.Drawing.Point(451, 49);
            this.labelControl55.Name = "labelControl55";
            this.labelControl55.Size = new System.Drawing.Size(78, 13);
            this.labelControl55.TabIndex = 18;
            this.labelControl55.Text = "Batch Name/No.";
            // 
            // labelControl57
            // 
            this.labelControl57.Location = new System.Drawing.Point(5, 84);
            this.labelControl57.Name = "labelControl57";
            this.labelControl57.Size = new System.Drawing.Size(82, 13);
            this.labelControl57.TabIndex = 6;
            this.labelControl57.Text = "1st Counsel Date";
            // 
            // firstCounselDate
            // 
            this.firstCounselDate.EditValue = null;
            this.firstCounselDate.Enabled = false;
            this.firstCounselDate.Location = new System.Drawing.Point(101, 81);
            this.firstCounselDate.Name = "firstCounselDate";
            this.firstCounselDate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.firstCounselDate.Properties.ReadOnly = true;
            this.firstCounselDate.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.firstCounselDate.Size = new System.Drawing.Size(120, 20);
            this.firstCounselDate.TabIndex = 7;
            this.firstCounselDate.ToolTip = "client_appointments.start_time";
            // 
            // labelControl60
            // 
            this.labelControl60.Location = new System.Drawing.Point(227, 14);
            this.labelControl60.Name = "labelControl60";
            this.labelControl60.Size = new System.Drawing.Size(85, 13);
            this.labelControl60.TabIndex = 10;
            this.labelControl60.Text = "Date File Modified";
            // 
            // labelControl61
            // 
            this.labelControl61.Location = new System.Drawing.Point(227, 84);
            this.labelControl61.Name = "labelControl61";
            this.labelControl61.Size = new System.Drawing.Size(85, 13);
            this.labelControl61.TabIndex = 13;
            this.labelControl61.Text = "2nd Counsel Date";
            // 
            // dateFileModified
            // 
            this.dateFileModified.EditValue = null;
            this.dateFileModified.Location = new System.Drawing.Point(324, 11);
            this.dateFileModified.Name = "dateFileModified";
            this.dateFileModified.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateFileModified.Properties.ReadOnly = true;
            this.dateFileModified.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dateFileModified.Size = new System.Drawing.Size(119, 20);
            this.dateFileModified.TabIndex = 11;
            // 
            // dt_ReferralDate
            // 
            this.dt_ReferralDate.EditValue = "";
            this.dt_ReferralDate.Location = new System.Drawing.Point(100, 46);
            this.dt_ReferralDate.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.dt_ReferralDate.Name = "dt_ReferralDate";
            this.dt_ReferralDate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dt_ReferralDate.Properties.Mask.EditMask = "";
            this.dt_ReferralDate.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.None;
            this.dt_ReferralDate.Properties.ReadOnly = true;
            this.dt_ReferralDate.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dt_ReferralDate.Size = new System.Drawing.Size(120, 20);
            this.dt_ReferralDate.TabIndex = 2;
            // 
            // notesTab
            // 
            this.notesTab.Controls.Add(this.panelControl10);
            this.notesTab.Name = "notesTab";
            this.notesTab.Size = new System.Drawing.Size(887, 551);
            this.notesTab.Text = "Notes";
            // 
            // panelControl10
            // 
            this.panelControl10.Controls.Add(this.showArchivedNotes);
            this.panelControl10.Controls.Add(this.showTemporaryNotes);
            this.panelControl10.Controls.Add(this.showAlertNotes);
            this.panelControl10.Controls.Add(this.showPermanentNotes);
            this.panelControl10.Controls.Add(this.showSystemNotes);
            this.panelControl10.Controls.Add(this.notesGrid);
            this.panelControl10.Location = new System.Drawing.Point(3, 0);
            this.panelControl10.Name = "panelControl10";
            this.panelControl10.Size = new System.Drawing.Size(886, 550);
            this.panelControl10.TabIndex = 5;
            // 
            // showArchivedNotes
            // 
            this.showArchivedNotes.Location = new System.Drawing.Point(292, 14);
            this.showArchivedNotes.MenuManager = this.barManager1;
            this.showArchivedNotes.Name = "showArchivedNotes";
            this.showArchivedNotes.Properties.Caption = "Archived Notes";
            this.showArchivedNotes.Size = new System.Drawing.Size(130, 20);
            this.showArchivedNotes.TabIndex = 6;
            this.showArchivedNotes.CheckedChanged += new System.EventHandler(this.showArchivedNotes_CheckedChanged);
            // 
            // showTemporaryNotes
            // 
            this.showTemporaryNotes.EditValue = true;
            this.showTemporaryNotes.Location = new System.Drawing.Point(211, 14);
            this.showTemporaryNotes.MenuManager = this.barManager1;
            this.showTemporaryNotes.Name = "showTemporaryNotes";
            this.showTemporaryNotes.Properties.Caption = "Temporary";
            this.showTemporaryNotes.Size = new System.Drawing.Size(75, 20);
            this.showTemporaryNotes.TabIndex = 5;
            this.showTemporaryNotes.CheckedChanged += new System.EventHandler(this.showTemporaryNotes_CheckedChanged);
            // 
            // showAlertNotes
            // 
            this.showAlertNotes.EditValue = true;
            this.showAlertNotes.Location = new System.Drawing.Point(152, 14);
            this.showAlertNotes.MenuManager = this.barManager1;
            this.showAlertNotes.Name = "showAlertNotes";
            this.showAlertNotes.Properties.Caption = "Alerts";
            this.showAlertNotes.Size = new System.Drawing.Size(53, 20);
            this.showAlertNotes.TabIndex = 4;
            this.showAlertNotes.CheckedChanged += new System.EventHandler(this.showAlertNotes_CheckedChanged);
            // 
            // showPermanentNotes
            // 
            this.showPermanentNotes.EditValue = true;
            this.showPermanentNotes.Location = new System.Drawing.Point(8, 14);
            this.showPermanentNotes.MenuManager = this.barManager1;
            this.showPermanentNotes.Name = "showPermanentNotes";
            this.showPermanentNotes.Properties.Caption = "Permanent";
            this.showPermanentNotes.Size = new System.Drawing.Size(75, 20);
            this.showPermanentNotes.TabIndex = 3;
            this.showPermanentNotes.CheckedChanged += new System.EventHandler(this.showPermanentNotes_CheckedChanged);
            // 
            // showSystemNotes
            // 
            this.showSystemNotes.EditValue = true;
            this.showSystemNotes.Location = new System.Drawing.Point(89, 14);
            this.showSystemNotes.MenuManager = this.barManager1;
            this.showSystemNotes.Name = "showSystemNotes";
            this.showSystemNotes.Properties.Caption = "System";
            this.showSystemNotes.Size = new System.Drawing.Size(57, 20);
            this.showSystemNotes.TabIndex = 2;
            this.showSystemNotes.CheckedChanged += new System.EventHandler(this.showSystemNotes_CheckedChanged);
            // 
            // notesGrid
            // 
            this.notesGrid.ContextMenuStrip = this.notesContextMenu;
            this.notesGrid.Location = new System.Drawing.Point(10, 39);
            this.notesGrid.MainView = this.gridView1;
            this.notesGrid.Name = "notesGrid";
            this.notesGrid.Size = new System.Drawing.Size(876, 508);
            this.notesGrid.TabIndex = 1;
            this.notesGrid.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // notesContextMenu
            // 
            this.notesContextMenu.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.notesContextMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem1,
            this.toolStripMenuItem2});
            this.notesContextMenu.Name = "contextMenuStrip1";
            this.notesContextMenu.Size = new System.Drawing.Size(104, 48);
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(103, 22);
            this.toolStripMenuItem1.Text = "Add";
            this.toolStripMenuItem1.Click += new System.EventHandler(this.toolStripMenuAdd_Click);
            // 
            // toolStripMenuItem2
            // 
            this.toolStripMenuItem2.Name = "toolStripMenuItem2";
            this.toolStripMenuItem2.Size = new System.Drawing.Size(103, 22);
            this.toolStripMenuItem2.Text = "Show";
            this.toolStripMenuItem2.Click += new System.EventHandler(this.toolStripMenuShow_Click);
            // 
            // gridView1
            // 
            this.gridView1.GridControl = this.notesGrid;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsView.ShowGroupPanel = false;
            this.gridView1.DoubleClick += new System.EventHandler(this.gridView1_DoubleClick);
            // 
            // submitBtn
            // 
            this.submitBtn.Enabled = false;
            this.submitBtn.Location = new System.Drawing.Point(428, 630);
            this.submitBtn.Name = "submitBtn";
            this.submitBtn.Size = new System.Drawing.Size(75, 23);
            this.submitBtn.TabIndex = 1;
            this.submitBtn.Text = "Submit";
            this.submitBtn.Click += new System.EventHandler(this.submitBtn_Click);
            // 
            // bar3
            // 
            this.bar3.BarName = "Status bar";
            this.bar3.CanDockStyle = DevExpress.XtraBars.BarCanDockStyle.Bottom;
            this.bar3.DockCol = 0;
            this.bar3.DockRow = 0;
            this.bar3.DockStyle = DevExpress.XtraBars.BarDockStyle.Bottom;
            this.bar3.OptionsBar.AllowQuickCustomization = false;
            this.bar3.OptionsBar.DrawDragBorder = false;
            this.bar3.OptionsBar.UseWholeRow = true;
            this.bar3.Text = "Status bar";
            // 
            // barManager2
            // 
            this.barManager2.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar5});
            this.barManager2.DockControls.Add(this.barDockControl1);
            this.barManager2.DockControls.Add(this.barDockControl2);
            this.barManager2.DockControls.Add(this.barDockControl3);
            this.barManager2.DockControls.Add(this.barDockControl4);
            this.barManager2.Form = this;
            this.barManager2.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.debtPlusIdItem,
            this.clientNameItem});
            this.barManager2.MaxItemId = 2;
            this.barManager2.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemTextEdit1});
            this.barManager2.StatusBar = this.bar5;
            // 
            // bar5
            // 
            this.bar5.BarName = "Status bar";
            this.bar5.CanDockStyle = DevExpress.XtraBars.BarCanDockStyle.Bottom;
            this.bar5.DockCol = 0;
            this.bar5.DockRow = 0;
            this.bar5.DockStyle = DevExpress.XtraBars.BarDockStyle.Bottom;
            this.bar5.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.debtPlusIdItem),
            new DevExpress.XtraBars.LinkPersistInfo(this.clientNameItem)});
            this.bar5.OptionsBar.AllowQuickCustomization = false;
            this.bar5.OptionsBar.DrawDragBorder = false;
            this.bar5.OptionsBar.UseWholeRow = true;
            this.bar5.Text = "Status bar";
            // 
            // debtPlusIdItem
            // 
            this.debtPlusIdItem.Caption = "barEditItem1";
            this.debtPlusIdItem.Edit = this.repositoryItemTextEdit1;
            this.debtPlusIdItem.Id = 0;
            this.debtPlusIdItem.Name = "debtPlusIdItem";
            toolTipItem2.Text = "OCS_Client.ClientId";
            superToolTip2.Items.Add(toolTipItem2);
            this.debtPlusIdItem.SuperTip = superToolTip2;
            this.debtPlusIdItem.Width = 96;
            // 
            // repositoryItemTextEdit1
            // 
            this.repositoryItemTextEdit1.AutoHeight = false;
            this.repositoryItemTextEdit1.Name = "repositoryItemTextEdit1";
            // 
            // clientNameItem
            // 
            this.clientNameItem.Caption = "None";
            this.clientNameItem.Id = 1;
            this.clientNameItem.Name = "clientNameItem";
            this.clientNameItem.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // barDockControl1
            // 
            this.barDockControl1.CausesValidation = false;
            this.barDockControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControl1.Location = new System.Drawing.Point(0, 0);
            this.barDockControl1.Size = new System.Drawing.Size(930, 0);
            // 
            // barDockControl2
            // 
            this.barDockControl2.CausesValidation = false;
            this.barDockControl2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControl2.Location = new System.Drawing.Point(0, 663);
            this.barDockControl2.Size = new System.Drawing.Size(930, 28);
            // 
            // barDockControl3
            // 
            this.barDockControl3.CausesValidation = false;
            this.barDockControl3.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControl3.Location = new System.Drawing.Point(0, 0);
            this.barDockControl3.Size = new System.Drawing.Size(0, 663);
            // 
            // barDockControl4
            // 
            this.barDockControl4.CausesValidation = false;
            this.barDockControl4.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControl4.Location = new System.Drawing.Point(930, 0);
            this.barDockControl4.Size = new System.Drawing.Size(0, 663);
            // 
            // forwardBtn
            // 
            this.forwardBtn.Enabled = false;
            this.forwardBtn.Location = new System.Drawing.Point(688, 630);
            this.forwardBtn.Name = "forwardBtn";
            this.forwardBtn.Size = new System.Drawing.Size(29, 20);
            this.forwardBtn.TabIndex = 11;
            this.forwardBtn.Text = ">";
            this.forwardBtn.Visible = false;
            this.forwardBtn.Click += new System.EventHandler(this.forwardBtn_Click);
            // 
            // backBtn
            // 
            this.backBtn.Enabled = false;
            this.backBtn.Location = new System.Drawing.Point(615, 630);
            this.backBtn.Name = "backBtn";
            this.backBtn.Size = new System.Drawing.Size(30, 20);
            this.backBtn.TabIndex = 12;
            this.backBtn.Text = "<";
            this.backBtn.Visible = false;
            this.backBtn.Click += new System.EventHandler(this.backBtn_Click);
            // 
            // bufferStatus
            // 
            this.bufferStatus.Location = new System.Drawing.Point(644, 630);
            this.bufferStatus.Name = "bufferStatus";
            this.bufferStatus.Properties.ReadOnly = true;
            this.bufferStatus.Size = new System.Drawing.Size(45, 20);
            this.bufferStatus.TabIndex = 21;
            this.bufferStatus.ToolTip = "addresses.PostalCode";
            this.bufferStatus.Visible = false;
            // 
            // toBeginBtn
            // 
            this.toBeginBtn.Enabled = false;
            this.toBeginBtn.Location = new System.Drawing.Point(585, 630);
            this.toBeginBtn.Name = "toBeginBtn";
            this.toBeginBtn.Size = new System.Drawing.Size(30, 20);
            this.toBeginBtn.TabIndex = 30;
            this.toBeginBtn.Text = "<<";
            this.toBeginBtn.Visible = false;
            this.toBeginBtn.Click += new System.EventHandler(this.toBeginBtn_Click);
            // 
            // toEndBtn
            // 
            this.toEndBtn.Enabled = false;
            this.toEndBtn.Location = new System.Drawing.Point(718, 630);
            this.toEndBtn.Name = "toEndBtn";
            this.toEndBtn.Size = new System.Drawing.Size(29, 20);
            this.toEndBtn.TabIndex = 31;
            this.toEndBtn.Text = ">>";
            this.toEndBtn.Visible = false;
            this.toEndBtn.Click += new System.EventHandler(this.toEndBtn_Click);
            // 
            // ClientForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(930, 691);
            this.Controls.Add(this.toEndBtn);
            this.Controls.Add(this.toBeginBtn);
            this.Controls.Add(this.bufferStatus);
            this.Controls.Add(this.backBtn);
            this.Controls.Add(this.forwardBtn);
            this.Controls.Add(this.submitBtn);
            this.Controls.Add(this.xtraTabControl1);
            this.Controls.Add(this.barDockControlLeft);
            this.Controls.Add(this.barDockControlRight);
            this.Controls.Add(this.barDockControlBottom);
            this.Controls.Add(this.barDockControlTop);
            this.Controls.Add(this.barDockControl3);
            this.Controls.Add(this.barDockControl4);
            this.Controls.Add(this.barDockControl2);
            this.Controls.Add(this.barDockControl1);
            this.Name = "ClientForm";
            this.Text = "ClientForm";
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl1)).EndInit();
            this.xtraTabControl1.ResumeLayout(false);
            this.clientTab.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl9)).EndInit();
            this.panelControl9.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            this.panelControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.program.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.timeDifferential.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.statusCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.timezone.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.queueName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.preferredLanguage.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.contactAttemptCount.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.contactType.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).EndInit();
            this.panelControl2.ResumeLayout(false);
            this.panelControl2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.aptNumber.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.aptSuite.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.streetSuffix.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.streetDirection.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.streetNumber.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.p1Salutation.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.p2Salutation.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.p2SSN.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.p2Email.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.p1SSN.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.p1Email.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.clientZipcode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.clientUSState.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.clientCity.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.clientAddress2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.clientAddress1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.p2Suffix.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.p2LastName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.p2MiddleName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.p2FirstName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.p1Suffix.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.p1LastName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.p1MiddleName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.p1FirstName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl3)).EndInit();
            this.panelControl3.ResumeLayout(false);
            this.panelControl3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ph5Number.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.userTimezoneComboBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPopupContainerEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPopupContainerEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ph4Number.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ph3Number.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ph2Number.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ph1Number.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ph5MTI.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ph5Type.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ph5Result.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ph4MTI.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ph4Type.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ph4Result.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ph3MTI.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ph3Type.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ph3Result.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ph2MTI.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ph2Type.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ph2Result.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ph1MTI.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ph1Type.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ph1Result.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl4)).EndInit();
            this.panelControl4.ResumeLayout(false);
            this.panelControl4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.stage4.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.stage3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.stage2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.stage1.Properties)).EndInit();
            this.accountTab.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl8)).EndInit();
            this.panelControl8.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl7)).EndInit();
            this.panelControl7.ResumeLayout(false);
            this.panelControl7.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.useHomeAddress.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.propertyZipcode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.propertyUSState.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.propertyCity.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.propertyAddress2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.propertyAddress1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl5)).EndInit();
            this.panelControl5.ResumeLayout(false);
            this.panelControl5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.actualSaleDate.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.actualSaleDate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.servicerIDNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt_TrialMod_Type.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.monthsPastDue.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.investorNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.loanModDate.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.loanModDate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.foreclosureDate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.daysPastDue.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.defaultReason.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.servicerLoanNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmtAppliedDate.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmtAppliedDate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nextPmtDate.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nextPmtDate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.servicer.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pastDueDate.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pastDueDate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trialMod.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl6)).EndInit();
            this.panelControl6.ResumeLayout(false);
            this.panelControl6.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtLastChanceFlag.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lastChanceDate.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lastChanceDate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.secondCounselDate.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.secondCounselDate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.duplicateFile.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.firstRPContact.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateFileReceived.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateFileReceived.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.agenyCase.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateClosed.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateClosed.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.batchName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.firstCounselDate.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.firstCounselDate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateFileModified.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateFileModified.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dt_ReferralDate.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dt_ReferralDate.Properties)).EndInit();
            this.notesTab.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl10)).EndInit();
            this.panelControl10.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.showArchivedNotes.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.showTemporaryNotes.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.showAlertNotes.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.showPermanentNotes.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.showSystemNotes.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.notesGrid)).EndInit();
            this.notesContextMenu.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bufferStatus.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraTab.XtraTabControl xtraTabControl1;
        private DevExpress.XtraTab.XtraTabPage clientTab;
        private DevExpress.XtraTab.XtraTabPage accountTab;
        private DevExpress.XtraTab.XtraTabPage notesTab;
        private DevExpress.XtraEditors.PanelControl panelControl2;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.LabelControl labelControl6;
        private DevExpress.XtraEditors.LabelControl labelControl5;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.LabelControl labelControl9;
        private DevExpress.XtraEditors.LabelControl labelControl8;
        private DevExpress.XtraEditors.LabelControl labelControl7;
        private DevExpress.XtraEditors.ComboBoxEdit contactType;
        private DevExpress.XtraEditors.TextEdit contactAttemptCount;
        private DevExpress.XtraEditors.TextEdit timeDifferential;
        private DevExpress.XtraEditors.ComboBoxEdit statusCode;
        private DevExpress.XtraEditors.ComboBoxEdit timezone;
        private DevExpress.XtraEditors.TextEdit queueName;
        private DevExpress.XtraEditors.ComboBoxEdit preferredLanguage;
        private DevExpress.XtraEditors.ComboBoxEdit p2Suffix;
        private DevExpress.XtraEditors.TextEdit p2LastName;
        private DevExpress.XtraEditors.TextEdit p2MiddleName;
        private DevExpress.XtraEditors.TextEdit p2FirstName;
        private DevExpress.XtraEditors.ComboBoxEdit p1Suffix;
        private DevExpress.XtraEditors.TextEdit p1LastName;
        private DevExpress.XtraEditors.TextEdit p1MiddleName;
        private DevExpress.XtraEditors.TextEdit p1FirstName;
        private DevExpress.XtraEditors.LabelControl labelControl11;
        private DevExpress.XtraEditors.LabelControl labelControl10;
        private DevExpress.XtraEditors.TextEdit p2SSN;
        private DevExpress.XtraEditors.TextEdit p2Email;
        private DevExpress.XtraEditors.TextEdit p1SSN;
        private DevExpress.XtraEditors.TextEdit p1Email;
        private DevExpress.XtraEditors.TextEdit clientZipcode;
        private DevExpress.XtraEditors.ComboBoxEdit clientUSState;
        private DevExpress.XtraEditors.TextEdit clientCity;
        private DevExpress.XtraEditors.TextEdit clientAddress2;
        private DevExpress.XtraEditors.TextEdit clientAddress1;
        private DevExpress.XtraEditors.LabelControl labelControl12;
        private DevExpress.XtraEditors.PanelControl panelControl3;
        private DevExpress.XtraEditors.ComboBoxEdit ph1MTI;
        private DevExpress.XtraEditors.LabelControl labelControl16;
        private DevExpress.XtraEditors.ComboBoxEdit ph1Type;
        private DevExpress.XtraEditors.LabelControl labelControl15;
        private DevExpress.XtraEditors.ComboBoxEdit ph1Result;
        private DevExpress.XtraEditors.LabelControl labelControl14;
        private DevExpress.XtraEditors.LabelControl labelControl13;
        private DevExpress.XtraEditors.ComboBoxEdit ph5MTI;
        private DevExpress.XtraEditors.LabelControl labelControl30;
        private DevExpress.XtraEditors.ComboBoxEdit ph5Type;
        private DevExpress.XtraEditors.LabelControl labelControl31;
        private DevExpress.XtraEditors.ComboBoxEdit ph5Result;
        private DevExpress.XtraEditors.LabelControl labelControl32;
        private DevExpress.XtraEditors.ComboBoxEdit ph4MTI;
        private DevExpress.XtraEditors.LabelControl labelControl27;
        private DevExpress.XtraEditors.ComboBoxEdit ph4Type;
        private DevExpress.XtraEditors.LabelControl labelControl28;
        private DevExpress.XtraEditors.ComboBoxEdit ph4Result;
        private DevExpress.XtraEditors.LabelControl labelControl29;
        private DevExpress.XtraEditors.ComboBoxEdit ph3MTI;
        private DevExpress.XtraEditors.LabelControl labelControl24;
        private DevExpress.XtraEditors.ComboBoxEdit ph3Type;
        private DevExpress.XtraEditors.LabelControl labelControl25;
        private DevExpress.XtraEditors.ComboBoxEdit ph3Result;
        private DevExpress.XtraEditors.LabelControl labelControl26;
        private DevExpress.XtraEditors.ComboBoxEdit ph2MTI;
        private DevExpress.XtraEditors.LabelControl labelControl21;
        private DevExpress.XtraEditors.ComboBoxEdit ph2Type;
        private DevExpress.XtraEditors.LabelControl labelControl22;
        private DevExpress.XtraEditors.ComboBoxEdit ph2Result;
        private DevExpress.XtraEditors.LabelControl labelControl23;
        private DevExpress.XtraEditors.LabelControl labelControl20;
        private DevExpress.XtraEditors.LabelControl labelControl19;
        private DevExpress.XtraEditors.LabelControl labelControl18;
        private DevExpress.XtraEditors.LabelControl labelControl17;
        private DevExpress.XtraEditors.PanelControl panelControl4;
        private DevExpress.XtraEditors.ComboBoxEdit stage4;
        private DevExpress.XtraEditors.LabelControl labelControl37;
        private DevExpress.XtraEditors.ComboBoxEdit stage3;
        private DevExpress.XtraEditors.LabelControl labelControl36;
        private DevExpress.XtraEditors.ComboBoxEdit stage2;
        private DevExpress.XtraEditors.ComboBoxEdit stage1;
        private DevExpress.XtraEditors.LabelControl labelControl35;
        private DevExpress.XtraEditors.LabelControl labelControl34;
        private DevExpress.XtraEditors.LabelControl labelControl33;
        private DevExpress.XtraEditors.SimpleButton submitBtn;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.DateEdit pmtAppliedDate;
        private DevExpress.XtraEditors.LabelControl labelControl41;
        private DevExpress.XtraEditors.LabelControl labelControl40;
        private DevExpress.XtraEditors.TextEdit defaultReason;
        private DevExpress.XtraEditors.LabelControl labelControl39;
        private DevExpress.XtraEditors.TextEdit foreclosureDate;
        private DevExpress.XtraEditors.TextEdit investorNo;
        private DevExpress.XtraEditors.DateEdit nextPmtDate;
        private DevExpress.XtraEditors.LabelControl labelControl45;
        private DevExpress.XtraEditors.LabelControl labelControl44;
        private DevExpress.XtraEditors.ComboBoxEdit trialMod;
        private DevExpress.XtraEditors.DateEdit pastDueDate;
        private DevExpress.XtraEditors.LabelControl labelControl43;
        private DevExpress.XtraEditors.TextEdit servicer;
        private DevExpress.XtraEditors.LabelControl labelControl42;
        private DevExpress.XtraEditors.TextEdit servicerIDNo;
        private DevExpress.XtraEditors.LabelControl labelControl49;
        private DevExpress.XtraEditors.DateEdit loanModDate;
        private DevExpress.XtraEditors.LabelControl labelControl48;
        private DevExpress.XtraEditors.TextEdit daysPastDue;
        private DevExpress.XtraEditors.LabelControl labelControl47;
        private DevExpress.XtraEditors.TextEdit servicerLoanNo;
        private DevExpress.XtraEditors.LabelControl labelControl46;
        private DevExpress.XtraEditors.PanelControl panelControl5;
        private DevExpress.XtraEditors.PanelControl panelControl6;
        private DevExpress.XtraEditors.CheckEdit duplicateFile;
        private DevExpress.XtraEditors.LabelControl labelControl56;
        private DevExpress.XtraEditors.LabelControl labelControl51;
        private DevExpress.XtraEditors.ComboBoxEdit firstRPContact;
        private DevExpress.XtraEditors.LabelControl labelControl50;
        private DevExpress.XtraEditors.DateEdit dateFileReceived;
        private DevExpress.XtraEditors.TextEdit agenyCase;
        private DevExpress.XtraEditors.LabelControl labelControl52;
        private DevExpress.XtraEditors.DateEdit dateClosed;
        private DevExpress.XtraEditors.LabelControl labelControl53;
        private DevExpress.XtraEditors.TextEdit batchName;
        private DevExpress.XtraEditors.LabelControl labelControl55;
        private DevExpress.XtraEditors.LabelControl labelControl57;
        private DevExpress.XtraEditors.DateEdit firstCounselDate;
        private DevExpress.XtraEditors.LabelControl labelControl60;
        private DevExpress.XtraEditors.LabelControl labelControl61;
        private DevExpress.XtraEditors.DateEdit dateFileModified;
        private DevExpress.XtraEditors.PanelControl panelControl7;
        private DevExpress.XtraEditors.CheckEdit useHomeAddress;
        private DevExpress.XtraEditors.LabelControl labelControl59;
        private DevExpress.XtraEditors.TextEdit propertyZipcode;
        private DevExpress.XtraEditors.ComboBoxEdit propertyUSState;
        private DevExpress.XtraEditors.TextEdit propertyCity;
        private DevExpress.XtraEditors.TextEdit propertyAddress2;
        private DevExpress.XtraEditors.TextEdit propertyAddress1;
        private DevExpress.XtraEditors.LabelControl labelControl58;
        private DevExpress.XtraEditors.ComboBoxEdit p1Salutation;
        private DevExpress.XtraEditors.ComboBoxEdit p2Salutation;
        private DevExpress.XtraGrid.GridControl notesGrid;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraEditors.TextEdit txt_TrialMod_Type;
        private DevExpress.XtraEditors.TextEdit monthsPastDue;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.DateEdit secondCounselDate;
        private DevExpress.XtraEditors.LabelControl labelControl64;
        private DevExpress.XtraEditors.LabelControl labelControl65;
        private DevExpress.XtraEditors.LabelControl labelControl63;
        private DevExpress.XtraEditors.LabelControl labelControl62;
        private DevExpress.XtraEditors.DateEdit actualSaleDate;
        private DevExpress.XtraEditors.LabelControl labelControl66;
        private DevExpress.XtraEditors.PanelControl panelControl8;
        private DevExpress.XtraEditors.PanelControl panelControl9;
        private DevExpress.XtraEditors.PanelControl panelControl10;
        private System.Windows.Forms.ContextMenuStrip notesContextMenu;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem2;
        private DevExpress.XtraBars.BarManager barManager1;
        private DevExpress.XtraBars.Bar bar2;
        private DevExpress.XtraBars.BarSubItem barSubItem_File;
        private DevExpress.XtraBars.BarDockControl barDockControlTop;
        private DevExpress.XtraBars.BarDockControl barDockControlBottom;
        private DevExpress.XtraBars.BarDockControl barDockControlLeft;
        private DevExpress.XtraBars.BarDockControl barDockControlRight;
        private DevExpress.XtraBars.BarSubItem barSubItem_Appointments;
        private DevExpress.XtraBars.BarSubItem barSubItem_Reports;
        private DevExpress.XtraBars.BarSubItem barSubItem_Help;
        private DevExpress.XtraBars.Bar bar3;
        private DevExpress.XtraBars.BarDockControl barDockControl3;
        private DevExpress.XtraBars.BarDockControl barDockControl4;
        private DevExpress.XtraBars.BarDockControl barDockControl2;
        private DevExpress.XtraBars.BarDockControl barDockControl1;
        private DevExpress.XtraBars.BarManager barManager2;
        private DevExpress.XtraBars.Bar bar5;
        private DevExpress.XtraBars.BarEditItem debtPlusIdItem;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit1;
        private DevExpress.XtraBars.BarStaticItem clientNameItem;
        private DevExpress.XtraEditors.TextEdit aptNumber;
        private DevExpress.XtraEditors.ComboBoxEdit aptSuite;
        private DevExpress.XtraEditors.ComboBoxEdit streetSuffix;
        private DevExpress.XtraEditors.ComboBoxEdit streetDirection;
        private DevExpress.XtraEditors.TextEdit streetNumber;
        private Data.Controls.PhoneNumber ph1Number;
        private Data.Controls.PhoneNumber ph5Number;
        private Data.Controls.PhoneNumber ph4Number;
        private Data.Controls.PhoneNumber ph3Number;
        private Data.Controls.PhoneNumber ph2Number;
        private DevExpress.XtraEditors.LabelControl labelControl67;
        private DevExpress.XtraEditors.TextEdit program;
        private DevExpress.XtraEditors.SimpleButton backBtn;
        private DevExpress.XtraEditors.SimpleButton forwardBtn;
        private DevExpress.XtraEditors.TextEdit bufferStatus;
        private DevExpress.XtraEditors.SimpleButton toEndBtn;
        private DevExpress.XtraEditors.SimpleButton toBeginBtn;
        private DevExpress.XtraEditors.SimpleButton callBtn4;
        private DevExpress.XtraEditors.SimpleButton callBtn3;
        private DevExpress.XtraEditors.SimpleButton callBtn2;
        private DevExpress.XtraEditors.SimpleButton callBtn5;
        private DevExpress.XtraEditors.SimpleButton callBtn1;
        private DevExpress.XtraBars.BarButtonItem barButtonItem2;
        private DevExpress.XtraBars.BarButtonItem restoreToActiveItem;
        private DevExpress.XtraBars.BarButtonItem barButtonItem4;
        private DevExpress.XtraBars.BarButtonItem barButtonItem1;
        private DevExpress.XtraBars.BarButtonItem barButtonItem5;
        private DevExpress.XtraBars.BarButtonItem barButtonItem6;
        private DevExpress.XtraBars.BarButtonItem barButtonItem7;
        private DevExpress.XtraBars.BarButtonItem barButtonItem8;
        private DevExpress.XtraBars.BarButtonItem barButtonItem3;
        private DevExpress.XtraBars.BarButtonItem barButtonItem9;
        private DevExpress.XtraBars.BarSubItem barSubItem1;
        private DevExpress.XtraBars.BarEditItem userTimezone;
        // private DevExpress.XtraEditors.Repository.RepositoryItemComboBox userTimezoneItems;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox userTimezoneComboBox;
        private DevExpress.XtraEditors.CheckEdit showSystemNotes;
        private DevExpress.XtraEditors.CheckEdit showTemporaryNotes;
        private DevExpress.XtraEditors.CheckEdit showAlertNotes;
        private DevExpress.XtraEditors.CheckEdit showPermanentNotes;
        private DevExpress.XtraBars.BarEditItem barEditItem1;
        private DevExpress.XtraEditors.Repository.RepositoryItemPopupContainerEdit repositoryItemPopupContainerEdit1;
        private DevExpress.XtraBars.BarEditItem barEditItem2;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit2;
        private DevExpress.XtraBars.BarEditItem barEditItem3;
        private DevExpress.XtraEditors.Repository.RepositoryItemPopupContainerEdit repositoryItemPopupContainerEdit2;
        private DevExpress.XtraEditors.DateEdit dt_ReferralDate;
        private DevExpress.XtraEditors.LabelControl labelControl68;
        private DevExpress.XtraEditors.LabelControl labelControl69;
        private DevExpress.XtraBars.BarSubItem barSubItem_Debtplus;
        private DevExpress.XtraBars.BarButtonItem barButtonItem_Debtplus;
        private DevExpress.XtraBars.BarButtonItem barButtonItem10;
        private DevExpress.XtraBars.BarButtonItem barButtonItem11;
        private DevExpress.XtraEditors.TextEdit txtLastChanceFlag;
        private DevExpress.XtraEditors.LabelControl labelControl54;
        private DevExpress.XtraEditors.DateEdit lastChanceDate;
        private DevExpress.XtraEditors.LabelControl labelControl38;
        internal DevExpress.XtraEditors.CheckEdit showArchivedNotes;
    }
}