﻿using System;
using System.Data;
using System.Windows.Forms;


namespace DebtPlus.UI.Housing.OCS
{
    internal partial class ClientSearchSelectForm : DebtPlus.Data.Forms.DebtPlusForm
    {
        /// <summary>
        /// Create an instance of our class
        /// </summary>
        /// <param name="SearchClass">Pointer to the parent search class</param>
        /// <remarks></remarks>
        public ClientSearchSelectForm(ClientSearchClass SearchClass) : this()
        {
            cls_client_search = SearchClass;
        }

        /// <summary>
        /// Create an instance of our class
        /// </summary>
        /// <param name="DatabaseInfo"></param>
        public ClientSearchSelectForm() : base()
        {
            InitializeComponent();

            // Add any initialization after the InitializeComponent() call
            grid_col_ssn.GroupFormat.Format = new DebtPlus.Utils.Format.SSN.MaskFormatter();
            grid_col_ssn.DisplayFormat.Format = new DebtPlus.Utils.Format.SSN.MaskFormatter();
            grid_col_client.GroupFormat.Format = new DebtPlus.Utils.Format.Client.CustomFormatter();
            grid_col_client.DisplayFormat.Format = new DebtPlus.Utils.Format.Client.CustomFormatter();

            // Ensure that our window is not hidden.
            Form frm = Form.ActiveForm;
            if (frm != null && !frm.InvokeRequired && frm.TopMost)
            {
                TopMost = true;
            }

            RegisterHandlers();
        }

        /// <summary>
        /// Register the event handlers for this form
        /// </summary>
        private void RegisterHandlers()
        {
            this.Load += new EventHandler(ClientListForm_Load);
            GridView1.FocusedRowChanged += new DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventHandler(GridView1_FocusedRowChanged);
            chk_view_preview.CheckedChanged += new EventHandler(chk_view_preview_CheckedChanged);
            Button_More.Click += new EventHandler(Button_More_Click);
            Button_Print.Click += new EventHandler(Button_Print_Click);
            Button_OK.Click += new EventHandler(Button_OK_Click);
            GridView1.DoubleClick += new EventHandler(GridView1_DoubleClick);
            GridView1.Layout += new EventHandler(GridView1_Layout);
        }

        /// <summary>
        /// Remove the event processing handlers for this form
        /// </summary>
        private void UnRegisterHandlers()
        {
            this.Load -= new EventHandler(ClientListForm_Load);
            GridView1.FocusedRowChanged -= new DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventHandler(GridView1_FocusedRowChanged);
            chk_view_preview.CheckedChanged -= new EventHandler(chk_view_preview_CheckedChanged);
            Button_More.Click -= new EventHandler(Button_More_Click);
            Button_Print.Click -= new EventHandler(Button_Print_Click);
            Button_OK.Click -= new EventHandler(Button_OK_Click);
            GridView1.DoubleClick -= new EventHandler(GridView1_DoubleClick);
            GridView1.Layout -= new EventHandler(GridView1_Layout);
        }

        protected internal event EventHandler NextPage;
        private readonly ClientSearchClass cls_client_search;
        private Int32 TargetClient = Int32.MinValue;
        private Int32 TargetOCSClient = Int32.MinValue;

        /// <summary>
        /// Applicaiton name for the saving of the form size/location
        /// </summary>
        /// <returns></returns>
        protected string ApplicationName()
        {
            System.Reflection.Assembly _asm = System.Reflection.Assembly.GetExecutingAssembly();
            return _asm.GetName().Name + Name;
        }

        /// <summary>
        /// Handle the CLICK event on the MORE button
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Button_More_Click(object sender, EventArgs e)
        {
            if( NextPage != null )
            {
                NextPage(this, new EventArgs());
            }
        }

        /// <summary>
        /// Ensure that the focused row is valid
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void GridView1_FocusedRowChanged(Object sender, DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs e)
        {
            SelectFocusedRow(e.FocusedRowHandle);
        }

        /// <summary>
        /// Retrieve the client when the focused row changes
        /// </summary>
        /// <param name="ControlRow"></param>
        private void SelectFocusedRow(Int32 ControlRow)
        {

            // if there is a row then process the double-click event. Ignore it if there is no row.
            DataRow row;
            if( ControlRow >= 0 )
            {
                row = (System.Data.DataRow) GridView1.GetDataRow(ControlRow);
            }
            else
            {
                row = null;
            }

            // From the datasource, we can find the Client in the table.
            if (row != null && row["ID"] != DBNull.Value)
            {
                TargetClient = Convert.ToInt32(row["Client"]);
                this.TargetOCSClient = Convert.ToInt32(row["ID"]);
            }
            else
            {
                TargetClient = Int32.MinValue;
            }

            // Enable the OK button if the creditor is specified
            Button_OK.Enabled = TargetClient >= 0;
        }

        /// <summary>
        /// Handle the DOUBLE CLICK event for the GridView
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void GridView1_DoubleClick(Object sender, EventArgs e)
        {
            if( Button_OK.Enabled )
            {
                // Find the row targeted as the double-click item
                DevExpress.XtraGrid.Views.Grid.GridView gv = ((DevExpress.XtraGrid.Views.Grid.GridView) sender);
                DevExpress.XtraGrid.GridControl ctl = ((DevExpress.XtraGrid.GridControl) gv.GridControl);
                DevExpress.XtraGrid.Views.Grid.ViewInfo.GridHitInfo hi = gv.CalcHitInfo((ctl.PointToClient(System.Windows.Forms.Control.MousePosition)));

                // if there is a row then process the double-click event. Ignore it if there is no row.
                DataRow row = null;
                System.Int32 ControlRow = hi.RowHandle;
                if( ControlRow >= 0 )
                {
                    row = (System.Data.DataRow) gv.GetDataRow(ControlRow);
                }

                // From the datasource, we can find the Client in the table.
                if( row != null )
                {
                    TargetClient = Convert.ToInt32(row["Client"]);
                    this.TargetOCSClient = Convert.ToInt32(row["ID"]);
                    Button_OK.PerformClick();
                }
            }
        }

        /// <summary>
        /// Return the path to the saved configuration data file
        /// </summary>
        /// <returns></returns>
        protected virtual string XMLBasePath
        {
            get
            {
                if (DesignMode)
                {
                    return string.Empty;
                }
                string BasePath = System.IO.Path.Combine(System.Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData), "DebtPlus");
                return System.IO.Path.Combine(BasePath, "Client.Search");
            }
        }

        protected const string XMLFileName = "ClientSearchResults.grid.xml";

        /// <summary>
        /// WHen the layout is changed, update the saved configuration file.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void GridView1_Layout(Object sender, EventArgs e)
        {
            try
            {
                string PathName = XMLBasePath;
                if( ! string.IsNullOrEmpty(PathName) )
                {
                    if( ! System.IO.Directory.Exists(PathName) )
                    {
                        System.IO.Directory.CreateDirectory(PathName);
                    }

                    string FileName = System.IO.Path.Combine(PathName, XMLFileName);
                    GridView1.SaveLayoutToXml(FileName);
                }

            }
            catch( Exception ex )
            {
                DebtPlus.Svc.Common.ErrorHandling.HandleErrors(ex);
            }
        }

        /// <summary>
        /// Handle the FORM LOAD event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ClientListForm_Load(Object sender, EventArgs e)
        {
            // Remove the handlers from tripping during the initialization routine.
            UnRegisterHandlers();
            try
            {
                // Set the checkbox status appropriately.
                bool Enabled = ShowAddresses;
                chk_view_preview.Checked = Enabled;
                GridView1.OptionsView.ShowPreview = Enabled;

                // Ensure that our window is not hidden.
                Form frm = Owner as Form;
                if (frm != null && frm.TopMost)
                {
                    TopMost = true;
                }

                GridView1.BestFitColumns();

                // Find the base path to the saved file location
                string PathName = XMLBasePath;
                if( ! string.IsNullOrEmpty(PathName) )
                {
                    string FileName = System.IO.Path.Combine(PathName, XMLFileName);
                    if( System.IO.File.Exists(FileName) )
                    {
                        GridView1.RestoreLayoutFromXml(FileName);
                    }
                }

                // Set the Client to the first row in the table
                TargetClient = Int32.MinValue;
                if( GridView1.RowCount > 0 )
                {
                    GridView1.FocusedRowHandle = 0;
                }

                // Set the Client from the focused row
                SelectFocusedRow(GridView1.FocusedRowHandle);
            }
            catch( Exception ex )
            {
                DebtPlus.Svc.Common.ErrorHandling.HandleErrors(ex);
            }
            finally
            {
                RegisterHandlers();
            }
        }

        /// <summary>
        /// Process the SHow Client Address checkbox CHANGE event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void chk_view_preview_CheckedChanged(object sender, EventArgs e)
        {
            UnRegisterHandlers();
            try
            {
                ShowAddresses = chk_view_preview.Checked;
                GridView1.OptionsView.ShowPreview = ShowAddresses;
            }
            finally
            {
                RegisterHandlers();
            }
        }

        /// <summary>
        /// Should the client address information be displayed?
        /// </summary>
        private static bool ShowAddresses
        {
            get
            {
                bool value = false;
                string KeyField = System.IO.Path.Combine(DebtPlus.Configuration.AssemblyInfo.InstallationRegistryKey, "Options");

                // Start with the system setting.
                using( Microsoft.Win32.RegistryKey key = Microsoft.Win32.Registry.LocalMachine.OpenSubKey(KeyField, false))
                {
                    if( key != null )
                    {
                        string stringValue = DebtPlus.Utils.Nulls.DStr(key.GetValue("Show Client Address", "False"));
                        bool Answer;
                        if(Boolean.TryParse( stringValue, out Answer ))
                        {
                            value = Answer;
                        }
                    }
                }

                // Look for the local user to override the system setting
                using( Microsoft.Win32.RegistryKey key = Microsoft.Win32.Registry.CurrentUser.OpenSubKey(KeyField, false))
                {
                    if( key != null )
                    {
                        bool Answer;
                        string stringValue = DebtPlus.Utils.Nulls.DStr(key.GetValue("Show Client Address", value.ToString()));
                        if(Boolean.TryParse( stringValue, out Answer ))
                        {
                            value = Answer;
                        }
                    }
                }

                return value;
            }

            set
            {
                try
                {
                    string KeyField = System.IO.Path.Combine(DebtPlus.Configuration.AssemblyInfo.InstallationRegistryKey, "Options");
                    using (Microsoft.Win32.RegistryKey key = Microsoft.Win32.Registry.CurrentUser.OpenSubKey(KeyField, true))
                    {
                        key.SetValue("Show Client Address", value.ToString());
                    }
                }
                catch { }
            }
        }

        /// <summary>
        /// Handle the PRINT event button click
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Button_Print_Click(object sender, EventArgs e)
        {
            bool Success = false;

            Cursor currentCursor = Cursor.Current;
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                if( GridControl1.IsPrintingAvailable )
                {
                    GridControl1.ShowPrintPreview();
                    Success = true;
                }
            }
            catch( Exception ex )
            {
                DebtPlus.Svc.Common.ErrorHandling.HandleErrors(ex);
            }
            finally
            {
                System.Windows.Forms.Cursor.Current = currentCursor;
            }

            if( ! Success )
            {
                DebtPlus.Data.Forms.MessageBox.Show(this, "Sorry, that function is not implemented at this time. It will be shortly.", "Function not available", MessageBoxButtons.OK);
            }
        }

        /// <summary>
        /// Handle the OK button CLICK event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Button_OK_Click(Object sender, EventArgs e)
        {
            if( cls_client_search.SetClient(TargetClient, TargetOCSClient) )
            {
                DialogResult = System.Windows.Forms.DialogResult.OK;
            }
        }

        private void Button_More_Click_1(object sender, EventArgs e)
        {

        }
    }
}