﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DebtPlus.Interfaces.Desktop;

namespace DebtPlus.UI.Housing.OCS
{
    public class ClientSearchLauncher : IDesktopMainline
    {
        public void OldAppMain(string[] args)
        {
            var t = new System.Threading.Thread(() =>
            {
                try
                {
                    using (var searchForm = new ClientSearchClass())
                    {
                        if (searchForm.ShowDialog() != System.Windows.Forms.DialogResult.OK)
                        {
                            return;
                        }

                        using (var bc = new DebtPlus.LINQ.BusinessContext())
                        {
                            using (var clientForm = new ClientForm(bc, searchForm.repository, searchForm.foundClient))
                            {
                                clientForm.ShowDialog();
                            }
                            bc.SubmitChanges();
                        }
                    }
                }

                catch (System.Exception ex)
                {
                    DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex);
                }
            });
            t.SetApartmentState(System.Threading.ApartmentState.STA);
            t.IsBackground = false;
            t.Start();
        }
    }
}
