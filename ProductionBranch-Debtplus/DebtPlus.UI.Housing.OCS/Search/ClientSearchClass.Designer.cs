namespace DebtPlus.UI.Housing.OCS
{
    partial class ClientSearchClass
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            FormDispose(disposing);
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Search_Field = new DevExpress.XtraEditors.ComboBoxEdit();
            this.Search_Operator = new DevExpress.XtraEditors.ComboBoxEdit();
            this.Label1 = new DevExpress.XtraEditors.LabelControl();
            this.Button_OK = new DevExpress.XtraEditors.SimpleButton();
            this.Button_Cancel = new DevExpress.XtraEditors.SimpleButton();
            this.Search_Value = new DevExpress.XtraEditors.ComboBoxEdit();
            this.archivedOnly = new DevExpress.XtraEditors.CheckEdit();
            ((System.ComponentModel.ISupportInitialize)(this.DxErrorProvider1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Search_Field.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Search_Operator.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Search_Value.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.archivedOnly.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // Search_Field
            // 
            this.Search_Field.Location = new System.Drawing.Point(8, 43);
            this.Search_Field.Name = "Search_Field";
            this.Search_Field.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Style3D;
            this.Search_Field.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.Search_Field.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.Search_Field.Size = new System.Drawing.Size(192, 22);
            this.Search_Field.TabIndex = 4;
            this.Search_Field.ToolTip = "Select the major criteria that you wish to perform a search from one of the items" +
                " in this list.";
            this.Search_Field.ToolTipController = this.ToolTipController1;
            // 
            // Search_Operator
            // 
            this.Search_Operator.Location = new System.Drawing.Point(206, 43);
            this.Search_Operator.Name = "Search_Operator";
            this.Search_Operator.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Style3D;
            this.Search_Operator.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.Search_Operator.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.Search_Operator.Size = new System.Drawing.Size(128, 22);
            this.Search_Operator.TabIndex = 5;
            this.Search_Operator.ToolTip = "Choose how the object is related to the value on the right from one of the items " +
                "on this list";
            this.Search_Operator.ToolTipController = this.ToolTipController1;
            // 
            // Label1
            // 
            this.Label1.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label1.Appearance.ForeColor = System.Drawing.Color.Red;
            this.Label1.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Label1.Location = new System.Drawing.Point(8, 12);
            this.Label1.Name = "Label1";
            this.Label1.Size = new System.Drawing.Size(108, 20);
            this.Label1.TabIndex = 0;
            this.Label1.Text = "Client Search";
            this.Label1.ToolTipController = this.ToolTipController1;
            this.Label1.UseMnemonic = false;
            // 
            // Button_OK
            // 
            this.Button_OK.Location = new System.Drawing.Point(277, 112);
            this.Button_OK.Name = "Button_OK";
            this.Button_OK.Size = new System.Drawing.Size(75, 25);
            this.Button_OK.TabIndex = 7;
            this.Button_OK.Text = "&OK";
            this.Button_OK.ToolTipController = this.ToolTipController1;
            // 
            // Button_Cancel
            // 
            this.Button_Cancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.Button_Cancel.Location = new System.Drawing.Point(384, 112);
            this.Button_Cancel.Name = "Button_Cancel";
            this.Button_Cancel.Size = new System.Drawing.Size(75, 25);
            this.Button_Cancel.TabIndex = 8;
            this.Button_Cancel.Text = "&Cancel";
            this.Button_Cancel.ToolTipController = this.ToolTipController1;
            this.Button_Cancel.Click += new System.EventHandler(this.Button_Cancel_Click);
            // 
            // Search_Value
            // 
            this.Search_Value.Location = new System.Drawing.Point(344, 43);
            this.Search_Value.Name = "Search_Value";
            this.Search_Value.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.Search_Value.Properties.AutoComplete = false;
            this.Search_Value.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Style3D;
            this.Search_Value.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.Search_Value.Size = new System.Drawing.Size(144, 22);
            this.Search_Value.TabIndex = 1;
            this.Search_Value.ToolTip = "Enter or choose the search criteria for the Client. if( \"Between\" is used, this i" +
                "s the lower limit.";
            this.Search_Value.ToolTipController = this.ToolTipController1;
            // 
            // archivedOnly
            // 
            this.archivedOnly.Location = new System.Drawing.Point(6, 110);
            this.archivedOnly.Name = "archivedOnly";
            this.archivedOnly.Properties.Caption = "Archived HistoryOnly";
            this.archivedOnly.Size = new System.Drawing.Size(128, 19);
            this.archivedOnly.TabIndex = 9;
            // 
            // ClientSearchClass
            // 
            this.AcceptButton = this.Button_OK;
            this.CancelButton = this.Button_Cancel;
            this.ClientSize = new System.Drawing.Size(498, 151);
            this.Controls.Add(this.archivedOnly);
            this.Controls.Add(this.Search_Value);
            this.Controls.Add(this.Button_Cancel);
            this.Controls.Add(this.Button_OK);
            this.Controls.Add(this.Label1);
            this.Controls.Add(this.Search_Operator);
            this.Controls.Add(this.Search_Field);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "ClientSearchClass";
            this.Text = "Client Search";
            ((System.ComponentModel.ISupportInitialize)(this.DxErrorProvider1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Search_Field.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Search_Operator.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Search_Value.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.archivedOnly.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }
        internal DevExpress.XtraEditors.ComboBoxEdit Search_Field;
        internal DevExpress.XtraEditors.ComboBoxEdit Search_Operator;
        internal DevExpress.XtraEditors.LabelControl Label1;
        internal DevExpress.XtraEditors.SimpleButton Button_OK;
        internal DevExpress.XtraEditors.SimpleButton Button_Cancel;
        internal DevExpress.XtraEditors.ComboBoxEdit Search_Value;
#endregion
        private DevExpress.XtraEditors.CheckEdit archivedOnly;
    }
}

