using System;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Text;
using System.Windows.Forms;
using DebtPlus.Svc.OCS;
using DebtPlus.OCS.Domain;
using System.Collections.Generic;
using DebtPlus.Interfaces.Desktop;

namespace DebtPlus.UI.Housing.OCS
{
    public partial class ClientSearchClass : DebtPlus.Data.Forms.DebtPlusForm, DebtPlus.Interfaces.Client.IClientSearch
    {
        // Information about the Client search
        ClientSearchSelectForm formClientList;      // This is our form listing the results of the search.
        protected Int32 lastClient;                 // Last Client in the selection list. Used for the "MORE" function as a starting point.
        protected string selectClause;              // Selection clause. Used to build the SQL statement
        private DataSet ds = new DataSet("ds");     // Dataset to hold the items
        private Int32 ocsId = Int32.MinValue;

        private SearchProvider provider;
        public OCSDbMapper repository = null;
        public OCSClient foundClient = null;

        private int numberClientsLoaded = 0;
        
        /// <summary>
        /// Create an instance of our new class
        /// </summary>
        /// <param name="DatabaseInfo"></param>        
        public ClientSearchClass() : base()
        {
            DebtPlus.LINQ.SQLInfoClass SqlInfo = DebtPlus.LINQ.SQLInfoClass.getDefault();
            var connectionString = DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString;
            DebtPlus.LINQ.BusinessContext bc = new LINQ.BusinessContext(connectionString);
            repository = new OCSDbMapper(bc);
            provider = new SearchProvider(repository);

            InitializeComponent();
            ClientId = Int32.MinValue;
            RegisterHandlers();        
        }

        public ClientSearchClass(SearchProvider pro, OCSDbMapper repo): this()
        {
            provider = pro;
            repository = repo;
        }

        /// <summary>
        /// Register the event handlers
        /// </summary>
        private void RegisterHandlers()
        {
            this.Load += new EventHandler(ClientSearchClass_Load);
            
            Search_Field.SelectedIndexChanged += new EventHandler(Combo_Category_SelectedIndexChanged);
            Search_Field.SelectedValueChanged += new EventHandler(Combo_Category_SelectedValueChanged);
            Search_Field.TextChanged += new EventHandler(Combo_Category_ValueChanged);
            
            Search_Operator.SelectedIndexChanged += new EventHandler(Combo_Relation_SelectedIndexChanged);
            
            Search_Value.Enter += new EventHandler(Combo_Value1_Enter);
            Search_Value.Leave += new EventHandler(Combo_Value1_Leave);
            Search_Value.TextChanged += new EventHandler(Form_Changed);
            Search_Value.SelectedIndexChanged += new EventHandler(Form_Changed);
                        
            Button_OK.Click += new EventHandler(Button_OK_Click); 
        }

        /// <summary>
        /// Remove the registration for the events
        /// </summary>
        private void UnRegisterHandlers()
        {
            this.Load -= new EventHandler(ClientSearchClass_Load);

            Search_Field.SelectedIndexChanged -= new EventHandler(Combo_Category_SelectedIndexChanged);
            Search_Field.SelectedValueChanged -= new EventHandler(Combo_Category_SelectedValueChanged);
            Search_Field.TextChanged -= new EventHandler(Combo_Category_ValueChanged);
            
            Search_Operator.SelectedIndexChanged -= new EventHandler(Combo_Relation_SelectedIndexChanged);
            
            Search_Value.Enter -= new EventHandler(Combo_Value1_Enter);
            Search_Value.Leave -= new EventHandler(Combo_Value1_Leave);
            Search_Value.TextChanged -= new EventHandler(Form_Changed);
            Search_Value.SelectedIndexChanged -= new EventHandler(Form_Changed);
                        
            Button_OK.Click -= new EventHandler(Button_OK_Click); 
        }

        /// <summary>
        /// Call the validating routine that the user may have set
        /// </summary>
        /// <param name="e"></param>
        /// <remarks></remarks>
        protected void RaiseValidating(CancelEventArgs e)
        {
            OnValidating(e);
        }

        /// <summary>
        /// Dispose the items generated in the form
        /// </summary>
        /// <remarks></remarks>
        private void FormDispose(bool disposing)
        {
            // Dispose the larger objects
            if( disposing )
            {
                if( ds != null )
                {
                    ds.Dispose();
                }
                if( formClientList != null )
                {
                    formClientList.Dispose();
                }
            }
        }

#region Combobox Values
        /// <summary>
        /// return the label associated with the selected category
        /// </summary>
        /// <returns></returns>
        /// <remarks></remarks>
        protected string CategoryLabel
        {
            get
            {
                string Answer = string.Empty;
                if( Search_Field.SelectedIndex >= 0 )
                {
                    Answer = Convert.ToString(((DebtPlus.Data.Controls.ComboboxItem) Search_Field.SelectedItem).value);
                }
                return Answer;
            }
        }

        /// <summary>
        /// return the label associated with the selected relation
        /// </summary>
        /// <returns></returns>
        /// <remarks></remarks>
        protected string RelationLabel
        {
            get
            {
                string Answer = string.Empty;
                if( Search_Operator.SelectedIndex >= 0 )
                {
                    Answer = Convert.ToString(((DebtPlus.Data.Controls.ComboboxItem) Search_Operator.SelectedItem).value);
                }
                return Answer;
            }
        }
#endregion

#region Form Navigation
        /// <summary>
        /// Process the load of the category list
        /// </summary>
        /// <remarks></remarks>
        protected virtual void Load_Category()
        {
            Search_Field.Properties.Items.Clear();

            foreach (var lh in provider.SearchFields) {
                Search_Field.Properties.Items.Add(new DebtPlus.Data.Controls.ComboboxItem(lh.Key, lh.Value));
            }

            Search_Field.SelectedIndex = 0;
        }

        /// <summary>
        /// Process the load of the relation list
        /// </summary>
        /// <remarks></remarks>
        protected void Load_Relation()
        {
            Search_Operator.Properties.Items.Clear();

            DebtPlus.Data.Controls.ComboboxItem field = (DebtPlus.Data.Controls.ComboboxItem)Search_Field.SelectedItem;

            foreach (var op in provider.GetOperators(field.value)) {
                Search_Operator.Properties.Items.Add(new DebtPlus.Data.Controls.ComboboxItem(op.Key, op.Value));
            }
            
            Search_Operator.SelectedIndex = 0;
            Load_Value_1();
        }

        /// <summary>
        /// Process the load of the value #1 list
        /// </summary>
        /// <remarks></remarks>
        protected void Load_Value_1() 
        {
            // Load the list of previous clients. Select the first one.
            string SavedText = Search_Value.Text;

            // Remove the items from the list so that they may be replaced en-mass.
            Search_Value.Properties.ReadOnly = false;
            Search_Value.Properties.Items.Clear();

            using (DebtPlus.Data.MRU mruList = new DebtPlus.Data.MRU(MruKey()))
            {
                string[] mruValues = mruList.GetList;
                foreach (string mruString in mruValues)
                {
                    Search_Value.Properties.Items.Add(new DebtPlus.Data.Controls.ComboboxItem(mruString));
                }
            }

            // Restore the previous text into the input area
            Search_Value.SelectedIndex = -1;
            Search_Value.Text = SavedText;
        }
                
        /// <summary>
        /// Do the load sequence for the form
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// <remarks></remarks>
        private void ClientSearchClass_Load(Object sender, EventArgs e)
        {
            UnRegisterHandlers();                    // Prevent evnet tripping when we are configuring the form.
            try
            {
                // Load the comboboxes
                Load_Category();                     // Load the category information
                Load_Relation();                     // Load the relations for the Client value
                EnableOK();                          // Disable the OK button for now
            }
            finally
            {
                RegisterHandlers();
            }
        }

        /// <summary>
        /// Process the change of the category
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// <remarks></remarks>
        protected void Combo_Category_SelectedValueChanged(object sender, EventArgs e)
        {
            Load_Relation();
            EnableOK();
        }

        /// <summary>
        /// Handle the change in the relation list
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// <remarks></remarks>
        protected void Combo_Relation_SelectedIndexChanged(object sender, EventArgs e)
        {
            EnableOK();
        }

#endregion

#region Form Validation
        /// <summary>
        /// Should the "OK" button be enabled?
        /// </summary>
        /// <remarks></remarks>
        protected virtual void EnableOK()
        {
            string CategorySelected = CategoryLabel;
            string RelationSelected = RelationLabel;
            
            // Validate the proper parameters
            if( CategorySelected == string.Empty )
            {
                Button_OK.Enabled = false;
                return;
            }

            if( RelationSelected == string.Empty )
            {
                Button_OK.Enabled = false;
                return;
            }

            //logic to validate right-hand side of search expression
            if (String.IsNullOrWhiteSpace(Search_Value.Text)) {
                Button_OK.Enabled = false;
                return;
            }

            // Everything is valid. Enable the OK button.
            Button_OK.Enabled = true;
        }

#endregion

#region Combo_Value
        /// <summary>
        /// Process the entry to the input fields. Select all of the text.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// <remarks></remarks>
        protected void Combo_Value1_Enter(Object sender, EventArgs e)
        {
            ((DevExpress.XtraEditors.ComboBoxEdit) sender).SelectAll();
        }

        /// <summary>
        /// Process the change in the second input box.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// <remarks></remarks>
        protected void Form_Changed(Object sender, EventArgs e)
        {
            EnableOK();
        }

        /// <summary>
        /// Leave the input fields.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// <remarks></remarks>
        protected void Combo_Value1_Leave(Object sender, EventArgs e)
        {
            DevExpress.XtraEditors.ComboBoxEdit cbx = (DevExpress.XtraEditors.ComboBoxEdit) sender;
            cbx.Text = cbx.Text.Trim();
            EnableOK();
        }
#endregion
        
        /// <summary>
        /// Handle the "OK" button. Do the database search operation.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// <remarks></remarks>
        protected void Button_OK_Click(Object sender, EventArgs e)
        {            
            DebtPlus.Data.Controls.ComboboxItem field = (DebtPlus.Data.Controls.ComboboxItem)Search_Field.SelectedItem;
            DebtPlus.Data.Controls.ComboboxItem op = (DebtPlus.Data.Controls.ComboboxItem)Search_Operator.SelectedItem;

            int _;
            if ((SearchProvider.LH_VALUE)field.value == SearchProvider.LH_VALUE.DebtPlusID && !Int32.TryParse(Search_Value.Text, out _))
            {
                DebtPlus.Data.Forms.MessageBox.Show("This number is too long to be a Client Id. Please make a new choice.", "Invalid Input", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Stop);
                return;
            }

            DataTable tbl = provider.Search(field.value, op.value, Search_Value.Text, numberClientsLoaded, SearchLimit(), archivedOnly.Checked);

            //see DebtPlus.UI.Client.Widgets...
            if (tbl == null)
            {
                return;
            }
                        
            if (tbl.Rows.Count == 0)
            {
                DebtPlus.Data.Forms.MessageBox.Show("There are no matches in the database to the search criteria that you entered. Please make a new choice.", "Information Not Found", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Stop);
                return;
            }

            if (tbl.Rows.Count == 1)
            {
                var _clientId = Convert.ToInt32(tbl.Rows[0]["Client"].ToString());
                var _ocsId = Convert.ToInt32(tbl.Rows[0]["ID"].ToString());

                if (SetClient(_clientId,_ocsId))
                {
                    DialogResult = System.Windows.Forms.DialogResult.OK;
                }

                foundClient = repository.GetByOCSId(ocsId);
                this.Close();
                
                return;
            }

            // Create a form to hold the excess
            formClientList = new ClientSearchSelectForm(this);
            System.Windows.Forms.DialogResult result;
            using (formClientList)
            {
                numberClientsLoaded = tbl.Rows.Count;
                formClientList.NextPage += new EventHandler(formClientList_NextPage);

                // Load the items into the form and enable the controls
                LoadListControl(tbl);

                // Run the dialog
                result = formClientList.ShowDialog();                               

                // Release the form
                formClientList.NextPage -= new EventHandler(formClientList_NextPage);
                numberClientsLoaded = 0;
            }
            formClientList = null;

            // If the dialog completed successfully then complete the search the same way.
            if (result == System.Windows.Forms.DialogResult.OK)
            {              
                foundClient = repository.GetByOCSId(ocsId);
                DialogResult = System.Windows.Forms.DialogResult.OK;
                this.Close();
            }
        }

        void formClientList_NextPage(object sender, EventArgs e)
        {
            DebtPlus.Data.Controls.ComboboxItem field = (DebtPlus.Data.Controls.ComboboxItem)Search_Field.SelectedItem;
            DebtPlus.Data.Controls.ComboboxItem op = (DebtPlus.Data.Controls.ComboboxItem)Search_Operator.SelectedItem;

            DataTable tbl = provider.Search(field.value, op.value, Search_Value.Text, numberClientsLoaded, SearchLimit(), archivedOnly.Checked);

            numberClientsLoaded += tbl.Rows.Count;

            LoadListControl(tbl);
        }

        /// <summary>
        /// Load the table into the display form.
        /// </summary>
        /// <param name="tbl"></param>
        /// <remarks></remarks>
        protected void LoadListControl(DataTable tbl)
        {
            Int32 limit = SearchLimit();

            // Enable or disable the MORE button if there are too many items
            formClientList.Button_More.Visible = false;
            if (limit > 0 && tbl.Rows.Count >= limit)
            {
                formClientList.Button_More.Visible = true;
            }

            // Bind the dataset to the grid control and populate it.
            formClientList.GridControl1.DataSource = tbl;
            formClientList.GridControl1.RefreshDataSource();
        }
                
#region Load the Client List Form Items
        /// <summary>
        /// Find the number of clients to return in the selection data
        /// </summary>
        /// <returns></returns>
        /// <remarks></remarks>
        protected static Int32 SearchLimit ()
        {
            Microsoft.Win32.RegistryKey RegKey = Microsoft.Win32.Registry.LocalMachine;
            Int32 Result = 1000;

            // Look in the machine values for the defaults for all users
            try
            {
                RegKey = RegKey.OpenSubKey(string.Format(@"{0}\options", DebtPlus.Configuration.AssemblyInfo.InstallationRegistryKey), false);
                if( RegKey != null )
                {
                    Object obj = RegKey.GetValue("Client search limit", string.Empty);
                    Int32 Answer = -1;
                    if( obj != null && Int32.TryParse(Convert.ToString(obj), out Answer) && Answer > 0 )
                    {
                        Result = Answer;
                    }
                }
            }
            catch( Exception ex )
            {
                DebtPlus.Svc.Common.ErrorHandling.HandleErrors(ex);
            }
            finally
            {
                if( RegKey != null )
                {
                    RegKey.Close();
                }
            }

            // Allow the user to override the machine settings
            RegKey = Microsoft.Win32.Registry.CurrentUser;
            try
            {
                if( RegKey != null )
                {
                    Object obj = RegKey.GetValue("Client search limit", string.Empty);
                    Int32 Answer = -1;
                    if( obj != null && Int32.TryParse(Convert.ToString(obj), out Answer) && Answer > 0 )
                    {
                        Result = Answer;
                    }
                }
            }
            catch( Exception ex )
            {
                DebtPlus.Svc.Common.ErrorHandling.HandleErrors(ex);
            }
            finally
            {
                if( RegKey != null )
                {
                    RegKey.Close();
                }
            }
            return Result;
        }
#endregion

        /// <summary>
        /// Process the change of the category
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// <remarks></remarks>
        protected virtual void Combo_Category_SelectedIndexChanged(Object sender, EventArgs e)
        {
            Load_Relation();
            EnableOK();
        }

        /// <summary>
        /// Process the change of the category
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// <remarks></remarks>
        protected virtual void Combo_Category_ValueChanged(Object sender, EventArgs e)
        {
            Load_Relation();
            EnableOK();
        }

        /// <summary>
        /// Client for the search
        /// </summary>
        /// <value></value>
        /// <returns></returns>
        /// <remarks></remarks>
        public Int32 ClientId { get; set; }

        /// <summary>
        /// Attempt to set the Client result. This will trip the validating event.
        /// </summary>
        /// <param name="ClientID">The Client ID found by the search</param>
        /// <returns>TRUE if the Client is validated. FALSE otherwise.</returns>
        /// <remarks></remarks>
        protected internal bool SetClient (Int32 ClientId, Int32 OCSId)
        {
            this.ClientId = ClientId;
            this.ocsId = OCSId;
            CancelEventArgs e = new CancelEventArgs(false);
            RaiseValidating(e);
            if( e.Cancel )
            {
                return false;
            }

            // Save the Client for the next time in the history
            Save_Recent(ClientId);
            return true;
        }

        /// <summary>
        /// Save the item to the history table
        /// </summary>
        /// <param name="Client"></param>
        internal void Save_Recent(Int32 ClientId)
        {
            // The Client is always saved in the Clients key.
            using (DebtPlus.Data.MRU mruList = new DebtPlus.Data.MRU("Clients"))
            {
                mruList.InsertTopMost(ClientId);
                mruList.SaveKey();
            }

            // Save the value in the selection index for the next operation
            if (MruKey() != "Clients")
            {
                using (DebtPlus.Data.MRU mruList = new DebtPlus.Data.MRU(MruKey()))
                {
                    mruList.InsertTopMost(Search_Value.Text.Trim());
                    mruList.SaveKey();
                }
            }
        }

        private string MruKey()
        {
            DebtPlus.Data.Controls.ComboboxItem field = (DebtPlus.Data.Controls.ComboboxItem)Search_Field.SelectedItem;
            return provider.MRUs[(SearchProvider.LH_VALUE)field.value];
        }
        
        /// <summary>
        /// Perform the client search operation
        /// </summary>
        /// <param name="owner">Owner window for the dialogs</param>
        /// <returns>A dialog result for the search.</returns>
        public DialogResult PerformClientSearch(IWin32Window owner)
        {
            return ShowDialog(owner);
        }

        private void Button_Cancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}