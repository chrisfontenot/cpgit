namespace DebtPlus.UI.Housing.OCS
{
    partial class ClientSortForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            FormDispose(disposing);
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Button_OK = new DevExpress.XtraEditors.SimpleButton();
            this.Button_Cancel = new DevExpress.XtraEditors.SimpleButton();
            this.activeInactive = new DevExpress.XtraEditors.ComboBoxEdit();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.mtiStatus = new DevExpress.XtraEditors.ComboBoxEdit();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.filterBy = new DevExpress.XtraEditors.ComboBoxEdit();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.language = new DevExpress.XtraEditors.ComboBoxEdit();
            this.labelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.timezoneCombo = new DevExpress.XtraEditors.ComboBoxEdit();
            this.state = new DevExpress.XtraEditors.ComboBoxEdit();
            this.minAttempts = new DevExpress.XtraEditors.SpinEdit();
            this.labelControl6 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl7 = new DevExpress.XtraEditors.LabelControl();
            this.maxAttempts = new DevExpress.XtraEditors.SpinEdit();
            this.statusCode = new DevExpress.XtraEditors.ComboBoxEdit();
            this.labelControl8 = new DevExpress.XtraEditors.LabelControl();
            this.popupFilterBy = new DevExpress.XtraEditors.PopupContainerEdit();
            this.popupContainerControl1 = new DevExpress.XtraEditors.PopupContainerControl();
            this.popupStateTimezone = new DevExpress.XtraEditors.PopupContainerEdit();
            this.popupContainerControl2 = new DevExpress.XtraEditors.PopupContainerControl();
            this.labelControl10 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl9 = new DevExpress.XtraEditors.LabelControl();
            this.queueLabel = new DevExpress.XtraEditors.LabelControl();
            this.queue = new DevExpress.XtraEditors.ComboBoxEdit();
            this.lastChance = new DevExpress.XtraEditors.ComboBoxEdit();
            this.labelControl11 = new DevExpress.XtraEditors.LabelControl();
            this.popupLastChance = new DevExpress.XtraEditors.PopupContainerEdit();
            this.popupContainerLastChance = new DevExpress.XtraEditors.PopupContainerControl();
            this.counseled_to_date = new DevExpress.XtraEditors.DateEdit();
            this.counseled_from_date = new DevExpress.XtraEditors.DateEdit();
            this.labelControl15 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl12 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl13 = new DevExpress.XtraEditors.LabelControl();
            this.counseledState = new DevExpress.XtraEditors.ComboBoxEdit();
            this.labelControl14 = new DevExpress.XtraEditors.LabelControl();
            ((System.ComponentModel.ISupportInitialize)(this.DxErrorProvider1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.activeInactive.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.mtiStatus.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.filterBy.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.language.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.timezoneCombo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.state.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.minAttempts.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.maxAttempts.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.statusCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupFilterBy.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerControl1)).BeginInit();
            this.popupContainerControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.popupStateTimezone.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerControl2)).BeginInit();
            this.popupContainerControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.queue.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lastChance.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupLastChance.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerLastChance)).BeginInit();
            this.popupContainerLastChance.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.counseled_to_date.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.counseled_to_date.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.counseled_from_date.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.counseled_from_date.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.counseledState.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // DefaultLookAndFeel1
            // 
            this.DefaultLookAndFeel1.LookAndFeel.SkinName = "McSkin";
            // 
            // Button_OK
            // 
            this.Button_OK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.Button_OK.Location = new System.Drawing.Point(121, 260);
            this.Button_OK.Name = "Button_OK";
            this.Button_OK.Size = new System.Drawing.Size(75, 25);
            this.Button_OK.TabIndex = 8;
            this.Button_OK.Text = "&OK";
            this.Button_OK.ToolTipController = this.ToolTipController1;
            this.Button_OK.Click += new System.EventHandler(this.Button_OK_Click_1);
            // 
            // Button_Cancel
            // 
            this.Button_Cancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.Button_Cancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.Button_Cancel.Location = new System.Drawing.Point(305, 260);
            this.Button_Cancel.Name = "Button_Cancel";
            this.Button_Cancel.Size = new System.Drawing.Size(75, 25);
            this.Button_Cancel.TabIndex = 9;
            this.Button_Cancel.Text = "&Cancel";
            this.Button_Cancel.ToolTipController = this.ToolTipController1;
            this.Button_Cancel.Click += new System.EventHandler(this.Button_Cancel_Click);
            // 
            // activeInactive
            // 
            this.activeInactive.Location = new System.Drawing.Point(143, 15);
            this.activeInactive.Name = "activeInactive";
            this.activeInactive.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.activeInactive.Size = new System.Drawing.Size(300, 20);
            this.activeInactive.TabIndex = 1;
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(44, 19);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(53, 13);
            this.labelControl1.TabIndex = 10;
            this.labelControl1.Text = "Active Flag";
            // 
            // mtiStatus
            // 
            this.mtiStatus.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.mtiStatus.Enabled = false;
            this.mtiStatus.Location = new System.Drawing.Point(143, 225);
            this.mtiStatus.Name = "mtiStatus";
            this.mtiStatus.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.mtiStatus.Size = new System.Drawing.Size(300, 20);
            this.mtiStatus.TabIndex = 7;
            // 
            // labelControl2
            // 
            this.labelControl2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.labelControl2.Location = new System.Drawing.Point(44, 229);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(52, 13);
            this.labelControl2.TabIndex = 12;
            this.labelControl2.Text = "MTI Status";
            // 
            // labelControl3
            // 
            this.labelControl3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.labelControl3.Location = new System.Drawing.Point(44, 159);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(39, 13);
            this.labelControl3.TabIndex = 14;
            this.labelControl3.Text = "Filter By";
            // 
            // filterBy
            // 
            this.filterBy.Location = new System.Drawing.Point(8, 16);
            this.filterBy.Name = "filterBy";
            this.filterBy.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.filterBy.Size = new System.Drawing.Size(300, 20);
            this.filterBy.TabIndex = 13;
            this.filterBy.SelectedIndexChanged += new System.EventHandler(this.filterBy_SelectedIndexChanged);
            // 
            // labelControl4
            // 
            this.labelControl4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.labelControl4.Location = new System.Drawing.Point(44, 194);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(47, 13);
            this.labelControl4.TabIndex = 16;
            this.labelControl4.Text = "Language";
            // 
            // language
            // 
            this.language.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.language.Location = new System.Drawing.Point(143, 190);
            this.language.Name = "language";
            this.language.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.language.Size = new System.Drawing.Size(300, 20);
            this.language.TabIndex = 6;
            // 
            // labelControl5
            // 
            this.labelControl5.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.labelControl5.Location = new System.Drawing.Point(44, 89);
            this.labelControl5.Name = "labelControl5";
            this.labelControl5.Size = new System.Drawing.Size(75, 13);
            this.labelControl5.TabIndex = 17;
            this.labelControl5.Text = "State/Timezone";
            // 
            // timezoneCombo
            // 
            this.timezoneCombo.Location = new System.Drawing.Point(59, 3);
            this.timezoneCombo.Name = "timezoneCombo";
            this.timezoneCombo.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.timezoneCombo.Size = new System.Drawing.Size(249, 20);
            this.timezoneCombo.TabIndex = 18;
            this.timezoneCombo.SelectedIndexChanged += new System.EventHandler(this.timezone_SelectedIndexChanged);
            this.timezoneCombo.SelectedValueChanged += new System.EventHandler(this.timezoneCombo_SelectedValueChanged);
            // 
            // state
            // 
            this.state.Location = new System.Drawing.Point(59, 29);
            this.state.Name = "state";
            this.state.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.state.Size = new System.Drawing.Size(249, 20);
            this.state.TabIndex = 19;
            this.state.SelectedIndexChanged += new System.EventHandler(this.state_SelectedIndexChanged);
            // 
            // minAttempts
            // 
            this.minAttempts.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.minAttempts.Enabled = false;
            this.minAttempts.Location = new System.Drawing.Point(96, 42);
            this.minAttempts.Name = "minAttempts";
            this.minAttempts.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.minAttempts.Properties.IsFloatValue = false;
            this.minAttempts.Properties.Mask.EditMask = "n0";
            this.minAttempts.Properties.EditValueChanging += new DevExpress.XtraEditors.Controls.ChangingEventHandler(this.minAttempts_Properties_EditValueChanging);
            this.minAttempts.Size = new System.Drawing.Size(100, 20);
            this.minAttempts.TabIndex = 20;
            // 
            // labelControl6
            // 
            this.labelControl6.Location = new System.Drawing.Point(8, 45);
            this.labelControl6.Name = "labelControl6";
            this.labelControl6.Size = new System.Drawing.Size(67, 13);
            this.labelControl6.TabIndex = 21;
            this.labelControl6.Text = "Min. Attempts";
            // 
            // labelControl7
            // 
            this.labelControl7.Location = new System.Drawing.Point(8, 71);
            this.labelControl7.Name = "labelControl7";
            this.labelControl7.Size = new System.Drawing.Size(71, 13);
            this.labelControl7.TabIndex = 23;
            this.labelControl7.Text = "Max. Attempts";
            // 
            // maxAttempts
            // 
            this.maxAttempts.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.maxAttempts.Enabled = false;
            this.maxAttempts.Location = new System.Drawing.Point(96, 68);
            this.maxAttempts.Name = "maxAttempts";
            this.maxAttempts.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.maxAttempts.Properties.IsFloatValue = false;
            this.maxAttempts.Properties.Mask.EditMask = "n0";
            this.maxAttempts.Size = new System.Drawing.Size(100, 20);
            this.maxAttempts.TabIndex = 22;
            this.maxAttempts.EditValueChanging += new DevExpress.XtraEditors.Controls.ChangingEventHandler(this.maxAttempts_EditValueChanging);
            // 
            // statusCode
            // 
            this.statusCode.Enabled = false;
            this.statusCode.Location = new System.Drawing.Point(96, 103);
            this.statusCode.Name = "statusCode";
            this.statusCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.statusCode.Size = new System.Drawing.Size(212, 20);
            this.statusCode.TabIndex = 24;
            this.statusCode.EditValueChanged += new System.EventHandler(this.statusCode_EditValueChanged);
            // 
            // labelControl8
            // 
            this.labelControl8.Location = new System.Drawing.Point(8, 106);
            this.labelControl8.Name = "labelControl8";
            this.labelControl8.Size = new System.Drawing.Size(55, 13);
            this.labelControl8.TabIndex = 25;
            this.labelControl8.Text = "OCS Status";
            // 
            // popupFilterBy
            // 
            this.popupFilterBy.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.popupFilterBy.Location = new System.Drawing.Point(143, 155);
            this.popupFilterBy.Name = "popupFilterBy";
            this.popupFilterBy.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.popupFilterBy.Properties.ShowPopupCloseButton = false;
            this.popupFilterBy.Size = new System.Drawing.Size(300, 20);
            this.popupFilterBy.TabIndex = 5;
            // 
            // popupContainerControl1
            // 
            this.popupContainerControl1.Controls.Add(this.filterBy);
            this.popupContainerControl1.Controls.Add(this.minAttempts);
            this.popupContainerControl1.Controls.Add(this.labelControl8);
            this.popupContainerControl1.Controls.Add(this.labelControl6);
            this.popupContainerControl1.Controls.Add(this.statusCode);
            this.popupContainerControl1.Controls.Add(this.maxAttempts);
            this.popupContainerControl1.Controls.Add(this.labelControl7);
            this.popupContainerControl1.Location = new System.Drawing.Point(69, 363);
            this.popupContainerControl1.Name = "popupContainerControl1";
            this.popupContainerControl1.Size = new System.Drawing.Size(311, 135);
            this.popupContainerControl1.TabIndex = 27;
            // 
            // popupStateTimezone
            // 
            this.popupStateTimezone.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.popupStateTimezone.Location = new System.Drawing.Point(143, 85);
            this.popupStateTimezone.Name = "popupStateTimezone";
            this.popupStateTimezone.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.popupStateTimezone.Properties.ShowPopupCloseButton = false;
            this.popupStateTimezone.Size = new System.Drawing.Size(300, 20);
            this.popupStateTimezone.TabIndex = 3;
            // 
            // popupContainerControl2
            // 
            this.popupContainerControl2.Controls.Add(this.labelControl10);
            this.popupContainerControl2.Controls.Add(this.labelControl9);
            this.popupContainerControl2.Controls.Add(this.timezoneCombo);
            this.popupContainerControl2.Controls.Add(this.state);
            this.popupContainerControl2.Location = new System.Drawing.Point(104, 332);
            this.popupContainerControl2.Name = "popupContainerControl2";
            this.popupContainerControl2.Size = new System.Drawing.Size(311, 56);
            this.popupContainerControl2.TabIndex = 29;
            // 
            // labelControl10
            // 
            this.labelControl10.Location = new System.Drawing.Point(8, 32);
            this.labelControl10.Name = "labelControl10";
            this.labelControl10.Size = new System.Drawing.Size(26, 13);
            this.labelControl10.TabIndex = 23;
            this.labelControl10.Text = "State";
            // 
            // labelControl9
            // 
            this.labelControl9.Location = new System.Drawing.Point(8, 6);
            this.labelControl9.Name = "labelControl9";
            this.labelControl9.Size = new System.Drawing.Size(45, 13);
            this.labelControl9.TabIndex = 22;
            this.labelControl9.Text = "Timezone";
            // 
            // queueLabel
            // 
            this.queueLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.queueLabel.Location = new System.Drawing.Point(44, 54);
            this.queueLabel.Name = "queueLabel";
            this.queueLabel.Size = new System.Drawing.Size(32, 13);
            this.queueLabel.TabIndex = 30;
            this.queueLabel.Text = "Queue";
            // 
            // queue
            // 
            this.queue.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.queue.Location = new System.Drawing.Point(143, 50);
            this.queue.Name = "queue";
            this.queue.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.queue.Size = new System.Drawing.Size(300, 20);
            this.queue.TabIndex = 2;
            // 
            // lastChance
            // 
            this.lastChance.Location = new System.Drawing.Point(96, 12);
            this.lastChance.Name = "lastChance";
            this.lastChance.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lastChance.Size = new System.Drawing.Size(210, 20);
            this.lastChance.TabIndex = 11;
            this.lastChance.SelectedIndexChanged += new System.EventHandler(this.lastChance_SelectedIndexChanged);
            // 
            // labelControl11
            // 
            this.labelControl11.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.labelControl11.Location = new System.Drawing.Point(44, 124);
            this.labelControl11.Name = "labelControl11";
            this.labelControl11.Size = new System.Drawing.Size(85, 13);
            this.labelControl11.TabIndex = 32;
            this.labelControl11.Text = "Last Chance Date";
            // 
            // popupLastChance
            // 
            this.popupLastChance.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.popupLastChance.Location = new System.Drawing.Point(143, 120);
            this.popupLastChance.Name = "popupLastChance";
            this.popupLastChance.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.popupLastChance.Properties.ShowPopupCloseButton = false;
            this.popupLastChance.Size = new System.Drawing.Size(300, 20);
            this.popupLastChance.TabIndex = 4;
            // 
            // popupContainerLastChance
            // 
            this.popupContainerLastChance.Controls.Add(this.counseled_to_date);
            this.popupContainerLastChance.Controls.Add(this.counseled_from_date);
            this.popupContainerLastChance.Controls.Add(this.labelControl15);
            this.popupContainerLastChance.Controls.Add(this.lastChance);
            this.popupContainerLastChance.Controls.Add(this.labelControl12);
            this.popupContainerLastChance.Controls.Add(this.labelControl13);
            this.popupContainerLastChance.Controls.Add(this.counseledState);
            this.popupContainerLastChance.Controls.Add(this.labelControl14);
            this.popupContainerLastChance.Location = new System.Drawing.Point(83, 101);
            this.popupContainerLastChance.Name = "popupContainerLastChance";
            this.popupContainerLastChance.Size = new System.Drawing.Size(311, 106);
            this.popupContainerLastChance.TabIndex = 28;
            // 
            // counseled_to_date
            // 
            this.counseled_to_date.EditValue = new System.DateTime(((long)(0)));
            this.counseled_to_date.Location = new System.Drawing.Point(214, 42);
            this.counseled_to_date.Name = "counseled_to_date";
            this.counseled_to_date.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.counseled_to_date.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.counseled_to_date.Size = new System.Drawing.Size(92, 20);
            this.counseled_to_date.TabIndex = 12;
            this.counseled_to_date.EditValueChanged += new System.EventHandler(this.counseled_to_date_EditValueChanged);
            // 
            // counseled_from_date
            // 
            this.counseled_from_date.EditValue = new System.DateTime(((long)(0)));
            this.counseled_from_date.Location = new System.Drawing.Point(54, 42);
            this.counseled_from_date.Name = "counseled_from_date";
            this.counseled_from_date.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.counseled_from_date.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.counseled_from_date.Size = new System.Drawing.Size(92, 20);
            this.counseled_from_date.TabIndex = 12;
            this.counseled_from_date.EditValueChanged += new System.EventHandler(this.counseled_from_date_EditValueChanged);
            // 
            // labelControl15
            // 
            this.labelControl15.Location = new System.Drawing.Point(8, 16);
            this.labelControl15.Name = "labelControl15";
            this.labelControl15.Size = new System.Drawing.Size(57, 13);
            this.labelControl15.TabIndex = 34;
            this.labelControl15.Text = "Date Range";
            // 
            // labelControl12
            // 
            this.labelControl12.Location = new System.Drawing.Point(8, 76);
            this.labelControl12.Name = "labelControl12";
            this.labelControl12.Size = new System.Drawing.Size(50, 13);
            this.labelControl12.TabIndex = 25;
            this.labelControl12.Text = "Counseled";
            // 
            // labelControl13
            // 
            this.labelControl13.Location = new System.Drawing.Point(8, 46);
            this.labelControl13.Name = "labelControl13";
            this.labelControl13.Size = new System.Drawing.Size(24, 13);
            this.labelControl13.TabIndex = 21;
            this.labelControl13.Text = "From";
            // 
            // counseledState
            // 
            this.counseledState.Location = new System.Drawing.Point(96, 72);
            this.counseledState.Name = "counseledState";
            this.counseledState.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.counseledState.Size = new System.Drawing.Size(210, 20);
            this.counseledState.TabIndex = 13;
            this.counseledState.SelectedIndexChanged += new System.EventHandler(this.counseledState_SelectedIndexChanged);
            // 
            // labelControl14
            // 
            this.labelControl14.Location = new System.Drawing.Point(179, 46);
            this.labelControl14.Name = "labelControl14";
            this.labelControl14.Size = new System.Drawing.Size(22, 13);
            this.labelControl14.TabIndex = 23;
            this.labelControl14.Text = "Thru";
            // 
            // ClientSortForm
            // 
            this.AcceptButton = this.Button_OK;
            this.CancelButton = this.Button_Cancel;
            this.ClientSize = new System.Drawing.Size(498, 304);
            this.Controls.Add(this.popupContainerLastChance);
            this.Controls.Add(this.popupLastChance);
            this.Controls.Add(this.labelControl11);
            this.Controls.Add(this.queue);
            this.Controls.Add(this.queueLabel);
            this.Controls.Add(this.popupContainerControl2);
            this.Controls.Add(this.popupStateTimezone);
            this.Controls.Add(this.popupContainerControl1);
            this.Controls.Add(this.popupFilterBy);
            this.Controls.Add(this.labelControl5);
            this.Controls.Add(this.labelControl4);
            this.Controls.Add(this.language);
            this.Controls.Add(this.labelControl3);
            this.Controls.Add(this.labelControl2);
            this.Controls.Add(this.mtiStatus);
            this.Controls.Add(this.labelControl1);
            this.Controls.Add(this.activeInactive);
            this.Controls.Add(this.Button_Cancel);
            this.Controls.Add(this.Button_OK);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "ClientSortForm";
            this.Text = "Client Sort";
            this.Load += new System.EventHandler(this.ClientSortForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.DxErrorProvider1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.activeInactive.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.mtiStatus.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.filterBy.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.language.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.timezoneCombo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.state.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.minAttempts.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.maxAttempts.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.statusCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupFilterBy.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerControl1)).EndInit();
            this.popupContainerControl1.ResumeLayout(false);
            this.popupContainerControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.popupStateTimezone.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerControl2)).EndInit();
            this.popupContainerControl2.ResumeLayout(false);
            this.popupContainerControl2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.queue.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lastChance.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupLastChance.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerLastChance)).EndInit();
            this.popupContainerLastChance.ResumeLayout(false);
            this.popupContainerLastChance.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.counseled_to_date.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.counseled_to_date.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.counseled_from_date.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.counseled_from_date.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.counseledState.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }
        internal DevExpress.XtraEditors.SimpleButton Button_OK;
        internal DevExpress.XtraEditors.SimpleButton Button_Cancel;
#endregion
        private DevExpress.XtraEditors.ComboBoxEdit activeInactive;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.ComboBoxEdit mtiStatus;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.ComboBoxEdit filterBy;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraEditors.ComboBoxEdit language;
        private DevExpress.XtraEditors.LabelControl labelControl5;
        private DevExpress.XtraEditors.ComboBoxEdit timezoneCombo;
        private DevExpress.XtraEditors.ComboBoxEdit state;
        private DevExpress.XtraEditors.SpinEdit minAttempts;
        private DevExpress.XtraEditors.LabelControl labelControl6;
        private DevExpress.XtraEditors.LabelControl labelControl7;
        private DevExpress.XtraEditors.SpinEdit maxAttempts;
        private DevExpress.XtraEditors.ComboBoxEdit statusCode;
        private DevExpress.XtraEditors.LabelControl labelControl8;
        private DevExpress.XtraEditors.PopupContainerEdit popupFilterBy;
        private DevExpress.XtraEditors.PopupContainerControl popupContainerControl1;
        private DevExpress.XtraEditors.PopupContainerEdit popupStateTimezone;
        private DevExpress.XtraEditors.PopupContainerControl popupContainerControl2;
        private DevExpress.XtraEditors.LabelControl labelControl10;
        private DevExpress.XtraEditors.LabelControl labelControl9;
        private DevExpress.XtraEditors.LabelControl queueLabel;
        private DevExpress.XtraEditors.ComboBoxEdit queue;
        private DevExpress.XtraEditors.ComboBoxEdit lastChance;
        private DevExpress.XtraEditors.LabelControl labelControl11;
        private DevExpress.XtraEditors.PopupContainerEdit popupLastChance;
        private DevExpress.XtraEditors.PopupContainerControl popupContainerLastChance;
        private DevExpress.XtraEditors.LabelControl labelControl15;
        private DevExpress.XtraEditors.LabelControl labelControl12;
        private DevExpress.XtraEditors.LabelControl labelControl13;
        private DevExpress.XtraEditors.ComboBoxEdit counseledState;
        private DevExpress.XtraEditors.LabelControl labelControl14;
        internal DevExpress.XtraEditors.DateEdit counseled_to_date;
        internal DevExpress.XtraEditors.DateEdit counseled_from_date;
    }
}

