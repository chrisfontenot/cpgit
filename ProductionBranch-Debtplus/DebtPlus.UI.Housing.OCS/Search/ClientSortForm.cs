using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Windows.Forms;

using DebtPlus.OCS.Domain;
using DebtPlus.Svc.OCS;
using DebtPlus.LINQ;
using DebtPlus.Interfaces.Desktop;

namespace DebtPlus.UI.Housing.OCS
{
    public partial class ClientSortForm : DebtPlus.Data.Forms.DebtPlusForm, DebtPlus.Interfaces.Client.IClientSearch
    {
        private PROGRAM program { get; set; }

        // Information about the Client search
        internal ClientSearchSelectForm formClientList;      // This is our form listing the results of the search.
        protected Int32 lastClient;                 // Last Client in the selection list. Used for the "MORE" function as a starting point.
        protected string selectClause;              // Selection clause. Used to build the SQL statement
        private DataSet ds = new DataSet("ds");     // Dataset to hold the items
        private SortProvider provider;
        public OCSDbMapper repository = null;
        public OCSClient claimedClient = null;
        public OCSSortArgs sortArgs = null;

        public void OldAppMain(string[] args)
        {
            var t = new System.Threading.Thread(() =>
            {
                using (var sortForm = new ClientSortForm())
                {
                    sortForm.ShowDialog();
                }
            });
            t.SetApartmentState(System.Threading.ApartmentState.STA);
            t.IsBackground = false;
            t.Start();
        }

        public ClientSortForm() : this(PROGRAM.FreddieMacEI) { }

            
        /// <summary>
        /// Create an instance of our new class
        /// </summary>
        /// <param name="DatabaseInfo"></param>
        public ClientSortForm(PROGRAM _program) : base()
        {
            DebtPlus.LINQ.SQLInfoClass SqlInfo = DebtPlus.LINQ.SQLInfoClass.getDefault();
            var connectionString = DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString;
            DebtPlus.LINQ.BusinessContext bc = new LINQ.BusinessContext(connectionString);
            repository = new OCSDbMapper(bc);
            provider = new SortProvider(repository);

            InitializeComponent();
            ClientId = Int32.MinValue;
            RegisterHandlers();

            popupStateTimezone.Properties.PopupControl = popupContainerControl2;
            popupFilterBy.Properties.PopupControl = popupContainerControl1;
            popupLastChance.Properties.PopupControl = popupContainerLastChance;
            program = _program;

            AlterForProgramSpecifics();
        }

        //it's an ugly solution, but any other solution will probably be uglier
        private void AlterForProgramSpecifics()
        {
            if (this.program != PROGRAM.FreddieMacPostMod && this.program != PROGRAM.FannieMaePostMod &&
                this.program != PROGRAM.NationStarSD1308 && this.program != PROGRAM.OneWestSD1308 &&
                this.program != PROGRAM.USBankSD1308 && this.program != PROGRAM.PostModPilotCheckin &&
                this.program != PROGRAM.PostModPilotCounseling)
            {
                this.Controls.Remove(queue);
                this.Controls.Remove(queueLabel);

                this.Height = 300;
            }            
        }
        
        public ClientSortForm(SortProvider pro, OCSDbMapper repo): this()
        {
            provider = pro;
            repository = repo;
        }       

        /// <summary>
        /// Register the event handlers
        /// </summary>
        private void RegisterHandlers()
        {
            this.Load += new EventHandler(ClientSearchClass_Load);                                    
            Button_OK.Click += new EventHandler(Button_OK_Click); 
        }

        /// <summary>
        /// Remove the registration for the events
        /// </summary>
        private void UnRegisterHandlers()
        {
            this.Load -= new EventHandler(ClientSearchClass_Load);                                    
            Button_OK.Click -= new EventHandler(Button_OK_Click); 
        }

        /// <summary>
        /// Call the validating routine that the user may have set
        /// </summary>
        /// <param name="e"></param>
        /// <remarks></remarks>
        protected void RaiseValidating(CancelEventArgs e)
        {
            OnValidating(e);
        }

        /// <summary>
        /// Dispose the items generated in the form
        /// </summary>
        /// <remarks></remarks>
        private void FormDispose(bool disposing)
        {
            // Dispose the larger objects
            if( disposing )
            {
                if( ds != null )
                {
                    ds.Dispose();
                    ds = null;
                }
                if( formClientList != null )
                {
                    formClientList.Dispose();
                    formClientList = null;
                }
            }
        }
        
        #region Form Navigation
        
        private void Load_ActiveCombo() {
            activeInactive.Properties.Items.Clear();


            activeInactive.Properties.Items.Add(new DebtPlus.Data.Controls.ComboboxItem(string.Empty, String.Empty));
            activeInactive.Properties.Items.Add(new DebtPlus.Data.Controls.ComboboxItem("Active",true,true));
            activeInactive.Properties.Items.Add(new DebtPlus.Data.Controls.ComboboxItem("InActive",false,true));

            activeInactive.SelectedIndex = 0;
        }

        private void Load_Timezones() {
            timezoneCombo.Properties.Items.Clear();

            timezoneCombo.Properties.Items.Add(new DebtPlus.Data.Controls.ComboboxItem(string.Empty, String.Empty));
            timezoneCombo.Properties.Items.Add(new DebtPlus.Data.Controls.ComboboxItem("Atlantic", "Atlantic"));
            timezoneCombo.Properties.Items.Add(new DebtPlus.Data.Controls.ComboboxItem("Eastern", "Eastern"));
            timezoneCombo.Properties.Items.Add(new DebtPlus.Data.Controls.ComboboxItem("Central", "Central"));
            timezoneCombo.Properties.Items.Add(new DebtPlus.Data.Controls.ComboboxItem("Mountain", "Mountain"));
            timezoneCombo.Properties.Items.Add(new DebtPlus.Data.Controls.ComboboxItem("Pacific", "Pacific"));
            timezoneCombo.Properties.Items.Add(new DebtPlus.Data.Controls.ComboboxItem("Alaska", "Alaska"));
            timezoneCombo.Properties.Items.Add(new DebtPlus.Data.Controls.ComboboxItem("Hawaii", "Hawaii"));       
        }
        
        private void Load_States() {
            DropdownHelper.LoadDropdown(state, DropdownConfig.GetUSStateAbbreviations());
        }

        private void Load_Languages() {
            
            language.Properties.Items.Clear();

            language.Properties.Items.Add(new DebtPlus.Data.Controls.ComboboxItem(string.Empty, String.Empty));
            language.Properties.Items.Add(new DebtPlus.Data.Controls.ComboboxItem("English", Person.LANGUAGE.English));
            language.Properties.Items.Add(new DebtPlus.Data.Controls.ComboboxItem("Spanish", Person.LANGUAGE.Spanish));

            language.SelectedIndex = 0;
        }
        
        private void Load_StatusCode() {
            statusCode.Properties.Items.Clear();

            foreach (var kvp in ProgramConfigService.getStatusCodes(program).ToDictionary(kp => kp.Value, kp => kp.Key))
            {
                statusCode.Properties.Items.Add(new DebtPlus.Data.Controls.ComboboxItem(kvp.Value, kvp.Key));            
            }

            statusCode.SelectedIndex = -1;
        }

        private void Load_Filters() {
            filterBy.Properties.Items.Clear();

            filterBy.Properties.Items.Add(new DebtPlus.Data.Controls.ComboboxItem(string.Empty, String.Empty));
            filterBy.Properties.Items.Add(new DebtPlus.Data.Controls.ComboboxItem("Contact Attempts (Min/Max)", "Min-Max"));
            filterBy.Properties.Items.Add(new DebtPlus.Data.Controls.ComboboxItem("Contacts - Any", "Any"));
            filterBy.Properties.Items.Add(new DebtPlus.Data.Controls.ComboboxItem("Specific OCS Status", "StatusCode"));

            filterBy.SelectedIndex = 0;
        }

        private void Load_Queues()
        {
            queue.Properties.Items.Clear();
                        
            queue.Properties.Items.Add(new DebtPlus.Data.Controls.ComboboxItem("High Touch",OCS_QUEUE.HighTouch));
            queue.Properties.Items.Add(new DebtPlus.Data.Controls.ComboboxItem("Counselor", OCS_QUEUE.Counselor));

            queue.SelectedIndex = 0;
        }

        private void Load_LastChanceDate()
        {
            lastChance.Properties.Items.Clear();

            lastChance.Properties.Items.Add(new DebtPlus.Data.Controls.ComboboxItem(string.Empty, String.Empty));
            lastChance.Properties.Items.Add(new DebtPlus.Data.Controls.ComboboxItem("Within Past Year (< 366 Days)", 1));
            lastChance.Properties.Items.Add(new DebtPlus.Data.Controls.ComboboxItem("Custom", 2));

            lastChance.SelectedIndex = 0;
        }

        private void Load_CounseledState()
        {
            counseledState.Properties.Items.Clear();

            counseledState.Properties.Items.Add(new DebtPlus.Data.Controls.ComboboxItem(string.Empty, String.Empty));
            counseledState.Properties.Items.Add(new DebtPlus.Data.Controls.ComboboxItem("Not Counseled", 1));
            counseledState.Properties.Items.Add(new DebtPlus.Data.Controls.ComboboxItem("Counseled Once", 2));
            counseledState.Properties.Items.Add(new DebtPlus.Data.Controls.ComboboxItem("Counseled Twice or more", 3));

            counseledState.SelectedIndex = 0;
        }
                        
        /// <summary>
        /// Do the load sequence for the form
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// <remarks></remarks>
        private void ClientSearchClass_Load(Object sender, EventArgs e)
        {
            UnRegisterHandlers();                    // Prevent evnet tripping when we are configuring the form.
            try
            {
                // Load the comboboxes
                Load_ActiveCombo();
                Load_Timezones();
                Load_States();
                Load_StatusCode();
                Load_Languages();
                Load_Filters();
                Load_Queues();
                Load_LastChanceDate();
                Load_CounseledState();
                EnableOK();                          // Disable the OK button for now
            }
            finally
            {
                RegisterHandlers();
            }
        }

        /// <summary>
        /// Process the change of the category
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// <remarks></remarks>
        protected void Combo_Category_SelectedValueChanged(object sender, EventArgs e)
        {
            EnableOK();
        }

        /// <summary>
        /// Handle the change in the relation list
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// <remarks></remarks>
        protected void Combo_Relation_SelectedIndexChanged(object sender, EventArgs e)
        {
            EnableOK();
        }

        #endregion

        #region Form Validation
        /// <summary>
        /// Should the "OK" button be enabled?
        /// </summary>
        /// <remarks></remarks>
        protected virtual void EnableOK()
        {
            bool enable = true;

            enable = enable && activeInactive.SelectedIndex != 0;
            enable = enable && timezoneCombo.SelectedIndex != 0;

            // Everything is valid. Enable the OK button.
            Button_OK.Enabled = true;
        }

        #endregion

        #region Combo_Value
        /// <summary>
        /// Process the entry to the input fields. Select all of the text.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// <remarks></remarks>
        protected void Combo_Value1_Enter(Object sender, EventArgs e)
        {
            ((DevExpress.XtraEditors.ComboBoxEdit) sender).SelectAll();
        }

        /// <summary>
        /// Process the change in the second input box.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// <remarks></remarks>
        protected void Form_Changed(Object sender, EventArgs e)
        {
            EnableOK();
        }

        /// <summary>
        /// Leave the input fields.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// <remarks></remarks>
        protected void Combo_Value1_Leave(Object sender, EventArgs e)
        {
            DevExpress.XtraEditors.ComboBoxEdit cbx = (DevExpress.XtraEditors.ComboBoxEdit) sender;
            cbx.Text = cbx.Text.Trim();
            EnableOK();
        }
        #endregion
        
        /// <summary>
        /// Handle the "OK" button. Do the database search operation.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// <remarks></remarks>
        protected void Button_OK_Click(Object sender, EventArgs e)
        {
        }

        /// <summary>
        /// Load the table into the display form.
        /// </summary>
        /// <param name="tbl"></param>
        /// <remarks></remarks>
        protected void LoadListControl(DataTable tbl)
        {
            Int32 limit = SearchLimit();

            // Enable or disable the MORE button if there are too many items
            formClientList.Button_More.Visible = false;
            if (limit > 0)
            {
                if (tbl.Rows.Count >= limit)
                {
                    formClientList.Button_More.Visible = true;
                    tbl.Rows.RemoveAt(limit);
                }
            }

            // Bind the dataset to the grid control and populate it.
            formClientList.GridControl1.DataSource = tbl;
            formClientList.GridControl1.RefreshDataSource();
        }
                
        #region Load the Client List Form Items
        /// <summary>
        /// Find the number of clients to return in the selection data
        /// </summary>
        /// <returns></returns>
        /// <remarks></remarks>
        protected static Int32 SearchLimit ()
        {
            Microsoft.Win32.RegistryKey RegKey = Microsoft.Win32.Registry.LocalMachine;
            Int32 Result = 1000;

            // Look in the machine values for the defaults for all users
            try
            {
                RegKey = RegKey.OpenSubKey(string.Format(@"{0}\options", DebtPlus.Configuration.AssemblyInfo.InstallationRegistryKey), false);
                if( RegKey != null )
                {
                    Object obj = RegKey.GetValue("Client search limit", string.Empty);
                    Int32 Answer = -1;
                    if( obj != null && Int32.TryParse(Convert.ToString(obj), out Answer) && Answer > 0 )
                    {
                        Result = Answer;
                    }
                }
            }
            catch( Exception ex )
            {
                DebtPlus.Svc.Common.ErrorHandling.HandleErrors(ex);
            }
            finally
            {
                if( RegKey != null )
                {
                    RegKey.Close();
                }
            }

            // Allow the user to override the machine settings
            RegKey = Microsoft.Win32.Registry.CurrentUser;
            try
            {
                if( RegKey != null )
                {
                    Object obj = RegKey.GetValue("Client search limit", string.Empty);
                    Int32 Answer = -1;
                    if( obj != null && Int32.TryParse(Convert.ToString(obj), out Answer) && Answer > 0 )
                    {
                        Result = Answer;
                    }
                }
            }
            catch( Exception ex )
            {
                DebtPlus.Svc.Common.ErrorHandling.HandleErrors(ex);
            }
            finally
            {
                if( RegKey != null )
                {
                    RegKey.Close();
                }
            }
            return Result;
        }
        #endregion
        
        /// <summary>
        /// Client for the search
        /// </summary>
        /// <value></value>
        /// <returns></returns>
        /// <remarks></remarks>
        public Int32 ClientId { get; set; }

        /// <summary>
        /// Attempt to set the Client result. This will trip the validating event.
        /// </summary>
        /// <param name="ClientID">The Client ID found by the search</param>
        /// <returns>TRUE if the Client is validated. FALSE otherwise.</returns>
        /// <remarks></remarks>
        protected internal bool SetClient (Int32 ClientId)
        {
            this.ClientId = ClientId;
            CancelEventArgs e = new CancelEventArgs(false);
            RaiseValidating(e);
            if( e.Cancel )
            {
                return false;
            }

            // Save the Client for the next time in the history
            return true;
        }
        
        /// <summary>
        /// Perform the client search operation
        /// </summary>
        /// <param name="owner">Owner window for the dialogs</param>
        /// <returns>A dialog result for the search.</returns>
        public DialogResult PerformClientSearch(IWin32Window owner)
        {
            return ShowDialog(owner);
        }

        private void Button_Cancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private string getStateTimezoneValue()
        {
            if (timezoneCombo.SelectedIndex == 0 && state.SelectedIndex == 0)
            {
                return string.Empty;
            }

            if (timezoneCombo.SelectedIndex < 1)
            {
                return state.EditValue == null ? string.Empty : state.EditValue.ToString();
            }
            
            if (state.SelectedIndex < 1)
            {
                return timezoneCombo.EditValue == null ? string.Empty : timezoneCombo.EditValue.ToString();
            }
            return string.Empty;
        }
        
        private void timezone_SelectedIndexChanged(object sender, EventArgs e)
        {            
            state.Enabled = (timezoneCombo.SelectedIndex == 0);
            popupStateTimezone.EditValue = getStateTimezoneValue();

            if (popupStateTimezone.EditValue.ToString() == string.Empty)
            {
                state.SelectedIndex = 0;
            }            
        }

        private void state_SelectedIndexChanged(object sender, EventArgs e)
        {
            timezoneCombo.Enabled = (state.SelectedIndex == 0);
            popupStateTimezone.EditValue = getStateTimezoneValue();

            if (popupStateTimezone.EditValue.ToString() == string.Empty)
            {
                timezoneCombo.SelectedIndex = 0;               
            }
        }
        
        private void filterBy_SelectedIndexChanged(object sender, EventArgs e)
        {
            popupFilterBy.EditValue = filterBy.SelectedText;

            if (filterBy.SelectedText == String.Empty || filterBy.SelectedText.StartsWith("Contacts - Any"))
            {
                minAttempts.Enabled = false;
                maxAttempts.Enabled = false;
                statusCode.Enabled = false;

                statusCode.SelectedIndex = -1;

                minAttempts.Value = 0m;
                minAttempts.EditValue = 0;
                maxAttempts.Value = 0m;
                maxAttempts.EditValue = 0;
            }
            else if (filterBy.SelectedText.StartsWith("Contact Attempts"))
            {
                minAttempts.Enabled = true;
                maxAttempts.Enabled = true;
                statusCode.Enabled = false;

                statusCode.SelectedIndex = -1;
            }
            else if (filterBy.SelectedText.StartsWith("Specific OCS Status"))
            {
                minAttempts.Enabled = false;
                maxAttempts.Enabled = false;              
                statusCode.Enabled = true;

                minAttempts.Value = 0m;
                minAttempts.EditValue = 0;
                maxAttempts.Value = 0m;
                maxAttempts.EditValue = 0;
            }
        }

        private void Button_OK_Click_1(object sender, EventArgs e)
        {
            sortArgs = new OCSSortArgs();

            if (language.SelectedIndex > 0)
            {
                sortArgs.PreferredLanguage = (Person.LANGUAGE)DropdownHelper.GetValue(language);
            }
            if (activeInactive.SelectedIndex > 0)
            {
                sortArgs.ActiveFlag = activeInactive.SelectedIndex == 1;
            }
            /* if the last chance flag is "Within Past Year (< 366 Days)", set the last chance flag */
            sortArgs.LastChanceFlag = (lastChance.SelectedIndex > 0) ? lastChance.SelectedIndex : 0;

            /* Consider the date only if the Date Range is Custom */
            if (sortArgs.LastChanceFlag == 2)
            {
                DateTime fromDate = DateTime.MinValue;
                fromDate = (DateTime)counseled_from_date.EditValue;

                if (fromDate > DateTime.MinValue)
                {
                    sortArgs.CounseledFromDate = fromDate;
                }
                else
                {
                    sortArgs.CounseledFromDate = null;
                }

                DateTime toDate = DateTime.MinValue;
                toDate = (DateTime)counseled_to_date.EditValue;

                if (toDate > DateTime.MinValue)
                {
                    sortArgs.CounseledToDate = toDate;
                }
                else
                {
                    sortArgs.CounseledToDate = null;
                }
            }

            if (counseledState.SelectedIndex > 0)
            {
                sortArgs.CounseledState = counseledState.SelectedIndex;
            }

            if (filterBy.EditValue.ToString().StartsWith("Specific OCS Status") && statusCode.SelectedIndex != -1)
            {
                sortArgs.StatusCode = (STATUS_CODE)DropdownHelper.GetValue(statusCode);
            }
            if (filterBy.EditValue.ToString().StartsWith("Contact Attempts"))
            {
                sortArgs.MinContacts = Convert.ToInt32(minAttempts.Text);
                sortArgs.MaxContacts = Convert.ToInt32(maxAttempts.Text);
            }
            if (popupStateTimezone.EditValue != null && popupStateTimezone.EditValue.ToString() != String.Empty && state.Enabled && state.SelectedIndex > -1 && (US_STATE)DropdownHelper.GetValue(state) != US_STATE.None)
            {
                sortArgs.State = (US_STATE)DropdownHelper.GetValue(state);
            }
            if (popupStateTimezone.EditValue != null && popupStateTimezone.EditValue.ToString() != String.Empty && timezoneCombo.Enabled && timezoneCombo.SelectedIndex > -1)
            {
                sortArgs.TzDescriptor = (string)DropdownHelper.GetValue(timezoneCombo);
            }
            if (this.Controls.Contains(queue)) 
            {
                sortArgs.Queue = (OCS_QUEUE)DropdownHelper.GetValue(queue);
            }

            sortArgs.Program = this.program;

            try
            {
                claimedClient = repository.claimNextSort(provider, sortArgs);

                if (claimedClient != null)
                {
                    DialogResult = System.Windows.Forms.DialogResult.OK;
                    this.Close();
                }
                else
                {
                    DebtPlus.Data.Forms.MessageBox.Show("Sorry, there are no client records that match your selection criteria.");
                }
            }
            catch (System.Data.SqlClient.SqlException sql)
            {
                if (sql.Message.Contains("permission was denied"))
                {
                    DebtPlus.Data.Forms.MessageBox.Show(this, "Sorry, it appears you do not have database permissions to perform this operation.", "Permission Denied", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                else
                {
                    throw;
                }
            }
            catch
            {
                DebtPlus.Data.Forms.MessageBox.Show(this, "Sorry, we are not allowed to call the selected timezone(s) before 8am.", "No Callable Timezone");
            }
        }
                
        private void minAttempts_Properties_EditValueChanging(object sender, DevExpress.XtraEditors.Controls.ChangingEventArgs e)
        {
            decimal val = Convert.ToDecimal(e.NewValue);
            e.Cancel = val < minAttempts.Properties.MinValue;
            if (e.Cancel)
            {
                minAttempts.Value = 0m;
                minAttempts.EditValue = 0;
            }
            else if(filterBy.SelectedText.StartsWith("Contact Attempts"))
            {
                var maxValue = maxAttempts.Value;
                popupFilterBy.EditValue = String.Format("Contact Attempts ({0} to {1})",val,maxValue);
            }
        }

        private void maxAttempts_EditValueChanging(object sender, DevExpress.XtraEditors.Controls.ChangingEventArgs e)
        {
            decimal val = Convert.ToDecimal(e.NewValue);
            e.Cancel = val < maxAttempts.Properties.MinValue;
            if (e.Cancel)
            {
                maxAttempts.Value = 0m;
                maxAttempts.EditValue = 0;
            }
            else if (filterBy.SelectedText.StartsWith("Contact Attempts"))
            {
                var minValue = minAttempts.Value;
                popupFilterBy.EditValue = String.Format("Contact Attempts ({0} to {1})", minValue, val);
            }
        }

        private void timezoneCombo_SelectedValueChanged(object sender, EventArgs e)
        {
            state.Enabled = (timezoneCombo.SelectedIndex == 0);
            popupStateTimezone.EditValue = getStateTimezoneValue();

            if (popupStateTimezone.EditValue.ToString() == string.Empty)
            {
                state.SelectedIndex = 0;
            }
        }

        private void statusCode_EditValueChanged(object sender, EventArgs e)
        {
            if (filterBy.EditValue.ToString().StartsWith("Specific OCS Status") && statusCode.SelectedIndex != -1)
            {
                popupFilterBy.EditValue = "Specific OCS Status" + 
                    (statusCode.EditValue != null ? 
                    " - " + statusCode.EditValue.ToString() : string.Empty);
            }

        }

        private void ClientSortForm_Load(object sender, EventArgs e)
        {
            this.Text = "Client Sort - " + ProgramConfigService.getDescription(this.program);

            this.counseled_from_date.EditValue = DateTime.Today;
            //this.counseled_from_date.Properties.MinValue = DateTime.Today.AddDays(-365);
            this.counseled_to_date.EditValue = DateTime.Today;
        }

        private void lastChance_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (lastChance.SelectedIndex == 2)
            {
                counseled_from_date.Enabled = true;
                counseled_to_date.Enabled = true;
                
            }
            else
            {
                counseled_from_date.Enabled = false;
                counseled_to_date.Enabled = false;
                counseledState.SelectedIndex = -1;
                
            }

            if (lastChance.SelectedIndex > 0)
            {
                counseledState.Enabled = true;
            }
            else
            {
                counseledState.Enabled = false;
            }

            SetCounseledPopupLabel();
        }

        private void SetCounseledPopupLabel()
        {
            string popUpText = string.Empty;
            if (lastChance.SelectedIndex > 0)
            {
                popUpText = lastChance.SelectedText;
            }

            if (lastChance.SelectedIndex == 2)
            {
                string fromDate = (counseled_from_date.EditValue != null) ? "" + ((DateTime)counseled_from_date.EditValue).ToString("MM/dd/yyyy") : "";
                string toDate = (counseled_to_date.EditValue != null) ? "" + ((DateTime)counseled_to_date.EditValue).ToString("MM/dd/yyyy") : "";
                popUpText = string.Format("{0} ({1} - {2})", lastChance.SelectedText, fromDate, toDate);
            }

            if(counseledState.SelectedIndex > 0)
            {
                if(!string.IsNullOrWhiteSpace(popUpText))
                {
                    popUpText += ", " + counseledState.SelectedText;
                }
                else
                {
                    popUpText = counseledState.SelectedText;
                }
            }

            popupLastChance.EditValue = popUpText;
        }

        private void counseledState_SelectedIndexChanged(object sender, EventArgs e)
        {
            SetCounseledPopupLabel();
        }

        private void counseled_from_date_EditValueChanged(object sender, EventArgs e)
        {
            //if (counseled_from_date.EditValue != null)
            //{
            //    DateTime fromDate = (DateTime)counseled_from_date.EditValue;
            //    DateTime toDate = (DateTime)counseled_to_date.EditValue;

            //    //counseled_to_date.Properties.MinValue = toDate;

            //    /*if (fromDate < DateTime.Today.AddDays(-365))
            //    {
            //        MessageBox.Show("Please select from date less than 365 days.");
            //        counseled_from_date.EditValue = DateTime.Today;
            //    }
            //    else*/
            //    if (toDate > DateTime.MinValue && toDate < fromDate)
            //    {
            //        MessageBox.Show("Please select from date less than thru date.");
            //        counseled_from_date.EditValue = DateTime.Today;
            //    }

            //    SetCounseledPopupLabel();
            //}
            SetCounseledPopupLabel();
        }

        private void counseled_to_date_EditValueChanged(object sender, EventArgs e)
        {
            //if (counseled_to_date.EditValue != null)
            //{
            //    DateTime fromDate = (DateTime)counseled_from_date.EditValue;
            //    DateTime toDate = (DateTime)counseled_to_date.EditValue;

            //    //counseled_from_date.Properties.MaxValue = toDate;

            //    /*if (toDate < DateTime.Today)
            //    {
            //        MessageBox.Show("Please select from date greater than today.");
            //        counseled_to_date.EditValue = DateTime.Today;
            //    }
            //    else*/
            //    if (toDate < fromDate)
            //    {
            //        MessageBox.Show("Please select thru date greater than from date.");
            //        counseled_to_date.EditValue = DateTime.Today;
            //    }

            //    SetCounseledPopupLabel();
            //}
            SetCounseledPopupLabel();
        }
    }
}