﻿namespace DebtPlus.UI.Housing.OCS
{
    partial class ClientSearchSelectForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Button_Cancel = new DevExpress.XtraEditors.SimpleButton();
            this.Button_OK = new DevExpress.XtraEditors.SimpleButton();
            this.chk_view_preview = new DevExpress.XtraEditors.CheckEdit();
            this.Button_Print = new DevExpress.XtraEditors.SimpleButton();
            this.GridControl1 = new DevExpress.XtraGrid.GridControl();
            this.GridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.grid_col_client = new DevExpress.XtraGrid.Columns.GridColumn();
            this.grid_col_name = new DevExpress.XtraGrid.Columns.GridColumn();
            this.grid_col_phone = new DevExpress.XtraGrid.Columns.GridColumn();
            this.grid_col_ssn = new DevExpress.XtraGrid.Columns.GridColumn();
            this.grid_col_program = new DevExpress.XtraGrid.Columns.GridColumn();
            this.grid_col_address = new DevExpress.XtraGrid.Columns.GridColumn();
            this.Button_More = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)(this.DxErrorProvider1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk_view_preview.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // Button_Cancel
            // 
            this.Button_Cancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.Button_Cancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.Button_Cancel.Location = new System.Drawing.Point(560, 60);
            this.Button_Cancel.Name = "Button_Cancel";
            this.Button_Cancel.Size = new System.Drawing.Size(75, 25);
            this.Button_Cancel.TabIndex = 2;
            this.Button_Cancel.Text = "&Cancel";
            this.Button_Cancel.ToolTip = "Click here to dismiss this dialog and return to the previous input form";
            this.Button_Cancel.ToolTipController = this.ToolTipController1;
            // 
            // Button_OK
            // 
            this.Button_OK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.Button_OK.Location = new System.Drawing.Point(560, 17);
            this.Button_OK.Name = "Button_OK";
            this.Button_OK.Size = new System.Drawing.Size(75, 25);
            this.Button_OK.TabIndex = 1;
            this.Button_OK.Text = "&OK";
            this.Button_OK.ToolTip = "Click here to accept the selected Client from the list";
            this.Button_OK.ToolTipController = this.ToolTipController1;
            // 
            // chk_view_preview
            // 
            this.chk_view_preview.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.chk_view_preview.Location = new System.Drawing.Point(6, 326);
            this.chk_view_preview.Name = "chk_view_preview";
            this.chk_view_preview.Properties.Caption = "Include Client home address";
            this.chk_view_preview.Size = new System.Drawing.Size(272, 19);
            this.chk_view_preview.TabIndex = 5;
            this.chk_view_preview.ToolTip = "Do you wish to display the Client\'s address in the list also?";
            this.chk_view_preview.ToolTipController = this.ToolTipController1;
            // 
            // Button_Print
            // 
            this.Button_Print.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.Button_Print.Location = new System.Drawing.Point(560, 296);
            this.Button_Print.Name = "Button_Print";
            this.Button_Print.Size = new System.Drawing.Size(75, 24);
            this.Button_Print.TabIndex = 4;
            this.Button_Print.Text = "&Print";
            this.Button_Print.ToolTip = "Click here to print the existing list to the printer. A preview is opened first, " +
                "so choose \"print\" from there if you wish.";
            this.Button_Print.ToolTipController = this.ToolTipController1;
            // 
            // GridControl1
            // 
            this.GridControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.GridControl1.Location = new System.Drawing.Point(8, 9);
            this.GridControl1.MainView = this.GridView1;
            this.GridControl1.Name = "GridControl1";
            this.GridControl1.Size = new System.Drawing.Size(544, 311);
            this.GridControl1.TabIndex = 0;
            this.GridControl1.ToolTipController = this.ToolTipController1;
            this.GridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.GridView1});
            // 
            // GridView1
            // 
            this.GridView1.Appearance.ColumnFilterButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(104)))), ((int)(((byte)(184)))), ((int)(((byte)(251)))));
            this.GridView1.Appearance.ColumnFilterButton.ForeColor = System.Drawing.Color.White;
            this.GridView1.Appearance.ColumnFilterButton.Options.UseBackColor = true;
            this.GridView1.Appearance.ColumnFilterButton.Options.UseBorderColor = true;
            this.GridView1.Appearance.ColumnFilterButton.Options.UseForeColor = true;
            this.GridView1.Appearance.ColumnFilterButtonActive.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(170)))), ((int)(((byte)(216)))), ((int)(((byte)(254)))));
            this.GridView1.Appearance.ColumnFilterButtonActive.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(170)))), ((int)(((byte)(216)))), ((int)(((byte)(254)))));
            this.GridView1.Appearance.ColumnFilterButtonActive.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.ColumnFilterButtonActive.Options.UseBackColor = true;
            this.GridView1.Appearance.ColumnFilterButtonActive.Options.UseBorderColor = true;
            this.GridView1.Appearance.ColumnFilterButtonActive.Options.UseForeColor = true;
            this.GridView1.Appearance.Empty.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(246)))), ((int)(((byte)(255)))));
            this.GridView1.Appearance.Empty.BackColor2 = System.Drawing.Color.White;
            this.GridView1.Appearance.Empty.Options.UseBackColor = true;
            this.GridView1.Appearance.EvenRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(251)))), ((int)(((byte)(255)))));
            this.GridView1.Appearance.EvenRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(251)))), ((int)(((byte)(255)))));
            this.GridView1.Appearance.EvenRow.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.EvenRow.Options.UseBackColor = true;
            this.GridView1.Appearance.EvenRow.Options.UseBorderColor = true;
            this.GridView1.Appearance.EvenRow.Options.UseForeColor = true;
            this.GridView1.Appearance.FilterCloseButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(104)))), ((int)(((byte)(184)))), ((int)(((byte)(251)))));
            this.GridView1.Appearance.FilterCloseButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(104)))), ((int)(((byte)(184)))), ((int)(((byte)(251)))));
            this.GridView1.Appearance.FilterCloseButton.ForeColor = System.Drawing.Color.White;
            this.GridView1.Appearance.FilterCloseButton.Options.UseBackColor = true;
            this.GridView1.Appearance.FilterCloseButton.Options.UseBorderColor = true;
            this.GridView1.Appearance.FilterCloseButton.Options.UseForeColor = true;
            this.GridView1.Appearance.FilterPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(246)))), ((int)(((byte)(255)))));
            this.GridView1.Appearance.FilterPanel.BackColor2 = System.Drawing.Color.White;
            this.GridView1.Appearance.FilterPanel.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.FilterPanel.Options.UseBackColor = true;
            this.GridView1.Appearance.FilterPanel.Options.UseForeColor = true;
            this.GridView1.Appearance.FixedLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(59)))), ((int)(((byte)(133)))), ((int)(((byte)(195)))));
            this.GridView1.Appearance.FixedLine.Options.UseBackColor = true;
            this.GridView1.Appearance.FocusedCell.BackColor = System.Drawing.Color.White;
            this.GridView1.Appearance.FocusedCell.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.FocusedCell.Options.UseBackColor = true;
            this.GridView1.Appearance.FocusedCell.Options.UseForeColor = true;
            this.GridView1.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(38)))), ((int)(((byte)(109)))), ((int)(((byte)(189)))));
            this.GridView1.Appearance.FocusedRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(59)))), ((int)(((byte)(139)))), ((int)(((byte)(206)))));
            this.GridView1.Appearance.FocusedRow.ForeColor = System.Drawing.Color.White;
            this.GridView1.Appearance.FocusedRow.Options.UseBackColor = true;
            this.GridView1.Appearance.FocusedRow.Options.UseBorderColor = true;
            this.GridView1.Appearance.FocusedRow.Options.UseForeColor = true;
            this.GridView1.Appearance.FooterPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(104)))), ((int)(((byte)(184)))), ((int)(((byte)(251)))));
            this.GridView1.Appearance.FooterPanel.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(104)))), ((int)(((byte)(184)))), ((int)(((byte)(251)))));
            this.GridView1.Appearance.FooterPanel.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.FooterPanel.Options.UseBackColor = true;
            this.GridView1.Appearance.FooterPanel.Options.UseBorderColor = true;
            this.GridView1.Appearance.FooterPanel.Options.UseForeColor = true;
            this.GridView1.Appearance.GroupButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(104)))), ((int)(((byte)(184)))), ((int)(((byte)(251)))));
            this.GridView1.Appearance.GroupButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(104)))), ((int)(((byte)(184)))), ((int)(((byte)(251)))));
            this.GridView1.Appearance.GroupButton.Options.UseBackColor = true;
            this.GridView1.Appearance.GroupButton.Options.UseBorderColor = true;
            this.GridView1.Appearance.GroupFooter.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(170)))), ((int)(((byte)(216)))), ((int)(((byte)(254)))));
            this.GridView1.Appearance.GroupFooter.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(170)))), ((int)(((byte)(216)))), ((int)(((byte)(254)))));
            this.GridView1.Appearance.GroupFooter.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.GroupFooter.Options.UseBackColor = true;
            this.GridView1.Appearance.GroupFooter.Options.UseBorderColor = true;
            this.GridView1.Appearance.GroupFooter.Options.UseForeColor = true;
            this.GridView1.Appearance.GroupPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(246)))), ((int)(((byte)(255)))));
            this.GridView1.Appearance.GroupPanel.BackColor2 = System.Drawing.Color.White;
            this.GridView1.Appearance.GroupPanel.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.GroupPanel.Options.UseBackColor = true;
            this.GridView1.Appearance.GroupPanel.Options.UseForeColor = true;
            this.GridView1.Appearance.GroupRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(170)))), ((int)(((byte)(216)))), ((int)(((byte)(254)))));
            this.GridView1.Appearance.GroupRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(170)))), ((int)(((byte)(216)))), ((int)(((byte)(254)))));
            this.GridView1.Appearance.GroupRow.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.GroupRow.Options.UseBackColor = true;
            this.GridView1.Appearance.GroupRow.Options.UseBorderColor = true;
            this.GridView1.Appearance.GroupRow.Options.UseForeColor = true;
            this.GridView1.Appearance.HeaderPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(139)))), ((int)(((byte)(201)))), ((int)(((byte)(254)))));
            this.GridView1.Appearance.HeaderPanel.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(139)))), ((int)(((byte)(201)))), ((int)(((byte)(254)))));
            this.GridView1.Appearance.HeaderPanel.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.HeaderPanel.Options.UseBackColor = true;
            this.GridView1.Appearance.HeaderPanel.Options.UseBorderColor = true;
            this.GridView1.Appearance.HeaderPanel.Options.UseForeColor = true;
            this.GridView1.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(105)))), ((int)(((byte)(170)))), ((int)(((byte)(225)))));
            this.GridView1.Appearance.HideSelectionRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(83)))), ((int)(((byte)(155)))), ((int)(((byte)(215)))));
            this.GridView1.Appearance.HideSelectionRow.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(246)))), ((int)(((byte)(255)))));
            this.GridView1.Appearance.HideSelectionRow.Options.UseBackColor = true;
            this.GridView1.Appearance.HideSelectionRow.Options.UseBorderColor = true;
            this.GridView1.Appearance.HideSelectionRow.Options.UseForeColor = true;
            this.GridView1.Appearance.HorzLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(104)))), ((int)(((byte)(184)))), ((int)(((byte)(251)))));
            this.GridView1.Appearance.HorzLine.Options.UseBackColor = true;
            this.GridView1.Appearance.OddRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(246)))), ((int)(((byte)(255)))));
            this.GridView1.Appearance.OddRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(246)))), ((int)(((byte)(255)))));
            this.GridView1.Appearance.OddRow.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.OddRow.Options.UseBackColor = true;
            this.GridView1.Appearance.OddRow.Options.UseBorderColor = true;
            this.GridView1.Appearance.OddRow.Options.UseForeColor = true;
            this.GridView1.Appearance.Preview.Font = new System.Drawing.Font("Verdana", 7.5F);
            this.GridView1.Appearance.Preview.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(83)))), ((int)(((byte)(155)))), ((int)(((byte)(215)))));
            this.GridView1.Appearance.Preview.Options.UseFont = true;
            this.GridView1.Appearance.Preview.Options.UseForeColor = true;
            this.GridView1.Appearance.Row.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(251)))), ((int)(((byte)(255)))));
            this.GridView1.Appearance.Row.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.Row.Options.UseBackColor = true;
            this.GridView1.Appearance.Row.Options.UseForeColor = true;
            this.GridView1.Appearance.RowSeparator.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(246)))), ((int)(((byte)(255)))));
            this.GridView1.Appearance.RowSeparator.BackColor2 = System.Drawing.Color.White;
            this.GridView1.Appearance.RowSeparator.Options.UseBackColor = true;
            this.GridView1.Appearance.SelectedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(83)))), ((int)(((byte)(155)))), ((int)(((byte)(215)))));
            this.GridView1.Appearance.SelectedRow.ForeColor = System.Drawing.Color.White;
            this.GridView1.Appearance.SelectedRow.Options.UseBackColor = true;
            this.GridView1.Appearance.SelectedRow.Options.UseForeColor = true;
            this.GridView1.Appearance.TopNewRow.BackColor = System.Drawing.Color.White;
            this.GridView1.Appearance.TopNewRow.Options.UseBackColor = true;
            this.GridView1.Appearance.VertLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(104)))), ((int)(((byte)(184)))), ((int)(((byte)(251)))));
            this.GridView1.Appearance.VertLine.Options.UseBackColor = true;
            this.GridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.grid_col_client,
            this.grid_col_name,
            this.grid_col_phone,
            this.grid_col_ssn,
            this.grid_col_program,
            this.grid_col_address});
            this.GridView1.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.GridView1.GridControl = this.GridControl1;
            this.GridView1.Name = "GridView1";
            this.GridView1.OptionsBehavior.Editable = false;
            this.GridView1.OptionsLayout.StoreDataSettings = false;
            this.GridView1.OptionsPrint.PrintPreview = true;
            this.GridView1.OptionsPrint.UsePrintStyles = true;
            this.GridView1.OptionsSelection.UseIndicatorForSelection = false;
            this.GridView1.OptionsView.AutoCalcPreviewLineCount = true;
            this.GridView1.OptionsView.RowAutoHeight = true;
            this.GridView1.OptionsView.ShowDetailButtons = false;
            this.GridView1.OptionsView.ShowGroupPanel = false;
            this.GridView1.OptionsView.ShowIndicator = false;
            this.GridView1.PreviewFieldName = "Address";
            this.GridView1.PreviewIndent = 10;
            this.GridView1.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.grid_col_client, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // grid_col_client
            // 
            this.grid_col_client.Caption = "Client";
            this.grid_col_client.CustomizationCaption = "Client ID Number";
            this.grid_col_client.DisplayFormat.FormatString = "0000000";
            this.grid_col_client.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.grid_col_client.FieldName = "Client";
            this.grid_col_client.GroupFormat.FormatString = "0000000";
            this.grid_col_client.GroupFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.grid_col_client.Name = "grid_col_client";
            this.grid_col_client.OptionsColumn.AllowEdit = false;
            this.grid_col_client.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.grid_col_client.Visible = true;
            this.grid_col_client.VisibleIndex = 0;
            this.grid_col_client.Width = 53;
            // 
            // grid_col_name
            // 
            this.grid_col_name.Caption = "Name";
            this.grid_col_name.CustomizationCaption = "Name";
            this.grid_col_name.FieldName = "Name";
            this.grid_col_name.Name = "grid_col_name";
            this.grid_col_name.OptionsColumn.AllowEdit = false;
            this.grid_col_name.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.grid_col_name.Visible = true;
            this.grid_col_name.VisibleIndex = 1;
            this.grid_col_name.Width = 201;
            // 
            // grid_col_phone
            // 
            this.grid_col_phone.Caption = "Phone";
            this.grid_col_phone.CustomizationCaption = "Hone Phone Number";
            this.grid_col_phone.FieldName = "Phone";
            this.grid_col_phone.Name = "grid_col_phone";
            this.grid_col_phone.OptionsColumn.AllowEdit = false;
            this.grid_col_phone.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.grid_col_phone.Visible = true;
            this.grid_col_phone.VisibleIndex = 2;
            this.grid_col_phone.Width = 74;
            // 
            // grid_col_ssn
            // 
            this.grid_col_ssn.Caption = "SSN";
            this.grid_col_ssn.CustomizationCaption = "S.S.N.";
            this.grid_col_ssn.DisplayFormat.FormatString = "000-00-0000";
            this.grid_col_ssn.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.grid_col_ssn.FieldName = "SSN";
            this.grid_col_ssn.GroupFormat.FormatString = "000-00-0000";
            this.grid_col_ssn.GroupFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.grid_col_ssn.Name = "grid_col_ssn";
            this.grid_col_ssn.OptionsColumn.AllowEdit = false;
            this.grid_col_ssn.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.grid_col_ssn.Visible = true;
            this.grid_col_ssn.VisibleIndex = 3;
            this.grid_col_ssn.Width = 81;
            // 
            // grid_col_program
            // 
            this.grid_col_program.Caption = "Program";
            this.grid_col_program.FieldName = "Program";
            this.grid_col_program.Name = "grid_col_program";
            this.grid_col_program.Visible = true;
            this.grid_col_program.VisibleIndex = 4;
            // 
            // grid_col_address
            // 
            this.grid_col_address.Caption = "Address";
            this.grid_col_address.CustomizationCaption = "Home Address";
            this.grid_col_address.FieldName = "Address";
            this.grid_col_address.Name = "grid_col_address";
            this.grid_col_address.OptionsColumn.AllowEdit = false;
            this.grid_col_address.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.grid_col_address.OptionsColumn.ShowInCustomizationForm = false;
            // 
            // Button_More
            // 
            this.Button_More.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.Button_More.Location = new System.Drawing.Point(560, 104);
            this.Button_More.Name = "Button_More";
            this.Button_More.Size = new System.Drawing.Size(75, 24);
            this.Button_More.TabIndex = 3;
            this.Button_More.Text = "&More";
            this.Button_More.ToolTip = "Click here to continue the search with the next list of possible matches";
            this.Button_More.ToolTipController = this.ToolTipController1;
            this.Button_More.Click += new System.EventHandler(this.Button_More_Click_1);
            // 
            // ClientSearchSelectForm
            // 
            this.AcceptButton = this.Button_OK;
            this.CancelButton = this.Button_Cancel;
            this.ClientSize = new System.Drawing.Size(648, 351);
            this.Controls.Add(this.Button_More);
            this.Controls.Add(this.GridControl1);
            this.Controls.Add(this.Button_Print);
            this.Controls.Add(this.chk_view_preview);
            this.Controls.Add(this.Button_Cancel);
            this.Controls.Add(this.Button_OK);
            this.Name = "ClientSearchSelectForm";
            this.Text = "List of matching clients";
            ((System.ComponentModel.ISupportInitialize)(this.DxErrorProvider1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk_view_preview.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridView1)).EndInit();
            this.ResumeLayout(false);

        }
        internal DevExpress.XtraEditors.SimpleButton Button_Cancel;
        internal DevExpress.XtraEditors.SimpleButton Button_OK;
        internal DevExpress.XtraEditors.CheckEdit chk_view_preview;
        internal DevExpress.XtraEditors.SimpleButton Button_Print;
        internal DevExpress.XtraGrid.GridControl GridControl1;
        internal DevExpress.XtraGrid.Views.Grid.GridView GridView1;
        internal DevExpress.XtraGrid.Columns.GridColumn grid_col_client;
        internal DevExpress.XtraGrid.Columns.GridColumn grid_col_ssn;
        internal DevExpress.XtraGrid.Columns.GridColumn grid_col_phone;
        internal DevExpress.XtraGrid.Columns.GridColumn grid_col_name;
        internal DevExpress.XtraGrid.Columns.GridColumn grid_col_address;
        internal DevExpress.XtraEditors.SimpleButton Button_More;
        #endregion
        private DevExpress.XtraGrid.Columns.GridColumn grid_col_program;
    }
}