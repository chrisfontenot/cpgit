﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DebtPlus.Interfaces.Desktop;
using DebtPlus.OCS.Domain;

namespace DebtPlus.UI.Housing.OCS
{
    public class ClientSortLauncher : IDesktopMainline
    {
        protected PROGRAM program { get; set; }
        protected String programDescription { get; set; }

        public void OldAppMain(string[] args)
        {
            var t = new System.Threading.Thread(() =>
            {
                try
                {
                    using (var sortForm = new ClientSortForm(program))
                    {
                        if (sortForm.ShowDialog() != System.Windows.Forms.DialogResult.OK)
                        {
                            return;
                        }

                        using (var bc = new DebtPlus.LINQ.BusinessContext())
                        {
                            using (var clientForm = new ClientForm(bc, sortForm.repository, sortForm.claimedClient, sortForm.sortArgs))
                            {
                                clientForm.ShowDialog();
                            }
                            bc.SubmitChanges();
                        }
                    }
                }

                catch (System.Exception ex)
                {
                    DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex);
                }
            });
            t.SetApartmentState(System.Threading.ApartmentState.STA);
            t.IsBackground = false;
            t.Start();
        }

        public ClientSortLauncher()
        {
            program = PROGRAM.FreddieMacEI;
        }
    }
}
