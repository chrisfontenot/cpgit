﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DebtPlus.UI.Client.Widgets.Search
{
    internal class SearchConfig
    {
        internal class Configuration{
            public Dictionary<string, string> Category { get; set; }
            public int DefaultCategory { get; set; }
            /* CategoryRelations is defined to provide with an in-memory pseudo-"table"
             * Example "entry" in the "table"
             ["depost amt", "birthdate"], [ "not = ", relation_NE,
                                            "less than", relation_LT ]
             */
            public List<CategoryRelation> CategoryRelations { get; set; }
        }

        internal class CategoryRelation {
            public List<string> Categories { get; set; }
            public Dictionary<string, string> Relations { get; set; }
        }

        // Values used for the categories (1st dropdown field)
        public const string category_birthdate = "birthdate";
        public const string category_client = "Client";
        public const string category_last_name = "last name";
        public const string category_alias = "alias";
        public const string category_ssn = "ssn";
        public const string category_account_number = "account number";
        public const string category_deposit_amt = "deposit amt";
        public const string category_deposit_ref = "deposit ref";
        public const string category_email = "email";
        public const string category_emp_address = "emp address";
        public const string category_emp_city = "emp city";
        public const string category_emp_name = "emp name";
        public const string category_home_address = "home address";
        public const string category_home_city = "home city";
        public const string category_personal_phone = "personal phone";
        public const string category_work_phone = "work phone";

        public const string relation_EQ = "=";
        public const string relation_GE = ">=";
        public const string relation_GT = ">";
        public const string relation_LT = "<";
        public const string relation_LE = "<=";
        public const string relation_NE = "not =";
        public const string relation_between = "between";
        public const string relation_not_between = "not between";
        public const string relation_contains = "contains";
        public const string relation_not_contains = "not contain";
        public const string relation_ends = "ends";
        public const string relation_starts = "starts";
        public const string relation_not_starts = "not starts";

        public static Dictionary<SearchTarget, Configuration> Config = new Dictionary<SearchTarget, Configuration> { 
            { SearchTarget.DebtPlus, new Configuration { 
                DefaultCategory = 1,
                Category = new Dictionary<string,string> {
                    {"Birth date", category_birthdate },
                    {"Client ID", category_client},
                    {"Client Last/Former Name only", category_last_name},
                    {"Client Last/Former Name or Alias", category_alias},
                    {"Client S.S.N.", category_ssn},
                    {"Creditor Account Number", category_account_number},
                    {"Deposit Amount", category_deposit_amt},
                    {"Deposit Reference", category_deposit_ref},
                    {"Email Address", category_email},
                    {"Employer's Address", category_emp_address},
                    {"Employer's City", category_emp_city},
                    {"Employer's Name", category_emp_name},
                    {"Home Address", category_home_address},
                    {"Home city", category_home_city},
                    {"Home/Cell/Msg Phone Number", category_personal_phone},
                    {"Work Phone Number", category_work_phone}
                },
                CategoryRelations = new List<CategoryRelation>{
                    new CategoryRelation{
                        Categories = new List<string> { "Client", "deposit amt", "birthdate" },
                        Relations = new Dictionary<string,string>{ 
                                { "=", relation_EQ  },
                                { "not =", relation_NE },
                                { "less than", relation_LT },
                                { "is at least", relation_GE },
                                { "is greater than", relation_GT },
                                { "not more than", relation_LE },
                                { "between", relation_between },
                                { "not between", relation_not_between }
                            }
                    },
                    new CategoryRelation{
                        Categories = new List<string> { "last name", "alias", "account number", "deposit ref", "email", "emp address", "emp city", "emp name", "home address", "home city" },
                        Relations = new Dictionary<string,string>{ 
                                { "=", relation_EQ  },
                                { "not =", relation_NE },
                                { "starts with", relation_starts },
                                { "ends with", relation_ends },
                                { "doesn't start with", relation_not_starts },
                                { "contains", relation_contains },
                                { "doesn't contain", relation_not_contains }
                            }
                    },
                    new CategoryRelation{
                        Categories = new List<string> { "personal phone", "work phone", "ssn" },
                        Relations = new Dictionary<string,string>{ 
                                { "=", relation_EQ  },
                                { "not =", relation_NE },
                                { "less than", relation_LT },
                                { "is at least", relation_GE },
                                { "is greater than", relation_GT },
                                { "not more than", relation_LE },
                                { "between", relation_between },
                                { "not between", relation_not_between },
                                { "starts with", relation_starts },
                                { "ends with", relation_ends },
                                { "doesn't start with", relation_not_starts },
                                { "contains", relation_contains },
                                { "doesn't contain", relation_not_contains }
                            }
                    }
                }                
            }},
            { SearchTarget.OCS, new Configuration { 
                DefaultCategory = 0,

                Category = new Dictionary<string,string> {
                    {"Client ID", category_client},
                    {"Client Last/Former Name only", category_last_name},
                    {"Home Street Address", category_home_address},
                    {"Home/Cell/Msg Phone Number", category_personal_phone},
                    {"Freddie Mac Loan Number","fmac number"},
                    {"Servicer Loan Number","servicer loan no"},
                    {"Servicer Number", "servicer number"},
                    {"Batch Name/No.", "batch name"}
                },
                CategoryRelations = new List<CategoryRelation>{
                    new CategoryRelation{
                        Categories = new List<string> { "batch name" },
                        Relations = new Dictionary<string,string>{ 
                                { "=", relation_EQ  }
                            }
                    },
                    new CategoryRelation{
                        Categories = new List<string> { "Client", "fmac number", "servicer loan no", "servicer number" },
                        Relations = new Dictionary<string,string>{ 
                                { "=", relation_EQ  },
                                { "not =", relation_NE },
                                { "less than", relation_LT },
                                { "is at least", relation_GE },
                                { "is greater than", relation_GT },
                                { "not more than", relation_LE },
                                { "between", relation_between },
                                { "not between", relation_not_between }
                            }
                    },
                    new CategoryRelation{
                        Categories = new List<string> { "last name", "home address" },
                        Relations = new Dictionary<string,string>{ 
                                { "=", relation_EQ  },
                                { "not =", relation_NE },
                                { "starts with", relation_starts },
                                { "ends with", relation_ends },
                                { "doesn't start with", relation_not_starts },
                                { "contains", relation_contains },
                                { "doesn't contain", relation_not_contains }
                            }
                    },
                    new CategoryRelation{
                        Categories = new List<string> { "personal phone" },
                        Relations = new Dictionary<string,string>{ 
                                { "=", relation_EQ  },
                                { "not =", relation_NE },
                                { "less than", relation_LT },
                                { "is at least", relation_GE },
                                { "is greater than", relation_GT },
                                { "not more than", relation_LE },
                                { "between", relation_between },
                                { "not between", relation_not_between },
                                { "starts with", relation_starts },
                                { "ends with", relation_ends },
                                { "doesn't start with", relation_not_starts },
                                { "contains", relation_contains },
                                { "doesn't contain", relation_not_contains }
                            }
                    }
                }                
            }}
        };
    }
}
