﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DebtPlus.OCS.Domain;
using DebtPlus.LINQ;

namespace DebtPlus.UI.Housing.OCS
{
    public static class DropdownConfig
    {
        public static Dictionary<string, object> GetSalutations()
        {
            return new Dictionary<string, object>{
                {"Dr.", Person.SALUTATION.Dr},
                {"Mr.", Person.SALUTATION.Mr},
                {"Mrs.", Person.SALUTATION.Mrs},
                {"Ms.", Person.SALUTATION.Ms},
                {"Sr.", Person.SALUTATION.Sr},
                {"Miss.", Person.SALUTATION.Miss}
            };
        }
        public static Dictionary<string, object> GetSuffixes()
        {
            return new Dictionary<string, object> { 
                {"Ph.D.",Person.SUFFIX.PhD},
                {"M.D.",Person.SUFFIX.MD},
                {"Jr.",Person.SUFFIX.Jr},
                {"Sr.",Person.SUFFIX.Sr},
                {"II",Person.SUFFIX.II},
                {"III",Person.SUFFIX.III},
                {"IV",Person.SUFFIX.IV},
                {"V",Person.SUFFIX.V}
            };
        }
        public static Dictionary<string, object> GetPhoneTypes()
        {
            return new Dictionary<string, object>{
                {"", PHONE_TYPE.None},
                {"Client - Home", PHONE_TYPE.ClientHome},
                {"Client - Msg", PHONE_TYPE.ClientMsg},
                {"App - Cell", PHONE_TYPE.ApplicantCell},
                {"App - Work", PHONE_TYPE.ApplicantWork},
                {"CoApp - Cell", PHONE_TYPE.CoApplicantCell},
                {"CoApp - Work", PHONE_TYPE.CoApplicantWork}
            };
        }
        public static Dictionary<string, object> GetTimezones()
        {
            return new Dictionary<string, object> { 
                {"Atlantic", TIMEZONE.Atlantic},
                {"Eastern", TIMEZONE.Eastern},
                {"Central", TIMEZONE.Central},
                {"Mountain", TIMEZONE.Mountain},
                {"Pacific", TIMEZONE.Pacific},
                {"Alaska", TIMEZONE.Alaska},
                {"Hawaii", TIMEZONE.Hawaii},
                {"Chamorro", TIMEZONE.Chamorro},
                {"Indeterminate", TIMEZONE.Indeterminate}
            };
        }
        public static Dictionary<string, object> GetUSStateAbbreviations()
        {
            var values = Enum.GetValues(typeof(US_STATE)).Cast<US_STATE>().ToList();
            var toReturn = new Dictionary<string, object>();

            values.ForEach(state =>
            {
                toReturn.Add(
                    ((US_STATE)state).ToString(),
                    state);
            });

            return toReturn;
        }    
    }
}
