﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DebtPlus.Interfaces.Desktop;
using DebtPlus.UI.Housing.OCS;

namespace DebtPlus.UI.Housing.OCS.PostModPilotCheckin
{
    public class ClientSortForm_PostModPilotCheckin : ClientSortLauncher
    {
        public ClientSortForm_PostModPilotCheckin()
        {
            program = DebtPlus.OCS.Domain.PROGRAM.PostModPilotCheckin;
        }
    }
}
