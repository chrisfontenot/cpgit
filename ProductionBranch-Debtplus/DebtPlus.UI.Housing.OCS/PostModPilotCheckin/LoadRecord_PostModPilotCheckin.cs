﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Excel;
using DebtPlus.Svc.OCS;
using DebtPlus.OCS;
using DebtPlus.OCS.Domain;

namespace DebtPlus.UI.Housing.OCS
{
    public partial class LoadRecord_PostModPilotCheckin : DebtPlus.Data.Forms.DebtPlusForm
    {
        private class RunReportArgs
        {
            public string FileName { get; set; }
            public bool ReplaceRecords { get; set; }
            public string BatchName { get; set; }
        }

        protected PROGRAM program { get; set; }
        protected String programName { get; set; }
        protected Dictionary<string, Func<string, bool>> fieldValidationFunctions { get; set; }
        protected Dictionary<string, Action<string, OCSClient, LINQ.OCS_UploadRecord>> fieldConvertActions { get; set; }

        private LINQ.BusinessContext bc = null;

        public LoadRecord_PostModPilotCheckin()
        {
            DebtPlus.LINQ.SQLInfoClass SqlInfo = DebtPlus.LINQ.SQLInfoClass.getDefault();
            var connectionString = SqlInfo.ConnectionString;
            bc = new LINQ.BusinessContext(connectionString);

            InitializeComponent();

            program = PROGRAM.PostModPilotCheckin;
            fieldValidationFunctions = PostModPilotCheckinExcelMap.fieldValidationFuncs;
            fieldConvertActions = PostModPilotCheckinExcelMap.fieldConvertActions;
        }

        public LoadRecord_PostModPilotCheckin(LINQ.BusinessContext _bc)
        {
            bc = _bc;
            InitializeComponent();
        }

        private void uploadFileButton_Click(object sender, EventArgs e)
        {
            DialogResult result = openFileDialog1.ShowDialog();
            if (result == DialogResult.OK)
            {
                fileName.Text = openFileDialog1.FileName;

                checkState();
            }
        }

        private void safeChange(Control ctrl, Action act)
        {
            if (ctrl.InvokeRequired)
            {
                ctrl.Invoke(act);
            }
            else
            {
                act();
            }
        }

        private void clearState()
        {

            safeChange(okButton, new Action(() => { okButton.Enabled = false; }));
            //safeChange(checkReplaceDupes,new Action(() => { checkReplaceDupes.Checked = false; }));
            safeChange(textBatchName, new Action(() => { textBatchName.Text = String.Empty; }));
            safeChange(fileName, new Action(() => { fileName.Text = String.Empty; }));

            openFileDialog1.Reset();
        }

        private void checkState()
        {
            if (!String.IsNullOrEmpty(openFileDialog1.FileName))
            {
                okButton.Enabled = true;
            }
            else
            {
                okButton.Enabled = false;
            }
        }


        private string FileFormatErrorMessage =
        @"The file format does not match this partner's requirements.

        1.  Please ensure you are uploading the correct file to the correct partner.

        2.  Please ensure all column headers and file contents are in the specified format for this partner.

        {0}";

        private void backgroundWorker1_DoWork(object sender, DoWorkEventArgs e)
        {
            var args = (RunReportArgs)e.Argument;

            using (var stream = new System.IO.FileStream(args.FileName, System.IO.FileMode.Open))
            {
                Tuple<List<Tuple<OCSClient, LINQ.OCS_UploadRecord>>, List<String>> allTheThings = null;
                
                string fileName = System.IO.Path.GetFileName(args.FileName);

                try
                {
                    //if the format of the excel spreadsheet changes (i.e. different program), then we will pass different maps of functions
                    allTheThings = ExcelToRelationalMapper.mapFile(
                                                            stream,
                                                            fieldValidationFunctions,
                                                            fieldConvertActions,
                                                            ProgramConfigService.getKeyColumn(program));
                }
                catch (InvalidSpreadsheetException ex)
                {//for now do nothing with the actual exception
                    var _err = String.Format(FileFormatErrorMessage, String.Join(Environment.NewLine + Environment.NewLine, ex.errors));
                    MessageBox.Show(_err, "Incorrect File Format", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    backgroundWorker1_RunWorkerCompleted(this, new RunWorkerCompletedEventArgs(null, null, false));
                    return;
                }

                var username = String.Format("{0}\\{1}", Environment.UserDomainName, Environment.UserName);

                try
                {
                    var report = RecordLoader.bulkLoad(
                                                bc,
                                                program,
                                                args.ReplaceRecords,
                                                args.BatchName,
                                                allTheThings.Item1,
                                                username, 
                                                fileName);

                    report.Errors = String.Join(Environment.NewLine, allTheThings.Item2).Trim();

                    backgroundWorker1_RunWorkerCompleted(this, new RunWorkerCompletedEventArgs(report, null, false));
                }
                catch (System.Data.SqlClient.SqlException sql)
                {
                    if (sql.Message.Contains("permission was denied"))
                    {
                        MessageBox.Show(this, "Sorry, it appears you do not have database permissions to perform this operation.", "Permission Denied", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                    else
                    {
                        throw;
                    }
                    return;
                }
            }
        }

        private void backgroundWorker1_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            clearState();

            if (e.Result != null)
            {
                LINQ.OCS_UploadReport report = (LINQ.OCS_UploadReport)e.Result;

                var msg =
                    String.Format("Program: {0}", programName) + Environment.NewLine +
                    String.Format("Servicer Name: {0}", report.Servicer) + Environment.NewLine +
                    String.Format("Batch Name: {0}", report.BatchName) + Environment.NewLine +
                    String.Format("Records Added: {0}", report.RecordsInserted) + Environment.NewLine +
                    String.Format("Phone Numbers: {0}", report.PhoneNumbersAdded) + Environment.NewLine +
                    String.Format("Duplicates: {0}", report.RecordsDuplicates) + Environment.NewLine +
                    String.Format("Duplicates Auto Archived/Replaced: {0}", report.DupeTableEntriesCreated) + Environment.NewLine +
                    String.Format("Missing Phone Numbers: {0}", report.RecordsMissingPhoneNumbers) + Environment.NewLine +
                    String.Format("Sent to MTI: {0}", report.RecordsSentToMTI) + Environment.NewLine + Environment.NewLine;

                if (!String.IsNullOrWhiteSpace(report.Errors))
                {
                    msg += "Completed with errors:" + Environment.NewLine + Environment.NewLine + report.Errors;
                }

                MessageBox.Show(msg, "Report");
            }
        }

        private void okButton_Click(object sender, EventArgs e)
        {
            okButton.Enabled = false;

            backgroundWorker1.RunWorkerAsync(new RunReportArgs
            {
                FileName = openFileDialog1.FileName,
                //ReplaceRecords = checkReplaceDupes.Checked,
                ReplaceRecords = true,
                BatchName = textBatchName.Text
            });

        }

        private void cancelButton_Click(object sender, EventArgs e)
        {
            clearState();
            this.Close();
        }

        private void textBatchName_TextChanged(object sender, EventArgs e)
        {
            checkState();
        }

        private void LoadRecords_Load(object sender, EventArgs e)
        {
            this.programName = ProgramConfigService.getDescription(this.program);
            this.Text = "OCS - Load - " + this.programName;
        }

        private void comboBoxEdit1_Properties_EditValueChanged(object sender, EventArgs e)
        {
            checkState();
        }
    }
}
