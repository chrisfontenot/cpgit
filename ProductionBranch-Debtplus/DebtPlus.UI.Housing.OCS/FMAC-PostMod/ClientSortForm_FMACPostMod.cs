﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DebtPlus.Interfaces.Desktop;
using DebtPlus.UI.Housing.OCS;

namespace DebtPlus.UI.Housing.OCS.FMAC_PostMod
{
    public class ClientSortForm_FMACPostMod : ClientSortLauncher
    {
        public ClientSortForm_FMACPostMod() {
            program = DebtPlus.OCS.Domain.PROGRAM.FreddieMacPostMod;
        }
    }
}
