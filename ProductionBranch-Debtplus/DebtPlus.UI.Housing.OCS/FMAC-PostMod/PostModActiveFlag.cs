﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using DebtPlus.Svc.OCS;
using System.IO;
using DebtPlus.OCS.Domain;

namespace DebtPlus.UI.Housing.OCS.FMACPostMod.Reports
{
    internal partial class PostModActiveFlagReport : DebtPlus.Reports.Template.Forms.DateReportParametersForm
    {
        internal PostModActiveFlagReport()
            : base(Utils.DateRange.Today)
        {
            InitializeComponent();
        }

        protected override void ButtonOK_Click(object sender, EventArgs e)
        {
            try
            {
                TopMost = false;
                Application.DoEvents();
                using (var reporter = new Reporter(new OCSDbMapper(new LINQ.BusinessContext())))
                {
                    reporter.GetActiveClientsSummaryExcel(PROGRAM.FreddieMacPostMod, Parameter_FromDate.Date, Parameter_ToDate.Date);
                }
                DebtPlus.Data.Forms.MessageBox.Show(Properties.Resources.ExcelSheetCreated, Properties.Resources.OperationCompleted);
                base.ButtonOK_Click(sender, e);
            }
            catch (Exception ex)
            {
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, Properties.Resources.ErrorCreatingExcel);
            }
        }
    }

    public class OCSPostModActiveFlag : DebtPlus.Interfaces.Desktop.IDesktopMainline
    {
        public void OldAppMain(string[] Arguments)
        {
            var thrd = new System.Threading.Thread(new System.Threading.ParameterizedThreadStart(Mainline_Thread))
            {
                Name = "OCSPostModActiveFlag",
                IsBackground = true
            };
            thrd.SetApartmentState(System.Threading.ApartmentState.STA);
            thrd.Start(Arguments);
        }

        private void Mainline_Thread(object obj)
        {
            using (var frm = new PostModActiveFlagReport())
            {
                frm.ShowDialog();
            }
        }
    }
}
