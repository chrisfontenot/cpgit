﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DebtPlus.OCS.Domain;

namespace DebtPlus.UI.Housing.OCS.FMAC_PostMod
{
    public class LoadRecords_FMACPostMod : DebtPlus.UI.Housing.OCS.LoadRecords
    {
        public LoadRecords_FMACPostMod() : base() {
            program = PROGRAM.FreddieMacPostMod;
            fieldValidationFunctions = FMACExcelMap.fieldValidationFuncs;
            fieldConvertActions = FMACExcelMap.fieldConvertActions;

            var oldFunc = fieldConvertActions["fmac lnno"];

            //for post-mod we need to set the Queue, while still leaving it null in the other programs
            //we accomplish that by "decorating" (lookup decorator pattern), the conversion action
            //for the primary key field
            //since this is the primary key field, we are guaranteed this will always be called for each record
            //(b/c otherwise the validators will error out before we even start doing anything)
            fieldConvertActions["fmac lnno"] = (val, client, upload) =>
            {
                client.Queue = OCS_QUEUE.HighTouch;

                oldFunc(val, client, upload);
            };

        }

        private void InitializeComponent()
        {
            ((System.ComponentModel.ISupportInitialize)(this.DxErrorProvider1)).BeginInit();
            this.SuspendLayout();
            // 
            // LoadRecords_FMACPostMod
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.ClientSize = new System.Drawing.Size(450, 251);
            this.MaximumSize = new System.Drawing.Size(460, 283);
            this.MinimumSize = new System.Drawing.Size(460, 283);
            this.Name = "LoadRecords_FMACPostMod";
            ((System.ComponentModel.ISupportInitialize)(this.DxErrorProvider1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }
    }
}