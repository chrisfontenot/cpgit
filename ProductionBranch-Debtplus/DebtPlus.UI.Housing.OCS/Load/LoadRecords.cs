﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Windows.Forms;
using DebtPlus.OCS;
using DebtPlus.OCS.Domain;
using DebtPlus.LINQ;
using System.Linq;

namespace DebtPlus.UI.Housing.OCS
{
    public partial class LoadRecords : DebtPlus.Data.Forms.DebtPlusForm
    {
        private class RunReportArgs
        {
            public string FileName { get; set; }
            public bool ReplaceRecords { get; set; }
            public string BatchName { get; set; }
        }

        protected PROGRAM program { get; set; }
        protected String programName { get; set; }
        protected Dictionary<string, Func<string, bool>> fieldValidationFunctions { get; set; }
        protected Dictionary<string, Action<string, OCSClient, LINQ.OCS_UploadRecord>> fieldConvertActions { get; set; }

        protected LINQ.BusinessContext bc = null;

        public LoadRecords()
            : base()
        {
            program = PROGRAM.FreddieMacEI;
            InitializeComponent();
            RegisterHandlers();
        }

        public LoadRecords(PROGRAM program)
            : this()
        {
            this.program = program;

            CustomizedProgram(program);
        }

        public LoadRecords(BusinessContext bc)
            : this()
        {
            this.bc = bc;
        }

        private void CustomizedProgram(PROGRAM program)
        {
            if (program == PROGRAM.FannieMaePostMod)
            {
                fieldValidationFunctions = FMAEExcelMap.fieldValidationFuncs;
                fieldConvertActions = FMAEExcelMap.fieldConvertActions;
            }
        }

        private void uploadFileButton_Click(object sender, EventArgs e)
        {
            DialogResult result = openFileDialog1.ShowDialog();
            if (result == DialogResult.OK)
            {
                fileName.Text = openFileDialog1.FileName;
                checkState();
            }
        }

        private void safeChange(Control ctrl, Action act)
        {
            if (ctrl.InvokeRequired)
            {
                ctrl.EndInvoke(ctrl.BeginInvoke(act));
            }
            else
            {
                act();
            }
        }

        private void clearState()
        {
            safeChange(okButton, new Action(() => { okButton.Enabled = false; }));
            safeChange(checkReplaceDupes, new Action(() => { checkReplaceDupes.Checked = false; }));
            safeChange(textBatchName, new Action(() => { textBatchName.Text = String.Empty; }));
            safeChange(fileName, new Action(() => { fileName.Text = String.Empty; }));

            openFileDialog1.Reset();
        }

        private void checkState()
        {
            okButton.Enabled = !String.IsNullOrEmpty(openFileDialog1.FileName);
        }

        private string FileFormatErrorMessage =
@"The file format does not match this partner's requirements.

1.  Please ensure you are uploading the correct file to the correct partner.

2.  Please ensure all column headers and file contents are in the specified format for this partner.

{0}";

        private void backgroundWorker1_DoWork(object sender, DoWorkEventArgs e)
        {
            var args = (RunReportArgs)e.Argument;
            
            using (var stream = new System.IO.FileStream(args.FileName, System.IO.FileMode.Open))
            {
                Tuple<List<Tuple<OCSClient, LINQ.OCS_UploadRecord>>, List<String>> allTheThings = null;

                string fileName = System.IO.Path.GetFileName(args.FileName);

                try
                {
                    //if the format of the excel spreadsheet changes (i.e. different program), then we will pass different maps of functions
                    allTheThings = ExcelToRelationalMapper.mapFile(
                        stream,
                        fieldValidationFunctions,
                        fieldConvertActions,
                        ProgramConfigService.getKeyColumn(program));
                }

                catch (InvalidSpreadsheetException ex)
                {   // for now do nothing with the actual exception
                    var _err = String.Format(FileFormatErrorMessage, String.Join(Environment.NewLine + Environment.NewLine, ex.errors));
                    DebtPlus.Data.Forms.MessageBox.Show(_err, "Incorrect File Format", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    e.Result = null;
                    return;
                }

                // Get the name from the system for the person running the request. Remove the domain designation from it.
                var username = DebtPlus.Utils.Format.Counselor.FormatCounselor(DebtPlus.LINQ.BusinessContext.suser_sname());

                try
                {
                    // If a connection is passed then use it.
                    if (bc != null)
                    {
                        var rpt = RecordLoader.bulkLoad(bc, program, args.ReplaceRecords, args.BatchName, allTheThings.Item1, username, fileName);
                        rpt.Errors = String.Join(Environment.NewLine, allTheThings.Item2).Trim();
                        e.Result = resultMessage(rpt);
                        return;
                    }

                    // Allocate a new business connection to the database and use it here.
                    using (var local_bc = new BusinessContext())
                    {
                        var rpt = RecordLoader.bulkLoad(local_bc, program, args.ReplaceRecords, args.BatchName, allTheThings.Item1, username, fileName);
                        rpt.Errors = String.Join(Environment.NewLine, allTheThings.Item2).Trim();
                        e.Result = resultMessage(rpt);

                        local_bc.SubmitChanges();
                        return;
                    }
                }

                catch (System.Data.SqlClient.SqlException ex)
                {
                    if (ex.Message.Contains("permission was denied"))
                    {
                        DebtPlus.Data.Forms.MessageBox.Show(this, "Sorry, it appears you do not have database permissions to perform this operation.", "Permission Denied", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                    else
                    {
                        DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error running request");
                    }
                }

                e.Result = null;
            }
        }

        private string resultMessage(LINQ.OCS_UploadReport report)
        {
            var msg = new System.Text.StringBuilder();
            msg.AppendFormat("Program: {0}\r\n", programName);
            msg.AppendFormat("Servicer Name: {0}\r\n", report.Servicer);
            msg.AppendFormat("Batch Name: {0}\r\n", report.BatchName);
            msg.AppendFormat("Records Added: {0}\r\n", report.RecordsInserted);
            msg.AppendFormat("Phone Numbers: {0}\r\n", report.PhoneNumbersAdded);
            msg.AppendFormat("Duplicates: {0}\r\n", report.RecordsDuplicates);
            msg.AppendFormat("Duplicates Auto Archived/Replaced: {0}\r\n", report.DupeTableEntriesCreated);
            msg.AppendFormat("Missing Phone Numbers: {0}\r\n", report.RecordsMissingPhoneNumbers);
            msg.AppendFormat("Sent to MTI: {0}\r\n\r\n", report.RecordsSentToMTI);

            if (!String.IsNullOrWhiteSpace(report.Errors))
            {
                msg.AppendFormat("Completed with errors:\r\n\r\n{0}", report.Errors);
            }

            return msg.ToString();
        }

        private void backgroundWorker1_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            clearState();

            string resultMessage = (string)e.Result;

            // Display the completion message if there is one
            if (!string.IsNullOrEmpty(resultMessage))
            {
                DebtPlus.Data.Forms.MessageBox.Show(resultMessage, "Report");
            }
        }

        private void okButton_Click(object sender, EventArgs e)
        {
            okButton.Enabled = false;

            backgroundWorker1.RunWorkerAsync(new RunReportArgs
            {
                FileName = openFileDialog1.FileName,
                ReplaceRecords = checkReplaceDupes.Checked,
                BatchName = textBatchName.Text
            });
        }

        private void cancelButton_Click(object sender, EventArgs e)
        {
            clearState();
            this.Close();
        }

        private void textBatchName_TextChanged(object sender, EventArgs e)
        {
            checkState();
        }

        private void LoadRecords_Load(object sender, EventArgs e)
        {
            this.programName = ProgramConfigService.getDescription(this.program);
            this.Text = "OCS - Load - " + this.programName;

            fieldValidationFunctions = FMACExcelMap.fieldValidationFuncs;
            fieldConvertActions = FMACExcelMap.fieldConvertActions;
        }

        private void comboBoxEdit1_Properties_EditValueChanged(object sender, EventArgs e)
        {
            checkState();
        }

        private void RegisterHandlers()
        {
            uploadFileButton.Click += uploadFileButton_Click;
            backgroundWorker1.DoWork += backgroundWorker1_DoWork;
            backgroundWorker1.RunWorkerCompleted += backgroundWorker1_RunWorkerCompleted;
            textBatchName.TextChanged += textBatchName_TextChanged;
            okButton.Click += okButton_Click;
            cancelButton.Click += cancelButton_Click;
            Load += LoadRecords_Load;
        }

        private void UnRegisterHandlers()
        {
            uploadFileButton.Click -= uploadFileButton_Click;
            backgroundWorker1.DoWork -= backgroundWorker1_DoWork;
            backgroundWorker1.RunWorkerCompleted -= backgroundWorker1_RunWorkerCompleted;
            textBatchName.TextChanged -= textBatchName_TextChanged;
            okButton.Click -= okButton_Click;
            cancelButton.Click -= cancelButton_Click;
            Load -= LoadRecords_Load;
        }
    }
}
