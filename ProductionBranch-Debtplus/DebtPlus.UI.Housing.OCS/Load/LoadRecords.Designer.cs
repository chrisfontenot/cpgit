﻿namespace DebtPlus.UI.Housing.OCS
{
    partial class LoadRecords
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.uploadFileButton = new DevExpress.XtraEditors.SimpleButton();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.backgroundWorker1 = new System.ComponentModel.BackgroundWorker();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.fileName = new DevExpress.XtraEditors.TextEdit();
            this.textBatchName = new DevExpress.XtraEditors.TextEdit();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.checkReplaceDupes = new DevExpress.XtraEditors.CheckEdit();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.checkAllRecords = new DevExpress.XtraEditors.CheckEdit();
            this.labelControl6 = new DevExpress.XtraEditors.LabelControl();
            this.checkDuplicateOnly = new DevExpress.XtraEditors.CheckEdit();
            this.okButton = new DevExpress.XtraEditors.SimpleButton();
            this.cancelButton = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)(this.DxErrorProvider1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fileName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBatchName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkReplaceDupes.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkAllRecords.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkDuplicateOnly.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // uploadFileButton
            // 
            this.uploadFileButton.Location = new System.Drawing.Point(149, 9);
            this.uploadFileButton.Name = "uploadFileButton";
            this.uploadFileButton.Size = new System.Drawing.Size(47, 20);
            this.uploadFileButton.TabIndex = 0;
            this.uploadFileButton.Text = "Browse";
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.DefaultExt = "xlsx";
            this.openFileDialog1.Filter = "Excel Files|*.xlsx";
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(15, 12);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(99, 13);
            this.labelControl1.TabIndex = 1;
            this.labelControl1.Text = "Document To Upload";
            // 
            // fileName
            // 
            this.fileName.Enabled = false;
            this.fileName.Location = new System.Drawing.Point(201, 9);
            this.fileName.Name = "fileName";
            this.fileName.Size = new System.Drawing.Size(237, 20);
            this.fileName.TabIndex = 2;
            // 
            // textBatchName
            // 
            this.textBatchName.Location = new System.Drawing.Point(201, 35);
            this.textBatchName.Name = "textBatchName";
            this.textBatchName.Size = new System.Drawing.Size(237, 20);
            this.textBatchName.TabIndex = 3;
            // 
            // labelControl2
            // 
            this.labelControl2.Location = new System.Drawing.Point(15, 38);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(78, 13);
            this.labelControl2.TabIndex = 4;
            this.labelControl2.Text = "Batch Name/No.";
            // 
            // checkReplaceDupes
            // 
            this.checkReplaceDupes.Location = new System.Drawing.Point(159, 74);
            this.checkReplaceDupes.Name = "checkReplaceDupes";
            this.checkReplaceDupes.Properties.Caption = "";
            this.checkReplaceDupes.Size = new System.Drawing.Size(19, 19);
            this.checkReplaceDupes.TabIndex = 5;
            // 
            // labelControl3
            // 
            this.labelControl3.Location = new System.Drawing.Point(13, 77);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(116, 13);
            this.labelControl3.TabIndex = 6;
            this.labelControl3.Text = "Archive/Replace Dupes?";
            // 
            // labelControl4
            // 
            this.labelControl4.Location = new System.Drawing.Point(13, 107);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(107, 13);
            this.labelControl4.TabIndex = 7;
            this.labelControl4.Text = "Reminder calls to MTI?";
            // 
            // labelControl5
            // 
            this.labelControl5.Location = new System.Drawing.Point(13, 136);
            this.labelControl5.Name = "labelControl5";
            this.labelControl5.Size = new System.Drawing.Size(53, 13);
            this.labelControl5.TabIndex = 9;
            this.labelControl5.Text = "All Records";
            // 
            // checkAllRecords
            // 
            this.checkAllRecords.Enabled = false;
            this.checkAllRecords.Location = new System.Drawing.Point(110, 136);
            this.checkAllRecords.Name = "checkAllRecords";
            this.checkAllRecords.Properties.Caption = "";
            this.checkAllRecords.Size = new System.Drawing.Size(19, 19);
            this.checkAllRecords.TabIndex = 8;
            // 
            // labelControl6
            // 
            this.labelControl6.Location = new System.Drawing.Point(13, 164);
            this.labelControl6.Name = "labelControl6";
            this.labelControl6.Size = new System.Drawing.Size(69, 13);
            this.labelControl6.TabIndex = 11;
            this.labelControl6.Text = "Duplicate Only";
            // 
            // checkDuplicateOnly
            // 
            this.checkDuplicateOnly.Enabled = false;
            this.checkDuplicateOnly.Location = new System.Drawing.Point(110, 161);
            this.checkDuplicateOnly.Name = "checkDuplicateOnly";
            this.checkDuplicateOnly.Properties.Caption = "";
            this.checkDuplicateOnly.Size = new System.Drawing.Size(19, 19);
            this.checkDuplicateOnly.TabIndex = 10;
            // 
            // okButton
            // 
            this.okButton.Enabled = false;
            this.okButton.Location = new System.Drawing.Point(128, 198);
            this.okButton.Name = "okButton";
            this.okButton.Size = new System.Drawing.Size(75, 23);
            this.okButton.TabIndex = 12;
            this.okButton.Text = "OK";
            // 
            // cancelButton
            // 
            this.cancelButton.Location = new System.Drawing.Point(244, 198);
            this.cancelButton.Name = "cancelButton";
            this.cancelButton.Size = new System.Drawing.Size(75, 23);
            this.cancelButton.TabIndex = 13;
            this.cancelButton.Text = "Cancel";
            // 
            // LoadRecords
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(450, 251);
            this.Controls.Add(this.cancelButton);
            this.Controls.Add(this.okButton);
            this.Controls.Add(this.labelControl6);
            this.Controls.Add(this.checkDuplicateOnly);
            this.Controls.Add(this.labelControl5);
            this.Controls.Add(this.checkAllRecords);
            this.Controls.Add(this.labelControl4);
            this.Controls.Add(this.labelControl3);
            this.Controls.Add(this.checkReplaceDupes);
            this.Controls.Add(this.labelControl2);
            this.Controls.Add(this.textBatchName);
            this.Controls.Add(this.fileName);
            this.Controls.Add(this.labelControl1);
            this.Controls.Add(this.uploadFileButton);
            this.MaximumSize = new System.Drawing.Size(466, 289);
            this.MinimumSize = new System.Drawing.Size(466, 289);
            this.Name = "LoadRecords";
            this.Text = "OCS - Load - Freddie Mac - EI";
            ((System.ComponentModel.ISupportInitialize)(this.DxErrorProvider1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fileName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBatchName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkReplaceDupes.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkAllRecords.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkDuplicateOnly.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.SimpleButton uploadFileButton;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.ComponentModel.BackgroundWorker backgroundWorker1;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.TextEdit fileName;
        private DevExpress.XtraEditors.TextEdit textBatchName;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.CheckEdit checkReplaceDupes;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraEditors.LabelControl labelControl5;
        private DevExpress.XtraEditors.CheckEdit checkAllRecords;
        private DevExpress.XtraEditors.LabelControl labelControl6;
        private DevExpress.XtraEditors.CheckEdit checkDuplicateOnly;
        private DevExpress.XtraEditors.SimpleButton okButton;
        private DevExpress.XtraEditors.SimpleButton cancelButton;
    }
}