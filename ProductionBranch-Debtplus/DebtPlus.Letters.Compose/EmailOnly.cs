using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DebtPlus;
using DebtPlus.LINQ;
using System.Linq;

using DevExpress.XtraRichEdit.API.Native;
using DevExpress.XtraRichEdit;
using System.Xml;

namespace DebtPlus.Letters.Compose
{
    public class EmailOnly : Base
    {

        /// <summary>
        /// Create instances of our class
        /// </summary>
        public EmailOnly() : base()
        {
        }

        public EmailOnly(string LetterCode) : base(LetterCode)
        {
        }

        public EmailOnly(Int32 LetterType) : base(LetterType)
        {
        }


        /// <summary>
        /// How the letter should be handled
        /// </summary>
        public override ProcessingMode Display_Mode
        {
            get
            {
                return ProcessingMode.MODE_QUEUE;
            }
            set
            {
                base.Display_Mode = value;
            }
        }


        /// <summary>
        /// Queue name for the letter
        /// </summary>
        public override string Queue_Name
        {
            get
            {
                if (base.Queue_Name == string.Empty)
                {
                    return "Queued Letter";
                }
                return base.Queue_Name;
            }
            set
            {
                base.Queue_Name = value;
            }
        }


        /// <summary>
        /// Print the letter to the printer
        /// </summary>
        protected override void DoPrintLetter(bool ShowPrinterDialog)
        {
            DoEmail();
        }
    }

    partial class Base
    {

        /// <summary>
        /// Send the current letter by Email
        /// </summary>
        public void DoEmail()
        {
            // Load the letter text for the letter
            EditFormInstance.Opacity = 0.0;
            EditFormInstance.ShowInTaskbar = false;
            EditFormInstance.Show();

            DoEmailRoutine();

            EditFormInstance.Close();

            // Write the system note that we have printed the letter
            WriteSystemNote();
        }


        /// <summary>
        /// Queue the letter for later printing if desired
        /// </summary>
        protected virtual void DoEmailRoutine()
        {
            LetterQueueThread clsx = new LetterQueueThread(this, ds, EditFormInstance.RichEditControl1, Description, "al.longyear@gmail.com");
            System.Threading.Thread thrd = new System.Threading.Thread(clsx.EmailProcedure);
            thrd.Name = "Email Letter";
            thrd.SetApartmentState(System.Threading.ApartmentState.STA);
            thrd.Start();
        }
    }

    partial class LetterQueueThread
    {
        public string EmailAddress;
        string HTMLLetterText;
        string PlainLetterText;
        string Description;

        internal LetterQueueThread(Forms.ILocalBase ltr, DataSet ds, RichEditControl EditorControl, string Description, string EmailAddress)
        {
            // Save the parameters for the queue operation
            ClientId = ltr.ClientId;
            Creditor = ltr.Creditor;
            Letter_Type = ltr.Letter_Type;
            QueueName = ltr.Queue_Name;

            // Save the parameters for this class
            this.EmailAddress = EmailAddress;
            this.ds = ds;
            this.Description = Description;

            // Obtain the letter information
            LeftMargin = EditorControl.Margin.Left;
            RightMargin = EditorControl.Margin.Right;
            TopMargin = EditorControl.Margin.Top;
            BottomMargin = EditorControl.Margin.Bottom;

            // Save the text directives from the editing control
            HTMLLetterText = EditorControl.HtmlText;
            PlainLetterText = EditorControl.Text;
        }


        /// <summary>
        /// This is the thread to send the letter to the client
        /// The actual send operation is performed in the background here.
        /// </summary>
        public void EmailProcedure()
        {
            // Find a directory for the images in the letter. Create it.
            string ImagesDirectory = System.IO.Path.Combine(System.IO.Path.GetTempPath(), System.IO.Path.GetRandomFileName());
            if (!System.IO.Directory.Exists(ImagesDirectory))
            {
                System.IO.Directory.CreateDirectory(ImagesDirectory);
            }

            try
            {
                using (DebtPlus.EmailOptions opt = DebtPlus.EmailOptions.Read())
                {
                    using (MailMergeLib.MailMergeMessage oMail = new MailMergeLib.MailMergeMessage(Description, PlainLetterText, CorrectIMG(ImagesDirectory)))
                    {
                        // adjust mail specific settings
                        oMail.CharacterEncoding = System.Text.Encoding.GetEncoding("iso-8859-1");
                        oMail.CultureInfo = new System.Globalization.CultureInfo("en-US");
                        oMail.TextTransferEncoding = System.Net.Mime.TransferEncoding.SevenBit;
                        oMail.BinaryTransferEncoding = System.Net.Mime.TransferEncoding.Base64;
                        oMail.FileBaseDir = ImagesDirectory;

                        // Add the "From" address
                        oMail.MailMergeAddresses.Add(new MailMergeLib.MailMergeAddress(MailMergeLib.MailAddressType.From, opt.SMTP_Sender_Address, opt.SMTP_Sender_Name));

                        // Add the recipient address
                        if (opt.SMTP_DebuggingTo != string.Empty)
                        {
                            oMail.MailMergeAddresses.Add(new MailMergeLib.MailMergeAddress(MailMergeLib.MailAddressType.To, opt.SMTP_DebuggingTo));
                        }
                        else
                        {
                            oMail.MailMergeAddresses.Add(new MailMergeLib.MailMergeAddress(MailMergeLib.MailAddressType.To, EmailAddress));
                        }

                        MailMergeLib.MailMergeSender oSMTP = new MailMergeLib.MailMergeSender();

                        // Define the output type, local host name and the number of failures allowed
                        oSMTP.MessageOutput = (MailMergeLib.MessageOutput)opt.SMTP_InterfaceMode;
                        oSMTP.LocalHostName = "mail." + Environment.MachineName;
                        oSMTP.MaxFailures = 1;

                        // Add the server name and authentication values
                        if (oSMTP.MessageOutput == MailMergeLib.MessageOutput.SmtpServer)
                        {
                            oSMTP.SmtpHost = opt.SMTP_Server;
                            oSMTP.SmtpPort = opt.SMTP_Port;
                            if (opt.SMTP_UserName != string.Empty)
                            {
                                oSMTP.EnableSsl = opt.SMTP_UseSSL;
                                oSMTP.SetSmtpAuthentification(opt.SMTP_UserName, opt.SMTP_Password);
                            }
                        }

                        // Ensure that the output directory exists if needed
                        if (oSMTP.MessageOutput == MailMergeLib.MessageOutput.Directory)
                        {
                            string OutputDirectory = opt.SMTP_MailOutputDirectory;
                            if (!System.IO.Directory.Exists(OutputDirectory))
                            {
                                System.IO.Directory.CreateDirectory(OutputDirectory);
                            }
                            oSMTP.MailOutputDirectory = OutputDirectory;
                        }

                        // Send the message
                        oSMTP.Send(oMail);
                    }
                }
            }
            catch (Exception ex)
            {
                DebtPlus.Errors.DebuggingException(ex);
                DebtPlus.Forms.MessageBox.Show(ex.ToString(), "Error sending Email message", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Error, System.Windows.Forms.MessageBoxDefaultButton.Button1);
            }
            finally
            {
                // Remove the images directory once the email has been sent
                DelTree(ImagesDirectory);
            }
        }

        /// <summary>
        /// Generate the proper HTML text reference. We need to correct the IMG tags.
        /// </summary>
        /// <param name="ImagesDirectory">Pointer to the directory for holding images</param>
        /// <returns></returns>
        /// <remarks></remarks>
        private string CorrectIMG(string ImagesDirectory)
        {
            // Try to remap the graphics
            HtmlAgilityPack.HtmlDocument doc = new HtmlAgilityPack.HtmlDocument();
            doc.LoadHtml(HTMLLetterText);

            // Find and replace the references to the images in the document.
            ParseNodes(doc.DocumentNode, ImagesDirectory);

            // Translate the document back to a string from the converted item.
            System.Text.StringBuilder sb = new System.Text.StringBuilder();
            using (System.IO.StringWriter oFile = new System.IO.StringWriter(sb))
            {
                doc.Save(oFile);
                oFile.Flush();
                oFile.Close();
                return sb.ToString();
            }
        }

        /// <summary>
        /// Replace the data: tags with the corresponding file references. The mailer will not
        /// accept the inline data tags when it goes to replace the files with cid references.
        /// So, parse the HTML document and find the img tags that need to be replaced. Save the
        /// images to the files, assigning them a new name each time.
        /// </summary>
        /// <param name="node">Pointer to the HTML node</param>
        /// <param name="ImageDirectory">Directory to hold the images</param>
        /// <remarks></remarks>
        private void ParseNodes(HtmlAgilityPack.HtmlNode node, string ImageDirectory)
        {
            // Handle the children in the nodes
            foreach (HtmlAgilityPack.HtmlNode childNode in node.ChildNodes)
            {
                ParseNodes(childNode, ImageDirectory);
            }

            // If the current node is an image then save the image binary to a file and replace it with the file reference.
            if (node.NodeType == HtmlAgilityPack.HtmlNodeType.Element && string.Compare(node.Name, "img", true) == 0)
            {
                string src = node.Attributes["src"].Value;
                if (src != string.Empty)
                {
                    // Look for the file type and the encoded data
                    // The other type of encoding is quoted-printable, but that is not for images so we can safely only look at base64 encoding.
                    System.Text.RegularExpressions.Match Match = System.Text.RegularExpressions.Regex.Match(src, @"^data:(?<type>[^;]+);base64,(?<data>.+)$", System.Text.RegularExpressions.RegexOptions.IgnoreCase | System.Text.RegularExpressions.RegexOptions.CultureInvariant);
                    if (Match.Success)
                    {
                        node.Attributes["src"].Value = Base64File(Match.Groups["data"].Value, ImageDirectory, Match.Groups["type"].Value);
                    }
                }
            }
        }

        /// <summary>
        /// Convert the input string from a base64 string to a file of bytes
        /// </summary>
        /// <param name="InputString">Encoded input string</param>
        /// <param name="ImageDirectory">string with the desired directory for the file</param>
        /// <param name="ImageType">File type</param>
        /// <returns>The local file name in the directory where the decoded string is stored</returns>
        /// <remarks></remarks>
        private string Base64File(string InputString, string ImageDirectory, string ImageType)
        {
            // From the mime type, find the associated extension to use
            string Extension = FindExtension(ImageType);

            // Generate the unique file in the directory
            string id = System.Text.RegularExpressions.Regex.Replace(System.Guid.NewGuid().ToString(), "[^a-zA-Z0-9]", string.Empty);
            string LocalFileName = string.Format("{0}.{1}", id, Extension);
            string FullyQualifiedName = System.IO.Path.Combine(ImageDirectory, LocalFileName);

            // Look for the most common, base64 encoding.
            using (System.IO.FileStream fs = new System.IO.FileStream(FullyQualifiedName, System.IO.FileMode.CreateNew, System.IO.FileAccess.Write, System.IO.FileShare.None))
            {
                byte[] b = Convert.FromBase64String(InputString);
                Int32 ByteCount = b.GetUpperBound(0) + 1;
                fs.Write(b, 0, ByteCount);
            }

            return LocalFileName;
        }

        /// <summary>
        /// Map the MIME type to the proper filename extension
        /// </summary>
        /// <param name="MimeType">Type of the mime entry, i.e. "image/jpeg"</param>
        /// <returns>The extension usually associated with a file of that type</returns>
        /// <remarks></remarks>
        private string FindExtension(string MimeType)
        {
            string Answer = string.Empty;
            Microsoft.Win32.RegistryKey reg = null;

            // Look for the mime type in the system registry.
            try
            {
                reg = Microsoft.Win32.Registry.ClassesRoot.OpenSubKey(string.Format(@"MIME\Database\Content Type\{0}", MimeType), false);
                if (reg != null)
                {
                    Answer = Convert.ToString(reg.GetValue("Extension", string.Empty));
                }
            }

            catch (Exception ex)
            {
                DebtPlus.Errors.DebuggingException(ex);
            }

            finally
            {
                if (reg != null)
                {
                    reg.Close();
                }
            }

            // If we don't have a value then use the right portion of the name for the type.
            if (Answer == string.Empty)
            {
                Int32 iPos = MimeType.TrimEnd('/').LastIndexOf('/');
                if (iPos >= 0)
                {
                    Answer = MimeType.Substring(iPos + 1);
                }
            }

            // Supply a default value if one is missing
            if (Answer == string.Empty || Answer == "jpeg")
            {
                Answer = "jpg";
            }

            // Remove a leading period from the name. We just want the text.
            return Answer.TrimStart('.');
        }

        /// <summary>
        /// Delete the directory tree at the indicated directory.
        /// </summary>
        /// <param name="Directory"></param>
        /// <returns></returns>
        /// <remarks></remarks>
        private bool DelTree(string Directory)
        {
            bool Answer = false;
            try
            {
                // Remove any sub-directory entry
                string[] Names = System.IO.Directory.GetDirectories(Directory);
                foreach (string name in Names)
                {
                    if (!DelTree(name))
                    {
                        return false;
                    }
                }

                // Remove the files in the directory
                Names = System.IO.Directory.GetFiles(Directory);
                foreach (string name in Names)
                {
                    System.IO.File.Delete(name);
                }

                // Remove the directory
                System.IO.Directory.Delete(Directory);
                Answer = true;
            }
            catch { }
            return Answer;
        }
    }
}
