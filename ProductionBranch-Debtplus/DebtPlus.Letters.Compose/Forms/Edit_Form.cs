﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DebtPlus;
using DebtPlus.LINQ;
using System.Linq;
using DevExpress.XtraRichEdit.API.Native;
using DevExpress.XtraRichEdit;
using DevExpress.XtraRichEdit.Commands;
using DevExpress.XtraSpellChecker;

namespace DebtPlus.Letters.Compose.Forms
{
    public partial class Edit_Form : DebtPlus.Data.Forms.DebtPlusForm
    {
        private const string LettersNameString = "Letters";
        private Base cls;

        /// <summary>
        /// Should the bars be loaded and saved to the system? We need only do this if the form is visible
        /// </summary>
        /// <returns></returns>
        private bool ShouldSaveBars()
        {
            return Opacity > 0.0;
        }

        /// <summary>
        /// Event generated when the letter should be printed
        /// </summary>
        public event PrintEventHandler PrintLetter;

        /// <summary>
        /// Process the print function for the letter
        /// </summary>
        /// <param name="e"></param>
        protected virtual void OnPrintLetter(PrintEventArgs e)
        {
            var evt = PrintLetter;
            if (evt != null)
            {
                evt(this, e);
            }
        }

        /// <summary>
        /// Process the print function for the letter
        /// </summary>
        protected void RaisePrintLetter(PrintEventArgs e)
        {
            OnPrintLetter(e);
        }

        /// <summary>
        /// Event generated when we need to write the system note
        /// </summary>
        public event EventHandler GenerateSysNote;

        /// <summary>
        /// Process the function to generate the system note
        /// </summary>
        /// <param name="e"></param>
        protected virtual void OnGenerateSysNote(EventArgs e)
        {
            var evt = GenerateSysNote;
            if (evt != null)
            {
                evt(this, e);
            }
        }

        /// <summary>
        /// Process the function to generate the system note
        /// </summary>
        protected void RaiseGenerateSysNote()
        {
            OnGenerateSysNote(EventArgs.Empty);
        }

        /// <summary>
        /// Create an instance of our class
        /// </summary>
        public Edit_Form() : base()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Create an instance of our class
        /// </summary>
        /// <param name="cls"></param>
        public Edit_Form(Base cls) : this()
        {
            this.cls = cls;

            // Register the event handler functions
            RegisterHandlers();

            // Register the spell checker to spell the main text window
            DevExpress.XtraSpellChecker.Native.SpellCheckTextControllersManager.Default.RegisterClass(typeof(DevExpress.XtraRichEdit.RichEditControl), typeof(DevExpress.XtraRichEdit.SpellChecker.RichEditSpellCheckController));
        }

        /// <summary>
        /// Register the event handlers
        /// </summary>
        private void RegisterHandlers()
        {
            Load += Edit_Form_Load;
            RichEditControl1.PopupMenuShowing += RichEditControl1_PopupMenuShowing;
            ShowSpellOptions1.ItemClick += ShowSpellOptions1_ItemClick;
            SpellChecker1.CheckCompleteFormShowing += SpellChecker1_CheckCompleteFormShowing;
            FormatFont1.ItemClick += FormatFont1_ItemClick;
            FormatParagraph1.ItemClick += FormatParagraph1_ItemClick;
            ExportToPDF1.ItemClick += ExportToPDF1_ItemClick;
            BarManager1.EndCustomization += BarManager1_EndCustomization;
            QuickPrint1.ItemClick += QuickPrint1_ItemClick;
            Print1.ItemClick += Print1_ItemClick;
            PrintPreview1.ItemClick += PrintPreview1_ItemClick;
            FormExit1.ItemClick += FormExit1_ItemClick;
            SendToEmail1.ItemClick += SendToEmail1_ItemClick;
            SpellDocument1.ItemClick += SpellDocument1_ItemClick;
        }

        /// <summary>
        /// Remove the event handler registration
        /// </summary>
        private void UnRegisterHandlers()
        {
            Load -= Edit_Form_Load;
            RichEditControl1.PopupMenuShowing -= RichEditControl1_PopupMenuShowing;
            ShowSpellOptions1.ItemClick -= ShowSpellOptions1_ItemClick;
            SpellChecker1.CheckCompleteFormShowing -= SpellChecker1_CheckCompleteFormShowing;
            FormatFont1.ItemClick -= FormatFont1_ItemClick;
            FormatParagraph1.ItemClick -= FormatParagraph1_ItemClick;
            ExportToPDF1.ItemClick -= ExportToPDF1_ItemClick;
            BarManager1.EndCustomization -= BarManager1_EndCustomization;
            QuickPrint1.ItemClick -= QuickPrint1_ItemClick;
            Print1.ItemClick -= Print1_ItemClick;
            PrintPreview1.ItemClick -= PrintPreview1_ItemClick;
            FormExit1.ItemClick -= FormExit1_ItemClick;
            SendToEmail1.ItemClick -= SendToEmail1_ItemClick;
            SpellDocument1.ItemClick -= SpellDocument1_ItemClick;
        }

        /// <summary>
        /// Directory for saving the bar information
        /// </summary>
        /// <returns></returns>
        private string SaveDirectory()
        {
            string baseDir = Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData);
            return System.IO.Path.Combine(baseDir, "DebtPlus", "Letters");
        }

        /// <summary>
        /// Filename for saving the bar information
        /// </summary>
        /// <returns></returns>
        private string SaveFile()
        {
            return System.IO.Path.Combine(SaveDirectory(), "Bars.xml");
        }

        /// <summary>
        /// Process the LOAD event for the FORM
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Edit_Form_Load(object sender, System.EventArgs e) // Handles this.Load
        {
            UnRegisterHandlers();
            try
            {
                if (ShouldSaveBars())
                {
                    base.LoadPlacement(LettersNameString);
                    string FileName = SaveFile();
                    try
                    {
                        if (System.IO.File.Exists(FileName))
                        {
                            BarManager1.RestoreLayoutFromXml(FileName);
                        }
                    }
                    catch { }
                }
            }
            finally
            {
                RegisterHandlers();
            }
        }

        private void RichEditControl1_PopupMenuShowing(object sender, DevExpress.XtraRichEdit.PopupMenuShowingEventArgs e)
        {
            DevExpress.Utils.Menu.DXMenuItem mnu = new DevExpress.Utils.Menu.DXMenuItem("Spell Document...", new EventHandler(delegate(Object o, EventArgs a)
            {
                SpellDocument();
            }), Properties.Resources.SpellCheck_16x16)
            {
                BeginGroup = true,
                Shortcut = Shortcut.F7
            };
            e.Menu.Items.Add(mnu);
        }

        /// <summary>
        ///     Spell the document
        /// </summary>
        private void SpellDocument1_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e) // Handles SpellDocument1.ItemClick
        {
            SpellDocument();
        }

#region Spell Checker
        private void SpellDocument()
        {
            // Restore the option settings for the speller
            ReadSpellingOptions();

            // Set our spelling options for the new processing
            SpellChecker1.OptionsSpelling.CheckFromCursorPos = DevExpress.Utils.DefaultBoolean.False;
            SpellChecker1.OptionsSpelling.CheckSelectedTextFirst = DevExpress.Utils.DefaultBoolean.False;

            // Do the actual spelling of the note
            SpellChecker1.Check(RichEditControl1);

            // Save the spell checker options should they have been shown
            SaveSpellCheckerOptions();

            // Display the completion event.
            DebtPlus.Data.Forms.MessageBox.Show("The spelling check is complete.", Application.ProductName, MessageBoxButtons.OK);
        }

        /// <summary>
        ///     Allow the spelling options to be changed
        /// </summary>
        private void ShowSpellOptions1_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            // Restore the option settings for the speller
            ReadSpellingOptions();

            if( SpellChecker1.FormsManager.ShowOptionsForm() == DialogResult.OK )
            {
                SpellChecker1.OptionsSpelling.IgnoreEmails = DevExpress.SpellChecker.Native.BoolDefaultBooleanConverter.ConvertBoolToDefaultBoolean(SpellChecker1.FormsManager.OptionsForm.IgnoreEmails);
                SpellChecker1.OptionsSpelling.IgnoreMixedCaseWords = DevExpress.SpellChecker.Native.BoolDefaultBooleanConverter.ConvertBoolToDefaultBoolean(SpellChecker1.FormsManager.OptionsForm.IgnoreMixedCaseWords);
                SpellChecker1.OptionsSpelling.IgnoreRepeatedWords = DevExpress.SpellChecker.Native.BoolDefaultBooleanConverter.ConvertBoolToDefaultBoolean(SpellChecker1.FormsManager.OptionsForm.IgnoreRepeatedWords);
                SpellChecker1.OptionsSpelling.IgnoreUpperCaseWords = DevExpress.SpellChecker.Native.BoolDefaultBooleanConverter.ConvertBoolToDefaultBoolean(SpellChecker1.FormsManager.OptionsForm.IgnoreUpperCase);
                SpellChecker1.OptionsSpelling.IgnoreUrls = DevExpress.SpellChecker.Native.BoolDefaultBooleanConverter.ConvertBoolToDefaultBoolean(SpellChecker1.FormsManager.OptionsForm.IgnoreUrls);
                SpellChecker1.OptionsSpelling.IgnoreWordsWithNumbers = DevExpress.SpellChecker.Native.BoolDefaultBooleanConverter.ConvertBoolToDefaultBoolean(SpellChecker1.FormsManager.OptionsForm.IgnoreWordsWithDigits);

                // Save the spelling culture
                SpellChecker1.Culture = SpellChecker1.FormsManager.OptionsForm.Culture;

                // Save the options
                SaveSpellCheckerOptions();
            }
        }

        private bool FirstSpelling = true;
        private static string savedOptionsPathName = System.IO.Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData), "DebtPlus", "Spelling");
        private static string SavedOptionsFileName = System.IO.Path.Combine(savedOptionsPathName, "Options.xml");

        /// <summary>
        ///     Set the spelling options into the spelling control
        /// </summary>
        private void ReadSpellingOptions()
        {
            try
            {
                using(DebtPlus.UI.Common.CursorManager cm = new DebtPlus.UI.Common.CursorManager())
                {
                    // Load the dictionaries on the first time
                    if( SharedDictionaryStorage1.Dictionaries.Count == 0 )
                    {
                        DebtPlus.UI.Common.SpellCheckerDictionaries.SetSpellCheckerDictionaries(ref SharedDictionaryStorage1);
                    }

                    // if this is the first time then load the options from the saved storage
                    if( FirstSpelling )
                    {
                        FirstSpelling = false;

                        if (System.IO.File.Exists(SavedOptionsFileName))
                        {
                            SpellChecker1.RestoreFromXML(SavedOptionsFileName);
                        }
                    }
               }
            }
            catch(System.Exception ex) // Don't trap if we have an error. Just ignore it.
            {
                DebtPlus.Svc.Common.ErrorHandling.HandleErrors(ex);
            }
        }

        /// <summary>
        ///     Save the spell checking options to the disk system for next time
        /// </summary>
        private void SaveSpellCheckerOptions()
        {
            try
            {
                if( ! System.IO.Directory.Exists(savedOptionsPathName) )
                {
                    System.IO.Directory.CreateDirectory(savedOptionsPathName);
                }
                SpellChecker1.SaveToXML(SavedOptionsFileName);
            }
            catch(System.Exception ex) // Don't trap if we have an error. Just ignore it.
            {
                DebtPlus.Svc.Common.ErrorHandling.HandleErrors(ex);
            }
        }

        /// <summary>
        ///     Callback from the speller to indicate that the operation is complete
        /// </summary>
        private void SpellChecker1_CheckCompleteFormShowing(object sender, DevExpress.XtraSpellChecker.FormShowingEventArgs e) // Handles SpellChecker1_CheckCompleteFormShowing
        {
            e.Handled = true;
        }

#endregion

        private void FormatFont1_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e) // Handles FormatFont1.ItemClick
        {
            ShowFontFormCommand cmd = new ShowFontFormCommand(RichEditControl1);
            cmd.Execute();
        }

        private void FormatParagraph1_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e) // Handles FormatParagraph1.ItemClick
        {
            ShowParagraphFormCommand cmd = new ShowParagraphFormCommand(RichEditControl1);
            cmd.Execute();
        }

        private void ExportToPDF1_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e) // Handles ExportToPDF1.ItemClick
        {
            string fileName = String.Empty;
            using(SaveFileDialog frm = new SaveFileDialog()
            {
                AddExtension = true,
                AutoUpgradeEnabled = true,
                CheckFileExists = false,
                CheckPathExists = true,
                DefaultExt = ".pdf",
                DereferenceLinks = true,
                Filter = "PDF Files (*.pdf)|*.pdf|All Files (*.*)|*.*",
                FilterIndex = 0,
                InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments),
                OverwritePrompt = true,
                RestoreDirectory = true,
                Title = "Save letter as PDF file"
            }) {
                if( frm.ShowDialog() == DialogResult.OK )
                {
                    fileName = frm.FileName;
                }
            }

            // Do the save operation
            if(! string.IsNullOrEmpty(fileName))
            {
                DevExpress.XtraPrinting.PrintingSystem ps = null;
                DevExpress.XtraPrinting.PrintableComponentLink pcl = null;

                try
                {
                    using(DebtPlus.UI.Common.CursorManager cm = new DebtPlus.UI.Common.CursorManager())
                    {
                        ps = CreatePS();
                        pcl = new DevExpress.XtraPrinting.PrintableComponentLink(ps)
                        {
                            Component = RichEditControl1,

                            // Set the options for the component linkage
                            EnablePageDialog = false,
                            Landscape = false,
                            Margins = new System.Drawing.Printing.Margins(25, 25, 25, 25),
                            PaperKind = System.Drawing.Printing.PaperKind.Letter
                        };

                        pcl.CreateDocument(ps);

                        // Create the letter
                        ps.ExportToPdf(fileName);

                        // Generate the system note at this point.
                        RaiseGenerateSysNote();
                    }
                }

                finally
                {
                    if( pcl != null )
                    {
                        pcl.Dispose();
                    }
                    if( ps != null )
                    {
                        ps.Dispose();
                    }
                }
            }
        }

        private void BarManager1_EndCustomization(object sender, EventArgs e) // Handles BarManager1.EndCustomization
        {
            if( ShouldSaveBars() )
            {
                string pathName = SaveDirectory();
                if (! System.IO.Directory.Exists(pathName))
                {
                    System.IO.Directory.CreateDirectory(pathName);
                }
                BarManager1.SaveLayoutToXml(SaveFile());
            }
        }

        private DevExpress.XtraPrinting.PrintingSystem CreatePS()
        {
            DevExpress.XtraPrinting.PrintingSystem ps = new DevExpress.XtraPrinting.PrintingSystem()
            {
                ShowMarginsWarning = false,
                ShowPrintStatusDialog = false
            };

            ps.ExportOptions.Pdf.ShowPrintDialogOnOpen = false;
            ps.ExportOptions.Pdf.ShowPrintDialogOnOpen = false;
            ps.ExportOptions.Pdf.Compressed = true;
            ps.ExportOptions.Pdf.DocumentOptions.Application = "DebtPlus";
            ps.ExportOptions.Pdf.DocumentOptions.Author = "DebtPlus, L.L.C.";
            ps.ExportOptions.Pdf.DocumentOptions.Subject = "Letter Document";
            ps.ExportOptions.Pdf.DocumentOptions.Title = cls.Description;

            ps.ExportOptions.PrintPreview.ActionAfterExport = DevExpress.XtraPrinting.ActionAfterExport.Open;
            ps.ExportOptions.PrintPreview.DefaultDirectory = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
            ps.ExportOptions.PrintPreview.DefaultSendFormat = DevExpress.XtraPrinting.PrintingSystemCommand.ExportPdf;
            ps.ExportOptions.PrintPreview.SaveMode = DevExpress.XtraPrinting.SaveMode.UsingSaveFileDialog;
            ps.ExportOptions.PrintPreview.ShowOptionsBeforeExport = false;

            return ps;
        }

        private void SendToEmail1_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e) // Handles SendToEmail1.ItemClick
        {
            DevExpress.XtraPrinting.PrintingSystem ps = null;
            DevExpress.XtraPrinting.PrintableComponentLink pcl = null;
            try
            {
                using(DebtPlus.UI.Common.CursorManager cm = new DebtPlus.UI.Common.CursorManager())
                {
                    ps = CreatePS();
                    pcl = new DevExpress.XtraPrinting.PrintableComponentLink(ps)
                    {
                        Component = RichEditControl1,

                        // Set the options for the component linkage
                        EnablePageDialog = false,
                        Landscape = false,
                        Margins = new System.Drawing.Printing.Margins(25, 25, 25, 25),
                        PaperKind = System.Drawing.Printing.PaperKind.Letter
                    };

                    pcl.CreateDocument(ps);
                    ps.ExecCommand(DevExpress.XtraPrinting.PrintingSystemCommand.SendPdf);

                    // Generate the system note at this point.
                    RaiseGenerateSysNote();
                }
            }
            finally
            {
                if( pcl != null )
                {
                    pcl.Dispose();
                }
                if( ps != null )
                {
                    ps.Dispose();
                }
            }
        }

        private void QuickPrint1_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e) // Handles QuickPrint1.ItemClick
        {
            PrintEventDirectArgs EArgs = new PrintEventDirectArgs(false);
            RaisePrintLetter(EArgs);
            if( ! EArgs.Cancel )
            {
                QuickPrintCommand cmd = new QuickPrintCommand(RichEditControl1);
                cmd.Execute();
                RaiseGenerateSysNote();
            }
        }

        private void Print1_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e) // Handles Print1.ItemClick
        {
            PrintEventDirectArgs EArgs = new PrintEventDirectArgs(false);
            RaisePrintLetter(EArgs);
            if( ! EArgs.Cancel )
            {
                PrintCommand cmd = new PrintCommand(RichEditControl1);
                cmd.Execute();
                RaiseGenerateSysNote();
            }
        }

        private void PrintPreview1_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e) // Handles PrintPreview1.ItemClick
        {
            DevExpress.XtraPrinting.PrintingSystem ps = null;
            DevExpress.XtraPrinting.PrintableComponentLink pcl = null;
            try
            {
                using(DebtPlus.UI.Common.CursorManager cm = new DebtPlus.UI.Common.CursorManager())
                {
                    ps = CreatePS();
                    pcl = new DevExpress.XtraPrinting.PrintableComponentLink(ps)
                    {
                        Component = RichEditControl1,

                        // Set the options for the component linkage
                        EnablePageDialog = false,
                        Landscape = false,
                        Margins = new System.Drawing.Printing.Margins(25, 25, 25, 25),
                        PaperKind = System.Drawing.Printing.PaperKind.Letter
                    };

                    pcl.CreateDocument(ps);

                    using(PreviewForm frm = new PreviewForm())
                    {
                        frm.PrintControl1.PrintingSystem = ps;

                        frm.PrintCommand1.ItemClick += Print1_ItemClick;
                        frm.QuickPrintCommand1.ItemClick += QuickPrint1_ItemClick;

                        frm.PrintCommand1.Enabled = true;
                        frm.QuickPrintCommand1.Enabled = true;

                        frm.ShowDialog();

                        frm.PrintCommand1.ItemClick -= Print1_ItemClick;
                        frm.QuickPrintCommand1.ItemClick -= QuickPrint1_ItemClick;
                    }
                }
            }
            finally
            {
                if( pcl != null )
                {
                    pcl.Dispose();
                }
                if( ps != null )
                {
                    ps.Dispose();
                }
            }
        }

        private void FormExit1_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e) // Handles FormExit1.ItemClick
        {
            DialogResult = DialogResult.OK;
            if (!Modal)
            {
                Close();
            }
        }
    }
}