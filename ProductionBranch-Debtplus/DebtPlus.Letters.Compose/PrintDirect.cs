﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DebtPlus.LINQ;

namespace DebtPlus.Letters.Compose.Forms
{
    public class PrintEventDirectArgs : PrintEventArgs
    {
        public override bool UsePrintDialog
        {
            get
            {
                return true;
            }
        }

        public PrintEventDirectArgs() : base()
        {
        }

        public PrintEventDirectArgs(bool Cancel) : base(Cancel)
        {
        }
    }
}
