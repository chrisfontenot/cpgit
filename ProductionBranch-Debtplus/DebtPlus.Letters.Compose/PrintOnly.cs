using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DebtPlus;
using DebtPlus.LINQ;
using System.Linq;

namespace DebtPlus.Letters.Compose
{
    public class PrintOnly : Base
    {

        /// <summary>
        /// Create instances of our class
        /// </summary>
        public PrintOnly() : base()
        {
        }

        public PrintOnly(string LetterCode) : base(LetterCode)
        {
        }

        public PrintOnly(Int32 LetterType) : base(LetterType)
        {
        }


        /// <summary>
        /// Indicate that the letter is to be printed only
        /// </summary>
        public override Base.ProcessingMode Display_Mode
        {
            get
            {
                return ProcessingMode.MODE_PRINT;
            }
            set
            {
                base.Display_Mode = value;
            }
        }


        /// <summary>
        /// Print the letter to the printer
        /// </summary>
        protected override void DoPrintLetter(bool ShowPrinterDialog)
        {
            DoPrint(ShowPrinterDialog);
        }
    }
}
