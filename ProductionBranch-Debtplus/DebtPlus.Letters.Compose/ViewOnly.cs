using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DebtPlus;
using DebtPlus.LINQ;
using System.Linq;

namespace DebtPlus.Letters.Compose
{
    public class ViewOnly : PrintOnly
    {
        /// <summary>
        /// Create instances of our class
        /// </summary>
        public ViewOnly() : base()
        {
        }

        /// <summary>
        /// Create instances of our class
        /// </summary>
        /// <param name="LetterCode"></param>
        public ViewOnly(string LetterCode) : base(LetterCode)
        {
        }

        /// <summary>
        /// Create instances of our class
        /// </summary>
        /// <param name="LetterType"></param>
        public ViewOnly(Int32 LetterType) : base(LetterType)
        {
        }

        /// <summary>
        /// Indicate that the letter is to be printed only
        /// </summary>
        public override Base.ProcessingMode Display_Mode
        {
            get
            {
                return ProcessingMode.MODE_VIEW;
            }
            set
            {
                base.Display_Mode = value;
            }
        }

        /// <summary>
        /// Do the "Print" operation to the local display
        /// </summary>
        /// <param name="ShowPrinterDialog"></param>
        protected override void DoPrintLetter(bool ShowPrinterDialog)
        {
            DoShow();
        }
    }
}
