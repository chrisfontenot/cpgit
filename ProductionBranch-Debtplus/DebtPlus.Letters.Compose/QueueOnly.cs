using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DebtPlus;
using DebtPlus.LINQ;
using System.Linq;

namespace DebtPlus.Letters.Compose
{
    public class QueueOnly : Base
    {

        /// <summary>
        /// Create instances of our class
        /// </summary>
        public QueueOnly() : base()
        {
        }

        public QueueOnly(string LetterCode) : base(LetterCode)
        {
        }

        public QueueOnly(Int32 LetterType) : base(LetterType)
        {
        }


        /// <summary>
        /// How the letter should be handled
        /// </summary>
        public override Base.ProcessingMode Display_Mode
        {
            get
            {
                return ProcessingMode.MODE_QUEUE;
            }
            set
            {
                base.Display_Mode = value;
            }
        }


        /// <summary>
        /// Queue name for the letter
        /// </summary>
        public override string Queue_Name
        {
            get
            {
                if( base.Queue_Name == string.Empty )
                {
                    return "Queued Letter";
                }
                return base.Queue_Name;
            }
            set
            {
                base.Queue_Name = value;
            }
        }


        /// <summary>
        /// Print the letter to the printer
        /// </summary>
        protected override void DoPrintLetter(bool ShowPrinterDialog)
        {
            DoQueue();
        }
    }
}
