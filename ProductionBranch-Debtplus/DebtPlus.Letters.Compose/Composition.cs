using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DebtPlus;
using DebtPlus.LINQ;
using System.Linq;

namespace DebtPlus.Letters.Compose
{
    public class Composition : Base
    {
        private bool privateSuppressShowPreview;

        /// <summary>
        /// Create instances of our class
        /// </summary>
        public Composition() : base()
        {
        }

        public Composition(string LetterCode) : base(LetterCode)
        {
        }

        public Composition(Int32 LetterType) : base(LetterType)
        {
        }


        /// <summary>
        /// Extend the class to allow the show function to be disabled
        /// </summary>
        public void PrintLetter(bool ShowPrintDialog, bool SuppressShowPreview)
        {
            privateSuppressShowPreview = SuppressShowPreview;
            base.PrintLetter(ShowPrintDialog);
        }


        /// <summary>
        /// Procedure for handling the letter processing
        /// </summary>
        public override ProcessingMode Display_Mode
        {
            get
            {
                if( base.Display_Mode == ProcessingMode.MODE_DEFAULT )
                {

                    // If the registry says "preview" then preview the letter.
                    if (DebtPlus.Configuration.Config.PreviewLetters)
                    {
                        return ProcessingMode.MODE_VIEW;
                    }

                    // If there is a queue name then queue the letter
                    if( Queue_Name != string.Empty )
                    {
                        return ProcessingMode.MODE_QUEUE;
                    }

                    // Otherwise, print it.
                    return ProcessingMode.MODE_PRINT;
                }

                // If the item says "queue" and there is no name then print it
                if( base.Display_Mode == ProcessingMode.MODE_QUEUE && Queue_Name == string.Empty )
                {
                    return ProcessingMode.MODE_PRINT;
                }

                // return the format as configured for the letter
                return base.Display_Mode;
            }

            set
            {
                base.Display_Mode = value;
            }
        }


        /// <summary>
        /// Print or otherwise generate the letter
        /// </summary>
        private bool privateShowPrinterDialog;
        protected override void DoPrintLetter(bool ShowPrinterDialog)
        {
            privateShowPrinterDialog = ShowPrinterDialog;

            // Process the events for the current mode setting
            switch( Display_Mode )
            {
                case ProcessingMode.MODE_PRINT:
                    DoPrint(privateShowPrinterDialog);
                    break;

                case ProcessingMode.MODE_QUEUE:
                    DoQueue();
                    break;

                case ProcessingMode.MODE_VIEW:
                    DoShow();
                    break;

                case ProcessingMode.MODE_DEFAULT:
                    System.Diagnostics.Debug.Assert(false);
                    break;

                default:
                    System.Diagnostics.Debug.Assert(false);
                    break;
            }
        }


        /// <summary>
        /// Callback from the user clicking on the PRINT button
        /// </summary>
        private void PrintingDesired(object Sender, System.ComponentModel.CancelEventArgs e)
        {

            // Cancel the normal printing operation.
            e.Cancel = true;

            // If there is a queue name then save the letter in the queue
            if( Queue_Name == string.Empty )
            {
                DoPrint(privateShowPrinterDialog);
            }
            else
            { // Do the queue operation in the background
                LetterQueueThread clsx = new LetterQueueThread(this, EditFormInstance.RichEditControl1.RtfText, FindLetterFields(colLetterFields));
                System.Threading.Thread thrd = new System.Threading.Thread(clsx.QueueProcedure);
                thrd.Name = "Queue Letter";
                thrd.SetApartmentState(System.Threading.ApartmentState.STA);
                thrd.Start();
            }

            // Write the system note that we have printed the letter
            WriteSystemNote();
        }
    }
}
