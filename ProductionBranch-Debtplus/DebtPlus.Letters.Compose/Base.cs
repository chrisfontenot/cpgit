using DebtPlus.LINQ;
using DevExpress.XtraRichEdit.API.Native;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;

#pragma warning disable 728

namespace DebtPlus.Letters.Compose
{
    // Implement the list clone operation to copy the letter_fields
    static class Extensions
    {
        public static IList<T> Clone<T>(this IList<T> listToClone) where T : ICloneable
        {
            return listToClone.Select(item => (T)item.Clone()).ToList();
        }
    }

    /// <summary>
    /// Main class to generate letters
    /// </summary>
    public abstract partial class Base : DebtPlus.Interfaces.Letters.ICompose, IDisposable, Forms.ILocalBase
    {
        public enum FieldTypeEnum
        {
            field_undefined = -1,
            field_calculated = 0,
            field_normal = 1,
            field_letter_date = 2,
            field_client = 3,
            field_creditor = 4,
            field_debt = 5,
            field_amount = 6,
            field_followup_date = 7,
            field_name = 8,
            field_address = 9,
            field_callback = 10,
            field_proc_items = 11,
            field_proc_text = 12
        }

        // Form for the text edit control.
        public Forms.Edit_Form EditFormInstance;

        // Localization for converting dates and numbers to strings
        protected System.Globalization.CultureInfo Localization;
        protected IFormatProvider DateTimeFormatProvider;
        protected IFormatProvider NumericFormatProvider;

        // Collection of letter fields and tables. We need to clone the letter_field item since we modify the additional fields.
        internal List<letter_field> colLetterFields;
        internal List<letter_table> colLetterTables;

        /// <summary>
        /// Processing mode for this letter
        /// </summary>
        public enum ProcessingMode
        {
            MODE_DEFAULT = 0,
            MODE_PRINT = 1,
            MODE_VIEW = 2,
            MODE_QUEUE = 3
        }

        public virtual ProcessingMode Display_Mode { get; set; }

        /// <summary>
        /// Create instances of our class
        /// </summary>
        public Base(Int32 LetterType)
            : this(string.Format("={0:f0}", LetterType))
        {
        }

        /// <summary>
        /// Create instances of our class
        /// </summary>
        public Base(string LetterCode)
            : this()
        {
            this.Letter_Code = LetterCode;
        }

        /// <summary>
        /// Create instances of our class
        /// </summary>
        public Base() : base()
        {
            // Include the list of letter tables
            colLetterTables = DebtPlus.LINQ.Cache.letter_table.getList();

            // Generate a !!COPY!! of the letter_fields so that we can manipulate it here and not change the cache.
            colLetterFields = new System.Collections.Generic.List<DebtPlus.LINQ.letter_field>();
            DebtPlus.LINQ.Cache.letter_field.getList().ForEach((item) =>
                {
                    colLetterFields.Add((DebtPlus.LINQ.letter_field)((ICloneable)item).Clone());
                });

            // Set the default values appropriately
            Display_Mode = ProcessingMode.MODE_DEFAULT;

            Letter_Type = -1;
            ClientId = -1;
            Debt = -1;
            Language = 1;
            Right_Margin = 0.25D;
            Left_Margin = 0.25D;
            Top_Margin = 0.25D;
            Bottom_Margin = 0.25D;
            Office = -1;
            FollowupAmount = 0M;
            ClientAppointment = -1;

            LetterDate = DateTime.Now.Date;
            FollowupDate = DateTime.Now.Date;

            Description = string.Empty;
            Filename = string.Empty;
            Queue_Name = string.Empty;
            Letter_Code = string.Empty;
            Creditor = string.Empty;

            // Derive the formatting logic for the default caller
            Localization = System.Globalization.CultureInfo.CurrentCulture;
            DateTimeFormatProvider = (IFormatProvider)Localization.GetFormat(typeof(System.Globalization.DateTimeFormatInfo));
            NumericFormatProvider = (IFormatProvider)Localization.GetFormat(typeof(System.Globalization.NumberFormatInfo));

            // Create the letter form
            EditFormInstance = new Forms.Edit_Form(this);
            EditFormInstance.Load += EditFormInstance_Load;
            EditFormInstance.GenerateSysNote += EditFormInstance_GenerateSysNote;

            // Generate a system note if required
            ShouldWriteNote = true;

            // Ensure that the fields defined in the list are not defined
            foreach (letter_field f in colLetterFields)
            {
                System.Diagnostics.Debug.Assert(! f.HasValue);
                System.Diagnostics.Debug.Assert(! f.referenced);
                // f.referenced = false;
                // f.HasValue = false;
                // f.value = string.Empty;
            }
        }

#region  IDisposable Support

        /// <summary>
        /// Disposal operations
        /// </summary>
        private bool disposedValue;       // To detect redundant calls

        // IDisposable
        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    EditFormInstance.Dispose();
                }
            }
            disposedValue = true;
        }

        // This code added by Visual Basic to correctly implement the disposable pattern.
        public void Dispose() // Implements IDisposable.Dispose, DebtPlus.Letters.ICompose.Dispose
        {
            // Do not change this code.  Put cleanup code in Dispose(bool disposing) above.
            Dispose(true);
            GC.SuppressFinalize(this);
        }
#endregion

#region  PrintLetter

        /// <summary>
        /// Main routine to print the letters
        /// </summary>
        public void PrintLetter() // Implements ICompose.PrintLetter
        {
            PrintLetter(false);
        }

        public void PrintLetter(bool ShowPrinterDialog) // Implements ICompose.PrintLetter
        {

            // Read the letter parameters from the database. We need these before
            // we do the DoPrintLetter since the overridden procedures may need to
            // examine the mode form the database.
            if (ReadLetterInformation())
            {
                DoPrintLetter(ShowPrinterDialog);
            }
        }

        protected abstract void DoPrintLetter(bool ShowPrinterDialog);
#endregion;

        /// <summary>
        /// Callback operation for determining values for specific items
        /// </summary>
        public event DebtPlus.Events.ParameterValueEventHandler GetValue;    // Implements ICompose.GetValue

        protected virtual void OnGetValue(DebtPlus.Events.ParameterValueEventArgs e)
        {
            if (GetValue != null)
            {
                GetValue(this, e);
            }
        }

        protected void RaiseGetValue(DebtPlus.Events.ParameterValueEventArgs e)
        {
            OnGetValue(e);
        }

#region Properties
        /// <summary>
        /// Letter type for the specific letter
        /// </summary>
        public Int32 Letter_Type { get; set; }

        /// <summary>
        /// Letter code for the general letter
        /// </summary>
        public string Letter_Code { get; set; }

        /// <summary>
        /// Client ID for client letters
        /// </summary>
        private Int32 privateClientId = -1;
        public Int32 ClientId // Implements Forms.ILocalBase.ClientId
        {
            get
            {
                if (privateClientId < 0)
                {
                    DebtPlus.Events.ParameterValueEventArgs e = new DebtPlus.Events.ParameterValueEventArgs(DebtPlus.Events.ParameterValueEventArgs.name_client);
                    RaiseGetValue(e);
                    if (e.Value != null)
                    {
                        privateClientId = Convert.ToInt32(e.Value);
                    }
                }
                return privateClientId;
            }
            set
            {
                privateClientId = value;
            }
        }

        /// <summary>
        /// Creditor ID for creditor letters
        /// </summary>
        private string privateCreditor = string.Empty;
        public string Creditor // Implements Forms.ILocalBase.creditor
        {
            get
            {
                if (privateCreditor == string.Empty)
                {
                    DebtPlus.Events.ParameterValueEventArgs e = new DebtPlus.Events.ParameterValueEventArgs(DebtPlus.Events.ParameterValueEventArgs.name_creditor);
                    RaiseGetValue(e);
                    if (e.Value != null)
                    {
                        privateCreditor = Convert.ToString(e.Value);
                    }
                }
                return privateCreditor;
            }
            set
            {
                privateCreditor = value;
            }
        }

        /// <summary>
        /// Pointer to the debt for a debt letter
        /// </summary>
        private Int32 privateDebt = -1;
        public Int32 Debt
        {
            get
            {
                if (privateDebt <= 0)
                {
                    DebtPlus.Events.ParameterValueEventArgs e = new DebtPlus.Events.ParameterValueEventArgs(DebtPlus.Events.ParameterValueEventArgs.name_debt);
                    RaiseGetValue(e);
                    if (e.Value != null)
                    {
                        privateDebt = Convert.ToInt32(e.Value);
                    }
                }

                return privateDebt;
            }
            set
            {
                privateDebt = value;
            }
        }

        /// <summary>
        /// Language ID code for the formatting mode
        /// </summary>
        public Int32 Language { get; set; }

        /// <summary>
        /// General description for the letter
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// File Name for the letter text
        /// </summary>
        private string privateFileName = string.Empty;
        public string Filename // Implements Forms.ILocalBase.filename
        {
            get
            {
                string Answer = privateFileName;

                // If the path is not absolute then prefix it with the registry settings
                if (Answer != string.Empty)
                {
                    if (!System.IO.Path.IsPathRooted(Answer))
                    {
                        string Path = DebtPlus.Configuration.Config.LettersDirectory;
                        if (Path != string.Empty)
                        {
                            Answer = System.IO.Path.Combine(Path, Answer);
                        }
                    }
                }

                return Answer;
            }

            set
            {
                privateFileName = value;
            }
        }


        /// <summary>
        /// Right margin setting
        /// </summary>
        public double Right_Margin { get; set; } // Implements Forms.ILocalBase.Right_Margin


        /// <summary>
        /// Bottom margin setting
        /// </summary>
        public double Bottom_Margin { get; set; } // Implements Forms.ILocalBase.Bottom_Margin


        /// <summary>
        /// Top margin setting
        /// </summary>
        public double Top_Margin { get; set; } // Implements Forms.ILocalBase.Top_Margin


        /// <summary>
        /// Left margin setting
        /// </summary>
        public double Left_Margin { get; set; } // Implements Forms.ILocalBase.Left_Margin


        /// <summary>
        /// Print date for the letter
        /// </summary>
        public DateTime LetterDate { get; set; }


        /// <summary>
        /// Office number for appointments
        /// </summary>
        public Int32 Office { get; set; }


        /// <summary>
        /// General purpose amount for the letter
        /// </summary>
        public decimal FollowupAmount { get; set; }


        /// <summary>
        /// General purpose date for the letter
        /// </summary>
        public DateTime FollowupDate { get; set; }


        /// <summary>
        /// Name of the queue for the letter
        /// </summary>
        private string privateQueueName = string.Empty;
        public virtual string Queue_Name // Implements Forms.ILocalBase.Queue_Name
        {
            get
            {
                return privateQueueName;
            }
            set
            {
                if (value.Length > 50)
                {
                    privateQueueName = value.Substring(0, 50);
                }
                else
                {
                    privateQueueName = value;
                }
            }
        }


        /// <summary>
        /// ClientId appointment ID
        /// </summary>
        private Int32 privateClientAppointment = -1;
        public Int32 ClientAppointment  // Implements Forms.ILocalBase.ClientAppointment
        {
            get
            {
                // If there is no value then ask the user for the value
                if (privateClientAppointment <= 0)
                {
                    DebtPlus.Events.ParameterValueEventArgs e = new DebtPlus.Events.ParameterValueEventArgs(DebtPlus.Events.ParameterValueEventArgs.name_appointment);
                    RaiseGetValue(e);
                    if (e.Value != null)
                    {
                        privateClientAppointment = Convert.ToInt32(e.Value);
                    }
                }

                // Use the client's last appointment from the database for the current appointment number
                if (privateClientAppointment <= 0)
                {
                    privateClientAppointment = DefaultClientAppointment();

                    // Change the "not found" to something that won't ask again but is still "not found"
                    if (privateClientAppointment < 0)
                    {
                        privateClientAppointment = 0;
                    }
                }

                return privateClientAppointment;
            }

            set
            {
                privateClientAppointment = value;
            }
        }
#endregion

#region Default ClientId Appointment

        /// <summary>
        /// Find the default client appointment if one is not available
        /// </summary>
        public virtual Int32 DefaultClientAppointment()
        {
            try
            {
                using (var dc = new BusinessContext())
                {
                    // Take the first pending appointment for the client
                    System.Nullable<int> answer = dc.map_client_to_first_pending_office_appt(ClientId);
                    if (answer.HasValue)
                    {
                        return answer.Value;
                    }

                    // Look at the most recently completed office appointments
                    answer = dc.map_client_to_last_completed_office_appt(ClientId);
                    if (answer.HasValue)
                    {
                        return answer.Value;
                    }

                    // Finally, take the appointment
                    answer = (from apt in dc.client_appointments orderby apt.Id descending where apt.status == 'P' && apt.office != null && apt.client == ClientId select apt.Id).Take(1).FirstOrDefault();
                    if (answer.HasValue)
                    {
                        return answer.Value;
                    }
                }
            }

            catch (System.Data.SqlClient.SqlException ex)
            {
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error finding client appointment");
            }
            return -1;
        }
#endregion

#region ReadLetterInformation

        /// <summary>
        /// Procedure to read the letter information
        /// </summary>
        protected virtual bool ReadLetterInformation()
        {
            using (var dc = new BusinessContext())
            {
                try
                {
                    xpr_letter_fetchResult r = dc.xpr_letter_fetch(Letter_Code, ClientId, -1, Creditor, Debt).FirstOrDefault();
                    if (r == null)
                    {
                        return false;
                    }

                    // Retrieve the field values
                    this.Description = r.description;
                    this.Display_Mode = (ProcessingMode)r.display_mode;
                    this.Queue_Name = r.queue_name;
                    this.Filename = r.filename;
                    this.Language = r.language;
                    this.Left_Margin = r.left_margin;
                    this.Top_Margin = r.top_margin;
                    this.Bottom_Margin = r.bottom_margin;
                    this.Right_Margin = r.right_margin;
                    this.Letter_Code = r.letter_code;
                    this.Letter_Type = r.letter_type;
                }

                catch(System.Data.SqlClient.SqlException ex)
                {
                    using(DebtPlus.Repository.GetDataResult gdr = new DebtPlus.Repository.GetDataResult())
                    {
                        gdr.HandleException(ex);
                        DebtPlus.UI.Common.ErrorHandling.HandleErrors(gdr, "Error reading letter specification");
                        return false;
                    }
                }

                // From the language, identify the language formatting code
                // We are passed our own language ID. We need the name from the languages table for localization.
                if (Language > 0)
                {
                    var q = (from l in dc.AttributeTypes where l.Id == Language select l.LanguageID).FirstOrDefault();
                    if (q != null)
                    {
                        try
                        {
                            Localization = System.Globalization.CultureInfo.GetCultureInfo(q);
                            if (Localization != null)
                            {
                                DateTimeFormatProvider = (IFormatProvider)Localization.GetFormat(typeof(System.Globalization.DateTimeFormatInfo));
                                NumericFormatProvider = (IFormatProvider)Localization.GetFormat(typeof(System.Globalization.NumberFormatInfo));
                            }
                        }
                        catch { }
                    }
                }
            }

            return true;
        }
#endregion

#region  Edit form load processing

        /// <summary>
        /// Load the letter text from the input disk file
        /// </summary>
        private void EditFormInstance_Load(object sender, EventArgs e) // Handles EditFormInstance.Load
        {
            try
            {
                EditFormInstance.RichEditControl1.Text = string.Empty;
                EditFormInstance.RichEditControl1.Margin = new System.Windows.Forms.Padding(Convert.ToInt32(Left_Margin * 100.0D), Convert.ToInt32(Top_Margin * 100.0D), Convert.ToInt32(Right_Margin * 100.0D), Convert.ToInt32(Bottom_Margin * 100.0D));

                // Try to load the file according to the file type
                string Ext = System.IO.Path.GetExtension(Filename).ToLower();
                switch( Ext )
                {
                    case ".rtf":
                        EditFormInstance.RichEditControl1.LoadDocument(Filename, DevExpress.XtraRichEdit.DocumentFormat.Rtf);
                        break;

                    case ".doc":
                        EditFormInstance.RichEditControl1.LoadDocument(Filename, DevExpress.XtraRichEdit.DocumentFormat.Doc);
                        break;

                    case ".docx":
                        EditFormInstance.RichEditControl1.LoadDocument(Filename, DevExpress.XtraRichEdit.DocumentFormat.OpenXml);
                        break;
                    
                    case ".txt":
                        EditFormInstance.RichEditControl1.LoadDocument(Filename, DevExpress.XtraRichEdit.DocumentFormat.PlainText);
                        break;
                    
                    case ".odt":
                        EditFormInstance.RichEditControl1.LoadDocument(Filename, DevExpress.XtraRichEdit.DocumentFormat.OpenDocument);
                        break;
                    
                    case ".html":
                        EditFormInstance.RichEditControl1.LoadDocument(Filename, DevExpress.XtraRichEdit.DocumentFormat.Html);
                        break;
                    
                    case ".htm":
                        EditFormInstance.RichEditControl1.LoadDocument(Filename, DevExpress.XtraRichEdit.DocumentFormat.Html);
                        break;
                    
                    default:
                        EditFormInstance.RichEditControl1.LoadDocument(Filename, DevExpress.XtraRichEdit.DocumentFormat.Undefined);
                        break;
                }
            }
#pragma warning disable 168
            catch (System.IO.FileNotFoundException ex)
            {
                DebtPlus.Data.Forms.MessageBox.Show(string.Format("The letter template \"{0}\" was not found.", Filename), "Unable to load letter template");
                EditFormInstance.RichEditControl1.Text = string.Empty;
                EditFormInstance.RichEditControl1.Margin = new System.Windows.Forms.Padding(Convert.ToInt32(Left_Margin * 100.0), Convert.ToInt32(Top_Margin * 100.0), Convert.ToInt32(Right_Margin * 100.0), Convert.ToInt32(Bottom_Margin * 100.0));
            }

            catch( System.IO.DirectoryNotFoundException ex )
            {
                DebtPlus.Data.Forms.MessageBox.Show(string.Format("The directory name \"{0}\" was not found.", System.IO.Path.GetDirectoryName(Filename), "Unable to load letter template"));
                EditFormInstance.RichEditControl1.Text = string.Empty;
                EditFormInstance.RichEditControl1.Margin = new System.Windows.Forms.Padding(Convert.ToInt32(Left_Margin * 100.0), Convert.ToInt32(Top_Margin * 100.0), Convert.ToInt32(Right_Margin * 100.0), Convert.ToInt32(Bottom_Margin * 100.0));
            }

            catch( Exception ex )
            {
                DebtPlus.Data.Forms.MessageBox.Show(string.Format("Attempting to load the letter \"{0}\" resulted in the following error:" + Environment.NewLine + Environment.NewLine + "{1}", Filename, ex.Message, "Unable to load letter template"));
                EditFormInstance.RichEditControl1.Text = string.Empty;
                EditFormInstance.RichEditControl1.Margin = new System.Windows.Forms.Padding(Convert.ToInt32(Left_Margin * 100.0), Convert.ToInt32(Top_Margin * 100.0), Convert.ToInt32(Right_Margin * 100.0), Convert.ToInt32(Bottom_Margin * 100.0));
            }
#pragma warning restore 168

            // Search and update the referenced flags for the fields.
            SearchDocumentForFields();

            // Define the values for the various fields
            if (colLetterFields.Count(fld => fld.referenced) > 0)
            {
                read_callback_values();          // any items that are a callback to the procedure creating the letter
                read_known_values();             // specific known values from the parameters list
                read_proc_values();              // read any stored procedure values
                read_normal_values();            // all other values from the system tables

                // Update the fields with the corresponding items
                Perform_Merge_Operation();
            }
        }


        /// <summary>
        /// Define the letter fields where we know the value
        /// </summary>
        protected void read_known_values()
        {
            foreach (letter_field f in colLetterFields.Where(f => f.referenced))
            {
                switch (f.type)
                {
                    case 2:
                        f.value = FormatItem(LetterDate, f.formatting);
                        f.HasValue = true;
                        break;

                    case 3:
                        f.value = DebtPlus.Utils.Format.Client.FormatClientID(ClientId);
                        f.HasValue = true;
                        break;

                    case 4:
                        f.value = FormatItem(Creditor, f.formatting);
                        f.HasValue = true;
                        break;

                    case 5:
                        f.value = FormatItem(Debt, f.formatting);
                        f.HasValue = true;
                        break;

                    case 6:
                        f.value = FormatItem(FollowupAmount, f.formatting);
                        f.HasValue = true;
                        break;

                    case 7:
                        f.value = FormatItem(FollowupDate, f.formatting);
                        f.HasValue = true;
                        break;

                    default:
                        break;
                }
            }
        }

        /// <summary>
        /// Define the letter fields where we know the value
        /// </summary>
        protected void read_callback_values()
        {
            // Ask the caller for any field marked "referenced"
            foreach (letter_field f in colLetterFields.Where(f => f.referenced && !f.HasValue))
            {
                DebtPlus.Events.ParameterValueEventArgs e = new DebtPlus.Events.ParameterValueEventArgs(f.Id);
                OnGetValue(e);
                if (e.Value != null && e.Value != DBNull.Value)
                {
                    f.value = FormatItem(e.Value, f.formatting);
                    f.HasValue = true;
                }
            }
        }

        /// <summary>
        /// Read the standard database field values
        /// </summary>
        protected void read_function_values()
        {
            foreach(letter_table t in colLetterTables.Where( tbl => tbl.query_type == 2 ))
            {
                string query = t.query;
                if (!string.IsNullOrEmpty(query))
                {
                    if ((from f in t.letter_fields where f.referenced && !f.HasValue && f.letter_table == t.Id select f).Count() > 0)
                    {
                        ReadFunctionValues(query, t);
                    }
                }
            }
        }


        /// <summary>
        /// Process the specific letter function request
        /// </summary>
        private void ReadFunctionValues(string QueryString, letter_table tbl)
        {

            // When reading the data, do not change this to a datareader since that will cause
            // serious problems with text fields like the office directions. (You would need to use
            // sequential access to obtain the office directions and we can't guarantee that they are
            // the ending fields.) So, leave it as a dataadapter and you won't have this problem.

            try
            {
                string ConnectionString = BusinessContext.ConnectionString();
                using (var cn = new System.Data.SqlClient.SqlConnection(ConnectionString))
                {
                    cn.Open();

                    System.Data.SqlClient.SqlCommand cmd = new System.Data.SqlClient.SqlCommand(tbl.query, cn);
                    cmd.CommandType = CommandType.Text;
                    AddParameterValues(ref cmd, tbl.query);

                    using (cmd)
                    {
                        System.Data.SqlClient.SqlDataReader rdr = cmd.ExecuteReader(CommandBehavior.SingleRow | CommandBehavior.CloseConnection);
                        using (rdr)
                        {
                            if (rdr != null && rdr.Read())
                            {
                                StoreFieldValues(ref rdr);
                            }
                        }
                    }
                }
            }

            catch (SqlException ex)
            {
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error reading letter parameter values");
            }
        }

        /// <summary>
        /// Read the standard database field values
        /// </summary>
        protected void read_normal_values()
        {
            foreach (letter_table tbl in colLetterTables.Where(t => t.query_type == 1))
            {
                if (!string.IsNullOrEmpty(tbl.query))
                {
                    try
                    {
                        System.Text.StringBuilder sb = new System.Text.StringBuilder();
                        foreach (letter_field f in (from fld in colLetterFields where fld.letter_table == tbl.Id && fld.referenced && !fld.HasValue select fld))
                        {
                            sb.AppendFormat(",{0} AS '{1}'", f.field_name, f.Id);
                        }

                        if (sb.Length > 0)
                        {
                            sb.Remove(0, 1);

                            string ConnectionString = BusinessContext.ConnectionString();
                            using (var cn = new System.Data.SqlClient.SqlConnection(ConnectionString))
                            {
                                cn.Open();

                                System.Data.SqlClient.SqlCommand cmd = new System.Data.SqlClient.SqlCommand(string.Format(tbl.query, sb.ToString()), cn);
                                cmd.CommandType = CommandType.Text;
                                AddParameterValues(ref cmd, tbl.query);

                                using (cmd)
                                {
                                    System.Data.SqlClient.SqlDataReader rdr = cmd.ExecuteReader(CommandBehavior.SingleRow | CommandBehavior.CloseConnection);
                                    using (rdr)
                                    {
                                        if (rdr != null && rdr.Read())
                                        {
                                            StoreFieldValues(ref rdr);
                                        }
                                    }
                                }
                            }
                        }
                    }

                    catch (System.Data.SqlClient.SqlException ex)
                    {
                        DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error reading letter parameter values");
                    }
                }
            }
        }


        /// <summary>
        /// Store the values from the query into the results
        /// </summary>
        private void StoreFieldValues(ref System.Data.SqlClient.SqlDataReader rdr)
        {
            for(Int32 colNumber = 0; colNumber < rdr.FieldCount; ++colNumber)
            {
                string colName = rdr.GetName(colNumber);
                letter_field fld = colLetterFields.Where(f => string.Compare(f.Id, colName, true) == 0).FirstOrDefault();
                if (fld != null)
                {
                    fld.value = FormatItem(rdr.GetValue(colNumber), fld.formatting);
                    fld.HasValue = true;
                }
            }
        }


        /// <summary>
        /// Read stored procedure field values
        /// </summary>
        protected void read_proc_values()
        {
            try
            {
                foreach (letter_table t in colLetterTables.Where(tbl => tbl.query_type == 4))
                {
                    if ((from f in colLetterFields where f.letter_table == t.Id && f.referenced && !f.HasValue select f).Count() > 0 && (!string.IsNullOrEmpty(t.query)))
                    {
                        string ConnectionString = BusinessContext.ConnectionString();
                        using (var cn = new System.Data.SqlClient.SqlConnection(ConnectionString))
                        {
                            cn.Open();

                            System.Data.SqlClient.SqlCommand cmd = new System.Data.SqlClient.SqlCommand() { Connection = cn };
                            string CommandText = t.query.Trim();

                            // Add any missing parameters to the list
                            AddParameterValues(ref cmd, t.query);

                            // Set the parameter values that are referenced
                            SetParameterValues(ref cmd);

                            cmd.CommandText = CommandText;
                            cmd.CommandType = CommandType.Text;

                            using (cmd)
                            {
                                System.Data.SqlClient.SqlDataReader rdr = cmd.ExecuteReader(CommandBehavior.SingleRow | CommandBehavior.CloseConnection);
                                using (rdr)
                                {
                                    if (rdr != null && rdr.Read())
                                    {
                                        StoreFieldValues(ref rdr);
                                    }
                                }
                            }
                        }
                    }
                }
            }

            catch (SqlException ex)
            {
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error reading letter parameter values");
            }
        }

        /// <summary>
        /// Retrieve the database information from the fields used in this letter
        /// This is an public procedure once the letter fields have been built.
        /// It is the interface to the database for the items.
        /// </summary>
        protected void AddParameterValues(ref SqlCommand cmd, string Query)
        {
            Query = Query.ToLower();

            // Add the client
            if (Query.Contains("@client"))
            {
                cmd.Parameters.Add("@client", SqlDbType.Int).Value = ClientId;
            }

            // Add the creditor
            if (Query.Contains("@creditor"))
            {
                cmd.Parameters.Add("@creditor", SqlDbType.VarChar, 10).Value = Creditor;
            }

            // Add the debt
            if (Query.Contains("@debt"))
            {
                cmd.Parameters.Add("@debt", SqlDbType.Int).Value = Debt;
            }

            // Add the office
            if (Query.Contains("@office"))
            {
                cmd.Parameters.Add("@office", SqlDbType.Int).Value = Office;
            }

            // Add the letter_type
            if (Query.Contains("@letter_type"))
            {
                cmd.Parameters.Add("@letter_type", SqlDbType.VarChar, 80).Value = Letter_Type;
            }

            // The client appointment requires a bit more logic.
            if (Query.Contains("@client_appointment"))
            {
                cmd.Parameters.Add("@client_appointment", SqlDbType.Int).Value = ClientAppointment;
            }
        }

        /// <summary>
        /// Retrieve the database information from the fields used in this letter
        /// This is an public procedure once the letter fields have been built.
        /// It is the interface to the database for the items.
        /// </summary>
        protected void SetParameterValues(ref SqlCommand cmd)
        {
            for (Int32 ParmIndex = 0; ParmIndex < cmd.Parameters.Count; ++ParmIndex)
            {
                SqlParameter parm = cmd.Parameters[ParmIndex];
                if (parm.IsNullable)
                {
                    parm.Value = DBNull.Value;
                }

                switch (parm.Direction)
                {
                    case ParameterDirection.Input:
                    case ParameterDirection.InputOutput:
                        switch (parm.ParameterName.ToLower())
                        {
                            case "@client":
                                parm.Value = ClientId;
                                break;

                            case "@creditor":
                            case "@creditor_label":
                            case "@creditor_id":
                            case "@creditorid":
                                if (parm.DbType == System.Data.DbType.String || parm.DbType == System.Data.DbType.AnsiString)
                                {
                                    parm.Value = Creditor;
                                }
                                break;

                            case "@debt":
                            case "@client_creditor":
                                parm.Value = Debt;
                                break;

                            case "@office":
                                parm.Value = Office;
                                break;

                            case "@letter_type":
                                parm.Value = Letter_Type;
                                break;

                            case "@client_appointment":
                            case "@appointment":
                                parm.Value = ClientAppointment;
                                break;

                            default:
                                // Ignore other parameter names
                                break;
                        }
                        break;

                    default:
                        // Ignore other direction codes
                        break;
                }
            }
        }

        /// <summary>
        /// Define the letter fields where we know the value
        /// </summary>
        protected void SearchDocumentForFields()
        {
            // Find the document that was just added
            DevExpress.XtraRichEdit.API.Native.Document doc = EditFormInstance.RichEditControl1.Document;

            // Generate a search request for the document
            char Leadin = Convert.ToChar(0xAB);
            char Trailing = Convert.ToChar(0xBB);
            System.Text.RegularExpressions.Regex rx = new System.Text.RegularExpressions.Regex(Leadin + @"[^" + Leadin + @"]+" + Trailing, System.Text.RegularExpressions.RegexOptions.CultureInvariant);
            DocumentRange[] Ranges = doc.FindAll(rx, doc.Range);

            foreach (DocumentRange Range in Ranges)
            {
                try
                {
                    // Find the key field
                    string strKey = doc.GetText(Range);
                    if (strKey.Length > 0)
                    {
                        strKey = strKey.Substring(1);
                        if (strKey.Length > 1)
                        {
                            strKey = strKey.Substring(0, strKey.Length - 1);
                        }
                    }

                    // Mark the row as being used
                    if (strKey.Length > 0)
                    {
                        var fld = (from f in colLetterFields where string.Compare(f.Id, strKey, true) == 0 select f).FirstOrDefault();
                        if (fld != null)
                        {
                            fld.referenced = true;
                        }
                    }
                }

                catch (Exception ex)
                {
                    DebtPlus.Svc.Common.ErrorHandling.HandleErrors(ex);
                }
            }
        }

        /// <summary>
        /// Merge the values into the letter template
        /// </summary>
        protected void Perform_Merge_Operation()
        {
            // Find the document that was just added
            EditFormInstance.RichEditControl1.BeginUpdate();
            try
            {
                DevExpress.XtraRichEdit.API.Native.Document doc = EditFormInstance.RichEditControl1.Document;

                // Find the fields in the letter text
                char Leadin = Convert.ToChar(0xAB);
                char Trailing = Convert.ToChar(0xBB);
                System.Text.RegularExpressions.Regex rx = new System.Text.RegularExpressions.Regex(Leadin + @"[^" + Leadin + @"]+" + Trailing, System.Text.RegularExpressions.RegexOptions.CultureInvariant);
                DocumentRange[] Ranges = doc.FindAll(rx, doc.Range);

                foreach (DocumentRange Range in Ranges)
                {
                    try
                    {
                        // Identify the key field in the search operation.
                        string strKey = doc.GetText(Range);
                        if (strKey.Length > 0)
                        {
                            strKey = strKey.Substring(1);
                            if (strKey.Length > 1)
                            {
                                strKey = strKey.Substring(0, strKey.Length - 1);
                            }
                        }

                        // Find the field for the replacement text
                        if (strKey.Length > 0)
                        {
                            string StrValue = KeyValue(strKey).Replace(@"\r", "\r").Replace(@"\n", "\n");
                            StrValue = StrValue.Replace("\r\n", "\n");

                            // Insert the text into the field
                            doc.InsertText(doc.CreatePosition(Range.Start.ToInt() + 1), StrValue);

                            // Remove the trailing characters from the field
                            doc.Delete(doc.CreateRange(doc.CreatePosition(Range.Start.ToInt() + StrValue.Length + 1), strKey.Length + 1));

                            // Remove the leading marker character from the field
                            doc.Delete(doc.CreateRange(Range.Start, 1));
                        }
                    }
                    catch (Exception ex)
                    {
                        DebtPlus.Svc.Common.ErrorHandling.HandleErrors(ex);
                    }
                }
            }
            finally
            {
                EditFormInstance.RichEditControl1.EndUpdate();
            }
        }

        /// <summary>
        /// Retrieve the value for the key item
        /// </summary>
        protected string KeyValue(string strKey)
        {
            var fld = (from f in colLetterFields where f.HasValue && string.Compare(f.Id, strKey, true) == 0 select f).FirstOrDefault();
            if (fld != null)
            {
                return fld.value;
            }
            return string.Empty;
        }


        /// <summary>
        /// Format the current item into a string
        /// </summary>
        protected string FormatItem(object varItem, object varFormat)
        {
            string Result = string.Empty;

            // If the format is "salutation" then this is special
            if (varFormat == null || varFormat == DBNull.Value)
            {
                varFormat = string.Empty;
            }
            string StringFormat = Convert.ToString(varFormat).Trim();

            // If the item is missing then there is no resulting string
            if (varItem == null || varItem == DBNull.Value)
            {
                return string.Empty;
            }

            // If there is no format then the result is the input without conversion
            if (StringFormat == string.Empty)
            {
                try
                {
                    if (varItem is DateTime)
                    {
                        DateTime dat = Convert.ToDateTime(varItem);
                        Result = string.Format(DateTimeFormatProvider, "{0:d}", dat);
                    }
                    else if (varItem is string)
                    {
                        Result = Convert.ToString(varItem);
                    }
                    else if (varItem is bool)
                    {
                        Result = Convert.ToBoolean(varItem).ToString();
                    }
                    else if (varItem is decimal)
                    {
                        Result = Convert.ToDecimal(varItem).ToString();
                    }
                    else if (varItem is double)
                    {
                        double dbl = Convert.ToDouble(varItem);
                        Result = string.Format(NumericFormatProvider, "{0:d}", dbl);
                    }
                    else if (varItem is Int64 || varItem is byte || varItem is Int32)
                    {
                        Int64 lng = Convert.ToInt64(varItem);
                        Result = string.Format(NumericFormatProvider, "{0:n0}", lng);
                    }
                    else
                    {
                        Result = Convert.ToInt32(varItem).ToString();
                    }
                }

#pragma warning disable 168
                catch (FormatException ex)
                {
                    Result = Convert.ToString(varItem);
                }
#pragma warning restore 168

                return Result;
            }

            // Look for special formatting strings
            double dblTemp;
            switch (StringFormat)
            {
                case "rtf":
                    Result = Convert.ToString(varItem);
                    break;

                case "telephone":
                    Result = DebtPlus.Utils.Format.PhoneNumber.FormatPhoneNumber(Convert.ToString(varItem));
                    break;

                case "zipcode":
                    Result = Convert.ToString(varItem);
                    break;

                case "percent":
                    dblTemp = Convert.ToDouble(varItem);
                    while (dblTemp >= 1.0D)
                    {
                        dblTemp /= 100.0D;
                    }

                    // Format the value as a percentage
                    Result = string.Format(NumericFormatProvider, "{0:p}", dblTemp);

                    // Look at the fractional part. If we can use a "better" symbol then do so
                    string[] Parts = Result.Split(new Char[] { '.', ',' });
                    if (Parts.GetUpperBound(0) > 0)
                    {
                        if (Parts[1] == "50" || Parts[1] == "5")
                        {
                            Result = Parts[0] + '\xBC' + System.Globalization.CultureInfo.CurrentCulture.NumberFormat.PercentSymbol; // + "??%"
                        }
                        else if (Parts[1] == "25")
                        {
                            Result = Parts[0] + '\xBD' + System.Globalization.CultureInfo.CurrentCulture.NumberFormat.PercentSymbol; // + "??%"
                        }
                        else if (Parts[1] == "75")
                        {
                            Result = Parts[0] + '\xBE' + System.Globalization.CultureInfo.CurrentCulture.NumberFormat.PercentSymbol; // + "??%"
                        }
                    }
                    break;

                case "pct":
                    dblTemp = Convert.ToDouble(varItem);
                    while (dblTemp >= 1.0D)
                    {
                        dblTemp /= 100.0;
                    }
                    Result = string.Format(NumericFormatProvider, "{0:p}", dblTemp);
                    break;

                case "currency":
                    Result = string.Format(NumericFormatProvider, "{0:c}", Convert.ToDecimal(varItem));
                    break;

                case "day":
                    if (varItem is DateTime)
                    {
                        Result = Format_DayOfMonth(((DateTime)varItem).Day);
                    }
                    else
                    {
                        Result = Format_DayOfMonth(Convert.ToInt32(varItem));
                    }
                    break;

                case "ssn":
                    Result = DebtPlus.Utils.Format.SSN.FormatSSN(Convert.ToString(varItem));
                    break;

                case "long date":
                    if (Convert.ToString(varItem) == string.Empty)
                    {
                        Result = string.Empty;
                    }
                    else
                    {
                        Result = string.Format(DateTimeFormatProvider, "{0:D}", Convert.ToDateTime(varItem));
                    }
                    break;

                case "medium date":
                    Result = string.Format(DateTimeFormatProvider, "{0:d}", Convert.ToDateTime(varItem));
                    break;

                case "short date":
                case "MM/dd/yyyy":
                    Result = string.Format(DateTimeFormatProvider, "{0:d}", Convert.ToDateTime(varItem));
                    break;

                case "short time":
                    Result = string.Format(DateTimeFormatProvider, "{0:t}", Convert.ToDateTime(varItem));
                    break;

                case "medium time":
                    if (Convert.ToString(varItem) == string.Empty)
                    {
                        Result = string.Empty;
                    }
                    else
                    {
                        Result = string.Format(DateTimeFormatProvider, "{0:t}", Convert.ToDateTime(varItem));
                    }
                    break;

                case "long time":
                    Result = string.Format(DateTimeFormatProvider, "{0:T}", Convert.ToDateTime(varItem));
                    break;

                default:
                    try
                    {
                        if (varItem is DateTime)
                        {
                            Result = string.Format(DateTimeFormatProvider, "{0:" + StringFormat + "}", Convert.ToDateTime(varItem));
                        }
                        else if (varItem is string)
                        {
                            Result = Convert.ToString(varItem);
                        }
                        else if (varItem is bool)
                        {
                            Result = Convert.ToBoolean(varItem).ToString();
                        }
                        else if (varItem is decimal)
                        {
                            Result = string.Format(NumericFormatProvider, "{0:" + StringFormat + "}", Convert.ToDecimal(varItem));
                        }
                        else if (varItem is double)
                        {
                            Result = string.Format(NumericFormatProvider, "{0:" + StringFormat + "}", Convert.ToDouble(varItem));
                        }
                        else
                        {
                            Result = string.Format(NumericFormatProvider, "{0:" + StringFormat + "}", Convert.ToInt64(varItem));
                        }
                    }

#pragma warning disable 168
                    catch (FormatException ex)
                    {
                        Result = Convert.ToString(varItem);
                    }
#pragma warning restore 168
                    break;
            }

            return Result.Trim();
        }


        /// <summary>
        /// Format the relative day of the month
        /// </summary>
        protected static string Format_DayOfMonth(Int32 nItem)
        {
            string Result = string.Format("{0:f0}", nItem);

            // Reduce the values from 21 on up to values less than 10
            if (nItem > 20)
            {
                nItem = nItem % 10;
            }

            // Add the appropriate suffix to the value so that it "sounds" good.
            switch (nItem)
            {
                case 1:
                    Result += "st";
                    break;
                case 2:
                    Result += "nd";
                    break;
                case 3:
                    Result += "rd";
                    break;
                default:
                    Result += "th";
                    break;
            }

            return Result;
        }
        #endregion

#region  DoShow

        /// <summary>
        /// Do the SHOW PREVIEW processing
        /// </summary>
        protected virtual void DoShow()
        {
            EditFormInstance.Opacity = 1.0;
            EditFormInstance.ShowInTaskbar = true;
            EditFormInstance.ShowDialog();
        }
#endregion

#region  DoPrint

        /// <summary>
        /// Print the formatted letter on the printer
        /// </summary>
        protected virtual void DoPrint(bool ShowPrintDialog)
        {

            // Load the letter text for the letter
            EditFormInstance.Opacity = 0.0;
            EditFormInstance.ShowInTaskbar = false;
            EditFormInstance.Show(); // This trips Load() and calls us back to load the letterDate

            DoPrintRoutine(ShowPrintDialog);

            EditFormInstance.Close();

            // Write the system note that we have printed the letter
            // This procedure will handle multiple calls and only write the note once.
            WriteSystemNote();
        }


        /// <summary>
        /// Print routine called either from "Print" or "Show" Print button
        /// </summary>
        protected virtual void DoPrintRoutine(bool ShowPrintDialog)
        {
            using (var ps = new DevExpress.XtraPrinting.PrintingSystem())
            {
                using (var pcl = new DevExpress.XtraPrinting.PrintableComponentLink(ps))
                {

                    // Create the document for the printer format.
                    pcl.Component = EditFormInstance.RichEditControl1;
                    pcl.CreateDocument();
                    ps.Document.Name = Description;

                    // Show the dialog if desired. Otherwise, just print the document on the default printer.
                    if (ShowPrintDialog)
                    {
                        ps.PrintDlg();
                    }
                    else
                    {
                        ps.Print();
                    }
                }
            }
        }
#endregion

#region  DoQueue

        /// <summary>
        /// Save the letter to the database (or print if no queue name)
        /// </summary>
        protected virtual void DoQueue()
        {

            // There must be a queue name or we are in trouble.
            if (Queue_Name == string.Empty)
            {
                throw new ArgumentNullException("Queue_Name");
            }

            // Load the letter text for the letter
            EditFormInstance.Opacity = 0.0;
            EditFormInstance.ShowInTaskbar = false;
            EditFormInstance.Show();

            DoQueueRoutine();

            EditFormInstance.Close();

            // Write the system note that we have printed the letter
            WriteSystemNote();
        }


        /// <summary>
        /// Queue the letter for later printing if desired
        /// </summary>
        protected virtual void DoQueueRoutine()
        {
            LetterQueueThread clsx = new LetterQueueThread(this, EditFormInstance.RichEditControl1.RtfText, FindLetterFields(colLetterFields));
            System.Threading.Thread thrd = new System.Threading.Thread(new System.Threading.ParameterizedThreadStart(clsx.QueueProcedure));
            thrd.Name = "Queue Letter";
            thrd.SetApartmentState(System.Threading.ApartmentState.STA);
            thrd.Start();
        }

        protected string FindLetterFields(System.Collections.Generic.List<letter_field> colFields)
        {
            // Serialize the "letter_fields" table so that we save the values used in generating the letter image
            // The serialization is just the id and the value so don't use the full xml serializer as that causes
            // problems with the letter_table references.
            System.Text.StringBuilder sb = new System.Text.StringBuilder();
            using (System.IO.StringWriter XMLStringWriter = new System.IO.StringWriter(sb))
            {
                using (System.Xml.XmlTextWriter tw = new System.Xml.XmlTextWriter(XMLStringWriter))
                {
                    tw.WriteStartDocument();
                    tw.WriteStartElement("letter_fields");
                    foreach (DebtPlus.LINQ.letter_field fld in colFields)
                    {
                        if (fld.referenced)
                        {
                            tw.WriteStartElement("letter_field");
                            tw.WriteAttributeString("Id", fld.Id);
                            tw.WriteString(fld.value);
                            tw.WriteEndElement();
                        }
                    }
                    tw.WriteEndElement();
                    tw.WriteEndDocument();

                    tw.Flush();
                    tw.Close();
                }
            }
            return sb.ToString();
        }
#endregion

        /// <summary>
        /// Write the system note that the letter was printed
        /// </summary>
        private bool ShouldWriteNote = true;
        protected virtual void WriteSystemNote()
        {
            // if we should write the note then do it only once per letter.
            if (ShouldWriteNote)
            {
                ShouldWriteNote = false;        // Only one note per letter, even if it is a re-print.

                try
                {
                    // Write the system note
                    using (var cm = new DebtPlus.UI.Common.CursorManager())
                    {
                        using (var dc = new BusinessContext())
                        {
                            // If there is a client then either write a client or debt note to the system
                            if (ClientId >= 0)
                            {
                                client_note cn = new client_note()
                                {
                                    client = ClientId,
                                    dont_delete = true,
                                    dont_edit = true,
                                    dont_print = true,
                                    type = 3,
                                    subject = string.Format("Printed letter '{0}'", Description),
                                    note = string.Format("Printed letter '{0}' for the client", Description)
                                };

                                if (Debt >= 0)
                                {
                                    cn.client_creditor = Debt;
                                }

                                dc.client_notes.InsertOnSubmit(cn);
                                dc.SubmitChanges();
                            }

                                // Write the creditor note to the system.
                            else if (Creditor != string.Empty)
                            {
                                creditor_note cn = new creditor_note()
                                {
                                    creditor = Creditor,
                                    dont_delete = true,
                                    dont_edit = true,
                                    dont_print = true,
                                    type = 3,
                                    subject = string.Format("Printed letter '{0}'", Description),
                                    note = string.Format("Printed letter '{0}' for the creditor", Description)
                                };

                                dc.creditor_notes.InsertOnSubmit(cn);
                                dc.SubmitChanges();
                            }
                        }
                    }
                }

                catch (SqlException ex)
                {
                    DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error Creating System Note");
                }
            }
        }

        /// <summary>
        /// Write the system note when the letter is printed
        /// </summary>
        private void EditFormInstance_GenerateSysNote(object Sender, EventArgs e) // Handles EditFormInstance.GenerateSysNote
        {
            WriteSystemNote();
        }

        /// <summary>
        /// Handle the condition where we want to print the letter
        /// </summary>
        private void EditFormInstance_PrintLetter(object Sender, Forms.PrintEventArgs e) // Handles EditFormInstance.PrintLetter
        {
            // Cancel the normal printing routine and write the system note before printing
            e.Cancel = true;
            WriteSystemNote();

            // If there is a queue name then save the letter in the queue
            if (Queue_Name == string.Empty)
            {
                DoPrintRoutine(e.UsePrintDialog);
            }
            else
            {
                DoQueueRoutine();
            }
        }
    }
}
