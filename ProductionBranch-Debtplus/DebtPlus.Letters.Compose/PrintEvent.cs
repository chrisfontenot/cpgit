﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DebtPlus.LINQ;

namespace DebtPlus.Letters.Compose.Forms
{
    public class PrintEventArgs : System.ComponentModel.CancelEventArgs
    {
        public virtual bool UsePrintDialog
        {
            get
            {
                return false;
            }
        }

        public PrintEventArgs() : base()
        {
        }

        public PrintEventArgs(bool Cancel) : base(Cancel)
        {
        }
    }
    public delegate void PrintEventHandler(object sender, PrintEventArgs e);
}
