using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DebtPlus;
using DebtPlus.LINQ;
using System.Linq;
using DevExpress.XtraRichEdit;

namespace DebtPlus.Letters.Compose
{

    /// <summary>
    /// private class to process the thread which saves the letter text
    /// </summary>
    public partial class LetterQueueThread : IDisposable
    {
        private DebtPlus.Letters.Compose.Base ltr;
        private string LetterText;
        private string LetterFields;

        internal LetterQueueThread(DebtPlus.Letters.Compose.Base ltr, string LetterText, string LetterFields)
        {
            this.ltr = ltr;
            this.LetterText = LetterText;
            this.LetterFields = LetterFields;
        }

        /// <summary>
        /// This is the thread to save the letter to the database
        /// The actual save operation is performed in the background here.
        /// </summary>
        public void QueueProcedure(object Parameters)
        {
            using (var bc = new BusinessContext())
            {
                // Translate the creditor string to the ID of the creditor in the tables.
                // WE REALLY SHOULD USE THE ID FOR ALL CREDITOR REFERENCES!! THIS IS STUPID TO NOT DO THAT.
                System.Nullable<int> CreditorID = new System.Nullable<int>();
                if (!string.IsNullOrEmpty(ltr.Creditor))
                {
                    CreditorID = (from c in bc.creditors where c.Id == ltr.Creditor select c.creditor_id).FirstOrDefault();
                }

                var que = DebtPlus.LINQ.Factory.Manufacture_letter_queue();
                que.client = ltr.ClientId;
                que.creditor = CreditorID;
                que.letter_type = ltr.Letter_Type;
                que.queue_name = ltr.Queue_Name;

                que.formatted_letter = LetterText;
                que.letter_fields = LetterFields;

                que.bottom_margin = ltr.Bottom_Margin;
                que.left_margin = ltr.Left_Margin;
                que.right_margin = ltr.Right_Margin;
                que.top_margin = ltr.Top_Margin;
                que.sort_order = string.Empty;

                bc.letter_queues.InsertOnSubmit(que);
                bc.SubmitChanges();
            }
        }

        private bool disposedValue = false;        // To detect redundant calls

        // IDisposable
        protected virtual void Dispose(bool disposing)
        {
            if( ! this.disposedValue )
            {
                this.disposedValue = true;
                this.ltr = null;
                this.LetterText = null;
            }
        }

        // This code added by Visual Basic to correctly implement the disposable pattern.
        public void Dispose() // Implements IDisposable.Dispose
        {
            // Do not change this code.  Put cleanup code in Dispose(bool disposing) above.
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}
