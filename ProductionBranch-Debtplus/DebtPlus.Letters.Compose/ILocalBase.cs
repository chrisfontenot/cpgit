using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DebtPlus;
using DebtPlus.LINQ;
using System.Linq;

namespace DebtPlus.Letters.Compose.Forms
{
    public interface ILocalBase : DebtPlus.Interfaces.Client.IClient, DebtPlus.Interfaces.Creditor.ICreditor
    {
        double  Left_Margin         { get; set; }
        double  Right_Margin        { get; set; }
        double  Top_Margin          { get; set; }
        double  Bottom_Margin       { get; set; }
        string  Filename            { get; set; }
        Int32   ClientAppointment   { get; set; }
        Int32   Letter_Type         { get; set; }
        string  Queue_Name          { get; set; }
    }
}