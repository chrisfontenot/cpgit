#region "Copyright 2000-2012 DebtPlus, L.L.C."

//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}

#endregion "Copyright 2000-2012 DebtPlus, L.L.C."

using System;
using System.Data;
using DebtPlus.UI.Debt.Update.pages;
using DevExpress.XtraEditors;
using DebtPlus.LINQ;
using System.Linq;

namespace DebtPlus.UI.Debt.Update.controls
{
    internal partial class FormHeader : ControlBaseDebt
    {
        public FormHeader()
            : base()
        {
            InitializeComponent();
        }

        #region " Windows Form Designer generated code "

        internal DevExpress.XtraEditors.LabelControl creditor_label;

        internal DevExpress.XtraEditors.LabelControl creditor_name;

        //NOTE: The following procedure is required by the Windows Form Designer
        //It can be modified using the Windows Form Designer.
        //Do not modify it using the code editor.
        internal DevExpress.XtraEditors.LabelControl LabelControl1;

        internal DevExpress.XtraEditors.LabelControl LabelControl5;

        internal DevExpress.XtraEditors.SpinEdit priority;

        private System.ComponentModel.IContainer components = null;

        //UserControl overrides dispose to clean up the component list.
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (components != null)
                {
                    components.Dispose();
                }
            }
            base.Dispose(disposing);
        }

        //Required by the Windows Form Designer
        [System.Diagnostics.DebuggerStepThrough()]
        private void InitializeComponent()
        {
            this.LabelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.creditor_label = new DevExpress.XtraEditors.LabelControl();
            this.LabelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.priority = new DevExpress.XtraEditors.SpinEdit();
            this.creditor_name = new DevExpress.XtraEditors.LabelControl();
            ((System.ComponentModel.ISupportInitialize)this.priority.Properties).BeginInit();
            this.SuspendLayout();
            //
            //LabelControl1
            //
            this.LabelControl1.Appearance.Options.UseTextOptions = true;
            this.LabelControl1.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.LabelControl1.Location = new System.Drawing.Point(32, 13);
            this.LabelControl1.Name = "LabelControl1";
            this.LabelControl1.Size = new System.Drawing.Size(43, 13);
            this.LabelControl1.TabIndex = 0;
            this.LabelControl1.Text = "Creditor:";
            //
            //creditor_label
            //
            this.creditor_label.Appearance.Font = new System.Drawing.Font("Arial", 14.25f, (System.Drawing.FontStyle)(System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline));
            this.creditor_label.Appearance.ForeColor = System.Drawing.SystemColors.ActiveCaption;
            this.creditor_label.Appearance.Options.UseFont = true;
            this.creditor_label.Appearance.Options.UseForeColor = true;
            this.creditor_label.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.creditor_label.Location = new System.Drawing.Point(88, 8);
            this.creditor_label.Name = "creditor_label";
            this.creditor_label.Size = new System.Drawing.Size(106, 22);
            this.creditor_label.TabIndex = 1;
            this.creditor_label.UseMnemonic = false;
            //
            //LabelControl5
            //
            this.LabelControl5.Appearance.Options.UseTextOptions = true;
            this.LabelControl5.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.LabelControl5.Location = new System.Drawing.Point(37, 40);
            this.LabelControl5.Name = "LabelControl5";
            this.LabelControl5.Size = new System.Drawing.Size(38, 13);
            this.LabelControl5.TabIndex = 4;
            this.LabelControl5.Text = "Priority:";
            //
            //priority
            //
            this.priority.EditValue = new decimal(new Int32[] {
				9,
				0,
				0,
				0
			});
            this.priority.Location = new System.Drawing.Point(88, 37);
            this.priority.Name = "priority";
            this.priority.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] { new DevExpress.XtraEditors.Controls.EditorButton() });
            this.priority.Properties.DisplayFormat.FormatString = "f0";
            this.priority.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.priority.Properties.EditFormat.FormatString = "f0";
            this.priority.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.priority.Properties.IsFloatValue = false;
            this.priority.Properties.Mask.EditMask = "n0";
            this.priority.Properties.MaxLength = 1;
            this.priority.Properties.MaxValue = new decimal(new Int32[] {
				9,
				0,
				0,
				0
			});
            this.priority.Size = new System.Drawing.Size(32, 20);
            this.priority.TabIndex = 5;
            //
            //creditor_name
            //
            this.creditor_name.Anchor = (System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) | System.Windows.Forms.AnchorStyles.Left) | System.Windows.Forms.AnchorStyles.Right);
            this.creditor_name.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.creditor_name.Location = new System.Drawing.Point(200, 16);
            this.creditor_name.Name = "creditor_name";
            this.creditor_name.Size = new System.Drawing.Size(341, 61);
            this.creditor_name.TabIndex = 6;
            //
            //FormHeader
            //
            this.Controls.Add(this.creditor_name);
            this.Controls.Add(this.creditor_label);
            this.Controls.Add(this.priority);
            this.Controls.Add(this.LabelControl5);
            this.Controls.Add(this.LabelControl1);
            this.Name = "FormHeader";
            this.Size = new System.Drawing.Size(544, 88);
            ((System.ComponentModel.ISupportInitialize)this.priority.Properties).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();
        }

        #endregion " Windows Form Designer generated code "

        private DebtPlus.Interfaces.Debt.IDebtRecord currentDebtRecord;
        public override void ReadForm(DebtPlus.Interfaces.Debt.IDebtRecord CurrentDebtRecord)
        {
            this.currentDebtRecord = CurrentDebtRecord;

            // Set the priority if there is one
            if (currentDebtRecord.priority >= 0)
            {
                priority.EditValue = currentDebtRecord.priority;
            }
            else
            {
                priority.EditValue = CreditorDefaultPriority();
            }

            string CreditorName = string.Empty;

            // Find the creditor ID
            if (!string.IsNullOrEmpty(currentDebtRecord.Creditor))
            {
                creditor_label.Text   = currentDebtRecord.Creditor;
                creditor_label.Click += CreditorClick;

                using (var bc = new BusinessContext())
                {
                    var q = bc.addresses.Join(bc.creditor_addresses, a => a.Id, cr => cr.AddressID, (a, cr) => new { creditor = cr.creditor, adr = a, attn = cr.attn }).Where(s => s.creditor == currentDebtRecord.Creditor).Select(s => new DebtPlus.LINQ.CreditorAddressEntry(s.adr, s.attn)).FirstOrDefault();
                    if (q != null)
                    {
                        CreditorName = q.ToString();
                    }
                }
            }

            // Supply the default information as needed
            if (CreditorName == string.Empty)
            {
                CreditorName = DebtPlus.Utils.Nulls.DStr(currentDebtRecord.creditor_name).Trim();

                // Merge the division name into the creditor reference if possible
                string DivisionName = DebtPlus.Utils.Nulls.DStr(currentDebtRecord.cr_division_name).Trim();
                if (CreditorName != string.Empty && DivisionName != string.Empty)
                {
                    CreditorName = CreditorName + Environment.NewLine + DivisionName;
                }
                else
                {
                    CreditorName = CreditorName + DivisionName;
                }

                if (CreditorName == string.Empty)
                {
                    CreditorName = "This creditor has no name.";
                }
            }

            creditor_name.Text = CreditorName;
        }

        public override void SaveForm()
        {
            currentDebtRecord.priority = Convert.ToInt32(priority.EditValue);
        }

        private void CreditorClick(object Sender, EventArgs e)
        {
            string NewCreditorID = ((LabelControl)Sender).Text.Trim();
            if (NewCreditorID != string.Empty)
            {
                System.Threading.Thread thrd = new System.Threading.Thread(new System.Threading.ParameterizedThreadStart(CreditorUpdateDoWork));
                thrd.SetApartmentState(System.Threading.ApartmentState.STA);
                thrd.Name = "Creditor Update";
                thrd.Start(NewCreditorID);
            }
        }

        private Int32 CreditorDefaultPriority()
        {
            Int32 Result = 9;
            if (!string.IsNullOrEmpty(currentDebtRecord.Creditor))
            {
                using (var bc = new DebtPlus.LINQ.BusinessContext())
                {
                    Result = bc.creditors.Where(s => s.Id == currentDebtRecord.Creditor).Select(s => s.usual_priority).FirstOrDefault();
                }
            }

            // Do a simple range check on the values
            if (Result < 0)
            {
                Result = 0;
            }
            else if (Result > 9)
            {
                Result = 9;
            }

            return Result;
        }

        private void CreditorUpdateDoWork(object obj)
        {
            string newCreditorId = Convert.ToString(obj);
            if (string.IsNullOrEmpty(newCreditorId))
            {
                return;
            }

            // Allocate a database context for the creditor update operation.
            var bc = new DebtPlus.LINQ.BusinessContext();
            try
            {
                var creditorRecord = bc.creditors.Where(s => s.Id == newCreditorId).FirstOrDefault();
                if (creditorRecord == null)
                {
                    DebtPlus.Data.Forms.MessageBox.Show("The creditor is no longer in the system. The edit operation is cancelled.", "Sorry, can not find the creditor", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Error);
                    return;
                }

                // Hook into the submit changes function so that we can record our system note.
                bc.BeforeSubmitChanges += bc.RecordSystemNoteHandler;

                // We have a creditor record. Do the edit operation on the new creditor
                using (var frm = new DebtPlus.UI.Creditor.Update.Forms.Form_CreditorUpdate(bc, creditorRecord, false))
                {
                    frm.ShowDialog();
                }

                // Submit the final set of changes to the creditor
                bc.SubmitChanges();
            }

            finally
            {
                bc.BeforeSubmitChanges -= bc.RecordSystemNoteHandler;
                bc.Dispose();
            }
        }
    }
}
