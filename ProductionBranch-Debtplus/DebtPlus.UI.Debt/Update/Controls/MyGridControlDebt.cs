#region "Copyright 2000-2012 DebtPlus, L.L.C."

//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}

#endregion "Copyright 2000-2012 DebtPlus, L.L.C."

using System;
using System.Data;
using System.IO;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;

namespace DebtPlus.UI.Debt.Update.controls
{
    internal partial class MyGridControlDebt : DebtPlus.UI.Debt.Update.controls.ControlBaseDebt
    {
        protected bool EnableMenus = false;
        protected Int32 ControlRow;

        public MyGridControlDebt()
            : base()
        {
            InitializeComponent();

            if (!DesignMode)
            {
                RegisterHandlers();
            }
        }

        private void RegisterHandlers()
        {
            Load                           += MyGridControl_Load;
            GridView1.FocusedRowChanged    += GridView1_FocusedRowChanged;
            GridView1.DoubleClick          += GridView1_DoubleClick;
            BarButtonItem_Delete.ItemClick += MenuItemDelete;
            BarButtonItem_Change.ItemClick += MenuItemEdit;
            BarButtonItem_Add.ItemClick    += MenuItemCreate;
            PopupMenu_Items.Popup          += PopupMenu_Items_Popup;
            GridView1.MouseDown            += GridView1_MouseDown;
            GridView1.MouseUp              += GridView1_MouseUp;
        }

        private void UnRegisterHandlers()
        {
            Load                           -= MyGridControl_Load;
            GridView1.FocusedRowChanged    -= GridView1_FocusedRowChanged;
            GridView1.DoubleClick          -= GridView1_DoubleClick;
            BarButtonItem_Delete.ItemClick -= MenuItemDelete;
            BarButtonItem_Change.ItemClick -= MenuItemEdit;
            BarButtonItem_Add.ItemClick    -= MenuItemCreate;
            PopupMenu_Items.Popup          -= PopupMenu_Items_Popup;
            GridView1.MouseDown            -= GridView1_MouseDown;
            GridView1.MouseUp              -= GridView1_MouseUp;
        }

#region Windows Form Designer generated code

        //NOTE: The following procedure is required by the Windows Form Designer
        //It can be modified using the Windows Form Designer.
        //Do not modify it using the code editor.

        private System.ComponentModel.IContainer components = null;

        //Form overrides dispose to clean up the component list.
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (components != null)
                {
                    components.Dispose();
                }
            }
            base.Dispose(disposing);
        }

        //Required by the Windows Form Designer
        [System.Diagnostics.DebuggerStepThrough]
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.GridControl1 = new DevExpress.XtraGrid.GridControl();
            this.GridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.barManager1 = new DevExpress.XtraBars.BarManager(this.components);
            this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
            this.BarButtonItem_Add = new DevExpress.XtraBars.BarButtonItem();
            this.BarButtonItem_Change = new DevExpress.XtraBars.BarButtonItem();
            this.BarButtonItem_Delete = new DevExpress.XtraBars.BarButtonItem();
            this.BarButtonItem_Reassign = new DevExpress.XtraBars.BarButtonItem();
            this.BarButtonItem_Extra2 = new DevExpress.XtraBars.BarButtonItem();
            this.PopupMenu_Items = new DevExpress.XtraBars.PopupMenu(this.components);
            this.BarSubItem_Print = new DevExpress.XtraBars.BarSubItem();
            this.BarButtonItem_Print_Standard = new DevExpress.XtraBars.BarButtonItem();
            this.BarButtonItem_Print_Full = new DevExpress.XtraBars.BarButtonItem();
            ((System.ComponentModel.ISupportInitialize)this.GridControl1).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.GridView1).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.barManager1).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.PopupMenu_Items).BeginInit();
            this.SuspendLayout();
            //
            //GridControl1
            //
            this.GridControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.GridControl1.Location = new System.Drawing.Point(0, 0);
            this.GridControl1.MainView = this.GridView1;
            this.GridControl1.Name = "GridControl1";
            this.GridControl1.Size = new System.Drawing.Size(576, 296);
            this.GridControl1.TabIndex = 0;
            this.GridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] { this.GridView1 });
            //
            //GridView1
            //
            this.GridView1.Appearance.ColumnFilterButton.BackColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(104), (Int32)Convert.ToByte(184), (Int32)Convert.ToByte(251));
            this.GridView1.Appearance.ColumnFilterButton.BorderColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(104), (Int32)Convert.ToByte(184), (Int32)Convert.ToByte(251));
            this.GridView1.Appearance.ColumnFilterButton.ForeColor = System.Drawing.Color.White;
            this.GridView1.Appearance.ColumnFilterButton.Options.UseBackColor = true;
            this.GridView1.Appearance.ColumnFilterButton.Options.UseBorderColor = true;
            this.GridView1.Appearance.ColumnFilterButton.Options.UseForeColor = true;
            this.GridView1.Appearance.ColumnFilterButtonActive.BackColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(170), (Int32)Convert.ToByte(216), (Int32)Convert.ToByte(254));
            this.GridView1.Appearance.ColumnFilterButtonActive.BorderColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(170), (Int32)Convert.ToByte(216), (Int32)Convert.ToByte(254));
            this.GridView1.Appearance.ColumnFilterButtonActive.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.ColumnFilterButtonActive.Options.UseBackColor = true;
            this.GridView1.Appearance.ColumnFilterButtonActive.Options.UseBorderColor = true;
            this.GridView1.Appearance.ColumnFilterButtonActive.Options.UseForeColor = true;
            this.GridView1.Appearance.Empty.BackColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(236), (Int32)Convert.ToByte(246), (Int32)Convert.ToByte(255));
            this.GridView1.Appearance.Empty.BackColor2 = System.Drawing.Color.White;
            this.GridView1.Appearance.Empty.Options.UseBackColor = true;
            this.GridView1.Appearance.EvenRow.BackColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(247), (Int32)Convert.ToByte(251), (Int32)Convert.ToByte(255));
            this.GridView1.Appearance.EvenRow.BorderColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(247), (Int32)Convert.ToByte(251), (Int32)Convert.ToByte(255));
            this.GridView1.Appearance.EvenRow.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.EvenRow.Options.UseBackColor = true;
            this.GridView1.Appearance.EvenRow.Options.UseBorderColor = true;
            this.GridView1.Appearance.EvenRow.Options.UseForeColor = true;
            this.GridView1.Appearance.FilterCloseButton.BackColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(104), (Int32)Convert.ToByte(184), (Int32)Convert.ToByte(251));
            this.GridView1.Appearance.FilterCloseButton.BorderColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(104), (Int32)Convert.ToByte(184), (Int32)Convert.ToByte(251));
            this.GridView1.Appearance.FilterCloseButton.ForeColor = System.Drawing.Color.White;
            this.GridView1.Appearance.FilterCloseButton.Options.UseBackColor = true;
            this.GridView1.Appearance.FilterCloseButton.Options.UseBorderColor = true;
            this.GridView1.Appearance.FilterCloseButton.Options.UseForeColor = true;
            this.GridView1.Appearance.FilterPanel.BackColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(236), (Int32)Convert.ToByte(246), (Int32)Convert.ToByte(255));
            this.GridView1.Appearance.FilterPanel.BackColor2 = System.Drawing.Color.White;
            this.GridView1.Appearance.FilterPanel.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.FilterPanel.Options.UseBackColor = true;
            this.GridView1.Appearance.FilterPanel.Options.UseForeColor = true;
            this.GridView1.Appearance.FixedLine.BackColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(59), (Int32)Convert.ToByte(133), (Int32)Convert.ToByte(195));
            this.GridView1.Appearance.FixedLine.Options.UseBackColor = true;
            this.GridView1.Appearance.FocusedCell.BackColor = System.Drawing.Color.White;
            this.GridView1.Appearance.FocusedCell.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.FocusedCell.Options.UseBackColor = true;
            this.GridView1.Appearance.FocusedCell.Options.UseForeColor = true;
            this.GridView1.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(38), (Int32)Convert.ToByte(109), (Int32)Convert.ToByte(189));
            this.GridView1.Appearance.FocusedRow.BorderColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(59), (Int32)Convert.ToByte(139), (Int32)Convert.ToByte(206));
            this.GridView1.Appearance.FocusedRow.ForeColor = System.Drawing.Color.White;
            this.GridView1.Appearance.FocusedRow.Options.UseBackColor = true;
            this.GridView1.Appearance.FocusedRow.Options.UseBorderColor = true;
            this.GridView1.Appearance.FocusedRow.Options.UseForeColor = true;
            this.GridView1.Appearance.FooterPanel.BackColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(104), (Int32)Convert.ToByte(184), (Int32)Convert.ToByte(251));
            this.GridView1.Appearance.FooterPanel.BorderColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(104), (Int32)Convert.ToByte(184), (Int32)Convert.ToByte(251));
            this.GridView1.Appearance.FooterPanel.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.FooterPanel.Options.UseBackColor = true;
            this.GridView1.Appearance.FooterPanel.Options.UseBorderColor = true;
            this.GridView1.Appearance.FooterPanel.Options.UseForeColor = true;
            this.GridView1.Appearance.GroupButton.BackColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(104), (Int32)Convert.ToByte(184), (Int32)Convert.ToByte(251));
            this.GridView1.Appearance.GroupButton.BorderColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(104), (Int32)Convert.ToByte(184), (Int32)Convert.ToByte(251));
            this.GridView1.Appearance.GroupButton.Options.UseBackColor = true;
            this.GridView1.Appearance.GroupButton.Options.UseBorderColor = true;
            this.GridView1.Appearance.GroupFooter.BackColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(170), (Int32)Convert.ToByte(216), (Int32)Convert.ToByte(254));
            this.GridView1.Appearance.GroupFooter.BorderColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(170), (Int32)Convert.ToByte(216), (Int32)Convert.ToByte(254));
            this.GridView1.Appearance.GroupFooter.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.GroupFooter.Options.UseBackColor = true;
            this.GridView1.Appearance.GroupFooter.Options.UseBorderColor = true;
            this.GridView1.Appearance.GroupFooter.Options.UseForeColor = true;
            this.GridView1.Appearance.GroupPanel.BackColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(236), (Int32)Convert.ToByte(246), (Int32)Convert.ToByte(255));
            this.GridView1.Appearance.GroupPanel.BackColor2 = System.Drawing.Color.White;
            this.GridView1.Appearance.GroupPanel.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.GroupPanel.Options.UseBackColor = true;
            this.GridView1.Appearance.GroupPanel.Options.UseForeColor = true;
            this.GridView1.Appearance.GroupRow.BackColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(170), (Int32)Convert.ToByte(216), (Int32)Convert.ToByte(254));
            this.GridView1.Appearance.GroupRow.BorderColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(170), (Int32)Convert.ToByte(216), (Int32)Convert.ToByte(254));
            this.GridView1.Appearance.GroupRow.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.GroupRow.Options.UseBackColor = true;
            this.GridView1.Appearance.GroupRow.Options.UseBorderColor = true;
            this.GridView1.Appearance.GroupRow.Options.UseForeColor = true;
            this.GridView1.Appearance.HeaderPanel.BackColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(139), (Int32)Convert.ToByte(201), (Int32)Convert.ToByte(254));
            this.GridView1.Appearance.HeaderPanel.BorderColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(139), (Int32)Convert.ToByte(201), (Int32)Convert.ToByte(254));
            this.GridView1.Appearance.HeaderPanel.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.HeaderPanel.Options.UseBackColor = true;
            this.GridView1.Appearance.HeaderPanel.Options.UseBorderColor = true;
            this.GridView1.Appearance.HeaderPanel.Options.UseForeColor = true;
            this.GridView1.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(105), (Int32)Convert.ToByte(170), (Int32)Convert.ToByte(225));
            this.GridView1.Appearance.HideSelectionRow.BorderColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(83), (Int32)Convert.ToByte(155), (Int32)Convert.ToByte(215));
            this.GridView1.Appearance.HideSelectionRow.ForeColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(236), (Int32)Convert.ToByte(246), (Int32)Convert.ToByte(255));
            this.GridView1.Appearance.HideSelectionRow.Options.UseBackColor = true;
            this.GridView1.Appearance.HideSelectionRow.Options.UseBorderColor = true;
            this.GridView1.Appearance.HideSelectionRow.Options.UseForeColor = true;
            this.GridView1.Appearance.HorzLine.BackColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(104), (Int32)Convert.ToByte(184), (Int32)Convert.ToByte(251));
            this.GridView1.Appearance.HorzLine.Options.UseBackColor = true;
            this.GridView1.Appearance.OddRow.BackColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(236), (Int32)Convert.ToByte(246), (Int32)Convert.ToByte(255));
            this.GridView1.Appearance.OddRow.BorderColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(236), (Int32)Convert.ToByte(246), (Int32)Convert.ToByte(255));
            this.GridView1.Appearance.OddRow.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.OddRow.Options.UseBackColor = true;
            this.GridView1.Appearance.OddRow.Options.UseBorderColor = true;
            this.GridView1.Appearance.OddRow.Options.UseForeColor = true;
            this.GridView1.Appearance.Preview.Font = new System.Drawing.Font("Verdana", 7.5f);
            this.GridView1.Appearance.Preview.ForeColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(83), (Int32)Convert.ToByte(155), (Int32)Convert.ToByte(215));
            this.GridView1.Appearance.Preview.Options.UseFont = true;
            this.GridView1.Appearance.Preview.Options.UseForeColor = true;
            this.GridView1.Appearance.Row.BackColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(247), (Int32)Convert.ToByte(251), (Int32)Convert.ToByte(255));
            this.GridView1.Appearance.Row.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.Row.Options.UseBackColor = true;
            this.GridView1.Appearance.Row.Options.UseForeColor = true;
            this.GridView1.Appearance.RowSeparator.BackColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(236), (Int32)Convert.ToByte(246), (Int32)Convert.ToByte(255));
            this.GridView1.Appearance.RowSeparator.BackColor2 = System.Drawing.Color.White;
            this.GridView1.Appearance.RowSeparator.Options.UseBackColor = true;
            this.GridView1.Appearance.SelectedRow.BackColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(83), (Int32)Convert.ToByte(155), (Int32)Convert.ToByte(215));
            this.GridView1.Appearance.SelectedRow.ForeColor = System.Drawing.Color.White;
            this.GridView1.Appearance.SelectedRow.Options.UseBackColor = true;
            this.GridView1.Appearance.SelectedRow.Options.UseForeColor = true;
            this.GridView1.Appearance.TopNewRow.BackColor = System.Drawing.Color.White;
            this.GridView1.Appearance.TopNewRow.Options.UseBackColor = true;
            this.GridView1.Appearance.VertLine.BackColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(104), (Int32)Convert.ToByte(184), (Int32)Convert.ToByte(251));
            this.GridView1.Appearance.VertLine.Options.UseBackColor = true;
            this.GridView1.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.GridView1.GridControl = this.GridControl1;
            this.GridView1.Name = "GridView1";
            this.GridView1.OptionsBehavior.AutoPopulateColumns = false;
            this.GridView1.OptionsBehavior.Editable = false;
            this.GridView1.OptionsView.EnableAppearanceEvenRow = true;
            this.GridView1.OptionsView.EnableAppearanceOddRow = true;
            this.GridView1.OptionsView.ShowGroupPanel = false;
            this.GridView1.OptionsView.ShowIndicator = false;
            this.GridView1.ShowButtonMode = DevExpress.XtraGrid.Views.Base.ShowButtonModeEnum.ShowForFocusedRow;
            //
            //barManager1
            //
            this.barManager1.DockControls.Add(this.barDockControlTop);
            this.barManager1.DockControls.Add(this.barDockControlBottom);
            this.barManager1.DockControls.Add(this.barDockControlLeft);
            this.barManager1.DockControls.Add(this.barDockControlRight);
            this.barManager1.Form = this;
            this.barManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
				this.BarButtonItem_Add,
				this.BarButtonItem_Change,
				this.BarButtonItem_Delete,
				this.BarButtonItem_Reassign,
				this.BarButtonItem_Extra2,
				this.BarSubItem_Print,
				this.BarButtonItem_Print_Standard,
				this.BarButtonItem_Print_Full
			});
            this.barManager1.MaxItemId = 8;
            //
            //BarButtonItem_Add
            //
            this.BarButtonItem_Add.Caption = "&Add";
            this.BarButtonItem_Add.Description = "Add an item to the list";
            this.BarButtonItem_Add.Id = 0;
            this.BarButtonItem_Add.Name = "BarButtonItem_Add";
            //
            //BarButtonItem_Change
            //
            this.BarButtonItem_Change.Caption = "&Change";
            this.BarButtonItem_Change.Description = "Change the list item";
            this.BarButtonItem_Change.Id = 1;
            this.BarButtonItem_Change.Name = "BarButtonItem_Change";
            //
            //BarButtonItem_Delete
            //
            this.BarButtonItem_Delete.Caption = "&Delete";
            this.BarButtonItem_Delete.Description = "Remove the item from the list";
            this.BarButtonItem_Delete.Id = 2;
            this.BarButtonItem_Delete.Name = "BarButtonItem_Delete";
            //
            //BarButtonItem_Reassign
            //
            this.BarButtonItem_Reassign.Caption = "&Reassign";
            this.BarButtonItem_Reassign.Description = "Transfer the debt to a new creditor";
            this.BarButtonItem_Reassign.Id = 3;
            this.BarButtonItem_Reassign.Name = "BarButtonItem_Reassign";
            this.BarButtonItem_Reassign.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            //
            //BarButtonItem_Extra2
            //
            this.BarButtonItem_Extra2.Caption = "&Extra2";
            this.BarButtonItem_Extra2.Description = "Extra Item";
            this.BarButtonItem_Extra2.Id = 4;
            this.BarButtonItem_Extra2.Name = "BarButtonItem_Extra2";
            this.BarButtonItem_Extra2.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            //
            //PopupMenu_Items
            //
            this.PopupMenu_Items.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
				new DevExpress.XtraBars.LinkPersistInfo(this.BarButtonItem_Add),
				new DevExpress.XtraBars.LinkPersistInfo(this.BarButtonItem_Change),
				new DevExpress.XtraBars.LinkPersistInfo(this.BarButtonItem_Delete),
				new DevExpress.XtraBars.LinkPersistInfo(this.BarButtonItem_Reassign, true),
				new DevExpress.XtraBars.LinkPersistInfo(this.BarButtonItem_Extra2),
				new DevExpress.XtraBars.LinkPersistInfo(this.BarSubItem_Print)
			});
            this.PopupMenu_Items.Manager = this.barManager1;
            this.PopupMenu_Items.MenuCaption = "Right Click Menus";
            this.PopupMenu_Items.Name = "PopupMenu_Items";
            //
            //BarSubItem_Print2
            //
            this.BarSubItem_Print.Caption = "Re-&print";
            this.BarSubItem_Print.Description = "Reprint proposals";
            this.BarSubItem_Print.Id = 5;
            this.BarSubItem_Print.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
				new DevExpress.XtraBars.LinkPersistInfo(this.BarButtonItem_Print_Standard),
				new DevExpress.XtraBars.LinkPersistInfo(this.BarButtonItem_Print_Full)
			});
            this.BarSubItem_Print.Name = "BarSubItem_Print2";
            this.BarSubItem_Print.Visibility = DevExpress.XtraBars.BarItemVisibility.OnlyInCustomizing;
            //
            //BarButtonItem_Print_Standard
            //
            this.BarButtonItem_Print_Standard.Caption = "&Standard...";
            this.BarButtonItem_Print_Standard.Description = "Reprint the proposal as a standard proposal";
            this.BarButtonItem_Print_Standard.Id = 6;
            this.BarButtonItem_Print_Standard.Name = "BarButtonItem_Print_Standard";
            //
            //BarButtonItem_Print_Full
            //
            this.BarButtonItem_Print_Full.Caption = "&Full Disclosure...";
            this.BarButtonItem_Print_Full.Description = "Reprint the proposal as a full-disclosure proposal";
            this.BarButtonItem_Print_Full.Id = 7;
            this.BarButtonItem_Print_Full.Name = "BarButtonItem_Print_Full";
            //
            //MyGridControl
            //
            this.Controls.Add(this.GridControl1);
            this.Controls.Add(this.barDockControlLeft);
            this.Controls.Add(this.barDockControlRight);
            this.Controls.Add(this.barDockControlBottom);
            this.Controls.Add(this.barDockControlTop);
            this.Name = "MyGridControlDebt";
            ((System.ComponentModel.ISupportInitialize)this.GridControl1).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.GridView1).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.barManager1).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.PopupMenu_Items).EndInit();
            this.ResumeLayout(false);
        }
        protected internal DevExpress.XtraGrid.GridControl GridControl1;
        protected internal DevExpress.XtraGrid.Views.Grid.GridView GridView1;
        protected internal DevExpress.XtraBars.BarButtonItem BarButtonItem_Add;
        protected internal DevExpress.XtraBars.BarButtonItem BarButtonItem_Change;
        protected internal DevExpress.XtraBars.BarButtonItem BarButtonItem_Delete;
        protected internal DevExpress.XtraBars.BarButtonItem BarButtonItem_Extra2;
        protected internal DevExpress.XtraBars.BarButtonItem BarButtonItem_Print_Full;
        protected internal DevExpress.XtraBars.BarButtonItem BarButtonItem_Print_Standard;
        protected internal DevExpress.XtraBars.BarButtonItem BarButtonItem_Reassign;
        protected internal DevExpress.XtraBars.BarDockControl barDockControlBottom;
        protected internal DevExpress.XtraBars.BarDockControl barDockControlLeft;
        protected internal DevExpress.XtraBars.BarDockControl barDockControlRight;
        protected internal DevExpress.XtraBars.BarDockControl barDockControlTop;
        protected internal DevExpress.XtraBars.BarManager barManager1;
        protected internal DevExpress.XtraBars.BarSubItem BarSubItem_Print;
        protected internal DevExpress.XtraBars.PopupMenu PopupMenu_Items;
#endregion Windows Form Designer generated code

        /// <summary>
        /// Process a CREATE menu item choice
        /// </summary>
        protected void MenuItemCreate(object sender, EventArgs e)
        {
            OnCreate();
        }

        /// <summary>
        /// Process a DELETE menu item
        /// </summary>
        protected void MenuItemDelete(object sender, EventArgs e)
        {
            if (ControlRow >= 0)
            {
                object obj = GridView1.GetRow(ControlRow);
                if (obj != null)
                {
                    OnDelete(obj);
                }
            }
        }

        /// <summary>
        /// Process an EDIT menu item choice
        /// </summary>
        protected void MenuItemEdit(object sender, EventArgs e)
        {
            if (ControlRow >= 0)
            {
                object obj = GridView1.GetRow(ControlRow);
                if (obj != null)
                {
                    OnEdit(obj);
                }
            }
        }

        /// <summary>
        /// Is the item valid to be created?
        /// </summary>
        protected virtual bool OkToCreate()
        {
            return true;
        }

        /// <summary>
        /// Is the item valid to be deleted?
        /// </summary>
        protected virtual bool OkToDelete(object obj)
        {
            return obj != null;
        }

        /// <summary>
        /// Is the item valid to be edited?
        /// </summary>
        protected virtual bool OkToEdit(object obj)
        {
            return obj != null;
        }

        /// <summary>
        /// Create the item
        /// </summary>
        protected virtual void OnCreate()
        {
        }

        /// <summary>
        /// Delete the item from the list
        /// </summary>
        protected virtual void OnDelete(object obj)
        {
        }

        /// <summary>
        /// Edit the item
        /// </summary>
        protected virtual void OnEdit(object obj)
        {
        }

        /// <summary>
        /// Overridable function to indicate that the row was selected
        /// </summary>
        protected virtual void OnSelect(object obj)
        {
        }

        /// <summary>
        /// Handle the condition where the popup menu was activated
        /// </summary>
        protected virtual void PopupMenu_Items_Popup(object sender, EventArgs e)
        {
            BarButtonItem_Add.Enabled = OkToCreate();
            BarButtonItem_Change.Enabled = (ControlRow >= 0) && OkToEdit(GridView1.GetRow(ControlRow));
            BarButtonItem_Delete.Enabled = (ControlRow >= 0) && OkToDelete(GridView1.GetRow(ControlRow));
        }

        /// <summary>
        /// Restore the layout from the saved file
        /// </summary>
        protected void ReloadLayout()
        {
        }

        /// <summary>
        /// Return the directory to the saved layout information
        /// </summary>
        protected virtual string XMLBasePath()
        {
            if (DesignMode || ParentForm == null)
            {
                return string.Empty;
            }
            return System.IO.Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData), "DebtPlus", "Debt.Update", ParentForm.Name);
        }

        /// <summary>
        /// Double Click on an item -- edit it
        /// </summary>
        private void GridView1_DoubleClick(object sender, EventArgs e)
        {
            // Find the row targeted as the double-click item
            GridView gv      = (GridView)sender;
            GridControl ctl  = (GridControl)gv.GridControl;
            GridHitInfo hi   = gv.CalcHitInfo((ctl.PointToClient(MousePosition)));
            Int32 ControlRow = hi.RowHandle;

            // Find the row from the tables and call the edit function
            if (ControlRow >= 0)
            {
                object obj = gv.GetRow(ControlRow);
                if (obj != null)
                {
                    OnEdit(obj);
                }
            }
        }

        /// <summary>
        /// Process a change in the current row for the control
        /// </summary>
        private void GridView1_FocusedRowChanged(object sender, FocusedRowChangedEventArgs e)
        {
            GridView gv      = (GridView)sender;
            GridControl ctl  = gv.GridControl;
            Int32 ControlRow = e.FocusedRowHandle;

            // Find the row from the tables and call the select function
            if (ControlRow >= 0)
            {
                object obj = gv.GetRow(ControlRow);
                if (obj != null)
                {
                    OnSelect(obj);
                }
            }
        }

        /// <summary>
        /// Look for the mouse down on the grid control
        /// </summary>
        private void GridView1_MouseDown(object sender, System.Windows.Forms.MouseEventArgs e)
        {
            GridView gv     = GridView1;
            GridControl ctl = (GridControl)gv.GridControl;
            GridHitInfo hi  = gv.CalcHitInfo(new System.Drawing.Point(e.X, e.Y));

            // Remember the position for the popup menu handler.
            if (hi.InRow)
            {
                ControlRow = hi.RowHandle;
            }
            else
            {
                ControlRow = -1;
            }
        }

        /// <summary>
        /// Handle the condition where the popup menu was activated
        /// </summary>
        private void GridView1_MouseUp(object sender, System.Windows.Forms.MouseEventArgs e)
        {
            if (EnableMenus && e.Button == System.Windows.Forms.MouseButtons.Right)
            {
                PopupMenu_Items.ShowPopup(MousePosition);
            }
        }

        /// <summary>
        /// If the layout is changed then update the file
        /// </summary>
        private void LayoutChanged(object Sender, EventArgs e)
        {
            UnRegisterHandlers();
            try
            {
                string PathName = XMLBasePath();
                if (PathName != string.Empty)
                {
                    if (!System.IO.Directory.Exists(PathName))
                    {
                        System.IO.Directory.CreateDirectory(PathName);
                    }

                    string FileName = Path.Combine(PathName, Name + ".Grid.xml");
                    GridView1.SaveLayoutToXml(FileName);
                }
            }
            finally
            {
                RegisterHandlers();
            }
        }

        /// <summary>
        /// When loading the control, restore the layout from the file
        /// </summary>
        private void MyGridControl_Load(object sender, EventArgs e)
        {
            UnRegisterHandlers();
            try
            {
                string PathName = XMLBasePath();

                if (PathName != string.Empty)
                {
                    string FileName = System.IO.Path.Combine(PathName, Name + ".Grid.xml");
                    if (System.IO.File.Exists(FileName))
                    {
                        GridView1.RestoreLayoutFromXml(FileName);
                    }
                }
            }

#pragma warning disable 168
            catch (System.IO.DirectoryNotFoundException ex) { }
            catch (System.IO.FileNotFoundException ex) { }
#pragma warning restore 168

            finally
            {
                RegisterHandlers();
            }
        }
    }
}
