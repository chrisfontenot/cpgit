#region "Copyright 2000-2012 DebtPlus, L.L.C."

//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}

#endregion "Copyright 2000-2012 DebtPlus, L.L.C."

using System;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Data;
using DebtPlus.Events;
using DevExpress.XtraEditors;

namespace DebtPlus.UI.Debt.Update.controls
{
    internal partial class ControlBaseDebt : XtraUserControl
    {
        protected DataRowView drv = null;

        public ControlBaseDebt()
            : base()
        {
            InitializeComponent();
        }

        protected DataTable UpdateTable { get; set; }

        /// <summary>
        /// Read the form data
        /// </summary>
        [Description("Called when the data is to be reloaded on this page.")]
        public virtual void ReadForm(DebtPlus.Interfaces.Debt.IDebtRecord DebtRecord)
        {
        }

        /// <summary>
        /// Save the form changes
        /// </summary>
        [Description("Called when the data is to be saved on this page.")]
        public virtual void SaveForm()
        {
        }

        #region " Windows Form Designer generated code "

        private System.ComponentModel.IContainer components = null;

        //UserControl overrides dispose to clean up the component list.
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (components != null)
                {
                    components.Dispose();
                }
            }
            base.Dispose(disposing);
        }

        //Required by the Windows Form Designer
        //NOTE: The following procedure is required by the Windows Form Designer
        //It can be modified using the Windows Form Designer.
        //Do not modify it using the code editor.
        [System.Diagnostics.DebuggerStepThrough()]
        private void InitializeComponent()
        {
            //
            //ControlBase
            //
            this.Name = "ControlBaseDebt";
            this.Size = new System.Drawing.Size(576, 296);
        }

        #endregion " Windows Form Designer generated code "
    }
}
