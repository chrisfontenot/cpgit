#region "Copyright 2000-2012 DebtPlus, L.L.C."

//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}

#endregion "Copyright 2000-2012 DebtPlus, L.L.C."

using System;
using System.Linq;
using System.Windows.Forms;
using DebtPlus.LINQ;
using DebtPlus.Reports.Proposals.Full;
using DebtPlus.Reports.Proposals.Standard;
using DebtPlus.UI.Debt.Update.forms;
using DevExpress.XtraBars;

namespace DebtPlus.UI.Debt.Update.controls
{
    internal partial class ProposalsList : MyGridControlDebt
    {
        // Pointer to the current business context block
        internal BusinessContext clientCreditorProposalBC                              = null;
        private DebtPlus.Interfaces.Debt.IDebtRecord currentDebtRecord                 = null;
        private System.Collections.Generic.List<client_creditor_proposal> colProposals = null;

        public ProposalsList()
            : base()
        {
            InitializeComponent();

            // Enable the menus
            EnableMenus                             = true;
            BarSubItem_Print.Visibility             = BarItemVisibility.Always;

            // Put in the "Handles" clauses here
            BarButtonItem_Print_Full.ItemClick     += BarButtonItem_Print_Full_Click;
            BarButtonItem_Print_Standard.ItemClick += BarButtonItem_Print_Standard_Click;
        }

        #region " Windows Form Designer generated code "

        internal DevExpress.XtraGrid.Columns.GridColumn GridColumn_client_creditor_proposal;
        internal DevExpress.XtraGrid.Columns.GridColumn GridColumn_created_by;
        internal DevExpress.XtraGrid.Columns.GridColumn GridColumn_date_created;
        internal DevExpress.XtraGrid.Columns.GridColumn GridColumn_formatted_proposal_result_disposition;
        internal DevExpress.XtraGrid.Columns.GridColumn GridColumn_formatted_proposal_result_reason;
        internal DevExpress.XtraGrid.Columns.GridColumn GridColumn_formatted_proposal_status;
        internal DevExpress.XtraGrid.Columns.GridColumn GridColumn_percent_balance;
        internal DevExpress.XtraGrid.Columns.GridColumn GridColumn_proposal_accepted_by;
        internal DevExpress.XtraGrid.Columns.GridColumn GridColumn_proposal_message;
        internal DevExpress.XtraGrid.Columns.GridColumn GridColumn_proposal_print_date;
        internal DevExpress.XtraGrid.Columns.GridColumn GridColumn_proposal_reject_disp;
        internal DevExpress.XtraGrid.Columns.GridColumn GridColumn_proposal_reject_reason;
        internal DevExpress.XtraGrid.Columns.GridColumn GridColumn_proposal_status;
        internal DevExpress.XtraGrid.Columns.GridColumn GridColumn_proposed_date;
        internal DevExpress.XtraGrid.Columns.GridColumn GridColumn_StatusDate;
        internal DevExpress.XtraGrid.Columns.GridColumn GridColumn_terms;

        //NOTE: The following procedure is required by the Windows Form Designer
        //It can be modified using the Windows Form Designer.
        //Do not modify it using the code editor.

        //Required by the Windows Form Designer
        private System.ComponentModel.IContainer components = null;

        //Form overrides dispose to clean up the component list.
        protected override void Dispose(bool disposing)
        {
            try
            {
                if (disposing)
                {
                    if (components != null)
                    {
                        components.Dispose();
                    }

                    // Dispose of the proposal database connection object
                    if (clientCreditorProposalBC != null)
                    {
                        clientCreditorProposalBC.Dispose();
                        clientCreditorProposalBC = null;
                    }
                }
            }
            finally
            {
                base.Dispose(disposing);
            }
        }

        [System.Diagnostics.DebuggerStepThrough()]
        private void InitializeComponent()
        {
            this.GridColumn_StatusDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.GridColumn_StatusDate.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.GridColumn_formatted_proposal_status = new DevExpress.XtraGrid.Columns.GridColumn();
            this.GridColumn_formatted_proposal_status.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.GridColumn_formatted_proposal_result_disposition = new DevExpress.XtraGrid.Columns.GridColumn();
            this.GridColumn_formatted_proposal_result_disposition.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.GridColumn_formatted_proposal_result_reason = new DevExpress.XtraGrid.Columns.GridColumn();
            this.GridColumn_formatted_proposal_result_reason.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.GridColumn_terms = new DevExpress.XtraGrid.Columns.GridColumn();
            this.GridColumn_terms.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.GridColumn_percent_balance = new DevExpress.XtraGrid.Columns.GridColumn();
            this.GridColumn_percent_balance.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.GridColumn_created_by = new DevExpress.XtraGrid.Columns.GridColumn();
            this.GridColumn_created_by.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.GridColumn_date_created = new DevExpress.XtraGrid.Columns.GridColumn();
            this.GridColumn_date_created.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.GridColumn_proposed_date = new DevExpress.XtraGrid.Columns.GridColumn();
            this.GridColumn_proposed_date.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.GridColumn_proposal_reject_disp = new DevExpress.XtraGrid.Columns.GridColumn();
            this.GridColumn_proposal_reject_disp.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.GridColumn_proposal_reject_reason = new DevExpress.XtraGrid.Columns.GridColumn();
            this.GridColumn_proposal_reject_reason.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.GridColumn_proposal_accepted_by = new DevExpress.XtraGrid.Columns.GridColumn();
            this.GridColumn_proposal_accepted_by.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.GridColumn_proposal_message = new DevExpress.XtraGrid.Columns.GridColumn();
            this.GridColumn_proposal_message.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.GridColumn_proposal_print_date = new DevExpress.XtraGrid.Columns.GridColumn();
            this.GridColumn_proposal_print_date.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.GridColumn_proposal_status = new DevExpress.XtraGrid.Columns.GridColumn();
            this.GridColumn_proposal_status.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.GridColumn_client_creditor_proposal = new DevExpress.XtraGrid.Columns.GridColumn();
            this.GridColumn_client_creditor_proposal.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            ((System.ComponentModel.ISupportInitialize)this.GridControl1).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.GridView1).BeginInit();
            this.SuspendLayout();
            //
            //GridControl1
            //
            //
            //GridControl1.EmbeddedNavigator
            //
            this.GridControl1.EmbeddedNavigator.Name = "";
            this.GridControl1.Name = "GridControl1";
            //
            //GridView1
            //
            this.GridView1.Appearance.ColumnFilterButton.BackColor = System.Drawing.Color.FromArgb(Convert.ToByte(104), Convert.ToByte(184), Convert.ToByte(251));
            this.GridView1.Appearance.ColumnFilterButton.BorderColor = System.Drawing.Color.FromArgb(Convert.ToByte(104), Convert.ToByte(184), Convert.ToByte(251));
            this.GridView1.Appearance.ColumnFilterButton.ForeColor = System.Drawing.Color.White;
            this.GridView1.Appearance.ColumnFilterButton.Options.UseBackColor = true;
            this.GridView1.Appearance.ColumnFilterButton.Options.UseBorderColor = true;
            this.GridView1.Appearance.ColumnFilterButton.Options.UseForeColor = true;
            this.GridView1.Appearance.ColumnFilterButtonActive.BackColor = System.Drawing.Color.FromArgb(Convert.ToByte(170), Convert.ToByte(216), Convert.ToByte(254));
            this.GridView1.Appearance.ColumnFilterButtonActive.BorderColor = System.Drawing.Color.FromArgb(Convert.ToByte(170), Convert.ToByte(216), Convert.ToByte(254));
            this.GridView1.Appearance.ColumnFilterButtonActive.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.ColumnFilterButtonActive.Options.UseBackColor = true;
            this.GridView1.Appearance.ColumnFilterButtonActive.Options.UseBorderColor = true;
            this.GridView1.Appearance.ColumnFilterButtonActive.Options.UseForeColor = true;
            this.GridView1.Appearance.Empty.BackColor = System.Drawing.Color.FromArgb(Convert.ToByte(236), Convert.ToByte(246), Convert.ToByte(255));
            this.GridView1.Appearance.Empty.BackColor2 = System.Drawing.Color.White;
            this.GridView1.Appearance.Empty.Options.UseBackColor = true;
            this.GridView1.Appearance.EvenRow.BackColor = System.Drawing.Color.FromArgb(Convert.ToByte(247), Convert.ToByte(251), Convert.ToByte(255));
            this.GridView1.Appearance.EvenRow.BorderColor = System.Drawing.Color.FromArgb(Convert.ToByte(247), Convert.ToByte(251), Convert.ToByte(255));
            this.GridView1.Appearance.EvenRow.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.EvenRow.Options.UseBackColor = true;
            this.GridView1.Appearance.EvenRow.Options.UseBorderColor = true;
            this.GridView1.Appearance.EvenRow.Options.UseForeColor = true;
            this.GridView1.Appearance.FilterCloseButton.BackColor = System.Drawing.Color.FromArgb(Convert.ToByte(104), Convert.ToByte(184), Convert.ToByte(251));
            this.GridView1.Appearance.FilterCloseButton.BorderColor = System.Drawing.Color.FromArgb(Convert.ToByte(104), Convert.ToByte(184), Convert.ToByte(251));
            this.GridView1.Appearance.FilterCloseButton.ForeColor = System.Drawing.Color.White;
            this.GridView1.Appearance.FilterCloseButton.Options.UseBackColor = true;
            this.GridView1.Appearance.FilterCloseButton.Options.UseBorderColor = true;
            this.GridView1.Appearance.FilterCloseButton.Options.UseForeColor = true;
            this.GridView1.Appearance.FilterPanel.BackColor = System.Drawing.Color.FromArgb(Convert.ToByte(236), Convert.ToByte(246), Convert.ToByte(255));
            this.GridView1.Appearance.FilterPanel.BackColor2 = System.Drawing.Color.White;
            this.GridView1.Appearance.FilterPanel.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.FilterPanel.Options.UseBackColor = true;
            this.GridView1.Appearance.FilterPanel.Options.UseForeColor = true;
            this.GridView1.Appearance.FixedLine.BackColor = System.Drawing.Color.FromArgb(Convert.ToByte(59), Convert.ToByte(133), Convert.ToByte(195));
            this.GridView1.Appearance.FixedLine.Options.UseBackColor = true;
            this.GridView1.Appearance.FocusedCell.BackColor = System.Drawing.Color.White;
            this.GridView1.Appearance.FocusedCell.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.FocusedCell.Options.UseBackColor = true;
            this.GridView1.Appearance.FocusedCell.Options.UseForeColor = true;
            this.GridView1.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb(Convert.ToByte(38), Convert.ToByte(109), Convert.ToByte(189));
            this.GridView1.Appearance.FocusedRow.BorderColor = System.Drawing.Color.FromArgb(Convert.ToByte(59), Convert.ToByte(139), Convert.ToByte(206));
            this.GridView1.Appearance.FocusedRow.ForeColor = System.Drawing.Color.White;
            this.GridView1.Appearance.FocusedRow.Options.UseBackColor = true;
            this.GridView1.Appearance.FocusedRow.Options.UseBorderColor = true;
            this.GridView1.Appearance.FocusedRow.Options.UseForeColor = true;
            this.GridView1.Appearance.FooterPanel.BackColor = System.Drawing.Color.FromArgb(Convert.ToByte(104), Convert.ToByte(184), Convert.ToByte(251));
            this.GridView1.Appearance.FooterPanel.BorderColor = System.Drawing.Color.FromArgb(Convert.ToByte(104), Convert.ToByte(184), Convert.ToByte(251));
            this.GridView1.Appearance.FooterPanel.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.FooterPanel.Options.UseBackColor = true;
            this.GridView1.Appearance.FooterPanel.Options.UseBorderColor = true;
            this.GridView1.Appearance.FooterPanel.Options.UseForeColor = true;
            this.GridView1.Appearance.GroupButton.BackColor = System.Drawing.Color.FromArgb(Convert.ToByte(104), Convert.ToByte(184), Convert.ToByte(251));
            this.GridView1.Appearance.GroupButton.BorderColor = System.Drawing.Color.FromArgb(Convert.ToByte(104), Convert.ToByte(184), Convert.ToByte(251));
            this.GridView1.Appearance.GroupButton.Options.UseBackColor = true;
            this.GridView1.Appearance.GroupButton.Options.UseBorderColor = true;
            this.GridView1.Appearance.GroupFooter.BackColor = System.Drawing.Color.FromArgb(Convert.ToByte(170), Convert.ToByte(216), Convert.ToByte(254));
            this.GridView1.Appearance.GroupFooter.BorderColor = System.Drawing.Color.FromArgb(Convert.ToByte(170), Convert.ToByte(216), Convert.ToByte(254));
            this.GridView1.Appearance.GroupFooter.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.GroupFooter.Options.UseBackColor = true;
            this.GridView1.Appearance.GroupFooter.Options.UseBorderColor = true;
            this.GridView1.Appearance.GroupFooter.Options.UseForeColor = true;
            this.GridView1.Appearance.GroupPanel.BackColor = System.Drawing.Color.FromArgb(Convert.ToByte(236), Convert.ToByte(246), Convert.ToByte(255));
            this.GridView1.Appearance.GroupPanel.BackColor2 = System.Drawing.Color.White;
            this.GridView1.Appearance.GroupPanel.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.GroupPanel.Options.UseBackColor = true;
            this.GridView1.Appearance.GroupPanel.Options.UseForeColor = true;
            this.GridView1.Appearance.GroupRow.BackColor = System.Drawing.Color.FromArgb(Convert.ToByte(170), Convert.ToByte(216), Convert.ToByte(254));
            this.GridView1.Appearance.GroupRow.BorderColor = System.Drawing.Color.FromArgb(Convert.ToByte(170), Convert.ToByte(216), Convert.ToByte(254));
            this.GridView1.Appearance.GroupRow.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.GroupRow.Options.UseBackColor = true;
            this.GridView1.Appearance.GroupRow.Options.UseBorderColor = true;
            this.GridView1.Appearance.GroupRow.Options.UseForeColor = true;
            this.GridView1.Appearance.HeaderPanel.BackColor = System.Drawing.Color.FromArgb(Convert.ToByte(139), Convert.ToByte(201), Convert.ToByte(254));
            this.GridView1.Appearance.HeaderPanel.BorderColor = System.Drawing.Color.FromArgb(Convert.ToByte(139), Convert.ToByte(201), Convert.ToByte(254));
            this.GridView1.Appearance.HeaderPanel.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.HeaderPanel.Options.UseBackColor = true;
            this.GridView1.Appearance.HeaderPanel.Options.UseBorderColor = true;
            this.GridView1.Appearance.HeaderPanel.Options.UseForeColor = true;
            this.GridView1.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.FromArgb(Convert.ToByte(105), Convert.ToByte(170), Convert.ToByte(225));
            this.GridView1.Appearance.HideSelectionRow.BorderColor = System.Drawing.Color.FromArgb(Convert.ToByte(83), Convert.ToByte(155), Convert.ToByte(215));
            this.GridView1.Appearance.HideSelectionRow.ForeColor = System.Drawing.Color.FromArgb(Convert.ToByte(236), Convert.ToByte(246), Convert.ToByte(255));
            this.GridView1.Appearance.HideSelectionRow.Options.UseBackColor = true;
            this.GridView1.Appearance.HideSelectionRow.Options.UseBorderColor = true;
            this.GridView1.Appearance.HideSelectionRow.Options.UseForeColor = true;
            this.GridView1.Appearance.HorzLine.BackColor = System.Drawing.Color.FromArgb(Convert.ToByte(104), Convert.ToByte(184), Convert.ToByte(251));
            this.GridView1.Appearance.HorzLine.Options.UseBackColor = true;
            this.GridView1.Appearance.OddRow.BackColor = System.Drawing.Color.FromArgb(Convert.ToByte(236), Convert.ToByte(246), Convert.ToByte(255));
            this.GridView1.Appearance.OddRow.BorderColor = System.Drawing.Color.FromArgb(Convert.ToByte(236), Convert.ToByte(246), Convert.ToByte(255));
            this.GridView1.Appearance.OddRow.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.OddRow.Options.UseBackColor = true;
            this.GridView1.Appearance.OddRow.Options.UseBorderColor = true;
            this.GridView1.Appearance.OddRow.Options.UseForeColor = true;
            this.GridView1.Appearance.Preview.Font = new System.Drawing.Font("Verdana", 7.5f);
            this.GridView1.Appearance.Preview.ForeColor = System.Drawing.Color.FromArgb(Convert.ToByte(83), Convert.ToByte(155), Convert.ToByte(215));
            this.GridView1.Appearance.Preview.Options.UseFont = true;
            this.GridView1.Appearance.Preview.Options.UseForeColor = true;
            this.GridView1.Appearance.Row.BackColor = System.Drawing.Color.FromArgb(Convert.ToByte(247), Convert.ToByte(251), Convert.ToByte(255));
            this.GridView1.Appearance.Row.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.Row.Options.UseBackColor = true;
            this.GridView1.Appearance.Row.Options.UseForeColor = true;
            this.GridView1.Appearance.RowSeparator.BackColor = System.Drawing.Color.FromArgb(Convert.ToByte(236), Convert.ToByte(246), Convert.ToByte(255));
            this.GridView1.Appearance.RowSeparator.BackColor2 = System.Drawing.Color.White;
            this.GridView1.Appearance.RowSeparator.Options.UseBackColor = true;
            this.GridView1.Appearance.SelectedRow.BackColor = System.Drawing.Color.FromArgb(Convert.ToByte(83), Convert.ToByte(155), Convert.ToByte(215));
            this.GridView1.Appearance.SelectedRow.ForeColor = System.Drawing.Color.White;
            this.GridView1.Appearance.SelectedRow.Options.UseBackColor = true;
            this.GridView1.Appearance.SelectedRow.Options.UseForeColor = true;
            this.GridView1.Appearance.TopNewRow.BackColor = System.Drawing.Color.White;
            this.GridView1.Appearance.TopNewRow.Options.UseBackColor = true;
            this.GridView1.Appearance.VertLine.BackColor = System.Drawing.Color.FromArgb(Convert.ToByte(104), Convert.ToByte(184), Convert.ToByte(251));
            this.GridView1.Appearance.VertLine.Options.UseBackColor = true;
            this.GridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
				this.GridColumn_StatusDate,
				this.GridColumn_proposal_print_date,
				this.GridColumn_formatted_proposal_status,
				this.GridColumn_formatted_proposal_result_disposition,
				this.GridColumn_formatted_proposal_result_reason,
				this.GridColumn_terms,
				this.GridColumn_percent_balance,
				this.GridColumn_created_by,
				this.GridColumn_date_created,
				this.GridColumn_proposed_date,
				this.GridColumn_proposal_reject_disp,
				this.GridColumn_proposal_reject_reason,
				this.GridColumn_proposal_accepted_by,
				this.GridColumn_proposal_message,
				this.GridColumn_proposal_status,
				this.GridColumn_client_creditor_proposal
			});
            this.GridView1.OptionsBehavior.Editable = false;
            this.GridView1.OptionsView.EnableAppearanceEvenRow = true;
            this.GridView1.OptionsView.EnableAppearanceOddRow = true;
            this.GridView1.OptionsView.ShowGroupPanel = false;
            this.GridView1.OptionsView.ShowIndicator = false;
            this.GridView1.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] { new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.GridColumn_date_created, DevExpress.Data.ColumnSortOrder.Descending) });
            //
            //GridColumn_StatusDate
            //
            this.GridColumn_StatusDate.AppearanceCell.Options.UseTextOptions = true;
            this.GridColumn_StatusDate.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.GridColumn_StatusDate.AppearanceHeader.Options.UseTextOptions = true;
            this.GridColumn_StatusDate.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.GridColumn_StatusDate.Caption = "Status Date";
            this.GridColumn_StatusDate.CustomizationCaption = "Date status changed";
            this.GridColumn_StatusDate.DisplayFormat.FormatString = "d";
            this.GridColumn_StatusDate.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.GridColumn_StatusDate.FieldName = "display_proposal_status_date";
            this.GridColumn_StatusDate.GroupFormat.FormatString = "d";
            this.GridColumn_StatusDate.GroupFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.GridColumn_StatusDate.Name = "GridColumn_StatusDate";
            this.GridColumn_StatusDate.Visible = true;
            this.GridColumn_StatusDate.VisibleIndex = 0;
            this.GridColumn_StatusDate.Width = 156;
            //
            //GridColumn_formatted_proposal_status
            //
            this.GridColumn_formatted_proposal_status.Caption = "Status";
            this.GridColumn_formatted_proposal_status.CustomizationCaption = "Status";
            this.GridColumn_formatted_proposal_status.FieldName = "proposal_status";
            this.GridColumn_formatted_proposal_status.Name = "GridColumn_formatted_proposal_status";
            this.GridColumn_formatted_proposal_status.AppearanceCell.Options.UseTextOptions = true;
            this.GridColumn_formatted_proposal_status.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.GridColumn_formatted_proposal_status.AppearanceHeader.Options.UseTextOptions = true;
            this.GridColumn_formatted_proposal_status.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.GridColumn_formatted_proposal_status.Visible = true;
            this.GridColumn_formatted_proposal_status.VisibleIndex = 2;
            this.GridColumn_formatted_proposal_status.Width = 281;
            //
            //GridColumn_formatted_proposal_result_disposition
            //
            this.GridColumn_formatted_proposal_result_disposition.Caption = "Disposition";
            this.GridColumn_formatted_proposal_result_disposition.CustomizationCaption = "Disposition of Rejected Proposal";
            this.GridColumn_formatted_proposal_result_disposition.FieldName = "proposal_reject_disp";
            this.GridColumn_formatted_proposal_result_disposition.Name = "GridColumn_formatted_proposal_result_disposition";
            //
            //GridColumn_formatted_proposal_result_reason
            //
            this.GridColumn_formatted_proposal_result_reason.Caption = "Reject Reason";
            this.GridColumn_formatted_proposal_result_reason.CustomizationCaption = "Reason why proposal rejected";
            this.GridColumn_formatted_proposal_result_reason.FieldName = "proposal_reject_reason";
            this.GridColumn_formatted_proposal_result_reason.Name = "GridColumn_formatted_proposal_result_reason";
            //
            //GridColumn_terms
            //
            this.GridColumn_terms.Caption = "Terms";
            this.GridColumn_terms.CustomizationCaption = "Terms";
            this.GridColumn_terms.FieldName = "terms";
            this.GridColumn_terms.Name = "GridColumn_terms";
            //
            //GridColumn_percent_balance
            //
            this.GridColumn_percent_balance.Caption = "%Balance";
            this.GridColumn_percent_balance.CustomizationCaption = "Percent of balance";
            this.GridColumn_percent_balance.DisplayFormat.FormatString = "{0:p3}";
            this.GridColumn_percent_balance.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.GridColumn_percent_balance.FieldName = "percent_balance";
            this.GridColumn_percent_balance.GroupFormat.FormatString = "{0:p3}";
            this.GridColumn_percent_balance.GroupFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.GridColumn_percent_balance.Name = "GridColumn_percent_balance";
            //
            //GridColumn_created_by
            //
            this.GridColumn_created_by.Caption = "Counselor";
            this.GridColumn_created_by.CustomizationCaption = "Person who created the proposal";
            this.GridColumn_created_by.FieldName = "created_by";
            this.GridColumn_created_by.Name = "GridColumn_created_by";
            //
            //GridColumn_date_created
            //
            this.GridColumn_date_created.AppearanceCell.Options.UseTextOptions = true;
            this.GridColumn_date_created.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.GridColumn_date_created.AppearanceHeader.Options.UseTextOptions = true;
            this.GridColumn_date_created.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.GridColumn_date_created.Caption = "Date Created";
            this.GridColumn_date_created.CustomizationCaption = "Date proposal created";
            this.GridColumn_date_created.DisplayFormat.FormatString = "d";
            this.GridColumn_date_created.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.GridColumn_date_created.FieldName = "date_created";
            this.GridColumn_date_created.GroupFormat.FormatString = "d";
            this.GridColumn_date_created.GroupFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.GridColumn_date_created.Name = "GridColumn_date_created";
            this.GridColumn_date_created.SortOrder = DevExpress.Data.ColumnSortOrder.Descending;
            this.GridColumn_date_created.SortMode = DevExpress.XtraGrid.ColumnSortMode.Value;
            this.GridColumn_date_created.SortIndex = 0;
            //
            //GridColumn_proposed_date
            //
            this.GridColumn_proposed_date.AppearanceCell.Options.UseTextOptions = true;
            this.GridColumn_proposed_date.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.GridColumn_proposed_date.AppearanceHeader.Options.UseTextOptions = true;
            this.GridColumn_proposed_date.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.GridColumn_proposed_date.Caption = "Start Date";
            this.GridColumn_proposed_date.CustomizationCaption = "Proposed Start Date";
            this.GridColumn_proposed_date.DisplayFormat.FormatString = "d";
            this.GridColumn_proposed_date.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.GridColumn_proposed_date.FieldName = "proposed_start_date";
            this.GridColumn_proposed_date.GroupFormat.FormatString = "d";
            this.GridColumn_proposed_date.GroupFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.GridColumn_proposed_date.Name = "GridColumn_proposed_date";
            //
            //GridColumn_proposal_reject_disp
            //
            this.GridColumn_proposal_reject_disp.AppearanceCell.Options.UseTextOptions = true;
            this.GridColumn_proposal_reject_disp.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.GridColumn_proposal_reject_disp.AppearanceHeader.Options.UseTextOptions = true;
            this.GridColumn_proposal_reject_disp.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.GridColumn_proposal_reject_disp.Caption = "ID Disposition";
            this.GridColumn_proposal_reject_disp.CustomizationCaption = "ID for reject disposition";
            this.GridColumn_proposal_reject_disp.DisplayFormat.FormatString = "f0";
            this.GridColumn_proposal_reject_disp.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.GridColumn_proposal_reject_disp.FieldName = "proposal_reject_disp";
            this.GridColumn_proposal_reject_disp.GroupFormat.FormatString = "f0";
            this.GridColumn_proposal_reject_disp.GroupFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.GridColumn_proposal_reject_disp.Name = "GridColumn_proposal_reject_disp";
            //
            //GridColumn_proposal_reject_reason
            //
            this.GridColumn_proposal_reject_reason.AppearanceCell.Options.UseTextOptions = true;
            this.GridColumn_proposal_reject_reason.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.GridColumn_proposal_reject_reason.AppearanceHeader.Options.UseTextOptions = true;
            this.GridColumn_proposal_reject_reason.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.GridColumn_proposal_reject_reason.Caption = "ID Reason";
            this.GridColumn_proposal_reject_reason.CustomizationCaption = "ID associated with reject reason";
            this.GridColumn_proposal_reject_reason.DisplayFormat.FormatString = "f0";
            this.GridColumn_proposal_reject_reason.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.GridColumn_proposal_reject_reason.FieldName = "proposal_reject_reason";
            this.GridColumn_proposal_reject_reason.GroupFormat.FormatString = "f0";
            this.GridColumn_proposal_reject_reason.GroupFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.GridColumn_proposal_reject_reason.Name = "GridColumn_proposal_reject_reason";
            //
            //GridColumn_proposal_accepted_by
            //
            this.GridColumn_proposal_accepted_by.Caption = "Accepted By";
            this.GridColumn_proposal_accepted_by.CustomizationCaption = "Who accepted the proposal";
            this.GridColumn_proposal_accepted_by.FieldName = "proposal_accepted_by";
            this.GridColumn_proposal_accepted_by.Name = "GridColumn_proposal_accepted_by";
            //
            //GridColumn_proposal_message
            //
            this.GridColumn_proposal_message.Caption = "Message";
            this.GridColumn_proposal_message.CustomizationCaption = "Message text";
            this.GridColumn_proposal_message.FieldName = "proposal_note";
            this.GridColumn_proposal_message.Name = "GridColumn_proposal_message";
            //
            //GridColumn_proposal_print_date
            //
            this.GridColumn_proposal_print_date.AppearanceCell.Options.UseTextOptions = true;
            this.GridColumn_proposal_print_date.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.GridColumn_proposal_print_date.AppearanceHeader.Options.UseTextOptions = true;
            this.GridColumn_proposal_print_date.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.GridColumn_proposal_print_date.Caption = "Letter Date";
            this.GridColumn_proposal_print_date.CustomizationCaption = "Date proposal was printed";
            this.GridColumn_proposal_print_date.DisplayFormat.FormatString = "{0:d}";
            this.GridColumn_proposal_print_date.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.GridColumn_proposal_print_date.FieldName = "letter_date";
            this.GridColumn_proposal_print_date.GroupFormat.FormatString = "{0:d}";
            this.GridColumn_proposal_print_date.GroupFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.GridColumn_proposal_print_date.Name = "GridColumn_proposal_print_date";
            this.GridColumn_proposal_print_date.Visible = true;
            this.GridColumn_proposal_print_date.VisibleIndex = 1;
            this.GridColumn_proposal_print_date.Width = 135;
            //
            //GridColumn_proposal_status
            //
            this.GridColumn_proposal_status.AppearanceCell.Options.UseTextOptions = true;
            this.GridColumn_proposal_status.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.GridColumn_proposal_status.AppearanceHeader.Options.UseTextOptions = true;
            this.GridColumn_proposal_status.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.GridColumn_proposal_status.Caption = "ID Status";
            this.GridColumn_proposal_status.CustomizationCaption = "ID for status";
            this.GridColumn_proposal_status.DisplayFormat.FormatString = "f0";
            this.GridColumn_proposal_status.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.GridColumn_proposal_status.FieldName = "proposal_status";
            this.GridColumn_proposal_status.GroupFormat.FormatString = "f0";
            this.GridColumn_proposal_status.GroupFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.GridColumn_proposal_status.Name = "GridColumn_proposal_status";
            //
            //GridColumn_client_creditor_proposal
            //
            this.GridColumn_client_creditor_proposal.AppearanceCell.Options.UseTextOptions = true;
            this.GridColumn_client_creditor_proposal.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.GridColumn_client_creditor_proposal.AppearanceHeader.Options.UseTextOptions = true;
            this.GridColumn_client_creditor_proposal.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.GridColumn_client_creditor_proposal.Caption = "Record #";
            this.GridColumn_client_creditor_proposal.CustomizationCaption = "ID";
            this.GridColumn_client_creditor_proposal.DisplayFormat.FormatString = "f0";
            this.GridColumn_client_creditor_proposal.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.GridColumn_client_creditor_proposal.FieldName = "client_creditor_proposal";
            this.GridColumn_client_creditor_proposal.GroupFormat.FormatString = "f0";
            this.GridColumn_client_creditor_proposal.GroupFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.GridColumn_client_creditor_proposal.Name = "GridColumn_client_creditor_proposal";
            //
            this.GridColumn_formatted_proposal_status.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.GridColumn_formatted_proposal_status.GroupFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.GridColumn_formatted_proposal_status.SortMode = DevExpress.XtraGrid.ColumnSortMode.DisplayText;
            this.GridColumn_formatted_proposal_status.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.GridColumn_formatted_proposal_status.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.GridColumn_formatted_proposal_result_reason.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.GridColumn_formatted_proposal_result_reason.GroupFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.GridColumn_formatted_proposal_result_reason.SortMode = DevExpress.XtraGrid.ColumnSortMode.DisplayText;
            this.GridColumn_formatted_proposal_result_reason.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.GridColumn_formatted_proposal_result_reason.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.GridColumn_formatted_proposal_result_disposition.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.GridColumn_formatted_proposal_result_disposition.GroupFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.GridColumn_formatted_proposal_result_disposition.SortMode = DevExpress.XtraGrid.ColumnSortMode.DisplayText;
            this.GridColumn_formatted_proposal_result_disposition.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.GridColumn_formatted_proposal_result_disposition.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            //
            //ProposalsList
            //
            this.Name = "ProposalsList";
            ((System.ComponentModel.ISupportInitialize)this.GridControl1).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.GridView1).EndInit();
            this.ResumeLayout(false);
        }

        #endregion " Windows Form Designer generated code "

        /// <summary>
        /// Read the data from the database when the form is loaded
        /// </summary>
        public override void ReadForm(DebtPlus.Interfaces.Debt.IDebtRecord CurrentDebtRecord)
        {
            // Save the debt record for later references
            this.currentDebtRecord = CurrentDebtRecord;

            // Set the proposal reject reason
            GridColumn_formatted_proposal_result_disposition.GroupFormat.Format   = new format_proposal_reject_disp();
            GridColumn_formatted_proposal_result_disposition.DisplayFormat.Format = new format_proposal_reject_disp();
            GridColumn_formatted_proposal_status.DisplayFormat.Format             = new format_proposal_status();
            GridColumn_formatted_proposal_status.GroupFormat.Format               = new format_proposal_status();
            GridColumn_formatted_proposal_result_reason.DisplayFormat.Format      = new format_proposal_reject_reason();
            GridColumn_formatted_proposal_result_reason.GroupFormat.Format        = new format_proposal_reject_reason();

            // Setup the database connection
            clientCreditorProposalBC = new BusinessContext();

            // Force the loading of the notes associated with the proposals.
            // Force the loading of the proposal_batch information as well since we use it for letter_date.
            System.Data.Linq.DataLoadOptions dl = new System.Data.Linq.DataLoadOptions();
            dl.LoadWith<DebtPlus.LINQ.client_creditor_proposal>(s => s.debt_notes);
            dl.LoadWith<DebtPlus.LINQ.client_creditor_proposal>(s => s.proposal_batch_id1);
            clientCreditorProposalBC.LoadOptions = dl;
            clientCreditorProposalBC.DeferredLoadingEnabled = false;

            colProposals = clientCreditorProposalBC.client_creditor_proposals.Where(pr => pr.client_creditor == currentDebtRecord.client_creditor).ToList();
            GridControl1.DataSource = colProposals;
            GridControl1.RefreshDataSource();
        }

        /// <summary>
        /// Is the item valid to be created?
        /// </summary>
        protected override bool OkToCreate()
        {
            foreach (DebtPlus.LINQ.client_creditor_proposal item in colProposals)
            {
                if (item.proposal_status <= 1)
                {
                    return false;
                }
            }

            return true;
        }

        /// <summary>
        /// Is the item valid to be deleted?
        /// </summary>
        protected override bool OkToDelete(object obj)
        {
            DebtPlus.LINQ.client_creditor_proposal record = obj as DebtPlus.LINQ.client_creditor_proposal;

            // No record == NO DELETE
            if (record == null)
            {
                return false;
            }

            // If it was just created then we can delete it
            if (record.proposal_status == 0)
            {
                return true;
            }

            // If the proposal is marked rejected or accepted then it is too late to delete it.
            if (record.proposal_status >= 2)
            {
                return false;
            }

            // If the proposal is in a transmitted batch (one sent to the creditor) then it is too late to delete it.
            DebtPlus.LINQ.proposal_batch_id pb = record.proposal_batch_id1;
            if (pb != null && (pb.date_transmitted.HasValue || pb.date_closed.HasValue))
            {
                return false;
            }

            return true;
        }

        /// <summary>
        /// Process the create operation
        /// </summary>
        protected override void OnCreate()
        {
            var record                        = DebtPlus.LINQ.Factory.Manufacture_client_creditor_proposal(currentDebtRecord.client_creditor, 1);
            record.proposed_date              = System.DateTime.Now;
            record.terms                      = currentDebtRecord.terms;
            record.percent_balance            = currentDebtRecord.percent_balance;
            record.rpps_client_type_indicator = DebtPlus.Utils.Nulls.DStr(currentDebtRecord.rpps_client_type_indicator, " ");

            if (EditRecord(record) != DialogResult.OK)
            {
                return;
            }

            clientCreditorProposalBC.client_creditor_proposals.InsertOnSubmit(record);
            clientCreditorProposalBC.SubmitChanges();

            colProposals.Add(record);
            GridView1.RefreshData();
            GridControl1.RefreshDataSource();
        }

        /// <summary>
        /// Do the delete operation on the current row
        /// </summary>
        protected override void OnDelete(object obj)
        {
            DebtPlus.LINQ.client_creditor_proposal record = obj as DebtPlus.LINQ.client_creditor_proposal;
            if (record == null)
            {
                return;
            }

            if (!OkToDelete(record))
            {
                DebtPlus.Data.Forms.MessageBox.Show("The proposal may not be deleted at this time.", "Sorry, delete is not possible");
                return;
            }

            // If OK to delete then delete the proposal
            if (DebtPlus.Data.Prompts.RequestConfirmation_Delete() != DialogResult.Yes)
            {
                return;
            }

            clientCreditorProposalBC.client_creditor_proposals.DeleteOnSubmit(record);
            clientCreditorProposalBC.SubmitChanges();

            // Remove the item from the collection
            colProposals.Remove(record);
            GridView1.RefreshData();
            GridControl1.RefreshDataSource();
        }

        /// <summary>
        /// Edit the current proposal
        /// </summary>
        protected override void OnEdit(object obj)
        {
            DebtPlus.LINQ.client_creditor_proposal record = obj as DebtPlus.LINQ.client_creditor_proposal;
            if (record == null)
            {
                return;
            }

            if (EditRecord(record) != DialogResult.OK)
            {
                return;
            }

            clientCreditorProposalBC.SubmitChanges();
            GridView1.RefreshData();
            GridControl1.RefreshDataSource();
        }

        /// <summary>
        /// Hook into the popup for the context menus to enable print
        /// </summary>
        protected override void PopupMenu_Items_Popup(object sender, EventArgs e)
        {
            base.PopupMenu_Items_Popup(sender, e);

            // A proposal may not be printed until the creditor is coded.
            BarSubItem_Print.Enabled = DebtPlus.Configuration.Config.ReprintProposals && (ControlRow >= 0) && DebtPlus.Utils.Nulls.DStr(currentDebtRecord.Creditor) != string.Empty;
        }

        private void BarButtonItem_Print_Full_Click(object sender, EventArgs e)
        {
            if (ControlRow < 0)
            {
                return;
            }

            DebtPlus.LINQ.client_creditor_proposal record = GridView1.GetRow(ControlRow) as DebtPlus.LINQ.client_creditor_proposal;
            if (record == null)
            {
                return;
            }

            // Validate that the proposal is correct
            if (record.Id <= 0)
            {
                return;
            }

            ProposalsFullReport rpt = new ProposalsFullReport();
            rpt.Parameter_Proposal = record.Id;
            if (rpt.RequestReportParameters() == DialogResult.OK)
            {
                rpt.RunReportInSeparateThread();
            }
        }

        private void BarButtonItem_Print_Standard_Click(object sender, EventArgs e)
        {
            if (ControlRow < 0)
            {
                return;
            }

            DebtPlus.LINQ.client_creditor_proposal record = GridView1.GetRow(ControlRow) as DebtPlus.LINQ.client_creditor_proposal;
            if (record == null)
            {
                return;
            }

            // Validate that the proposal is correct
            if (record.Id <= 0)
            {
                return;
            }

            ProposalsStandardReport rpt = new ProposalsStandardReport();
            rpt.Parameter_Proposal = record.Id;
            rpt.RunReportInSeparateThread();
        }

        private System.Windows.Forms.DialogResult EditRecord(DebtPlus.LINQ.client_creditor_proposal record)
        {
            using (Form_ProposalUpdate frm = new Form_ProposalUpdate(record))
            {
                // Execute the edit operation now
                System.Windows.Forms.DialogResult Answer = frm.ShowDialog();
                if (Answer != DialogResult.OK)
                {
                    return Answer;
                }

                // Include the note text if there is any
                System.Text.StringBuilder InputString = new System.Text.StringBuilder(frm.proposal_note.Text.Trim());
                InputString.Replace("\r\n", " ");
                InputString.Replace("\r", " ");
                InputString.Replace("\n", " ");

                // Go through the file one character at a time and toss any garbage
                for (Int32 CharacterIndex = InputString.Length - 1; CharacterIndex >= 0; CharacterIndex += -1)
                {
                    char CharacterValue = InputString[CharacterIndex];
                    if (!char.IsLetterOrDigit(CharacterValue) && CharacterValue != ' ' && !char.IsPunctuation(CharacterValue))
                    {
                        InputString.Remove(CharacterIndex, 1);
                    }
                }

                // Replace the text buffer
                if (InputString.Length > 0)
                {
                    // If there is no note but we need one, then allocate a new note
                    if (record.debt_notes == null)
                    {
                        record.debt_notes      = DebtPlus.LINQ.Factory.Manufacture_debt_note(currentDebtRecord.ClientId, currentDebtRecord.client_creditor);
                        record.debt_notes.type = DebtPlus.LINQ.debt_note.Type_ProposalNote;
                        clientCreditorProposalBC.debt_notes.InsertOnSubmit(record.debt_notes);
                    }
                    record.debt_notes.note_text = InputString.ToString();
                }
                else
                {
                    // Delete the current note from the system if there is no not associated with the proposal
                    if (record.debt_notes != null)
                    {
                        clientCreditorProposalBC.debt_notes.DeleteOnSubmit(record.debt_notes);
                        record.debt_notes = null;
                    }
                }

                // The answer is the result of the dialog edit. Which, at this point is "OK".
                return Answer;
            }
        }

#region " format_proposal_status "
        /// <summary>
        /// Print the standard proposal report for this proposal
        /// </summary>
        /// <summary>
        /// Print the full disclosure report for this proposal
        /// </summary>
        private class format_proposal_status : IFormatProvider, ICustomFormatter
        {
            public format_proposal_status()
            {
            }

            public string Format(string format1, object arg, IFormatProvider formatProvider)
            {
                return DebtPlus.UI.Debt.Update.Helpers.getProposalStatusTypeDescription(DebtPlus.Utils.Nulls.v_Int32(arg));
            }

            public object GetFormat(Type formatType)
            {
                return this;
            }
        }
#endregion " format_proposal_status "

        #region " format_proposal_reject_reason "

        private class format_proposal_reject_reason : IFormatProvider, ICustomFormatter
        {
            public format_proposal_reject_reason()
            {
            }

            public string Format(string format1, object arg, IFormatProvider formatProvider)
            {
                return DebtPlus.UI.Debt.Update.Helpers.getproposal_result_reasonDescription(DebtPlus.Utils.Nulls.v_Int32(arg));
            }

            public object GetFormat(Type formatType)
            {
                return this;
            }
        }

        #endregion " format_proposal_reject_reason "

        #region " format_proposal_reject_disp "

        private class format_proposal_reject_disp : IFormatProvider, ICustomFormatter
        {
            public format_proposal_reject_disp()
            {
            }

            public string Format(string format1, object arg, IFormatProvider formatProvider)
            {
                return DebtPlus.UI.Debt.Update.Helpers.getproposal_result_dispositionDescription(DebtPlus.Utils.Nulls.v_Int32(arg));
            }

            public object GetFormat(Type formatType)
            {
                return this;
            }
        }

        #endregion " format_proposal_reject_disp "
    }
}

namespace DebtPlus.UI.Debt.Update
{
    internal static class Helpers
    {
        public static string getProposalStatusTypeDescription(System.Nullable<Int32> Key)
        {
            if (Key.HasValue)
            {
                var q = DebtPlus.LINQ.Cache.ProposalStatusType.getList().Find(s => s.Id == Key.Value);
                if (q != null)
                {
                    return q.description;
                }
            }
            return string.Empty;
        }

        public static string getproposal_result_reasonDescription(System.Nullable<Int32> Key)
        {
            if (Key.HasValue)
            {
                var q = DebtPlus.LINQ.Cache.proposal_result_reason.getList().Find(s => s.Id == Key.Value);
                if (q != null)
                {
                    return q.description;
                }
            }
            return string.Empty;
        }

        public static string getproposal_result_dispositionDescription(System.Nullable<Int32> Key)
        {
            if (Key.HasValue)
            {
                var q = DebtPlus.LINQ.Cache.proposal_result_disposition.getList().Find(s => s.Id == Key.Value);
                if (q != null)
                {
                    return q.description;
                }
            }
            return string.Empty;
        }
    }
}
