#region "Copyright 2000-2012 DebtPlus, L.L.C."

//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}

#endregion "Copyright 2000-2012 DebtPlus, L.L.C."

using System;
using System.Data;
using System.Data.SqlClient;
using System.Windows.Forms;
using DebtPlus.UI.Debt.Update.forms;
using DevExpress.XtraEditors.Controls;

namespace DebtPlus.UI.Debt.Update.controls
{
    internal partial class BalanceInformation : ControlBaseDebt
    {
        private bool BalanceWasChanged = false;

        // Current debt record being edited
        private DebtPlus.Interfaces.Debt.IDebtRecord currentDebtRecord;

        private decimal PreviousBalance               = 0m;
        private decimal PreviousOrigBalance           = 0m;
        private decimal PreviousOrigBalanceAdjustment = 0m;
        private decimal PreviousTotalInterest         = 0m;
        private decimal PreviousTotalPayments         = 0m;

        // TRUE if the original balance should be changed. FALSE to change the adjustment figure.
        private bool UpdateOriginalBalance;

        /// <summary>
        /// Read the information and display it on the control
        /// </summary>
        public BalanceInformation() : base()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Event raised when the balance information is changed in the database
        /// </summary>
        public event EventHandler BalanceChanged;

        /// <summary>
        /// Event raised when the balance figure is being changed.
        /// </summary>
        public event DevExpress.XtraEditors.Controls.ChangingEventHandler BalanceChanging;

        /// <summary>
        /// Current balance figure
        /// </summary>
        public decimal CurrentBalance
        {
            get
            {
                decimal CurrentBalanceAmount = default(decimal);
                if (current_balance.EditValue != null && !object.ReferenceEquals(current_balance.EditValue, DBNull.Value))
                {
                    CurrentBalanceAmount = Convert.ToDecimal(current_balance.EditValue);
                }
                else
                {
                    CurrentBalanceAmount = 0m;
                }
                return CurrentBalanceAmount;
            }
        }

        /// <summary>
        /// Displayed debt balance
        /// </summary>
        public decimal DisplayedBalance
        {
            get { return PreviousOrigBalance + PreviousOrigBalanceAdjustment + PreviousTotalInterest - PreviousTotalPayments; }
        }

        /// <summary>
        /// Read the information for the current debt balance
        /// </summary>
        /// <param name="CurrentDebtRecord">Pointer to the current debt record</param>
        public override void ReadForm(DebtPlus.Interfaces.Debt.IDebtRecord CurrentDebtRecord)
        {
            this.currentDebtRecord = CurrentDebtRecord;
            UpdateDisplay();

            if (verified_balance())
            {
                SimpleButton_Verified.Text = "Verified Balance:";
            }
            else
            {
                SimpleButton_Verified.Text = "Non-Verified Balance:";
            }
        }

        /// <summary>
        /// Raise the BalanceChanged event
        /// </summary>
        protected virtual void OnBalanceChanged(EventArgs e)
        {
            RaiseBalanceChanged(e);
        }

        /// <summary>
        /// Raise the BalanceChanging event
        /// </summary>
        protected virtual void OnBalanceChanging(DevExpress.XtraEditors.Controls.ChangingEventArgs e)
        {
            RaiseBalanceChanging(e);
        }

        /// <summary>
        /// Raise the BalanceChanged event
        /// </summary>
        protected void RaiseBalanceChanged(EventArgs e)
        {
            var evt = BalanceChanged;
            if (evt != null)
            {
                evt(this, e);
            }
        }

        /// <summary>
        /// Raise the BalanceChanging event
        /// </summary>
        protected void RaiseBalanceChanging(DevExpress.XtraEditors.Controls.ChangingEventArgs e)
        {
            var evt = BalanceChanging;
            if (evt != null)
            {
                evt(this, e);
            }
        }

        /// <summary>
        /// Process the validation event on the balance figure
        /// </summary>
        private void current_balance_Validated(object sender, EventArgs e)
        {
            // Indicate that the balance information was changed
            if (!BalanceWasChanged)
            {
                return;
            }

            UnRegisterHandlers();
            try
            {
                // Indicate that the balance is no longer verified
                SimpleButton_Verified.Text = "Changed Balance:";
                BalanceWasChanged = false;

                // Force the balance update to occur immediately. This keeps things simple. Change the balance here and it is changed there!
                using (SqlConnection cn = new SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString))
                {
                    cn.Open();
                    using (SqlCommand cmd = new SqlCommand("xpr_debt_balance", cn))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Add("@client_creditor", SqlDbType.Int).Value = currentDebtRecord.client_creditor;
                        cmd.Parameters.Add("@value", SqlDbType.Decimal).Value = CurrentBalance;

                        // This procedure updates the balance to the value and returns the balance components as a result set.
                        // We need the components to the balance, not the balance figure to properly display the information.
                        using (System.Data.SqlClient.SqlDataReader rd = cmd.ExecuteReader(CommandBehavior.SingleRow))
                        {
                            // Update the balance verification status if desired as well
                            if (PreviousBalance != 0m)
                            {
                                DoBalanceVerification();
                            }

                            if (rd.Read())
                            {
                                for (Int32 indx = rd.FieldCount - 1; indx >= 0; indx += -1)
                                {
                                    if (!rd.IsDBNull(indx))
                                    {
                                        switch (rd.GetName(indx).ToLower())
                                        {
                                            case "orig_balance":
                                                currentDebtRecord.orig_balance = Convert.ToDecimal(rd.GetValue(indx));
                                                break;

                                            case "orig_balance_adjustment":
                                                currentDebtRecord.orig_balance_adjustment = Convert.ToDecimal(rd.GetValue(indx));
                                                break;

                                            case "total_interest":
                                                currentDebtRecord.total_interest = Convert.ToDecimal(rd.GetValue(indx));
                                                break;

                                            case "total_payments":
                                                currentDebtRecord.total_payments = Convert.ToDecimal(rd.GetValue(indx));
                                                break;

                                            default:
                                                break;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }

                // Raise the balance update event and reset the flag for the next time.
                OnBalanceChanged(EventArgs.Empty);
            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error updating debt balance");
            }
            finally
            {
                RegisterHandlers();
            }

            // Adjust the values displayed on the control appropriately
            UpdateDisplay();
        }

        /// <summary>
        /// Trip the BALANCE CHANGED event when we have validated the form
        /// </summary>
        /// <summary>
        /// Negative balances are not allowed
        /// </summary>
        private void current_balance_Validating(object sender, System.ComponentModel.CancelEventArgs e)
        {
            decimal NewBalance = DebtPlus.Utils.Nulls.DDec(current_balance.EditValue);

            if (NewBalance < 0m)
            {
                e.Cancel = true;
                current_balance.ErrorText = "Balance may not be negative";
                return;
            }

            if (currentDebtRecord.ccl_zero_balance && NewBalance > 0m)
            {
                e.Cancel = true;
                current_balance.ErrorText = "Creditor does not allow balances";
                return;
            }

            current_balance.ErrorText = string.Empty;
        }

        /// <summary>
        /// Process a change in the balance figure. The person attempted to change the balance amount.
        /// </summary>
        private void current_balance_ValueChanged(object sender, EventArgs e)
        {
            // Find the current balance information. The previous balance information is in "globals"
            if (CurrentBalance != DisplayedBalance)
            {
                DevExpress.XtraEditors.Controls.ChangingEventArgs ChangingEventArgs = new DevExpress.XtraEditors.Controls.ChangingEventArgs(PreviousBalance, CurrentBalance);
                OnBalanceChanging(ChangingEventArgs);
                if (!ChangingEventArgs.Cancel)
                {
                    BalanceWasChanged = true;
                }
            }
        }

        /// <summary>
        /// Request the verification status for the balance
        /// </summary>
        private void DoBalanceVerification()
        {
            // Ask if the balance should be verified
            using (var frm = new Form_BalanceVerified(currentDebtRecord))
            {
                switch (frm.ShowDialog())
                {
                    case DialogResult.Yes:
                        SimpleButton_Verified.Text = "Verified Balance:";
                        break;

                    case DialogResult.No:
                        SimpleButton_Verified.Text = "Non-Verified Balance:";
                        break;

                    default:
                        break;
                }
            }
        }

        /// <summary>
        /// Register the event handlers
        /// </summary>
        private void RegisterHandlers()
        {
            current_balance.Validated        += current_balance_Validated;
            current_balance.Validating       += current_balance_Validating;
            SimpleButton_Verified.Click      += SimpleButton_Verified_Click;
            current_balance.EditValueChanged += current_balance_ValueChanged;
        }

        /// <summary>
        /// Update the balance verification status
        /// </summary>
        private void SimpleButton_Verified_Click(object sender, EventArgs e)
        {
            DoBalanceVerification();
        }

        /// <summary>
        /// Remove the event handler registrations
        /// </summary>
        private void UnRegisterHandlers()
        {
            current_balance.Validated        -= current_balance_Validated;
            current_balance.Validating       -= current_balance_Validating;
            SimpleButton_Verified.Click      -= SimpleButton_Verified_Click;
            current_balance.EditValueChanged -= current_balance_ValueChanged;
        }

        private void UpdateDisplay()
        {
            UnRegisterHandlers();

            try
            {
                // Compute the values for the display
                PreviousOrigBalance           = DebtPlus.Utils.Nulls.DDec(currentDebtRecord.orig_balance);
                PreviousOrigBalanceAdjustment = DebtPlus.Utils.Nulls.DDec(currentDebtRecord.orig_balance_adjustment);
                PreviousTotalInterest         = DebtPlus.Utils.Nulls.DDec(currentDebtRecord.total_interest);
                PreviousTotalPayments         = DebtPlus.Utils.Nulls.DDec(currentDebtRecord.total_payments);
                PreviousBalance               = DisplayedBalance;

                // Should we update the original balance or the adjustment figure?
                UpdateOriginalBalance = (PreviousOrigBalanceAdjustment == 0m) && (PreviousTotalInterest == 0m) && (PreviousTotalPayments == 0m);

                // Process the information if we have pointers to the data
                orig_balance.Text            = string.Format("{0:c}", PreviousOrigBalance);
                orig_balance_adjustment.Text = string.Format("{0:c}", PreviousOrigBalanceAdjustment);
                total_interest.Text          = string.Format("{0:c}", PreviousTotalInterest);
                total_payments.Text          = string.Format("{0:c}", PreviousTotalPayments);

                // The current balance is a combination of the items above
                current_balance.EditValue = PreviousBalance;
                BalanceWasChanged         = false;
            }
            finally
            {
                RegisterHandlers();
            }
        }

        /// <summary>
        /// Do special logic when the balance information changes
        /// </summary>
        /// <summary>
        /// Is the balance verified?
        /// </summary>
        private bool verified_balance()
        {
            // We need a person to have verified the balance
            if (string.IsNullOrWhiteSpace(DebtPlus.Utils.Nulls.v_String(currentDebtRecord.balance_verify_by)))
            {
                return false;
            }

            // We need a date when it was verified
            System.Nullable<DateTime> verifiedDate = DebtPlus.Utils.Nulls.v_DateTime(currentDebtRecord.balance_verify_date);
            if (!verifiedDate.HasValue || verifiedDate.Value.Year <= 1960)
            {
                return false;
            }

            return true;
        }

        #region " Windows Form Designer generated code "

        //NOTE: The following procedure is required by the Windows Form Designer
        //It can be modified using the Windows Form Designer.
        //Do not modify it using the code editor.
        private System.ComponentModel.IContainer components = null;

        // Form overrides dispose to clean up the component list.
        protected override void Dispose(bool disposing)
        {
            try
            {
                if (disposing)
                {
                    if (components != null) components.Dispose();
                }
            }
            finally
            {
                base.Dispose(disposing);
            }
        }

        //Required by the Windows Form Designer
        [System.Diagnostics.DebuggerStepThrough()]
        private void InitializeComponent()
        {
            this.GroupControl2 = new DevExpress.XtraEditors.GroupControl();
            this.SimpleButton_Verified = new DevExpress.XtraEditors.SimpleButton();
            this.LabelControl22 = new DevExpress.XtraEditors.LabelControl();
            this.LabelControl30 = new DevExpress.XtraEditors.LabelControl();
            this.LabelControl31 = new DevExpress.XtraEditors.LabelControl();
            this.LabelControl32 = new DevExpress.XtraEditors.LabelControl();
            this.LabelControl33 = new DevExpress.XtraEditors.LabelControl();
            this.LabelControl34 = new DevExpress.XtraEditors.LabelControl();
            this.current_balance = new DevExpress.XtraEditors.CalcEdit();
            this.total_payments = new DevExpress.XtraEditors.LabelControl();
            this.total_interest = new DevExpress.XtraEditors.LabelControl();
            this.orig_balance_adjustment = new DevExpress.XtraEditors.LabelControl();
            this.orig_balance = new DevExpress.XtraEditors.LabelControl();
            this.LabelControl24 = new DevExpress.XtraEditors.LabelControl();
            this.LabelControl23 = new DevExpress.XtraEditors.LabelControl();
            this.LabelControl21 = new DevExpress.XtraEditors.LabelControl();
            ((System.ComponentModel.ISupportInitialize)this.GroupControl2).BeginInit();
            this.GroupControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)this.current_balance.Properties).BeginInit();
            this.SuspendLayout();
            //
            //GroupControl2
            //
            this.GroupControl2.Controls.Add(this.SimpleButton_Verified);
            this.GroupControl2.Controls.Add(this.LabelControl22);
            this.GroupControl2.Controls.Add(this.LabelControl30);
            this.GroupControl2.Controls.Add(this.LabelControl31);
            this.GroupControl2.Controls.Add(this.LabelControl32);
            this.GroupControl2.Controls.Add(this.LabelControl33);
            this.GroupControl2.Controls.Add(this.LabelControl34);
            this.GroupControl2.Controls.Add(this.current_balance);
            this.GroupControl2.Controls.Add(this.total_payments);
            this.GroupControl2.Controls.Add(this.total_interest);
            this.GroupControl2.Controls.Add(this.orig_balance_adjustment);
            this.GroupControl2.Controls.Add(this.orig_balance);
            this.GroupControl2.Controls.Add(this.LabelControl24);
            this.GroupControl2.Controls.Add(this.LabelControl23);
            this.GroupControl2.Controls.Add(this.LabelControl21);
            this.GroupControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.GroupControl2.Location = new System.Drawing.Point(0, 0);
            this.GroupControl2.Name = "GroupControl2";
            this.GroupControl2.Size = new System.Drawing.Size(256, 104);
            this.GroupControl2.TabIndex = 0;
            this.GroupControl2.Text = "Balance Information";
            //
            //SimpleButton_Verified
            //
            this.SimpleButton_Verified.Appearance.Options.UseTextOptions = true;
            this.SimpleButton_Verified.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.SimpleButton_Verified.Location = new System.Drawing.Point(5, 80);
            this.SimpleButton_Verified.Name = "SimpleButton_Verified";
            this.SimpleButton_Verified.Size = new System.Drawing.Size(120, 20);
            this.SimpleButton_Verified.TabIndex = 14;
            this.SimpleButton_Verified.Text = "Non-Verified Balance:";
            //
            //LabelControl22
            //
            this.LabelControl22.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.Horizontal;
            this.LabelControl22.Location = new System.Drawing.Point(8, 39);
            this.LabelControl22.Name = "LabelControl22";
            this.LabelControl22.Size = new System.Drawing.Size(117, 13);
            this.LabelControl22.TabIndex = 3;
            this.LabelControl22.Text = "Adjustments to Orig Bal:";
            this.LabelControl22.UseMnemonic = false;
            //
            //LabelControl30
            //
            this.LabelControl30.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25f, System.Drawing.FontStyle.Bold);
            this.LabelControl30.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.LabelControl30.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.LabelControl30.Location = new System.Drawing.Point(240, 84);
            this.LabelControl30.Name = "LabelControl30";
            this.LabelControl30.Size = new System.Drawing.Size(9, 13);
            this.LabelControl30.TabIndex = 13;
            this.LabelControl30.Text = "=";
            this.LabelControl30.UseMnemonic = false;
            //
            //LabelControl31
            //
            this.LabelControl31.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25f, System.Drawing.FontStyle.Bold);
            this.LabelControl31.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.LabelControl31.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.LabelControl31.Location = new System.Drawing.Point(240, 67);
            this.LabelControl31.Name = "LabelControl31";
            this.LabelControl31.Size = new System.Drawing.Size(9, 13);
            this.LabelControl31.TabIndex = 11;
            this.LabelControl31.Text = "-";
            this.LabelControl31.UseMnemonic = false;
            //
            //LabelControl32
            //
            this.LabelControl32.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25f, System.Drawing.FontStyle.Bold);
            this.LabelControl32.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.LabelControl32.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.LabelControl32.Location = new System.Drawing.Point(240, 53);
            this.LabelControl32.Name = "LabelControl32";
            this.LabelControl32.Size = new System.Drawing.Size(9, 13);
            this.LabelControl32.TabIndex = 8;
            this.LabelControl32.Text = "+";
            this.LabelControl32.UseMnemonic = false;
            //
            //LabelControl33
            //
            this.LabelControl33.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25f, System.Drawing.FontStyle.Bold);
            this.LabelControl33.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.LabelControl33.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.LabelControl33.Location = new System.Drawing.Point(240, 39);
            this.LabelControl33.Name = "LabelControl33";
            this.LabelControl33.Size = new System.Drawing.Size(9, 13);
            this.LabelControl33.TabIndex = 5;
            this.LabelControl33.Text = "+";
            this.LabelControl33.UseMnemonic = false;
            //
            //LabelControl34
            //
            this.LabelControl34.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25f, System.Drawing.FontStyle.Bold);
            this.LabelControl34.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.LabelControl34.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.LabelControl34.Location = new System.Drawing.Point(240, 25);
            this.LabelControl34.Name = "LabelControl34";
            this.LabelControl34.Size = new System.Drawing.Size(9, 13);
            this.LabelControl34.TabIndex = 2;
            this.LabelControl34.Text = "+";
            this.LabelControl34.UseMnemonic = false;
            //
            //current_balance
            //
            this.current_balance.Location = new System.Drawing.Point(131, 80);
            this.current_balance.Name = "current_balance";
            this.current_balance.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.False;
            this.current_balance.Properties.Appearance.Options.UseTextOptions = true;
            this.current_balance.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.current_balance.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] { new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo) });
            this.current_balance.Properties.DisplayFormat.FormatString = "c2";
            this.current_balance.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.current_balance.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.current_balance.Properties.Mask.EditMask = "n2";
            this.current_balance.Properties.NullText = "$0.00";
            this.current_balance.Properties.Precision = 2;
            this.current_balance.Size = new System.Drawing.Size(100, 20);
            this.current_balance.TabIndex = 12;
            this.current_balance.Tag = "";
            this.current_balance.ToolTip = "Enter the current balance associated with the debt.";
            //
            //total_payments
            //
            this.total_payments.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.total_payments.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.total_payments.Location = new System.Drawing.Point(115, 67);
            this.total_payments.Name = "total_payments";
            this.total_payments.Size = new System.Drawing.Size(95, 13);
            this.total_payments.TabIndex = 10;
            this.total_payments.Text = "$0.00";
            this.total_payments.UseMnemonic = false;
            //
            //total_interest
            //
            this.total_interest.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.total_interest.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.total_interest.Location = new System.Drawing.Point(134, 53);
            this.total_interest.Name = "total_interest";
            this.total_interest.Size = new System.Drawing.Size(76, 13);
            this.total_interest.TabIndex = 7;
            this.total_interest.Text = "$0.00";
            this.total_interest.UseMnemonic = false;
            //
            //orig_balance_adjustment
            //
            this.orig_balance_adjustment.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.orig_balance_adjustment.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.orig_balance_adjustment.Location = new System.Drawing.Point(115, 39);
            this.orig_balance_adjustment.Name = "orig_balance_adjustment";
            this.orig_balance_adjustment.Size = new System.Drawing.Size(95, 13);
            this.orig_balance_adjustment.TabIndex = 4;
            this.orig_balance_adjustment.Text = "$0.00";
            this.orig_balance_adjustment.UseMnemonic = false;
            //
            //orig_balance
            //
            this.orig_balance.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.orig_balance.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.orig_balance.Location = new System.Drawing.Point(115, 24);
            this.orig_balance.Name = "orig_balance";
            this.orig_balance.Size = new System.Drawing.Size(95, 13);
            this.orig_balance.TabIndex = 1;
            this.orig_balance.Text = "$0.00";
            this.orig_balance.UseMnemonic = false;
            //
            //LabelControl24
            //
            this.LabelControl24.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.Horizontal;
            this.LabelControl24.Location = new System.Drawing.Point(8, 67);
            this.LabelControl24.Name = "LabelControl24";
            this.LabelControl24.Size = new System.Drawing.Size(78, 13);
            this.LabelControl24.TabIndex = 9;
            this.LabelControl24.Text = "Total Payments:";
            this.LabelControl24.UseMnemonic = false;
            //
            //LabelControl23
            //
            this.LabelControl23.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.Horizontal;
            this.LabelControl23.Location = new System.Drawing.Point(8, 53);
            this.LabelControl23.Name = "LabelControl23";
            this.LabelControl23.Size = new System.Drawing.Size(97, 13);
            this.LabelControl23.TabIndex = 6;
            this.LabelControl23.Text = "Total Interest/Fees:";
            this.LabelControl23.UseMnemonic = false;
            //
            //LabelControl21
            //
            this.LabelControl21.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.Horizontal;
            this.LabelControl21.Location = new System.Drawing.Point(8, 25);
            this.LabelControl21.Name = "LabelControl21";
            this.LabelControl21.Size = new System.Drawing.Size(80, 13);
            this.LabelControl21.TabIndex = 0;
            this.LabelControl21.Text = "Original Balance:";
            this.LabelControl21.UseMnemonic = false;
            //
            //BalanceInformation
            //
            this.Controls.Add(this.GroupControl2);
            this.Name = "BalanceInformation";
            this.Size = new System.Drawing.Size(256, 104);
            ((System.ComponentModel.ISupportInitialize)this.GroupControl2).EndInit();
            this.GroupControl2.ResumeLayout(false);
            this.GroupControl2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)this.current_balance.Properties).EndInit();
            this.ResumeLayout(false);
        }

        private DevExpress.XtraEditors.CalcEdit current_balance;
        private DevExpress.XtraEditors.GroupControl GroupControl2;
        private DevExpress.XtraEditors.LabelControl LabelControl21;
        private DevExpress.XtraEditors.LabelControl LabelControl22;
        private DevExpress.XtraEditors.LabelControl LabelControl23;
        private DevExpress.XtraEditors.LabelControl LabelControl24;
        private DevExpress.XtraEditors.LabelControl LabelControl30;
        private DevExpress.XtraEditors.LabelControl LabelControl31;
        private DevExpress.XtraEditors.LabelControl LabelControl32;
        private DevExpress.XtraEditors.LabelControl LabelControl33;
        private DevExpress.XtraEditors.LabelControl LabelControl34;
        private DevExpress.XtraEditors.LabelControl orig_balance;
        private DevExpress.XtraEditors.LabelControl orig_balance_adjustment;
        private DevExpress.XtraEditors.SimpleButton SimpleButton_Verified;
        private DevExpress.XtraEditors.LabelControl total_interest;
        private DevExpress.XtraEditors.LabelControl total_payments;

        #endregion " Windows Form Designer generated code "
    }
}