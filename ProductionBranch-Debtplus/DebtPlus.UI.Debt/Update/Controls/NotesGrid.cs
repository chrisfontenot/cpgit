#region "Copyright 2000-2012 DebtPlus, L.L.C."

//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}

#endregion "Copyright 2000-2012 DebtPlus, L.L.C."

using System;
using System.ComponentModel;
using System.Data.SqlClient;
using System.Linq;
using System.Threading;
using DebtPlus.LINQ;
using DebtPlus.Notes;
using DebtPlus.UI.Debt.Update.pages;

namespace DebtPlus.UI.Debt.Update.controls
{
    internal partial class NotesGrid : MyGridControlDebt
    {
        // Thread to process the reading of the notes from the database.
        // This may take a while so do it in the background.
        private System.ComponentModel.BackgroundWorker bt;

        // We display the grid from this list
        private System.Collections.Generic.List<Note> colNotes;
        private DebtPlus.Interfaces.Debt.IDebtRecord currentDebtRecord;

        private string currentUser;
        private bool isAdmin;

        public NotesGrid()
            : base()
        {
            InitializeComponent();

            bt          = new System.ComponentModel.BackgroundWorker();
            EnableMenus = true;
            isAdmin     = false;
            currentUser = null;
            colNotes    = null;

            RegisterHandlers();
        }

        private void RegisterHandlers()
        {
            bt.DoWork                             += bt_DoWork;
            bt.RunWorkerCompleted                 += bt_RunWorkerCompleted;
            CheckEdit_Alerts.EditValueChanging    += CheckEdit_Alerts_EditValueChanging;
            CheckEdit_Perm.EditValueChanging      += CheckEdit_Alerts_EditValueChanging;
            CheckEdit_System.EditValueChanging    += CheckEdit_Alerts_EditValueChanging;
            CheckEdit_Temporary.EditValueChanging += CheckEdit_Alerts_EditValueChanging;
            CheckEdit_Alerts.CheckStateChanged    += CheckEdit_CheckStateChanged;
            CheckEdit_Temporary.CheckStateChanged += CheckEdit_CheckStateChanged;
            CheckEdit_Perm.CheckStateChanged      += CheckEdit_CheckStateChanged;
            CheckEdit_System.CheckStateChanged    += CheckEdit_CheckStateChanged;
            GridView1.CustomColumnDisplayText     += GridView1_CustomColumnDisplayText;
        }

        private void UnRegisterHandlers()
        {
            bt.DoWork                             -= bt_DoWork;
            bt.RunWorkerCompleted                 -= bt_RunWorkerCompleted;
            CheckEdit_Alerts.EditValueChanging    -= CheckEdit_Alerts_EditValueChanging;
            CheckEdit_Perm.EditValueChanging      -= CheckEdit_Alerts_EditValueChanging;
            CheckEdit_System.EditValueChanging    -= CheckEdit_Alerts_EditValueChanging;
            CheckEdit_Temporary.EditValueChanging -= CheckEdit_Alerts_EditValueChanging;
            CheckEdit_Alerts.CheckStateChanged    -= CheckEdit_CheckStateChanged;
            CheckEdit_Temporary.CheckStateChanged -= CheckEdit_CheckStateChanged;
            CheckEdit_Perm.CheckStateChanged      -= CheckEdit_CheckStateChanged;
            CheckEdit_System.CheckStateChanged    -= CheckEdit_CheckStateChanged;
            GridView1.CustomColumnDisplayText     -= GridView1_CustomColumnDisplayText;
        }

        /// <summary>
        /// Do any custom formatting of fields for the current grid. We format the type to a string.
        /// </summary>
        private void GridView1_CustomColumnDisplayText(object sender, DevExpress.XtraGrid.Views.Base.CustomColumnDisplayTextEventArgs e)
        {
            // Find the record being displayed
            var record = GridView1.GetRow(e.RowHandle) as Note;
            if (record == null)
            {
                return;
            }

            // If the column is the note type then format it properly
            if (e.Column == GridColumn_type)
            {
                try
                {
                    var noteType = (DebtPlus.LINQ.InMemory.Notes.NoteTypes)record.type;
                    e.DisplayText = noteType.ToString();
                }
                catch
                {
                    e.DisplayText = "Unknown";
                }
            }
        }

        private delegate void EditCompletionDelegate(object obj);

        public void CreateNote(object obj)
        {
            // Create the note.
            using (NoteClass.DebtNote NoteClass = new NoteClass.DebtNote())
            {
                if (NoteClass.Create(currentDebtRecord.ClientId, currentDebtRecord.client_creditor))
                {
                    EditCompletion(obj);
                }
            }
        }

        public void DeleteNote(object obj)
        {
            Note currentNote = obj as Note;
            if (currentNote == null)
            {
                return;
            }

            using (NoteClass.ClientNote n = new NoteClass.ClientNote())
            {
                if (n.Delete(currentNote.Id))
                {
                    EditCompletion(obj);
                }
            }
        }

        public void EditNote(object obj)
        {
            Note currentNote = obj as Note;
            if (currentNote == null)
            {
                return;
            }

            using (DebtPlus.Notes.NoteClass.DebtNote n = new DebtPlus.Notes.NoteClass.DebtNote())
            {
                if (OkToEditNote(obj))
                {
                    n.Edit(currentNote.Id);
                    EditCompletion(obj);
                }
                else
                {
                    n.Show(currentNote.Id);
                }
            }
        }

        public override void ReadForm(DebtPlus.Interfaces.Debt.IDebtRecord CurrentDebtRecord)
        {
            this.currentDebtRecord = CurrentDebtRecord;
            currentUser = BusinessContext.suser_sname();
            isAdmin = string.Compare(currentUser, "sa", false) == 0;

            // Remove the system identifier from the user name
            Int32 iPos = currentUser.LastIndexOfAny(new char[] { System.IO.Path.DirectorySeparatorChar, System.IO.Path.AltDirectorySeparatorChar });
            if (iPos > 0)
            {
                currentUser = currentUser.Substring(iPos + 1).Trim();
            }

            RefreshList();
        }

        internal void RefreshList()
        {
            if (!bt.IsBusy)
            {
                // Create the class to pass to the worker thread
                OptionsClass Options = new OptionsClass
                {
                    CurrentDebtRecord = currentDebtRecord,
                    IncludePerm = CheckEdit_Perm.Checked,
                    IncludeTemp = CheckEdit_Temporary.Checked,
                    IncludeSystem = CheckEdit_System.Checked,
                    IncludeAlert = CheckEdit_Alerts.Checked
                };

                // Set the wait cursor and clear the current list
                GridControl1.UseWaitCursor = true;
                GridControl1.DataSource = null;
                GridControl1.RefreshDataSource();

                // Run the thread to read the notes list
                bt.RunWorkerAsync(Options);
            }
        }

        /// <summary>
        /// Determine if the current user is allowed to delete the current note.
        /// </summary>
        protected override bool OkToDelete(object obj)
        {
            if (!base.OkToDelete(obj))
                return false;

            // Find the current note. If none then don't allow the edit operation
            Note currentNote = obj as Note;
            if (currentNote == null)
                return false;

            // Admin users are always allowed to delete the note
            if (isAdmin)
                return true;

            // If the user is not allowed to delete a note then the user is not allowed
            if (!DebtPlus.Configuration.Config.DeleteNotes)
                return false;

            // Look at the status if the user is different
            if (string.Compare(currentNote.created_by, currentUser, true) != 0)
            {
                return !currentNote.dont_delete;
            }

            // Finally, the user is the same. Allow the delete operation.
            return true;
        }

        /// <summary>
        /// Determine if the current user is allowed to edit the current note.
        /// </summary>
        /// <param name="obj">Pointer to the Note structure for the current note</param>
        protected bool OkToEditNote(object obj)
        {
            // THe user must be allowed to edit the note.
            if (!base.OkToEdit(obj))
                return false;

            // Find the current note. If none then don't allow the edit operation
            Note currentNote = obj as Note;
            if (currentNote == null)
                return false;

            // System notes are never allowed to be edited, even by admin users
            if (currentNote.type == 3)
                return false;

            // Admin users are always allowed to edit the note
            if (isAdmin)
                return true;

            // If the user is the same then the edit operation is allowed if the registry says so
            if (string.Compare(currentNote.created_by, currentUser, true) == 0)
            {
                return DebtPlus.Configuration.Config.EditExistingNoteSameUser;
            }

            // If the user is not the same then we look at the indicator flags
            return DebtPlus.Configuration.Config.EditExistingNoteOtherUser && !currentNote.dont_edit;
        }

        protected override void OnCreate()
        {
            Thread ithrd = new System.Threading.Thread(new System.Threading.ParameterizedThreadStart(CreateNote))
            {
                Name = "CreateNote",
                IsBackground = true
            };
            ithrd.SetApartmentState(ApartmentState.STA);
            ithrd.Start(null);
        }

        protected override void OnDelete(object obj)
        {
            Thread ithrd = new System.Threading.Thread(new System.Threading.ParameterizedThreadStart(DeleteNote))
            {
                Name = "DeleteNote",
                IsBackground = true
            };
            ithrd.SetApartmentState(ApartmentState.STA);
            ithrd.Start(obj);
        }

        protected override void OnEdit(object obj)
        {
            Thread ithrd = new System.Threading.Thread(new System.Threading.ParameterizedThreadStart(EditNote))
            {
                Name = "EditNote",
                IsBackground = true
            };
            ithrd.SetApartmentState(ApartmentState.STA);
            ithrd.Start(obj);
        }

        protected override void PopupMenu_Items_Popup(object sender, EventArgs e)
        {
            // Change the text to show the action allowed for the edit operation.
            if (ControlRow >= 0)
            {
                object obj = GridView1.GetRow(ControlRow);
                if (obj != null)
                {
                    BarButtonItem_Change.Caption = OkToEditNote(obj) ? "&Edit..." : "&Show...";
                }
            }

            // Finally, do the standard logic for the event.
            base.PopupMenu_Items_Popup(sender, e);
        }

        private void bt_DoWork(object sender, DoWorkEventArgs e)
        {
            OptionsClass options = (OptionsClass)e.Argument;

            // Build the request to retrieve the list of notes
            try
            {
                using (BusinessContext dc = new BusinessContext())
                {
                    // Build the list of client note types permitted
                    System.Collections.Generic.List<Int32> types = new System.Collections.Generic.List<Int32>();
                    if (options.IncludePerm)
                    {
                        types.Add(1);
                    }
                    if (options.IncludeTemp)
                    {
                        types.Add(2);
                    }
                    if (options.IncludeSystem)
                    {
                        types.Add(3);
                    }
                    if (options.IncludeAlert)
                    {
                        types.Add(4);
                    }

                    // If nothing is checked then include the client notes and nothing else
                    if (types.Count == 0 || types.Count == 4)
                    {
                        colNotes = dc.client_notes.Where(n => n.client == options.CurrentDebtRecord.ClientId && n.client_creditor.GetValueOrDefault() == options.CurrentDebtRecord.client_creditor && n.type > 0).Select(n => new Note(1, n.Id, n.type, n.subject, n.date_created, n.created_by, n.dont_edit, n.dont_delete)).ToList();
                    }
                    else if (types.Count > 0)
                    {
                        colNotes = dc.client_notes.Where(n => n.client == options.CurrentDebtRecord.ClientId && n.client_creditor.GetValueOrDefault() == options.CurrentDebtRecord.client_creditor && types.Contains(n.type)).Select(n => new Note(1, n.Id, n.type, n.subject, n.date_created, n.created_by, n.dont_edit, n.dont_delete)).ToList();
                    }
                }
            }
            catch (SqlException ex)
            {
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex);
            }
        }

        private void bt_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            GridControl1.UseWaitCursor = false;
            GridControl1.DataSource = colNotes;
            GridControl1.RefreshDataSource();
        }

        private void CheckEdit_Alerts_EditValueChanging(object sender, DevExpress.XtraEditors.Controls.ChangingEventArgs e)
        {
            // Always allow the check to be set
            if (Convert.ToBoolean(e.NewValue))
            {
                return;
            }

            // If the check is being cleared, we need at least two (soon to be one) selected
            Int32 SelectedCount = 0;
            if (CheckEdit_Alerts.Checked)
                SelectedCount += 1;
            if (CheckEdit_Perm.Checked)
                SelectedCount += 1;
            if (CheckEdit_System.Checked)
                SelectedCount += 1;
            if (CheckEdit_Temporary.Checked)
                SelectedCount += 1;

            // Cancel the change if we are turning off all checked controls
            if (SelectedCount < 2)
            {
                e.Cancel = true;
            }
        }

        private void CheckEdit_CheckStateChanged(object sender, EventArgs e)
        {
            RefreshList();
        }

        private void EditCompletion(object obj)
        {
            // Ensure that we are in the proper thread space.
            if (InvokeRequired)
            {
                IAsyncResult ia = BeginInvoke(new EditCompletionDelegate(EditCompletion), new object[] { obj });
                object Result = EndInvoke(ia);
            }
            else
            {
                Note currentNote = obj as Note;

                // Refresh the list of notes on the display if needed
                if (((forms.Form_DebtUpdate)FindForm()).FindCurrentPage().GetType() == typeof (Page_Notes))
                {
                    RefreshList();
                }
            }
        }

        private class Note
        {
            private string m_created_by;

            public Note()
            {
            }

            public Note(Int32 source, Int32 Id, Int32 type, string subject, DateTime date_created, string created_by, bool dont_edit, bool dont_delete)
                : this()
            {
                this.source = source;
                this.Id = Id;
                this.type = type;
                this.subject = subject;
                this.date_created = date_created;
                this.created_by = created_by;
                this.dont_edit = dont_edit;
                this.dont_delete = dont_delete;
            }

            public string created_by
            {
                get
                {
                    return m_created_by;
                }
                set
                {
                    if (string.IsNullOrEmpty(value))
                    {
                        m_created_by = string.Empty;
                        return;
                    }

                    Int32 iPos = value.LastIndexOfAny(new char[] { System.IO.Path.DirectorySeparatorChar, System.IO.Path.AltDirectorySeparatorChar });
                    if (iPos > 0)
                    {
                        m_created_by = value.Substring(iPos + 1).Trim();
                        return;
                    }
                    m_created_by = value.Trim();
                }
            }

            public DateTime date_created { get; set; }
            public bool dont_delete { get; set; }
            public bool dont_edit { get; set; }
            public Int32 Id { get; set; }
            public Int32 source { get; set; }
            public string subject { get; set; }
            public Int32 type { get; set; }
        }

        private class OptionsClass : IDisposable
        {
            public bool IncludeAlert = true;

            public bool IncludeDisbCurrent = true;

            public bool IncludeDisbPrior = true;

            public bool IncludePerm = true;

            public bool IncludeSystem = true;

            public bool IncludeTemp = true;

            public DebtPlus.Interfaces.Debt.IDebtRecord CurrentDebtRecord;

            public void Dispose()
            {
                Dispose(true);
                GC.SuppressFinalize(this);
            }

            protected virtual void Dispose(bool disposing)
            {
            }
        }

        #region " Windows Form Designer generated code "

        internal DevExpress.XtraEditors.CheckEdit CheckEdit_Alerts;

        internal DevExpress.XtraEditors.CheckEdit CheckEdit_Perm;

        internal DevExpress.XtraEditors.CheckEdit CheckEdit_System;

        internal DevExpress.XtraEditors.CheckEdit CheckEdit_Temporary;

        internal DevExpress.XtraGrid.Columns.GridColumn GridColumn_Counselor;

        internal DevExpress.XtraGrid.Columns.GridColumn GridColumn_date_created;

        internal DevExpress.XtraGrid.Columns.GridColumn GridColumn_dont_delete;

        internal DevExpress.XtraGrid.Columns.GridColumn GridColumn_dont_edit;

        internal DevExpress.XtraGrid.Columns.GridColumn GridColumn_dont_print;

        internal DevExpress.XtraGrid.Columns.GridColumn GridColumn_ID;

        internal DevExpress.XtraGrid.Columns.GridColumn GridColumn_Source;

        internal DevExpress.XtraGrid.Columns.GridColumn GridColumn_Subject;

        internal DevExpress.XtraGrid.Columns.GridColumn GridColumn_type;

        //NOTE: The following procedure is required by the Windows Form Designer
        //It can be modified using the Windows Form Designer.
        //Do not modify it using the code editor.
        internal DevExpress.XtraEditors.GroupControl GroupControl1;

        //Required by the Windows Form Designer
        private System.ComponentModel.IContainer components = null;

        //Form overrides dispose to clean up the component list.
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (components != null)
                {
                    components.Dispose();
                }
            }
            base.Dispose(disposing);
        }

        [System.Diagnostics.DebuggerStepThrough()]
        private void InitializeComponent()
        {
            this.GroupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.CheckEdit_Alerts = new DevExpress.XtraEditors.CheckEdit();
            this.CheckEdit_System = new DevExpress.XtraEditors.CheckEdit();
            this.CheckEdit_Temporary = new DevExpress.XtraEditors.CheckEdit();
            this.CheckEdit_Perm = new DevExpress.XtraEditors.CheckEdit();
            this.GridColumn_date_created = new DevExpress.XtraGrid.Columns.GridColumn();
            this.GridColumn_type = new DevExpress.XtraGrid.Columns.GridColumn();
            this.GridColumn_Counselor = new DevExpress.XtraGrid.Columns.GridColumn();
            this.GridColumn_Subject = new DevExpress.XtraGrid.Columns.GridColumn();
            this.GridColumn_ID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.GridColumn_dont_edit = new DevExpress.XtraGrid.Columns.GridColumn();
            this.GridColumn_dont_delete = new DevExpress.XtraGrid.Columns.GridColumn();
            this.GridColumn_dont_print = new DevExpress.XtraGrid.Columns.GridColumn();
            this.GridColumn_Source = new DevExpress.XtraGrid.Columns.GridColumn();
            ((System.ComponentModel.ISupportInitialize)this.barManager1).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.PopupMenu_Items).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.GridControl1).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.GridView1).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.GroupControl1).BeginInit();
            this.GroupControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)this.CheckEdit_Alerts.Properties).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.CheckEdit_System.Properties).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.CheckEdit_Temporary.Properties).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.CheckEdit_Perm.Properties).BeginInit();
            this.SuspendLayout();
            //
            //barDockControlTop
            //
            this.barDockControlTop.Size = new System.Drawing.Size(576, 0);
            //
            //barDockControlBottom
            //
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 296);
            this.barDockControlBottom.Size = new System.Drawing.Size(576, 0);
            //
            //barDockControlLeft
            //
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 296);
            //
            //barDockControlRight
            //
            this.barDockControlRight.Location = new System.Drawing.Point(576, 0);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 296);
            //
            //GridControl1
            //
            this.GridControl1.Anchor = (System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) | System.Windows.Forms.AnchorStyles.Left) | System.Windows.Forms.AnchorStyles.Right);
            this.GridControl1.Dock = System.Windows.Forms.DockStyle.None;
            this.GridControl1.Location = new System.Drawing.Point(0, 48);
            this.GridControl1.Size = new System.Drawing.Size(576, 248);
            //
            //GridView1
            //
            this.GridView1.Appearance.ColumnFilterButton.BackColor = System.Drawing.Color.FromArgb(Convert.ToInt32(Convert.ToByte(104)), Convert.ToInt32(Convert.ToByte(184)), Convert.ToInt32(Convert.ToByte(251)));
            this.GridView1.Appearance.ColumnFilterButton.BorderColor = System.Drawing.Color.FromArgb(Convert.ToInt32(Convert.ToByte(104)), Convert.ToInt32(Convert.ToByte(184)), Convert.ToInt32(Convert.ToByte(251)));
            this.GridView1.Appearance.ColumnFilterButton.ForeColor = System.Drawing.Color.White;
            this.GridView1.Appearance.ColumnFilterButton.Options.UseBackColor = true;
            this.GridView1.Appearance.ColumnFilterButton.Options.UseBorderColor = true;
            this.GridView1.Appearance.ColumnFilterButton.Options.UseForeColor = true;
            this.GridView1.Appearance.ColumnFilterButtonActive.BackColor = System.Drawing.Color.FromArgb(Convert.ToInt32(Convert.ToByte(170)), Convert.ToInt32(Convert.ToByte(216)), Convert.ToInt32(Convert.ToByte(254)));
            this.GridView1.Appearance.ColumnFilterButtonActive.BorderColor = System.Drawing.Color.FromArgb(Convert.ToInt32(Convert.ToByte(170)), Convert.ToInt32(Convert.ToByte(216)), Convert.ToInt32(Convert.ToByte(254)));
            this.GridView1.Appearance.ColumnFilterButtonActive.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.ColumnFilterButtonActive.Options.UseBackColor = true;
            this.GridView1.Appearance.ColumnFilterButtonActive.Options.UseBorderColor = true;
            this.GridView1.Appearance.ColumnFilterButtonActive.Options.UseForeColor = true;
            this.GridView1.Appearance.Empty.BackColor = System.Drawing.Color.FromArgb(Convert.ToInt32(Convert.ToByte(236)), Convert.ToInt32(Convert.ToByte(246)), Convert.ToInt32(Convert.ToByte(255)));
            this.GridView1.Appearance.Empty.BackColor2 = System.Drawing.Color.White;
            this.GridView1.Appearance.Empty.Options.UseBackColor = true;
            this.GridView1.Appearance.EvenRow.BackColor = System.Drawing.Color.FromArgb(Convert.ToInt32(Convert.ToByte(247)), Convert.ToInt32(Convert.ToByte(251)), Convert.ToInt32(Convert.ToByte(255)));
            this.GridView1.Appearance.EvenRow.BorderColor = System.Drawing.Color.FromArgb(Convert.ToInt32(Convert.ToByte(247)), Convert.ToInt32(Convert.ToByte(251)), Convert.ToInt32(Convert.ToByte(255)));
            this.GridView1.Appearance.EvenRow.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.EvenRow.Options.UseBackColor = true;
            this.GridView1.Appearance.EvenRow.Options.UseBorderColor = true;
            this.GridView1.Appearance.EvenRow.Options.UseForeColor = true;
            this.GridView1.Appearance.FilterCloseButton.BackColor = System.Drawing.Color.FromArgb(Convert.ToInt32(Convert.ToByte(104)), Convert.ToInt32(Convert.ToByte(184)), Convert.ToInt32(Convert.ToByte(251)));
            this.GridView1.Appearance.FilterCloseButton.BorderColor = System.Drawing.Color.FromArgb(Convert.ToInt32(Convert.ToByte(104)), Convert.ToInt32(Convert.ToByte(184)), Convert.ToInt32(Convert.ToByte(251)));
            this.GridView1.Appearance.FilterCloseButton.ForeColor = System.Drawing.Color.White;
            this.GridView1.Appearance.FilterCloseButton.Options.UseBackColor = true;
            this.GridView1.Appearance.FilterCloseButton.Options.UseBorderColor = true;
            this.GridView1.Appearance.FilterCloseButton.Options.UseForeColor = true;
            this.GridView1.Appearance.FilterPanel.BackColor = System.Drawing.Color.FromArgb(Convert.ToInt32(Convert.ToByte(236)), Convert.ToInt32(Convert.ToByte(246)), Convert.ToInt32(Convert.ToByte(255)));
            this.GridView1.Appearance.FilterPanel.BackColor2 = System.Drawing.Color.White;
            this.GridView1.Appearance.FilterPanel.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.FilterPanel.Options.UseBackColor = true;
            this.GridView1.Appearance.FilterPanel.Options.UseForeColor = true;
            this.GridView1.Appearance.FixedLine.BackColor = System.Drawing.Color.FromArgb(Convert.ToInt32(Convert.ToByte(59)), Convert.ToInt32(Convert.ToByte(133)), Convert.ToInt32(Convert.ToByte(195)));
            this.GridView1.Appearance.FixedLine.Options.UseBackColor = true;
            this.GridView1.Appearance.FocusedCell.BackColor = System.Drawing.Color.White;
            this.GridView1.Appearance.FocusedCell.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.FocusedCell.Options.UseBackColor = true;
            this.GridView1.Appearance.FocusedCell.Options.UseForeColor = true;
            this.GridView1.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb(Convert.ToInt32(Convert.ToByte(38)), Convert.ToInt32(Convert.ToByte(109)), Convert.ToInt32(Convert.ToByte(189)));
            this.GridView1.Appearance.FocusedRow.BorderColor = System.Drawing.Color.FromArgb(Convert.ToInt32(Convert.ToByte(59)), Convert.ToInt32(Convert.ToByte(139)), Convert.ToInt32(Convert.ToByte(206)));
            this.GridView1.Appearance.FocusedRow.ForeColor = System.Drawing.Color.White;
            this.GridView1.Appearance.FocusedRow.Options.UseBackColor = true;
            this.GridView1.Appearance.FocusedRow.Options.UseBorderColor = true;
            this.GridView1.Appearance.FocusedRow.Options.UseForeColor = true;
            this.GridView1.Appearance.FooterPanel.BackColor = System.Drawing.Color.FromArgb(Convert.ToInt32(Convert.ToByte(104)), Convert.ToInt32(Convert.ToByte(184)), Convert.ToInt32(Convert.ToByte(251)));
            this.GridView1.Appearance.FooterPanel.BorderColor = System.Drawing.Color.FromArgb(Convert.ToInt32(Convert.ToByte(104)), Convert.ToInt32(Convert.ToByte(184)), Convert.ToInt32(Convert.ToByte(251)));
            this.GridView1.Appearance.FooterPanel.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.FooterPanel.Options.UseBackColor = true;
            this.GridView1.Appearance.FooterPanel.Options.UseBorderColor = true;
            this.GridView1.Appearance.FooterPanel.Options.UseForeColor = true;
            this.GridView1.Appearance.GroupButton.BackColor = System.Drawing.Color.FromArgb(Convert.ToInt32(Convert.ToByte(104)), Convert.ToInt32(Convert.ToByte(184)), Convert.ToInt32(Convert.ToByte(251)));
            this.GridView1.Appearance.GroupButton.BorderColor = System.Drawing.Color.FromArgb(Convert.ToInt32(Convert.ToByte(104)), Convert.ToInt32(Convert.ToByte(184)), Convert.ToInt32(Convert.ToByte(251)));
            this.GridView1.Appearance.GroupButton.Options.UseBackColor = true;
            this.GridView1.Appearance.GroupButton.Options.UseBorderColor = true;
            this.GridView1.Appearance.GroupFooter.BackColor = System.Drawing.Color.FromArgb(Convert.ToInt32(Convert.ToByte(170)), Convert.ToInt32(Convert.ToByte(216)), Convert.ToInt32(Convert.ToByte(254)));
            this.GridView1.Appearance.GroupFooter.BorderColor = System.Drawing.Color.FromArgb(Convert.ToInt32(Convert.ToByte(170)), Convert.ToInt32(Convert.ToByte(216)), Convert.ToInt32(Convert.ToByte(254)));
            this.GridView1.Appearance.GroupFooter.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.GroupFooter.Options.UseBackColor = true;
            this.GridView1.Appearance.GroupFooter.Options.UseBorderColor = true;
            this.GridView1.Appearance.GroupFooter.Options.UseForeColor = true;
            this.GridView1.Appearance.GroupPanel.BackColor = System.Drawing.Color.FromArgb(Convert.ToInt32(Convert.ToByte(236)), Convert.ToInt32(Convert.ToByte(246)), Convert.ToInt32(Convert.ToByte(255)));
            this.GridView1.Appearance.GroupPanel.BackColor2 = System.Drawing.Color.White;
            this.GridView1.Appearance.GroupPanel.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.GroupPanel.Options.UseBackColor = true;
            this.GridView1.Appearance.GroupPanel.Options.UseForeColor = true;
            this.GridView1.Appearance.GroupRow.BackColor = System.Drawing.Color.FromArgb(Convert.ToInt32(Convert.ToByte(170)), Convert.ToInt32(Convert.ToByte(216)), Convert.ToInt32(Convert.ToByte(254)));
            this.GridView1.Appearance.GroupRow.BorderColor = System.Drawing.Color.FromArgb(Convert.ToInt32(Convert.ToByte(170)), Convert.ToInt32(Convert.ToByte(216)), Convert.ToInt32(Convert.ToByte(254)));
            this.GridView1.Appearance.GroupRow.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.GroupRow.Options.UseBackColor = true;
            this.GridView1.Appearance.GroupRow.Options.UseBorderColor = true;
            this.GridView1.Appearance.GroupRow.Options.UseForeColor = true;
            this.GridView1.Appearance.HeaderPanel.BackColor = System.Drawing.Color.FromArgb(Convert.ToInt32(Convert.ToByte(139)), Convert.ToInt32(Convert.ToByte(201)), Convert.ToInt32(Convert.ToByte(254)));
            this.GridView1.Appearance.HeaderPanel.BorderColor = System.Drawing.Color.FromArgb(Convert.ToInt32(Convert.ToByte(139)), Convert.ToInt32(Convert.ToByte(201)), Convert.ToInt32(Convert.ToByte(254)));
            this.GridView1.Appearance.HeaderPanel.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.HeaderPanel.Options.UseBackColor = true;
            this.GridView1.Appearance.HeaderPanel.Options.UseBorderColor = true;
            this.GridView1.Appearance.HeaderPanel.Options.UseForeColor = true;
            this.GridView1.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.FromArgb(Convert.ToInt32(Convert.ToByte(105)), Convert.ToInt32(Convert.ToByte(170)), Convert.ToInt32(Convert.ToByte(225)));
            this.GridView1.Appearance.HideSelectionRow.BorderColor = System.Drawing.Color.FromArgb(Convert.ToInt32(Convert.ToByte(83)), Convert.ToInt32(Convert.ToByte(155)), Convert.ToInt32(Convert.ToByte(215)));
            this.GridView1.Appearance.HideSelectionRow.ForeColor = System.Drawing.Color.FromArgb(Convert.ToInt32(Convert.ToByte(236)), Convert.ToInt32(Convert.ToByte(246)), Convert.ToInt32(Convert.ToByte(255)));
            this.GridView1.Appearance.HideSelectionRow.Options.UseBackColor = true;
            this.GridView1.Appearance.HideSelectionRow.Options.UseBorderColor = true;
            this.GridView1.Appearance.HideSelectionRow.Options.UseForeColor = true;
            this.GridView1.Appearance.HorzLine.BackColor = System.Drawing.Color.FromArgb(Convert.ToInt32(Convert.ToByte(104)), Convert.ToInt32(Convert.ToByte(184)), Convert.ToInt32(Convert.ToByte(251)));
            this.GridView1.Appearance.HorzLine.Options.UseBackColor = true;
            this.GridView1.Appearance.OddRow.BackColor = System.Drawing.Color.FromArgb(Convert.ToInt32(Convert.ToByte(236)), Convert.ToInt32(Convert.ToByte(246)), Convert.ToInt32(Convert.ToByte(255)));
            this.GridView1.Appearance.OddRow.BorderColor = System.Drawing.Color.FromArgb(Convert.ToInt32(Convert.ToByte(236)), Convert.ToInt32(Convert.ToByte(246)), Convert.ToInt32(Convert.ToByte(255)));
            this.GridView1.Appearance.OddRow.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.OddRow.Options.UseBackColor = true;
            this.GridView1.Appearance.OddRow.Options.UseBorderColor = true;
            this.GridView1.Appearance.OddRow.Options.UseForeColor = true;
            this.GridView1.Appearance.Preview.Font = new System.Drawing.Font("Verdana", 7.5f);
            this.GridView1.Appearance.Preview.ForeColor = System.Drawing.Color.FromArgb(Convert.ToInt32(Convert.ToByte(83)), Convert.ToInt32(Convert.ToByte(155)), Convert.ToInt32(Convert.ToByte(215)));
            this.GridView1.Appearance.Preview.Options.UseFont = true;
            this.GridView1.Appearance.Preview.Options.UseForeColor = true;
            this.GridView1.Appearance.Row.BackColor = System.Drawing.Color.FromArgb(Convert.ToInt32(Convert.ToByte(247)), Convert.ToInt32(Convert.ToByte(251)), Convert.ToInt32(Convert.ToByte(255)));
            this.GridView1.Appearance.Row.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.Row.Options.UseBackColor = true;
            this.GridView1.Appearance.Row.Options.UseForeColor = true;
            this.GridView1.Appearance.RowSeparator.BackColor = System.Drawing.Color.FromArgb(Convert.ToInt32(Convert.ToByte(236)), Convert.ToInt32(Convert.ToByte(246)), Convert.ToInt32(Convert.ToByte(255)));
            this.GridView1.Appearance.RowSeparator.BackColor2 = System.Drawing.Color.White;
            this.GridView1.Appearance.RowSeparator.Options.UseBackColor = true;
            this.GridView1.Appearance.SelectedRow.BackColor = System.Drawing.Color.FromArgb(Convert.ToInt32(Convert.ToByte(83)), Convert.ToInt32(Convert.ToByte(155)), Convert.ToInt32(Convert.ToByte(215)));
            this.GridView1.Appearance.SelectedRow.ForeColor = System.Drawing.Color.White;
            this.GridView1.Appearance.SelectedRow.Options.UseBackColor = true;
            this.GridView1.Appearance.SelectedRow.Options.UseForeColor = true;
            this.GridView1.Appearance.TopNewRow.BackColor = System.Drawing.Color.White;
            this.GridView1.Appearance.TopNewRow.Options.UseBackColor = true;
            this.GridView1.Appearance.VertLine.BackColor = System.Drawing.Color.FromArgb(Convert.ToInt32(Convert.ToByte(104)), Convert.ToInt32(Convert.ToByte(184)), Convert.ToInt32(Convert.ToByte(251)));
            this.GridView1.Appearance.VertLine.Options.UseBackColor = true;
            this.GridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
				this.GridColumn_date_created,
				this.GridColumn_type,
				this.GridColumn_Counselor,
				this.GridColumn_Subject,
				this.GridColumn_ID,
				this.GridColumn_dont_edit,
				this.GridColumn_dont_delete,
				this.GridColumn_dont_print,
				this.GridColumn_Source
			});
            this.GridView1.OptionsBehavior.AutoPopulateColumns = false;
            this.GridView1.OptionsBehavior.Editable = false;
            this.GridView1.OptionsView.EnableAppearanceEvenRow = true;
            this.GridView1.OptionsView.EnableAppearanceOddRow = true;
            this.GridView1.OptionsView.ShowGroupPanel = false;
            this.GridView1.OptionsView.ShowIndicator = false;
            this.GridView1.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] { new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.GridColumn_date_created, DevExpress.Data.ColumnSortOrder.Descending) });
            //
            //GroupControl1
            //
            this.GroupControl1.Anchor = (System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) | System.Windows.Forms.AnchorStyles.Right);
            this.GroupControl1.Controls.Add(this.CheckEdit_Alerts);
            this.GroupControl1.Controls.Add(this.CheckEdit_System);
            this.GroupControl1.Controls.Add(this.CheckEdit_Temporary);
            this.GroupControl1.Controls.Add(this.CheckEdit_Perm);
            this.GroupControl1.Location = new System.Drawing.Point(0, 0);
            this.GroupControl1.Name = "GroupControl1";
            this.GroupControl1.Size = new System.Drawing.Size(576, 48);
            this.GroupControl1.TabIndex = 1;
            this.GroupControl1.Text = "Show Notes of the following type(s)";
            //
            //CheckEdit_Alerts
            //
            this.CheckEdit_Alerts.EditValue = true;
            this.CheckEdit_Alerts.Location = new System.Drawing.Point(162, 24);
            this.CheckEdit_Alerts.Name = "CheckEdit_Alerts";
            this.CheckEdit_Alerts.Properties.Caption = "Alerts";
            this.CheckEdit_Alerts.Size = new System.Drawing.Size(80, 20);
            this.CheckEdit_Alerts.TabIndex = 4;
            //
            //CheckEdit_System
            //
            this.CheckEdit_System.EditValue = true;
            this.CheckEdit_System.Location = new System.Drawing.Point(81, 24);
            this.CheckEdit_System.Name = "CheckEdit_System";
            this.CheckEdit_System.Properties.Caption = "System";
            this.CheckEdit_System.Size = new System.Drawing.Size(80, 20);
            this.CheckEdit_System.TabIndex = 1;
            //
            //CheckEdit_Temporary
            //
            this.CheckEdit_Temporary.EditValue = true;
            this.CheckEdit_Temporary.Location = new System.Drawing.Point(243, 24);
            this.CheckEdit_Temporary.Name = "CheckEdit_Temporary";
            this.CheckEdit_Temporary.Properties.Caption = "Temporary";
            this.CheckEdit_Temporary.Size = new System.Drawing.Size(80, 20);
            this.CheckEdit_Temporary.TabIndex = 3;
            //
            //CheckEdit_Perm
            //
            this.CheckEdit_Perm.EditValue = true;
            this.CheckEdit_Perm.Location = new System.Drawing.Point(0, 24);
            this.CheckEdit_Perm.Name = "CheckEdit_Perm";
            this.CheckEdit_Perm.Properties.Caption = "Permanent";
            this.CheckEdit_Perm.Size = new System.Drawing.Size(80, 20);
            this.CheckEdit_Perm.TabIndex = 0;
            //
            //GridColumn_date_created
            //
            this.GridColumn_date_created.Caption = "Date";
            this.GridColumn_date_created.CustomizationCaption = "Date and Time";
            this.GridColumn_date_created.DisplayFormat.FormatString = "M/d/yyyy h:mm tt";
            this.GridColumn_date_created.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.GridColumn_date_created.FieldName = "date_created";
            this.GridColumn_date_created.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.Date;
            this.GridColumn_date_created.Name = "GridColumn_date_created";
            this.GridColumn_date_created.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.GridColumn_date_created.SortMode = DevExpress.XtraGrid.ColumnSortMode.Value;
            this.GridColumn_date_created.Visible = true;
            this.GridColumn_date_created.VisibleIndex = 0;
            this.GridColumn_date_created.Width = 135;
            //
            //GridColumn_type
            //
            this.GridColumn_type.AppearanceCell.Options.UseTextOptions = true;
            this.GridColumn_type.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.GridColumn_type.AppearanceHeader.Options.UseTextOptions = true;
            this.GridColumn_type.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.GridColumn_type.Caption = "Type";
            this.GridColumn_type.CustomizationCaption = "Note Type";
            this.GridColumn_type.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.GridColumn_type.FieldName = "type";
            this.GridColumn_type.GroupFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.GridColumn_type.Name = "GridColumn_type";
            this.GridColumn_type.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.GridColumn_type.Visible = true;
            this.GridColumn_type.VisibleIndex = 1;
            this.GridColumn_type.Width = 93;
            //
            //GridColumn_Counselor
            //
            this.GridColumn_Counselor.Caption = "Counselor";
            this.GridColumn_Counselor.CustomizationCaption = "Counselor";
            this.GridColumn_Counselor.FieldName = "created_by";
            this.GridColumn_Counselor.Name = "GridColumn_Counselor";
            this.GridColumn_Counselor.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.GridColumn_Counselor.Visible = true;
            this.GridColumn_Counselor.VisibleIndex = 2;
            this.GridColumn_Counselor.Width = 108;
            //
            //GridColumn_Subject
            //
            this.GridColumn_Subject.Caption = "Subject";
            this.GridColumn_Subject.CustomizationCaption = "Subject";
            this.GridColumn_Subject.FieldName = "subject";
            this.GridColumn_Subject.Name = "GridColumn_Subject";
            this.GridColumn_Subject.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.GridColumn_Subject.Visible = true;
            this.GridColumn_Subject.VisibleIndex = 3;
            this.GridColumn_Subject.Width = 263;
            //
            //GridColumn_ID
            //
            this.GridColumn_ID.Caption = "ID";
            this.GridColumn_ID.CustomizationCaption = "Record ID";
            this.GridColumn_ID.DisplayFormat.FormatString = "f0";
            this.GridColumn_ID.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.GridColumn_ID.FieldName = "Id";
            this.GridColumn_ID.Name = "GridColumn_ID";
            this.GridColumn_ID.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            //
            //GridColumn_dont_edit
            //
            this.GridColumn_dont_edit.Caption = "No Edit?";
            this.GridColumn_dont_edit.CustomizationCaption = "Disallow Edit?";
            this.GridColumn_dont_edit.FieldName = "dont_edit";
            this.GridColumn_dont_edit.Name = "GridColumn_dont_edit";
            this.GridColumn_dont_edit.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            //
            //GridColumn_dont_delete
            //
            this.GridColumn_dont_delete.Caption = "No Delete?";
            this.GridColumn_dont_delete.CustomizationCaption = "Disallow Delete?";
            this.GridColumn_dont_delete.FieldName = "dont_delete";
            this.GridColumn_dont_delete.Name = "GridColumn_dont_delete";
            this.GridColumn_dont_delete.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            //
            //GridColumn_dont_print
            //
            this.GridColumn_dont_print.Caption = "No Print?";
            this.GridColumn_dont_print.CustomizationCaption = "Disallow Print?";
            this.GridColumn_dont_print.FieldName = "dont_print";
            this.GridColumn_dont_print.Name = "GridColumn_dont_print";
            this.GridColumn_dont_print.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            //
            //GridColumn_Source
            //
            this.GridColumn_Source.Caption = "Source";
            this.GridColumn_Source.CustomizationCaption = "Record Source";
            this.GridColumn_Source.DisplayFormat.FormatString = "f0";
            this.GridColumn_Source.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.GridColumn_Source.FieldName = "source";
            this.GridColumn_Source.Name = "GridColumn_Source";
            this.GridColumn_Source.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            //
            //NotesGrid
            //
            this.Controls.Add(this.GroupControl1);
            this.Name = "NotesGrid";
            this.Controls.SetChildIndex(this.barDockControlTop, 0);
            this.Controls.SetChildIndex(this.barDockControlBottom, 0);
            this.Controls.SetChildIndex(this.barDockControlRight, 0);
            this.Controls.SetChildIndex(this.barDockControlLeft, 0);
            this.Controls.SetChildIndex(this.GroupControl1, 0);
            this.Controls.SetChildIndex(this.GridControl1, 0);
            ((System.ComponentModel.ISupportInitialize)this.barManager1).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.PopupMenu_Items).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.GridControl1).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.GridView1).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.GroupControl1).EndInit();
            this.GroupControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)this.CheckEdit_Alerts.Properties).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.CheckEdit_System.Properties).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.CheckEdit_Temporary.Properties).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.CheckEdit_Perm.Properties).EndInit();
            this.ResumeLayout(false);
        }

        #endregion " Windows Form Designer generated code "
    }
}
