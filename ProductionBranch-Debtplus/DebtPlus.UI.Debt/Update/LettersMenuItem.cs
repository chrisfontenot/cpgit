using System;
using System.Reflection;
using System.Threading;
using System.Windows.Forms;
using DebtPlus.Events;
using DebtPlus.Letters.Compose;
using DevExpress.XtraBars;

namespace DebtPlus.UI.Debt.Update
{
    internal partial class LettersMenuItem : BarButtonItem
    {
        private DebtPlus.LINQ.letter_type currentLetter;
        private DebtPlus.Interfaces.Debt.IDebtRecord currentDebtRecord;

        public LettersMenuItem() : base()
        {
        }

        public LettersMenuItem(string text) : this()
        {
            Caption = text;
        }

        public LettersMenuItem(DebtPlus.Interfaces.Debt.IDebtRecord CurrentDebtRecord, string Name, DebtPlus.LINQ.letter_type CurrentLetter) : this(Name)
        {
            this.currentDebtRecord = CurrentDebtRecord;
            this.currentLetter = CurrentLetter;
        }

        public LettersMenuItem(string text, ItemClickEventHandler OnClickHandler) : this(text)
        {
            ItemClick += OnClickHandler;
        }

        public LettersMenuItem(string text, ItemClickEventHandler OnClickHandler, Shortcut Shortcut) : this(text, OnClickHandler)
        {
            ItemShortcut = new BarShortcut(Shortcut);
        }

        /// <summary>
        /// Handle the CLICK event
        /// </summary>
        protected override void OnClick(BarItemLink link)
        {
            base.OnClick(link);

            if (currentLetter != null && currentDebtRecord != null)
            {
                // Start a thread to process the letter and the class. Just let it run.
                Thread thrd = new Thread(new System.Threading.ParameterizedThreadStart(Show))
                {
                    IsBackground = false,
                    Name = "Letters"
                };
                thrd.SetApartmentState(ApartmentState.STA);
                thrd.Start(null);
            }
        }

        private void GetField(object sender, DebtPlus.Events.ParameterValueEventArgs e)
        {
            // The debt number is special. It is our primary key to the data.
            if (e.Name == ParameterValueEventArgs.name_debt)
            {
                e.Value = currentDebtRecord.client_creditor;
            }
            else if (e.Name == ParameterValueEventArgs.name_client)
            {
                e.Value = currentDebtRecord.ClientId;
            }
            else if (e.Name == ParameterValueEventArgs.name_creditor)
            {
                e.Value = currentDebtRecord.Creditor;
            }
            else
            {
                // Look for specific names that we might use as an alias
                switch (e.Name.ToLower())
                {
                    case "balance":
                    case "client_creditor.balance":
                        decimal balance = DebtPlus.Utils.Nulls.DDec(currentDebtRecord.orig_balance) + DebtPlus.Utils.Nulls.DDec(currentDebtRecord.orig_balance_adjustment) + DebtPlus.Utils.Nulls.DDec(currentDebtRecord.total_interest) - DebtPlus.Utils.Nulls.DDec(currentDebtRecord.total_payments);
                        if (balance < 0m)
                        {
                            balance = 0m;
                        }
                        e.Value = balance;
                        break;

                    default:
                        try
                        {
                            // All others, try to obtain the information from the debt record for this item

                            // Split the field name from the table modifier
                            string ModifiedName = e.Name.ToLower();
                            string[] Fields = ModifiedName.Split('.');
                            if (Fields.GetUpperBound(0) >= 0)
                            {
                                ModifiedName = Fields[Fields.GetUpperBound(0)];
                            }

                            // Try to find the information in the debt record.
                            PropertyInfo pi = currentDebtRecord.GetType().GetProperty(ModifiedName);
                            if (pi != null)
                            {
                                e.Value = pi.GetValue(currentDebtRecord, null);
                            }
                        }
                        catch { }
                        break;
                }
            }
        }

        private void Show(object obj)
        {
            using (Composition ltr = new Composition(currentLetter.Id))
            {
                ltr.GetValue += GetField;
                ltr.PrintLetter();
                ltr.GetValue -= GetField;
            }
        }
    }
}
