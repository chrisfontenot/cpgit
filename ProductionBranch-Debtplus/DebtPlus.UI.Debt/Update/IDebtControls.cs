namespace DebtPlus.UI.Debt.CS.Update
{
    internal interface IDebtControls
    {
        void ReadForm(DebtPlus.Interfaces.Debt.IDebtRecord DebtRecord);
        void SaveForm();
    }
}