using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;

namespace DebtPlus.UI.Debt.Update.forms
{
	partial class Form_BalanceVerified : DebtPlus.Data.Forms.DebtPlusForm
	{

		//Form overrides dispose to clean up the component list.
		[System.Diagnostics.DebuggerNonUserCode()]
		protected override void Dispose(bool disposing)
		{
			if (disposing && components != null) {
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		//Required by the Windows Form Designer

		private System.ComponentModel.IContainer components = null;
		//NOTE: The following procedure is required by the Windows Form Designer
		//It can be modified using the Windows Form Designer.  
		//Do not modify it using the code editor.
		[System.Diagnostics.DebuggerStepThrough()]
		private void InitializeComponent()
		{
			this.LabelControl1 = new DevExpress.XtraEditors.LabelControl();
			this.LabelControl2 = new DevExpress.XtraEditors.LabelControl();
			this.LabelControl3 = new DevExpress.XtraEditors.LabelControl();
			this.SimpleButton_verify = new DevExpress.XtraEditors.SimpleButton();
			this.SimpleButton_unverified = new DevExpress.XtraEditors.SimpleButton();
			this.SimpleButton_cancel = new DevExpress.XtraEditors.SimpleButton();
			this.DateEdit_verified_date = new DevExpress.XtraEditors.DateEdit();
			this.TextEdit_verified_by = new DevExpress.XtraEditors.TextEdit();
			((System.ComponentModel.ISupportInitialize)this.DxErrorProvider1).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.DateEdit_verified_date.Properties.VistaTimeProperties).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.DateEdit_verified_date.Properties).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.TextEdit_verified_by.Properties).BeginInit();
			this.SuspendLayout();
			//
			//LabelControl1
			//
			this.LabelControl1.Location = new System.Drawing.Point(13, 13);
			this.LabelControl1.Name = "LabelControl1";
			this.LabelControl1.Size = new System.Drawing.Size(350, 13);
			this.LabelControl1.TabIndex = 0;
			this.LabelControl1.Text = "Please enter the information for verifying the balance in the fields below.";
			//
			//LabelControl2
			//
			this.LabelControl2.Location = new System.Drawing.Point(13, 51);
			this.LabelControl2.Name = "LabelControl2";
			this.LabelControl2.Size = new System.Drawing.Size(51, 13);
			this.LabelControl2.TabIndex = 1;
			this.LabelControl2.Text = "Verified &By";
			//
			//LabelControl3
			//
			this.LabelControl3.Location = new System.Drawing.Point(13, 83);
			this.LabelControl3.Name = "LabelControl3";
			this.LabelControl3.Size = new System.Drawing.Size(69, 13);
			this.LabelControl3.TabIndex = 3;
			this.LabelControl3.Text = "Effective &Date";
			//
			//SimpleButton_verify
			//
			this.SimpleButton_verify.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
			this.SimpleButton_verify.DialogResult = System.Windows.Forms.DialogResult.Yes;
			this.SimpleButton_verify.Enabled = false;
			this.SimpleButton_verify.Location = new System.Drawing.Point(31, 122);
			this.SimpleButton_verify.Name = "SimpleButton_verify";
			this.SimpleButton_verify.Size = new System.Drawing.Size(75, 23);
			this.SimpleButton_verify.TabIndex = 5;
			this.SimpleButton_verify.Text = "&Verify";
			this.SimpleButton_verify.ToolTip = "Click here to mark the balance as having been verified";
			//
			//SimpleButton_unverified
			//
			this.SimpleButton_unverified.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
			this.SimpleButton_unverified.DialogResult = System.Windows.Forms.DialogResult.No;
			this.SimpleButton_unverified.Location = new System.Drawing.Point(150, 122);
			this.SimpleButton_unverified.Name = "SimpleButton_unverified";
			this.SimpleButton_unverified.Size = new System.Drawing.Size(75, 23);
			this.SimpleButton_unverified.TabIndex = 6;
			this.SimpleButton_unverified.Text = "&Un-Verified";
			this.SimpleButton_unverified.ToolTip = "Click here to remove the previous verified status";
			//
			//SimpleButton_cancel
			//
			this.SimpleButton_cancel.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
			this.SimpleButton_cancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			this.SimpleButton_cancel.Location = new System.Drawing.Point(269, 122);
			this.SimpleButton_cancel.Name = "SimpleButton_cancel";
			this.SimpleButton_cancel.Size = new System.Drawing.Size(75, 23);
			this.SimpleButton_cancel.TabIndex = 7;
			this.SimpleButton_cancel.Text = "&Cancel";
			this.SimpleButton_cancel.ToolTip = "Click here to make no changes to the balance verification status. Verified balanc" + "es are still left verified. Non-Verified balances are left as non-verified.";
			//
			//DateEdit_verified_date
			//
			this.DateEdit_verified_date.EditValue = null;
			this.DateEdit_verified_date.Location = new System.Drawing.Point(106, 79);
			this.DateEdit_verified_date.Name = "DateEdit_verified_date";
			this.DateEdit_verified_date.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] { new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo) });
			this.DateEdit_verified_date.Properties.VistaEditTime = DevExpress.Utils.DefaultBoolean.False;
			this.DateEdit_verified_date.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] { new DevExpress.XtraEditors.Controls.EditorButton() });
			this.DateEdit_verified_date.Size = new System.Drawing.Size(100, 20);
			this.DateEdit_verified_date.TabIndex = 4;
			this.DateEdit_verified_date.ToolTip = "This is normally the date that the balance verification was made but it may be an" + "y valid non-null date.";
			//
			//TextEdit_verified_by
			//
			this.TextEdit_verified_by.Anchor = (System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) | System.Windows.Forms.AnchorStyles.Right);
			this.TextEdit_verified_by.Location = new System.Drawing.Point(106, 47);
			this.TextEdit_verified_by.Name = "TextEdit_verified_by";
			this.TextEdit_verified_by.Properties.MaxLength = 50;
			this.TextEdit_verified_by.Size = new System.Drawing.Size(252, 20);
			this.TextEdit_verified_by.TabIndex = 2;
			this.TextEdit_verified_by.ToolTip = "This is the name of the person or agency who supplied the current balance to you." + " IT IS NOT YOUR NAME, but the name of the agency who gave you the balance.";
			//
			//Form_BalanceVerified
			//
			this.AcceptButton = this.SimpleButton_verify;
			this.AutoScaleDimensions = new System.Drawing.SizeF(6f, 13f);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.CancelButton = this.SimpleButton_cancel;
			this.ClientSize = new System.Drawing.Size(374, 160);
			this.Controls.Add(this.TextEdit_verified_by);
			this.Controls.Add(this.DateEdit_verified_date);
			this.Controls.Add(this.SimpleButton_cancel);
			this.Controls.Add(this.SimpleButton_unverified);
			this.Controls.Add(this.SimpleButton_verify);
			this.Controls.Add(this.LabelControl3);
			this.Controls.Add(this.LabelControl2);
			this.Controls.Add(this.LabelControl1);
			this.LookAndFeel.UseDefaultLookAndFeel = true;
			this.Name = "Form_BalanceVerified";
			this.Text = "Balance Verification Status";
			((System.ComponentModel.ISupportInitialize)this.DxErrorProvider1).EndInit();
			((System.ComponentModel.ISupportInitialize)this.DateEdit_verified_date.Properties.VistaTimeProperties).EndInit();
			((System.ComponentModel.ISupportInitialize)this.DateEdit_verified_date.Properties).EndInit();
			((System.ComponentModel.ISupportInitialize)this.TextEdit_verified_by.Properties).EndInit();
			this.ResumeLayout(false);
			this.PerformLayout();

		}
		internal DevExpress.XtraEditors.LabelControl LabelControl1;
		internal DevExpress.XtraEditors.LabelControl LabelControl2;
		internal DevExpress.XtraEditors.LabelControl LabelControl3;
		internal DevExpress.XtraEditors.SimpleButton SimpleButton_verify;
		internal DevExpress.XtraEditors.SimpleButton SimpleButton_unverified;
		internal DevExpress.XtraEditors.SimpleButton SimpleButton_cancel;
		internal DevExpress.XtraEditors.DateEdit DateEdit_verified_date;
		internal DevExpress.XtraEditors.TextEdit TextEdit_verified_by;
	}
}


