#region "Copyright 2000-2012 DebtPlus, L.L.C."

//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}

#endregion "Copyright 2000-2012 DebtPlus, L.L.C."

using System;
using System.Diagnostics;
using DebtPlus.Data.Forms;
using DebtPlus.LINQ;

namespace DebtPlus.UI.Debt.Update.forms
{
    internal partial class Form_Event : DebtPlusForm
    {
        private System.Collections.Generic.List<DebtPlus.LINQ.client_creditor_event> colClientCreditorEvents;
        private System.Collections.Generic.List<DebtPlus.LINQ.config_fee> colConfigFee;

        // Pointer to the record to be updated
        private DebtPlus.LINQ.client_creditor_event evt;

        private bool Locked;

        public Form_Event()
            : base()
        {
            InitializeComponent();

            // Add the "Handles" clauses here
            Load            += Form_Message_Load;
            Button_OK.Click += Button_OK_Click;
        }

        internal Form_Event(System.Collections.Generic.List<DebtPlus.LINQ.client_creditor_event> colClientCreditorEvents, System.Collections.Generic.List<DebtPlus.LINQ.config_fee> colConfigFee, DebtPlus.LINQ.client_creditor_event evt)
            : this()
        {
            this.evt                     = evt;
            this.colClientCreditorEvents = colClientCreditorEvents;
            this.colConfigFee            = colConfigFee;
        }

        #region " Windows Form Designer generated code "

        internal DevExpress.XtraEditors.SimpleButton Button_Cancel;
        internal DevExpress.XtraEditors.SimpleButton Button_OK;
        internal DevExpress.XtraEditors.LookUpEdit config_fee;
        internal DevExpress.XtraEditors.DateEdit effective_date;
        internal DevExpress.XtraEditors.LabelControl LabelControl2;
        internal DevExpress.XtraEditors.LabelControl LabelControl3;
        internal DevExpress.XtraEditors.LabelControl LabelControl4;
        internal DevExpress.XtraEditors.CalcEdit value;

        //NOTE: The following procedure is required by the Windows Form Designer
        //It can be modified using the Windows Form Designer.
        //Do not modify it using the code editor.
        private System.ComponentModel.IContainer components = null;

        //Form overrides dispose to clean up the component list.
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (components != null)
                {
                    components.Dispose();
                }
            }
            base.Dispose(disposing);
        }

        //Required by the Windows Form Designer
        [System.Diagnostics.DebuggerStepThrough()]
        private void InitializeComponent()
        {
            this.LabelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.LabelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.LabelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.Button_OK = new DevExpress.XtraEditors.SimpleButton();
            this.Button_Cancel = new DevExpress.XtraEditors.SimpleButton();
            this.value = new DevExpress.XtraEditors.CalcEdit();
            this.effective_date = new DevExpress.XtraEditors.DateEdit();
            this.config_fee = new DevExpress.XtraEditors.LookUpEdit();
            ((System.ComponentModel.ISupportInitialize)this.DxErrorProvider1).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.value.Properties).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.effective_date.Properties.VistaTimeProperties).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.effective_date.Properties).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.config_fee.Properties).BeginInit();
            this.SuspendLayout();
            //
            //DefaultLookAndFeel1
            //
            this.DefaultLookAndFeel1.LookAndFeel.SkinName = "McSkin";
            //
            //LabelControl2
            //
            this.LabelControl2.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.Horizontal;
            this.LabelControl2.Location = new System.Drawing.Point(8, 20);
            this.LabelControl2.Name = "LabelControl2";
            this.LabelControl2.Size = new System.Drawing.Size(49, 13);
            this.LabelControl2.TabIndex = 0;
            this.LabelControl2.Text = "Fee Type:";
            //
            //LabelControl3
            //
            this.LabelControl3.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.Horizontal;
            this.LabelControl3.Location = new System.Drawing.Point(8, 52);
            this.LabelControl3.Name = "LabelControl3";
            this.LabelControl3.Size = new System.Drawing.Size(27, 13);
            this.LabelControl3.TabIndex = 2;
            this.LabelControl3.Text = "Date:";
            //
            //LabelControl4
            //
            this.LabelControl4.Anchor = (System.Windows.Forms.AnchorStyles)(System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right);
            this.LabelControl4.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.Horizontal;
            this.LabelControl4.Location = new System.Drawing.Point(192, 52);
            this.LabelControl4.Name = "LabelControl4";
            this.LabelControl4.Size = new System.Drawing.Size(37, 13);
            this.LabelControl4.TabIndex = 4;
            this.LabelControl4.Text = "Amount";
            //
            //Button_OK
            //
            this.Button_OK.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.Button_OK.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.Button_OK.Location = new System.Drawing.Point(104, 88);
            this.Button_OK.Name = "Button_OK";
            this.Button_OK.Size = new System.Drawing.Size(75, 23);
            this.Button_OK.TabIndex = 6;
            this.Button_OK.Text = "&OK";
            //
            //Button_Cancel
            //
            this.Button_Cancel.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.Button_Cancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.Button_Cancel.Location = new System.Drawing.Point(192, 88);
            this.Button_Cancel.Name = "Button_Cancel";
            this.Button_Cancel.Size = new System.Drawing.Size(75, 23);
            this.Button_Cancel.TabIndex = 7;
            this.Button_Cancel.Text = "&Cancel";
            //
            //value
            //
            this.value.Anchor = (System.Windows.Forms.AnchorStyles)(System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right);
            this.value.Location = new System.Drawing.Point(236, 48);
            this.value.Name = "value";
            this.value.Properties.Appearance.Options.UseTextOptions = true;
            this.value.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.value.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] { new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo) });
            this.value.Properties.DisplayFormat.FormatString = "c2";
            this.value.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.value.Properties.EditFormat.FormatString = "f2";
            this.value.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.value.Properties.Mask.BeepOnError = true;
            this.value.Properties.Mask.EditMask = "c";
            this.value.Properties.Precision = 2;
            this.value.Size = new System.Drawing.Size(100, 20);
            this.value.TabIndex = 5;
            //
            //effective_date
            //
            this.effective_date.EditValue = null;
            this.effective_date.Location = new System.Drawing.Point(80, 48);
            this.effective_date.Name = "effective_date";
            this.effective_date.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] { new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo) });
            this.effective_date.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] { new DevExpress.XtraEditors.Controls.EditorButton() });
            this.effective_date.Size = new System.Drawing.Size(100, 20);
            this.effective_date.TabIndex = 3;
            //
            //config_fee
            //
            this.config_fee.Anchor = (System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) | System.Windows.Forms.AnchorStyles.Right);
            this.config_fee.Location = new System.Drawing.Point(80, 16);
            this.config_fee.Name = "config_fee";
            this.config_fee.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.False;
            this.config_fee.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] { new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo) });
            this.config_fee.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
				new DevExpress.XtraEditors.Controls.LookUpColumnInfo("config_fee", "Fee ID", 30, DevExpress.Utils.FormatType.Numeric, "f0", false, DevExpress.Utils.HorzAlignment.Far),
				new DevExpress.XtraEditors.Controls.LookUpColumnInfo("description", "Description", 100, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Default, DevExpress.Data.ColumnSortOrder.Ascending)
			});
            this.config_fee.Properties.DisplayMember = "description";
            this.config_fee.Properties.NullText = "Choose a fee calculation from below...";
            this.config_fee.Properties.ShowFooter = false;
            this.config_fee.Properties.ShowHeader = false;
            this.config_fee.Properties.ShowLines = false;
            this.config_fee.Properties.SortColumnIndex = 1;
            this.config_fee.Properties.ValueMember = "config_fee";
            this.config_fee.Size = new System.Drawing.Size(256, 20);
            this.config_fee.TabIndex = 1;
            this.config_fee.ToolTip = "Choose the appropriate fee calculation for this client";
            //
            //Form_Event
            //
            this.AcceptButton = this.Button_OK;
            this.AutoScaleBaseSize = new System.Drawing.Size(5, 14);
            this.CancelButton = this.Button_Cancel;
            this.ClientSize = new System.Drawing.Size(352, 126);
            this.Controls.Add(this.config_fee);
            this.Controls.Add(this.effective_date);
            this.Controls.Add(this.value);
            this.Controls.Add(this.Button_Cancel);
            this.Controls.Add(this.Button_OK);
            this.Controls.Add(this.LabelControl4);
            this.Controls.Add(this.LabelControl3);
            this.Controls.Add(this.LabelControl2);
            this.Name = "Form_Event";
            this.Text = "Scheduled Events on Client Debts";
            ((System.ComponentModel.ISupportInitialize)this.DxErrorProvider1).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.value.Properties).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.effective_date.Properties.VistaTimeProperties).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.effective_date.Properties).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.config_fee.Properties).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();
        }

        #endregion " Windows Form Designer generated code "

        private void Button_OK_Click(System.Object sender, System.EventArgs e)
        {
            if (!Locked)
            {
                evt.effective_date = Convert.ToDateTime(effective_date.EditValue);
                evt.value          = Convert.ToDecimal(value.EditValue);
                evt.config_fee     = Convert.ToInt32(config_fee.EditValue);
            }
        }

        private void Form_Message_Load(object sender, EventArgs e)
        {
            // There should be a record pointer.
            Debug.Assert(evt != null);

            // Set the read-only status to TRUE if the effective date is too early
            System.DateTime EffectiveDate = evt.effective_date;
            Locked                        = System.DateTime.Compare(EffectiveDate.Date, System.DateTime.Now) <= 0;

            config_fee.Properties.DataSource    = colConfigFee;
            config_fee.Properties.ReadOnly      = Locked;
            config_fee.Properties.DisplayMember = "description";
            config_fee.Properties.ValueMember   = "Id";
            config_fee.EditValue                = evt.config_fee;

            value.Properties.ReadOnly = Locked;
            value.EditValue = evt.value;

            if (!Locked)
            {
                effective_date.Properties.MinValue = System.DateTime.Now.AddDays(1).Date;
            }
            effective_date.Properties.ReadOnly = Locked;
            effective_date.EditValue           = evt.effective_date;

            // Add the handlers for the update events
            config_fee.EditValueChanged     += FormChanged;
            effective_date.EditValueChanged += FormChanged;
            value.EditValueChanged          += FormChanged;
            config_fee.EditValueChanging    += DebtPlus.Data.Validation.LookUpEdit_ActiveTest;

            // There must be a message text or we can't accept the form
            Button_OK.Enabled = !HasErrors();
        }

        private void FormChanged(object sender, EventArgs e)
        {
            Button_OK.Enabled = !HasErrors();
        }

        private bool HasErrors()
        {
            if (Locked)
            {
                return true;
            }

            if (config_fee.EditValue == null)
            {
                return true;
            }

            if (effective_date.EditValue == null)
            {
                return true;
            }
            return false;
        }
    }
}
