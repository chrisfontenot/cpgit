#region "Copyright 2000-2012 DebtPlus, L.L.C."

//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}

#endregion "Copyright 2000-2012 DebtPlus, L.L.C."

using System;
using System.Linq;
using DebtPlus.Data.Forms;
using DevExpress.XtraEditors.Controls;

namespace DebtPlus.UI.Debt.Update.forms
{
    internal partial class Form_ProposalUpdate : DebtPlusForm
    {
        private DebtPlus.LINQ.client_creditor_proposal record;

        public Form_ProposalUpdate()
            : base()
        {
            InitializeComponent();
        }

        public Form_ProposalUpdate(DebtPlus.LINQ.client_creditor_proposal record)
            : this()
        {
            this.record = record;

            // Define the lookup types
            proposal_status.Properties.DataSource        = DebtPlus.LINQ.Cache.ProposalStatusType.getList();
            proposal_reject_reason.Properties.DataSource = DebtPlus.LINQ.Cache.proposal_result_reason.getList();
            proposal_reject_disp.Properties.DataSource   = DebtPlus.LINQ.Cache.proposal_result_disposition.getList();

            // Add the message values
            message_list.Properties.Items.Clear();
            foreach (DebtPlus.LINQ.proposal_message item in DebtPlus.LINQ.Cache.proposal_message.getList().OrderBy(s => s.message_text).ToList())
            {
                message_list.Properties.Items.Add(new DebtPlus.Data.Controls.ComboboxItem(item.message_text, item.Id, true));
            }

            RegisterHandlers();
        }

        internal void RegisterHandlers()
        {
            proposal_status.EditValueChanging        += DebtPlus.Data.Validation.LookUpEdit_ActiveTest;
            proposal_status.EditValueChanging        += proposal_status_EditValueChanging;
            proposal_status.EditValueChanged         += FormChanged;
            proposal_status.EditValueChanged         += proposal_status_EditValueChanged;
            proposal_reject_reason.EditValueChanging += DebtPlus.Data.Validation.LookUpEdit_ActiveTest;
            proposal_reject_reason.EditValueChanged  += FormChanged;
            proposal_reject_disp.EditValueChanging   += DebtPlus.Data.Validation.LookUpEdit_ActiveTest;
            proposal_reject_disp.EditValueChanged    += FormChanged;
            proposal_accepted_by.EditValueChanged    += FormChanged;
            proposal_note.EditValueChanged           += FormChanged;
            proposal_status_date.EditValueChanged    += FormChanged;
            proposed_date.EditValueChanged           += FormChanged;
            message_list.SelectedIndexChanged        += message_list_SelectedIndexChanged;
            message_list.EditValueChanged            += FormChanged;
            Load                                     += Form_Load;
            Button_OK.Click                          += Button_OK_Click;
        }

        internal void UnRegisterHandlers()
        {
            proposal_status.EditValueChanging        -= DebtPlus.Data.Validation.LookUpEdit_ActiveTest;
            proposal_status.EditValueChanging        -= proposal_status_EditValueChanging;
            proposal_status.EditValueChanged         -= FormChanged;
            proposal_status.EditValueChanged         -= proposal_status_EditValueChanged;
            proposal_reject_reason.EditValueChanging -= DebtPlus.Data.Validation.LookUpEdit_ActiveTest;
            proposal_reject_reason.EditValueChanged  -= FormChanged;
            proposal_reject_disp.EditValueChanging   -= DebtPlus.Data.Validation.LookUpEdit_ActiveTest;
            proposal_reject_disp.EditValueChanged    -= FormChanged;
            proposal_accepted_by.EditValueChanged    -= FormChanged;
            proposal_note.EditValueChanged           -= FormChanged;
            proposal_status_date.EditValueChanged    -= FormChanged;
            proposed_date.EditValueChanged           -= FormChanged;
            message_list.SelectedIndexChanged        -= message_list_SelectedIndexChanged;
            message_list.EditValueChanged            -= FormChanged;
            Load                                     -= Form_Load;
            Button_OK.Click                          -= Button_OK_Click;
        }

        #region " Windows Form Designer generated code "

        internal DevExpress.XtraEditors.SimpleButton Button_Cancel;

        internal DevExpress.XtraEditors.SimpleButton Button_OK;

        internal DevExpress.XtraEditors.LabelControl Disposition;

        //NOTE: The following procedure is required by the Windows Form Designer
        //It can be modified using the Windows Form Designer.
        //Do not modify it using the code editor.
        internal DevExpress.XtraEditors.LabelControl LabelControl2;

        internal DevExpress.XtraEditors.LabelControl LabelControl3;

        internal DevExpress.XtraEditors.LabelControl LabelControl4;

        internal DevExpress.XtraEditors.LabelControl LabelControl5;

        internal DevExpress.XtraEditors.LabelControl LabelControl6;

        internal DevExpress.XtraEditors.LabelControl LabelControl7;

        internal DevExpress.XtraEditors.ComboBoxEdit message_list;

        internal DevExpress.XtraEditors.TextEdit proposal_accepted_by;

        internal DevExpress.XtraEditors.MemoEdit proposal_note;

        internal DevExpress.XtraEditors.LookUpEdit proposal_reject_disp;

        internal DevExpress.XtraEditors.LookUpEdit proposal_reject_reason;

        internal DevExpress.XtraEditors.LookUpEdit proposal_status;

        internal DevExpress.XtraEditors.DateEdit proposal_status_date;

        internal DevExpress.XtraEditors.DateEdit proposed_date;

        internal DevExpress.XtraEditors.LabelControl status_area;

        private System.ComponentModel.IContainer components = null;

        //Form overrides dispose to clean up the component list.
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (components != null)
                {
                    components.Dispose();
                }
            }
            base.Dispose(disposing);
        }

        //Required by the Windows Form Designer
        [System.Diagnostics.DebuggerStepThrough()]
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form_ProposalUpdate));
            this.status_area = new DevExpress.XtraEditors.LabelControl();
            this.LabelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.LabelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.proposal_status_date = new DevExpress.XtraEditors.DateEdit();
            this.proposed_date = new DevExpress.XtraEditors.DateEdit();
            this.LabelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.LabelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.proposal_reject_reason = new DevExpress.XtraEditors.LookUpEdit();
            this.proposal_reject_disp = new DevExpress.XtraEditors.LookUpEdit();
            this.Disposition = new DevExpress.XtraEditors.LabelControl();
            this.proposal_status = new DevExpress.XtraEditors.LookUpEdit();
            this.LabelControl6 = new DevExpress.XtraEditors.LabelControl();
            this.proposal_accepted_by = new DevExpress.XtraEditors.TextEdit();
            this.LabelControl7 = new DevExpress.XtraEditors.LabelControl();
            this.proposal_note = new DevExpress.XtraEditors.MemoEdit();
            this.message_list = new DevExpress.XtraEditors.ComboBoxEdit();
            this.Button_OK = new DevExpress.XtraEditors.SimpleButton();
            this.Button_Cancel = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)this.DxErrorProvider1).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.proposal_status_date.Properties.VistaTimeProperties).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.proposal_status_date.Properties).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.proposed_date.Properties.VistaTimeProperties).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.proposed_date.Properties).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.proposal_reject_reason.Properties).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.proposal_reject_disp.Properties).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.proposal_status.Properties).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.proposal_accepted_by.Properties).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.proposal_note.Properties).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.message_list.Properties).BeginInit();
            this.SuspendLayout();
            //
            //status_area
            //
            this.status_area.Anchor = (System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) | System.Windows.Forms.AnchorStyles.Right);
            this.status_area.Appearance.BackColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(255), (Int32)Convert.ToByte(255), (Int32)Convert.ToByte(192));
            this.status_area.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.status_area.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Top;
            this.status_area.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.status_area.AutoEllipsis = true;
            this.status_area.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.status_area.CausesValidation = false;
            this.status_area.Location = new System.Drawing.Point(8, 4);
            this.status_area.Name = "status_area";
            this.status_area.Size = new System.Drawing.Size(404, 87);
            this.status_area.TabIndex = 0;
            this.status_area.Tag = "";
            this.status_area.UseMnemonic = false;
            //
            //LabelControl2
            //
            this.LabelControl2.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.Horizontal;
            this.LabelControl2.Location = new System.Drawing.Point(8, 100);
            this.LabelControl2.Name = "LabelControl2";
            this.LabelControl2.Size = new System.Drawing.Size(61, 13);
            this.LabelControl2.TabIndex = 12;
            this.LabelControl2.Text = "Status Date:";
            //
            //LabelControl3
            //
            this.LabelControl3.Anchor = (System.Windows.Forms.AnchorStyles)(System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right);
            this.LabelControl3.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.Horizontal;
            this.LabelControl3.Location = new System.Drawing.Point(238, 100);
            this.LabelControl3.Name = "LabelControl3";
            this.LabelControl3.Size = new System.Drawing.Size(73, 13);
            this.LabelControl3.TabIndex = 14;
            this.LabelControl3.Text = "Effective Date:";
            //
            //proposal_status_date
            //
            this.proposal_status_date.EditValue = null;
            this.proposal_status_date.Location = new System.Drawing.Point(128, 97);
            this.proposal_status_date.Name = "proposal_status_date";
            this.proposal_status_date.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.False;
            this.proposal_status_date.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] { new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo) });
            this.proposal_status_date.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] { new DevExpress.XtraEditors.Controls.EditorButton() });
            this.proposal_status_date.Size = new System.Drawing.Size(104, 20);
            this.proposal_status_date.TabIndex = 13;
            this.proposal_status_date.ToolTip = "Date when the proposal was accepted or rejected.";
            //
            //proposed_date
            //
            this.proposed_date.Anchor = (System.Windows.Forms.AnchorStyles)(System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right);
            this.proposed_date.EditValue = null;
            this.proposed_date.Location = new System.Drawing.Point(316, 97);
            this.proposed_date.Name = "proposed_date";
            this.proposed_date.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.False;
            this.proposed_date.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] { new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo) });
            this.proposed_date.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] { new DevExpress.XtraEditors.Controls.EditorButton() });
            this.proposed_date.Size = new System.Drawing.Size(100, 20);
            this.proposed_date.TabIndex = 15;
            this.proposed_date.ToolTip = "Date when the proposal should be sent to the creditor";
            //
            //LabelControl4
            //
            this.LabelControl4.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.Horizontal;
            this.LabelControl4.Location = new System.Drawing.Point(8, 126);
            this.LabelControl4.Name = "LabelControl4";
            this.LabelControl4.Size = new System.Drawing.Size(35, 13);
            this.LabelControl4.TabIndex = 1;
            this.LabelControl4.Text = "Status:";
            //
            //LabelControl5
            //
            this.LabelControl5.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.Horizontal;
            this.LabelControl5.Location = new System.Drawing.Point(8, 152);
            this.LabelControl5.Name = "LabelControl5";
            this.LabelControl5.Size = new System.Drawing.Size(86, 13);
            this.LabelControl5.TabIndex = 3;
            this.LabelControl5.Text = "Rejected Reason:";
            //
            //proposal_reject_reason
            //
            this.proposal_reject_reason.Anchor = (System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) | System.Windows.Forms.AnchorStyles.Right);
            this.proposal_reject_reason.Location = new System.Drawing.Point(128, 149);
            this.proposal_reject_reason.Name = "proposal_reject_reason";
            this.proposal_reject_reason.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.False;
            this.proposal_reject_reason.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] { new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo) });
            this.proposal_reject_reason.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
				new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Id", "Status", 30, DevExpress.Utils.FormatType.Numeric, "f0", false, DevExpress.Utils.HorzAlignment.Far),
				new DevExpress.XtraEditors.Controls.LookUpColumnInfo("description", "Description", 100, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Default, DevExpress.Data.ColumnSortOrder.Ascending)
			});
            this.proposal_reject_reason.Properties.DisplayMember = "description";
            this.proposal_reject_reason.Properties.NullText = "Please choose a rejected reason...";
            this.proposal_reject_reason.Properties.ShowFooter = false;
            this.proposal_reject_reason.Properties.ShowHeader = false;
            this.proposal_reject_reason.Properties.ShowLines = false;
            this.proposal_reject_reason.Properties.SortColumnIndex = 1;
            this.proposal_reject_reason.Properties.ValueMember = "Id";
            this.proposal_reject_reason.Size = new System.Drawing.Size(288, 20);
            this.proposal_reject_reason.TabIndex = 4;
            this.proposal_reject_reason.ToolTip = "If the proposal is rejected, this is the reason why it was rejected.";
            this.proposal_reject_reason.Properties.SortColumnIndex = 1;
            //
            //proposal_reject_disp
            //
            this.proposal_reject_disp.Anchor = (System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) | System.Windows.Forms.AnchorStyles.Right);
            this.proposal_reject_disp.Location = new System.Drawing.Point(128, 175);
            this.proposal_reject_disp.Name = "proposal_reject_disp";
            this.proposal_reject_disp.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.False;
            this.proposal_reject_disp.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] { new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo) });
            this.proposal_reject_disp.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
				new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Id", "Status", 30, DevExpress.Utils.FormatType.Numeric, "f0", false, DevExpress.Utils.HorzAlignment.Far),
				new DevExpress.XtraEditors.Controls.LookUpColumnInfo("description", "Description", 100, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Default, DevExpress.Data.ColumnSortOrder.Ascending)
			});
            this.proposal_reject_disp.Properties.DisplayMember = "description";
            this.proposal_reject_disp.Properties.NullText = "Please choose a disposition reason from below...";
            this.proposal_reject_disp.Properties.ShowFooter = false;
            this.proposal_reject_disp.Properties.ShowHeader = false;
            this.proposal_reject_disp.Properties.ShowLines = false;
            this.proposal_reject_disp.Properties.SortColumnIndex = 1;
            this.proposal_reject_disp.Properties.ValueMember = "Id";
            this.proposal_reject_disp.Size = new System.Drawing.Size(288, 20);
            this.proposal_reject_disp.TabIndex = 6;
            this.proposal_reject_disp.ToolTip = "If the proposal is rejected, this is what you did about the rejected status.";
            this.proposal_reject_disp.Properties.SortColumnIndex = 1;
            //
            //Disposition
            //
            this.Disposition.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.Horizontal;
            this.Disposition.Location = new System.Drawing.Point(8, 178);
            this.Disposition.Name = "Disposition";
            this.Disposition.Size = new System.Drawing.Size(101, 13);
            this.Disposition.TabIndex = 5;
            this.Disposition.Text = "Rejected Disposition:";
            //
            //proposal_status
            //
            this.proposal_status.Anchor = (System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) | System.Windows.Forms.AnchorStyles.Right);
            this.proposal_status.Location = new System.Drawing.Point(128, 123);
            this.proposal_status.Name = "proposal_status";
            this.proposal_status.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] { new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo) });
            this.proposal_status.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
				new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Id", "Status", 30, DevExpress.Utils.FormatType.Numeric, "f0", false, DevExpress.Utils.HorzAlignment.Far, DevExpress.Data.ColumnSortOrder.Ascending),
				new DevExpress.XtraEditors.Controls.LookUpColumnInfo("description", 100, "Description")
			});
            this.proposal_status.Properties.DisplayMember = "description";
            this.proposal_status.Properties.NullText = "You must choose one of the following status values...";
            this.proposal_status.Properties.ShowFooter = false;
            this.proposal_status.Properties.ShowHeader = false;
            this.proposal_status.Properties.ShowLines = false;
            this.proposal_status.Properties.ValueMember = "Id";
            this.proposal_status.Size = new System.Drawing.Size(288, 20);
            this.proposal_status.TabIndex = 2;
            this.proposal_status.ToolTip = "Status for this proposal.";
            this.proposal_status.Properties.SortColumnIndex = 1;
            //
            //LabelControl6
            //
            this.LabelControl6.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.Horizontal;
            this.LabelControl6.Location = new System.Drawing.Point(8, 204);
            this.LabelControl6.Name = "LabelControl6";
            this.LabelControl6.Size = new System.Drawing.Size(64, 13);
            this.LabelControl6.TabIndex = 7;
            this.LabelControl6.Text = "Accepted By:";
            //
            //proposal_accepted_by
            //
            this.proposal_accepted_by.Anchor = (System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) | System.Windows.Forms.AnchorStyles.Right);
            this.proposal_accepted_by.Location = new System.Drawing.Point(128, 201);
            this.proposal_accepted_by.Name = "proposal_accepted_by";
            this.proposal_accepted_by.Size = new System.Drawing.Size(288, 20);
            this.proposal_accepted_by.TabIndex = 8;
            this.proposal_accepted_by.ToolTip = "If the proposal was accepted by the creditor, enter the name of the person AT THE CREDITOR AGENCY who accepted the proposal.";
            //
            //LabelControl7
            //
            this.LabelControl7.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.Horizontal;
            this.LabelControl7.Location = new System.Drawing.Point(8, 229);
            this.LabelControl7.Name = "LabelControl7";
            this.LabelControl7.Size = new System.Drawing.Size(46, 13);
            this.LabelControl7.TabIndex = 9;
            this.LabelControl7.Text = "Message:";
            //
            //proposal_note
            //
            this.proposal_note.Anchor = (System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) | System.Windows.Forms.AnchorStyles.Left) | System.Windows.Forms.AnchorStyles.Right);
            this.proposal_note.Location = new System.Drawing.Point(128, 227);
            this.proposal_note.Name = "proposal_note";
            this.proposal_note.Size = new System.Drawing.Size(288, 66);
            this.proposal_note.TabIndex = 10;
            this.proposal_note.ToolTip = resources.GetString("proposal_note.ToolTip");
            //
            //message_list
            //
            this.message_list.Anchor = (System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) | System.Windows.Forms.AnchorStyles.Right);
            this.message_list.Location = new System.Drawing.Point(128, 301);
            this.message_list.Name = "message_list";
            this.message_list.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] { new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo) });
            this.message_list.Properties.NullText = "If you wish a standard message, choose it here.";
            this.message_list.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.message_list.Size = new System.Drawing.Size(288, 20);
            this.message_list.TabIndex = 11;
            this.message_list.ToolTip = "Choose a standard note from this list if you wish. Or, simply type your note above.";
            //
            //Button_OK
            //
            this.Button_OK.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.Button_OK.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.Button_OK.Location = new System.Drawing.Point(128, 333);
            this.Button_OK.Name = "Button_OK";
            this.Button_OK.Size = new System.Drawing.Size(75, 23);
            this.Button_OK.TabIndex = 16;
            this.Button_OK.Text = "&OK";
            this.Button_OK.ToolTip = "Click here to update the proposal record in the database";
            //
            //Button_Cancel
            //
            this.Button_Cancel.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.Button_Cancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.Button_Cancel.Location = new System.Drawing.Point(224, 333);
            this.Button_Cancel.Name = "Button_Cancel";
            this.Button_Cancel.Size = new System.Drawing.Size(75, 23);
            this.Button_Cancel.TabIndex = 17;
            this.Button_Cancel.Text = "&Cancel";
            this.Button_Cancel.ToolTip = "Click here to cancel any changes that you have made and return to the previous screen.";
            //
            //Form_ProposalUpdate
            //
            this.AcceptButton = this.Button_OK;
            this.CancelButton = this.Button_Cancel;
            this.ClientSize = new System.Drawing.Size(424, 371);
            this.Controls.Add(this.Button_Cancel);
            this.Controls.Add(this.Button_OK);
            this.Controls.Add(this.message_list);
            this.Controls.Add(this.proposal_note);
            this.Controls.Add(this.LabelControl7);
            this.Controls.Add(this.proposal_accepted_by);
            this.Controls.Add(this.LabelControl6);
            this.Controls.Add(this.proposal_status);
            this.Controls.Add(this.Disposition);
            this.Controls.Add(this.proposal_reject_disp);
            this.Controls.Add(this.proposal_reject_reason);
            this.Controls.Add(this.LabelControl5);
            this.Controls.Add(this.LabelControl4);
            this.Controls.Add(this.proposed_date);
            this.Controls.Add(this.proposal_status_date);
            this.Controls.Add(this.LabelControl3);
            this.Controls.Add(this.LabelControl2);
            this.Controls.Add(this.status_area);
            this.Name = "Form_ProposalUpdate";
            this.Text = "Proposal Letter Information";
            ((System.ComponentModel.ISupportInitialize)this.DxErrorProvider1).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.proposal_status_date.Properties.VistaTimeProperties).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.proposal_status_date.Properties).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.proposed_date.Properties.VistaTimeProperties).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.proposed_date.Properties).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.proposal_reject_reason.Properties).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.proposal_reject_disp.Properties).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.proposal_status.Properties).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.proposal_accepted_by.Properties).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.proposal_note.Properties).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.message_list.Properties).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();
        }

        #endregion " Windows Form Designer generated code "

        private void Button_OK_Click(object sender, System.EventArgs e)
        {
            // Update the proposal information if the form is valid
            record.proposal_status        = Convert.ToInt32(proposal_status.EditValue);
            record.proposal_reject_reason = DebtPlus.Utils.Nulls.v_Int32(proposal_reject_reason.EditValue);
            record.proposal_reject_disp   = DebtPlus.Utils.Nulls.v_Int32(proposal_reject_disp.EditValue);
            record.proposal_accepted_by   = DebtPlus.Utils.Nulls.v_String(proposal_accepted_by.EditValue);
            record.proposal_status_date   = DebtPlus.Utils.Nulls.v_DateTime(proposal_status_date.EditValue);
            record.proposed_date          = Convert.ToDateTime(proposed_date.EditValue);
        }

        private void Form_Load(object sender, EventArgs e)
        {
            UnRegisterHandlers();

            try
            {
                // Set the editing controls for the new record
                proposal_status_date.Properties.NullText = string.Format("{0:d}", DateTime.Now.Date);
                proposed_date.Properties.NullText        = string.Format("{0:d}", DateTime.Now.Date);

                proposal_status.EditValue        = record.proposal_status;
                proposal_reject_reason.EditValue = record.proposal_reject_reason;
                proposal_reject_disp.EditValue   = record.proposal_reject_disp;
                proposal_accepted_by.EditValue   = record.proposal_accepted_by;
                proposal_status_date.EditValue   = record.proposal_status_date;
                proposed_date.EditValue          = record.proposed_date;

                status_area.Text                 = StatusInformation(record);

                // Message text. This is stored in the debt_notes table under a type "PR" with a pointer to the proposal.
                DebtPlus.LINQ.debt_note dn = record.debt_notes;
                if (dn != null)
                {
                    proposal_note.Text = dn.note_text;
                }

                Button_OK.Enabled = !HasErrors();
                EnableControls();
            }
            finally
            {
                RegisterHandlers();
            }
        }

        private string StatusInformation(DebtPlus.LINQ.client_creditor_proposal record)
        {
            System.Text.StringBuilder sb = new System.Text.StringBuilder();

            // Date created and created_by fields
            DateTime dateCreated = record.date_created;
            if (dateCreated != DateTime.MinValue)
            {
                sb.Append(Environment.NewLine);
                sb.Append("This proposal was created");
                sb.AppendFormat(" on {0:d}", dateCreated);

                string created_by = record.created_by;
                if (!string.IsNullOrEmpty(created_by))
                {
                    sb.AppendFormat(" by {0}", created_by);
                }
            }

            // Include the letter date
            if (record.letter_date.HasValue)
            {
                sb.Append(Environment.NewLine);
                sb.AppendFormat("The proposal letter is dated {0:d}", record.letter_date.Value);
            }

            // Print date
            if (record.proposal_print_date.HasValue)
            {
                sb.Append(Environment.NewLine);
                sb.AppendFormat("The proposal was printed/transmitted on {0:d} at {0:t}", record.proposal_print_date.Value);
            }

            // Proposal status date
            if (record.proposal_status_date.HasValue)
            {
                sb.Append(Environment.NewLine);
                sb.AppendFormat("The proposal status was last changed on {0:d}", record.proposal_status_date.Value);
            }

            string proposalStatus = record.rpps_client_type_indicator;
            if (string.IsNullOrEmpty(proposalStatus))
            {
                proposalStatus = " ";
            }

            sb.Append(Environment.NewLine);
            DebtPlus.LINQ.InMemory.rpps_client_type_indicator q = DebtPlus.LINQ.InMemory.rpps_client_type_indicators.getList().Find(s => string.Compare(s.Id, proposalStatus, true) == 0);
            if (q != null)
            {
                sb.AppendFormat("Proposed: {0}", q.description);
                if (q.enabled)
                {
                    sb.AppendFormat(" for {0:p} over {1:f0} months", record.percent_balance, record.terms);
                }
            }

            if (sb.Length > 0)
            {
                sb.Remove(0, 2);
            }
            return sb.ToString();
        }

        #region " Enabled status "

        private static bool proposal_status_date_enabled()
        {
            return true;
        }

        private bool InClosedBatch()
        {
            DebtPlus.LINQ.proposal_batch_id pb = record.proposal_batch_id1;
            if (pb != null)
            {
                return pb.date_closed.HasValue || pb.date_transmitted.HasValue;
            }
            return false;
        }

        private bool message_list_enabled()
        {
            return proposal_message_enabled();
        }

        private bool proposal_accepted_by_enabled()
        {
            return Convert.ToInt32(proposal_status.EditValue) == 3;
            // It must be in "accepted" status
        }

        private bool proposal_message_enabled()
        {
            return Convert.ToInt32(proposal_status.EditValue) == 1 && !InClosedBatch();
        }

        private bool proposal_reject_disp_enabled()
        {
            return Convert.ToInt32(proposal_status.EditValue) == 4;
            // It must be in "reject" status
        }

        private bool proposal_reject_reason_enabled()
        {
            return Convert.ToInt32(proposal_status.EditValue) == 4;
            // It must be in "reject" status
        }

        private Int32 ProposalBatchID()
        {
            if (record.Id >= 0 && record.proposal_batch_id.HasValue)
            {
                return record.proposal_batch_id.Value;
            }
            return -1;
        }

        private bool proposed_date_enabled()
        {
            return !InClosedBatch();
        }

        #endregion " Enabled status "

        #region " Change Events "

        private void EnableControls()
        {
            // Enable/Disable other fields depending upon the current settings.
            proposed_date.Enabled = proposed_date_enabled();
            proposal_status_date.Enabled = proposal_status_date_enabled();
            proposal_accepted_by.Enabled = proposal_accepted_by_enabled();
            proposal_reject_disp.Enabled = proposal_reject_disp_enabled();
            proposal_reject_reason.Enabled = proposal_reject_reason_enabled();
        }

        private void FormChanged(object sender, EventArgs e)
        {
            Button_OK.Enabled = !HasErrors();
        }

        private void message_list_SelectedIndexChanged(object sender, EventArgs e)
        {
            // Do the standard logic for the form being changed
            FormChanged(sender, e);
            proposal_note.Text = message_list.Text.Trim();
        }

        private void proposal_status_EditValueChanged(object sender, EventArgs e)
        {
            // Set the status date to today
            proposal_status_date.EditValue = DateTime.Now;
            EnableControls();

            // Clear the reject and accept status if the new value is PENDING
            if (Convert.ToInt32(proposal_status.EditValue) == 1)
            {
                proposal_reject_disp.EditValue = null;
                proposal_reject_reason.EditValue = null;
                proposal_accepted_by.EditValue = null;
            }
        }

        private void proposal_status_EditValueChanging(object sender, ChangingEventArgs e)
        {
            // Once the item is in a batch, do not allow the status to go to "pending" again. You can't change an accepted proposal back to pending.
            if (ProposalBatchID() >= 0)
            {
                if (e.NewValue != null)
                {
                    if (Convert.ToInt32(e.NewValue) < 2)
                    {
                        e.Cancel = true;
                    }
                }
            }
        }

        #endregion " Change Events "

        #region " HasErrors "

        private bool HasErrors()
        {
            // If there is a missing piece then jump out of the loop here
            if (proposal_status.EditValue == null)
            {
                return true;
            }

            // Look at the status to determine if a field is missing.
            switch (Convert.ToInt32(proposal_status.EditValue))
            {
                case 3:
                    // accepted
                    if (proposal_accepted_by.Text.Trim() == string.Empty)
                    {
                        return true;
                    }
                    break;


                case 4:
                    // rejected
                    if (proposal_reject_reason.Properties.GetDataSourceRowByDisplayValue(proposal_reject_reason.Text) == null)
                    {
                        return true;
                    }

                    if (proposal_reject_disp.Properties.GetDataSourceRowByDisplayValue(proposal_reject_disp.Text) == null)
                    {
                        return true;
                    }
                    break;


                default:
                    return false;
            }

            return false;
        }

        #endregion " HasErrors "
    }
}


