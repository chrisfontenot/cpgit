#region "Copyright 2000-2012 DebtPlus, L.L.C."

//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}

#endregion "Copyright 2000-2012 DebtPlus, L.L.C."

using System;
using System.Globalization;
using DevExpress.XtraEditors.Controls;

namespace DebtPlus.UI.Debt.Update.forms
{
    internal partial class Form_BalanceVerified
    {
        public Form_BalanceVerified()
            : base()
        {
            InitializeComponent();

            // Add "Handles" clauses here
            TextEdit_verified_by.EditValueChanging   += TextEdit_verified_by_EditValueChanging;
            DateEdit_verified_date.EditValueChanging += DateEdit_verified_date_EditValueChanging;
            SimpleButton_unverified.Click            += SimpleButton_unverified_Click;
            SimpleButton_verify.Click                += SimpleButton_verify_Click;
        }

        private DebtPlus.Interfaces.Debt.IDebtRecord currentDebtRecord;
        public Form_BalanceVerified(DebtPlus.Interfaces.Debt.IDebtRecord CurrentDebtRecord)
            : this()
        {
            this.currentDebtRecord = CurrentDebtRecord;

            // Empty the form fields
            DateEdit_verified_date.EditValue = System.DateTime.Now.Date;
            TextEdit_verified_by.EditValue   = string.Empty;
        }

        private void DateEdit_verified_date_EditValueChanging(object sender, ChangingEventArgs e)
        {
            EnableVerify(TextEdit_verified_by.EditValue, e.NewValue);
        }

        private void EnableVerify(object NewText, object NewDate)
        {
            string strValue = DebtPlus.Utils.Nulls.DStr(NewText);
            if (NewText != null && !object.ReferenceEquals(NewText, DBNull.Value))
            {
                strValue = Convert.ToString(NewText, CultureInfo.InvariantCulture);
            }

            DateTime dateValue = System.DateTime.MaxValue;
            if (NewDate != null && !object.ReferenceEquals(NewDate, DBNull.Value))
            {
                dateValue = Convert.ToDateTime(NewDate, CultureInfo.InvariantCulture);
            }

            SimpleButton_verify.Enabled = (strValue != string.Empty && dateValue < System.DateTime.MaxValue);
        }

        private void SimpleButton_unverified_Click(object sender, EventArgs e)
        {
            currentDebtRecord.balance_verify_by   = string.Empty;
            currentDebtRecord.balance_verify_date = DBNull.Value;
        }

        private void SimpleButton_verify_Click(object sender, EventArgs e)
        {
            currentDebtRecord.balance_verify_by   = DebtPlus.Utils.Nulls.v_String(TextEdit_verified_by.EditValue) ?? string.Empty;
            currentDebtRecord.balance_verify_date = DateEdit_verified_date.EditValue;
        }

        private void TextEdit_verified_by_EditValueChanging(object sender, ChangingEventArgs e)
        {
            EnableVerify(e.NewValue, DateEdit_verified_date.EditValue);
        }
    }
}
