#region "Copyright 2000-2012 DebtPlus, L.L.C."

//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}
#endregion "Copyright 2000-2012 DebtPlus, L.L.C."

using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Windows.Forms;
using DebtPlus.UI.Debt.Update.pages;
using DevExpress.XtraBars;
using DevExpress.XtraTab;

namespace DebtPlus.UI.Debt.Update.forms
{
    public partial class Form_DebtUpdate : DebtPlus.Data.Forms.DebtPlusForm
    {
        // Current debt record being edited
        private DebtPlus.Interfaces.Debt.IDebtRecord currentDebtRecord = null;

        /// <summary>
        /// Initialize the form
        /// </summary>
        public Form_DebtUpdate()
            : base()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Initialize the form
        /// </summary>
        /// <param name="debtRecord">Current record being edited</param>
        /// <param name="ClientDS">Current client Dataset</param>
        /// <remarks></remarks>
        public Form_DebtUpdate(DebtPlus.Interfaces.Debt.IDebtRecord DebtRecord)
            : this()
        {
            this.currentDebtRecord = DebtRecord;
            RegisterHandlers();
        }

        private void RegisterHandlers()
        {
            FormClosing                          += Form_DebtUpdate_FormClosing;
            BarButtonItem_File_Exit.ItemClick    += BarButtonItem_File_Exit_ItemClick;
            BarSubItem_View.Popup                += MenuItem_View_Popup;
            Load                                 += DebtUpdateForm_Load;
            XtraTabControl1.SelectedPageChanging += XtraTabControl1_SelectedPageChanging;
            XtraTabControl1.SelectedPageChanged  += XtraTabControl1_SelectedPageChanged;
        }

        private void UnRegisterHandlers()
        {
            FormClosing                          -= Form_DebtUpdate_FormClosing;
            BarButtonItem_File_Exit.ItemClick    -= BarButtonItem_File_Exit_ItemClick;
            BarSubItem_View.Popup                -= MenuItem_View_Popup;
            Load                                 -= DebtUpdateForm_Load;
            XtraTabControl1.SelectedPageChanging -= XtraTabControl1_SelectedPageChanging;
            XtraTabControl1.SelectedPageChanged  -= XtraTabControl1_SelectedPageChanged;
        }
#region Form
        /// <summary>
        /// Process the FORM : CLOSING event
        /// </summary>
        private void Form_DebtUpdate_FormClosing(object sender, FormClosingEventArgs e)
        {
            UnRegisterHandlers();

            // If we are not closing because the user choose close then abort early.
            // We don't want to prevent the shutdown operation from occurring.
            if (e.CloseReason != CloseReason.UserClosing && e.CloseReason != CloseReason.None)
            {
                return;
            }

            // Save the current page information. We can cancel the close if the validation
            // routine fails. However, it will leave the window up.
            if (! SaveTabPage())
            {
                RegisterHandlers();
                e.Cancel = true;
                return;
            }

            // Save the header information. This is just the "priority" field.
            FormHeader1.SaveForm();
        }

        /// <summary>
        /// Process the FORM : LOAD event
        /// </summary>
        private void DebtUpdateForm_Load(object sender, EventArgs e)
        {
            UnRegisterHandlers();
            try
            {
                // Read the information for the header field
                FormHeader1.ReadForm(currentDebtRecord);

                // Load the reports menu
                Load_View_MenuItems();

                // Load the reports menu
                Load_Reports_MenuItems();

                // Load the letters menu
                Load_Letters_MenuItems();

                // Complete by reading the information for the status line
                BarStaticItem_ID.EditValue = string.IsNullOrWhiteSpace(currentDebtRecord.Creditor) ? string.Empty : string.Format("{0:0000000}*{1}", currentDebtRecord.ClientId, currentDebtRecord.Creditor);
                BarStaticItem_Name.Caption = currentDebtRecord.cr_creditor_name;

                // Read the list of creditor methods that are supported by this creditor
                if (!string.IsNullOrWhiteSpace(currentDebtRecord.Creditor))
                {
                    using (DebtPlus.LINQ.BusinessContext bc = new DebtPlus.LINQ.BusinessContext())
                    {
                        // Collection of the creditor methods for this creditor
                        System.Collections.Generic.List<DebtPlus.LINQ.creditor_method> colMethods = bc.creditors
                            .Join(bc.creditor_methods, cr => cr.creditor_id, m => m.creditor, (cr, m) => new { Id = cr.Id, m = m })
                            .Join(bc.banks, m => m.m.bank, b => b.Id, (a, b) => new { Id = a.Id, bankType = b.type, m = a.m })
                            .Where(s => s.Id == currentDebtRecord.Creditor && new string[] { "E", "R" }.Contains(s.bankType))
                            .Select(s => s.m)
                            .ToList();

                        // Look for the proposals being electronic
                        DebtPlus.LINQ.creditor_method q = colMethods.Find(s => s.type == "EDI");
                        if (q != null)
                        {
                            BarStaticItem_CDP.Visibility = BarItemVisibility.Always;
                        }

                        q = colMethods.Find(s => s.type == "BAL");
                        if (q != null)
                        {
                            BarStaticItem_CDV.Visibility = BarItemVisibility.Always;
                        }

                        q = colMethods.Find(s => s.type == "MSG");
                        if (q != null)
                        {
                            BarStaticItem_CDD.Visibility = BarItemVisibility.Always;
                        }
                    }
                }

                // Fire the first page update event
                LoadTabPage();
            }
            finally
            {
                RegisterHandlers();
            }
        }
#endregion Form
#region TabControl
        /// <summary>
        /// Routine to find the currently selected page's control item
        /// </summary>
        internal DebtPlus.UI.Debt.Update.controls.ControlBaseDebt FindCurrentPage()
        {
            XtraTabPage TabPage = XtraTabControl1.SelectedTabPage;
            if (TabPage != null)
            {
                var CurrentPage = TabPage.Controls[0] as DebtPlus.UI.Debt.Update.controls.ControlBaseDebt;
                if (CurrentPage != null)
                {
                    return CurrentPage;
                }
            }
            return null;
        }

        private void LoadTabPage()
        {
            BarSubItem_Items.ItemLinks.Clear();

            var currentpage = FindCurrentPage();
            if (currentpage != null)
            {
                currentpage.ReadForm(currentDebtRecord);
            }
        }

        private bool SaveTabPage()
        {
            // First, tab to the next field to force the field validation to occur.
            // SendKeys.Send("{TAB}");

            // Find the current tab page and then the control base
            var CurrentPage = FindCurrentPage();
            if (CurrentPage != null)
            {
                try
                {
                    CurrentPage.SaveForm();
                }
                catch (ApplicationException ex)
                {
                    DebtPlus.Data.Forms.MessageBox.Show(ex.Message, "Form validation error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return false;
                }
            }

            return true;
        }

        /// <summary>
        /// Process the Page change event for the tab control
        /// </summary>
        private void XtraTabControl1_SelectedPageChanged(object sender, TabPageChangedEventArgs e)
        {
            UnRegisterHandlers();
            try
            {
                LoadTabPage();
            }
            finally
            {
                RegisterHandlers();
            }
        }

        /// <summary>
        /// Process the Page change event for the tab control
        /// </summary>
        private void XtraTabControl1_SelectedPageChanging(object sender, TabPageChangingEventArgs e)
        {
            UnRegisterHandlers();
            try
            {
                if (!SaveTabPage())
                {
                    e.Cancel = true;
                }
            }
            finally
            {
                RegisterHandlers();
            }
        }
#endregion TabControl
#region File Menu
        /// <summary>
        /// Process the FILE_MENU : EXIT : CLICK event
        /// </summary>
        private void BarButtonItem_File_Exit_ItemClick(object sender, ItemClickEventArgs e)
        {
            //if (SaveTabPage())
            //{
                DialogResult = DialogResult.OK;
            //}
        }
#endregion File Menu
#region View Menu
        /// <summary>
        /// Load the view menu items from the tab pages
        /// </summary>
        private void Load_View_MenuItems()
        {
            Int32 ViewItemCount = 0;

            // Erase the list of items from the menu
            BarSubItem_View.ItemLinks.Clear();

            // Load the view menu items with the tab pages
            foreach (XtraTabPage TabPage in XtraTabControl1.TabPages)
            {
                ViewItemCount += 1;
                BarCheckItem NewMenu = new BarCheckItem(barManager1, false)
                {
                    Caption = "&" + TabPage.Text,
                    Tag = TabPage.Tag,
                    Name = string.Format("View_Item_{0:f0}", ViewItemCount)
                };
                BarSubItem_View.AddItem(NewMenu);
                NewMenu.ItemClick += MenuItem_View_Click;
            }
        }

        /// <summary>
        /// Process the VIEW menu items CLICK event
        /// </summary>
        private void MenuItem_View_Click(object sender, EventArgs e)
        {
            UnRegisterHandlers();
            try
            {
                BarItemLink Link = barManager1.PressedLink;
                BarItem Item = Link.Item;
                string SelectedPage = Convert.ToString(Item.Tag);

                // Find the desired page and select it. It will trip the update events automatically.
                foreach (XtraTabPage TabPage in XtraTabControl1.TabPages)
                {
                    if (Convert.ToString(TabPage.Tag) == SelectedPage)
                    {
                        XtraTabControl1.SelectedTabPageIndex = TabPage.TabIndex;
                        break;
                    }
                }
            }
            finally
            {
                RegisterHandlers();
            }
        }

        /// <summary>
        /// Process the VIEW menu items POPUP event
        /// </summary>
        private void MenuItem_View_Popup(object sender, EventArgs e)
        {
            BarSubItem SubItem = (BarSubItem)sender;
            string ShowingPage = Convert.ToString(XtraTabControl1.SelectedTabPage.Tag);
            foreach (BarCheckItemLink Item in SubItem.ItemLinks)
            {
                ((BarCheckItem)Item.Item).Checked = string.Compare(Convert.ToString(SubItem.Tag), ShowingPage, false) == 0;
            }
        }
#endregion View Menu
#region Reports Menu
        private Int32 addedMenuItem = 0;

        /// <summary>
        /// Load the reports menu items from the database tables
        /// </summary>
        private void Load_Reports_MenuItems()
        {
            // Clear the current reports menu items. We need to remove the "Temp" item that enables the popup
            BarSubItem_Reports.ClearLinks();

            foreach (var rptItem in DebtPlus.LINQ.Cache.report.getList().Where(r => (!string.IsNullOrWhiteSpace(r.ClassName)) && (!string.IsNullOrWhiteSpace(r.menu_name)) && r.Type == "DB").OrderBy(r => r.menu_name))
            {
                var rpt = rptItem;
                string menuName = rpt.menu_name.Trim(new char[] {' ', '\\'});

                // Find the corresponding link to the item in the menu tree
                BarSubItem band = BarSubItem_Reports;
                Int32 directoryOffset = menuName.IndexOf('\\');

                // Find the directory name in the string. If none, terminate to add the item.
                while (directoryOffset >= 0)
                {
                    // Ignore leading \ characters in the name.
                    if (directoryOffset == 0)
                    {
                        menuName = menuName.Substring(1);
                        directoryOffset = menuName.IndexOf('\\');
                        continue;
                    }

                    // Split the name into a group and the "rest of the string"
                    string groupName = menuName.Substring(0, directoryOffset).Trim();
                    menuName = menuName.Substring(directoryOffset + 1);

                    if (groupName != string.Empty)
                    {
                        BarSubItem foundItem = band.ItemLinks.OfType<BarSubItem>().Where(s => string.Compare(s.Caption, groupName, true) == 0).FirstOrDefault();
                        if (foundItem == null)
                        {
                            addedMenuItem += 1;
                            foundItem = new BarSubItem()
                            {
                                Manager = barManager1,
                                Caption = groupName,
                                Name = string.Format("Reports_Band_{0:f0}", addedMenuItem)
                            };
                            band.AddItem(foundItem);
                        }

                        // Start the search from this point
                        band = foundItem;
                    }

                    directoryOffset = menuName.IndexOf('\\');
                }

                // Add the item to the menu
                addedMenuItem += 1;
                ReportsMenuItem NewItem = new ReportsMenuItem(currentDebtRecord, menuName, rpt)
                {
                    Manager = barManager1,
                    Name = string.Format("Reports_Item_{0:f0}", addedMenuItem)
                };
                band.AddItem(NewItem);
            }
        }
#endregion Reports Menu
#region Letters Menu
        /// <summary>
        /// Load the Letters menu items from the database tables
        /// </summary>
        private void Load_Letters_MenuItems()
        {
            foreach (DebtPlus.LINQ.letter_type currentLetter in DebtPlus.LINQ.Cache.letter_type.getList().FindAll(s => s.letter_group == "DEBT" && (s.menu_name ?? string.Empty).Trim() != string.Empty).OrderBy(s => s.menu_name))
            {
                string menuName = currentLetter.menu_name.Trim(new char[] {' ', '\\'});

                // Find the corresponding link to the item in the menu tree
                BarSubItem band = BarSubItem_Letters;
                Int32 directoryOffset = menuName.IndexOf('\\');

                while (directoryOffset >= 0)
                {
                    // Ignore leading \ characters in the name.
                    if (directoryOffset == 0)
                    {
                        menuName = menuName.Substring(1);
                        directoryOffset = menuName.IndexOf('\\');
                        continue;
                    }

                    // Split the name into a group and the "rest of the string"
                    string groupName = menuName.Substring(0, directoryOffset).Trim();
                    menuName = menuName.Substring(directoryOffset + 1);

                    if (groupName != string.Empty)
                    {
                        // Find the name of the group from the current base so that we may follow it down the tree.
                        BarSubItem foundItem = band.ItemLinks.OfType<BarSubItem>().Where(s => string.Compare(s.Caption, groupName, true) == 0).FirstOrDefault();
                        if (foundItem == null)
                        {
                            addedMenuItem += 1;
                            foundItem = new BarSubItem
                            {
                                Manager = barManager1,
                                Caption = groupName,
                                Name = string.Format("Letters_Band_{0:f0}", addedMenuItem)
                            };
                            band.AddItem(foundItem);
                        }

                        // Start the search from this point
                        band = foundItem;
                    }

                    directoryOffset = menuName.IndexOf('\\');
                }

                // Add the item to the bar manager so that we may link to it
                addedMenuItem += 1;
                LettersMenuItem NewItem = new LettersMenuItem(currentDebtRecord, menuName, currentLetter)
                {
                    Manager = barManager1,
                    Name = string.Format("Letters_Item_{0:f0}", addedMenuItem)
                };
                band.AddItem(NewItem);
            }
        }
#endregion Letters Menu
    }
}
