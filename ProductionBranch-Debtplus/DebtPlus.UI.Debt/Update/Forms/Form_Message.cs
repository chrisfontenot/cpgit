#region "Copyright 2000-2012 DebtPlus, L.L.C."

//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}

#endregion "Copyright 2000-2012 DebtPlus, L.L.C."

using System;
using System.Diagnostics;
using System.Linq;
using DebtPlus.Data.Forms;
using DebtPlus.LINQ;

namespace DebtPlus.UI.Debt.Update.forms
{
    internal partial class Form_Message : DebtPlusForm
    {
        // Pointer to the record to be updated
        private readonly debt_note note;
        private readonly System.Collections.Generic.List<debt_note> col;
        private bool Locked;

        public Form_Message()
            : base()
        {
            InitializeComponent();
        }

        public Form_Message(System.Collections.Generic.List<debt_note> col, debt_note note)
            : this()
        {
            this.note = note;
            this.col = col;

            RegisterHandlers();
        }

        private void RegisterHandlers()
        {
            Load                              += Form_Message_Load;
            Button_OK.Click                   += Button_OK_Click;
            message_list.SelectedIndexChanged += message_list_SelectedIndexChanged;
            note_text.TextChanged             += note_text_TextChanged;
        }

        private void UnRegisterHandlers()
        {
            Load                              -= Form_Message_Load;
            Button_OK.Click                   -= Button_OK_Click;
            message_list.SelectedIndexChanged -= message_list_SelectedIndexChanged;
            note_text.TextChanged             -= note_text_TextChanged;
        }

        #region " Windows Form Designer generated code "

        internal DevExpress.XtraEditors.SimpleButton Button_Cancel;

        internal DevExpress.XtraEditors.SimpleButton Button_OK;

        //NOTE: The following procedure is required by the Windows Form Designer
        //It can be modified using the Windows Form Designer.
        //Do not modify it using the code editor.
        internal DevExpress.XtraEditors.LabelControl LabelControl1;

        internal DevExpress.XtraEditors.LabelControl LabelControl2;

        internal DevExpress.XtraEditors.LabelControl LabelControl3;

        internal DevExpress.XtraEditors.LabelControl LabelControl4;

        internal DevExpress.XtraEditors.ComboBoxEdit message_list;

        internal DevExpress.XtraEditors.CalcEdit note_amount;

        internal DevExpress.XtraEditors.DateEdit note_date;

        internal DevExpress.XtraEditors.MemoEdit note_text;

        private System.ComponentModel.IContainer components = null;

        //Form overrides dispose to clean up the component list.
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (components != null)
                {
                    components.Dispose();
                }
            }
            base.Dispose(disposing);
        }

        //Required by the Windows Form Designer
        [System.Diagnostics.DebuggerStepThrough()]
        private void InitializeComponent()
        {
            this.LabelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.LabelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.LabelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.LabelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.Button_OK = new DevExpress.XtraEditors.SimpleButton();
            this.Button_Cancel = new DevExpress.XtraEditors.SimpleButton();
            this.note_amount = new DevExpress.XtraEditors.CalcEdit();
            this.note_date = new DevExpress.XtraEditors.DateEdit();
            this.message_list = new DevExpress.XtraEditors.ComboBoxEdit();
            this.note_text = new DevExpress.XtraEditors.MemoEdit();

            ((System.ComponentModel.ISupportInitialize)this.note_amount.Properties).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.note_date.Properties).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.message_list.Properties).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.note_text.Properties).BeginInit();
            this.SuspendLayout();

            //
            //LabelControl1
            //
            this.LabelControl1.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.Horizontal;
            this.LabelControl1.Location = new System.Drawing.Point(8, 11);
            this.LabelControl1.Name = "LabelControl1";
            this.LabelControl1.Size = new System.Drawing.Size(27, 13);
            this.LabelControl1.TabIndex = 0;
            this.LabelControl1.Text = "Note:";
            //
            //LabelControl2
            //
            this.LabelControl2.Anchor = (System.Windows.Forms.AnchorStyles)(System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left);
            this.LabelControl2.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.Horizontal;
            this.LabelControl2.Location = new System.Drawing.Point(8, 116);
            this.LabelControl2.Name = "LabelControl2";
            this.LabelControl2.Size = new System.Drawing.Size(54, 13);
            this.LabelControl2.TabIndex = 2;
            this.LabelControl2.Text = "Note Type:";
            //
            //LabelControl3
            //
            this.LabelControl3.Anchor = (System.Windows.Forms.AnchorStyles)(System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left);
            this.LabelControl3.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.Horizontal;
            this.LabelControl3.Location = new System.Drawing.Point(8, 148);
            this.LabelControl3.Name = "LabelControl3";
            this.LabelControl3.Size = new System.Drawing.Size(27, 13);
            this.LabelControl3.TabIndex = 4;
            this.LabelControl3.Text = "Date:";
            //
            //LabelControl4
            //
            this.LabelControl4.Anchor = (System.Windows.Forms.AnchorStyles)(System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right);
            this.LabelControl4.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.Horizontal;
            this.LabelControl4.Location = new System.Drawing.Point(192, 148);
            this.LabelControl4.Name = "LabelControl4";
            this.LabelControl4.Size = new System.Drawing.Size(37, 13);
            this.LabelControl4.TabIndex = 6;
            this.LabelControl4.Text = "Amount";
            //
            //Button_OK
            //
            this.Button_OK.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.Button_OK.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.Button_OK.Location = new System.Drawing.Point(104, 184);
            this.Button_OK.Name = "Button_OK";
            this.Button_OK.TabIndex = 8;
            this.Button_OK.Text = "&OK";
            //
            //Button_Cancel
            //
            this.Button_Cancel.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.Button_Cancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.Button_Cancel.Location = new System.Drawing.Point(192, 184);
            this.Button_Cancel.Name = "Button_Cancel";
            this.Button_Cancel.TabIndex = 9;
            this.Button_Cancel.Text = "&Cancel";
            //
            //note_amount
            //
            this.note_amount.Anchor = (System.Windows.Forms.AnchorStyles)(System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right);
            this.note_amount.Location = new System.Drawing.Point(236, 144);
            this.note_amount.Name = "note_amount";
            //
            //note_amount.Properties
            //
            this.note_amount.Properties.Appearance.Options.UseTextOptions = true;
            this.note_amount.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.note_amount.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] { new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo) });
            this.note_amount.Properties.DisplayFormat.FormatString = "c2";
            this.note_amount.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.note_amount.Properties.EditFormat.FormatString = "f2";
            this.note_amount.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.note_amount.Properties.Mask.BeepOnError = true;
            this.note_amount.Properties.Mask.EditMask = "c";
            this.note_amount.Properties.Precision = 2;
            this.note_amount.TabIndex = 7;
            //
            //note_date
            //
            this.note_date.Anchor = (System.Windows.Forms.AnchorStyles)(System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left);
            this.note_date.EditValue = null;
            this.note_date.Location = new System.Drawing.Point(80, 144);
            this.note_date.Name = "note_date";
            //
            //note_date.Properties
            //
            this.note_date.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] { new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo) });
            this.note_date.TabIndex = 5;
            //
            //message_list
            //
            this.message_list.Anchor = (System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) | System.Windows.Forms.AnchorStyles.Right);
            this.message_list.Location = new System.Drawing.Point(80, 112);
            this.message_list.Name = "message_list";
            //
            //message_list.Properties
            //
            this.message_list.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] { new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo) });
            this.message_list.Properties.NullText = "If you wish a standard message, choose it here.";
            this.message_list.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.message_list.Size = new System.Drawing.Size(256, 20);
            this.message_list.TabIndex = 3;
            this.message_list.ToolTip = "Choose a standard note from this list if you wish. Or, simply type your note abov" + "e.";
            //
            //note_text
            //
            this.note_text.Anchor = (System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) | System.Windows.Forms.AnchorStyles.Left) | System.Windows.Forms.AnchorStyles.Right);
            this.note_text.Location = new System.Drawing.Point(80, 8);
            this.note_text.Name = "note_text";
            this.note_text.Size = new System.Drawing.Size(256, 96);
            this.note_text.TabIndex = 1;
            //
            //Form_Message
            //
            this.AcceptButton = this.Button_OK;
            this.AutoScaleBaseSize = new System.Drawing.Size(5, 14);
            this.CancelButton = this.Button_Cancel;
            this.ClientSize = new System.Drawing.Size(352, 222);
            this.Controls.Add(this.note_text);
            this.Controls.Add(this.message_list);
            this.Controls.Add(this.note_date);
            this.Controls.Add(this.note_amount);
            this.Controls.Add(this.Button_Cancel);
            this.Controls.Add(this.Button_OK);
            this.Controls.Add(this.LabelControl4);
            this.Controls.Add(this.LabelControl3);
            this.Controls.Add(this.LabelControl2);
            this.Controls.Add(this.LabelControl1);
            this.Name = "Form_Message";
            this.ToolTipController1.SetSuperTip(this, null);
            this.Text = "Creditor Message";

            ((System.ComponentModel.ISupportInitialize)this.note_amount.Properties).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.note_date.Properties).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.message_list.Properties).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.note_text.Properties).EndInit();
            this.ResumeLayout(false);
        }

        #endregion " Windows Form Designer generated code "

        private void Button_OK_Click(object sender, EventArgs e)
        {
            if (!Locked)
            {
                note.note_amount = Convert.ToDecimal(note_amount.EditValue);
                note.note_date   = Convert.ToDateTime(note_date.EditValue);
                note.note_text   = Convert.ToString(note_text.EditValue);
            }
        }

        /// <summary>
        /// Load the form
        /// </summary>
        private void Form_Message_Load(object sender, EventArgs e)
        {
            UnRegisterHandlers();
            try
            {
                // There should be a record pointer.
                Debug.Assert(note != null);
                Debug.Assert(col != null);

                // If the item is locked then set the flag
                Locked = !string.IsNullOrEmpty(note.output_batch);

                // Bind the other fields to the data
                note_amount.EditValue = note.note_amount;
                note_date.EditValue   = note.note_date;
                note_text.EditValue   = note.note_text;

                // List of standard messages
                message_list.Properties.Items.Clear();
                foreach (var item in DebtPlus.LINQ.Cache.disbursement_creditor_note_type.getList().OrderBy(n => n.description))
                {
                    message_list.Properties.Items.Add(new DebtPlus.Data.Controls.ComboboxItem(item.description, item.Id, true));
                }

                // Lock the controls if indicated
                note_amount.Properties.ReadOnly = Locked;
                note_date.Properties.ReadOnly   = Locked;
                note_text.Properties.ReadOnly   = Locked;

                // There must be a message text or we can't accept the form
                Button_OK.Enabled = note_text.Text.Trim().Length > 0 && !Locked;
            }
            finally
            {
                RegisterHandlers();
            }
        }

        /// <summary>
        /// Process a change in the message list
        /// </summary>
        private void message_list_SelectedIndexChanged(object sender, EventArgs e)
        {
            note_text.EditValue = message_list.Text.Trim();
        }

        /// <summary>
        /// Process a change in the note text
        /// </summary>
        private void note_text_TextChanged(object sender, EventArgs e)
        {
            Button_OK.Enabled = note_text.Text.Trim().Length > 0 & !Locked;
        }
    }
}
