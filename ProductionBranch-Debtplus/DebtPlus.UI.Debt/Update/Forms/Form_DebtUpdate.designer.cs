using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using DebtPlus.UI.Debt.Update.controls;
using DebtPlus.UI.Debt.Update.pages;

namespace DebtPlus.UI.Debt.Update.forms
{
    partial class Form_DebtUpdate
    {
        //Form overrides dispose to clean up the component list.
        [System.Diagnostics.DebuggerNonUserCode()]
        protected override void Dispose(bool disposing)
        {
            try
            {
                if (disposing)
                {
                    if (components != null)
                    {
                        components.Dispose();
                    }
                }
            }
            finally
            {
                base.Dispose(disposing);
            }
        }

        //Required by the Windows Form Designer
        private System.ComponentModel.IContainer components = null;

        //NOTE: The following procedure is required by the Windows Form Designer
        //It can be modified using the Windows Form Designer.  
        //Do not modify it using the code editor.
        [System.Diagnostics.DebuggerStepThrough]
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.barManager1 = new DevExpress.XtraBars.BarManager(this.components);
            this.MainMenu = new DevExpress.XtraBars.Bar();
            this.BarSubItem_File = new DevExpress.XtraBars.BarSubItem();
            this.BarButtonItem_File_Exit = new DevExpress.XtraBars.BarButtonItem();
            this.BarSubItem_Items = new DevExpress.XtraBars.BarSubItem();
            this.BarSubItem_View = new DevExpress.XtraBars.BarSubItem();
            this.BarSubItem_Letters = new DevExpress.XtraBars.BarSubItem();
            this.BarSubItem_Reports = new DevExpress.XtraBars.BarSubItem();
            this.StatusBar = new DevExpress.XtraBars.Bar();
            this.BarStaticItem_ID = new DevExpress.XtraBars.BarEditItem();
            this.RepositoryItemTextEdit_ID = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.BarStaticItem_Name = new DevExpress.XtraBars.BarStaticItem();
            this.BarStaticItem_CDP = new DevExpress.XtraBars.BarStaticItem();
            this.BarStaticItem_CDD = new DevExpress.XtraBars.BarStaticItem();
            this.BarStaticItem_CDV = new DevExpress.XtraBars.BarStaticItem();
            this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
            this.FormHeader1 = new FormHeader();
            this.components.Add(FormHeader1);
            this.XtraTabControl1 = new DevExpress.XtraTab.XtraTabControl();
            this.XtraTabPage_Account = new DevExpress.XtraTab.XtraTabPage();
            this.Page_Account1 = new Page_Account();
            this.components.Add(Page_Account1);
            this.XtraTabPage_Proposals = new DevExpress.XtraTab.XtraTabPage();
            this.Page_Proposals1 = new Page_Proposals();
            this.components.Add(this.Page_Proposals1);
            this.XtraTabPage_Contacts = new DevExpress.XtraTab.XtraTabPage();
            this.Page_Contacts1 = new Page_Contacts();
            this.components.Add(this.Page_Contacts1);
            this.XtraTabPage_Notes = new DevExpress.XtraTab.XtraTabPage();
            this.Page_Notes1 = new Page_Notes();
            this.components.Add(this.Page_Notes1);
            this.XtraTabPage_Events = new DevExpress.XtraTab.XtraTabPage();
            this.Page_Events1 = new Page_Events();
            this.components.Add(this.Page_Events1);
            this.XtraTabPage_Messages = new DevExpress.XtraTab.XtraTabPage();
            this.Page_Messages1 = new Page_Messages();
            this.components.Add(this.Page_Messages1);
            ((System.ComponentModel.ISupportInitialize)this.DxErrorProvider1).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.barManager1).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.RepositoryItemTextEdit_ID).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.XtraTabControl1).BeginInit();
            this.XtraTabControl1.SuspendLayout();
            this.XtraTabPage_Account.SuspendLayout();
            this.XtraTabPage_Proposals.SuspendLayout();
            this.XtraTabPage_Contacts.SuspendLayout();
            this.XtraTabPage_Notes.SuspendLayout();
            this.XtraTabPage_Events.SuspendLayout();
            this.XtraTabPage_Messages.SuspendLayout();
            this.SuspendLayout();
            //
            //barManager1
            //
            this.barManager1.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
				this.MainMenu,
				this.StatusBar
			});
            this.barManager1.DockControls.Add(this.barDockControlTop);
            this.barManager1.DockControls.Add(this.barDockControlBottom);
            this.barManager1.DockControls.Add(this.barDockControlLeft);
            this.barManager1.DockControls.Add(this.barDockControlRight);
            this.barManager1.Form = this;
            this.barManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
				this.BarSubItem_File,
				this.BarSubItem_Items,
				this.BarSubItem_View,
				this.BarSubItem_Letters,
				this.BarSubItem_Reports,
				this.BarButtonItem_File_Exit,
				this.BarStaticItem_ID,
				this.BarStaticItem_Name,
				this.BarStaticItem_CDP,
				this.BarStaticItem_CDD,
				this.BarStaticItem_CDV
			});
            this.barManager1.MainMenu = this.MainMenu;
            this.barManager1.MaxItemId = 13;
            this.barManager1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] { this.RepositoryItemTextEdit_ID });
            this.barManager1.StatusBar = this.StatusBar;
            //
            //MainMenu
            //
            this.MainMenu.BarName = "Main menu";
            this.MainMenu.DockCol = 0;
            this.MainMenu.DockRow = 0;
            this.MainMenu.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.MainMenu.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
				new DevExpress.XtraBars.LinkPersistInfo(this.BarSubItem_File),
				new DevExpress.XtraBars.LinkPersistInfo(this.BarSubItem_Items),
				new DevExpress.XtraBars.LinkPersistInfo(this.BarSubItem_View),
				new DevExpress.XtraBars.LinkPersistInfo(this.BarSubItem_Letters),
				new DevExpress.XtraBars.LinkPersistInfo(this.BarSubItem_Reports)
			});
            this.MainMenu.OptionsBar.MultiLine = true;
            this.MainMenu.OptionsBar.UseWholeRow = true;
            this.MainMenu.Text = "Main menu";
            //
            //BarSubItem_File
            //
            this.BarSubItem_File.Caption = "&File";
            this.BarSubItem_File.Id = 1;
            this.BarSubItem_File.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] { new DevExpress.XtraBars.LinkPersistInfo(this.BarButtonItem_File_Exit) });
            this.BarSubItem_File.Name = "BarSubItem_File";
            //
            //BarButtonItem_File_Exit
            //
            this.BarButtonItem_File_Exit.Caption = "&Exit";
            this.BarButtonItem_File_Exit.Id = 6;
            this.BarButtonItem_File_Exit.Name = "BarButtonItem_File_Exit";
            //
            //BarSubItem_Items
            //
            this.BarSubItem_Items.Caption = "&Items";
            this.BarSubItem_Items.Id = 2;
            this.BarSubItem_Items.Name = "BarSubItem_Items";
            //
            //BarSubItem_View
            //
            this.BarSubItem_View.Caption = "&View";
            this.BarSubItem_View.Id = 3;
            this.BarSubItem_View.Name = "BarSubItem_View";
            //
            //BarSubItem_Letters
            //
            this.BarSubItem_Letters.Caption = "&Letters";
            this.BarSubItem_Letters.Id = 4;
            this.BarSubItem_Letters.Name = "BarSubItem_Letters";
            //
            //BarSubItem_Reports
            //
            this.BarSubItem_Reports.Caption = "&Reports";
            this.BarSubItem_Reports.Id = 5;
            this.BarSubItem_Reports.Name = "BarSubItem_Reports";
            //
            //StatusBar
            //
            this.StatusBar.BarName = "Status bar";
            this.StatusBar.CanDockStyle = DevExpress.XtraBars.BarCanDockStyle.Bottom;
            this.StatusBar.DockCol = 0;
            this.StatusBar.DockRow = 0;
            this.StatusBar.DockStyle = DevExpress.XtraBars.BarDockStyle.Bottom;
            this.StatusBar.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
				new DevExpress.XtraBars.LinkPersistInfo(this.BarStaticItem_ID),
				new DevExpress.XtraBars.LinkPersistInfo(this.BarStaticItem_Name),
				new DevExpress.XtraBars.LinkPersistInfo(this.BarStaticItem_CDP),
				new DevExpress.XtraBars.LinkPersistInfo(this.BarStaticItem_CDD),
				new DevExpress.XtraBars.LinkPersistInfo(this.BarStaticItem_CDV)
			});
            this.StatusBar.OptionsBar.AllowQuickCustomization = false;
            this.StatusBar.OptionsBar.DrawDragBorder = false;
            this.StatusBar.OptionsBar.UseWholeRow = true;
            this.StatusBar.Text = "Status bar";
            //
            //BarStaticItem_ID
            //
            this.BarStaticItem_ID.Caption = "BarEditItemID";
            this.BarStaticItem_ID.Edit = this.RepositoryItemTextEdit_ID;
            this.BarStaticItem_ID.Id = 12;
            this.BarStaticItem_ID.Name = "BarStaticItem_ID";
            this.BarStaticItem_ID.Width = 117;
            //
            //RepositoryItemTextEdit_ID
            //
            this.RepositoryItemTextEdit_ID.AutoHeight = false;
            this.RepositoryItemTextEdit_ID.Name = "RepositoryItemTextEdit_ID";
            this.RepositoryItemTextEdit_ID.ReadOnly = true;
            //
            //BarStaticItem_Name
            //
            this.BarStaticItem_Name.AutoSize = DevExpress.XtraBars.BarStaticItemSize.Spring;
            this.BarStaticItem_Name.Id = 8;
            this.BarStaticItem_Name.Name = "BarStaticItem_Name";
            this.BarStaticItem_Name.TextAlignment = System.Drawing.StringAlignment.Near;
            this.BarStaticItem_Name.Width = 32;
            //
            //BarStaticItem_CDP
            //
            this.BarStaticItem_CDP.Caption = "CDP";
            this.BarStaticItem_CDP.Id = 9;
            this.BarStaticItem_CDP.Name = "BarStaticItem_CDP";
            this.BarStaticItem_CDP.TextAlignment = System.Drawing.StringAlignment.Near;
            this.BarStaticItem_CDP.Visibility = DevExpress.XtraBars.BarItemVisibility.OnlyInCustomizing;
            //
            //BarStaticItem_CDD
            //
            this.BarStaticItem_CDD.Caption = "CDD";
            this.BarStaticItem_CDD.Id = 10;
            this.BarStaticItem_CDD.Name = "BarStaticItem_CDD";
            this.BarStaticItem_CDD.TextAlignment = System.Drawing.StringAlignment.Near;
            this.BarStaticItem_CDD.Visibility = DevExpress.XtraBars.BarItemVisibility.OnlyInCustomizing;
            //
            //BarStaticItem_CDV
            //
            this.BarStaticItem_CDV.Caption = "CDV";
            this.BarStaticItem_CDV.Id = 11;
            this.BarStaticItem_CDV.Name = "BarStaticItem_CDV";
            this.BarStaticItem_CDV.TextAlignment = System.Drawing.StringAlignment.Near;
            this.BarStaticItem_CDV.Visibility = DevExpress.XtraBars.BarItemVisibility.OnlyInCustomizing;
            //
            //barDockControlTop
            //
            this.barDockControlTop.CausesValidation = false;
            this.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControlTop.Location = new System.Drawing.Point(0, 0);
            this.barDockControlTop.Size = new System.Drawing.Size(576, 25);
            //
            //barDockControlBottom
            //
            this.barDockControlBottom.CausesValidation = false;
            this.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 537);
            this.barDockControlBottom.Size = new System.Drawing.Size(576, 28);
            //
            //barDockControlLeft
            //
            this.barDockControlLeft.CausesValidation = false;
            this.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 25);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 512);
            //
            //barDockControlRight
            //
            this.barDockControlRight.CausesValidation = false;
            this.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControlRight.Location = new System.Drawing.Point(576, 25);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 512);
            //
            //FormHeader1
            //
            this.FormHeader1.Anchor = (System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) | System.Windows.Forms.AnchorStyles.Right);
            this.FormHeader1.Location = new System.Drawing.Point(2, 24);
            this.FormHeader1.Name = "FormHeader1";
            this.FormHeader1.Size = new System.Drawing.Size(576, 90);
            this.FormHeader1.TabIndex = 4;
            //
            //XtraTabControl1
            //
            this.XtraTabControl1.Anchor = (System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) | System.Windows.Forms.AnchorStyles.Left) | System.Windows.Forms.AnchorStyles.Right);
            this.XtraTabControl1.Location = new System.Drawing.Point(0, 120);
            this.XtraTabControl1.Name = "XtraTabControl1";
            this.XtraTabControl1.Padding = new System.Windows.Forms.Padding(5);
            this.XtraTabControl1.SelectedTabPage = this.XtraTabPage_Account;
            this.XtraTabControl1.Size = new System.Drawing.Size(576, 422);
            this.XtraTabControl1.TabIndex = 5;
            this.XtraTabControl1.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
				this.XtraTabPage_Account,
				this.XtraTabPage_Proposals,
				this.XtraTabPage_Contacts,
				this.XtraTabPage_Notes,
				this.XtraTabPage_Events,
				this.XtraTabPage_Messages
			});
            //
            //XtraTabPage_Account
            //
            this.XtraTabPage_Account.Appearance.PageClient.BackColor = System.Drawing.Color.Maroon;
            this.XtraTabPage_Account.Appearance.PageClient.BackColor2 = System.Drawing.Color.Maroon;
            this.XtraTabPage_Account.Appearance.PageClient.BorderColor = System.Drawing.Color.Maroon;
            this.XtraTabPage_Account.Appearance.PageClient.Options.UseBackColor = true;
            this.XtraTabPage_Account.Appearance.PageClient.Options.UseBorderColor = true;
            this.XtraTabPage_Account.Controls.Add(this.Page_Account1);
            this.XtraTabPage_Account.Name = "XtraTabPage_Account";
            this.XtraTabPage_Account.Size = new System.Drawing.Size(568, 392);
            this.XtraTabPage_Account.Tag = "account";
            this.XtraTabPage_Account.Text = "Account";
            //
            //Page_Account1
            //
            this.Page_Account1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Page_Account1.Location = new System.Drawing.Point(0, 0);
            this.Page_Account1.Name = "Page_Account1";
            this.Page_Account1.Size = new System.Drawing.Size(568, 392);
            this.Page_Account1.TabIndex = 0;
            //
            //XtraTabPage_Proposals
            //
            this.XtraTabPage_Proposals.Controls.Add(this.Page_Proposals1);
            this.XtraTabPage_Proposals.Name = "XtraTabPage_Proposals";
            this.XtraTabPage_Proposals.Size = new System.Drawing.Size(568, 392);
            this.XtraTabPage_Proposals.Tag = "proposals";
            this.XtraTabPage_Proposals.Text = "Proposals";
            //
            //Page_Proposals1
            //
            this.Page_Proposals1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Page_Proposals1.Location = new System.Drawing.Point(0, 0);
            this.Page_Proposals1.Name = "Page_Proposals1";
            this.Page_Proposals1.Size = new System.Drawing.Size(568, 392);
            this.Page_Proposals1.TabIndex = 0;
            //
            //XtraTabPage_Contacts
            //
            this.XtraTabPage_Contacts.Controls.Add(this.Page_Contacts1);
            this.XtraTabPage_Contacts.Name = "XtraTabPage_Contacts";
            this.XtraTabPage_Contacts.Size = new System.Drawing.Size(568, 392);
            this.XtraTabPage_Contacts.Tag = "contacts";
            this.XtraTabPage_Contacts.Text = "Contacts";
            //
            //Page_Contacts1
            //
            this.Page_Contacts1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Page_Contacts1.Location = new System.Drawing.Point(0, 0);
            this.Page_Contacts1.Name = "Page_Contacts1";
            this.Page_Contacts1.Size = new System.Drawing.Size(568, 392);
            this.Page_Contacts1.TabIndex = 0;
            //
            //XtraTabPage_Notes
            //
            this.XtraTabPage_Notes.Controls.Add(this.Page_Notes1);
            this.XtraTabPage_Notes.Name = "XtraTabPage_Notes";
            this.XtraTabPage_Notes.Size = new System.Drawing.Size(568, 392);
            this.XtraTabPage_Notes.Tag = "notes";
            this.XtraTabPage_Notes.Text = "Notes";
            //
            //Page_Notes1
            //
            this.Page_Notes1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Page_Notes1.Location = new System.Drawing.Point(0, 0);
            this.Page_Notes1.Name = "Page_Notes1";
            this.Page_Notes1.Size = new System.Drawing.Size(568, 392);
            this.Page_Notes1.TabIndex = 0;
            //
            //XtraTabPage_Events
            //
            this.XtraTabPage_Events.Controls.Add(this.Page_Events1);
            this.XtraTabPage_Events.Name = "XtraTabPage_Events";
            this.XtraTabPage_Events.Size = new System.Drawing.Size(568, 392);
            this.XtraTabPage_Events.Tag = "events";
            this.XtraTabPage_Events.Text = "Events";
            //
            //Page_Events1
            //
            this.Page_Events1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Page_Events1.Location = new System.Drawing.Point(0, 0);
            this.Page_Events1.Name = "Page_Events1";
            this.Page_Events1.Size = new System.Drawing.Size(568, 392);
            this.Page_Events1.TabIndex = 0;
            //
            //XtraTabPage_Messages
            //
            this.XtraTabPage_Messages.Controls.Add(this.Page_Messages1);
            this.XtraTabPage_Messages.Name = "XtraTabPage_Messages";
            this.XtraTabPage_Messages.Size = new System.Drawing.Size(568, 392);
            this.XtraTabPage_Messages.Tag = "messages";
            this.XtraTabPage_Messages.Text = "Messages";
            //
            //Page_Messages1
            //
            this.Page_Messages1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Page_Messages1.Location = new System.Drawing.Point(0, 0);
            this.Page_Messages1.Name = "Page_Messages1";
            this.Page_Messages1.Size = new System.Drawing.Size(568, 392);
            this.Page_Messages1.TabIndex = 0;
            //
            //Form_DebtUpdate
            //
            this.AutoScaleDimensions = new System.Drawing.SizeF(6f, 13f);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(576, 565);
            this.Controls.Add(this.FormHeader1);
            this.Controls.Add(this.XtraTabControl1);
            this.Controls.Add(this.barDockControlLeft);
            this.Controls.Add(this.barDockControlRight);
            this.Controls.Add(this.barDockControlBottom);
            this.Controls.Add(this.barDockControlTop);
            this.Name = "Form_DebtUpdate";
            this.Text = "Debt information for client";
            ((System.ComponentModel.ISupportInitialize)this.DxErrorProvider1).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.barManager1).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.RepositoryItemTextEdit_ID).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.XtraTabControl1).EndInit();
            this.XtraTabControl1.ResumeLayout(false);
            this.XtraTabPage_Account.ResumeLayout(false);
            this.XtraTabPage_Proposals.ResumeLayout(false);
            this.XtraTabPage_Contacts.ResumeLayout(false);
            this.XtraTabPage_Notes.ResumeLayout(false);
            this.XtraTabPage_Events.ResumeLayout(false);
            this.XtraTabPage_Messages.ResumeLayout(false);
            this.ResumeLayout(false);

        }
        internal DevExpress.XtraBars.BarManager barManager1;
        internal DevExpress.XtraBars.Bar MainMenu;
        internal DevExpress.XtraBars.Bar StatusBar;
        internal DevExpress.XtraBars.BarDockControl barDockControlTop;
        internal DevExpress.XtraBars.BarDockControl barDockControlBottom;
        internal DevExpress.XtraBars.BarDockControl barDockControlLeft;
        internal DevExpress.XtraBars.BarDockControl barDockControlRight;
        internal FormHeader FormHeader1;
        internal DevExpress.XtraTab.XtraTabControl XtraTabControl1;
        internal DevExpress.XtraTab.XtraTabPage XtraTabPage_Account;
        internal Page_Account Page_Account1;
        internal DevExpress.XtraTab.XtraTabPage XtraTabPage_Contacts;
        internal Page_Contacts Page_Contacts1;
        internal DevExpress.XtraTab.XtraTabPage XtraTabPage_Notes;
        internal Page_Notes Page_Notes1;
        internal DevExpress.XtraTab.XtraTabPage XtraTabPage_Events;
        internal Page_Events Page_Events1;
        internal DevExpress.XtraTab.XtraTabPage XtraTabPage_Messages;
        internal Page_Messages Page_Messages1;
        internal DevExpress.XtraBars.BarSubItem BarSubItem_File;
        internal DevExpress.XtraBars.BarSubItem BarSubItem_Items;
        internal DevExpress.XtraBars.BarSubItem BarSubItem_View;
        internal DevExpress.XtraBars.BarSubItem BarSubItem_Letters;
        internal DevExpress.XtraBars.BarSubItem BarSubItem_Reports;
        internal DevExpress.XtraBars.BarButtonItem BarButtonItem_File_Exit;
        internal DevExpress.XtraBars.BarStaticItem BarStaticItem_Name;
        internal DevExpress.XtraBars.BarStaticItem BarStaticItem_CDP;
        internal DevExpress.XtraBars.BarStaticItem BarStaticItem_CDD;
        internal DevExpress.XtraBars.BarStaticItem BarStaticItem_CDV;
        internal DevExpress.XtraTab.XtraTabPage XtraTabPage_Proposals;
        internal Page_Proposals Page_Proposals1;
        internal DevExpress.XtraBars.BarEditItem BarStaticItem_ID;
        internal DevExpress.XtraEditors.Repository.RepositoryItemTextEdit RepositoryItemTextEdit_ID;
    }
}