#region "Copyright 2000-2012 DebtPlus, L.L.C."
//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}
#endregion "Copyright 2000-2012 DebtPlus, L.L.C."

using System;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using DebtPlus.Interfaces.Client;
using DebtPlus.Interfaces.Creditor;
using DebtPlus.Interfaces.Debt;
using DebtPlus.LINQ;
using DebtPlus.Svc.DataSets;

public class DebtUpdateClass : IDisposable, IDebt, IClient, ICreditor, DebtPlus.UI.Debt.CS.Update.IDebtContext
{
    /// <summary>
    /// Formatting string for converting decimal numbers to suitable strings
    /// </summary>
    public static readonly string DecimalFormat = "{0:c}";

    public DataSet DebtDS = new DataSet("DebtDS");

    private string _creditor = string.Empty;

    // Local storage
    private IDebtRecord privateDebtRecord = null;

    /// <summary>
    /// Initialize the class
    /// </summary>
    /// <remarks></remarks>
    public DebtUpdateClass(ClientDataSet ClientDS) : this()
    {
        this.ClientDS = ClientDS;
    }

    /// <summary>
    /// Current dataset for updating debt local storage
    /// </summary>
    /// <remarks></remarks>
    /// <summary>
    /// Initialize the class
    /// </summary>
    /// <remarks></remarks>
    private DebtUpdateClass()
    {
    }

    /// <summary>
    /// Pointer to the client update data set which holds the current client information
    /// </summary>
    /// <value></value>
    /// <returns></returns>
    /// <remarks></remarks>
    public ClientDataSet ClientDS { get; set; }

    /// <summary>
    /// Client ID information
    /// </summary>
    /// <value></value>
    /// <returns></returns>
    /// <remarks></remarks>
    public Int32 ClientId
    {
        get
        {
            return ClientDS.ClientId;
        }
        set
        {
            throw new NotImplementedException();
        }
    }

    /// <summary>
    /// Creditor ID from the debt record
    /// </summary>
    /// <value></value>
    /// <returns></returns>
    /// <remarks></remarks>
    public string Creditor
    {
        get
        {
            return DebtRecord.Creditor;
        }
        set
        {
            throw new NotImplementedException();
        }
    }

    /// <summary>
    /// The current debt record id.
    /// </summary>
    /// <value></value>
    /// <returns></returns>
    /// <remarks></remarks>
    public Int32 DebtId
    {
        get
        {
            return DebtRecord.client_creditor;
        }
        set
        {
            throw new NotImplementedException();
        }
    }

    /// <summary>
    /// Pointer to the current debt record being edited
    /// </summary>
    /// <value></value>
    /// <returns></returns>
    /// <remarks></remarks>
    public IDebtRecord DebtRecord
    {
        get
        {
            return privateDebtRecord;
        }
        set
        {
            privateDebtRecord = value;
        }
    }

    /// <summary>
    /// Pointer to the debt update context information
    /// </summary>
    /// <value></value>
    /// <returns></returns>
    /// <remarks></remarks>
    public DebtUpdateClass DebtUpdateContext
    {
        get
        {
            return this;
        }
    }

    #region " IDisposable Support "

    private bool disposedValue = false;
    // To detect redundant calls

    // This code added by Visual Basic to correctly implement the disposable pattern.
    public void Dispose()
    {
        // Do not change this code.  Put cleanup code in Dispose(ByVal disposing As Boolean) above.
        Dispose(true);
        GC.SuppressFinalize(this);
    }

    protected virtual void Dispose(bool disposing)
    {
        if (!this.disposedValue)
        {
            if (disposing)
            {
                DebtDS.Dispose();
            }

            // Release the pointers to other objects so that they may be disposed later
            ClientDS = null;
            privateDebtRecord = null;
        }
        this.disposedValue = true;
    }

    #endregion " IDisposable Support "

    #region " creditor addresses "

    public string FormattedAddress(string Type)
    {
        string answer = string.Empty;

        // Find the row for the creditor address of the indicated type
        DataTable tbl = CreditorAddressesTable();
        if (tbl != null)
        {
            DataRow row = tbl.Rows.Find(Type);

            if (row != null)
            {
                // Add the attention string. The attention string is not part of the standard address record.
                string AddressString = string.Empty;
                string AttnString = DebtPlus.Utils.Nulls.DStr(row["Attn"]).Trim();

                if (AttnString != string.Empty)
                {
                    AttnString = "Attn: " + AttnString;
                }

                // Fill in the other address information
                Int32 AddressID = DebtPlus.Utils.Nulls.DInt(row["AddressID"]);
                if (AddressID > 0)
                {
                    using (DebtPlus.LINQ.BusinessContext bc = new DebtPlus.LINQ.BusinessContext())
                    {
                        // We need to use the flags that will cause the addresses to be fully formatted when converted to a string.
                        // So, we can't use the default creation. We need to do a specific creation of our class object.
                        DebtPlus.LINQ.address q = bc.addresses.Where(s => s.Id == AddressID).Select(a => new CreditorAddressEntry(a)).FirstOrDefault();
                        if (q != null)
                        {
                            AddressString = q.ToString();
                        }
                    }
                }

                if (AttnString != string.Empty && AddressString != string.Empty)
                {
                    answer = AttnString + Environment.NewLine + AddressString;
                }
                else
                {
                    answer = AttnString + AddressString;
                }
            }
        }

        return answer;
    }

    /// <summary>
    /// List of creditor addresses
    /// </summary>
    internal DataTable CreditorAddressesTable()
    {
        const string MyTableName = "creditor_addresses";
        DataTable tbl = DebtDS.Tables[MyTableName];

        if (tbl == null)
        {
            lock (DebtDS)
            {
                try
                {
                    using (SqlCommand cmd = new SqlCommand())
                    {
                        var _with1 = cmd;
                        _with1.Connection = new SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString);
                        _with1.CommandText = "SELECT [oID],[Creditor],[Type],[Attn],[AddressID] FROM creditor_addresses WHERE [creditor] = @creditor";
                        _with1.CommandType = CommandType.Text;
                        _with1.Parameters.Add("@creditor", SqlDbType.VarChar, 10).Value = Creditor;

                        using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                        {
                            da.Fill(DebtDS, MyTableName);
                        }
                    }

                    tbl = DebtDS.Tables[MyTableName];
                    if (tbl.PrimaryKey.GetUpperBound(0) < 0)
                        tbl.PrimaryKey = new DataColumn[] { tbl.Columns["Type"] };
                    var _with2 = tbl.Columns["oID"];
                    _with2.AutoIncrement = true;
                    _with2.AutoIncrementSeed = -1;
                    _with2.AutoIncrementStep = -1;
                }
                catch (DBConcurrencyException ex)
                {
                    DebtPlus.Svc.Common.ErrorHandling.HandleErrors(ex);
                }
                catch (SqlException ex)
                {
                    //Errors.ExceptionMessageBox(ex, "Error reading " + MyTableName + " table")
                }
            }
        }

        return tbl;
    }

    /// <summary>
    /// Save the changes to the creditor addresses
    /// </summary>

    internal void SaveCreditorAddresses()
    {
        SqlCommand UpdateCmd = new SqlCommand();
        SqlCommand InsertCmd = new SqlCommand();
        SqlCommand DeleteCmd = new SqlCommand();

        try
        {
            // Update an address for the creditor
            var _with3 = UpdateCmd;
            _with3.Connection = new SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString);
            _with3.CommandText = "xpr_update_creditor_addresses";
            _with3.CommandType = CommandType.StoredProcedure;
            _with3.Parameters.Add("@oID", SqlDbType.Int, 0, "oID");
            _with3.Parameters.Add("@Attn", SqlDbType.VarChar, 80, "Attn");
            _with3.Parameters.Add("@AddressID", SqlDbType.Int, 0, "AddressID");

            // Insert a new address for the creditor
            var _with4 = InsertCmd;
            _with4.Connection = new SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString);
            _with4.CommandText = "xpr_insert_creditor_addresses";
            _with4.CommandType = CommandType.StoredProcedure;
            _with4.Parameters.Add("@creditor", SqlDbType.VarChar, 10, "creditor");
            _with4.Parameters.Add("@type", SqlDbType.VarChar, 1, "type");
            _with4.Parameters.Add("@attn", SqlDbType.VarChar, 80, "attn");
            _with4.Parameters.Add("@AddressID", SqlDbType.Int, 0, "AddressID");
            _with4.Parameters.Add("@oID", SqlDbType.Int, 0, "oID").Direction = ParameterDirection.ReturnValue;

            // Delete an address for the creditor
            var _with5 = DeleteCmd;
            _with5.Connection = new SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString);
            _with5.CommandText = "xpr_delete_creditor_addresses";
            _with5.CommandType = CommandType.StoredProcedure;
            _with5.Parameters.Add("@oID", SqlDbType.Int, 0, "oID");

            // Do the update on the creditor address table
            using (SqlDataAdapter da = new SqlDataAdapter())
            {
                var _with6 = da;
                _with6.InsertCommand = InsertCmd;
                _with6.UpdateCommand = UpdateCmd;
                _with6.DeleteCommand = DeleteCmd;
                lock (DebtDS)
                {
                    _with6.Update(CreditorAddressesTable());
                }
            }
        }
        catch (SqlException ex)
        {
            //Errors.ExceptionMessageBox(ex, "Error updating creditor address information")
        }
        finally
        {
            InsertCmd.Dispose();
            UpdateCmd.Dispose();
            DeleteCmd.Dispose();
        }
    }

    #endregion " creditor addresses "

    #region " creditor contribution pcts "

    /// <summary>
    /// Creditor contribution percentages
    /// </summary>
    internal DataTable creditor_contribution_pcts_table()
    {
        string MyTableName = "creditor_contribution_pcts";
        DataTable tbl = DebtDS.Tables[MyTableName];

        // If the table is not found then read the information from the database
        if (tbl == null)
        {
            try
            {
                using (SqlCommand cmd = new SqlCommand())
                {
                    var _with7 = cmd;
                    _with7.Connection = new SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString);
                    _with7.CommandText = "select creditor_contribution_pct, effective_date, fairshare_pct_check, fairshare_pct_eft, creditor_type_eft, creditor_type_check, updated, created_by, date_created, dbo.format_contribution_type(creditor_type_eft, creditor_type_check) as status, case when updated <> 0 then effective_date else null end as date_updated, convert(varchar(10), effective_date, 101) + ' 00:00:00' as formatted_effective_date from creditor_contribution_pcts with (nolock) where creditor=@creditor";
                    _with7.CommandType = CommandType.Text;
                    _with7.Parameters.Add("@creditor", SqlDbType.VarChar, 10).Value = Creditor;

                    // Ask the database for the information
                    using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                    {
                        // Lock the tables
                        lock (DebtDS)
                        {
                            da.Fill(DebtDS, MyTableName);
                        }
                    }
                }

                tbl = DebtDS.Tables[MyTableName];

                // When we do an update, the update results in zero rows being updated. IGNORE IT.
            }
            catch (DBConcurrencyException ex)
            {
                DebtPlus.Svc.Common.ErrorHandling.HandleErrors(ex);

                // Something happened in trying to talk to the database.
            }
            catch (SqlException ex)
            {
                //Errors.ExceptionMessageBox(ex, "Error reading " + MyTableName + " table")
            }
        }

        return tbl;
    }

    #endregion " creditor contribution pcts "

    #region " creditor methods "

    /// <summary>
    /// Creditor methods information
    /// </summary>
    internal DataTable CreditorMethodsTable()
    {
        const string MyTableName = "creditor_methods";
        DataTable tbl = DebtDS.Tables[MyTableName];

        if (tbl == null)
        {
            try
            {
                using (SqlCommand cmd = new SqlCommand())
                {
                    var _with8 = cmd;
                    _with8.Connection = new SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString);
                    _with8.CommandText = "SELECT m.*, isnull(b.type,'C') as bank_type, r.description AS formatted_region FROM creditor_methods m with (nolock) left outer join banks b with (nolock) on b.bank = m.bank left outer join regions r with (nolock) on m.region = r.region WHERE m.creditor = @creditor";
                    _with8.Parameters.Add("@creditor", SqlDbType.Int).Value = GetCreditorID();

                    using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                    {
                        da.Fill(DebtDS, MyTableName);
                    }
                }

                tbl = DebtDS.Tables[MyTableName];
                var _with9 = tbl;
                _with9.PrimaryKey = new DataColumn[] { _with9.Columns["creditor_method"] };
                var _with10 = _with9.Columns["creditor_method"];
                _with10.AutoIncrement = true;
                _with10.AutoIncrementSeed = -1;
                _with10.AutoIncrementStep = -1;
            }
            catch (DBConcurrencyException ex)
            {
                DebtPlus.Svc.Common.ErrorHandling.HandleErrors(ex);
            }
            catch (SqlException ex)
            {
                //Errors.ExceptionMessageBox(ex, "Error reading " + MyTableName + " table")
            }
        }

        return tbl;
    }

    #endregion " creditor methods "

    #region " creditors "

    /// <summary>
    /// return the creditor serial number for the default name
    /// </summary>
    public Int32 GetCreditorID()
    {
        return GetCreditorID(Creditor);
    }

    /// <summary>
    /// Creditor information
    /// </summary>
    public DataRow GetCreditorRow()
    {
        return GetCreditorRowByID(Creditor);
    }

    /// <summary>
    /// Creditor information
    /// </summary>
    internal DataTable CreditorTable()
    {
        const string MyTableName = "creditors";
        DataTable tbl = DebtDS.Tables[MyTableName];

        if (tbl == null)
        {
            // Lock the tables
            lock (DebtDS)
            {
                try
                {
                    using (SqlCommand cmd = new SqlCommand())
                    {
                        var _with13 = cmd;
                        _with13.Connection = new SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString);
                        _with13.CommandText = "SELECT * FROM creditors WITH (NOLOCK)";

                        using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                        {
                            da.FillSchema(DebtDS, SchemaType.Source, MyTableName);
                        }
                    }

                    tbl = DebtDS.Tables[MyTableName];
                    var _with14 = tbl;
                    if (_with14.PrimaryKey.GetUpperBound(0) < 0)
                    {
                        _with14.PrimaryKey = new DataColumn[] { _with14.Columns["creditor"] };
                    }
                }
                catch (DBConcurrencyException ex)
                {
                    DebtPlus.Svc.Common.ErrorHandling.HandleErrors(ex);
                }
                catch (SqlException ex)
                {
                    //Errors.ExceptionMessageBox(ex, "Error reading " + MyTableName + " table")
                }
            }
        }

        return tbl;
    }

    /// <summary>
    /// return the creditor serial number for the name given
    /// </summary>
    internal Int32 GetCreditorID(string CreditorLabel)
    {
        Int32 answer = -1;
        DataRow row = GetCreditorRowByID(CreditorLabel);
        if (row != null)
        {
            answer = Convert.ToInt32(row["creditor_id"]);
        }
        return answer;
    }

    /// <summary>
    /// return the creditor serial number for the default name
    /// </summary>
    internal string GetCreditorName()
    {
        return GetCreditorName(Creditor);
    }

    /// <summary>
    /// return the creditor serial number for the name given
    /// </summary>
    internal string GetCreditorName(string CreditorLabel)
    {
        string answer = string.Empty;
        DataRow row = GetCreditorRowByID(CreditorLabel);
        if (row != null)
        {
            answer = Convert.ToString(row["creditor_name"]);
        }
        return answer;
    }

    /// <summary>
    /// Creditor information
    /// </summary>
    internal DataRow GetCreditorRowByID(string Creditor)
    {
        DataRow row = null;
        DataTable tbl = CreditorTable();
        if (tbl != null)
        {
            row = tbl.Rows.Find(Creditor);
            if (row != null)
            {
                return row;
            }
        }

        ReadCreditorRowByID(Creditor);
        tbl = CreditorTable();
        if (tbl != null)
        {
            return tbl.Rows.Find(Creditor);
        }

        return null;
    }

    /// <summary>
    /// Creditor information
    /// </summary>
    internal void ReadCreditorRowByID(string Creditor)
    {
        const string MyTableName = "creditors";

        // Lock the dataset
        lock (DebtDS)
        {
            try
            {
                using (SqlCommand cmd = new SqlCommand())
                {
                    var _with11 = cmd;
                    _with11.Connection = new SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString);
                    _with11.CommandText = "SELECT * FROM creditors WITH (NOLOCK) WHERE creditor=@creditor";
                    _with11.Parameters.Add("@creditor", SqlDbType.VarChar, 80).Value = Creditor;

                    using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                    {
                        da.Fill(DebtDS, MyTableName);
                    }
                }

                DataTable tbl = DebtDS.Tables[MyTableName];
                var _with12 = tbl;
                if (_with12.PrimaryKey.GetUpperBound(0) < 0)
                {
                    _with12.PrimaryKey = new DataColumn[] { _with12.Columns["creditor"] };
                }
            }
            catch (DBConcurrencyException ex)
            {
                DebtPlus.Svc.Common.ErrorHandling.HandleErrors(ex);
            }
            catch (SqlException ex)
            {
                //Errors.ExceptionMessageBox(ex, "Error reading " + MyTableName + " table")
            }
        }
    }

    /// <summary>
    /// Record changes to the pending creditors table
    /// </summary>
    internal void SaveCreditors()
    {
        lock (DebtDS)
        {
            try
            {
                using (SqlCommand cmd = new SqlCommand())
                {
                    var _with15 = cmd;
                    _with15.Connection = new SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString);
                    _with15.CommandText = "UPDATE update_creditors SET [sic]=@sic,[creditor_name]=@creditor_name,[comment]=@comment,[division]=@division,[creditor_class]=@creditor_class,[voucher_spacing]=@voucher_spacing,[mail_priority]=@mail_priority,[payment_balance]=@payment_balance,[prohibit_use]=@prohibit_use,[full_disclosure]=@full_disclosure,[suppress_invoice]=@suppress_invoice,[proposal_budget_info]=@proposal_budget_info,[proposal_income_info]=@proposal_income_info,[contrib_cycle]=@contrib_cycle,[contrib_bill_month]=@contrib_bill_month,[pledge_amt]=@pledge_amt,[pledge_cycle]=@pledge_cycle,[pledge_bill_month]=@pledge_bill_month,[min_accept_amt]=@min_accept_amt,[min_accept_pct]=@min_accept_pct,[min_accept_per_bill]=@min_accept_per_bill,[lowest_apr_pct]=@lowest_apr_pct,[medium_apr_pct]=@medium_apr_pct,[highest_apr_pct]=@highest_apr_pct,[medium_apr_amt]=@medium_apr_amt,[highest_apr_amt]=@highest_apr_amt,[max_clients_per_check]=@max_clients_per_check,[max_amt_per_check]=@max_amt_per_check,[max_fairshare_per_debt]=@max_fairshare_per_debt,[chks_per_invoice]=@chks_per_invoice,[returned_mail]=@returned_mail,[po_number]=@po_number,[usual_priority]=@usual_priority,[percent_balance]=@percent_balance,[acceptance_days]=@acceptance_days,[check_payments]=@check_payments,[check_bank]=@check_bank WHERE creditor = @creditor";
                    _with15.CommandType = CommandType.Text;

                    _with15.Parameters.Add("@sic", SqlDbType.VarChar, 80, "sic");
                    _with15.Parameters.Add("@creditor_name", SqlDbType.VarChar, 80, "creditor_name");
                    _with15.Parameters.Add("@comment", SqlDbType.VarChar, 80, "comment");
                    _with15.Parameters.Add("@division", SqlDbType.VarChar, 80, "division");
                    _with15.Parameters.Add("@creditor_class", SqlDbType.Int, 0, "creditor_class");
                    _with15.Parameters.Add("@voucher_spacing", SqlDbType.Int, 0, "voucher_spacing");
                    _with15.Parameters.Add("@mail_priority", SqlDbType.Int, 0, "mail_priority");
                    _with15.Parameters.Add("@payment_balance", SqlDbType.VarChar, 10, "payment_balance");
                    _with15.Parameters.Add("@prohibit_use", SqlDbType.Bit, 0, "prohibit_use");
                    _with15.Parameters.Add("@full_disclosure", SqlDbType.Bit, 0, "full_disclosure");
                    _with15.Parameters.Add("@suppress_invoice", SqlDbType.Bit, 0, "suppress_invoice");
                    _with15.Parameters.Add("@proposal_budget_info", SqlDbType.Bit, 0, "proposal_budget_info");
                    _with15.Parameters.Add("@proposal_income_info", SqlDbType.Bit, 0, "proposal_income_info");
                    _with15.Parameters.Add("@contrib_cycle", SqlDbType.VarChar, 10, "contrib_cycle");
                    _with15.Parameters.Add("@contrib_bill_month", SqlDbType.Int, 0, "contrib_bill_month");
                    _with15.Parameters.Add("@pledge_amt", SqlDbType.Decimal, 0, "pledge_amt");
                    _with15.Parameters.Add("@pledge_cycle", SqlDbType.VarChar, 10, "pledge_cycle");
                    _with15.Parameters.Add("@pledge_bill_month", SqlDbType.Int, 0, "pledge_bill_month");
                    _with15.Parameters.Add("@min_accept_amt", SqlDbType.Decimal, 0, "min_accept_amt");
                    _with15.Parameters.Add("@min_accept_pct", SqlDbType.Float, 0, "min_accept_pct");
                    _with15.Parameters.Add("@min_accept_per_bill", SqlDbType.Decimal, 0, "min_accept_per_bill");
                    _with15.Parameters.Add("@lowest_apr_pct", SqlDbType.Float, 0, "lowest_apr_pct");
                    _with15.Parameters.Add("@medium_apr_pct", SqlDbType.Float, 0, "medium_apr_pct");
                    _with15.Parameters.Add("@highest_apr_pct", SqlDbType.Float, 0, "highest_apr_pct");
                    _with15.Parameters.Add("@medium_apr_amt", SqlDbType.Decimal, 0, "medium_apr_amt");
                    _with15.Parameters.Add("@highest_apr_amt", SqlDbType.Decimal, 0, "highest_apr_amt");
                    _with15.Parameters.Add("@max_clients_per_check", SqlDbType.Int, 0, "max_clients_per_check");
                    _with15.Parameters.Add("@max_amt_per_check", SqlDbType.Decimal, 0, "max_amt_per_check");
                    _with15.Parameters.Add("@max_fairshare_per_debt", SqlDbType.Decimal, 0, "max_fairshare_per_debt");
                    _with15.Parameters.Add("@chks_per_invoice", SqlDbType.Int, 0, "chks_per_invoice");
                    _with15.Parameters.Add("@returned_mail", SqlDbType.DateTime, 0, "returned_mail");
                    _with15.Parameters.Add("@po_number", SqlDbType.VarChar, 80, "po_number");
                    _with15.Parameters.Add("@usual_priority", SqlDbType.Int, 0, "usual_priority");
                    _with15.Parameters.Add("@percent_balance", SqlDbType.Float, 0, "percent_balance");
                    _with15.Parameters.Add("@acceptance_days", SqlDbType.Int, 0, "acceptance_days");
                    _with15.Parameters.Add("@check_payments", SqlDbType.Int, 0, "check_payments");
                    _with15.Parameters.Add("@check_bank", SqlDbType.Int, 0, "check_bank");

                    _with15.Parameters.Add("@creditor", SqlDbType.VarChar, 10, "creditor");

                    using (SqlDataAdapter da = new SqlDataAdapter())
                    {
                        da.UpdateCommand = cmd;
                        da.Update(CreditorTable());
                    }
                }
            }
            catch (SqlException ex)
            {
                //Errors.ExceptionMessageBox(ex, "Error updating creditor information")
            }
        }
    }

    #endregion " creditors "

    #region " Debt Notes "

    /// <summary>
    /// List of creditor debt notes
    /// </summary>
    public DataTable DebtNotesTable()
    {
        string MyTableName = "debt_notes";
        DataTable tbl = DebtDS.Tables[MyTableName];

        // If the table is not found then read the information from the database
        if (tbl == null)
        {
            lock (DebtDS)
            {
                try
                {
                    using (SqlCommand cmd = new SqlCommand())
                    {
                        var _with16 = cmd;
                        _with16.Connection = new SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString);
                        _with16.CommandText = "SELECT * FROM debt_notes dn WITH (NOLOCK) WHERE type<>'PR' AND client_creditor=@client_creditor";
                        _with16.CommandType = CommandType.Text;
                        _with16.Parameters.Add("@client_creditor", SqlDbType.Int).Value = DebtId;

                        // Ask the database for the information
                        using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                        {
                            da.Fill(DebtDS, MyTableName);
                            tbl = DebtDS.Tables[MyTableName];
                        }
                    }

                    var _with17 = tbl;
                    if (_with17.PrimaryKey.GetUpperBound(0) < 0)
                        _with17.PrimaryKey = new DataColumn[] { _with17.Columns["debt_note"] };
                    var _with18 = _with17.Columns["debt_note"];
                    _with18.AutoIncrement = true;
                    _with18.AutoIncrementSeed = -1;
                    _with18.AutoIncrementStep = -1;
                    _with17.Columns["type"].DefaultValue = "AN";
                    _with17.Columns["created_by"].DefaultValue = "Me";
                    _with17.Columns["note_date"].DefaultValue = System.DateTime.Now.Date;
                    _with17.Columns["note_amount"].DefaultValue = 0m;

                    // When we do an update, the update results in zero rows being updated. IGNORE IT.
                }
                catch (DBConcurrencyException ex)
                {
                    DebtPlus.Svc.Common.ErrorHandling.HandleErrors(ex);

                    // Something happend in trying to talk to the database.
                }
                catch (SqlException ex)
                {
                    //Errors.ExceptionMessageBox(ex, "Error reading " + MyTableName + " table")
                }
            }
        }

        return tbl;
    }

    #endregion " Debt Notes "
}
