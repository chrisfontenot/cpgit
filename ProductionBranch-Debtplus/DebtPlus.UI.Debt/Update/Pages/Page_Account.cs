#region "Copyright 2000-2012 DebtPlus, L.L.C."

//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}

#endregion "Copyright 2000-2012 DebtPlus, L.L.C."

using System;
using System.ComponentModel;
using System.Windows.Forms;
using DebtPlus.LINQ;
using DebtPlus.UI.Debt.Update.controls;
using DevExpress.XtraEditors.Controls;

namespace DebtPlus.UI.Debt.Update.pages
{
    internal partial class Page_Account : ControlBaseDebt
    {
        public Page_Account()
            : base()
        {
            InitializeComponent();
        }

        private DebtPlus.Interfaces.Debt.IDebtRecord currentDebtRecord;
        public override void ReadForm(DebtPlus.Interfaces.Debt.IDebtRecord CurrentDebtRecord)
        {
            // Save the pointer to the editing record for the save operation
            this.currentDebtRecord = CurrentDebtRecord;

            UnRegisterHandlers();
            try
            {
                account_number.EditValue           = currentDebtRecord.account_number;
                message.EditValue                  = currentDebtRecord.Message;
                disbursement_factor.EditValue      = currentDebtRecord.display_disbursement_factor;
                sched_payment.EditValue            = currentDebtRecord.sched_payment;
                dmp_payout_interest.EditValue      = currentDebtRecord.dmp_payout_interest;
                last_payment_date_b4_dmp.EditValue = currentDebtRecord.last_payment_date_b4_dmp;
                non_dmp_interest.EditValue         = currentDebtRecord.non_dmp_interest;
                non_dmp_payment.EditValue          = currentDebtRecord.non_dmp_payment;
                dmp_interest.EditValue             = currentDebtRecord.dmp_interest;

                // Read the balance information
                BalanceInformation1.ReadForm(currentDebtRecord);

                // Set the payments this month and last month from the balance record as well
                payments_month_0.Text       = string.Format("{0:c}", currentDebtRecord.payments_month_0);
                payments_month_1.Text       = string.Format("{0:c}", currentDebtRecord.payments_month_1);
                orig_dmp_payment.Text       = string.Format("{0:c}", currentDebtRecord.orig_dmp_payment);
                interest_this_creditor.Text = string.Format("{0:c}", currentDebtRecord.interest_this_creditor);
                payments_this_creditor.Text = string.Format("{0:c}", currentDebtRecord.payments_this_creditor - currentDebtRecord.returns_this_creditor);

                // Set the payout date. A null value is acceptable for the payout date.
                System.Nullable<DateTime> payoutDate = DebtPlus.Utils.Nulls.v_DateTime(currentDebtRecord.expected_payout_date);
                if (payoutDate.HasValue)
                {
                    expected_payout_date.Text = payoutDate.Value.ToShortDateString();
                }

                // Try to find the last payment date
                last_payment.Text = LastPaymentDate();

                // Proposed balance figure
                double ProposedPercent = 1.0;
                if (currentDebtRecord.percent_balance >= 0.0)
                {
                    ProposedPercent = currentDebtRecord.percent_balance;
                    if (ProposedPercent < 0.0)
                    {
                        ProposedPercent = 0.0;
                    }
                    else if (ProposedPercent > 1.0)
                    {
                        ProposedPercent = 1.0;
                    }
                }
                percent_balance.Text = string.Format("Proposed Balance: {0:p3}", ProposedPercent);

                System.Nullable<DateTime> DisbursementDateChange = DebtPlus.Utils.Nulls.v_DateTime(currentDebtRecord.date_disp_changed);
                date_disp_changed.Text = (DisbursementDateChange.HasValue) ? string.Format("as of {0:d}", DisbursementDateChange.Value) : string.Empty;

                dmp_interest.Properties.NullText = string.Empty;
                System.Nullable<double> creditorInterestRate = DebtPlus.Utils.Nulls.v_Double(currentDebtRecord.creditor_interest);
                if (creditorInterestRate.HasValue)
                {
                    double Rate = creditorInterestRate.Value;
                    while (Rate >= 1.0)
                    {
                        Rate /= 100.0;
                    }
                    dmp_interest.Properties.NullText = string.Format("{0:p3}", Rate);
                }
            }
            finally
            {
                RegisterHandlers();
            }
        }

        /// <summary>
        /// Save any changes to the forms when we switch away from our tab
        /// </summary>
        public override void SaveForm()
        {
            // Do the balance update first
            BalanceInformation1.SaveForm();

            // Follow it with our fields
            currentDebtRecord.display_disbursement_factor = DebtPlus.Utils.Nulls.v_Decimal(disbursement_factor.EditValue).GetValueOrDefault();
            currentDebtRecord.sched_payment               = DebtPlus.Utils.Nulls.v_Decimal(sched_payment.EditValue).GetValueOrDefault();
            currentDebtRecord.non_dmp_payment             = DebtPlus.Utils.Nulls.v_Decimal(non_dmp_payment.EditValue).GetValueOrDefault();
            currentDebtRecord.last_payment_date_b4_dmp    = DebtPlus.Utils.Nulls.v_DateTime(last_payment_date_b4_dmp.EditValue);
            currentDebtRecord.dmp_payout_interest         = DebtPlus.Utils.Nulls.v_Double(dmp_payout_interest.EditValue);
            currentDebtRecord.non_dmp_interest            = DebtPlus.Utils.Nulls.v_Double(non_dmp_interest.EditValue).GetValueOrDefault();
            currentDebtRecord.dmp_interest                = DebtPlus.Utils.Nulls.v_Double(dmp_interest.EditValue);
            currentDebtRecord.account_number              = (DebtPlus.Utils.Nulls.v_String(account_number.EditValue) ?? string.Empty).Trim();
            currentDebtRecord.Message                     = System.String.IsNullOrWhiteSpace(DebtPlus.Utils.Nulls.v_String(message.EditValue)) ? null : DebtPlus.Utils.Nulls.v_String(message.EditValue).Trim();
        }

        /// <summary>
        /// Read and display the current values when we switch to our tab
        /// </summary>
        /// <summary>
        /// When the account number changes, re-validate the account number again.
        /// </summary>
        private void account_number_EditValueChanged(object sender, EventArgs e)
        {
            UnRegisterHandlers();
            try
            {
                // When the account number changes, force the lookup of the payment information.
                currentDebtRecord.payment_rpps_mask = System.DBNull.Value;
                currentDebtRecord.rpps_mask         = System.DBNull.Value;
            }
            finally
            {
                RegisterHandlers();
            }
        }

        /// <summary>
        /// When the balance changes, recalculate the disbursement factor
        /// </summary>
        private void BalanceInformation1_BalanceChanged(object Sender, EventArgs e)
        {
            UnRegisterHandlers();
            try
            {
                // Refresh the disbursement factor
                disbursement_factor.EditValue = currentDebtRecord.display_disbursement_factor;

                // Do this only if there is a balance on the debt
                if (DebtPlus.Utils.Nulls.DBool(currentDebtRecord.ccl_zero_balance))
                {
                    return;
                }

                // If the balance is changed then update the minimum disbursement factor
                decimal curMinimum = currentDebtRecord.MinimumPayment(BalanceInformation1.CurrentBalance);

                // If there is no disbursement factor then set it
                if (currentDebtRecord.disbursement_factor <= 0m && curMinimum > 0m)
                {
                    disbursement_factor.EditValue = curMinimum;
                    return;
                }

                // If the balance is being changed then ask if the disbursement factor is to be changed as well
                if (curMinimum > 0m && DebtPlus.Utils.Nulls.DDec(currentDebtRecord.disbursement_factor) < curMinimum)
                {
                    if (DebtPlus.Data.Forms.MessageBox.Show("Do you also wish to adjust the disbursement factor now?", "Adjust Disbursement Amount", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1) == DialogResult.Yes)
                    {
                        disbursement_factor.EditValue = curMinimum;
                    }
                }
            }
            finally
            {
                disbursement_factor.Refresh();
                sched_payment.Refresh();
                RegisterHandlers();
            }
        }

        private void disbursement_factor_CustomDisplayText(object sender, CustomDisplayTextEventArgs e)
        {
            string answer = string.Empty;

            // Find the value that should be displayed
            if (e.Value != null && !object.ReferenceEquals(e.Value, DBNull.Value))
            {
                decimal DisbursementFactor = Convert.ToDecimal(e.Value);

                // If this is a normal debt then limit it to the balance
                if (!DebtPlus.Utils.Nulls.DBool(currentDebtRecord.ccl_zero_balance))
                {
                    // Find the proper balance information
                    decimal CurrentBalance = BalanceInformation1.CurrentBalance;
                    if (CurrentBalance < 0m)
                    {
                        CurrentBalance = 0m;
                    }

                    // Ensure that the disbursement factor is not too big
                    if (DisbursementFactor > CurrentBalance)
                    {
                        DisbursementFactor = CurrentBalance;
                    }
                }

                // Return the proper string value
                answer = string.Format("{0:c}", DisbursementFactor);
            }

            // Update the text field for the control with the formatted answer
            e.DisplayText = answer;
        }

        /// <summary>
        /// Update the status figure when the disbursement is changed
        /// </summary>
        private void disbursement_factor_EditValueChanged(object sender, EventArgs e)
        {
            UnRegisterHandlers();
            try
            {
                currentDebtRecord.display_disbursement_factor = DebtPlus.Utils.Nulls.v_Decimal(disbursement_factor.EditValue).GetValueOrDefault();
                date_disp_changed.Text = string.Format("{0:d}", System.DateTime.Now);
            }
            finally
            {
                RegisterHandlers();
            }
        }

        /// <summary>
        /// Complain if the amount is too small
        /// </summary>
        private void disbursement_factor_Validating(object sender, CancelEventArgs e)
        {
            UnRegisterHandlers();
            try
            {
                decimal NewAmount = 0m;
                if (disbursement_factor.EditValue != null && !object.ReferenceEquals(disbursement_factor.EditValue, DBNull.Value))
                {
                    NewAmount = System.Math.Truncate(Convert.ToDecimal(disbursement_factor.EditValue) * 100M) / 100M;
                }

                decimal MinimumAmount = currentDebtRecord.MinimumPayment();

                if (MinimumAmount > 0m && NewAmount < MinimumAmount)
                {
                    if (DebtPlus.Data.Forms.MessageBox.Show(string.Format("The minimum payment for this debt should be {0:c}." + Environment.NewLine + "Do you wish to use this amount?", MinimumAmount), "Minimum Amount", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button1) == DialogResult.Yes)
                    {
                        disbursement_factor.EditValue = MinimumAmount;
                    }
                }
            }
            finally
            {
                RegisterHandlers();
            }
        }

        /// <summary>
        /// Find the last payment date for this debt.
        /// </summary>
        private string LastPaymentDate()
        {
            string result = "NONE";
            if (currentDebtRecord != null)
            {
                if (currentDebtRecord.last_payment_date != null && !object.ReferenceEquals(currentDebtRecord.last_payment_date, DBNull.Value))
                {
                    result = Convert.ToDateTime(currentDebtRecord.last_payment_date).ToShortDateString();
                }
            }

            return result;
        }

        private void RegisterHandlers()
        {
            BalanceInformation1.BalanceChanged    += BalanceInformation1_BalanceChanged;
            account_number.EditValueChanged       += account_number_EditValueChanged;
            disbursement_factor.EditValueChanged  += disbursement_factor_EditValueChanged;
            disbursement_factor.Validating        += disbursement_factor_Validating;
            disbursement_factor.CustomDisplayText += disbursement_factor_CustomDisplayText;
            sched_payment.EditValueChanged        += sched_payment_EditValueChanged;
            non_dmp_payment.EditValueChanged      += non_dmp_payment_EditValueChanged;
        }

        private void UnRegisterHandlers()
        {
            BalanceInformation1.BalanceChanged    -= BalanceInformation1_BalanceChanged;
            account_number.EditValueChanged       -= account_number_EditValueChanged;
            disbursement_factor.EditValueChanged  -= disbursement_factor_EditValueChanged;
            disbursement_factor.Validating        -= disbursement_factor_Validating;
            disbursement_factor.CustomDisplayText -= disbursement_factor_CustomDisplayText;
            sched_payment.EditValueChanged        -= sched_payment_EditValueChanged;
            non_dmp_payment.EditValueChanged      -= non_dmp_payment_EditValueChanged;
        }

        /// <summary>
        /// Make the changes to the scheduled pay immediately reflected in the debt record so that it shows up in the total screen
        /// </summary>
        private void sched_payment_EditValueChanged(object sender, System.EventArgs e)
        {
            currentDebtRecord.sched_payment = DebtPlus.Utils.Nulls.v_Decimal(sched_payment.EditValue).GetValueOrDefault();
        }

        /// <summary>
        /// Make the changes to the non-DMP payment immediately reflected in the debt record so that it shows up in the total screen
        /// </summary>
        private void non_dmp_payment_EditValueChanged(object sender, System.EventArgs e)
        {
            currentDebtRecord.non_dmp_payment = DebtPlus.Utils.Nulls.v_Decimal(non_dmp_payment.EditValue).GetValueOrDefault();
        }

        #region " Windows Form Designer generated code "

        // Form overrides dispose to clean up the component list.
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (components != null)
                {
                    components.Dispose();
                }
            }
            base.Dispose(disposing);
        }

        // Required by the Windows Form Designer
        private System.ComponentModel.IContainer components = null;

        // NOTE: The following procedure is required by the Windows Form Designer
        // It can be modified using the Windows Form Designer.
        // Do not modify it using the code editor.
        [System.Diagnostics.DebuggerStepThrough]
        private void InitializeComponent()
        {
            this.LabelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.LabelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.LabelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.LabelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.LabelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.LabelControl6 = new DevExpress.XtraEditors.LabelControl();
            this.LabelControl7 = new DevExpress.XtraEditors.LabelControl();
            this.LabelControl8 = new DevExpress.XtraEditors.LabelControl();
            this.LabelControl9 = new DevExpress.XtraEditors.LabelControl();
            this.LabelControl10 = new DevExpress.XtraEditors.LabelControl();
            this.LabelControl13 = new DevExpress.XtraEditors.LabelControl();
            this.LabelControl16 = new DevExpress.XtraEditors.LabelControl();
            this.LabelControl15 = new DevExpress.XtraEditors.LabelControl();
            this.LabelControl14 = new DevExpress.XtraEditors.LabelControl();
            this.LabelControl17 = new DevExpress.XtraEditors.LabelControl();
            this.LabelControl18 = new DevExpress.XtraEditors.LabelControl();
            this.expected_payout_date = new DevExpress.XtraEditors.LabelControl();
            this.last_payment = new DevExpress.XtraEditors.LabelControl();
            this.payments_month_1 = new DevExpress.XtraEditors.LabelControl();
            this.payments_month_0 = new DevExpress.XtraEditors.LabelControl();
            this.orig_dmp_payment = new DevExpress.XtraEditors.LabelControl();
            this.account_number = new DevExpress.XtraEditors.TextEdit();
            this.message = new DevExpress.XtraEditors.TextEdit();
            this.disbursement_factor = new DevExpress.XtraEditors.CalcEdit();
            this.sched_payment = new DevExpress.XtraEditors.CalcEdit();
            this.dmp_interest = new DebtPlus.Data.Controls.PercentEdit();
            this.date_disp_changed = new DevExpress.XtraEditors.LabelControl();
            this.percent_balance = new DevExpress.XtraEditors.LabelControl();
            this.dmp_payout_interest = new DebtPlus.Data.Controls.PercentEdit();
            this.GroupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.non_dmp_interest = new DebtPlus.Data.Controls.PercentEdit();
            this.non_dmp_payment = new DevExpress.XtraEditors.CalcEdit();
            this.last_payment_date_b4_dmp = new DevExpress.XtraEditors.DateEdit();
            this.payments_this_creditor = new DevExpress.XtraEditors.LabelControl();
            this.interest_this_creditor = new DevExpress.XtraEditors.LabelControl();
            this.BalanceInformation1 = new BalanceInformation();
            ((System.ComponentModel.ISupportInitialize)this.account_number.Properties).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.message.Properties).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.disbursement_factor.Properties).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.sched_payment.Properties).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.dmp_interest.Properties).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.dmp_payout_interest.Properties).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.GroupControl1).BeginInit();
            this.GroupControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)this.non_dmp_interest.Properties).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.non_dmp_payment.Properties).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.last_payment_date_b4_dmp.Properties.VistaTimeProperties).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.last_payment_date_b4_dmp.Properties).BeginInit();
            this.SuspendLayout();
            //
            //LabelControl1
            //
            this.LabelControl1.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.Horizontal;
            this.LabelControl1.Location = new System.Drawing.Point(43, 16);
            this.LabelControl1.Name = "LabelControl1";
            this.LabelControl1.Size = new System.Drawing.Size(109, 13);
            this.LabelControl1.TabIndex = 0;
            this.LabelControl1.Text = "Original DMP Payment:";
            this.LabelControl1.UseMnemonic = false;
            //
            //LabelControl2
            //
            this.LabelControl2.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.Horizontal;
            this.LabelControl2.Location = new System.Drawing.Point(43, 29);
            this.LabelControl2.Name = "LabelControl2";
            this.LabelControl2.Size = new System.Drawing.Size(109, 13);
            this.LabelControl2.TabIndex = 2;
            this.LabelControl2.Text = "Current Disbursement:";
            this.LabelControl2.UseMnemonic = false;
            //
            //LabelControl3
            //
            this.LabelControl3.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.Horizontal;
            this.LabelControl3.Location = new System.Drawing.Point(27, 42);
            this.LabelControl3.Name = "LabelControl3";
            this.LabelControl3.Size = new System.Drawing.Size(125, 13);
            this.LabelControl3.TabIndex = 4;
            this.LabelControl3.Text = "Last Month Disbursement:";
            this.LabelControl3.UseMnemonic = false;
            //
            //LabelControl4
            //
            this.LabelControl4.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.Horizontal;
            this.LabelControl4.Location = new System.Drawing.Point(57, 55);
            this.LabelControl4.Name = "LabelControl4";
            this.LabelControl4.Size = new System.Drawing.Size(95, 13);
            this.LabelControl4.TabIndex = 6;
            this.LabelControl4.Text = "Last Payment Date:";
            this.LabelControl4.UseMnemonic = false;
            //
            //LabelControl5
            //
            this.LabelControl5.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.Horizontal;
            this.LabelControl5.Location = new System.Drawing.Point(40, 68);
            this.LabelControl5.Name = "LabelControl5";
            this.LabelControl5.Size = new System.Drawing.Size(112, 13);
            this.LabelControl5.TabIndex = 8;
            this.LabelControl5.Text = "Expected Payout Date:";
            this.LabelControl5.UseMnemonic = false;
            //
            //LabelControl6
            //
            this.LabelControl6.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.Horizontal;
            this.LabelControl6.Location = new System.Drawing.Point(16, 124);
            this.LabelControl6.Name = "LabelControl6";
            this.LabelControl6.Size = new System.Drawing.Size(79, 13);
            this.LabelControl6.TabIndex = 16;
            this.LabelControl6.Text = "Account Number";
            //
            //LabelControl7
            //
            this.LabelControl7.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.Horizontal;
            this.LabelControl7.Location = new System.Drawing.Point(16, 148);
            this.LabelControl7.Name = "LabelControl7";
            this.LabelControl7.Size = new System.Drawing.Size(42, 13);
            this.LabelControl7.TabIndex = 18;
            this.LabelControl7.Text = "Message";
            //
            //LabelControl8
            //
            this.LabelControl8.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.Horizontal;
            this.LabelControl8.Location = new System.Drawing.Point(16, 172);
            this.LabelControl8.Name = "LabelControl8";
            this.LabelControl8.Size = new System.Drawing.Size(99, 13);
            this.LabelControl8.TabIndex = 20;
            this.LabelControl8.Text = "Disbursement Factor";
            //
            //LabelControl9
            //
            this.LabelControl9.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.Horizontal;
            this.LabelControl9.Location = new System.Drawing.Point(16, 196);
            this.LabelControl9.Name = "LabelControl9";
            this.LabelControl9.Size = new System.Drawing.Size(94, 13);
            this.LabelControl9.TabIndex = 23;
            this.LabelControl9.Text = "Scheduled Payment";
            //
            //LabelControl10
            //
            this.LabelControl10.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.Horizontal;
            this.LabelControl10.Location = new System.Drawing.Point(16, 220);
            this.LabelControl10.Name = "LabelControl10";
            this.LabelControl10.Size = new System.Drawing.Size(63, 13);
            this.LabelControl10.TabIndex = 25;
            this.LabelControl10.Text = "DMP Interest";
            //
            //LabelControl13
            //
            this.LabelControl13.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.Horizontal;
            this.LabelControl13.Location = new System.Drawing.Point(16, 244);
            this.LabelControl13.Name = "LabelControl13";
            this.LabelControl13.Size = new System.Drawing.Size(80, 13);
            this.LabelControl13.TabIndex = 27;
            this.LabelControl13.Text = "Payout &Interest:";
            //
            //LabelControl16
            //
            this.LabelControl16.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.Horizontal;
            this.LabelControl16.Location = new System.Drawing.Point(8, 76);
            this.LabelControl16.Name = "LabelControl16";
            this.LabelControl16.Size = new System.Drawing.Size(90, 13);
            this.LabelControl16.TabIndex = 4;
            this.LabelControl16.Text = "&Non-DMP Interest:";
            //
            //LabelControl15
            //
            this.LabelControl15.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.Horizontal;
            this.LabelControl15.Location = new System.Drawing.Point(8, 52);
            this.LabelControl15.Name = "LabelControl15";
            this.LabelControl15.Size = new System.Drawing.Size(93, 13);
            this.LabelControl15.TabIndex = 2;
            this.LabelControl15.Text = "&Non-DMP Payment:";
            //
            //LabelControl14
            //
            this.LabelControl14.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.Horizontal;
            this.LabelControl14.Location = new System.Drawing.Point(8, 28);
            this.LabelControl14.Name = "LabelControl14";
            this.LabelControl14.Size = new System.Drawing.Size(95, 13);
            this.LabelControl14.TabIndex = 0;
            this.LabelControl14.Text = "&Last Payment Date:";
            //
            //LabelControl17
            //
            this.LabelControl17.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.Horizontal;
            this.LabelControl17.Location = new System.Drawing.Point(313, 124);
            this.LabelControl17.Name = "LabelControl17";
            this.LabelControl17.Size = new System.Drawing.Size(144, 13);
            this.LabelControl17.TabIndex = 11;
            this.LabelControl17.Text = "Net Payments to this creditor:";
            this.LabelControl17.UseMnemonic = false;
            //
            //LabelControl18
            //
            this.LabelControl18.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.Horizontal;
            this.LabelControl18.Location = new System.Drawing.Point(369, 137);
            this.LabelControl18.Name = "LabelControl18";
            this.LabelControl18.Size = new System.Drawing.Size(87, 13);
            this.LabelControl18.TabIndex = 13;
            this.LabelControl18.Text = "Interest Charged:";
            this.LabelControl18.UseMnemonic = false;
            //
            //expected_payout_date
            //
            this.expected_payout_date.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.expected_payout_date.Location = new System.Drawing.Point(168, 68);
            this.expected_payout_date.Name = "expected_payout_date";
            this.expected_payout_date.Size = new System.Drawing.Size(88, 13);
            this.expected_payout_date.TabIndex = 9;
            this.expected_payout_date.Text = "NOT DETERMINED";
            this.expected_payout_date.UseMnemonic = false;
            //
            //last_payment
            //
            this.last_payment.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.last_payment.Location = new System.Drawing.Point(168, 55);
            this.last_payment.Name = "last_payment";
            this.last_payment.Size = new System.Drawing.Size(28, 13);
            this.last_payment.TabIndex = 7;
            this.last_payment.Text = "NONE";
            this.last_payment.UseMnemonic = false;
            //
            //payments_month_1
            //
            this.payments_month_1.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.payments_month_1.Location = new System.Drawing.Point(168, 42);
            this.payments_month_1.Name = "payments_month_1";
            this.payments_month_1.Size = new System.Drawing.Size(28, 13);
            this.payments_month_1.TabIndex = 5;
            this.payments_month_1.Text = "$0.00";
            this.payments_month_1.UseMnemonic = false;
            //
            //payments_month_0
            //
            this.payments_month_0.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.payments_month_0.Location = new System.Drawing.Point(168, 29);
            this.payments_month_0.Name = "payments_month_0";
            this.payments_month_0.Size = new System.Drawing.Size(28, 13);
            this.payments_month_0.TabIndex = 3;
            this.payments_month_0.Text = "$0.00";
            this.payments_month_0.UseMnemonic = false;
            //
            //orig_dmp_payment
            //
            this.orig_dmp_payment.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.orig_dmp_payment.Location = new System.Drawing.Point(168, 16);
            this.orig_dmp_payment.Name = "orig_dmp_payment";
            this.orig_dmp_payment.Size = new System.Drawing.Size(28, 13);
            this.orig_dmp_payment.TabIndex = 1;
            this.orig_dmp_payment.Text = "$0.00";
            this.orig_dmp_payment.UseMnemonic = false;
            //
            //account_number
            //
            this.account_number.Location = new System.Drawing.Point(120, 120);
            this.account_number.Name = "account_number";
            this.account_number.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.account_number.Properties.MaxLength = 22;
            this.account_number.Properties.NullText = "...Not Specified...";
            this.account_number.Size = new System.Drawing.Size(160, 20);
            this.account_number.TabIndex = 17;
            //
            //message
            //
            this.message.Location = new System.Drawing.Point(120, 144);
            this.message.Name = "message";
            this.message.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.message.Properties.MaxLength = 256;
            this.message.Properties.NullText = "...Not Specified...";
            this.message.Size = new System.Drawing.Size(160, 20);
            this.message.TabIndex = 19;
            //
            //disbursement_factor
            //
            this.disbursement_factor.Location = new System.Drawing.Point(120, 168);
            this.disbursement_factor.Name = "disbursement_factor";
            this.disbursement_factor.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.False;
            this.disbursement_factor.Properties.Appearance.Options.UseTextOptions = true;
            this.disbursement_factor.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.disbursement_factor.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] { new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo) });
            this.disbursement_factor.Properties.DisplayFormat.FormatString = "c2";
            this.disbursement_factor.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.disbursement_factor.Properties.EditFormat.FormatString = "f2";
            this.disbursement_factor.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.disbursement_factor.Properties.Mask.EditMask = "n2";
            this.disbursement_factor.Properties.Precision = 2;
            this.disbursement_factor.Size = new System.Drawing.Size(88, 20);
            this.disbursement_factor.TabIndex = 21;
            //
            //sched_payment
            //
            this.sched_payment.Location = new System.Drawing.Point(120, 192);
            this.sched_payment.Name = "sched_payment";
            this.sched_payment.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.False;
            this.sched_payment.Properties.Appearance.Options.UseTextOptions = true;
            this.sched_payment.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.sched_payment.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] { new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo) });
            this.sched_payment.Properties.DisplayFormat.FormatString = "c2";
            this.sched_payment.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.sched_payment.Properties.EditFormat.FormatString = "f2";
            this.sched_payment.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.sched_payment.Properties.Mask.EditMask = "n2";
            this.sched_payment.Properties.Precision = 2;
            this.sched_payment.Size = new System.Drawing.Size(88, 20);
            this.sched_payment.TabIndex = 24;
            //
            //dmp_interest
            //
            this.dmp_interest.Location = new System.Drawing.Point(120, 216);
            this.dmp_interest.Name = "dmp_interest";
            this.dmp_interest.Properties.Allow100Percent = false;
            this.dmp_interest.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.dmp_interest.Properties.Appearance.Options.UseTextOptions = true;
            this.dmp_interest.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.dmp_interest.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] { new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo) });
            this.dmp_interest.Properties.DisplayFormat.FormatString = "p";
            this.dmp_interest.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.dmp_interest.Properties.EditFormat.FormatString = "p";
            this.dmp_interest.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.dmp_interest.Properties.Mask.EditMask = "n3";
            this.dmp_interest.Properties.Precision = 3;
            this.dmp_interest.Size = new System.Drawing.Size(88, 20);
            this.dmp_interest.TabIndex = 26;
            //
            //date_disp_changed
            //
            this.date_disp_changed.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.Horizontal;
            this.date_disp_changed.Location = new System.Drawing.Point(216, 172);
            this.date_disp_changed.Name = "date_disp_changed";
            this.date_disp_changed.Size = new System.Drawing.Size(77, 13);
            this.date_disp_changed.TabIndex = 22;
            this.date_disp_changed.Text = "as of MM/DD/YY";
            this.date_disp_changed.UseMnemonic = false;
            //
            //percent_balance
            //
            this.percent_balance.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.percent_balance.Location = new System.Drawing.Point(369, 111);
            this.percent_balance.Name = "percent_balance";
            this.percent_balance.Size = new System.Drawing.Size(121, 13);
            this.percent_balance.TabIndex = 10;
            this.percent_balance.Text = "Proposed Balance: 100%";
            this.percent_balance.UseMnemonic = false;
            this.percent_balance.Visible = false;
            //
            //dmp_payout_interest
            //
            this.dmp_payout_interest.Location = new System.Drawing.Point(120, 240);
            this.dmp_payout_interest.Name = "dmp_payout_interest";
            this.dmp_payout_interest.Properties.Allow100Percent = false;
            this.dmp_payout_interest.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.dmp_payout_interest.Properties.Appearance.Options.UseTextOptions = true;
            this.dmp_payout_interest.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.dmp_payout_interest.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] { new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo) });
            this.dmp_payout_interest.Properties.DisplayFormat.FormatString = "p";
            this.dmp_payout_interest.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.dmp_payout_interest.Properties.EditFormat.FormatString = "p";
            this.dmp_payout_interest.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.dmp_payout_interest.Properties.Mask.EditMask = "n3";
            this.dmp_payout_interest.Properties.Precision = 3;
            this.dmp_payout_interest.Size = new System.Drawing.Size(88, 20);
            this.dmp_payout_interest.TabIndex = 28;
            //
            //GroupControl1
            //
            this.GroupControl1.Controls.Add(this.LabelControl16);
            this.GroupControl1.Controls.Add(this.non_dmp_interest);
            this.GroupControl1.Controls.Add(this.non_dmp_payment);
            this.GroupControl1.Controls.Add(this.last_payment_date_b4_dmp);
            this.GroupControl1.Controls.Add(this.LabelControl15);
            this.GroupControl1.Controls.Add(this.LabelControl14);
            this.GroupControl1.Location = new System.Drawing.Point(344, 0);
            this.GroupControl1.Name = "GroupControl1";
            this.GroupControl1.Size = new System.Drawing.Size(224, 100);
            this.GroupControl1.TabIndex = 29;
            this.GroupControl1.Text = "Non-DMP Information";
            //
            //non_dmp_interest
            //
            this.non_dmp_interest.Location = new System.Drawing.Point(112, 72);
            this.non_dmp_interest.Name = "non_dmp_interest";
            this.non_dmp_interest.Properties.Allow100Percent = false;
            this.non_dmp_interest.Properties.Appearance.Options.UseTextOptions = true;
            this.non_dmp_interest.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.non_dmp_interest.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] { new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo) });
            this.non_dmp_interest.Properties.DisplayFormat.FormatString = "p";
            this.non_dmp_interest.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.non_dmp_interest.Properties.EditFormat.FormatString = "p";
            this.non_dmp_interest.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.non_dmp_interest.Properties.Mask.EditMask = "n3";
            this.non_dmp_interest.Properties.Precision = 3;
            this.non_dmp_interest.Size = new System.Drawing.Size(100, 20);
            this.non_dmp_interest.TabIndex = 5;
            //
            //non_dmp_payment
            //
            this.non_dmp_payment.Location = new System.Drawing.Point(112, 48);
            this.non_dmp_payment.Name = "non_dmp_payment";
            this.non_dmp_payment.Properties.Appearance.Options.UseTextOptions = true;
            this.non_dmp_payment.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.non_dmp_payment.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] { new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo) });
            this.non_dmp_payment.Properties.DisplayFormat.FormatString = "c2";
            this.non_dmp_payment.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.non_dmp_payment.Properties.EditFormat.FormatString = "f2";
            this.non_dmp_payment.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.non_dmp_payment.Properties.Mask.EditMask = "n2";
            this.non_dmp_payment.Properties.Precision = 2;
            this.non_dmp_payment.Size = new System.Drawing.Size(100, 20);
            this.non_dmp_payment.TabIndex = 3;
            //
            //last_payment_date_b4_dmp
            //
            this.last_payment_date_b4_dmp.EditValue = null;
            this.last_payment_date_b4_dmp.Location = new System.Drawing.Point(112, 24);
            this.last_payment_date_b4_dmp.Name = "last_payment_date_b4_dmp";
            this.last_payment_date_b4_dmp.Properties.Appearance.Options.UseTextOptions = true;
            this.last_payment_date_b4_dmp.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.last_payment_date_b4_dmp.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] { new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo) });
            this.last_payment_date_b4_dmp.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] { new DevExpress.XtraEditors.Controls.EditorButton() });
            this.last_payment_date_b4_dmp.Size = new System.Drawing.Size(100, 20);
            this.last_payment_date_b4_dmp.TabIndex = 1;
            //
            //payments_this_creditor
            //
            this.payments_this_creditor.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.payments_this_creditor.Location = new System.Drawing.Point(465, 124);
            this.payments_this_creditor.Name = "payments_this_creditor";
            this.payments_this_creditor.Size = new System.Drawing.Size(28, 13);
            this.payments_this_creditor.TabIndex = 12;
            this.payments_this_creditor.Text = "$0.00";
            this.payments_this_creditor.UseMnemonic = false;
            //
            //interest_this_creditor
            //
            this.interest_this_creditor.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.interest_this_creditor.Location = new System.Drawing.Point(465, 137);
            this.interest_this_creditor.Name = "interest_this_creditor";
            this.interest_this_creditor.Size = new System.Drawing.Size(28, 13);
            this.interest_this_creditor.TabIndex = 14;
            this.interest_this_creditor.Text = "$0.00";
            this.interest_this_creditor.UseMnemonic = false;
            //
            //BalanceInformation1
            //
            this.BalanceInformation1.Location = new System.Drawing.Point(312, 156);
            this.BalanceInformation1.Name = "BalanceInformation1";
            this.BalanceInformation1.Size = new System.Drawing.Size(256, 104);
            this.BalanceInformation1.TabIndex = 15;
            //
            //Page_Account
            //
            this.Controls.Add(this.BalanceInformation1);
            this.Controls.Add(this.interest_this_creditor);
            this.Controls.Add(this.payments_this_creditor);
            this.Controls.Add(this.LabelControl18);
            this.Controls.Add(this.LabelControl17);
            this.Controls.Add(this.GroupControl1);
            this.Controls.Add(this.dmp_payout_interest);
            this.Controls.Add(this.LabelControl13);
            this.Controls.Add(this.percent_balance);
            this.Controls.Add(this.date_disp_changed);
            this.Controls.Add(this.dmp_interest);
            this.Controls.Add(this.sched_payment);
            this.Controls.Add(this.disbursement_factor);
            this.Controls.Add(this.message);
            this.Controls.Add(this.account_number);
            this.Controls.Add(this.LabelControl10);
            this.Controls.Add(this.LabelControl9);
            this.Controls.Add(this.LabelControl8);
            this.Controls.Add(this.LabelControl7);
            this.Controls.Add(this.LabelControl6);
            this.Controls.Add(this.expected_payout_date);
            this.Controls.Add(this.last_payment);
            this.Controls.Add(this.payments_month_1);
            this.Controls.Add(this.payments_month_0);
            this.Controls.Add(this.orig_dmp_payment);
            this.Controls.Add(this.LabelControl5);
            this.Controls.Add(this.LabelControl4);
            this.Controls.Add(this.LabelControl3);
            this.Controls.Add(this.LabelControl2);
            this.Controls.Add(this.LabelControl1);
            this.Name = "Page_Account";
            this.Size = new System.Drawing.Size(568, 272);
            ((System.ComponentModel.ISupportInitialize)this.account_number.Properties).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.message.Properties).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.disbursement_factor.Properties).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.sched_payment.Properties).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.dmp_interest.Properties).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.dmp_payout_interest.Properties).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.GroupControl1).EndInit();
            this.GroupControl1.ResumeLayout(false);
            this.GroupControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)this.non_dmp_interest.Properties).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.non_dmp_payment.Properties).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.last_payment_date_b4_dmp.Properties.VistaTimeProperties).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.last_payment_date_b4_dmp.Properties).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();
        }
        internal DevExpress.XtraEditors.TextEdit account_number;
        internal BalanceInformation BalanceInformation1;
        internal DevExpress.XtraEditors.LabelControl date_disp_changed;
        internal DevExpress.XtraEditors.CalcEdit disbursement_factor;
        internal DebtPlus.Data.Controls.PercentEdit dmp_interest;
        internal DebtPlus.Data.Controls.PercentEdit dmp_payout_interest;
        internal DevExpress.XtraEditors.LabelControl expected_payout_date;
        internal DevExpress.XtraEditors.GroupControl GroupControl1;
        internal DevExpress.XtraEditors.LabelControl interest_this_creditor;
        internal DevExpress.XtraEditors.LabelControl LabelControl1;
        internal DevExpress.XtraEditors.LabelControl LabelControl10;
        internal DevExpress.XtraEditors.LabelControl LabelControl13;
        internal DevExpress.XtraEditors.LabelControl LabelControl14;
        internal DevExpress.XtraEditors.LabelControl LabelControl15;
        internal DevExpress.XtraEditors.LabelControl LabelControl16;
        internal DevExpress.XtraEditors.LabelControl LabelControl17;
        internal DevExpress.XtraEditors.LabelControl LabelControl18;
        internal DevExpress.XtraEditors.LabelControl LabelControl2;
        internal DevExpress.XtraEditors.LabelControl LabelControl3;
        internal DevExpress.XtraEditors.LabelControl LabelControl4;
        internal DevExpress.XtraEditors.LabelControl LabelControl5;
        internal DevExpress.XtraEditors.LabelControl LabelControl6;
        internal DevExpress.XtraEditors.LabelControl LabelControl7;
        internal DevExpress.XtraEditors.LabelControl LabelControl8;
        internal DevExpress.XtraEditors.LabelControl LabelControl9;
        internal DevExpress.XtraEditors.LabelControl last_payment;
        internal DevExpress.XtraEditors.DateEdit last_payment_date_b4_dmp;
        internal DevExpress.XtraEditors.TextEdit message;
        internal DebtPlus.Data.Controls.PercentEdit non_dmp_interest;
        internal DevExpress.XtraEditors.CalcEdit non_dmp_payment;
        internal DevExpress.XtraEditors.LabelControl orig_dmp_payment;
        internal DevExpress.XtraEditors.LabelControl payments_month_0;
        internal DevExpress.XtraEditors.LabelControl payments_month_1;
        internal DevExpress.XtraEditors.LabelControl payments_this_creditor;
        internal DevExpress.XtraEditors.LabelControl percent_balance;
        internal DevExpress.XtraEditors.CalcEdit sched_payment;

        #endregion " Windows Form Designer generated code "
    }
}
