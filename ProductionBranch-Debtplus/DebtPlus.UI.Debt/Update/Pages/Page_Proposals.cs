#region "Copyright 2000-2012 DebtPlus, L.L.C."

//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}

#endregion "Copyright 2000-2012 DebtPlus, L.L.C."

using System;
using System.ComponentModel;
using System.Linq;
using DebtPlus.LINQ;
using DebtPlus.UI.Debt.Update.controls;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Controls;

namespace DebtPlus.UI.Debt.Update.pages
{
    internal partial class Page_Proposals : DebtPlus.UI.Debt.Update.controls.ControlBaseDebt
    {
        public Page_Proposals()
            : base()
        {
            InitializeComponent();
        }

        private DebtPlus.Interfaces.Debt.IDebtRecord currentDebtRecord;
        public override void ReadForm(DebtPlus.Interfaces.Debt.IDebtRecord CurrentDebtRecord)
        {
            this.currentDebtRecord = CurrentDebtRecord;
            UnRegisterHandlers();
            try
            {
                // Ask the proposal list to supply the data
                ProposalsList1.ReadForm(currentDebtRecord);

                // Prenote date may be null
                if (currentDebtRecord.prenote_date == null || System.Object.Equals(currentDebtRecord.prenote_date, System.DBNull.Value))
                {
                    prenote_date.Text = "NOT PRENOTED";
                }
                else
                {
                    prenote_date.Text = Convert.ToDateTime(currentDebtRecord.prenote_date).ToShortDateString();
                }

                // Define the values for the combobox/drop-down controls
                send_bal_verify.Properties.DataSource            = DebtPlus.LINQ.InMemory.BalanceVerificationStatusTypes.getList();
                rpps_client_type_indicator.Properties.DataSource = DebtPlus.LINQ.InMemory.rpps_client_type_indicators.getList();
                drop_reason.Properties.DataSource                = DebtPlus.LINQ.Cache.drop_reason.getList();

                // Find the client's start date if one is given.
                using (var bc = new BusinessContext())
                {
                    System.DateTime? defaultDate = bc.clients.Where(s => s.Id == currentDebtRecord.ClientId).Select(s => s.start_date).FirstOrDefault();
                    start_date.Properties.NullText = (defaultDate.HasValue) ? string.Format("{0:d}", defaultDate.Value) : string.Empty;
                }

                // Load the editing controls with the record contents
                hold_disbursements.Checked                       = currentDebtRecord.hold_disbursements;
                student_loan_release.Checked                     = currentDebtRecord.student_loan_release;
                balance_verification_release.Checked             = DebtPlus.Utils.Nulls.v_Boolean(currentDebtRecord.balance_verification_release).GetValueOrDefault();
                irs_form_on_file.Checked                         = currentDebtRecord.irs_form_on_file;

                send_bal_verify.EditValue                        = DebtPlus.Utils.Nulls.v_Int32(currentDebtRecord.send_bal_verify);
                fairshare_pct_check.EditValue                    = DebtPlus.Utils.Nulls.v_Double(currentDebtRecord.fairshare_pct_check);
                fairshare_pct_eft.EditValue                      = DebtPlus.Utils.Nulls.v_Double(currentDebtRecord.fairshare_pct_eft);
                percent_balance.EditValue                        = DebtPlus.Utils.Nulls.v_String(currentDebtRecord.percent_balance);
                terms.EditValue                                  = DebtPlus.Utils.Nulls.v_Int32(currentDebtRecord.terms);
                last_stmt_date.EditValue                         = DebtPlus.Utils.Nulls.v_DateTime(currentDebtRecord.last_stmt_date);
                start_date.EditValue                             = DebtPlus.Utils.Nulls.v_DateTime(currentDebtRecord.start_date);
                drop_reason.EditValue                            = DebtPlus.Utils.Nulls.v_Int32(currentDebtRecord.drop_reason);
                rpps_client_type_indicator.EditValue             = DebtPlus.Utils.Nulls.v_String(currentDebtRecord.rpps_client_type_indicator);

                rpps_client_type_indicator.Properties.PopupWidth = rpps_client_type_indicator.Width + 100;
                send_bal_verify.Properties.SortColumnIndex       = 1;
            }
            finally
            {
                RegisterHandlers();
            }

            // Lock/unlock the percent balance information
            ChangePercentBalance();
        }

        public override void SaveForm()
        {
            // Pass the save event along to the proposal information
            ProposalsList1.SaveForm();

            // Retrieve the values from the editing controls
            currentDebtRecord.hold_disbursements           = hold_disbursements.Checked;
            currentDebtRecord.student_loan_release         = student_loan_release.Checked;
            currentDebtRecord.irs_form_on_file             = irs_form_on_file.Checked;
            currentDebtRecord.balance_verification_release = balance_verification_release.Checked;
            currentDebtRecord.send_bal_verify              = DebtPlus.Utils.Nulls.v_Int32(send_bal_verify.EditValue).GetValueOrDefault();
            currentDebtRecord.percent_balance              = DebtPlus.Utils.Nulls.v_Double(percent_balance.EditValue).GetValueOrDefault();
            currentDebtRecord.terms                        = DebtPlus.Utils.Nulls.v_Int32(terms.EditValue).GetValueOrDefault();
            currentDebtRecord.fairshare_pct_check          = DebtPlus.Utils.Nulls.ToDbNull(DebtPlus.Utils.Nulls.v_Double(fairshare_pct_check.EditValue));
            currentDebtRecord.fairshare_pct_eft            = DebtPlus.Utils.Nulls.ToDbNull(DebtPlus.Utils.Nulls.v_Double(fairshare_pct_eft.EditValue));
            currentDebtRecord.last_stmt_date               = DebtPlus.Utils.Nulls.ToDbNull(DebtPlus.Utils.Nulls.v_DateTime(last_stmt_date.EditValue));
            currentDebtRecord.start_date                   = DebtPlus.Utils.Nulls.ToDbNull(DebtPlus.Utils.Nulls.v_DateTime(start_date.EditValue));
            currentDebtRecord.drop_reason                  = DebtPlus.Utils.Nulls.ToDbNull(DebtPlus.Utils.Nulls.v_Int32(drop_reason.EditValue));
            currentDebtRecord.rpps_client_type_indicator   = DebtPlus.Utils.Nulls.ToDbNull(DebtPlus.Utils.Nulls.v_String(rpps_client_type_indicator.EditValue));
        }

        private void ChangePercentBalance()
        {
            DebtPlus.LINQ.InMemory.rpps_client_type_indicator item = rpps_client_type_indicator.GetSelectedDataRow() as DebtPlus.LINQ.InMemory.rpps_client_type_indicator;
            if (item != null)
            {
                percent_balance.Enabled = item.enabled;
                if (item.rate != 0.0)
                {
                    percent_balance.EditValue = item.rate;
                }
            }
        }

        private void drop_reason_EditValueChanged(object sender, EventArgs e)
        {
            if (drop_reason.EditValue != null)
            {
                if (Convert.ToInt32(drop_reason.EditValue) == 0)
                {
                    drop_reason.EditValue = null;
                }
            }
        }

        private void percent_balance_EditValueChanging(object sender, ChangingEventArgs e)
        {
            double Value = 0.0;
            try
            {
                if (e.NewValue != null)
                {
                    if (!(e.NewValue is string))
                    {
                        Value = Convert.ToDouble(e.NewValue);
                    }
                }
            }
            catch { }

            if (Value < 0.0 || Value > 100.0)
            {
                e.Cancel = true;
            }
        }

        private void RegisterHandlers()
        {
            percent_balance.EditValueChanging           += percent_balance_EditValueChanging;
            terms.Validating                            += terms_Validating;
            drop_reason.EditValueChanged                += drop_reason_EditValueChanged;
            rpps_client_type_indicator.EditValueChanged += rpps_client_type_indicator_EditValueChanged;
            send_bal_verify.EditValueChanging           += send_bal_verify_EditValueChanging;
        }

        private void UnRegisterHandlers()
        {
            percent_balance.EditValueChanging           -= percent_balance_EditValueChanging;
            terms.Validating                            -= terms_Validating;
            drop_reason.EditValueChanged                -= drop_reason_EditValueChanged;
            rpps_client_type_indicator.EditValueChanged -= rpps_client_type_indicator_EditValueChanged;
            send_bal_verify.EditValueChanging           -= send_bal_verify_EditValueChanging;
        }

        private void rpps_client_type_indicator_EditValueChanged(object sender, EventArgs e)
        {
            ChangePercentBalance();
        }

        private void send_bal_verify_EditValueChanging(object sender, ChangingEventArgs e)
        {
            string ErrorMessage = string.Empty;
            switch (Convert.ToInt32(e.NewValue))
            {
                case 0:
                case 1:
                    break;

                case 2:    // These are the normal status values
                    break;

                case 3:
                case 4:
                case 5:    // A debt is created with this status. It may be set back if needed.
                    e.Cancel = true;
                    send_bal_verify.ErrorText = "Invalid state";
                    break;

                default:
                    break;
            }

            // Set the error status accordingly.
            send_bal_verify.ErrorText = ErrorMessage;
            if (ErrorMessage != string.Empty)
            {
                e.Cancel = true;
            }
        }

        private void terms_Validating(object sender, CancelEventArgs e)
        {
            string ErrorMessage = string.Empty;

            var ctl = (TextEdit)sender;
            Int32 Value = 0;
            try
            {
                if (ctl.EditValue != null)
                {
                    if (!(ctl.EditValue is string))
                    {
                        Value = Convert.ToInt32(ctl.EditValue);
                    }
                }
            }
            catch { }

            if (Value < 0)
            {
                ErrorMessage = "Minimum value is 0";
            }
            else if (Value > 60)
            {
                ErrorMessage = "Maximum value is 60";
            }

            if (ErrorMessage != string.Empty)
            {
                e.Cancel = true;
            }

            ctl.ErrorText = ErrorMessage;
        }

        #region " Windows Form Designer generated code "

        internal DevExpress.XtraEditors.CheckEdit balance_verification_release;
        internal DevExpress.XtraEditors.LookUpEdit drop_reason;
        internal DevExpress.XtraLayout.EmptySpaceItem EmptySpaceItem1;
        internal DebtPlus.Data.Controls.PercentEdit fairshare_pct_check;
        internal DebtPlus.Data.Controls.PercentEdit fairshare_pct_eft;
        internal DevExpress.XtraEditors.CheckEdit hold_disbursements;
        internal DevExpress.XtraEditors.CheckEdit irs_form_on_file;
        internal DevExpress.XtraEditors.DateEdit last_stmt_date;
        internal DevExpress.XtraLayout.LayoutControl LayoutControl1;
        internal DevExpress.XtraLayout.LayoutControlGroup LayoutControlGroup1;
        internal DevExpress.XtraLayout.LayoutControlGroup LayoutControlGroup2;
        internal DevExpress.XtraLayout.LayoutControlGroup LayoutControlGroup3;
        internal DevExpress.XtraLayout.LayoutControlGroup LayoutControlGroup4;
        internal DevExpress.XtraLayout.LayoutControlItem LayoutControlItem1;
        internal DevExpress.XtraLayout.LayoutControlItem LayoutControlItem10;
        internal DevExpress.XtraLayout.LayoutControlItem LayoutControlItem11;
        internal DevExpress.XtraLayout.LayoutControlItem LayoutControlItem12;
        internal DevExpress.XtraLayout.LayoutControlItem LayoutControlItem13;
        internal DevExpress.XtraLayout.LayoutControlItem LayoutControlItem14;
        internal DevExpress.XtraLayout.LayoutControlItem LayoutControlItem15;
        internal DevExpress.XtraLayout.LayoutControlItem LayoutControlItem2;
        internal DevExpress.XtraLayout.LayoutControlItem LayoutControlItem3;
        internal DevExpress.XtraLayout.LayoutControlItem LayoutControlItem4;
        internal DevExpress.XtraLayout.LayoutControlItem LayoutControlItem5;
        internal DevExpress.XtraLayout.LayoutControlItem LayoutControlItem6;
        internal DevExpress.XtraLayout.LayoutControlItem LayoutControlItem7;
        internal DevExpress.XtraLayout.LayoutControlItem LayoutControlItem8;
        internal DevExpress.XtraLayout.LayoutControlItem LayoutControlItem9;
        internal DebtPlus.Data.Controls.PercentEdit percent_balance;
        internal DevExpress.XtraEditors.LabelControl prenote_date;
        internal ProposalsList ProposalsList1;
        internal DevExpress.XtraEditors.LookUpEdit rpps_client_type_indicator;
        internal DevExpress.XtraEditors.LookUpEdit send_bal_verify;
        internal DevExpress.XtraEditors.DateEdit start_date;
        internal DevExpress.XtraEditors.CheckEdit student_loan_release;
        internal DevExpress.XtraEditors.CalcEdit terms;

        //NOTE: The following procedure is required by the Windows Form Designer
        //It can be modified using the Windows Form Designer.
        //Do not modify it using the code editor.
        private System.ComponentModel.IContainer components = null;

        //Form overrides dispose to clean up the component list.
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (components != null)
                {
                    components.Dispose();
                }
            }
            base.Dispose(disposing);
        }

        //Required by the Windows Form Designer
        [System.Diagnostics.DebuggerStepThrough()]
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Page_Proposals));
            this.components = new System.ComponentModel.Container();
            DevExpress.Utils.SuperToolTip SuperToolTip1 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem ToolTipTitleItem1 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem ToolTipItem1 = new DevExpress.Utils.ToolTipItem();
            this.prenote_date = new DevExpress.XtraEditors.LabelControl();
            this.LayoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.fairshare_pct_eft = new DebtPlus.Data.Controls.PercentEdit();
            this.start_date = new DevExpress.XtraEditors.DateEdit();
            this.rpps_client_type_indicator = new DevExpress.XtraEditors.LookUpEdit();
            this.last_stmt_date = new DevExpress.XtraEditors.DateEdit();
            this.balance_verification_release = new DevExpress.XtraEditors.CheckEdit();
            this.terms = new DevExpress.XtraEditors.CalcEdit();
            this.drop_reason = new DevExpress.XtraEditors.LookUpEdit();
            this.percent_balance = new DebtPlus.Data.Controls.PercentEdit();
            this.hold_disbursements = new DevExpress.XtraEditors.CheckEdit();
            this.fairshare_pct_check = new DebtPlus.Data.Controls.PercentEdit();
            this.student_loan_release = new DevExpress.XtraEditors.CheckEdit();
            this.irs_form_on_file = new DevExpress.XtraEditors.CheckEdit();
            this.ProposalsList1 = new ProposalsList();
            this.components.Add(this.ProposalsList1);
            this.send_bal_verify = new DevExpress.XtraEditors.LookUpEdit();
            this.LayoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.LayoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.LayoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.LayoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.LayoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.LayoutControlItem15 = new DevExpress.XtraLayout.LayoutControlItem();
            this.LayoutControlGroup3 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.LayoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.LayoutControlItem6 = new DevExpress.XtraLayout.LayoutControlItem();
            this.LayoutControlGroup4 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.LayoutControlItem10 = new DevExpress.XtraLayout.LayoutControlItem();
            this.LayoutControlItem11 = new DevExpress.XtraLayout.LayoutControlItem();
            this.LayoutControlItem12 = new DevExpress.XtraLayout.LayoutControlItem();
            this.LayoutControlItem13 = new DevExpress.XtraLayout.LayoutControlItem();
            this.LayoutControlItem14 = new DevExpress.XtraLayout.LayoutControlItem();
            this.LayoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.EmptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.LayoutControlItem9 = new DevExpress.XtraLayout.LayoutControlItem();
            this.LayoutControlItem8 = new DevExpress.XtraLayout.LayoutControlItem();
            this.LayoutControlItem7 = new DevExpress.XtraLayout.LayoutControlItem();
            ((System.ComponentModel.ISupportInitialize)this.LayoutControl1).BeginInit();
            this.LayoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)this.fairshare_pct_eft.Properties).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.start_date.Properties.VistaTimeProperties).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.start_date.Properties).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.rpps_client_type_indicator.Properties).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.last_stmt_date.Properties.VistaTimeProperties).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.last_stmt_date.Properties).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.balance_verification_release.Properties).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.terms.Properties).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.drop_reason.Properties).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.percent_balance.Properties).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.hold_disbursements.Properties).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.fairshare_pct_check.Properties).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.student_loan_release.Properties).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.irs_form_on_file.Properties).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.send_bal_verify.Properties).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.LayoutControlGroup1).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.LayoutControlGroup2).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.LayoutControlItem2).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.LayoutControlItem3).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.LayoutControlItem4).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.LayoutControlItem15).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.LayoutControlGroup3).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.LayoutControlItem5).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.LayoutControlItem6).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.LayoutControlGroup4).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.LayoutControlItem10).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.LayoutControlItem11).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.LayoutControlItem12).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.LayoutControlItem13).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.LayoutControlItem14).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.LayoutControlItem1).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.EmptySpaceItem1).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.LayoutControlItem9).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.LayoutControlItem8).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.LayoutControlItem7).BeginInit();
            this.SuspendLayout();
            //
            //prenote_date
            //
            this.prenote_date.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.Horizontal;
            this.prenote_date.Location = new System.Drawing.Point(103, 358);
            this.prenote_date.Name = "prenote_date";
            this.prenote_date.Size = new System.Drawing.Size(77, 13);
            this.prenote_date.StyleController = this.LayoutControl1;
            this.prenote_date.TabIndex = 13;
            this.prenote_date.Text = "NOT PRENOTED";
            this.prenote_date.UseMnemonic = false;
            //
            //LayoutControl1
            //
            this.LayoutControl1.Controls.Add(this.fairshare_pct_eft);
            this.LayoutControl1.Controls.Add(this.start_date);
            this.LayoutControl1.Controls.Add(this.rpps_client_type_indicator);
            this.LayoutControl1.Controls.Add(this.last_stmt_date);
            this.LayoutControl1.Controls.Add(this.balance_verification_release);
            this.LayoutControl1.Controls.Add(this.terms);
            this.LayoutControl1.Controls.Add(this.drop_reason);
            this.LayoutControl1.Controls.Add(this.percent_balance);
            this.LayoutControl1.Controls.Add(this.hold_disbursements);
            this.LayoutControl1.Controls.Add(this.fairshare_pct_check);
            this.LayoutControl1.Controls.Add(this.student_loan_release);
            this.LayoutControl1.Controls.Add(this.irs_form_on_file);
            this.LayoutControl1.Controls.Add(this.ProposalsList1);
            this.LayoutControl1.Controls.Add(this.prenote_date);
            this.LayoutControl1.Controls.Add(this.send_bal_verify);
            this.LayoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.LayoutControl1.Location = new System.Drawing.Point(0, 0);
            this.LayoutControl1.Name = "LayoutControl1";
            this.LayoutControl1.Root = this.LayoutControlGroup1;
            this.LayoutControl1.Size = new System.Drawing.Size(576, 392);
            this.LayoutControl1.TabIndex = 17;
            this.LayoutControl1.Text = "LayoutControl1";
            //
            //fairshare_pct_eft
            //
            this.fairshare_pct_eft.Location = new System.Drawing.Point(502, 178);
            this.fairshare_pct_eft.Name = "fairshare_pct_eft";
            this.fairshare_pct_eft.Properties.Allow100Percent = false;
            this.fairshare_pct_eft.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.fairshare_pct_eft.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] { new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo) });
            this.fairshare_pct_eft.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.fairshare_pct_eft.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.fairshare_pct_eft.Properties.Precision = 3;
            this.fairshare_pct_eft.Size = new System.Drawing.Size(50, 20);
            this.fairshare_pct_eft.StyleController = this.LayoutControl1;
            this.fairshare_pct_eft.TabIndex = 3;
            //
            //start_date
            //
            this.start_date.EditValue = null;
            this.start_date.Location = new System.Drawing.Point(474, 322);
            this.start_date.Name = "start_date";
            this.start_date.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.start_date.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] { new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo) });
            this.start_date.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] { new DevExpress.XtraEditors.Controls.EditorButton() });
            this.start_date.Size = new System.Drawing.Size(78, 20);
            this.start_date.StyleController = this.LayoutControl1;
            this.start_date.TabIndex = 11;
            //
            //rpps_client_type_indicator
            //
            this.rpps_client_type_indicator.Location = new System.Drawing.Point(383, 226);
            this.rpps_client_type_indicator.Name = "rpps_client_type_indicator";
            this.rpps_client_type_indicator.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] { new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo) });
            this.rpps_client_type_indicator.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
				new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Id", 5, "ID"),
				new DevExpress.XtraEditors.Controls.LookUpColumnInfo("description", "description", 20, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Default, DevExpress.Data.ColumnSortOrder.Ascending)
			});
            this.rpps_client_type_indicator.Properties.DisplayMember = "description";
            this.rpps_client_type_indicator.Properties.NullText = "";
            this.rpps_client_type_indicator.Properties.ShowFooter = false;
            this.rpps_client_type_indicator.Properties.ValueMember = "Id";
            this.rpps_client_type_indicator.Size = new System.Drawing.Size(169, 20);
            this.rpps_client_type_indicator.StyleController = this.LayoutControl1;
            this.rpps_client_type_indicator.TabIndex = 3;
            this.rpps_client_type_indicator.Properties.SortColumnIndex = 1;
            //
            //last_stmt_date
            //
            this.last_stmt_date.EditValue = null;
            this.last_stmt_date.Location = new System.Drawing.Point(474, 298);
            this.last_stmt_date.Name = "last_stmt_date";
            this.last_stmt_date.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.last_stmt_date.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] { new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo) });
            this.last_stmt_date.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] { new DevExpress.XtraEditors.Controls.EditorButton() });
            this.last_stmt_date.Size = new System.Drawing.Size(78, 20);
            this.last_stmt_date.StyleController = this.LayoutControl1;
            this.last_stmt_date.TabIndex = 9;
            //
            //balance_verification_release
            //
            this.balance_verification_release.Location = new System.Drawing.Point(383, 66);
            this.balance_verification_release.Name = "balance_verification_release";
            this.balance_verification_release.Properties.Caption = "Balance Verify Release on File";
            this.balance_verification_release.Size = new System.Drawing.Size(169, 19);
            this.balance_verification_release.StyleController = this.LayoutControl1;
            this.balance_verification_release.TabIndex = 17;
            //
            //terms
            //
            this.terms.Location = new System.Drawing.Point(474, 274);
            this.terms.Name = "terms";
            this.terms.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.terms.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] { new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo) });
            this.terms.Properties.DisplayFormat.FormatString = "f0";
            this.terms.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.terms.Properties.EditFormat.FormatString = "f0";
            this.terms.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.terms.Properties.Mask.BeepOnError = true;
            this.terms.Properties.Mask.EditMask = "f0";
            this.terms.Properties.Precision = 0;
            this.terms.Size = new System.Drawing.Size(78, 20);
            this.terms.StyleController = this.LayoutControl1;
            this.terms.TabIndex = 7;
            //
            //drop_reason
            //
            this.drop_reason.Location = new System.Drawing.Point(103, 334);
            this.drop_reason.Name = "drop_reason";
            this.drop_reason.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.drop_reason.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] { new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo) });
            this.drop_reason.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
				new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Id", "ID", 20, DevExpress.Utils.FormatType.None, "", false, DevExpress.Utils.HorzAlignment.Default),
				new DevExpress.XtraEditors.Controls.LookUpColumnInfo("description", "Description", 20, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Default, DevExpress.Data.ColumnSortOrder.Ascending)
			});
            this.drop_reason.Properties.NullText = "";
            this.drop_reason.Properties.ShowFooter = false;
            this.drop_reason.Properties.ShowHeader = false;
            this.drop_reason.Properties.ShowLines = false;
            this.drop_reason.Size = new System.Drawing.Size(264, 20);
            this.drop_reason.StyleController = this.LayoutControl1;
            this.drop_reason.TabIndex = 16;
            this.drop_reason.Properties.SortColumnIndex = 1;
            this.drop_reason.Properties.DisplayMember = "description";
            this.drop_reason.Properties.ValueMember = "Id";
            //
            //percent_balance
            //
            this.percent_balance.Location = new System.Drawing.Point(474, 250);
            this.percent_balance.Name = "percent_balance";
            this.percent_balance.Properties.Allow100Percent = true;
            this.percent_balance.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.percent_balance.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] { new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo) });
            this.percent_balance.Properties.DisplayFormat.FormatString = "{0:p3}";
            this.percent_balance.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.percent_balance.Properties.EditFormat.FormatString = "{0:p3}";
            this.percent_balance.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.percent_balance.Properties.Mask.BeepOnError = true;
            this.percent_balance.Properties.Mask.EditMask = "p3";
            this.percent_balance.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.percent_balance.Properties.Precision = 3;
            this.percent_balance.Size = new System.Drawing.Size(78, 20);
            this.percent_balance.StyleController = this.LayoutControl1;
            this.percent_balance.TabIndex = 5;
            //
            //hold_disbursements
            //
            this.hold_disbursements.Location = new System.Drawing.Point(383, 43);
            this.hold_disbursements.Name = "hold_disbursements";
            this.hold_disbursements.Properties.Caption = "Hold All Disbursements";
            this.hold_disbursements.Size = new System.Drawing.Size(169, 19);
            this.hold_disbursements.StyleController = this.LayoutControl1;
            this.hold_disbursements.TabIndex = 0;
            //
            //fairshare_pct_check
            //
            this.fairshare_pct_check.Location = new System.Drawing.Point(421, 178);
            this.fairshare_pct_check.Name = "fairshare_pct_check";
            this.fairshare_pct_check.Properties.Allow100Percent = false;
            this.fairshare_pct_check.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.fairshare_pct_check.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] { new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo) });
            this.fairshare_pct_check.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.fairshare_pct_check.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.fairshare_pct_check.Properties.Precision = 3;
            this.fairshare_pct_check.Size = new System.Drawing.Size(50, 20);
            this.fairshare_pct_check.StyleController = this.LayoutControl1;
            this.fairshare_pct_check.TabIndex = 1;
            //
            //student_loan_release
            //
            this.student_loan_release.Location = new System.Drawing.Point(383, 89);
            this.student_loan_release.Name = "student_loan_release";
            this.student_loan_release.Properties.Caption = "Student Loan Release on File";
            this.student_loan_release.Size = new System.Drawing.Size(169, 19);
            this.student_loan_release.StyleController = this.LayoutControl1;
            this.student_loan_release.TabIndex = 1;
            //
            //irs_form_on_file
            //
            this.irs_form_on_file.Location = new System.Drawing.Point(383, 112);
            this.irs_form_on_file.Name = "irs_form_on_file";
            this.irs_form_on_file.Properties.Caption = "IRS Form on File";
            this.irs_form_on_file.Size = new System.Drawing.Size(169, 19);
            this.irs_form_on_file.StyleController = this.LayoutControl1;
            this.irs_form_on_file.TabIndex = 2;
            //
            //ProposalsList1
            //
            this.ProposalsList1.Location = new System.Drawing.Point(12, 12);
            this.ProposalsList1.Name = "ProposalsList1";
            this.ProposalsList1.Size = new System.Drawing.Size(355, 296);
            this.ProposalsList1.TabIndex = 0;
            //
            //SuperToolTip1
            //
            SuperToolTip1.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            ToolTipTitleItem1.Text = "Balance Verification Status";
            ToolTipItem1.LeftIndent = 6;
            ToolTipItem1.Text = resources.GetString("ToolTipItem1.Text");
            SuperToolTip1.Items.Add(ToolTipTitleItem1);
            SuperToolTip1.Items.Add(ToolTipItem1);
            //
            //send_bal_verify
            //
            this.send_bal_verify.EditValue = false;
            this.send_bal_verify.Location = new System.Drawing.Point(103, 312);
            this.send_bal_verify.Name = "send_bal_verify";
            this.send_bal_verify.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.False;
            this.send_bal_verify.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.send_bal_verify.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] { new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo) });
            this.send_bal_verify.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
				new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Id", "ID", 20, DevExpress.Utils.FormatType.None, "f0", false, DevExpress.Utils.HorzAlignment.Default),
				new DevExpress.XtraEditors.Controls.LookUpColumnInfo("description", "Description", 65, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Default, DevExpress.Data.ColumnSortOrder.Ascending),
				new DevExpress.XtraEditors.Controls.LookUpColumnInfo("SuspendProposals", "Hold Proposals?", 35, DevExpress.Utils.FormatType.Numeric, "Yes;Yes;No", true, DevExpress.Utils.HorzAlignment.Default)
			});
            this.send_bal_verify.Properties.DisplayMember = "description";
            this.send_bal_verify.Properties.NullText = "";
            this.send_bal_verify.Properties.ShowFooter = false;
            this.send_bal_verify.Properties.ValueMember = "Id";
            this.send_bal_verify.Size = new System.Drawing.Size(264, 18);
            this.send_bal_verify.StyleController = this.LayoutControl1;
            this.send_bal_verify.TabIndex = 14;
            this.send_bal_verify.Properties.SortColumnIndex = 1;
            this.send_bal_verify.SuperTip = SuperToolTip1;
            //
            //LayoutControlGroup1
            //
            this.LayoutControlGroup1.CustomizationFormText = "LayoutControlGroup1";
            this.LayoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.LayoutControlGroup1.GroupBordersVisible = false;
            this.LayoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
				this.LayoutControlGroup2,
				this.LayoutControlGroup3,
				this.LayoutControlGroup4,
				this.LayoutControlItem1,
				this.EmptySpaceItem1,
				this.LayoutControlItem9,
				this.LayoutControlItem8,
				this.LayoutControlItem7
			});
            this.LayoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.LayoutControlGroup1.Name = "LayoutControlGroup1";
            this.LayoutControlGroup1.Size = new System.Drawing.Size(576, 392);
            this.LayoutControlGroup1.Text = "LayoutControlGroup1";
            this.LayoutControlGroup1.TextVisible = false;
            //
            //LayoutControlGroup2
            //
            this.LayoutControlGroup2.CustomizationFormText = "Various Indicators";
            this.LayoutControlGroup2.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
				this.LayoutControlItem2,
				this.LayoutControlItem3,
				this.LayoutControlItem4,
				this.LayoutControlItem15
			});
            this.LayoutControlGroup2.Location = new System.Drawing.Point(359, 0);
            this.LayoutControlGroup2.Name = "LayoutControlGroup2";
            this.LayoutControlGroup2.Size = new System.Drawing.Size(197, 135);
            this.LayoutControlGroup2.Text = "Various Indicators";
            //
            //LayoutControlItem2
            //
            this.LayoutControlItem2.Control = this.irs_form_on_file;
            this.LayoutControlItem2.CustomizationFormText = "LayoutControlItem2";
            this.LayoutControlItem2.Location = new System.Drawing.Point(0, 69);
            this.LayoutControlItem2.Name = "LayoutControlItem2";
            this.LayoutControlItem2.Size = new System.Drawing.Size(173, 23);
            this.LayoutControlItem2.Text = "LayoutControlItem2";
            this.LayoutControlItem2.TextSize = new System.Drawing.Size(0, 0);
            this.LayoutControlItem2.TextToControlDistance = 0;
            this.LayoutControlItem2.TextVisible = false;
            //
            //LayoutControlItem3
            //
            this.LayoutControlItem3.Control = this.student_loan_release;
            this.LayoutControlItem3.CustomizationFormText = "LayoutControlItem3";
            this.LayoutControlItem3.Location = new System.Drawing.Point(0, 46);
            this.LayoutControlItem3.Name = "LayoutControlItem3";
            this.LayoutControlItem3.Size = new System.Drawing.Size(173, 23);
            this.LayoutControlItem3.Text = "LayoutControlItem3";
            this.LayoutControlItem3.TextSize = new System.Drawing.Size(0, 0);
            this.LayoutControlItem3.TextToControlDistance = 0;
            this.LayoutControlItem3.TextVisible = false;
            //
            //LayoutControlItem4
            //
            this.LayoutControlItem4.Control = this.hold_disbursements;
            this.LayoutControlItem4.CustomizationFormText = "LayoutControlItem4";
            this.LayoutControlItem4.Location = new System.Drawing.Point(0, 0);
            this.LayoutControlItem4.Name = "LayoutControlItem4";
            this.LayoutControlItem4.Size = new System.Drawing.Size(173, 23);
            this.LayoutControlItem4.Text = "LayoutControlItem4";
            this.LayoutControlItem4.TextSize = new System.Drawing.Size(0, 0);
            this.LayoutControlItem4.TextToControlDistance = 0;
            this.LayoutControlItem4.TextVisible = false;
            //
            //LayoutControlItem15
            //
            this.LayoutControlItem15.Control = this.balance_verification_release;
            this.LayoutControlItem15.CustomizationFormText = "LayoutControlItem15";
            this.LayoutControlItem15.Location = new System.Drawing.Point(0, 23);
            this.LayoutControlItem15.Name = "LayoutControlItem15";
            this.LayoutControlItem15.Size = new System.Drawing.Size(173, 23);
            this.LayoutControlItem15.Text = "LayoutControlItem15";
            this.LayoutControlItem15.TextSize = new System.Drawing.Size(0, 0);
            this.LayoutControlItem15.TextToControlDistance = 0;
            this.LayoutControlItem15.TextVisible = false;
            //
            //LayoutControlGroup3
            //
            this.LayoutControlGroup3.CustomizationFormText = "Fairshare Percentages";
            this.LayoutControlGroup3.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
				this.LayoutControlItem5,
				this.LayoutControlItem6
			});
            this.LayoutControlGroup3.Location = new System.Drawing.Point(359, 135);
            this.LayoutControlGroup3.Name = "LayoutControlGroup3";
            this.LayoutControlGroup3.Size = new System.Drawing.Size(197, 67);
            this.LayoutControlGroup3.Text = "Fairshare Percentages";
            //
            //LayoutControlItem5
            //
            this.LayoutControlItem5.Control = this.fairshare_pct_check;
            this.LayoutControlItem5.CustomizationFormText = "Check";
            this.LayoutControlItem5.Location = new System.Drawing.Point(0, 0);
            this.LayoutControlItem5.Name = "LayoutControlItem5";
            this.LayoutControlItem5.Size = new System.Drawing.Size(92, 24);
            this.LayoutControlItem5.Text = "Check:";
            this.LayoutControlItem5.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize;
            this.LayoutControlItem5.TextSize = new System.Drawing.Size(33, 13);
            this.LayoutControlItem5.TextToControlDistance = 5;
            //
            //LayoutControlItem6
            //
            this.LayoutControlItem6.Control = this.fairshare_pct_eft;
            this.LayoutControlItem6.CustomizationFormText = "EFT";
            this.LayoutControlItem6.Location = new System.Drawing.Point(92, 0);
            this.LayoutControlItem6.Name = "LayoutControlItem6";
            this.LayoutControlItem6.Size = new System.Drawing.Size(81, 24);
            this.LayoutControlItem6.Text = "EFT:";
            this.LayoutControlItem6.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize;
            this.LayoutControlItem6.TextSize = new System.Drawing.Size(22, 13);
            this.LayoutControlItem6.TextToControlDistance = 5;
            //
            //LayoutControlGroup4
            //
            this.LayoutControlGroup4.CustomizationFormText = "LayoutControlGroup4";
            this.LayoutControlGroup4.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
				this.LayoutControlItem10,
				this.LayoutControlItem11,
				this.LayoutControlItem12,
				this.LayoutControlItem13,
				this.LayoutControlItem14
			});
            this.LayoutControlGroup4.Location = new System.Drawing.Point(359, 202);
            this.LayoutControlGroup4.Name = "LayoutControlGroup4";
            this.LayoutControlGroup4.Size = new System.Drawing.Size(197, 144);
            this.LayoutControlGroup4.Text = "LayoutControlGroup4";
            this.LayoutControlGroup4.TextVisible = false;
            //
            //LayoutControlItem10
            //
            this.LayoutControlItem10.Control = this.rpps_client_type_indicator;
            this.LayoutControlItem10.CustomizationFormText = "RPPS Client Status";
            this.LayoutControlItem10.Location = new System.Drawing.Point(0, 0);
            this.LayoutControlItem10.Name = "LayoutControlItem10";
            this.LayoutControlItem10.Size = new System.Drawing.Size(173, 24);
            this.LayoutControlItem10.Text = "RPPS Client Status";
            this.LayoutControlItem10.TextSize = new System.Drawing.Size(0, 0);
            this.LayoutControlItem10.TextToControlDistance = 0;
            this.LayoutControlItem10.TextVisible = false;
            //
            //LayoutControlItem11
            //
            this.LayoutControlItem11.Control = this.percent_balance;
            this.LayoutControlItem11.CustomizationFormText = "Proposed %";
            this.LayoutControlItem11.Location = new System.Drawing.Point(0, 24);
            this.LayoutControlItem11.Name = "LayoutControlItem11";
            this.LayoutControlItem11.Size = new System.Drawing.Size(173, 24);
            this.LayoutControlItem11.Text = "Proposed %:";
            this.LayoutControlItem11.TextSize = new System.Drawing.Size(87, 13);
            //
            //LayoutControlItem12
            //
            this.LayoutControlItem12.Control = this.terms;
            this.LayoutControlItem12.CustomizationFormText = "Term";
            this.LayoutControlItem12.Location = new System.Drawing.Point(0, 48);
            this.LayoutControlItem12.Name = "LayoutControlItem12";
            this.LayoutControlItem12.Size = new System.Drawing.Size(173, 24);
            this.LayoutControlItem12.Text = "Term:";
            this.LayoutControlItem12.TextSize = new System.Drawing.Size(87, 13);
            //
            //LayoutControlItem13
            //
            this.LayoutControlItem13.Control = this.last_stmt_date;
            this.LayoutControlItem13.CustomizationFormText = "Statement:";
            this.LayoutControlItem13.Location = new System.Drawing.Point(0, 72);
            this.LayoutControlItem13.Name = "LayoutControlItem13";
            this.LayoutControlItem13.Size = new System.Drawing.Size(173, 24);
            this.LayoutControlItem13.Text = "Statement:";
            this.LayoutControlItem13.TextSize = new System.Drawing.Size(87, 13);
            //
            //LayoutControlItem14
            //
            this.LayoutControlItem14.Control = this.start_date;
            this.LayoutControlItem14.CustomizationFormText = "Debt Start:";
            this.LayoutControlItem14.Location = new System.Drawing.Point(0, 96);
            this.LayoutControlItem14.Name = "LayoutControlItem14";
            this.LayoutControlItem14.Size = new System.Drawing.Size(173, 24);
            this.LayoutControlItem14.Text = "Debt Start:";
            this.LayoutControlItem14.TextSize = new System.Drawing.Size(87, 13);
            //
            //LayoutControlItem1
            //
            this.LayoutControlItem1.Control = this.ProposalsList1;
            this.LayoutControlItem1.CustomizationFormText = "Proposal List";
            this.LayoutControlItem1.Location = new System.Drawing.Point(0, 0);
            this.LayoutControlItem1.Name = "LayoutControlItem1";
            this.LayoutControlItem1.Size = new System.Drawing.Size(359, 300);
            this.LayoutControlItem1.Text = "Proposal List";
            this.LayoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.LayoutControlItem1.TextToControlDistance = 0;
            this.LayoutControlItem1.TextVisible = false;
            this.LayoutControlItem1.TrimClientAreaToControl = false;
            //
            //EmptySpaceItem1
            //
            this.EmptySpaceItem1.AllowHotTrack = false;
            this.EmptySpaceItem1.CustomizationFormText = "EmptySpaceItem1";
            this.EmptySpaceItem1.Location = new System.Drawing.Point(172, 346);
            this.EmptySpaceItem1.Name = "EmptySpaceItem1";
            this.EmptySpaceItem1.Size = new System.Drawing.Size(384, 26);
            this.EmptySpaceItem1.Text = "EmptySpaceItem1";
            this.EmptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            //
            //LayoutControlItem9
            //
            this.LayoutControlItem9.Control = this.drop_reason;
            this.LayoutControlItem9.CustomizationFormText = "Drop Reason";
            this.LayoutControlItem9.Location = new System.Drawing.Point(0, 322);
            this.LayoutControlItem9.Name = "LayoutControlItem9";
            this.LayoutControlItem9.Size = new System.Drawing.Size(359, 24);
            this.LayoutControlItem9.Text = "Drop Reason:";
            this.LayoutControlItem9.TextSize = new System.Drawing.Size(87, 13);
            //
            //LayoutControlItem8
            //
            this.LayoutControlItem8.Control = this.send_bal_verify;
            this.LayoutControlItem8.CustomizationFormText = "Balance Verification Status";
            this.LayoutControlItem8.Location = new System.Drawing.Point(0, 300);
            this.LayoutControlItem8.Name = "LayoutControlItem8";
            this.LayoutControlItem8.Size = new System.Drawing.Size(359, 22);
            this.LayoutControlItem8.Text = "Balance Verify:";
            this.LayoutControlItem8.TextSize = new System.Drawing.Size(87, 13);
            //
            //LayoutControlItem7
            //
            this.LayoutControlItem7.Control = this.prenote_date;
            this.LayoutControlItem7.CustomizationFormText = "Payment Prenote Date";
            this.LayoutControlItem7.Location = new System.Drawing.Point(0, 346);
            this.LayoutControlItem7.Name = "LayoutControlItem7";
            this.LayoutControlItem7.Size = new System.Drawing.Size(172, 26);
            this.LayoutControlItem7.Text = "Payment Prenote:";
            this.LayoutControlItem7.TextSize = new System.Drawing.Size(87, 13);
            //
            //Page_Proposals
            //
            this.Controls.Add(this.LayoutControl1);
            this.Name = "Page_Proposals";
            this.Size = new System.Drawing.Size(576, 392);
            ((System.ComponentModel.ISupportInitialize)this.LayoutControl1).EndInit();
            this.LayoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)this.fairshare_pct_eft.Properties).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.start_date.Properties.VistaTimeProperties).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.start_date.Properties).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.rpps_client_type_indicator.Properties).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.last_stmt_date.Properties.VistaTimeProperties).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.last_stmt_date.Properties).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.balance_verification_release.Properties).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.terms.Properties).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.drop_reason.Properties).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.percent_balance.Properties).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.hold_disbursements.Properties).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.fairshare_pct_check.Properties).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.student_loan_release.Properties).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.irs_form_on_file.Properties).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.send_bal_verify.Properties).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.LayoutControlGroup1).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.LayoutControlGroup2).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.LayoutControlItem2).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.LayoutControlItem3).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.LayoutControlItem4).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.LayoutControlItem15).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.LayoutControlGroup3).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.LayoutControlItem5).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.LayoutControlItem6).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.LayoutControlGroup4).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.LayoutControlItem10).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.LayoutControlItem11).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.LayoutControlItem12).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.LayoutControlItem13).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.LayoutControlItem14).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.LayoutControlItem1).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.EmptySpaceItem1).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.LayoutControlItem9).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.LayoutControlItem8).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.LayoutControlItem7).EndInit();
            this.ResumeLayout(false);
        }
        #endregion " Windows Form Designer generated code "
    }
}
